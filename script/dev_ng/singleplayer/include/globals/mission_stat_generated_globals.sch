

CONST_INT MAX_TRACKED_MISSION_STATS 16
ENUM ENUM_MISSION_STATS
	UNSET_MISSION_STAT_ENUM = -1,
	// stats for mission SP_MISSION_ARMENIAN_1
	ARM1_NOSCRATCH, // Not a scratch   (Player repo car takes less than 10% damage during mission)
	ARM1_CAR_CHOSEN, // Which car did the player choose at the start of the mission? ENUMS: 0/False = Red, 1/True = White
	ARM1_LOSE_WANTED_LVL, // Time taken to lose the wanted level which is given to the player at the end of the race cutscene. Start the timer as soon as the player is given control, and end as soon as they lose the wanted level.
	ARM1_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission.
	ARM1_SPECIAL_ABILITY_TIME, // The time in milliseconds which the player used their characters special ability on this mission.
	ARM1_WINNER, // Did the player win the race between Franklin and Lamar? Set this to true if Franklin was in first place when the car park cutscene started.
	ARM1_HIT_ALIENS, // Did the player drive through the movie studio without hitting any aliens? Call when/if the player hits one to fail them, otherwise pass.
	ARM1_TIME, // Total time taken to complete the mission
	ARM1_MAX_SPEED, // Max speed the player reached in their chosen stolen car
	// stats for mission SP_MISSION_ARMENIAN_2
	ARM2_TIME, // Mission Time (Player completes the mission within 6 minutes 30 seconds)
	ARM2_PETROL_FIRE, // Trail blazer (The player shoots the petrol trail which blows up the car)
	ARM2_BULLETS_FIRED, // The number of bullets fired during this mission.
	ARM2_CAR_DAMAGE, // The total amount of car damage the player recieves whilst on this mission. The watch target should always be the players current vehicle.
	ARM2_HEADSHOTS, // The number of headshot kills performed during this mission.
	ARM2_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission (run over for example)
	ARM2_KILLS, // The number of enemies killed whilst on mission. This should not count innocent pedestrians.
	ARM2_MAX_SPEED, // The top speed reached whilst driving a vehicle on this mission. The watch target should always be the player's current vehicle.
	ARM2_PLAYER_DAMAGE, // The amount of health damage the player recieves whilst on this mission.
	ARM2_SPECIAL_ABILITY_TIME, // The time spent using Franklin's special driving ability during this mission.
	ARM2_CRASH_BIKER, // Did the player use their vehicle to knock the biker off the bike?
	ARM2_ACCURACY, // Tracking the players overall accuracy
	// stats for mission SP_MISSION_ARMENIAN_3
	ARM3_TIME, // Mission Time (Player completes the mission within 5 minutes)
	ARM3_DAMAGE, // Damage taken (Player takes no damage during the brawl with Michael)
	ARM3_CAR_DAMAGE, // This should track the total car damage taken on this mission for any vehicles the player uses. The watch target should always be the players current vehicle.
	ARM3_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission (run over etc)
	ARM3_SPECIAL_ABILITY_TIME, // The time in ms spent using Franklin's special driving ability during this mission.
	ARM3_MAX_SPEED, // The top speed reached whilst driving ANY vehicle on this mission. The watch target should always be the players current vehicle.
	ARM3_ACTION_CAM_TIME, // The time spent using the action cam to view Michael in the back of the car. This should ONLY be tracking time using the action cam during the Michael back seat part of the mission.
	ARM3_COUNTERS, // The number of counters (dodges) performed by Michael in the fight with Simeon.
	ARM3_GARDEN_KO, // Call if they knock out the gardener with a stealth attack
	// stats for mission SP_MISSION_ASSASSIN_1
	ASS1_TIME, // Total time taken to complete the mission
	ASS1_BULLETS_FIRED, // Total bullets fired on the mission
	ASS1_HEADSHOTS, // Total headshot kills on the mission
	ASS1_KILLS, // Total enemies killed on the mission
	ASS1_NOT_SPOTTED, // Player escaped without being spotted
	ASS1_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	ASS1_ACCURACY, // Total shooting accuracy on the mission
	ASS1_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	ASS1_MIRROR_TIME, // Mirrored stat, special case
	ASS1_MIRROR_SNIPER_USED, // Mirrored stat
	ASS1_MIRROR_CASH, // Money earned. Mirror.
	ASS1_MIRROR_PERCENT, // Completion percentage. Mirrored.
	ASS1_MIRROR_MEDAL, // Medal achieved. Mirror.
	// stats for mission SP_MISSION_ASSASSIN_2
	ASS2_TIME, // Total time taken to complete the mission
	ASS2_BULLETS_FIRED, // Total bullets fired on the mission
	ASS2_HEADSHOTS, // Total headshot kills on the mission
	ASS2_CAR_DAMAGE, // Damage taken by the players vehicle while on the mission
	ASS2_MAX_SPEED, // The top speed reached whilst driving ANY vehicle on this mission. The watch target should always be the players current vehicle.
	ASS2_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	ASS2_KILLS, // Total enemies killed on the mission
	ASS2_ACCURACY, // Total shooting accuracy on the mission
	ASS2_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	ASS2_MIRROR_TIME, // Mirrored stat, special case
	ASS2_MIRROR_QUICKBOOL, // Mirrored stat
	ASS2_MIRROR_PERCENT, // Completion percentage.Mirrored.
	ASS2_MIRROR_MEDAL, // Medal achieved. Mirror.
	ASS2_MIRROR_CASH, // Money earned. Mirror.
	// stats for mission SP_MISSION_ASSASSIN_3
	ASS3_TIME, // Total time taken to complete the mission
	ASS3_BULLETS_FIRED, // Total bullets fired on the mission
	ASS3_HEADSHOTS, // Total headshot kills on the mission
	ASS3_CAR_DAMAGE, // Damage taken by the players vehicle while on the mission
	ASS3_MAX_SPEED, // The top speed reached whilst driving ANY vehicle on this mission. The watch target should always be the players current vehicle.
	ASS3_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	ASS3_KILLS, // Total enemies killed on the mission
	ASS3_ACCURACY, // Total shooting accuracy on the mission
	ASS3_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	ASS3_MIRROR_TIME, // Mirrored stat, special case
	ASS3_MIRROR_CLEAN_ESCAPE, // Mirrored stat
	ASS3_MIRROR_CASH, // Money earned. Mirror.
	ASS3_MIRROR_PERCENTAGE, // Completion percentage. Mirrored.
	ASS3_MIRROR_MEDAL, // Medal achieved. Mirror.
	// stats for mission SP_MISSION_ASSASSIN_4
	ASS4_TIME, // Total time taken to complete the mission
	ASS4_BUS_DAMAGE, // Damage taken by the bus while on the mission
	ASS4_MAX_SPEED, // The top speed reached whilst driving the bus on this mission.
	ASS4_HIT_N_RUN, // Did player kill target with a hit and run? set to true if they did
	ASS4_BULLETS_FIRED, // Total bullets fired on the mission
	ASS4_HEADSHOTS, // Total headshot kills on the mission
	ASS4_WANTED_LOSS_TIME, // Time taken to lose wanted level
	ASS4_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	ASS4_ACCURACY, // Total shooting accuracy on the mission
	ASS4_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	ASS4_MIRROR_TIME, // Mirrored stat, special case
	ASS4_MIRROR_HIT_AND_RUN, // Mirrored stat
	ASS4_MIRROR_CASH, // Money earned. Mirror.
	ASS4_MIRROR_PERCENTAGE, // Completion percentage. Mirrored.
	ASS4_MIRROR_MEDAL, // Medal achieved. Mirror.
	// stats for mission SP_MISSION_ASSASSIN_5
	ASS5_TIME, // Total time taken to complete the mission
	ASS5_DAMAGE, // Health damage taken by the player on the mission
	ASS5_KILLS, // Total enemies killed on the mission
	ASS5_HEADSHOTS, // Total headshot kills on the mission
	ASS5_BULLETS_FIRED, // Total bullets fired on the mission
	ASS5_ACCURACY, // Total shooting accuracy on the mission
	ASS5_CAR_DAMAGE, // Damage taken by the players vehicle while on the mission
	ASS5_MAX_SPEED, // The top speed reached whilst driving the bus on this mission.
	ASS5_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	ASS5_ESCAPE_TIME, // Time taken to escape the construction site after the target is eliminated
	ASS5_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	ASS5_MIRROR_TIME, // Mirrored stat, special case
	ASS5_MIRROR_NO_FLY, // Mirrored stat
	ASS5_MIRROR_CASH, // Money earned. Mirror.
	ASS5_MIRROR_PERCENT, // Completion percentage. Mirrored.
	ASS5_MIRROR_MEDAL, // Medal achieved. Mirror.
	// stats for mission SP_MISSION_CARSTEAL_2
	CS2_NO_SCRATCH, // No damage to stolen car (Player causes zero damage to the stolen car)
	CS2_MAX_SPEED, // Fastest speed reached in the stolen car.
	CS2_INNOCENTS_KILLED, // Number of innocent pedestrians killed on mission. No one should be killed in a perfect run through, so treat any kills as innocent.
	CS2_TARGETS_SCANNED, // Total number of targets scanned. Please update the target integer if the number of possible targets changes, although this is hidden on the pass screen anyway.
	CS2_CHAD_FOCUS_TIME, // Total time in ms spent with Chad in the focus of the heli camera.
	CS2_TIME, // Total time taken to complete the mission
	CS2_EAVESDROPPER, // Update for each of the 3 conversations listened to
	CS2_SCANMAN, // Chad scanned "right away" in the car park.
	CS2_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	// stats for mission SP_MISSION_CARSTEAL_1
	CS1_DROVE_BETWEEN_TRUCKS, // Drove between trucks
	CS1_DROVE_BETWEEN_BUSES, // Drove between buses
	CS1_DROVE_THROUGH_TUNNEL, // Drove through tunnel
	CS1_TIME, // Mission time (Player completes the mission in a set amount of time) - 12 mins
	CS1_SPECIAL_USED, // Franklin special used (Player uses the special ability in the race while playing as Franklin)
	CS1_MAX_SPEED, // Fastest speed reached in any vehicle under the players control. The watch target should always be the players current vehicle.
	CS1_CAR_DAMAGE, // Total damage taken by ANY vehicles driven by the player on this mission. The watch target should always be the players current vehicle.
	CS1_SWITCHES, // Number of character switches.
	CS1_SPECIAL_ABILITY_TIME, // The time in ms using special ability (all chars)
	// stats for mission SP_MISSION_CARSTEAL_3
	CS3_NO_SCRATCH, // Not a Scratch (Player doesn’t cause any damage to the stolen car)
	CS3_FASTEST_SPEED, // Fastest Speed (Player reaches maximum speed in the stolen car)
	CS3_ACTOR_KNOCKOUT, // Stealthy Recasting (Player kills the actor with a knock out)
	CS3_EJECTOR_SEAT_USED, // Premature Ejector (Player uses the ejector seat within 10 seconds)
	CS3_RAN_OVER_ACTOR_AGAIN, // Second Strike (Runs over the actor a second time)
	CS3_INNOCENTS_KILLED, // Number of innocent pedestrians killed (ran over etc). This should not count the actor.
	CS3_SPECIAL_ABILITY_TIME, // Time in ms which the player used their special ability on this mission.
	CS3_DAMAGE, // Total health damage taken by the player on this mission.
	CS3_TIME, // Total time taken to complete the mission - 4 mins
	// stats for mission SP_MISSION_CARSTEAL_4
	CS4_TIME, // Mission Time (complete the mission in 12 mins) The timer should start when the player enters the vehicle.
	CS4_NO_SCRATCH, // None of the stolen cars take any damage. Update: Now JB700 as only one you drive
	CS4_MAX_SPEED, // Fastest speed reached in ANY vehicle the player uses on the mission. Watch target should always be the players current vehicle
	CS4_INNOCENTS_KILLED, // Number of innocent pedestrians killed during the mission.
	CS4_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters)
	CS4_COP_LOSS_TIME, // Time taken to lose the wanted level
	CS4_SHREDS, // Increment for each car hit with the stringer strip
	// stats for mission SP_MISSION_CHINESE_1
	CHI1_BODY_COUNT, // Body count (Player kills at least 32 enemies)
	CHI1_UNMARKED, // Unmarked (Player takes less than 50 damage). 
	CHI1_HEADSHOTS, // Headshots
	CHI1_CAR_DAMAGE, // Total damage taken to vehicles driven by the player
	CHI1_MAX_SPEED, // Max speed of the dirtbike. Set the entity watch to the bike while trevor is riding it. The max speed target is set one that the bike could only just achieve by going flat out on the highway in v307.1
	CHI1_INNOCENTS_KILLED, // Innocent peds killed (run over) etc
	CHI1_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	CHI1_BULLETS_FIRED, // Total bullets fired on this mission (including grenades fired etc)
	CHI1_ACCURACY, // Overall accuracy
	CHI1_VEHICLES_DESTROYED, // Number of vehicles blown up
	CHI1_GRENADE_LAUNCHER_KILLS, // Number of kills using the grenade launcher
	CHI1_TIME, // Total time taken to complete the mission
	// stats for mission SP_MISSION_CHINESE_2
	CHI2_HEADSHOTS, // Headshots (Player gets 10 headshots)
	CHI2_UNMARKED, // Unmarked (Player takes less than 50 damage)
	CHI2_ONE_SHOT_TWO_KILLS, // 2 Birds 1 Stone (Player kills at least 2 enemies with one bullet)
	CHI2_KILLS, // Total enemy kills
	CHI2_CAR_DAMAGE, // Damage taken to Trevor's truck
	CHI2_MAX_SPEED, // Max speed reached in Trevor's truck
	CHI2_INNOCENTS_KILLED, // Innocent peds killed (run over etc)
	CHI2_SPECIAL_ABILITY_TIME, // time in ms using special ability
	CHI2_BULLETS_FIRED, // Total bullets fired
	CHI2_GAS_POUR_TIME, // Time in ms spent pouring gasoline
	CHI2_GAS_USED, // total amount of gasoline poured
	CHI2_ACCURACY, // Weapon accuracy
	CHI2_TIME, // Total time taken to complete mission
	// stats for mission SP_MISSION_EXILE_1
	EXL1_ACCURACY, // Accuracy (Player has 80% shooting accuracy in the mission)
	EXL1_FLYING_HIGH, // Number of times the player is warned about flying too high.
	EXL1_KILLS, // The number of enemies killed whilst on mission.
	EXL1_HEADSHOTS, // The number of enemies killed by headshots whilst on mission.
	EXL1_BULLETS_FIRED, // The number of bullets fired during this mission.
	EXL1_DAMAGE, // Damage taken by the player throughout the mission
	EXL1_FREEFALL_TIME, // Time spent in freefall before opening the parachute. This should start as soon as the player enters freefall and end as soon as they open the parachute.
	EXL1_HARRIER_CAM_TIME, // Time spent using the harrier cam. Only applicable when the harrier cam is available.
	EXL1_BAIL_IN_CAR, // Did the player exit the plane in the car? Set to true if they do
	EXL1_TIME, // Total time taken to complete mission - 15 mins
	EXL1_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_EXILE_2
	EXL2_ACCURACY, // Accuracy (Player has 70% shooting accuracy in the mission)
	EXL2_TIME, // Mission Time (Player completes the mission in 9 minutes)
	EXL2_HEADSHOTS, // Headshots (Player gets 3  headshots in the mission)
	EXL2_ANIMAL_KILLED, // Thin the herd (Player doesn’t kill any animals during the sniping section) call this to fail when the player kills an animal
	EXL2_CAR_DAMAGE, // Total car damage taken across the mission.
	EXL2_MAX_SPEED, // Max speed in any car on the mission.
	EXL2_BULLETS_FIRED, // The number of bullets fired during this mission.
	EXL2_TIME_AS_CHOP, // Time spent as chop in the mission. Should only track the time the player is actively using Chop.
	EXL2_DEATH_FROM_ABOVE, // Time taken to snipe all of the enemies from the helicopter.
	EXL2_SWITCHES, // Number of times the player switched to a different character while on the mission.
	EXL2_KILLS, // The number of enemies killed whilst on mission.
	EXL2_DAMAGE, // Damage taken by the player throughout the mission
	EXL2_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_EXILE_3
	EXL3_TIME, // Mission Time (Player completes mission in a set amount of time) - 11 mins 30 secs
	EXL3_FASTEST_SPEED, // -	Fastest speed (Player reaches maximum speed on the bike)
	EXL3_JUMPED_AT_FIRST_OPPERTUNITY, // First jump succeeded (Player jumps from the track to the train at the first opportunity)
	EXL3_BULLETS_FIRED, // The number of bullets fired during this mission.
	EXL3_KILLS, // The number of enemies killed (by the player) whilst on mission.
	EXL3_DAMAGE, // Damage taken by the player throughout the mission
	EXL3_HEADSHOTS, // Number of headshot kills
	EXL3_ACCURACY, // Overall accuracy while on mission
	EXL3_SWITCHES, // Number of player switches made on this mission.
	EXL3_TRAIN_TIME, // Time spent driving the train as Trevor.
	EXL3_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FAMILY_1
	FAM1_QUICK_CATCH, // Quick Catch (Player catches Jimmy within 10 seconds of him being on the mast)
	FAM1_NOSCRATCH, // "Traffic dodger" no damage taken from other traffic while player is controlling the vehicle. Set watch entity to the vehicle the player is driving.
	FAM1_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission (run over etc)
	FAM1_QUICK_BOARDING, // The time it takes the player to get Franklin on the boat from the start of the gameplay sequence where you chase after it.
	FAM1_BULLETS_FIRED, // The number of bullets fired during this mission.
	FAM1_KILLS, // The number of enemies killed whilst on mission.
	FAM1_TIME, // Total time taken to complete mission
	FAM1_MAX_SPEED, // The top speed reached whilst driving a car on this mission (in metres per second)
	FAM1_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FAMILY_2
	FAM2_FASTEST_SPEED, // Fastest speed (Player’s jet-ski reaches maximum speed)
	FAM2_FELL_OFF_BIKE, // Stay on the bike (Player doesn’t fall off of the bike) Updated to track number of bails from bike. Add 1 each time the player bails.
	FAM2_FAST_SWIM, // Faster than fish (Player swims to the boat in 55 seconds). This one probably needs a better name
	FAM2_CAR_DAMAGE, // This should track the total car damage taken on this mission for any vehicles the player uses.
	FAM2_MAX_CAR_SPEED, // The top speed reached whilst driving a car on this mission (in metres per second)
	FAM2_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission.
	FAM2_CYCLING_TIME, // The time taken (ms) to reach the end of the pier from the start of the cycling sequence.
	FAM2_DAMAGE, // Total health damage taken by the player on this mission (including all switch characters if applicable)
	FAM2_LOSE_JETSKI, // Time taken to lose the pursuing jetski.
	FAM2_CHOICES, // Which bike does the player choose? 0 = Racer, 1 = BMX, 2 = Cruiser
	FAM2_TIME, // Total time taken to complete the mission
	FAM2_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FAMILY_3
	FAM3_NOSCRATCH, // Not a scratch (Player’s car takes less than 10% damage)
	FAM3_TIME, // Mission Time (Player completes the mission in 5 minutes)
	FAM3_MAX_SPEED, // Maximum speed reached in the pickup truck.
	FAM3_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission (run over etc)
	FAM3_GOON_LOSS_TIME, // Time taken to lose the goons after pulling down the house. Timer should start as soon as the blip appears on the map and end when the last blip disappears.
	FAM3_SWITCHES, // Number of player switches used on the mission.
	FAM3_BULLETS_FIRED, // The number of bullets fired during this mission.
	FAM3_KILLS, // The number of enemies killed whilst on mission.
	FAM3_HEADSHOTS, // The number of enemies killed with headshots.
	FAM3_ACCURACY, // Overall accuracy throughout mission
	FAM3_VEHICLE_KILLS, // Gain three or more kills in vehicle, for M or F.
	FAM3_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FAMILY_4
	FAM4_LORRY_SPEED, // Fastest Speed (Player reaches maximum speed in lorry)
	FAM4_GOT_TOO_FAR_FROM_LAZ, // Stay close to Lazlow (Player stays within a certain distance on Lazlow for the entire chase) Call bool update if the player fails to stay close enough.
	FAM4_CAR_DAMAGE, // Total amount of vehicle damage taken on the mission (across all vehicles the player has driven)
	FAM4_MAX_SPEED, // Maximum speed reached in any vehicle used on this mission
	FAM4_INNOCENTS_KILLED, // Number of innocent pedestrians killed (run over etc)
	FAM4_LAZLOW_CAM_USE, // Amount of time in ms which the player used the Lazlow cam
	FAM4_TIME, // Total time taken to complete mission
	FAM4_COORD_KO, // Set if the player knocks out the event coordinator
	FAM4_TRUCK_UNHOOKED, // Call inform on this if the player unhooks the trailer
	FAM4_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FAMILY_5
	FAM5_TIME, // 15 mins
	FAM5_WARRIOR_POSE, // Warrior (Yoga pose complete)
	FAM5_TRIANGLE_POSE, // Triangle (Yoga pose complete)
	FAM5_SUN_SALUTATION_POSE, // The sun salutation  (Yoga pose complete)
	FAM5_CAR_DAMAGE, // Damage taken to Michael's car
	FAM5_INNOCENTS_KILLED, // Number of innocent pedestrians killed (run over etc)
	FAM5_DAMAGE, // Health damage taken by Michael
	FAM5_MAX_SPEED, // highest speed reached in Michael's car
	FAM5_HOME_TIME, // Time taken to get home after waking up in the park.
	FAM5_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FAMILY_6
	FAM6_TIME, // Mission time (Player completes mission in10 mins 30 secs)
	FAM6_CAR_DAMAGE, // Total damage taken by any/all vehicles used by the player on this mission. Watch target should always be the players current vehicle.
	FAM6_MAX_SPEED, // Fastest speed reached in any vehicle used by the player on this mission. Watch target should always be the players current vehicle.
	FAM6_INNOCENTS_KILLED, // Innocent pedestrians killed on this mission.
	FAM6_PIERCING_TIME, // Time the player spent piercing Lazlow.
	FAM6_TATTOO_TIME, // Time the player spent tattooing Lazlow.
	FAM6_FRONT_OR_BACK, // Player chooses to tattoo Lazlows front (0/False) or back (1/True)
	FAM6_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_MISSION_FINALE_C1
	FIN_TIME, // Mission Time (Player completes mission in 21 mins 30 secs)
	FIN_CAR_DAMAGE, // Total damage taken by all cars in use by the player on the mission. Watch target should always be the players current vehicle
	FIN_MAX_SPEED, // Fastest speed reached in any vehicle used by the player. Watch target should always be the players current vehicle.
	FIN_SPECIAL_ABILITY_TIME, // Total time in ms which the player uses special ability (across all characters)
	FIN_INNOCENTS_KILLED, // Innocent peds killed (run over etc)
	FIN_BULLETS_FIRED, // Total Bullets fired (over all characters while controlled by the player)
	FIN_DAMAGE, // Total health damage taken across all characters while in use by the player.
	FIN_KILLS, // Total enemies killed (across all characters used by the player)
	FIN_HEADSHOTS, // Total enemies killed with headshot by player (across all characters)
	FIN_ACCURACY, // Overall accuracy across all characters
	FIN_STICKY_BOMBS_USED, // Number of sticky bombs used on the mission
	FIN_SNIPER_ACCURACY, // Accuracy during the Haines sniping section of the mission. Should only count shots fired with the sniper rifle at this point.
	FIN_SWITCHES, // Number of character switches
	FIN_CHENG_BOMBER, // Player killed Cheng with a sticky bomb
	FIN_HAINES_HEADSHOT, // Player killed Hains with a headshot
	FIN_STRETCH_STRONGARM, // Player killed Stretch with a melee attack
	// stats for mission SP_MISSION_FBI_1
	FBI1_ACCURACY, // Accuracy (Player has 70% shooting accuracy in the mission)
	FBI1_HEADSHOTS, // Headshots (Player gets 14 headshots in the mission)
	FBI1_TIME, // Mission Time (Player completes the mission in 6 minutes)
	FBI1_SPECKILLS, // Special Ability Kills (Player kills X number of enemies while using special ability)
	FBI1_UNMARKED, // Unmarked (Player takes less than 50 damage)
	FBI1_INNOCENTS_KILLED, // Number of innocent pedestrians killed (run over, doctors killed etc)
	FBI1_BULLETS_FIRED, // Total bullets fired on mission
	FBI1_KILLS, // Enemies killed
	FBI1_COP_LOSS_TIME, // Time taken to lose the wanted level given on the mission. This should start as soon as the player gets a wanted level and end as soon as they lose it (at the end of the mission).
	FBI1_SPECIAL_ABILITY_TIME, // Total time in ms which the player uses special ability (across all characters)
	// stats for mission SP_MISSION_FBI_2
	FBI2_TIME, // Mission Time (Player completes the mission in 7.5 minutes)
	FBI2_ACCURACY, // Accuracy (Player has 33% or greater shooting accuracy in the mission, machine guns against the helis make this tricky, see 759431) - old issue
	FBI2_HEADSHOTS, // Headshots (Player gets 10 headshots in the mission)
	FBI2_INNOCENTS_KILLED, // Innocent pedestrians killed
	FBI2_BULLETS_FIRED, // Total bullets fired on mission
	FBI2_KILLS, // Total enemies killed on mission
	FBI2_CAR_DAMAGE, // Total damage taken by ALL vehicles driven by the player on this mission. The watch target for this stat should always be set to the players current vehicle.
	FBI2_MAX_SPEED, // Max Speed reached by ANY vehicle driven by the player on this mission. The watch target for this stat should always be set to the players current vehicle.
	FBI2_SWITCHES, // Number of character switches
	FBI2_RAPPEL_TIME, // Time taken for the player to rappel down the building and smash through the window. Should start when the player has control of the descent and end as soon as they press the button to smash through the window.
	FBI2_SPECIAL_ABILITY_TIME, // Total time in ms which the player uses special ability (across all characters)
	// stats for mission SP_MISSION_FBI_3
	FBI3_HEART_STOPPED, // Don’t stop me now (Player passes the mission without the victim’s heart stopping) Default pass. Fail if adrenaline is used.
	FBI3_ELECTROCUTION, // Electrocution (Player electrocutes the victim)
	FBI3_TOOTH, // The Tooth Hurts (Player pulls out the victim’s tooth)
	FBI3_WRENCH, // Wrench (Player hits victim with the wrench)
	FBI3_WATERBOARD, // It’s Legal! (Player tortures victim with waterboarding)
	FBI3_INNOCENTS_KILLED, // Number of innocent pedestrians killed on the mission.
	FBI3_CAR_DAMAGE, // This should track all damage taken by ANY vehicles used by the player. The watch target should always be the players current vehicle.
	FBI3_BULLETS_FIRED, // Total number of shots fired on the mission.
	FBI3_MAX_SPEED, // Maximum speed reached in ANY vehicle used by the player in the mission. Watch target should always be the players current vehicle.
	FBI3_ASSASSIN_TIME, // Time taken to find and assassinate the target in the final part of the mission. Start the timer when the player is in control of Michael with the sniper rifle.
	FBI3_TIME, // Total time taken to complete mission
	FBI3_SPECIAL_ABILITY_TIME, // Total time in ms which the player uses special ability (across all characters)
	// stats for mission SP_MISSION_FBI_4
	FBI4_UNMARKED, // Unmarked (Player takes less than 40% damage per character)
	FBI4_TIME, // Mission Time (Player completes the mission in 11 minutes)
	FBI4_HELISHOT, // Helicopter shot down (The player shoots down the helicopter with the RPG while playing as Trevor)
	FBI4_HEADSHOTS, // Total enemy headshot kills
	FBI4_ACCURACY, // Overall accuracy across all characters
	FBI4_KILLS, // Total enemies killed
	FBI4_BULLETS_FIRED, // Total bullets fired across all characters (under players control)
	FBI4_SWITCHES, // Number of character switches
	FBI4_STICKY_BOMB_KILLS, // Number of kills with sticky bombs
	FBI4_VEHICLES_DESTROYED, // Number of vehicles blown up
	FBI4_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	// stats for mission SP_MISSION_FBI_4_PREP_1
	FBI4P1_TIME_TAKEN, // Player completes the mission in 5 minutes
	FBI4P1_VEHICLE_DAMAGE, // Damage caused to the garbage truck
	FBI4P1_MAX_SPEED, // Max speed reached in the garbage truck
	FBI4P1_LEVEL_LOSS, // Time Taken To Lose Wanted Level
	FBI4P1_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	// stats for mission SP_MISSION_FBI_4_PREP_2
	FBI4P2_TIME_TAKEN, // Overall mission time - 3 minutes
	FBI4P2_VEHICLE_DAMAGE, // Damage taken by the tow truck
	FBI4P2_MAX_SPEED, // Max speed reached in the tow truck
	FBI4P2_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	// stats for mission SP_MISSION_FBI_5
	FBI5_STUNS, // Enemies stunned (Player uses the stungun on at least 8  enemies)
	FBI5_KILLS, // Total enemies killed by the player across any switches. Should stunned enemies be counted as kills? Probably, if they don't get back up.
	FBI5_BULLETS_FIRED, // Total number of bullets fired by the player on the mission
	FBI5_DAMAGE, // Total health damage taken by the player (across all character switches)
	FBI5_HEADSHOTS, // Total number of enemies killed by headshots by the player (across all characters)
	FBI5_ACCURACY, // Overall accuracy across all characters used by the player.
	FBI5_BAR_CUT_TIME, // Time taken to successfully cut the bars at the start of the mission. Start the timer when the player is given control and end once the last bar has been cut.
	FBI5_SWITCHES, // Total number of character switches
	FBI5_CAR_DAMAGE, // Total vehicle damage taken by all vehicles used by the player. Watch target should always be the players current vehicle.
	FBI5_MAX_SPEED, // Max speed reached in any vehicle used by the player. Watch target should always be the players current vehicle.
	FBI5_TIME, // Total time taken to complete mission
	FBI5_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	// stats for mission SP_MISSION_FRANKLIN_0
	FRA0_NOSCRATCH, // Not a scratch (Player’s vehicle takes less than 10%)
	FRA0_DOGCAM, // Dog cam use(Player uses Dog Cam for 10 seconds)
	FRA0_INNOCENTS_KILLED, // The number of innocent pedestrians killed while on the mission (run over etc)
	FRA0_SPECIAL_ABILITY_TIME, // The time in milliseconds which the player used their characters special ability on this mission.
	FRA0_DOGGY_STYLE, // The time (ms) which the player watched the dogs humping before calling Chop back.
	FRA0_SWITCHES, // The number of times the player used character switches on this mission.
	FRA0_MAX_SPEED, // The top speed reached whilst driving a vehicle on this mission (in metres per second)
	FRA0_TIME, // Total time taken to complete mission
	// stats for mission SP_MISSION_FRANKLIN_1
	FRA1_TIME, // Target time 7 mins
	FRA1_FRANKLIN_KILLS, // Number of enemies killed as Franklin
	FRA1_CAR_DAMAGE, // Car damage taken when driving to the drug house.
	FRA1_TREVOR_KILLS, // Number of enemies killed as Trevor
	FRA1_INNOCENTS_KILLED, // Innocent pedestrians killed
	FRA1_MAX_SPEED, // Maximum speed reached in the van
	FRA1_KILLS, // Total kills by the player on this mission
	FRA1_ACCURACY, // Overall accuracy throughout the mission (including all switches)
	FRA1_BULLETS_FIRED, // Total bullets fired by the player on this mission (including all switches)
	FRA1_DAMAGE, // Total damage taken by the player. Stat should track damage done to any characters the player is in control of.
	FRA1_SWITCHES, // Number of character switches.
	FRA1_HEADSHOTS, // Total number of headshots.
	FRA1_COP_LOSS_TIME, // Time taken to lose the wanted level at the end of the mission. Start the timer when "Lose the Cops" is shown on screen.
	FRA1_SPECIAL_ABILITY_TIME, // The time in milliseconds which the player used their characters special ability on this mission.
	// stats for mission SP_MISSION_FRANKLIN_2
	FRA2_DAMAGE, // Dodging bullets - Didnt lose any health - This stat should track the total amount of health lost by the player.
	FRA2_CAR_DAMAGE, // Total damage taken by any vehicles the player uses on the mission. Watch target should always be players current vehicle.
	FRA2_MAX_SPEED, // Fastest speed reached in any vehicle used by the player on the mission. Watch target should always be the players current vehicle.
	FRA2_KILLS, // Total enemies killed (across all characters)
	FRA2_HEADSHOTS, // Total enemies killed by headshots (across all characters)
	FRA2_ACCURACY, // Overall weapon accuracy on mission (across all characters)
	FRA2_BULLETS_FIRED, // Total bullets fired by the player
	FRA2_SPECIAL_ABILITY_TIME, // Total time in ms using special ability (across all characters)
	FRA2_INNOCENTS_KILLED, // Total innocent pedestrians killed (run over etc)
	FRA2_SWITCHES, // Number of character switches
	FRA2_VEHICLE_CHOSEN, // Vehicle chosen to start the mission: 0 = Bike, 1 = Car, 2 = Other
	FRA2_TIME, // Total time taken to complete mission
	// stats for mission SP_MISSION_LAMAR
	LAM1_ACCURACY, // Accuracy (Player has 60% shooting accuracy in the mission)
	LAM1_HEADSHOTS, // Headshots (Player gets 10 headshots in the mission)
	LAM1_UNMARKED, // Unmarked (Player takes less than 50 damage)
	LAM1_TIME, // Mission Time (Player has to complete the mission in 10 minutes 30 seconds)
	LAM1_CAR_DAMAGE, // Amount of car damage taken when driving to the meet.
	LAM1_MAX_SPEED, // Maximum vehicle speed reached on the mission. This should count all vehicles driven by the player.
	LAM1_INNOCENTS_KILLED, // Number of innocent pedestrians killed during the course of the mission (e.g. run over)
	LAM1_KILLS, // Number of enemies killed on the mission.
	LAM1_BULLETS_FIRED, // Total bullets fired on the mission.
	LAM1_COP_LOSS_TIME, // Time taken to lose the wanted level at the end of the mission. This could start when the player first gets a wanted level in the mission, or could start from the point of escaping the building.
	LAM1_BODY_ARMOR, // Did player buy body armor from Ammunation?
	LAM1_SPECIAL_ABILITY_TIME, // Total time in ms using special ability (across all characters)
	// stats for mission SP_MISSION_LESTER_1
	LES1A_TIME, // Mission Time (Player completes the mission in 8.5 minutes)
	LES1A_CLEAR_POPUPS, // Clearing pop ups (Player clears pop-ups off of the screen in 32 seconds)
	LES1A_CAR_DAMAGE, // Total damage taken by any vehicles used by the player in this mission.
	LES1A_MAX_SPEED, // Highest vehicle speed reached by the player, in any vehicle they enter throughout the length of the mission.
	LES1A_INNOCENTS_KILLED, // Number of innocent pedestrians killed on this mission (e.g. run over)
	LES1A_MOUSE_CLICKS, // Number of times the player clicked the mouse while clearing the popups.
	LES1A_POPUPS_CLOSED, // Actual number of popups the player closed.
	LES1B_TIME_WATCHING, // Time spent watching Jay Norris' announcement before detonating the bomb.
	LES1A_SPECIAL_ABILITY_TIME, // Total time in ms using special ability (across all characters)
	// stats for mission SP_MISSION_MARTIN_1
	MAR1_FASTEST_SPEED, // Fastest Speed (Player reaches maximum speed on the bike)
	MAR1_FELL_OFF_THE_BIKE, // Don’t fall off the bike (Player never falls off of the bike) This stat should track the total number of times the player bails from the bike
	MAR1_ONE_SHOT, // (It will be passed if the player manages to shoot the plane down with 3 shots)This stat should track the number of bullets fired during the plane shootdown section.
	MAR1_TIME, // Mission time (Player completes the mission in 9 minutes)
	MAR1_CAR_DAMAGE, // Total damage taken across any vehicles driven by the player. Watch target should always be the players current vehicle.
	MAR1_SPECIAL_ABILITY_TIME, // Time in ms which the player used Franklins Special Ability.
	MAR1_PLANE_HIT_TIME, // Time in ms taken to aim and fire an accurate shot to take down the plane.
	// stats for mission SP_MISSION_MICHAEL_1
	MIC1_TIME, // Mission time (Player completes mission in a set number of time) - 11 mins
	MIC1_HEADSHOTS, // Headshots (Player gets 20 headshots in the mission)
	MIC1_KILLS, // Total enemies killed
	MIC1_BULLETS_FIRED, // Total bullets fired
	MIC1_ACCURACY, // Overall accuracy on the mission
	MIC1_LANDING_TIME, // Time taken for the player to land the plane on the airstrip. Timer should start when they are given control of the plane and end when they successfully touch down.
	MIC1_CAR_DAMAGE, // Total damage taken to the players road vehicle during this mission. Watch target should always be the players car or road vehicle.
	MIC1_MAX_SPEED, // Fastest speed reached by the player in any road vehicle. Watch target should always be the players current road vehicle.
	MIC1_INNOCENTS_KILLED, // Innocent pedestrians killed (run over etc)
	MIC1_PLANE_DAMAGE, // Total damage done to any/all planes used by the player on the mission.
	MIC1_DAMAGE, // Total health damage taken by the player (across all characters) on the mission
	MIC1_SPECIAL_ABILITY_TIME, // Time in ms using special abilities across all characters.
	MIC1_STARTING_CHAR, // Which character did the player use to start the mission? 0/False = Michael, 1/True = Trevor
	// stats for mission SP_MISSION_MICHAEL_2
	MIC2_TIMES_SWITCHED, // Character Switches (Player completes the mission using only 3 switches)
	MIC2_ACCURACY, // Accuracy (Player has 70% shooting accuracy in the mission)
	MIC2_HEADSHOTS, // Headshots (Player gets 10 headshots in the mission)
	MIC2_MIKE_RESCUE_TIMER, // Reaching Michael quickly (Player rescues Michael in 3 minutes 30 seconds)
	MIC2_WAYPOINT_USED, // No map waypoint used (Player completes the mission without setting a waypoint on the map) call this when they set a waypoint
	MIC2_CAR_DAMAGE, // Total car damage taken on mission by ALL vehicles the player drives. Watch target should always be the players current vehicle.
	MIC2_INNOCENTS_KILLED, // Innocent pedestrians killed.
	MIC2_KILLS, // Enemies killed
	MIC2_BULLETS_FIRED, // Total bullets fired on mission
	MIC2_DAMAGE, // Total health damage taken by the player, across all switches
	MIC2_TIME_TO_LOSE_TRIADS, // Time taken to lose the Triads at the end of the mission.
	MIC2_MAX_SPEED, // Maximum speed reached by any vehicle under the players control throughout the mission. The watch target should always be the players current vehicle.
	MIC2_TIME_TO_SAVE_FRANKLIN, // Time taken to save Franklin from the knife attacker.
	MIC2_UNIQUE_TRIAD_DEATHS, // Number of unique Triad deaths seen (acid bath, grinder 1, chopper, grinder 2).
	MIC2_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	MIC2_TIME, // Total time taken to complete mission
	// stats for mission SP_MISSION_MICHAEL_3
	MIC3_TIME, // Target time: 7 minutes
	MIC3_KILLS, // Total enemies killed by the player.
	MIC3_HEADSHOTS, // Total enemies killed by the player with headshots.
	MIC3_BULLETS_FIRED, // Total bullets fired on the mission
	MIC3_ACCURACY, // Overall accuracy on mission
	MIC3_CAR_DAMAGE, // Total damage taken by any vehicles the player uses on mission. Watch target should always be the players current vehicle.
	MIC3_MAX_SPEED, // Highest speed reached in any vehicles used by the player on the mission. Watch target should always be the players current vehicle.
	MIC3_SPECIAL_ABILITY_TIME, // Total time in ms the player has used characters special ability while on the mission. This should include a total of all different characters special ability use if applicable.
	MIC3_SWITCHES, // Total number of Character switches performed
	MIC3_INNOCENTS_KILLED, // Number of innocent pedestrians killed (run over etc)
	MIC3_VEHICLES_DESTROYED, // Total number of vehicles blown up on the mission
	MIC3_HELIKILL, // Pass if the player takes out the pursuing helicopter.
	// stats for mission SP_MISSION_MICHAEL_4
	MIC4_TIME, // Target time 6:30 (increased due to limo section)
	MIC4_CAR_DAMAGE, // Total damage taken by any vehicles used on the mission by the player. Watch target should always be the players current vehicle.
	MIC4_MAX_SPEED, // Fastest speed reached in any vehicle on the mission. Watch target should always be the players current vehicle.
	MIC4_DAMAGE, // Total health damage taken by the player
	MIC4_KILLS, // Total enemies killed by the player
	MIC4_HEADSHOTS, // Enemies killed by headshots
	MIC4_ACCURACY, // Overall accuracy across the entire mission
	MIC4_BULLETS_FIRED, // Total bullets fired on the mission
	MIC4_SPECIAL_ABILITY_TIME, // Total time in ms using special ability
	MIC4_HOME_TIME, // Time taken to get back to the house. Should start the timer when the player is given control of Michael and end when they reach the house.
	MIC4_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	MIC4_HEADSHOT_RESCUE, // Player rescues Amanda and Tracey with a headshot.
	MIC4_VEHICLE_CHOSEN, // Which vehicle did the player choose at the start of the mission? 0 = Limo, 1 = Sports car, 2 = Other
	// stats for mission SP_MISSION_PROLOGUE
	PRO_MAX_SPEED, // Max Speed reached in getaway vehicle.
	PRO_MICHAEL_SAVE_TIME, // Time taken (in ms) to save Michael when switching to Trevor.Timer should start the instant Michael is captured (as long as the player has control) and end when the guard has been killed.
	PRO_GETAWAY_TIME, // Time taken (in ms) to complete the driving part of the mission.Timer should start when the player takes control of the vehicle and should end when the train crash cutscene kicks in.
	PRO_BULLETS_FIRED, // Total bullets fired on mission.
	PRO_KILLS, // Total enemies killed on mission.
	PRO_HEADSHOTS, // Number of headshot kills
	PRO_ACCURACY, // Overall accuracy throughout the mission
	PRO_DAMAGE, // Total health damage taken by any characters which the player is controlling.
	PRO_STAY_ON_ROAD, // Did the player stay on the road throughout the driving part of the mission? If they left the road at any point, this should be false.
	PRO_SWITCHES, // Number of character switches performed on the mission.
	PRO_TIME, // Total time taken to complete the mission
	PRO_CAR_DAMAGE, // Car damage taken throughout the mission.
	// stats for mission SP_MISSION_SOLOMON_2
	SOL2_TIME, // Mission Time (Player completes mission in a set number of time) - 3 mins
	SOL2_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	SOL2_MAX_SPEED, // Max speed reached by the any player controlled road vehicle in this mission
	SOL2_CAR_DAMAGE, // Car damage taken by the players vehicle on this mission. Should track damage done to ANY vehicle the player enters while on mission.
	SOL2_DAMAGE, // Amount of health damage taken on the mission
	SOL2_BULLETS_FIRED, // Total bullets fired on the mission.
	SOL2_INNOCENT_KILLS, // Innocent pedestrians killed (e.g. run over)
	SOL2_HEADSHOTS, // Number of headshot kills while on mission.
	// stats for mission SP_MISSION_SOLOMON_3
	SOL3_TIME, // Mission Time (Player completes mission in 5 mins 30)
	SOL3_HEADSHOTS, // Total enemies killed by headshots
	SOL3_DAMAGE, // Total health damage taken by any characters controlled by the player. The watch target should always be the currently controlled character.
	SOL3_MAX_SPEED, // Fastest speed reached in any car. This should track across ANY car the player enters. Watch target should always be the players current vehicle.
	SOL3_ACCURACY, // Total accuracy across all characters involved on mission.
	SOL3_CAR_DAMAGE, // Total car damage taken across all vehicles used by the player on this mission. Watch target should always be the players current vehicle.
	SOL3_BULLETS_FIRED, // Total bullets fired on mission
	SOL3_KILLS, // Total number of enemies killed on this mission
	SOL3_COP_LOSS_TIME, // Time taken to lose the wanted level. Start the timer when the player gets a wanted level and end it when the level has been lost. - 2 mins
	SOL3_INNOCENT_KILLS, // Innocent pedestrians killed (e.g. run over)
	SOL3_NEWS_HELI_CAM_TIME, // Total time in ms which the player views the news heli cam. This should only track action cam use when the news heli cam is available. Target 15 seconds
	SOL3_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all character switches)
	// stats for mission SP_MISSION_TREVOR_1
	TRV1_NO_SURVIVORS, // Set to true when/if the player kills all the fleeing bikers after the shootout.
	TRV1_HEADSHOTS, // Headshots (Player gets 12 headshots)
	TRV1_TIME, // Mission Time (Player completes mission in 12 minutes)
	TRV1_TRAILER_DAMAGE, // Trailer Damage (Player causes $X amount of damage to the trailer)
	TRV1_BIKERS_KILLED_BEFORE_LOCATION, // Lost and damned (Player kills the Lost and Damned bikers during the chase before they reach the location)
	TRV1_CAR_DAMAGE, // Total damage taken to Trevor's truck
	TRV1_MAX_SPEED, // Max speed reached in Trevor's truck
	TRV1_INNOCENTS_KILLED, // Innocent pedestrians killed (run over etc)
	TRV1_KILLS, // Enemies killed
	TRV1_DAMAGE, // Trevor health damage
	TRV1_SPECIAL_ABILITY_TIME, // Time in ms using special ability.
	TRV1_BULLETS_FIRED, // Total bullets fired on mission
	TRV1_ACCURACY, // Overall accuracy
	// stats for mission SP_MISSION_TREVOR_2
	TRV2_HEADSHOTS, // Headshots (Player gets 5 headshots)
	TRV2_TIME, // Mission Time (Player completes the mission in 12 mis 30 seconds)
	TRV2_RACEBACK_WON, // Nervous Twitch (Player beats the buddy back to the airstrip)
	TRV2_UNDERBRIDGES, // 6 bridges, 1 plane (Player flies under any 6 bridges over the river)
	TRV2_RACE_TIME, // Time taken to fly back to the airstrip after dropping off the package.
	TRV2_ATV_DAMAGE, // Damage taken to the ATV
	TRV2_ATV_MAX_SPEED, // Max speed reached on the ATV
	TRV2_INNOCENTS_KILLED, // Innocents pedestrians killed (run over etc)
	TRV2_KILLS, // Enemies killed
	TRV2_DAMAGE, // Health damage taken by Trevor
	TRV2_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	TRV2_BULLETS_FIRED, // Total bullets fired on mission
	TRV2_PLANE_MAX_SPEED, // Max speed reached in the plane
	TRV2_PLANE_DAMAGE, // Damage taken by the plane.
	TRV2_SHOTS_TO_KILL_HELI, // Number of shots fired to take down the helicopter.
	TRV2_ALL_BIKERS_KILLED_ON_WING, // Call when/if trevor kills all the bikers.
	// stats for mission SP_MISSION_TREVOR_3
	TRV3_HEADSHOTS, // Headshots (Player gets 5 headshots)
	TRV3_NOT_DETECTED, // Mystery gift (Undetected while setting up the explosives)
	TRV3_UNMARKED, // Unmarked (Player takes less than 50 damage)
	TRV3_ALL_TRAILERS_AT_ONCE, // Perfect gift (Detonated all trailers in one go)
	TRV3_STEALTH_KILLS, // Number of stealth kills performed (melee and silenced weapons combined)
	TRV3_KILLS, // Total enemy kills
	TRV3_INNOCENTS_KILLED, // Innocent pedestrians killed (run over etc)
	TRV3_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	TRV3_BULLETS_FIRED, // Total bullets fired on this mission
	TRV3_ACCURACY, // Overall accuracy
	TRV3_STICKY_BOMBS_USED, // Number of sticky bombs used on mission.
	TRV3_TIME, // Total time taken to complete mission
	// stats for mission SP_HEIST_AGENCY_1
	AH1_EAGLE_EYE, // Eagle eye - Check all the number plates with zooming
	AH1_MISSED_A_SPOT, // Cleanly tailed - Get through to the janitors place without getting too close to be spotted
	AH1_CLEANED_OUT, // Cleaned out â€“ Get through the mission in quick time (9 minutes real time)
	AH1_CAR_DAMAGE, // Total damage taken by any vehicles the player uses while on this mission. Watch target should always be the players current vehicle.
	AH1_MAX_SPEED, // Fastest speed reached by any vehicle the player drives on the mission. Watch target should always be the players current vehicle.
	AH1_INNOCENTS_KILLED, // Number of innocent pedestrians killed (run over etc). As this mission shouldn't involve killing anyone, any kills can be considered innocent.
	AH1_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_HEIST_AGENCY_2
	AH2_QUICK_GETAWAY, // Quick getaway - Leave the building site within a set amount of time (45 seconds real time)
	AH2_DAMAGE, // Total health damage taken by the player.
	AH2_INNOCENTS_KILLED, // Number of innocent pedestrians killed on mission (run over etc). Shoudn't count the architect.
	AH2_METHOD, // Method used to kill the architect. 0 = Melee, 1 = Threaten with weapon till he faints, 2 = Stun gun, 3 = Shot with silenced weapon. Please update with any other possible methods.
	AH2_TIME, // Total time taken to complete mission
	AH2_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_HEIST_AGENCY_PREP_1
	AH1P_FIRETRUCK_SPEED, // Max speed reached in the firetruck
	AH1P_FACTORY_TIME, // Time taken to get to the garment factory. Start when the player acquires the fire truck.
	AH1P_INNOCENTS_KILLED, // Innocent peds killed (run over etc)
	AHP1_TRUCKCALLED, // Set passed if they call a fire truck
	AHP1_NOSCRATCH, // Set the watch to any fire truck the player gets in
	AH1P_TIME, // Total time taken to complete mission
	AH1P_SPECIAL_ABILITY_TIME, // Time in ms using special ability
	// stats for mission SP_HEIST_AGENCY_3A
	AH3A_KILLS, // Total enemies killed by the player, across all characters on this mission.
	AH3A_HEADSHOTS, // Total enemies the player has killed by headshots, across all characters on mission.
	AH3A_BULLETS_FIRED, // Total bullets fired throughout the mission, as all characters combined
	AH3A_ACCURACY, // Overall accuracy while on mission
	AH3A_DAMAGE, // Total health damage taken by the player (across all character switches)
	AH3A_CAR_DAMAGE, // Total vehicle damage taken on the mission. The watch target should always be the players current vehicle.
	AH3A_MAX_SPEED, // Fastest speed reached by any vehicle used by the player on the mission. Watch target should always be the players current vehicle.
	AH3A_SWITCHES, // Total number of character switches
	AH3A_INNOCENTS_KILLED, // Total number of innocent pedestrians killed (run over etc)
	AH3A_SPECIAL_ABILITY_TIME, // Total time in ms spent using the player's special ability (across all characters)
	AH3A_TIME, // Total time taken to complete mission - 18 mins
	AH3A_OXYGEN_REMAINING, // A pure percentage count of how much oxygen remains at the end of the mission. - 40%
	AH3A_FLOOR_MOP_TIME, // A timer of how long the player mops the floor. Start when they begin mopping, end when they finish planting the last bomb. Target = 3 mins
	AH3A_ABSEIL_TIME, // Start the timer when they begin abseiling, end when done. - 30 seconds
	// stats for mission SP_HEIST_AGENCY_3B
	AH3B_SWITCHES, // Total number of character switches
	AH3B_SPECIAL_ABILITY_TIME, // Total time in ms spent using the player
	AH3B_MAX_SPEED, // Fastest speed reached by any vehicle used by the player on the mission. Watch target should always be the players current vehicle.
	AH3B_KILLS, // Total enemies killed by the player, across all characters on this mission.
	AH3B_INNOCENTS_KILLED, // No innocent pedestrians/NPC's killed
	AH3B_HEADSHOTS, // Total enemies the player has killed by headshots, across all characters on mission.
	AH3B_DAMAGE, // Total health damage taken by the player (across all character switches)
	AH3B_CAR_DAMAGE, // Total vehicle damage taken on the mission. The watch target should always be the players current vehicle.
	AH3B_BULLETS_FIRED, // Total bullets fired throughout the mission, as all characters combined
	AH3B_ACCURACY, // Overall accuracy while on mission
	AH3B_FREEFALL_TIME, // Time in ms the player freefalls before opening their parachute. Start the timer as soon as they exit the vehicle and end as soon as they open their chute.
	AH3B_LANDING_ACCURACY, // Accuracy of parachute landing, bool
	AH3B_HACKING_ERRORS, // Number of errors made during the hacking mini game.
	AH3B_HACKING_TIME, // Time spent playing the hacking mini game. - 45 secs
	AH3B_TIME, // Total time taken to complete mission - 19 mins
	// stats for mission SP_HEIST_DOCKS_1
	DH1_TIME, // Mission Time - time (total play time of the mission). Target 20 mins
	DH1_EMPLOYEE_OF_THE_MONTH, // Employee of the month - bool (award for not causing any damage to any of the containers during the tasks at the docks)
	DH1_PERFECT_SURVEILLANCE, // Perfect surveillance - awarded for taking all three photographs.
	DH1_HONEST_DAYS_WORK, // An honest days work - bool (awarded for not causing any disturbance at the docks and remaining undetected)
	DH1_CAR_DAMAGE, // Total damage taken by the players vehicles while on this mission. The watch target should always be set to the players current vehicle.
	DH1_INNOCENTS_KILLED, // Number of innocent pedestrians killed
	DH1_MAX_SPEED, // Highest speed reached in ANY of the vehicles used by the player on this mission. The watch target should always be the players current vehicle.
	DH1_FORKLIFT_TIME, // Time taken to clear the forklift section of the mission. Start the timer as soon as the player gets in the vehicle and end when they drop off the last container.
	DH1_CRANE_TIME, // Time taken to clear the crane section of the mission. Start the timer as soon as the player gets in the crane and end when they drop off the last container.
	DH1_PHOTOS_TAKEN, // Number of photos taken on the mission.
	DH1_SPECIAL_ABILITY_TIME, // Total time in ms spent using the player
	// stats for mission SP_HEIST_DOCKS_PREP_1
	DH1P_SUB_TIME, // Time taken to pilot the submarine back
	DH1P_SUB_DAMAGE, // Damage taken by the submarine
	DH1P_BULLETS_FIRED, // Total bullets fired
	DH1P_TRUCK_DAMAGE, // Damage done to the truck.
	DH1P_INNOCENTS_KILLED, // Innocent peds killed
	DHP1_TIME, // Complete within 08:30
	DHP1_NO_BOARDING, // Steal the Sub without boarding the boat. Fail If the player gets on the boat
	DH1P_SPECIAL_ABILITY_TIME, // Total time in ms spent using the player
	// stats for mission SP_HEIST_DOCKS_PREP_2B
	DH2BP_KILLS, // Total enemies killed
	DH2BP_ACCURACY, // Overall accuracy
	DH2BP_HEADSHOTS, // Total headshots
	DH2BP_BULLETS_FIRED, // Total bullets fired on the mission
	DH2BP_DAMAGE, // Total health damage taken by the player
	DH2BP_HELI_TIME, // Time taken to take the Cargobob back
	DH2BP_MAX_HELI_SPEED, // Fastest speed reached in the Cargobob.
	DH2BP_HELI_TAKEDOWN_TIME, // Time taken to take down the other helicopters while in the Hunter
	DHP2_TIME, // Complete within 05:30
	DH2BP_SPECIAL_ABILITY_TIME, // Total time in ms spent using the player
	// stats for mission SP_HEIST_DOCKS_2A
	DH2A_HEADSHOTS, // Enemies killed by headshots
	DH2A_ACCURACY, // Overall accuracy across all switches
	DH2A_KILLS, // Total enemies killed across all switches
	DH2A_DAMAGE, // Total health damage taken by all characters under the players control. Watch target should always be the current controlled character
	DH2A_BULLETS_FIRED, // Total bullets fired on mission
	DH2A_ACTUAL_TAKE, // Actual take of the heist
	DH2A_PERSONAL_TAKE, // Players personal take of the heist
	DH2A_SWITCHES, // Number of character switches
	DH2A_STEALTH_KILLS, // Number of stealth kills (melee or silenced weapons) without being detected.
	DH2A_CAR_DAMAGE, // Total vehicle damage taken by any/all vehicles used by the player on the mission. Watch target should always be the players current vehicle.
	DH2A_MAX_SPEED, // Fastest speed reached in any/all vehicles used by the player on the mission. Watch target should always be the players current vehicle.
	DH2A_TIME_TO_FIND_CONTAINER, // Time taken to find the container during the scuba diving section 60 secs
	DH2A_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters)
	DH2A_TIME, // Total time taken to complete mission - 14:30
	DH2A_NO_ALARMS, // No alarms triggered, call inform if they trigger an alarm to fail them. Otherwise defaults to pass
	DH2A_TIME_TO_CLEAR, // Start timer when the shootout begins, stop it at the end. - 1 min 30 secs
	// stats for mission SP_HEIST_DOCKS_2B
	DH2B_DAMAGE, // Total health damage taken by all characters under the players control. Watch target should always be the current controlled character
	DH2B_ACTUAL_TAKE, // Actual take of the heist
	DH2B_PERSONAL_TAKE, // Players personal take of the heist
	DH2B_SWITCHES, // Number of character switches
	DH2B_MAX_SPEED, // Fastest speed reached in any/all vehicles used by the player on the mission. Watch target should always be the players current vehicle.
	DH2B_SUB_DAMAGE, // Damage taken in the Submarine
	DH2B_HELI_DAMAGE, // Damage taken in the Helicopter
	DH2B_TIME_TO_FIND_CONTAINER, // Time taken to find the container during the submarine section.
	DH2B_TIME, // Total time taken to complete mission - 14:30
	DH2B_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters)
	DH2B_ACCURACY, // Overall accuracy across all switches
	DH2B_KILLED_ALL_MERRYWEATHER, // The player kills all the merryweather peds that pursue them.
	DH2B_ESCAPE_TIME, // Time taken for the player to escape/kill merryweather.
	DH2B_KILLS, // Total enemies killed across all switches
	DH2B_HEADSHOTS, // Enemies killed by headshots
	DH2B_BULLETS_FIRED, // Total bullets fired on mission
	// stats for mission SP_HEIST_FINALE_1
	FH1_CAR_DAMAGE, // Total damage taken by any road vehicles used by the player on the mission. Watch target should always be the players current road vehicle.
	FH1_HELI_DAMAGE, // Total damage taken by the helicopter.
	FH1_MAX_SPEED, // Fastest speed reached in a road vehicle on this mission. Watch target should always be the players current road vehicle.
	FH1_MAX_HELI_SPEED, // Fastest speed reached in the helicopter.
	FH1_INNOCENTS_KILLED, // Total number of innocent pedestrians killed (run over etc)
	FH1_SWITCHES, // Total number of character switches
	FH1_PERFECT_DISTANCE, // Player was never warned for flying too close or too far away from the security van. Set to true if they were warned.
	FH1_FIND_HOLE_TIME, // Time it takes the player to find the hole after being instructed by the on screen message.
	FH1_UNDER_BRIDGE, // Player flew under the bridge. Set to true if the player flies under the bridge.
	FH1_THROUGH_TUNNEL, // Player flew through the tunnel. Set to true if the player flies through the tunnel.
	FH1_TIME, // Total time taken to complete mission - 11 mins
	FH1_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters)
	// stats for mission SP_HEIST_FINALE_PREP_A
	FHPA_ESCAPE_TIME, // How long to escape with wagon, open time window when entering wagon, close when escape complete - 2 mins
	FHPA_DAMAGE_TO_WAGON, // Damage to the Police wagon
	FHPA_TIME, // Total time taken to complete the mission
	FHPA_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters)
	// stats for mission SP_HEIST_FINALE_PREP_B
	FHPB_CAR_DAMAGE, // Total damage taken in the truck
	FHPB_MAX_SPEED, // Fastest speed reached in the truck
	FHPB_INNOCENTS_KILLED, // Innocent pedestrians killed (run over etc)
	FHPB_TRUCK_TIME, // Time taken to get to the destination in the truck
	FHPB_SNEAK_THIEF, // Steal the Cutter without being detected
	FHPB_TIME, // Complete within 05:00
	FHPB_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters)
	// stats for mission SP_HEIST_FINALE_2A
	FH2A_CAR_DAMAGE, // Total vehicle damage taken across all vehicles used by the player. Watch target should always be the players current vehicle.
	FH2A_MAX_SPEED, // Fastest speed reached in any vehicle the player uses on mission. Watch target should always be the players current vehicle.
	FH2A_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	FH2A_INNOCENTS_KILLED, // Innocent pedestrians killed (run over etc)
	FH2A_ACTUAL_TAKE, // Actual take from Heist
	FH2A_CREW_TAKE, // Crew take from Heist
	FH2A_KILLS, // Enemies killed in total across all characters
	FH2A_SWITCHES, // Total number of character switches
	FH2A_HEADSHOTS, // Total number of enemies killed by headshot - 20
	FH2A_DAMAGE, // Total health damage taken by the player. This should count damage taken by all characters involved in the mission when controlled by the player.
	FH2A_TRAFFIC_LIGHT_CHANGES, // Number of traffic light changes - 10
	FH2A_PERSONAL_TAKE, // Personal take from Heist
	FH2A_TIME, // Total time taken to complete mission - target 10 mins
	FH2A_ACCURACY, // General accuracy
	// stats for mission SP_HEIST_FINALE_2B
	FH2B_ACTUAL_TAKE, // Actual take from Heist
	FH2B_CAR_DAMAGE, // Total vehicle damage taken across all vehicles used by the player. Watch target should always be the players current vehicle.
	FH2B_CREW_TAKE, // Crew take from Heist
	FH2B_DAMAGE, // Total health damage taken by the player. This should count damage taken by all characters involved in the mission when controlled by the player.
	FH2B_HEADSHOTS, // Total number of enemies killed by headshot
	FH2B_INNOCENTS_KILLED, // Innocent pedestrians killed (run over etc)
	FH2B_KILLS, // Enemies killed in total across all characters
	FH2B_MAX_SPEED, // Fastest speed reached in any vehicle the player uses on mission. Watch target should always be the players current vehicle.
	FH2B_PERSONAL_TAKE, // Personal take from Heist
	FH2B_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	FH2B_SWITCHES, // Total number of character switches
	FH2B_TIME, // Total time taken to complete mission - target 16 mins
	FH2B_CHASE_TIME, // How long the chase takes - 1 min 10 secs
	FH2B_GOLD_DROP_TIME, // How long does it take to drop off the gold
	FH2B_ACCURACY, // Finish with a shooting accuracy of at least 60%
	// stats for mission SP_HEIST_JEWELRY_1
	JH1_CAR_DAMAGE, // Car damage taken
	JH1_MAX_SPEED, // Max vehicle speed reached
	JH1_INNOCENTS_KILLED, // Innocent pedestrians killed (e.g. run over)
	JH1_PHOTOS_TAKEN, // Number of photos taken with the glasses.
	JH1_TIME, // Total time taken to complete mission
	JH1_PERFECT_PIC, // Capture all 3 security features in one picture
	JH1_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	// stats for mission SP_HEIST_JEWELRY_PREP_1A
	JH1P_INNOCENTS_KILLED, // Innocents peds killed (run over etc)
	JH1P_MAX_SPEED, // Fastest speed the player has reached in the van
	JH1P_VAN_DAMAGE, // Total damage taken by the van
	JHP1A_SNEAKY_PEST, // Sneaky Pest ? ask imran
	JHP1A_TIME, // Total time taken to complete mission
	JH1P_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	// stats for mission SP_HEIST_JEWELRY_PREP_2A
	JH2AP_MAX_SPEED, // Fastest speed the player has reached in any vehicle while on the mission.
	JH2AP_CAR_DAMAGE, // Total damage taken by any vehicles used by the player on the mission.
	JH2AP_INNOCENTS_KILLED, // Innocents peds killed (run over etc)
	JH2AP_FACTORY_TIME, // Time taken to return the grenades to the factory. Timer should start when the player acquires them.
	JHP2A_LOOSE_CARGO, // Shoot open the back doors to release the cargo
	JHP2A_TIME, // Total time taken to complete mission
	JHP2A_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	// stats for mission SP_HEIST_RURAL_1
	RH1_LEISURELY_DRIVE, // Leisurely drive - Player drives to the bank in 3 mins 30 secs
	RH1_CAR_DAMAGE, // Car damage taken throughout the mission.
	RH1_MAX_SPEED, // Max car speed reached on mission.
	RH1_ACCURACY, // Accuracy on mission. Shooting the alarm should count as a shot on target.
	RH1_BULLETS_FIRED, // Total bullets fired on this mission
	RH1_SWITCHES, // Number of times the player switched characters during the mission
	RH1_WINNER, // Player wins the race back to the meth lab.
	RH1_TIME, // Total time taken to complete mission - 11 mins
	RH1_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	// stats for mission SP_HEIST_RURAL_PREP_1
	RH1P_MAX_SPEED, // Fastest speed reached in any vehicle controlled by the player
	RH1P_KILLS, // Total enemies killed
	RH1P_ACCURACY, // Overall accuracy on mission
	RH1P_HEADSHOTS, // Enemies killed by headshots
	RH1P_DAMAGE, // Total damage taken by the player
	RH1P_BULLETS_FIRED, // Total bullets fired on mission
	RH1P_VAN_TO_LAB_TIME, // Time taken to get the van to the meth lab
	RH1P_CAR_DAMAGE, // Total vehicle damage taken across all vehicles used on the mission
	RH1P_CONVOY_STOPPED, // player uses sticky bombs to stop the convoy – blow lead car or rear car.
	RH1P_TIME, // Total time taken to complete mission
	RH1P_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	// stats for mission SP_HEIST_RURAL_2
	RH2_CAR_DAMAGE, // Car damage taken by the van at the start of the mission.
	RH2_MAX_SPEED, // Max speed reached by the van at the start of the mission.
	RH2_BULLETS_FIRED, // Total bullets fired on the mission.
	RH2_KILLS, // Number of enemies killed on the mission.
	RH2_DAMAGE, // Amount of health damage taken by Franklin while under the player's control.
	RH2_ACTUAL_TAKE, // Actual take value of heist.
	RH2_PERSONAL_TAKE, // Personal take value of heist.
	RH2_HEADSHOTS, // Number of headshot kills while on mission.
	RH2_ACCURACY, // Overall accuracy while on mission.
	RH2_TIME, // Time taken to complete mission
	RH2_COLLATERAL_DAMAGE, // An arbitrary dollar total of the damage done, increment to raise. Call when/if the player hits gas station to pass.
	RH2_SPECIAL_ABILITY_TIME, // Total time in ms using special abilities (across all characters).
	// stats for mission RC_BARRY_1
	BA1_DAMAGE_TAKEN, // Unmarked (player takes less than 50 damage)
	BA1_KILLCHAIN, // Kill Chain – Player kills X number of aliens in under X amount of seconds (more likely with Minigun). Barry 1 
	// stats for mission RC_BARRY_2
	BA2_VANS_DESTROYED, // Pre-Emptive Strike – Player destroys X clown vans quickly before they have a chance to spawn clowns. Barry 2.
	BA2_DANCING_CLOWNS_KILLED, // Greatest Dancer - Kill 6 Clowns while they are dancing. Barry 2
	// stats for mission RC_BARRY_3A
	BA3A_DELIVERY_TIME, // Mission Time – Player delivers vehicle under a set amount of time (more generous due to losing cops). Barry 3A
	BA3A_AVOID_STAKEOUT, // Player completes the mission without becoming wanted. Starts TRUE, call this enum if the player is detected to fail them. Otherwise passes.
	// stats for mission RC_BARRY_3C
	BA3C_STASH_UNHOOKED, // Hooked – Stash car never unhooks from tow truck. Barry 3C . Call this to fail it. Otherwise defaults to pass.
	BA3C_DELIVERY_TIME, // Mission Time – Player delivers vehicle within a set amount of time.  Barry 3C. 
	// stats for mission RC_DREYFUSS_1
	DR1_DREYFUSS_KILLED, // Cut! – Player kills Dreyfuss
	// stats for mission RC_EPSILON_4
	EP4_ARTIFACT_DETECTOR_USED, // Use the Force – Player completes the mission without using the artefact detector. Epsilon 4. Call if they use it. Otherwise this defaults to pass.
	// stats for mission RC_EPSILON_6
	EP6_PERFECT_LANDING, // Touchdown – Player lands the epsilon plane perfectly (no damage). Epsilon 6. 
	EP6_UNDER_BRIDGE, // Canyon Crusader - Fly under one of the bridges down Raton Canyon.
	// stats for mission RC_EPSILON_8
	EP8_SECURITY_WIPED_OUT, // Extremist Exit/Cult Intervention – Player wipes out all epsilon security. Epsilon 8
	EP8_MONEY_STOLEN, // Judas/Show me the Money – Player steals the epsilon money and escapes. Epsilon 8
	// stats for mission RC_EXTREME_1
	EXT1_FALL_TIME, // Free Faller – Player falls for 7 seconds before opening shoot. Extreme 1
	EXT1_BIG_AIR, // Big Air – Player gets 2.5 seconds of air time during the bike race (can be done). Extreme 1
	EXT1_RACE_WON, // Downhill King – Player wins the bike race against Dom. Extreme 1
	// stats for mission RC_EXTREME_2
	EXT2_LEAP_FROM_ATV_TO_WATER, // Dive Bomber – Player jumps off ATV and free falls into water (doesn’t use parachute).  Extreme 2
	EXT2_NUMBER_OF_SPINS, // Sky Blazer – Player performs X number of spins on the quad bike. Extreme 2. 
	// stats for mission RC_EXTREME_3
	EXT3_DARE_DEVIL, // Dare Devil – Player opens their parachute after Dom. Extreme 3
	EXT3_PERFECT_LANDING, // Bullseye – Player lands perfectly on the back of the truck. Extreme 3
	// stats for mission RC_EXTREME_4
	EXT4_FALL_SURVIVED, // Leap of Faith – Player jumps and survives after following Dom; chute auto opens. Extreme 4
	// stats for mission RC_FANATIC_1
	FA1_SHORTCUT_USED, // Contender – Win without using shortcuts.  Fanatic 1. Call if the player does take a shortcut. Otherwise defaults to pass.
	// stats for mission RC_FANATIC_2
	FA2_BUMPED_INTO_MARY_ANN, // Good cyclist - didn't bump into Mary Ann during the race. Call this bool stat if the bump happens to fail them. Otherwise they pass.
	FA2_QUICK_WIN, // Won race under a set par time. Call when/if the player beats the par time.
	// stats for mission RC_FANATIC_3
	FA3_SHORTCUT_USED, // Champion – Win without using shortcuts. Fanatic 3. Call if the player does take a shortcut. Otherwise defaults to pass.
	// stats for mission RC_HUNTING_1
	HU1_TWO_COYOTES_KILLED_IN_ONE, // 2 for 1 – Player kills 2 coyotes with one shot.  Hunting 1. 
	HU1_TYRE_SHOOTING_ACCURACY, // Pop! Pop! – Player has X% shooting accuracy popping all three car tires. Hunting 1. Update once with percentage value.
	HU1_HIT_EACH_SATELLITE_FIRST_GO, // Bad Signal – Player hits all three satellite dishes without missing. Hunting 1
	// stats for mission RC_HUNTING_2
	HU2_DETECTED_BY_ELK, // Downwind – Player does not get detected by any elk. Hunting 2. Call this to fail them. Otherwise passes by default.
	HU2_ELK_HEADSHOTS, // Heart Hunter – Increment this stat when an elk is shot in the heart, this was changed due to 1202655
	// stats for mission RC_JOSH_2
	JO2_JOSH_MELEED, // Pulverizer – Player uses a melee weapon to beat up Avery. Josh 2
	JO2_STOPPED_IN_TIME, // Quick Catch – Player stops Avery’s car within X seconds of him leaving his house. Josh 2
	// stats for mission RC_JOSH_3
	JO3_ESCAPE_WITHOUT_ALERTING_COPS, // Out of the Frying Pan/No Time for Bacon – Player escapes without alerting the cops. Josh 3
	JO3_POUR_FUEL_IN_ONE_GO, // Pyromaniac – Player pours fuel trail in one go. Josh 3
	// stats for mission RC_JOSH_4
	JO4_JOSH_KILLED_BEFORE_ESCAPE, // Dirty Rat – Player kills Josh before escaping. Josh 4
	JO4_ESCAPE_IN_PARKED_COP_CAR, // Hot Pursuit – Player escapes in the parked cop car. Josh 4
	// stats for mission RC_MINUTE_1
	MIN1_VEHICLE_TAKEN_AFTER_STUNS, // Mariachi My Ride – Player takes the Mariachi vehicle after stunning both band members   Minute 1
	MIN1_FAST_STOP, // Stop the Music – Player stops the Mariachi within X time of the leaving bar.  Minute 1
	// stats for mission RC_MINUTE_2
	MIN2_FIRST_CAPTURE, // Downed – Capture the first immigrant within X seconds. Minute 2
	MIN2_SECOND_CAPTURE, // Double Downed – Capture the second group of immigrants within X time. Minute 2
	MIN2_STUNNED_ALL, // Shock and Awe – Player/AI uses stun gun to stop all immigrants (not ramming the off road) Minute 2
	// stats for mission RC_MINUTE_3
	MIN3_STUNNED_AND_KILLED, // What goes around… – Player uses the stun gun on both Joe and Josef before killing them. Minute 3
	MIN3_KILL_BEFORE_ESCAPE, // No Migration – Player kills Joe and Josef before they can leave the farm. Minute 3
	// stats for mission RC_NIGEL_1A
	NI1A_PLAYER_DAMAGE_DURING_BRAWL, // Fist Fury – Player takes no damage during the fight with Willy from Love Fist.  Nigel 1A
	NI1A_ENTOURAGE, // player will get awarded it for talking to the entourage
	// stats for mission RC_NIGEL_1B
	NI1B_GARDENER_TAKEN_OUT, // Weed Killer – Player take outs the gardener. Nigel 1B
	NI1B_CLOTHES_TAKEN_NO_ALERTS, // Sneak Thief – Player steals the clothes without detection. Nigel 1B
	// stats for mission RC_NIGEL_1C
	NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY, // Hot on the Paws – Player keeps close to Mr Muffy Cakes throughout chase. Nigel 1C. Call when the player gets too far away. Otherwise passes by default. 
	// stats for mission RC_NIGEL_1D
	NI1D_GOLF_CLUB_STOLEN_IN_TIME, // Under Par – Player steals the golf club off Glen Stanky in under X seconds after Starky is alerted or killed. Nigel 1D. Open the window when they can steal the club. Close it when they do.
	NI1D_HEADSHOTTED_STANKY, // Hole in One – Headshot Glen Stanky. Nigel 1D. Put only him on the watch list.
	NI1D_KILLED_STANKY_AND_GUARDS, // FOUR! – Kill Glen Stanky and his 3 security guards. Nigel 1D
	// stats for mission RC_NIGEL_2
	NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI, // Stalker – Player keeps close to Al Di Napoli during the chase. Nigel 2. Call this when the player is too far away. Otherwise they pass by default.
	NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH, // Accident & Emergency – Player doesn’t harm anyone whilst driving through the hospital. Nigel 2 Call this if the player causes any harm. Otherwise they by default pass it.
	NI2_VEHICLE_DAMAGE, // Not a Scratch – Player takes less than X% vehicle damage during chase. Nigel 2
	// stats for mission RC_NIGEL_3
	NI3_BAILED_AT_LAST_MOMENT, // Skin of your Teeth – Player exits the car at the last possible moment on train tracks. Nigel 3
	NI3_REVERSED_TO_KILL, // Locomotivation – Player kill Al Di Napoli with the train. Nigel 3
	// stats for mission RC_PAPARAZZO_1
	PAP1_TAKEN_OUT_IN_ONE_SWING, // Smack Down – Player ensures Beverly takes out the rival on his first swing. Paparazzo 1
	PAP1_PICTURES_SNAPPED, // Picture Perfect – Player helps Beverly snap X number of pictures. Paparazzo 1
	// stats for mission RC_PAPARAZZO_2
	PAP2_POOL_JUMP, // Quick Dip – Player jumps into the swimming pool whilst following Beverly. Paparazzo 2
	PAP2_FACE_RECOG_PERCENT, // Money shot – Player gets face recognition software up to X% during chase. Paparazzo 2. Call once with the percentage achieved as the optional argument.
	// stats for mission RC_PAPARAZZO_3A
	PAP3A_FAR_FROM_POPPY, // Thick of it – Player stays close to Poppy throughout the chase. Paparazzo 3A Call this if the player gets too far away. Otherwise default pass.
	PAP3A_PHOTO_IN_CUFFS, // DUI Diva – Player takes the photo of Poppy once she’s been cuffed by the cop. Paparazzo 3A
	// stats for mission RC_PAPARAZZO_3B
	PAP3B_PHOTO_SPOTTED, // Silent Snapper – Player takes the photograph of the Princess without being spotted. Paparazzo 3B Call this when if the player is spotted. It will default to pass otherwise
	PAP3B_PHOTO_OF_USE, // Royal Drag – Player takes the photo of the Princess as she’s taking a drag. Paparazzo 3B
	// stats for mission RC_PAPARAZZO_4
	PAP4_ENTIRE_CREW_KILLED_IN_ONE, // Action! – Player kills the entire crew with one shot (explosives). Paparazzo 4
	// stats for mission RC_THELASTONE
	TLO_WOUNDS, // Wounded – Player wounds the Last One X times before catching him. RCM The Last One
	TLO_AMBIENT_ANIMAL_KILLS, // Hunter – Player kills X ambient animals whilst chasing the Last One. RCM The Last One
	TLO_TO_SITE_ON_FOOT, // Mr Green – Player heads to the scat site on foot (doesn’t use Dune buggy). RCM The Last One
	// stats for mission SP_HEIST_JEWELRY_2
	JH2A_ACTUAL_TAKE, // Total value of heist
	JH2A_PERSONAL_TAKE, // Personal take from heist after deductions
	JH2A_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	JH2A_TIME, // Total time taken to complete mission - 16 mins
	JH2A_CASE_GRAB_TIME, // open window when player can smash cases, close when they're done. - 50 secs
	JH2A_FRANKLIN_BIKE_DAMAGE, // Damage taken by franklin's bike during the chase while you are in the van. Set entity watch then.
	JH2A_CASES_SMASHED, // Increment for each case smashed
	JH2A_CAR_DAMAGE, // Total damage taken by any vehicles used by the player on the mission.
	JH2A_MAX_SPEED, // Fastest speed the player has reached in any vehicle on the mission
	JH2A_INNOCENTS_KILLED, // Innocents peds killed (run over etc)
	// stats for mission SP_MISSION_FBI_4_PREP_4
	FBI4P4_FACE_TIME, // How quickly did the player buy masks? target 20 seconds
	FBI4P4_CLICHE, // Player buys all hockey masks
	FBI4P4_TIME, // Total time taken to complete the mission
	FBI4P4_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_MISSION_FBI_4_PREP_5
	FBI4P5_QUICK_SHOPPER, // Time Taken to purchase suits on entering store - 15 secs
	FBI4P5_UNITED_COLOURS, // Player bought a different colour for each heist member
	FBI4P5_TIME, // Total time taken to complete the mission
	FBI4P5_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission RC_TONYA_1
	TON1_TIME, // Complete within 05:00
	TON1_UNHOOK, // Keep the vehicle hooked until delivery. Call this to fail it. Otherwise defaults to pass
	TON1_QUICK_UNHOOK, // 
	// stats for mission RC_TONYA_2
	TON2_TIME, // Complete within 05:30
	TON2_UNHOOK, // Keep the vehicle hooked until delivery. Call this to fail it. Otherwise defaults to pass
	TON2_QUICK_UNHOOK, // 
	// stats for mission RC_TONYA_3
	TON3_TIME, // Complete within 07:00
	TON3_UNHOOK, // Keep the vehicle hooked until delivery. Call this to fail it. Otherwise defaults to pass
	TON3_QUICK_UNHOOK, // 
	// stats for mission RC_TONYA_4
	TON4_TIME, // Complete within 06:00
	TON4_UNHOOK, // Keep the vehicle hooked until delivery. Call this to fail it. Otherwise defaults to pass
	TON4_QUICK_UNHOOK, // 
	// stats for mission RC_TONYA_5
	TON5_TIME, // Complete within 05:00
	TON5_UNHOOK, // Keep the vehicle hooked until delivery. Call this to fail it. Otherwise defaults to pass
	TON5_QUICK_UNHOOK, // 
	// stats for mission RC_HAO_1
	HAO1_FASTEST_LAP, // Fastest lap time. This may need a new command to override it directly rather than time it.
	HAO1_RACE_TIME, // Overall race time
	HAO1_COLLISIONS, // Fewer than 10 collisions
	// stats for mission SP_MISSION_FINALE_A
	FINA_KILLTREV, // Award stat for killing Trevor. Pass if player killsTrevor and not Michael
	FINA_TIME, // Time taken to pass the mission
	FINA_MAX_SPEED, // Max speed reached by Franklin's car in the chase
	FINA_CAR_DAMAGE, // Car damage taken by Franklin's car in the chase
	FINA_BULLETS_FIRED, // Total bullets fired by the player on the mission
	FINA_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_MISSION_FINALE_B
	FINB_KILLMIC, // Auto awarded stat for killing Michael. No action required. No action required.
	FINB_TIME, // Total time taken to pass mission
	FINB_MAX_SPEED, // Max speed reached in Franklins car in the chase
	FINB_BULLETS_FIRED, // Number of bullets fired by the player on the mission
	FINB_CAR_DAMAGE, // Damage taken by Franklin's car in the chase
	FINB_CHOICE, // Store whether the player attempted to save or kill Michael. 0 = save, 1 = kill
	FINB_FOOTCHASE_TIME, // Time taken to chase michael on foot after the car chase. Start timer when Franklin exits the car and end when the chase is over.
	FINB_DAMAGE, // Health damage taken by franklin on the mission
	FINB_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_MISSION_SOLOMON_1
	SOL1_TIME, // Complete the mission in 10 minutes
	SOL1_SILENT_TAKEDOWNS, // Silent takedowns, either melee or silenced weapons
	SOL1_BRAWL_DAMAGE, // Take no damage during the fight with Rocco
	SOL1_PERFECT_LANDING, // Landed the helicopter without damaging it
	SOL1_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	SOL1_MAX_SPEED, // Fastest speed reached in any vehicle on the mission. Watch target should always be the players current vehicle.
	SOL1_CAR_DAMAGE, // Car damage taken by the players car on the mission. This should watch any/every car they enter.
	SOL1_INNOCENT_KILLS, // Innocent pedestrians killed (e.g. run over)
	SOL1_HELI_DAMAGE, // Heli damage taken in the flying section of the mission.
	// stats for mission SP_MISSION_TREVOR_4
	TRV4_TIME, // Overall mission time - 4 mins
	TRV4_MAX_SPEED, // Max speed reached in Trevor's truck
	TRV4_CAR_DAMAGE, // Car damage taken in trevors truck
	TRV4_INNOCENTS_KILLED, // Number of innocent pedestrians killed (run over etc)
	TRV4_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_HEIST_FINALE_PREP_C1
	FINPC1_MAPPED, // Player took the gauntlet that was in the email picture
	FINPC1_TIME, // Total time taken to complete the mission after entering the car
	FINPC1_CAR_DAMAGE, // Total car damage taken on the mission in the Gauntlet
	FINPC1_MAX_SPEED, // Max speed reached in the Gauntlet
	FINPC1_TIME_TO_GARAGE, // Time taken to get the car to the garage. Start timer when entering the car, end when entering the garage
	FINPC1_INNOCENTS_KILLED, // Number of innocents killed (run over etc)
	FINPC1_MOD_GAUNTLET, // Total amount spent modding Gauntlet
	FINPC1_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_HEIST_FINALE_PREP_C2
	FINPC2_MAPPED, // Player took the gauntlet that was in the email picture
	FINPC2_TIME, // Total time taken to complete the mission after entering the car
	FINPC2_CAR_DAMAGE, // Total car damage taken on the mission in the Gauntlet
	FINPC2_MAX_SPEED, // Max speed reached in the Gauntlet
	FINPC2_TIME_TO_GARAGE, // Time taken to get the car to the garage. Start timer when entering the car, end when entering the garage
	FINPC2_INNOCENTS_KILLED, // Number of innocents killed (run over etc)
	FINPC2_MOD_GAUNTLET, // Total amount spent modding Gauntlet
	FINPC2_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_HEIST_FINALE_PREP_C3
	FINPC3_MAPPED, // Player took the gauntlet that was in the email picture
	FINPC3_TIME, // Total time taken to complete the mission after entering the car
	FINPC3_CAR_DAMAGE, // Total car damage taken on the mission in the Gauntlet
	FINPC3_MAX_SPEED, // Max speed reached in the Gauntlet
	FINPC3_TIME_TO_GARAGE, // Time taken to get the car to the garage. Start timer when entering the car, end when entering the garage
	FINPC3_INNOCENTS_KILLED, // Number of innocents killed (run over etc)
	FINPC3_MOD_GAUNTLET, // Total amount spent modding Gauntlet
	FINPC3_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_HEIST_FINALE_PREP_D
	FINPD_TIME, // Complete within 04:30 
	FINPD_UNDETECTED, // Steal the train without being detected
	FINPD_KILLS, // Number of enemies killed
	FINPD_STEALTH_KILLS, // Number of enemies killed by stealth
	FINPD_MAX_HELI_SPEED, // Max speed reached in the helicopter
	FINPD_HELI_DAMAGE, // Damage taken in the helicopter
	FINPD_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	// stats for mission SP_HEIST_JEWELRY_PREP_1B
	JHP1B_SWIFT_GETAWAY, // Lose the wanted level within 02:00
	JHP1B_TIME, // Total time taken to complete the mission
	JHP1B_CAR_DAMAGE, // Damage taken to the stolen FIB car
	JHP1B_DAMAGE, // Health damage taken by the player
	JHP1B_MAX_SPEED, // Max speed reached in the stolen FIB vehicle
	JHP1B_INNOCENTS_KILLED, // Number of innocent peds killed (run over etc)
	JHP1B_SPECIAL_ABILITY_TIME, // Amount of time in ms using special ability.
	MAX_MISSION_STATS,	
	EMPTY_MISSION_STAT,

//BEGIN CLF STATS
	UNSET_MISSION_STAT_ENUM_CLF = -1,
	// stats for mission SP_MISSION_CLF_ASS_POL
	DAS_POL_lawyered, // Lawyered Up - Find and take out the lawyer to gain access to the target. (1717126)
	DAS_POL_loudExec, // Loud Execution - Kill the target using explosives. (1717134)
	DAS_POL_shanked, // Shanked - Kill the target using a knife. (1717128)
	DAS_POL_time, // Mission Time - Complete within 02:50 (1717768)
	DAS_POL_unmarked, // Unmarked - Complete with minimal damage to health and armor (1717130)
	DAS_POL_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_POL_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_POL_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_POL_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_POL_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_POL_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_POL_KILLS, // Track total kills on enemies in the mission.
	DAS_POL_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_POL_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_RET
	DAS_RET_carrier, // Carriers Capped - Kill the briefcase carriers with a headshot (1717198)
	DAS_RET_noManSta, // No Man Standing - Kill all security guards (1717190)
	DAS_RET_preForce, // Pre-emptive Force - Plant a Sticky Bomb at the deal location to kill targets on arrival (1717191)
	DAS_RET_sightsee, // Sightseer - Use a telescope to observe the deal (1717193)
	DAS_RET_time, // Mission Time - Complete within 06:40 (1717808)
	DAS_RET_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_RET_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_RET_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_RET_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_RET_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_RET_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_RET_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_RET_KILLS, // Track total kills on enemies in the mission.
	DAS_RET_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_RET_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_CAB
	DAS_CAB_disconn, // Disconnected - Snipe the cable car rig to drop the car and kill the target (1717173)
	DAS_CAB_lazyKill, // Lazy Kill - Wait for the target to reach the bottom before killing him (1717176)
	DAS_CAB_outOfBat, // Out of Batteries - Stop the cable car using the console (1717178)
	DAS_CAB_paraDr, // Parachute Drive-by - Kill the target as you parachute past (1717175)
	DAS_CAB_time, // Mission Time Complete within 03:15 (1717801 & 1718529)
	DAS_CAB_tipTop, // Tip Top - Reach the top of mount Chiliad within 03:30 time (1717177 & 1717992)
	DAS_CAB_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_CAB_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_CAB_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_CAB_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_CAB_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_CAB_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_CAB_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_CAB_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_CAB_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_GEN
	DAS_GEN_falseAl, // False Alarm – Kill the general without being detected (1717153)
	DAS_GEN_hidPack, // Hidden Package – Hide in the back of the truck to enter the base (1717151)
	DAS_GEN_overOut, // Over and Out – Snipe the general from the radio tower (1717155)
	DAS_GEN_strMst, // Stairmaster – Take the stairs to the top of the control tower (1717159) ...and back down again (1769973)
	DAS_GEN_time, // Mission Time - Complete within 06:00 (1717770)
	DAS_GEN_miliHard, // Military Hardware – Kill the general using a Buzzard (1717156)
	DAS_GEN_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_GEN_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_GEN_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_GEN_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_GEN_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_GEN_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_GEN_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_GEN_KILLS, // Track total kills on enemies in the mission.
	DAS_GEN_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_GEN_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_SUB
	DAS_SUB_Headhunt, // Headhunted – Kill the target with a headshot (1737419)
	DAS_SUB_ScaredOf, // Scared of the Dark – Follow the tram on the surface (1717163)
	DAS_SUB_time, // Mission Time - Complete within 06:10 (1717781 & 1744661)
	DAS_SUB_StayOnTa, // Stay on Target – Do not kill the target’s companion (1737418)
	DAS_SUB_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_SUB_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_SUB_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_SUB_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_SUB_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_SUB_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_SUB_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_SUB_KILLS, // Track total kills on enemies in the mission.
	DAS_SUB_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_SUB_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_DEA
	DAS_DEA_demoliti, // Demolition Kid - Kill targets using a vehicle armed with Sticky Bombs (1717172)
	DAS_DEA_hearNoEv, // Hear No Evil - Kill 3 enemies using stealth attacks (1717174)
	DAS_DEA_noMercy, // No Mercy - Kill all targets before they start to escape (1717185)
	DAS_DEA_runningM, // Running Men - Kill all fleeing targets with a sniper rifle (1717181)
	DAS_DEA_time, // Mission Time - Complete within 02:20 (1717807)
	DAS_DEA_untouch, // Untouchable - Complete with no damage to health and armor (1717186) (note - changed to unmarked)
	DAS_DEA_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_DEA_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_DEA_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_DEA_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_DEA_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_DEA_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_DEA_KILLS, // Track total kills on enemies in the mission.
	DAS_DEA_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_DEA_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_HEL
	DAS_HEL_untouch, // Untouchable - Complete with minimal damage to health and armor (1717149)
	DAS_HEL_time, // Mission Time - Complete within 02:50 (1717769)
	DAS_HEL_headshot, // Headshot Pilot - kill the pilot as he lands to make it crash and kill all targets (1717146)
	DAS_HEL_grounded, // Grounded - Wait for the helicopter to land before taking out the targets (1717141)
	DAS_HEL_flySwat, // Fly Swatter - Shoot down the helicopter with a rocket (1717136)
	DAS_HEL_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_HEL_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_HEL_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_HEL_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_HEL_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_HEL_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_HEL_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_HEL_KILLS, // Track total kills on enemies in the mission.
	DAS_HEL_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_HEL_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_BAR
	DAS_BAR_hotShave, // Hot Shave - kill target with scissors
	DAS_BAR_OffTop, // A Little off the Top - Kill the target with a headshot
	DAS_BAR_quartet, // Quartet - Kill all 3 guards and the target
	DAS_BAR_silence, // Silence Please - kill the target with a silenced weapon
	DAS_BAR_time, // Mission Time Complete within 01:20
	DAS_BAR_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_BAR_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_BAR_KILLS, // Track total kills on enemies in the mission.
	DAS_BAR_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_BAR_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_BAR_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_BAR_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_BAR_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_BAR_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	DAS_BAR_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	// stats for mission SP_MISSION_CLF_ASS_STR
	DAS_STR_driveByW, // Drive by Win - Kill all racers using drive by (1717182)
	DAS_STR_pullOver, // Pull Over - Use a cop car to stop the racers (1717180)
	DAS_STR_sticky, // Sticky Win - Kill all racers using sticky bombs (1717179)
	DAS_STR_time, // Mission Time Complete within 03:10 (1717797)
	DAS_STR_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_STR_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_STR_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_STR_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_STR_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_STR_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_STR_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_STR_KILLS, // Track total kills on enemies in the mission.
	DAS_STR_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_STR_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_VIN
	DAS_VIN_Accuracy, // Accuracy - Finish with a shooting accuracy of 100% (1717164& 1718259)
	DAS_VIN_Capod, // Capo'd - Headshot each boss (1717161)
	DAS_VIN_EngineTr, // Engine Trouble - Use a Sticky Bomb to kill all targets after they've entered their car (1747236)
	DAS_VIN_RainingG, // Raining Grenades - Kill all targets in one explosion thrown blind over the house (1717160 & 1747245)
	DAS_VIN_time, // Mission Time Complete within 01:50 (1717804 & 1718385)
	DAS_VIN_Unmark, // Unmarked - Complete with minimal damage to health and armor (1717165) (Note - changed to Untouchable)
	DAS_VIN_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_VIN_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_VIN_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_VIN_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_VIN_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_VIN_KILLS, // Track total kills on enemies in the mission.
	DAS_VIN_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_VIN_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_HNT
	DAS_HNT_Huntsman, // Huntsman - Kill 3 animals (1717152)
	DAS_HNT_ProofKil, // Proof of Kill - Snap a photo of the dead hunter (1717148)
	DAS_HNT_Scoped, // Scoped - Kill the hunter with a sniper rifle (1717144)
	DAS_HNT_time, // Mission Time Complete within 02:40 (1717805 & 1718524)
	DAS_HNT_UpClose, // Up Close and Personal - Melee kill the hunter (1717154)
	DAS_HNT_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_HNT_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_HNT_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_HNT_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_HNT_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_HNT_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_HNT_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_HNT_KILLS, // Track total kills on enemies in the mission.
	DAS_HNT_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_HNT_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_ASS_SKY
	DAS_SKY_HeadTrau, // Head Trauma - Headshot at least 1 target as they parachute down (1717138)
	DAS_SKY_OffCours, // Off Course - Shoot all 3 targets parachutes to send them off course (1717135)
	DAS_SKY_RoughLan, // Rough Landing - Kill the all 3 characters before they land (1717124)
	DAS_SKY_time, // Mission Time Complete within 01:30 (1717810 & 1718581)
	DAS_SKY_ACCURACY, // Track weapon accuracy throughout the mission.
	DAS_SKY_BULLETS_FIRED, // Track total shots fired by the player on the mission.
	DAS_SKY_CAR_DAMAGE, // Track total damage taken by all vehicles driven by the player on the mission.
	DAS_SKY_COP_LOSS_TIME, // If required, track the total time the player spent escaping a wanted level on the mission. Start the timer when they get the wanted level and end it when they lose it completely. If possible make this increment if the player has multiple wanted levels on the mission.
	DAS_SKY_HEADSHOTS, // Track total headshot kills on enemies in the mission.
	DAS_SKY_HEALTH_DAMAGE, // Track health damage taken by the player on the mission.
	DAS_SKY_INNOCENTS_KILLED, // Track innocent civilians killed by the player on the mission.
	DAS_SKY_KILLS, // Track total kills on enemies in the mission.
	DAS_SKY_MAX_SPEED, // Track the fastest speed reached by the players vehicle on the mission.
	DAS_SKY_SPECIAL_ABILITY_TIME, // Track the time in milliseconds which the player used the special ability on the mission.
	// stats for mission SP_MISSION_CLF_TRAIN
	DCLF_TRAIN_COMP, // Complete Trevor Train (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_FIN
	DCLF_FIN_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_SPA_1
	DCLF_SPA1_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_SPA_2
	DCLF_SPA2_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_SPA_FIN
	DCLF_SPA_FIN_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CIA_TRA
	Dclf_CIA_TRA_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CIA_LIE
	Dclf_CIA_LIE_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CIA_JET
	Dclf_CIA_JET_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CIA_HEL
	Dclf_CIA_HEL_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CIA_DRO
	Dclf_CIA_DRO_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CIA_RTS
	Dclf_CIA_RTS_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_KOR_PRO
	Dclf_KOR_PRO_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_KOR_RES
	Dclf_KOR_RES_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_KOR_SUB
	Dclf_KOR_SUB_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_KOR_SAT
	Dclf_KOR_SAT_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_KOR_TNK
	Dclf_KOR_5_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RUS_PLA
	Dclf_RUS_PLA_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RUS_CAR
	Dclf_RUS_CAR_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RUS_VAS
	Dclf_RUS_VAS_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RUS_SAT
	Dclf_RUS_SAT_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RUS_JET
	Dclf_RUS_JET_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RUS_CLK
	Dclf_RUS_CLK_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_ARA_1
	Dclf_ARA_1_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_ARA_DEF
	Dclf_ARA_DEF_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_ARA_FAKE
	Dclf_ARA_FAKE_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_ARA_TNK
	Dclf_ARA_TNK_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CAS_SET
	Dclf_CAS_SET_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CAS_PR1
	Dclf_CAS_PR1_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CAS_PR2
	Dclf_CAS_PR2_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CAS_PR3
	Dclf_CAS_PR3_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_CAS_HEI
	Dclf_CAS_HEI_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_MIL_DAM
	Dclf_MIL_DAM_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_MIL_PLA
	Dclf_MIL_PLA_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_MIL_RKT
	Dclf_MIL_RKT_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_JET_1
	Dclf_JET_1_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_JET_2
	Dclf_JET_2_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_JET_3
	Dclf_JET_3_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_JET_REP
	Dclf_JET_REP_COMP, // Complete Mission (PLACEHOLDER)
	// stats for mission SP_MISSION_CLF_RC1_1
	Dclf_RC1_1_COMP, // Complete Mission (PLACEHOLDER)
	MAX_MISSION_STATS_CLF,
	EMPTY_MISSION_STAT_CLF,
//END CLF STATS

//BEGIN NRM STATS
	UNSET_MISSION_STAT_ENUM_NRM = -1,
	MAX_MISSION_STATS_NRM,
	EMPTY_MISSION_STAT_NRM
//END NRM STATS
	
ENDENUM
