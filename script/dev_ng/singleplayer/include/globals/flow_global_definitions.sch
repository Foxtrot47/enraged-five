
//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 	Flow Global Definitions Header							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			19/11/13												│
//│		DESCRIPTION: 	All global definitions required by the singleplayer		│
//│						gameflow.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "Blip_Enums.sch"
USING "flow_commands_enums_GAME.sch"
USING "flow_debug_globals.sch"
USING "vector_id_globals.sch"
USING "cellphone_globals.sch"
USING "clock_globals.sch"
USING "comms_control_globals.sch"
USING "mission_stat_globals.sch"
USING "vehicle_gen_globals.sch"
USING "ped_component_globals.sch"

USING "flow_structs_core.sch"
USING "flow_structs_core_override.sch"
USING "flow_structs_GAME.sch"
USING "cutscene_control_globals.sch"


// ===========================================================================================================
//      Control Variables - Unsaved
// ===========================================================================================================

//Cutscene section indexes. Shorthand INT versions of CUTSCENE_SECTION.
//Must match the CUTSCENE_SECTION enum in commands_cutscene.sch
CONST_INT   CS_NONE -1
CONST_INT   CS_ALL  0
CONST_INT   CS_1    BIT0
CONST_INT   CS_2    BIT1
CONST_INT   CS_3    BIT2
CONST_INT   CS_4    BIT3
CONST_INT   CS_5    BIT4
CONST_INT   CS_6    BIT5
CONST_INT   CS_7    BIT6
CONST_INT   CS_8    BIT7
CONST_INT   CS_9    BIT8
CONST_INT   CS_10   BIT9
CONST_INT   CS_11   BIT10
CONST_INT   CS_12   BIT11
CONST_INT   CS_13   BIT12

//Flow cutscene variation setting variables.
STRUCT FlowCutsceneVariation
    INT             iHandleIndex
    PED_COMPONENT   eComponent
    INT             iDrawable
    INT             iTexture
ENDSTRUCT

STRUCT FlowCutsceneOutfit
    INT                 iHandleIndex
    MODEL_NAMES         eModel
    PED_COMP_NAME_ENUM  eOutfit
ENDSTRUCT

STRUCT FlowPedVariation
    INT             iHandleIndex
    PED_INDEX       ped
ENDSTRUCT

STRUCT FlowCutsceneProp
    INT                 iHandleIndex
    PED_PROP_POSITION   ePosition
    INT                 iPropIndex
    INT                 iTextureIndex
ENDSTRUCT

CONST_INT MAX_QUEUED_CUTSCENE_HANDLES 6
CONST_INT MAX_QUEUED_CUTSCENE_VARIATIONS 30
CONST_INT MAX_QUEUED_OUTFIT_VARIATIONS  4
CONST_INT MAX_QUEUED_PED_VARIATIONS 5
CONST_INT MAX_QUEUED_CUTSCENE_PROPS 5


// Defines the range from a mission blip at which mission cutscenes will start loading/unloading.
CONST_FLOAT FLOW_LOAD_INTRO_CUTSCENE_RANGE      50.0
CONST_FLOAT FLOW_UNLOAD_INTRO_CUTSCENE_RANGE    60.0

CONST_INT   FLOW_FAIL_FADE_TIME                 2000
CONST_INT   FLOW_STANDARD_FAIL_FADE_DELAY       2000
CONST_INT   FLOW_DEATH_ARREST_FAIL_FADE_DELAY   4500


// Used by completion percentage + repeat play to track mission type
ENUM enumGrouping
    CP_GROUP_NO_GROUP,
    CP_GROUP_MISSIONS,
    CP_GROUP_MISSIONS_INGAME_BUT_NOT_100PERCENT,
    CP_GROUP_MINIGAMES,
    CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT,
    CP_GROUP_ODDJOBS,
    CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT,
    CP_GROUP_RANDOMCHARS,
    CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT,
    CP_GROUP_RANDOMEVENTS,
    CP_GROUP_RANDOMEVENTS_INGAME_BUT_NOT_100PERCENT,
    CP_GROUP_MISCELLANEOUS,
    CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT,
    CP_GROUP_FRIENDS,
    CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT
ENDENUM


// ===========================================================================================================
//      Repeat Play Variables.
// ===========================================================================================================
STRUCT RepeatPlayData
    INT iMissionIndex               // the index of the mission that is currently being repeat played
    enumGrouping eMissionType       // story mission/ RC.  Used to check correct array when using mission index
ENDSTRUCT

// Bitset used to store repeat play global bools
ENUM REPEAT_PLAY_BITS
    RPB_ACTIVE,
    RPB_PASSED,        				// did the player pass this repeat play mission?
	RPB_PAUSED_GAME,				// has the game been paused for repeat play setup?
	RPB_FROZEN_PLAYER,				// has the repeat play controller frozen the player?
	RPB_FROZEN_ON_CABLE_CAR_UP,		// were we on the cable car moving up when repeat play kicked in
	RPB_FROZEN_ON_CABLE_CAR_DOWN,	// were we on the cable car moving down when repeat play kicked in
	RPB_FROZEN_ON_FERRIS_WHEEL,		// were we on the ferris wheel when repeat play kicked in
	RPB_FROZEN_ON_ROLLERCOASTER,	// were we on the rollercoaster when repeat play kicked in
	RPB_PLAYER_BROWSING_MOD_SHOP,	// were we in the mod shop buying things
	RPB_PLAYER_BROWSING_TATTOO_SHOP	// were we in the tattoo shop buying things
ENDENUM


// --------Mission titles--------------------------------------------
// State enum for displaying mission titles
ENUM MISSION_TITLE_STATE
    MTS_CHECK_INTRO,
    MTS_LOAD_INTRO,
    MTS_PLAY_INTRO,
    MTS_POST_INTRO,
    MTS_DISPLAY_CUTSCENE, 
    MTS_DISPLAY_GAMEPLAY,
    MTS_DONE
ENDENUM


// ===========================================================================================================
//      Available Mission Tracking
// ===========================================================================================================

// Available Missions
// Contains the index within the mission variables array for all missions currently available to the player

// -----------------------------------------------------------------------------------------------------------

// REMEMBER: New additions to the struct need to be cleared out: Clear_Available_Missions_Array()

STRUCT AVAILABLE_MISSION_STORAGE
    INT             index               // The array position of this mission's variables
    INT             bitflags            // Available Mission bools organised as BitFlags
    INT             missionTriggerID    // The Mission Trigger ID - used for missions that register with the mission triggering system
    INT             missionCandidateID  // The Mission Candidate ID - used for missions that communicate directly with the mission candidate system
    THREADID        theThread           // ThreadID for this available mission - only used when running
ENDSTRUCT


// ===========================================================================================================
//      Individual Mission's Data
// ===========================================================================================================

CONST_INT   NULL_HOUR   -1

//Mission settings bit indexes.
CONST_INT   MF_INDEX_NO_SAVE            	0   // The game won't autosave after this mission.
CONST_INT   MF_INDEX_NO_FAIL            	1   // No fail screen will display when failing this mission.
CONST_INT   MF_INDEX_NO_REPLAY          	2   // No replay will be offered on failing this mission.
CONST_INT   MF_INDEX_NO_TITLE           	3   // No title will be displayed as this mission starts.
CONST_INT   MF_INDEX_NO_PASS            	4   // No mission passed audio or popup at the end of this mission.
CONST_INT   MF_INDEX_NO_REPEAT          	5   // Once the mission is completed it won't be available to repreat from the front-end.
CONST_INT   MF_INDEX_NO_COMP_PERC       	6   // This mission does not count towards 100% completion.
CONST_INT   MF_INDEX_NO_COMP_ORDER      	7   // This mission does not count in flow completion order tracking.
CONST_INT   MF_INDEX_DO_TRIGGER_TOD_M   	8   // This mission will time of day skip to meet time restrictions before any lead-in scene is created for Michael.
CONST_INT   MF_INDEX_DO_TRIGGER_TOD_F   	9   // This mission will time of day skip to meet time restrictions before any lead-in scene is created for Franklin.
CONST_INT   MF_INDEX_DO_TRIGGER_TOD_T   	10  // This mission will time of day skip to meet time restrictions before any lead-in scene is created for Trevor.
CONST_INT	MF_INDEX_BLIP_IS_HIDDEN			11	// The blip for this mission is hidden.
CONST_INT   MF_INDEX_NO_SWITCH_COUNT    	12  // This mission doesn't count to the available mission counter when bringing up the switch HUD.
CONST_INT   MF_INDEX_HAS_LEADIN         	13  // This mission creates ambient off-mission assets at its trigger location.
CONST_INT   MF_INDEX_IS_HEIST           	14  // This mission is a heist finale mission.
CONST_INT   MF_INDEX_IS_PREP            	15  // This mission is a heist prep mission.
CONST_INT   MF_INDEX_NO_SHITSKIP        	16  // This mission does not allow the player to skip past sections of gameplay if they repeatedly fail.
CONST_INT	MF_INDEX_FORCE_RESTART			17	// This mission can not be exited out of when failing. The player must replay on fail.
CONST_INT	MF_INDEX_HIDE_PHONE_ON_START	18	// This mission will force the phone off-screen as it launches.
CONST_INT	MF_INDEX_MUST_LAUNCH_PRIORITY	19	// This mission applies to the candidate controller with the must launch priority.
CONST_INT	MF_INDEX_HIGH_MEMORY_LEADIN		20	// The leadin for this mission has particularly high memory and should cause minigame launchers with ambient assets to clean up.
CONST_INT	MF_INDEX_SEAMLESS_TRIGGER		21	// This mission triggers seamlessly. Probably from a phonecall.
CONST_INT	MF_INDEX_NO_LEADIN_TIMEOUT		22	// This mission's leadin will not trigger automatically after the player is locked in for a number of seconds.
CONST_INT	MF_INDEX_DO_TOD_AUTO_START		23	// If this missions time of day trigger is hit it automatically starts the mission instead of doing a timelapse cutscene.
CONST_INT	MF_INDEX_NO_VEH_EXIT			24	// The player won't be forced out of their vehicle as they hit the mission trigger.
CONST_INT	MF_INDEX_NO_VEH_STOP			25	// The player's vehicle won't be forced to halt when they hit the mission trigger.
CONST_INT	MF_INDEX_NO_SAVE_ON_LEAD_IN		26	// The player won't be allowed to use savegame bed if the mission lead-in is active.
CONST_INT	MF_INDEX_NO_LEAVE_AREA_CHECK	27	// The mission's trigger doesn't set the leave area flag as it comes online. Allows the mission to activate near the player.
CONST_INT	MF_INDEX_FORCE_CORONA_M			28	// A corona will always display for this trigger for Michael.
CONST_INT	MF_INDEX_PRESTREAM_FULLY_OFF	29	// When loading the intro cutscene for this mission ensure all map pre-streaming is fully turned off.
CONST_INT	MF_INDEX_MID_MISSION_TOD		30	// This mission's TOD skip is done during the mission, rather than at the start.

// Non-saved static data for each story mission.
STRUCT g_structMissionStaticData
    TEXT_LABEL_23           scriptName
    INT                     scriptHash
    INT                     coreVariablesIndex
    TEXT_LABEL_7            statID
    STATIC_BLIP_NAME_ENUM   blip
    INT                     triggerCharBitset
    INT                     friendCharBitset
    INT                     startHour
    INT                     endHour
    INT                     settingsBitset
    
    INT                     statCount
    ENUM_MISSION_STATS      stats[MAX_TRACKED_MISSION_STATS]
    
    #IF IS_DEBUG_BUILD
        
    #ENDIF
ENDSTRUCT

// Non-saved active data for each story mission.
STRUCT g_structMissionActiveData
    BOOL                    leaveArea                   // TRUE if the player must leave the mission triggering area before the mission becomes triggerable.
    FLOAT                   leaveAreaDistance = 25.0    // How far does the player have to move away from this mission's trigger before it reactivates. Trigger scenes update this value.
ENDSTRUCT

// Non-saved static debug data for each story mission.
#IF IS_DEBUG_BUILD
    STRUCT g_structMissionDebugData
        JUMP_LABEL_IDS          launchLabel        		 	// The label the flow launcher should jump to to start this mission.
        JUMP_LABEL_IDS          beforeLaunchLabel  	 		// The label the flow launcher should jump to to jump the flow to a moment before the mission triggers.
        BOOL                    doLaunchWarp       	 		// Should the flow launcher perform a warp to trigger this mission?
        VECTOR                  warpPosition       	 		// Defined if we should warp to a debug position instead of the main mission blip.
    	INT						charBitsBlockDebugLaunch	// Define which characters shouldn't be selectable to debug launch the mission as. Even if they can trigger the mission.
	ENDSTRUCT
#ENDIF


// ===========================================================================================================
//      Mission Triggering
// ===========================================================================================================

// Mission Triggering Character BitFlag IDs.
CONST_INT   BIT_NOBODY      0
CONST_INT   BIT_MICHAEL     1
CONST_INT   BIT_FRANKLIN    2
CONST_INT   BIT_TREVOR      4
CONST_INT   BIT_LAMAR       8   // Used for friend bitflag only.
CONST_INT   BIT_JIMMY       16  // Used for friend bitflag only.
CONST_INT   BIT_AMANDA      32  // Used for friend bitflag only.

CONST_FLOAT     MISSION_PASSED_CENTER_X     0.1495
CONST_FLOAT     MISSION_PASSED_CENTER_Y     0.3159
CONST_FLOAT     MISSION_PASSED_SIZE_X       0.2021
CONST_FLOAT     MISSION_PASSED_SIZE_Y       0.5111


// ===========================================================================================================
//      Character Requests
// ===========================================================================================================

// Saved Character Request BitFlag IDs.
// ****
// **** IMPORTANT: Variables may get saved using these bits so once setup the order MUST STAY THE SAME ***
// ****
CONST_INT       BITS_CHARACTER_REQUEST_ACTIVE       0
CONST_INT       BITS_CHARACTER_REQUEST_COMPLETE     1


// ===========================================================================================================
//      Shitskip Control Variables
// ===========================================================================================================

//The number of fails without progress before a shitship is offered.
CONST_INT   SHITSKIP_MAX    2


// ===========================================================================================================
//      Relaunch Variables for Flow Dependent Scripts.
// ===========================================================================================================

// Specifies the size of the Restore_Launch array, struct and the enum which is publically accessible.
// When adding an entry to the enum, please ensure that a matching script name and stack size is included
// in initial.sc within the Define_Restore_Launched_ScriptNames() procedure.
 
ENUM e_g_Restore_Launched_ScriptBits

    LAUNCH_BIT_HEIST_CTRL_AGENCY,       //0
    LAUNCH_BIT_HEIST_CTRL_DOCKS,        //1
    LAUNCH_BIT_HEIST_CTRL_FINALE,       //2
    LAUNCH_BIT_HEIST_CTRL_JEWEL,        //3
	
	//Remove old Nice House Heist controller launch bit but need to pad 
	//the globals to match the original submission. This bit could be 
	//used for another script later down the line. -BenR
    LAUNCH_BIT_DUMMY_FOR_PATCH,        	//4
	
    LAUNCH_BIT_HEIST_CTRL_RURAL,        //5
    
    LAUNCH_BIT_RC_AMB_EPSILON_CARS,     //6
    LAUNCH_BIT_RC_AMB_EPSILON_DESERT,   //7
    LAUNCH_BIT_RC_AMB_EPSILON_ROBES,    //8
    LAUNCH_BIT_RC_AMB_EPSILON_TRACT,    //9
    
    LAUNCH_BIT_RC_AMB_MRS_PHILIPS,      //10
    
    LAUNCH_BIT_RC_AMB_FOR_SALE_SIGNS,   //11
    LAUNCH_BIT_RC_AMB_LETTER_SCRAPS,    //12
    LAUNCH_BIT_RC_AMB_SONAR_COLLECTION, //13
    LAUNCH_BIT_RC_AMB_SPACESHIP_PARTS,  //14
    
    LAUNCH_BIT_MG_CTRL_TOWING,          //15
    LAUNCH_BIT_MG_CTRL_TAXI,            //16
    LAUNCH_BIT_MG_CTRL_TRAF,            //17
    
    LAUNCH_BIT_EXILE_CITY_DENIAL,       //18
    
    LAUNCH_BIT_FLOW_BUILDINGSITE_AUD,   //19
    LAUNCH_BIT_FLOW_FINALE_CHOICE,      //20
    LAUNCH_BIT_FLOW_PICKUP_VEHICLES,    //21
    
    LAUNCH_BIT_BAIL_BOND_CONTROLLER,    //22
    LAUNCH_BIT_RAMPAGE_CONTROLLER,      //23

    LAUNCH_BIT_SH_INTRO_F_HILLS,        //24
    LAUNCH_BIT_SH_INTRO_M_HOME,         //25
    
    LAUNCH_BIT_FBI4_PREP3,              //26
    LAUNCH_BIT_AMB_DIVING_PICKUPS,      //27
    LAUNCH_BIT_RC_AMB_TONYA,            //28
    
    LAUNCH_BIT_AF_INTRO_T_SANDY,        //29
    LAUNCH_BIT_AMBIENT_SOLOMON,         //30
	
    LAUNCH_BIT_HEIST_FINALE_PREP_E,	    //31
    LAUNCH_BIT_HEIST_AGENCY_PREP_2,     //32
	
	//Remove Blimp DLC launch bit but need to pad the globals to match the original submission.
	//This bit could be used for another script later down the line. -BenR
	LAUNCH_BIT_DUMMY_FOR_PATCH2,     	//33 
	
    //Add any new entries above this line.
    MAXIMUM_LAUNCH_BITS                 //62
ENDENUM
 
 
STRUCT structRestore_Launched_Scripts
    TEXT_LABEL_63 Name   
    INT           Stacksize
    INT           NameHash
ENDSTRUCT


// ===========================================================================================================
//      Checklist Variables.
// ===========================================================================================================

CONST_INT MAX_CHECKLIST_LENGTH      52


// ===========================================================================================================
//      SP Initialisation Bit Flags.
// ===========================================================================================================

CONST_INT SP_INIT_INSTALL_SCREEN_FINISHED		0
CONST_INT SP_INIT_TITLE_SEQUENCE_DISPLAYED		1
CONST_INT SP_INIT_TURN_ON_LOST_BIKER_GROUP		2
CONST_INT SP_INIT_RESTORE_SLEEP_MODE			3
CONST_INT SP_INIT_LOADED_DIRECTLY_INTO_MISSION	4
CONST_INT SP_INIT_UNLOCK_SHINE_A_LIGHT			5
CONST_INT SP_INIT_SHRINK_SESSION_ATTENDED		6
CONST_INT SP_INIT_BEAST_PEYOTES_COLLECTED		7
CONST_INT SP_INIT_BEAST_HUNT_COMPLETED			8
CONST_INT SP_INIT_BEAST_FIGHT_FAILED			9
CONST_INT SP_INIT_BEAST_KILLED_AND_UNLOCKED		10
CONST_INT SP_INIT_BEAST_LAST_PEYOTE_DAY			13	//11-13 for values 0-6
CONST_INT SP_INIT_BEAST_CURRENT_CHECKPOINT		17	//14-17 for values 0-12
CONST_INT SP_INIT_BEAST_Next_CHECKPOINT			21	//18-21 for values 0-12
CONST_INT SP_INIT_BEAST_Call_Made				22	//Turned on when a Sasquach call was made, awaiting Beast reply



// ===========================================================================================================
//      Flow Project Specific Saved Variables.
// ===========================================================================================================

CONST_INT	MAX_STRAND_POINTER_OVERRIDES	3
CONST_INT	NO_MISSION_ACTIVATE_BITSETS		3

STRUCT FlowCustomSaved
	// Flow strand pointer overriding
    STRANDS 		strandToOverride[MAX_STRAND_POINTER_OVERRIDES]
    INT     		commandPointerOverride[MAX_STRAND_POINTER_OVERRIDES]
	INT				commandPointerHashID[MAX_STRAND_POINTER_OVERRIDES]
	SP_MISSIONS 	missionToUncomplete[MAX_STRAND_POINTER_OVERRIDES]
	BOOL 			applyOnMPSwitchOnly[MAX_STRAND_POINTER_OVERRIDES]
	INT				numberStoredOverrides
	
	// Mission custom saved data.
	INT				iMissionsCompleted		// How many missions the player has completed. Used by the repeat play system to order missions in the phone.	
	INT				iMissionGolds			// How many missions the player has got a gold medal on
	
	// Saving with the screen faded.
	BOOL			wasFadedOut				// Was the screen faded on last save? Used for save between prolog and armenian 1
	BOOL			wasFadedOut_switch		// Will the game fade-in on save be script controlled? Used for switching after loading a save.

	INT				spInitBitset
	INT				missionFirstActivateBitset[NO_MISSION_ACTIVATE_BITSETS]
	
	INT				iFirstPersonCoverHelpCountMission	// How many times have we shown first person cover help while on mission.
	INT				iFirstPersonCoverHelpCountFlow		// How many times have we shown first person cover help while off mission.
	INT				iVehDuckHelpCount					// How many times have we shown vehicle duck help.
ENDSTRUCT
