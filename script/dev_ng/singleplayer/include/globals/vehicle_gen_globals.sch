//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	vehicle_gen_globals.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used for the vehicle generation.					//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "website_globals.sch"
USING "commands_hud.sch"
USING "shared_global_definitions.sch"
USING "SceneTool_globals.sch"

CONST_INT NUMBER_OF_DYNAMIC_VEHICLE_GENS	23

// List of all the vehicle gens handled by script
CONST_INT NUMBER_OF_VEHICLES_TO_GEN 		68	// - CC
ENUM VEHICLE_GEN_NAME_ENUM
	VEHGEN_MICHAEL_SAVEHOUSE = 0,
	VEHGEN_MICHAEL_SAVEHOUSE_COUNTRY,
	VEHGEN_FRANKLIN_SAVEHOUSE_CAR,
	VEHGEN_FRANKLIN_SAVEHOUSE_BIKE,
	VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_CAR,
	VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_BIKE,
	VEHGEN_TREVOR_SAVEHOUSE_COUNTRY,
	VEHGEN_TREVOR_SAVEHOUSE_CITY,
	VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB,
	VEHGEN_MOUNTAIN_BIKE_CH,
	VEHGEN_SEASHARK_SM,
	VEHGEN_DUSTER,
	//VEHGEN_IMPOUND_TOW_TRUCK,
	
	// Vehicle gens with dynamic data
	VEHGEN_WEB_HANGAR_MICHAEL,
	VEHGEN_WEB_HANGAR_FRANKLIN,
	VEHGEN_WEB_HANGAR_TREVOR,
	VEHGEN_WEB_MARINA_MICHAEL,
	VEHGEN_WEB_MARINA_FRANKLIN,
	VEHGEN_WEB_MARINA_TREVOR,
	VEHGEN_WEB_HELIPAD_MICHAEL,
	VEHGEN_WEB_HELIPAD_FRANKLIN,
	VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY,
	VEHGEN_WEB_CAR_MICHAEL,
	VEHGEN_WEB_CAR_FRANKLIN,
	VEHGEN_WEB_CAR_TREVOR,
	VEHGEN_MISSION_VEH,
	VEHGEN_MISSION_VEH_FBI4_PREP,
	
	VEHGEN_MICHAEL_GARAGE_1, 	// Keep order
	VEHGEN_FRANKLIN_GARAGE_1,	//
	VEHGEN_TREVOR_GARAGE_1,		//
	VEHGEN_MICHAEL_GARAGE_2,	//
	VEHGEN_FRANKLIN_GARAGE_2,	//
	VEHGEN_TREVOR_GARAGE_2,		//
	VEHGEN_MICHAEL_GARAGE_3,	//
	VEHGEN_FRANKLIN_GARAGE_3,	//
	VEHGEN_TREVOR_GARAGE_3,		//
	
	
	// One-off mission specific vehicle gens.
	VEHGEN_TREV1_SMASHED_TRAILER,
	VEHGEN_BJXL_CRASH_POST_ARM3,
	VEHGEN_EPSILON6_PLANE,
	VEHGEN_DOCKSP2B_CHINOOK,
	VEHGEN_DOCKSP2B_SUB,
	VEHGEN_FBI4_TRASH,
	VEHGEN_FBI4_TOWING,
	VEHGEN_RURALH_MILITARY_TRUCK,
	VEHGEN_AGENCY_PREP_FIRETRUCK,
	
	// RCM: Epsilon 3 - One-off vehicles gets used for ambient car script - keep these in this order
	VEHGEN_EPS3_VACCA,
	VEHGEN_EPS3_SURANO,
	VEHGEN_EPS3_TORNADO2,
	VEHGEN_EPS3_SUPERD,
	VEHGEN_EPS3_DOUBLE,
	VEHGEN_EPS3_DOUBLE_2,
	VEHGEN_EPS3_DOUBLE_3,

	// RCM: Mrs Philips 2 - Trevor must drive one back to his mom
	VEHGEN_MRSP_PHARMACY1,
	VEHGEN_MRSP_PHARMACY2,
	VEHGEN_MRSP_HOSPITAL1,
	VEHGEN_MRSP_HOSPITAL2,
	VEHGEN_MRSP_HOSPITAL3,
	VEHGEN_MRSP_HOSPITAL4,

	// Property gens
	VEHGEN_PROPERTY_MARINA_SUB,
	VEHGEN_PROPERTY_MARINA_ZODIAC,
	
	// B*1267136
	VEHGEN_AIRPORT_VEHICLE,			
	
	// Preorder Blimp
	VEHGEN_BLIMP_CASINO,			
	VEHGEN_BLIMP_DOCKS,
	
	// Completion reward - Save Trevor
	VEHGEN_TREVOR_BLAZER3,
	
	// Completion rewards - CG to NG content
	VEHGEN_WILDPHOTO_SUB,			// Wildlife Photography sub spawn.
	VEHGEN_RE_DUEL_CITY,			// RE Duel car city spawn.
	VEHGEN_RE_DUEL_COUNTRY,			// RE Duel car country spawn.
	VEHGEN_RE_SEAPLANE_CITY,		// RE Seaplane city spawn.
	VEHGEN_RE_SEAPLANE_COUNTRY,		// RE Seaplane country spawn.
	
	// Completion
	
	// ^^^ Increment the const int above when adding a new name
	VEHGEN_NONE = -1
ENDENUM

// Flags used to set the bits in the saved data properties
ENUM VEHICLE_GEN_SAVED_FLAG_ENUM
	VEHGEN_S_FLAG_AVAILABLE = 0,
	VEHGEN_S_FLAG_HELP_PROCESSED,
	VEHGEN_S_FLAG_BLIP_PROCESSED,
	VEHGEN_S_FLAG_TXT_PROCESSED,
	VEHGEN_S_FLAG_PLAYER_USED_VEH,
	VEHGEN_S_FLAG_ACQUIRED
ENDENUM

// Flags used to set the bits in the non-saved data properties
ENUM VEHICLE_GEN_NON_SAVED_FLAG_ENUM
	VEHGEN_NS_FLAG_BLIP = 0,
	VEHGEN_NS_FLAG_BLIP_LONG_RANGE,
	VEHGEN_NS_FLAG_BLIP_ONCE,
	VEHGEN_NS_FLAG_PRINT_HELP_ONCE,
	VEHGEN_NS_FLAG_SEND_TXT_ONCE,
	VEHGEN_NS_FLAG_CAN_SAVE_IN_GARAGE,
	VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK,
	VEHGEN_NS_FLAG_DO_PED_CHECK,
	VEHGEN_NS_FLAG_NON_INTERACTABLE,
	VEHGEN_NS_FLAG_FORCE_COLOURS,
	VEHGEN_NS_FLAG_DYNAMIC_DATA,							// Allow for the vehicle setup struct to be set by another script
	VEHGEN_NS_FLAG_DYNAMIC_COORDS,							// Allow for the dynamic coords to be used
	VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS,			// Ignore nearby vehicle model checks when creating vehicle that was handed over by another script
	VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE,						// Once player interacts with the vehicle the vehicle gen will be marked as unavailable
	VEHGEN_NS_FLAG_PURCHASABLE,								// Player must purchase (acquire) the vehicle gen before it activates
	VEHGEN_NS_FLAG_LINKED_TO_PROPERTY,						// Player must purchase the property before this is unlocked
	VEHGEN_NS_FLAG_PROPERTY_CAR,
	VEHGEN_NS_FLAG_PROPERTY_PLANE,
	VEHGEN_NS_FLAG_PROPERTY_MODSHOP,
	VEHGEN_NS_FLAG_PLAYER_VEH_BIKE,
	VEHGEN_NS_FLAG_PLAYER_VEH_CAR,
	VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION,
	VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED,
	VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS,				// Use this to override the cleanup/create distances with longer range variants
	VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS,
	VEHGEN_NS_FLAG_USE_WORLD_RANGE_DIST_CHECKS,				// Use this to force all veh gens to be created (for warping into the players garage)
	VEHGEN_NS_FLAG_LOCK_DOORS, 								// B*1513943 - Trevor should have to break into the Mrs Philips 2 vehicle gens
	VEHGEN_NS_FLAG_BLOCK_CLEANUP_ON_ENTRY,
	VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED,
	VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE,
	VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION,
	VEHGEN_NS_FLAG_ANCHORED
ENDENUM

// Struct to use when grabbing info about a vehicle gen
STRUCT VEHICLE_GEN_DATA_STRUCT
	VECTOR coords
	FLOAT heading
	MODEL_NAMES model
	TEXT_LABEL_15 help
	INT flags
	INT colour1
	INT colour2
	enumCharacterList ped
	
	BLIP_SPRITE blip
	
	INT dynamicSlotIndex
	
	VECTOR scenario_block_minXYZ
	VECTOR scenario_block_maxXYZ

	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 dbg_name
	#ENDIF
ENDSTRUCT

// Struct to use when grabbing info about a purchasable garage
STRUCT PURCHASABLE_GARAGE_DATA_STRUCT
	
	VECTOR vBuyPos1
	VECTOR vBuyPos2
	FLOAT fBuyWidth
	
	VECTOR vMenuPos1
	VECTOR vMenuPos2
	FLOAT fMenuWidth
	
	structSceneTool_Pan mPans1
	structSceneTool_Pan mPans2
	
	VECTOR vPlayerCoords
	FLOAT fPlayerHeading
	
	VECTOR vStorePos1
	VECTOR vStorePos2
	FLOAT fStoreWidth
	
	
	VECTOR vBlipCoords
	BLIP_SPRITE eBlipSprite
	TEXT_LABEL_15 tl15BlipLabel
	
	TEXT_LABEL_15 tl15HelpText
	
	INT iCost
	
	FLOAT fDoorHeading
	
	BOOL bWarpToGarage
	
	VECTOR vGarageExitCoords[2]
	FLOAT fGarageExitHeading[2]
	
	BOOL bDataSet
ENDSTRUCT

// Struct to contain non saved data
STRUCT VehicleGenDataNonSaved
	BOOL bLeaveAreaBeforeCreating[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bCheckPlayerVehicleCleanupTimer[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bDisabledForMissions // Stops the vehicle gen controller from working when on mission
	VEHICLE_INDEX vehicleID[NUMBER_OF_VEHICLES_TO_GEN]
	
	BLIP_INDEX blipID[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bGarageActive[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bScenarioBlockSet[NUMBER_OF_VEHICLES_TO_GEN]
	SCENARIO_BLOCKING_INDEX scenarioBlock[NUMBER_OF_VEHICLES_TO_GEN]
	
	VEHICLE_INDEX vehicleSelectID[NUMBER_OF_VEHICLES_TO_GEN]
	
	BOOL bCheckVehGensLoaded
	INT iCheckVehGensLoadedCounter
	
	VEHICLE_GEN_DATA_STRUCT sRuntimeStruct[1]
	
	BOOL bInGarage
	VECTOR vGarageEntryCoords
	
	VEHICLE_INDEX vehPlayerVehicle
	enumCharacterList ePlayerVehiclePed
	BLIP_INDEX blipPlayerVehicle
	VECTOR vPlayerVehicleCoords
	VECTOR vPlayerVehicleCoordsCached
	BOOL bPlayerVehicleBlipUsingCoords
	BOOL bPlayerVehicleBlipUsingCoordsCached
	
	SITE_BUYABLE_VEHICLE eWebVehicles[NUMBER_OF_BUYABLE_VEHICLES_SP]
ENDSTRUCT
VehicleGenDataNonSaved g_sVehicleGenNSData

STRUCT PURCHASABLE_VEHICLE_DATA_STRUCT
	INT iSiteID[NUMBER_OF_BUYABLE_VEHICLES_SP]
	VEHICLE_GEN_NAME_ENUM eVehGen[NUMBER_OF_BUYABLE_VEHICLES_SP]
	TIMEOFDAY todEmailDate[NUMBER_OF_BUYABLE_VEHICLES_SP]
ENDSTRUCT

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT VehicleGenDataSaved
	INT iProperties[NUMBER_OF_VEHICLES_TO_GEN] // Set bits: Available[0], HelpProcessed[1], BlipProcessed[2], TxtProcessed[3] etc.
	
	// Dynamic vehicle gen data
	VEHICLE_SETUP_STRUCT sDynamicData[NUMBER_OF_DYNAMIC_VEHICLE_GENS]
	VECTOR vDynamicCoords[NUMBER_OF_DYNAMIC_VEHICLE_GENS]
	FLOAT fDynamicHeading[NUMBER_OF_DYNAMIC_VEHICLE_GENS]
	INT iPlayerVehicle[NUMBER_OF_DYNAMIC_VEHICLE_GENS]
		
	PURCHASABLE_VEHICLE_DATA_STRUCT sWebVehicles[3]
	
	BOOL bGarageIntroRun
	
	TIMEOFDAY eMissionVehTimeStamp
	
	BOOL bInitialDataSetup
	
	// Vehicle setup for Finale Heist Prep vehicles
	VEHICLE_SETUP_STRUCT sHeistPrepVehicles[3]
	
	// Vehicle setup for impound vehicles
	VEHICLE_SETUP_STRUCT sImpoundVehicles[3][2]
	
	VEHICLE_SETUP_STRUCT sCarForImpound
	BOOL bTrackingImpoundVehicle
	BOOL bImpoundVehicleClearedBySwitch
	enumCharacterList eCurrentTrackedChar
	enumCharacterList eLastCharToUseMissionVehGen
	
	INT iImpoundVehicleSlot[3]
	BOOL bImpoundVehicleHelp[3]
	
	VEHICLE_SETUP_STRUCT sImpoundSwitchVehicles[3]
ENDSTRUCT

VEHICLE_INDEX g_vCarToTrackForImpound
VEHICLE_INDEX g_vehHandoverToGen
VEHICLE_GEN_NAME_ENUM g_eVehGenToRecieveVehicle = VEHGEN_NONE
BOOL g_bVehHandoverUseVehicleCoords
VEHICLE_SETUP_STRUCT sTempImpoundVehData
enumCharacterList eTempImpoundChar

INT g_iLastMissionVehgenOwnerHash = 0

BOOL g_bForceCleaupVehgenForRecordingPlayback = FALSE
