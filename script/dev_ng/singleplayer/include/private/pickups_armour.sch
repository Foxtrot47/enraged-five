// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "script_debug.sch"

CONST_INT NUMBER_OF_ARMOUR_PICKUPS(45)

#IF IS_DEBUG_BUILD

	/// PURPOSE: Returns location name of armour pickup
	FUNC STRING GET_ARMOUR_LOCATION_NAME(INT iIndex)

		SWITCH iIndex
			CASE 0  RETURN "01: Tower scaffold at docks" 						BREAK
			CASE 1  RETURN "02: Behind Bin at housing blocks" 					BREAK
			CASE 2  RETURN "03: St. Fiacre teaching hospital" 					BREAK
			CASE 3  RETURN "04: Junkyard" 										BREAK
			CASE 4  RETURN "05: Maze Bank" 										BREAK
			CASE 5  RETURN "06: Museum" 										BREAK
			CASE 6  RETURN "07: Office block parking garage" 					BREAK
			CASE 7  RETURN "08: The Pink Cage motel swimming pool" 				BREAK
			CASE 8  RETURN "09: Construction site near freeway" 				BREAK
			CASE 9  RETURN "10: End of pier walkway" 							BREAK

			CASE 10 RETURN "11: Conveyor belt at Airport" 						BREAK
			CASE 11 RETURN "12: House on the beach" 							BREAK
			CASE 12 RETURN "13: Recess under rockface" 							BREAK
			CASE 13 RETURN "14: Sewage works near air base" 					BREAK
			CASE 14 RETURN "15: Entrance to tunnel through hillside" 			BREAK
			CASE 15 RETURN "16: Barn near cow field " 							BREAK
			CASE 16 RETURN "17: Hippy Camp" 									BREAK
			CASE 17 RETURN "18: Dam" 											BREAK
			CASE 18 RETURN "19: Train station beside refinery" 					BREAK
			CASE 19 RETURN "20: Storage tank in the hills" 						BREAK
			CASE 20 RETURN "21: Building top" 									BREAK
			
			CASE 21 RETURN "22: Gas station" 									BREAK
			CASE 22 RETURN "23: Vineyard restaurant" 							BREAK
			CASE 23 RETURN "24: Below Radio Tower" 								BREAK
			CASE 24 RETURN "25: Fishery" 										BREAK
			CASE 25 RETURN "26: Police Stations. A - Paleto Bay (dumpsters)"	BREAK
			CASE 26 RETURN "27: Police Stations. B - Sandy Shores" 				BREAK
			CASE 27 RETURN "28: Police Stations. C - Vinewood" 					BREAK
			CASE 28 RETURN "29: Police Stations. D - Downtown"					BREAK
			CASE 29 RETURN "30: Police Stations. E - Vespucci PD" 				BREAK

			CASE 30 RETURN "31: Police Stations. F - Davis Sheriff’s station" 	BREAK
			CASE 31 RETURN "32: Police Stations. G - LS Airport" 				BREAK
			CASE 32 RETURN "33: Vinewood Hills - Portaloos" 					BREAK
			CASE 33 RETURN "34: Vinewood Hills - Construction scaffolding" 		BREAK
			CASE 34 RETURN "35: Vinewood Hills - Generator" 					BREAK
			CASE 35 RETURN "36: Vinewood Hills - Forest River" 					BREAK
			CASE 36 RETURN "38: Back of meth lab" 								BREAK
			CASE 37 RETURN "39: Grove street porch" 							BREAK
			CASE 38 RETURN "41: Trailer storage area" 							BREAK

			CASE 39 RETURN "42: Office desk" 									BREAK
			CASE 40 RETURN "43: Hobo underpass" 								BREAK
			CASE 41 RETURN "44: In hole near shootout"							BREAK
			CASE 42 RETURN "45: Construction site next to cabin"				BREAK
			CASE 43 RETURN "46: Galilee boat house"								BREAK
			CASE 44 RETURN "47: Altruist Camp Church"							BREAK
		ENDSWITCH
		
		
		RETURN "Invalid Location Index" 
	ENDFUNC
#ENDIF

/// PURPOSE: Returns coords of armour pickup
FUNC VECTOR GET_ARMOUR_PICKUP_COORDS(INT iIndex, BOOL bForChop=FALSE)

	// If any of these have a bForChop vector set, Chop will lead the player to the bForChop vector then orientate himself towards the actual pickup vector.
	// Small alterations are fine but if you move a pickup more than 1-2 metres away from its current position you must update the bForChop vector too or else we'll have Chop leading the player to nothing.
	// The bForChop vector must be on a section of navmesh that is connected to the main navmesh. See Kev E if unsure.
	SWITCH iIndex
	
		CASE 0  RETURN << 473.1829, -2643.1838, 4.7209 >> 	BREAK
		CASE 1
			IF bForChop = TRUE
				RETURN << 291.41, -2090.78, 16.29 >>
			ELSE
				RETURN <<289.2062, -2091.2209, 16.0234>>
			ENDIF
		BREAK
		CASE 2
			IF bForChop = TRUE
				RETURN << 1173.83923, -1499.78076, 33.84875 >>
			ELSE
				RETURN << 1173.5656, -1495.7628, 34.1121 >>
			ENDIF
		BREAK
		CASE 3
			IF bForChop = TRUE
				RETURN << -461.71393, -1699.20190, 17.85122 >>
			ELSE
				RETURN << -462, -1701, 18.8 >>
			ENDIF
		BREAK
		CASE 4  RETURN << -1384.6276, -521.0416, 30.0840 >> BREAK
		CASE 5  RETURN << -521.5692, -601.7532, 29.4482 >>  BREAK
		CASE 6  RETURN << -664.5000, 165.4552, 58.3821 >> 	BREAK
		CASE 7  RETURN << 329.3609, -190.5986, 53.2262 >> 	BREAK
		CASE 8
			IF bForChop = TRUE
				RETURN << -477.44, -1052.99, 39.81 >>
			ELSE
				RETURN << -478.7744, -1051.0792, 40.93 >>
			ENDIF
		BREAK
		CASE 9  RETURN << -1268.0712, -1917.4076, 4.8583 >> BREAK
		CASE 10 RETURN << -1221.9080, -2771.7622, 13.0545 >> BREAK
		CASE 11 RETURN << -3088.6682, 652.9629, 0.989 >> 	BREAK
		CASE 12
			IF bForChop = TRUE
				RETURN <<-629.3048, 2036.0667, 157.3307>>
			ELSE
				RETURN << -627.2166, 2035.0320, 157.5585 >>
			ENDIF
		BREAK
		CASE 13
			IF bForChop = TRUE
				RETURN << -2071.90259, 3411.50415, 30.36414 >>
			ELSE
				RETURN << -2048.4832, 3406.1624, 31.1504 >>
			ENDIF
		BREAK
		CASE 14 RETURN << -424.0321, 4012.7671, 80.4744 >> 	BREAK
		CASE 15 RETURN << 1983.1, 5174.5, 47.6 >> 			BREAK
		CASE 16
			IF bForChop = TRUE
				RETURN << 2341.1, 2576.0, 45.7 >>
			ELSE
				RETURN << 2340.17, 2573.89, 45.77 >>
			ENDIF
		BREAK
		CASE 17
			IF bForChop = TRUE
				RETURN << 1663.90039, -25.91945, 172.77475 >>
			ELSE
				RETURN << 1663.9600, -24.1101, 172.7747 >>
			ENDIF
		BREAK
		CASE 18
			IF bForChop = TRUE
				RETURN << 2614.45068, 1695.12708, 26.59907 >>
			ELSE
				RETURN << 2617.7290, 1693.4167, 30.8743 >>
			ENDIF
		BREAK
		CASE 19
			IF bForChop = TRUE
				RETURN << -187.31, 1545.16, 314.07 >>
			ELSE
				RETURN << -187.0338, 1555.6766, 321.2431 >>
			ENDIF
		BREAK
		CASE 20
			IF bForChop = TRUE
				RETURN << 859.50250, 2426.03955, 53.18517 >>
			ELSE
				RETURN << 868.7573, 2423.5361, 60.8942 >>
			ENDIF
		BREAK
		CASE 21
			IF bForChop = TRUE
				RETURN << -1822.07507, 805.78137, 137.72650 >>
			ELSE
				RETURN << -1822.9667, 803.8147, 138.2005 >>
			ENDIF
		BREAK
		CASE 22
			IF bForChop = TRUE
				RETURN << -1852.92212, 2054.41968, 139.98405 >>
			ELSE
				RETURN << -1851.6283, 2053.8359, 139.9841 >>
			ENDIF
		BREAK
		CASE 23
			IF bForChop = TRUE
				RETURN << -1075.51489, 4898.49658, 213.27675 >>
			ELSE
				RETURN << -1070.6506, 4898.9336, 213.2752 >>
			ENDIF
		BREAK
		CASE 24
			IF bForChop = TRUE
				RETURN << 1301.56, 4306.60, 36.39 >>
			ELSE
				RETURN << 1300.1, 4308.1, 37.3 >>
			ENDIF
		BREAK
		CASE 25 //police stations start here
			IF bForChop = TRUE
				RETURN << -436.44, 5983.50, 30.49 >>
			ELSE
				RETURN << -437.94, 5985.04, 31.29 >>
			ENDIF
		BREAK
		CASE 26
			IF bForChop = TRUE
				RETURN << 1840.5687, 3693.7703, 33.2668 >>
			ELSE
				RETURN << 1838.5770, 3692.0281, 33.6172 >>
			ENDIF
		BREAK
		CASE 27 RETURN << 533.1, -23.8, 70.6 >> 			BREAK
		CASE 28
			IF bForChop = TRUE
				RETURN << 445.83, -986.48, 29.69 >>
			ELSE
				RETURN << 446.4, -988.5, 29.770 >>
			ENDIF
		BREAK
		CASE 29 RETURN << -1119.4977, -847.3371, 18.6886 >>	BREAK
		
		CASE 30 RETURN << 391.2, -1603.9, 28.380 >>			BREAK
		CASE 31 RETURN << -890.8, -2386.2, 13.4 >> 			BREAK
		CASE 32
			IF bForChop = TRUE
				RETURN <<282.75, 953.05, 209.95>>
			ELSE
				RETURN <<284.7, 954.2, 210.1514>>
			ENDIF
		BREAK
		CASE 33 RETURN <<-2003.1145, 544.7549, 114.5077>> 	BREAK
		CASE 34
			IF bForChop = TRUE
				RETURN <<-397.57858, 1127.35315, 321.72913>>
			ELSE
				RETURN <<-386.8739, 1142.4197, 321.5892>>
			ENDIF
		BREAK
		CASE 35 RETURN <<-1521.8113, 1560.2010, 106.9392>> 	BREAK
		
		CASE 36 RETURN <<1391.9453, 3616.6548, 37.9259>> 	BREAK //<<1391.3459, 3619.9265, 39.0259>> is on table but it breaks
		CASE 37
			IF bForChop = TRUE
				RETURN <<45.37, -1911.45, 20.79>>
			ELSE
				RETURN <<40.5177, -1910.7271, 20.9706>>
			ENDIF
		BREAK
		
		CASE 38
			IF bForChop = TRUE
				RETURN <<68.14, 3749.36, 38.73>>
			ELSE
				RETURN <<70.3786, 3755.6055, 38.7487>>
			ENDIF
		BREAK
		//CASE 39 RETURN <<117.6710, -754.2290, 258.1322>> 	BREAK
		CASE 39 RETURN <<119.45, -755.29, 258.15>> 	BREAK
		CASE 40
			IF bForChop = TRUE
				RETURN <<156.35, -1192.10, 28.37>>
			ELSE
				RETURN <<160.4134, -1191.7, 28.87>>
			ENDIF
		BREAK
		CASE 41 RETURN <<23.9296, -633.9206, 6.5084>> 	BREAK
		CASE 42
			IF bForChop = TRUE
				RETURN <<31.67, 6542.78, 30.44>>
			ELSE
				RETURN <<30.9162, 6546.7910, 30.4058>>
			ENDIF
		BREAK
		CASE 43
			IF bForChop
				RETURN << 1333.0566, 4381.2778, 43.3703 >>
			ELSE		
				RETURN << 1331.3549, 4392.8872, 44.0120 >>
			ENDIF
		BREAK
		CASE 44
			IF bForChop = TRUE
				RETURN << -1147.31, 4939.27, 221.27 >>
			ELSE
				RETURN << -1149.3918, 4938.7710, 221.3682 >>
			ENDIF
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid index passed to GET_ARMOUR_PICKUP_COORDS")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Gets the interior name for the pickup
FUNC STRING GET_ARMOUR_PICKUP_ROOM_NAME(INT iIndex)
	SWITCH iIndex
		CASE 28 RETURN "PH_LOBBY_ROOM"		BREAK
		CASE 39 RETURN "V_FIB03_atr_off2"	BREAK
		CASE 41 RETURN "GtaMloRoomTun2"		BREAK
		DEFAULT 
			RETURN ""
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns TRUE if Chop can reach the armour pickup
FUNC BOOL CAN_CHOP_REACH_ARMOUR(INT index)
	SWITCH (index)
		CASE 33 FALLTHRU // Navigation nightmare
		CASE 39 FALLTHRU // In office highrise
		CASE 41 FALLTHRU // Bottom of construction site
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
