
#IF IS_DEBUG_BUILD
	USING "player_ped_debug.sch"
#ENDIF

///private header for the data required in PR_SCENE_X_TRAFFIC
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	SCENE PRIVATE COORD FUNCTIONS
// *******************************************************************************************



FUNC VECTOR get_random_vector(FLOAT MinFloat = -0.5, FLOAT MaxFloat = 0.5)
	RETURN <<GET_RANDOM_FLOAT_IN_RANGE(MinFloat,MaxFloat), GET_RANDOM_FLOAT_IN_RANGE(MinFloat,MaxFloat), 0>>
ENDFUNC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Default_Traffic_Scene_Coordinates(
		VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[])
	
	
	
	VECTOR vAddOffset = <<5,7,0>>*0.7
	
	vTraffic_veh_offset[0] = <<-9.2247, -22.7769, 0.0>>+get_random_vector()
	vTraffic_veh_offset[1] = <<-0.8890, -21.4697, 0.0>>+get_random_vector()
	
	CONST_INT iSTARTCOORD 2
	INT iVeh
	FOR iVeh = iSTARTCOORD TO (COUNT_OF(vTraffic_veh_offset) - 1)
		vTraffic_veh_offset[iVeh] = vTraffic_veh_offset[iVeh-2]
		vTraffic_veh_offset[iVeh] +=vAddOffset*(1.0+(TO_FLOAT(iVeh)/TO_FLOAT(COUNT_OF(vTraffic_veh_offset)+1-iSTARTCOORD)))
		vTraffic_veh_offset[iVeh] +=get_random_vector()
		fTraffic_veh_offset[iVeh] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168
		
	ENDFOR
	
	vTruck_veh_offset[0] = <<-19.4623, -36.7339, 0.0>>
	vTruck_veh_offset[1] = <<-11.7179, -35.5764, 0.0>>
	vTruck_veh_offset[2] = vTruck_veh_offset[0]+vAddOffset
	vTruck_veh_offset[3] = vTruck_veh_offset[1]+vAddOffset
	
	vAmbient_veh_offset[0] = <<2.1, 8.1, 0.0>>+get_random_vector()
	vAmbient_veh_offset[1] = <<4.4, 15.3, 0.0>>+get_random_vector()
	vAmbient_veh_offset[2] = <<-23.7000, -15.3000, -11.4000>>+get_random_vector()
	vAmbient_veh_offset[3] = <<-13.4000, -10.9000, -12.7000>>+get_random_vector()
	
	
	fTraffic_veh_offset[0] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168
	fTraffic_veh_offset[1] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168
	
	fTraffic_veh_offset[2] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168							
	fTraffic_veh_offset[3] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168
	
	fTraffic_veh_offset[4] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168							
	fTraffic_veh_offset[5] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168
	
	fTraffic_veh_offset[6] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)			//(146, 153) - 168
	
	fTruck_veh_offset[0] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)				//(146, 153) - 168							
	fTruck_veh_offset[1] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)				//(146, 153) - 168							
	fTruck_veh_offset[2] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)				//(146, 153) - 168							
	fTruck_veh_offset[3] = GET_RANDOM_FLOAT_IN_RANGE(-22, -15)				//(146, 153) - 168
	
	fAmbient_veh_offset[0] = GET_RANDOM_FLOAT_IN_RANGE(-6, 4)				//(162, 172) - 168							
	fAmbient_veh_offset[1] = GET_RANDOM_FLOAT_IN_RANGE(-16, -6)				//(152, 162) - 168							
	fAmbient_veh_offset[2] = GET_RANDOM_FLOAT_IN_RANGE(-38, -28)			//(130, 140) - 168							
	fAmbient_veh_offset[3] = GET_RANDOM_FLOAT_IN_RANGE(-28, -18)			//(140, 150) - 168
	
ENDPROC
PROC Initialise_Default_Traffic_Scene_Models(MODEL_NAMES &traffic_ped_model[],
		MODEL_NAMES &traffic_veh_model[],
		MODEL_NAMES &truck_veh_model[],
		MODEL_NAMES &ambient_veh_model[],
		MODEL_NAMES &nextgen_veh_model[])
	traffic_ped_model[0] = a_m_m_Salton_02			//1600kb
	traffic_ped_model[1] = traffic_ped_model[0]
	traffic_ped_model[2] = A_M_M_PROLHOST_01		//1192kb
	traffic_ped_model[3] = a_m_m_AfriAmer_01		//2160kb
	
	traffic_veh_model[0] = camper
	traffic_veh_model[1] = dilettante	// buffalo Changed to as it's Franklins car so why have too many?
	traffic_veh_model[2] = futo			// issi Changed to fix build error
	traffic_veh_model[3] = blista		// bodhi Changed to fix build error
	traffic_veh_model[4] = regina		// mesa Changed to fix build error
	traffic_veh_model[5] = sultan		// tailgater Changed to as it's Michael's car so why have too many?
	traffic_veh_model[6] = asterope
	traffic_veh_model[7] = sadler
	traffic_veh_model[8] = surfer
	traffic_veh_model[9] = dubsta
	traffic_veh_model[10] = picador
	traffic_veh_model[11] = phoenix
	
	truck_veh_model[0] = PACKER
	truck_veh_model[1] = HAULER
	truck_veh_model[2] = TANKER
	truck_veh_model[3] = TRAILERLOGS
	
	ambient_veh_model[0] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[1] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[2] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[3] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	
	INT iNG
	REPEAT COUNT_OF(nextgen_veh_model) iNG
		nextgen_veh_model[iNG] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ENDREPEAT
ENDPROC

PROC Initialise_M_Traffic_C_Scene_Models(MODEL_NAMES &traffic_ped_model[],
		MODEL_NAMES &traffic_veh_model[],
		MODEL_NAMES &truck_veh_model[],	
		MODEL_NAMES &ambient_veh_model[],
		MODEL_NAMES &nextgen_veh_model[])
	traffic_ped_model[0] = a_m_m_Salton_02			//1600kb
	traffic_ped_model[1] = traffic_ped_model[0]
	traffic_ped_model[2] = A_M_M_PROLHOST_01		//1192kb
	traffic_ped_model[3] = a_m_m_AfriAmer_01		//2160kb
	
	traffic_veh_model[0] = camper
	traffic_veh_model[1] = dilettante	// buffalo Changed to as it's Franklins car so why have too many?
	traffic_veh_model[2] = futo			// issi Changed to fix build error
	traffic_veh_model[3] = blista		// bodhi Changed to fix build error
	traffic_veh_model[4] = regina		// mesa Changed to fix build error
	traffic_veh_model[5] = sultan		// tailgater Changed to as it's Michael's car so why have too many?
	traffic_veh_model[6] = asterope
	traffic_veh_model[7] = sadler
	traffic_veh_model[8] = surfer
	traffic_veh_model[9] = dubsta
	traffic_veh_model[10] = picador
	traffic_veh_model[11] = phoenix
	
	truck_veh_model[0] = BUS
	truck_veh_model[1] = BOXVILLE
	truck_veh_model[2] = BUS
	truck_veh_model[3] = BOXVILLE2
	
	ambient_veh_model[0] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[1] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[2] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[3] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	
	INT iNG
	REPEAT COUNT_OF(nextgen_veh_model) iNG
		nextgen_veh_model[iNG] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ENDREPEAT
ENDPROC

PROC Initialise_F_Traffic_B_Scene_Models(MODEL_NAMES &traffic_ped_model[],
		MODEL_NAMES &traffic_veh_model[],
		MODEL_NAMES &truck_veh_model[],	
		MODEL_NAMES &ambient_veh_model[],
		MODEL_NAMES &nextgen_veh_model[])
	traffic_ped_model[0] = A_M_M_SALTON_01			//2016kb
	traffic_ped_model[1] = A_M_M_PROLHOST_01		//1192kb
	traffic_ped_model[2] = a_m_m_AfriAmer_01		//2160kb
	traffic_ped_model[3] = traffic_ped_model[2]
	
	traffic_veh_model[0] = CAMPER
	traffic_veh_model[2] = FUTO
	traffic_veh_model[3] = BLISTA
	traffic_veh_model[4] = REGINA
	traffic_veh_model[7] = SADLER
	traffic_veh_model[9] = MANANA
	
	MODEL_NAMES traffic_veh_modeltempA, traffic_veh_modeltempB
	SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
		CASE 0
			traffic_veh_modeltempA = dilettante
		BREAK
		CASE 1
			traffic_veh_modeltempA = SURFER
		BREAK
		CASE 2
			traffic_veh_modeltempA = sultan
		BREAK
	ENDSWITCH
	SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
		CASE 0
			traffic_veh_modeltempB = asterope
		BREAK
		CASE 1
			traffic_veh_modeltempB = Phoenix
		BREAK
		CASE 2
			traffic_veh_modeltempB = picador
		BREAK
	ENDSWITCH
	
	traffic_veh_model[1] = traffic_veh_modeltempA
	traffic_veh_model[8] = traffic_veh_modeltempA
	traffic_veh_model[5] = traffic_veh_modeltempA
	
	traffic_veh_model[6] = traffic_veh_modeltempB
	traffic_veh_model[11] = traffic_veh_modeltempB
	traffic_veh_model[10] = traffic_veh_modeltempB

	
	truck_veh_model[0] = BENSON
	truck_veh_model[1] = BUS
	truck_veh_model[2] = COACH
	truck_veh_model[3] = MULE
	
	ambient_veh_model[0] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[1] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[2] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[3] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	
	INT iNG
	REPEAT COUNT_OF(nextgen_veh_model) iNG
		nextgen_veh_model[iNG] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ENDREPEAT
ENDPROC

PROC Initialise_F_Traffic_C_Scene_Models(MODEL_NAMES &traffic_ped_model[],
		MODEL_NAMES &traffic_veh_model[],
		MODEL_NAMES &truck_veh_model[],	
		MODEL_NAMES &ambient_veh_model[],
		MODEL_NAMES &nextgen_veh_model[])
	traffic_ped_model[0] = A_M_M_SALTON_01			//2016kb
	traffic_ped_model[1] = A_M_M_PROLHOST_01		//1192kb		//1192kb
	traffic_ped_model[2] = traffic_ped_model[1]
	traffic_ped_model[3] = traffic_ped_model[2]
	
	traffic_veh_model[0] = Taxi
	traffic_veh_model[2] = issi2		
	traffic_veh_model[3] = washington	
	traffic_veh_model[7] = peyote
	traffic_veh_model[8] = surge
	traffic_veh_model[11] = ADDER
	
	MODEL_NAMES traffic_veh_modeltempA, traffic_veh_modeltempB
	SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
		CASE 0
			traffic_veh_modeltempA = carbonizzare
		BREAK
		CASE 1
			traffic_veh_modeltempA = rocoto
		BREAK
		CASE 2
			traffic_veh_modeltempA = asterope
		BREAK
	ENDSWITCH
	SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
		CASE 0
			traffic_veh_modeltempB = fq2
		BREAK
		CASE 1
			traffic_veh_modeltempB = landstalker
		BREAK
		CASE 2
			traffic_veh_modeltempB = emperor
		BREAK
	ENDSWITCH
	
	traffic_veh_model[10] = traffic_veh_modeltempA
	traffic_veh_model[9] = traffic_veh_modeltempA
	traffic_veh_model[1] = traffic_veh_modeltempA
	
	traffic_veh_model[6] = traffic_veh_modeltempB
	traffic_veh_model[4] = traffic_veh_modeltempB
	traffic_veh_model[5] = traffic_veh_modeltempB

	
	truck_veh_model[0] = STRETCH
	truck_veh_model[1] = BOXVILLE2
	truck_veh_model[2] = BUS
	truck_veh_model[3] = TRASH
	
	ambient_veh_model[0] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[1] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[2] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ambient_veh_model[3] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	
	INT iNG
	REPEAT COUNT_OF(nextgen_veh_model) iNG
		nextgen_veh_model[iNG] = traffic_veh_model[GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(traffic_veh_model))]
	ENDREPEAT
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Traffic_Placeholder_Scene_Coordinates(PED_REQUEST_SCENE_ENUM eScene,
		VECTOR vTraffic_veh_0_coord,	VECTOR vTraffic_veh_1_coord,
		FLOAT fTraffic_veh_0_head,
		VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[])
	
	VECTOR vCreateCoords
	FLOAT fCreateHead
	TEXT_LABEL_31 tRoom
	GET_PLAYER_PED_POSITION_FOR_SCENE(eScene, vCreateCoords, fCreateHead, tRoom)
	
	
	VECTOR vAddOffset = vTraffic_veh_1_coord-vTraffic_veh_0_coord
	
	vTraffic_veh_offset[0] = (vTraffic_veh_0_coord-vCreateCoords) + get_random_vector()
	vTraffic_veh_offset[1] = vTraffic_veh_offset[0] + (vAddOffset*<<1,-0.5,0>>) +get_random_vector()
	
	CONST_INT iSTARTCOORD 2
	INT iVeh
	FOR iVeh = iSTARTCOORD TO (COUNT_OF(vTraffic_veh_offset) - 1)
		vTraffic_veh_offset[iVeh] = vTraffic_veh_offset[iVeh-2]
		vTraffic_veh_offset[iVeh] += vAddOffset
		vTraffic_veh_offset[iVeh] += get_random_vector()
		
	ENDFOR
	
	vTruck_veh_offset[0] = vTraffic_veh_offset[0] - (vAddOffset*2.0) + get_random_vector()
	vTruck_veh_offset[1] = vTraffic_veh_offset[1] - (vAddOffset*2.0) + get_random_vector()
	vTruck_veh_offset[2] = vTraffic_veh_offset[0] + vAddOffset
	vTruck_veh_offset[3] = vTraffic_veh_offset[1] + vAddOffset
	
	vAmbient_veh_offset[0] = get_random_vector() + ((vTraffic_veh_offset[0] + vTraffic_veh_offset[1])/2.0) * 0.2
	vAmbient_veh_offset[1] = get_random_vector() + ((vTraffic_veh_offset[0] + vTraffic_veh_offset[1])/2.0) * 0.4
	vAmbient_veh_offset[2] = get_random_vector() + ((vTraffic_veh_offset[0] + vTraffic_veh_offset[1])/2.0) * 0.6
	vAmbient_veh_offset[3] = get_random_vector() + ((vTraffic_veh_offset[0] + vTraffic_veh_offset[1])/2.0) * 0.8
	
	
	FLOAT fAddOffset = fTraffic_veh_0_head - fCreateHead
	
	fTraffic_veh_offset[0] = fAddOffset + GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
	FOR iVeh = 1 TO (COUNT_OF(fTraffic_veh_offset) - 1)
		fTraffic_veh_offset[iVeh] = fTraffic_veh_offset[iVeh-1] + GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
	ENDFOR
	
	fTruck_veh_offset[0] = fAddOffset + GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
	FOR iVeh = 1 TO (COUNT_OF(fTruck_veh_offset) - 1)
		fTruck_veh_offset[iVeh] = fTruck_veh_offset[iVeh-1] + GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
	ENDFOR		
	
	fAmbient_veh_offset[0] = fAddOffset + GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
	FOR iVeh = 1 TO (COUNT_OF(fAmbient_veh_offset) - 1)
		fAmbient_veh_offset[iVeh] = fAmbient_veh_offset[iVeh-1] + GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
	ENDFOR
	
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_M_Traffic_A_Scene_Coordinates(
		VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[])
	vTraffic_veh_offset[0] = <<-8.7715, -22.2829, 0.0>>		fTraffic_veh_offset[0] = -16.0462
	vTraffic_veh_offset[1] = <<-0.9052, -21.7043, 0.0>>		fTraffic_veh_offset[1] = -18.0565
	vTraffic_veh_offset[2] = <<-4.7120, -16.5554, 0.0>>		fTraffic_veh_offset[2] = -18.3452
	vTraffic_veh_offset[3] = <<3.3062, -15.4478, 0.0>>		fTraffic_veh_offset[3] = -21.8350
	vTraffic_veh_offset[4] = <<-0.3094, -9.6423, 0.0>>		fTraffic_veh_offset[4] = -20.3657
	vTraffic_veh_offset[5] = <<8.4392, -8.6499, 0.0>>		fTraffic_veh_offset[5] = -18.8008
	vTraffic_veh_offset[6] = <<5.3358, -2.3998, 0.0>>		fTraffic_veh_offset[6] = -20.5240
	vTraffic_veh_offset[7] = <<14.4868, -0.4662, 0.0>>		fTraffic_veh_offset[7] = -21.9109
	vTraffic_veh_offset[8] = <<11.1226, 6.3618, 0.0>>		fTraffic_veh_offset[8] = -17.9999
	vTraffic_veh_offset[9] = <<21.1994, 8.5525, 0.0>>		fTraffic_veh_offset[9] = -17.9017
	vTraffic_veh_offset[10] = <<17.6094, 15.4512, 0.0>>		fTraffic_veh_offset[10] = -15.6317
	vTraffic_veh_offset[11] = <<27.8973, 18.7321, 0.0>>		fTraffic_veh_offset[11] = -21.7654
	
	vTruck_veh_offset[0] = <<-19.4623, -36.7339, 0.0>>		fTruck_veh_offset[0] = -15.2806
	vTruck_veh_offset[1] = <<-11.7179, -35.5764, 0.0>>		fTruck_veh_offset[1] = -18.5494
	vTruck_veh_offset[2] = <<-15.9623, -31.8339, 0.0>>		fTruck_veh_offset[2] = -15.1402
	vTruck_veh_offset[3] = <<-8.2179, -30.6764, 0.0>>		fTruck_veh_offset[3] = -21.3636
	
	vAmbient_veh_offset[0] = <<2.0308, 8.0361, 0.0>>		fAmbient_veh_offset[0] = 2.1740
	vAmbient_veh_offset[1] = <<4.0933, 15.6763, 0.0>>		fAmbient_veh_offset[1] = -8.9755
	vAmbient_veh_offset[2] = <<-23.3930, -14.9052, -11.4>>	fAmbient_veh_offset[2] = -29.2004
	vAmbient_veh_offset[3] = <<-13.8158, -10.8706, -12.7>>	fAmbient_veh_offset[3] = -27.8686
	
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_M_Traffic_B_Scene_Variables(
		VECTOR &vTraffic_veh_offset[], FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[], FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[], FLOAT &fAmbient_veh_offset[])
	vTraffic_veh_offset[0] = <<-38.3005, 8.5562, 0.0>>		fTraffic_veh_offset[0] = -16.0462
	vTraffic_veh_offset[1] = <<-29.9505, 8.6162, 0.0>>		fTraffic_veh_offset[1] = -18.0565
	vTraffic_veh_offset[2] = <<-23.7805, 13.1962, 0.0>>		fTraffic_veh_offset[2] = -18.3452
	vTraffic_veh_offset[3] = <<-29.6605, 16.1762, 0.0>>		fTraffic_veh_offset[3] = -21.8350
	vTraffic_veh_offset[4] = <<-14.3505, 6.5462, 0.0>>		fTraffic_veh_offset[4] = -20.3657
	vTraffic_veh_offset[5] = <<-18.3605, 0.5062, 0.0>>		fTraffic_veh_offset[5] = -18.8008
	vTraffic_veh_offset[6] = <<-29.4805, 2.2262, 0.0>>		fTraffic_veh_offset[6] = -20.5240
	vTraffic_veh_offset[7] = <<-24.1905, 4.6262, 0.0>>		fTraffic_veh_offset[7] = -21.9109
	vTraffic_veh_offset[8] = <<-24.3405, -2.2238, 0.0>>		fTraffic_veh_offset[8] = -17.9999
	vTraffic_veh_offset[9] = <<-13.5805, -4.2538, 0.0>>		fTraffic_veh_offset[9] = -17.9017
	vTraffic_veh_offset[10] = <<-19.6705, -6.3938, 0.0>>	fTraffic_veh_offset[10] = -15.6317
	vTraffic_veh_offset[11] = <<-15.2805, -10.5438, 0.0>>	fTraffic_veh_offset[11] = -21.7654
	
	vTruck_veh_offset[0] = <<-47.51, 23.097, 0.0>>			fTruck_veh_offset[0] = -50.2806
	vTruck_veh_offset[1] = <<-44.64, 28.267, 0.0>>			fTruck_veh_offset[1] = -8.5494
	vTruck_veh_offset[2] = <<-43.8405, 19.3262, 0.0>>		fTruck_veh_offset[2] = -15.1402
	vTruck_veh_offset[3] = <<-8.2179, -30.6764, 0.0>>		fTruck_veh_offset[3] = -21.3636
	
	vAmbient_veh_offset[0] = <<-7.25, -9.213, 0.0>>			fAmbient_veh_offset[0] = 2.1740
	vAmbient_veh_offset[1] = <<-9.14, -15.643, 0.0>>		fAmbient_veh_offset[1] = -8.9755
	vAmbient_veh_offset[2] = <<1.83, -16.063, 0.0>>			fAmbient_veh_offset[2] = -29.2004
	vAmbient_veh_offset[3] = <<-3.76, -20.593, 0.0>>		fAmbient_veh_offset[3] = -27.8686
	
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_M_Traffic_C_Scene_Variables(
		VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[])
	vTraffic_veh_offset[0] = <<-29.899, -48.6, 0.0>>	fTraffic_veh_offset[0] = 296.0
	vTraffic_veh_offset[1] = <<-17.809, -40.73, 0.0>>	fTraffic_veh_offset[1] = 274.46
	vTraffic_veh_offset[2] = <<-18.339, -26.16, 0.0>>	fTraffic_veh_offset[2] = 309.22
	vTraffic_veh_offset[3] = <<-12.76, -37.72, 0.0>>	fTraffic_veh_offset[3] = 273.00
	vTraffic_veh_offset[4] = <<-7.669, -34.99, 0.0>>	fTraffic_veh_offset[4] = 270.00
	vTraffic_veh_offset[5] = <<4.461, -23.85, 0.0>>		fTraffic_veh_offset[5] = 270.00
	vTraffic_veh_offset[6] = <<-2.609, -32.32, 0.0>>	fTraffic_veh_offset[6] = 270.00
	vTraffic_veh_offset[7] = <<3.591, -30.44, 0.0>>		fTraffic_veh_offset[7] = 266.00
	vTraffic_veh_offset[8] = <<9.701, -22.01, 0.0>>		fTraffic_veh_offset[8] = 259.00
	vTraffic_veh_offset[9] = <<34.851, -18.68, 0.0>>	fTraffic_veh_offset[9] = 259.00
	vTraffic_veh_offset[10] = <<2.311, -7.35, 0.0>>		fTraffic_veh_offset[10] = 353.49
	vTraffic_veh_offset[11] = <<7.721, -5.22, 0.0>>		fTraffic_veh_offset[11] = 353.49
	
	vTruck_veh_offset[0] = <<-21.429, -36.16, 0.0>>		fTruck_veh_offset[0] = -40.2806
	vTruck_veh_offset[1] = <<-32.949, -44.24, 0.0>>		fTruck_veh_offset[1] = 296.0
	vTruck_veh_offset[2] = <<-5.419, -28.52, 0.0>>		fTruck_veh_offset[2] = 268.79
	vTruck_veh_offset[3] = <<-16.149, -34.45, 0.0>>		fTruck_veh_offset[3] = 269.44
	
	vAmbient_veh_offset[0] = <<9.911, -27.02, 0.0>>		fAmbient_veh_offset[0] = 265.00
	vAmbient_veh_offset[1] = <<15.671, -24.5, 0.0>>		fAmbient_veh_offset[1] = 262.00
	vAmbient_veh_offset[2] = <<20.341, -22.64, 0.0>>	fAmbient_veh_offset[2] = 260.00
	vAmbient_veh_offset[3] = <<28.061, -20.67, 0.0>>	fAmbient_veh_offset[3] = 269.00
	
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_F_Traffic_B_Scene_Variables(
		VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[])
	vTraffic_veh_offset[0] = <<1.34, -37.14, 0.0>>		fTraffic_veh_offset[0] = -91.57
	vTraffic_veh_offset[1] = <<7.99,-38.55, 0.0>>		fTraffic_veh_offset[1] = -91.57
	vTraffic_veh_offset[2] = <<11.35, -25.5, 0.0>>		fTraffic_veh_offset[2] = -87.74
	vTraffic_veh_offset[3] = <<19.16, -25.28, 0.0>>		fTraffic_veh_offset[3] = -94.86
	vTraffic_veh_offset[4] = <<40.1, -8.519, 0.0>>		fTraffic_veh_offset[4] = -108.59
	vTraffic_veh_offset[5] = <<35.52, -12.35, 0.0>>		fTraffic_veh_offset[5] = -108.59
	vTraffic_veh_offset[6] = <<23.22, -21.68, 0.0>>		fTraffic_veh_offset[6] = -105.31
	vTraffic_veh_offset[7] = <<21.56, -38.68, 0.0>>		fTraffic_veh_offset[7] = 90.43
	vTraffic_veh_offset[8] = <<12.05, -10.24, 0.0>>		fTraffic_veh_offset[8] = -4.32
	vTraffic_veh_offset[9] = <<6.32, -5.309, 0.0>>		fTraffic_veh_offset[9] = -4.32
	vTraffic_veh_offset[10] = <<51.94, -36.66, 0.0>>	fTraffic_veh_offset[10] = -177.9
	vTraffic_veh_offset[11] = <<57.47, -41.3, 0.0>>		fTraffic_veh_offset[11] = -177.9
	
	vTruck_veh_offset[0] = <<29.53, -17.97, 0.0>>		fTruck_veh_offset[0] = -103.07
	vTruck_veh_offset[1] = <<35.68, 0.27, 0.0>>			fTruck_veh_offset[1] = -66.81
	vTruck_veh_offset[2] = <<11.74, -31.94, 0.0>>		fTruck_veh_offset[2] = -126.95
	vTruck_veh_offset[3] = <<45.13, -2.58, 0.0>>		fTruck_veh_offset[3] = -89.89
	
	vAmbient_veh_offset[0] = <<26.69, -41.23, 0.0>>		fAmbient_veh_offset[0] = 87.02
	vAmbient_veh_offset[1] = <<22.21, -46.2, 0.0>>		fAmbient_veh_offset[1] = 90.07
	vAmbient_veh_offset[2] = <<45.62, -10.14, 0.0>>		fAmbient_veh_offset[2] = -86.76
	vAmbient_veh_offset[3] = <<52.11, -3.18, 0.0>>		fAmbient_veh_offset[3] = -86.76
	
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_F_Traffic_C_Scene_Variables(
		VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[])
	
	//- vectors -//
	vTraffic_veh_offset[0] = <<16.3320, -30.0800, 0.0>>		fTraffic_veh_offset[0] = -90.4349
	vTraffic_veh_offset[1] = <<22.7820, -37.6800, 0.0>>		fTraffic_veh_offset[1] = -90.4349
	vTraffic_veh_offset[2] = <<28.2720, -29.2200, 0.0>>		fTraffic_veh_offset[2] = -90.7451
	vTraffic_veh_offset[3] = <<22.2820, -32.0500, 0.0>>		fTraffic_veh_offset[3] = -97.2851
	vTraffic_veh_offset[4] = <<15.0120, -34.5300, 0.0>>		fTraffic_veh_offset[4] = -94.9851
	vTraffic_veh_offset[5] = <<-14.0880, -53.3100, 0.0>>	fTraffic_veh_offset[5] = 174.4849
	vTraffic_veh_offset[6] = <<-8.6180, -54.6800, 0.0>>		fTraffic_veh_offset[6] = 176.0549
	vTraffic_veh_offset[7] = <<4.4320, -2.4200, 0.0>>		fTraffic_veh_offset[7] = -4.3151
	vTraffic_veh_offset[8] = <<1.4220, 5.5300, 0.0>>		fTraffic_veh_offset[8] = -4.3151
	vTraffic_veh_offset[9] = <<28.9220, -34.4200, 0.0>>		fTraffic_veh_offset[9] = -96.0151
	vTraffic_veh_offset[10] = <<34.0120, -36.5100, 0.0>>	fTraffic_veh_offset[10] = -99.9451
	vTraffic_veh_offset[11] = <<-4.8880, -0.6800, 0.0>>		fTraffic_veh_offset[11] = -5.7000
	
	vTruck_veh_offset[0] = <<-0.8180, -16.7000, 0.0>>		fTruck_veh_offset[0] = 7.2000
	vTruck_veh_offset[1] = <<21.2120, -26.7900, 0.0>>		fTruck_veh_offset[1] = -97.2851
	vTruck_veh_offset[2] = <<-31.2080, -27.7300, 0.0>>		fTruck_veh_offset[2] = 83.7149
	vTruck_veh_offset[3] = <<36.1820, -32.3500, 0.0>>		fTruck_veh_offset[3] = -99.9451

	vAmbient_veh_offset[0] = <<-8.7980, -15.9900, 0.0>>		fAmbient_veh_offset[0] = -54.0451
	vAmbient_veh_offset[1] = <<-16.4080, -17.5900, 0.0>>	fAmbient_veh_offset[1] = -96.0451
	vAmbient_veh_offset[2] = <<-18.1080, -12.5400, 0.0>>	fAmbient_veh_offset[2] = -88.5451
	vAmbient_veh_offset[3] = <<52.1100, -3.1800, 0.0>>		fAmbient_veh_offset[3] = -86.7600
	
ENDPROC

PROC Initialise_This_Traffic_Scene_Variables(PED_REQUEST_SCENE_ENUM eScene, MODEL_NAMES &traffic_ped_model[],
		MODEL_NAMES &traffic_veh_model[],	VECTOR &vTraffic_veh_offset[],	FLOAT &fTraffic_veh_offset[],
		MODEL_NAMES &truck_veh_model[],		VECTOR &vTruck_veh_offset[],	FLOAT &fTruck_veh_offset[],
		MODEL_NAMES &ambient_veh_model[],	VECTOR &vAmbient_veh_offset[],	FLOAT &fAmbient_veh_offset[],
		MODEL_NAMES &nextgen_veh_model[],	VECTOR &vNextgen_veh_offset[],	FLOAT &fNextgen_veh_offset[])
	SWITCH eScene
		CASE PR_SCENE_M_TRAFFIC_A
			Initialise_Default_Traffic_Scene_Coordinates(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_Default_Traffic_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			VECTOR vMTrafficACoords
			FLOAT fMTrafficAHead
			TEXT_LABEL_31 tMTrafficARoom
			GET_PLAYER_PED_POSITION_FOR_SCENE(eScene, vMTrafficACoords, fMTrafficAHead, tMTrafficARoom)
			vNextgen_veh_offset[0] = <<-453.6000, -1565.9159, 38.0807>> - vMTrafficACoords		fNextgen_veh_offset[0] = 171.8581 - fMTrafficAHead
			vNextgen_veh_offset[1] = <<-467.9528, -1599.4768, 38.0940>> - vMTrafficACoords		fNextgen_veh_offset[1] = 151.2346 - fMTrafficAHead
			vNextgen_veh_offset[2] = <<-442.0422, -1566.7344, 38.1786>> - vMTrafficACoords		fNextgen_veh_offset[2] = 170.7510 - fMTrafficAHead
			vNextgen_veh_offset[3] = <<-486.7076, -1614.5006, 32.4872>> - vMTrafficACoords		fNextgen_veh_offset[3] = 146.9473 - fMTrafficAHead
		BREAK
		CASE PR_SCENE_M_TRAFFIC_B
			Initialise_M_Traffic_B_Scene_Variables(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_Default_Traffic_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			vNextgen_veh_offset[0] = <<-7.5000, 0.0000, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[0] = 0.0000 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[1] = <<0.0000, -21.6000, 12.0000>>	+ get_random_vector()		fNextgen_veh_offset[1] = -8.1000 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[2] = <<2.4000, -6.3000, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[2] = 0.0000 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[3] = <<9.3000, -5.4000, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[3] = 0.0000 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
		BREAK
		CASE PR_SCENE_M_TRAFFIC_C
			Initialise_M_Traffic_C_Scene_Variables(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_M_Traffic_C_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			vNextgen_veh_offset[0] = <<17.4294, -18.7658, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[0] = -82.8600 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[1] = <<-8.4000, -0.2185, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[1] = 326.8800 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[2] = <<-1.0773, 11.1031, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[2] = -16.5600 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[3] = <<3.6826, 6.6924, 0.0000>>		+ get_random_vector()		fNextgen_veh_offset[3] = -21.4898 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
		BREAK
		
		CASE PR_SCENE_F_TRAFFIC_a
		CASE PR_SCENE_Fa_AGENCY1
			Initialise_Default_Traffic_Scene_Coordinates(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_Default_Traffic_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			VECTOR vFTrafficACoords
			FLOAT fFTrafficAHead
			TEXT_LABEL_31 tFTrafficARoom
			GET_PLAYER_PED_POSITION_FOR_SCENE(eScene, vFTrafficACoords, fFTrafficAHead, tFTrafficARoom)
			vNextgen_veh_offset[0] = <<-453.6000, -1565.9159, 38.0807>> - vFTrafficACoords		fNextgen_veh_offset[0] = 171.8581 - fFTrafficAHead
			vNextgen_veh_offset[1] = <<-467.9528, -1599.4768, 38.0940>> - vFTrafficACoords		fNextgen_veh_offset[1] = 151.2346 - fFTrafficAHead
			vNextgen_veh_offset[2] = <<-442.0422, -1566.7344, 38.1786>> - vFTrafficACoords		fNextgen_veh_offset[2] = 170.7510 - fFTrafficAHead
			vNextgen_veh_offset[3] = <<-486.7076, -1614.5006, 32.4872>> - vFTrafficACoords		fNextgen_veh_offset[3] = 146.9473 - fFTrafficAHead
		BREAK
		CASE PR_SCENE_F_TRAFFIC_B
			Initialise_F_Traffic_B_Scene_Variables(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_F_Traffic_B_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			vNextgen_veh_offset[0] = <<12.4248, -18.4198, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[0] = -42.9526 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[1] = <<-6.4904, 6.1436, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[1] = -0.0000 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[2] = <<15.2000, -46.6000, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[2] = 74.0878 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[3] = <<9.0000, -43.7000, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[3] = -102.2400 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
		BREAK
		CASE PR_SCENE_F_TRAFFIC_C
			Initialise_F_Traffic_C_Scene_Variables(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_F_Traffic_C_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			vNextgen_veh_offset[0] = <<-3.4877, -8.4660, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[0] = -10.2600 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[1] = <<-2.7229, 5.6192, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[1] = 2.3194 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[2] = <<6.8717, 5.1081, 0.0000>>		+ get_random_vector()		fNextgen_veh_offset[2] = 350.1767 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
			vNextgen_veh_offset[3] = <<0.9970, -9.6576, 0.0000>>	+ get_random_vector()		fNextgen_veh_offset[3] = 349.0776 + GET_RANDOM_FLOAT_IN_RANGE(-5,5)
		BREAK
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "placeholder variables for traffic scene ", Get_String_From_Ped_Request_Scene_Enum(eScene))
			#ENDIF
			
			Initialise_Default_Traffic_Scene_Coordinates(
					vTraffic_veh_offset,	fTraffic_veh_offset,
					vTruck_veh_offset,		fTruck_veh_offset,
					vAmbient_veh_offset,	fAmbient_veh_offset)
			Initialise_Default_Traffic_Scene_Models(traffic_ped_model,
					traffic_veh_model, truck_veh_model, ambient_veh_model, nextgen_veh_model)
			INT iNG
			REPEAT COUNT_OF(vNextgen_veh_offset) iNG
				nextgen_veh_model[iNG] = DUMMY_MODEL_FOR_SCRIPT
				vNextgen_veh_offset[iNG] = <<0,0,0>>
				fNextgen_veh_offset[iNG] = GET_RANDOM_FLOAT_IN_RANGE(0,360)
			ENDREPEAT
		BREAK
	ENDSWITCH
	
ENDPROC
