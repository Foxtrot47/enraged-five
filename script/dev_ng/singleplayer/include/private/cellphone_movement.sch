USING "Script_maths.sch"





FUNC BOOL ShouldFirstPersonPhoneHudMovementBeHidden()

    
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())

        
        IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT //See 2001926. For 99 percent of calls we want to show the phone on the hud
        OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL

            IF g_Use_Prologue_Cellphone = FALSE //2027292
                IF g_TheContactInvolvedinCall <> CHAR_DETONATEBOMB  //2029318

                    IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())   //If the ped isn't in a vehicle, but on call we want the phone to be seen on the hud.   
                                                                    //The phone isn't held to ear in vehicles, so the player can see the hud on call. 
                                                                    //Vehicle exception checks are detailed below.


                        IF g_ConversationStatus <> CONV_STATE_WAITING_ON_PLAYER_ACCREJ //Don't display the hud phone if the call is still in hand waiting for the user to accept or reject it.
                                                                                       //2031019
            
                            //RETURN FALSE //We need to show the phone on the hud if a call comes in on foot, return FALSE. Removed for 2038700

                            //Redacting the FALSE return for this for NY. See Bug Change: 2038700. An "on call" icon will display beside the radar and response required calls will move to waist height  
                            //via dialogue handler.


                        ENDIF

                    ENDIF

                ENDIF
            ENDIF
       
        ENDIF



        IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)

            RETURN FALSE //Fixes 2199569

        ENDIF



        IF IS_PED_IN_COVER(PLAYER_PED_ID()) 
 
            RETURN FALSE  //1996871  - Need to show the phone in cover in all camera modes.

        ENDIF


        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
            IF NOT IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING() //2140718
                IF IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))

                    RETURN FALSE //2140718

                ENDIF

            ENDIF
        ENDIF



        //Swimming check needs to go in here for bug 2017766
        //IF GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()) > 0.8
        IF IS_PED_SWIMMING (PLAYER_PED_ID())
        OR IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())
        OR IS_PLAYER_CLIMBING (PLAYER_ID()) //2057254
        OR IS_PED_HANGING_ON_TO_VEHICLE(PLAYER_PED_ID()) //2114965
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_HidePhoneInWater) AND (NETWORK_IS_ACTIVITY_SESSION() OR g_bFMMC_InTestMode_MissionController))

            RETURN FALSE

        ENDIF
        
        // B*2053172 - set when phone needs to be forced to display on the hud if active when a passenger in a taxi
        IF g_bTaxiShouldForcePhoneDislayOnHud
        
            RETURN FALSE
        
        ENDIF

    ENDIF
    




    IF g_bInMultiplayer

        RETURN FALSE  // No phone in hand in MP. We need the phone on the hud at all times.

    ELSE
     
        CAM_VIEW_MODE_CONTEXT activeViewModeContext
        CAM_VIEW_MODE activeViewMode

        BOOL b_InFirstPerson_and_NoHudCompatible = FALSE
        
        activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
        //IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT //Example exclusion

            activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)

            IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON AND
                (activeViewModeContext = CAM_VIEW_MODE_CONTEXT_ON_FOOT OR
                 IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING())

               b_InFirstPerson_and_NoHudCompatible = TRUE //Okay we're in first person...


               IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                    
                    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 

                        VEHICLE_INDEX tempCP_VehicleIndex

                        tempCP_VehicleIndex = GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID()) //Any additions to the model list below should be cloned in appCamera's abort list as a failsafe.

           
                        IF (IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL (tempCP_VehicleIndex))) //1992643
                        OR (IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL (tempCP_VehicleIndex)))
                        OR (IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL (tempCP_VehicleIndex)))
                        OR (GET_ENTITY_MODEL(tempCP_VehicleIndex)) = SEASHARK
                        OR (GET_ENTITY_MODEL(tempCP_VehicleIndex)) = SEASHARK2
                        OR (GET_ENTITY_MODEL(tempCP_VehicleIndex)) = RHINO //2060760
                        OR (GET_ENTITY_MODEL(tempCP_VehicleIndex)) = SUBMERSIBLE //2166536
                        OR (GET_ENTITY_MODEL(tempCP_VehicleIndex)) = SUBMERSIBLE2//2166536
                        OR (GET_ENTITY_MODEL(tempCP_VehicleIndex)) = TORO//2329939

                            /* This would spam every frame.
                            #if IS_DEBUG_BUILD
                                
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_MOVEMENT - ShouldFirstPersonPhoneHudMovementBeHidden, in 1st person setting b_InFirstPerson = FALSE due to unsuitable vehicle type.")
                                cdPrintnl ()

                            #endif
                            */

                            b_InFirstPerson_and_NoHudCompatible = FALSE  //Oh, crap. Those vehicle groups above need to show the hud as the phone doesn't appear in hand.

                        ENDIF

                    ENDIF

              ENDIF

                /*
                #if IS_DEBUG_BUILD 

                    cdPrintnl()
                    cdPrintstring("FP view mode detected - hiding hud.") 
                    cdPrintnl()

                #endif
                */

            ELSE

                /*
                #if IS_DEBUG_BUILD 

                    cdPrintnl()
                    cdPrintstring("Not in first person view - showing hud.") 
                    cdPrintnl()

                #endif
                */

            ENDIF
        
        //ENDIF //End of example exclusion conditon check



        IF g_b_ShouldCellphoneUseFirstPersonAnims //from cellphone_globals_TU.sch. Can be controlled by widget in root of script - Cellphone and Dialog Debug.
        
        OR b_InFirstPerson_and_NoHudCompatible = TRUE


            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTrackify")) > 0 
            OR g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable
                
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael2")) > 0 //Trackify dodgy in 1st person but it seems okay in Michael 2 now.

                    RETURN TRUE

                ELSE

                    RETURN FALSE  //Trackify doesn't suit being hidden in submarines which is the only other major SP use.

                ENDIF

            ELSE

                RETURN TRUE
            
            ENDIF

        ELSE
            
            RETURN FALSE

        ENDIF


    ENDIF


ENDFUNC





// declared here simply on the basis that it need not be a global (or we'd have to change all the scripts everywhere to use it)
INT s_iTimeStampStart = 0

FUNC VECTOR TWEEN_IT(VECTOR fStart, VECTOR fEnd, FLOAT tVal)
    RETURN fStart + (fEnd-fStart)*tVal
ENDFUNC




FUNC FLOAT MovePhoneToDestination(VECTOR vStartPos, VECTOR vEndPos, VECTOR vStartRot, VECTOR vEndRot, FLOAT fTime, BOOL bBounce)


    IF g_FirstPersonTransitionOngoing = FALSE //check for exceptions here.
    
        //Special handling to make sure the phone moves away nicely when in a first person exceptional case.
        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
        AND g_Cellphone.PhoneDS < PDS_TAKEOUT 

            
            VECTOR CurrentCellphonePosition

            GET_MOBILE_PHONE_POSITION (CurrentCellphonePosition) 
            
            IF g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio].y = CurrentCellphonePosition.Y
        
                g_FirstPersonTransitionOngoing = TRUE

                #if IS_DEBUG_BUILD
                    
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_MOVEMENT - Exception fired for movement!")
                    cdPrintnl ()

                #endif



            ENDIF
        
        ENDIF

    ENDIF



    IF ShouldFirstPersonPhoneHudMovementBeHidden()  //In SP first person mode we don't need to display any hud phone movement...
    AND g_FirstPersonTransitionOngoing = FALSE      //but if this is true, that means we have moved into 1st person from 3rd whilst the phone was up, so we DO need to perform a valid visual move down.
                                                    //regardless of now being in 1st person mode. If it's also FALSE, then we don't need to display any visual movement.
    
    //AND g_3DPhoneNeedsMovedUp = FALSE //Doesn't fix blimp issue, caused by duplicate takeouts due to bad forceonscreen use. See 2027104

        #if IS_DEBUG_BUILD
            
            cdPrintnl()
            cdPrintstring ("CELLPHONE_MOVEMENT - Returning 2.0")
            cdPrintnl ()

        #endif




        RETURN 2.0 //Scripts that check the return value of MovePhoneToDestination accept the phone movement as complete once it return 1.0 or greater.
                   //Returning 2.0 instantly will make phone movement instantly complete, so it should remain off screen in its original creation position.

    ELSE


        // If the anim hasn't started, start it now
        IF s_iTimeStampStart = 0
            s_iTimeStampStart = GET_GAME_TIMER()
        ENDIF


        FLOAT t = CLAMP(TO_FLOAT(GET_GAME_TIMER() - s_iTimeStampStart) / fTime, 0.0, 1.0)
        
        IF t < 1.0
            // given a percentage from 0 to 1, adjust the percentage via a non-linear interpolation function to get a more bouncy look
        
            FLOAT adjT = t
            IF bBounce
                adjT -= 1.0
                // ease out/Back
                FLOAT EaseValue = 0.670158
                adjT = adjT * adjT * ((EaseValue + 1.0)*adjT + EaseValue) + 1.0
            ELSE
                // sin out
                adjT = SIN(t * 90)
            ENDIF
            
            g_3dPhonePosVec = TWEEN_IT( vStartPos, vEndPos, adjT )
            g_3dPhoneRotVec = TWEEN_IT( vStartRot, vEndRot, adjT )
        ELSE 
            g_3dPhonePosVec = vEndPos
            g_3dPhoneRotVec = vEndRot
            
        ENDIF
        
        SET_MOBILE_PHONE_POSITION( g_3dPhonePosVec )
        SET_MOBILE_PHONE_ROTATION( g_3dPhoneRotVec )
        RETURN t // coulda been a bool, but one script needs to know when the phone is mostly up


    ENDIF


ENDFUNC

