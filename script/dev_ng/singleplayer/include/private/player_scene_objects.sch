//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_objects.sch									//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "building_globals.sch"
USING "commands_entity.sch"

#IF IS_DEBUG_BUILD
FUNC STRING SAFE_GET_MODEL_NAME_FOR_DEBUG(MODEL_NAMES ModelHashKey)
	IF (ModelHashKey = DUMMY_MODEL_FOR_SCRIPT)
		RETURN "dummy_model_for_script"
	ENDIF
	
	IF (ModelHashKey = V_RES_TT_FLUSHER)
		RETURN "V_RES_TT_FLUSHER"
	ENDIF
	
	RETURN GET_MODEL_NAME_FOR_DEBUG(ModelHashKey)
ENDFUNC
#ENDIF



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ENUM enumPlayerSceneObjectAction
	PSOA_0_detach = 0,
	PSOA_1_delete,
	PSOA_2_synched,
	PSOA_3_world,
	PSOA_4_synchSwap,
	PSOA_5_explode,
	PSOA_6_keepHide,
	PSOA_7_weaponSwap,
	PSOA_8_ptfxSwap,
	
	PSOA_9_synchAndPtfxSwap,
	
	PSOA_NULL = -1
ENDENUM

FUNC BOOL GET_OBJECTS_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		MODEL_NAMES &ePropModel, VECTOR &propOffset, VECTOR &propRotation, PED_BONETAG &eAttachBonetag,
		FLOAT &fDetachAnimPhase, enumPlayerSceneObjectAction &thisSceneObjectAction)
	
	propOffset = <<0,0,0>>
	propRotation= <<0,0,0>>
	
	SWITCH eScene
//		CASE PR_SCENE_M_S_FAMILY4
//			ePropModel = P_KITCH_JUICER_S
//			
//			propRotation= <<0,0,-25.0>>
//			propOffset = <<-0.72, -0.289, 1.09>>
//			
//			eAttachBonetag = BONETAG_NULL
//			fDetachAnimPhase = 1.0
//			thisSceneObjectAction = PSOA_0_detach
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3
			ePropModel = Prop_phone_ING_03
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.90
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1
//			RETURN GET_OBJECTS_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3,
//					ePropModel, propOffset, propRotation, eAttachBonetag,
//					fDetachAnimPhase, thisSceneObjectAction)
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3
//			RETURN GET_OBJECTS_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3,
//					ePropModel, propOffset, propRotation, eAttachBonetag,
//					fDetachAnimPhase, thisSceneObjectAction)
//		BREAK
		CASE PR_SCENE_F1_NEWHOUSE
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				fDetachAnimPhase = 0.7
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_b
			ePropModel = prop_table_05_chr
			
			PropOffset = <<0,0,0>>		//<<0.6619, 0.0777, -0.9754>>	//<<0.0000, 0.1500, -0.5900>>	//<<0.4867, 0.1893, -0.9754>>
			propRotation = <<0,0,0>>	//<<-27.000,0,180.000>>
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.05
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK	
		CASE PR_SCENE_M_BENCHCALL_a
		CASE PR_SCENE_M_BENCHCALL_b
			ePropModel = Prop_phone_ING
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = -1
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK	
		CASE PR_SCENE_M6_ONPHONE
			RETURN GET_OBJECTS_FOR_SCENE(PR_SCENE_M_BENCHCALL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
		BREAK
		CASE PR_SCENE_M6_DEPRESSED
			RETURN GET_OBJECTS_FOR_SCENE(PR_SCENE_M_BENCHCALL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
		BREAK
		CASE PR_SCENE_M2_PHARMACY
			ePropModel = Prop_phone_ING
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.9300
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED
			ePropModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL)
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 1.0
			thisSceneObjectAction = PSOA_7_weaponSwap
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_CANAL_a
			ePropModel = PROP_CIGAR_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_CANAL_b
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M_CANAL_c
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M_PIER_b
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M_VWOODPARK_a
		CASE PR_SCENE_M_VWOODPARK_b
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				fDetachAnimPhase = 0.7234	//0.7136	//0.723
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_MORNING_a
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T6_SMOKECRYSTAL
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_CANAL_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M_COFFEE_a
			ePropModel = P_ING_COFFEECUP_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_COFFEE_b
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_COFFEE_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				ePropModel = P_AMB_COFFEECUP_01
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M_COFFEE_c
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_COFFEE_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				ePropModel = P_ING_COFFEECUP_01
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M4_CINEMA
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_COFFEE_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				ePropModel = PROP_PLASTIC_CUP_02
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_COFFEE
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_M_COFFEE_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				ePropModel = P_ING_COFFEECUP_01
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_READSCRIPT
			ePropModel = P_CS_SCRIPT_S
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.86
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA
			ePropModel = PROP_CIGAR_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			ePropModel = PROP_TENNIS_RACK_01
			
			propOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), -1.0000>>
			propRotation = <<90,0,GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)>>
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.05
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			ePropModel = PROP_CONTROLLER_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			ePropModel = P_BankNote_S
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
//		CASE PR_SCENE_M_HOOKERCAR
			ePropModel = P_BankNote_S
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			ePropModel = PROP_BEACH_BAG_02		//Prop_Beach_lotion_01, Prop_Beach_lotion_02, Prop_Beach_lotion_03, Prop_Beach_bag_02, Prop_Beach_bag_01a, Prop_Beach_towel_03
			
			propOffset = <<1.0027, 2.3148, -0.9300>>
			propRotation = <<0.0000, 0.0000, 141.4800>>
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F0_TANISHAFIGHT
			ePropModel = v_ilev_fa_frontdoor
			
//			propOffset = <<-14.959,-1441.302,30.910>>	- <<-14.6713, -1438.8420, 31.1001>>
//			propRotation = <<0.0000, 0.0000, 179.56 - -151.5000 - 360>>
			
			PropOffset = <<0,-0.180,0>>		//<<0,0,0>>			//<<-0.1977, -2.3400, 0.0799>>
			PropRotation = <<0,0,180>>		//<<0,0,180>>		//<<0.0000, 0.0000, -28.9400>>
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.01
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_SH_ASLEEP
			ePropModel = V_ILev_fh_lampA_on	//V_Res_FH_lampA_on
			
			thisSceneObjectAction = PSOA_3_world
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			
			propOffset = <<-2.895,525.526,170.134>>		- <<-0.4500, 525.4900, 169.6400>>
			propRotation = <<0.0, 0.0, 104.8200>>		- <<0,0,-147.0000>>
			
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_READING
		CASE PR_SCENE_F1_SH_READING
			ePropModel = PROP_CS_BOOK_01
			
			eAttachBonetag = BONETAG_PH_L_HAND
			fDetachAnimPhase = 0.705
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_GARBAGE
		CASE PR_SCENE_F1_GARBAGE
			ePropModel = PROP_CS_RUB_BINBAG_01 	//Prop_LD_rub_binbag_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			
			IF(eScene = PR_SCENE_F0_GARBAGE)
				fDetachAnimPhase = 0.6645	//0.6647
				thisSceneObjectAction = PSOA_2_synched
			ELIF(eScene = PR_SCENE_F1_GARBAGE)
				fDetachAnimPhase = 0.9401
				thisSceneObjectAction = PSOA_0_detach
			ELSE
				thisSceneObjectAction = PSOA_2_synched
				fDetachAnimPhase = 0.8
			ENDIF
			
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WATCHINGTV
		CASE PR_SCENE_T_FLOYDSAVEHOUSE
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_F1_WATCHINGTV,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				
				thisSceneObjectAction = PSOA_0_detach
				IF (eScene = PR_SCENE_M4_WATCHINGTV)
					thisSceneObjectAction = PSOA_3_world
				ENDIF
				IF (eScene = PR_SCENE_T_FLOYDSAVEHOUSE)
				//	ePropModel = V_RES_TRE_REMOTE
				//	thisSceneObjectAction = PSOA_3_world
				ENDIF
				
				fDetachAnimPhase = 0.3964
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_F0_WATCHINGTV
		CASE PR_SCENE_F1_WATCHINGTV
			ePropModel = PROP_CS_REMOTE_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.2706	//0.4206	//0.65
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_CLEANCAR
		CASE PR_SCENE_F1_CLEANCAR
			ePropModel = Prop_Scourer_01
			
			propOffset = <<0,0,0>>
			propRotation = <<0,0,0>>
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F1_CLEANINGAPT
//			ePropModel = Prop_Wine_Glass 
//			
//			eAttachBonetag = BONETAG_PH_R_HAND
//			fDetachAnimPhase = 0.70
//			thisSceneObjectAction = PSOA_0_detach
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F1_SNACKING
			ePropModel = Prop_Crisp
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.349
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			ePropModel = Prop_Laptop_Jimmy
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			ePropModel = P_Laptop_02_S		//Prop_Laptop_02_CLOSED
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.7558
			thisSceneObjectAction = PSOA_4_synchSwap
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONCELL
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				fDetachAnimPhase = 0.85
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_a
		CASE PR_SCENE_F_S_AGENCY_2A_b
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				fDetachAnimPhase = 0.85
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			ePropModel = PROP_CHATEAU_TABLE_01
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			
			propOffset = <<-0.0002, 0.0002, 0.3640>>
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_RONBORING		//Michaels chair
			ePropModel = PROP_CHAIR_06
			
			propOffset = <<1975.425,3822.014,32.430>> - <<1978.0699, 3819.5640, 32.4290>>
			propRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.65
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_RESTAURANT		//players chair
			ePropModel = PROP_CHATEAU_CHAIR_01
			
			propOffset = <<-116.247,363.356,112.393>> - <<-115.9200, 363.5000, 112.8857>>
			propRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.90
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_IRONING
			ePropModel = PROP_IRON_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a
			ePropModel = P_WEED_BOTTLE_S
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.25+0.1
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC
			ePropModel = P_WEED_BOTTLE_S
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.782	//0.738		//0.766
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_b
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_F_KUSH_DOC_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				thisSceneObjectAction = PSOA_1_delete
				fDetachAnimPhase = 0.65
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_c
			IF GET_OBJECTS_FOR_SCENE(PR_SCENE_F_KUSH_DOC_a,
					ePropModel, propOffset, propRotation, eAttachBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
				thisSceneObjectAction = PSOA_1_delete
				fDetachAnimPhase = 0.65
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M_PIER_a
			ePropModel = PROP_CIGAR_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.85
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD
			ePropModel = PROP_TACO_01
			
			eAttachBonetag = BONETAG_PH_L_HAND
			fDetachAnimPhase = 0.8308
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			ePropModel = PROP_TACO_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.6
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_THROW_CUP
			ePropModel = P_AMB_COFFEECUP_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.7
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			ePropModel = P_AMB_COFFEECUP_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.74
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_BAR
			ePropModel = PROP_CS_BEER_BOT_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_2_synched
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_STRIPCLUB_out
			ePropModel = P_PANTIES_S
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DOCKS_a
		CASE PR_SCENE_T_DOCKS_b
		CASE PR_SCENE_T_DOCKS_c
		CASE PR_SCENE_T_DOCKS_d
			ePropModel = P_NOTEPAD_01_S
			
			eAttachBonetag = BONETAG_PH_L_HAND
			fDetachAnimPhase = 0.8989
			thisSceneObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			ePropModel = PROP_MR_RASPBERRY_01
			
			propOffset = <<0.1774, 1.4431, 1.2766>>
			propRotation= <<0,-85,0>>
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.7
			thisSceneObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			ePropModel = PROP_ACC_GUITAR_01
			
			propOffset = <<0.1,0,0>>
			propRotation= <<0,-85,0>>
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.554
			thisSceneObjectAction = PSOA_9_synchAndPtfxSwap
			
			RETURN TRUE
		BREAK

		CASE PR_SCENE_T_CR_ALLEYDRUNK
		CASE PR_SCENE_T_SC_ALLEYDRUNK
		CASE PR_SCENE_T_CN_WAKETRASH_b
		CASE PR_SCENE_T_CR_WAKEBEACH
		CASE PR_SCENE_T_CN_WAKEBARN
		CASE PR_SCENE_T_CN_WAKETRAIN
		CASE PR_SCENE_T_CR_WAKEROOFTOP
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
		CASE PR_SCENE_T_NAKED_GARDEN
		CASE PR_SCENE_T_CN_CHATEAU_b
		CASE PR_SCENE_T_CN_CHATEAU_c
		CASE PR_SCENE_T_CR_CHATEAU_d
			ePropModel = PROP_CS_BEER_BOT_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.527
			
			thisSceneObjectAction = PSOA_8_ptfxSwap
			
			RETURN TRUE
		BREAK

		CASE PR_SCENE_T_NAKED_ISLAND
			ePropModel = PROP_CS_BEER_BOT_01
			
			eAttachBonetag = BONETAG_PH_L_HAND
			fDetachAnimPhase = 0.4548
			
			thisSceneObjectAction = PSOA_8_ptfxSwap
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_FLOYD_DOLL
			ePropModel = P_DEFILIED_RAGDOLL_01_S
			
			propOffset = <<0,0,0>>
			propRotation = <<0,0,0>>
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			ePropModel = PROP_PINEAPPLE
			
			propOffset = <<0,0,0>>
			propRotation = <<0,0,0>>
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.114	//0.132	//0.1366
			thisSceneObjectAction = PSOA_8_ptfxSwap
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			ePropModel = P_HAND_TOILET_S
			
			propOffset = <<1971.6298, 3819.5647, 32.9692>>		- <<1972.2371, 3817.8601, 33.4287>>
			propRotation= <<0,0,GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)>>
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			thisSceneObjectAction = PSOA_4_synchSwap
			
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_CR_DUMPSTER
//			ePropModel = PROP_SKIP_01A
//			
//			propOffset = <<-0.2000, 0.0000, -1.1900>>
//			propRotation = <<0.0000, 0.0000, 90.0000>>
//
//			eAttachBonetag = BONETAG_NULL
//			fDetachAnimPhase = 0.99
//			thisSceneObjectAction = PSOA_0_detach
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_SMOKEMETH
		CASE PR_SCENE_Ta_RC_MRSP2
			ePropModel = Prop_CS_CRACKPIPE
			eAttachBonetag = BONETAG_PH_R_HAND
			
			propOffset = <<0,0,0>>
			propRotation = <<0,0,0>>

			fDetachAnimPhase = 0.65
			thisSceneObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP
//			ePropModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_GRENADE)
//			eAttachBonetag = BONETAG_NULL
//			
//			propOffset = <<0,0,0>>
//			propRotation = <<0,0,0>>
//
//			fDetachAnimPhase = 0.60
//			thisSceneObjectAction = PSOA_5_explode
//			
//			RETURN TRUE
//		BREAK
		
	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_63 sInvalid
//	sInvalid = "no object for eScene: "
//	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
//	
//	CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ", sInvalid)
//	#ENDIF
	
	ePropModel = DUMMY_MODEL_FOR_SCRIPT
	
	propOffset = <<0,0,0>>
	propRotation= <<0,0,0>>
	
	eAttachBonetag = BONETAG_NULL
	fDetachAnimPhase = -1.0
	thisSceneObjectAction = PSOA_NULL
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_EXTRA_OBJECTS_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		MODEL_NAMES &eExtraObjectModel, VECTOR &extraPropOffset, VECTOR &extraPropRotation, PED_BONETAG &eAttachBonetag,
		FLOAT &fDetachAnimPhase, enumPlayerSceneObjectAction &thisSceneExtraObjectAction, BOOL &bAttachedToBuddy)
	
	extraPropOffset = <<0,0,0>>
	extraPropRotation= <<0,0,0>>
	bAttachedToBuddy = FALSE
	
	SWITCH eScene
		CASE PR_SCENE_F1_SNACKING
			eExtraObjectModel = Prop_CS_Crisps_01
			
			eAttachBonetag = BONETAG_PH_L_HAND
			fDetachAnimPhase = 0.574
			thisSceneExtraObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_CLEANCAR
		CASE PR_SCENE_F1_CLEANCAR
			eExtraObjectModel = PROP_BUCKET_01A
			
			IF (eScene = PR_SCENE_F0_CLEANCAR)
				ExtraPropOffset = <<0.6277, -2.0464, -1.0000>>
				extraPropRotation = <<0,0,0>>
			ELIF (eScene = PR_SCENE_F1_CLEANCAR)
				ExtraPropOffset = <<-0.3577, -0.8936, -0.9900>>
				extraPropRotation = <<6,0,0>>
			ENDIF
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			thisSceneExtraObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA
			eExtraObjectModel = PROP_YOGA_MAT_03
			
			eAttachBonetag = BONETAG_NULL
			ExtraPropOffset = <<-0.6700, -0.1300, 0.0050>>
			ExtraPropRotation = <<0.0000, 0.0000, 5.8800>>
			
			fDetachAnimPhase = 0.99
			thisSceneExtraObjectAction = PSOA_1_delete
			bAttachedToBuddy = TRUE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			eExtraObjectModel = PROP_TENNIS_RACK_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.99
			thisSceneExtraObjectAction = PSOA_0_detach
			bAttachedToBuddy = TRUE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			eExtraObjectModel = PROP_CONTROLLER_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = -1
			thisSceneExtraObjectAction = PSOA_NULL
			bAttachedToBuddy = TRUE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			eExtraObjectModel = PROP_ING_CAMERA_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = -1
			thisSceneExtraObjectAction = PSOA_1_delete
			bAttachedToBuddy = TRUE
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_ROUNDTABLE
//			eExtraObjectModel = V_ILEV_M_DINECHAIR
//			extraPropOffset = <<0.1044, 0.7528, 0.0077>>
//			
//			eAttachBonetag = BONETAG_NULL
//			fDetachAnimPhase = -1
//			thisSceneExtraObjectAction = PSOA_4_synchSwap
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			eExtraObjectModel = V_ILev_FH_DineEamesA		//V_Res_FH_DineEamesA
			
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.65
			thisSceneExtraObjectAction = PSOA_3_world
			
			
			RETURN TRUE
		BREAK

		CASE PR_SCENE_M6_RONBORING		//Rons chair
			eExtraObjectModel = PROP_CHAIR_06
			
			extraPropOffset = <<1977.433,3819.045,32.453>> - <<1978.0699, 3819.5640, 32.4290>>
			extraPropRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 1.0
			thisSceneExtraObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_RESTAURANT		/*wifes chair
			eExtraObjectModel = PROP_CHATEAU_CHAIR_01
			
		//	extraPropOffset = <<-115.134,365.344,112.395>> - <<-115.9200, 363.5000, 112.8857>>
			extraPropOffset = <<0.8706, 2.0176, -0.4948>>
			extraPropRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			thisSceneExtraObjectAction = PSOA_3_world
			
			RETURN TRUE*/			//table
			eExtraObjectModel = PROP_CHATEAU_TABLE_01
			
		//	extraPropOffset = <<-115.462,364.338,112.290>> - <<-115.9200, 363.5000, 112.8857>>
			extraPropOffset = <<0.2733, 0.9596, -0.6005>>
			extraPropRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			thisSceneExtraObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_IRONING
			eExtraObjectModel = Prop_CS_IRONING_BOARD

			extraPropOffset = <<2.4300, -2.7600, 0.0250 - 0.0050>>
			extraPropRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.99
			thisSceneExtraObjectAction = PSOA_0_detach
			
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F1_CLEANINGAPT
//			eExtraObjectModel = V_RES_FH_EASYCHAIR
//			
//			extraPropOffset = <<2.5800, -2.7000, 0.0000>>
//			extraPropRotation = <<0.0000, 0.0000, 0.0000>>
//			
//			eAttachBonetag = BONETAG_NULL
//			fDetachAnimPhase = 0.99
//			thisSceneExtraObjectAction = PSOA_3_world
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			eExtraObjectModel = V_RES_TT_FLUSHER
			
			extraPropOffset = <<0,0,0>>
			extraPropRotation = <<0,0,0>>
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.5	//0.11	//0.5
			thisSceneExtraObjectAction = PSOA_3_world
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_CR_DUMPSTER
//			eExtraObjectModel = Prop_DUMPSTER_01A
//
//			eAttachBonetag = BONETAG_NULL
//			fDetachAnimPhase = -1.0
//			thisSceneExtraObjectAction = PSOA_3_world
//			
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T_SMOKEMETH
//			ePropModel = P_CS_LIGHTER_01
//			eAttachBonetag = BONETAG_PH_L_HAND
//			
//			propOffset = <<0,0,0>>
//			propRotation = <<0,0,0>>
//
//			fDetachAnimPhase = 0.99
//			thisSceneObjectAction = PSOA_0_detach
//			
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP
//			eExtraObjectModel = PROP_WHISKEY_BOTTLE
//			eAttachBonetag = BONETAG_PH_L_HAND
//			
//			extraPropOffset = <<0,0,0>>
//			extraPropRotation = <<0,0,0>>
//
//			fDetachAnimPhase = 0.99
//			thisSceneExtraObjectAction = PSOA_0_detach
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_DOCKS_a
		CASE PR_SCENE_T_DOCKS_b
		CASE PR_SCENE_T_DOCKS_c
		CASE PR_SCENE_T_DOCKS_d
			eExtraObjectModel = PROP_PENCIL_01
			
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.8989
			thisSceneExtraObjectAction = PSOA_1_delete
			
			RETURN TRUE
		BREAK
		BREAK
		CASE PR_SCENE_T_SC_BAR
			eExtraObjectModel = PROP_STOOL_01

			extraPropOffset = <<0.8100, -0.3600, -1.0300>>
			extraPropRotation = <<0.0000, 0.0000, 0.0000>>

			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.65
			thisSceneExtraObjectAction = PSOA_0_detach
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_63 sInvalid
//	sInvalid = "no extra object for eScene: "
//	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
//	
//	CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ", sInvalid)
//	#ENDIF
	
	eExtraObjectModel = DUMMY_MODEL_FOR_SCRIPT
	
	extraPropOffset = <<0,0,0>>
	extraPropRotation= <<0,0,0>>
	
	eAttachBonetag = BONETAG_NULL
	fDetachAnimPhase = -1.0
	thisSceneExtraObjectAction = PSOA_NULL
	bAttachedToBuddy = FALSE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_LOOP_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		TEXT_LABEL_63 &tLoopSyncObjAnim,
		TEXT_LABEL_63 &tLoopSyncExtraObjAnim)
	tLoopSyncExtraObjAnim = ""
	
	SWITCH eScene
		CASE PR_SCENE_M7_ROUNDTABLE
			tLoopSyncObjAnim = "AROUND_THE_TABLE_SELFISH_BASE_Lap_Top"
//			tLoopSyncExtraObjAnim = "AROUND_THE_TABLE_SELFISH_BASE_Chair"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			tLoopSyncObjAnim = "001927_01_FRAS_V2_4_ON_LAPTOP_IDLE_LAPTOP"
			tLoopSyncExtraObjAnim = "001927_01_FRAS_V2_4_ON_LAPTOP_IDLE_CHAIR"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			tLoopSyncObjAnim = "loop_table"
			tLoopSyncExtraObjAnim = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_RONBORING
			tLoopSyncObjAnim = "BASE_CHAIR_01"
			tLoopSyncExtraObjAnim = "BASE_CHAIR_02"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			tLoopSyncObjAnim = ""
			tLoopSyncExtraObjAnim = ""
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			tLoopSyncObjAnim = "001839_02_MICS3_20_DROPPING_OFF_JMY_IDLE_$"
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_TRACEYEXITSCAR
//		CASE PR_SCENE_M_HOOKERCAR
//			tLoopSyncObjAnim = "001839_02_MICS3_2""0_DROPPING_OFF_JMY_IDLE_$"
//			RETURN TRUE
//		BREAK

		CASE PR_SCENE_M_PIER_a
			tLoopSyncObjAnim = "pier_lean_smoke_idle_CIG"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_GARBAGE
			tLoopSyncObjAnim = "Garbage_Idle_BAG"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_THROW_CUP
			tLoopSyncObjAnim = "THROW_CUP_LOOP_CUP"
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_GARBAGE_FOOD
//			tLoopSyncObjAnim = "LOOP_Taco"
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			tLoopSyncObjAnim = "LOOP_Taco"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			tLoopSyncObjAnim = "HIT_CUP_HAND_LOOP_CUP"
			RETURN TRUE
		BREAK

		CASE PR_SCENE_T_SC_BAR
			tLoopSyncObjAnim = "LOOP_Beer"
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_GUITARBEATDOWN
			tLoopSyncObjAnim = "001370_02_TRVS_8_GUITAR_BEATDOWN_IDLE_GUITAR"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			tLoopSyncObjAnim = "BEAR_IN_FLOYDS_FACE_LOOP_rasp"
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_FLOYD_DOLL
//			tLoopSyncObjAnim = "BEAR_FLOYDS_FACE_SMELL_LOOP_doll"
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			tLoopSyncObjAnim = "Pineapple_LOOP_PINEAPPLE"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			tLoopSyncObjAnim = "002057_03_TRVS_27_FLUSHES_FOOT_IDLE_Foot"
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_SAVEHOUSE1_b
			tLoopSyncObjAnim = "IDLE_CHAIR"
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		TEXT_LABEL_63 &tExitSyncObjAnim,
		TEXT_LABEL_63 &tExitSyncExtraObjAnim, INT &iFlags)
		
	iFlags = 0
	tExitSyncExtraObjAnim = ""
	
	SWITCH eScene
		CASE PR_SCENE_F_MD_KUSH_DOC
			tExitSyncObjAnim = "002110_04_MAGD_3_WEED_EXCHANGE_WEEDBOTTLE"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			tExitSyncObjAnim = "AROUND_THE_TABLE_SELFISH_Lap_Top"
//			tExitSyncExtraObjAnim = "AROUND_THE_TABLE_SELFISH_Chair"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			tExitSyncObjAnim = "001927_01_FRAS_V2_4_ON_LAPTOP_EXIT_LAPTOP"
			tExitSyncExtraObjAnim = "001927_01_FRAS_V2_4_ON_LAPTOP_EXIT_CHAIR"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			tExitSyncObjAnim = "exit_table"
			tExitSyncExtraObjAnim = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_RONBORING
			tExitSyncObjAnim = "RONEX_IG5_P2_CHAIR_01"
			tExitSyncExtraObjAnim = "RONEX_IG5_P2_CHAIR_02"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			tExitSyncObjAnim = "001510_02_GC_MICS3_IG_1_EXIT_CHAIR"
			tExitSyncExtraObjAnim = ""	//"001510_02_GC_MICS3_IG_1_EXIT_CHAIR_2"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			tExitSyncObjAnim = "001839_02_MICS3_20_DROPPING_OFF_JMY_EXIT_$"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
//		CASE PR_SCENE_M_HOOKERCAR
			tExitSyncObjAnim = "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_CASH"
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_GUITARBEATDOWN
			tExitSyncObjAnim = "001370_02_TRVS_8_GUITAR_BEATDOWN_EXIT_GUITAR"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			tExitSyncObjAnim = "BEAR_IN_FLOYDS_FACE_EXIT_rasp"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL
			tExitSyncObjAnim = "BEAR_FLOYDS_FACE_SMELL_EXIT_doll"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			tExitSyncObjAnim = "Pineapple_EXIT_PINEAPPLE"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			tExitSyncObjAnim = "002057_03_TRVS_27_FLUSHES_FOOT_EXIT_Foot"
			RETURN TRUE
		BREAK

		CASE PR_SCENE_M_PIER_a
			tExitSyncObjAnim = "pier_lean_smoke_outro_CIG"
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_GARBAGE_FOOD
//			tExitSyncObjAnim = "EXIT_Taco"
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			tExitSyncObjAnim = "EXIT_Taco"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_GARBAGE
			tExitSyncObjAnim = "Garbage_Toss_BAG"
			iFlags = ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_THROW_CUP
			tExitSyncObjAnim = "THROW_CUP_EXIT_CUP"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			tExitSyncObjAnim = "HIT_CUP_HAND_EXIT_CUP"
			RETURN TRUE
		BREAK

		CASE PR_SCENE_T_SC_BAR
			tExitSyncObjAnim = "EXIT_Beer"
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M7_READSCRIPT
			tExitSyncObjAnim = "001404_01_MICS3_16_READS_SCRIPT_EXIT_PROP"
			RETURN TRUE
		BREAK

//		CASE PR_SCENE_T6_BLOWSHITUP
//			tExitSyncObjAnim = "GRENADE_THROWING_grenade"
//			RETURN TRUE
//		BREAK
		
		CASE PR_SCENE_M2_SAVEHOUSE1_b
			tExitSyncObjAnim = "EXIT_FORWARD_CHAIR"
			RETURN TRUE
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(OBJECT_INDEX &ObjectIndex, enumPlayerSceneObjectAction thisSceneObjectAction)
	
	
	PED_REQUEST_SCENE_ENUM eScene = Get_Player_Timetable_Scene_In_Progress()
	
	SWITCH thisSceneObjectAction
		CASE PSOA_0_detach
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_0_detach ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			IF IS_ENTITY_ATTACHED(ObjectIndex)
				DETACH_ENTITY(ObjectIndex)
			ENDIF
			
			IF (GET_ENTITY_MODEL(ObjectIndex) <> Prop_CS_IRONING_BOARD)
			AND (GET_ENTITY_MODEL(ObjectIndex) <> PROP_IRON_01)
				FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)
				SET_ENTITY_DYNAMIC(ObjectIndex, TRUE)
				
//				FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)
//				APPLY_FORCE_TO_ENTITY(ObjectIndex, APPLY_TYPE_FORCE, <<0,0,-0.001>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
			ENDIF
			
			IF (GET_ENTITY_MODEL(ObjectIndex) = PROP_IRON_01)
				VECTOR vIronPos, vIronRot
				vIronPos = GET_ENTITY_COORDS(ObjectIndex)
				vIronRot = GET_ENTITY_ROTATION(ObjectIndex)
				
				SET_ENTITY_COORDS(ObjectIndex, vIronPos+<<0,0,0.1>>)
				SET_ENTITY_ROTATION(ObjectIndex, <<vIronRot.x,vIronRot.y,vIronRot.z>>)
			ENDIF
			
			IF (GET_ENTITY_MODEL(ObjectIndex) <> P_DEFILIED_RAGDOLL_01_S)
				SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE PSOA_1_delete
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_1_delete ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			DELETE_OBJECT(ObjectIndex)
			
			RETURN TRUE
		BREAK
		CASE PSOA_2_synched
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_2_synched ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			STOP_SYNCHRONIZED_ENTITY_ANIM(ObjectIndex, NORMAL_BLEND_OUT, TRUE)
			
			// do nothing...
			IF IS_ENTITY_ATTACHED(ObjectIndex)
				DETACH_ENTITY(ObjectIndex)
			ENDIF
			FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)
			SET_ENTITY_DYNAMIC(ObjectIndex, TRUE)
			
			SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
			ObjectIndex = NULL
			
			RETURN TRUE
		BREAK
		CASE PSOA_3_world
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_3_world ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			// do nothing...
			IF IS_ENTITY_ATTACHED(ObjectIndex)
				DETACH_ENTITY(ObjectIndex)
			ENDIF
			
			IF (GET_ENTITY_MODEL(ObjectIndex) <> PROP_CHATEAU_TABLE_01)
			AND (GET_ENTITY_MODEL(ObjectIndex) <> V_RES_TT_FLUSHER)
			AND (GET_ENTITY_MODEL(ObjectIndex) <> V_ILev_fh_lampA_on)
			AND (GET_ENTITY_MODEL(ObjectIndex) <> PROP_CS_REMOTE_01)
				STOP_SYNCHRONIZED_ENTITY_ANIM(ObjectIndex, NORMAL_BLEND_OUT, TRUE)
			ENDIF
			
			IF (GET_ENTITY_MODEL(ObjectIndex) <> V_ILev_FH_DineEamesA)
			AND (GET_ENTITY_MODEL(ObjectIndex) <> V_RES_TT_FLUSHER)
			AND (GET_ENTITY_MODEL(ObjectIndex) <> V_ILev_fh_lampA_on)	//V_ILev_fh_lampA_on)
				FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)
			ENDIF
			
			SET_ENTITY_DYNAMIC(ObjectIndex, TRUE)
			
			SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
			
			RETURN TRUE
		BREAK
		CASE PSOA_4_synchSwap
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_4_synchSwap ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			VECTOR v4ObjectCoord, v4ObjectRot
			v4ObjectCoord = GET_ENTITY_COORDS(ObjectIndex)
			v4ObjectRot = GET_ENTITY_ROTATION(ObjectIndex)
			
			MODEL_NAMES e4ObjectSwapModel
			e4ObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
			SWITCH GET_ENTITY_MODEL(ObjectIndex)
				CASE P_Laptop_02_S
					e4ObjectSwapModel = PROP_LAPTOP_02_CLOSED
				BREAK
				CASE V_ILEV_M_DINECHAIR
					e4ObjectSwapModel = P_DINECHAIR_01_S
				BREAK
				CASE PROP_ACC_GUITAR_01
					e4ObjectSwapModel = Prop_ACC_Guitar_01_D1
				BREAK
				CASE P_DEFILIED_RAGDOLL_01_S
					e4ObjectSwapModel = PROP_DEFILIED_RAGDOLL_01
				BREAK
				CASE P_HAND_TOILET_S
					e4ObjectSwapModel = Prop_ToiletFoot_Static
				BREAK
				
				CASE PROP_LAPTOP_02_CLOSED
				CASE P_DINECHAIR_01_S
				CASE Prop_ACC_Guitar_01_D1
				CASE PROP_DEFILIED_RAGDOLL_01
					STOP_SYNCHRONIZED_ENTITY_ANIM(ObjectIndex, NORMAL_BLEND_OUT, TRUE)
					
					RETURN TRUE
				BREAK
				CASE Prop_ToiletFoot_Static
					
					VECTOR vProp_ToiletFoot_Static_Coord, vProp_ToiletFoot_Static_Rot
					vProp_ToiletFoot_Static_Coord = <<1971.6689, 3819.4490, 32.7660>>
					vProp_ToiletFoot_Static_Rot = <<0.0000, 0.0000, 53.6000>>
					
					SET_ENTITY_COORDS(ObjectIndex, vProp_ToiletFoot_Static_Coord)
					SET_ENTITY_ROTATION(ObjectIndex, vProp_ToiletFoot_Static_Rot)
					
					
//					START_WIDGET_Group("Prop_ToiletFoot_Static")
//						ADD_WIDGET_VECTOR_SLIDER("vProp_ToiletFoot_Static_Coord", vProp_ToiletFoot_Static_Coord, -4000, 4000, 0.001)
//						ADD_WIDGET_VECTOR_SLIDER("vProp_ToiletFoot_Static_Rot", vProp_ToiletFoot_Static_Rot, -180, 180, 0.1)
//					STOP_WIDGET_Group()
//					WHILE DOES_ENTITY_EXIST(ObjectIndex)
//						SET_ENTITY_COORDS(ObjectIndex, vProp_ToiletFoot_Static_Coord)
//						SET_ENTITY_ROTATION(ObjectIndex, vProp_ToiletFoot_Static_Rot)
//						
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
//							
//							SAVE_STRING_TO_DEBUG_FILE("vProp_ToiletFoot_Static_Coord = ")
//							SAVE_VECTOR_TO_DEBUG_FILE(vProp_ToiletFoot_Static_Coord)
//							SAVE_NEWLINE_TO_DEBUG_FILE()
//							SAVE_STRING_TO_DEBUG_FILE("vProp_ToiletFoot_Static_Rot = ")
//							SAVE_VECTOR_TO_DEBUG_FILE(vProp_ToiletFoot_Static_Rot)
//							SAVE_NEWLINE_TO_DEBUG_FILE()
//							SAVE_NEWLINE_TO_DEBUG_FILE()
//							
//							FREEZE_ENTITY_POSITION(ObjectIndex, TRUE)
//							RETURN TRUE
//						ENDIF
//						
//						WAIT(0)
//					ENDWHILE
					
					FREEZE_ENTITY_POSITION(ObjectIndex, TRUE)
					RETURN TRUE
				BREAK
				
				DEFAULT
					e4ObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
					SCRIPT_ASSERT("invalid swap model (4)")
					RETURN FALSE
				BREAK
				
			ENDSWITCH
			
			REQUEST_MODEL(e4ObjectSwapModel)
			WHILE NOT HAS_MODEL_LOADED(e4ObjectSwapModel)
				// Hide the HUD
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				CLEAR_REMINDER_MESSAGE()
				
				IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			// Hide the HUD
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			CLEAR_REMINDER_MESSAGE()
			
			IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
				DISABLE_CELLPHONE(TRUE)
			ENDIF
			
			
			DELETE_OBJECT(ObjectIndex)
			ObjectIndex = CREATE_OBJECT(e4ObjectSwapModel, v4ObjectCoord)
			SET_ENTITY_ROTATION(ObjectIndex, v4ObjectRot)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(e4ObjectSwapModel)
		//	SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
			
			RETURN TRUE
		BREAK
		CASE PSOA_5_explode
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_5_explode ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			ADD_EXPLOSION(GET_ENTITY_COORDS(ObjectIndex),EXP_TAG_GRENADE)
			DELETE_OBJECT(ObjectIndex)
			
			RETURN TRUE
		BREAK
		CASE PSOA_6_keepHide
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_6_keepHide ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
//			IF IS_ENTITY_ATTACHED(ObjectIndex)
//				DETACH_ENTITY(ObjectIndex)
//			ENDIF
			
//			FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)
//			APPLY_FORCE_TO_ENTITY(ObjectIndex, APPLY_TYPE_FORCE, <<0,0,-0.001>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
			
//			SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
			
			RETURN TRUE
		BREAK
		CASE PSOA_7_weaponSwap
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_7_weaponSwap ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			IF (GET_ENTITY_MODEL(ObjectIndex) = GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL))
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 30)
					ENDIF
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
				ENDIF
				
				DELETE_OBJECT(ObjectIndex)
				RETURN TRUE
			ELSE
				SCRIPT_ASSERT("missing prop details???")
				DELETE_OBJECT(ObjectIndex)
				RETURN FALSE
			ENDIF
			
		BREAK
		CASE PSOA_8_ptfxSwap
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_8_ptfxSwap ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			VECTOR v8ObjectCoord, v8ObjectRot
			v8ObjectCoord = GET_ENTITY_COORDS(ObjectIndex)
			v8ObjectRot = GET_ENTITY_ROTATION(ObjectIndex)
			
			STRING fx8Name
			fx8Name = ""
			SWITCH GET_ENTITY_MODEL(ObjectIndex)
				CASE PROP_CS_BEER_BOT_01
					fx8Name = "scr_pts_glass_bottle"
				BREAK
				CASE PROP_PINEAPPLE
					fx8Name = "ent_dst_pineapple"
				BREAK
				CASE PROP_ACC_GUITAR_01
					fx8Name = "scr_pts_guitar_break"
				BREAK
				DEFAULT
					fx8Name = ""
					SCRIPT_ASSERT("invalid ptfx model (8)")
					RETURN FALSE
				BREAK
			ENDSWITCH
			
			REQUEST_PTFX_ASSET()
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				
					
				CPRINTLN(DEBUG_SWITCH, "HAS_PTFX_ASSET_LOADED()")
				
				// Hide the HUD
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				CLEAR_REMINDER_MESSAGE()
				
				IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			// Hide the HUD
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			CLEAR_REMINDER_MESSAGE()
			
			IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
				DISABLE_CELLPHONE(TRUE)
			ENDIF
			
			START_PARTICLE_FX_NON_LOOPED_AT_COORD(fx8Name, v8ObjectCoord, v8ObjectRot)
			
			DELETE_OBJECT(ObjectIndex)
			RETURN TRUE
		BREAK
		CASE PSOA_9_synchAndPtfxSwap
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " * PSOA_9_synchAndPtfxSwap ", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ObjectIndex)))
			#ENDIF
			
			VECTOR v9ObjectCoord, v9ObjectRot
			v9ObjectCoord = GET_ENTITY_COORDS(ObjectIndex)
			v9ObjectRot = GET_ENTITY_ROTATION(ObjectIndex)
			
			STRING fx9Name
			fx9Name = ""
			SWITCH GET_ENTITY_MODEL(ObjectIndex)
				CASE PROP_CS_BEER_BOT_01
					fx9Name = "scr_pts_glass_bottle"
				BREAK
				CASE PROP_PINEAPPLE
					fx9Name = "ent_dst_pineapple"
				BREAK
				CASE PROP_ACC_GUITAR_01
				CASE Prop_ACC_Guitar_01_D1
					fx9Name = "scr_pts_guitar_break"
				BREAK
				DEFAULT
					fx9Name = ""
					SCRIPT_ASSERT("invalid ptfx model (9)")
					RETURN FALSE
				BREAK
			ENDSWITCH
			
			REQUEST_PTFX_ASSET()
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				
					
				CPRINTLN(DEBUG_SWITCH, "HAS_PTFX_ASSET_LOADED()")
				
				// Hide the HUD
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				CLEAR_REMINDER_MESSAGE()
				
				IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			MODEL_NAMES e9ObjectSwapModel
			e9ObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
			SWITCH GET_ENTITY_MODEL(ObjectIndex)
				
				CASE Prop_ACC_Guitar_01_D1
					RETURN FALSE
				BREAK
				
				CASE P_Laptop_02_S
					e9ObjectSwapModel = PROP_LAPTOP_02_CLOSED
				BREAK
				CASE V_ILEV_M_DINECHAIR
					e9ObjectSwapModel = P_DINECHAIR_01_S
				BREAK
				CASE PROP_ACC_GUITAR_01
					e9ObjectSwapModel = Prop_ACC_Guitar_01_D1
				BREAK
				CASE P_DEFILIED_RAGDOLL_01_S
					e9ObjectSwapModel = PROP_DEFILIED_RAGDOLL_01
				BREAK
				DEFAULT
					e9ObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
					SCRIPT_ASSERT("invalid swap model (9)")
					RETURN FALSE
				BREAK
			ENDSWITCH
			
			REQUEST_MODEL(e9ObjectSwapModel)
			WHILE NOT HAS_MODEL_LOADED(e9ObjectSwapModel)
				// Hide the HUD
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				CLEAR_REMINDER_MESSAGE()
				
				IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			// Hide the HUD
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			CLEAR_REMINDER_MESSAGE()
			
			IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
				DISABLE_CELLPHONE(TRUE)
			ENDIF
			
			START_PARTICLE_FX_NON_LOOPED_AT_COORD(fx9Name, v9ObjectCoord, v9ObjectRot)
			
			DELETE_OBJECT(ObjectIndex)
			ObjectIndex = CREATE_OBJECT(e9ObjectSwapModel, v9ObjectCoord)
			SET_ENTITY_ROTATION(ObjectIndex, v9ObjectRot)
			
			TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
			INT iFlags
			IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_EXIT_SCENE(eScene, tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject, iFlags)
				
				TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
				ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
				//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
				
				IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(eScene,
						tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
						playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
					IF NOT IS_ENTITY_PLAYING_ANIM(ObjectIndex,
								tPlayerSceneSyncAnimDict,
								tPlayerSceneSyncObject, ANIM_SYNCED_SCENE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(ObjectIndex,
								g_iPlayer_Timetable_Exit_SynchSceneID,
								tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
								INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iFlags)
						
						
						CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM")
					ELSE
						CPRINTLN(DEBUG_SWITCH, "is playing anim")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SWITCH, "no anim dict")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SWITCH, "no object anim")
			ENDIF

			SET_MODEL_AS_NO_LONGER_NEEDED(e9ObjectSwapModel)
	//		SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL GET_DOORS_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		MODEL_NAMES &eDoorModel_L, MODEL_NAMES &eDoorModel_R,
		VECTOR &vDoorOffset_L, VECTOR &vDoorOffset_R,
		DOOR_HASH_ENUM &eDoorHash_L, DOOR_HASH_ENUM &eDoorHash_R,
		BOOL &bReplaceDoor, FLOAT &fHideDoorRadius)
	
	eDoorHash_L = DUMMY_DOORHASH
	eDoorHash_R = DUMMY_DOORHASH
	
	vDoorOffset_L = <<0,0,0>>
	vDoorOffset_R= <<0,0,0>>
	
	SWITCH eScene
//		CASE PR_SCENE_M4_DOORSTUMBLE
//			eDoorModel_L = PROP_HW1_04_DOOR_L1
//			eDoorModel_R = PROP_HW1_04_DOOR_R1
//			
//			bReplaceDoor = TRUE
//			fHideDoorRadius = 5.0
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M7_EXITBARBER
			eDoorModel_L = V_ILEV_HD_DOOR_L			vDoorOffset_L = <<-2.3267, -0.7259, 0.2500>>
			eDoorModel_R = V_ILEV_HD_DOOR_R			vDoorOffset_R = <<-1.5708, -2.0352, 0.2500>>
			
			eDoorHash_L = DOORHASH_HAIR_SALON_L
			eDoorHash_R = DOORHASH_HAIR_SALON_R
			
			bReplaceDoor = TRUE
			fHideDoorRadius = 5.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP
			eDoorModel_L = V_ILEV_CH_GLASSDOOR		vDoorOffset_L = <<GET_RANDOM_FLOAT_IN_RANGE(-3,3),GET_RANDOM_FLOAT_IN_RANGE(-3,3),0>>
			eDoorModel_R = V_ILEV_CH_GLASSDOOR		vDoorOffset_R = <<GET_RANDOM_FLOAT_IN_RANGE(-3,3),GET_RANDOM_FLOAT_IN_RANGE(-3,3),0>>
			
			eDoorHash_L = DUMMY_DOORHASH
			eDoorHash_R = DUMMY_DOORHASH
	
			bReplaceDoor = TRUE
			fHideDoorRadius = 5.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a
		CASE PR_SCENE_M7_BYESOLOMON_b
			eDoorModel_L = prop_ss1_10_door_l		vDoorOffset_L = <<-720.393,256.862,80.290>> - <<-718.8135, 256.7636, 79.8384>>
			eDoorModel_R = prop_ss1_10_door_r		vDoorOffset_R = <<-718.417,257.789,80.290>> - <<-718.8135, 256.7636, 79.8384>>
			
			eDoorHash_L = DUMMY_DOORHASH
			eDoorHash_R = DUMMY_DOORHASH
	
			bReplaceDoor = FALSE
			fHideDoorRadius = 2.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			eDoorModel_L = PROP_PATIO_LOUNGER1		vDoorOffset_L = <<-1353.544,356.114,63.084>>	- <<-1353.7910, 355.1845, 64.0800>>
//			eDoorModel_R = PROP_PATIO_LOUNGER1		vDoorOffset_R = <<-1352.644,357.697,63.082>>	- <<-1353.7910, 355.1845, 64.0800>>
			
			eDoorHash_L = DUMMY_DOORHASH
			eDoorHash_R = DUMMY_DOORHASH
	
			bReplaceDoor = FALSE
			fHideDoorRadius = 1.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
			eDoorModel_R = V_ILEV_M_DINECHAIR		vDoorOffset_R = <<-796.655,181.225,71.836>>	- <<-796.7593, 180.4725, 71.8266>>
			
			eDoorHash_R = DUMMY_DOORHASH
	
			bReplaceDoor = FALSE
			fHideDoorRadius = -1		//grab from the world instead of hide
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT
			eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
			eDoorModel_R = v_ilev_fa_frontdoor		vDoorOffset_R = <<-14.869,-1441.182,31.193>>	- <<-14.8689, -1441.1821, 31.1932>>
			
			eDoorHash_R = DOORHASH_F_HOUSE_SC_F
	
			bReplaceDoor = TRUE
			fHideDoorRadius = 5.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_LINGERIE
			eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
			eDoorModel_R = Prop_HW1_23_Door
			
			eDoorHash_R = DUMMY_DOORHASH
	
			bReplaceDoor = TRUE
			fHideDoorRadius = 5.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_CHASE
			eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
			eDoorModel_R = PROP_STRIP_DOOR_01		vDoorOffset_R = <<127.941,-1298.486,29.420>>	- <<127.957,-1298.513,29.420>>
			
			eDoorHash_R = DOORHASH_STRIPCLUB_F
	
			bReplaceDoor = TRUE
			fHideDoorRadius = 5.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			eDoorModel_L = V_ILEV_MM_DOORM_L		vDoorOffset_L = <<1.0340, 9.0780, 2.2874>>
			eDoorModel_R = V_ILEV_MM_DOORM_R		vDoorOffset_R = <<1.6432, 7.4908, 2.2874>>
			
			eDoorHash_L = DOORHASH_M_MANSION_F_L
			eDoorHash_R = DOORHASH_M_MANSION_F_R
			
			bReplaceDoor = TRUE
			fHideDoorRadius = 5.0
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_SNACKING
			eDoorModel_L = V_ILEV_FH_KITCHENSTOOL	vDoorOffset_L = <<0.8976*2, 0.3148*2, -0.9993>>
			eDoorModel_R = DUMMY_MODEL_FOR_SCRIPT
			
			eDoorHash_L = DUMMY_DOORHASH
	
			bReplaceDoor = TRUE
			fHideDoorRadius = -1		//grab from the world instead of hide
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			eDoorModel_L = PROP_CHATEAU_CHAIR_01	vDoorOffset_L = <<0.5,0.5,0.5>>
			eDoorModel_R = PROP_CHATEAU_CHAIR_01	vDoorOffset_R = vDoorOffset_L*-1.0
			
			eDoorHash_L = DUMMY_DOORHASH
			eDoorHash_R = DUMMY_DOORHASH
	
			bReplaceDoor = TRUE
			fHideDoorRadius = 0.5
			RETURN TRUE
		BREAK

	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_63 sInvalid
//	sInvalid = "no Doors for eScene: "
//	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
//	
//	CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ", sInvalid)
//	#ENDIF
	
	eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
	eDoorModel_R = DUMMY_MODEL_FOR_SCRIPT
	
	vDoorOffset_L = <<0,0,0>>
	vDoorOffset_R = <<0,0,0>>
	
	fHideDoorRadius = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SYNCHRONIZED_DOOR_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tExitSyncDoorAnimL, TEXT_LABEL_63 &tExitSyncDoorAnimR)
	
	tExitSyncDoorAnimL = ""
	tExitSyncDoorAnimR = ""
	
	SWITCH eScene
//		CASE PR_SCENE_M4_DOORSTUMBLE
//			tExitSyncDoorAnimL = "000610_03_MICS2_4_STUMBLES_THROUGH_DOORS_EXIT_L_DOOR"
//			tExitSyncDoorAnimR = "000610_03_MICS2_4_STUMBLES_THROUGH_DOORS_EXIT_R_DOOR"
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M7_EXITBARBER
			tExitSyncDoorAnimL = "001406_01_MICS3_7_EXITS_BARBER_EXIT_L_DOOR"
			tExitSyncDoorAnimR = "001406_01_MICS3_7_EXITS_BARBER_EXIT_R_DOOR"
			
//SWITCH@/MICHAEL@/EXITS_BARBER/001406_01_MICS3_7_EXITS_BARBER_IDLE_L_DOOR.anim #0/1 <binary>
//SWITCH@/MICHAEL@/EXITS_BARBER/001406_01_MICS3_7_EXITS_BARBER_IDLE_R_DOOR.anim #0/1 <binary>
			
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP
			tExitSyncDoorAnimL = "001405_01_MICS3_8_EXITS_FANCYSHOP_EXIT_L_DOOR"
			tExitSyncDoorAnimR = "001405_01_MICS3_8_EXITS_FANCYSHOP_EXIT_R_DOOR"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			tExitSyncDoorAnimL = "001523_01_MICS3_9_LOUNGE_CHAIRS_EXIT_Lounger_01"
			tExitSyncDoorAnimR = "001523_01_MICS3_9_LOUNGE_CHAIRS_EXIT_Lounger_02"
			
//SWITCH@\MICHAEL@\LOUNGE_CHAIRS\001523_01_MICS3_9_LOUNGE_CHAIRS_IDLE_Lounger_01.anim
//SWITCH@\MICHAEL@\LOUNGE_CHAIRS\001523_01_MICS3_9_LOUNGE_CHAIRS_IDLE_Lounger_02.anim
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M7_ROUNDTABLE
			tExitSyncDoorAnimL = ""
			tExitSyncDoorAnimR = "AROUND_THE_TABLE_SELFISH_Chair"

//AROUND_THE_TABLE_SELFISH_BASE_Chair

			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT
			tExitSyncDoorAnimL = ""
			tExitSyncDoorAnimR = "Tanisha_Argue_DOOR"

			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_CR_LINGERIE
			tExitSyncDoorAnimL = ""
			tExitSyncDoorAnimR = "trev_exit_lingerie_shop_outro_DOOR"
			
//SWITCH@TREVOR@LINGERIE_SHOP/trev_exit_lingerie_shop_idle_DOOR.anim

			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_CHASE
			tExitSyncDoorAnimL = ""
			tExitSyncDoorAnimR = "EXIT_DOOR"
			
//SWITCH@TREVOR@CHASE_STRIPPERS/LOOP_DOOR.anim

			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			tExitSyncDoorAnimL = "EXIT_LEFT_DOOR"
			tExitSyncDoorAnimR = "EXIT_RIGHT_DOOR"
			
//SWITCH@/MICHAEL@/BIKING_WITH_JIMMY/LOOP_LEFT_DOOR.anim
//SWITCH@/MICHAEL@/BIKING_WITH_JIMMY/LOOP_RIGHT_DOOR.anim
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_SNACKING
			tExitSyncDoorAnimL = "001922_01_FRAS_V2_3_SNACKING_EXIT_STOOL"
			tExitSyncDoorAnimR = ""
			
//SWITCH@FRANKLIN@SNACKING/001922_01_FRAS_V2_3_SNACKING_IDLE_STOOL.anim
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			tExitSyncDoorAnimL = "exit_chair_02"
			tExitSyncDoorAnimR = "exit_chair_01"
			
//tLoopSyncObjAnim = "loop_chair_01"
//tLoopSyncExtraObjAnim = "loop_chair_02"
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
