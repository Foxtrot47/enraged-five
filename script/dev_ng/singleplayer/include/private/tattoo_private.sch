//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   tattoo_private.sch                                       	//
//      AUTHOR          :   Kenneth Ross                                                //
//      DESCRIPTION     :   Data file for all the SP and MP tattoos						//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_extrametadata.sch"

#IF USE_TU_CHANGES
//These are used to prevent tattoos from overlapping each other, only used for MP 
ENUM TATTOO_GROUP_ENUM
	TG_ARM_LEFT_FULL_SLEEVE = 0,	//	Arm Left Full - Sleeve, TP_ARM_LEFT_SLEEVE, GROUP 20
	TG_ARM_LEFT_SHORT_SLEEVE,		// Arm Left 3/4 - Sleeve , TP_ARM_LEFT_SHORT_SLEEVE	
	TG_ARM_LEFT_LOWER_SLEEVE,		// Arm Left Lower - Sleeve , TP_ARM_LEFT_1, GROUP 11
	TG_ARM_LEFT_LOWER_INNER,		// Arm Left Lower - Inner, TP_ARM_LEFT_LOWER_INNER	
	TG_ARM_LEFT_LOWER_OUTER,		// Arm Left Lower - Outer, TP_ARM_LEFT_LOWER_OUTER	
	TG_ARM_LEFT_WRIST,				// Arm Left Wrist, TP_ARM_LEFT_WRIST, GROUP 44
	TG_ARM_LEFT_UPPER_SLEEVE,		// Arm Left Upper - Sleeve, TP_ARM_LEFT_2, GROUP 12
	TG_ARM_LEFT_UPPER_TRICEP,		// Arm Left Upper - Tricep, TP_ARM_LEFT_TRICEP	
	TG_ARM_LEFT_UPPER_SIDE,			// Arm Left Upper  - Side			
	TG_ARM_LEFT_UPPER_BICEP,		// Arm Left Upper  - Bicep, TP_ARM_LEFT_BICEP, GROUP 42
	TG_ARM_LEFT_SHOULDER,			// Arm Left Shoulder, TP_ARM_LEFT_SHOULDER, GROUP 43
	TG_ARM_LEFT_ELBOW,				// Arm Left Elbow, TP_ARM_LEFT_ELBOW, GROUP 39
	TG_ARM_RIGHT_FULL_SLEEVE,		// Arm Right Full - Sleeve, TP_ARM_RIGHT_SLEEVE, GROUP 22
	TG_ARM_RIGHT_SHORT_SLEEVE,		// Arm Right 3/4 - Sleeve , TP_ARM_RIGHT_SHORT_SLEEVE	
	TG_ARM_RIGHT_LOWER_SLEEVE,		// Arm Right Lower - Sleeve , TP_ARM_RIGHT_LOWER, GROUP 21
	TG_ARM_RIGHT_LOWER_INNER,		// Arm Right Lower - Inner, TP_ARM_RIGHT_LOWER_INNER	
	TG_ARM_RIGHT_LOWER_OUTER,		// Arm Right Lower - Outer, TP_ARM_RIGHT_LOWER_OUTER	
	TG_ARM_RIGHT_WRIST,				// Arm Right Wrist, TP_ARM_RIGHT_WRIST, GROUP 47
	TG_ARM_RIGHT_UPPER_SLEEVE,		// Arm Right Upper - Sleeve, TP_ARM_RIGHT, GROUP 13
	TG_ARM_RIGHT_UPPER_TRICEP,		// Arm Right Upper - Tricep, TP_ARM_RIGHT_TRICEP, GROUP 45
	TG_ARM_RIGHT_UPPER_SIDE,		// Arm Right Upper  - Side			
	TG_ARM_RIGHT_UPPER_BICEP,		// Arm Right Upper  - Bicep, TP_ARM_RIGHT_BICEP	
	TG_ARM_RIGHT_SHOULDER,		// Arm Right Shoulder, TP_ARM_RIGHT_SHOULDER, GROUP 46
	TG_ARM_RIGHT_ELBOW,		// Arm Right Elbow, TP_ARM_RIGHT_ELBOW, GROUP 48
	TG_HAND_LEFT,		// Hand Left, TP_HAND_LEFT, GROUP 35
	TG_HAND_RIGHT,		// Hand Right, TP_HAND_RIGHT, GROUP 36
	TG_BACK_FULL,		// Back Full, TP_BACK_1, GROUP 2
	TG_BACK_FULL_ARMS_FULL_BACK,		// XXXXXXXXXX
	TG_BACK_FULL_SHORT,		// Back 3/4 Full, TP_BACK_MID, GROUP 10
	TG_BACK_MID,		// Back Middle			
	TG_BACK_UPPER,		// Back Upper			
	TG_BACK_UPPER_LEFT,		// Back Upper Left, TP_BACK_3, GROUP 25
	TG_BACK_UPPER_RIGHT,		// Back Upper Right, TP_BACK_4, GROUP 26
	TG_BACK_LOWER,		// Back Lower, TP_BACK_2, GROUP 3
	TG_BACK_LOWER_LEFT,		// Back Lower Left, TP_BACK_LEFT, GROUP 40
	TG_BACK_LOWER_MID,		// Back Lower Middle, TP_BACK_BOTTOM, GROUP 49
	TG_BACK_LOWER_RIGHT,		// Back Lower Right			
	TG_CHEST_FULL,			// Chest Full, TP_CHEST_FULL, GROUP 24
	TG_CHEST_STOM,			// Chest Mid & Stomach Upper, TP_CHEST_STOM, GROUP 34
	TG_CHEST_STOM_FULL,			// Chest Mid & Stomach Upper, TP_CHEST_STOM, GROUP 34
	TG_CHEST_LEFT,			// Chest Left, TP_CHEST_LEFT, GROUP 4
	TG_CHEST_UPPER_LEFT,		// Chest Left Upper, TP_CHEST_LEFT_UPPER, GROUP 29
	TG_CHEST_RIGHT,		// Chest Right, TP_CHEST_RIGHT, GROUP 5
	TG_CHEST_UPPER_RIGHT,		// Chest Right Upper, TP_CHEST_RIGHT_UPPER, GROUP 27
	TG_CHEST_MID,		// Chest Upper, TP_CHEST_MID, GROUP 1
	TG_TORSO_SIDE_RIGHT,		// Torso Side Right, TP_SIDE_RIGHT, GROUP 9
	TG_TORSO_SIDE_LEFT,		// Torso Side Left, TP_SIDE_LEFT, GROUP 50
	TG_STOMACH_FULL,		// Stomach Full, TP_STOMACH_FULL, GROUP 28
	TG_STOMACH_UPPER,		// Stomach Upper, TP_STOMACH_MID, GROUP 8
	TG_STOMACH_UPPER_LEFT,		// Stomach Upper Left			
	TG_STOMACH_UPPER_RIGHT,		// Stomach Upper Right			
	TG_STOMACH_LOWER,		// Stomach Lower			
	TG_STOMACH_LOWER_LEFT,		// Stomach Lower Left, TP_STOMACH_LEFT, GROUP 7
	TG_STOMACH_LOWER_RIGHT,		// Stomach Lower Right, TP_STOMACH_RIGHT, GROUP 6
	TG_STOMACH_LEFT,		// Stomach Lower Left, TP_STOMACH_LEFT, GROUP 7
	TG_STOMACH_RIGHT,		// Stomach Lower Right, TP_STOMACH_RIGHT, GROUP 6
	TG_FACE,		// Face, TP_FACE, GROUP 19
	TG_HEAD_LEFT,		// Head Left, TP_HEAD_LEFT, GROUP 30
	TG_HEAD_RIGHT,		// Head Right, TP_HEAD_RIGHT, GROUP 31
	TG_NECK_FRONT,		// Neck Front, TP_NECK_FRONT, GROUP 32
	TG_NECK_BACK,		// Neck Back, TP_NECK_BACK, GROUP 33
	TG_NECK_LEFT_FULL,		// Neck Left Full, TP_NECK_LEFT_2, GROUP 51
	TG_NECK_LEFT_BACK,		// Neck Left Back, TP_NECK_LEFT, GROUP 18
	TG_NECK_RIGHT_FULL,		// Neck Right Full, TP_NECK_RIGHT_2, GROUP 17
	TG_NECK_RIGHT_BACK,		// Neck Right Back, TP_NECK_RIGHT, GROUP 16
	TG_LEG_LEFT_FULL_SLEEVE,		// Leg Left Full - Sleeve, TP_LEG_LEFT_FULL, GROUP 37
	TG_LEG_LEFT_FULL_FRONT,		// Leg Left Full - Front			
	TG_LEG_LEFT_FULL_BACK,		// Leg Left Full - Back			
	TG_LEG_LEFT_UPPER_SLEEVE,		// Leg Left Upper - Sleeve			
	TG_LEG_LEFT_UPPER_FRONT,		// Leg Left Upper - Front, TP_LEG_LEFT_UPPER, GROUP 41
	TG_LEG_LEFT_UPPER_BACK,		// Leg Left Upper - Back			
	TG_LEG_LEFT_UPPER_OUTER,		// Leg Left Upper - Outer			
	TG_LEG_LEFT_UPPER_INNER,		// Leg Left Upper - Inner			
	TG_LEG_LEFT_LOWER_SLEEVE,		// Leg Left Lower - Sleeve			
	TG_LEG_LEFT_LOWER_FRONT,		// Leg Left Lower - Front			
	TG_LEG_LEFT_LOWER_BACK,		// Leg Left Lower - Back, TP_LEG_LEFT, GROUP 15
	TG_LEG_LEFT_LOWER_OUTER,		// Leg Left Lower - Outer			
	TG_LEG_LEFT_LOWER_INNER,		// Leg Left Lower - Inner			
	TG_LEG_LEFT_KNEE,		// Leg Left Knee			
	TG_LEG_LEFT_ANKLE,		// Leg Left Ankle			
	TG_LEG_LEFT_CALF,		// Leg Left Calf			
	TG_LEG_RIGHT_FULL_SLEEVE,		// Leg Right Full - Sleeve, TP_LEG_RIGHT_2, GROUP 23
	TG_LEG_RIGHT_FULL_FRONT,		// Leg Right Full - Front			
	TG_LEG_RIGHT_FULL_BACK,		// Leg Right Full - Back			
	TG_LEG_RIGHT_UPPER_SLEEVE,		// Leg Right Upper - Sleeve, TP_LEG_RIGHT_UPPER, GROUP 38
	TG_LEG_RIGHT_UPPER_FRONT,		// Leg Right Upper - Front			
	TG_LEG_RIGHT_UPPER_BACK,		// Leg Right Upper - Back			
	TG_LEG_RIGHT_UPPER_OUTER,		// Leg Right Upper - Outer			
	TG_LEG_RIGHT_UPPER_INNER,		// Leg Right Upper - Inner			
	TG_LEG_RIGHT_LOWER_SLEEVE,		// Leg Right Lower - Sleeve, TP_LEG_RIGHT, GROUP 14
	TG_LEG_RIGHT_LOWER_FRONT,		// Leg Right Lower - Front			
	TG_LEG_RIGHT_LOWER_BACK,		// Leg Right Lower - Back			
	TG_LEG_RIGHT_LOWER_OUTER,		// Leg Right Lower - Outer			
	TG_LEG_RIGHT_LOWER_INNER,		// Leg Right Lower - Inner			
	TG_LEG_RIGHT_KNEE,		// Leg Right Knee			
	TG_LEG_RIGHT_ANKLE,		// Leg Right Ankle			
	TG_LEG_RIGHT_CALF,		// Leg Right Calf			
	TG_FOOT_LEFT,		// Foot Left			
	TG_FOOT_RIGHT,		// Foot Right
	
	
	//
	MAX_TATTOO_GROUPS,
	TG_INVALID = -1
ENDENUM
#ENDIF

FUNC BOOL IS_TATTOO_ENUM_DLC(TATTOO_NAME_ENUM eTattoo)
	#IF USE_TU_CHANGES
		RETURN (eTattoo >= TATTOO_MP_FM_DLC)
	#ENDIF
	UNUSED_PARAMETER(eTattoo)
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the enum name for a tattoo given the bitset index and bit index
FUNC TATTOO_FACTION_ENUM GET_TATTOO_FACTION_FOR_PLAYER_PED(enumCharacterList ePed)
	SWITCH ePed
		CASE CHAR_MICHAEL
			RETURN TATTOO_SP_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			RETURN TATTOO_SP_FRANKLIN
		BREAK
		
		CASE CHAR_TREVOR
			RETURN TATTOO_SP_TREVOR
		BREAK
	ENDSWITCH
	
	RETURN TATTOO_SP_MICHAEL
ENDFUNC

/// PURPOSE: Returns the enum name for a tattoo given the bitset index and bit index
FUNC TATTOO_FACTION_ENUM GET_TATTOO_FACTION_FOR_PED(PED_INDEX pedID)
	IF IS_PED_DEAD_OR_DYING(pedID)
	
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("GET_TATTOO_FACTION_FOR_PED - Ped model is unknown.")
		#ENDIF
		
		RETURN TATTOO_SP_MICHAEL
	ENDIF
	
	#IF USE_TU_CHANGES
		SWITCH GET_ENTITY_MODEL(pedID)
			CASE PLAYER_ZERO
				RETURN TATTOO_SP_MICHAEL
			BREAK
			
			CASE PLAYER_ONE
				RETURN TATTOO_SP_FRANKLIN
			BREAK
			
			CASE PLAYER_TWO
				RETURN TATTOO_SP_TREVOR
			BREAK
			
			CASE MP_M_FREEMODE_01
				RETURN TATTOO_MP_FM
			BREAK
			
			CASE MP_F_FREEMODE_01
				RETURN TATTOO_MP_FM_F
			BREAK
		ENDSWITCH
	#ENDIF
	
	#IF NOT USE_TU_CHANGES
		SWITCH GET_ENTITY_MODEL(pedID)
			CASE PLAYER_ZERO
				RETURN TATTOO_SP_MICHAEL
			BREAK
			
			CASE PLAYER_ONE
				RETURN TATTOO_SP_FRANKLIN
			BREAK
			
			CASE PLAYER_TWO
				RETURN TATTOO_SP_TREVOR
			BREAK
			
			CASE MP_M_FREEMODE_01
			CASE MP_F_FREEMODE_01
				RETURN TATTOO_MP_FM
			BREAK
		ENDSWITCH
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("GET_TATTOO_FACTION_FOR_PED - Ped model is unknown.")
	#ENDIF
	
	RETURN TATTOO_SP_MICHAEL
ENDFUNC

/// PURPOSE: Returns the enum name for a tattoo given the bitset index and bit index
FUNC TATTOO_NAME_ENUM GET_TATTOO_NAME_FROM_BITSET_INDEX(INT iBitsetIndex, INT iBitIndex)
	// Some error checking
	IF iBitsetIndex < 0 OR iBitsetIndex > MAX_NUMBER_OF_TATTOO_BITSETS-1
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_TATTOO_NAME_FROM_BITSET_INDEX() - invalid bitset specified - ", iBitsetIndex)
			SCRIPT_ASSERT("GET_TATTOO_NAME_FROM_BITSET_INDEX() - invalid bitset specified")
		#ENDIF
	ENDIF
	IF iBitIndex < 0 OR iBitIndex > 31
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_TATTOO_NAME_FROM_BITSET_INDEX() - invalid bit specified - ", iBitIndex)
			SCRIPT_ASSERT("GET_TATTOO_NAME_FROM_BITSET_INDEX() - invalid bit specified")
		#ENDIF
	ENDIF
	
	RETURN INT_TO_ENUM(TATTOO_NAME_ENUM, ((iBitsetIndex*32) + (iBitIndex)))
ENDFUNC

/// PURPOSE: Helper proc to fill the ped tattoo struct
PROC FILL_TATTOO_DATA(TATTOO_DATA_STRUCT &sTattooData, TATTOO_NAME_ENUM eTattoo, PED_INDEX pedID, STRING paramLabel, STRING paramCollection, STRING paramPreset, STRING paramUpgradeGroup, TATTOO_FACTION_ENUM paramFaction, INT paramCost, TATTOO_FACING tattooFacing = TATTOO_FRONT)

	sTattooData.eEnum = eTattoo
	sTattooData.sLabel = paramLabel
	sTattooData.iCollection = GET_HASH_KEY(paramCollection)
	sTattooData.iPreset = GET_HASH_KEY(paramPreset)
	sTattooData.iUpgradeGroup = GET_HASH_KEY(paramUpgradeGroup)
	sTattooData.eFaction = paramFaction
	sTattooData.iCost = paramCost
	sTattooData.iBitset = (ENUM_TO_INT(eTattoo) / 32)
	sTattooData.iBitIndex = (ENUM_TO_INT(eTattoo) % 32)
	sTattooData.eFace = tattooFacing
	
	// Add ped specific details to the preset
	IF DOES_ENTITY_EXIST(pedID)
	AND NOT IS_PED_INJURED(pedID)
		IF sTattooData.eFaction = TATTOO_SP_MICHAEL
		OR sTattooData.eFaction = TATTOO_SP_FRANKLIN
		OR sTattooData.eFaction = TATTOO_SP_TREVOR
			// Do nothing
		ELIF sTattooData.eFaction = TATTOO_MP_FM // Freemode
		
		#IF USE_TU_CHANGES
			OR sTattooData.eFaction = TATTOO_MP_FM_F // Freemode Female
		#ENDIF
			
			TEXT_LABEL_31 sPresetName = ""
			
			// Crew emblems
			IF sTattooData.eEnum = TATTOO_MP_FM_CREW_A
			OR sTattooData.eEnum = TATTOO_MP_FM_CREW_B
			OR sTattooData.eEnum = TATTOO_MP_FM_CREW_C
			OR sTattooData.eEnum = TATTOO_MP_FM_CREW_D
			
		#IF USE_TU_CHANGES // not in 1st sub so remove from SP scripts- AndyM
			OR sTattooData.eEnum = TATTOO_MP_FM_CREW_E
			OR sTattooData.eEnum = TATTOO_MP_FM_CREW_F
		#ENDIF
			
				// Append FACTION tag
				sPresetName += "FM_"
				
				// Append CREW tag
				sPresetName += "CREW_"
			
				// Append GENDER tag
				IF GET_ENTITY_MODEL(pedID) = MP_M_FREEMODE_01
					sPresetName += "M_"
				ELSE
					sPresetName += "F_"
				ENDIF
				
				// Append ID tag
				sPresetName += paramPreset

			ELIF ARE_STRINGS_EQUAL(paramUpgradeGroup, "torsoDecal")
			
			//Removing for SP scripts as this was not in the FirstSub. -BenR
			#IF USE_TU_CHANGES
			OR	ARE_STRINGS_EQUAL(paramUpgradeGroup, "hairOverlay")
			#ENDIF

				// don't append anything to the torso Decal tattoos
				// or hair overlays
				sPresetName = paramPreset
				
				
			// Rewards
			ELSE
				
				IF sTattooData.eEnum = TATTOO_MP_FM_HEAD_BANGER
				OR sTattooData.eEnum = TATTOO_MP_FM_SLAYER
				OR sTattooData.eEnum = TATTOO_MP_FM_GANGHIDEOUT_CLEAR
				OR sTattooData.eEnum = TATTOO_MP_FM_HUSTLER
				OR sTattooData.eEnum = TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN	
				OR sTattooData.eEnum = TATTOO_MP_FM_WIN_EVER_MODE_ONCE	
				OR sTattooData.eEnum = TATTOO_MP_FM_BOUNTY_KILLER	
				OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_WORLD_RECORD	
				OR sTattooData.eEnum = TATTOO_MP_FM_FULL_MODDED	
				OR sTattooData.eEnum = TATTOO_MP_FM_REVENGE_KILL	
				OR sTattooData.eEnum = TATTOO_MP_FM_KILL_3_RACERS	
				OR sTattooData.eEnum = TATTOO_MP_FM_REACH_RANK_1
				OR sTattooData.eEnum = TATTOO_MP_FM_REACH_RANK_2
				OR sTattooData.eEnum = TATTOO_MP_FM_REACH_RANK_3
				OR sTattooData.eEnum = TATTOO_MP_FM_FMKILLCHEATER
				OR sTattooData.eEnum = TATTOO_MP_FM_RACES_WON
				OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_1
				OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_2
				OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_3
				OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_4
					// Append FACTION tag
					sPresetName += "FM_Tat_Award_"
				ELSE
					sPresetName += "FM_Tat_"
				ENDIF
				
				// Append GENDER tag
				IF GET_ENTITY_MODEL(pedID) = MP_M_FREEMODE_01
				
				//Removing for SP scripts as this was not in the FirstSub. -BenR
				#IF USE_TU_CHANGES
				OR sTattooData.eEnum = TATTOO_MP_FM_04 //B*1599749 The female version got randomly removed so use male version	
				#ENDIF
				
					sPresetName += "M_"
				ELSE
					sPresetName += "F_"
				ENDIF
				
				// Append ID tag
				sPresetName += paramPreset
			ENDIF
			
			// Update stored data
			sTattooData.iPreset = GET_HASH_KEY(sPresetName)
			
			// Mark as invalid if it doesn't exist
			IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = PDZ_INVALID
				sTattooData.eEnum = INVALID_TATTOO
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC FILL_TATTOO_DATA_USING_MODEL(TATTOO_DATA_STRUCT &sTattooData, TATTOO_NAME_ENUM eTattoo, MODEL_NAMES pedModel, STRING paramLabel, STRING paramCollection, STRING paramPreset, STRING paramUpgradeGroup, TATTOO_FACTION_ENUM paramFaction, INT paramCost, TATTOO_FACING tattooFacing = TATTOO_FRONT)

	sTattooData.eEnum 			= eTattoo
	sTattooData.sLabel 			= paramLabel
	sTattooData.iCollection 	= GET_HASH_KEY(paramCollection)
	sTattooData.iPreset 		= GET_HASH_KEY(paramPreset)
	sTattooData.iUpgradeGroup 	= GET_HASH_KEY(paramUpgradeGroup)
	sTattooData.eFaction 		= paramFaction
	sTattooData.iCost 			= paramCost
	sTattooData.iBitset 		= (ENUM_TO_INT(eTattoo) / 32)
	sTattooData.iBitIndex 		= (ENUM_TO_INT(eTattoo) % 32)
	sTattooData.eFace 			= tattooFacing
	
	// Add ped specific details to the preset
	
	IF sTattooData.eFaction = TATTOO_SP_MICHAEL
	OR sTattooData.eFaction = TATTOO_SP_FRANKLIN
	OR sTattooData.eFaction = TATTOO_SP_TREVOR
		// Do nothing
	ELIF sTattooData.eFaction = TATTOO_MP_FM // Freemode
	
	#IF USE_TU_CHANGES
		OR sTattooData.eFaction = TATTOO_MP_FM_F // Freemode Female
	#ENDIF
		
		TEXT_LABEL_31 sPresetName = ""
		
		// Crew emblems
		IF sTattooData.eEnum = TATTOO_MP_FM_CREW_A
		OR sTattooData.eEnum = TATTOO_MP_FM_CREW_B
		OR sTattooData.eEnum = TATTOO_MP_FM_CREW_C
		OR sTattooData.eEnum = TATTOO_MP_FM_CREW_D
		
	#IF USE_TU_CHANGES // not in 1st sub so remove from SP scripts- AndyM
		OR sTattooData.eEnum = TATTOO_MP_FM_CREW_E
		OR sTattooData.eEnum = TATTOO_MP_FM_CREW_F
	#ENDIF
		
			// Append FACTION tag
			sPresetName += "FM_"
			
			// Append CREW tag
			sPresetName += "CREW_"
		
			// Append GENDER tag
			IF pedModel = MP_M_FREEMODE_01
				sPresetName += "M_"
			ELSE
				sPresetName += "F_"
			ENDIF
			
			// Append ID tag
			sPresetName += paramPreset

		ELIF ARE_STRINGS_EQUAL(paramUpgradeGroup, "torsoDecal")
		
		//Removing for SP scripts as this was not in the FirstSub. -BenR
		#IF USE_TU_CHANGES
		OR	ARE_STRINGS_EQUAL(paramUpgradeGroup, "hairOverlay")
		#ENDIF

			// don't append anything to the torso Decal tattoos
			// or hair overlays
			sPresetName = paramPreset
			
			
		// Rewards
		ELSE
			
			IF sTattooData.eEnum = TATTOO_MP_FM_HEAD_BANGER
			OR sTattooData.eEnum = TATTOO_MP_FM_SLAYER
			OR sTattooData.eEnum = TATTOO_MP_FM_GANGHIDEOUT_CLEAR
			OR sTattooData.eEnum = TATTOO_MP_FM_HUSTLER
			OR sTattooData.eEnum = TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN	
			OR sTattooData.eEnum = TATTOO_MP_FM_WIN_EVER_MODE_ONCE	
			OR sTattooData.eEnum = TATTOO_MP_FM_BOUNTY_KILLER	
			OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_WORLD_RECORD	
			OR sTattooData.eEnum = TATTOO_MP_FM_FULL_MODDED	
			OR sTattooData.eEnum = TATTOO_MP_FM_REVENGE_KILL	
			OR sTattooData.eEnum = TATTOO_MP_FM_KILL_3_RACERS	
			OR sTattooData.eEnum = TATTOO_MP_FM_REACH_RANK_1
			OR sTattooData.eEnum = TATTOO_MP_FM_REACH_RANK_2
			OR sTattooData.eEnum = TATTOO_MP_FM_REACH_RANK_3
			OR sTattooData.eEnum = TATTOO_MP_FM_FMKILLCHEATER
			OR sTattooData.eEnum = TATTOO_MP_FM_RACES_WON
			OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_1
			OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_2
			OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_3
			OR sTattooData.eEnum = TATTOO_MP_FM_HOLD_UP_SHOPS_4
				// Append FACTION tag
				sPresetName += "FM_Tat_Award_"
			ELSE
				sPresetName += "FM_Tat_"
			ENDIF
			
			// Append GENDER tag
			IF pedModel = MP_M_FREEMODE_01
			
			//Removing for SP scripts as this was not in the FirstSub. -BenR
			#IF USE_TU_CHANGES
			OR sTattooData.eEnum = TATTOO_MP_FM_04 //B*1599749 The female version got randomly removed so use male version	
			#ENDIF
			
				sPresetName += "M_"
			ELSE
				sPresetName += "F_"
			ENDIF
			
			// Append ID tag
			sPresetName += paramPreset
		ENDIF
		
		// Update stored data
		sTattooData.iPreset = GET_HASH_KEY(sPresetName)
		
		// Mark as invalid if it doesn't exist
		IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = PDZ_INVALID
			sTattooData.eEnum = INVALID_TATTOO
		ENDIF
	ENDIF
	
ENDPROC
/// PURPOSE: Helper proc to fill the ped tattoo struct
PROC FILL_TATTOO_DATA_DLC(TATTOO_DATA_STRUCT &sTattooData, TATTOO_FACTION_ENUM eFaction, TATTOO_NAME_ENUM eTattoo, TATTOO_NAME_ENUM eDLCTattoo)

	INT iDLCIndex = (ENUM_TO_INT(eTattoo)-ENUM_TO_INT(eDLCTattoo))
	INT iDLCItemCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
	
	IF (iDLCItemCount > 0 AND iDLCIndex < iDLCItemCount)
		
		sTattooShopItemValues sDLCTattooData
		IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
		
			// Content has to be unlocked. It's possible to install compatibility pack but remove individual items.
			IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
			
				#IF NOT USE_TU_CHANGES
					sTattooData.eEnum = eTattoo
					sTattooData.sLabel = sDLCTattooData.Label
					sTattooData.iPreset = sDLCTattooData.Preset
					sTattooData.iCollection = sDLCTattooData.Collection
					
					// TEMP - USE THESE LABELS UNTIL WE GET LABEL SYSTEM SORTED
						sTattooData.sLabel = "TAT_DLC_"
						sTattooData.sLabel += iDLCIndex
					// ENDTEMP
					
					sTattooData.iUpgradeGroup = sDLCTattooData.UpdateGroup
					sTattooData.eFaction = eFaction
					sTattooData.iCost = 0//sDLCTattooData.Cost
					sTattooData.iBitset = (ENUM_TO_INT(eTattoo) / 32)
					sTattooData.iBitIndex = (ENUM_TO_INT(eTattoo) % 32)
					sTattooData.eFace = INT_TO_ENUM(TATTOO_FACING, sDLCTattooData.Facing)
				#ENDIF
				
				#IF USE_TU_CHANGES
					sTattooData.eEnum = eTattoo
					sTattooData.sLabel = sDLCTattooData.Label
					sTattooData.iPreset = sDLCTattooData.Preset
					sTattooData.iCollection = sDLCTattooData.Collection
					sTattooData.iUpgradeGroup = sDLCTattooData.UpdateGroup
					sTattooData.eFaction = eFaction
					sTattooData.iCost = sDLCTattooData.Cost
					sTattooData.iBitset = (ENUM_TO_INT(eTattoo) / 32)
					sTattooData.iBitIndex = (ENUM_TO_INT(eTattoo) % 32)
					sTattooData.eFace = INT_TO_ENUM(TATTOO_FACING, sDLCTattooData.Facing)
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets the collection and preset strings for use with ADD_PED_DECORATION()
FUNC BOOL GET_TATTOO_DATA(TATTOO_DATA_STRUCT &sTattooData, TATTOO_NAME_ENUM eTattoo, TATTOO_FACTION_ENUM eFaction, PED_INDEX pedID, SHOP_NAME_ENUM eInShop = EMPTY_SHOP)
	//INT iDummyCost = 50
	// Set default data
	FILL_TATTOO_DATA(sTattooData, INVALID_TATTOO, NULL, "", "", "", "", eFaction, -1)
	
	MODEL_NAMES ePedModel
	IF DOES_ENTITY_EXIST(pedID)
	AND NOT IS_PED_INJURED(pedID)
		ePedModel = GET_ENTITY_MODEL(pedID)
	ENDIF
	
	#IF USE_SP_DLC
		UNUSED_PARAMETER(ePedModel)
	#ENDIF
	
	INT iPrice
	
	SWITCH eFaction
		CASE TATTOO_SP_MICHAEL
			SWITCH eTattoo
				// Singleplayer																					Text label			Collection					Preset			Upgrade Group		Faction		Cost
				CASE TATTOO_SP_MICHAEL_1 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_01", 		"singleplayer_overlays", 	"MK_000", 		"",					eFaction,	300,      TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_MICHAEL_2							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_02", 		"singleplayer_overlays", 	"MK_001", 		"",					eFaction,	450,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_3							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_03", 		"singleplayer_overlays", 	"MK_002", 		"",					eFaction,	250,      TATTOO_FRONT_LEFT) BREAK
				CASE TATTOO_SP_MICHAEL_4							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_04", 		"singleplayer_overlays", 	"MK_003", 		"",					eFaction,	175,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_6							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_06", 		"singleplayer_overlays", 	"MK_005", 		"",					eFaction,	380,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_7							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_07", 		"singleplayer_overlays", 	"MK_006", 		"",					eFaction,	180,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_8							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_08", 		"singleplayer_overlays", 	"MK_007", 		"",					eFaction,	240,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_10							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_10", 		"singleplayer_overlays", 	"MK_009", 		"",					eFaction,	195,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_11							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_11", 		"singleplayer_overlays", 	"MK_010", 		"",					eFaction,	280,      TATTOO_FRONT_RIGHT) BREAK
				
				// SE/CE - Tiki Pin-up girl
				CASE TATTOO_SP_MICHAEL_12 
					// Fix for 2023924 - Last gen SE/CE players gets these pack related items for free. Otherwise pay new price confirmed by Imran/Tarek.
					IF IS_SPECIAL_EDITION_GAME() OR IS_COLLECTORS_EDITION_GAME() 
						iPrice = 400
						IF IS_LAST_GEN_PLAYER() 
						AND (HAS_LAST_GEN_PLAYER_GOT_SPECIAL_EDITION() OR HAS_LAST_GEN_PLAYER_GOT_COLLECTORS_EDITION())
							iPrice = 0
						ENDIF	
						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_12", 		"singleplayer_overlays", 	"MK_011", 		"",					eFaction,	iPrice,      TATTOO_LEFT)
					ENDIF 
				BREAK
				
				CASE TATTOO_SP_MICHAEL_13							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_13", 		"singleplayer_overlays", 	"MK_012", 		"",					eFaction,	320,      TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_MICHAEL_14							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_14", 		"singleplayer_overlays", 	"MK_013", 		"",					eFaction,	500,      TATTOO_LEFT) BREAK
				CASE TATTOO_SP_MICHAEL_16							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_16", 		"singleplayer_overlays", 	"MK_015", 		"",					eFaction,	220,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_17							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_17", 		"singleplayer_overlays", 	"MK_016", 		"",					eFaction,	320,      TATTOO_FRONT_LEFT) BREAK
				CASE TATTOO_SP_MICHAEL_18							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_18", 		"singleplayer_overlays", 	"MK_017", 		"",					eFaction,	140,      TATTOO_LEFT) BREAK
				CASE TATTOO_SP_MICHAEL_20							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_20", 		"singleplayer_overlays", 	"MK_019", 		"",					eFaction,	350,      TATTOO_FRONT) BREAK				
				CASE TATTOO_SP_MICHAEL_21							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_21", 		"singleplayer_overlays", 	"MK_020", 		"",					eFaction,	350,      TATTOO_FRONT) BREAK
				CASE TATTOO_SP_MICHAEL_5							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_05", 		"singleplayer_overlays", 	"MK_004", 		"",					eFaction,	120,      TATTOO_BACK) BREAK
				CASE TATTOO_SP_MICHAEL_9							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_09", 		"singleplayer_overlays", 	"MK_008", 		"",					eFaction,	99,       TATTOO_BACK) BREAK
				CASE TATTOO_SP_MICHAEL_15							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_15", 		"singleplayer_overlays", 	"MK_014", 		"",					eFaction,	400,      TATTOO_BACK) BREAK
				CASE TATTOO_SP_MICHAEL_19 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_MIC_19", 		"singleplayer_overlays", 	"MK_018", 		"",					eFaction,	420,      TATTOO_BACK) BREAK
				DEFAULT
					FILL_TATTOO_DATA_DLC(sTattooData, eFaction, eTattoo, TATTOO_SP_MICHAEL_DLC)
				BREAK

			ENDSWITCH
		BREAK
		
		CASE TATTOO_SP_FRANKLIN
			SWITCH eTattoo
				// Singleplayer																					Text label			Collection					Preset			Upgrade Group		Faction		Cost
				
				// SE/CE - Chamberlain
				CASE TATTOO_SP_FRANKLIN_1 
					// Fix for 2023924 - Last gen SE/CE players gets these pack related items for free. Otherwise pay new price confirmed by Imran/Tarek.
					IF IS_SPECIAL_EDITION_GAME() OR IS_COLLECTORS_EDITION_GAME() 
						iPrice = 450
						IF IS_LAST_GEN_PLAYER() 
						AND (HAS_LAST_GEN_PLAYER_GOT_SPECIAL_EDITION() OR HAS_LAST_GEN_PLAYER_GOT_COLLECTORS_EDITION())
							iPrice = 0
						ENDIF	
						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_01", 		"singleplayer_overlays", 	"fr_000", 		"",					eFaction,	iPrice,  TATTOO_FRONT) 
					ENDIF 
				BREAK
				
				CASE TATTOO_SP_FRANKLIN_2							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_02", 		"singleplayer_overlays", 	"fr_001", 		"",					eFaction,	230,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_3							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_03", 		"singleplayer_overlays", 	"fr_002", 		"",					eFaction,	310,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_4							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_04", 		"singleplayer_overlays", 	"fr_003", 		"",					eFaction,	260,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_7							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_07", 		"singleplayer_overlays", 	"fr_006", 		"",					eFaction,	240,  TATTOO_LEFT) BREAK
				CASE TATTOO_SP_FRANKLIN_8							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_08", 		"singleplayer_overlays", 	"fr_007", 		"",					eFaction,	190,  TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_FRANKLIN_9							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_09", 		"singleplayer_overlays", 	"fr_008", 		"",					eFaction,	200,  TATTOO_LEFT) BREAK
				CASE TATTOO_SP_FRANKLIN_10							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_10", 		"singleplayer_overlays", 	"fr_009", 		"",					eFaction,	225,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_12							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_12", 		"singleplayer_overlays", 	"fr_011", 		"",					eFaction,	195,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_13							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_13", 		"singleplayer_overlays", 	"fr_012", 		"",					eFaction,	275,  TATTOO_LEFT) BREAK
				CASE TATTOO_SP_FRANKLIN_14							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_14", 		"singleplayer_overlays", 	"fr_013", 		"",					eFaction,	80,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_15							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_15", 		"singleplayer_overlays", 	"fr_014", 		"",					eFaction,	95,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_16							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_16", 		"singleplayer_overlays", 	"fr_015", 		"",					eFaction,	300,  TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_FRANKLIN_17							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_17", 		"singleplayer_overlays", 	"fr_016", 		"",					eFaction,	450,  TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_FRANKLIN_18							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_18", 		"singleplayer_overlays", 	"fr_017", 		"",					eFaction,	345,  TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_FRANKLIN_19							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_19", 		"singleplayer_overlays", 	"fr_018", 		"",					eFaction,	550,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_20							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_20", 		"singleplayer_overlays", 	"fr_019", 		"",					eFaction,	200,  TATTOO_FRONT) BREAK				
				CASE TATTOO_SP_FRANKLIN_21							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_21", 		"singleplayer_overlays", 	"fr_020", 		"",					eFaction,	180,  TATTOO_FRONT) BREAK				
				CASE TATTOO_SP_FRANKLIN_22							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_22", 		"singleplayer_overlays", 	"fr_021", 		"",					eFaction,	140,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_24							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_24", 		"singleplayer_overlays", 	"fr_023", 		"",					eFaction,	245,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_25							/*FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_25", 		"singleplayer_overlays", 	"fr_024", 		"",					eFaction,	395,  TATTOO_FRONT)*/ BREAK   //B*1356026 dupe tattoo
				CASE TATTOO_SP_FRANKLIN_26							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_26", 		"singleplayer_overlays", 	"fr_025", 		"",					eFaction,	370,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_27							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_27", 		"singleplayer_overlays", 	"fr_026", 		"",					eFaction,	350,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_28							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_28", 		"singleplayer_overlays", 	"fr_027", 		"",					eFaction,	310,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_29							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_29", 		"singleplayer_overlays", 	"fr_028", 		"",					eFaction,	210,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_30							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_30", 		"singleplayer_overlays", 	"fr_029", 		"",					eFaction,	245,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_31							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_31", 		"singleplayer_overlays", 	"fr_030", 		"",					eFaction,	85,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_32							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_32", 		"singleplayer_overlays", 	"fr_031", 		"",					eFaction,	210,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_33							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_33", 		"singleplayer_overlays", 	"fr_032", 		"",					eFaction,	225,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_34							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_34", 		"singleplayer_overlays", 	"fr_033", 		"",					eFaction,	145,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_35							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_35", 		"singleplayer_overlays", 	"fr_034", 		"",					eFaction,	230,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_36							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_36", 		"singleplayer_overlays", 	"fr_035", 		"",					eFaction,	195,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_37							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_37", 		"singleplayer_overlays", 	"fr_036", 		"",					eFaction,	255,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_38							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_38", 		"singleplayer_overlays", 	"fr_037", 		"",					eFaction,	300,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_40							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_40", 		"singleplayer_overlays", 	"fr_039", 		"",					eFaction,	300,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_FRANKLIN_5							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_05", 		"singleplayer_overlays", 	"fr_004", 		"",					eFaction,	255,  TATTOO_BACK) BREAK
				CASE TATTOO_SP_FRANKLIN_6							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_06", 		"singleplayer_overlays", 	"fr_005", 		"",					eFaction,	175,  TATTOO_BACK) BREAK
				CASE TATTOO_SP_FRANKLIN_11							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_11", 		"singleplayer_overlays", 	"fr_010", 		"",					eFaction,	520,  TATTOO_BACK) BREAK
				CASE TATTOO_SP_FRANKLIN_23							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_23", 		"singleplayer_overlays", 	"fr_022", 		"",					eFaction,	125,  TATTOO_BACK) BREAK
				CASE TATTOO_SP_FRANKLIN_39							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FRA_39", 		"singleplayer_overlays", 	"fr_038", 		"",					eFaction,	365,  TATTOO_BACK) BREAK
				DEFAULT
					FILL_TATTOO_DATA_DLC(sTattooData, eFaction, eTattoo, TATTOO_SP_FRANKLIN_DLC)
				BREAK
			ENDSWITCH
		BREAK
		
		
		CASE TATTOO_SP_TREVOR
			SWITCH eTattoo
				// Singleplayer																					Text label			Collection					Preset			Upgrade Group		Faction		Cost
				CASE TATTOO_SP_TREVOR_1								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_01", 		"singleplayer_overlays", 	"tp_000", 		"",					eFaction,	120,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_2								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_02", 		"singleplayer_overlays", 	"tp_001", 		"",					eFaction,	150,   TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_TREVOR_3								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_03", 		"singleplayer_overlays", 	"tp_002", 		"",					eFaction,	100,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_4								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_04", 		"singleplayer_overlays", 	"tp_003", 		"",					eFaction,	140,  TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_5								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_05", 		"singleplayer_overlays", 	"tp_004", 		"",					eFaction,	250,  TATTOO_FRONT) BREAK
				
				// SE/CE - Love To Hate
				CASE TATTOO_SP_TREVOR_6 
					// Fix for 2023924 - Last gen SE/CE players gets these pack related items for free. Otherwise pay new price confirmed by Imran/Tarek.
					IF IS_SPECIAL_EDITION_GAME() OR IS_COLLECTORS_EDITION_GAME() 
						iPrice = 380
						IF IS_LAST_GEN_PLAYER() 
						AND (HAS_LAST_GEN_PLAYER_GOT_SPECIAL_EDITION() OR HAS_LAST_GEN_PLAYER_GOT_COLLECTORS_EDITION())
							iPrice = 0
						ENDIF	
						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_06", 		"singleplayer_overlays", 	"tp_005", 		"",					eFaction,	iPrice,  TATTOO_RIGHT)
					ENDIF 
				BREAK
			
				CASE TATTOO_SP_TREVOR_7								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_07", 		"singleplayer_overlays", 	"tp_006", 		"",					eFaction,	120,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_8								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_08", 		"singleplayer_overlays", 	"tp_007", 		"",					eFaction,	250,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_9								FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_09", 		"singleplayer_overlays", 	"tp_008", 		"",					eFaction,	50,    TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_TREVOR_10							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_10", 		"singleplayer_overlays", 	"tp_009", 		"",					eFaction,	135,   TATTOO_LEFT) BREAK
				CASE TATTOO_SP_TREVOR_11							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_11", 		"singleplayer_overlays", 	"tp_010", 		"",					eFaction,	245,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_12							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_12", 		"singleplayer_overlays", 	"tp_011", 		"",					eFaction,	280,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_13							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_13", 		"singleplayer_overlays", 	"tp_012", 		"",					eFaction,	65,    TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_14							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_14", 		"singleplayer_overlays", 	"tp_013", 		"",					eFaction,	150,   TATTOO_FRONT_RIGHT) BREAK
				CASE TATTOO_SP_TREVOR_15							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_15", 		"singleplayer_overlays", 	"tp_014", 		"",					eFaction,	200,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_16							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_16", 		"singleplayer_overlays", 	"tp_015", 		"",					eFaction,	145,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_17							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_17", 		"singleplayer_overlays", 	"tp_016", 		"",					eFaction,	290,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_18							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_18", 		"singleplayer_overlays", 	"tp_017", 		"",					eFaction,	350,   TATTOO_RIGHT) BREAK
				CASE TATTOO_SP_TREVOR_19							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_19", 		"singleplayer_overlays", 	"tp_018", 		"",					eFaction,	70,    TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_20							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_20", 		"singleplayer_overlays", 	"tp_019", 		"",					eFaction,	180,   TATTOO_FRONT_RIGHT) BREAK				
				CASE TATTOO_SP_TREVOR_21							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_21", 		"singleplayer_overlays", 	"tp_020", 		"",					eFaction,	230,   TATTOO_FRONT) BREAK				
				CASE TATTOO_SP_TREVOR_22							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_22", 		"singleplayer_overlays", 	"tp_021", 		"",					eFaction,	200,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_24							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_24", 		"singleplayer_overlays", 	"tp_023", 		"",					eFaction,	240,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_25							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_25", 		"singleplayer_overlays", 	"tp_024", 		"",					eFaction,	195,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_26							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_26", 		"singleplayer_overlays", 	"tp_025", 		"",					eFaction,	225,   TATTOO_LEFT) BREAK
				CASE TATTOO_SP_TREVOR_28							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_28", 		"singleplayer_overlays", 	"tp_027", 		"",					eFaction,	175,   TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_29							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_29", 		"singleplayer_overlays", 	"tp_028", 		"",					eFaction,	65,    TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_30							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_30", 		"singleplayer_overlays", 	"tp_029", 		"",					eFaction,	50,    TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_31							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_31", 		"singleplayer_overlays", 	"tp_030", 		"",					eFaction,	70,    TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_34							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_34", 		"singleplayer_overlays", 	"tp_033", 		"",					eFaction,	70,    TATTOO_FRONT) BREAK
				CASE TATTOO_SP_TREVOR_23							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_23", 		"singleplayer_overlays", 	"tp_022", 		"",					eFaction,	500,   TATTOO_BACK) BREAK
				CASE TATTOO_SP_TREVOR_27							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_27", 		"singleplayer_overlays", 	"tp_026", 		"",					eFaction,	300,   TATTOO_BACK) BREAK
				CASE TATTOO_SP_TREVOR_32							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_32", 		"singleplayer_overlays", 	"tp_031", 		"",					eFaction,	190,   TATTOO_BACK) BREAK
				CASE TATTOO_SP_TREVOR_33							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_TRV_33", 		"singleplayer_overlays", 	"tp_032", 		"",					eFaction,	129,   TATTOO_BACK) BREAK
				DEFAULT
					FILL_TATTOO_DATA_DLC(sTattooData, eFaction, eTattoo, TATTOO_SP_TREVOR_DLC)
				BREAK
			ENDSWITCH
		BREAK
		
		//New Title Update changes for Freemode data. Won't be compiled in by SP scripts. -BenR
		#IF NOT USE_SP_DLC
		#IF USE_TU_CHANGES
		CASE TATTOO_MP_FM
		CASE TATTOO_MP_FM_F
			SWITCH eTattoo
				// Freemode																						Text label			Collection					Preset				Upgrade Group		Faction		Cost		Camera
				CASE TATTOO_MP_FM_HEAD_BANGER 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_008", 		"multiplayer_overlays", 	"000", 				"",					eFaction,	ROUND((TO_FLOAT(20000) * g_sMPTunables.ftattoo_mp_fm_head_banger_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_SLAYER						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_009", 		"multiplayer_overlays", 	"001", 				"",					eFaction,	ROUND((TO_FLOAT(1400) * g_sMPTunables.ftattoo_mp_fm_slayer_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_GANGHIDEOUT_CLEAR				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_010", 		"multiplayer_overlays", 	"002", 				"",					eFaction,	ROUND((TO_FLOAT(9750) * g_sMPTunables.ftattoo_mp_fm_ganghideout_clear_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_011", 		"multiplayer_overlays", 	"003", 				"",					eFaction,	ROUND((TO_FLOAT(2150) * g_sMPTunables.ftattoo_mp_fm_armoured_van_takedown_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_HUSTLER						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_012", 		"multiplayer_overlays", 	"004", 				"",					eFaction,	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_hustler_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				
				CASE TATTOO_MP_FM_WIN_EVER_MODE_ONCE			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_013", 		"multiplayer_overlays", 	"005", 				"",					eFaction,	ROUND((TO_FLOAT(12400) * g_sMPTunables.ftattoo_mp_fm_win_ever_mode_once_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_BOUNTY_KILLER					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_014", 		"multiplayer_overlays", 	"006", 				"",					eFaction,	ROUND((TO_FLOAT(3500) * g_sMPTunables.ftattoo_mp_fm_bounty_killer_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_HOLD_WORLD_RECORD				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_015", 		"multiplayer_overlays", 	"007", 				"",					eFaction,	ROUND((TO_FLOAT(4950) * g_sMPTunables.ftattoo_mp_fm_hold_world_record_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_FULL_MODDED					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_016", 		"multiplayer_overlays", 	"008", 				"",					eFaction,	ROUND((TO_FLOAT(1350) * g_sMPTunables.ftattoo_mp_fm_full_modded_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_REVENGE_KILL					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_017", 		"multiplayer_overlays", 	"009", 				"",					eFaction,	ROUND((TO_FLOAT(1450) * g_sMPTunables.ftattoo_mp_fm_revenge_kill_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_KILL_3_RACERS					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_018", 		"multiplayer_overlays", 	"010", 				"",					eFaction,	ROUND((TO_FLOAT(2700) * g_sMPTunables.ftattoo_mp_fm_kill_3_racers_expenditure_tunable)), 		TATTOO_BACK_RIGHT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_1					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_019", 		"multiplayer_overlays", 	"011", 				"rank",				eFaction,	ROUND((TO_FLOAT(1200) * g_sMPTunables.ftattoo_mp_fm_reach_rank_1_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_2					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_020", 		"multiplayer_overlays", 	"012", 				"rank",				eFaction,	ROUND((TO_FLOAT(1500) * g_sMPTunables.ftattoo_mp_fm_reach_rank_2_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_3					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_021", 		"multiplayer_overlays", 	"013", 				"rank",				eFaction,	ROUND((TO_FLOAT(2650) * g_sMPTunables.ftattoo_mp_fm_reach_rank_3_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_FMKILLCHEATER					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_022", 		"multiplayer_overlays", 	"014", 				"",					eFaction,	ROUND((TO_FLOAT(1900) * g_sMPTunables.ftattoo_mp_fm_fmkillcheater_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_RACES_WON						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_023", 		"multiplayer_overlays", 	"015", 				"",					eFaction,	ROUND((TO_FLOAT(4950) * g_sMPTunables.ftattoo_mp_fm_races_won_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_1				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_024", 		"multiplayer_overlays", 	"016", 				"",					eFaction,	ROUND((TO_FLOAT(2400) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_1_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_2				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_025", 		"multiplayer_overlays", 	"017", 				"",					eFaction,	ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_2_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_3				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_026", 		"multiplayer_overlays", 	"018", 				"",					eFaction,	ROUND((TO_FLOAT(7400) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_3_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_4				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_027", 		"multiplayer_overlays", 	"019", 				"",					eFaction,	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_4_expenditure_tunable)), 		TATTOO_BACK) BREAK
				
				CASE TATTOO_MP_FM_01 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_201", 		"multiplayer_overlays", 	"005", 				"",					eFaction,	 ROUND((TO_FLOAT(2400) * g_sMPTunables.ftattoo_mp_fm_01_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_02 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_202", 		"multiplayer_overlays", 	"006", 				"",					eFaction,	 ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_02_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_03 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_203", 		"multiplayer_overlays", 	"015", 				"",					eFaction,	 ROUND((TO_FLOAT(3600) * g_sMPTunables.ftattoo_mp_fm_03_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_04		 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_204", 		"multiplayer_overlays", 	"000", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_04_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_05 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_205", 		"multiplayer_overlays", 	"001", 				"",					eFaction,	 ROUND((TO_FLOAT(12500) * g_sMPTunables.ftattoo_mp_fm_05_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_06 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_206", 		"multiplayer_overlays", 	"003", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_06_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_07		 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_207", 		"multiplayer_overlays", 	"014", 				"",					eFaction,	 ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_07_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_08 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_208", 		"multiplayer_overlays", 	"018", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_08_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_09 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_209", 		"multiplayer_overlays", 	"002", 				"",					eFaction,	 ROUND((TO_FLOAT(3750) * g_sMPTunables.ftattoo_mp_fm_09_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_10		 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_210", 		"multiplayer_overlays", 	"007", 				"",					eFaction,	 ROUND((TO_FLOAT(3750) * g_sMPTunables.ftattoo_mp_fm_10_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_11 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_211", 		"multiplayer_overlays", 	"008", 				"",					eFaction,	 ROUND((TO_FLOAT(4800) * g_sMPTunables.ftattoo_mp_fm_11_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_12 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_212", 		"multiplayer_overlays", 	"017", 				"",					eFaction,	 ROUND((TO_FLOAT(3500) * g_sMPTunables.ftattoo_mp_fm_12_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_13 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_213", 		"multiplayer_overlays", 	"009", 				"",					eFaction,	 ROUND((TO_FLOAT(12350) * g_sMPTunables.ftattoo_mp_fm_13_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_14 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_214", 		"multiplayer_overlays", 	"011", 				"",					eFaction,	 ROUND((TO_FLOAT(1900) * g_sMPTunables.ftattoo_mp_fm_14_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_15 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_215", 		"multiplayer_overlays", 	"013", 				"",					eFaction,	 ROUND((TO_FLOAT(4500) * g_sMPTunables.ftattoo_mp_fm_15_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_16 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_216", 		"multiplayer_overlays", 	"016", 				"",					eFaction,	 ROUND((TO_FLOAT(12250) * g_sMPTunables.ftattoo_mp_fm_16_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_17 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_217", 		"multiplayer_overlays", 	"019", 				"",					eFaction,	 ROUND((TO_FLOAT(12300) * g_sMPTunables.ftattoo_mp_fm_17_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_18 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_218", 		"multiplayer_overlays", 	"010", 				"",					eFaction,	 ROUND((TO_FLOAT(2500) * g_sMPTunables.ftattoo_mp_fm_18_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_19 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_219", 		"multiplayer_overlays", 	"004", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_19_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_20 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_220", 		"multiplayer_overlays", 	"012", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_20_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_21 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_221", 		"multiplayer_overlays", 	"020", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_21_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_22 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_222", 		"multiplayer_overlays", 	"021", 				"",					eFaction,	 ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_22_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_23 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_223", 		"multiplayer_overlays", 	"022", 				"",					eFaction,	 ROUND((TO_FLOAT(7300) * g_sMPTunables.ftattoo_mp_fm_23_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_24 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_224", 		"multiplayer_overlays", 	"023", 				"",					eFaction,	 ROUND((TO_FLOAT(7250) * g_sMPTunables.ftattoo_mp_fm_24_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_25 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_225", 		"multiplayer_overlays", 	"024", 				"",					eFaction,	 ROUND((TO_FLOAT(11900) * g_sMPTunables.ftattoo_mp_fm_25_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_26 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_226", 		"multiplayer_overlays", 	"025", 				"",					eFaction,	 ROUND((TO_FLOAT(2750) * g_sMPTunables.ftattoo_mp_fm_26_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_27 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_227", 		"multiplayer_overlays", 	"026", 				"",					eFaction,	 ROUND((TO_FLOAT(1750) * g_sMPTunables.ftattoo_mp_fm_27_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_28 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_228", 		"multiplayer_overlays", 	"027", 				"",					eFaction,	 ROUND((TO_FLOAT(7300) * g_sMPTunables.ftattoo_mp_fm_28_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_29 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_229", 		"multiplayer_overlays", 	"028", 				"",					eFaction,	 ROUND((TO_FLOAT(3250) * g_sMPTunables.ftattoo_mp_fm_29_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_30 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_230", 		"multiplayer_overlays", 	"029", 				"",					eFaction,	 ROUND((TO_FLOAT(1000) * g_sMPTunables.ftattoo_mp_fm_30_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_31 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_231", 		"multiplayer_overlays", 	"030", 				"",					eFaction,	 ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_31_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_32 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_232", 		"multiplayer_overlays", 	"031", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_32_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_33 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_233", 		"multiplayer_overlays", 	"032", 				"",					eFaction,	 ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_33_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_34 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_234", 		"multiplayer_overlays", 	"033", 				"",					eFaction,	 ROUND((TO_FLOAT(5050) * g_sMPTunables.ftattoo_mp_fm_34_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_35 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_235", 		"multiplayer_overlays", 	"034", 				"",					eFaction,	 ROUND((TO_FLOAT(2450) * g_sMPTunables.ftattoo_mp_fm_35_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_36 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_236", 		"multiplayer_overlays", 	"035", 				"",					eFaction,	 ROUND((TO_FLOAT(4950) * g_sMPTunables.ftattoo_mp_fm_36_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_37 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_237", 		"multiplayer_overlays", 	"036", 				"",					eFaction,	 ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_37_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_38 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_238", 		"multiplayer_overlays", 	"037", 				"",					eFaction,	 ROUND((TO_FLOAT(12250) * g_sMPTunables.ftattoo_mp_fm_38_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_39 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_239", 		"multiplayer_overlays", 	"038", 				"",					eFaction,	 ROUND((TO_FLOAT(1150) * g_sMPTunables.ftattoo_mp_fm_39_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_40 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_240", 		"multiplayer_overlays", 	"039", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_40_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_41 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_241", 		"multiplayer_overlays", 	"040", 				"",					eFaction,	 ROUND((TO_FLOAT(7600) * g_sMPTunables.ftattoo_mp_fm_41_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_42 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_242", 		"multiplayer_overlays", 	"041", 				"",					eFaction,	 ROUND((TO_FLOAT(2600) * g_sMPTunables.ftattoo_mp_fm_42_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_43 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_243", 		"multiplayer_overlays", 	"042", 				"",					eFaction,	 ROUND((TO_FLOAT(2500) * g_sMPTunables.ftattoo_mp_fm_43_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_44 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_244", 		"multiplayer_overlays", 	"043", 				"",					eFaction,	 ROUND((TO_FLOAT(7450) * g_sMPTunables.ftattoo_mp_fm_44_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_45 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_245", 		"multiplayer_overlays", 	"044", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_45_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_46 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_246", 		"multiplayer_overlays", 	"045", 				"",					eFaction,	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_46_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_47 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_247", 		"multiplayer_overlays", 	"047", 				"",					eFaction,	ROUND((TO_FLOAT(2500) * g_sMPTunables.ftattoo_mp_fm_47_expenditure_tunable)), 		TATTOO_LEFT) BREAK
			ENDSWITCH
			
			//----------- Freemode male---------------
			IF ePedModel = MP_M_FREEMODE_01
			
				SWITCH eTattoo
					// Male Torso Decals
					CASE TATTOO_MP_FM_MTD_1 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_001", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_2 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_002", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_3 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_003", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_4 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_004", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_5 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_005", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_6 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_006", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					
					
					CASE TATTOO_MP_FM_MTD_9 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_009", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_13					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_013", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_14 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_014", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_15 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_015", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_16 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_016", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_19 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_019", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_20 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_020", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_36 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_036", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_17 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_017", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_18 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_018", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_46 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_046", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_45 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_045", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					
					// Tshirt transfers
					CASE  TSHIRT_TRANS_MP_FM_REDSKULL			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_000", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_redskull_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE  TSHIRT_TRANS_MP_FM_LSBELLE 			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_001", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_lsbelle_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE  TSHIRT_TRANS_MP_FM_ROCKSTAR 			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_002", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_rockstar_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					
					// Hair overlays
					CASE TATTOO_MP_FM_MHO_1A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_002", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_003", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_004", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_005", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_A				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_006", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_B				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_007", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_C				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_008", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_D				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_009", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_E				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_010", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_011", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_012", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_013", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_014", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_015", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_6A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGBea_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_6B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGBea_M_Hair_001", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_6C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGBus_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_6D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGBus_M_Hair_001", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_6E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGHip_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGHip_M_Hair_001", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"NGInd_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"not_in_use", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"not_in_use", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"not_in_use", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_HO_FUZZ					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"mpBeach_overlays", 		"FM_Hair_Fuzz", 	"hairOverlay",	eFaction, 	100, 		TATTOO_FRONT)  BREAK
					
					CASE TATTOO_MP_FM_CREW_A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_100", 		"multiplayer_overlays", 	"000_A", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_a_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_101", 		"multiplayer_overlays", 	"000_B", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_b_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FMM_CLB", 		"multiplayer_overlays", 	"000_C", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_c_expenditure_tunable)), 		TATTOO_LEFT) BREAK
					CASE TATTOO_MP_FM_CREW_D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_100", 		"multiplayer_overlays", 	"000_D", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_101", 		"multiplayer_overlays", 	"000_E", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_F					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FMM_CLB", 		"multiplayer_overlays", 	"000_F", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				ENDSWITCH
	
			//------- Freemode female----------------
			ELIF ePedModel = MP_F_FREEMODE_01
				SWITCH eTattoo
					// Female Torso Decals
					CASE TATTOO_MP_FM_FTD_27 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_027_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_28 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_028_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_34 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_034_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_36 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_036_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 	BREAK
					CASE TATTOO_MP_FM_FTD_48 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_048", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_52 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_052", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_53 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_053", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_54 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_054", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_55 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_055", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_56 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_056", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_58 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_058", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_67 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_067", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_68 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_068", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_51 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_051", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) BREAK
					
					// Tshirt transfers
					CASE TSHIRT_TRANS_MP_FM_REDSKULL			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_000", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_redskull_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE TSHIRT_TRANS_MP_FM_LSBELLE 			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_001", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_lsbelle_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE TSHIRT_TRANS_MP_FM_ROCKSTAR 			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_002", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_rockstar_expenditure_tunable)), 		TATTOO_FRONT)  BREAK

					// Hair overlays
					CASE TATTOO_MP_FM_FHO_LONG_A				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_001", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_B				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_002", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_C				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_003", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_D				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_004", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_E				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_005", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_006", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_007", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_008", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_009", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_010", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_011", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_012", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_013", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_014", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_015", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGBea_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGBea_F_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGBus_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGBus_F_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGHip_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGHip_F_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"NGInd_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14E					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_HO_FUZZ					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 		"mpBeach_overlays", 		"FM_Hair_Fuzz", 		"hairOverlay",	eFaction, 	100, 		TATTOO_FRONT)  BREAK
					
					CASE TATTOO_MP_FM_CREW_A					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_100", 	"multiplayer_overlays", 	"000_A", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_a_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_B					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_100", 	"multiplayer_overlays", 	"000_B", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_b_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_C					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_101", 	"multiplayer_overlays", 	"000_C", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_c_expenditure_tunable)), 		TATTOO_LEFT) BREAK
					CASE TATTOO_MP_FM_CREW_D					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_101", 	"multiplayer_overlays", 	"000_D", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				ENDSWITCH
			ENDIF
		BREAK
		#ENDIF
		#ENDIF
		
		//Original FinalSub version. Compiled in by SP scripts. Must not change. -BenR
		#IF NOT USE_TU_CHANGES
		CASE TATTOO_MP_FM
			SWITCH eTattoo
				// Freemode																						Text label			Collection					Preset				Upgrade Group		Faction		Cost		Camera
				CASE TATTOO_MP_FM_HEAD_BANGER 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_008", 		"multiplayer_overlays", 	"000", 				"",					eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_SLAYER						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_009", 		"multiplayer_overlays", 	"001", 				"",					eFaction,	150, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_GANGHIDEOUT_CLEAR				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_010", 		"multiplayer_overlays", 	"002", 				"",					eFaction,	150, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_011", 		"multiplayer_overlays", 	"003", 				"",					eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_HUSTLER						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_012", 		"multiplayer_overlays", 	"004", 				"",					eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_WIN_EVER_MODE_ONCE			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_013", 		"multiplayer_overlays", 	"005", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_BOUNTY_KILLER					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_014", 		"multiplayer_overlays", 	"006", 				"",					eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_HOLD_WORLD_RECORD				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_015", 		"multiplayer_overlays", 	"007", 				"",					eFaction,	150, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_FULL_MODDED					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_016", 		"multiplayer_overlays", 	"008", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_REVENGE_KILL					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_017", 		"multiplayer_overlays", 	"009", 				"",					eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_KILL_3_RACERS					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_018", 		"multiplayer_overlays", 	"010", 				"",					eFaction,	150, 		TATTOO_BACK_RIGHT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_1					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_019", 		"multiplayer_overlays", 	"011", 				"rank",				eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_2					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_020", 		"multiplayer_overlays", 	"012", 				"rank",				eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_3					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_021", 		"multiplayer_overlays", 	"013", 				"rank",				eFaction,	150, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_FMKILLCHEATER					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_022", 		"multiplayer_overlays", 	"014", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_RACES_WON						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_023", 		"multiplayer_overlays", 	"015", 				"",					eFaction,	150, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_1				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_024", 		"multiplayer_overlays", 	"016", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_2				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_025", 		"multiplayer_overlays", 	"017", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_3				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_026", 		"multiplayer_overlays", 	"018", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_4				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_027", 		"multiplayer_overlays", 	"019", 				"",					eFaction,	150, 		TATTOO_BACK) BREAK
				
				CASE TATTOO_MP_FM_CREW_A						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_100", 		"multiplayer_overlays", 	"000_A", 			"",					eFaction, 	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_CREW_B						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_101", 		"multiplayer_overlays", 	"000_B", 			"",					eFaction, 	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_CREW_C						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_102", 		"multiplayer_overlays", 	"000_C", 			"",					eFaction, 	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_CREW_D						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_103", 		"multiplayer_overlays", 	"000_D", 			"",					eFaction, 	100, 		TATTOO_FRONT) BREAK
				
				CASE TATTOO_MP_FM_01 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_201", 		"multiplayer_overlays", 	"005", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_02 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_202", 		"multiplayer_overlays", 	"006", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_03 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_203", 		"multiplayer_overlays", 	"015", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_04		 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_204", 		"multiplayer_overlays", 	"000", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_05 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_205", 		"multiplayer_overlays", 	"001", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_06 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_206", 		"multiplayer_overlays", 	"003", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_07		 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_207", 		"multiplayer_overlays", 	"014", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_08 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_208", 		"multiplayer_overlays", 	"018", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_09 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_209", 		"multiplayer_overlays", 	"002", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_10		 					FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_210", 		"multiplayer_overlays", 	"007", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_11 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_211", 		"multiplayer_overlays", 	"008", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_12 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_212", 		"multiplayer_overlays", 	"017", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_13 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_213", 		"multiplayer_overlays", 	"009", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_14 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_214", 		"multiplayer_overlays", 	"011", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_15 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_215", 		"multiplayer_overlays", 	"013", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_16 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_216", 		"multiplayer_overlays", 	"016", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_17 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_217", 		"multiplayer_overlays", 	"019", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_18 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_218", 		"multiplayer_overlays", 	"010", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_19 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_219", 		"multiplayer_overlays", 	"004", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_20 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_220", 		"multiplayer_overlays", 	"012", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK

				CASE TATTOO_MP_FM_21 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_221", 		"multiplayer_overlays", 	"020", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_22 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_222", 		"multiplayer_overlays", 	"021", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_23 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_223", 		"multiplayer_overlays", 	"022", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_24 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_224", 		"multiplayer_overlays", 	"023", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_25 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_225", 		"multiplayer_overlays", 	"024", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_26 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_226", 		"multiplayer_overlays", 	"025", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_27 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_227", 		"multiplayer_overlays", 	"026", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_28 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_228", 		"multiplayer_overlays", 	"027", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_29 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_229", 		"multiplayer_overlays", 	"028", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_30 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_230", 		"multiplayer_overlays", 	"029", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_31 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_231", 		"multiplayer_overlays", 	"030", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_32 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_232", 		"multiplayer_overlays", 	"031", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_33 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_233", 		"multiplayer_overlays", 	"032", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_34 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_234", 		"multiplayer_overlays", 	"033", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_35 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_235", 		"multiplayer_overlays", 	"034", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_36 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_236", 		"multiplayer_overlays", 	"035", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_37 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_237", 		"multiplayer_overlays", 	"036", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_38 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_238", 		"multiplayer_overlays", 	"037", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_39 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_239", 		"multiplayer_overlays", 	"038", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_40 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_240", 		"multiplayer_overlays", 	"039", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_41 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_241", 		"multiplayer_overlays", 	"040", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_42 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_242", 		"multiplayer_overlays", 	"041", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_43 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_243", 		"multiplayer_overlays", 	"042", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_44 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_244", 		"multiplayer_overlays", 	"043", 				"",					eFaction,	100, 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_45 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_245", 		"multiplayer_overlays", 	"044", 				"",					eFaction,	100, 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_46 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_246", 		"multiplayer_overlays", 	"045", 				"",					eFaction,	100, 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_47 							FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "TAT_FM_247", 		"multiplayer_overlays", 	"047", 				"",					eFaction,	100, 		TATTOO_LEFT) BREAK
			ENDSWITCH
			
			//----------- Freemode male---------------
			IF ePedModel = MP_M_FREEMODE_01
			
				// Male Torso Decals
				IF eTattoo =  TATTOO_MP_FM_MTD_1 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_001", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_2 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_002", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_3 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_003", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_4 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_004", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_5 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_005", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_6 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_006", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_7 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_007", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_8 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_008", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_9 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_009", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_10 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_010", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_11 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_011", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_12 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_012", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_13						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_013", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_14 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_014", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_15 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_015", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_16 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_016", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_17 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_017", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_18 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_018", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_19 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_019", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_20 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_020", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_22 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_022", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_23 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_023", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_24 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_024", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_25 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_025", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_27 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_027", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_28 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_028", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_29 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_029", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_31 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_031", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_32 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_032", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_34 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_034", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_35 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_035", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_36 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_036", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_37 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_037", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_38 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_038", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_39 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_039", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_40 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_040", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_41 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_041", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_42 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_042", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_43 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_043", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_44 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_044", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_45 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_045", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_46 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_046", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_MTD_47 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_047", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				
				// Tshirt transfers
				ELIF eTattoo =  TSHIRT_TRANS_MP_FM_REDSKULL				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_000", 	"",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo =  TSHIRT_TRANS_MP_FM_LSBELLE 				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_001", 	"",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo =  TSHIRT_TRANS_MP_FM_ROCKSTAR 			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_002", 	"",	eFaction,	100, 		TATTOO_FRONT) 

				ENDIF
	
			//------- Freemode female----------------
			ELIF ePedModel = MP_F_FREEMODE_01
				// Female Torso Decals
				IF eTattoo = TATTOO_MP_FM_FTD_27 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_027_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_28 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_028_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_34 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_034_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_36 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_036_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 	
				ELIF eTattoo = TATTOO_MP_FM_FTD_39 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_039_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_48 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_048", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_49 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_049", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_50 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_050", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_51 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_051", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_52 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_052", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_53 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_053", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_54 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_054", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_55 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_055", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_56 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_056", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_57 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_057", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_58 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_058", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_59 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_059", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_60 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_060", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_61 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_061", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_62 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_062", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_66 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_066", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_67 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_067", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_68 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_068", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_69						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_069",	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo = TATTOO_MP_FM_FTD_70 						FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_070", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 
			
				// Tshirt transfers
				ELIF eTattoo =  TSHIRT_TRANS_MP_FM_REDSKULL				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_000", 	"",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo =  TSHIRT_TRANS_MP_FM_LSBELLE 				FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_001", 	"",	eFaction,	100, 		TATTOO_FRONT) 
				ELIF eTattoo =  TSHIRT_TRANS_MP_FM_ROCKSTAR 			FILL_TATTOO_DATA(sTattooData, eTattoo, pedID, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_002", 	"",	eFaction,	100, 		TATTOO_FRONT) 

				ENDIF
			ENDIF
		BREAK
		#ENDIF
		
	ENDSWITCH
	
	#IF NOT USE_SP_DLC
	#IF USE_TU_CHANGES
		IF (eFaction = TATTOO_MP_FM OR eFaction = TATTOO_MP_FM_F)
		AND eTattoo >= TATTOO_MP_FM_DLC
			FILL_TATTOO_DATA_DLC(sTattooData, eFaction, eTattoo, TATTOO_MP_FM_DLC)
		ENDIF
	#ENDIF
	#ENDIF
	
	// Double cost in the Vinewood shop as it's a famous venue. B* 1199479
	IF eInShop = TATTOO_PARLOUR_01_HW
		sTattooData.iCost = sTattooData.iCost * 2
	ENDIF
	
	RETURN (sTattooData.eEnum != INVALID_TATTOO)
ENDFUNC

FUNC BOOL GET_TATTOO_DATA_USING_MODEL(TATTOO_DATA_STRUCT &sTattooData, TATTOO_NAME_ENUM eTattoo, TATTOO_FACTION_ENUM eFaction, MODEL_NAMES ePedModel, SHOP_NAME_ENUM eInShop = EMPTY_SHOP)
	
	FILL_TATTOO_DATA_USING_MODEL(sTattooData, INVALID_TATTOO, DUMMY_MODEL_FOR_SCRIPT, "", "", "", "", eFaction, -1)
	
	SWITCH eFaction		
		
		CASE TATTOO_MP_FM
		CASE TATTOO_MP_FM_F
			SWITCH eTattoo
				// Freemode																						Text label			Collection					Preset				Upgrade Group		Faction		Cost		Camera
				CASE TATTOO_MP_FM_HEAD_BANGER 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_008", 		"multiplayer_overlays", 	"000", 				"",					eFaction,	ROUND((TO_FLOAT(20000) * g_sMPTunables.ftattoo_mp_fm_head_banger_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_SLAYER						FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_009", 		"multiplayer_overlays", 	"001", 				"",					eFaction,	ROUND((TO_FLOAT(1400) * g_sMPTunables.ftattoo_mp_fm_slayer_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_GANGHIDEOUT_CLEAR				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_010", 		"multiplayer_overlays", 	"002", 				"",					eFaction,	ROUND((TO_FLOAT(9750) * g_sMPTunables.ftattoo_mp_fm_ganghideout_clear_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_011", 		"multiplayer_overlays", 	"003", 				"",					eFaction,	ROUND((TO_FLOAT(2150) * g_sMPTunables.ftattoo_mp_fm_armoured_van_takedown_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_HUSTLER						FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_012", 		"multiplayer_overlays", 	"004", 				"",					eFaction,	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_hustler_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				
				CASE TATTOO_MP_FM_WIN_EVER_MODE_ONCE			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_013", 		"multiplayer_overlays", 	"005", 				"",					eFaction,	ROUND((TO_FLOAT(12400) * g_sMPTunables.ftattoo_mp_fm_win_ever_mode_once_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_BOUNTY_KILLER					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_014", 		"multiplayer_overlays", 	"006", 				"",					eFaction,	ROUND((TO_FLOAT(3500) * g_sMPTunables.ftattoo_mp_fm_bounty_killer_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_HOLD_WORLD_RECORD				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_015", 		"multiplayer_overlays", 	"007", 				"",					eFaction,	ROUND((TO_FLOAT(4950) * g_sMPTunables.ftattoo_mp_fm_hold_world_record_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_FULL_MODDED					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_016", 		"multiplayer_overlays", 	"008", 				"",					eFaction,	ROUND((TO_FLOAT(1350) * g_sMPTunables.ftattoo_mp_fm_full_modded_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_REVENGE_KILL					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_017", 		"multiplayer_overlays", 	"009", 				"",					eFaction,	ROUND((TO_FLOAT(1450) * g_sMPTunables.ftattoo_mp_fm_revenge_kill_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_KILL_3_RACERS					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_018", 		"multiplayer_overlays", 	"010", 				"",					eFaction,	ROUND((TO_FLOAT(2700) * g_sMPTunables.ftattoo_mp_fm_kill_3_racers_expenditure_tunable)), 		TATTOO_BACK_RIGHT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_1					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_019", 		"multiplayer_overlays", 	"011", 				"rank",				eFaction,	ROUND((TO_FLOAT(1200) * g_sMPTunables.ftattoo_mp_fm_reach_rank_1_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_2					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_020", 		"multiplayer_overlays", 	"012", 				"rank",				eFaction,	ROUND((TO_FLOAT(1500) * g_sMPTunables.ftattoo_mp_fm_reach_rank_2_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_REACH_RANK_3					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_021", 		"multiplayer_overlays", 	"013", 				"rank",				eFaction,	ROUND((TO_FLOAT(2650) * g_sMPTunables.ftattoo_mp_fm_reach_rank_3_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_FMKILLCHEATER					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_022", 		"multiplayer_overlays", 	"014", 				"",					eFaction,	ROUND((TO_FLOAT(1900) * g_sMPTunables.ftattoo_mp_fm_fmkillcheater_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_RACES_WON						FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_023", 		"multiplayer_overlays", 	"015", 				"",					eFaction,	ROUND((TO_FLOAT(4950) * g_sMPTunables.ftattoo_mp_fm_races_won_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_1				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_024", 		"multiplayer_overlays", 	"016", 				"",					eFaction,	ROUND((TO_FLOAT(2400) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_1_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_2				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_025", 		"multiplayer_overlays", 	"017", 				"",					eFaction,	ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_2_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_3				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_026", 		"multiplayer_overlays", 	"018", 				"",					eFaction,	ROUND((TO_FLOAT(7400) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_3_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_HOLD_UP_SHOPS_4				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_027", 		"multiplayer_overlays", 	"019", 				"",					eFaction,	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_hold_up_shops_4_expenditure_tunable)), 		TATTOO_BACK) BREAK
				
				CASE TATTOO_MP_FM_01 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_201", 		"multiplayer_overlays", 	"005", 				"",					eFaction,	 ROUND((TO_FLOAT(2400) * g_sMPTunables.ftattoo_mp_fm_01_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_02 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_202", 		"multiplayer_overlays", 	"006", 				"",					eFaction,	 ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_02_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_03 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_203", 		"multiplayer_overlays", 	"015", 				"",					eFaction,	 ROUND((TO_FLOAT(3600) * g_sMPTunables.ftattoo_mp_fm_03_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_04		 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_204", 		"multiplayer_overlays", 	"000", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_04_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_05 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_205", 		"multiplayer_overlays", 	"001", 				"",					eFaction,	 ROUND((TO_FLOAT(12500) * g_sMPTunables.ftattoo_mp_fm_05_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_06 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_206", 		"multiplayer_overlays", 	"003", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_06_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_07		 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_207", 		"multiplayer_overlays", 	"014", 				"",					eFaction,	 ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_07_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_08 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_208", 		"multiplayer_overlays", 	"018", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_08_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_09 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_209", 		"multiplayer_overlays", 	"002", 				"",					eFaction,	 ROUND((TO_FLOAT(3750) * g_sMPTunables.ftattoo_mp_fm_09_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_10		 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_210", 		"multiplayer_overlays", 	"007", 				"",					eFaction,	 ROUND((TO_FLOAT(3750) * g_sMPTunables.ftattoo_mp_fm_10_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_11 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_211", 		"multiplayer_overlays", 	"008", 				"",					eFaction,	 ROUND((TO_FLOAT(4800) * g_sMPTunables.ftattoo_mp_fm_11_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_12 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_212", 		"multiplayer_overlays", 	"017", 				"",					eFaction,	 ROUND((TO_FLOAT(3500) * g_sMPTunables.ftattoo_mp_fm_12_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_13 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_213", 		"multiplayer_overlays", 	"009", 				"",					eFaction,	 ROUND((TO_FLOAT(12350) * g_sMPTunables.ftattoo_mp_fm_13_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_14 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_214", 		"multiplayer_overlays", 	"011", 				"",					eFaction,	 ROUND((TO_FLOAT(1900) * g_sMPTunables.ftattoo_mp_fm_14_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_15 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_215", 		"multiplayer_overlays", 	"013", 				"",					eFaction,	 ROUND((TO_FLOAT(4500) * g_sMPTunables.ftattoo_mp_fm_15_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_16 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_216", 		"multiplayer_overlays", 	"016", 				"",					eFaction,	 ROUND((TO_FLOAT(12250) * g_sMPTunables.ftattoo_mp_fm_16_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_17 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_217", 		"multiplayer_overlays", 	"019", 				"",					eFaction,	 ROUND((TO_FLOAT(12300) * g_sMPTunables.ftattoo_mp_fm_17_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_18 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_218", 		"multiplayer_overlays", 	"010", 				"",					eFaction,	 ROUND((TO_FLOAT(2500) * g_sMPTunables.ftattoo_mp_fm_18_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_19 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_219", 		"multiplayer_overlays", 	"004", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_19_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_20 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_220", 		"multiplayer_overlays", 	"012", 				"",					eFaction,	 ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_20_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_21 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_221", 		"multiplayer_overlays", 	"020", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_21_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_22 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_222", 		"multiplayer_overlays", 	"021", 				"",					eFaction,	 ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_22_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_23 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_223", 		"multiplayer_overlays", 	"022", 				"",					eFaction,	 ROUND((TO_FLOAT(7300) * g_sMPTunables.ftattoo_mp_fm_23_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_24 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_224", 		"multiplayer_overlays", 	"023", 				"",					eFaction,	 ROUND((TO_FLOAT(7250) * g_sMPTunables.ftattoo_mp_fm_24_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_25 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_225", 		"multiplayer_overlays", 	"024", 				"",					eFaction,	 ROUND((TO_FLOAT(11900) * g_sMPTunables.ftattoo_mp_fm_25_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_26 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_226", 		"multiplayer_overlays", 	"025", 				"",					eFaction,	 ROUND((TO_FLOAT(2750) * g_sMPTunables.ftattoo_mp_fm_26_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_27 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_227", 		"multiplayer_overlays", 	"026", 				"",					eFaction,	 ROUND((TO_FLOAT(1750) * g_sMPTunables.ftattoo_mp_fm_27_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_28 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_228", 		"multiplayer_overlays", 	"027", 				"",					eFaction,	 ROUND((TO_FLOAT(7300) * g_sMPTunables.ftattoo_mp_fm_28_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_29 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_229", 		"multiplayer_overlays", 	"028", 				"",					eFaction,	 ROUND((TO_FLOAT(3250) * g_sMPTunables.ftattoo_mp_fm_29_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_30 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_230", 		"multiplayer_overlays", 	"029", 				"",					eFaction,	 ROUND((TO_FLOAT(1000) * g_sMPTunables.ftattoo_mp_fm_30_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_31 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_231", 		"multiplayer_overlays", 	"030", 				"",					eFaction,	 ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_31_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_32 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_232", 		"multiplayer_overlays", 	"031", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_32_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_33 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_233", 		"multiplayer_overlays", 	"032", 				"",					eFaction,	 ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_33_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_34 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_234", 		"multiplayer_overlays", 	"033", 				"",					eFaction,	 ROUND((TO_FLOAT(5050) * g_sMPTunables.ftattoo_mp_fm_34_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_35 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_235", 		"multiplayer_overlays", 	"034", 				"",					eFaction,	 ROUND((TO_FLOAT(2450) * g_sMPTunables.ftattoo_mp_fm_35_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_36 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_236", 		"multiplayer_overlays", 	"035", 				"",					eFaction,	 ROUND((TO_FLOAT(4950) * g_sMPTunables.ftattoo_mp_fm_36_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_37 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_237", 		"multiplayer_overlays", 	"036", 				"",					eFaction,	 ROUND((TO_FLOAT(5100) * g_sMPTunables.ftattoo_mp_fm_37_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_38 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_238", 		"multiplayer_overlays", 	"037", 				"",					eFaction,	 ROUND((TO_FLOAT(12250) * g_sMPTunables.ftattoo_mp_fm_38_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_39 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_239", 		"multiplayer_overlays", 	"038", 				"",					eFaction,	 ROUND((TO_FLOAT(1150) * g_sMPTunables.ftattoo_mp_fm_39_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_40 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_240", 		"multiplayer_overlays", 	"039", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_40_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_41 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_241", 		"multiplayer_overlays", 	"040", 				"",					eFaction,	 ROUND((TO_FLOAT(7600) * g_sMPTunables.ftattoo_mp_fm_41_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_42 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_242", 		"multiplayer_overlays", 	"041", 				"",					eFaction,	 ROUND((TO_FLOAT(2600) * g_sMPTunables.ftattoo_mp_fm_42_expenditure_tunable)), 		TATTOO_LEFT) BREAK
				CASE TATTOO_MP_FM_43 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_243", 		"multiplayer_overlays", 	"042", 				"",					eFaction,	 ROUND((TO_FLOAT(2500) * g_sMPTunables.ftattoo_mp_fm_43_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_44 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_244", 		"multiplayer_overlays", 	"043", 				"",					eFaction,	 ROUND((TO_FLOAT(7450) * g_sMPTunables.ftattoo_mp_fm_44_expenditure_tunable)), 		TATTOO_RIGHT) BREAK
				CASE TATTOO_MP_FM_45 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_245", 		"multiplayer_overlays", 	"044", 				"",					eFaction,	 ROUND((TO_FLOAT(7500) * g_sMPTunables.ftattoo_mp_fm_45_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				CASE TATTOO_MP_FM_46 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_246", 		"multiplayer_overlays", 	"045", 				"",					eFaction,	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_46_expenditure_tunable)), 		TATTOO_BACK) BREAK
				CASE TATTOO_MP_FM_47 							FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_247", 		"multiplayer_overlays", 	"047", 				"",					eFaction,	ROUND((TO_FLOAT(2500) * g_sMPTunables.ftattoo_mp_fm_47_expenditure_tunable)), 		TATTOO_LEFT) BREAK
			ENDSWITCH
			
			//----------- Freemode male---------------
			IF ePedModel = MP_M_FREEMODE_01
			
				SWITCH eTattoo
					// Male Torso Decals
					CASE TATTOO_MP_FM_MTD_1 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_001", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_2 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_002", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_3 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_003", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_4 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_004", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_5 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_005", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_6 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_006", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					
					
					CASE TATTOO_MP_FM_MTD_9 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_009", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_13					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_013", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_14 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_014", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_15 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_015", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_16 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_016", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_19 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_019", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_20 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_020", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_36 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_036", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_17 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_017", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_18 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_018", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_46 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_046", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MTD_45 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_045", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					
					// Tshirt transfers
					CASE  TSHIRT_TRANS_MP_FM_REDSKULL			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_000", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_redskull_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE  TSHIRT_TRANS_MP_FM_LSBELLE 			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_001", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_lsbelle_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE  TSHIRT_TRANS_MP_FM_ROCKSTAR 			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_002", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_rockstar_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					
					// Hair overlays
					CASE TATTOO_MP_FM_MHO_1A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_002", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_003", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_004", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_1E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_005", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_A				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_006", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_B				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_007", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_C				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_008", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_D				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_009", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_LONG_E				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_010", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_011", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_012", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_013", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_014", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_3E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NG_M_Hair_015", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_6A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGBea_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_MHO_6B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGBea_M_Hair_001", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_6C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGBus_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_6D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGBus_M_Hair_001", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_6E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGHip_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGHip_M_Hair_001", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"NGInd_M_Hair_000", "hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"not_in_use", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"not_in_use", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_MHO_8E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"not_in_use", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_HO_FUZZ					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"mpBeach_overlays", 		"FM_Hair_Fuzz", 	"hairOverlay",	eFaction, 	100, 		TATTOO_FRONT)  BREAK
					
					CASE TATTOO_MP_FM_CREW_A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_100", 		"multiplayer_overlays", 	"000_A", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_a_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_101", 		"multiplayer_overlays", 	"000_B", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_b_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FMM_CLB", 		"multiplayer_overlays", 	"000_C", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_c_expenditure_tunable)), 		TATTOO_LEFT) BREAK
					CASE TATTOO_MP_FM_CREW_D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_100", 		"multiplayer_overlays", 	"000_D", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_101", 		"multiplayer_overlays", 	"000_E", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_F					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FMM_CLB", 		"multiplayer_overlays", 	"000_F", 			"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				ENDSWITCH
	
			//------- Freemode female----------------
			ELIF ePedModel = MP_F_FREEMODE_01
				SWITCH eTattoo
					// Female Torso Decals
					CASE TATTOO_MP_FM_FTD_27 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_027_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_28 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_028_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_34 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_034_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_36 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_036_f", "torsoDecal",	eFaction,	100, 		TATTOO_FRONT) 	BREAK
					CASE TATTOO_MP_FM_FTD_48 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_048", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_52 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_052", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_53 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_053", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_54 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_054", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_55 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_055", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_56 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_056", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_58 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_058", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_67 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_067", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_68 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_068", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FTD_51 					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"mp_fm_branding_051", 	"torsoDecal",	eFaction,	100, 		TATTOO_FRONT) BREAK
					
					// Tshirt transfers
					CASE TSHIRT_TRANS_MP_FM_REDSKULL			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_000", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_redskull_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE TSHIRT_TRANS_MP_FM_LSBELLE 			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_001", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_lsbelle_expenditure_tunable)), 		TATTOO_FRONT)  BREAK
					CASE TSHIRT_TRANS_MP_FM_ROCKSTAR 			FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 			"multiplayer_overlays", 	"FM_Tshirt_Award_F_002", 	"torsoDecal",	eFaction,	ROUND((TO_FLOAT(100) * g_sMPTunables.ftshirt_trans_mp_fm_rockstar_expenditure_tunable)), 		TATTOO_FRONT)  BREAK

					// Hair overlays
					CASE TATTOO_MP_FM_FHO_LONG_A				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_001", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_B				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_002", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_C				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_003", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_D				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_004", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_LONG_E				FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_005", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_006", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_007", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_008", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_009", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_3E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_010", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_011", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_012", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_013", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_014", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_5E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NG_F_Hair_015", 		"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGBea_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGBea_F_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGBus_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGBus_F_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_6E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGHip_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGHip_F_Hair_001", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"NGInd_F_Hair_000", 	"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_13E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_FHO_14E					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"multiplayer_overlays", 	"not_in_use", 			"hairOverlay",	eFaction,	100, 		TATTOO_FRONT)  BREAK
					CASE TATTOO_MP_FM_HO_FUZZ					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "NO_LABEL", 		"mpBeach_overlays", 		"FM_Hair_Fuzz", 		"hairOverlay",	eFaction, 	100, 		TATTOO_FRONT)  BREAK
					
					CASE TATTOO_MP_FM_CREW_A					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_100", 	"multiplayer_overlays", 	"000_A", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_a_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_B					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_100", 	"multiplayer_overlays", 	"000_B", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(5000) * g_sMPTunables.ftattoo_mp_fm_crew_b_expenditure_tunable)), 		TATTOO_FRONT) BREAK
					CASE TATTOO_MP_FM_CREW_C					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_101", 	"multiplayer_overlays", 	"000_C", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_c_expenditure_tunable)), 		TATTOO_LEFT) BREAK
					CASE TATTOO_MP_FM_CREW_D					FILL_TATTOO_DATA_USING_MODEL(sTattooData, eTattoo, ePedModel, "TAT_FM_101", 	"multiplayer_overlays", 	"000_D", 				"crewLogo",		eFaction, 	ROUND((TO_FLOAT(10000) * g_sMPTunables.ftattoo_mp_fm_crew_d_expenditure_tunable)), 		TATTOO_FRONT) BREAK
				ENDSWITCH
			ENDIF
		BREAK
				
	ENDSWITCH
	
	
	IF (eFaction = TATTOO_MP_FM OR eFaction = TATTOO_MP_FM_F)
	AND eTattoo >= TATTOO_MP_FM_DLC
		FILL_TATTOO_DATA_DLC(sTattooData, eFaction, eTattoo, TATTOO_MP_FM_DLC)
	ENDIF
		
	// Double cost in the Vinewood shop as it's a famous venue. B* 1199479
	IF eInShop = TATTOO_PARLOUR_01_HW
		sTattooData.iCost = sTattooData.iCost * 2
	ENDIF
	
	RETURN (sTattooData.eEnum != INVALID_TATTOO)
ENDFUNC
#IF USE_TU_CHANGES
FUNC TATTOO_GROUP_ENUM GET_TATTOO_GROUP_FROM_LABEL(MODEL_NAMES ePedModel, STRING sLabel, INT iupdateGroup)
	
	SWITCH iUpdateGroup
		CASE HASH("crewLogo")
		CASE HASH("hairOverlay")
		CASE HASH("torsoDecal")
			RETURN TG_INVALID
		BREAK
	ENDSWITCH
	
	SWITCH iUpdateGroup
		CASE HASH("ARM_LEFT_FULL_SLEEVE")		RETURN TG_ARM_LEFT_FULL_SLEEVE BREAK
		CASE HASH("ARM_LEFT_SHORT_SLEEVE")		RETURN TG_ARM_LEFT_SHORT_SLEEVE BREAK
		CASE HASH("ARM_LEFT_LOWER_SLEEVE")		RETURN TG_ARM_LEFT_LOWER_SLEEVE BREAK
		CASE HASH("ARM_LEFT_LOWER_INNER")		RETURN TG_ARM_LEFT_LOWER_INNER BREAK
		CASE HASH("ARM_LEFT_LOWER_OUTER")		RETURN TG_ARM_LEFT_LOWER_OUTER BREAK
		CASE HASH("ARM_LEFT_WRIST")		RETURN TG_ARM_LEFT_WRIST BREAK
		CASE HASH("ARM_LEFT_UPPER_SLEEVE")		RETURN TG_ARM_LEFT_UPPER_SLEEVE BREAK
		CASE HASH("ARM_LEFT_UPPER_TRICEP")		RETURN TG_ARM_LEFT_UPPER_TRICEP BREAK
		CASE HASH("ARM_LEFT_UPPER_SIDE")		RETURN TG_ARM_LEFT_UPPER_SIDE BREAK
		CASE HASH("ARM_LEFT_UPPER_BICEP")		RETURN TG_ARM_LEFT_UPPER_BICEP BREAK
		CASE HASH("ARM_LEFT_SHOULDER")		RETURN TG_ARM_LEFT_SHOULDER BREAK
		CASE HASH("ARM_LEFT_ELBOW")		RETURN TG_ARM_LEFT_ELBOW BREAK
		CASE HASH("ARM_RIGHT_FULL_SLEEVE")		RETURN TG_ARM_RIGHT_FULL_SLEEVE BREAK
		CASE HASH("ARM_RIGHT_SHORT_SLEEVE")		RETURN TG_ARM_RIGHT_SHORT_SLEEVE BREAK
		CASE HASH("ARM_RIGHT_LOWER_SLEEVE")		RETURN TG_ARM_RIGHT_LOWER_SLEEVE BREAK
		CASE HASH("ARM_RIGHT_LOWER_INNER")		RETURN TG_ARM_RIGHT_LOWER_INNER BREAK
		CASE HASH("ARM_RIGHT_LOWER_OUTER")		RETURN TG_ARM_RIGHT_LOWER_OUTER BREAK
		CASE HASH("ARM_RIGHT_WRIST")		RETURN TG_ARM_RIGHT_WRIST BREAK
		CASE HASH("ARM_RIGHT_UPPER_SLEEVE")		RETURN TG_ARM_RIGHT_UPPER_SLEEVE BREAK
		CASE HASH("ARM_RIGHT_UPPER_TRICEP")		RETURN TG_ARM_RIGHT_UPPER_TRICEP BREAK
		CASE HASH("ARM_RIGHT_UPPER_SIDE")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK
		CASE HASH("ARM_RIGHT_UPPER_BICEP")		RETURN TG_ARM_RIGHT_UPPER_BICEP BREAK
		CASE HASH("ARM_RIGHT_SHOULDER")		RETURN TG_ARM_RIGHT_SHOULDER BREAK
		CASE HASH("ARM_RIGHT_ELBOW")		RETURN TG_ARM_RIGHT_ELBOW BREAK
		CASE HASH("HAND_LEFT")		RETURN TG_HAND_LEFT BREAK
		CASE HASH("HAND_RIGHT")		RETURN TG_HAND_RIGHT BREAK
		CASE HASH("BACK_FULL")		RETURN TG_BACK_FULL BREAK
		CASE HASH("BACK_FULL_ARMS_FULL_BACK")		RETURN TG_BACK_FULL_ARMS_FULL_BACK BREAK
		CASE HASH("BACK_FULL_SHORT")		RETURN TG_BACK_FULL_SHORT BREAK
		CASE HASH("BACK_MID")		RETURN TG_BACK_MID BREAK
		CASE HASH("BACK_UPPER")		RETURN TG_BACK_UPPER BREAK
		CASE HASH("BACK_UPPER_LEFT")		RETURN TG_BACK_UPPER_LEFT BREAK
		CASE HASH("BACK_UPPER_RIGHT")		RETURN TG_BACK_UPPER_RIGHT BREAK
		CASE HASH("BACK_LOWER")		RETURN TG_BACK_LOWER BREAK
		CASE HASH("BACK_LOWER_LEFT")		RETURN TG_BACK_LOWER_LEFT BREAK
		CASE HASH("BACK_LOWER_MID")		RETURN TG_BACK_LOWER_MID BREAK
		CASE HASH("BACK_LOWER_RIGHT")		RETURN TG_BACK_LOWER_RIGHT BREAK
		CASE HASH("CHEST_FULL")		RETURN TG_CHEST_FULL BREAK
		CASE HASH("CHEST_STOM")		RETURN TG_CHEST_STOM BREAK
		CASE HASH("CHEST_STOM_FULL")		RETURN TG_CHEST_STOM_FULL BREAK
		CASE HASH("CHEST_LEFT")		RETURN TG_CHEST_LEFT BREAK
		CASE HASH("CHEST_UPPER_LEFT")		RETURN TG_CHEST_UPPER_LEFT BREAK
		CASE HASH("CHEST_RIGHT")		RETURN TG_CHEST_RIGHT BREAK
		CASE HASH("CHEST_UPPER_RIGHT")		RETURN TG_CHEST_UPPER_RIGHT BREAK
		CASE HASH("CHEST_MID")		RETURN TG_CHEST_MID BREAK
		CASE HASH("TORSO_SIDE_RIGHT")		RETURN TG_TORSO_SIDE_RIGHT BREAK
		CASE HASH("TORSO_SIDE_LEFT")		RETURN TG_TORSO_SIDE_LEFT BREAK
		CASE HASH("STOMACH_FULL")		RETURN TG_STOMACH_FULL BREAK
		CASE HASH("STOMACH_UPPER")		RETURN TG_STOMACH_UPPER BREAK
		CASE HASH("STOMACH_UPPER_LEFT")		RETURN TG_STOMACH_UPPER_LEFT BREAK
		CASE HASH("STOMACH_UPPER_RIGHT")		RETURN TG_STOMACH_UPPER_RIGHT BREAK
		CASE HASH("STOMACH_LOWER")		RETURN TG_STOMACH_LOWER BREAK
		CASE HASH("STOMACH_LOWER_LEFT")		RETURN TG_STOMACH_LOWER_LEFT BREAK
		CASE HASH("STOMACH_LOWER_RIGHT")		RETURN TG_STOMACH_LOWER_RIGHT BREAK
		CASE HASH("STOMACH_LEFT")		RETURN TG_STOMACH_LEFT BREAK
		CASE HASH("STOMACH_RIGHT")		RETURN TG_STOMACH_RIGHT BREAK
		CASE HASH("FACE")		RETURN TG_FACE BREAK
		CASE HASH("HEAD_LEFT")		RETURN TG_HEAD_LEFT BREAK
		CASE HASH("HEAD_RIGHT")		RETURN TG_HEAD_RIGHT BREAK
		CASE HASH("NECK_FRONT")		RETURN TG_NECK_FRONT BREAK
		CASE HASH("NECK_BACK")		RETURN TG_NECK_BACK BREAK
		CASE HASH("NECK_LEFT_FULL")		RETURN TG_NECK_LEFT_FULL BREAK
		CASE HASH("NECK_LEFT_BACK")		RETURN TG_NECK_LEFT_BACK BREAK
		CASE HASH("NECK_RIGHT_FULL")		RETURN TG_NECK_RIGHT_FULL BREAK
		CASE HASH("NECK_RIGHT_BACK")		RETURN TG_NECK_RIGHT_BACK BREAK
		CASE HASH("LEG_LEFT_FULL_SLEEVE")		RETURN TG_LEG_LEFT_FULL_SLEEVE BREAK
		CASE HASH("LEG_LEFT_FULL_FRONT")		RETURN TG_LEG_LEFT_FULL_FRONT BREAK
		CASE HASH("LEG_LEFT_FULL_BACK")		RETURN TG_LEG_LEFT_FULL_BACK BREAK
		CASE HASH("LEG_LEFT_UPPER_SLEEVE")		RETURN TG_LEG_LEFT_UPPER_SLEEVE BREAK
		CASE HASH("LEG_LEFT_UPPER_FRONT")		RETURN TG_LEG_LEFT_UPPER_FRONT BREAK
		CASE HASH("LEG_LEFT_UPPER_BACK")		RETURN TG_LEG_LEFT_UPPER_BACK BREAK
		CASE HASH("LEG_LEFT_UPPER_OUTER")		RETURN TG_LEG_LEFT_UPPER_OUTER BREAK
		CASE HASH("LEG_LEFT_UPPER_INNER")		RETURN TG_LEG_LEFT_UPPER_INNER BREAK
		CASE HASH("LEG_LEFT_LOWER_SLEEVE")		RETURN TG_LEG_LEFT_LOWER_SLEEVE BREAK
		CASE HASH("LEG_LEFT_LOWER_FRONT")		RETURN TG_LEG_LEFT_LOWER_FRONT BREAK
		CASE HASH("LEG_LEFT_LOWER_BACK")		RETURN TG_LEG_LEFT_LOWER_BACK BREAK
		CASE HASH("LEG_LEFT_LOWER_OUTER")		RETURN TG_LEG_LEFT_LOWER_OUTER BREAK
		CASE HASH("LEG_LEFT_LOWER_INNER")		RETURN TG_LEG_LEFT_LOWER_INNER BREAK
		CASE HASH("LEG_LEFT_KNEE")		RETURN TG_LEG_LEFT_KNEE BREAK
		CASE HASH("LEG_LEFT_ANKLE")		RETURN TG_LEG_LEFT_ANKLE BREAK
		CASE HASH("LEG_LEFT_CALF")		RETURN TG_LEG_LEFT_CALF BREAK
		CASE HASH("LEG_RIGHT_FULL_SLEEVE")		RETURN TG_LEG_RIGHT_FULL_SLEEVE BREAK
		CASE HASH("LEG_RIGHT_FULL_FRONT")		RETURN TG_LEG_RIGHT_FULL_FRONT BREAK
		CASE HASH("LEG_RIGHT_FULL_BACK")		RETURN TG_LEG_RIGHT_FULL_BACK BREAK
		CASE HASH("LEG_RIGHT_UPPER_SLEEVE")		RETURN TG_LEG_RIGHT_UPPER_SLEEVE BREAK
		CASE HASH("LEG_RIGHT_UPPER_FRONT")		RETURN TG_LEG_RIGHT_UPPER_FRONT BREAK
		CASE HASH("LEG_RIGHT_UPPER_BACK")		RETURN TG_LEG_RIGHT_UPPER_BACK BREAK
		CASE HASH("LEG_RIGHT_UPPER_OUTER")		RETURN TG_LEG_RIGHT_UPPER_OUTER BREAK
		CASE HASH("LEG_RIGHT_UPPER_INNER")		RETURN TG_LEG_RIGHT_UPPER_INNER BREAK
		CASE HASH("LEG_RIGHT_LOWER_SLEEVE")		RETURN TG_LEG_RIGHT_LOWER_SLEEVE BREAK
		CASE HASH("LEG_RIGHT_LOWER_FRONT")		RETURN TG_LEG_RIGHT_LOWER_FRONT BREAK
		CASE HASH("LEG_RIGHT_LOWER_BACK")		RETURN TG_LEG_RIGHT_LOWER_BACK BREAK
		CASE HASH("LEG_RIGHT_LOWER_OUTER")		RETURN TG_LEG_RIGHT_LOWER_OUTER BREAK
		CASE HASH("LEG_RIGHT_LOWER_INNER")		RETURN TG_LEG_RIGHT_LOWER_INNER BREAK
		CASE HASH("LEG_RIGHT_KNEE")		RETURN TG_LEG_RIGHT_KNEE BREAK
		CASE HASH("LEG_RIGHT_ANKLE")		RETURN TG_LEG_RIGHT_ANKLE BREAK
		CASE HASH("LEG_RIGHT_CALF")		RETURN TG_LEG_RIGHT_CALF BREAK
		CASE HASH("FOOT_LEFT")		RETURN TG_FOOT_LEFT BREAK
		CASE HASH("FOOT_RIGHT")		RETURN TG_FOOT_RIGHT BREAK
	ENDSWITCH
	
	SWITCH GET_HASH_KEY(sLabel)
		CASE HASH("NO_LABEL")		RETURN TG_INVALID BREAK
		
		CASE HASH("TAT_FM_204")		RETURN TG_ARM_RIGHT_FULL_SLEEVE BREAK	// Brotherhood								
		CASE HASH("TAT_FM_205")		RETURN TG_ARM_RIGHT_FULL_SLEEVE BREAK	// Dragons 								
		CASE HASH("TAT_FM_209")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_LEG_LEFT_LOWER_BACK ELSE RETURN TG_LEG_LEFT_FULL_SLEEVE ENDIF BREAK	// Melting Skull	 
		CASE HASH("TAT_FM_221")		RETURN TG_BACK_FULL BREAK	// Dragon								
		CASE HASH("TAT_FM_219")		RETURN TG_STOMACH_FULL BREAK	// Faith								
		CASE HASH("TAT_FM_201")		RETURN TG_ARM_LEFT_SHORT_SLEEVE BREAK	// Serpents								
		CASE HASH("TAT_FM_222")		RETURN TG_LEG_LEFT_LOWER_BACK BREAK	// Serpent Skull								
		CASE HASH("TAT_FM_202")		RETURN TG_ARM_LEFT_SHORT_SLEEVE BREAK	// Oriental Mural								
		CASE HASH("TAT_FM_210")		RETURN TG_LEG_RIGHT_LOWER_SLEEVE BREAK	// The Warrior								
		CASE HASH("TAT_FM_211")		RETURN TG_LEG_LEFT_LOWER_SLEEVE BREAK	// Dragon Mural								
		CASE HASH("TAT_FM_247")		RETURN TG_ARM_RIGHT_SHOULDER BREAK	// Lion								
		CASE HASH("TAT_FM_223")		RETURN TG_LEG_RIGHT_LOWER_BACK BREAK	// Fiery Dragon								
		CASE HASH("TAT_FM_213")		RETURN TG_BACK_FULL BREAK	// Skull on the Cross								
		CASE HASH("TAT_FM_224")		RETURN TG_LEG_LEFT_LOWER_SLEEVE BREAK	// Hottie								
		CASE HASH("TAT_FM_225")		RETURN TG_CHEST_STOM BREAK	// Flaming Cross								
		CASE HASH("TAT_FM_218")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_CHEST_LEFT ELSE RETURN TG_CHEST_RIGHT ENDIF BREAK	// LS Flames	 
		CASE HASH("TAT_FM_214")		RETURN TG_BACK_UPPER_LEFT BREAK	// LS Script								
		CASE HASH("TAT_FM_226")		RETURN TG_CHEST_RIGHT BREAK	// LS Bold								
		CASE HASH("TAT_FM_220")		RETURN TG_STOMACH_FULL BREAK	// Los Santos Bills								
		CASE HASH("TAT_FM_227")		RETURN TG_LEG_LEFT_LOWER_BACK BREAK	// Smoking Dagger 								
		CASE HASH("TAT_FM_228")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Virgin Mary								
		CASE HASH("TAT_FM_215")		RETURN TG_BACK_UPPER_RIGHT BREAK	// Eagle and Serpent								
		CASE HASH("TAT_FM_229")		RETURN TG_ARM_RIGHT_LOWER_SLEEVE BREAK	// Mermaid								
		CASE HASH("TAT_FM_230")		RETURN TG_STOMACH_LOWER_LEFT BREAK	// Trinity Knot								
		CASE HASH("TAT_FM_231")		RETURN TG_BACK_UPPER BREAK	// Lucky Celtic Dogs								
		CASE HASH("TAT_FM_232")		RETURN TG_ARM_LEFT_UPPER_SLEEVE BREAK	// Lady M								
		CASE HASH("TAT_FM_233")		RETURN TG_LEG_LEFT_CALF BREAK	// Faith								
		CASE HASH("TAT_FM_207")		RETURN TG_ARM_RIGHT_SHORT_SLEEVE BREAK	// Flower Mural 								
		CASE HASH("TAT_FM_234")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_LEG_LEFT_FULL_BACK ELSE RETURN TG_LEG_LEFT_LOWER_BACK ENDIF 	BREAK	// Chinese Dragon
		CASE HASH("TAT_FM_235")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_CHEST_LEFT ELSE RETURN TG_CHEST_RIGHT ENDIF BREAK	// Flaming Shamrock 
		CASE HASH("TAT_FM_236")		RETURN TG_LEG_LEFT_LOWER_SLEEVE BREAK	// Dragon								
		CASE HASH("TAT_FM_237")		RETURN TG_STOMACH_FULL BREAK	// Way of the gun								
		CASE HASH("TAT_FM_238")		RETURN TG_LEG_LEFT_LOWER_SLEEVE BREAK	// Grim Reaper								
		CASE HASH("TAT_FM_203")		RETURN TG_ARM_LEFT_SHOULDER BREAK	// Zodiac Skull								
		CASE HASH("TAT_FM_216")		RETURN TG_BACK_FULL BREAK	// Evil Clown								
		CASE HASH("TAT_FM_011")		RETURN TG_CHEST_RIGHT BREAK	// Blackjack								
		CASE HASH("TAT_FM_212")		RETURN TG_LEG_RIGHT_LOWER_SLEEVE BREAK	// Tribal								
		CASE HASH("TAT_FM_239")		RETURN TG_ARM_RIGHT_LOWER_OUTER BREAK	// Dagger								
		CASE HASH("TAT_FM_240")		RETURN TG_LEG_RIGHT_LOWER_BACK BREAK	// Broken Skull								
		CASE HASH("TAT_FM_241")		RETURN TG_LEG_RIGHT_FULL_FRONT BREAK	// Flaming Skull								
		CASE HASH("TAT_FM_208")		RETURN TG_ARM_RIGHT_SHORT_SLEEVE BREAK	// Serpent Skull								
		CASE HASH("TAT_FM_017")		RETURN TG_LEG_LEFT_LOWER_BACK BREAK	// Dragon and Dagger								
		CASE HASH("TAT_FM_217")		RETURN TG_BACK_FULL BREAK	// The Wages of Sin								
		CASE HASH("TAT_FM_242")		RETURN TG_ARM_LEFT_SHOULDER BREAK	// Dope skull								
		CASE HASH("TAT_FM_243")		RETURN TG_LEG_RIGHT_LOWER_FRONT BREAK	// Flaming Scorpion								
		CASE HASH("TAT_FM_244")		RETURN TG_LEG_RIGHT_LOWER_SLEEVE BREAK	// Indian Ram								
		CASE HASH("TAT_FM_245")		RETURN TG_CHEST_STOM BREAK	// Stone Cross								
		CASE HASH("TAT_FM_010")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Grim Reaper Smoking Gun								
		CASE HASH("TAT_FM_246")		RETURN TG_BACK_FULL BREAK	// Skulls and Rose 								
		CASE HASH("TAT_FM_206")		RETURN TG_ARM_RIGHT_UPPER_SLEEVE BREAK	// Dragons and Skull								
		CASE HASH("TAT_FM_008")		RETURN TG_FACE BREAK	// Skull								
		CASE HASH("TAT_FM_009")		RETURN TG_ARM_LEFT_LOWER_INNER BREAK	// Burning Heart								
		CASE HASH("TAT_FM_012")		RETURN TG_STOMACH_FULL BREAK	// Hustler								
		CASE HASH("TAT_FM_013")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_BACK_FULL_SHORT ELSE RETURN TG_BACK_FULL ENDIF BREAK	// Angel	BACK_FULL 
		CASE HASH("TAT_FM_014")		RETURN TG_LEG_RIGHT_LOWER_SLEEVE BREAK	// Skull and Sword								
		CASE HASH("TAT_FM_015")		RETURN TG_ARM_LEFT_UPPER_SLEEVE BREAK	// Racing Blonde								
		CASE HASH("TAT_FM_016")		RETURN TG_BACK_LOWER BREAK	// Los Santos Customs								
		CASE HASH("TAT_FM_018")		RETURN TG_ARM_RIGHT_LOWER_OUTER BREAK	// Ride or Die								
		CASE HASH("TAT_FM_019")		RETURN TG_CHEST_LEFT BREAK	// Blank Scroll								
		CASE HASH("TAT_FM_020")		RETURN TG_CHEST_LEFT BREAK	// Embellished Scroll								
		CASE HASH("TAT_FM_021")		RETURN TG_CHEST_LEFT BREAK	// Seven Deadly Sins								
		CASE HASH("TAT_FM_022")		RETURN TG_BACK_LOWER BREAK	// Trust No One								
		CASE HASH("TAT_FM_023")		RETURN TG_ARM_LEFT_UPPER_SLEEVE BREAK	// Racing Brunette								
		CASE HASH("TAT_FM_024")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_BACK_FULL_SHORT ELSE RETURN TG_BACK_FULL ENDIF BREAK	// Clown	 
		CASE HASH("TAT_FM_025")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_BACK_FULL_SHORT ELSE RETURN TG_BACK_FULL ENDIF BREAK	// Clown and Gun	
		CASE HASH("TAT_FM_026")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_BACK_FULL_SHORT ELSE RETURN TG_BACK_FULL ENDIF BREAK	// Clown Dual Wield	 
		CASE HASH("TAT_FM_027")		IF ePedModel = MP_M_FREEMODE_01 RETURN TG_BACK_FULL_SHORT ELSE RETURN TG_BACK_FULL ENDIF BREAK	// Clown Dual Wield Dollars	 
		CASE HASH("TAT_BB_000")		RETURN TG_CHEST_MID BREAK	// Los Santos Wreath								
		CASE HASH("TAT_BB_001")		RETURN TG_BACK_UPPER_RIGHT BREAK	// Hibiscus Flower Duo								
		CASE HASH("TAT_BB_002")		RETURN TG_ARM_LEFT_LOWER_INNER BREAK	// Tribal Flower								
		CASE HASH("TAT_BB_003")		RETURN TG_BACK_LOWER BREAK	// Rock Solid								
		CASE HASH("TAT_BB_004")		RETURN TG_BACK_UPPER_LEFT BREAK	// Catfish								
		CASE HASH("TAT_BB_005")		RETURN TG_BACK_LOWER BREAK	// Shrimp								
		CASE HASH("TAT_BB_006")		RETURN TG_TORSO_SIDE_RIGHT BREAK	// Love Dagger								
		CASE HASH("TAT_BB_007")		RETURN TG_LEG_RIGHT_LOWER_SLEEVE BREAK	// School of Fish								
		CASE HASH("TAT_BB_008")		RETURN TG_NECK_RIGHT_BACK BREAK	// Tribal Butterfly								
		CASE HASH("TAT_BB_009")		RETURN TG_STOMACH_LOWER_RIGHT BREAK	// Hibiscus Flower								
		CASE HASH("TAT_BB_010")		RETURN TG_STOMACH_LOWER_LEFT BREAK	// Dolphin								
		CASE HASH("TAT_BB_011")		RETURN TG_BACK_UPPER BREAK	// Sea Horses								
		CASE HASH("TAT_BB_012")		RETURN TG_CHEST_UPPER_LEFT BREAK	// Anchor								
		CASE HASH("TAT_BB_013")		RETURN TG_CHEST_UPPER_RIGHT BREAK	// Anchor								
		CASE HASH("TAT_BB_014")		RETURN TG_STOMACH_LOWER_LEFT BREAK	// Swallow								
		CASE HASH("TAT_BB_015")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Tribal Fish								
		CASE HASH("TAT_BB_016")		RETURN TG_ARM_LEFT_UPPER_SIDE BREAK	// Parrot								
		CASE HASH("TAT_BB_017")		RETURN TG_ARM_LEFT_SHOULDER BREAK	// Mermaid L.S.								
		CASE HASH("TAT_BB_018")		RETURN TG_BACK_FULL_SHORT BREAK	// Ship Arms								
		CASE HASH("TAT_BB_019")		RETURN TG_CHEST_LEFT BREAK	// Tribal Hammerhead								
		CASE HASH("TAT_BB_020")		RETURN TG_CHEST_RIGHT BREAK	// Tribal Shark								
		CASE HASH("TAT_BB_021")		RETURN TG_FACE BREAK	// Pirate Skull								
		CASE HASH("TAT_BB_022")		RETURN TG_HEAD_LEFT BREAK	// Surf LS								
		CASE HASH("TAT_BB_023")		RETURN TG_STOMACH_LOWER_LEFT BREAK	// Swordfish								
		CASE HASH("TAT_BB_024")		RETURN TG_ARM_LEFT_UPPER_SIDE BREAK	// Tiki Tower								
		CASE HASH("TAT_BB_025")		RETURN TG_LEG_RIGHT_LOWER_BACK BREAK	// Tribal Tiki Tower								
		CASE HASH("TAT_BB_026")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Tribal Sun								
		CASE HASH("TAT_BB_027")		RETURN TG_LEG_LEFT_LOWER_BACK BREAK	// Tribal Star								
		CASE HASH("TAT_BB_028")		RETURN TG_NECK_LEFT_BACK BREAK	// Little Fish								
		CASE HASH("TAT_BB_029")		RETURN TG_NECK_RIGHT_FULL BREAK	// Surfs Up								
		CASE HASH("TAT_BB_030")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Vespucci Beauty								
		CASE HASH("TAT_BB_031")		RETURN TG_HEAD_RIGHT BREAK	// Shark								
		CASE HASH("TAT_BB_032")		RETURN TG_STOMACH_FULL BREAK	// Wheel								
		CASE HASH("TAT_BUS_F_002")	RETURN TG_CHEST_FULL BREAK	// High Roller									
		CASE HASH("TAT_BUS_F_000")	RETURN TG_BACK_LOWER BREAK	// Respect									
		CASE HASH("TAT_BUS_F_006")	RETURN TG_LEG_LEFT_CALF BREAK	// Single									
		CASE HASH("TAT_BUS_F_007")	RETURN TG_NECK_RIGHT_BACK BREAK	// Val-de-Grace Logo									
		CASE HASH("TAT_BUS_F_008")	RETURN TG_NECK_LEFT_BACK BREAK	// Money Rose									
		CASE HASH("TAT_BUS_F_010")	RETURN TG_LEG_RIGHT_CALF BREAK	// Diamond Crown									
		CASE HASH("TAT_BUS_F_009")	RETURN TG_ARM_RIGHT_LOWER_INNER BREAK	// Dollar Sign									
		CASE HASH("TAT_BUS_F_011")	RETURN TG_STOMACH_UPPER BREAK	// Diamond Back									
		CASE HASH("TAT_BUS_F_003")	RETURN TG_CHEST_FULL BREAK	// Makin' Money									
		CASE HASH("TAT_BUS_F_001")	RETURN TG_BACK_LOWER BREAK	// Gold Digger									
		CASE HASH("TAT_BUS_F_005")	RETURN TG_ARM_LEFT_LOWER_INNER BREAK	// Greed is Good									
		CASE HASH("TAT_BUS_F_004")	RETURN TG_CHEST_UPPER_LEFT BREAK	// Love Money									
		CASE HASH("TAT_BUS_F_012")	RETURN TG_STOMACH_LOWER_LEFT BREAK	// Santo Capra Logo									
		CASE HASH("TAT_BUS_F_013")	RETURN TG_STOMACH_LOWER_RIGHT BREAK	// Money Bag									
		CASE HASH("TAT_BUS_005")	RETURN TG_NECK_FRONT BREAK	// Cash King									
		CASE HASH("TAT_BUS_003")	RETURN TG_ARM_LEFT_LOWER_INNER BREAK	// $100 Bill									
		CASE HASH("TAT_BUS_011")	RETURN TG_STOMACH_UPPER BREAK	// Refined Hustler									
		CASE HASH("TAT_BUS_000")	RETURN TG_BACK_LOWER BREAK	// Makin' Paper									
		CASE HASH("TAT_BUS_006")	RETURN TG_NECK_RIGHT_BACK BREAK	// Bold Dollar Sign									
		CASE HASH("TAT_BUS_007")	RETURN TG_NECK_LEFT_BACK BREAK	// Script Dollar Sign									
		CASE HASH("TAT_BUS_008")	RETURN TG_NECK_BACK BREAK	// $100									
		CASE HASH("TAT_BUS_009")	RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Dollar Skull									
		CASE HASH("TAT_BUS_001")	RETURN TG_CHEST_LEFT BREAK	// Rich									
		CASE HASH("TAT_BUS_002")	RETURN TG_CHEST_RIGHT BREAK	// $$$									
		CASE HASH("TAT_BUS_004")	RETURN TG_ARM_LEFT_ELBOW BREAK	// All-Seeing Eye									
		CASE HASH("TAT_BUS_010")	RETURN TG_ARM_RIGHT_LOWER_INNER BREAK	// Green									
		CASE HASH("TAT_BUS_012")	RETURN TG_CHEST_LEFT BREAK	// Crew Emblem									
		CASE HASH("TAT_BUS_013")	RETURN TG_ARM_RIGHT_SHOULDER BREAK	// Crew Emblem									
		CASE HASH("TAT_BUS_F_014")	RETURN TG_CHEST_LEFT BREAK	// Crew Emblem									
		CASE HASH("TAT_BUS_F_015")	RETURN TG_ARM_RIGHT_SHOULDER BREAK	// Crew Emblem									
		CASE HASH("TAT_HP_000")		RETURN TG_BACK_UPPER BREAK	// Crossed Arrows								
		CASE HASH("TAT_HP_001")		RETURN TG_ARM_RIGHT_UPPER_TRICEP BREAK	// Single Arrow								
		CASE HASH("TAT_HP_002")		RETURN TG_CHEST_LEFT BREAK	// Chemistry								
		CASE HASH("TAT_HP_003")		RETURN TG_ARM_LEFT_LOWER_OUTER BREAK	// Diamond Sparkle								
		CASE HASH("TAT_HP_004")		RETURN TG_ARM_RIGHT_LOWER_INNER BREAK	// Bone								
		CASE HASH("TAT_HP_005")		RETURN TG_NECK_LEFT_BACK BREAK	// Beautiful Eye								
		CASE HASH("TAT_HP_006")		RETURN TG_TORSO_SIDE_RIGHT BREAK	// Feather Birds								
		CASE HASH("TAT_HP_007")		RETURN TG_HAND_LEFT BREAK	// Bricks								
		CASE HASH("TAT_HP_008")		RETURN TG_ARM_RIGHT_SHOULDER BREAK	// Cube								
		CASE HASH("TAT_HP_009")		RETURN TG_LEG_LEFT_CALF BREAK	// Squares								
		CASE HASH("TAT_HP_010")		RETURN TG_HAND_RIGHT BREAK	// Horseshoe								
		CASE HASH("TAT_HP_011")		RETURN TG_BACK_UPPER BREAK	// Infinity								
		CASE HASH("TAT_HP_012")		RETURN TG_BACK_UPPER BREAK	// Antlers								
		CASE HASH("TAT_HP_013")		RETURN TG_CHEST_MID BREAK	// Boombox								
		CASE HASH("TAT_HP_014")		RETURN TG_ARM_RIGHT_LOWER_INNER BREAK	// Spray Can								
		CASE HASH("TAT_HP_015")		RETURN TG_ARM_LEFT_UPPER_BICEP BREAK	// Mustache								
		CASE HASH("TAT_HP_016")		RETURN TG_ARM_LEFT_LOWER_INNER BREAK	// Lightning Bolt								
		CASE HASH("TAT_HP_017")		RETURN TG_ARM_RIGHT_LOWER_OUTER BREAK	// Eye Triangle								
		CASE HASH("TAT_HP_018")		RETURN TG_ARM_RIGHT_WRIST BREAK	// Origami								
		CASE HASH("TAT_HP_019")		RETURN TG_LEG_LEFT_LOWER_BACK BREAK	// Charm								
		CASE HASH("TAT_HP_020")		RETURN TG_ARM_RIGHT_ELBOW BREAK	// Geo Pattern								
		CASE HASH("TAT_HP_021")		RETURN TG_NECK_RIGHT_BACK BREAK	// Geo Fox								
		CASE HASH("TAT_HP_022")		RETURN TG_ARM_RIGHT_LOWER_OUTER BREAK	// Pencil								
		CASE HASH("TAT_HP_023")		RETURN TG_HAND_RIGHT BREAK	// Smiley								
		CASE HASH("TAT_HP_024")		RETURN TG_BACK_UPPER_RIGHT BREAK	// Pyramid								
		CASE HASH("TAT_HP_025")		RETURN TG_BACK_UPPER_LEFT BREAK	// Watch Your Step								
		CASE HASH("TAT_HP_026")		RETURN TG_ARM_LEFT_SHOULDER BREAK	// Pizza								
		CASE HASH("TAT_HP_027")		RETURN TG_ARM_LEFT_WRIST BREAK	// Padlock								
		CASE HASH("TAT_HP_028")		RETURN TG_ARM_LEFT_LOWER_OUTER BREAK	// Thorny Rose								
		CASE HASH("TAT_HP_029")		RETURN TG_STOMACH_LOWER_LEFT BREAK	// Sad								
		CASE HASH("TAT_HP_030")		RETURN TG_BACK_LOWER_MID BREAK	// Shark Fin								
		CASE HASH("TAT_HP_031")		RETURN TG_BACK_UPPER BREAK	// Skateboard								
		CASE HASH("TAT_HP_032")		RETURN TG_BACK_UPPER_RIGHT BREAK	// Paper Plane								
		CASE HASH("TAT_HP_033")		RETURN TG_CHEST_STOM BREAK	// Stag								
		CASE HASH("TAT_HP_034")		RETURN TG_HAND_LEFT BREAK	// Stop								
		CASE HASH("TAT_HP_035")		RETURN TG_STOMACH_LOWER_LEFT BREAK	// Sewn Heart								
		CASE HASH("TAT_HP_036")		RETURN TG_ARM_RIGHT_UPPER_SIDE BREAK	// Shapes								
		CASE HASH("TAT_HP_037")		RETURN TG_ARM_LEFT_SHOULDER BREAK	// Sunrise								
		CASE HASH("TAT_HP_038")		RETURN TG_LEG_RIGHT_LOWER_OUTER BREAK	// Grub								
		CASE HASH("TAT_HP_039")		RETURN TG_ARM_LEFT_FULL_SLEEVE BREAK	// Sleeve								
		CASE HASH("TAT_HP_040")		RETURN TG_LEG_LEFT_CALF BREAK	// Black Anchor								
		CASE HASH("TAT_HP_041")		RETURN TG_BACK_LOWER_LEFT BREAK	// Tooth								
		CASE HASH("TAT_HP_042")		RETURN TG_LEG_RIGHT_LOWER_BACK BREAK	// Sparkplug								
		CASE HASH("TAT_HP_043")		RETURN TG_ARM_LEFT_SHOULDER BREAK	// Triangle White								
		CASE HASH("TAT_HP_044")		RETURN TG_ARM_RIGHT_SHOULDER BREAK	// Triangle Black								
		CASE HASH("TAT_HP_045")		RETURN TG_ARM_RIGHT_LOWER_SLEEVE BREAK	// Mesh Band								
		CASE HASH("TAT_HP_046")		RETURN TG_BACK_UPPER BREAK	// Triangles								
		CASE HASH("TAT_HP_047")		RETURN TG_CHEST_LEFT BREAK	// Cassette								
		CASE HASH("TAT_HP_048")		RETURN TG_HAND_LEFT BREAK	// Peace
	ENDSWITCH
	
	IF iUpdateGroup = 0
	AND IS_STRING_NULL_OR_EMPTY(sLabel)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SHOPS, "GET_TATTOO_GROUP_FROM_LABEL - null update group and label passed in.")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		RETURN TG_INVALID 
	ENDIF
	
	SWITCH iUpdateGroup
		CASE HASH("rank")
			RETURN TG_INVALID
		BREAK
	ENDSWITCH

	#IF IS_DEBUG_BUILD
	CASSERTLN(DEBUG_SHOPS, "GET_TATTOO_GROUP_FROM_LABEL - missing update group \"", sLabel, "\", iUpdateGroup: ", iUpdateGroup)
	#ENDIF
	
	RETURN TG_INVALID 
ENDFUNC

FUNC STRING GET_STRING_FROM_TATTOO_GROUP(TATTOO_GROUP_ENUM group)
	SWITCH group
		CASE TG_ARM_LEFT_FULL_SLEEVE	RETURN "ARM_LEFT_FULL_SLEEVE" BREAK
		CASE TG_ARM_LEFT_SHORT_SLEEVE	RETURN "ARM_LEFT_SHORT_SLEEVE" BREAK
		CASE TG_ARM_LEFT_LOWER_SLEEVE	RETURN "ARM_LEFT_LOWER_SLEEVE" BREAK
		CASE TG_ARM_LEFT_LOWER_INNER	RETURN "ARM_LEFT_LOWER_INNER" BREAK
		CASE TG_ARM_LEFT_LOWER_OUTER	RETURN "ARM_LEFT_LOWER_OUTER" BREAK
		CASE TG_ARM_LEFT_WRIST	RETURN "ARM_LEFT_WRIST" BREAK
		CASE TG_ARM_LEFT_UPPER_SLEEVE	RETURN "ARM_LEFT_UPPER_SLEEVE" BREAK
		CASE TG_ARM_LEFT_UPPER_TRICEP	RETURN "ARM_LEFT_UPPER_TRICEP" BREAK
		CASE TG_ARM_LEFT_UPPER_SIDE	RETURN "ARM_LEFT_UPPER_SIDE" BREAK
		CASE TG_ARM_LEFT_UPPER_BICEP	RETURN "ARM_LEFT_UPPER_BICEP" BREAK
		CASE TG_ARM_LEFT_SHOULDER	RETURN "ARM_LEFT_SHOULDER" BREAK
		CASE TG_ARM_LEFT_ELBOW	RETURN "ARM_LEFT_ELBOW" BREAK
		CASE TG_ARM_RIGHT_FULL_SLEEVE	RETURN "ARM_RIGHT_FULL_SLEEVE" BREAK
		CASE TG_ARM_RIGHT_SHORT_SLEEVE	RETURN "ARM_RIGHT_SHORT_SLEEVE" BREAK
		CASE TG_ARM_RIGHT_LOWER_SLEEVE	RETURN "ARM_RIGHT_LOWER_SLEEVE" BREAK
		CASE TG_ARM_RIGHT_LOWER_INNER	RETURN "ARM_RIGHT_LOWER_INNER" BREAK
		CASE TG_ARM_RIGHT_LOWER_OUTER	RETURN "ARM_RIGHT_LOWER_OUTER" BREAK
		CASE TG_ARM_RIGHT_WRIST	RETURN "ARM_RIGHT_WRIST" BREAK
		CASE TG_ARM_RIGHT_UPPER_SLEEVE	RETURN "ARM_RIGHT_UPPER_SLEEVE" BREAK
		CASE TG_ARM_RIGHT_UPPER_TRICEP	RETURN "ARM_RIGHT_UPPER_TRICEP" BREAK
		CASE TG_ARM_RIGHT_UPPER_SIDE	RETURN "ARM_RIGHT_UPPER_SIDE" BREAK
		CASE TG_ARM_RIGHT_UPPER_BICEP	RETURN "ARM_RIGHT_UPPER_BICEP" BREAK
		CASE TG_ARM_RIGHT_SHOULDER	RETURN "ARM_RIGHT_SHOULDER" BREAK
		CASE TG_ARM_RIGHT_ELBOW	RETURN "ARM_RIGHT_ELBOW" BREAK
		CASE TG_HAND_LEFT	RETURN "HAND_LEFT" BREAK
		CASE TG_HAND_RIGHT	RETURN "HAND_RIGHT" BREAK
		CASE TG_BACK_FULL	RETURN "BACK_FULL" BREAK
		CASE TG_BACK_FULL_ARMS_FULL_BACK	RETURN "BACK_FULL_ARMS_FULL_BACK" BREAK
		CASE TG_BACK_FULL_SHORT	RETURN "BACK_FULL_SHORT" BREAK
		CASE TG_BACK_MID	RETURN "BACK_MID" BREAK
		CASE TG_BACK_UPPER	RETURN "BACK_UPPER" BREAK
		CASE TG_BACK_UPPER_LEFT	RETURN "BACK_UPPER_LEFT" BREAK
		CASE TG_BACK_UPPER_RIGHT	RETURN "BACK_UPPER_RIGHT" BREAK
		CASE TG_BACK_LOWER	RETURN "BACK_LOWER" BREAK
		CASE TG_BACK_LOWER_LEFT	RETURN "BACK_LOWER_LEFT" BREAK
		CASE TG_BACK_LOWER_MID	RETURN "BACK_LOWER_MID" BREAK
		CASE TG_BACK_LOWER_RIGHT	RETURN "BACK_LOWER_RIGHT" BREAK
		CASE TG_CHEST_FULL	RETURN "CHEST_FULL" BREAK
		CASE TG_CHEST_STOM	RETURN "CHEST_STOM" BREAK
		CASE TG_CHEST_STOM_FULL	RETURN "CHEST_STOM_FULL" BREAK
		CASE TG_CHEST_LEFT	RETURN "CHEST_LEFT" BREAK
		CASE TG_CHEST_UPPER_LEFT	RETURN "CHEST_UPPER_LEFT" BREAK
		CASE TG_CHEST_RIGHT	RETURN "CHEST_RIGHT" BREAK
		CASE TG_CHEST_UPPER_RIGHT	RETURN "CHEST_UPPER_RIGHT" BREAK
		CASE TG_CHEST_MID	RETURN "CHEST_MID" BREAK
		CASE TG_TORSO_SIDE_RIGHT	RETURN "TORSO_SIDE_RIGHT" BREAK
		CASE TG_TORSO_SIDE_LEFT	RETURN "TORSO_SIDE_LEFT" BREAK
		CASE TG_STOMACH_FULL	RETURN "STOMACH_FULL" BREAK
		CASE TG_STOMACH_UPPER	RETURN "STOMACH_UPPER" BREAK
		CASE TG_STOMACH_UPPER_LEFT	RETURN "STOMACH_UPPER_LEFT" BREAK
		CASE TG_STOMACH_UPPER_RIGHT	RETURN "STOMACH_UPPER_RIGHT" BREAK
		CASE TG_STOMACH_LOWER	RETURN "STOMACH_LOWER" BREAK
		CASE TG_STOMACH_LOWER_LEFT	RETURN "STOMACH_LOWER_LEFT" BREAK
		CASE TG_STOMACH_LOWER_RIGHT	RETURN "STOMACH_LOWER_RIGHT" BREAK
		CASE TG_STOMACH_LEFT	RETURN "STOMACH_LEFT" BREAK
		CASE TG_STOMACH_RIGHT	RETURN "STOMACH_RIGHT" BREAK
		CASE TG_FACE	RETURN "FACE" BREAK
		CASE TG_HEAD_LEFT	RETURN "HEAD_LEFT" BREAK
		CASE TG_HEAD_RIGHT	RETURN "HEAD_RIGHT" BREAK
		CASE TG_NECK_FRONT	RETURN "NECK_FRONT" BREAK
		CASE TG_NECK_BACK	RETURN "NECK_BACK" BREAK
		CASE TG_NECK_LEFT_FULL	RETURN "NECK_LEFT_FULL" BREAK
		CASE TG_NECK_LEFT_BACK	RETURN "NECK_LEFT_BACK" BREAK
		CASE TG_NECK_RIGHT_FULL	RETURN "NECK_RIGHT_FULL" BREAK
		CASE TG_NECK_RIGHT_BACK	RETURN "NECK_RIGHT_BACK" BREAK
		CASE TG_LEG_LEFT_FULL_SLEEVE	RETURN "LEG_LEFT_FULL_SLEEVE" BREAK
		CASE TG_LEG_LEFT_FULL_FRONT	RETURN "LEG_LEFT_FULL_FRONT" BREAK
		CASE TG_LEG_LEFT_FULL_BACK	RETURN "LEG_LEFT_FULL_BACK" BREAK
		CASE TG_LEG_LEFT_UPPER_SLEEVE	RETURN "LEG_LEFT_UPPER_SLEEVE" BREAK
		CASE TG_LEG_LEFT_UPPER_FRONT	RETURN "LEG_LEFT_UPPER_FRONT" BREAK
		CASE TG_LEG_LEFT_UPPER_BACK	RETURN "LEG_LEFT_UPPER_BACK" BREAK
		CASE TG_LEG_LEFT_UPPER_OUTER	RETURN "LEG_LEFT_UPPER_OUTER" BREAK
		CASE TG_LEG_LEFT_UPPER_INNER	RETURN "LEG_LEFT_UPPER_INNER" BREAK
		CASE TG_LEG_LEFT_LOWER_SLEEVE	RETURN "LEG_LEFT_LOWER_SLEEVE" BREAK
		CASE TG_LEG_LEFT_LOWER_FRONT	RETURN "LEG_LEFT_LOWER_FRONT" BREAK
		CASE TG_LEG_LEFT_LOWER_BACK	RETURN "LEG_LEFT_LOWER_BACK" BREAK
		CASE TG_LEG_LEFT_LOWER_OUTER	RETURN "LEG_LEFT_LOWER_OUTER" BREAK
		CASE TG_LEG_LEFT_LOWER_INNER	RETURN "LEG_LEFT_LOWER_INNER" BREAK
		CASE TG_LEG_LEFT_KNEE	RETURN "LEG_LEFT_KNEE" BREAK
		CASE TG_LEG_LEFT_ANKLE	RETURN "LEG_LEFT_ANKLE" BREAK
		CASE TG_LEG_LEFT_CALF	RETURN "LEG_LEFT_CALF" BREAK
		CASE TG_LEG_RIGHT_FULL_SLEEVE	RETURN "LEG_RIGHT_FULL_SLEEVE" BREAK
		CASE TG_LEG_RIGHT_FULL_FRONT	RETURN "LEG_RIGHT_FULL_FRONT" BREAK
		CASE TG_LEG_RIGHT_FULL_BACK	RETURN "LEG_RIGHT_FULL_BACK" BREAK
		CASE TG_LEG_RIGHT_UPPER_SLEEVE	RETURN "LEG_RIGHT_UPPER_SLEEVE" BREAK
		CASE TG_LEG_RIGHT_UPPER_FRONT	RETURN "LEG_RIGHT_UPPER_FRONT" BREAK
		CASE TG_LEG_RIGHT_UPPER_BACK	RETURN "LEG_RIGHT_UPPER_BACK" BREAK
		CASE TG_LEG_RIGHT_UPPER_OUTER	RETURN "LEG_RIGHT_UPPER_OUTER" BREAK
		CASE TG_LEG_RIGHT_UPPER_INNER	RETURN "LEG_RIGHT_UPPER_INNER" BREAK
		CASE TG_LEG_RIGHT_LOWER_SLEEVE	RETURN "LEG_RIGHT_LOWER_SLEEVE" BREAK
		CASE TG_LEG_RIGHT_LOWER_FRONT	RETURN "LEG_RIGHT_LOWER_FRONT" BREAK
		CASE TG_LEG_RIGHT_LOWER_BACK	RETURN "LEG_RIGHT_LOWER_BACK" BREAK
		CASE TG_LEG_RIGHT_LOWER_OUTER	RETURN "LEG_RIGHT_LOWER_OUTER" BREAK
		CASE TG_LEG_RIGHT_LOWER_INNER	RETURN "LEG_RIGHT_LOWER_INNER" BREAK
		CASE TG_LEG_RIGHT_KNEE	RETURN "LEG_RIGHT_KNEE" BREAK
		CASE TG_LEG_RIGHT_ANKLE	RETURN "LEG_RIGHT_ANKLE" BREAK
		CASE TG_LEG_RIGHT_CALF	RETURN "LEG_RIGHT_CALF" BREAK
		CASE TG_FOOT_LEFT	RETURN "FOOT_LEFT" BREAK
		CASE TG_FOOT_RIGHT	RETURN "FOOT_RIGHT" BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL sGroup = ENUM_TO_INT(group)
	CASSERTLN(DEBUG_SHOPS, "GET_STRING_FROM_TATTOO_GROUP unknown group id: ", sGroup)
	RETURN GET_STRING_FROM_STRING(sGroup, 0, GET_LENGTH_OF_LITERAL_STRING(sGroup))
	#ENDIF
	#IF NOT IS_DEBUG_BUILD
	RETURN ""
	#ENDIF
ENDFUNC

FUNC BOOL DO_TATTOO_GROUPS_OVERLAP(TATTOO_GROUP_ENUM groupA, TATTOO_GROUP_ENUM groupB)
	IF groupA = groupB
		RETURN TRUE
	ENDIF
	
	IF groupA = TG_INVALID
	OR groupB = TG_INVALID
		RETURN FALSE
	ENDIF
	
	SWITCH groupA
		CASE TG_ARM_LEFT_FULL_SLEEVE
			SWITCH groupB
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_LOWER_INNER
				CASE TG_ARM_LEFT_LOWER_OUTER
				CASE TG_ARM_LEFT_WRIST
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_TRICEP
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_ARM_LEFT_UPPER_BICEP
				CASE TG_ARM_LEFT_SHOULDER
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_SHORT_SLEEVE
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_LOWER_INNER
				CASE TG_ARM_LEFT_LOWER_OUTER
				CASE TG_ARM_LEFT_WRIST
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_TRICEP
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_ARM_LEFT_UPPER_BICEP
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_LOWER_SLEEVE
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_INNER
				CASE TG_ARM_LEFT_LOWER_OUTER
				CASE TG_ARM_LEFT_WRIST
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_LOWER_INNER
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_WRIST
				CASE TG_ARM_LEFT_ELBOW
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_LOWER_OUTER
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_WRIST
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_LOWER_INNER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_UPPER_SLEEVE
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_UPPER_TRICEP
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_ARM_LEFT_UPPER_BICEP
				CASE TG_ARM_LEFT_SHOULDER
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_UPPER_TRICEP
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_UPPER_SIDE
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_TRICEP
				CASE TG_ARM_LEFT_SHOULDER
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_UPPER_BICEP
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_UPPER_SLEEVE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_SHOULDER
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_LEFT_ELBOW
			SWITCH groupB
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_LOWER_INNER
				CASE TG_ARM_LEFT_LOWER_OUTER
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_TRICEP
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_FULL_SLEEVE
			SWITCH groupB
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_INNER
				CASE TG_ARM_RIGHT_LOWER_OUTER
				CASE TG_ARM_RIGHT_WRIST
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_UPPER_BICEP
				CASE TG_ARM_RIGHT_SHOULDER
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_SHORT_SLEEVE
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_INNER
				CASE TG_ARM_RIGHT_LOWER_OUTER
				CASE TG_ARM_RIGHT_WRIST
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_UPPER_BICEP
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_LOWER_SLEEVE
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_INNER
				CASE TG_ARM_RIGHT_LOWER_OUTER
				CASE TG_ARM_RIGHT_WRIST
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_LOWER_INNER
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_WRIST
				CASE TG_ARM_RIGHT_ELBOW
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_LOWER_OUTER
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_WRIST
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_INNER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_UPPER_SLEEVE
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_UPPER_BICEP
				CASE TG_ARM_RIGHT_SHOULDER
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_UPPER_TRICEP
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_UPPER_SIDE
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_SHOULDER
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_UPPER_BICEP
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_BICEP
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_SHOULDER
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_SHOULDER
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_ARM_RIGHT_ELBOW
			SWITCH groupB
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_INNER
				CASE TG_ARM_RIGHT_LOWER_OUTER
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_ELBOW
				CASE TG_BACK_FULL_ARMS_FULL_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_HAND_LEFT
			SWITCH groupB
				CASE TG_HAND_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_HAND_RIGHT
			SWITCH groupB
				CASE TG_HAND_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_FULL
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_MID
				CASE TG_BACK_UPPER
				CASE TG_BACK_UPPER_LEFT
				CASE TG_BACK_UPPER_RIGHT
				CASE TG_BACK_LOWER
				CASE TG_BACK_LOWER_LEFT
				CASE TG_BACK_LOWER_MID
				CASE TG_BACK_LOWER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_FULL_ARMS_FULL_BACK
			SWITCH groupB
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_MID
				CASE TG_BACK_UPPER
				CASE TG_BACK_UPPER_LEFT
				CASE TG_BACK_UPPER_RIGHT
				CASE TG_BACK_LOWER
				CASE TG_BACK_LOWER_LEFT
				CASE TG_BACK_LOWER_MID
				CASE TG_BACK_LOWER_RIGHT
				CASE TG_ARM_LEFT_FULL_SLEEVE
				CASE TG_ARM_LEFT_SHORT_SLEEVE
				CASE TG_ARM_LEFT_LOWER_SLEEVE
				CASE TG_ARM_LEFT_LOWER_OUTER
				CASE TG_ARM_LEFT_UPPER_SLEEVE
				CASE TG_ARM_LEFT_UPPER_TRICEP
				CASE TG_ARM_LEFT_UPPER_SIDE
				CASE TG_ARM_LEFT_SHOULDER
				CASE TG_ARM_LEFT_ELBOW
				CASE TG_ARM_RIGHT_FULL_SLEEVE
				CASE TG_ARM_RIGHT_SHORT_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_SLEEVE
				CASE TG_ARM_RIGHT_LOWER_OUTER
				CASE TG_ARM_RIGHT_UPPER_SLEEVE
				CASE TG_ARM_RIGHT_UPPER_TRICEP
				CASE TG_ARM_RIGHT_UPPER_SIDE
				CASE TG_ARM_RIGHT_SHOULDER
				CASE TG_ARM_RIGHT_ELBOW
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_FULL_SHORT
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_MID
				CASE TG_BACK_UPPER
				CASE TG_BACK_UPPER_LEFT
				CASE TG_BACK_UPPER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_MID
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_MID
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_UPPER
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_UPPER
				CASE TG_BACK_UPPER_LEFT
				CASE TG_BACK_UPPER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_UPPER_LEFT
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_UPPER
				CASE TG_BACK_UPPER_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_UPPER_RIGHT
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_FULL_SHORT
				CASE TG_BACK_UPPER
				CASE TG_BACK_UPPER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_LOWER
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_LOWER
				CASE TG_BACK_LOWER_LEFT
				CASE TG_BACK_LOWER_MID
				CASE TG_BACK_LOWER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_LOWER_LEFT
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_LOWER
				CASE TG_BACK_LOWER_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_LOWER_MID
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_LOWER
				CASE TG_BACK_LOWER_MID
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_BACK_LOWER_RIGHT
			SWITCH groupB
				CASE TG_BACK_FULL
				CASE TG_BACK_FULL_ARMS_FULL_BACK
				CASE TG_BACK_LOWER
				CASE TG_BACK_LOWER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_FULL
			SWITCH groupB
				CASE TG_CHEST_FULL
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_LEFT
				CASE TG_CHEST_UPPER_LEFT
				CASE TG_CHEST_RIGHT
				CASE TG_CHEST_UPPER_RIGHT
				CASE TG_CHEST_MID
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_STOM
			SWITCH groupB
				CASE TG_CHEST_FULL
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_LEFT
				CASE TG_CHEST_RIGHT
				CASE TG_CHEST_MID
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_LEFT
				CASE TG_STOMACH_UPPER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_STOM_FULL
			SWITCH groupB
				CASE TG_CHEST_FULL
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_LEFT
				CASE TG_CHEST_UPPER_LEFT
				CASE TG_CHEST_RIGHT
				CASE TG_CHEST_UPPER_RIGHT
				CASE TG_CHEST_MID
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_LEFT
				CASE TG_STOMACH_RIGHT
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_LEFT
				CASE TG_STOMACH_UPPER_RIGHT
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_LEFT
				CASE TG_STOMACH_LOWER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_LEFT
			SWITCH groupB
				CASE TG_CHEST_FULL
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_LEFT
				CASE TG_CHEST_UPPER_LEFT
				CASE TG_CHEST_MID
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_UPPER_LEFT
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_FULL
				CASE TG_CHEST_LEFT
				CASE TG_CHEST_UPPER_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_RIGHT
			SWITCH groupB
				CASE TG_CHEST_FULL
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_RIGHT
				CASE TG_CHEST_UPPER_RIGHT
				CASE TG_CHEST_MID
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_UPPER_RIGHT
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_FULL
				CASE TG_CHEST_RIGHT
				CASE TG_CHEST_UPPER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_CHEST_MID
			SWITCH groupB
				CASE TG_CHEST_FULL
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_CHEST_LEFT
				CASE TG_CHEST_RIGHT
				CASE TG_CHEST_MID
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_TORSO_SIDE_RIGHT
			SWITCH groupB
				CASE TG_TORSO_SIDE_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_TORSO_SIDE_LEFT
			SWITCH groupB
				CASE TG_TORSO_SIDE_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_FULL
			SWITCH groupB
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_LEFT
				CASE TG_STOMACH_UPPER_RIGHT
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_LEFT
				CASE TG_STOMACH_LOWER_RIGHT
				CASE TG_STOMACH_LEFT
				CASE TG_STOMACH_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_UPPER
			SWITCH groupB
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_LEFT
				CASE TG_STOMACH_UPPER_RIGHT
				CASE TG_STOMACH_LEFT
				CASE TG_STOMACH_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_UPPER_LEFT
			SWITCH groupB
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_LEFT
				CASE TG_STOMACH_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_UPPER_RIGHT
			SWITCH groupB
				CASE TG_CHEST_STOM
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_RIGHT
				CASE TG_STOMACH_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_LOWER
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_LEFT
				CASE TG_STOMACH_LOWER_RIGHT
				CASE TG_STOMACH_LEFT
				CASE TG_STOMACH_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_LOWER_LEFT
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_LEFT
				CASE TG_STOMACH_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_LOWER_RIGHT
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_RIGHT
				CASE TG_STOMACH_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_LEFT
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_LEFT
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_STOMACH_RIGHT
			SWITCH groupB
				CASE TG_CHEST_STOM_FULL
				CASE TG_STOMACH_FULL
				CASE TG_STOMACH_LOWER
				CASE TG_STOMACH_LOWER_RIGHT
				CASE TG_STOMACH_UPPER
				CASE TG_STOMACH_UPPER_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_FACE
			SWITCH groupB
				CASE TG_FACE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_HEAD_LEFT
			SWITCH groupB
				CASE TG_HEAD_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_HEAD_RIGHT
			SWITCH groupB
				CASE TG_HEAD_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_NECK_FRONT
			SWITCH groupB
				CASE TG_NECK_FRONT
				CASE TG_NECK_LEFT_FULL
				CASE TG_NECK_RIGHT_FULL
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_NECK_BACK
			SWITCH groupB
				CASE TG_NECK_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_NECK_LEFT_FULL
			SWITCH groupB
				CASE TG_NECK_FRONT
				CASE TG_NECK_LEFT_FULL
				CASE TG_NECK_LEFT_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_NECK_LEFT_BACK
			SWITCH groupB
				CASE TG_NECK_LEFT_FULL
				CASE TG_NECK_LEFT_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_NECK_RIGHT_FULL
			SWITCH groupB
				CASE TG_NECK_FRONT
				CASE TG_NECK_RIGHT_FULL
				CASE TG_NECK_RIGHT_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_NECK_RIGHT_BACK
			SWITCH groupB
				CASE TG_NECK_RIGHT_FULL
				CASE TG_NECK_RIGHT_BACK
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_FULL_SLEEVE
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_UPPER_BACK
				CASE TG_LEG_LEFT_UPPER_OUTER
				CASE TG_LEG_LEFT_UPPER_INNER
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_KNEE
				CASE TG_LEG_LEFT_ANKLE
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_FULL_FRONT
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_UPPER_OUTER
				CASE TG_LEG_LEFT_UPPER_INNER
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_KNEE
				CASE TG_LEG_LEFT_ANKLE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_FULL_BACK
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_BACK
				CASE TG_LEG_LEFT_UPPER_OUTER
				CASE TG_LEG_LEFT_UPPER_INNER
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_ANKLE
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_UPPER_SLEEVE
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_UPPER_BACK
				CASE TG_LEG_LEFT_UPPER_OUTER
				CASE TG_LEG_LEFT_UPPER_INNER
				CASE TG_LEG_LEFT_KNEE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_UPPER_FRONT
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_UPPER_OUTER
				CASE TG_LEG_LEFT_UPPER_INNER
				CASE TG_LEG_LEFT_KNEE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_UPPER_BACK
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_BACK
				CASE TG_LEG_LEFT_UPPER_OUTER
				CASE TG_LEG_LEFT_UPPER_INNER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_UPPER_OUTER
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_UPPER_BACK
				CASE TG_LEG_LEFT_UPPER_OUTER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_UPPER_INNER
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_UPPER_BACK
				CASE TG_LEG_LEFT_UPPER_INNER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_LOWER_SLEEVE
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_KNEE
				CASE TG_LEG_LEFT_ANKLE
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_LOWER_FRONT
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_KNEE
				CASE TG_LEG_LEFT_ANKLE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_LOWER_BACK
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_ANKLE
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_LOWER_OUTER
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_LOWER_INNER
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_KNEE
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_UPPER_SLEEVE
				CASE TG_LEG_LEFT_UPPER_FRONT
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_KNEE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_ANKLE
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_FRONT
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_FRONT
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_ANKLE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_LEFT_CALF
			SWITCH groupB
				CASE TG_LEG_LEFT_FULL_SLEEVE
				CASE TG_LEG_LEFT_FULL_BACK
				CASE TG_LEG_LEFT_LOWER_SLEEVE
				CASE TG_LEG_LEFT_LOWER_BACK
				CASE TG_LEG_LEFT_LOWER_OUTER
				CASE TG_LEG_LEFT_LOWER_INNER
				CASE TG_LEG_LEFT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_FULL_SLEEVE
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_UPPER_BACK
				CASE TG_LEG_RIGHT_UPPER_OUTER
				CASE TG_LEG_RIGHT_UPPER_INNER
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_KNEE
				CASE TG_LEG_RIGHT_ANKLE
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_FULL_FRONT
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_UPPER_OUTER
				CASE TG_LEG_RIGHT_UPPER_INNER
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_KNEE
				CASE TG_LEG_RIGHT_ANKLE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_FULL_BACK
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_BACK
				CASE TG_LEG_RIGHT_UPPER_OUTER
				CASE TG_LEG_RIGHT_UPPER_INNER
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_ANKLE
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_UPPER_SLEEVE
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_UPPER_BACK
				CASE TG_LEG_RIGHT_UPPER_OUTER
				CASE TG_LEG_RIGHT_UPPER_INNER
				CASE TG_LEG_RIGHT_KNEE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_UPPER_FRONT
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_UPPER_OUTER
				CASE TG_LEG_RIGHT_UPPER_INNER
				CASE TG_LEG_RIGHT_KNEE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_UPPER_BACK
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_BACK
				CASE TG_LEG_RIGHT_UPPER_OUTER
				CASE TG_LEG_RIGHT_UPPER_INNER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_UPPER_OUTER
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_UPPER_BACK
				CASE TG_LEG_RIGHT_UPPER_OUTER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_UPPER_INNER
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_UPPER_BACK
				CASE TG_LEG_RIGHT_UPPER_INNER
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_LOWER_SLEEVE
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_KNEE
				CASE TG_LEG_RIGHT_ANKLE
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_LOWER_FRONT
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_KNEE
				CASE TG_LEG_RIGHT_ANKLE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_LOWER_BACK
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_ANKLE
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_LOWER_OUTER
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_LOWER_INNER
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_KNEE
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_UPPER_SLEEVE
				CASE TG_LEG_RIGHT_UPPER_FRONT
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_KNEE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_ANKLE
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_FRONT
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_FRONT
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_ANKLE
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_LEG_RIGHT_CALF
			SWITCH groupB
				CASE TG_LEG_RIGHT_FULL_SLEEVE
				CASE TG_LEG_RIGHT_FULL_BACK
				CASE TG_LEG_RIGHT_LOWER_SLEEVE
				CASE TG_LEG_RIGHT_LOWER_BACK
				CASE TG_LEG_RIGHT_LOWER_OUTER
				CASE TG_LEG_RIGHT_LOWER_INNER
				CASE TG_LEG_RIGHT_CALF
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_FOOT_LEFT
			SWITCH groupB
				CASE TG_FOOT_LEFT
					RETURN TRUE
			ENDSWITCH BREAK
		CASE TG_FOOT_RIGHT
			SWITCH groupB
				CASE TG_FOOT_RIGHT
					RETURN TRUE
			ENDSWITCH BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SHOPS, "DO_TATTOO_GROUPS_OVERLAP - missing groupA: ", groupA)
			#ENDIF
			BREAK
	ENDSWITCH
	
	
	RETURN FALSE
ENDFUNC

FUNC TATTOO_NAME_ENUM GET_TATTOO_ENUM_FROM_DLC_HASH(INT iDLCTattooNameHash, TATTOO_FACTION_ENUM eFaction)

	INT iDLCIndex = GET_TATTOO_SHOP_DLC_ITEM_INDEX(eFaction, -1, iDLCTattooNameHash)
	IF iDLCIndex != -1
		RETURN INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
	ENDIF
	
	RETURN INVALID_TATTOO
ENDFUNC

#ENDIF

