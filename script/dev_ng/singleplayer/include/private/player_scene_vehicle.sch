
#IF IS_DEBUG_BUILD
USING "player_ped_debug.sch"
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_vehicle.sch									//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF USE_TU_CHANGES
FUNC BOOL IsPlayerLastVehModelInvalid(enumCharacterList ePed)
	IF (g_sPlayerLastVeh[ePed].model = BLIMP)
	OR (g_sPlayerLastVeh[ePed].model = BLIMP2)
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = SUBMERSIBLE)
	OR (g_sPlayerLastVeh[ePed].model = SUBMERSIBLE2)
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = freight)
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = PACKER)
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = ASEA2)			//Michael1
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = BURRITO2)		//Jewel Heist
	OR (g_sPlayerLastVeh[ePed].model = FBI2)			//Jewel Heist
		RETURN TRUE
	ENDIF
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF (g_sPlayerLastVeh[ePed].model = ENTITYXF)		//Carsteal1
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_1].completed
		RETURN TRUE
	ENDIF
	IF (g_sPlayerLastVeh[ePed].model = CHEETAH)			//Carsteal1
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_1].completed
		RETURN TRUE
	ENDIF
	IF (g_sPlayerLastVeh[ePed].model = POLICEB)			//Carsteal1
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_1].completed
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = ZTYPE)			//Carsteal2
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_2].completed
		RETURN TRUE
	ENDIF
	IF (g_sPlayerLastVeh[ePed].model = POLMAV)			//Carsteal2
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_2].completed
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = JB700)			//Carsteal3
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_3].completed
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = MONROE)			//Carsteal4
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_4].completed
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = FIRETRUK)		//#1618656
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = HANDLER)			//#1618525
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = MONROE)
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = PHANTOM)			//#2078439
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = GAUNTLET)
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_PREP_C1].completed
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_PREP_C2].completed
	AND NOT g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_PREP_C3].completed
		RETURN TRUE
	ENDIF
	
	#ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL GetPostMissionLastVeh(enumCharacterList ePed, PED_VEH_DATA_STRUCT &sVehData, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
	/*
	IF (g_sPlayerLastVeh[ePed].model <> DUMMY_MODEL_FOR_SCRIPT)
		sVehData = g_sPlayerLastVeh[ePed]
		RETURN TRUE
	ELSE
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	*/
	
	IF (g_sPlayerLastVeh[ePed].model = DUMMY_MODEL_FOR_SCRIPT)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	IF (g_sPlayerLastVeh[ePed].model = BLIMP)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	#IF USE_TU_CHANGES
	IF IsPlayerLastVehModelInvalid(ePed)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(g_sPlayerLastVeh[ePed].model)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(g_sPlayerLastVeh[ePed].model)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(g_sPlayerLastVeh[ePed].model)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_TRAIN(g_sPlayerLastVeh[ePed].model)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	IF (eTypePreference = VEHICLE_TYPE_CAR)
		IF NOT IS_THIS_MODEL_A_CAR(g_sPlayerLastVeh[ePed].model)
			GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
			sVehData.eType = eTypePreference
			RETURN TRUE
		ENDIF
	ELIF (eTypePreference = VEHICLE_TYPE_BIKE)
		IF NOT IS_THIS_MODEL_A_BIKE(g_sPlayerLastVeh[ePed].model)
			GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
			sVehData.eType = eTypePreference
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_sPlayerLastVeh[ePed].model)
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		RETURN TRUE
	ENDIF
	
	#IF NOT USE_TU_CHANGES
	GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
	sVehData.eType = eTypePreference
	
	IF (g_sPlayerLastVeh[ePed].model != sVehData.model)
		sVehData = g_sPlayerLastVeh[ePed]
		sVehData.eType = eTypePreference
		RETURN FALSE
	ENDIF
	#ENDIF
	
	#IF USE_TU_CHANGES
	IF eTypePreference != VEHICLE_TYPE_DEFAULT
		GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		sVehData.eType = eTypePreference
		
		IF (g_sPlayerLastVeh[ePed].model != sVehData.model)
			sVehData = g_sPlayerLastVeh[ePed]
			sVehData.eType = VEHICLE_TYPE_DEFAULT
			
			CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - VEHICLE_TYPE_", eTypePreference, " [", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[ePed].model), " != ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
			RETURN FALSE
		ENDIF
		
		CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - VEHICLE_TYPE_", eTypePreference, " [", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[ePed].model), " = ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
	ELSE
		//do players stored car
		GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
		sVehData.eType = VEHICLE_TYPE_CAR
		
		IF (g_sPlayerLastVeh[ePed].model = sVehData.model)
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - DEFAULT//CAR [", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[ePed].model), " = ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
			RETURN TRUE
		ENDIF
		CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - DEFAULT//CAR [", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[ePed].model), " != ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
		
		//do players stored bike
		GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_BIKE)
		sVehData.eType = VEHICLE_TYPE_BIKE
		
		IF (g_sPlayerLastVeh[ePed].model = sVehData.model)
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_BIKE)
			sVehData.eType = VEHICLE_TYPE_BIKE
			
			CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - DEFAULT//BIKE [", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[ePed].model), " = ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
			RETURN TRUE
		ENDIF
		CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - DEFAULT//BIKE [", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[ePed].model), " != ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
		
		sVehData = g_sPlayerLastVeh[ePed]
		sVehData.eType = VEHICLE_TYPE_DEFAULT
		
		CPRINTLN(DEBUG_SWITCH, "GetPostMissionLastVeh(CHAR_", ePed, ") - DEFAULT//FALLTHROUGH!!!")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
	sVehData.eType = eTypePreference
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_PLAYER_VEH_POSITION_FOR_SCENE(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eScene,
		PED_VEH_DATA_STRUCT &sVehData, VECTOR &vVehCoordsOffset, FLOAT &fVehHeadOffset,
		VECTOR &vDriveOffset, FLOAT &fDriveSpeed)
	
	sVehData.fHealth = 1000
	sVehData.modelTrailer = DUMMY_MODEL_FOR_SCRIPT
	
	#IF USE_TU_CHANGES
	sVehData.iTyreR = 255
	sVehData.iTyreG = 255
	sVehData.iTyreB = 255
	#ENDIF
	
	//force Michaels exile car to be the premier!
	#IF IS_DEBUG_BUILD
	IF g_bWarpDebugPlayerCharScene
		IF (ePed = CHAR_MICHAEL)
			SWITCH eScene
				CASE PR_SCENE_Ma_RURAL1
				
				CASE PR_SCENE_M6_CARSLEEP
				
				CASE PR_SCENE_M6_DRIVING_a
				CASE PR_SCENE_M6_DRIVING_b
				CASE PR_SCENE_M6_DRIVING_c
				CASE PR_SCENE_M6_DRIVING_d
				CASE PR_SCENE_M6_DRIVING_e
				CASE PR_SCENE_M6_DRIVING_f
				CASE PR_SCENE_M6_DRIVING_g
				CASE PR_SCENE_M6_DRIVING_h
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
				CASE PR_SCENE_M6_PARKEDHILLS_a
				CASE PR_SCENE_M6_PARKEDHILLS_b
				CASE PR_SCENE_M6_PARKEDHILLS_c
				CASE PR_SCENE_M6_PARKEDHILLS_d
				CASE PR_SCENE_M6_PARKEDHILLS_e
					IF NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT]
						g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT] = TRUE
					ENDIF
					IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED]
						g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED] = FALSE
					ENDIF
				BREAK
				
				DEFAULT
					IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT]
						g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT] = FALSE
					ENDIF
					IF NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED]
						g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED] = TRUE
					ENDIF
				BREAK
				#endif
				#endif
			ENDSWITCH
		ENDIF
	ENDIF
	#ENDIF
	
	SWITCH eScene
		CASE PR_SCENE_M_DEFAULT
			sVehData = g_sPlayerLastVeh[ePed]
			
			IF g_ePlayerLastVehState[ePed] <> PTVS_2_playerInVehicle
				
				#IF USE_TU_CHANGES		//#1798965
				IF IS_POINT_IN_ANGLED_AREA(g_vPlayerLastVehCoord[ePed], <<-829.747803,192.117035,76.732483>>, <<-827.138367,153.859512,59.961723>>, 33.250000)
					IF g_ePlayerLastVehState[ePed] = PTVS_1_playerWithVehicle
						
						vVehCoordsOffset = g_vPlayerLastVehCoord[ePed] - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
						fVehHeadOffset = g_fPlayerLastVehHead[ePed] - g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]
						
						IF VMAG2(vVehCoordsOffset) > (5.0*5.0)
							SCRIPT_ASSERT("fix for #1798965 - reposition Michaels last known vehicle on a default switch if its a long range switch")
							
							vVehCoordsOffset = <<0,0,0>>
							fVehHeadOffset = 0
							RETURN FALSE
						ENDIF
						
					ENDIF
				ENDIF
				#ENDIF
				
				vVehCoordsOffset = g_vPlayerLastVehCoord[ePed] - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
				fVehHeadOffset = g_fPlayerLastVehHead[ePed] - g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]
				
				IF VMAG2(vVehCoordsOffset) < (0.5*0.5)
					vVehCoordsOffset *= 1.5
				ENDIF
			ELSE
				vVehCoordsOffset = <<0,0,0>>
				fVehHeadOffset = 0
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_DEFAULT
			sVehData = g_sPlayerLastVeh[ePed]
			
			IF g_ePlayerLastVehState[ePed] <> PTVS_2_playerInVehicle
				vVehCoordsOffset = g_vPlayerLastVehCoord[ePed] - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
				fVehHeadOffset = g_fPlayerLastVehHead[ePed] - g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]
				
				IF VMAG2(vVehCoordsOffset) < (0.5*0.5)
					vVehCoordsOffset *= 1.5
				ENDIF
			ELSE
				vVehCoordsOffset = <<0,0,0>>
				fVehHeadOffset = 0
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DEFAULT
			sVehData = g_sPlayerLastVeh[ePed]
			
			IF g_ePlayerLastVehState[ePed] <> PTVS_2_playerInVehicle
				vVehCoordsOffset = g_vPlayerLastVehCoord[ePed] - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
				fVehHeadOffset = g_fPlayerLastVehHead[ePed] - g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]
				
				IF VMAG2(vVehCoordsOffset) < (0.5*0.5)
					vVehCoordsOffset *= 1.5
				ENDIF
			ELSE
				vVehCoordsOffset = <<0,0,0>>
				fVehHeadOffset = 0
			ENDIF
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<37.4300, -23.8100, 0.0000>>
			fVehHeadOffset = 127.0000
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_ARM3
//			sVehData = g_sPlayerLastVeh[ePed]
//			vVehCoordsOffset = g_vPlayerLastVehCoordOff[ePed]
//			fVehHeadOffset = g_fPlayerLastVehHeadOff[ePed]
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1
//			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_Fa_STRIPCLUB_ARM3,
//					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3
//			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_Fa_STRIPCLUB_ARM3,
//					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
//		BREAK
		
		
		CASE PR_SCENE_Fa_PHONECALL_ARM3
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = MESA
			
			vVehCoordsOffset = <<-90.961,-1277.560,28.826>>		- <<-90.0089, -1324.1947, 28.3203>>
			fVehHeadOffset = 1.27								- 194.1887
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM1
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_Fa_PHONECALL_ARM3,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM3
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_Fa_PHONECALL_ARM3,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		
//		CASE PR_SCENE_Fa_FAMILY1
//			sVehData.bIsPlayerVehicle = FALSE
//			sVehData.model = SENTINEL2
//			
//			sVehData.iColour1 = 28
//			sVehData.iColour2 = 28
//			sVehData.bColourCombo = FALSE
//			
//			vDriveOffset = << 0,5,0 >>
//			fDriveSpeed = 5.0
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_FAMILY3
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,5,0 >>
			fDriveSpeed = 5.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI1
			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_BIKE)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FBI2
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = FROGGER
			
			sVehData.iColour1 = 34
			sVehData.iColour2 = 34
			sVehData.iColourExtra1 = 0
			sVehData.iColourExtra2 = 0
			sVehData.bColourCombo = FALSE		//1011834 
			
			sVehData.bExtraOn[0] = TRUE
			sVehData.bExtraOn[1] = TRUE
			sVehData.bExtraOn[2] = TRUE
			sVehData.bExtraOn[3] = TRUE
			sVehData.bExtraOn[4] = TRUE
			sVehData.bExtraOn[5] = TRUE
			sVehData.bExtraOn[6] = TRUE
			sVehData.bExtraOn[7] = TRUE
			sVehData.bExtraOn[8] = TRUE
			
			vDriveOffset = << 0,50,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL4
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = MESA
			
			vVehCoordsOffset = <<782.1644, -2965.4985, 4.8006>>	- <<798.4536, -2975.3408, 4.0205>>
			fVehHeadOffset = 246.1684							- 90.0000
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FINALE2intro
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 20.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE2intro
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = penumbra
			
			vVehCoordsOffset = <<66.028,-1431.464,28.764>>		- <<37.5988, -1351.5203, 28.2954>>
			fVehHeadOffset = 141.28								- 90.0339
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Ma_FAMILY1
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = TAXI
			sVehData.fDirtLevel = 0.0
			sVehData.iColourCombo = 0
			sVehData.bColourCombo = TRUE
			
			vDriveOffset = << 0,5,0 >>
			fDriveSpeed = 5.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI4intro
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,15,0 >>
			fDriveSpeed = 5.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FBI4intro
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FBI3
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI4
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,15,0 >>
			fDriveSpeed = 10.0
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_FBI5
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_FBI5
//			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
//			sVehData.eType = VEHICLE_TYPE_CAR
//			
//			vDriveOffset = << 0,20,0 >>
//			fDriveSpeed = 25.0
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_AGENCY1
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_TRAFFIC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_Fa_AGENCYprep1
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCY3B
			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<21.6401, 38.5601, 1.9708>>
			fVehHeadOffset = -84.0000
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FRANKLIN2
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Ta_FRANKLIN2
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Ma_FBI1end
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_MARTIN1
//			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
//			sVehData.eType = VEHICLE_TYPE_CAR
//			
//			vDriveOffset = << 0,20,0 >>
//			fDriveSpeed = 25.0
//			
//			RETURN TRUE
//		BREAK
//		
//		CASE PR_SCENE_Ta_MARTIN1
//			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
//			sVehData.eType = VEHICLE_TYPE_CAR
//			
//			vVehCoordsOffset = << 13.64,-13.48,0.0 >> - <<1.5,-1.5,0>>
//			fVehHeadOffset = -93.0 - 90.0
//			
//			RETURN TRUE
//		BREAK
		
//		CASE PR_SCENE_Ta_FBI4
//			GetPostMissionLastVeh(ePed, sVehData)
//			
//			vDriveOffset = << 0,5,0 >>
//			fDriveSpeed = 5.0
//			
//			RETURN TRUE
//		BREAK
		
//		CASE PR_SCENE_Ma_FAMILY4_a
//			sVehData.bIsPlayerVehicle = FALSE
//			sVehData.model = EMPEROR2
//			
//			vVehCoordsOffset = <<1153.46,-331.57,68.39>>		- <<1179.8916, -287.0152, 67.9908>>
//			fVehHeadOffset = 0.20								- -140.8983
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Ma_FAMILY4_b
//			sVehData.bIsPlayerVehicle = FALSE
//			sVehData.model = ISSI2
//			
//			vVehCoordsOffset = <<173.67, -71.95, 68.01>> 	- <<191.7754, -40.3418, 67.5119>>
//			fVehHeadOffset = 2.76							- -104.5510
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Ta_FAMILY4
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = PHANTOM
			
//			vVehCoordsOffset = <<173.67, -71.95, 68.01>> 	- <<191.7754, -40.3418, 67.5119>>
//			fVehHeadOffset = 2.76							- -104.5510
			
			vDriveOffset = << 0,50,0 >>
			fDriveSpeed = 20.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALEC
			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_BIKE
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 10.0
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_CARSTEAL1
//			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M_TRAFFIC_a,
//					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
//		BREAK
//		CASE PR_SCENE_Ta_NICE1B
//			GetPostMissionLastVeh(ePed, sVehData)
//			
//			vDriveOffset = << 0,5,0 >>
//			fDriveSpeed = 5.0
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_RURAL2A
			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_BIKE)
			sVehData.eType = VEHICLE_TYPE_BIKE
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK

		CASE PR_SCENE_Ma_RURAL1
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-2.7200, 0.4500, 1.0000>>
			fVehHeadOffset = -137.0000
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ta_CARSTEAL1
//			sVehData.bIsPlayerVehicle = FALSE
//			sVehData.model = BOBCATXL
//			
//			vVehCoordsOffset = <<62.2776, -22.6920, -1.0222>>
//			fVehHeadOffset = 33.7354
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_CARSTEAL1
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = blista
			
			vVehCoordsOffset = <<54.081,-1417.047,29.170>>		- <<72.2278, -1464.6798, 28.2915>>
			fVehHeadOffset = 0.72								- 156.8827
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCY2
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = SEMINOLE
			
			vVehCoordsOffset = <<792.3,-938.8,24.9>>		- <<763.0000, -906.0000, 24.2312>>
			fVehHeadOffset = 2.23							- 7.2736
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL1
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = peyote
			
			vVehCoordsOffset = <<306.036,-1090.070,28.701>>		- <<257.9167, -1120.7855, 28.3684>>
			fVehHeadOffset = -1									- 97.2736
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_RC_MRSP2
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			vVehCoordsOffset = << 16.5182, -8.5576, -2.3291>>	//<< 16.5200 , -8.5600 , 0.0 >>
			fVehHeadOffset = 174.2400
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Ta_CARSTEAL2
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = POLMAV
			
			//#556139
			sVehData.bExtraOn[0] = TRUE
			sVehData.bExtraOn[1] = TRUE
			sVehData.bExtraOn[2] = TRUE
			sVehData.bExtraOn[3] = TRUE
			sVehData.bExtraOn[4] = TRUE
			sVehData.bExtraOn[5] = TRUE
			sVehData.bExtraOn[6] = TRUE
			sVehData.bExtraOn[7] = TRUE
			sVehData.bExtraOn[8] = TRUE
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			vVehCoordsOffset = << 16.5182, -8.5576, -2.3291>>	//<< 16.5200 , -8.5600 , 0.0 >>
			fVehHeadOffset = 174.2400
			
			RETURN TRUE
		BREAK
		#ENDIF
		CASE PR_SCENE_M_MD_FBI2
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 2.5,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_MD_FRANKLIN2
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 2.5,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_Fa_NICE2B
//			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_TRAFFIC_a,
//					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
//		BREAK
//		CASE PR_SCENE_Ta_NICE2B
//			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)     sVehData.eType = VEHICLE_TYPE_CAR
//			
//			vVehCoordsOffset = <<-1.9500, 1.6500, 0.0000>>
//			fVehHeadOffset = 90.0000
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1a
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = MESA
			
			vVehCoordsOffset = <<782.1644, -2965.4985, 4.8006>>	- <<798.4536, -2975.3408, 4.0205>>
			fVehHeadOffset = 246.1684							- 90.0000
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_FTa_FRANKLIN1b
//			sVehData.bIsPlayerVehicle = FALSE
//			sVehData.model = TipTruck
//			
//			vVehCoordsOffset = <<796.308,-2940.973,5.510>>	- <<733.2842, -3017.2427, 8.3068>>
//			fVehHeadOffset = -1.65							- 114.8398
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1c
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = MESA
			
			vVehCoordsOffset = <<659.5297, -2912.0430, 5.4446>>	- <<709.0244, -2916.4788, 5.0589>>
			fVehHeadOffset = 247.4806							- 355.3260
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1d
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = sadler
			
			vVehCoordsOffset = <<656.9753, -2936.8425, 5.1176>>	- <<643.5248, -2917.3250, 5.1337>>
			fVehHeadOffset = 253.7760							- 334.1068
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1e
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = Mixer
			
			vVehCoordsOffset = <<593.033,-2769.795,5.681>>	- <<594.4415, -2819.0852, 5.0558>>
			fVehHeadOffset = 2.62							- 299.0519
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_EXILE2
			sVehData.bIsPlayerVehicle = FALSE
			
			sVehData.model = cavalcade
			
			sVehData.iColour1 = 0
			sVehData.iColour2 = 0
			sVehData.iColourExtra1 = 0
			sVehData.iColourExtra2 = 0
			sVehData.bColourCombo = FALSE
			
			#IF USE_TU_CHANGES
			sVehData.tlNumberPlate = "22LJK483"
			#ENDIF

			vVehCoordsOffset = <<0,0,0>>
			fVehHeadOffset = 0
			
			vDriveOffset = << 0,10,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_EXILE3
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << -1.5,35.0,3.0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_EXILE3
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<3.8721, -5.9568, 0.0000>>
			fVehHeadOffset = 164.2466-180
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_MICHAEL3
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,10,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_MICHAEL3
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = STRETCH
			
			vVehCoordsOffset = <<-1170.7203, -550.2679, 28.1755>>		- <<  -1257.5000, -526.9999, 30.2361  >>
			fVehHeadOffset = 310.4708									- 220.9554
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_DOCKS2A
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 12.5
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_DOCKS2A
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 12.5
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_DOCKS2B
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FINALE1
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 25.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCY3A
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-2.9117, -15.0499, -0.3468>>
			fVehHeadOffset = -139.9751
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FINALE2B
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE2A
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = PRAIRIE
			
			vVehCoordsOffset = <<-737.1358, -85.1799, 36.5734>>		- <<  -852, 111, 54 >>
			fVehHeadOffset = 64.1848								- 180.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE2B
		CASE PR_SCENE_Ta_FINALE1
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = FROGGER2
			
			sVehData.iColour1 = 40
			sVehData.iColour2 = 0
			sVehData.iColourExtra1 = 0
			sVehData.iColourExtra2 = 0
			sVehData.bColourCombo = FALSE
			
			sVehData.bExtraOn[0] = TRUE
			sVehData.bExtraOn[1] = TRUE
			sVehData.bExtraOn[2] = TRUE
			sVehData.bExtraOn[3] = TRUE
			sVehData.bExtraOn[4] = TRUE
			sVehData.bExtraOn[5] = TRUE
			sVehData.bExtraOn[6] = TRUE
			sVehData.bExtraOn[7] = TRUE
			sVehData.bExtraOn[8] = TRUE
			
			sVehData.iLIVERY = 1
			
			IF eScene = PR_SCENE_Ta_FINALE1
				vVehCoordsOffset = <<5.5414, -6.6035, 1.0473>>
				fVehHeadOffset = -83.2547
			ENDIF
			IF eScene = PR_SCENE_Ta_FINALE2B
				vVehCoordsOffset = <<5.7209, -12.3958, 1.0746>>
				fVehHeadOffset = -152.2593
			ENDIF
			
			RETURN TRUE
		BREAK
	
		CASE PR_SCENE_M_PARKEDHILLS_a
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>
			fVehHeadOffset = GET_RANDOM_FLOAT_IN_RANGE(-180, 180)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M_PARKEDHILLS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>
				fVehHeadOffset = GET_RANDOM_FLOAT_IN_RANGE(-180, 180)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_CYCLING_a
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = SCORCHER
			sVehData.fDirtLevel = 0.0
			sVehData.iColourCombo = 0
			sVehData.bColourCombo = TRUE
			vVehCoordsOffset = <<0,0,0>>
			fVehHeadOffset = 0
			
			vDriveOffset = << 1.0, 12.5,0 >>
			fDriveSpeed = 5.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_CYCLING_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_CYCLING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				
				vDriveOffset = << 5,20,0 >>
				fDriveSpeed = 5.0
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_CYCLING_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_CYCLING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				
				vDriveOffset = << 0,30,0 >>
				fDriveSpeed = 8.0
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = Cruiser
			
			vVehCoordsOffset = << -0.0, 0.0, 0.0 >>
			fVehHeadOffset = 0.1
			
			vDriveOffset = << 0.1,0.1,0.1 >>
			fDriveSpeed = 0.1
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_PHARMACY
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			vVehCoordsOffset = <<-1.9002, 1.2050, -0.3537>>
			fVehHeadOffset = -180.0000
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_TRAFFIC_a
			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
			#IF NOT USE_TU_CHANGES
			sVehData.eType = VEHICLE_TYPE_CAR
			#ENDIF
			
			vDriveOffset = << 0,0.1,0 >>
			fDriveSpeed = 0.5
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_TRAFFIC_b
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M_TRAFFIC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M_TRAFFIC_c
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M_TRAFFIC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			vVehCoordsOffset = << -0.0, 0.0, 0.0 >>
			fVehHeadOffset = 0.1
			
			vDriveOffset = << 0.1,0.1,0.1 >>
			fDriveSpeed = 0.1
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			vVehCoordsOffset = << -0.0, 0.0, 0.0 >>
			fVehHeadOffset = 0.0
			
			vDriveOffset = << 0.0,0.0,0.0>>
			fDriveSpeed = 0.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_DROPOFFDAU_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_DROPOFFDAU_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_DROPOFFDAU_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M4_PARKEDBEACH
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<3.2267, 1.0966, -0.3540>>
			fVehHeadOffset = -31.7300
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_HOOKERMOTEL
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-13.7322, 1.8783, 1.0593>>
			fVehHeadOffset = 55.3652
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_BOATING
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = TROPIC	//SKIVVY
			sVehData.fDirtLevel = 0.0
			sVehData.iColourCombo = 0
			sVehData.bColourCombo = TRUE
			vVehCoordsOffset = <<0,0,0>>
			fVehHeadOffset = 0
			
			//#954712
			sVehData.bExtraOn[0] = FALSE
			sVehData.bExtraOn[1] = FALSE
			sVehData.bExtraOn[2] = TRUE
			sVehData.bExtraOn[3] = FALSE
			sVehData.bExtraOn[4] = FALSE
			sVehData.bExtraOn[5] = TRUE
			sVehData.bExtraOn[6] = TRUE
			sVehData.bExtraOn[7] = TRUE
			sVehData.bExtraOn[8] = TRUE
			
			vDriveOffset = << 0,20,0 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_CARSLEEP_a
		CASE PR_SCENE_M2_CARSLEEP_b
		CASE PR_SCENE_M6_CARSLEEP
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vDriveOffset = << 0,0,0 >>
			fDriveSpeed = 0.1
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_CANAL_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<15.4538, -8.7110, 5.7900>>
			fVehHeadOffset = 2.4942
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_CANAL_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M_CANAL_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-8.1, -9.01, 0.4439>>
				fVehHeadOffset = 94.03
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_MARINA 
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<18.8468, 40.7721, 0.0000>>
			fVehHeadOffset = -116.3936
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_COFFEE_a
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-531.6035, -33.7052, 43.5170>>		- <<-511.7300, -21.8700, 45.6141>>
			fVehHeadOffset = 177.2590 - 180							- 69.0000
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_COFFEE_b
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<1.7826, 12.2098, -0.6964>>
			fVehHeadOffset = -96.0001
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_COFFEE_c
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<11.8705, -1.4684, -1.6487>>
			fVehHeadOffset = -125.8331
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-13.1578, 16.3962, 0.6396>>
			fVehHeadOffset = -177.0000
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_LUNCH_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-21.0518, 0.5037, 0.4091>>
			fVehHeadOffset = -83.1262
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_VWOODPARK_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<10.8971, 2.0809, -0.7983>>
			fVehHeadOffset = -150.9417
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_VWOODPARK_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<79.9901, -52.8300, -0.3533>>
			fVehHeadOffset = 44.7231
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M6_PARKEDHILLS_a
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>
			fVehHeadOffset = GET_RANDOM_FLOAT_IN_RANGE(-180, 180)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_PARKEDHILLS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>
				fVehHeadOffset = GET_RANDOM_FLOAT_IN_RANGE(-180, 180)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_PARKEDHILLS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>
				fVehHeadOffset = GET_RANDOM_FLOAT_IN_RANGE(-180, 180)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_PARKEDHILLS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>
				fVehHeadOffset = GET_RANDOM_FLOAT_IN_RANGE(-180, 180)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_PARKEDHILLS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<2.4845, 2.3286, -0.3830>>
				fVehHeadOffset = -31.4884
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_M2_DRIVING_a
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,15,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_DRIVING_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,40,0 >>
				fDriveSpeed = 12.5
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			vDriveOffset = << 0,25,0 >>
			fDriveSpeed = 10.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_DRIVING_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,25,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,25,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_d
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,25,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_e
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,25,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_f
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,15,0 >>
				fDriveSpeed = 7.5
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_g
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,25,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_DRIVING_h
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M6_DRIVING_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vDriveOffset = << 0,25,0 >>
				fDriveSpeed = 10.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_M7_RESTAURANT
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_LUNCH_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-13.2213, 16.3310, 0.0000>>
				fVehHeadOffset = 6.0000
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_COFFEE
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M_COFFEE_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<10.3876, -10.3585, -1.2183>>
				fVehHeadOffset = 8.6726
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_HOOKERS
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<1.0793, 15.6310, 1.1744>>
			fVehHeadOffset = 2.5200
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TALKTOGUARD
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_WIFEEXITSCAR,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			vVehCoordsOffset = << -0.0, 0.0, 0.0 >>
			fVehHeadOffset = 0.1
			
			vDriveOffset = << 0.1,0.1,0.1 >>
			fDriveSpeed = 0.1
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_WIFEEXITSCAR,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_M2_WIFEEXITSCAR,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		
		CASE PR_SCENE_F_TRAFFIC_a
			GetPostMissionLastVeh(ePed, sVehData, VEHICLE_TYPE_CAR)
			#IF NOT USE_TU_CHANGES
			sVehData.eType = VEHICLE_TYPE_CAR
			#ENDIF
			vDriveOffset = << 0,0.1,0 >>
			fDriveSpeed = 0.5
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_TRAFFIC_b
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_TRAFFIC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_F_TRAFFIC_c
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_TRAFFIC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		
		CASE PR_SCENE_F0_BIKE
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_BIKE)
			sVehData.eType = VEHICLE_TYPE_BIKE
			
			vVehCoordsOffset = << -2.1900, -1.2300, 0.0000 >>
			fVehHeadOffset = 90.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_CLEANCAR
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			sVehData.fDirtLevel = 0.0
			
			vVehCoordsOffset = <<-1.3547, 2.1615, -0.3723>>
			fVehHeadOffset = 177.8041
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_BIKE
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F0_BIKE,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-4.2075, 1.0943, 0.0000>>
				fVehHeadOffset = 15.8200
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F1_CLEANCAR
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			sVehData.fDirtLevel = 0.0
			
			vVehCoordsOffset = <<2.2910, 1.0879, 0.0635>>
			fVehHeadOffset = 177.7980
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_BYETAXI
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = TAXI
			sVehData.fDirtLevel = 0.0
			sVehData.iColourCombo = 0
			sVehData.bColourCombo = TRUE
			vVehCoordsOffset = <<-0.9714, 1.6112, -0.2773>>
			fVehHeadOffset = -7.0583
			
			vDriveOffset = <<174.7651, 578.5989, 183.8081>>			- <<10.9694, 551.7596, 176.0860>>
			fDriveSpeed = 10.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = TAXI
			sVehData.fDirtLevel = 0.0
			sVehData.iColourCombo = 0
			sVehData.bColourCombo = TRUE
			vVehCoordsOffset = <<-17.4132, -1457.9152, 29.4846>>	- <<-14.8689, -1441.1821, 31.1932>>
			fVehHeadOffset = 89.0026								- -1.5000
			
			vDriveOffset = <<-147.0307, -1563.4609, 33.6125>>		- <<-14.8689, -1441.1821, 31.1932>>
			fDriveSpeed = 10.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CLUB
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-531.4000, -33.5909, 43.5168>>		- <<-521.1300, -28.5400, 45.2617>>
			fVehHeadOffset = 357.1407								- 84.9600
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BIKE_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F0_BIKE,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-2.1752, -2.3781, 0.0000>>
				fVehHeadOffset = -68.4000
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F_BIKE_d
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F0_BIKE,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-3.9761, 0.2542, 0.0000>>
				fVehHeadOffset = -70.5273
				RETURN TRUE
			ENDIF
		BREAK		
		CASE PR_SCENE_F_S_AGENCY_2A_a
		CASE PR_SCENE_F_S_AGENCY_2A_b
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<-70.4456, -1015.1096, 28.2250>>	- << -78.4023, -1019.2347, 27.5447  >>
			fVehHeadOffset = 162.098038								- 109.0206
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_MD_KUSH_DOC
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1190.0171, -1531.1130, 3.4030>>	- << -1174.4580, -1573.6320, 4.7514>>
			fVehHeadOffset = 302.1820								- 105.9810
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1190.0171, -1531.1130, 3.4030>>	- << -1175.2980, -1573.6920, 4.3599>>
			fVehHeadOffset = 302.1820								- 172.9187
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_KUSH_DOC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<12.7400, 3.2600, 0.0000>>
				fVehHeadOffset = 95.2170
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_KUSH_DOC_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-1.3400, 7.6840, 0.0000>>
				fVehHeadOffset = 173.5200
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_F_THROW_CUP
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<44.4802, -1607.5681, 28.4055>>		- <<2.8895, -1607.2864, 28.2858-0.5+1.5>>
			fVehHeadOffset = 318.2674								- (310.879-180)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<3.6009, -1592.7875, 28.1773>>		- <<2.8895, -1607.2864, 29.2903>>
			fVehHeadOffset = 322.6286								- 130.8790
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_GYM
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<91.3579, 18.1788, -0.1911>>
			fVehHeadOffset = -90.3475
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE 
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<12.5073, -3.4625, -0.3702>>
			fVehHeadOffset = 14.3405
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CS_WIPEHANDS 
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_CS_CHECKSHOE,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<21.8700, -10.5000, 0.0000>>
				fVehHeadOffset = -104.7600
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT 
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_F_CS_CHECKSHOE,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_F_BAR_a_01
			GetPostMissionLastVeh(ePed, sVehData)
			
			//1207806
			vVehCoordsOffset = <<24.6,-1356.9,28.7>>				- <<28.9860, -1351.4120, 29.3437>>
			fVehHeadOffset = 270.1767								- 160.0000-180
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_b_01
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-19.8544, -10.4863, -0.0334>>
			fVehHeadOffset = -120.1200
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_c_02
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<21.5897, -6.8544, 0.6015>>
			fVehHeadOffset = -141.0000
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_d_02
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-7.6041, 0.1369, 0.5812>>
			fVehHeadOffset = -145.7690
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_e_01
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1.6000, 7.6802, 0.6947>>
			fVehHeadOffset = -50.9900
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_STRIPCLUB_out
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<22.3220, -6.2034, -0.0>>
			fVehHeadOffset = 73.0710
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DOCKS_a
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vVehCoordsOffset = <<-1.2901, -5.5798, -0.0357>>
			fVehHeadOffset = 160.5600
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DOCKS_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_DOCKS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-4.0739, 7.7692, -0.2984>>
				fVehHeadOffset = -48.9552
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_DOCKS_c
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_DOCKS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<-5.7780, -4.2397, 0.9091>>
				fVehHeadOffset = -12.9418
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_DOCKS_d
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_DOCKS_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				vVehCoordsOffset = <<0.6968, 1.1033, 0.9120>>
				fVehHeadOffset = 90.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_CR_CHASECAR_a
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vDriveOffset = << 0.0,8.0,0.6 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_CR_CHASECAR_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_CR_CHASECAR_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER		// Trevor chasing a guy on a scooter and screaming 'Scooter Brother!'
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_CR_CHASECAR_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
				sVehData.bIsPlayerVehicle = FALSE
				sVehData.model = FAGGIO2
				sVehData.eType = VEHICLE_TYPE_DEFAULT
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_CR_POLICE_a
			GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
			sVehData.eType = VEHICLE_TYPE_CAR
			
			vDriveOffset = << 0.0,8.0,0.6 >>
			fDriveSpeed = 15.0
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CN_POLICE_b
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_CR_POLICE_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_T_CN_POLICE_c
			RETURN GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, PR_SCENE_T_CR_POLICE_a,
					sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
		BREAK
		CASE PR_SCENE_T_NAKED_ISLAND
			IF NOT g_bMagDemoActive
				sVehData.bIsPlayerVehicle = FALSE
				sVehData.model = TROPIC	//SKIVVY
				
				//#954712
				sVehData.bExtraOn[0] = FALSE
				sVehData.bExtraOn[1] = FALSE
				sVehData.bExtraOn[2] = TRUE
				sVehData.bExtraOn[3] = FALSE
				sVehData.bExtraOn[4] = FALSE
				sVehData.bExtraOn[5] = TRUE
				sVehData.bExtraOn[6] = TRUE
				sVehData.bExtraOn[7] = TRUE
				sVehData.bExtraOn[8] = TRUE
			ELSE
				sVehData.bIsPlayerVehicle = FALSE
				sVehData.model = DINGHY
			ENDIF
			
//			vVehCoordsOffset = <<-8.1709, 11.4042, 0.6117>>
//			fVehHeadOffset = -12.0934
			
			vVehCoordsOffset = <<2779.7590, -1440.4209, -0.6187>>	- <<2789.8450, -1453.7310, 0.3109>>
			vVehCoordsOffset.z += 0.5
			fVehHeadOffset =  340.0835								- 4.4400
			
			vVehCoordsOffset = <<-3.3962, 16.5500, -0.7>> + <<-0.5,0.5,0.5>>
			fVehHeadOffset = 23.6441 + 90
			
			vVehCoordsOffset = <<-4.0164, 19.9594, -0.00>>
			fVehHeadOffset = 113.6465
			
			vVehCoordsOffset *= 1.1
			
			RETURN TRUE BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = SANCHEZ
			vVehCoordsOffset = <<9.8707, 13.0084, 0.0000>>
			fVehHeadOffset = -114.0000 - 43.0000 + 133.0000
			RETURN TRUE BREAK
		CASE PR_SCENE_T_UNDERPIER
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = SPEEDO		//url:bugstar:968989
			
			vVehCoordsOffset = <<-7.7078, 23.9099, 1.7307>>
			fVehHeadOffset = 24.3187
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_TRAF_AIR
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-4.5662, -3.2485, 0.9455-1.7>>
			fVehHeadOffset = -138.6056
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_METHLAB
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<1399.6621, 3597.0466, 33.8797>>	- << 1394.2081, 3602.2839, 37.9419 >>
			fVehHeadOffset = 315.9865								- 122.5269
			RETURN TRUE BREAK
			
		CASE PR_SCENE_T6_DIGGING
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<10.5999, 3.3997, 1.0212>>
			fVehHeadOffset = -50.3062
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_FUNERAL
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = ROMERO
			vVehCoordsOffset = <<-13.2279, -7.5348, 0.0000>>
			fVehHeadOffset = 4.3200
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRASH_b
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = BMX
			vVehCoordsOffset = <<11.9596, 0.3450, -1.0016>>
			fVehHeadOffset = -42.4225
			RETURN TRUE	BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = GBURRITO	//url:bugstar:993024
			
			vVehCoordsOffset = <<3.4240, 7.6462, 0.8227>>
			fVehHeadOffset = -150.0000
			
			RETURN TRUE BREAK
			
		CASE PR_SCENE_T_FIGHTBAR_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1243.6498, -1094.2047, 7.1164>>	- << -1242.68, -1105.15, 7.10 >>
			fVehHeadOffset = 14.0848								- 120.0000
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1672.5349, -930.5448, 6.8143>>	- <<-1667.1479, -974.7168, 6.4790 >>
			fVehHeadOffset = 144.9416								- 171.2530
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_c
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-267.5488, 6276.1196, 30.3025>>	- <<-301.4778, 6250.8999, 30.5054 >>
			fVehHeadOffset = 130.9896								- 10.2470
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FLYING_PLANE
			sVehData.bIsPlayerVehicle = FALSE
			sVehData.model = CUBAN800	//VULKAN
			
			vDriveOffset = << 0.0,150.0,20.0 >>
			fDriveSpeed = 30.0
			
			RETURN TRUE BREAK
			
	ENDSWITCH
	SWITCH eScene
		CASE PR_SCENE_M_BENCHCALL_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-147.1660, -387.5143, 32.5629>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=341.4322 - (133.000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-1306.7816, -689.1462, 24.4283>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=214.6826 - (33.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-708.9244, 254.0631, 79.3324>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=115.2022 - ( -176.2500)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-708.9244, 254.0631, 79.3324>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=115.2022 - ( -147.1920)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-1100.8779, -441.1681, 35.0054>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=297.5568 - ( -144.6220)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITBARBER
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-825.3141, -199.5354, 36.3852>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=29.4869 - ( -62.5000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-730.8218, -144.1027, 36.2086>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=28.5320 - (119.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOT_JIMMY
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-1144.1743, -523.2257, 31.7307>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=299.2956 - ( -22.3200)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-1421.7998, 314.6989, 60.9436>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=335.4134 - ( 72.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_REJECTENTRY
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-706.1870, 255.3143, 79.4690>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=117.3069 - (37.2700)
			RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<185.4888, -1677.7335, 28.7165>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=54.2538 - ( -83.8)
			RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-17.5560, -1405.1594, 28.3218>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=91.3098 - ( -70.4124)
			RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-219.3172, -1492.1511, 30.2611>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=139.2572 - ( -12.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5
			GetPostMissionLastVeh(ePed, sVehData)
			
			
			vVehCoordsOffset = <<-22.6138, -1811.6935, 25.3018>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=141.0423 - ( -117.3560)
			RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_a
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<164.1308, 773.6719, 208.5337>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=136.3104 - ( -36.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-263.3745, 396.9240, 108.9995>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=284.4611 - ( -95.5880)
			RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CN_PARK_b
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1883.8361, 2030.4458, 139.5782>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=340.5746 - ( 9.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PIER
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-200.6840, 6560.5566, 10.0296>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=134.5505 - (110.5931)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1278.0234, -1083.7513, 6.4647>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=115.8919 - (26.3597)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<325.8113, 164.5124, 102.4417>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=68.4108 - (10.7700)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHATEAU_d
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1622.2205, -122.9896, 56.6160>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=210.8653 - ( 13.7207)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_LINGERIE
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<172.4420, -213.7796, 53.0019>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=250.3032 - ( -40.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-887.9977, -836.0328, 17.3426>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=270.8607 - ( -81.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-1264.2178, -1182.7039, 4.8359>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=298.4328 - ( -150.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_ESCORTED_OUT
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-80.4564, 289.0073, 104.8218>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=247.6446 - ( -122.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<434.9171, -1477.2819, 28.2762>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=228.6353 - (18.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_THROW_FOOD
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<434.9171, -1477.2819, 28.2762>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=228.6353 - ( -51.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<288.9770, 177.7729, 103.1881>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=68.9831 - (138.0000-180)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT
			GetPostMissionLastVeh(ePed, sVehData)
			
			vVehCoordsOffset = <<-161.4589, -432.4635, 32.7794>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=340.0368 - ( -153.0000)
			RETURN TRUE BREAK
			
			
			
		CASE PR_SCENE_M_CANAL_b
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-806.8813, 828.3607, 202.1143>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=101.1612 - ( -54.3470)
			RETURN TRUE BREAK
		CASE PR_SCENE_M_PIER_a
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1724.7200, -1108.0811, 12.0174>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=319.8931 - (143.4931)
			RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1859.5045, -628.4899, 10.2248>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=229.0784 - (99.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1393.4424, 90.4242, 53.1469>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=123.1782 - (-45.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT
			GetPostMissionLastVeh(ePed, sVehData)
//			vVehCoordsOffset = <<370.8787, 186.9402, 101.9217>>-g_sPedSceneData[eScene].vCreateCoords
//			fVehHeadOffset=157.7107 - (70.0000)
			vVehCoordsOffset = <<370.5876, 186.1865, 101.9210>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=159.7861 - (70.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M4_CINEMA
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1391.1559, -188.5636, 45.9871>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=36.5172 - ( -45.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_M6_LIQUORSTORE
			GetPostMissionLastVeh(ePed, sVehData)
//			vVehCoordsOffset = <<2005.1534, 3075.1667, 46.0574>>-g_sPedSceneData[eScene].vCreateCoords
//			fVehHeadOffset=324.6471 - ( -33.1280)
			vVehCoordsOffset = <<2001.9182, 3076.7419, 46.0567>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset=328.1010 - ( -33.1280)
			RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_b
			GetPostMissionLastVeh(ePed, sVehData)
//			vVehCoordsOffset = <<-1420.7147, 316.1814, 60.9876>>-g_sPedSceneData[eScene].vCreateCoords
//			fVehHeadOffset=332.4690 - ( -132.0000)
			vVehCoordsOffset = <<-1421.8212, 314.7191, 60.9442>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 336.5938 - ( -132.0000)
			RETURN TRUE BREAK
			
			
			
		CASE PR_SCENE_T6_HUNTING1
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-569.3535, 5643.7256, 37.4888>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 296.1685 - (GET_HEADING_FROM_VECTOR_2D(7.4998, -7.4995))
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING2
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1555.5929, 4717.7280, 47.4526>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 236.2230 - (GET_HEADING_FROM_VECTOR_2D(-10.6345, -0.7246))
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING3
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1553.8610, 4629.1479, 22.7549>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 332.7842 - (GET_HEADING_FROM_VECTOR_2D(3.4271, 13.6787))
			RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<631.8275, -1009.7451, 35.9161>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 98.8128 - ( -33.7700)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_GARDEN
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-111.9033, 900.8749, 234.6825>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 6.1087 - (155.6800)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_b
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<1546.3232, 3636.1509, 33.5351>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 298.4009 - (  -4.1240)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_c
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-179.4242, 6439.6665, 30.5120>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 225.5593 - (108.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_DUMPSTER
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<486.7419, -1390.5446, 28.2977>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 178.2980 - ( -90.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEBEACH
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-3052.8945, 143.2342, 10.5662>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 85.3429 - (68.8227)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEBARN
			GetPostMissionLastVeh(ePed, sVehData)
//			vVehCoordsOffset = <<2298.4309, 4893.7275, 40.2791>>-g_sPedSceneData[eScene].vCreateCoords
//			fVehHeadOffset=338.3418 - (56.2037)
			vVehCoordsOffset = <<2202.3752, 4934.0801, 39.9155>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 314.2654 - (56.2037)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEROOFTOP
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<401.2502, -782.0952, 28.1490>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 179.9905 - ( -106.6605)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBBUILD
//			sVehData.bIsPlayerVehicle = FALSE						sVehData.model = CRUISER
//			vVehCoordsOffset = <<6.8584, 4.0742, -0.7053>>
//			fVehHeadOffset = 179.9823
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1195.2559, -1534.5072, 3.3919>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 305.8221 - ( -165.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_SCARETRAMP
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-573.3765, -1241.2125, 12.8792>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 316.9941 - ( -171.0000)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_YELLATDOORMAN
//			sVehData.bIsPlayerVehicle = FALSE						sVehData.model = SUPERD
//			vVehCoordsOffset = <<-17.9282, -11.6275, 0.0000>>
//			fVehHeadOffset = -6.5100							- -24.0000							+ -32.4880
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-724.4290, -1298.5391, 4.0002>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 230.5715 - ( -32.4880)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b
//			sVehData.bIsPlayerVehicle = FALSE						sVehData.model = SUPERD
//			vVehCoordsOffset = <<-1304.0609, 276.1191, 63.1113>>	- <<-1280.0540, 303.9235, 63.9553>>
//			fVehHeadOffset = 335.8063								- -29.0930-360
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-1309.1135, 250.8387, 61.2030>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 10.7756 - ( -29.0930)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTCASINO
//			sVehData.bIsPlayerVehicle = FALSE						sVehData.model = SUPERD
//			vVehCoordsOffset = <<909.96,48.19,80.31>>				- <<924.1288, 48.0048, 79.7644>>
//			fVehHeadOffset = 2.57									- 229.6085
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<917.6678, 60.3233, 79.7640>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= 148.0210 - (229.6085)
			RETURN TRUE BREAK
			
#IF USE_TU_CHANGES
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<2919,8588,350>>-g_sPedSceneData[eScene].vCreateCoords
			fVehHeadOffset= GET_RANDOM_FLOAT_IN_RANGE(-180, 180)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_DRUNKHOWLING
			GetPostMissionLastVeh(ePed, sVehData)
			vVehCoordsOffset = <<-48.5171, 28.4211, 3.0057>>
			fVehHeadOffset = -1.3831
			RETURN TRUE BREAK
#ENDIF
			
	ENDSWITCH
	
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for veh pos: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
	
	CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", sInvalid)
//	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
//	sVehData.bIsPlayerVehicle = FALSE
//	sVehData.model = DUMMY_MODEL_FOR_SCRIPT
//	sVehData.fDirtLevel = 0.0
//	sVehData.fHealth = 0
//	sVehData.iColourCombo = 0
//	sVehData.bColourCombo = TRUE
//	vVehCoordsOffset = <<0,0,0>>
//	fVehHeadOffset = 0
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PLAYER_VEH_RECORDING_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		TEXT_LABEL &tRecording_name, INT &iRecording_num,
		FLOAT &fRecording_start, FLOAT &fRecording_skip, FLOAT &fRecording_stop,
		FLOAT &fSpeed_switch, FLOAT &fSpeed_exit)
	
	SWITCH eScene
		CASE PR_SCENE_F_S_EXILE2		tRecording_name = "lkexcile2_chase"	iRecording_num = 100	fRecording_start = 6500	fRecording_skip = 12700.0	fRecording_stop = 5000+12700.0	fSpeed_switch = 1.0	fSpeed_exit = 1.0	RETURN TRUE BREAK
		CASE PR_SCENE_M_MD_FBI2			tRecording_name = "scene_m_fbi2_"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 6200.0	fRecording_stop = 5000+6200.0	fSpeed_switch = 0.4	fSpeed_exit = 0.8	RETURN TRUE BREAK
		CASE PR_SCENE_F_MD_FRANKLIN2	tRecording_name = "scene_f_fra2_"	iRecording_num = 001	fRecording_start = 1000	fRecording_stop = 8500.0	fRecording_skip = 4750			fSpeed_switch = 0.4	fSpeed_exit = 0.7	RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_BIKINGJIMMY	tRecording_name = "scene_m_biking"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 2000		fRecording_stop = 5000			fSpeed_switch = 1.0	fSpeed_exit = 1.0	RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CR_CHASECAR_a	tRecording_name = "scene_t_chaseA"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 2000		fRecording_stop = 7500			fSpeed_switch = 1.0	fSpeed_exit = 1.0	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b	tRecording_name = "scene_t_chaseB"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 1500		fRecording_stop = 10000			fSpeed_switch = 0.5	fSpeed_exit = 0.75	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE	tRecording_name = "scene_t_chaseC"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 1500		fRecording_stop = 10000			fSpeed_switch = 0.9	fSpeed_exit = 1.1	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER	tRecording_name = "scene_t_chaseD"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 2000		fRecording_stop = 5000			fSpeed_switch = 1.0	fSpeed_exit = 1.0	RETURN TRUE BREAK

		CASE PR_SCENE_T_CR_POLICE_a		tRecording_name = "scene_t_policeA"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 2000		fRecording_stop = 10000			fSpeed_switch = 0.5	fSpeed_exit = 0.75	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_POLICE_b		tRecording_name = "scene_t_policeB"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 2000		fRecording_stop = 5000			fSpeed_switch = 1.0	fSpeed_exit = 1.0	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_POLICE_c		tRecording_name = "scene_t_policeC"	iRecording_num = 001	fRecording_start = 1000	fRecording_skip = 2000		fRecording_stop = 5000			fSpeed_switch = 1.0	fSpeed_exit = 1.0	RETURN TRUE BREAK
	ENDSWITCH
	
	tRecording_name = ""
	iRecording_num = -1
	
	fRecording_start = -1
	fRecording_skip = -1
	fRecording_stop = -1
	
	fSpeed_switch = 0.0
	fSpeed_exit = 0.0
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tPlayerSceneSyncAnimVehLoop)
	
	SWITCH eScene
		CASE PR_SCENE_M2_WIFEEXITSCAR		tPlayerSceneSyncAnimVehLoop = "000606_02_MICS1_5_AMANDA_EXITS_CAR_IDLE_CAR" RETURN TRUE BREAK
		
		CASE PR_SCENE_M2_CARSLEEP_a			tPlayerSceneSyncAnimVehLoop = "BASE_CAR" RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M2_CARSLEEP_a, tPlayerSceneSyncAnimVehLoop) BREAK
		
		CASE PR_SCENE_M6_CARSLEEP			tPlayerSceneSyncAnimVehLoop = "BASE_PREMIER_CAR" RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_OPENDOORFORAMA		tPlayerSceneSyncAnimVehLoop = "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_IDLE_CAR" RETURN TRUE BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY		tPlayerSceneSyncAnimVehLoop = "001839_02_MICS3_20_DROPPING_OFF_JMY_IDLE_CAR" RETURN TRUE BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR		tPlayerSceneSyncAnimVehLoop = "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_IDLE_CAR" RETURN TRUE BREAK
//		CASE PR_SCENE_M_HOOKERCAR			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M7_TRACEYEXITSCAR, tPlayerSceneSyncAnimVehLoop) BREAK
		
		CASE PR_SCENE_M7_TALKTOGUARD		tPlayerSceneSyncAnimVehLoop = "001393_02_MICS3_3_TALKS_TO_GUARD_IDLE_CAR" RETURN TRUE BREAK
		
		CASE PR_SCENE_F1_BYETAXI			tPlayerSceneSyncAnimVehLoop = "001938_01_FRAS_V2_7_BYE_TAXI_IDLE_TAXI" RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR			tPlayerSceneSyncAnimVehLoop = "001946_01_GC_FRAS_V2_IG_5_EXIT_CAR" RETURN TRUE BREAK
		CASE PR_SCENE_F0_CLEANCAR			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F1_CLEANCAR, tPlayerSceneSyncAnimVehLoop) BREAK
		
		CASE PR_SCENE_F0_BIKE				tPlayerSceneSyncAnimVehLoop = "BASE_BIKE" RETURN TRUE BREAK
		CASE PR_SCENE_F1_BIKE				RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_F_BIKE_c				RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_F_BIKE_d				RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncAnimVehLoop) BREAK
		
//		CASE PR_SCENE_M7_BIKINGJIMMY		tPlayerSceneSyncAnimVehLoop = "LOOP_Cruiser" RETURN TRUE		BREAK
		
		CASE PR_SCENE_M_PARKEDHILLS_a		tPlayerSceneSyncAnimVehLoop = "SITTING_ON_CAR_BONNET_LOOP_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_M4_PARKEDBEACH		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		
		CASE PR_SCENE_Ma_RURAL1 			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a		tPlayerSceneSyncAnimVehLoop = "SITTING_ON_CAR_PREMIERE_LOOP_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehLoop) BREAK
		
		CASE PR_SCENE_M2_PHARMACY			tPlayerSceneSyncAnimVehLoop = "MICS1_IG_11_LOOP_CAR" RETURN TRUE		BREAK
		
	ENDSWITCH
	
	tPlayerSceneSyncAnimVehLoop = ""
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tPlayerSceneSyncAnimVehExit)
	
	SWITCH eScene
		CASE PR_SCENE_M2_WIFEEXITSCAR		tPlayerSceneSyncAnimVehExit = "000606_02_MICS1_5_AMANDA_EXITS_CAR_EXIT_CAR" RETURN TRUE		BREAK
		
		CASE PR_SCENE_M2_CARSLEEP_a			tPlayerSceneSyncAnimVehExit = "SLEEP_IN_CAR_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M2_CARSLEEP_b			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M2_CARSLEEP_a, tPlayerSceneSyncAnimVehExit) BREAK
		
		CASE PR_SCENE_M6_CARSLEEP			tPlayerSceneSyncAnimVehExit = "SLEEP_IN_CAR_PREMIER_CAR" RETURN TRUE		BREAK
		
		CASE PR_SCENE_M7_OPENDOORFORAMA		tPlayerSceneSyncAnimVehExit = "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_EXIT_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY		tPlayerSceneSyncAnimVehExit = "001839_02_MICS3_20_DROPPING_OFF_JMY_EXIT_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR		tPlayerSceneSyncAnimVehExit = "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_CAR" RETURN TRUE		BREAK
//		CASE PR_SCENE_M_HOOKERCAR			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M7_TRACEYEXITSCAR, tPlayerSceneSyncAnimVehExit) BREAK
		
		CASE PR_SCENE_M7_TALKTOGUARD		tPlayerSceneSyncAnimVehExit = "001393_02_MICS3_3_TALKS_TO_GUARD_EXIT_CAR" RETURN TRUE		BREAK
		
		CASE PR_SCENE_F1_BYETAXI			tPlayerSceneSyncAnimVehExit = "001938_01_FRAS_V2_7_BYE_TAXI_EXIT_TAXI" RETURN TRUE		BREAK
		CASE PR_SCENE_F1_CLEANCAR			tPlayerSceneSyncAnimVehExit = "001946_01_GC_FRAS_V2_IG_5_BASE_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_F0_CLEANCAR			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F1_CLEANCAR, tPlayerSceneSyncAnimVehExit) BREAK
		
		CASE PR_SCENE_F0_BIKE				tPlayerSceneSyncAnimVehExit = "EXIT_BIKE" RETURN TRUE		BREAK
		CASE PR_SCENE_F1_BIKE				RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_F_BIKE_c				RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_F_BIKE_d				RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncAnimVehExit) BREAK
		
		CASE PR_SCENE_M7_BIKINGJIMMY		tPlayerSceneSyncAnimVehExit = "EXIT_Cruiser" RETURN TRUE		BREAK
		
		CASE PR_SCENE_M_PARKEDHILLS_a		tPlayerSceneSyncAnimVehExit = "SITTING_ON_CAR_BONNET_EXIT_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_M4_PARKEDBEACH		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		
		CASE PR_SCENE_Ma_RURAL1 			RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a		tPlayerSceneSyncAnimVehExit = "SITTING_ON_CAR_PREMIERE_EXIT_CAR" RETURN TRUE		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e		RETURN GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncAnimVehExit) BREAK
		
		CASE PR_SCENE_M2_PHARMACY			tPlayerSceneSyncAnimVehExit = "MICS1_IG_11_EXIT_CAR" RETURN TRUE		BREAK
		
	ENDSWITCH
	
	tPlayerSceneSyncAnimVehExit = ""
	RETURN FALSE
ENDFUNC


FUNC BOOL SET_SCENE_VEH_RADIO_STATION(VEHICLE_INDEX vehicleIndex, PED_REQUEST_SCENE_ENUM ePedScene, BOOL bSetupVehRadio)


	// #1199581
	IF ePedScene = PR_SCENE_T_NAKED_ISLAND		//#1199581
		IF bSetupVehRadio
			IF g_bMagDemoActive
				// No country music was playing in the boat during Mag Demo 2
				
				SET_VEHICLE_RADIO_ENABLED(vehicleIndex, TRUE)
				SET_VEH_RADIO_STATION(vehicleIndex, "RADIO_06_COUNTRY")
				SET_CUSTOM_RADIO_TRACK_LIST("RADIO_06_COUNTRY", "MAGDEMO2_RADIO_DINGHY", TRUE)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "Turning on radio for veh in scene ", Get_String_From_Ped_Request_Scene_Enum(ePedScene))
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF ePedScene = PR_SCENE_M7_DROPPINGOFFJMY	//#1258231 & #1542164
		IF bSetupVehRadio
	//		could you please try calling:
			
			SET_MOBILE_PHONE_RADIO_STATE(TRUE)
			SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
			SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
			
	//		When you spawn the car:
			SET_VEH_RADIO_STATION(vehicleIndex, "RADIO_01_CLASS_ROCK")
			
	//		And when cutting back to game play (after michael is inside the car) 
	//		
	//		SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "Turning on radio for veh in scene ", Get_String_From_Ped_Request_Scene_Enum(ePedScene))
			#ENDIF
			
//			SCRIPT_ASSERT("M7_DROPPINGOFFJMY - radio setup")
			
			RETURN TRUE
		ELSE
	//		could you please try calling:
	//		
	//		SET_MOBILE_PHONE_RADIO_STATE(TRUE)
	//		SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
	//		SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
	//		
	//		When you spawn the car:
	//		SET_VEH_RADIO_STATION(vehicleIndex, "RADIO_01_CLASS_ROCK")
	//		
	//		And when cutting back to game play (after michael is inside the car) 
	//		
			SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "Turning off radio for veh in scene ", Get_String_From_Ped_Request_Scene_Enum(ePedScene))
			#ENDIF
			
//			SCRIPT_ASSERT("M7_DROPPINGOFFJMY - radio reset")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
