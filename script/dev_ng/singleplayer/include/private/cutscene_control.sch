//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 04/04/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  	Off-mission Cutscene Control							│
//│																				│
//│		DESCRIPTION: Lightweight control thread to manage multiple scripts		│
//│		requesting to preload off-mission cutscenes at the same time. Gives 	│
//│		permission to load to one request at a time.							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"


PROC CLEANUP_CUTSCENE_CONTROL()
	INT iIndex
	REPEAT MAX_OFFMISSION_CUTSCENE_REQUESTS iIndex
		g_sOffMissionCutsceneRequests[iIndex].iID = NULL_OFFMISSION_CUTSCENE_REQUEST
		g_sOffMissionCutsceneRequests[iIndex].eType = OCT_NULL
	ENDREPEAT

	g_bOffMissionCutsceneChange = FALSE
	g_iOffMissionCutsceneRequestCount = 0
	g_iOffMissionCutsceneRequestAllowed = NULL_OFFMISSION_CUTSCENE_REQUEST
	g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST
	g_iFlowIntroCutsceneRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST

	CPRINTLN(DEBUG_FLOW_CUTS, "Off-mission cutscene controller terminated.")
	TERMINATE_THIS_THREAD()
ENDPROC


FUNC INT GET_INDEX_OF_REQUEST_ID(INT paramRequestID)
	INT iRequestIndex
	REPEAT g_iOffMissionCutsceneRequestCount iRequestIndex
		IF g_sOffMissionCutsceneRequests[iRequestIndex].iID = paramRequestID
			RETURN iRequestIndex
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("Get_Index_Of_Request_ID: Off-mission cutscene controller tried to retrieve the index of an ID that wasn't registered.")
	RETURN -1
ENDFUNC


#IF IS_DEBUG_BUILD
	PROC CREATE_CONTROLLER_WIDGET()
		INT iTempIndex
		TEXT_LABEL_15 tWidgetName
	
		START_WIDGET_GROUP("Off-mission Cutscenes")
			//Monitor list state.
			ADD_WIDGET_STRING("Request List")
			REPEAT MAX_OFFMISSION_CUTSCENE_REQUESTS iTempIndex
				tWidgetName = ""
				tWidgetName += iTempIndex
				ADD_WIDGET_INT_READ_ONLY(tWidgetName, g_sOffMissionCutsceneRequests[iTempIndex].iID)
			ENDREPEAT
			ADD_WIDGET_INT_READ_ONLY("List Count", g_iOffMissionCutsceneRequestCount)
			//Monitor allowed and active variables.
			ADD_WIDGET_STRING("Controller State")
			ADD_WIDGET_INT_READ_ONLY("Allowed ID", g_iOffMissionCutsceneRequestAllowed)
			ADD_WIDGET_INT_READ_ONLY("Active ID", g_iOffMissionCutsceneRequestActive)
		STOP_WIDGET_GROUP()
	ENDPROC
#ENDIF


PROC UPDATE_CUTSCENE_CONTROL()
	IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY_FRIENDS) OR g_bFlowLoadIntroCutscene	
		//Has the state of the list changed?
		IF g_bOffMissionCutsceneChange
			//Are there any requests in the list?
			IF g_iOffMissionCutsceneRequestCount > 0
				//Try and find a request with a higher priority than current.
				INT iCurrentPriority = -1 
				IF g_iOffMissionCutsceneRequestAllowed != NULL_OFFMISSION_CUTSCENE_REQUEST
					iCurrentPriority = ENUM_TO_INT(g_sOffMissionCutsceneRequests[Get_Index_Of_Request_ID(g_iOffMissionCutsceneRequestAllowed)].eType)
				ENDIF
				BOOL bFoundNew = FALSE
				INT iRequestIndex = 0
				WHILE (NOT bFoundNew) AND (iRequestIndex < g_iOffMissionCutsceneRequestCount)
					IF ENUM_TO_INT(g_sOffMissionCutsceneRequests[iRequestIndex].eType) > iCurrentPriority
						//Found request with a higher priority. Set this as the allowed ID.
						g_iOffMissionCutsceneRequestAllowed = g_sOffMissionCutsceneRequests[iRequestIndex].iID
						CPRINTLN(DEBUG_FLOW_CUTS, "Off-mission cutscene controller changed allowed ID to ", g_iOffMissionCutsceneRequestAllowed, ".")
						bFoundNew = TRUE
					ENDIF
					iRequestIndex++
				ENDWHILE
			ELSE
				//No requests in list. Clear both allowed and loaded variables.
				g_iOffMissionCutsceneRequestAllowed = NULL_OFFMISSION_CUTSCENE_REQUEST
				g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST
			ENDIF
		 	g_bOffMissionCutsceneChange = FALSE
		ENDIF
	ENDIF
ENDPROC
