
#IF IS_DEBUG_BUILD

PROC GET_MP_OUTFIT_DATA_FOR_GENERATING_XML_MALE_2(MP_OUTFIT_ENUM eOutfit, MP_OUTFITS_DATA &sOutfitsData, scrShopPedComponent &componentItem, scrShopPedProp &propItem)
	SWITCH eOutfit
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_LEGS_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_SPECIAL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_DECL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_JBIB_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_LEGS_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_FEET_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_SPECIAL_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_DECL_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_JBIB_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_LEGS_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_5_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_3_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_3_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_8_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_4_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_TORSO_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_1_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PLEFT_WRIST_7_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PRIGHT_WRIST_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_JBIB_4_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_M_LOWR0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_9_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_M_LEGS_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_M_FEET_1_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_10_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IND_M_PHEAD_6_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_LEGS_3_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_0_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_7_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARENA_M_PHEAD_3_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_12_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_35_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_17_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_TORSO_1_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_7_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_10_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_6_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_ARENA_WARS_PETE
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_FEET_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_IMPOTENT_RAGE
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 148	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 168	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 115	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 91	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 145	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 291	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] = 74	sOutfitsData.iComponentTextureID[PED_COMP_HAIR] = 0
		BREAK
		
		CASE OUTFIT_CASINO_HIGHROLLER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 116	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_TEETH0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 146	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 292	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2
		BREAK
	ENDSWITCH
	
	SWITCH eOutfit
		//Green & Yellow Space Creature
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		//Orange Space Cyclops
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_TORSO_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		//Light Blue Space Horror
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_7_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_TORSO_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		//Green Space Cyclops
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_BERD_6_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_TORSO_0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_LEGS_6_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_JBIB_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_UNDERTAKER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_TEETH0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_ACCS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_9_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 10	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 13	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_M_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 10	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_JBIB3_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_M_WATCH0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_55_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_M_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH1_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_7_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 10	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 13	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 3

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_VWD_M_PLEFT_WRIST_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_25_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 10	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_JAN_M_SPECIAL2_2_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_M_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_M_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_SPECIAL_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_SPECIAL_2_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_7_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_SPECIAL_2_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_JBIB_24_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_SPECIAL_9_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_M_JBIB_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_69_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 11	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_63_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH1_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_63_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_M_FEET_0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_TEETH0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_11_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB3_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_63_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_M_FEET_0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_8_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB2_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_63_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_63_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_8_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_63_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB2_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_18_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_10_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_18_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_10_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_10_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_18_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_10_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_18_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_10_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_18_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_10_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_14_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_35_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_14_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_2_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_6_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_35_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_6_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_14_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_7_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_8_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_7_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_8_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_7_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_3_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_8_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_7_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_3_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_8_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_7_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_3_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_8_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_TORSO_7_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_3_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_8_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIB_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH0_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 10	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 53	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_JBIB0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_CASINO_HEIST_FIB_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_1_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 11	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 54	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 4	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6

		BREAK

		CASE OUTFIT_CASINO_HEIST_FIB_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 10	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 55	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_JBIB5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5


		BREAK

		CASE OUTFIT_CASINO_HEIST_FIB_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 13	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 55	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_25_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 13	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 8	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 13

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_M_WATCH0_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 8	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 13

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_CASINO_HEIST_STEALTH_II_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_21_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 3	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 9	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_JBIB_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 9	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_21_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_1_25), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_M_JBIB_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 7	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 3	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_21_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 3	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 8	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_9_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_SPECIAL_2_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 9	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_JBIB_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 13	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 9	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_7_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_22), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_25_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_25_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_25_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_16_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_40_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_16_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_18_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_36_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_VALET
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH1_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_6
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_7
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_4_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_4_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
	ENDSWITCH
	
	SWITCH eOutfit
	
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_X17_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_X17_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 7	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_X17_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_2_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_15_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_74_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_15_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_73_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_3_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_2_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_45_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_4_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_3_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_BERD_15_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_71_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_LEGS0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 0	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 10
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(-260815913, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(-2034025655, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1168472040, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(-260815913, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(-690794131, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1168472040, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 8	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(-260815913, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(-690794131, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(1900824849, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 2	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 15
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(648243112, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(-476526155, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1168472040, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_47_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 2	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 0
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(-1623489018, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_69_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 4	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_13_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_6_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(-1357503045, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_69_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_FEET_1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_13_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_FEET0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
	
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_3_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_2_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 7
		BREAK
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_4_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5
		BREAK
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_2_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6
		BREAK
	
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_LEGS_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_TEETH0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_SPECIAL_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_JBIB_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6
		BREAK
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_SPECIAL_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_LEGS_1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_SPECIAL_4_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_JBIB_2_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_VWD_M_PEYES_0_10), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_LEGS_5_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_SPECIAL_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_JBIB_2_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_VWD_M_PEYES_0_11), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
	
	ENDSWITCH
	
	SWITCH eOutfit
		
//		CASE OUTFIT_CNC_COP_LSPD_OFFICER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_LSPD_OFFICER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_M_WATCH0_3), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_LSPD_OFFICER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_6), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_PARK_RANGER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_3_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_7), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_0_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_PARK_RANGER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_3_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_4_7), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_2_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_M_PLEFT_WRIST_6_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_PARK_RANGER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_3_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_6_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_7), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_3_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(1167517582, propItem) // MISSING DLC NAME
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_M_WATCH_0_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_SHERIFF_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_3_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_2_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_0_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_SHERIFF_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_3_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_5_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_1_3), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6
//		BREAK
//		CASE OUTFIT_CNC_COP_SHERIFF_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS1_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_4_5), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_1_3), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6
//		BREAK
//		CASE OUTFIT_CNC_COP_HIGHWAY_PATROL_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_3_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_FEET_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_7_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_4_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_0_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(MP_DLC_LUXE_M_PLEFT_WRIST_4_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_HIGHWAY_PATROL_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_2_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_7_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_4_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(MP_DLC_LUXE_M_PLEFT_WRIST_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_HIGHWAY_PATROL_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_2_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_7_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_M_WATCH0_3), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TECH_CONTRACTOR_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_SPECIAL_11_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 13	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 5
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_M_EAR1_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TECH_CONTRACTOR_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_4_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_10), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_SPECIAL_12_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_JBIB3_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 9
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_M_EAR1_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(MP_DLC_LUXE_M_PLEFT_WRIST_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TECH_CONTRACTOR_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_LEGS1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_9), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_SPECIAL_13_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 3	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 7
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_M_EAR1_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TACTICAL_SHERIFF_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_TEETH_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_9), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_9_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_JBIB_6_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_M_WATCH_0_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TACTICAL_SHERIFF_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_TORSO_9_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_TEETH_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_DECL_11_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_JBIB_7_7), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 15	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 6
//		BREAK
//		CASE OUTFIT_CNC_COP_TACTICAL_SHERIFF_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_FEET_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_TEETH_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_SPECIAL_9_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_DECL_13_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_M_PHEAD_1_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 2
//		BREAK
//		
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_LEGS_0_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 7	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_12_4), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_1_16), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_1_5), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_JBIB_0_16), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_2
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_17_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_LEGS_0_10), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 7	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_12_5), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_3
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_19_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_LEGS_1_25), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_1_13), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_JBIB_0_25), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_BALLAS_ENFORCER_0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_BERD_10_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_M_LEGS_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_4_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_SPECIAL_9_16), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SMUG_M_PHEAD_8_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_ONEILS_HACKER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_2_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_SPECIAL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_JBIB_11_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_STUNT_M_PHEAD_9_16), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_ONEILS_HACKER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 12	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 4
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 5	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_SPECIAL_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_JBIB_13_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_HEAD0_4), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_ONEILS_HACKER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_SPECIAL_21_14), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_M_JBIB_11_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_6_10), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 9
//		BREAK
		
		CASE OUTFIT_SUM20_ALIEN_AWARD
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_BERD_0_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
	ENDSWITCH
	
	SWITCH eOutfit
		CASE OUTFIT_ISLAND_HEIST_GUARD_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 12	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_3_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1008973152, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_GUARD_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_2_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 4	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_JBIB6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_7), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_GUARD_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 9
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 9	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_GUARD_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_JBIB_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_1_22), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 11	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 7
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 5
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 4	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_14_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_LEGS0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 12	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 12	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_14_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 4	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_2_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 9	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 14

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_FEET_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 9	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_LEGS_2_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_1_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_JBIB_15_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_M_LEGS_2_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_M_FEET_0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_2_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_M_JBIB_3_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TEETH_12_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_14_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_1_15), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TEETH_10_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_14_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_2_16), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TEETH_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_15_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_4_8), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_1_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TEETH_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_15_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H4_M_PHEAD_0_14), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_LEGS0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_FEET_3_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_4_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 9	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 13
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 12	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 5	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_3_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_1_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_2_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_JAN_M_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 6
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_0_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_1_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_11_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_3_23), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_4_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_16_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_4_24), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 9	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_6_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_16_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_5_19), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_LEGS_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_4_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_JBIB_10_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_JBIB_10_25), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_4_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 15	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_LEGS_4_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_SPECIAL_1_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 15	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 15	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_0_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_FEET_6_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 15	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_LEGS_0_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_M_FEET_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_M_GLASSES_1_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_M_PLEFT_WRIST_2_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Heist4_Tee_049_M")
			sOutfitsData.iDLCTattooOverlay[1] = HASH("MP_Heist4_Tee_050_M")
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_M_LOWR0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 0	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 10
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_JBIB_7_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IE_M_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PRIGHT_WRIST_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_M_LOWR1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_M_FEET0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_3_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_HEAD1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_M_GLASSES1_6), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_M_FEET0_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_15_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 3	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 7
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_JBIB11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_M_GLASSES_1_7), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK

	ENDSWITCH
	
	SWITCH eOutfit
		CASE OUTFIT_TUNER_ROBBER_BRAVADO
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_LAMPADATI
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 0	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 10
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_M_JBIB4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
		BREAK

		CASE OUTFIT_TUNER_ROBBER_ANORAK
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_JBIB_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_M_PHEAD_0_13), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_ROBBER_SMOLDER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_38_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_LEGS_0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_JBIB_1_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IE_M_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
		BREAK

		CASE OUTFIT_TUNER_ROBBER_RED_RACING
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_LEGS_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_SPECIAL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_DECL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_M_JBIB_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_PUFF
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_LEGS_4_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_JBIB_5_25), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IE_M_PHEAD_0_5), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_ROBBER_NEW_SKOOL
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_LEGS_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_FEET_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_M_JBIB_7_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_M_PHEAD_0_21), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_M_PLEFT_WRIST_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_ROBBER_POLO
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_LEGS_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_M_PHEAD_0_3), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK

		CASE OUTFIT_TUNER_ROBBER_CHECK
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_JBIB_5_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_M_PHEAD_0_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_LURKER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 4	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 7	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_ROBBER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_36_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 12	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_TUNER_SECURITY_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 11	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 10	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 10	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_JBIB5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5
		BREAK

		CASE OUTFIT_TUNER_SECURITY_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 10	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 10	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 10	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_6), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_SECURITY_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 10	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 11	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 4	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_6), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_SECURITY_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 11	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_TEETH0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 10	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 4	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_6), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_LOST_MC_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 12	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_SPECIAL_7_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5
		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_LEGS_5_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_SPECIAL_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 1	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PLEFT_WRIST_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PRIGHT_WRIST_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_35_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_LEGS_1_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_SPECIAL_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PRIGHT_WRIST_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_LEGS_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_SPECIAL_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_M_PHEAD_0_5), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 4	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 7
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_LEGS_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_APA_M_PHEAD_1_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 2	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 0
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_LEGS_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_M_PHEAD_8_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_DOCK_WORKER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 0	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 3
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 15	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 2
		BREAK


	ENDSWITCH
	
	SWITCH eOutfit
	
		CASE OUTFIT_FIXER_SETUP_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_SETUP_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_M_JBIB_16_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_SETUP_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_36_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_M_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_10_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_LEGS_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_6_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IE_M_PHEAD_0_4), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_LEGS_5_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_FEET_1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_2_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_LEGS_5_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 11
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_9_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_LEGS_0_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_M_GLASSES_1_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_ACCS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB1_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_ACCS1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_ACCS1_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_LEGS0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_ACCS1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI2_M_JBIB0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_GOLF_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 12	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 382	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 159	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 0
		BREAK


		CASE OUTFIT_FIXER_GOLF_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 141	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_FEET_1_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 383	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 1

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 156	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 24
		BREAK


		CASE OUTFIT_FIXER_GOLF_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 12	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_FEET_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 382	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 6

		BREAK


		CASE OUTFIT_FIXER_GOLF_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 141	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 7	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 383	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 7

		BREAK


		CASE OUTFIT_FIXER_GOLF_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 12	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 383	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 159	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 0
		BREAK


		CASE OUTFIT_FIXER_GOLF_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 141	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_FEET_1_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 382	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 156	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 24
		BREAK


		CASE OUTFIT_FIXER_GOLF_6
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 12	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_M_FEET_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 383	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 6

		BREAK


		CASE OUTFIT_FIXER_GOLF_7
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 196	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 141	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 7	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 382	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 7

		BREAK
		
		CASE OUTFIT_HEIST_COVERALLS_NAVY
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_M_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_JBIB_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
	
	ENDSWITCH
	
	SWITCH eOutfit
		CASE OUTFIT_LD_ORGANICS_AWARD_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SUM2_M_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Sum2_Tee_001_M")
		BREAK
		
		CASE OUTFIT_SUM22_IAA_AGENT_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 8	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 11	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_8_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 5	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
		BREAK


		CASE OUTFIT_SUM22_IAA_AGENT_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_M_JBIB_5_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_WATCH_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK


		CASE OUTFIT_SUM22_IAA_AGENT_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_LEGS_3_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 13	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_M_WATCH_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK


		CASE OUTFIT_SUM22_IAA_AGENT_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_LEGS_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_M_FEET1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_M_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_8_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 3	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 9
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_DECL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_TEETH_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_6
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_BERD_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_TEETH_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_M_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_BERD_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_34_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 7	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_2_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_M_JBIB_7_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SMUG_M_PHEAD_8_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 5	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_2_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_12_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_27_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_12_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_5_20), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 3	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 10
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_2_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 3	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 1
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Biker_Tee_016_M")
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_42_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_M_LEGS_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 3	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_M_PHEAD_5_21), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Biker_Tee_009_M")
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 5	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_M_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_M_ACCS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_M_JBIB_6_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex


			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Biker_Tee_021_M")
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_6
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_M_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_M_BERD_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TORSO_36_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 5	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 15
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 2	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 13
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_M_JBIB_2_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SMUG_M_PHEAD_8_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_MP_OUTFIT_DATA_FOR_GENERATING_XML_FEMALE_2(MP_OUTFIT_ENUM eOutfit, MP_OUTFITS_DATA &sOutfitsData, scrShopPedComponent &componentItem, scrShopPedProp &propItem)
	SWITCH eOutfit
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_LEGS_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_SPECIAL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_DECL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_JBIB_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_LEGS_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_SPECIAL_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_DECL_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_JBIB_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_LEGS_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_GENERAL_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_SPECIAL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_5_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_3_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_3_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_8_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_4_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_1_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 4	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 14
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_F_PLEFT_WRIST_7_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_F_PRIGHT_WRIST_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_APOCALYPSE_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_JBIB_4_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_LOWR0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_9_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 2	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 11	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_10_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IND_F_PHEAD_6_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_WASTELAND_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_LEGS_3_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_0_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_7_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARENA_F_PHEAD_3_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 8	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_12_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_20_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_17_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_TORSO_1_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_7_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_10_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_6_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		CASE OUTFIT_ARENA_WARS_CONTENDER_SCIFI_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_ARENA_WARS_PETE
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_SPECIAL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_IMPOTENT_RAGE
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 148	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 209	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 122	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 95	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 186	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 304	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] = 78	sOutfitsData.iComponentTextureID[PED_COMP_HAIR] = 0
		BREAK
		
		CASE OUTFIT_CASINO_HIGHROLLER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 6	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 322	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 7
		BREAK
	ENDSWITCH
	
	SWITCH eOutfit
		//Green & Yellow Space Creature
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		//Orange Space Cyclops
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_TORSO_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		//Light Blue Space Horror
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_7_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_TORSO_0_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		//Green Space Cyclops
		CASE OUTFIT_VERSUS_HIDDEN_SPACELING_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_BERD_6_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_TORSO_0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_LEGS_6_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_JBIB_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_UNDERTAKER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 13	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_8_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_9_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 7	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_9_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_9_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex

		BREAK


		
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL2_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL2_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_21_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL2_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_F_WATCH0_3), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_F_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL2_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL2_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL2_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK	
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_29_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_8_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 5	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_JBIB_1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_SPECIAL_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 4	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_12_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_42_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_8_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_SPECIAL_2_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 4	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_8_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_SPECIAL_2_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 4	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_JBIB_25_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 5	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_JBIB_1_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_81_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_F_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 4	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_11_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_15_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_8_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_14_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_5_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_8_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_14_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_68_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_14_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_21_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_11_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_21_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_11_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_21_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_11_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_21_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_11_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_21_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_11_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_21_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_11_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_20_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_47_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_4_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_20_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_2_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_47_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_20_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_5_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_8_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_5_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_8_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_5_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_3_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_8_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_5_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_3_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_8_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_5_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_3_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_8_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_TORSO_5_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_3_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_8_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIB_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_5_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 13	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 53	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_CASINO_HEIST_FIB_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 7	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 13	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 53	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_11_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_CASINO_HEIST_FIB_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 54	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 6	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0

		BREAK

		CASE OUTFIT_CASINO_HEIST_FIB_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 55	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 9	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_29_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_2_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_42_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_0_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_I_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_0_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_21_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_6_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_JBIB_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_21_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 9
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_FEET_1_25), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_19_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE2_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_JBIB_30_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_II_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_21_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_SPECIAL_2_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_8_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_JBIB_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_STEALTH_III_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_0_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_SPECIAL_2_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_6_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_18_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_18_22), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_18_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_29_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_18_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_29_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_18_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_29_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_18_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_10_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_21_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_47_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_42_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_BERD_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_16_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_VALET
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_6
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_FIREFIGHTER_7
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_4_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_4_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_NOOSE_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
	ENDSWITCH
	
	SWITCH eOutfit
	
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_X17_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_X17_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_GRUPPE_SECHS_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_X17_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_2_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_81_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_81_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_3_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_2_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_F_GLASSES_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_55_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_3_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_BUGSTARS_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_BERD_15_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_81_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_14_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 8
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 11	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(-507729881, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(-648709350, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1024027777, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_9_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(550612611, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(285245268, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1024027777, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_9_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(550612611, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(285245268, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(549201043, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(-201458582, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_9_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_CELEB_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 9
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(601355898, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(1391779974, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(-1024027777, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_9_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_55_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 12	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 0
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(574609641, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_81_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_7_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_14_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_6_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_F_GLASSES_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_81_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_7_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_F_FEET_1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_14_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_MAINTENANCE_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_3_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_2_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		CASE OUTFIT_CASINO_HEIST_PRISON_GUARD_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_DECL_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_2_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 2
		BREAK
		
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 6	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_19_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 8	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 14	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_19_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5
		BREAK
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 14	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_20_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_F_EYES0_4), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_CASINO_HEIST_HIGH_ROLLER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 15	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 14	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 13
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_19_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
	
	ENDSWITCH
	
	SWITCH eOutfit
	
//		CASE OUTFIT_CNC_COP_LSPD_OFFICER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_LSPD_OFFICER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_LSPD_OFFICER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_FEET_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//		BREAK
//		CASE OUTFIT_CNC_COP_PARK_RANGER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_PARK_RANGER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_PARK_RANGER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_FEET_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//		BREAK
//		CASE OUTFIT_CNC_COP_SHERIFF_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_4_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_0_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_SHERIFF_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_4_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_5_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_1_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
//		BREAK
//		CASE OUTFIT_CNC_COP_SHERIFF_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_1_2), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_5_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_5), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_1_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
//		BREAK
//		CASE OUTFIT_CNC_COP_HIGHWAY_PATROL_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_HIGHWAY_PATROL_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_HIGHWAY_PATROL_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_FEET_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//		BREAK
//		CASE OUTFIT_CNC_COP_TECH_CONTRACTOR_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_SPECIAL_11_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_JBIB_14_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(266244209, propItem) // MISSING DLC NAME
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TECH_CONTRACTOR_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_1_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 13	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_SPECIAL_12_4), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_JBIB_17_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EAR1_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_1_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TECH_CONTRACTOR_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_1_3), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_1), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_SPECIAL_12_4), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_JBIB_16_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_F_GLASSES_1_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EAR1_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PLEFT_WRIST_3_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TACTICAL_SHERIFF_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TACTICAL_SHERIFF_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_COP_TACTICAL_SHERIFF_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_FEET_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//		BREAK
//		
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_FEET_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//		BREAK
//		CASE OUTFIT_CNC_CROOK_UNAFFILIATED_FOOTSOLDIER_3
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_BALLAS_ENFORCER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_ONEILS_HACKER_0
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_5_0), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_ONEILS_HACKER_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_1_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_5_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_3_2), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
//		BREAK
//		CASE OUTFIT_CNC_CROOK_ONEILS_HACKER_2
//			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_LEGS_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_FEET_0_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_3_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARC1_F_DECL_4_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
//			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_2_0), componentItem)
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
//
//			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_ARC1_F_PHEAD_4_1), propItem)
//			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
//			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
//		BREAK
		
		CASE OUTFIT_SUM20_ALIEN_AWARD
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_BERD_0_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 6	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
		BREAK
	ENDSWITCH
	
	SWITCH eOutfit
		CASE OUTFIT_ISLAND_HEIST_GUARD_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 2	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_F_JBIB_3_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(648441926, propItem) // MISSING DLC NAME
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_GUARD_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_2_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 3	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 14	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_7), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_GUARD_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 14	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_GUARD_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_JBIB_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_1_22), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 9	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_JBIB_6_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 3	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 4
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_JBIB_17_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 2	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_F_LEGS0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 9	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 1

		BREAK
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 13	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 4	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 14
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_JBIB_17_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 3	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_2_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 14	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 14	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 3

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_LEGS_1_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_FEET_1_21), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_13_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_LEGS_1_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_FEET_0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_2_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_JBIB_4_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TEETH_12_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_20_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_1_15), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TEETH_10_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_20_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_2_16), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TEETH_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_0_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_21_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_4_8), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_1_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TEETH_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_21_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H4_F_PHEAD_0_14), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_JBIB_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_FEET_3_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 5	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_JBIB_3_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 14	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 9
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 4	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 14	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 5	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_JBIB_2_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_1_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_2_18), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_JAN_F_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_0_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_1_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_12_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_3_23), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_FEET_4_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_4_24), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_FEET_6_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_5_19), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_18_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_LEGS_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_FEET_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(88471080, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_FEET_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(-1914575127, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_5_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_FEET_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(-1914575127, componentItem)   // MISSING DLC NAME
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 15	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_LEGS_4_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_SPECIAL_1_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 15	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_0_17), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_FEET_6_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 5	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 7

		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_LEGS_0_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_F_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 4	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 9
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_13_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Heist4_Tee_049_F")
			sOutfitsData.iDLCTattooOverlay[1] = HASH("MP_Heist4_Tee_050_F")
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS2_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_FEET004), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_TEETH0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_JBIB_7_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 13	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 0
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 7	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 2
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PLEFT_WRIST_3_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_LEGS_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_FEET010), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_ACCS0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_JBIB_3_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 2	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 5
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_LOWR109), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 15	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 14
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 10
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_JBIB_18_19), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_F_HEAD4_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 6	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 10
		BREAK
		
		CASE OUTFIT_ISLAND_HEIST_BEACH_PARTY_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 2	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 4

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 1	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 7
		BREAK

	ENDSWITCH
	
	SWITCH eOutfit
		CASE OUTFIT_TUNER_ROBBER_BRAVADO
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 6
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_TUNER_ROBBER_LAMPADATI
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 3	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 5	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_JBIB4_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK

		CASE OUTFIT_TUNER_ROBBER_ANORAK
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_LEGS_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_JBIB_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_F_PHEAD_0_13), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_ROBBER_SMOLDER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_46_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_LEGS_0_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_JBIB_1_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IE_F_PHEAD_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_RED_RACING
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_LEGS_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_SPECIAL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_DECL_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_STUNT_F_JBIB_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_TUNER_ROBBER_PUFF
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_LEGS_4_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_JBIB_5_25), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_IE_F_PHEAD_0_5), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_NEW_SKOOL
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_LEGS_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_FEET_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_JBIB_7_15), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_F_PHEAD_0_21), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PLEFT_WRIST_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_ROBBER_POLO
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_TEETH_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW_F_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_F_PEARS_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PLEFT_WRIST_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_CHECK
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_JBIB_4_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW2_F_PEARS_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_ROBBER_LURKER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 2	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_TUNER_ROBBER_ROBBER
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_29_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 11	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 7	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_17_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_TUNER_SECURITY_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 3
		BREAK
		
		CASE OUTFIT_TUNER_SECURITY_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_TUNER_SECURITY_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 6	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 13	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL_F_ACCS0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 7	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_SECURITY_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_LEGS_1_23), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_SPECIAL_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 0	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_5_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_SPECIAL_4_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H4_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_SPECIAL_12_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_F_GLASSES1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_F_PLEFT_WRIST_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BIKER_F_PRIGHT_WRIST_7_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_48_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_ARENA_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 5	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_SPECIAL_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_5_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_TUNER_LOST_MC_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE2_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_SPECIAL_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 1
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_LEGS_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_APA_F_PHEAD_1_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 0	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_F_BERD_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_LEGS_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SMUG_F_PHEAD_8_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_LEGS_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_SPECIAL_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_H3_F_PHEAD_8_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_TUNER_DOCK_WORKER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_TORSO_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_LEGS_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 0	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 3
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HEIST_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
		BREAK


	ENDSWITCH
	
	SWITCH eOutfit
	
		CASE OUTFIT_FIXER_SETUP_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_SPECIAL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_SETUP_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_47_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_JBIB_22_20), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_SETUP_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_22_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_42_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_SPECIAL_13_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_9_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_LEGS_5_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_HIPS_F_GLASSES_1_3), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_VWD_F_PEARS_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_LEGS_0_24), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 13
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_3_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW_F_PEARS_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 12	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_LEGS_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 4	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_4_25), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LOW2_F_PEARS_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PLEFT_WRIST_1_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_PARTY_PROMOTER_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_LEGS_0_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 12
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_TEETH1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 11
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_3_13), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_FIXER_F_PHEAD_2_9), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_F_LEGS_1_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VAL2_F_FEET_0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_VWD_F_JBIB_19_11), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_VWD_F_PEARS_0_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_LEGS_1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 14	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_TEETH_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_JBIB_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PLEFT_WRIST_0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PRIGHT_WRIST_3_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 11	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 8	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_F_FEET1_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_TEETH_11_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_JBIB_0_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_3_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_F_WATCH0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE2_F_PRIGHT_WRIST_5_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_RIGHT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_RIGHT_WRIST] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_BILLIONAIRE_GAMES_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HIPS_F_LEGS1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BUSI_F_FEET0_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_TEETH_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 13	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 15
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_JBIB_2_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BEACH_F_EYES0_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EYES] = propItem.m_textureIndex
			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_LUXE_F_PEARS_8_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_EARS] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_EARS] = propItem.m_textureIndex
		BREAK
		
		CASE OUTFIT_FIXER_GOLF_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_LOWR8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_FEET_1_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 400	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 155	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 24
		BREAK


		CASE OUTFIT_FIXER_GOLF_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 148	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_FEET_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 401	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 3

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 157	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 25
		BREAK


		CASE OUTFIT_FIXER_GOLF_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 14	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 8
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 13
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 400	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 5

		BREAK


		CASE OUTFIT_FIXER_GOLF_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 148	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_FEET_1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 401	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

		BREAK


		CASE OUTFIT_FIXER_GOLF_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BEACH_F_LOWR8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_FEET_1_7), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 401	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 0

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 155	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 24
		BREAK


		CASE OUTFIT_FIXER_GOLF_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 148	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_FEET_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 400	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 3

			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = 157	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = 25
		BREAK


		CASE OUTFIT_FIXER_GOLF_6
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 14	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 8
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 13
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 401	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 5

		BREAK


		CASE OUTFIT_FIXER_GOLF_7
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 241	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 148	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_TUNER_F_FEET_1_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 56	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = 400	sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = 2

		BREAK
		
		CASE OUTFIT_HEIST_COVERALLS_NAVY
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_PILOT_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_JBIB_13_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK
	
	ENDSWITCH
	
	SWITCH eOutfit
		CASE OUTFIT_LD_ORGANICS_AWARD_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 14	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 1	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 1	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_13_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SUM2_F_PHEAD_1_0), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Sum2_Tee_001_F")
		BREAK
		
		CASE OUTFIT_SUM22_IAA_AGENT_0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = 3	sOutfitsData.iComponentTextureID[PED_COMP_LEG] = 8
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_11_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_SUM22_IAA_AGENT_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_LEGS_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_F_WATCH0_3), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_SUM22_IAA_AGENT_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 9	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_LEGS_3_4), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_H3_F_JBIB_4_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_BUSINESS_F_WATCH0_3), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_LEFT_WRIST] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_LEFT_WRIST] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_SUM22_IAA_AGENT_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_LEGS_9_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_FEET_4_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_X17_F_TEETH_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_11_3), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iPropDrawableID[ANCHOR_EYES] = 11	sOutfitsData.iPropTextureID[ANCHOR_EYES] = 0
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_1
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_DECL_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_9_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_2
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_6_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_3
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_TEETH_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_4
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_8_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_5
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_DECL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_DECL] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_6
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_7_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_BERD_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_TORSO_2_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_LEGS_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_FEET_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_TEETH_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 15	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM2_F_JBIB_4_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_F_BERD_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_42_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_LEGS_3_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_GR_F_FEET_0_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_SPECIAL_3_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SUM_F_JBIB_7_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SMUG_F_PHEAD_8_2), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 3
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_11_10), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_32_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_LEGS_4_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 10	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_11_9), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_5_20), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_3
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_IE_F_LEGS_0_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_FEET_1_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_SPECIAL_0_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_11_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Biker_Tee_016_F")
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_4
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TORSO_45_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_12_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_SPECIAL_6_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_11_5), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_GR_F_PHEAD_5_21), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Biker_Tee_009_F")
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_5
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_LEGS_0_8), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_FEET_2_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LUXE_F_SPECIAL_0_16), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LOW2_F_JBIB_3_14), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex
			
			sOutfitsData.iDLCTattooOverlay[0] = HASH("MP_Biker_Tee_021_F")
		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_6
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = 0	sOutfitsData.iComponentTextureID[PED_COMP_BERD] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = 3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BIKER_F_LEGS_1_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = 3	sOutfitsData.iComponentTextureID[PED_COMP_FEET] = 15
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_8_1), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

		BREAK

		CASE OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_7
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_SMUG_F_BERD_10_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_BERD] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_LTS_F_UPPR_1_0), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_TORSO] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_APA_F_LEGS_12_2), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_LEG] = componentItem.m_textureIndex
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_BH_F_FEET_2_6), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_FEET] = componentItem.m_textureIndex
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] = 0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] = 14	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = 0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] = 0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] = 0	sOutfitsData.iComponentTextureID[PED_COMP_DECL] = 0
			GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_FIXER_F_JBIB_7_12), componentItem)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] = componentItem.m_drawableIndex		sOutfitsData.iComponentTextureID[PED_COMP_JBIB] = componentItem.m_textureIndex

			GET_SHOP_PED_PROP(ENUM_TO_INT(DLC_MP_SMUG_F_PHEAD_8_1), propItem)
			sOutfitsData.iPropDrawableID[ANCHOR_HEAD] = propItem.m_propIndex	sOutfitsData.iPropTextureID[ANCHOR_HEAD] = propItem.m_textureIndex
		BREAK
		
	ENDSWITCH
ENDPROC

#ENDIF
