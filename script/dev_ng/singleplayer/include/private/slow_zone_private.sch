//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 		 Slow Zone Private Header							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			11/06/13												│
//│		DESCRIPTION: 	Private header to define and manage slow zones on the	│
//│						SP map.													│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "candidate_public.sch"


ENUM SlowZone
	SZ_FLOYDS_FLAT_STAIRS = 0,
	MAX_SLOW_ZONES
ENDENUM

STRUCT SlowZoneVars
	VECTOR vSZCenter
	VECTOR vSZAngledAreaPosA
	VECTOR vSZAngledAreaPosB
	FLOAT fSZAngledAreaWidth
ENDSTRUCT


STRUCT SlowZoneLocalVars
	INT iSlowZoneToCheckThisFrame
	SlowZoneVars sSlowZone[MAX_SLOW_ZONES]
ENDSTRUCT


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Debug Functions  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

	FUNC STRING GET_SLOW_ZONE_DEBUG_STRING(SlowZone paramSlowZone)
		SWITCH(paramSlowZone)
			CASE SZ_FLOYDS_FLAT_STAIRS		RETURN "SZ_FLOYDS_FLAT_STAIRS"	BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("GET_SLOW_ZONE_DEBUG_STRING: Debug string not defined for slow zone.")
		RETURN "ERROR!"
	ENDFUNC

#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ Data Setup ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SETUP_SLOW_ZONE_DATA(SlowZoneLocalVars &paramLocalVars)
	SlowZone eSlowZone
	
	eSlowZone = SZ_FLOYDS_FLAT_STAIRS
	paramLocalVars.sSlowZone[eSlowZone].vSZCenter			= <<-1145.4558, -1519.6444, 6.3386>>
	paramLocalVars.sSlowZone[eSlowZone].vSZAngledAreaPosA	= <<-1142.947632,-1518.458252,12.018578>>
	paramLocalVars.sSlowZone[eSlowZone].vSZAngledAreaPosB	= <<-1147.955078,-1521.888306,2.838008>>
	paramLocalVars.sSlowZone[eSlowZone].fSZAngledAreaWidth	= 3.750000
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ Main Update ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_SLOW_ZONES(SlowZoneLocalVars &paramLocalVars)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
			SlowZoneVars sZoneToCheck = paramLocalVars.sSlowZone[paramLocalVars.iSlowZoneToCheckThisFrame]
			
			BOOL bInZoneThisFrame = FALSE
		
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), sZoneToCheck.vSZCenter) < 900 //30^2
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
											sZoneToCheck.vSZAngledAreaPosA, 
											sZoneToCheck.vSZAngledAreaPosB,
											sZoneToCheck.fSZAngledAreaWidth)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPRINT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_DIVE)
					bInZoneThisFrame = TRUE
				ENDIF
			ENDIF
			
			IF NOT bInZoneThisFrame
				paramLocalVars.iSlowZoneToCheckThisFrame++
				IF paramLocalVars.iSlowZoneToCheckThisFrame >= ENUM_TO_INT(MAX_SLOW_ZONES)
					paramLocalVars.iSlowZoneToCheckThisFrame = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
