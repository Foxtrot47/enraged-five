USING "Flow_Mission_Data_Public.sch"
USING "vehicle_gen_public.sch"

#IF USE_CLF_DLC
	using "spy_vehicle_system.sch"
#endif 


proc RESOLVE_VEHICLES_AT_MISSION_TRIGGER_PRIVATE(vector vTriggerLocation, vector vMoveToLocation, float fMoveToHeading, vector vAllowableSize, bool deleteIfCharacterVehicle = FALSE,
													   VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT, bool clearTriggerLocation = TRUE,
													   bool checkIfVehicleInGarage = FALSE, bool setVehicleAsVehGen = TRUE, 
													   bool bHeliAllowed = TRUE, bool bPlaneAllowed = TRUE, bool bBoatAllowed = TRUE)
	
	BOOL			bFound
	BOOL			bAllowPlayerLastVehicle
	VECTOR 			vRTemp
	VECTOR			vVehicleCoord
	MODEL_NAMES		VehicleModel
	VEHICLE_INDEX	veh
		
		
	// removing vehicle gen functionality from resolve system
	IF setVehicleAsVehGen
		setVehicleAsVehGen = FALSE
	ENDIF		
	
	CONST_FLOAT 	fCONST_REPOSITION_VEHICLE_RADIUS	30.0

	veh = GET_PLAYERS_LAST_VEHICLE()

	if DOES_ENTITY_EXIST(veh)
		if IS_VEHICLE_DRIVEABLE(veh)
		
			vVehicleCoord 	= GET_ENTITY_COORDS(veh)
			VehicleModel	= GET_ENTITY_MODEL(veh)
			
			bAllowPlayerLastVehicle = TRUE									//allow any driveable last player vehicle by default							
			
			IF IS_THIS_MODEL_A_HELI(VehicleModel)							//allow or disallow helis
				IF bHeliAllowed = FALSE
					bAllowPlayerLastVehicle = FALSE
				ENDIF
			ENDIF	
			
			IF IS_THIS_MODEL_A_PLANE(VehicleModel)							//allow or disallow planes
				IF bPlaneAllowed = FALSE
					bAllowPlayerLastVehicle = FALSE
				ENDIF
			ENDIF
			
			IF IS_THIS_MODEL_A_BOAT(VehicleModel)							//allow or disallow planes
				IF bBoatAllowed = FALSE
					bAllowPlayerLastVehicle = FALSE
				ENDIF
			ENDIF
			
			vector vVehicleSizeMin,vVehicleSizeMax
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh),vVehicleSizeMin,vVehicleSizeMax)
			
			IF vVehicleSizeMax.x - vVehicleSizeMin.x > vAllowableSize.x
				bAllowPlayerLastVehicle = FALSE
			ELIF vVehicleSizeMax.y - vVehicleSizeMin.y > vAllowableSize.y
				bAllowPlayerLastVehicle = FALSE
			ELIF vVehicleSizeMax.z - vVehicleSizeMin.z > vAllowableSize.z
				bAllowPlayerLastVehicle = FALSE
			ENDIF

			
			IF bAllowPlayerLastVehicle
				if deleteIfCharacterVehicle
					if IS_PLAYER_USING_DEFAULT_VEHICLE(eTypePreference)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							vRTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(),vRTemp)
						ENDIF
						if DOES_ENTITY_EXIST(veh)
							if not DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh)
							OR NOT IS_ENTITY_A_MISSION_ENTITY(veh)
								SET_ENTITY_AS_MISSION_ENTITY(veh,TRUE,TRUE)
							ENDIF
							DELETE_VEHICLE(veh)

						ENDIF
					ELSE
						bFound=TRUE
					ENDIF
				ELSE		
					bFound=TRUE
				ENDIF
			
			
				// if the player last vehicle was a taxi that he was passenger in, delete it
				if IS_VEHICLE_DRIVEABLE(veh)
					IF IS_VEHICLE_MODEL(veh, TAXI)
						IF GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) != PLAYER_PED_ID()
						AND GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) != NULL
							if not DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh)
							OR NOT IS_ENTITY_A_MISSION_ENTITY(veh)
								SET_ENTITY_AS_MISSION_ENTITY(veh)
							ENDIF
							DELETE_VEHICLE(veh)

							bFound = FALSE
						ENDIF
					ENDIF
				ENDIF
			
				if checkIfVehicleInGarage
					if IS_VEHICLE_IN_PLAYERS_GARAGE(veh, GET_CURRENT_PLAYER_PED_ENUM(), TRUE)
						bFound = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	if bFound=true
		//bug 392509 - only reposition last known vehicle if within 30m of trigger coord
		IF (VDIST2(vVehicleCoord, vTriggerLocation) < (fCONST_REPOSITION_VEHICLE_RADIUS*fCONST_REPOSITION_VEHICLE_RADIUS))
			
			if DOES_ENTITY_EXIST(veh)
				if not DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh)
				OR NOT IS_ENTITY_A_MISSION_ENTITY(veh)
					SET_ENTITY_AS_MISSION_ENTITY(veh,TRUE,TRUE)					
				ENDIF

				// Cleanup previous vehgens that have been used for mission vehicles
				/*
				IF setVehicleAsVehGen
					if GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH) != veh
						CLEANUP_MISSION_VEHICLE_GEN_VEHICLE()
					endif
				ENDIF
				*/

				CLEAR_AREA(vMoveToLocation,6.0,true)
				SET_ENTITY_COORDS(veh,vMoveToLocation)
				SET_ENTITY_HEADING(veh,fMoveToHeading)
				SET_VEHICLE_DOORS_SHUT(veh, true)
				SET_VEHICLE_ON_GROUND_PROPERLY(veh)
				SET_VEHICLE_ENGINE_ON(veh, FALSE, TRUE)
				
				// Kenneth R.
				// Pass the vehicle over to the vehicle gen controller script so that it persists until the player interacts with it.
				/*
				IF setVehicleAsVehGen
					if GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH) != veh
						SET_MISSION_VEHICLE_GEN_VEHICLE(veh, <<0.0,0.0,0.0>>, 0.0)
					ENDIF
				ENDIF
				*/
			ELSE
				CLEAR_AREA(vMoveToLocation,6.0,true)
			ENDIF
		ENDIF
	ENDIF
	
	IF clearTriggerLocation
		//float clearAreaSize = GET_DISTANCE_BETWEEN_COORDS(vTriggerLocation,vMoveToLocation)	
		
		CLEAR_AREA(vTriggerLocation,LOCATE_SIZE_ANY_MEANS,true)
	ENDIF
ENDPROC

//check if two lines intersect within the boundaries of the line end points
func bool do_lines_cross(vector vA, vector vB, vector vP, vector vU)
	float p0_x, p0_y, p1_x, p1_y 
    float p2_x, p2_y, p3_x, p3_y

	p0_x = vA.x
	p0_y = vA.y
	p1_x = vB.x
	p1_y = vB.y
	p2_x = vP.x
	p2_y = vP.y
	p3_x = vU.x
	p3_y = vU.y

    float s1_x, s1_y, s2_x, s2_y
    s1_x = p1_x - p0_x
	s1_y = p1_y - p0_y
    s2_x = p3_x - p2_x
	s2_y = p3_y - p2_y

    float s, t
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y)
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y)

    if (s >= 0 AND s <= 1 AND t >= 0 AND t <= 1)    
        return true
	endif

    return false // No collision
endfunc

FUNC BOOL DOES_VEHICLE_OVERLAP_ANGLED_AREA(vehicle_index veh, VECTOR VecCoors1, VECTOR VecCoors2, FLOAT AreaWidth)
	IF IS_VEHICLE_DRIVEABLE(veh)
		VecCoors1.z = VecCoors2.z
		vector vNorm = NORMALISE_VECTOR(VecCoors1-VecCoors2)
		vector vTemp = vNorm
		vector vA,vB,vC,vD
		vector vE[4]

		vNorm.x = -vTemp.y
		vNorm.y = vTemp.x
		vNorm.z = 0
		
		vA = VecCoors1 - (vNorm * (AreaWidth / 2.0))
		vB = VecCoors1 + (vNorm * (AreaWidth / 2.0))
		vC = VecCoors2 - (vNorm * (AreaWidth / 2.0))
		vD = VecCoors2 + (vNorm * (AreaWidth / 2.0))
		
		//find corners of vehicle			
		vector vVehicleSizeMin,vVehicleSizeMax
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh),vVehicleSizeMin,vVehicleSizeMax)

		vE[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh,<<vVehicleSizeMin.x,vVehicleSizeMin.y,0>>)
		vE[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh,<<vVehicleSizeMin.x,vVehicleSizeMax.y,0>>)
		vE[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh,<<vVehicleSizeMax.x,vVehicleSizeMin.y,0>>)
		vE[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh,<<vVehicleSizeMax.x,vVehicleSizeMax.y,0>>)

		if do_lines_cross(ve[0],ve[1],vA,vB)
		or do_lines_cross(ve[0],ve[1],vB,vD)
		or do_lines_cross(ve[0],ve[1],vC,vD)
		or do_lines_cross(ve[0],ve[1],vA,vC)
		
		or do_lines_cross(ve[1],ve[3],vA,vB)
		or do_lines_cross(ve[1],ve[3],vB,vD)
		or do_lines_cross(ve[1],ve[3],vC,vD)
		or do_lines_cross(ve[1],ve[3],vA,vC)
		
		or do_lines_cross(ve[3],ve[2],vA,vB)
		or do_lines_cross(ve[3],ve[2],vB,vD)
		or do_lines_cross(ve[3],ve[2],vC,vD)
		or do_lines_cross(ve[3],ve[2],vA,vC)
		
		or do_lines_cross(ve[2],ve[0],vA,vB)
		or do_lines_cross(ve[2],ve[0],vB,vD)
		or do_lines_cross(ve[2],ve[0],vC,vD)
		or do_lines_cross(ve[2],ve[0],vA,vC)
			RETURN TRUE
		endif
	endif
	RETURN FALSE
ENDFUNC

/// PURPOSE: restores the 3 vehicle healths to a safe level. 
///    		 e.g. Incase the vehicle is resolved with an engine health of 100 (Driveable). Code automatically reduces 
///    		 the engine health if the health is <= 200. Therfore the vehicle could become undriveable by the time a 
///          mocap cutscene has finished playing and the mission resumes.   
proc make_vehicle_healths_safe(vehicle_index veh)

	if does_entity_exist(veh)
		if is_vehicle_driveable(veh)

			if get_vehicle_engine_health(veh) <= 200 //code automatically reduce health <= 200
				set_vehicle_engine_health(veh, 500)
			endif 
			
			if get_vehicle_petrol_tank_health(veh) <= 700 //code automatically reduce health <= 700
				set_vehicle_engine_health(veh, 900)
			endif 
			
			if get_entity_health(veh) < 200
				set_vehicle_engine_health(veh, 500)
			endif 

		endif 
	endif 
	
endproc 

PROC PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA(VECTOR VecCoors1, VECTOR VecCoors2, FLOAT AreaWidth, vector vMoveToCoord, float fMoveToHeading, vector vMaxAllowableSize, bool bClearAngledArea=TRUE, bool setVehicleAsVehGen=TRUE, bool ignoreVehicleInPlayerGarage=TRUE, bool EngineOffDoorsClosed=FALSE, bool allow_make_vehicle_healths_safe = false)
	// removing vehicle gen functionality from resolve system
	IF setVehicleAsVehGen
		setVehicleAsVehGen = FALSE
	ENDIF
				
	VEHICLE_INDEX	veh
	bool bVehIsNotSafe
	bool bAllowPlayerLastVehicle = TRUE
	bool bSetAsMissonEntity = FALSE
	
	veh = GET_PLAYERS_LAST_VEHICLE()	
	
	
	if DOES_ENTITY_EXIST(veh)
		
		// Bug fix - because CLEAR_ANGLED_AREA_OF_VEHICLES deletes any vehicle classed as abandoned (even if it's outside of the specified area), the player's last vehicle could get deleted
		// setting it as a mission entity stops this happening, it's gets set as SET_VEHICLE_AS_NO_LONGER_NEEDED again at the end of the proc
		if not IS_ENTITY_A_MISSION_ENTITY(veh)
			SET_ENTITY_AS_MISSION_ENTITY(veh)				
			bSetAsMissonEntity = TRUE
			CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " SET_ENTITY_AS_MISSION_ENTITY")
		ENDIF
		
		if IS_VEHICLE_DRIVEABLE(veh)
			
			vector vVehicleSizeMin,vVehicleSizeMax
			
			if allow_make_vehicle_healths_safe
				make_vehicle_healths_safe(veh)
			endif 

			IF IS_ENTITY_IN_ANGLED_AREA(veh,VecCoors1,VecCoors2,AreaWidth)
				bVehIsNotSafe = TRUE
			
			ELSE
				vector vCar = GET_ENTITY_COORDS(veh)
				IF (vCar.z > VecCoors1.z and vCar.z < VecCoors2.z)
				OR (vCar.z > VecCoors2.z and vCar.z < VecCoors1.z)
			
					//find corners of angled area
					IF DOES_VEHICLE_OVERLAP_ANGLED_AREA(veh,VecCoors1,VecCoors2,AreaWidth)
						bVehIsNotSafe = TRUE
						
					ENDIF
				endif
			ENDIF						

			// if the player last vehicle was a taxi that he was passenger in, delete it
			//this below was originally added in rev #18 of clearmissionarea.sch. That fix turns out to delete the player from a taxi wherever they are in the map.
			//change below to deal with this. See cause, bug 1910193 NG.
			
			
			if IS_VEHICLE_DRIVEABLE(veh)
				IF IS_VEHICLE_MODEL(veh, TAXI)					
					IF GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) != PLAYER_PED_ID()
					AND GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) != NULL
						IF GET_DISTANCE_BETWEEN_COORDS((VecCoors1+VecCoors2)/2.0,get_entity_coords(veh)) < 20
							//DELETE_VEHICLE(veh) //removed from here so it can be dealth with below.
							bVehIsNotSafe = true
							bAllowPlayerLastVehicle = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
				
			//if player's last vehicle is in a player garage, don't reposition it and don't let it be deleted.	
			if ignoreVehicleInPlayerGarage
				if IS_VEHICLE_IN_PLAYERS_GARAGE(veh, GET_CURRENT_PLAYER_PED_ENUM(), TRUE)
					bVehIsNotSafe = FALSE
				ENDIF
			ENDIF
			
			/*
			if not IS_ENTITY_A_MISSION_ENTITY(veh)
				SET_ENTITY_AS_MISSION_ENTITY(veh)				
			ENDIF
			*/


			if bVehIsNotSafe
				//check if vehicle is of the allowable size
								
				IF NOT IS_VECTOR_ZERO(vMaxAllowableSize)	
					IF IS_VEHICLE_DRIVEABLE(veh)
						model_names vehModel
						vehModel = GET_ENTITY_MODEL(veh)
						
						GET_VEHICLE_SIZE(veh, vVehicleSizeMin, vVehicleSizeMax) //GET_MODEL_DIMENSIONS(vehModel,vVehicleSizeMin,vVehicleSizeMax)
						
						IF IS_THIS_MODEL_A_HELI(vehModel)
							//choppers seem to have unreasonably large dimensions. Much larger than their blades.
							vMaxAllowableSize.x += 3
							vMaxAllowableSize.y += 3
						ENDIF
						
						IF ENUM_TO_INT(vehModel) = HASH("zentorno")		//#1696370
						OR ENUM_TO_INT(vehModel) = HASH("btype")		//#1738925
						OR ENUM_TO_INT(vehModel) = HASH("dubsta3")		//#1846988
						OR ENUM_TO_INT(vehModel) = HASH("Monster")		//#1891164
							vMaxAllowableSize *= 1.1
						ELIF ENUM_TO_INT(vehModel) = HASH("t20")		//#2333237 & 2335303
						OR ENUM_TO_INT(vehModel) = HASH("virgo")		//#2345087
							vMaxAllowableSize *= 1.2
						ENDIF
						
						IF vVehicleSizeMax.x - vVehicleSizeMin.x > vMaxAllowableSize.x
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle X too large [", vVehicleSizeMax.x - vVehicleSizeMin.x, " > ", vMaxAllowableSize.x, "]")
							bAllowPlayerLastVehicle = FALSE
						ELIF vVehicleSizeMax.y - vVehicleSizeMin.y > vMaxAllowableSize.y
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle Y too large [", vVehicleSizeMax.y - vVehicleSizeMin.y, " > ", vMaxAllowableSize.y, "]")
							bAllowPlayerLastVehicle = FALSE
						ELIF vVehicleSizeMax.z - vVehicleSizeMin.z > vMaxAllowableSize.z
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle Z too large [", vVehicleSizeMax.z - vVehicleSizeMin.z, " > ", vMaxAllowableSize.z, "]")
							bAllowPlayerLastVehicle = FALSE
						ELSE
							
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle within size limits")
						ENDIF
					ENDIF
				endif
				
			
	
				IF IS_VEHICLE_DRIVEABLE(veh)
					IF bAllowPlayerLastVehicle
						CLEAR_AREA_OF_VEHICLES(vMoveToCoord,5)
						SET_ENTITY_HEADING(veh,fMoveToHeading)
						SET_ENTITY_COORDS(veh,vMoveToCoord)
						set_vehicle_on_ground_properly(veh)
						
						IF EngineOffDoorsClosed
							SET_VEHICLE_ENGINE_ON(veh, FALSE, TRUE)
							SET_VEHICLE_DOORS_SHUT(veh,TRUE)
						ENDIF
						CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " repositioned ", vMoveToCoord)
					else
						if NOT IS_ENTITY_A_MISSION_ENTITY(veh)
						OR NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh)
							SET_ENTITY_AS_MISSION_ENTITY(veh,true,true)
						ENDIF
						IF IS_PED_IN_VEHICLE(player_ped_id(),veh)
							SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(veh))							
						ENDIF
						DELETE_VEHICLE(veh)
						CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " deleted")
					endif
				endif
			endif
										
			//These two were in the previous vehicle check.	Removing as these should prob be done on a case by case basis. Want to try and leave things as the player found them.
			//SET_VEHICLE_DOORS_SHUT(veh, true)
			//SET_VEHICLE_ENGINE_ON(veh, FALSE, TRUE)							
		
							
			// Kenneth R.
			// Pass the vehicle over to the vehicle gen controller script so that it persists until the player interacts with it.
			/*
			if IS_VEHICLE_DRIVEABLE(veh)
				SET_VEHICLE_ON_GROUND_PROPERLY(veh)
				IF setVehicleAsVehGen
					// Cleanup previous vehgens that have been used for mission vehicles
					IF GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH) != veh
						CLEANUP_MISSION_VEHICLE_GEN_VEHICLE()
						SET_MISSION_VEHICLE_GEN_VEHICLE(veh, <<0.0,0.0,0.0>>, 0.0)
					ENDIF
				ENDIF
			ENDIF
			*/
			
			IF bClearAngledArea
				CLEAR_ANGLED_AREA_OF_VEHICLES(VecCoors1, VecCoors2, AreaWidth)
			ENDIF
			
			IF bSetAsMissonEntity = TRUE
				IF DOES_ENTITY_EXIST(veh)
					IF IS_ENTITY_A_MISSION_ENTITY(veh)
						CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " SET_VEHICLE_AS_NO_LONGER_NEEDED ")
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			if not IS_ENTITY_A_MISSION_ENTITY(veh)
				SET_ENTITY_AS_MISSION_ENTITY(veh)				
			ENDIF
			
			//warp peds from undriveable car and delete undriveable car.
			ped_index aPed
			
			aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_DRIVER)
			IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
				SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
			ENDIF
			
			int iSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(veh))
			IF iSeats <= 2				
				aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_FRONT_RIGHT)
				IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
					SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
				ENDIF
			ENDIF
			
			IF iSeats <= 4
				aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_BACK_LEFT)
				IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
					SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
				ENDIF
				aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_BACK_RIGHT)
				IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
					SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
				ENDIF
			ENDIF
			
			
			#IF USE_CLF_DLC
				if spy_vehicle_is_this_vehicle_the_spy_vehicle(veh)
			    	spy_vehicle_delete_spy_vehicle()
				else 
					delete_vehicle(veh)
				endif 
			#ENDIF

			#IF NOT USE_CLF_DLC
				delete_vehicle(veh)
			#ENDIF
			
		ENDIF	
	endif
	
ENDPROC

//-----------SPY_VEHICLE
#IF USE_CLF_DLC

PROC private_reposition_spy_vehicle_and_last_players_vehicle_inside_angled_area(VECTOR VecCoors1, VECTOR VecCoors2, FLOAT AreaWidth, vector vMoveToCoord, float fMoveToHeading, vector spy_veh_move_to_coord, float spy_veh_move_to_heading, vector vMaxAllowableSize, bool bClearAngledArea=TRUE, bool ignoreVehicleInPlayerGarage=TRUE, bool EngineOffDoorsClosed=FALSE, bool allow_make_vehicle_healths_safe = true)
				
	VEHICLE_INDEX veh
	bool bVehIsNotSafe
	bool bAllowPlayerLastVehicle = TRUE
	bool bSetAsMissonEntity = FALSE
	
	if spy_vehicle_does_global_spy_vehicle_exist()
	
		veh = spy_vehicle_get_vehicle_index()
		
		// Bug fix - because CLEAR_ANGLED_AREA_OF_VEHICLES deletes any vehicle classed as abandoned (even if it's outside of the specified area), the player's last vehicle could get deleted
		// setting it as a mission entity stops this happening, it's gets set as SET_VEHICLE_AS_NO_LONGER_NEEDED again at the end of the proc
//		if not IS_ENTITY_A_MISSION_ENTITY(veh)
//			SET_ENTITY_AS_MISSION_ENTITY(veh)				
//			bSetAsMissonEntity = TRUE
//			CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " SET_ENTITY_AS_MISSION_ENTITY")
//		ENDIF
		
		if IS_VEHICLE_DRIVEABLE(veh)
			
			vector vVehicleSizeMin,vVehicleSizeMax
			
			if allow_make_vehicle_healths_safe
				make_vehicle_healths_safe(veh)
			endif 

			IF IS_ENTITY_IN_ANGLED_AREA(veh,VecCoors1,VecCoors2,AreaWidth)
				bVehIsNotSafe = TRUE
			
			ELSE
				vector vCar = GET_ENTITY_COORDS(veh)
				IF (vCar.z > VecCoors1.z and vCar.z < VecCoors2.z)
				OR (vCar.z > VecCoors2.z and vCar.z < VecCoors1.z)
			
					//find corners of angled area
					IF DOES_VEHICLE_OVERLAP_ANGLED_AREA(veh,VecCoors1,VecCoors2,AreaWidth)
						bVehIsNotSafe = TRUE
						
					ENDIF
				endif
			ENDIF						

			// if the player last vehicle was a taxi that he was passenger in, delete it
			//this below was originally added in rev #18 of clearmissionarea.sch. That fix turns out to delete the player from a taxi wherever they are in the map.
			//change below to deal with this. See cause, bug 1910193 NG.
			
			
			if IS_VEHICLE_DRIVEABLE(veh)
				IF IS_VEHICLE_MODEL(veh, TAXI)					
					IF GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) != PLAYER_PED_ID()
					AND GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER) != NULL
						IF GET_DISTANCE_BETWEEN_COORDS((VecCoors1+VecCoors2)/2.0,get_entity_coords(veh)) < 20
							//DELETE_VEHICLE(veh) //removed from here so it can be dealth with below.
							bVehIsNotSafe = true
							bAllowPlayerLastVehicle = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
				
			//if player's last vehicle is in a player garage, don't reposition it and don't let it be deleted.	
			if ignoreVehicleInPlayerGarage
				if IS_VEHICLE_IN_PLAYERS_GARAGE(veh, GET_CURRENT_PLAYER_PED_ENUM(), TRUE)
					bVehIsNotSafe = FALSE
				ENDIF
			ENDIF


			if bVehIsNotSafe
				//check if vehicle is of the allowable size
								
				IF NOT IS_VECTOR_ZERO(vMaxAllowableSize)	
					IF IS_VEHICLE_DRIVEABLE(veh)
						model_names vehModel
						vehModel = GET_ENTITY_MODEL(veh)
						
					//	vector vVehicleSizeMinb,vVehicleSizeMax
						GET_MODEL_DIMENSIONS(vehModel,vVehicleSizeMin,vVehicleSizeMax)
						
						If IS_THIS_MODEL_A_HELI(vehModel)
							//choppers seem to have unreasonably large dimensions. Much larger than their blades.
							vMaxAllowableSize.x += 3
							vMaxAllowableSize.y += 3
						ENDIF
					//	cprintln(debug_trevor3,vVehicleSizeMax," ",vMaxAllowableSize)
					
						#IF NOT USE_TU_CHANGES
						IF vVehicleSizeMax.x - vVehicleSizeMin.x > vMaxAllowableSize.x
							CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle X too large")
							bAllowPlayerLastVehicle = FALSE
						ELIF vVehicleSizeMax.y - vVehicleSizeMin.y > vMaxAllowableSize.y
							CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle Y too large")
							bAllowPlayerLastVehicle = FALSE
						ELIF vVehicleSizeMax.z - vVehicleSizeMin.z > vMaxAllowableSize.z
							CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle Z too large")
							bAllowPlayerLastVehicle = FALSE
						ELSE
							
							CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle within size limits")
						ENDIF
						#ENDIF
						#IF USE_TU_CHANGES
						IF ENUM_TO_INT(vehModel) = HASH("zentorno")		//#1696370
						OR ENUM_TO_INT(vehModel) = HASH("btype")		//#1738925
						OR ENUM_TO_INT(vehModel) = HASH("dubsta3")		//#1846988
						OR ENUM_TO_INT(vehModel) = HASH("Monster")		//#1891164
							vMaxAllowableSize *= 1.1
						ENDIF
						
						IF vVehicleSizeMax.x - vVehicleSizeMin.x > vMaxAllowableSize.x
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle X too large [", vVehicleSizeMax.x - vVehicleSizeMin.x, " > ", vMaxAllowableSize.x, "]")
							bAllowPlayerLastVehicle = FALSE
						ELIF vVehicleSizeMax.y - vVehicleSizeMin.y > vMaxAllowableSize.y
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle Y too large [", vVehicleSizeMax.y - vVehicleSizeMin.y, " > ", vMaxAllowableSize.y, "]")
							bAllowPlayerLastVehicle = FALSE
						ELIF vVehicleSizeMax.z - vVehicleSizeMin.z > vMaxAllowableSize.z
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle Z too large [", vVehicleSizeMax.z - vVehicleSizeMin.z, " > ", vMaxAllowableSize.z, "]")
							bAllowPlayerLastVehicle = FALSE
						ELSE
							
							CPRINTLN(DEBUG_MISSION, "#TU PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - vehicle within size limits")
						ENDIF
						#ENDIF
					ENDIF
				endif
				
			
	
				IF IS_VEHICLE_DRIVEABLE(veh)
					IF bAllowPlayerLastVehicle
						CLEAR_AREA_OF_VEHICLES(spy_veh_move_to_coord,5)
						SET_ENTITY_HEADING(veh,spy_veh_move_to_heading)
						SET_ENTITY_COORDS(veh,spy_veh_move_to_coord)
						set_vehicle_on_ground_properly(veh)
						
						IF EngineOffDoorsClosed
							SET_VEHICLE_ENGINE_ON(veh, FALSE, TRUE)
							SET_VEHICLE_DOORS_SHUT(veh,TRUE)
						ENDIF
						CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " repositioned")
					else
						if NOT IS_ENTITY_A_MISSION_ENTITY(veh)
						OR NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh)
							SET_ENTITY_AS_MISSION_ENTITY(veh,true,true)
						ENDIF
						IF IS_PED_IN_VEHICLE(player_ped_id(),veh)
							SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(veh))							
						ENDIF
						DELETE_VEHICLE(veh)
						CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " deleted")
					endif
				endif
			endif
			
			IF bClearAngledArea
				CLEAR_ANGLED_AREA_OF_VEHICLES(VecCoors1, VecCoors2, AreaWidth)
			ENDIF
			
			IF bSetAsMissonEntity = TRUE
				IF DOES_ENTITY_EXIST(veh)
					IF IS_ENTITY_A_MISSION_ENTITY(veh)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
						CPRINTLN(DEBUG_MISSION, "PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA - player last vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(veh), " SET_VEHICLE_AS_NO_LONGER_NEEDED")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			if not IS_ENTITY_A_MISSION_ENTITY(veh)
				SET_ENTITY_AS_MISSION_ENTITY(veh)				
			ENDIF
			
			//warp peds from undriveable car and delete undriveable car.
			ped_index aPed
			
			aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_DRIVER)
			IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
				SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
			ENDIF
			
			int iSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(veh))
			IF iSeats <= 2				
				aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_FRONT_RIGHT)
				IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
					SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
				ENDIF
			ENDIF
			
			IF iSeats <= 4
				aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_BACK_LEFT)
				IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
					SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
				ENDIF
				aPed = GET_PED_IN_VEHICLE_SEAT(veh,VS_BACK_RIGHT)
				IF DOES_ENTITY_EXIST(aPed) AND NOT IS_PED_INJURED(aPed)
					SET_ENTITY_COORDS(aPed,GET_ENTITY_COORDS(aPed))
				ENDIF
			ENDIF
			
			spy_vehicle_delete_spy_vehicle()

		ENDIF
		
	endif
	
	
	//resolve the last players vehicle
	
	PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA(VecCoors1, VecCoors2, AreaWidth, vMoveToCoord, fMoveToHeading, vMaxAllowableSize, bClearAngledArea, TRUE, ignoreVehicleInPlayerGarage, EngineOffDoorsClosed, allow_make_vehicle_healths_safe)
	
ENDPROC

#endif 
//----------

//handy for checking if any vehicles are blocking an area near a ped. Uses the overlapping command above
FUNC BOOL CHECK_AREA_NEAR_TO_PED_FREE_OF_CARS(PED_INDEX pedToCheck,vector vLocate1,vector vLocate2, float areaWidth)
	VEHICLE_INDEX vehCars[20]
	int iNumCars
	int i
	IF NOT IS_PED_INJURED(pedToCheck)
		iNumCars = GET_PED_NEARBY_VEHICLES(pedToCheck,vehCars)
		FOR i = 0 to iNumCars-1
			IF IS_VEHICLE_DRIVEABLE(vehCars[i])
				IF DOES_VEHICLE_OVERLAP_ANGLED_AREA(vehCars[i],vLocate1,vLocate2,areaWidth)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN TRUE
ENDFUNC
