
//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	Ambient_Speech.sch											//
//		AUTHOR			:	Brenda Carey												//
//		DESCRIPTION		:	Functions for reading in from dialoguestar					//
//							and playing dialogue										//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
///    
///    
///    
///    
///   
///    
///    
///  

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_path.sch"
USING "dialogue_public.sch"




//PROC GetModelEnums(TEXT_LABEL MissionPrefix, MODEL_NAMES& ModelList[], TEXT_LABEL Role)
//
//	INT I = 0
//	INT NumberOfModels = 0
//
//	TEXT_LABEL ConstructedLabel
//	
//	//Start with the mission prefix
//	ConstructedLabel = MissionPrefix
//	
//	//Add _Model_
//	ConstructedLabel += "_MODELS_"
//	
//	//Add the role
//	ConstructedLabel += Role
//	
//
//	STRING TheString = GET_STRING_FROM_TEXT_FILE(aString)
//
//	WHILE GET_LENGTH_OF_LITERAL_STRING(TheString) > I
//		TEXT_LABEL IndividualLetter = GET_STRING_FROM_STRING(TheString, I, I+1)
//		IF NOT ARE_STRINGS_EQUAL(IndividualLetter, " ")
//			Result += IndividualLetter
//		ELSE
//			ModelList[NumberOfModels] = Result
//			NumberOfModels++
//		ENDIF
//		I++
//	ENDWHILE
//
//ENDPROC
/*
FUNC TEXT_LABEL ReturnMissionPrefix(STRING SubtitleGroupID)

	TEXT_LABEL MissionPrefix = "PRE_" 
	MissionPrefix += SubtitleGroupID
	
	MissionPrefix = GET_STRING_FROM_TEXT_FILE(MissionPrefix)
	RETURN MissionPrefix

ENDFUNC
*/

/*
FUNC TEXT_LABEL_63 GetSuffixUsed(TEXT_LABEL MissionPrefix, PED_INDEX SpeakingPed)

	//REBRK_G_M_Y_MexGoon_02
	INT PedModelHash = ENUM_TO_INT(GET_ENTITY_MODEL(SpeakingPed)) 
	TEXT_LABEL_63 PrefixLabel = MissionPrefix 
	PrefixLabel += "_" 
	PrefixLabel += PedModelHash
	RETURN PrefixLabel
	
ENDFUNC
*/

/*
FUNC TEXT_LABEL ConstructFinalLineToPlay(STRING LineRoot, TEXT_LABEL ActualPrefix)

	TEXT_LABEL LineToPlay
	LineToPlay = LineRoot 
	LineToPlay += ActualPrefix
	RETURN LineToPlay
ENDFUNC
*/

/*
FUNC TEXT_LABEL GetVoiceForPed(PED_INDEX SpeakingPed, STRING SubtitleGroupID)
	
	TEXT_LABEL MissionPrefix = ReturnMissionPrefix(SubtitleGroupID)
	
	TEXT_LABEL PedsVoice = MissionPrefix
	PedsVoice += "_VOICE_"
	INT PedModelHash = ENUM_TO_INT(GET_ENTITY_MODEL(SpeakingPed)) 
	PedsVoice += PedModelHash

	RETURN PedsVoice
ENDFUNC
*/

FUNC BOOL PLAY_AMBIENT_DIALOGUE_LINE(structPedsForConversation& Conversation, PED_INDEX SpeakingPed, STRING SubtitleGroupID, STRING LineRoot)
	TEXT_LABEL LineToPlay
	REQUEST_ADDITIONAL_TEXT(SubtitleGroupID, MISSION_DIALOGUE_TEXT_SLOT)
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		IF NOT IS_PED_INJURED(SpeakingPed)
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: SubtitleGroupID = ")
			PRINTSTRING(SubtitleGroupID)
			PRINTNL()
			PRINTSTRING("***********************************")
			
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: LineRoot = ")
			PRINTSTRING(LineRoot)
			PRINTNL()
			PRINTSTRING("***********************************")
		
	/*	
			TEXT_LABEL MissionPrefix = ReturnMissionPrefix(SubtitleGroupID)
			
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: MissionPrefix = ")
			PRINTSTRING(MissionPrefix)
			PRINTNL()
			PRINTSTRING("***********************************")
	*/
			//REBRK_G_M_Y_MexGoon_02
	/*
			TEXT_LABEL_63 PrefixLabel = GetSuffixUsed(MissionPrefix, SpeakingPed)
			
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: PrefixLabel = ")
			PRINTSTRING(PrefixLabel)
			PRINTNL()
			PRINTSTRING("***********************************")
	*/

			//Get DialogueStar Chosen Prefix For this Ped
	/*
			TEXT_LABEL ActualPrefix = GET_STRING_FROM_TEXT_FILE(PrefixLabel)
			
			IF IS_STRING_NULL(ActualPrefix)
			OR ARE_STRINGS_EQUAL(ActualPrefix, "NULL")
				PRINTSTRING("***********************************")
				PRINTNL()
				PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: Model Mismatch between script and dialogueStar")
				PRINTNL()
				PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: It's possible you haven't used LOAD_ADDITIONAL_TEXT for this block")
				PRINTNL()
				PRINTSTRING("***********************************")
			ENDIF
			
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: ActualPrefix = ")
			PRINTSTRING(ActualPrefix)
			PRINTNL()
			PRINTSTRING("***********************************")
	*/		
			//Construct the Line to be played ... REBRK_START4
	/*
			LineToPlay = ConstructFinalLineToPlay(LineRoot, ActualPrefix)
			 
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: LineToPlay = ")
			PRINTSTRING(LineToPlay)
			PRINTNL()
			PRINTSTRING("***********************************") 
	*/		 
			LineToPlay = GET_TEXT_KEY_FOR_LINE_OF_AMBIENT_SPEECH(SpeakingPed, SubtitleGroupID, LineRoot)
			
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: LineToPlay = ")
			PRINTSTRING(LineToPlay)
			PRINTNL()
			PRINTSTRING("***********************************") 
		 
			IF CREATE_CONVERSATION(Conversation, SubtitleGroupID, LineToPlay, CONV_PRIORITY_MEDIUM)

				RETURN TRUE
			
			ENDIF
		
		ELSE
			PRINTSTRING("***********************************")
			PRINTNL()
			PRINTSTRING("PLAY_AMBIENT_DIALOGUE_LINE: Passed in an injured ped")
			PRINTNL()
			PRINTSTRING("***********************************")
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC





