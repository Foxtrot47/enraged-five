//╒═════════════════════════════════════════════════════════════════════════════╕
//│                           Cutscene Private Header                           │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│                                                                             │
//│       AUTHOR:         Ben Rollinson               					        │
//│       DATE:           13/06/13                                              │
//│       DESCRIPTION:    Private script commands related to cutscenes.         │                                 
//│                                                                             │
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_cutscene.sch"
USING "commands_camera.sch"


//To be called by the main thread to manage per-frame calls that need to run while
//scripts are set safe for cutscene.
PROC PRIVATE_Safe_For_Cutscene_Update()
	IF g_bScriptsSetSafeForCutscene
		THEFEED_HIDE_THIS_FRAME()
	ENDIF
ENDPROC


//To be called by the main thread to manage fading the game world out to help facilitate
//better streaming if cutscenes fail to load in time.
PROC PRIVATE_Cutscene_Failsafe_Update()
	IF g_bCutsceneFailsafeActive
		IF IS_CUTSCENE_PLAYING()
			CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> A cutscene started running while scripts were in failsafe mode. Clearing failsafe mode.")
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Fading the screen in as cutscene failsafe mode ends.")
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			g_bCutsceneFailsafeActive = FALSE
		ELIF NOT IS_CUTSCENE_ACTIVE()
			CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Cutscene was discarded while scripts were in failsafe mode. Clearing failsafe mode.")
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Fading the screen in as cutscene failsafe mode ends.")
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			g_bCutsceneFailsafeActive = FALSE		
		ELIF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Fading the screen out as cutscene failsafe mode starts.")
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD

	CONST_INT	UNAUTH_CUT_SCREEN_DIPLAY_TIME	15000

	PROC PRIVATE_Debug_Update_Unauthorized_Cutscene_Screen(BOOL &paramGamePaused)
		IF g_iUnauthCutTimeDisplayed != -1
			IF g_iUnauthCutTimeDisplayed < UNAUTH_CUT_SCREEN_DIPLAY_TIME
				g_iUnauthCutTimeDisplayed += ROUND(GET_FRAME_TIME()*1000)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(g_txtUnauthCutName)
					IF NOT paramGamePaused
						SET_GAME_PAUSED(TRUE)
						paramGamePaused = TRUE
					ENDIF
					HIDE_LOADING_ON_FADE_THIS_FRAME()
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
					DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)

					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
					SET_TEXT_SCALE (0.7, 0.55)
					SET_TEXT_WRAP(0.0, 1.0)
					SET_TEXT_DROPSHADOW (0, 0, 0, 0, 255)
					SET_TEXT_COLOUR(255, 255, 255, 255)
					SET_TEXT_EDGE (0, 0, 0, 0, 255)     
					SET_TEXT_PROPORTIONAL (FALSE)                                                 
					SET_TEXT_FONT(FONT_STANDARD)
					DISPLAY_TEXT_WITH_LITERAL_STRING(	0.5 - (GET_STRING_WIDTH_WITH_STRING("UCUT_MAIN", g_txtUnauthCutName)), 
														0.5, 
														"UCUT_MAIN",
														g_txtUnauthCutName)
				ELSE
					SCRIPT_ASSERT("Trying to display an unathorized cutscene screen for a null or empty cutscene name.")
					CPRINTLN(DEBUG_FLOW_CUTS, "<UNAUTH-CUT> Unathorized cutscene screen was cancelled due to invalid cutscene name.")
					g_iUnauthCutTimeDisplayed = -1
					g_txtUnauthCutName = ""
					SET_GAME_PAUSED(FALSE)
					paramGamePaused = FALSE
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					CPRINTLN(DEBUG_FLOW_CUTS, "<UNAUTH-CUT> Unathorized cutscene screen for ", g_txtUnauthCutName, " was skipped by player.")
					g_iUnauthCutTimeDisplayed = -1
					g_txtUnauthCutName = ""
					SET_GAME_PAUSED(FALSE)
					paramGamePaused = FALSE
				ENDIF				
			ELSE
				CPRINTLN(DEBUG_FLOW_CUTS, "<UNAUTH-CUT> Unathorized cutscene screen for ", g_txtUnauthCutName, " timed out.")
				g_iUnauthCutTimeDisplayed = -1
				g_txtUnauthCutName = ""
				SET_GAME_PAUSED(FALSE)
				paramGamePaused = FALSE
			ENDIF
		ENDIF
	ENDPROC
	
#ENDIF

