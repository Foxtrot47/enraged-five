USING "rage_builtins.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_graphics.sch"
USING "commands_script.sch"

USING "player_ped_public.sch"

USING "script_player.sch"
USING "candidate_public.sch"
USING "title_update_globals.sch"
USING "buildtype.sch"
USING "mp_globals_fm.sch"
//USING "net_gang_boss.sch"
//USING "cellphone_public.sch"


//Make sure to do a GET_CELLPHONE_OWNER before calling this...

PROC SetUpPhoneSurroundTint() //Bit daft and inconsistent with colour of blips on radar for character missions. Requested by Stu Petri in url:bugstar:1827354

    //WARNING! The tints are not uniform ids. "Blue" is not tint ID 0 across all phone props.

    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())  

       IF g_Cellphone.PhoneOwner = CHAR_MICHAEL //Franklin's tints seem jumbled...

            #if USE_CLF_DLC
            SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            #if USE_NRM_DLC
            SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            #if not USE_SP_DLC
            SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            
                CASE 1 //Light Blue Theme from Settings Menu
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 0)
                BREAK

                CASE 2 //Green
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 1)
                BREAK

                CASE 3 //Red
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 2)
                BREAK

                CASE 4 //Orange
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 3)
                BREAK

                CASE 5 //Grey
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 4)
                BREAK

                CASE 6 //Purple
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 5)
                BREAK

                CASE 7 //Pink
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 6)
                BREAK

                DEFAULT

                    //Not setting any pallette here.

                BREAK

            #if USE_CLF_DLC
            ENDSWITCH           
            #endif
            #if USE_NRM_DLC
            ENDSWITCH           
            #endif
            #if not USE_SP_DLC
            ENDSWITCH           
            #endif

        ENDIF

        IF g_Cellphone.PhoneOwner = CHAR_FRANKLIN //Remove this when tints are fixed...
            #if USE_CLF_DLC
            SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            #if USE_NRM_DLC
            SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            #if not USE_SP_DLC
            SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif

                CASE 1 //Light Blue Theme from Settings Menu
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 6)
                BREAK

                CASE 2 //Green
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 0)
                BREAK

                CASE 3 //Red
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 1)
                BREAK

                CASE 4 //Orange
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 2)
                BREAK

                CASE 5 //Grey
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 3)
                BREAK

                CASE 6 //Purple
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 4)
                BREAK

                CASE 7 //Pink
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 5)
                BREAK

                DEFAULT

                    //Not setting any pallette here.

                BREAK

            #if USE_CLF_DLC
            ENDSWITCH           
            #endif
            #if USE_NRM_DLC
            ENDSWITCH           
            #endif
            #if not USE_SP_DLC
            ENDSWITCH           
            #endif

        ENDIF

        IF g_Cellphone.PhoneOwner = CHAR_TREVOR
            #if USE_CLF_DLC
            SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            #if USE_NRM_DLC
            SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif
            #if not USE_SP_DLC
            SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
            #endif

                CASE 1 //Light Blue Theme from Settings Menu
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 5)
                BREAK

                CASE 2 //Green
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 6)
                BREAK

                CASE 3 //Red
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 1)
                BREAK

                CASE 4 //Orange
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 0)
                BREAK

                CASE 5 //Grey
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 2)
                BREAK

                CASE 6 //Purple
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 3)
                BREAK

                CASE 7 //Pink
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 4)
                BREAK

                DEFAULT

                    //Not setting any pallette here.

                BREAK

            
            #if USE_CLF_DLC
            ENDSWITCH           
            #endif
            #if USE_NRM_DLC
            ENDSWITCH           
            #endif
            #if not USE_SP_DLC
            ENDSWITCH           
            #endif
        ENDIF

        IF g_Cellphone.PhoneOwner = CHAR_MULTIPLAYER  //MP uses same phone as Michael. ( Mobile_1 )

            SWITCH Temp_MPcurrentTheme

                CASE 1 //Light Blue Theme from Settings Menu
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 0)
                BREAK

                CASE 2 //Green
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 1)
                BREAK

                CASE 3 //Red
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 2)
                BREAK

                CASE 4 //Orange
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 3)
                BREAK

                CASE 5 //Grey
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 4)
                BREAK

                CASE 6 //Purple
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 5)
                BREAK

                CASE 7 //Pink
                    SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 6)
                BREAK

                DEFAULT

                    //Not setting any pallette here.

                BREAK

            ENDSWITCH

        ENDIF

   ENDIF

ENDPROC

//Just a little wrapper that I can inject special case clauses into.
FUNC BOOL Is_Phone_Control_Just_Pressed (CONTROL_TYPE PassedControl_Type, CONTROL_ACTION PassedAction_Enum, BOOL bCheckDisabled = FALSE)

    IF IS_CONTROL_JUST_PRESSED(PassedControl_Type, PassedAction_Enum)
    OR (bCheckDisabled = TRUE AND IS_DISABLED_CONTROL_JUST_PRESSED(PassedControl_Type, PassedAction_Enum)) // B* 2285479 - Check disabled input so snapmatic double tap works on keyboard and mouse.


        IF IS_PC_VERSION()
            IF UPDATE_ONSCREEN_KEYBOARD() = OSK_PENDING //2175597 - do not act on phone input if virtual keyboard is active on PC version. 
            OR (NETWORK_TEXT_CHAT_IS_TYPING() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)) // and don't act on phone input if the MP text chat is on screen

                #if IS_DEBUG_BUILD
                    PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Just_Pressed interrogated but returning FALSE due to virtual keyboard being active")
                    PRINTNL()
                    
                    //Making life easier by also having this in the phone's dedicated debug channel.
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "CELLPHONE_PRIV Is_Phone_Control_Just_Pressed interrogated but returning FALSE due to virtual keyboard being active")
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "")
                #endif

                RETURN FALSE

            ELSE

                #if IS_DEBUG_BUILD
                    PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Just_Pressed !")
                    PRINTNL()

                    //Making life easier by also having this in the phone's dedicated debug channel.
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "Is_Phone_Control_Just_Pressed on PC")
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "")
                #endif

            ENDIF
        ENDIF


        IF IS_PAUSE_MENU_ACTIVE() //Special clause to prevent MP phone inputs returning true when the pause menu is onscreen, but the game is not "paused" in the traditional sense.
        OR IS_WARNING_MESSAGE_ACTIVE()

            #if IS_DEBUG_BUILD

                PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Just_Pressed interrogated but returning FALSE due to PAUSE_MENU or WARNING_MESSAGE active.")
                PRINTNL()

            #endif

            RETURN FALSE

        ELSE

            RETURN TRUE

        ENDIF

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Similar as just_pressed wrapper, but this is used to make sure controls are not being pressed at the same time as the primary takeout control.
FUNC BOOL Is_Phone_Control_Pressed (CONTROL_TYPE PassedControl_Type, CONTROL_ACTION PassedAction_Enum)

    IF IS_CONTROL_PRESSED (PassedControl_Type, PassedAction_Enum)

        
        IF IS_PC_VERSION()
            IF UPDATE_ONSCREEN_KEYBOARD() = OSK_PENDING //2175597 - do not act on phone input if virtual keyboard is active on PC version. 

                #if IS_DEBUG_BUILD
                    PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Pressed interrogated but returning FALSE due to virtual keyboard being active")
                    PRINTNL()
                #endif

                RETURN FALSE
          
            ENDIF
        ENDIF


        IF IS_PAUSE_MENU_ACTIVE() //Special clause to prevent MP phone inputs returning true when the pause menu is onscreen, but the game is not "paused" in the traditional sense.
        OR IS_WARNING_MESSAGE_ACTIVE()

            #if IS_DEBUG_BUILD

                PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Pressed interrogated but returning FALSE due to PAUSE_MENU or WARNING_MESSAGE active.")
                PRINTNL()

            #endif

            RETURN FALSE

        ELSE

            RETURN TRUE

        ENDIF

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Just a little wrapper that I can inject special case clauses into.
FUNC BOOL Is_Phone_Control_Just_Released (CONTROL_TYPE PassedControl_Type, CONTROL_ACTION PassedAction_Enum)

    IF IS_CONTROL_JUST_RELEASED(PassedControl_Type, PassedAction_Enum)


        IF IS_PC_VERSION()
            IF UPDATE_ONSCREEN_KEYBOARD() = OSK_PENDING //2175597 - do not act on phone input if virtual keyboard is active on PC version. 
            OR (NETWORK_TEXT_CHAT_IS_TYPING() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)) // and don't act on phone input if the MP text chat is on screen

                #if IS_DEBUG_BUILD
                    PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Just_Pressed interrogated but returning FALSE due to virtual keyboard being active")
                    PRINTNL()
                    
                    //Making life easier by also having this in the phone's dedicated debug channel.
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "CELLPHONE_PRIV Is_Phone_Control_Just_Pressed interrogated but returning FALSE due to virtual keyboard being active")
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "")
                #endif

                RETURN FALSE

            ELSE

                #if IS_DEBUG_BUILD
                    PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Just_Pressed !")
                    PRINTNL()

                    //Making life easier by also having this in the phone's dedicated debug channel.
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "Is_Phone_Control_Just_Pressed on PC")
                    CDEBUG1LN (DEBUG_CELL_DIALOG, "")
                #endif

            ENDIF
        ENDIF


        IF IS_PAUSE_MENU_ACTIVE() //Special clause to prevent MP phone inputs returning true when the pause menu is onscreen, but the game is not "paused" in the traditional sense.
        OR IS_WARNING_MESSAGE_ACTIVE()

            #if IS_DEBUG_BUILD

                PRINTSTRING ("CELLPHONE_PRIV : Is_Phone_Control_Just_Pressed interrogated but returning FALSE due to PAUSE_MENU or WARNING_MESSAGE active.")
                PRINTNL()

            #endif

            RETURN FALSE

        ELSE

            RETURN TRUE

        ENDIF

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Checked against "Call_Scaleform_Input_Keypress_Right()"
PROC LEGACY_SCALEFORM_MOVIE_METHOD (SCALEFORM_INDEX Legacy_iScaleformMovieId, STRING Legacy_cMethodName)

    BEGIN_SCALEFORM_MOVIE_METHOD (Legacy_iScaleformMovieId, Legacy_cMethodName) 
    END_SCALEFORM_MOVIE_METHOD ()

ENDPROC

//Checked against CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 11) for numpad entry
PROC LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SCALEFORM_INDEX Legacy_iScaleformMovieId, STRING Legacy_cMethodName,
    FLOAT f_LegacyParam1, FLOAT f_LegacyParam2 = INVALID_SCALEFORM_PARAM, FLOAT f_LegacyParam3 = INVALID_SCALEFORM_PARAM,
    FLOAT f_LegacyParam4 = INVALID_SCALEFORM_PARAM, FLOAT f_LegacyParam5 = INVALID_SCALEFORM_PARAM)

    BEGIN_SCALEFORM_MOVIE_METHOD (Legacy_iScaleformMovieId, Legacy_cMethodName)                    

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam1))

        IF f_LegacyParam2 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam2))

        ENDIF

        IF f_LegacyParam3 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam3))

        ENDIF

        IF f_LegacyParam4 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam4))

        ENDIF

        IF f_LegacyParam5 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam5))

        ENDIF

    END_SCALEFORM_MOVIE_METHOD ()

ENDPROC

//Checked against CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_221") for numpad view state header
PROC LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING(SCALEFORM_INDEX Legacy_iScaleformMovieId, STRING Legacy_cMethodName,
    STRING c_LegacyParam1, STRING c_LegacyParam2 = NULL, STRING c_LegacyParam3 = NULL, STRING c_LegacyParam4 = NULL, STRING c_LegacyParam5 = NULL)

    BEGIN_SCALEFORM_MOVIE_METHOD (Legacy_iScaleformMovieId, Legacy_cMethodName) 

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam1)

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam2))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam2)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam3))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam3)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam4))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam4)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam5))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam5)

        ENDIF

    END_SCALEFORM_MOVIE_METHOD ()

ENDPROC

PROC LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SCALEFORM_INDEX Legacy_iScaleformMovieId, STRING Legacy_cMethodName, 
    FLOAT f_LegacyParam1, FLOAT f_LegacyParam2 = INVALID_SCALEFORM_PARAM, FLOAT f_LegacyParam3 = INVALID_SCALEFORM_PARAM,
    FLOAT f_LegacyParam4 = INVALID_SCALEFORM_PARAM, FLOAT f_LegacyParam5 = INVALID_SCALEFORM_PARAM, 
    STRING c_LegacyParam1 = NULL, STRING c_LegacyParam2 = NULL, STRING c_LegacyParam3 = NULL,
    STRING c_LegacyParam4 = NULL, STRING c_LegacyParam5 = NULL)

    BEGIN_SCALEFORM_MOVIE_METHOD (Legacy_iScaleformMovieId, Legacy_cMethodName)                    

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam1))

        IF f_LegacyParam2 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam2))

        ENDIF

        IF f_LegacyParam3 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam3))

        ENDIF

        IF f_LegacyParam4 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam4))

        ENDIF

        IF f_LegacyParam5 <> INVALID_SCALEFORM_PARAM

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_LegacyParam5))

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam1))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam1)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam2))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam2)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam3))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam3)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam4))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam4)

        ENDIF

        IF NOT (IS_STRING_NULL_OR_EMPTY(c_LegacyParam5))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (c_LegacyParam5)

        ENDIF

    END_SCALEFORM_MOVIE_METHOD ()

ENDPROC

//Cloned procedure for Stephen Robertson's work on PC instructional buttons. This is referred to extensively by AppCamera.sc

PROC INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SCALEFORM_INDEX Legacy_iScaleformMovieId, STRING Legacy_cMethodName, 
    FLOAT f_Order, STRING s_Button = NULL, STRING s_Label = NULL)

//  BEGIN_SCALEFORM_MOVIE_METHOD(Legacy_iScaleformMovieId, "TOGGLE_MOUSE_BUTTONS" )
//       SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
//  END_SCALEFORM_MOVIE_METHOD()

    BEGIN_SCALEFORM_MOVIE_METHOD (Legacy_iScaleformMovieId, Legacy_cMethodName)     

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(f_Order))

        IF NOT IS_STRING_NULL_OR_EMPTY(s_Button)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(s_Button)    
            CPRINTLN(DEBUG_AMBIENT, "Camera scaleform Button = ", s_Button)
        ENDIF

        IF NOT IS_STRING_NULL_OR_EMPTY(s_Label)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(s_Label)
        ENDIF

    END_SCALEFORM_MOVIE_METHOD ()

ENDPROC

//Added to hook into tunable for 6474707
FUNC BOOL SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE()

	//Interrogates tunable status Kevin added in 6474707.
	//Also check 6508581 for the .json status of this tunable in 6508581

	#IF FEATURE_COPS_N_CROOKS
	IF g_sMPTunables.bEnableArcadePhoneQuickLaunch = TRUE
		//CDEBUG1LN (DEBUG_CELL_DIALOG, "SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE() returning true.")
		RETURN TRUE	
	ENDIF
	#ENDIF
	
	//CDEBUG1LN (DEBUG_CELL_DIALOG, "SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE() returning false.")
	RETURN FALSE

ENDFUNC

//6417795 - April 2020
//Used to add the 3x3 grid soft key to all homescreen modes mapped to PS4 Square and other platform equivalents.
PROC ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()


	IF g_bInMultiplayer
	
		//Tunable support added for 6474707 - shouldn't add this soft key if tunable to switch off arcade mode quick launch is set.
		//We don't need the second virtual screen, so remove any indication of how to swap to it.
		IF SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE() = FALSE
			#if IS_DEBUG_BUILD
				CDEBUG1LN (DEBUG_CELL_DIALOG, "ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY called but secondary mode is unavailable.")
			#ENDIF
			
			EXIT
		ENDIF
	
	
	
		//4th pararmeter set to 1 displays the key on our scaleform powered cellphone.
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
		            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ENTER NO." - Other
					
		SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
		
		#if IS_DEBUG_BUILD
			CDEBUG1LN (DEBUG_CELL_DIALOG, "ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY called.")
		#ENDIF
		
	ENDIF

ENDPROC 

 
//6417795 - April 2020
//Used to remove the 3x3 grid soft key when the phone is in modes other than displaying any homescreen app arrangement.
//E.g we don't want to display this new key during any phonecall.
PROC REMOVE_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()

	//Example scenario where we need to remove this in MP:
	//Set_SF_Buttons_to_OngoingCall()
	
	IF g_bInMultiplayer
	
		//4th pararmeter set to 0 removes the key from the scaleform cellphone display in-game.
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
		            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ENTER NO." - Other
					
		CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
		
		#if IS_DEBUG_BUILD
			CDEBUG1LN (DEBUG_CELL_DIALOG, "REMOVE_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY called.")
		#ENDIF
		
	ENDIF

ENDPROC  
 
 
 
 
PROC Update_Page_Indicator_SoftKey()

        IF g_b_Secondary_Screen_Available = FALSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

        ELSE

            IF g_bInMultiplayer //no secondary screen for multiplayer...

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ELSE

                IF g_Homescreen_In_Secondary_Mode = TRUE

                    IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages 2
                    ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages 2
                    ENDIF

                ELSE

                    IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages
                    ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages
                    ENDIF

                ENDIF

                SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ENDIF

        ENDIF

ENDPROC

#if IS_DEBUG_BUILD
    PROC cdPrintstring (STRING passedSTRING)
        IF g_b_CellDialDebugTextToggle = TRUE
            PRINTSTRING (passedSTRING)
        ELSE
            CDEBUG1LN (DEBUG_CELL_DIALOG, passedSTRING)
        ENDIF
    ENDPROC

    PROC cdPrintint (INT passedINT)
        IF g_b_CellDialDebugTextToggle = TRUE
            PRINTINT(passedINT)
        ELSE
            CDEBUG1LN (DEBUG_CELL_DIALOG, passedINT)
        ENDIF
    ENDPROC

    PROC cdPrintnl ()
        IF g_b_CellDialDebugTextToggle = TRUE
            PRINTNL()
        ELSE
            CDEBUG1LN (DEBUG_CELL_DIALOG, "")
        ENDIF
    ENDPROC

#endif

//Since we will have multiple "player" characters, we need to ascertain who the current cellphone belongs to
PROC Get_Cellphone_Owner()

    IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //the player enum isn't returned correctly in Director mode, so we go for an explicit model check.


        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

            IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_MICHAEL].game_model
                g_Cellphone.PhoneOwner = CHAR_MICHAEL
            ELSE

                IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_FRANKLIN].game_model
                    g_Cellphone.PhoneOwner = CHAR_FRANKLIN
                ELSE

                    IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_TREVOR].game_model
                        g_Cellphone.PhoneOwner = CHAR_TREVOR
                    ELSE

                        g_Cellphone.PhoneOwner = CHAR_MICHAEL //Default to Michael's phone as a failsafe.   
                    
                        /*
                        #if IS_DEBUG_BUILD
                            cdPrintstring("CELLPHONE_PRIVATE - GET_CURRENT_PLAYER_PED_ENUM() is returning an abnormal player model in Director Mode. For safety, setting owner to CHAR_MICHAEL.")
                            cdPrintnl()
                        #endif
                        */

                    ENDIF

                ENDIF

            ENDIF
        
        ENDIF



    ELSE


        g_Cellphone.PhoneOwner = GET_CURRENT_PLAYER_PED_ENUM()  //Use Kenneth's function to determine whether Michael, Franklin or Trevor are under control.

        //Multiplayer hack...
        IF g_Cellphone.PhoneOwner = NO_CHARACTER //this would be returned by multiplayer game going on, from Kenneth's function above

            //g_Cellphone.PhoneOwner = CHAR_MICHAEL //use this as a stopgap until we suss out exactly what will happen with this stuff. Prevents array overrun in settings.

            //Virtual Character
            g_Cellphone.PhoneOwner = CHAR_MULTIPLAYER

        ENDIF

        IF g_bInMultiplayer

            g_Cellphone.PhoneOwner = CHAR_MULTIPLAYER

        ENDIF

        IF g_Cellphone.PhoneOwner > CHAR_MULTIPLAYER

            g_Cellphone.PhoneOwner = CHAR_MULTIPLAYER //In the mission creator, the player ped is in a funny state so was returning 153! The game cannot set g_bInMultiplayer here either.
                                                      //Force the return value to be the multiplayer phone at this time as a precaution #1357711

            #if IS_DEBUG_BUILD

                cdPrintstring("CELLPHONE_PRIVATE - GET_CURRENT_PLAYER_PED_ENUM() is returning an abnormal number, for safety, setting owner to CHAR_MULTIPLAYER.")
                cdPrintnl()

            #endif

        ENDIF


    ENDIF


ENDPROC





FUNC INT Get_Cellphone_Provider()

    Get_Cellphone_Owner()
    #if USE_CLF_DLC
        RETURN g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ProviderForThisPlayer
    #endif
    #if USE_NRM_DLC
        RETURN g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ProviderForThisPlayer
    #endif
    #if not USE_SP_DLC
        RETURN g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ProviderForThisPlayer
    #endif

ENDFUNC

PROC Perform_cp_StockMarketUpdate(enumCharacterList WhichCharacter)

    INT i_cp_Provider_Ident
    BSMF_TYPES bsaq_cp_TextsDecision
    #if USE_CLF_DLC
        i_cp_Provider_Ident = g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[WhichCharacter].ProviderForThisPlayer
    #endif
    #if USE_NRM_DLC
        i_cp_Provider_Ident = g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[WhichCharacter].ProviderForThisPlayer
    #endif
    #if not USE_SP_DLC
        i_cp_Provider_Ident = g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[WhichCharacter].ProviderForThisPlayer
    #endif
/*
    SWITCH i_cp_Provider_Ident

        CASE PROVIDER_BADGER

            bsaq_cp_TextsDecision = BSMF_TEXTSUSED_FOR_BDG

        BREAK

        CASE PROVIDER_WHIZ

            bsaq_cp_TextsDecision = BSMF_TEXTSUSED_FOR_WIZ

        BREAK

        CASE PROVIDER_TINKLE

            bsaq_cp_TextsDecision = BSMF_TEXTSUSED_FOR_TNK

        BREAK

        DEFAULT //Default to badger.

            bsaq_cp_TextsDecision = BSMF_TEXTSUSED_FOR_BDG

        BREAK

    ENDSWITCH

    ////////BAWSAQ_INCREMENT_MODIFIER (bsaq_cp_TextsDecision, 1)
*/
    i_cp_Provider_Ident = i_cp_Provider_Ident //before I re-enable this
    bsaq_cp_TextsDecision = bsaq_cp_TextsDecision
    /*
    #if IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_FH - Performing texts used stock update.")
        cdPrintnl()

    #endif
    */

ENDPROC

PROC Scaleform_Setup_and_Fill_Numpad() //Don't think this needs to be globalised at this point. May split up in the future.

    BOOL Has_this_NumpadSlot_Been_Assigned[MAX_NUMPAD_ENTRIES]

    //Assign slots...
    NumpadList[NUM_0].NumpadLabel = "CELL_900"
    NumpadList[NUM_0].ThisNumpadPosition = 10

    NumpadList[NUM_1].NumpadLabel = "CELL_901"
    NumpadList[NUM_1].ThisNumpadPosition = 0

    NumpadList[NUM_2].NumpadLabel = "CELL_902"
    NumpadList[NUM_2].ThisNumpadPosition = 1

    NumpadList[NUM_3].NumpadLabel = "CELL_903"
    NumpadList[NUM_3].ThisNumpadPosition = 2

    NumpadList[NUM_4].NumpadLabel = "CELL_904"
    NumpadList[NUM_4].ThisNumpadPosition = 3

    NumpadList[NUM_5].NumpadLabel = "CELL_905"
    NumpadList[NUM_5].ThisNumpadPosition = 4

    NumpadList[NUM_6].NumpadLabel = "CELL_906"
    NumpadList[NUM_6].ThisNumpadPosition = 5

    NumpadList[NUM_7].NumpadLabel = "CELL_907"
    NumpadList[NUM_7].ThisNumpadPosition = 6

    NumpadList[NUM_8].NumpadLabel = "CELL_908"
    NumpadList[NUM_8].ThisNumpadPosition = 7

    NumpadList[NUM_9].NumpadLabel = "CELL_909"
    NumpadList[NUM_9].ThisNumpadPosition = 8

    NumpadList[NUM_10_ASTERISK].NumpadLabel = "CELL_910"
    NumpadList[NUM_10_ASTERISK].ThisNumpadPosition = 9

    NumpadList[NUM_11_HASH].NumpadLabel = "CELL_911"
    NumpadList[NUM_11_HASH].ThisNumpadPosition = 11

    //Clear slot bools...

    INT slotIndex = 0

    WHILE slotIndex < ENUM_TO_INT(MAX_NUMPAD_ENTRIES)

        Has_this_NumpadSlot_Been_Assigned[slotindex] = FALSE

        slotindex ++

    ENDWHILE

    //Temp..
    IF NumpadSlot[0] = 0
    ENDIF

    //reset slotindex                                           
    slotIndex = 0

    WHILE slotIndex <  ENUM_TO_INT(MAX_NUMPAD_ENTRIES)

        INT matchSlotIndex = 0

        WHILE matchSlotIndex < (ENUM_TO_INT(MAX_NUMPAD_ENTRIES))

            IF slotindex = NumpadList[matchSlotIndex].ThisNumpadPosition

                IF Has_this_NumpadSlot_Been_Assigned[slotindex] = FALSE

                    NumpadSlot[SlotIndex] = MatchSlotIndex

                    //CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(11)), (TO_FLOAT(SlotIndex)), 
                        //INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, NumpadList[matchSlotIndex].NumpadLabel) 

                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")                    

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(11)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Slotindex)

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(NumpadList[matchSlotIndex].NumpadLabel)

                    END_SCALEFORM_MOVIE_METHOD ()

                    #IF IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PRIV - Numpad slot placement: ")
                        cdPrintint (slotIndex)
                        cdPrintnl()
                    #endif

                    Has_this_NumpadSlot_Been_Assigned[slotindex] = TRUE

                ENDIF

            ENDIF

            matchSlotIndex ++

        ENDWHILE

        slotIndex ++

    ENDWHILE

ENDPROC



#IF FEATURE_FREEMODE_ARCADE
#IF FEATURE_COPS_N_CROOKS
//Added to aid removal of Joblist, Arcade Mode and Internet App links whilst in CnC. See bugs 6417788 6457063 6461213
FUNC BOOL IS_CELLPHONE_IN_CNC()

		IF g_bInMultiplayer
					
			//Checked with Bobby, Steve Tiley that this is the best way to check if the player is definitely in CnC.
			IF IS_FREEMODE_ARCADE()
				IF GET_ARCADE_MODE() = ARC_COPS_CROOKS	
					CDEBUG1LN (DEBUG_CELL_DIALOG, "May 2020 - IS_CELLPHONE_IN_CNC Cellphone IS in CnC and in FreeMode Arcade Mode. See #6494642")
					RETURN TRUE
				ELSE
					CDEBUG1LN (DEBUG_CELL_DIALOG, "May 2020 - IS_CELLPHONE_IN_CNC Cellphone is in FreeMode Arcade Mode but not in CnC. See #6494642")
					RETURN FALSE
				ENDIF
			ELSE
				CDEBUG1LN (DEBUG_CELL_DIALOG, "May 2020 - IS_CELLPHONE_IN_CNC Related feature active but Cellphone NOT in FreeMode Arcade Mode.See #6494642")
				RETURN FALSE
			ENDIF
		
		ENDIF
		
	
	
	CDEBUG1LN (DEBUG_CELL_DIALOG, "May 2020 - IS_CELLPHONE_IN_CNC Cellphone NOT in Multiplayer or feature not present.")
	RETURN FALSE
	
ENDFUNC
#ENDIF
#ENDIF


PROC Scaleform_Place_Items_on_Homescreen(INT CheckPrecedence)

	CDEBUG1LN (DEBUG_CELL_DIALOG, "CELLPHONE_PRIV JULY 2020 - Scaleform_Place_Items_on_Homescreen commencing...")
    CDEBUG1LN (DEBUG_CELL_DIALOG, "")

    g_CurrentNumberOfUnreadTexts = 0

    g_LastUsed_Homescreen_Page_Precedence = CheckPrecedence //Keep a store of the last homescreen precedence stipulated so that we can return to it from an application exit.                       

    //Clear slot bools...

    INT slotIndex = 0

    WHILE slotIndex < 9

        Has_this_AppSlot_Been_Assigned[slotindex] = FALSE

        slotindex ++

    ENDWHILE

    #IF IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("CELLPHONE_PRIV - Placing apps on homescreen.")
        cdPrintnl()

    #endif

    //reset slotindex                                           
    slotIndex = 0

    WHILE slotIndex < 9 // 3 x 3 grid on homescreen //(ENUM_TO_INT(MAX_APPLICATIONS))  //Need to fix this to display secondary apps instead!

        INT matchSlotIndex = 0


            IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //If player is in director mode, limit phone apps to Snapmatic use only. 2171957
                
                WHILE matchSlotIndex < (ENUM_TO_INT(MAX_APPLICATIONS))

                    IF CheckPrecedence = g_AppList[matchSlotIndex].Homescreen_Page_Precedence      
                    IF slotindex = g_AppList[matchSlotIndex].ThisAppPosition

                        IF Has_this_AppSlot_Been_Assigned[slotindex] = FALSE

                            AppSlot[SlotIndex] = MatchSlotIndex


                            IF (MatchSlotIndex = ENUM_TO_INT(AppCAMERA))

                                INT i_TempAlpha

                                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("CELLPHONE_PRIVATE: Placing camera icon in director mode, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME was set.")
                                        cdPrintnl()   
                                    #endif

                                    i_TempAlpha = i_HomescreenIcon_GreyedOut

                                    g_b_CameraGreyedOut = TRUE

                                ELSE

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("CELLPHONE_PRIVATE: Placing camera icon in director mode, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME is clear.")
                                        cdPrintnl()   
                                    #endif

                                    i_TempAlpha = i_HomescreenIcon_FullAlpha  

                                    g_b_CameraGreyedOut = FALSE

                                ENDIF

                                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_TempAlpha)

                                END_SCALEFORM_MOVIE_METHOD()

                            ENDIF 



                            IF g_CellphoneHasMPAccess //2411448

                                IF (MatchSlotIndex = ENUM_TO_INT(AppMPJOBLIST)) //Add available number of jobs in here when available.

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                            (TO_FLOAT(g_numNewJoblistInvites)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 

                                ENDIF

                            ENDIF



                            Has_this_AppSlot_Been_Assigned[slotindex] = TRUE

                        ENDIF

                    ENDIF
                    ENDIF

                    matchSlotIndex ++

                ENDWHILE


            ELSE



                WHILE matchSlotIndex < (ENUM_TO_INT(MAX_APPLICATIONS))

                    IF CheckPrecedence = g_AppList[matchSlotIndex].Homescreen_Page_Precedence      
                    IF slotindex = g_AppList[matchSlotIndex].ThisAppPosition

                        IF Has_this_AppSlot_Been_Assigned[slotindex] = FALSE

                            AppSlot[SlotIndex] = MatchSlotIndex

                            IF (MatchSlotIndex = ENUM_TO_INT(AppTexts))
                            
                                INT TempTxtMsgArrayIndex = 0
                                WHILE TempTxtMsgArrayIndex < MAX_TEXT_MESSAGES
                                
                                    #if USE_CLF_DLC
                                        IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].TxtMsgLockStatus <> TXTMSG_EMPTY
                                            IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                                               IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE
                                                    g_CurrentNumberOfUnreadTexts ++                                     
                                                ENDIF
                                            ENDIF
                                        ENDIF
                                    #endif
                                    #if USE_NRM_DLC
                                        IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].TxtMsgLockStatus <> TXTMSG_EMPTY
                                            IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                                               IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE
                                                    g_CurrentNumberOfUnreadTexts ++                                     
                                                ENDIF
                                            ENDIF
                                        ENDIF
                                    #endif
                                    #if not USE_SP_DLC
                                        IF g_savedGlobals.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].TxtMsgLockStatus <> TXTMSG_EMPTY
                                            IF g_savedGlobals.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                                               IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempTxtMsgArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE
                                                    g_CurrentNumberOfUnreadTexts ++                                    
                                                ENDIF
                                            ENDIF
                                        ENDIF                           
                                    #endif  

                                    TempTxtMsgArrayIndex ++

                                ENDWHILE

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring ("Number of unread texts is set to :")
                                    cdPrintint (g_CurrentNumberOfUnreadTexts)
                                    cdPrintnl()
                                #endif

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                    (TO_FLOAT(g_CurrentNumberOfUnreadTexts)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 

                            ELSE




                                IF (MatchSlotIndex = ENUM_TO_INT(AppEmail))

                                    
        #IF USE_TU_CHANGES
                                    IF g_bInMultiplayer



                                        //New marketing email work.
                                        //Get marketing number here.
                                        INT i_NumberOfUnreadMPEmails = 0

                                        i_NumberOfUnreadMPEmails = g_NumberOfUnreadMarketingEmailsThisSession //Latter value determined once per session from cellphone_controller.

                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring ("Number of unread MP emails for marketing is set to :")
                                            cdPrintint (i_NumberOfUnreadMPEmails)
                                            cdPrintnl()
                                        #endif
	
										
                                       	//LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                        //(TO_FLOAT(i_NumberOfUnreadMPEmails)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 


                                        ///* Pre Marketing Email work.
                                        INT TempEmailArrayIndex = 0
                                        //INT i_NumberOfUnreadMPEmails = 0

                                        WHILE TempEmailArrayIndex < MAX_MP_EMAILS

                                            IF g_EmailMessage[TempEmailArrayIndex].EmailLockStatus <> EMAIL_EMPTY
                                                IF g_EmailMessage[TempEmailArrayIndex].EmailReadStatus = UNREAD_MPEMAIL
                                                   IF g_EmailMessage[TempEmailArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE

                                                        i_NumberOfUnreadMPEmails ++

                                                    ENDIF
                                                ENDIF
                                            ENDIF

                                            TempEmailArrayIndex ++

                                        ENDWHILE

                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring ("Number of unread MP emails is set to :")
                                            cdPrintint (i_NumberOfUnreadMPEmails)
                                            cdPrintnl()
                                        #endif

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                        (TO_FLOAT(i_NumberOfUnreadMPEmails)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName)
                                        //*/ 

                                    ELSE // not in MP, use existing legacy routine for SP emails.
        #ENDIF  //  USE_TU_CHANGES
                                        INT i_UnreadEmailsForThisOwner

                                        /* Comment in to test. This are maintained by Andrew's email system.
                                            g_iUnreadEmailsSP0 = 5
                                            g_iUnreadEmailsSP1 = 7
                                            g_iUnreadEmailsSP2 = 9                       
                                        */

                                        SWITCH g_Cellphone.PhoneOwner           

                                            CASE CHAR_MICHAEL  

                                                i_UnreadEmailsForThisOwner = g_iUnreadEmailsSP0    

                                            BREAK

                                            CASE CHAR_FRANKLIN

                                                i_UnreadEmailsForThisOwner = g_iUnreadEmailsSP1

                                            BREAK

                                            CASE CHAR_TREVOR

                                                i_UnreadEmailsForThisOwner = g_iUnreadEmailsSP2

                                            BREAK

                                            DEFAULT

                                                #if IS_DEBUG_BUILD

                                                    cdPrintnl()
                                                    cdPrintstring ("CELLPHONE_PRIV - Abnormal owner decision, unread emails set to 0.")
                                                    cdPrintnl()

                                                #endif

                                            BREAK

                                        ENDSWITCH

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                            (TO_FLOAT(i_UnreadEmailsForThisOwner)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 

        #IF USE_TU_CHANGES
                                    ENDIF
        #ENDIF  //  USE_TU_CHANGES

                                ELSE

                                    IF (MatchSlotIndex = ENUM_TO_INT(AppMPJOBLIST)) //Add available number of jobs in here when available.

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                            (TO_FLOAT(g_numNewJoblistInvites)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 

                                    ELSE

                                        IF (MatchSlotIndex = ENUM_TO_INT(AppQUICKSAVE))

                                            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_HomescreenIcon_AlphaValue)

                                            END_SCALEFORM_MOVIE_METHOD()

                                        ELSE

                                            IF (MatchSlotIndex = ENUM_TO_INT(AppINTERNET))

                                                INT i_TempAlpha

                                                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME)

                                                    i_TempAlpha = i_HomescreenIcon_GreyedOut  

                                                ELSE

                                                    i_TempAlpha = i_HomescreenIcon_FullAlpha  

                                                ENDIF

                                                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_TempAlpha)

                                                END_SCALEFORM_MOVIE_METHOD()

                                            ELSE

                                                IF (MatchSlotIndex = ENUM_TO_INT(AppCAMERA))

                                                    INT i_TempAlpha

                                                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)

                                                        #if IS_DEBUG_BUILD
                                                            cdPrintnl()
                                                            cdPrintstring("CELLPHONE_PRIVATE: Placing camera icon, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME was set.")
                                                            cdPrintnl()   
                                                        #endif

                                                        i_TempAlpha = i_HomescreenIcon_GreyedOut

                                                        g_b_CameraGreyedOut = TRUE

                                                    ELSE

                                                        #if IS_DEBUG_BUILD
                                                            cdPrintnl()
                                                            cdPrintstring("CELLPHONE_PRIVATE: Placing camera icon, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME is clear.")
                                                            cdPrintnl()   
                                                        #endif

                                                        i_TempAlpha = i_HomescreenIcon_FullAlpha  

                                                        g_b_CameraGreyedOut = FALSE

                                                    ENDIF

                                                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_TempAlpha)

                                                    END_SCALEFORM_MOVIE_METHOD()

                                                ELSE 

                                                    IF (MatchSlotIndex = ENUM_TO_INT(AppOrganiser)) //Now spare, so grey it out

                                                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_HomescreenIcon_GreyedOut)

                                                        END_SCALEFORM_MOVIE_METHOD()

                                                    ELSE   //Just do normal fill routine...

                                                        //If the internet app is disabled and Bennys MWorks is using the dummy app position, make sure we grey it out. 2534800
                                                        IF MatchSlotIndex = ENUM_TO_INT (AppDummyApp0)
                                                        AND (ARE_STRINGS_EQUAL (g_AppList[MatchSlotIndex].AppName, "CELL_BENWEB"))
                                                        AND (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME))

                                                            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_HomescreenIcon_GreyedOut)

                                                            END_SCALEFORM_MOVIE_METHOD()

                                                        ELSE


                                                            //2583404 - Need to display Boss Agency invite overlay.
                                                            IF g_AppList[matchSlotIndex].SF_IconEnum = 57 //Boss Agency App is positioned in the DummyApp position, we know this because the Boss Agency icon int is unique.
                                                            AND MatchSlotIndex = ENUM_TO_INT (AppDummyApp0) 
                                                               
                                                                INT i_BossAgencyInvitesOverlay = 0
                                                                

                                                                    #if IS_DEBUG_BUILD
                                                                        cdPrintstring("CELLPHONE_PRIV - Grabbing Gang Boss Invite total which was: ")
                                                                        cdPrintint (g_sBossAgencyApp.iTotalUnreadBossAgencyInvites)
                                                                        cdPrintnl()
                                                                    #endif

                                                                    i_BossAgencyInvitesOverlay = g_sBossAgencyApp.iTotalUnreadBossAgencyInvites  //GB_GET_TOTAL_UNREAD_INVITES() causes compile problems due to inclusion cycles.

                                                                                                                            
                                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                                                    (TO_FLOAT(i_BossAgencyInvitesOverlay)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 
                                                            
                                                            ELSE

                                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(1)), (TO_FLOAT(SlotIndex)), (TO_FLOAT(g_AppList[matchSlotIndex].SF_IconEnum)), 
                                                                (TO_FLOAT(0)), INVALID_SCALEFORM_PARAM, g_AppList[matchSlotIndex].AppName) 

                                                            ENDIF

                                                        ENDIF

                                                    ENDIF

                                                ENDIF

                                            ENDIF

                                        ENDIF

                                    ENDIF

                                ENDIF

                            ENDIF

                            //Special case update for checking sleep mode...

                            
                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_PRIV - Homescreen slot placement: ")
                                cdPrintint (SlotIndex)
                                cdPrintnl()

                                PRINTSTRING("CELLPHONE_PRIV - Homescreen slot placement: ")
                                PRINTINT(slotIndex)
                                PRINTNL()


                            #endif
                            
							
								#IF FEATURE_FREEMODE_ARCADE
								#IF FEATURE_COPS_N_CROOKS
								//May 2020 Grey out of Joblist, Arcade Mode and Internet App links whilst in CnC. See bugs 6417788 6457063 6461213
					            IF IS_CELLPHONE_IN_CNC()
									IF (MatchSlotIndex = ENUM_TO_INT(AppINTERNET))
									OR (MatchSlotIndex = ENUM_TO_INT(AppMPJOBLIST))
									OR (MatchSlotIndex = ENUM_TO_INT(AppDummyApp1))

                                            INT i_TempAlpha = i_HomescreenIcon_GreyedOut


                                            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SlotIndex)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (g_AppList[matchSlotIndex].SF_IconEnum)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[matchSlotIndex].AppName)
                                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_TempAlpha)

                                            END_SCALEFORM_MOVIE_METHOD()

                                     ENDIF
								 ENDIF
								#ENDIF
								#ENDIF
							
							

                            Has_this_AppSlot_Been_Assigned[slotindex] = TRUE

                       
                        ENDIF

                    ENDIF
                    ENDIF

                    matchSlotIndex ++

                ENDWHILE


        ENDIF  //End of director mode clause.

        slotIndex ++

    ENDWHILE

ENDPROC

//Used by temp draw procedure for cellphone - will be moved.
PROC format_medium_ostext(INT r, INT g, INT b, INT a)

    SET_TEXT_SCALE (0.3, 0.3)//(0.22, 0.24)
    //SET_TEXT_WRAP (0.0, 0.9)
    SET_TEXT_DROPSHADOW (0, 0, 0, 0, 0)
    //SET_TEXT_BACKGROUND (TRUE)
    SET_TEXT_EDGE (1, 0, 0, 0, 205)     
    SET_TEXT_PROPORTIONAL (TRUE)
    SET_TEXT_FONT (FONT_STANDARD)                                                      
    SET_TEXT_COLOUR (r, g, b, a)

ENDPROC

PROC format_cellcam_ostext(INT r, INT g, INT b, INT a)

    SET_TEXT_SCALE (0.4, 0.4)//(0.22, 0.24)
    //SET_TEXT_WRAP (0.0, 0.9)
    SET_TEXT_DROPSHADOW (0, 0, 0, 0, 0)
    //SET_TEXT_BACKGROUND (TRUE)
    SET_TEXT_EDGE (1, 0, 0, 0, 205)     
    SET_TEXT_PROPORTIONAL (TRUE)
    SET_TEXT_FONT (FONT_STANDARD)                                                      
    SET_TEXT_COLOUR (r, g, b, a)

ENDPROC

PROC format_cellcam_fs_ostext(INT r, INT g, INT b, INT a)

    SET_TEXT_SCALE (0.7, 0.7)//(0.22, 0.24)
    //SET_TEXT_WRAP (0.0, 0.9)
    SET_TEXT_DROPSHADOW (0, 0, 0, 0, 0)
    //SET_TEXT_BACKGROUND (TRUE)
    SET_TEXT_EDGE (1, 0, 0, 0, 205)     
    SET_TEXT_PROPORTIONAL (TRUE)
    SET_TEXT_FONT (FONT_STANDARD)                                                      
    SET_TEXT_COLOUR (r, g, b, a)

ENDPROC




FUNC BOOL IsCurrentCamFirstPerson()

    CAM_VIEW_MODE_CONTEXT activeViewModeContext
    CAM_VIEW_MODE activeViewMode

    activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
    //IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT //Example exclusion

    activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)

    IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC







//First Person Cellphone experimental mode.
FUNC BOOL ShouldCellphoneUseFirstPersonAnimations()

    IF g_bInMultiplayer //Cellphone does not appear in hand in MP so this becomes redundant.

        RETURN FALSE

    ELSE 

        CAM_VIEW_MODE_CONTEXT activeViewModeContext
        CAM_VIEW_MODE activeViewMode

        BOOL b_InFirstPerson = FALSE

        activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
        //IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT //Example exclusion

            activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)

            IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON

                b_InFirstPerson = TRUE

                /*
                #if IS_DEBUG_BUILD 

                    cdPrintnl()
                    cdPrintstring("FP view mode detected!") 
                    cdPrintnl()

                #endif
                */

            ELSE

                /*
                #if IS_DEBUG_BUILD 

                    cdPrintnl()
                    cdPrintstring("Not in first person view.") 
                    cdPrintnl()

                #endif
                */

            ENDIF

        //ENDIF

        IF g_b_ShouldCellphoneUseFirstPersonAnims  //from cellphone_globals_TU.sch. Can be controlled by widget in root of script - Cellphone and Dialog Debug.
        //OR GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON //Colin suggests this command should not be used.
        OR b_InFirstPerson

            RETURN TRUE

        ELSE

            //RETURN FALSE
            //Update: Requested by #1987460 that anims play in all gameplay camera modes.
            RETURN TRUE

        ENDIF

    ENDIF

ENDFUNC

PROC CellphoneFirstPersonHorizontalModeToggle(BOOL WhichToggle)

    IF ShouldCellphoneUseFirstPersonAnimations()

        IF WhichToggle = TRUE

            CELL_HORIZONTAL_MODE_TOGGLE (TRUE)

            #if IS_DEBUG_BUILD 

                cdPrintnl()
                cdPrintstring("FP_Horizontal Mode toggled ON")
                cdPrintnl()

            #endif

        ELSE

            IF g_Cellphone.PhoneDS > PDS_AWAY
                
                CELL_HORIZONTAL_MODE_TOGGLE (FALSE)

            ELSE

                #if IS_DEBUG_BUILD 

                    cdPrintnl()
                    cdPrintstring("FP_Horizontal Mode toggled OFF aborted as phone is hanging up. See 2015070")
                    cdPrintnl()

                #endif

            ENDIF

            #if IS_DEBUG_BUILD 

                cdPrintnl()
                cdPrintstring("FP_Horizontal Mode toggled OFF if PDS > PDS_AWAY. See 2015070")
                cdPrintnl()

            #endif

        ENDIF

    ENDIF

ENDPROC

/*
ENUM CELL_INPUT
    CELL_INPUT_NONE,
    CELL_INPUT_UP,
    CELL_INPUT_DOWN,
    CELL_INPUT_LEFT,
    CELL_INPUT_RIGHT,
    CELL_INPUT_SELECT
ENDENUM
*/

PROC CellphoneFirstPersonAnimNavUp()

    IF ShouldCellphoneUseFirstPersonAnimations()

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("FP_NavUp anim should be cued.")
            cdPrintnl()

        #endif

        IF Movement_Exception = FALSE

            CELL_SET_INPUT(CELL_INPUT_UP)

        ELSE

            CELL_SET_INPUT(CELL_INPUT_DOWN)

        ENDIF

    ENDIF

ENDPROC



PROC CellphoneFirstPersonAnimNavDown()

    IF ShouldCellphoneUseFirstPersonAnimations()

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("FP_NavDown anim should be cued.")
            cdPrintnl()

        #endif

        IF Movement_Exception = FALSE
            
            CELL_SET_INPUT(CELL_INPUT_DOWN)

        ELSE

            CELL_SET_INPUT(CELL_INPUT_UP)

        ENDIF


    ENDIF

ENDPROC



PROC CellphoneFirstPersonAnimNavLeft()

    IF ShouldCellphoneUseFirstPersonAnimations()

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("FP_NavLeft anim should be cued.")
            cdPrintnl()

        #endif

        IF Movement_Exception = FALSE

            CELL_SET_INPUT(CELL_INPUT_LEFT)

        ELSE

            CELL_SET_INPUT(CELL_INPUT_RIGHT)

        ENDIF



    ENDIF

ENDPROC




PROC CellphoneFirstPersonAnimNavRight()

    IF ShouldCellphoneUseFirstPersonAnimations()

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("FP_NavRight anim should be cued.")
            cdPrintnl()

        #endif

        IF Movement_Exception = FALSE

            CELL_SET_INPUT(CELL_INPUT_RIGHT)    

        ELSE

            CELL_SET_INPUT(CELL_INPUT_LEFT)

        ENDIF

    ENDIF

ENDPROC



PROC CellphoneFirstPersonAnimNavSelect()

    IF ShouldCellphoneUseFirstPersonAnimations()

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("FP_NavSelect anim should be cued.")
            cdPrintnl()

        #endif

        CELL_SET_INPUT(CELL_INPUT_SELECT)

    ENDIF

ENDPROC

//Sound notes... if these stop working, consider playing the sound from the player ped entity.

PROC Call_Scaleform_Input_Keypress_Right()

     LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_INPUT_EVENT", (TO_FLOAT(2)))

     #if IS_DEBUG_BUILD
        TestingNavigationInt ++
        //cdPrintstring ("CELLPHONE_PRIV - Scaleform cursor right has been pressed. Navigation int total at ")
        //cdPrintnl()
        //cdPrintint(TestingNavigationInt)
        //cdPrintnl()
     #endif

     //Sound notes... if these stop working, consider playing the sound from the player ped entity.
     //PLAY_SOUND_FRONTEND (-1, "PHONE_GENERIC_KEY_02")
    PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

    CellphoneFirstPersonAnimNavRight()

ENDPROC

PROC Call_Scaleform_Input_Keypress_Left()

     LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_INPUT_EVENT", (TO_FLOAT(4)))

    /*
     #IF IS_DEBUG_BUILD
        cdPrintstring ("CELLPHONE_PRIV - Scaleform cursor left has been pressed.")
        cdPrintnl()
     #endif
     */

    //Sound notes... if these stop working, consider playing the sound from the player ped entity.
    //PLAY_SOUND_FRONTEND (-1, "PHONE_GENERIC_KEY_02")
    PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

    CellphoneFirstPersonAnimNavLeft()

ENDPROC

PROC Call_Scaleform_Input_Keypress_Up()

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_INPUT_EVENT", (TO_FLOAT(1)))

    //Sound notes... if these stop working, consider playing the sound from the player ped entity.
    //PLAY_SOUND_FRONTEND (-1, "PHONE_GENERIC_KEY_03")

    PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

    CellphoneFirstPersonAnimNavUp()

ENDPROC

PROC Call_Scaleform_Input_Keypress_Down()

     LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_INPUT_EVENT", (TO_FLOAT(3)))

    //Sound notes... if these stop working, consider playing the sound from the player ped entity.
    //PLAY_SOUND_FRONTEND (-1, "PHONE_GENERIC_KEY_03")

    PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

    CellphoneFirstPersonAnimNavDown()

ENDPROC

PROC Play_Select_Beep()

    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

        PLAY_SOUND_FRONTEND (-1,  "Menu_Accept", g_Owner_Soundset)

        //PLAY_SOUND_FROM_ENTITY (-1, "PHONE_GENERIC_KEY_01", PLAYER_PED_ID())

        CellphoneFirstPersonAnimNavSelect()

    ENDIF

ENDPROC

PROC Play_Back_Beep()

    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

        PLAY_SOUND_FRONTEND (-1,  "Menu_Back", g_Owner_Soundset)

        //PLAY_SOUND_FROM_ENTITY (-1, "PHONE_GENERIC_KEY_01", PLAYER_PED_ID())

    ENDIF

ENDPROC

PROC Set_SF_Buttons_to_OngoingCall()

    //Temp - until we get button transparency
    //g_SF_ButtonsPosY = 0.325

    IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)
        //IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)
		
			//6417795 - April 2020
			//Make sure that the new homescreen mode switch soft key is removed during a phonecall.
			REMOVE_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()

            IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED

                /*
                CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //0 - transparent, 1 - opaque

                CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_203") //"ANSWER" Positive
                */

                //Badger
                IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_203") //"ANSWER" - Positive
                ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ANSWER" - Positive
                ENDIF

                IF g_ForcePlayerAnswer //Don't display reject button if call is a FORCE ANSWER. See bug 122481 

                    /*
                    //Monochrome
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 0,
                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_204") //"REJECT" - Negative
                    */

                    //Badger
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                        6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM ) //"REJECT" - Negative   //Don't display text for off button regardless.

                ELSE
                    /*
                    //Monochrome
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,                        
                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_204") //"REJECT" - Negative
                    */

                    //Badger
                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Don't display hang up button if unavailable.

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                    ELSE

                        IF g_b_ToggleButtonLabels
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_204") //"REJECT" - Negative
                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"REJECT" - Negative
                        ENDIF

                    ENDIF

                ENDIF

                //This button should not be present on screen, so must be switched off via method second int parameter.
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
                */

                //Badger
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ELSE


                    //Badger
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Positive     

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Don't display hang up button if unavailable.

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                    ELSE

                        IF g_b_ToggleButtonLabels
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_202") //"END CALL" - Negative

                            IF g_Use_Prologue_Cellphone //No End Call in prologue regardless of labels. #844402
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                            ENDIF

                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                        ENDIF

                    ENDIF

            ENDIF

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PRIV - Set_SF_Buttons_SuppText_For_ConnectedCall has malfunctioned - 03")
                cdPrintnl()

                //SCRIPT_ASSERT ("Trying to call movie methods yet either the buttons or gui movie has not loaded for ongoing call! See Steve T.")

            #endif

        //ENDIF
    ENDIF

ENDPROC

PROC Set_SF_Buttons_SuppText_For_ConnectedCall()

        IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)

            //"Connected"

             //Badger
            IF NOT g_IsThisAnMPJobOffer  //Job offer automatically goes to yes / no combo...

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Positive     

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Don't display hang up button if unavailable.

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                ELSE

                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_202") //"END CALL" - Negative

                        IF g_Use_Prologue_Cellphone //No End Call in prologue regardless of labels. #844402
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                        ENDIF

                    ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                    ENDIF

                ENDIF

                IF g_IsThisAnMPChatCall //If this is a human to human chat call we need to display the player's tag and a headshot or blank default head.

                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)

                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~

                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)

                            END_TEXT_COMMAND_SCALEFORM_STRING()

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //"CONNECTED"

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //This field is invisible, so feel free to duplicate with previous text label.

                    END_SCALEFORM_MOVIE_METHOD ()

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintString("CELLPHONE_PRIVATE - Set_SF_Buttons_SuppText_For_ConnectedCall() - g_IsThisAnMPChatCall is TRUE!")
                        cdPrintnl()
                    #endif

                ELSE
                    #if USE_CLF_DLC

                        IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER

                             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel, 
                                "CELL_300",
                                "CELL_219",
                                "CELL_195") 
                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].label,
                                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Picture,
                                "CELL_219",
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].label )
                        ENDIF

                    #endif
                    #if USE_NRM_DLC 

                        IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER

                             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel, 
                                "CELL_300",
                                "CELL_219",
                                "CELL_195")   
                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].label,
                                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Picture,
                                "CELL_219",
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].label )

                        ENDIF

                    #endif
                    #if not USE_SP_DLC
                        IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER		
                             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel, 
                                "CELL_300",
                                "CELL_219",
                                "CELL_195")  
                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                                g_sCharacterSheetAll[g_TheContactInvolvedinCall].Picture,
                                "CELL_219",
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                g_sCharacterSheetAll[g_TheContactInvolvedinCall].label )
                        ENDIF
                    #endif 

                ENDIF

            ENDIF

            //Vital. Cannot be removed. Displays "Connected". 
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen and update any changes

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 101")
                cdPrintnl()
                cdPrintstring ("Display View 4 Vital - CPRIV1")
                cdPrintnl ()

            #endif

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PRIV - Set_SF_Buttons_SuppText_For_ConnectedCall has malfunctioned - 01")
                cdPrintnl()

                //SCRIPT_ASSERT ("Trying to call movie methods yet either the buttons or gui movie has not loaded for connected call! See Steve T.")

            #endif

        ENDIF

ENDPROC

//Whenever a change in g_Cellphone.PhoneDS takes place, we call this procedure once to make sure that the view state enum of 
//scaleform phone is updated accordingly after specifying which method data to set.

PROC Update_Scaleform_VSE()

    IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called but Movie Fail flag returns true, so exiting procedure...")
            cdPrintnl ()

        #endif

        EXIT

    ENDIF

    IF g_Cellphone.PhoneDS < PDS_TAKEOUT

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called but phone state says it should exit anyway due to AWAY state...")
            cdPrintnl ()

        #endif

        EXIT

    ENDIF

    //These two while clauses deliberately separated in order to track down nasty bug 101769 where console log suggests movies are loaded but my assert below can fire.
    //This might be dangerous for multiplayer but it's multiplayer that seems to feck the movie loading up.

    WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)

		IF g_bInMultiplayer
		
			 #if IS_DEBUG_BUILD

	            cdPrintnl()
	            cdPrintstring ("CELLPHONE_PRIV - Multiplayer, exit as cellphone scaleform movie isn't loaded")
	            cdPrintnl()

	        #endif 
		
			EXIT
		ENDIF

        WAIT(0)

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PRIV - Waiting on cellphone scaleform movie to fully load!")
            cdPrintnl()

        #endif 

    ENDWHILE


    //IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)

    SWITCH g_Cellphone.PhoneDS

        CASE PDS_MAXIMUM



            //New for 2222248 diagnosis. Making sure the phone's homescreen is empty.
            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("CELLPHONE_PRIVATE - New for 2222248 diagnosis. Making sure the phone's homescreen is empty.")   
                cdPrintnl()
                        
            #endif

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)





            //Temp - until we get button transparency
            //g_SF_ButtonsPosY = 0.325

            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //transparent

            //Need to do a Scaleform_Place_Items_on_Homescreen() here, can I move this to cellphone_private?
            Scaleform_Place_Items_on_Homescreen (g_LastUsed_Homescreen_Page_Precedence)

            IF g_LastUsed_HomeScreen_Page_Precedence = 1

                //IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)) //Faulty state transition screwing up shutter screen 16 to contacts 2. See 1025518
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (Previous_SelectedAppCursorPos_pageOne)) //Set enum state to home screen
                //ENDIF

                AppCursorIndex = Previous_SelectedAppCursorPos_pageOne

            ELSE

                //IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)) //Faulty state transition screwing up shutter screen 16 to contacts 2. See 1025518
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (Previous_SelectedAppCursorPos_pageTwo))
                //ENDIF

                AppCursorIndex = Previous_SelectedAppCursorPos_pageTwo

            ENDIF

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 102")
                cdPrintnl ()

            #endif


            //Badger - Select Back combo
            IF g_b_ToggleButtonLabels
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

            ELSE
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
            ENDIF

            IF g_b_Secondary_Screen_Available = FALSE

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ELSE

                IF g_bInMultiplayer //no secondary screen for multiplayer...

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                ELSE

                    IF g_Homescreen_In_Secondary_Mode = TRUE

                        IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages 2
                        ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages 2
                        ENDIF

                    ELSE

                        IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages
                        ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages
                        ENDIF

                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                ENDIF

            ENDIF
							
			//6417795 - April 2020
			//Critical!
			//When a major update of the phone's maximum state takes place, e.g on player exit from an application, we will move back
			//to the main, first mode of the homescreen, so add the homescreen switch key again, and clear any "in second mode" bit flag.
			If g_bInMultiplayer
			
				ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()
				CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_MP_CELLPHONE_IN_SECONDARY_MODE)
				
				//Previous_SelectedAppCursorPos_pageOne specifies which slot is selected on return to the homescreen.
				//TODO - we may need to prevent this happening when Settings has been launched then exited as this will put us back
				//to slot 0 on the first mode of the homescreen, regardless of where we might have been before switching homescreen modes. 
				//It's technically correct behaviour but a little weird.
				
				LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(Previous_SelectedAppCursorPos_pageOne)) 
				
			ENDIF
			
        BREAK

        CASE PDS_RUNNINGAPP

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 103")
                cdPrintnl ()

            #endif

            //Update 15.09.10

            //No calls will be allowed to the phone when an app is running. VSE states and buttons should be initialised within the app itself

        BREAK

        CASE PDS_ATTEMPTING_TO_CALL_CONTACT

            //Temp - until we get button transparency
            //g_SF_ButtonsPosY = 0.325

            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //transparent

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 110")
                cdPrintnl()
                cdPrintstring ("Display View 4 - CPRIV2")
                cdPrintnl ()

            #endif


            //Badger
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Positive     

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Don't display hang up button if unavailable.

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

            ELSE

                IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_202") //"END CALL" - Negative

                        IF g_Use_Prologue_Cellphone //No End Call in prologue regardless of labels. #844402
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                        ENDIF

                ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                ENDIF

            ENDIF

        BREAK

        CASE PDS_ONGOING_CALL

            IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED 
                SetUpPhoneSurroundTint()
                
                //New addition for #1546794
                #if USE_CLF_DLC
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
                #endif               
                #if USE_NRM_DLC
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
                #endif
                #if not USE_SP_DLC
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
                #endif   

                IF g_IsThisAnMPChatCall //If it is human to human chat call we need to display the player's tag and a headshot or blank default head.


                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)

                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~

                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)

                            END_TEXT_COMMAND_SCALEFORM_STRING()

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_217")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_217") //This field is invisible, so feel free to duplicate with previous text label.

                     END_SCALEFORM_MOVIE_METHOD ()

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintString("CELLPHONE_PRIVATE - g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED - g_IsThisAnMPChatCall is TRUE!")
                        cdPrintnl()
                    #endif

                ELSE
                    #if USE_CLF_DLC

                        IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER
                             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(2)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                                "CELL_300",     //Use the blank default headshot as this is an unknown caller.
                                "CELL_217",//INCOMING CALL
                                "CELL_195")   
                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(2)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label,
                                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Picture,
                                "CELL_217", 

                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label )
                        ENDIF

                    #endif
                    #if USE_NRM_DLC

                        IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER

                             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(2)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                                "CELL_300",     //Use the blank default headshot as this is an unknown caller.
                                "CELL_217",//INCOMING CALL
                                "CELL_195")   

                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(2)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label,
                                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Picture,
                                "CELL_217", 

                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label )
                        ENDIF
                    #endif
                    #if not USE_SP_DLC
                        IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER				
                             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(2)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                                "CELL_300",     //Use the blank default headshot as this is an unknown caller.
                                "CELL_217",//INCOMING CALL
                                "CELL_195")  
                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(2)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                g_sCharacterSheetAll[g_TheContactInvolvedinCall].Label,
                               	g_sCharacterSheetAll[g_TheContactInvolvedinCall].Picture,
                                "CELL_217", 

                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                g_sCharacterSheetAll[g_TheContactInvolvedinCall].Label )
                        ENDIF
                    #endif
                    
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintString("CELLPHONE_PRIVATE - g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED - g_IsThisAnMPChatCall is FALSE")
                        cdPrintnl()
                    #endif
                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen and update any changes

                #if IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 107")
                    cdPrintnl ()
                    cdPrintnl()
                    cdPrintstring ("Display View 4 - CPRIV3")

                #endif

            ELSE

                IF g_ConversationStatus = CONV_STATE_PLAYING //only draw this screen if conversation is playing.
                OR g_ConversationStatus = CONV_STATE_PAUSE_ANSWERING_TIME

                    //New addition for #1546794
                    #if USE_CLF_DLC
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
                    #endif
                    #if USE_NRM_DLC
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
                    #endif
                    #if not USE_SP_DLC
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
                    #endif
                    SetUpPhoneSurroundTint()

                    IF g_IsThisAnMPChatCall //If it is human to human chat call we need to display the player's tag and a headshot or blank default head. //Fix for 1592981

                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)

                                BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~

                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)

                                END_TEXT_COMMAND_SCALEFORM_STRING()

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //Connected

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //This field is invisible, so feel free to duplicate with previous text label.

                         END_SCALEFORM_MOVIE_METHOD ()

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("Fix for 1592981 swooped into action for chat call!")
                            cdPrintnl()

                        #endif

                    ELSE

                        TEXT_LABEL_23 label_Calling_or_Connected

                        IF g_b_Is_MultipartRepliesInProgress //This is set to true by dialoguehandler when it is about to play a multipart REPLY branch.

                            //If a multipart reply is about to played make sure we keep the call status as "Connected"

                            label_Calling_or_Connected = "CELL_219" //"Connected."

                        ELSE

                            //A fresh outgoing call, make sure status initially says "Calling..."
                            label_Calling_or_Connected = "CELL_211" //"Calling..."

                        ENDIF
                        #if USE_CLF_DLC
                            IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 4)                         

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                                    "CELL_300",
                                    label_Calling_or_Connected,
                                    "CELL_195")   

                            ELSE

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 4) 

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                     g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label,
                                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Picture,
                                    label_Calling_or_Connected,

                                    //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label )

                            ENDIF
                        #endif
                        #if USE_NRM_DLC
                            IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 4)                         

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                                    "CELL_300",
                                    label_Calling_or_Connected,
                                    "CELL_195")   

                            ELSE

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 4) 

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                     g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label,
                                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Picture,
                                    label_Calling_or_Connected,

                                    //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_TheContactInvolvedinCall].Label )

                            ENDIF
                        #endif

                        #if not USE_SP_DLC
                            IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 4)                         

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                                    "CELL_300",
                                    label_Calling_or_Connected,
                                    "CELL_195")   

                            ELSE

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 4) 

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].Label,
                                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].Picture,
                                    label_Calling_or_Connected,

                                    //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].Label )

                            ENDIF
                        #endif

                    ENDIF                                        

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen and update any changes

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 161")
                        cdPrintnl()
                        cdPrintstring ("Display View 4 - CPRIV4")
                        cdPrintnl()

                    #endif


                ENDIF

            ENDIF

            Set_SF_Buttons_to_OngoingCall()

        BREAK

        DEFAULT

            SCRIPT_ASSERT ("Update_Scaleform_VSE in funny state! Bug Steve T with console log.")

            //Perhaps force phone away to be on the safe side? This would class as an interrupted call?

        BREAK

    ENDSWITCH

    //ENDIF  //Maybe have "loading contacts for picture send" here or something.

ENDPROC

//#IF IS_DEBUG_BUILD 
PROC highlight_item()

    SET_TEXT_SCALE (0.4, 0.4)
    SET_TEXT_COLOUR (255, 128, 0, 255) //White
    SET_TEXT_DROPSHADOW (1, 0, 78, 255, 255)

ENDPROC
//#endif

PROC Check_for_navigation() //maybe pass in screen type enum e.g contacts / omni-directional type so that nav types can be handled e.g Contacts will only move up / down 
                            //whereas app screen is up / down / left right.

    IF dpad_pause_cued

        IF TIMERA() > 50

            dpad_pause_cued = FALSE

        ENDIF
    ENDIF

    // Scrollwheel support
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

            // Mousewheel scroll forward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)

                SWITCH AppCursorIndex   

                    CASE 0
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 1
//                      cdPrintString("Contacts scroll - was cursor idx 0, keypress = right, AppCursorIndex = 1")
//                      cdPrintnl()
                    BREAK   

                    CASE 1
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 2
//                      cdPrintString("Contacts scroll - was cursor idx 1, keypress = right, AppCursorIndex = 2")
//                      cdPrintnl()
                    BREAK

                    CASE 2
                        Call_Scaleform_Input_Keypress_Right()
                        Call_Scaleform_Input_Keypress_Down()
                        AppCursorIndex = 3
//                      cdPrintString("Contacts scroll - was cursor idx 2, keypress = right/down, AppCursorIndex = 3")
//                      cdPrintnl()
                    BREAK

                    CASE 3
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 4
//                      cdPrintString("Contacts scroll - was cursor idx 3, keypress = right, AppCursorIndex = 4")
//                      cdPrintnl()
                    BREAK   

                    CASE 4
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 5
//                      cdPrintString("Contacts scroll - was cursor idx 4, keypress = right, AppCursorIndex = 5")
//                      cdPrintnl()
                    BREAK

                    CASE 5
                        Call_Scaleform_Input_Keypress_Right()
                        Call_Scaleform_Input_Keypress_Down()
                        AppCursorIndex = 6
//                      cdPrintString("Contacts scroll - was cursor idx 5, keypress = right/down, AppCursorIndex = 6")
//                      cdPrintnl()
                    BREAK

                    CASE 6
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 7
//                      cdPrintString("Contacts scroll - was cursor idx 6, keypress = right, AppCursorIndex = 7")
//                      cdPrintnl()
                    BREAK   

                    CASE 7
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 8
//                      cdPrintString("Contacts scroll - was cursor idx 7, keypress = right, AppCursorIndex = 8")
//                      cdPrintnl()
                    BREAK

                    CASE 8
                        Call_Scaleform_Input_Keypress_Right()
                        Call_Scaleform_Input_Keypress_Down()
                        AppCursorIndex = 9
//                      cdPrintString("Contacts scroll - was cursor idx 8, keypress = right/down, AppCursorIndex = 9")
//                      cdPrintnl()
                    BREAK

                    CASE 9
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 10
//                      cdPrintString("Contacts scroll - was cursor idx 9, keypress = right/down, AppCursorIndex = 10")
//                      cdPrintnl()
                    BREAK

                    CASE 10
                        Call_Scaleform_Input_Keypress_Right()
                        AppCursorIndex = 11
//                      cdPrintString("Contacts scroll - was cursor idx 10, keypress = right/down, AppCursorIndex = 11")
//                      cdPrintnl()
                    BREAK

                    CASE 11
                        Call_Scaleform_Input_Keypress_Right()
                        Call_Scaleform_Input_Keypress_Down()
                        AppCursorIndex = 0
//                      cdPrintString("Contacts scroll - was cursor idx 11, keypress = right/down, AppCursorIndex = 0")
//                      cdPrintnl()
                    BREAK

                ENDSWITCH

                //dpad_pause_cued = TRUE
                //SETTIMERA (0)

            ENDIF

            // Scroll backward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)

                SWITCH AppCursorIndex   

                    CASE 0
                        Call_Scaleform_Input_Keypress_Left()
                        Call_Scaleform_Input_Keypress_Up()
                        AppCursorIndex = 11
                    BREAK   

                    CASE 1
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 0
                    BREAK

                    CASE 2
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 1
                    BREAK

                    CASE 3
                        Call_Scaleform_Input_Keypress_Left()
                        Call_Scaleform_Input_Keypress_Up()
                        AppCursorIndex = 2
                    BREAK   

                    CASE 4
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 3
                    BREAK

                    CASE 5
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 4
                    BREAK

                    CASE 6
                        Call_Scaleform_Input_Keypress_Left()
                        Call_Scaleform_Input_Keypress_Up()
                        AppCursorIndex = 5
                    BREAK   

                    CASE 7
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 6
                    BREAK

                    CASE 8
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 7
                    BREAK

                    CASE 9
                        Call_Scaleform_Input_Keypress_Left()
                        Call_Scaleform_Input_Keypress_Up()
                        AppCursorIndex = 8
                    BREAK   

                    CASE 10
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 9
                    BREAK

                    CASE 11
                        Call_Scaleform_Input_Keypress_Left()
                        AppCursorIndex = 10
                    BREAK

                ENDSWITCH

                //dpad_pause_cued = TRUE
                //SETTIMERA (0)

            ENDIF

        ENDIF

    IF dpad_pause_cued = FALSE //We need to check this clause individually for each pre

       IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))

            Call_Scaleform_Input_Keypress_Right()

            dpad_pause_cued = TRUE
            SETTIMERA (0)

            SWITCH g_Cellphone.PhoneDS

                CASE PDS_MAXIMUM

                    IF AppCursorIndex < (ENUM_TO_INT(MAX_APPLICATIONS) - 1)

                        AppCursorIndex ++

                    ENDIF

                BREAK

                CASE PDS_MINIMAL

                    IF AppCursorIndex < 3 //We don't want the cursor to move beyond the minimal apps. This has been set up as having Apps 0, 1, 2 and 3 as a temp measure.

                        AppCursorIndex ++

                    ENDIF

                BREAK

            ENDSWITCH

        ENDIF

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))

            //Keypress Left
            Call_Scaleform_Input_Keypress_Left()

            dpad_pause_cued = TRUE
            SETTIMERA (0)

            IF AppCursorIndex > 0
                AppCursorIndex --
            ENDIF

        ENDIF

        //IF g_Cellphone.PhoneDS = PDS_MAXIMUM - removed so that a numpad or prospective keypad could use this proc.

            //IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))

                //Keypress Up
                Call_Scaleform_Input_Keypress_Up()

                dpad_pause_cued = TRUE
                SETTIMERA (0)

                IF AppCursorIndex > 3
                    AppCursorIndex = AppCursorIndex - 4
                ENDIF

            ENDIF       

            //IF IS_BUTTON_PRESSED (PAD1, INT_TO_ENUM (PAD_BUTTON_NUMBER, PHONE_NAV_DOWN_INPUT))
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

                //Keypress Down
                Call_Scaleform_Input_Keypress_Down()

                dpad_pause_cued = TRUE
                SETTIMERA (0)

                IF AppCursorIndex < 8
                    AppCursorIndex = AppCursorIndex + 4
                ENDIF

            ENDIF   

        //ENDIF

    ENDIF

ENDPROC

PROC Request_Load_Scriptstring(STRING scriptstring_to_load)

    REQUEST_SCRIPT (scriptstring_to_load)

    WHILE NOT HAS_SCRIPT_LOADED (scriptstring_to_load)

        WAIT(0)

    ENDWHILE

ENDPROC

//Fill in the App Sheet with the used defined data for each field.
PROC Fill_App_Sheet(enumApplicationList PassedApp, STRING PassedAppName, INT PassedAppInt, STRING PassedScriptName, INT PassedIconEnum, INT PassedPrecedence, enumAppAvailabilityOWNER1 PassedO1, enumAppAvailabilityOWNER2 PassedO2, enumAppAvailabilityOWNER3 PassedO3)

    g_AppList[PassedApp].AppName = PassedAppName
    g_AppList[PassedApp].ThisAppPosition = PassedAppInt
    g_AppList[PassedApp].AppScriptName = PassedScriptName

    g_AppList[PassedApp].AppScriptNameHash = GET_HASH_KEY(PassedScriptName)

    g_AppList[PassedApp].SF_IconEnum = PassedIconEnum
    g_AppList[PassedApp].Homescreen_Page_Precedence = PassedPrecedence

    g_AppList[PassedApp].O1_Availability = PassedO1
    g_AppList[PassedApp].O2_Availability = PassedO2
    g_AppList[PassedApp].O3_Availability = PassedO3

    //Temporary checks to remove unreferenced reporting. These vars within the struct are likely to be used before project end.
    IF g_AppList[PassedApp].O1_Availability = APP_O1_LOCKED
       g_AppList[PassedApp].O1_Availability = APP_O1_LOCKED

    ENDIF

    IF g_AppList[PassedApp].O2_Availability = APP_O2_LOCKED
       g_AppList[PassedApp].O2_Availability = APP_O2_LOCKED

    ENDIF

    IF g_AppList[PassedApp].O3_Availability = APP_O3_LOCKED
       g_AppList[PassedApp].O3_Availability = APP_O3_LOCKED

    ENDIF

ENDPROC

//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
//Determine if the player is currently acting as Franklin or Lamar in a mulitplayer context.
FUNC BOOL IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
	
	IF g_bInMultiplayer
		IF g_bUseFranklinThemeInMP OR g_bUseLamarThemeInMP = TRUE		
			CDEBUG1LN (DEBUG_CELL_DIALOG, "Cellphone_Fixer: Playing as Franklin or Lamar in MP.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC Fill_All_Primary_App_Sheets()
    /*Icon list from Gareth

                * CAMERA = 1
                * TEXT_MESSAGE = 2
                * CALENDAR = 3
                * EMAIL = 4
                * CALL = 5
                * EYEFIND = 6
                * MAP = 7
                * APPS = 8
                * PHOTOS = 9
                * ATTACHMENT = 10             
                 * NEW_CONTACT = 11          
                 * SIDE_TASKS = 12 
                 * BAWSAQ = 13     
                 * MULTIPLAYER = 14             
                 * MUSIC = 15         
                 * GPS = 16             
                 * SPARE = 17         
                 * RINGTONE = 18  
                 * TEXT_TONE = 19 
                 * VIBRATE_ON = 20              
                 * VIBRATE_OFF = 21             
                 * VOLUME = 22     
                 * SETTINGS_1 = 23                
                 * SETTINGS_2 = 24                
                 * PROFILE = 25
                * SLEEP_MODE = 26
                * MISSED_CALL = 27
                * UNREAD_EMAIL = 28
                * READ_EMAIL = 29
                * REPLY_EMAIL = 30

    */

    //Changing the third parameter will change the slot that the app occupies in the master list.
    //Depending on the locked / unlocked status this slot should change appropriately. This is just a temp list.

    //Notes:
    //We need to make sure that every slot ( the third parameter) of any homescreen page is filled with something - even a dummy. The dummy MUST be unique  
    //with regards to the first enum parameter however and can't be filled twice or we will risk an undefined slot. Same goes for other apps. 
    //If the same app needs to appear on a few pages, just add another unique enum for it but fill it out with the same launch script, icon and label data.  

    //All apps from the appenum should be declared for SP or MP. 

    //Make sure that two apps do not occupy the same position on the homescreen. Parameter 3 passed to Fill_App_Sheet must be unique to that app if you want
    //it to appear on the homescreen.

    //As a precaution default every app homescreen position to -99, so they will not appear unless specified to for SP or MP branch below.
    INT tempAppIndex = 0
    WHILE tempAppIndex < (ENUM_TO_INT(MAX_APPLICATIONS))
        g_AppList[tempAppIndex].ThisAppPosition = -99
        tempAppIndex ++
    ENDWHILE

    IF g_bInMultiplayer = FALSE //Set up apps for singleplayer....

		CDEBUG1LN (DEBUG_CELL_DIALOG, "CELLPHONE_PRIV JULY 2020 - Primary App Sheet filling for Singleplayer.")
        CDEBUG1LN (DEBUG_CELL_DIALOG, "")

        //Work out if we need to display the normal cotacts icon or the missed call icon instead.   This segment could go in before a "place items on homescreen"
        //call if we need to update the gui on the fly rather than just when the phone is put away.

        BOOL    MissedCallFound = FALSE
        INT     tempIndex

        #if USE_CLF_DLC
            WHILE tempIndex < (ENUM_TO_INT(MAX_CLF_CHARACTERS))
                IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[tempIndex].missedCallStatus[g_Cellphone.PhoneOwner] = MISSED_CALL
                    MissedCallFound = TRUE
                ENDIF
                tempIndex ++
            ENDWHILE    
        #endif
        #if USE_NRM_DLC
            WHILE tempIndex < (ENUM_TO_INT(MAX_NRM_CHARACTERS))
                IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[tempIndex].missedCallStatus[g_Cellphone.PhoneOwner] = MISSED_CALL
                    MissedCallFound = TRUE
                ENDIF
                tempIndex ++
            ENDWHILE    
        #endif
        #if not USE_SP_DLC
            WHILE tempIndex < (ENUM_TO_INT(MAX_CHARACTERS))
                IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, tempIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                    MissedCallFound = TRUE
                ENDIF
                tempIndex ++
            ENDWHILE
        #endif

        //Page 1... Note the sixth parameter to determine which page the app should appear on. Make sure this is correct!

        Fill_App_Sheet (AppEMAIL, "CELL_5", 0, "appEmail", 4, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)     //4
        #if USE_CLF_DLC
            Fill_App_Sheet (AppTEXTS, "CELL_1", 1, "appTextMessCLF", 2, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        #endif 

        #if USE_NRM_DLC
            Fill_App_Sheet (AppTEXTS, "CELL_1", 1, "appTextMessNRM", 2, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        #endif 
        #if not USE_SP_DLC
            Fill_App_Sheet (AppTEXTS, "CELL_1", 1, "appTextMessage", 2, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        #endif

        Fill_App_Sheet (AppCHECKLIST, "CELL_23", 2, "appChecklist", 39, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)


        IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
        AND (IS_REPEAT_PLAY_ACTIVE() = FALSE) // Added at request of #1416986
        AND (g_QuickSaveDisabledByScript = FALSE) // #1423552
        //IF g_b_ReplaceThisWithQSavailable = TRUE
            Fill_App_Sheet (AppQUICKSAVE, "CELL_32", 3, "appSettings", 43, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)  
            g_b_QuickSaveGreyedOut = FALSE
            g_HomescreenIcon_AlphaValue = i_HomescreenIcon_FullAlpha
        ELSE
            Fill_App_Sheet (AppQUICKSAVE, "CELL_32", 3, "appSettings", 43, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            g_b_QuickSaveGreyedOut = TRUE
            g_HomescreenIcon_AlphaValue = i_HomescreenIcon_GreyedOut
        ENDIF

        #if USE_CLF_DLC
            IF MissedCallFound = TRUE
                Fill_App_Sheet (AppCONTACTS, "CELL_0", 4, "appContactsCLF", 27, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ELSE
                Fill_App_Sheet (AppCONTACTS, "CELL_0", 4, "appContactsCLF", 5, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ENDIF
            Fill_App_Sheet (AppSETTINGS, "CELL_16", 5, "appSettingsCLF", 24, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        #endif

        #if USE_NRM_DLC
            IF MissedCallFound = TRUE
                Fill_App_Sheet (AppCONTACTS, "CELL_0", 4, "appContactsNRM", 27, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ELSE
                Fill_App_Sheet (AppCONTACTS, "CELL_0", 4, "appContactsNRM", 5, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ENDIF
            Fill_App_Sheet (AppSETTINGS, "CELL_16", 5, "appSettings", 24, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        #endif

        #if not USE_SP_DLC
            IF MissedCallFound = TRUE
                Fill_App_Sheet (AppCONTACTS, "CELL_0", 4, "appContacts", 27, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ELSE
                Fill_App_Sheet (AppCONTACTS, "CELL_0", 4, "appContacts", 5, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ENDIF
            Fill_App_Sheet (AppSETTINGS, "CELL_16", 5, "appSettings", 24, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        #endif

        Fill_App_Sheet (AppCAMERA, "CELL_7", 6, "appCamera", 1, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppINTERNET, "CELL_2", 7, "appInternet", 6, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        #if USE_CLF_DLC
            IF g_savedGlobalsClifford.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE
                Fill_App_Sheet (AppTRACKIFY, "CELL_28", 8, "appTrackify", 42, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ENDIF
            IF g_savedGlobalsClifford.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE
                Fill_App_Sheet (AppSNIPER, "CELL_25", 0, "appContacts", 40, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED) //Icon should be 40. Check in next build.
            ENDIF    
        #endif
        #if USE_NRM_DLC
            IF g_savedGlobalsnorman.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE
                Fill_App_Sheet (AppTRACKIFY, "CELL_28", 8, "appTrackify", 42, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ENDIF
            IF g_savedGlobalsnorman.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE
                Fill_App_Sheet (AppSNIPER, "CELL_25", 0, "appContacts", 40, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED) //Icon should be 40. Check in next build.
            ENDIF     
        #endif
        #if not USE_SP_DLC
            IF g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE
                Fill_App_Sheet (AppTRACKIFY, "CELL_28", 8, "appTrackify", 42, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
            ENDIF
            IF g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE
                Fill_App_Sheet (AppSNIPER, "CELL_25", 0, "appContacts", 40, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED) //Icon should be 40. Check in next build.
            ENDIF    
        #endif
        

        Fill_App_Sheet (AppDummyApp2, "CELL_14", -99, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        //Bawsaq
        Fill_App_Sheet (AppBAWSAQ, "CELL_13", -99, "appContacts", 13, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        //Fill_App_Sheet (AppSIDETASK, "CELL_4", 4, "appSideTask", 12, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppPAGE2APPS, "CELL_14", -99, "appContacts", 8, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppDummyApp4, "CELL_15", -99, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp5, "CELL_15", -99, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp6, "CELL_15", -99, "appSettings", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppDummyApp7, "CELL_15", -99, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        //Include a couple more for testing overflow.
        Fill_App_Sheet (AppDummyApp8, "CELL_15", -99, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp9, "CELL_15", -99, "appSettings", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

    ELSE  //Fill app sheets for multiplayer...
		
		CDEBUG1LN (DEBUG_CELL_DIALOG, "CELLPHONE_PRIV JULY 2020 - Primary App Sheet filling for Multiplayer...")
        CDEBUG1LN (DEBUG_CELL_DIALOG, "")

        // KGM 8/2/13: Switched homepage position with the 'Cop Backup' app since RaceToPoint app has been removed

        Fill_App_Sheet (AppEMAIL, "CELL_5", 0, "appMPEmail", 4, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)           //Note this is a different script to SP.

        Fill_App_Sheet (AppSETTINGS, "CELL_16", 5, "appSettings", 24, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)     //Should be "appSettings"

        Fill_App_Sheet (AppTEXTS, "CELL_1", 1, "appTextMessage", 2, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppCAMERA, "CELL_7", 6, "appCamera", 1, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        // KGM 18/10/12: Activated Internet in CnC
        Fill_App_Sheet (AppINTERNET, "CELL_2", 7, "appInternet", 6, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        // KGM 5/9/11: Swapped over to using the new MP Joblist app
        // KGM 7/2/12: Cops Joblist will now be titled 'Leads'
        // KGM 22/9/12: Freemode Joblist will now be titled 'Activities'
        Fill_App_Sheet (AppMPJOBLIST, "CELL_29", 4, "appMPJobListNEW", 12, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppCONTACTS, "CELL_0", 2, "appContacts", 5, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppJIPMP, "CELL_37", 3, "AppJIPMP", 14, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        #if USE_TU_CHANGES
        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY) = TRUE

            Fill_App_Sheet (AppTRACKIFY, "CELL_28", 8, "appTrackify", 42, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        ENDIF
        #endif

        /*
        IF (g_bBroadcastAppEnabled)
            Fill_App_Sheet (AppBroadcast, "CELL_39", 0, "appBroadcast", 49, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        ENDIF
        */

        //getting rid of appMpPlayerList
        Fill_App_Sheet (AppMPCOPBACKUP, "CELL_35", -99, "appMPCopBackup", 39, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        // KGM 6/9/11: Add Cop Backup app for Cops only
        // KGM 16/3/12: Add Race Point To Point - NOTE: Using the Email icon for now but we'll need a special icon
        // KGM 16/9/11: Because two different apps can occupy the same slot, clear the slot number for the app that is not needed by setting to -99
        // KGM 14/11/12: Race To Point app is initially locked - change title to show 'LOCKED'
        // KGM 8/3/12: Race To Point app removed - switched homepage position with the Settings App
        Fill_App_Sheet (AppMPCOPBACKUP, "CELL_20", -99, "appMPCopBackup", 16, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)         // Not Active in Freemode

        Fill_App_Sheet (AppPAUSEMAP, "CELL_18", -99, "appContacts", 8, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)                // PauseMap Not Active

        //Bawsaq and Sidetask removed... replaced by spare dummies. Note slot position of -99
        Fill_App_Sheet (AppBAWSAQ, "CELL_13", -99, "appContacts", 13, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppSIDETASK, "CELL_4", -99, "appContacts", 12, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        //Page 2
        Fill_App_Sheet (AppDummyApp0, "CELL_15", 0, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp1, "CELL_15", 1, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp2, "CELL_15", 2, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        //Bawsaq
        Fill_App_Sheet (AppDummyApp3, "CELL_15", 3, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppDummyApp4, "CELL_15", 4, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp5, "CELL_15", 5, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp6, "CELL_15", 6, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppDummyApp7, "CELL_15", 7, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppDummyApp8, "CELL_15", 8, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
        Fill_App_Sheet (AppDummyApp9, "CELL_15", 9, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        Fill_App_Sheet (AppDummyApp10, "CELL_15", 10, "appContacts", 17, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)





        //2086898 - Add in icon for Circuit breaking hacking minigame. Must be done here after the dummy fill.
        #if USE_TU_CHANGES
            
            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY) = TRUE
            
                IF g_bEnableHackingApp

                    #if IS_DEBUG_BUILD
                    
                        cdPrintstring ("CELLPHONE_PRIV - g_bEnableHackingApp is TRUE - adding icon for hacking minigame. 2086898")
                        cdPrintnl()

                    #endif

                    //AppVLSI will now be a standard app launch. See 2150653. Has its own dedicated script in the project; AppVLSI
                    Fill_App_Sheet (AppDummyApp0, "CELL_CIRCBREAK", 8, "AppVLSI", 54, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)


                ELSE


                    IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP) = TRUE

                        #if IS_DEBUG_BUILD
                    
                            cdPrintstring ("CELLPHONE_PRIV - g_bEnableHackingApp is FALSE, but SightSeer bit is enabled - adding icon for SightSeer app. 2556852")
                            cdPrintnl()

                        #endif

                        //Sam G. overwrote the old Media App icon at position 59 as we don't use that any longer.
                        Fill_App_Sheet (AppDummyApp0, "CELL_SIGHTS", 8, "AppSettings", 59, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)


                    ELSE

                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP) = TRUE
                             
                            #if IS_DEBUG_BUILD
                    
                                cdPrintstring ("CELLPHONE_PRIV - g_bEnableHackingApp is FALSE, no SightSeer bit, but Extraction App bit is set  - adding icon for Extraction app. 2576009")
                                cdPrintnl()

                            #endif

                            Fill_App_Sheet (AppDummyApp0, "CELL_EXTRACT", 8, "AppExtraction", 58, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
						
						
						ELSE
						
                       		IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP) = TRUE
								
								//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
								//The Securoserv icon doesn't exist as a thematically correct icon in the Badger OS. We need to make sure
								//it's not present when playing as these characters in MP. ( They don't need to access Securoserv hacking. )
								IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
								
									#if IS_DEBUG_BUILD
	                                	cdPrintstring ("CELLPHONE_PRIV - Securoserv Hack bit is set but player is currently Franklin or Lamar. Not adding app entry.")
	                                	cdPrintnl()
	                            	#endif
								
									//Make sure the dummy app in the bottom right hand corner is empty.
					 				Fill_App_Sheet (AppDummyApp0, "CELL_0", -99, "appContacts", 5, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
								
								ELSE
								
									#if IS_DEBUG_BUILD
	                                	cdPrintstring ("CELLPHONE_PRIV - g_bEnableHackingApp is FALSE, no SightSeer bit, no Extraction App, Securoserv Hack bit is set  - adding icon for Securoserv Hack app. 3151533")
	                                	cdPrintnl()
	                            	#endif
								
                           			Fill_App_Sheet (AppDummyApp0, "CELL_SECHACK", 8, "AppSecuroHack", 57, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
								
								ENDIF

							ENDIF
						
                        ENDIF

                    ENDIF
     
                ENDIF

            ELSE

                #if IS_DEBUG_BUILD
                
                    cdPrintstring ("CELLPHONE_PRIV - g_bEnableHackingApp is TRUE but not adding as Trackify is still available in MP.")
                    cdPrintnl()

                #endif

            ENDIF



            //2411187 - Add launch mechanism for Benny's Motorworks  -  url:bugstar:2554214 Update. This was only for the duration of Lowrider. 
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY) = FALSE
            AND g_bEnableHackingApp = FALSE
            AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP) = FALSE
            AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP) = FALSE
			AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP) = FALSE
                //Benny's Lowrider app launch setup - No longer required.
                //Fill_App_Sheet (AppDummyApp0, "CELL_BENWEB", 8, "AppInternet", 56, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

                //For url:bugstar:2554214, Benny's has been replaced by Boss Agency App. 
                //Re-adding Boss Agency after 29th Flow play lockdown.
				
				//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
				//The Securoserv icon doesn't exist as a thematically correct icon in the Badger OS. We need to make sure
				//it's not present when playing as these characters in MP. ( They don't need to access Securoserv's boss agency. )
				IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
								
					#if IS_DEBUG_BUILD
                    	cdPrintstring ("CELLPHONE_PRIV - Securoserv Hack bit is set but player is currently Franklin or Lamar. Not adding app entry.")
                    	cdPrintnl()
                	#endif
				
					//Make sure the dummy app in the bottom right hand corner is empty.
					 Fill_App_Sheet (AppDummyApp0, "CELL_0", -99, "appContacts", 5, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
								
				ELSE
				
                	Fill_App_Sheet (AppDummyApp0, "CELL_BOSSAGE", 8, "appMPBossAgency", 57, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
				
				ENDIF

            ENDIF


			//6417795 - April 2020
			//Clobbers any MP specific positioning of the "settings" application made above.
			//Adds dummy app for launching arcade mode in MP only at homescreen cell position 5 on the first screen. 
			//Using the Trackify icon 42 temporarily and moved settings in MP onto a virtual second homescreen page.
			
			//Needs tunable support added for 6474707 - shouldn't add DummyApp1 and leave settings where it was, if the new secondary screen mode is switched off.		
			
			//#6564297 Steve T - adding individual wrap for this. Don't want it to be obvious that AppZit was being used for something here.
			#IF FEATURE_FREEMODE_ARCADE
			#IF FEATURE_COPS_N_CROOKS
			IF SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE()
			
				#if IS_DEBUG_BUILD
					CDEBUG1LN (DEBUG_CELL_DIALOG, "Fill_App_Sheet adding Arcade Mode selection to first screen as secondary mode is deemed available")
				#ENDIF

				Fill_App_Sheet (AppSETTINGS, "CELL_16", 0, "appSettings", 24, 2, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)    
				
				//Previous Arcade Mode launcher not associated with any valid script.
				//Fill_App_Sheet (AppDummyApp1, "CELL_AMODESEL", 5, "D_App1_Sc", 42, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED) 
				
				//Investigation for 6478196, need to have the Arcade Mode launcher fire a confirmation screen. Has to be embedded in an app. 
				//AppZit.cs isn't being used for anything so we can use that rather than risk adding anything to the project.
                Fill_App_Sheet (AppDummyApp1, "CELL_AMODESEL", 5, "AppZit", 42, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)   

				
			ENDIF
			#ENDIF
			#ENDIF
			
			
			#IF FEATURE_FREEMODE_ARCADE
			#IF FEATURE_COPS_N_CROOKS
			//May 2020 Joblist, Arcade Mode and Internet App app icons disabled whilst in CnC. See bugs 6417788 6457063 6461213
			//Swap Snapmatic and Joblist icons in CnC so the player central navigation position 4 is over Snapmatic rather than the now disabled Joblist app.
			IF IS_CELLPHONE_IN_CNC()
			
				Fill_App_Sheet (AppCAMERA, "CELL_7", 4, "appCamera", 1, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
				Fill_App_Sheet (AppMPJOBLIST, "CELL_29", 6, "appMPJobListNEW", 12, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
	
			ENDIF
			#ENDIF
			#ENDIF
		//End of use TU changes clause.
        #endif










    ENDIF

    //End of primary fill.

ENDPROC

PROC Check_For_Secondary_Screen_Selection()

    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT))

        IF g_bInMultiplayer = FALSE

            IF g_b_Secondary_Screen_Available

                IF g_Homescreen_In_Secondary_Mode = FALSE

                    g_Homescreen_In_Secondary_Mode = TRUE

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 193")
                        cdPrintnl ()

                    #endif

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)

                    Scaleform_Place_Items_on_Homescreen(2)

                    Update_Page_Indicator_SoftKey() 

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(Previous_SelectedAppCursorPos_pageTwo)) //Need to make the cursor point to position zero.

                    AppCursorIndex = Previous_SelectedAppCursorPos_pageTwo

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 195")
                        cdPrintnl ()

                    #endif

                    //Set enum state to HOME_SCREEN

                ELSE

                    g_Homescreen_In_Secondary_Mode = FALSE

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 196")
                        cdPrintnl ()

                    #endif

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)

                    Scaleform_Place_Items_on_Homescreen(1)

                    Update_Page_Indicator_SoftKey() 

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (Previous_SelectedAppCursorPos_pageOne)) //Set enum state to HOME_SCREEN

                    AppCursorIndex = Previous_SelectedAppCursorPos_pageOne

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PRIV - Update_Scaleform_VSE called - 197")
                        cdPrintnl ()

                    #endif

                ENDIF

            ENDIF

        ENDIF

    ENDIF
    ENDIF

ENDPROC

//Creates physical 3d phone below screen with the correct model depending on the current player character. Also assigns global soundset for that particular phone.
PROC Create_3dPhoneObject() 

    #IF IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("CELLPHONE_PRIV - Creating 3d cellphone via Create_3dPhoneObject")
        cdPrintnl()

    #endif

    IF g_bInMultiplayer

        // KGM 4/9/12: Ensures MP Freemode phone is always consistent
        // NOTE: If we decide to allow FM players a choice of phone then this is where a global would be read and used to physically change the phone
        //          and the 'theme' would need set after the phone had been created - so this is done in cellphone_flashhand.sc (search for 'ThemeDecision')

        // Freemode (and, by default, any other modes we add)
        g_Owner_Soundset = "Phone_SoundSet_Default"
        CREATE_MOBILE_PHONE (MOBILE_1)

    ELSE

        IF g_Use_Prologue_Cellphone

            CREATE_MOBILE_PHONE (MOBILE_5)

            g_Owner_Soundset = "Phone_SoundSet_Prologue"

            //g_Owner_Soundset = "Phone_SoundSet_Prologue"  //Comment in when George submits and check this.

        ELSE

            SWITCH g_Cellphone.PhoneOwner 

                //Mobile_5 clause would go in here.

                CASE CHAR_MICHAEL  

                    g_Owner_Soundset = "Phone_SoundSet_Michael"

                    CREATE_MOBILE_PHONE (MOBILE_1)

                BREAK

                CASE CHAR_TREVOR

                    g_Owner_Soundset = "Phone_SoundSet_Trevor"

                    CREATE_MOBILE_PHONE (MOBILE_2)

                BREAK

                CASE CHAR_FRANKLIN

                    g_Owner_Soundset = "Phone_SoundSet_Franklin"

                    CREATE_MOBILE_PHONE (MOBILE_3)

                BREAK

                DEFAULT

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("CELLPHONE_PRIV - Phone model decision weird! Defaulting to model 1.")
                        cdPrintnl()

                    #endif

                    g_Owner_Soundset = "Phone_SoundSet_Default"

                    CREATE_MOBILE_PHONE (MOBILE_1)

                BREAK

            ENDSWITCH 

        ENDIF

    ENDIF

    g_3dPhonePocketRotVec = <<-90, -130, 0>>

    IF GET_IS_HIDEF()

        g_Chosen_Ratio = DISPLAY_16_9 //Safe zone manipulation to handle 4-3 automatically, so we always default to 16-9 even if using 4-3?

        g_This_Screen_3dPhoneStartVec[DISPLAY_16_9] = <<(GET_SAFE_ZONE_SIZE() * 117.2), (GET_SAFE_ZONE_SIZE() * -158.8), -113.0>>        
        g_This_Screen_3dPhoneEndVec[DISPLAY_16_9] = <<(GET_SAFE_ZONE_SIZE() * 117.2), (GET_SAFE_ZONE_SIZE() * -53.3), -113.0>> 

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PRIV - Selecting HI_DEF position.")
            cdPrintnl()

        #endif

    ELSE

        g_Chosen_Ratio = DISPLAY_16_9 

        //Adopt larger size for phone in 4_3, CRT - see bug #1560031

        g_This_Screen_3dPhoneStartVec[DISPLAY_16_9] = <<(GET_SAFE_ZONE_SIZE() * 85.7), (GET_SAFE_ZONE_SIZE() * -121.8), -91.5>> //#1579950
        g_This_Screen_3dPhoneEndVec[DISPLAY_16_9] = <<(GET_SAFE_ZONE_SIZE() * 85.7), (GET_SAFE_ZONE_SIZE() * -35.3), -91.5>>     //#1579950

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PRIV - Selecting STANDARD DEF position.")
            cdPrintnl()

        #endif

    ENDIF

    g_3dPhonePosVec = g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio]

    //Put this in once I work out the start position = remove to debug...

    SET_MOBILE_PHONE_POSITION (g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio])

    SET_MOBILE_PHONE_ROTATION (g_3dPhonePocketRotVec) //As the phone has just been created, set the initial rotation as if it was being pulled from a pocket.

    g_3DPhoneNeedsMovedUp = TRUE //Tell flashhand that we need to move the phone up on screen.

    //SET_BIT (BitSet_CellphoneDisplay_Third, g_CELLPHONE_MOVIE_STREAMING_IN)  //Comment in for resolution of #1135663

ENDPROC

//This will make the player put the phone handset to his ear.

PROC Put_Cellphone_To_Ear()

    //Commented out until asserts are fixed
    #if IS_DEBUG_BUILD
        cdPrintstring ("CELLPHONE_PRIV - Phone put up to ear request. Attempting to set bit")
        cdPrintnl()
        PRINTLN("CELLPHONE_PUB - Phone put up to ear request from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    BOOL b_WearingProblemHeadgear = FALSE


    BOOL b_InCoverFirstPerson = FALSE

    IF NOT IS_PED_INJURED (PLAYER_PED_ID())
        IF IsCurrentCamFirstPerson()
        AND IS_PED_IN_COVER(PLAYER_PED_ID())

            b_InCoverFirstPerson = TRUE

        ENDIF
    ENDIF


    IF NOT g_bInMultiplayer
        IF NOT IS_PED_INJURED (PLAYER_PED_ID())
        AND b_InCoverFirstPerson = FALSE         

            INT i_Headgear

            i_Headgear = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)

            IF g_bInMultiplayer

                IF i_Headgear = 15  
                OR i_Headgear = 16  
                OR i_Headgear = 17                 
                OR i_Headgear = 18 
                OR i_Headgear = 19 
                OR IS_PED_WEARING_HELMET(PLAYER_PED_ID())

                    b_WearingProblemHeadgear = TRUE

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("CELLPHONE_PRIV. Player is wearing headgear that would cause phone to clip through prop, not placing phone to ear.")
                        PRINTNL()

                    #endif

                ENDIF

            ELSE

                IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())

                    b_WearingProblemHeadgear = TRUE

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("CELLPHONE_PRIV. Player is wearing headgear that would cause phone to clip through prop, not placing phone to ear.")
                        PRINTNL()

                    #endif

                ENDIF

            ENDIF

            IF NOT (g_Use_Prologue_Cellphone) // Removed this for the prologue mission as that phone is only used as a detonator: See bug 1101927

                IF g_TheContactInvolvedinCall <> CHAR_DETONATEBOMB //1309268   

                    IF b_WearingProblemHeadgear = FALSE

                        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                                #if IS_DEBUG_BUILD
                                    cdPrintstring ("CELLPHONE_PRIV - Attempt to put phone to ear, but player in vehicle. See TODO 1888348")
                                    cdPrintnl()
                                #endif

                            ELSE

                                #if IS_DEBUG_BUILD
                                    cdPrintstring ("CELLPHONE_PRIV - Phone put up to ear. Bit set.")
                                    cdPrintnl()
                                #endif

                                IF g_bInMultiplayer
                                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, FALSE)
                                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, FALSE)
                                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, FALSE)
                                ENDIF

                                SET_BIT (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)

                                TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), TRUE)

                            ENDIF

                        ENDIF

                    ENDIF

                ENDIF

            ENDIF

        ENDIF
    ENDIF

ENDPROC

//Removes the handset from the player's ear.

PROC Remove_Cellphone_From_Ear()

    //Commented out until asserts are fixed
    #if IS_DEBUG_BUILD
        cdPrintstring ("CELLPHONE_PRIV - Phone remove from ear request. Checking bit.")
        cdPrintnl()
    #endif

    IF NOT g_bInMultiplayer
        IF NOT IS_PED_INJURED (PLAYER_PED_ID())

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)

                IF NOT (g_Use_Prologue_Cellphone) // Removed this for the prologue mission as that phone is only used as a detonator: See bug 1101927
                    TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), FALSE)                                                                                          
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PRIV - Phone removed from ear - bit cleared.")
                    cdPrintnl()
                #endif

                IF g_bInMultiplayer
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, TRUE)
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
                ENDIF

                //SCRIPT_ASSERT("Task test")
                //CLEAR_PED_TASKS_IMMEDIATELY (PLAYER_PED_ID()) 

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)

            ENDIF

        ENDIF
    ENDIF

ENDPROC

FUNC BOOL IsFirstTimeNewerThanSecondTime (structTxtMsgTimeSent FirstTimeToCompare, structTxtMsgTimeSent SecondTimeToCompare)

    structTxtMsgTimeSent FirstComparisonStruct, SecondComparisonStruct 

    INT FirstSecsMinsHoursDaysValue, SecondSecsMinsHoursDaysValue

    IF FirstTimeToCompare.TxtMsgYear < SecondTimeToCompare.TxtMsgYear //First value is older as the second value occurred in a more recent year.

        RETURN FALSE

    ENDIF

    IF FirstTimeToCompare.TxtMsgYear > SecondTimeToCompare.TxtMsgYear //First value is more recent as the second value occurred in an earlier year.

        RETURN TRUE

    ENDIF

    IF FirstTimeToCompare.TxtMsgYear = SecondTimeToCompare.TxtMsgYear //If we haven't returned true or false we can assume that the year fields the of the two times are the same

        IF FirstTimeToCompare.TxtMsgMonth < SecondTimeToCompare.TxtMsgMonth //First value is older as the second value occurred in a more recent month.

            RETURN FALSE

        ENDIF

        IF FirstTimeToCompare.TxtMsgMonth > SecondTimeToCompare.TxtMsgMonth //First value is more recent as the second value occurred in an earlier month.

            RETURN TRUE

        ENDIF

        IF FirstTimeToCompare.TxtMsgMonth = SecondTimeToCompare.TxtMsgMonth  //Months must be the same also. We need to see which day is most recent.

             //Work out how many seconds are in each field of the two temp comparison structs. The resultant int will be that day's value.

             FirstComparisonStruct.TxtMsgSecs = FirstTimeToCompare.TxtMsgSecs

             FirstComparisonStruct.TxtMsgMins = (FirstTimeToCompare.TxtMsgMins * 60) //60 secs in a min.
             FirstComparisonStruct.TxtMsgHours = (FirstTimeToCompare.TxtMsgHours * 3600) //3600 secs in a hour.

             FirstComparisonStruct.TxtMsgDay = (FirstTimeToCompare.TxtMsgDay * (3600 * 24)) //86400 secs in a day.

             FirstSecsMinsHoursDaysValue = (FirstComparisonStruct.TxtMsgSecs + FirstComparisonStruct.TxtMsgMins + FirstComparisonStruct.TxtMsgHours + FirstComparisonStruct.TxtMsgDay)

             SecondComparisonStruct.TxtMsgSecs = SecondTimeToCompare.TxtMsgSecs

             SecondComparisonStruct.TxtMsgMins = (SecondTimeToCompare.TxtMsgMins * 60) //60 secs in a min.
             SecondComparisonStruct.TxtMsgHours = (SecondTimeToCompare.TxtMsgHours * 3600) //3600 secs in a hour.

             SecondComparisonStruct.TxtMsgDay = (SecondTimeToCompare.TxtMsgDay * (3600 * 24)) //86400 secs in a day.

             SecondSecsMinsHoursDaysValue = (SecondComparisonStruct.TxtMsgSecs + SecondComparisonStruct.TxtMsgMins + SecondComparisonStruct.TxtMsgHours + SecondComparisonStruct.TxtMsgDay)

             IF FirstSecsMinsHoursDaysValue > SecondSecsMinsHoursDaysValue  //First value is more recent. The month is the same and the year is the same but the assigned value 
             OR FirstSecsMinsHoursDaysValue = SecondSecsMinsHoursDaysValue  //using weighted multipliers is larger, so we return true.

                RETURN TRUE

             ELSE 

                RETURN FALSE

             ENDIF

        ENDIF

    ENDIF

    RETURN FALSE

ENDFUNC

#if USE_CLF_DLC
PROC ForceDeletionOfEarliestTextMessage_CLF()
//Now that MP and SP have distinct portions of the global text message array to work with, we need to define the array checking limiters.

    INT TextArray_StartIndex, TextArray_EndIndex
    IF g_bInMultiplayer
        TextArray_StartIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch
        TextArray_EndIndex = (MAX_TEXT_MESSAGES - 2) //safe to check until end of array minus dummy positions reserved for time comparison checks.
    ELSE //Singleplayer
        TextArray_StartIndex = 0    //SP portion of text message array starts at beginning, i.e zero.
        TextArray_EndIndex = SP_TEXT_PORTION_END    //same as MP_TEXT_PORTION_START, but due to use of WHILE loop and "less than" comparison this position will never be filled
                                                    //by single player array.               
    ENDIF

    INT TempArrayIndex = TextArray_StartIndex

    TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                                                                         //will definitely be "older"

    WHILE TempArrayIndex < (TextArray_EndIndex)                                                                    
        IF NOT IsFirstTimeNewerThanSecondTime (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgTimeSent,  g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent)
            //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.
            TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
        ENDIF
        TempArrayIndex ++
    ENDWHILE
    //Unlock the earliest found text message, so that it can be written over next cycle.
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED

ENDPROC
#endif

#if USE_NRM_DLC
PROC ForceDeletionOfEarliestTextMessage_NRM()
//Now that MP and SP have distinct portions of the global text message array to work with, we need to define the array checking limiters.

    INT TextArray_StartIndex, TextArray_EndIndex
    IF g_bInMultiplayer
        TextArray_StartIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch
        TextArray_EndIndex = (MAX_TEXT_MESSAGES - 2) //safe to check until end of array minus dummy positions reserved for time comparison checks.
    ELSE //Singleplayer
        TextArray_StartIndex = 0    //SP portion of text message array starts at beginning, i.e zero.
        TextArray_EndIndex = SP_TEXT_PORTION_END    //same as MP_TEXT_PORTION_START, but due to use of WHILE loop and "less than" comparison this position will never be filled
                                                    //by single player array.               
    ENDIF

    INT TempArrayIndex = TextArray_StartIndex

    TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                                                                         //will definitely be "older"

    WHILE TempArrayIndex < (TextArray_EndIndex)                                                                    
        IF NOT IsFirstTimeNewerThanSecondTime (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgTimeSent,  g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent)
            //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.
            TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
        ENDIF
        TempArrayIndex ++
    ENDWHILE
    //Unlock the earliest found text message, so that it can be written over next cycle.
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED

ENDPROC
#endif

PROC ForceDeletionOfEarliestTextMessage() //This will identify the earliest locked message and remove the lock. Unlikely to ever be needed but just in case...
#if USE_CLF_DLC
    ForceDeletionOfEarliestTextMessage_CLF()
#endif
#if USE_NRM_DLC
    ForceDeletionOfEarliestTextMessage_NRM()
#endif

#if not USE_SP_DLC
    //Now that MP and SP have distinct portions of the global text message array to work with, we need to define the array checking limiters.

    INT TextArray_StartIndex, TextArray_EndIndex

    IF g_bInMultiplayer

        TextArray_StartIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch
        TextArray_EndIndex = (MAX_TEXT_MESSAGES - 2) //safe to check until end of array minus dummy positions reserved for time comparison checks.

    ELSE //Singleplayer

        TextArray_StartIndex = 0    //SP portion of text message array starts at beginning, i.e zero.
        TextArray_EndIndex = SP_TEXT_PORTION_END    //same as MP_TEXT_PORTION_START, but due to use of WHILE loop and "less than" comparison this position will never be filled
                                                    //by single player array.               

    ENDIF

    INT TempArrayIndex = TextArray_StartIndex

    TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                                                                         //will definitely be "older"

    WHILE TempArrayIndex < (TextArray_EndIndex)                                                                          

        IF NOT IsFirstTimeNewerThanSecondTime (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgTimeSent,  g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent)

            //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.

            TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex

            /*
            #IF IS_DEBUG_BUILD

                cdPrintstring ("Earliest available index identified at ")
                cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                cdPrintnl()

            #endif
            */

        ENDIF

        TempArrayIndex ++

    ENDWHILE

    //Unlock the earliest found text message, so that it can be written over next cycle.
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
#endif
ENDPROC

PROC ForceDeletionOfCompletedSidetask()

    //May need to flesh these out once limits are decided. This would only need to be done if we all sidetasks in the live array  were set to LOCKED.

ENDPROC

#if USE_CLF_DLC
FUNC BOOL CheckForTextMessageSpace_CLF()
    
    INT TextArray_StartIndex, TextArray_EndIndex
    IF g_bInMultiplayer
        TextArray_StartIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch
        TextArray_EndIndex = (MAX_TEXT_MESSAGES - 2) //safe to check until end of array minus dummy positions reserved for time comparison checks.
    ELSE //Singleplayer
        TextArray_StartIndex = 0    //SP portion of text message array starts at beginning, i.e zero.
        TextArray_EndIndex = SP_TEXT_PORTION_END    //same as MP_TEXT_PORTION_START, but due to use of WHILE loop and "less than" comparison this position will never be filled
                                                    //by single player array.    
    ENDIF

    INT TempArrayIndex = TextArray_StartIndex
    WHILE TempArrayIndex < (TextArray_EndIndex)   
        IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
            TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - Identified text message empty position at ")
                cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                cdPrintnl()
            #endif
            RETURN TRUE 
        ENDIF
        TempArrayIndex ++
    ENDWHILE
    
    //If the preceding section has not returned TRUE by this point, that means that every position in the text message array has been used at least once. We must now check for 
    //any unlocked positions to overwrite.
    TempArrayIndex = TextArray_StartIndex
    TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                              //will definitely be "older"
    WHILE TempArrayIndex < (TextArray_EndIndex)    
        IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
        OR g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED //This array position is unlocked. That means it is the secondary suitable candidate to store a new message.
            IF NOT IsFirstTimeNewerThanSecondTime (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgTimeSent,  g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent)
                //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.
                TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
            ENDIF
        ENDIF
        TempArrayIndex ++
    ENDWHILE

    //If no slot has been identified then there can be no space - and every text message in the array must have locked status!
    IF TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //As the TxtMsgIdentifiedFreeArrayIndex is still equal to the dummy value no free position could have been found. 
        RETURN FALSE
    ELSE
        #IF IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - Preparing to overwrite previously written text message array index. Setting all SP characters to FALSE presence.")
            cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
            cdPrintnl()
        #endif
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[0] = FALSE
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[1] = FALSE
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[2] = FALSE
        RETURN TRUE
    ENDIF
ENDFUNC
#endif

#if USE_NRM_DLC
FUNC BOOL CheckForTextMessageSpace_NRM()
    
    INT TextArray_StartIndex, TextArray_EndIndex
    IF g_bInMultiplayer
        TextArray_StartIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch
        TextArray_EndIndex = (MAX_TEXT_MESSAGES - 2) //safe to check until end of array minus dummy positions reserved for time comparison checks.
    ELSE //Singleplayer
        TextArray_StartIndex = 0    //SP portion of text message array starts at beginning, i.e zero.
        TextArray_EndIndex = SP_TEXT_PORTION_END    //same as MP_TEXT_PORTION_START, but due to use of WHILE loop and "less than" comparison this position will never be filled
                                                    //by single player array.    
    ENDIF

    INT TempArrayIndex = TextArray_StartIndex
    WHILE TempArrayIndex < (TextArray_EndIndex)   
        IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
            TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - Identified text message empty position at ")
                cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                cdPrintnl()
            #endif
            RETURN TRUE 
        ENDIF
        TempArrayIndex ++
    ENDWHILE

    //If the preceding section has not returned TRUE by this point, that means that every position in the text message array has been used at least once. We must now check for 
    //any unlocked positions to overwrite.

    TempArrayIndex = TextArray_StartIndex
    TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                              //will definitely be "older"
    WHILE TempArrayIndex < (TextArray_EndIndex)    
        IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
        OR g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED //This array position is unlocked. That means it is the secondary suitable candidate to store a new message.
            IF NOT IsFirstTimeNewerThanSecondTime (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgTimeSent,  g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent)
                //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.
                TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
            ENDIF
        ENDIF
        TempArrayIndex ++
    ENDWHILE

    //If no slot has been identified then there can be no space - and every text message in the array must have locked status!
    IF TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //As the TxtMsgIdentifiedFreeArrayIndex is still equal to the dummy value no free position could have been found. 
        RETURN FALSE
    ELSE
        #IF IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - Preparing to overwrite previously written text message array index. Setting all SP characters to FALSE presence.")
            cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
            cdPrintnl()
        #endif
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[0] = FALSE
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[1] = FALSE
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[2] = FALSE
        RETURN TRUE
    ENDIF
ENDFUNC
#endif

FUNC BOOL CheckForTextMessageSpace()
#if USE_CLF_DLC
    return CheckForTextMessageSpace_CLF()
#endif
#if USE_NRM_DLC
    return CheckForTextMessageSpace_NRM()
#endif
#if not USE_SP_DLC
    //TxtMsgIdentifiedFreeArrayIndex = NumberOfTextMessagesCurrentlyStored

    //Now that MP and SP have distinct portions of the global text message array to work with, we need to define the array checking limiters.

    INT TextArray_StartIndex, TextArray_EndIndex
    IF g_bInMultiplayer
        TextArray_StartIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch
        TextArray_EndIndex = (MAX_TEXT_MESSAGES - 2) //safe to check until end of array minus dummy positions reserved for time comparison checks.
    ELSE //Singleplayer
        TextArray_StartIndex = 0    //SP portion of text message array starts at beginning, i.e zero.
        TextArray_EndIndex = SP_TEXT_PORTION_END    //same as MP_TEXT_PORTION_START, but due to use of WHILE loop and "less than" comparison this position will never be filled
                                                    //by single player array.    
    ENDIF

    INT TempArrayIndex = TextArray_StartIndex
    WHILE TempArrayIndex < (TextArray_EndIndex)   
        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
            TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - Identified text message empty position at ")
                cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                cdPrintnl()
            #endif
            RETURN TRUE 
        ENDIF
        TempArrayIndex ++
    ENDWHILE

    //If the preceding section has not returned TRUE by this point, that means that every position in the text message array has been used at least once. We must now check for 
    //any unlocked positions to overwrite.

    TempArrayIndex = TextArray_StartIndex
    TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                              //will definitely be "older"
    WHILE TempArrayIndex < (TextArray_EndIndex)    
        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
        OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED //This array position is unlocked. That means it is the secondary suitable candidate to store a new message.
            IF NOT IsFirstTimeNewerThanSecondTime (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgTimeSent,  g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent)
                //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.
                TxtMsgIdentifiedFreeArrayIndex = TempArrayIndex
            ENDIF
        ENDIF
		
		IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED		
				#IF IS_DEBUG_BUILD
					cdPrintstring ("CELLPHONE_PRIV 2018 - CheckForTextMessageSpace() Found locked message with label at array index:  ")
					cdPrintstring(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TempArrayIndex].TxtMsgLabel) 
	                cdPrintint (TempArrayIndex)
	                cdPrintnl()
				#endif
		ENDIF
		
        TempArrayIndex ++
    ENDWHILE

        //If no slot has been identified then there can be no space - and every text message in the array must have locked status!
        IF TxtMsgIdentifiedFreeArrayIndex = (MAX_TEXT_MESSAGES - 1) //As the TxtMsgIdentifiedFreeArrayIndex is still equal to the dummy value no free position could have been found. 
            RETURN FALSE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - Preparing to overwrite previously written text message array index. Setting all SP characters to FALSE presence.")
                cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                cdPrintnl()
            #endif
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[0] = FALSE
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[1] = FALSE
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[2] = FALSE
            RETURN TRUE
        ENDIF
#endif  
ENDFUNC

FUNC BOOL CheckForSideTaskSpace()

    //TxtMsgIdentifiedFreeArrayIndex = NumberOfTextMessagesCurrentlyStored

    INT TempArrayIndex = 0

    WHILE TempArrayIndex < (MAX_NUM_SIDETASKS - 2)                                                                          

        IF g_LiveSideTask[TempArrayIndex].SideTaskLockStatus = SIDETASK_EMPTY

                SideTaskIdentifiedFreeArrayIndex = TempArrayIndex

                #IF IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - Identified empty sidetask slot at ")
                    cdPrintint (SideTaskIdentifiedFreeArrayIndex)
                    cdPrintnl()

                #endif

                RETURN TRUE                                           

        ENDIF

        TempArrayIndex ++

    ENDWHILE

        //If the preceding section has not returned TRUE by this point, that means that every position in the live side task array has been used at least once. We must now check for 
        //any unlocked positions to overwrite.

        TempArrayIndex = 0

        SideTaskIdentifiedFreeArrayIndex = (MAX_NUM_SIDETASKS - 1) //Assign the TxtMsgIdentifiedFreeArrayIndex to the dummy value.

        //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
        g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs = -1 
        g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins = 0
        g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours = 0
        g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay = 0
        g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                                  //will definitely be "older"

        WHILE TempArrayIndex < (MAX_NUM_SIDETASKS - 2)                                                                          

            IF g_LiveSideTask[TempArrayIndex].SideTaskLockStatus = SIDETASK_EMPTY
            OR g_LiveSideTask[TempArrayIndex].SideTaskLockStatus = SIDETASK_UNLOCKED //This array position is unlocked. That means it is the secondary suitable candidate to store a new sidetask.

                IF NOT IsFirstTimeNewerThanSecondTime (g_LiveSideTask[TempArrayIndex].TxtMsgTimeSent,  g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].TxtMsgTimeSent)

                    //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.

                    SideTaskIdentifiedFreeArrayIndex = TempArrayIndex

                    /*
                    #IF IS_DEBUG_BUILD

                        cdPrintstring ("Earliest available index identified at ")
                        cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                        cdPrintnl()

                    #endif
                    */

                ENDIF

            ENDIF

            TempArrayIndex ++

        ENDWHILE

        //If no slot has been identified then there can be no space - and every sidetask in the live array must have locked status!
        IF SideTaskIdentifiedFreeArrayIndex = (MAX_NUM_SIDETASKS - 1) //As the SideTaskIdentifiedFreeArrayIndex is still equal to the dummy value no free position could have been found. 

            RETURN FALSE

        ELSE

            #IF IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Preparing to overwrite previously written sidetask live array index ")
                cdPrintint (SideTaskIdentifiedFreeArrayIndex)
                cdPrintnl()

            #endif

            //No incrementation required as we are overwriting a slot.

            RETURN TRUE

        ENDIF

ENDFUNC

//Will consolidate these three procs if we do go ahead with sidetask stuff...

PROC SetTimeThatTextWasSent (INT WhichArraySlotNumber)

    INT TempSecs, TempMins, TempHours
    INT TempDay,  TempMonth, TempYear

    TempSecs = GET_CLOCK_SECONDS()
    TempMins = GET_CLOCK_MINUTES() //sic. This is actually spelled like that in COMMANDS_CLOCK.sch
    TempHours = GET_CLOCK_HOURS()

    TempDay = GET_CLOCK_DAY_OF_MONTH()

    TempMonth = (ENUM_TO_INT(GET_CLOCK_MONTH()) + 1) //January returns zero. Need to increment returned month for display purposes.

    TempYear = GET_CLOCK_YEAR()
    #if USE_CLF_DLC
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgSecs = TempSecs
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMins = TempMins
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgHours = TempHours
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgDay = TempDay
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMonth = TempMonth
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgYear = TempYear
    #endif
    #if USE_NRM_DLC
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgSecs = TempSecs
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMins = TempMins
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgHours = TempHours
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgDay = TempDay
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMonth = TempMonth
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgYear = TempYear
    #endif
    #if not USE_SP_DLC
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgSecs = TempSecs
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMins = TempMins
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgHours = TempHours
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgDay = TempDay
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMonth = TempMonth
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgYear = TempYear
    #endif
ENDPROC

PROC SetTimeSideTaskWentLive (INT WhichArraySlotNumber)

    INT TempSecs, TempMins, TempHours
    INT TempDay,  TempMonth, TempYear

    TempSecs = GET_CLOCK_SECONDS()
    TempMins = GET_CLOCK_MINUTES() //sic. This is actually spelled like that in COMMANDS_CLOCK.sch
    TempHours = GET_CLOCK_HOURS()

    TempDay = GET_CLOCK_DAY_OF_MONTH()

    TempMonth = (ENUM_TO_INT(GET_CLOCK_MONTH()) + 1) //January returns zero. Need to increment returned month for display purposes.

    TempYear = GET_CLOCK_YEAR()

    g_LiveSideTask[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgSecs = TempSecs
    g_LiveSideTask[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMins = TempMins
    g_LiveSidetask[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgHours = TempHours

    g_LiveSideTask[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgDay = TempDay
    g_LiveSideTask[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgMonth = TempMonth
    g_LiveSideTask[WhichArraySlotNumber].TxtMsgTimeSent.TxtMsgYear = TempYear

ENDPROC

#if USE_CLF_DLC
FUNC BOOL ProcessTextMessage_CLF ( enumCharacterList WhichSender, STRING WhichTextLabel, enumTxtMsgLockedStatus WhichLockStatus, 

                                enumTxtMsgSpecialComponents WhichComponentType,
                                STRING WhichBodyStringComponent, STRING WhichSenderStringComponent, 
                                INT WhichNumberComponent = -99, 

                                enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_DO_NOT_AUTO_UNLOCK,

                                enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,
                                enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER,
                                INT WhichNumberofAdditionalStrings = 0,
                                STRING WhichSecondString = NULL, 
                                STRING WhichThirdString = NULL,
                                enumRecipientList WhichRecipient = NO_SILENT_RECIPIENT)
//Unref hack. 

    IF WhichNumberOfAdditionalStrings > 99
    ENDIF

    IF ARE_STRINGS_EQUAL (WhichSecondString, WhichThirdString)
    ENDIF

    Get_Cellphone_Owner()

    BOOL Bypass_Checks_For_Silent_Recipient = FALSE
    SWITCH WhichRecipient
        CASE SILENT_MICHAEL
            IF g_Cellphone.PhoneOwner = CHAR_MICHAEL 
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            ELSE  
                Bypass_Checks_For_Silent_Recipient = TRUE
            ENDIF
        BREAK

        CASE SILENT_TREVOR
            IF g_Cellphone.PhoneOwner = CHAR_TREVOR 
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            ELSE 
                Bypass_Checks_For_Silent_Recipient = TRUE
            ENDIF
        BREAK

        CASE SILENT_FRANKLIN
            IF g_Cellphone.PhoneOwner = CHAR_FRANKLIN  
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            ELSE                
                Bypass_Checks_For_Silent_Recipient = TRUE
            ENDIF
        BREAK

        DEFAULT 
            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PRIVATE - No silent recipient. Not bypassing send checks!")   
                cdPrintnl()
            #endif
            Bypass_Checks_For_Silent_Recipient = FALSE
        BREAK
    ENDSWITCH

    IF Bypass_Checks_For_Silent_Recipient = FALSE
        //For bug #1162976, do not allow text message send while player is swimming underwater. Trial basis.
        IF IS_PLAYER_PLAYING (PLAYER_ID()) 
           IF IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Not allowing text message to be sent while player_ped is swimming underwater. #1162976")   
                    cdPrintnl()
                #endif
                RETURN FALSE
            ENDIF
        ENDIF

        IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
            #if IS_DEBUG_BUILD 
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - Previously sent critical text message for this owner has not been read! Can't send new text message!")
                cdPrintnl ()
            #endif
            RETURN FALSE
        ENDIF        
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appTextMessCLF")) > 0
            #if IS_DEBUG_BUILD
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - CLF Text message app currently onscreen!")
                cdPrintnl ()
            #endif
            RETURN FALSE
        ENDIF
        //Don't allow texts to be sent when player is running text app.
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessCLF")) > 0
            #IF IS_DEBUG_BUILD
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - appTextMessCLF is currently running! Can't send new text message! See Steve T if you really need to do this.")
                cdPrintnl ()
            #endif
            RETURN FALSE
        ENDIF        
    ENDIF

    IF CheckForTextMessageSpace() = FALSE   
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PRIV - Warning! Text Message System full with locked messages - Let Steve T know.")
            cdPrintstring ("CELLPHONE_PRIV - No more space left for further text messages - all messages locked!")
            cdPrintnl ()
            cdPrintstring ("CELLPHONE_PRIV - Unlocking oldest locked message to free space for next cycle.")
            cdPrintnl ()
        #endif

        ForceDeletionOfEarliestTextMessage() 
        RETURN FALSE //Return false this cycle but should be able to accept text message next cycle as space has been freed.

    ELSE

        SetTimeThatTextWasSent (TxtMsgIdentifiedFreeArrayIndex)     //As we have decided there's enough space to add another text message, we 
                                                                    //store the text message's struct contents in the text message array.

        //Removed on trial basis. Text messages are using global text rather than a text block in the interim.
        //g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtBlockToLoad = WhichBlockOfTextToLoad

        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLabel = WhichTextLabel
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender = WhichSender

        IF WhichLockStatus = TXTMSG_EMPTY
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - When sending, you cannot define a text message lock status as empty. See Steve T")
                cdPrintnl ()
                RETURN FALSE
            #endif
        ELSE
            g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLockStatus = WhichLockStatus
        ENDIF

        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgCritical = WhichCriticalStatus
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgAutoUnlockStatus = WhichAutoUnlockStatus
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgReplyStatus = WhichReplyStatus
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgCanCallSenderStatus = WhichCanCallSenderStatus
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgBarterStatus = WhichBarterStatus
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = WhichComponentType
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgStringComponent = WhichBodyStringComponent
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberComponent = WhichNumberComponent
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSenderStringComponent = WhichSenderStringComponent
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = WhichNumberofAdditionalStrings
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent = WhichSecondString
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent = WhichThirdString

             #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - Additional number of strings is ")
                cdPrintint (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings)
                cdPrintnl()
                cdPrintstring(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                cdPrintnl()
                cdPrintstring(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                cdPrintnl()
            #endif

        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS)

            g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[0] = TRUE
            g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[1] = TRUE
            g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[2] = TRUE

            //Do not include MP character in here! It would mean that an mp purge could get rid of single player messages also. The mp filter should remain distinct.
            g_LastTextSentTo = WAS_SENT_TO_EVERY_SP_CHARACTER
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to all singleplayer characters.")
                cdPrintnl()
            #endif
            Perform_cp_StockMarketUpdate(CHAR_MICHAEL)
            Perform_cp_StockMarketUpdate(CHAR_TREVOR)
            Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)
        ELSE

            Get_Cellphone_Owner()
            SWITCH WhichRecipient
                CASE NO_SILENT_RECIPIENT
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message has no silent recipient - sent to current player.")
                        cdPrintnl()
                    #endif
                BREAK
                CASE SILENT_MICHAEL
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as MICHAEL")
                        cdPrintnl()
                    #endif
                BREAK
                CASE SILENT_TREVOR
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_TREVOR] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as TREVOR")
                        cdPrintnl()
                    #endif
                BREAK
                CASE SILENT_FRANKLIN
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_FRANKLIN] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as FRANKLIN")
                        cdPrintnl()
                    #endif
                BREAK
            ENDSWITCH
            //Would need to add options to allow character.
            //g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE

            IF WhichRecipient = NO_SILENT_RECIPIENT //Only update this is message is explicitly sent.
                SWITCH ENUM_TO_INT (g_Cellphone.PhoneOwner)
                    CASE 0
                        Perform_cp_StockMarketUpdate(CHAR_MICHAEL)
                        g_LastTextSentTo = WAS_SENT_TO_MICHAEL
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Michael.")
                            cdPrintnl()
                        #endif
                    BREAK
                    CASE 1
                        Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)
                        g_LastTextSentTo = WAS_SENT_TO_FRANKLIN
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Franklin.")
                            cdPrintnl()
                        #endif
                    BREAK
                    CASE 2
                        Perform_cp_StockMarketUpdate(CHAR_TREVOR)
                        g_LastTextSentTo = WAS_SENT_TO_TREVOR
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Trevor.")
                            cdPrintnl()
                        #endif
                    BREAK
                    //Added to take care of bug 282407...
                    CASE 3
                        Perform_cp_StockMarketUpdate(CHAR_MULTIPLAYER)
                        g_LastTextSentTo = WAS_SENT_TO_MULTIPLAYER
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to multiplayer character.")
                            cdPrintnl()
                        #endif
                    BREAK
                    DEFAULT
                        g_LastTextSentTo = WAS_SENT_TO_EVERY_SP_CHARACTER
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Text message has been sent but not sure who to!")
                            cdPrintnl()
                        #endif
                    BREAK
                ENDSWITCH  
            ENDIF
        ENDIF

        IF WhichCriticalStatus = TXTMSG_CRITICAL

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) //singleplayer characters

                //These are set to false by bringing up phone and apptextmessage launching...
                g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = TRUE
                g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = TRUE
                g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = TRUE

            ELSE

                SWITCH WhichRecipient

                    CASE NO_SILENT_RECIPIENT
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(g_Cellphone.PhoneOwner)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had no silent recipient, marking MustBeRead for current owner.")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_MICHAEL
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_MICHAEL)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_MICHAEL as the recipient, marking MustBeRead for him.")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_TREVOR
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_TREVOR)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_TREVOR as the recipient, marking MustBeRead for him.")
                            cdPrintnl()
                        #endif
                    BREAK

                    CASE SILENT_FRANKLIN
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_FRANKLIN)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_FRANKLIN as the recipient, marking MustBeRead for him.")
                            cdPrintnl()
                        #endif
                    BREAK
                ENDSWITCH
            ENDIF
        ENDIF

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - Added text message into array index at position ")
            cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
            cdPrintnl ()
            cdPrintstring("Time Stamp Details")
            cdPrintnl()
            cdPrintstring("Seconds: ")
            cdPrintint(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs) 
            cdPrintnl()
            cdPrintstring("Minutes: ")
            cdPrintint(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins) 
            cdPrintnl()
            cdPrintstring("Hours: ")
            cdPrintint(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours) 
            cdPrintnl()
            cdPrintstring("Day: ")
            cdPrintint(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay) 
            cdPrintnl()
            cdPrintstring("Month: ")
            cdPrintint(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMonth) 
            cdPrintnl()
            cdPrintstring("Year: ")
            cdPrintint(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear) 
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PRIV - Clearing headshot string store before feed processing.")
        #endif

        //Make sure that by default we don't associate a headshot ID with any new text message. If one is required then this will be created and reassigned when the
        //feed entry is constructed. Only MP is required to use user generated headshots so they should always require feed entries.

        //Default associated headshotID to NULL
        g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = NULL
        //PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", "Phone_SoundSet_Michael")
        IF ShouldDisplayNewTxtMsgSignifier
            //Moved to individual text message public functions in cellphone public so we can be certain signifier display data is placed before updating the movie.
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)
            Get_Cellphone_Owner()
            //Also need to declare soundsets here just in case phone hasn't already been brought up and a text comes in. See bug 427977 .
            IF g_Use_Prologue_Cellphone
                g_Owner_Soundset = "Phone_SoundSet_Prologue"
            ELSE
                 SWITCH g_Cellphone.PhoneOwner 
                    //Mobile_5 clause would go in here.
                    CASE CHAR_MICHAEL  
                        g_Owner_Soundset = "Phone_SoundSet_Michael"
                    BREAK
                    CASE CHAR_TREVOR        
                        g_Owner_Soundset = "Phone_SoundSet_Trevor"
                    BREAK
                    CASE CHAR_FRANKLIN
                        g_Owner_Soundset = "Phone_SoundSet_Franklin"
                    BREAK
                    DEFAULT
                        g_Owner_Soundset = "Phone_SoundSet_Default"
                    BREAK
                ENDSWITCH 

            ENDIF

            //Don't play text reception tone if player is in "quiet" mode.
            IF NOT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_QUIET_MODE))
                #IF USE_TU_CHANGES
                    IF NOT IS_TRANSITION_ACTIVE()  //#1645435
                #ENDIF
                    PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", g_Owner_Soundset)
                #IF USE_TU_CHANGES
                    ENDIF
                #ENDIF
            ENDIF

            //SET_BIT (BitSet_CellphoneDisplay, g_BS_DO_TEXT_MESSAGE_PREVIEW)
        ENDIF

        IF NOT g_Homescreen_In_Secondary_Mode
            IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update number of texts notifications when user is on first homescreen page.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
                Scaleform_Place_Items_on_Homescreen(1)
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))
            ENDIF
        ENDIF 

        RETURN TRUE
    ENDIF                               
ENDFUNC                             
#endif

#if USE_NRM_DLC
FUNC BOOL ProcessTextMessage_NRM ( enumCharacterList WhichSender, STRING WhichTextLabel, enumTxtMsgLockedStatus WhichLockStatus, 

                                enumTxtMsgSpecialComponents WhichComponentType,
                                STRING WhichBodyStringComponent, STRING WhichSenderStringComponent, 
                                INT WhichNumberComponent = -99, 

                                enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_DO_NOT_AUTO_UNLOCK,

                                enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,
                                enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER,
                                INT WhichNumberofAdditionalStrings = 0,
                                STRING WhichSecondString = NULL, 
                                STRING WhichThirdString = NULL,
                                enumRecipientList WhichRecipient = NO_SILENT_RECIPIENT)
    //Unref hack. 

    IF WhichNumberOfAdditionalStrings > 99
    ENDIF

    IF ARE_STRINGS_EQUAL (WhichSecondString, WhichThirdString)
    ENDIF

    Get_Cellphone_Owner()

    BOOL Bypass_Checks_For_Silent_Recipient = FALSE
    SWITCH WhichRecipient
        CASE SILENT_MICHAEL
            IF g_Cellphone.PhoneOwner = CHAR_MICHAEL 
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            ELSE  
                Bypass_Checks_For_Silent_Recipient = TRUE
            ENDIF
        BREAK

        CASE SILENT_TREVOR
            IF g_Cellphone.PhoneOwner = CHAR_TREVOR 
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            ELSE 
                Bypass_Checks_For_Silent_Recipient = TRUE
            ENDIF
        BREAK

        CASE SILENT_FRANKLIN
            IF g_Cellphone.PhoneOwner = CHAR_FRANKLIN  
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            ELSE                
                Bypass_Checks_For_Silent_Recipient = TRUE
            ENDIF
        BREAK

        DEFAULT 
            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PRIVATE - No silent recipient. Not bypassing send checks!")   
                cdPrintnl()
            #endif
            Bypass_Checks_For_Silent_Recipient = FALSE
        BREAK
    ENDSWITCH

    IF Bypass_Checks_For_Silent_Recipient = FALSE
        //For bug #1162976, do not allow text message send while player is swimming underwater. Trial basis.
        IF IS_PLAYER_PLAYING (PLAYER_ID()) 
           IF IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - Not allowing text message to be sent while player_ped is swimming underwater. #1162976")   
                    cdPrintnl()
                #endif
                RETURN FALSE
            ENDIF
        ENDIF

        IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
            #if IS_DEBUG_BUILD 
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - Previously sent critical text message for this owner has not been read! Can't send new text message!")
                cdPrintnl ()
            #endif
            RETURN FALSE
        ENDIF

        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appTextMessNRM")) > 0
            #if IS_DEBUG_BUILD
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - NRM Text message app currently onscreen!")
                cdPrintnl ()
            #endif
            RETURN FALSE
        ENDIF
        //Don't allow texts to be sent when player is running text app.
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessNRM")) > 0
            #IF IS_DEBUG_BUILD
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - appTextMessNRM is currently running! Can't send new text message! See Steve T if you really need to do this.")
                cdPrintnl ()
            #endif
            RETURN FALSE
        ENDIF

    ENDIF

    IF CheckForTextMessageSpace() = FALSE   //From cellphone_private.sch, we need to check the text message array to make
                                            //sure we have enough space. If not, we make space where possible. If that 
                                            //fails, which it shouldn't... assert!

        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PRIV - Warning! Text Message System full with locked messages - Let Steve T know.")
            cdPrintstring ("CELLPHONE_PRIV - No more space left for further text messages - all messages locked!")
            cdPrintnl ()
            cdPrintstring ("CELLPHONE_PRIV - Unlocking oldest locked message to free space for next cycle.")
            cdPrintnl ()
        #endif
        ForceDeletionOfEarliestTextMessage() 
        RETURN FALSE //Return false this cycle but should be able to accept text message next cycle as space has been freed.
    ELSE

        SetTimeThatTextWasSent (TxtMsgIdentifiedFreeArrayIndex)     //As we have decided there's enough space to add another text message, we 
                                                                    //store the text message's struct contents in the text message array.
        //Removed on trial basis. Text messages are using global text rather than a text block in the interim.
        //g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtBlockToLoad = WhichBlockOfTextToLoad

        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLabel = WhichTextLabel
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender = WhichSender

        IF WhichLockStatus = TXTMSG_EMPTY

            #IF IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - When sending, you cannot define a text message lock status as empty. See Steve T")
                cdPrintnl ()

                RETURN FALSE

            #endif

        ELSE

            g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLockStatus = WhichLockStatus

        ENDIF

        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgCritical = WhichCriticalStatus
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgAutoUnlockStatus = WhichAutoUnlockStatus
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgReplyStatus = WhichReplyStatus
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgCanCallSenderStatus = WhichCanCallSenderStatus
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgBarterStatus = WhichBarterStatus
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = WhichComponentType
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgStringComponent = WhichBodyStringComponent
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberComponent = WhichNumberComponent
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSenderStringComponent = WhichSenderStringComponent
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = WhichNumberofAdditionalStrings
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent = WhichSecondString
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent = WhichThirdString

             #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PRIV - Additional number of strings is ")
                cdPrintint (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings)
                cdPrintnl()
                cdPrintstring(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                cdPrintnl()
                cdPrintstring(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                cdPrintnl()
            #endif

        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS)

            g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[0] = TRUE
            g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[1] = TRUE
            g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[2] = TRUE

            //Do not include MP character in here! It would mean that an mp purge could get rid of single player messages also. The mp filter should remain distinct.
            g_LastTextSentTo = WAS_SENT_TO_EVERY_SP_CHARACTER
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to all singleplayer characters.")
                cdPrintnl()
            #endif
            Perform_cp_StockMarketUpdate(CHAR_MICHAEL)
            Perform_cp_StockMarketUpdate(CHAR_TREVOR)
            Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)
        ELSE

            Get_Cellphone_Owner()
            SWITCH WhichRecipient
                CASE NO_SILENT_RECIPIENT
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message has no silent recipient - sent to current player.")
                        cdPrintnl()
                    #endif
                BREAK
                CASE SILENT_MICHAEL
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as MICHAEL")
                        cdPrintnl()
                    #endif
                BREAK
                CASE SILENT_TREVOR
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_TREVOR] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as TREVOR")
                        cdPrintnl()
                    #endif
                BREAK
                CASE SILENT_FRANKLIN
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_FRANKLIN] = TRUE
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as FRANKLIN")
                        cdPrintnl()
                    #endif
                BREAK
            ENDSWITCH
            //Would need to add options to allow character.
            //g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE
            IF WhichRecipient = NO_SILENT_RECIPIENT //Only update this is message is explicitly sent.
                SWITCH ENUM_TO_INT (g_Cellphone.PhoneOwner)
                    CASE 0
                        Perform_cp_StockMarketUpdate(CHAR_MICHAEL)
                        g_LastTextSentTo = WAS_SENT_TO_MICHAEL
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Michael.")
                            cdPrintnl()
                        #endif
                    BREAK

                    CASE 1
                        Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)
                        g_LastTextSentTo = WAS_SENT_TO_FRANKLIN
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Franklin.")
                            cdPrintnl()
                        #endif
                    BREAK

                    CASE 2
                        Perform_cp_StockMarketUpdate(CHAR_TREVOR)
                        g_LastTextSentTo = WAS_SENT_TO_TREVOR
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Trevor.")
                            cdPrintnl()
                        #endif
                    BREAK

                    //Added to take care of bug 282407...
                    CASE 3
                        Perform_cp_StockMarketUpdate(CHAR_MULTIPLAYER)
                        g_LastTextSentTo = WAS_SENT_TO_MULTIPLAYER
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to multiplayer character.")
                            cdPrintnl()
                        #endif
                    BREAK
                    DEFAULT
                        g_LastTextSentTo = WAS_SENT_TO_EVERY_SP_CHARACTER
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring ("CELLPHONE_PRIV - Text message has been sent but not sure who to!")
                            cdPrintnl()
                        #endif
                    BREAK
                ENDSWITCH  
            ENDIF

        ENDIF

        IF WhichCriticalStatus = TXTMSG_CRITICAL
            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) //singleplayer characters
                //These are set to false by bringing up phone and apptextmessage launching...
                g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = TRUE
                g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = TRUE
                g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = TRUE
            ELSE
                SWITCH WhichRecipient
                    CASE NO_SILENT_RECIPIENT
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(g_Cellphone.PhoneOwner)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had no silent recipient, marking MustBeRead for current owner.")
                            cdPrintnl()                        #endif
                    BREAK
                    CASE SILENT_MICHAEL
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_MICHAEL)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_MICHAEL as the recipient, marking MustBeRead for him.")
                            cdPrintnl()
                        #endif
                    BREAK
                    CASE SILENT_TREVOR
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_TREVOR)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_TREVOR as the recipient, marking MustBeRead for him.")
                            cdPrintnl()
                        #endif
                    BREAK
                    CASE SILENT_FRANKLIN
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_FRANKLIN)].g_LastMessageSentMustBeRead = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_FRANKLIN as the recipient, marking MustBeRead for him.")
                            cdPrintnl()
                        #endif
                    BREAK
                ENDSWITCH
            ENDIF

        ENDIF

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - Added text message into array index at position ")
            cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
            cdPrintnl ()
            cdPrintstring("Time Stamp Details")
            cdPrintnl()
            cdPrintstring("Seconds: ")
            cdPrintint(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs) 
            cdPrintnl()
            cdPrintstring("Minutes: ")
            cdPrintint(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins) 
            cdPrintnl()
            cdPrintstring("Hours: ")
            cdPrintint(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours) 
            cdPrintnl()
            cdPrintstring("Day: ")
            cdPrintint(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay) 
            cdPrintnl()
            cdPrintstring("Month: ")
            cdPrintint(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMonth) 
            cdPrintnl()
            cdPrintstring("Year: ")
            cdPrintint(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear) 
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PRIV - Clearing headshot string store before feed processing.")
        #endif

        //Make sure that by default we don't associate a headshot ID with any new text message. If one is required then this will be created and reassigned when the
        //feed entry is constructed. Only MP is required to use user generated headshots so they should always require feed entries.

        //Default associated headshotID to NULL
        g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = NULL

        //PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", "Phone_SoundSet_Michael")

        IF ShouldDisplayNewTxtMsgSignifier

            //Moved to individual text message public functions in cellphone public so we can be certain signifier display data is placed before updating the movie.
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

            Get_Cellphone_Owner()
            //Also need to declare soundsets here just in case phone hasn't already been brought up and a text comes in. See bug 427977 .
            IF g_Use_Prologue_Cellphone
                g_Owner_Soundset = "Phone_SoundSet_Prologue"
            ELSE
                SWITCH g_Cellphone.PhoneOwner 
                    //Mobile_5 clause would go in here.
                    CASE CHAR_MICHAEL  
                        g_Owner_Soundset = "Phone_SoundSet_Michael"
                    BREAK
                    CASE CHAR_TREVOR
                        g_Owner_Soundset = "Phone_SoundSet_Trevor"
                    BREAK
                    CASE CHAR_FRANKLIN
                        g_Owner_Soundset = "Phone_SoundSet_Franklin"
                    BREAK
                    DEFAULT
                        g_Owner_Soundset = "Phone_SoundSet_Default"
                    BREAK
                ENDSWITCH 
            ENDIF

            //Don't play text reception tone if player is in "quiet" mode.
            IF NOT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_QUIET_MODE))

                #IF USE_TU_CHANGES
                    IF NOT IS_TRANSITION_ACTIVE()  //#1645435
                #ENDIF
                    PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", g_Owner_Soundset)
                #IF USE_TU_CHANGES
                    ENDIF
                #ENDIF

            ENDIF
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_DO_TEXT_MESSAGE_PREVIEW)
        ENDIF
        IF NOT g_Homescreen_In_Secondary_Mode
            IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update number of texts notifications when user is on first homescreen page.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
                Scaleform_Place_Items_on_Homescreen(1)
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))
            ENDIF
        ENDIF 
        
        RETURN TRUE
    ENDIF                       
ENDFUNC 
#endif

FUNC BOOL ProcessTextMessage ( enumCharacterList WhichSender, STRING WhichTextLabel, enumTxtMsgLockedStatus WhichLockStatus, 

                                enumTxtMsgSpecialComponents WhichComponentType,
                                STRING WhichBodyStringComponent, STRING WhichSenderStringComponent, 
                                INT WhichNumberComponent = -99, 

                                enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_DO_NOT_AUTO_UNLOCK,

                                enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,
                                enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER,
                                INT WhichNumberofAdditionalStrings = 0,
                                STRING WhichSecondString = NULL, 
                                STRING WhichThirdString = NULL,
                                enumRecipientList WhichRecipient = NO_SILENT_RECIPIENT)

#if USE_CLF_DLC
return ProcessTextMessage_CLF (WhichSender, WhichTextLabel, WhichLockStatus,WhichComponentType,WhichBodyStringComponent,WhichSenderStringComponent, 
                                WhichNumberComponent,WhichCriticalStatus,WhichAutoUnlockStatus,WhichReplyStatus,ShouldDisplayNewTxtMsgSignifier,
                                WhichBarterStatus, WhichCanCallSenderStatus,WhichNumberofAdditionalStrings,WhichSecondString,WhichThirdString,
                                WhichRecipient)
#endif
#if USE_NRM_DLC
return ProcessTextMessage_NRM (WhichSender, WhichTextLabel, WhichLockStatus,WhichComponentType,WhichBodyStringComponent,WhichSenderStringComponent, 
                                WhichNumberComponent,WhichCriticalStatus,WhichAutoUnlockStatus,WhichReplyStatus,ShouldDisplayNewTxtMsgSignifier,
                                WhichBarterStatus, WhichCanCallSenderStatus,WhichNumberofAdditionalStrings,WhichSecondString,WhichThirdString,
                                WhichRecipient)
#endif
#if not USE_SP_DLC
        //Unref hack. 

        IF WhichNumberOfAdditionalStrings > 99
        ENDIF

        IF ARE_STRINGS_EQUAL (WhichSecondString, WhichThirdString)
        ENDIF

        Get_Cellphone_Owner()

        BOOL Bypass_Checks_For_Silent_Recipient = FALSE
        SWITCH WhichRecipient
            CASE SILENT_MICHAEL
                IF g_Cellphone.PhoneOwner = CHAR_MICHAEL 
                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                        cdPrintnl()
                    #endif
                    Bypass_Checks_For_Silent_Recipient = FALSE
                ELSE  
                    Bypass_Checks_For_Silent_Recipient = TRUE
                ENDIF
            BREAK

            CASE SILENT_TREVOR
                IF g_Cellphone.PhoneOwner = CHAR_TREVOR 
                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                        cdPrintnl()
                    #endif
                    Bypass_Checks_For_Silent_Recipient = FALSE
                ELSE 
                    Bypass_Checks_For_Silent_Recipient = TRUE
                ENDIF
            BREAK

            CASE SILENT_FRANKLIN
                IF g_Cellphone.PhoneOwner = CHAR_FRANKLIN  
                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                        cdPrintnl()
                    #endif
                    Bypass_Checks_For_Silent_Recipient = FALSE
                ELSE                
                    Bypass_Checks_For_Silent_Recipient = TRUE
                ENDIF
            BREAK

            DEFAULT 
                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PRIVATE - No silent recipient. Not bypassing send checks!")   
                    cdPrintnl()
                #endif
                Bypass_Checks_For_Silent_Recipient = FALSE
            BREAK
        ENDSWITCH

        IF Bypass_Checks_For_Silent_Recipient = FALSE
            //For bug #1162976, do not allow text message send while player is swimming underwater. Trial basis.
            IF IS_PLAYER_PLAYING (PLAYER_ID()) 
               IF IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())
                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PRIVATE - Not allowing text message to be sent while player_ped is swimming underwater. #1162976")   
                        cdPrintnl()
                    #endif
                    RETURN FALSE
                ENDIF
            ENDIF

            IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
                #if IS_DEBUG_BUILD 
                    cdPrintnl ()
                    cdPrintstring ("CELLPHONE_PRIV - Previously sent critical text message for this owner has not been read! Can't send new text message!")
                    cdPrintnl ()
                #endif
                RETURN FALSE
            ENDIF
            
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appTextMessage")) > 0
                #if IS_DEBUG_BUILD
                    cdPrintnl ()
                    cdPrintstring ("CELLPHONE_PRIV - Text message app currently onscreen!")
                    cdPrintnl ()
                #endif
                RETURN FALSE
            ENDIF
            //Don't allow texts to be sent when player is running text app.
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessage")) > 0
                #IF IS_DEBUG_BUILD
                    cdPrintnl ()
                    cdPrintstring ("CELLPHONE_PRIV - appTextMessage is currently running! Can't send new text message! See Steve T if you really need to do this.")
                    cdPrintnl ()
                #endif
                //If we allow this, I will need to put a check in for complex_app, single message display.
                RETURN FALSE
            ENDIF
         
        ENDIF

        IF CheckForTextMessageSpace() = FALSE   //From cellphone_private.sch, we need to check the text message array to make
                                                //sure we have enough space. If not, we make space where possible. If that 
                                                //fails, which it shouldn't... assert!

            #if IS_DEBUG_BUILD

                SCRIPT_ASSERT ("CELLPHONE_PRIV - Warning! Jan 2018. Text Message System full with locked messages - Let Steve T know.")
                cdPrintstring ("CELLPHONE_PRIV - No more space left for further text messages - all messages locked!")
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - Unlocking oldest locked message to free space for next cycle. ")
                cdPrintnl ()

            #endif

            ForceDeletionOfEarliestTextMessage() 

            RETURN FALSE //Return false this cycle but should be able to accept text message next cycle as space has been freed.

        ELSE

            SetTimeThatTextWasSent (TxtMsgIdentifiedFreeArrayIndex)     //As we have decided there's enough space to add another text message, we 
                                                                        //store the text message's struct contents in the text message array.

            //Removed on trial basis. Text messages are using global text rather than a text block in the interim.
            //g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtBlockToLoad = WhichBlockOfTextToLoad

            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLabel = WhichTextLabel
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender = WhichSender

            IF WhichLockStatus = TXTMSG_EMPTY

                #IF IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - When sending, you cannot define a text message lock status as empty. See Steve T")
                    cdPrintnl ()

                    RETURN FALSE

                #endif

            ELSE

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLockStatus = WhichLockStatus

            ENDIF

            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgCritical = WhichCriticalStatus
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgAutoUnlockStatus = WhichAutoUnlockStatus
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgReplyStatus = WhichReplyStatus
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgCanCallSenderStatus = WhichCanCallSenderStatus
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgBarterStatus = WhichBarterStatus
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = WhichComponentType
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgStringComponent = WhichBodyStringComponent
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberComponent = WhichNumberComponent
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSenderStringComponent = WhichSenderStringComponent
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = WhichNumberofAdditionalStrings
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent = WhichSecondString
            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent = WhichThirdString

                 #if IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - Additional number of strings is ")
                    cdPrintint (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings)
                    cdPrintnl()
                    cdPrintstring(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                    cdPrintnl()
                    cdPrintstring(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                    cdPrintnl()

                #endif

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS)

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[0] = TRUE
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[1] = TRUE
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[2] = TRUE

                //Do not include MP character in here! It would mean that an mp purge could get rid of single player messages also. The mp filter should remain distinct.

                g_LastTextSentTo = WAS_SENT_TO_EVERY_SP_CHARACTER

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to all singleplayer characters.")
                    cdPrintnl()
                #endif

                Perform_cp_StockMarketUpdate(CHAR_MICHAEL)
                Perform_cp_StockMarketUpdate(CHAR_TREVOR)
                Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)

            ELSE

                Get_Cellphone_Owner()

                SWITCH WhichRecipient

                    CASE NO_SILENT_RECIPIENT

                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This text message has no silent recipient - sent to current player.")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_MICHAEL

                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as MICHAEL")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_TREVOR

                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_TREVOR] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as TREVOR")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_FRANKLIN

                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_FRANKLIN] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS TEXT MESSAGE - This text message had a silent recipient specified as FRANKLIN")
                            cdPrintnl()
                        #endif

                    BREAK

                ENDSWITCH

                //Would need to add options to allow character.
                //g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE

                IF WhichRecipient = NO_SILENT_RECIPIENT //Only update this is message is explicitly sent.

                    SWITCH ENUM_TO_INT (g_Cellphone.PhoneOwner)

                        CASE 0

                            Perform_cp_StockMarketUpdate(CHAR_MICHAEL)

                            g_LastTextSentTo = WAS_SENT_TO_MICHAEL

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Michael.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE 1

                            Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)

                            g_LastTextSentTo = WAS_SENT_TO_FRANKLIN

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Franklin.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE 2

                            Perform_cp_StockMarketUpdate(CHAR_TREVOR)

                            g_LastTextSentTo = WAS_SENT_TO_TREVOR

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to Trevor.")
                                cdPrintnl()
                            #endif

                        BREAK

                        //Added to take care of bug 282407...
                        CASE 3

                            Perform_cp_StockMarketUpdate(CHAR_MULTIPLAYER)

                            g_LastTextSentTo = WAS_SENT_TO_MULTIPLAYER

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful text message sent to multiplayer character.")
                                cdPrintnl()
                            #endif

                        BREAK

                        DEFAULT

                            g_LastTextSentTo = WAS_SENT_TO_EVERY_SP_CHARACTER

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Text message has been sent but not sure who to!")
                                cdPrintnl()
                            #endif

                        BREAK

                    ENDSWITCH                                                                                  

                ENDIF

            ENDIF

            IF WhichCriticalStatus = TXTMSG_CRITICAL

                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) //singleplayer characters

                    //These are set to false by bringing up phone and apptextmessage launching...
                    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = TRUE
                    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = TRUE
                    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = TRUE

                ELSE

                    SWITCH WhichRecipient

                        CASE NO_SILENT_RECIPIENT

                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(g_Cellphone.PhoneOwner)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had no silent recipient, marking MustBeRead for current owner.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE SILENT_MICHAEL

                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_MICHAEL)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_MICHAEL as the recipient, marking MustBeRead for him.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE SILENT_TREVOR

                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_TREVOR)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_TREVOR as the recipient, marking MustBeRead for him.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE SILENT_FRANKLIN
                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_FRANKLIN)].g_LastMessageSentMustBeRead = TRUE
                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical text message had SILENT_FRANKLIN as the recipient, marking MustBeRead for him.")
                                cdPrintnl()
                            #endif

                        BREAK

                    ENDSWITCH

                ENDIF

            ENDIF

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Added text message into array index at position ")
                cdPrintint (TxtMsgIdentifiedFreeArrayIndex)
                cdPrintnl ()
                cdPrintstring("Time Stamp Details")
                cdPrintnl()
                cdPrintstring("Seconds: ")
                cdPrintint(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgSecs) 
                cdPrintnl()

                cdPrintstring("Minutes: ")
                cdPrintint(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMins) 
                cdPrintnl()

                cdPrintstring("Hours: ")
                cdPrintint(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgHours) 
                cdPrintnl()

                cdPrintstring("Day: ")
                cdPrintint(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgDay) 
                cdPrintnl()

                cdPrintstring("Month: ")
                cdPrintint(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgMonth) 
                cdPrintnl()

                cdPrintstring("Year: ")
                cdPrintint(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgTimeSent.TxtMsgYear) 
                cdPrintnl()

                cdPrintstring ("CELLPHONE_PRIV - Clearing headshot string store before feed processing.")

            #endif

            //Make sure that by default we don't associate a headshot ID with any new text message. If one is required then this will be created and reassigned when the
            //feed entry is constructed. Only MP is required to use user generated headshots so they should always require feed entries.

            //Default associated headshotID to NULL
            g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = NULL

            //PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", "Phone_SoundSet_Michael")

            IF ShouldDisplayNewTxtMsgSignifier

                //Moved to individual text message public functions in cellphone public so we can be certain signifier display data is placed before updating the movie.
                //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
                //SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

                Get_Cellphone_Owner()

                //Also need to declare soundsets here just in case phone hasn't already been brought up and a text comes in. See bug 427977 .
                IF g_Use_Prologue_Cellphone

                    g_Owner_Soundset = "Phone_SoundSet_Prologue"

                ELSE

                     SWITCH g_Cellphone.PhoneOwner 

                        //Mobile_5 clause would go in here.

                        CASE CHAR_MICHAEL  

                            g_Owner_Soundset = "Phone_SoundSet_Michael"

                        BREAK

                        CASE CHAR_TREVOR                                                                            

                            g_Owner_Soundset = "Phone_SoundSet_Trevor"

                        BREAK

                        CASE CHAR_FRANKLIN

                            g_Owner_Soundset = "Phone_SoundSet_Franklin"

                        BREAK

                        DEFAULT

                            g_Owner_Soundset = "Phone_SoundSet_Default"

                        BREAK

                    ENDSWITCH 

                ENDIF

                //Don't play text reception tone if player is in "quiet" mode.
                IF NOT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_QUIET_MODE))

                    #IF USE_TU_CHANGES

                        IF NOT IS_TRANSITION_ACTIVE()  //#1645435

                    #ENDIF

                        PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", g_Owner_Soundset)

                    #IF USE_TU_CHANGES

                        ENDIF

                    #ENDIF

                ENDIF

                //SET_BIT (BitSet_CellphoneDisplay, g_BS_DO_TEXT_MESSAGE_PREVIEW)

            ENDIF

            IF NOT g_Homescreen_In_Secondary_Mode
                IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update number of texts notifications when user is on first homescreen page.

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)

                    Scaleform_Place_Items_on_Homescreen(1)

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))

                ENDIF
            ENDIF 

			BOOL bRequested
			IF g_iDLCContentRequestedContact != -1
			AND WhichSender = (INT_TO_ENUM(enumCharacterList, g_iDLCContentRequestedContact))
				bRequested = TRUE
			ENDIF

			PROCESS_NPC_PHONE_PLAYSTATS_TEXT_MESSAGE(WhichSender, WhichTextLabel, bRequested, GET_PLAYER_RANK(PLAYER_ID()))

            RETURN TRUE

        ENDIF
#endif
ENDFUNC

#if USE_CLF_DLC
PROC Create_Car_List_String_For_Scaleform_CLF (INT passed_top_time_comparison)
    INT tempConstructionInt = 0
	
    SWITCH g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent

        CASE 1
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL01") //This refers to a label with one ~a~ car component...
        BREAK
        CASE 2
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL02") //This refers to a label with two ~a~ car components...
        BREAK
        CASE 3
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL03") //This refers to a label with three ~a~ car components...
        BREAK
        CASE 4
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL04") //This refers to a label with four ~a~ car components...
        BREAK
        CASE 5
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL05") //This refers to a label with five ~a~ car components...
        BREAK
        CASE 6
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL06") //This refers to a label with six ~a~ car components...
        BREAK
        CASE 7
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL07") //This refers to a label with seven ~a~ car components...
        BREAK
        CASE 8
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL08") //This refers to a label with eight ~a~ car components...
        BREAK
        CASE 9
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL09") //This refers to a label with nine ~a~ car components...
        BREAK
    ENDSWITCH

    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgLabel)  //Add in user-defined label.

    WHILE tempConstructionInt < g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent
        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.
        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgSpecialComponents

                CASE CAR_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                //New additions.
                CASE SUPERAUTOS_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SuperAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE LEGENDARY_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_LegendaryVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE PEDALMETAL_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PedalMetalVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE WARSTOCK_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_WarstockVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE ELITAS_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_ElitasVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE DOCKTEASE_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DockTeaseVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE DAILYOBJ_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DailyObjectivesList[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()
ENDPROC
#endif

#if USE_NRM_DLC
PROC Create_Car_List_String_For_Scaleform_NRM (INT passed_top_time_comparison)
    INT tempConstructionInt = 0

    SWITCH g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent

        CASE 1
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL01") //This refers to a label with one ~a~ car component...
        BREAK
        CASE 2
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL02") //This refers to a label with two ~a~ car components...
        BREAK
        CASE 3
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL03") //This refers to a label with three ~a~ car components...
        BREAK
        CASE 4
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL04") //This refers to a label with four ~a~ car components...
        BREAK
        CASE 5
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL05") //This refers to a label with five ~a~ car components...
        BREAK
        CASE 6
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL06") //This refers to a label with six ~a~ car components...
        BREAK
        CASE 7
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL07") //This refers to a label with seven ~a~ car components...
        BREAK
        CASE 8
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL08") //This refers to a label with eight ~a~ car components...
        BREAK
        CASE 9
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL09") //This refers to a label with nine ~a~ car components...
        BREAK
    ENDSWITCH

    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgLabel)  //Add in user-defined label.

    WHILE tempConstructionInt < g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent
        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.
        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgSpecialComponents

                CASE CAR_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                //New additions.
                CASE SUPERAUTOS_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SuperAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE LEGENDARY_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_LegendaryVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE PEDALMETAL_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PedalMetalVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE WARSTOCK_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_WarstockVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE ELITAS_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_ElitasVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE DOCKTEASE_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DockTeaseVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE DAILYOBJ_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DailyObjectivesList[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()
ENDPROC
#endif

PROC Create_Car_List_String_For_Scaleform (INT passed_top_time_comparison)
#if USE_CLF_DLC
    Create_Car_List_String_For_Scaleform_CLF (passed_top_time_comparison)
#endif
#if USE_NRM_DLC
    Create_Car_List_String_For_Scaleform_NRM (passed_top_time_comparison)
#endif

#if not USE_SP_DLC
    INT tempConstructionInt = 0

    SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent

        CASE 1
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL01") //This refers to a label with one ~a~ car component...
        BREAK
        CASE 2
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL02") //This refers to a label with two ~a~ car components...
        BREAK
        CASE 3
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL03") //This refers to a label with three ~a~ car components...
        BREAK
        CASE 4
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL04") //This refers to a label with four ~a~ car components...
        BREAK
        CASE 5
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL05") //This refers to a label with five ~a~ car components...
        BREAK
        CASE 6
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL06") //This refers to a label with six ~a~ car components...
        BREAK
        CASE 7
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL07") //This refers to a label with seven ~a~ car components...
        BREAK
        CASE 8
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL08") //This refers to a label with eight ~a~ car components...
        BREAK
        CASE 9
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL09") //This refers to a label with nine ~a~ car components...
        BREAK
    ENDSWITCH

    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgLabel)  //Add in user-defined label.

    WHILE tempConstructionInt < g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent
        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.
        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgSpecialComponents

                CASE CAR_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                //New additions.
                CASE SUPERAUTOS_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SuperAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE LEGENDARY_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_LegendaryVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK
                
                CASE PEDALMETAL_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PedalMetalVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK
                
                CASE WARSTOCK_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_WarstockVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK
                
                CASE ELITAS_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_ElitasVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK
                
                CASE DOCKTEASE_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DockTeaseVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK

                CASE DAILYOBJ_LIST_COMPONENT
                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DailyObjectivesList[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                BREAK
				
        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()

#endif
ENDPROC

///
/// url:bugstar:7188269 - Exotic Exports - As a QoL update, can we please add the ability to request a text message from Sessanta that lists the vehicles you still need to get that day?
///    

#if USE_CLF_DLC
PROC Create_Calkboard_Car_List_String_For_Scaleform_CLF (INT passed_top_time_comparison)
    INT tempConstructionInt = 0

	// g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgLabel

    SWITCH g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent

        CASE 1
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L01") //This refers to a label with one ~a~ car component...
        BREAK
        CASE 2
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L02") //This refers to a label with two ~a~ car components...
        BREAK
        CASE 3
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L03") //This refers to a label with three ~a~ car components...
        BREAK
        CASE 4
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L04") //This refers to a label with four ~a~ car components...
        BREAK
        CASE 5
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L05") //This refers to a label with five ~a~ car components...
        BREAK
        CASE 6
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L06") //This refers to a label with six ~a~ car components...
        BREAK
        CASE 7
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L07") //This refers to a label with seven ~a~ car components...
        BREAK
        CASE 8
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L08") //This refers to a label with eight ~a~ car components...
        BREAK
        CASE 9
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L09") //This refers to a label with nine ~a~ car components...
        BREAK
		CASE 10
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L10") //This refers to a label with ten ~a~ car components...
        BREAK
    ENDSWITCH

    WHILE tempConstructionInt < g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent
        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.
        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgSpecialComponents

				CASE DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_CalkboardAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the calkboard vehicle list					
                BREAK

        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()
ENDPROC
#endif

#if USE_NRM_DLC
PROC Create_Calkboard_Car_List_String_For_Scaleform_NRM (INT passed_top_time_comparison)
    INT tempConstructionInt = 0

    SWITCH g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent

       CASE 1
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L01") //This refers to a label with one ~a~ car component...
        BREAK
        CASE 2
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L02") //This refers to a label with two ~a~ car components...
        BREAK
        CASE 3
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L03") //This refers to a label with three ~a~ car components...
        BREAK
        CASE 4
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L04") //This refers to a label with four ~a~ car components...
        BREAK
        CASE 5
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L05") //This refers to a label with five ~a~ car components...
        BREAK
        CASE 6
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L06") //This refers to a label with six ~a~ car components...
        BREAK
        CASE 7
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L07") //This refers to a label with seven ~a~ car components...
        BREAK
        CASE 8
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L08") //This refers to a label with eight ~a~ car components...
        BREAK
        CASE 9
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L09") //This refers to a label with nine ~a~ car components...
        BREAK
		CASE 10
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L10") //This refers to a label with ten ~a~ car components...
        BREAK
    ENDSWITCH

    WHILE tempConstructionInt < g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent
        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.
        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgSpecialComponents
				
				CASE DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_CalkboardAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the calkboard vehicle list					
                BREAK

        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()
ENDPROC
#endif

PROC Create_Calkboard_Car_List_String_For_Scaleform (INT passed_top_time_comparison)
#if USE_CLF_DLC
    Create_Calkboard_Car_List_String_For_Scaleform_CLF (passed_top_time_comparison)
#endif
#if USE_NRM_DLC
    Create_Calkboard_Car_List_String_For_Scaleform_NRM (passed_top_time_comparison)
#endif

#if not USE_SP_DLC
    INT tempConstructionInt = 0

    SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent

        CASE 1
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L01") //This refers to a label with one ~a~ car component...
        BREAK
        CASE 2
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L02") //This refers to a label with two ~a~ car components...
        BREAK
        CASE 3
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L03") //This refers to a label with three ~a~ car components...
        BREAK
        CASE 4
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L04") //This refers to a label with four ~a~ car components...
        BREAK
        CASE 5
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L05") //This refers to a label with five ~a~ car components...
        BREAK
        CASE 6
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L06") //This refers to a label with six ~a~ car components...
        BREAK
        CASE 7
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L07") //This refers to a label with seven ~a~ car components...
        BREAK
        CASE 8
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L08") //This refers to a label with eight ~a~ car components...
        BREAK
        CASE 9
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L09") //This refers to a label with nine ~a~ car components...
        BREAK
		CASE 10
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("TUN_CBL_L10") //This refers to a label with ten ~a~ car components...
        BREAK
    ENDSWITCH

    WHILE tempConstructionInt < g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgNumberComponent
        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.
        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passed_top_time_comparison].TxtMsgSpecialComponents

				// url:bugstar:7188269 - Exotic Exports - As a QoL update, can we please add the ability to request a text message from Sessanta that lists the vehicles you still need to get that day?
				CASE DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_CalkboardAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the calkboard vehicle list					
                BREAK

        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()

#endif
ENDPROC

// ----

FUNC BOOL ProcessLiveSideTask ( enumCharacterList WhichSideTaskGiver, enumSideTaskList WhichSideTaskIdentifier, BOOL ShouldDisplayNewSideTaskSignifier = TRUE,
                                enumSideTaskLockedStatus WhichLockStatus = SIDETASK_UNLOCKED )

        //Don't allow texts to be sent when player is running text app.
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appSideTask")) > 0
            RETURN FALSE
        ENDIF

        IF CheckForSideTaskSpace() = FALSE   //From cellphone_private.sch, we need to check the text message array to make
                                                //sure we have enough space. If not, we make space where possible. If that 
                                                //fails, which it shouldn't... assert!

            #IF IS_DEBUG_BUILD

                SCRIPT_ASSERT ("CELLPHONE_PRIV - No more space left for further sidetasks - See Steve T!")
                cdPrintstring ("CELLPHONE_PRIV - No more space left for further sidetasks - unlocking earliest sidetask")
                cdPrintnl ()

            #endif

            ForceDeletionOfCompletedSidetask() //May need to flesh this out....

            RETURN FALSE //Return false this cycle but should be able to accept sidetask next cycle as space has been freed.

        ELSE

            SetTimeSideTaskWentLive (SideTaskIdentifiedFreeArrayIndex)  //As we have decided there's enough space to add sidetask, we 
                                                                        //store the sidetask's struct contents in the live sidetask array.

            g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].SideTaskIdentifier = WhichSideTaskIdentifier

            g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].SideTaskGiver = WhichSideTaskGiver

            IF WhichLockStatus = SIDETASK_EMPTY

                #IF IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - When setting a new live side task, you cannot define the lock status as empty. See Steve T")
                    cdPrintnl()

                    RETURN FALSE

                #endif

            ELSE

                g_LiveSideTask[SideTaskIdentifiedFreeArrayIndex].SideTaskLockStatus = WhichLockStatus

            ENDIF

            #IF IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Adding live sidetask into array index at position ")
                cdPrintint (SideTaskIdentifiedFreeArrayIndex)
                cdPrintnl()

            #endif

            IF ShouldDisplayNewSideTaskSignifier
                //g_DisplayNewSideTaskSignifier = TRUE //Removing. Sidetasks are redundant so going to use this global for Eyefind.
            ENDIF

            RETURN TRUE

        ENDIF

ENDFUNC

PROC Check_For_Ongoing_Call_Interruption()

    IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL 
    OR g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT //if the pro-active call is outgoing and hung up, we still need to know.

        #IF IS_DEBUG_BUILD

            cdPrintstring("CELLPHONE_PRIV - Ongoing call or pro-active waiting for answer interrupted by damage, water, death or arrest to player!")
            cdPrintnl()

        #endif

        g_CallRequiringPlayerResponse = FALSE //Required to nicely tie off fix for Bug 113398. It makes sure that any phonecall which is in "waiting on player response"
                                              //state in the PlayCellphoneConversation loop from dialogue handler, will exit nicely.    

        g_LastCellphoneCallInterrupted = TRUE

        //Dialogue Handler should also set this to true when a phone call has been hung up or rejected via the back button.

    ENDIF

ENDPROC

//These procs are used in appSettings and appHS_Sleep to temporarily remove some settings from the list if the player is on a mission.
PROC Temporarily_Remove_Special_Case_Settings()

    IF g_OnMissionState != MISSION_TYPE_OFF_MISSION

        //WIPNov
        Get_Cellphone_Owner()

        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[1] = FALSE //Temporarily remove sleep mode from profile setting if on mission

        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[PROFILE_SLEEP_MODE] = FALSE //Temporarily remove sleep mode from profile setting if on mission

    ENDIF

ENDPROC

PROC Restore_Special_Case_Settings()

    //WIPNov
    Get_Cellphone_Owner()

    //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[1] = TRUE //Restore sleep mode to profile setting.
    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[PROFILE_SLEEP_MODE] = TRUE //Restore sleep mode to profile setting.

ENDPROC

#IF IS_DEBUG_BUILD

//This will draw a grid overlay with x, y coordinates marked.
PROC draw_debug_screen_grid()

    FLOAT screenlatX = 0.0
    FLOAT screenlatY = 0.5

    FLOAT screenlongX = 0.5
    FLOAT screenlongY = 0.0

    INT latitudeIndex = 0
    //INT longitudeIndex = 0

    WHILE latitudeIndex < 10

        DRAW_RECT(screenlatX, screenlatY, 0.005, 1.0, 255, 255, 255, 125)
        DRAW_RECT(screenlongX, screenlongY, 1.0, 0.005, 255, 255, 255, 125)

        format_medium_ostext(0, 255, 0, 255)                                                                    
        DISPLAY_TEXT_WITH_FLOAT((screenlatX), (screenlatY - 0.15), "CELL_500", screenlatX, 2)

        format_medium_ostext(125, 125, 255, 255)                                                                    
        DISPLAY_TEXT_WITH_FLOAT((screenlongX - 0.16), (screenlongY), "CELL_501", screenlongY, 2)

        screenlatX = screenlatX + 0.1

        screenlongY = screenlongY + 0.1

        latitudeindex ++

    ENDWHILE

ENDPROC
#endif

#if IS_DEBUG_BUILD

//This is used to draw a rectangle which can be dynamically sized on to the game hud.
PROC draw_debug_rectangle()

    DRAW_RECT(debugrect_posX, debugrect_posY, debugrect_sizeX, debugrect_sizeY, 255, 255, 255, 125)

    //format_cellcam_fs_ostext (255, 255, 255, 255)
    //DISPLAY_TEXT (debugrect_posX, debugrect_posY, "CELL_286")

    DISPLAY_TEXT_WITH_FLOAT(0.5, 0.1, "CELL_500", debugrect_posX, 3)
    DISPLAY_TEXT_WITH_FLOAT(0.5, 0.15, "CELL_501", debugrect_posY, 3)

    DISPLAY_TEXT_WITH_FLOAT(0.5, 0.8, "CELL_500", debugrect_sizeX, 3)
    DISPLAY_TEXT_WITH_FLOAT(0.5, 0.85, "CELL_501", debugrect_sizeY, 3)

    IF IS_BUTTON_PRESSED (PAD1, DPADUP)  

        debugrect_posY = debugrect_posY - 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADDOWN)  

        debugrect_posY = debugrect_posY + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADLEFT)  

        debugrect_posX = debugrect_posX - 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADRIGHT)  

        debugrect_posX = debugrect_posX + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER1)  

        debugrect_sizeY = debugrect_sizeY + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER2)  

        debugrect_sizeY = debugrect_sizeY - 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)  

        debugrect_sizeX = debugrect_sizeX + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER2)  

        debugrect_sizeX = debugrect_sizeX - 0.003

    ENDIF

ENDPROC
#endif

#IF IS_DEBUG_BUILD

//This is used to draw a rectangle which can be dynamically sized on to the game hud.
PROC debug_3dphone_pos_rot()

    IF IS_BUTTON_PRESSED (PAD1, DPADUP)  

        debugrect_posY = debugrect_posY - 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADDOWN)  

        debugrect_posY = debugrect_posY + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADLEFT)  

        debugrect_posX = debugrect_posX - 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADRIGHT)  

        debugrect_posX = debugrect_posX + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER1)  

        debugrect_sizeY = debugrect_sizeY + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER2)  

        debugrect_sizeY = debugrect_sizeY - 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)  

        debugrect_sizeX = debugrect_sizeX + 0.003

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER2)  

        debugrect_sizeX = debugrect_sizeX - 0.003

    ENDIF

    //SET_MOBILE_PHONE_POSITION (g_3dPhonePosVec) 

    //SET_MOBILE_PHONE_POSITION (g_3dPhoneRotVec)

ENDPROC
#endif

 //url:bugstar:1607328 TU patch proc.
 PROC TitleUpdate_Add_Hide_Phone_Soft_Key() //This has to be moved here as a new proc as we can no longer alter the existing Set_SF_Buttons_SuppText_For_ConnectedCall()

    IF g_IsThisAnMPChatCall

       IF g_bInMultiplayer //Only add the hide phone button if in MP.

            IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)

                IF NOT (IS_PC_VERSION()) //2193981 - Conflicts with handbrake. Confusing.
            
                
                    // Could remove this block for Next Gen as vanishing the phone causes great confusion in first person.
                    
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            8, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //Hands Free / Hide Phone button - NEEDS NEW ICON! #1566736  Swap over to 8

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintString("CELLPHONE_PUBLIC - Adding Hands Free option SOFT KEY from TU proc - 12")
                        cdPrintnl()

                    #endif

                    SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DO_HIDE_PHONE)
                
               
                ENDIF



                /*
                IF NOT IS_BIT_SET(BitSet_CellphoneDisplay_Third,  g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE)

                    PRINT_HELP("CELL_HF_HIDE")

                    SET_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE)

                ENDIF
                */

            ENDIF

        ENDIF

    ENDIF

ENDPROC

PROC TitleUpdate_Post_Set_Player_Chat_Call_As_Connected()

    IF g_Cellphone.PhoneDS > PDS_AWAY

        g_Cellphone.PhoneDS = PDS_ONGOING_CALL  //#1602347 - Makes sure that the hands free option can be used from the outgoing call end.

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("STATE ASSIGNMENT 36. Cellphone_Controller assigns PDS_ONGOING_CALL")
            cdPrintnl()

        #endif

        g_IsThisAnMPChatCall = TRUE //Required so that the hands free handler can work for the call initiator

        #if IS_DEBUG_BUILD

            cdPrintString ("CHAT_CALL assignment 2 - has been set to TRUE")
            cdPrintnl()
            cdPrintnl()

        #endif

        IF g_bInMultiplayer //Add hands free button if in MP.


            IF NOT (IS_PC_VERSION()) //2193981 - Conflicts with handbrake. Confusing.


                // Could remove this block for Next Gen as vanishing the phone causes great confusion in first person.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                    8, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //Hands Free / Hide Phone button - NEEDS NEW ICON! #1566736

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString("CELLPHONE_PUBLIC - Adding Hands Free option SOFT KEY - 2")
                    cdPrintnl()

                #endif

                SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DO_HIDE_PHONE)
            

            ENDIF







            /*
            IF NOT IS_BIT_SET(BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE)

                PRINT_HELP("CELL_HF_HIDE")

                SET_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE)

            ENDIF
            */

        ENDIF

    ENDIF

ENDPROC

PROC TitleUpdate_Patch_In_JobList_For_SP_Phone()

    IF g_bInMultiplayer = FALSE

        g_AppList[appMPJOBLIST].ThisAppPosition = -99
        g_AppList[appCHECKLIST].ThisAppPosition = -99

        //Critical - Title Update
        IF g_CellphoneHasMPAccess

            #if IS_DEBUG_BUILD
                cdPrintString("CELL_PRIVATE - Player has GTAO access, displaying Joblist app from TitleUpdate patch procedure")
                cdPrintnl()
            #endif


            IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //2411448 If in director mode, move the joblist app beside Snapmatic so it can be navigated to.

                Fill_App_Sheet (AppINTERNET, "CELL_2", 2, "appInternet", 6, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)
                Fill_App_Sheet (AppMPJOBLIST, "CELL_29", 7, "appMPJobListNEW", 12, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)


            ELSE

                Fill_App_Sheet (AppMPJOBLIST, "CELL_29", 2, "appMPJobListNEW", 12, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

            ENDIF




        ELSE

            #if IS_DEBUG_BUILD
                cdPrintString("CELL_PRIVATE - No GTAO access, displaying Checklist app from TitleUpdate patch procedure")
                cdPrintnl()
            #endif

            Fill_App_Sheet (AppCHECKLIST, "CELL_23", 2, "appChecklist", 39, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)

        ENDIF

    ENDIF

ENDPROC

//___________________________________________________________________________________________________________________________________________________________________________
//
//
//  New MP Email work.
//
//
//___________________________________________________________________________________________________________________________________________________________________________

FUNC BOOL Email_IsFirstTimeNewerThanSecondTime (structMPemailTimeSent FirstTimeToCompare, structMPemailTimeSent SecondTimeToCompare)

    structMPemailTimeSent FirstComparisonStruct, SecondComparisonStruct 

    INT FirstSecsMinsHoursDaysValue, SecondSecsMinsHoursDaysValue

    IF FirstTimeToCompare.EmailYear < SecondTimeToCompare.EmailYear //First value is older as the second value occurred in a more recent year.

        RETURN FALSE

    ENDIF

    IF FirstTimeToCompare.EmailYear > SecondTimeToCompare.EmailYear //First value is more recent as the second value occurred in an earlier year.

        RETURN TRUE

    ENDIF

    IF FirstTimeToCompare.EmailYear = SecondTimeToCompare.EmailYear //If we haven't returned true or false we can assume that the year fields the of the two times are the same

        IF FirstTimeToCompare.EmailMonth < SecondTimeToCompare.EmailMonth //First value is older as the second value occurred in a more recent month.

            RETURN FALSE

        ENDIF

        IF FirstTimeToCompare.EmailMonth > SecondTimeToCompare.EmailMonth //First value is more recent as the second value occurred in an earlier month.

            RETURN TRUE

        ENDIF

        IF FirstTimeToCompare.EmailMonth = SecondTimeToCompare.EmailMonth  //Months must be the same also. We need to see which day is most recent.

             //Work out how many seconds are in each field of the two temp comparison structs. The resultant int will be that day's value.

             FirstComparisonStruct.EmailSecs = FirstTimeToCompare.EmailSecs

             FirstComparisonStruct.EmailMins = (FirstTimeToCompare.EmailMins * 60) //60 secs in a min.
             FirstComparisonStruct.EmailHours = (FirstTimeToCompare.EmailHours * 3600) //3600 secs in a hour.

             FirstComparisonStruct.EmailDay = (FirstTimeToCompare.EmailDay * (3600 * 24)) //86400 secs in a day.

             FirstSecsMinsHoursDaysValue = (FirstComparisonStruct.EmailSecs + FirstComparisonStruct.EmailMins + FirstComparisonStruct.EmailHours + FirstComparisonStruct.EmailDay)

             SecondComparisonStruct.EmailSecs = SecondTimeToCompare.EmailSecs

             SecondComparisonStruct.EmailMins = (SecondTimeToCompare.EmailMins * 60) //60 secs in a min.
             SecondComparisonStruct.EmailHours = (SecondTimeToCompare.EmailHours * 3600) //3600 secs in a hour.

             SecondComparisonStruct.EmailDay = (SecondTimeToCompare.EmailDay * (3600 * 24)) //86400 secs in a day.

             SecondSecsMinsHoursDaysValue = (SecondComparisonStruct.EmailSecs + SecondComparisonStruct.EmailMins + SecondComparisonStruct.EmailHours + SecondComparisonStruct.EmailDay)

             IF FirstSecsMinsHoursDaysValue > SecondSecsMinsHoursDaysValue  //First value is more recent. The month is the same and the year is the same but the assigned value 
             OR FirstSecsMinsHoursDaysValue = SecondSecsMinsHoursDaysValue  //using weighted multipliers is larger, so we return true.

                RETURN TRUE

             ELSE 

                RETURN FALSE

             ENDIF

        ENDIF

    ENDIF

    RETURN FALSE

ENDFUNC

PROC ForceDeletionOfEarliestEmailMessage() //This will identify the earliest locked message and remove the lock. Unlikely to ever be needed but just in case...

    //Now that MP and SP have distinct portions of the global text message array to work with, we need to define the array checking limiters.

    INT TextArray_StartIndex, TextArray_EndIndex

    TextArray_StartIndex = 0    
    TextArray_EndIndex = (MAX_MP_EMAILS - 2)    

    INT TempArrayIndex = TextArray_StartIndex

    EmailIdentifiedFreeArrayIndex = (MAX_MP_EMAILS - 1) //Assign the EmailIdentifiedFreeArrayIndex to the dummy value.

    //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailSecs = -1 
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailMins = 0
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailHours = 0
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailDay = 0
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                                                                         //will definitely be "older"

    WHILE TempArrayIndex < (TextArray_EndIndex)                                                                          

        IF NOT Email_IsFirstTimeNewerThanSecondTime (g_EmailMessage[TempArrayIndex].EmailTimeSent,  g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent)

            //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.

            EmailIdentifiedFreeArrayIndex = TempArrayIndex

        ENDIF

        TempArrayIndex ++

    ENDWHILE

    //Unlock the earliest found text message, so that it can be written over next cycle.
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailLockStatus = Email_UNLOCKED

ENDPROC

FUNC BOOL CheckForEmailMessageSpace()

    INT TextArray_StartIndex, TextArray_EndIndex

    TextArray_StartIndex = 0    
    TextArray_EndIndex = (MAX_MP_EMAILS - 2)   

    INT TempArrayIndex = TextArray_StartIndex

    WHILE TempArrayIndex < (TextArray_EndIndex)                                                                          

        IF g_EmailMessage[TempArrayIndex].EmailLockStatus = Email_EMPTY

                EmailIdentifiedFreeArrayIndex = TempArrayIndex

                #IF IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - Identified email message empty position at ")
                    cdPrintint (EmailIdentifiedFreeArrayIndex)
                    cdPrintnl()

                #endif

                RETURN TRUE                                           

        ENDIF

        TempArrayIndex ++

    ENDWHILE

        //If the preceding section has not returned TRUE by this point, that means that every position in the text message array has been used at least once. We must now check for 
        //any unlocked positions to overwrite.

        TempArrayIndex = TextArray_StartIndex

        EmailIdentifiedFreeArrayIndex = (MAX_MP_EMAILS - 1) //Assign the EmailIdentifiedFreeArrayIndex to the dummy value.

        //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
        g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailSecs = -1 
        g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailMins = 0
        g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailHours = 0
        g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailDay = 0
        g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailYear = 99999 //Put in a farcical date in the future for the dummy, so that the first unlocked comparison
                                                                                  //will definitely be "older"

        WHILE TempArrayIndex < (TextArray_EndIndex)                                                                          

            IF g_EmailMessage[TempArrayIndex].EmailLockStatus = Email_EMPTY
            OR g_EmailMessage[TempArrayIndex].EmailLockStatus = Email_UNLOCKED //This array position is unlocked. That means it is the secondary suitable candidate to store a new message.

                IF NOT Email_IsFirstTimeNewerThanSecondTime (g_EmailMessage[TempArrayIndex].EmailTimeSent,  g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent)

                    //the first time must therefore be older than current identified array index, so the first time becomes the new prime candidate.

                    EmailIdentifiedFreeArrayIndex = TempArrayIndex

                    /*
                    #IF IS_DEBUG_BUILD

                        cdPrintstring ("Earliest available index identified at ")
                        cdPrintint (EmailIdentifiedFreeArrayIndex)
                        cdPrintnl()

                    #endif
                    */

                ENDIF

            ENDIF

            TempArrayIndex ++

        ENDWHILE

        //If no slot has been identified then there can be no space - and every text message in the array must have locked status!
        IF EmailIdentifiedFreeArrayIndex = (MAX_MP_EMAILS - 1) //As the EmailIdentifiedFreeArrayIndex is still equal to the dummy value no free position could have been found. 

            RETURN FALSE

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Preparing to overwrite previously written email message array index. Setting all SP characters to FALSE presence.")
                cdPrintint (EmailIdentifiedFreeArrayIndex)
                cdPrintnl()

            #endif

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[0] = FALSE
            g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[1] = FALSE
            g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[2] = FALSE

            //No incrementation required as we are overwriting a slot.

            RETURN TRUE

        ENDIF

ENDFUNC

PROC SetTimeThatEmailWasSent (INT WhichArraySlotNumber)

    INT TempSecs, TempMins, TempHours
    INT TempDay,  TempMonth, TempYear

    TempSecs = GET_CLOCK_SECONDS()
    TempMins = GET_CLOCK_MINUTES() //sic. This is actually spelled like that in COMMANDS_CLOCK.sch
    TempHours = GET_CLOCK_HOURS()

    TempDay = GET_CLOCK_DAY_OF_MONTH()

    TempMonth = (ENUM_TO_INT(GET_CLOCK_MONTH()) + 1) //January returns zero. Need to increment returned month for display purposes.

    TempYear = GET_CLOCK_YEAR()

    g_EmailMessage[WhichArraySlotNumber].EmailTimeSent.EmailSecs = TempSecs
    g_EmailMessage[WhichArraySlotNumber].EmailTimeSent.EmailMins = TempMins
    g_EmailMessage[WhichArraySlotNumber].EmailTimeSent.EmailHours = TempHours

    g_EmailMessage[WhichArraySlotNumber].EmailTimeSent.EmailDay = TempDay
    g_EmailMessage[WhichArraySlotNumber].EmailTimeSent.EmailMonth = TempMonth
    g_EmailMessage[WhichArraySlotNumber].EmailTimeSent.EmailYear = TempYear

ENDPROC

//Reuses enumTxtMsgSpecialComponents

FUNC BOOL ProcessEmailMessage ( enumCharacterList WhichSender, STRING WhichTextLabel, enumMPemailLockedStatus WhichLockStatus, 

                                enumTxtMsgSpecialComponents WhichComponentType,
                                STRING WhichBodyStringComponent, STRING WhichSenderStringComponent, 
                                INT WhichNumberComponent = -99, 

                                enumMPemailMissionCritical WhichCriticalStatus = EMAIL_NOT_CRITICAL,
                                enumMPemailAutoUnlockAfterRead WhichAutoUnlockStatus = EMAIL_DO_NOT_AUTO_UNLOCK,

                                enumMPemailIsReplyRequired WhichReplyStatus = MPE_NO_REPLY_REQUIRED, BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,
                                enumMPemailIsBarterRequired WhichBarterStatus = MPE_NO_BARTER_REQUIRED,
                                enumMPemailCanCallSender  WhichCanCallSenderStatus = MPE_CANNOT_CALL_SENDER,
                                INT WhichNumberofAdditionalStrings = 0,
                                STRING WhichSecondString = NULL, 
                                STRING WhichThirdString = NULL,
                                enumRecipientList WhichRecipient = NO_SILENT_RECIPIENT)

        //Unref hack. 

        IF WhichNumberOfAdditionalStrings > 99

        ENDIF

        IF ARE_STRINGS_EQUAL (WhichSecondString, WhichThirdString)
        ENDIF


        Get_Cellphone_Owner()

        BOOL Bypass_Checks_For_Silent_Recipient = FALSE

        SWITCH WhichRecipient

            CASE SILENT_MICHAEL

                IF g_Cellphone.PhoneOwner = CHAR_MICHAEL    

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                        cdPrintnl()

                    #endif

                    Bypass_Checks_For_Silent_Recipient = FALSE

                ELSE                

                    Bypass_Checks_For_Silent_Recipient = TRUE

                ENDIF

            BREAK

            CASE SILENT_TREVOR

                IF g_Cellphone.PhoneOwner = CHAR_TREVOR 

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                        cdPrintnl()

                    #endif

                    Bypass_Checks_For_Silent_Recipient = FALSE

                ELSE                

                    Bypass_Checks_For_Silent_Recipient = TRUE

                ENDIF

            BREAK

            CASE SILENT_FRANKLIN

                IF g_Cellphone.PhoneOwner = CHAR_FRANKLIN   

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_PRIVATE - Current owner is same as silent recipient. Not bypassing send checks!")   
                        cdPrintnl()

                    #endif

                    Bypass_Checks_For_Silent_Recipient = FALSE

                ELSE                

                    Bypass_Checks_For_Silent_Recipient = TRUE

                ENDIF

            BREAK

            DEFAULT 

                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_PRIVATE - No silent recipient. Not bypassing send checks!")   
                    cdPrintnl()

                #endif

                Bypass_Checks_For_Silent_Recipient = FALSE

            BREAK

        ENDSWITCH

        IF Bypass_Checks_For_Silent_Recipient = FALSE

            //For bug #1162976, do not allow text message send while player is swimming underwater. Trial basis.
            IF IS_PLAYER_PLAYING (PLAYER_ID()) 

               IF IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_PRIVATE - Not allowing email message to be sent while player_ped is swimming underwater. #1162976")   
                        cdPrintnl()

                    #endif

                    RETURN FALSE

                ENDIF

            ENDIF

            IF g_LastEmailSentMustBeRead = TRUE

                #if IS_DEBUG_BUILD 

                    cdPrintnl ()
                    cdPrintstring ("CELLPHONE_PRIV - Previously sent critical email message for this owner has not been read! Can't send new email message!")
                    cdPrintnl ()

                #endif

                RETURN FALSE

            ENDIF

            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appEmail")) > 0

                #if IS_DEBUG_BUILD

                    cdPrintnl ()
                    cdPrintstring ("CELLPHONE_PRIV - SP email app currently onscreen!")
                    cdPrintnl ()

                #endif

                RETURN FALSE

            ENDIF

            //Don't allow texts to be sent when player is running text app.
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appMPEmail")) > 0

                #IF IS_DEBUG_BUILD

                    cdPrintnl ()
                    cdPrintstring ("CELLPHONE_PRIV - appMPEmail is currently running! Can't send new email message! See Steve T if you really need to do this.")
                    cdPrintnl ()

                #endif

                //If we allow this, I will need to put a check in for complex_app, single message display.

                RETURN FALSE

            ENDIF

        ENDIF

        IF CheckForEmailMessageSpace() = FALSE      //From cellphone_private.sch, we need to check the email message array to make
                                                    //sure we have enough space. If not, we make space where possible. If that 
                                                    //fails, which it shouldn't... assert!

            #if IS_DEBUG_BUILD

                SCRIPT_ASSERT ("CELLPHONE_PRIV - Warning! Email Message System full with locked messages - Let Steve T know.")
                cdPrintstring ("CELLPHONE_PRIV - No more space left for further email messages - all messages locked!")
                cdPrintnl ()
                cdPrintstring ("CELLPHONE_PRIV - Unlocking oldest locked message to free space for next cycle.")
                cdPrintnl ()

            #endif

            ForceDeletionOfEarliestEmailMessage() 

            RETURN FALSE //Return false this cycle but should be able to accept text message next cycle as space has been freed.

        ELSE

            SetTimeThatEmailWasSent (EmailIdentifiedFreeArrayIndex)     //As we have decided there's enough space to add another email message, we 
                                                                        //store the email message's struct contents in the email message array.

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailLabel = WhichTextLabel

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSender = WhichSender

            IF WhichLockStatus = EMAIL_EMPTY

                #if IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - When sending, you cannot define an email message lock status as empty. See Steve T")
                    cdPrintnl ()

                    RETURN FALSE

                #endif

            ELSE

                g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailLockStatus = WhichLockStatus

            ENDIF

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailCritical = WhichCriticalStatus

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailAutoUnlockStatus = WhichAutoUnlockStatus

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailReplyStatus = WhichReplyStatus

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailCanCallSenderStatus = WhichCanCallSenderStatus

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailBarterStatus = WhichBarterStatus

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailReadStatus = UNREAD_MPEMAIL

            //This one uses txtmsg Special components by design...
            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = WhichComponentType

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailStringComponent = WhichBodyStringComponent

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberComponent = WhichNumberComponent

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSenderStringComponent = WhichSenderStringComponent

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberOfAdditionalStrings = WhichNumberofAdditionalStrings

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent = WhichSecondString

            g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent = WhichThirdString

                 #if IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PRIV - Additional number of strings is ")
                    cdPrintint (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberOfAdditionalStrings)
                    cdPrintnl()
                    cdPrintstring(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                    cdPrintnl()
                    cdPrintstring(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)
                    cdPrintnl()

                #endif


                Get_Cellphone_Owner()

                SWITCH WhichRecipient

                    CASE NO_SILENT_RECIPIENT

                        g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS EMAIL MESSAGE - This email message has no silent recipient - sent to current player.")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_MICHAEL

                        g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS EMAIL MESSAGE - This email message had a silent recipient specified as MICHAEL")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_TREVOR

                        g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[CHAR_TREVOR] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS EMAIL MESSAGE - This email message had a silent recipient specified as TREVOR")
                            cdPrintnl()
                        #endif

                    BREAK

                    CASE SILENT_FRANKLIN

                        g_EmailMessage[EmailIdentifiedFreeArrayIndex].PhonePresence[CHAR_FRANKLIN] = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("PROCESS EMAIL MESSAGE - This email message had a silent recipient specified as FRANKLIN")
                            cdPrintnl()
                        #endif

                    BREAK

                ENDSWITCH

                //Would need to add options to allow character.
                //g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].PhonePresence[CHAR_MICHAEL] = TRUE

                IF WhichRecipient = NO_SILENT_RECIPIENT //Only update this is message is explicitly sent.

                    SWITCH ENUM_TO_INT (g_Cellphone.PhoneOwner)

                        CASE 0

                            Perform_cp_StockMarketUpdate(CHAR_MICHAEL)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful email message sent to Michael.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE 1

                            Perform_cp_StockMarketUpdate(CHAR_FRANKLIN)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful email message sent to Franklin.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE 2

                            Perform_cp_StockMarketUpdate(CHAR_TREVOR)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful email message sent to Trevor.")
                                cdPrintnl()
                            #endif

                        BREAK

                        //Added to take care of bug 282407...
                        CASE 3

                            Perform_cp_StockMarketUpdate(CHAR_MULTIPLAYER)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Successful email message sent to multiplayer character.")
                                cdPrintnl()
                            #endif

                        BREAK

                        DEFAULT

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring ("CELLPHONE_PRIV - Email message has been sent but not sure who to!")
                                cdPrintnl()
                            #endif

                        BREAK

                    ENDSWITCH                                                                                  

                ENDIF

            //ENDIF End of IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) condition check

            IF WhichCriticalStatus = EMAIL_CRITICAL


                    SWITCH WhichRecipient

                        CASE NO_SILENT_RECIPIENT

                            g_LastEmailSentMustBeRead = TRUE

                            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(g_Cellphone.PhoneOwner)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS EMAIL MESSAGE - This critical EMAIL message had no silent recipient, marking MustBeRead for current owner.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE SILENT_MICHAEL

                            g_LastEmailSentMustBeRead = TRUE

                            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_MICHAEL)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical EMAIL message had SILENT_MICHAEL as the recipient, marking MustBeRead for him.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE SILENT_TREVOR

                            g_LastEmailSentMustBeRead = TRUE

                            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_TREVOR)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical EMAIL message had SILENT_TREVOR as the recipient, marking MustBeRead for him.")
                                cdPrintnl()
                            #endif

                        BREAK

                        CASE SILENT_FRANKLIN

                            g_LastEmailSentMustBeRead = TRUE

                            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[ENUM_TO_INT(CHAR_FRANKLIN)].g_LastMessageSentMustBeRead = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("PROCESS TEXT MESSAGE - This critical EMAIL message had SILENT_FRANKLIN as the recipient, marking MustBeRead for him.")
                                cdPrintnl()
                            #endif

                        BREAK

                    ENDSWITCH

//                ENDIF

            ENDIF

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PRIV - Added email message into array index at position ")
                cdPrintint (EmailIdentifiedFreeArrayIndex)
                cdPrintnl ()
                cdPrintstring("Time Stamp Details")
                cdPrintnl()
                cdPrintstring("Seconds: ")
                cdPrintint(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailSecs) 
                cdPrintnl()

                cdPrintstring("Minutes: ")
                cdPrintint(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailMins) 
                cdPrintnl()

                cdPrintstring("Hours: ")
                cdPrintint(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailHours) 
                cdPrintnl()

                cdPrintstring("Day: ")
                cdPrintint(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailDay) 
                cdPrintnl()

                cdPrintstring("Month: ")
                cdPrintint(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailMonth) 
                cdPrintnl()

                cdPrintstring("Year: ")
                cdPrintint(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailTimeSent.EmailYear) 
                cdPrintnl()

                cdPrintstring ("CELLPHONE_PRIV - Clearing headshot string store before feed processing.")

            #endif

            //Make sure that by default we don't associate a headshot ID with any new text message. If one is required then this will be created and reassigned when the
            //feed entry is constructed. Only MP is required to use user generated headshots so they should always require feed entries.

            //Default associated headshotID to NULL
            g_TxtMsgHeadshotID[EmailIdentifiedFreeArrayIndex] = NULL

            //PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", "Phone_SoundSet_Michael")

            IF ShouldDisplayNewTxtMsgSignifier

                //Moved to individual text message public functions in cellphone public so we can be certain signifier display data is placed before updating the movie.
                //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
                //SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

                Get_Cellphone_Owner()

                //Also need to declare soundsets here just in case phone hasn't already been brought up and a text comes in. See bug 427977 .
                IF g_Use_Prologue_Cellphone

                    g_Owner_Soundset = "Phone_SoundSet_Prologue"

                ELSE

                     SWITCH g_Cellphone.PhoneOwner 

                        //Mobile_5 clause would go in here.

                        CASE CHAR_MICHAEL  

                            g_Owner_Soundset = "Phone_SoundSet_Michael"

                        BREAK

                        CASE CHAR_TREVOR                                                                            

                            g_Owner_Soundset = "Phone_SoundSet_Trevor"

                        BREAK

                        CASE CHAR_FRANKLIN

                            g_Owner_Soundset = "Phone_SoundSet_Franklin"

                        BREAK

                        DEFAULT

                            g_Owner_Soundset = "Phone_SoundSet_Default"

                        BREAK

                    ENDSWITCH 

                ENDIF

                //Don't play text reception tone if player is in "quiet" mode.
                IF NOT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_QUIET_MODE))

                    #IF USE_TU_CHANGES

                        IF NOT IS_TRANSITION_ACTIVE()  //#1645435

                    #ENDIF

                        PLAY_SOUND_FRONTEND (-1, "Text_Arrive_Tone", g_Owner_Soundset)

                    #IF USE_TU_CHANGES

                        ENDIF

                    #ENDIF

                ENDIF

                //SET_BIT (BitSet_CellphoneDisplay, g_BS_DO_TEXT_MESSAGE_PREVIEW)

            ENDIF

            IF NOT g_Homescreen_In_Secondary_Mode
                IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update number of texts notifications when user is on first homescreen page.

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)

                    Scaleform_Place_Items_on_Homescreen(1)

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))

                ENDIF
            ENDIF 

			BOOL bRequested
			IF g_iDLCContentRequestedContact != -1
			AND WhichSender = (INT_TO_ENUM(enumCharacterList, g_iDLCContentRequestedContact))
				bRequested = TRUE
			ENDIF

			PROCESS_NPC_PHONE_PLAYSTATS_EMAIL(WhichSender, WhichTextLabel, bRequested, GET_PLAYER_RANK(PLAYER_ID()))

            RETURN TRUE

        ENDIF

ENDFUNC

PROC Create_Email_Car_List_String_For_Scaleform (INT passed_top_time_comparison)

    INT tempConstructionInt = 0

    SWITCH g_EmailMessage[passed_top_time_comparison].EmailNumberComponent

        CASE 1

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL01") //This refers to a label with one ~a~ car component...

        BREAK

        CASE 2

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL02") //This refers to a label with two ~a~ car components...

        BREAK

        CASE 3

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL03") //This refers to a label with three ~a~ car components...

        BREAK

        CASE 4

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL04") //This refers to a label with four ~a~ car components...

        BREAK

        CASE 5

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL05") //This refers to a label with five ~a~ car components...

        BREAK

        CASE 6

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL06") //This refers to a label with six ~a~ car components...

        BREAK

        CASE 7

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL07") //This refers to a label with seven ~a~ car components...

        BREAK

        CASE 8

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL08") //This refers to a label with eight ~a~ car components...

        BREAK

        CASE 9

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CL09") //This refers to a label with nine ~a~ car components...

        BREAK

    ENDSWITCH

    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[passed_top_time_comparison].EmailLabel)  //Add in user-defined label.

    WHILE tempConstructionInt < g_EmailMessage[passed_top_time_comparison].EmailNumberComponent

        //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP.

        //Adding in support for multiple car lists at request of 1600460    
        SWITCH g_EmailMessage[passed_top_time_comparison].EmailSpecialComponents

                CASE CAR_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                //New additions.
                CASE SUPERAUTOS_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SuperAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                CASE LEGENDARY_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_LegendaryVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                CASE PEDALMETAL_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PedalMetalVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                CASE WARSTOCK_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_WarstockVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                CASE ELITAS_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_ElitasVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                CASE DOCKTEASE_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DockTeaseVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

                CASE DAILYOBJ_LIST_COMPONENT

                     ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DailyObjectivesList[tempConstructionInt]) //This is taken from MP and is the standard import / export list.

                BREAK

        ENDSWITCH

        tempConstructionInt ++

    ENDWHILE

    END_TEXT_COMMAND_SCALEFORM_STRING()


ENDPROC






PROC LaunchTransitionMoveUpIfRequired()
         

    VECTOR CurrentCellphonePosition

    GET_MOBILE_PHONE_POSITION (CurrentCellphonePosition) 
                
    IF g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio].y = CurrentCellphonePosition.Y

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("CELLPHONE_PRIV - LaunchTransitionMoveUpIfRequired() says phone already up!")   
            cdPrintnl()

        #endif

    ELSE

        g_3DPhoneNeedsMovedUp = TRUE  

    ENDIF


ENDPROC





PROC LaunchTransitionMoveDownIfRequired()
         
    VECTOR CurrentCellphonePosition

    GET_MOBILE_PHONE_POSITION (CurrentCellphonePosition) 
                
    IF g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio].y = CurrentCellphonePosition.Y

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("CELLPHONE_PRIV - LaunchTransitionMoveDownIfRequired() says phone already down!")   
            cdPrintnl()

        #endif

    ELSE

        g_FirstPersonTransitionOngoing = TRUE 

    ENDIF


ENDPROC




