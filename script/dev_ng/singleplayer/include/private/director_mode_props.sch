
USING "menu_public.sch"
USING "commands_replay.sch"
USING "snapshot_private.sch"
USING "director_mode_prop_data.sch"

///-----------------------------------------------------------------------------------
///----------PROCS AND FUNCTIONS------------------------------------------------------


FUNC BOOL IsPropModelRestrictedUse(MODEL_NAMES PassedModel)

    IF PassedModel = PROP_PIPES_02B       //Pipe Pile
    OR PassedModel = PROP_LOGPILE_01      //Large Log Pile
    OR PassedModel = PROP_LOGPILE_03      //Small Log Pile
	OR PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_01")) //Fix for 3190881 - limit the number of the animated tubes that can be placed
	OR PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_02")) 
	OR PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_03")) 
	OR PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_04")) 
	OR PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_05")) 
	OR PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speed")) //Causes probs when spam placed. 3196193
	//These next four spirals need restricted for 3201251
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_02"))	
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_02b"))		
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_05"))
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_05b"))
	//Stunt prop rings of fire
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_constraction_01a"))
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Hoop_Small_01"))
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	//These props can be fragged by collision or have moving parts.
			OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))		
			OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall"))						
			OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_tube_wall"))	
			
			OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Take_Flight_Sign"))	//Has moving parts.
	#ENDIF		
	
	
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC


FUNC VECTOR GetPropModelWarpOffset(MODEL_NAMES PassedModel)

	IF PassedModel =INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fork")) //Fix for 3191209
		RETURN <<-4.0, 0.0, 0.0>>
	ENDIF
	
	//Fix for 3195349
	IF PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_goal"))
		RETURN <<-2.0, 0.0, 0.0>>
	ENDIF
			
	//Fix for 3199849
	IF Passedmodel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowling_pin")) 
		RETURN <<-5.0, 0.0, 0.0>>
	ENDIF
					
	
	//Fix for 3194186
	IF PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("sst_prop_hoop_constraction_01a"))
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_small_01"))
		RETURN <<-4.0, -4.0, 0.0>>
	ENDIF
			
			
			
	//Fix for 3199268 - these speed up or slow down props don't have collision on them.
	IF PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp"))	
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T1"))
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T2"))
	OR PassedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))		
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2"))		
		RETURN <<-10.5, -10.0, 0.0>>		
	ENDIF
	
	//Fix for 3193664 corner props warp position - Stunt  Tracks category..
	IF PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_180D_bar"))	//Track Bend Barrier 180D -OK
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_180d"))		//Track Bend 180d - OK
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_l_b"))			//Track Bend XL - OK
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_bar_l_b"))		//Track Bend Bar XL	-OK
		RETURN <<15.0, 0.0, 0.0>>	
	ENDIF
	
	
	//Fix for 3193664 corner props warp position - Stunt Tracks Extra category.
	IF PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_turn")) 		//Stunt Track Turn - OK
	OR PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwturn")) //Stunt Track Wide Turn - OK
		RETURN <<25.0, 0.0, 0.0>>
	ENDIF
			
	IF PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_uturn"))  	//Stunt Track U-Turn
			RETURN <<0.0, 43.0, 0.0>>
	ENDIF
	
	IF PassedModel =  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwuturn")) //Stunt Track Wide U-Turn - OK.
		RETURN <<40.0, 0.0, 0.0>>	
	ENDIF
			
			
	RETURN <<0,0,0>>

ENDFUNC




//Make sure we have jarring hud elements hidden during transition into Prop Editor. See 2498577, 2497632
PROC PropEditorHidesPerFrame(BOOL bPiMouseCameraOverride)

    HIDE_HUD_AND_RADAR_THIS_FRAME()
    THEFEED_HIDE_THIS_FRAME()
    DISABLE_SELECTOR_THIS_FRAME() //2502655
    DISABLE_CELLPHONE_THIS_FRAME_ONLY()
    REPLAY_PREVENT_RECORDING_THIS_FRAME()
    SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
    SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
            
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
        
        IF NOT bPiMouseCameraOverride
            SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
            SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
        ENDIF
        
        ENABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
                
    ELSE
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
    ENDIF
    
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL) 
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)   
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)  
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER) //player jacks vehicle when hitting Y on PI menu 
        
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_PEDAL)    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)

    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT) //player Can jump out of a car while the PI menu is on
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT) // flying vehicle yaw (LB,RB) controls conflict with time scrubbing
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)

    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
   
ENDPROC

PROC GET_CANNOT_PLACE_LABEL_TEXT_PROPERTIES(TEXT_PLACEMENT &eTP, TEXT_STYLE &eTextStyle, FLOAT fX, FLOAT fY)
    
    eTP.x = fX
    eTP.y = fY
    eTextStyle.aFont = FONT_STANDARD
    eTextStyle.XScale = 0.5
    eTextStyle.YScale = 0.5
    eTextStyle.drop = DROPSTYLE_OUTLINEONLY
    GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_RED), eTextStyle.r, eTextStyle.g, eTextStyle.b, eTextStyle.a)
    
ENDPROC

PROC DRAW_CANNOT_PLACE_LABEL()

    FLOAT fTempX2, fTempY2
    TEXT_PLACEMENT eTP
    TEXT_STYLE eTextStyle
    VECTOR vVector

    IF NOT IS_PED_INJURED(PLAYER_PED_ID())    
    
        vVector = GET_ENTITY_COORDS(PLAYER_PED_ID())
        
        IF GET_SCREEN_COORD_FROM_WORLD_COORD(vVector+<<0.0,0.0,1.5>>, fTempx2, fTempy2)
            GET_CANNOT_PLACE_LABEL_TEXT_PROPERTIES(eTP, eTextStyle, fTempX2, fTempY2)
            DRAW_TEXT(eTP, eTextStyle,  "DMPP_NOPLACE") //Cannot place here.
        ENDIF

    ENDIF

ENDPROC

FUNC INT DMPROP_GET_NUM_PROPS()
    RETURN iNumProps
ENDFUNC

FLOAT           fPartitionLineX         = 0.1124
FLOAT           fPartitionLineY         = 0.23340
FLOAT           fPartitionLineW         = 0.225
FLOAT           fPartitionLineH         = 0.0027

FLOAT           fSceneHelpX             = 0.295
FLOAT           fSceneHelpY             = 0.150
FLOAT           fSceneHelpXOffset       = 0.005
FLOAT           fSceneHelpRightJustX    = 0.0235
FLOAT           fSceneHelpYOffset       = 0.0035 // 0.030 // 0.058
FLOAT           fSceneHelpYGap          = 0.030
FLOAT           fSceneHelpW             = 0.20
FLOAT           fSceneHelpH             = 0.178

INT             iTextFont               = ENUM_TO_INT( FONT_ROCKSTAR_TAG )
FLOAT           fXScale                 = 1.0
FLOAT           fYScale                 = 0.575
INT             iDrop                   = ENUM_TO_INT( DROPSTYLE_NONE )
INT             iTextType               = ENUM_TO_INT( TEXTTYPE_TS_STANDARDSMALL )

BOOL            bShowCurrentSceneInfo   = FALSE

#IF IS_DEBUG_BUILD
BOOL 	 bEnableResetHeightDebug = FALSE // should only be checked in as false. Debug Only feature for displaying how much the mover prop has been elevated from the Z attained when using the "reset height" feature.
VECTOR	vResetOriginal
VECTOR  vCurrentVec
#ENDIF

//===========STUNT PROP VARIABLES===============//
#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT 
	CAMERA_INDEX ElevatedCam
	VECTOR vecCamOffset = <<-20,0,55>>
	FLOAT fPropMoveRate, fPropMoveAccel = 0.05
	FLOAT fCamRotation = 0, fCamHeight = 40, fCamRadius = 20
	FLOAT fCamMaxHeight = 200	//fCamMinHeight = 2, 
	FLOAT fMoverStuntMaxZ = 1500
	FLOAT fMoverStuntZMultiplier = 3
	VECTOR vecPlayerPositionBeforeCam
	FLOAT fPlayerHeadingBeforeCam
	INT iToggleCamFadeTimeLeft, iToggleCamFadeTime = 1000
//	FLOAT fToggleCamFadeRange = 100
	VEHICLE_INDEX vehCamSwitchVehicle
	INTERIOR_INSTANCE_INDEX intPlayer
	INT iPlayerRoomKey
#ENDIF

#IF IS_DEBUG_BUILD
PROC INIT_PROP_SCENE_INFO_HELP_DEBUG_WIDGET()
    START_WIDGET_GROUP( "Prop Editor Scene Info Help." )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpX",          fSceneHelpX,            0.0,    1.0,                                0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpY",          fSceneHelpY,            0.0,    1.0,                                0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpXOffset",    fSceneHelpXOffset,      0.0,    1.0,                                0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpRightJustX", fSceneHelpRightJustX,   -10.0,  10.0,                               0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpYOffset",    fSceneHelpYOffset,      0.0,    1.0,                                0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpW",          fSceneHelpW,            0.0,    1.0,                                0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fSceneHelpH",          fSceneHelpH,            0.0,    1.0,                                0.0005  )
        
        ADD_WIDGET_INT_SLIDER   (   "iTextFont",            iTextFont,              0,      COUNT_OF( TEXT_FONTS ) -1,          1       )
        ADD_WIDGET_FLOAT_SLIDER (   "fXScale",              fXScale,                0.0,    1.0,                                0.0005  )
        ADD_WIDGET_FLOAT_SLIDER (   "fYScale",              fYScale,                0.0,    1.0,                                0.0005  )
        ADD_WIDGET_INT_SLIDER   (   "iDrop",                iDrop,                  0,      COUNT_OF( DROPSTYLE ) -1,           1       )
        ADD_WIDGET_INT_SLIDER   (   "iTextType",            iTextType,              0,      COUNT_OF( STANDARD_TEXTTYPE ) -1,   1       )
        
        ADD_WIDGET_BOOL( "bShowCurrentSceneInfo", bShowCurrentSceneInfo )
    STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

FUNC INT GET_NUM_PROPS_IN_SCENE( INT iScene, BOOL bReturnDynamicCount = FALSE )
    //Current scene = -1
    IF iScene = -1
        IF bReturnDynamicCount
            RETURN iNumDynamicProps
        ELSE
            RETURN iNumProps
        ENDIF
    ENDIF

    INT iIndex      = 0
    INT iPropCount  = 0
    INT iDynmCount  = 0
    
    REPEAT ciMAXOBJECTS iIndex
        IF eScenes[ iScene ][ iIndex ].mModel != DUMMY_MODEL_FOR_SCRIPT
        iPropCount++
            IF bReturnDynamicCount
                IF GET_IS_DM_PROP_DYNAMIC( eScenes[ iScene ][ iIndex ].mModel )
                    iDynmCount++
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    IF bReturnDynamicCount
        //CPRINTLN( DEBUG_DIRECTOR, " GET_NUM_PROPS_IN_SCENE - Returning ", iDynmCount, " dynamic props." )
        RETURN iDynmCount
    ELSE
        //CPRINTLN( DEBUG_DIRECTOR, " GET_NUM_PROPS_IN_SCENE - Returning ", iPropCount, " static props." )
        RETURN iPropCount
    ENDIF
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_ALL_SCENE_PROP_LOCATION_INFORMATION( STRING &tlLocations[ ], INT &iLocationCount[ ], INT iNumLocations )

    INT iIndex = 0
    
    CDEBUG3LN( DEBUG_DIRECTOR, "")
    CDEBUG3LN( DEBUG_DIRECTOR, " ### Location Spew ###")    
    CDEBUG3LN( DEBUG_DIRECTOR, " - iNumLocations = ", iNumLocations)
    CDEBUG3LN( DEBUG_DIRECTOR, "")
    REPEAT ciMAXOBJECTS iIndex
        IF NOT IS_STRING_NULL_OR_EMPTY( tlLocations[ iIndex ] )
            CDEBUG3LN( DEBUG_DIRECTOR, " - tlLocations[ ", iIndex, " ] = ", tlLocations[ iIndex ] )
        ENDIF
    ENDREPEAT

    REPEAT ciMAXOBJECTS iIndex
        IF iLocationCount[ iIndex ] != 0
            CDEBUG3LN( DEBUG_DIRECTOR, " - iLocationCount[ ", iIndex, " ] = ", iLocationCount[ iIndex ] )
        ENDIF
    ENDREPEAT

    CDEBUG3LN( DEBUG_DIRECTOR, "")
ENDPROC
#ENDIF

FUNC TEXT_LABEL_15 GET_SCENE_MODE_PROP_LOCATION( INT iScene )
    INT             iIndex
    TEXT_LABEL_15   tlLocations[ ciMAXOBJECTS ]
    INT             iLocationCount[ ciMAXOBJECTS ]
    INT             iNumLocations = 0
    
    //  Go through all props in scene
    REPEAT sdSceneData[ iScene ].iTotalProps iIndex
        IF eScenes[ iScene ][ iIndex ].mModel != DUMMY_MODEL_FOR_SCRIPT
            STRING strCurrLoc = GET_NAME_OF_ZONE( eScenes[ iScene ][ iIndex ].vPos )
            IF iNumLocations > 0    //  If not first index
                INT iLocIndex = 0
                BOOL bLocFound = FALSE
                REPEAT iNumLocations iLocIndex  //  Go through existing locations and see if this one has already been counted
                    IF ARE_STRINGS_EQUAL( strCurrLoc, tlLocations[ iLocIndex ] )    //  If found up count
                        iLocationCount[ iLocIndex ]++
                        bLocFound = TRUE
                        BREAKLOOP
                    ENDIF
                ENDREPEAT
                IF NOT bLocFound    //  If not found add to the locations array and set count to 1
                    tlLocations[ iNumLocations ] = strCurrLoc
                    iLocationCount[ iNumLocations ] = 1
                    iNumLocations++
                ENDIF
            ELSE    //  First index add to locations array and up count for that index
                tlLocations[ 0 ] = strCurrLoc
                iLocationCount[ 0 ] = 1
                iNumLocations++
            ENDIF           
        ENDIF
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
    //PRINT_ALL_SCENE_PROP_LOCATION_INFORMATION( tlLocations, iLocationCount, iNumLocations )
    #ENDIF
    
    INT iHighestCount = 0
    INT iHighestIndex = 0
    //  Go thorugh all the found locations and check the higest count to get the mode (most popular)
    REPEAT iNumLocations iIndex
        IF iLocationCount[ iIndex ] > iHighestCount
            iHighestCount = iLocationCount[ iIndex ]
            iHighestIndex = iIndex
        ENDIF
    ENDREPEAT
    
    CPRINTLN( DEBUG_DIRECTOR, " - iHighestCount = ", iHighestCount, " - iHighestIndex = ", iHighestIndex, " - RETURNING: ", tlLocations[ iHighestIndex ] )
    RETURN tlLocations[ iHighestIndex ]
ENDFUNC

PROC DRAW_PROP_MODE_LOCATION_MINI_MAP( INT iScene )
    
    //Hide current scene blips if first time here
    IF bPropSceneInfoMiniMapActive = FALSE
        INT iIndex
        REPEAT ciMAXOBJECTS iIndex
            IF DOES_BLIP_EXIST(eProps[iIndex].BlipIndex)
                SET_BLIP_DISPLAY(eProps[iIndex].BlipIndex, DISPLAY_NOTHING)
            ENDIF
        ENDREPEAT
    ENDIF
    
    //Only draw if there's something to draw
    IF GET_NUM_PROPS_IN_SCENE(iScene) > 0
        CPRINTLN( DEBUG_DIRECTOR, "DRAW_PROP_MODE_LOCATION_MINI_MAP - Scene[", iScene, "]" )
        
        INT iIndex
        //  Store the max and min, x and y to zoom the map to the distance and get mid point between max and min coords
        FLOAT fMinX, fMinY, fMaxX, fMaxY
        INT iPropInLocCount = 0
        //  Blip all prop locations in the mode zone
        REPEAT ciMAXOBJECTS iIndex
            MODEL_NAMES model
            VECTOR vPropCoord
            
            IF iScene = -1
                model = eProps[iIndex].mModel
                vPropCoord = eProps[iIndex].vPos
            ELSE
                model = eScenes[ iScene ][ iIndex ].mModel
                vPropCoord = eScenes[ iScene ][ iIndex ].vPos
            ENDIF
            
            IF model != DUMMY_MODEL_FOR_SCRIPT 
    //            STRING strCurrLoc = GET_NAME_OF_ZONE( vPropCoord )
    //            IF ARE_STRINGS_EQUAL( tlModeLocation, strCurrLoc )  //  If prop position is in the mode location
    //          ENDIF
                    
                biScenePropBlips[ iPropInLocCount ] = CREATE_BLIP_FOR_COORD( vPropCoord )
                SET_BLIP_SCALE( biScenePropBlips[ iPropInLocCount ], 0.75 )
                SET_BLIP_NAME_FROM_TEXT_FILE( biScenePropBlips[ iPropInLocCount ], GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL( model ) )
                SET_BLIP_COLOUR( biScenePropBlips[ iPropInLocCount ], BLIP_COLOUR_PURPLEDARK)
                SHOW_HEIGHT_ON_BLIP( biScenePropBlips[ iPropInLocCount ], FALSE )
                
                //  Grab max and min x and y coords
                IF vPropCoord.x < fMinX OR fMinX = 0.0      fMinX = vPropCoord.x        ENDIF
                IF vPropCoord.y < fMinY OR fMinY = 0.0      fMinY = vPropCoord.y        ENDIF
                IF vPropCoord.x > fMaxX OR fMaxX = 0.0      fMaxX = vPropCoord.x        ENDIF
                IF vPropCoord.y > fMaxY OR fMaxY = 0.0      fMaxY = vPropCoord.y        ENDIF
                
                iPropInLocCount++
            ENDIF
        ENDREPEAT
        
        //Make sure min/max are within decent - pause map - limits
        fMinX = FMAX(-4000, FMIN(4000, fMinX))
        fMaxX = FMAX(-4000, FMIN(4000, fMaxX))
        fMinY = FMAX(-4000, FMIN(8000, fMinY))
        fMaxY = FMAX(-4000, FMIN(8000, fMaxY))

        FLOAT   fMaxXDistance = fMaxX - fMinX 
        FLOAT   fMaxYDistance = fMaxY - fMinY
            
        //  Average location of all blipped prop locations
        // Steve T - we'll probably no longer need this as we need to cycle through the placed props on the expanded at all times minimap.
        VECTOR  vMidPoint = << fMinX + ( fMaxXDistance / 2.0 ), fMinY + ( fMaxYDistance / 2.0 ), 0.0 >>
        
        fMaxXDistance *= 1.1     // Add a 10% to the distance between the to furtherest blips so theyre not on the edge of the minimap
        fMaxYDistance *= 1.1
        
        //  Max distance between two props for the map zoom
        FLOAT fMaxDistance = FMAX( fMaxXDistance, fMaxYDistance )
        fMaxDistance /= 2.0 //  Distance is done from centre to edge, not total width
        fMaxDistance = FMAX( fMaxDistance, 60.0 )   //  If under 60m default to 60m
        
        fRadarZoom = fMaxDistance

        //Setup big map 
        LOCK_MINIMAP_POSITION( vMidPoint.x, vMidPoint.y )
        SET_RADAR_ZOOM_TO_DISTANCE(fRadarZoom)
        LOCK_MINIMAP_ANGLE(ROUND(GET_HEADING_BETWEEN_VECTORS_2D(vMidPoint,<<0,25000,0>>)))  //Rotate to the North blip
//      DISPLAY_RADAR( TRUE )
//      SET_BIGMAP_ACTIVE( TRUE, FALSE )
    ENDIF
    
    bPropSceneInfoMiniMapActive = TRUE
ENDPROC

PROC CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP( BOOL bFullClean = TRUE)
    
    CPRINTLN( DEBUG_DIRECTOR, "CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP - Full clean = ", PICK_STRING( bFullClean, "TRUE", "FALSE" ) )
    INT iIndex  
    //  Clean blipped prop coords
    REPEAT ciMAXOBJECTS iIndex
        IF DOES_BLIP_EXIST( biScenePropBlips[ iIndex] )
            REMOVE_BLIP( biScenePropBlips[ iIndex] )
        ELSE
            BREAKLOOP   //  Breaks out of repeat after finding a null blip as there will be no more blip in the array after this
        ENDIF
    ENDREPEAT
    fRadarZoom = fDefaultRadarZoom
    UNLOCK_MINIMAP_POSITION()
    
    //  Reset minimap
    IF bFullClean
        iPrevSelectedScene = -2     //Now using -2 as NO SCENES
        //Reset current scene blips
        REPEAT ciMAXOBJECTS iIndex
            IF DOES_BLIP_EXIST(eProps[iIndex].BlipIndex)
                SET_BLIP_DISPLAY(eProps[iIndex].BlipIndex, DISPLAY_BOTH)
            ENDIF
        ENDREPEAT
        UNLOCK_MINIMAP_ANGLE()
    ENDIF
    
    bPropSceneInfoMiniMapActive = FALSE
ENDPROC

PROC REFRESH_PROP_MODE_LOCATION_MINI_MAP( INT iNewScene)
    CPRINTLN( DEBUG_DIRECTOR, "REFRESH_PROP_MODE_LOCATION_MINI_MAP - New Scene[ ", iNewScene, "]" )
    
    CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP( FALSE )
    DRAW_PROP_MODE_LOCATION_MINI_MAP( iNewScene)        
    
ENDPROC

PROC DRAW_SCENE_INFORMATION( INT iScene )

    INT             iScenePropCount     = sdSceneData[ iScene ].iTotalProps
    INT             iSceneDynmPropCount = sdSceneData[ iScene ].iDynamicProps
    TEXT_LABEL_15   tlModeLocation      = sdSceneData[ iScene ].tlModeLocation
    //TEXT_LABEL_23   tlSceneName         = sdSceneData[ iScene ].tlSceneName
    
    TEXT_PLACEMENT  textPlacement
    TEXT_STYLE      textStyle
    
    fSceneHelpX             = CUSTOM_MENU_W + 0.07
    
    textPlacement.x         = fSceneHelpX + fSceneHelpXOffset
    textPlacement.y         = fSceneHelpY + fSceneHelpYOffset
    #IF IS_DEBUG_BUILD
    textPlacement.strName   = "Scene Data"
    #ENDIF
    
    textStyle.aFont         = INT_TO_ENUM( TEXT_FONTS, iTextFont )
    textStyle.XScale        = fXScale   
    textStyle.YScale        = fYScale   
    textStyle.r             = 255                           
    textStyle.g             = 255                       
    textStyle.b             = 255
    textStyle.a             = 255
    textStyle.drop          = INT_TO_ENUM( DROPSTYLE, iDrop )
    textStyle.aTextType     = INT_TO_ENUM( STANDARD_TEXTTYPE, iTextType )
    
    FLOAT fSceneInformationH = fSceneHelpH
    IF iScenePropCount = 0
        fSceneInformationH = 0.1085
    ENDIF
    
    DRAW_RECT_FROM_CORNER( fSceneHelpX - 0.0015, fSceneHelpY - 0.0025, fSceneHelpW + 0.003, fSceneInformationH + 0.005, 255, 255, 255, 200 )
    DRAW_RECT_FROM_CORNER( fSceneHelpX, fSceneHelpY, fSceneHelpW, fSceneInformationH, 0, 0, 0, 200 )
    

    //2518982 Localisation fix.
    //CPRINTLN( DEBUG_DIRECTOR, " ST_Displaying Scene info for ", iScene)

    //This is a bit of a long winded way of doing this as you can just add iScene + 1 to construct the CM_PROPEDSC suffix but I want to make certain QA are seeing the correct thing here in the first instance.
    TEXT_LABEL_23 SceneDisplayLabel
    
    SWITCH iScene

        CASE 0 SceneDisplayLabel = "CM_PROPEDSC1" BREAK
        CASE 1 SceneDisplayLabel = "CM_PROPEDSC2" BREAK
        CASE 2 SceneDisplayLabel = "CM_PROPEDSC3" BREAK
        CASE 3 SceneDisplayLabel = "CM_PROPEDSC4" BREAK

    ENDSWITCH


    //DRAW_TEXT_WITH_STRING( textPlacement, textStyle, "STRING", tlSceneName ) - ST. Removed this for 2518982. Explicit string reference is bad. Needs to be a text label.

    DRAW_TEXT(textPlacement, textStyle, SceneDisplayLabel)
    
    DRAW_RECT_FROM_CORNER( fSceneHelpX, textPlacement.y + fSceneHelpYGap + 0.0065, fSceneHelpW, 0.0025, 255, 255, 255, 100 )
        
    textPlacement.y += fSceneHelpYGap + 0.01
    textStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
    textStyle.YScale = 0.50
    DRAW_TEXT( textPlacement, textStyle, "DMPP_DYNPPLCD" ) //  DYNAMIC PROPS
    
    TEXT_PLACEMENT tempTextPlacement = textPlacement
    tempTextPlacement.x += fSceneHelpW - fSceneHelpRightJustX
    DRAW_TEXT_WITH_2_NUMBERS( tempTextPlacement, textStyle, "CM_ITEM_COUNT", iSceneDynmPropCount, ciMAXDYNAMICOBJECTS, FONT_CENTRE, TRUE ) //  x/x
    
    textPlacement.y += fSceneHelpYGap
    DRAW_TEXT( textPlacement, textStyle, "DMPP_PROPSPLCD" )  //  PROPS PLACED
    
    tempTextPlacement = textPlacement
    tempTextPlacement.x += fSceneHelpW - fSceneHelpRightJustX
    DRAW_TEXT_WITH_2_NUMBERS( tempTextPlacement, textStyle, "CM_ITEM_COUNT", iScenePropCount, ciMAXOBJECTS, FONT_CENTRE, TRUE )  //  x/x
    
    IF iScenePropCount > 0
        DRAW_RECT_FROM_CORNER( fSceneHelpX, textPlacement.y + fSceneHelpYGap + 0.005, fSceneHelpW, 0.0025, 255, 255, 255, 100 )
        
        textPlacement.y += fSceneHelpYGap + 0.01
        DRAW_TEXT( textPlacement, textStyle, "DMPP_MODELOC" ) // Location...
            
        textPlacement.y += fSceneHelpYGap
        textStyle.aFont = FONT_CURSIVE
        DRAW_TEXT( textPlacement, textStyle, tlModeLocation ) // Mode location
    
        IF NOT bPropSceneInfoMiniMapActive
            DRAW_PROP_MODE_LOCATION_MINI_MAP( iScene, tlModeLocation )
        ELIF iPrevSelectedScene != iScene
            REFRESH_PROP_MODE_LOCATION_MINI_MAP( iScene, tlModeLocation )
        ENDIF
        
        SET_RADAR_ZOOM_TO_DISTANCE( fRadarZoom )
    ELSE
        CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP( FALSE ) //Was TRUE - updated for bug 2485908 - have the minimap expanded at all times. 
    ENDIF
    iPrevSelectedScene = iScene
ENDPROC

PROC Manage_PropMenu_Scaleform_Partition_Line( INT iPropCount ) 
                
    IF iPropCount > 0
    
        BOOL    bDrawPartitionLine  = TRUE
        FLOAT   fAdjustedLineY      = fPartitionLineY
        INT     iTopMenuItem        = GET_TOP_MENU_ITEM()
                
        IF iTopMenuItem > 0
        
            IF iTopMenuItem >= ciPROPADDLINES
                bDrawPartitionLine = FALSE
            ELSE
                FLOAT fYAdjustment = ( CUSTOM_MENU_ITEM_BAR_H * iTopMenuItem )
                fAdjustedLineY -= fYAdjustment
            ENDIF
        
        ENDIF
            
        IF bDrawPartitionLine
            SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
            SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
                
            ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9()
            DRAW_RECT( fPartitionLineX, fAdjustedLineY, fPartitionLineW, fPartitionLineH, 255, 255, 255, 255 )
            
            RESET_SCRIPT_GFX_ALIGN()
        ENDIF
    ENDIF
    
ENDPROC


FUNC FLOAT DMPROP_GET_MARKER_SCALE(DM_PROP prop)
	FLOAT scale = 0.5
	IF FMAX(prop.vSize.x, prop.vSize.y) > 100
		scale = 10
	ELIF FMAX(prop.vSize.x, prop.vSize.y) > 50
		scale = 5
	ELIF FMAX(prop.vSize.x, prop.vSize.y) > 10
		scale = 2
	ELIF FMAX(prop.vSize.x, prop.vSize.y) > 2
		scale = 1
	ENDIF
	
	RETURN scale
ENDFUNC

FUNC VECTOR GetMoverChevronOffset(MODEL_NAMES passedModel)

	IF passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_turn")) //3199592
		RETURN <<0,0,40.0>>
	ENDIF
	
	IF passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_uturn")) //3199592
		RETURN <<0,0,55.0>>
	ENDIF

	//Making sure this sign has the chevron during the mover phase seen from all angles.
	IF passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Ron_Sign")) //3199592 Stunt misc
		RETURN <<0, 0, -8.0>>
	ENDIF
	
	IF passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Xero_Sign")) //3199592 Stunt misc
	OR passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowling_pin")) //Stunt Dynamic
	OR passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_gantry_01")) //Stunt misc
	OR passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_goal")) //Stunt misc
	OR passedModel = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall")) //Stunt Dynamic
		RETURN <<0, 0, -5.0>>
	ENDIF




	RETURN <<0, 0, 0>>
ENDFUNC


PROC DMPROP_DRAW_MARKER(VECTOR position, FLOAT scale = 0.5, INT r=200, INT g = 100, INT b = 100, INT a = 150, BOOL bUseCamDistScaling = FALSE, BOOL bUseChevronOffset = TRUE)

	IF bUseCamDistScaling
		//Get the distance to the camera
		VECTOR vCam = GET_FINAL_RENDERED_CAM_COORD()
		FLOAT dist = VDIST(vCam, position)
		
		scale = FMAX( FMIN(dist / 20, FMIN(20, FMIN(moverProp.vSize.x, moverProp.vSize.y) * 3)), 0.5)
//		TEXT_LABEL_15 txt = "Scale-"
//		txt += ROUND(scale * 10)
//		DRAW_DEBUG_TEXT_2D(txt,<<0.5,0.7,0>>)
	ENDIF

   	IF bUseChevronOffset 
	AND DOES_ENTITY_EXIST(moverProp.obj) AND ePEState = PES_PropPlace
		position = position - (GetMoverChevronOffset(moverProp.mModel)) //Manipulate the position for any problematic props. See bug 3199592. Just limiting this to the mover for just now as it's cheaper.
	ENDIF

	DRAW_MARKER(MARKER_ARROW, position, <<0,0,0>>, <<0,180,0>>, <<scale, scale, scale>>, r, g, b, a, FALSE, TRUE)
		
ENDPROC



FUNC INT DMPROP_GET_NTH_PROP_INDEX(INT iIndex)
    int i = 0, oldIndex = iIndex
    REPEAT ciMAXOBJECTS i       //Repeat from 0 to max objects, incrementing i each frame.
//        CPRINTLN(debug_dan, "DMPROP_ GET_NTH_PROP ",iIndex," Repeat Index ", i) 
        IF DOES_ENTITY_EXIST(eProps[i].obj)
        OR eProps[i].mModel != DUMMY_MODEL_FOR_SCRIPT
            IF iIndex = 0
//                CPRINTLN(debug_director, "DMPROP_GET_NTH_PROP_INDEX ",iIndex," returning eProp array index ", i) 
                RETURN i
            ELSE
                iIndex -= 1
            ENDIF
        ENDIF
    ENDREPEAT   
    CPRINTLN(debug_director, "DMPROP_GET_NTH_PROP prop index passed that was out of range ", oldIndex)
	DEBUG_PRINTCALLSTACK()
    SCRIPT_ASSERT("DMPROP_GET_NTH_PROP_INDEX, prop index out of range - for prop #")
    RETURN -1
ENDFUNC

FUNC DM_PROP DMPROP_GET_NTH_PROP(INT iIndex)
    INT i = DMPROP_GET_NTH_PROP_INDEX(iIndex)
    IF i >= 0 
        RETURN eProps[i]
    ENDIF
    
    CASSERTLN(debug_director, "DMPROP_GET_NTH_PROP: No prop found, Returning the 1st prop")
    RETURN eProps[0]
ENDFUNC













PROC Manage_PropMenu_Scaleform_Help(INT iPassedSel)
    iPassedSel = iPassedSel
    INT iPropCount = DMPROP_GET_NUM_PROPS()

//    Manage_PropMenu_Scaleform_Partition_Line( iPropCount )

//    IF iPassedSel < ciPROPSCENELINES
    IF ePEState = PES_SceneSelect
        //Scene panel help display
        IF NOT bShowCurrentSceneInfo
            sPropEditorHelpString = "DMPP_HLP_SCNINFO"      
        ELIF b_PropSceneUpdatedSinceLastSave
            sPropEditorHelpString = "DMPP_HLP_SCNSVE"
        ELSE
            sPropEditorHelpString = "RemovePropMenuHelp"
        ENDIF
    ELIF ePEState = PES_SceneEdit
        IF b_PropSceneUpdatedSinceLastSave
            sPropEditorHelpString = "DMPP_HLP_SCNSVE"   //  Ensure you save
        ELIF iPassedSel = ciPISceneSave OR iPassedSel = ciPISceneCopy
            sPropEditorHelpString = "DMPP_HLP_SAVEQUIT" //Saves only written on exiting DM
        ELSE
            sPropEditorHelpString = "RemovePropMenuHelp"
        ENDIF

    ELIF ePEState = PES_PropPlace
//        IF GET_GAME_TIMER() > iLastResultTimer + 1000
        //Update the mover help.
        IF GET_GAME_TIMER() > iLastResultTimer +100
			//DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.14, "STRING", "Checking LastResultTimer")
            IF iPropCount = 0 AND (ePropWarning = PW_Fine OR ePropWarning = PW_NotReady)			
				//DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "Checking Warnings")
                IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                    sPropEditorHelpString = "DMPP_HLP1PC"   //  Generic help on how to use
                ELSE
                    sPropEditorHelpString = "DMPP_HLP1"     
                ENDIF
            ELIF ePropWarning = PW_NotReady
                //Reset help
                sPropEditorHelpString = "RemovePropMenuHelp"
            ELSE
                    //Check if any prop warning is active
                    SWITCH ePropWarning
                        CASE PW_AboveGround                 sPropEditorHelpString = "DMPP_HLPGround"        BREAK
                        CASE PW_Colliding                   sPropEditorHelpString = "DMPP_HLPCollide"       BREAK
                        CASE PW_LandPropOnWater             sPropEditorHelpString = "DMPP_HLPLandprop"      BREAK
                        CASE PW_WaterPropOnLand             sPropEditorHelpString = "DMPP_HLPWaterprop"     BREAK
                        CASE PW_MaxDynamicProps             sPropEditorHelpString = "DMPP_HLPMaxDynam"      BREAK
                        CASE PW_MaxProps                    sPropEditorHelpString = "DMPP_HLPMaxProps"      BREAK
                        CASE PW_PropInInterior              sPropEditorHelpString = "DMPP_HLPInterior"      BREAK
                        CASE PW_PropBiggerThanWaterDepth    sPropEditorHelpString = "DMPP_HLPBigWaterProp"  BREAK
                        CASE PW_RestrictedArea              sPropEditorHelpString = "DMPP_HLPRestrict"      BREAK
                        CASE PW_RestrictedUse               sPropEditorHelpString = "DMPP_HLPSpecType"      BREAK
                        //  Display nothing if prop placed and not in prop list, or scene index.
                        DEFAULT                 sPropEditorHelpString = "RemovePropMenuHelp"    BREAK   
                    ENDSWITCH           
            ENDIF
        ENDIF
    ELIF ePEState = PES_PropList
        
        //Update help message if the player is not allowed to warp to an interior prop. 2506996  
        DM_PROP Temp_WarpProp = DMPROP_GET_NTH_PROP(iBlippedProp)

        IF DOES_ENTITY_EXIST(Temp_WarpProp.Obj)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
            INTERIOR_INSTANCE_INDEX WarpInterior
            WarpInterior = GET_INTERIOR_FROM_ENTITY(Temp_WarpProp.Obj)
            IF IS_VALID_INTERIOR(WarpInterior)
			OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	//Can't move to a prop when in a vehicle
                sPropEditorHelpString = "DMPP_HLP_PLCD" 
            ELSE
                sPropEditorHelpString = "DMPP_HLP_PLCD2"        //  You can edit, delete or move to this prop.
            ENDIF
        ENDIF

    ELIF ePEState = PES_SceneEdit
        sPropEditorHelpString = "DMPP_HLP_PLCD"         //  You can edit/ delte props
    ENDIF


    //Makes sure we have no unreferenced problems if feature is switched off and dump debug for removal.
    IF ARE_STRINGS_EQUAL (sPropEditorHelpString, "RemovePropMenuHelp")
        //CPRINTLN(debug_director, "RemovePropMenuHelp has been called from Manage_PropMenu")
    ENDIF



ENDPROC

FUNC FLOAT GET_SURFACE_Z_BELOW(VECTOR vPos, BOOL bIncludeWater = TRUE)
    FLOAT fRet
    SCRIPT_WATER_TEST_RESULT result = TEST_VERTICAL_PROBE_AGAINST_ALL_WATER(vPos + <<0,0,2>>,SCRIPT_INCLUDE_ALL,fRet)
    
    IF result = SCRIPT_WATER_TEST_RESULT_BLOCKED
        RETURN fRet
    ELIF result = SCRIPT_WATER_TEST_RESULT_WATER
        IF bIncludeWater RETURN fRet
        ELSE
            IF NOT GET_GROUND_Z_FOR_3D_COORD(vPos + <<0,0,2>>, fRet)
                fRet = -200
            ENDIF
            RETURN fRet
        ENDIF
    ELSE 
        RETURN -200.0       //Failsafe, should not get here
    ENDIF
ENDFUNC

PROC INITIALISE_DM_PROPS()

    //Request labels
    REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)

    iCurrentScene = -1  //Force loading the 1st scene on startup

    FILL_IN_PROP_DATA()

ENDPROC

FUNC INT DMPROP_GET_NUM_MENU_ENTRIES()
    SWITCH ePEState
        case PES_SceneSelect
            RETURN ciMAXSCENES
        break
        case PES_SceneEdit
            RETURN 5    //Edit props / Save scene / Copy scene / Clear scene
        break
        case PES_PropPlace
            RETURN 2    //Type / Category
        break
        case PES_PropList
            RETURN iNumProps
        break
    ENDSWITCH
    RETURN iNumProps    //category/type
ENDFUNC

PROC DMPROP_SET_STATE(enum_propedit_state eState)
    CPRINTLN(debug_director,"Switching Prop Editor mode to ",eState)
    ePEState = eState
    
    iLastResultTimer = 0                            //Reset the shape test / help message timer
    sPropEditorHelpString = "RemovePropMenuHelp"    //Reset the help message
ENDPROC

PROC SAFE_RELEASE_MODEL(MODEL_NAMES model)
	IF model != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(model)
	ENDIF
ENDPROC

PROC DMPROP_CLEAR_MOVER_DATA(BOOL bClearPositionData = TRUE)
    IF DOES_ENTITY_EXIST(moverProp.obj)
        DELETE_OBJECT(moverProp.obj)
    ENDIF
//	CPRINTLN(debug_director,"Clear mover data position:",bClearPositionData)
	SAFE_RELEASE_MODEL(moverProp.mModel)
    moverProp.mModel = DUMMY_MODEL_FOR_SCRIPT
    IF bClearPositionData
        moverProp.vPos = <<0,0,0>>
        moverProp.vRot= <<0,0,0>>
        moverProp.fZoffset = 0
    ENDIF
    bMoverCreated = FALSE
	bMoverPtfxCreated = FALSE
    
    //Reset shapetest data
    bLastResult = FALSE
    vLastCheckedShapeTestPos = <<0,0,-1000>>
    vOldMoverPosition = <<0,0,0>>
    fOldMoverHeading = 0
    
    //Reset help data
    sPropEditorHelpString = "RemovePropMenuHelp"
    iLastResultTimer = 0
ENDPROC

FUNC INT DMPROP_GET_PROP_TYPE_INDEX(DM_PROP prop)
    INT i
    REPEAT ciMAXTYPES i
        IF ePropTypes[i].model = prop.mModel
            RETURN i
        ENDIF
    ENDREPEAT
    
    RETURN -1
ENDFUNC
FUNC INT DMPROP_GET_PROP_CATEGORY_POSITION(enum_Category category, MODEL_NAMES model)
    INT i, num = 0
    REPEAT ciMAXTYPES i
        IF ePropTypes[i].category = category
            IF ePropTypes[i].model = model
                RETURN num  //We've found the model in the category
            ENDIF
            num += 1    //Advance the category index
        ENDIF
    ENDREPEAT
    RETURN -1   //Should not get here
ENDFUNC

FUNC TEXT_LABEL_15 DMPROP_GET_NTH_TYPE_LABEL(INT iCategory, INT iNum)  //This probably doesn't need to spam either if it was mapped to input presses - Steve T.


    //Need to check what's getting passed in here as the iNum parameter.

    IF b_DoNotProcessNthLabel = TRUE

        TEXT_LABEL_15 tlTemp

        tlTemp = ePropTypes[iCurrentProp].name

        RETURN tlTemp

    ENDIF

//    CPRINTLN(debug_director,"DMPROP_GET_NTH_TYPE_LABEL running cat=",iCategory," num=",iNum," current=", iCurrentProp)

    INT i = 0
    TEXT_LABEL_15 tlTemp = ""
    WHILE iNum >= 0
        IF ePropTypes[i].category = INT_TO_ENUM(enum_Category,iCategory)
            iNum -= 1
            IF iNum <0
                iCurrentProp = i
                
                //Apply dynamic colouring
                IF ePropTypes[i].bFrozen
                    IF iNumProps = ciMAXOBJECTS
					OR  ePropWarning = PW_RestrictedUse
                        SET_MENU_ROW_TEXT_COLOUR(ciPIPlaceType,HUD_COLOUR_RED,HUD_COLOUR_RED,TRUE)
                    ELSE
                        SET_MENU_ROW_TEXT_COLOUR(ciPIPlaceType,HUD_COLOUR_BLUEDARK,HUD_COLOUR_BLUE,TRUE)
                    ENDIF
                ELSE
                    IF iNumDynamicProps = ciMAXDYNAMICOBJECTS
                    OR iNumProps = ciMAXOBJECTS
					OR  ePropWarning = PW_RestrictedUse
                        SET_MENU_ROW_TEXT_COLOUR(ciPIPlaceType,HUD_COLOUR_RED,HUD_COLOUR_RED,TRUE)
                    ELSE
                        SET_MENU_ROW_TEXT_COLOUR(ciPIPlaceType,HUD_COLOUR_GREENDARK,HUD_COLOUR_GREEN,TRUE)
                    ENDIF
                ENDIF
                
                tlTemp = ePropTypes[i].name
                RETURN tlTemp
            ENDIF
        ENDIF
        i += 1
        IF i >= ciMAXTYPES
            i = 0
        ENDIF
    ENDWHILE
    RETURN tlTemp
ENDFUNC
        

PROC DMPROP_NEXT(INT iRow, INT iteration = 0)

    IF ePEState = PES_PropPlace
    PLAY_SOUND_FRONTEND (-1, "Cycle_Item", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
        IF iRow = 0    //Categories
            eSelectedPropType[0] += 1
            CPRINTLN(debug_director, "Prop category moving to ", eSelectedPropType[0])
            IF eSelectedPropType[0] >= ENUM_TO_INT(CAT_MAX)
                eSelectedPropType[0] = ENUM_TO_INT(CAT_Null) + 1
                CPRINTLN(debug_director, "Prop category resetting to ", eSelectedPropType[0])
            ENDIF
			
			//Empty category check - recursive search
			IF ePropTypesPerCategory[eSelectedPropType[0]] <= 0 AND iteration < 10
				DMPROP_NEXT(iRow, iteration +1)
			ENDIF
			
            //Reset category
            eSelectedPropType[1] = 0
        ELIF iRow = 1   //Type
            eSelectedPropType[1] += 1

            CPRINTLN(debug_director, "Prop type moving to ", eSelectedPropType[1])



            //This line was screwing up the selection of props in a category: Steve T.
            //IF eSelectedPropType[1] >= ePropTypesPerCategory[eTypeCategory[eSelectedPropType[0]]]


            //This is what it should be. Steve T.
            IF eSelectedPropType[1] >= ePropTypesPerCategory[eSelectedPropType[0]]
                
                eSelectedPropType[1] = 0
                CPRINTLN(debug_director, "Prop type resetting to ", eSelectedPropType[1])

            ENDIF

        ENDIF
         //Destroy the existing mover
        DMPROP_CLEAR_MOVER_DATA(FALSE)

    ENDIF

ENDPROC

PROC DMPROP_PREV(INT iRow, INT iteration = 0)

    IF ePEState = PES_PropPlace
    PLAY_SOUND_FRONTEND (-1, "Cycle_Item", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
    IF iRow = 0     //Categories
        eSelectedPropType[0] -= 1
        IF eSelectedPropType[0] <= ENUM_TO_INT(CAT_Null)
            eSelectedPropType[0] = ENUM_TO_INT(CAT_MAX) - 1
        ENDIF
		
		//Empty category check - recursive search
		IF ePropTypesPerCategory[eSelectedPropType[0]] <= 0 AND iteration < 10
			DMPROP_PREV(iRow, iteration +1)
		ENDIF
		
        //Reset category
        eSelectedPropType[1] = 0
    ELIF iRow = 1   //Type
        eSelectedPropType[1] -= 1
        IF eSelectedPropType[1] < 0 
            eSelectedPropType[1] = ePropTypesPerCategory[eSelectedPropType[0]] - 1
        ENDIF
    ENDIF
    //Destroy the existing mover
    DMPROP_CLEAR_MOVER_DATA(FALSE)
    ENDIF
ENDPROC

FUNC STRING DMPROP_GET_TYPE_LABEL_FOR_MODEL(MODEL_NAMES mModel)
    STRING temp = ""
    INT i
    FOR i = 0 to ciMAXTYPES - 1
        IF ePropTypes[i].model = mModel
            temp = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(ePropTypes[i].name, GET_LENGTH_OF_LITERAL_STRING(ePropTypes[i].name))
            RETURN temp
        ENDIF
    ENDFOR
    RETURN ""
ENDFUNC



FUNC STRING DMPROP_GET_PROP_NAME_AT(INT iIndex)
    DM_PROP prop = DMPROP_GET_NTH_PROP(iIndex)
    IF DOES_ENTITY_EXIST(prop.obj)
    OR prop.mModel != DUMMY_MODEL_FOR_SCRIPT
        RETURN DMPROP_GET_TYPE_LABEL_FOR_MODEL(prop.mModel)
    ENDIF
    RETURN "ERROR"
ENDFUNC

FUNC BOOL DMPROP_IS_PROP_IN_BLOCKED_AREA(DM_PROP prop)
    //Specific check for Ferris Wheel clipping as noted in //2503752
    IF IS_ENTITY_IN_ANGLED_AREA(prop.obj, <<-1664.3,-1146.7, 11.0>>, <<-1663.7,-1107.1, 50.2>>, 15.0) //2503752
        RETURN TRUE
    //Michael's house doors
    ELIF VDIST2(<<-816.46680, 178.30634, 71.57366>>, prop.vpos) < 3
    OR VDIST2(<<-793.75677, 181.53970, 73.10931>>, prop.vpos) < 3.5
    OR VDIST2(<<-795.52295, 177.58992, 73.07761>>, prop.vpos) < 3.5
        RETURN TRUE
    ENDIF

    RETURN FALSE
ENDFUNC

#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT

FUNC BOOL DMPROP_IS_PROP_SPECIALRACE_CURVED_TRACK(int iPropIndex)

	IF ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U5"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U15"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U30"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U45"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D5"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D15"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D30"))
	OR  ePropTypes[iPropIndex].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D45"))
		RETURN FALSE //TRUE - Redacting this so we can allow these props to snap again. Whilst we get the odd issue, we'd have to take more action to prevent other props snapping to these.
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC


/// PURPOSE:
 ///    This checks if the given prop index is a Stunt prop
 FUNC BOOL DMPROP_IS_PROP_A_STUNT_PROP(INT iPropIndex, BOOL includeDynamic = FALSE)
	 	RETURN (ePropTypes[iPropIndex].category = CAT_StuntRamps)
		OR (ePropTypes[iPropIndex].category = CAT_StuntTubes)
		OR (ePropTypes[iPropIndex].category = CAT_StuntTrack)
		OR (ePropTypes[iPropIndex].category = CAT_StuntTyres)
		OR (ePropTypes[iPropIndex].category = CAT_StuntMisc)
		OR (ePropTypes[iPropIndex].category = CAT_StuntWalls)
		OR (ePropTypes[iPropIndex].category = CAT_StuntExtraTrk)
		OR (ePropTypes[iPropIndex].category = CAT_StuntBlocks)
		OR (ePropTypes[iPropIndex].category = CAT_StuntDynamic AND includeDynamic)	
 ENDFUNC
#ENDIF





FUNC BOOL IsInteriorPlacementRestricted(INT passedGroupID)

    /*
    We will allow props to be placed in exempted interiors.

    Trevor, Michael Safehouse                               Interior group ID 0
    Fleeca Band                                             Interior group ID 0
    Ponsonby's                                              Interior group ID 0
    Road Tunnel at -2602.5, 3005.5, 16.6                    Interior group ID 12    - Exempted!
    Road Tunnel in bug 2506996 at -1148.5, -640.7, 14.6     Interior group ID 5     - Exempted!

    Subway near Michael's house                             Interior group ID 1.
    Underground Car park (Bug 2539501)                      Interior group ID 18   - Exempted!
    Senora Freeway tunnel (Bug 2539465)                     Interior group ID 9    - Exempted!
    */

    IF passedGroupID = 12
    OR passedGroupID = 5
    OR passedGroupID = 18
    OR passedGroupID = 9
        RETURN FALSE
    ELSE
        RETURN TRUE
    ENDIF

ENDFUNC



/// PURPOSE:
///    Checks if the mover object is outside the map area
/// RETURNS:
///    
FUNC BOOL DMPROP_IS_MOVER_OUTSIDE_BOUNDS()
	VECTOR position = moverProp.vPos
	IF DOES_ENTITY_EXIST(moverProp.obj)
		position = GET_ENTITY_COORDS(moverProp.obj)
	ENDIF
	
	IF position.x < -5000 OR position.x > 6000
	OR position.y < -7000 OR position.y > 10000
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clamps the mover's location to within a certain range around the map
PROC DMPROP_CLAMP_MOVER_POSITION()
	//Clamp the prop position to around the main map
	moverProp.vPos.x = CLAMP(moverProp.vPos.x, -5000, 6000)
	moverProp.vPos.y = CLAMP(moverProp.vPos.y, -7000, 10000)
ENDPROC



//Function that checks if the area around the mover is clear.
FUNC BOOL DMPROP_IS_MOVER_CLEAR_TO_PLACE_PROP(DM_PROP &eMover)
    VECTOR vHitPos,vHitNormal
    //MATERIAL_NAMES HitMaterial //Placeholder work for bug 2450679 - static props are ghosted through by trains. 
    FLOAT fGroundZ
    ENTITY_INDEX eHitEntity
    SHAPETEST_STATUS stStatus
    BOOL bTimerReadToReturn = (GET_GAME_TIMER() > iLastResultTimer + 100)
    INT ThisCategoryIndex = DMPROP_GET_PROP_TYPE_INDEX(eMover)  //If the Mover is not a water category prop. Return false - we can't place these on water.
    INTERIOR_INSTANCE_INDEX currentInterior //bug 2496767

    //Sanity check: no check needed if the mover has no model or object
    IF eMover.mModel = DUMMY_MODEL_FOR_SCRIPT
    OR NOT DOES_ENTITY_EXIST (eMover.obj)
        RETURN FALSE
    ENDIF
    
	//Bounds check: Is it outside the map area?
	IF DMPROP_IS_MOVER_OUTSIDE_BOUNDS()
		ePropWarning = PW_RestrictedArea
		RETURN FALSE
	ENDIF
	

    IF ThisCategoryIndex > -1 //Fix for array overrun noted in 2502458.
    
        GET_MODEL_DIMENSIONS(eMover.mModel,vModelMin,vModelMax)

        vTestLastPos = eMover.vPos + eMover.vRot + <<0,0,emover.fZoffset>>

        IF b_MoverPropOverWater AND ePropTypes[ThisCategoryIndex].category != CAT_Water
        AND NOT (ePropTypes[ThisCategoryIndex].fIgnoreZ > 1.0 AND NOT ePropTypes[ThisCategoryIndex].bPlaceOnGround AND NOT ePropTypes[ThisCategoryIndex].bFrozen) //Allow prop placement of floating props
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
		AND NOT DMPROP_IS_PROP_A_STUNT_PROP(ThisCategoryIndex) //Allow Stunt props to be placed over water. 3189734
		#ENDIF
            CPRINTLN(debug_director, "DMPROP: Trying to place non-water prop over water, disabling")  
            ePropWarning = PW_LandPropOnWater
            RETURN FALSE
        ELIF (NOT b_MoverPropOverWater) AND ePropTypes[ThisCategoryIndex].category = CAT_Water
            CPRINTLN(debug_director, "DMPROP: Trying to place water prop on land, disabling")        
            ePropWarning = PW_WaterPropOnLand
            RETURN FALSE
        ENDIF


        //Restricting the number of problematic log and pipe pile props that can be placed for 2499306 and 2503897
        IF IsPropModelRestrictedUse(eMover.mModel)
            //CPRINTLN(debug_director, "DMPROP: Currently selected mover is a restricted use prop. Current number placed is ", iNumRestrictedUseProps)
            IF iNumRestrictedUseProps >= c_i_RestrictedUsePropLimit 
                ePropWarning = PW_RestrictedUse
                RETURN FALSE
            ENDIF
        ENDIF
        

        //Do not allow prop placement in interior 2496767
        
        IF DOES_ENTITY_EXIST(MoverProp.obj)
            currentInterior = GET_INTERIOR_FROM_ENTITY(MoverProp.obj)
            IF IS_VALID_INTERIOR(currentInterior)
                //CPRINTLN(debug_director,"Update Mover Prop says mover is in interior")
                CPRINTLN(debug_director,"Update Mover Prop says mover is in interior of group ID ", GET_INTERIOR_GROUP_ID (currentInterior))
                
                IF IsInteriorPlacementRestricted(GET_INTERIOR_GROUP_ID (currentInterior)) //Some interiors such as road tunnels are okay to place props in.

					#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT 
					IF NOT DMPROP_IS_PROP_A_STUNT_PROP(ThisCategoryIndex)
					#ENDIF
					
                    	ePropWarning = PW_PropInInterior
                    	RETURN FALSE
					
					#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT 
					ENDIF
					#ENDIF

                ENDIF

            ENDIF
        ENDIF
        

        //  Water depth check
        IF ePropTypes[ThisCategoryIndex].category = CAT_Water
            IF NOT GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD( eMover.vPos, fGroundZ )
                fGroundZ = -200
            ENDIF
            GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD( eMover.vPos, fWaterZ, TRUE)
            FLOAT fWaterDepth = fWaterZ - fGroundZ 
            FLOAT fObjectHeight =vModelMax.z - vModelMin.z
            IF ePropTypes[ThisCategoryIndex].model = PROP_BYARD_FLOAT_02
                fObjectHeight = 2.0     //Extra minimum height for the platforms
            ENDIF
    //      DISPLAY_TEXT_WITH_FLOAT(0.4,0.7,"NUMBER",fWaterDepth,2)
    //    DISPLAY_TEXT_WITH_FLOAT(0.4,0.6,"NUMBER",fGroundZ,2)
            
            IF ( fObjectHeight / 2.0 ) > fWaterDepth
                CDEBUG1LN( DEBUG_DIRECTOR, " - Prop bigger than the water height. " )
                ePropWarning = PW_PropBiggerThanWaterDepth
                RETURN FALSE
            ENDIF
            
            //Water prop on top of object check
            IF fWaterZ < GET_SURFACE_Z_BELOW(eMover.vPos,FALSE)
                ePropWarning = PW_WaterPropOnLand
                RETURN FALSE
            ENDIF
        ENDIF
    
    ELSE

        CPRINTLN(debug_director, "Warning DMPROP_IS_MOVER_CLEAR_TO_PLACE_PROP reports ThisCategoryIndex less than zero!")
        RETURN FALSE
         
    ENDIF
    
    //Special restricted areas
    IF DMPROP_IS_PROP_IN_BLOCKED_AREA(moverProp)
        bLastResult = FALSE
        ePropWarning = PW_RestrictedArea
        RETURN FALSE
    ENDIF

    FLOAT fPrecision = 0.1
    IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.1
        fPrecision += 0.3
        bTimerReadToReturn = (GET_GAME_TIMER() > iLastResultTimer + 100) //Reduce the wait timer
    ENDIF
    
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT 
	//Quick fix for placement test, ignore collision and ground checks for the Stunt Ramps and Tubes categories. Stunt Dynamic will still perform a shapetest however.
  	IF DMPROP_IS_PROP_A_STUNT_PROP(ThisCategoryIndex)
		//DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.64, "STRING", "Stunt Prop")
		ePropWarning = PW_Fine //Fix for 3199888 and 3199245
		//iLastResultTimer = 0 //No longer required as we've moved the ARE_VECTORS_ALMOST_EQUAL check within the next ELSE condition.
		RETURN TRUE
    ELSE
	#ENDIF
	
	
		//If the mover hasn't moved, return the last test
    	IF ARE_VECTORS_ALMOST_EQUAL(vTestLastPos,vLastCheckedShapeTestPos,fPrecision)
       	 RETURN (bLastResult AND bTimerReadToReturn)
    	ELSE
        	ePropWarning = PW_NotReady
        	iLastResultTimer = GET_GAME_TIMER() + 150
    	ENDIF
	

        //Create shape test if not created
        IF siTest = NULL
            siTest = START_SHAPE_TEST_BOUND(eMover.obj,LOS_FLAGS_BOUNDING_BOX|SCRIPT_INCLUDE_MOVER)
        ELSE                                
            stStatus = GET_SHAPE_TEST_RESULT(siTest,iSTResult,vHitPos,vHitNormal,eHitEntity)
            //stStatus = GET_SHAPE_TEST_RESULT_INCLUDING_MATERIAL(siTest,iSTResult,vHitPos,vHitNormal, HitMaterial, eHitEntity) //Placeholder work for bug 2450679 - static props are ghosted through by trains.
            IF stStatus = SHAPETEST_STATUS_RESULTS_READY

                //Placeholder work for bug 2450679 - static props are ghosted through by trains. Having trouble getting this recognised reliably with the floating mover and might be more expensive than it's worth.
                /*
                IF (HitMaterial = TRAIN_TRACKS_RAILS)
                OR HitMaterial = TRAIN_TRACKS_GRAVEL
                OR HitMaterial = TRAIN_TRACKS_GRAVEL_2
                    CPRINTLN( DEBUG_DIRECTOR, "Hitting train tracks")  
                ENDIF
                */

                vLastCheckedShapeTestPos = vTestLastPos
                IF iSTResult = 0

                    //Below surface check
                    vHitPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eMover.obj, <<0,0,vModelMin.z>>)
                    IF vHitPos.z < GET_SURFACE_Z_BELOW(vHitPos)
                    AND NOT (ePropTypes[ThisCategoryIndex].category = CAT_Water)
                        bLastResult = FALSE
//                        CPRINTLN(debug_director, "Shape Test centre point failed, below ground.",vHitPos.z - fGroundZ)
                        ePropWarning = PW_AboveGround
                        RETURN FALSE
                    ENDIF
                    
                    //Centre point ground check, only allow if mover is very close to ground
                    GET_GROUND_Z_FOR_3D_COORD(vHitPos+<<0,0,3>>,fGroundZ)   //Reduced the additional Z component to 3 from 10 after discussion with Dan. See bug 2411412
                    fGroundZ = FMAX(fGroundZ,FMAX(fWaterZ,0))   //Don't place props under the water surface
                    IF absf(vHitPos.z - GET_SURFACE_Z_BELOW(vHitPos)) > ePropTypes[ThisCategoryIndex].fIgnoreZ  //Disable past a certain height
                        bLastResult = FALSE
                        ePropWarning = PW_AboveGround

//                        CPRINTLN(debug_director, "Shape Test centre point failed.",vHitPos.z - fGroundZ)

                        RETURN bLastResult
                    ENDIF

                    vModelMin.x *= ePropTypes[ThisCategoryIndex].fGroundScale
                    vModelMin.y *= ePropTypes[ThisCategoryIndex].fGroundScale
                    vModelMax.x *= ePropTypes[ThisCategoryIndex].fGroundScale
                    vModelMax.y *= ePropTypes[ThisCategoryIndex].fGroundScale
                    //Only allow if mover is very close to ground, check all 4 corners of the ground Z
                    vHitPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eMover.obj, <<vModelMin.x,vModelMin.y,vModelMin.z>>)
                    GET_GROUND_Z_FOR_3D_COORD(vHitPos+<<0,0,3>>,fGroundZ)   //Reduced the additional Z component to 3 from 10 after discussion with Dan. See bug 2411412
                    fGroundZ = FMAX(fGroundZ,FMAX(fWaterZ,0))   //Don't place props under the water surface
                    IF absf(vHitPos.z - GET_SURFACE_Z_BELOW(vHitPos)) > ePropTypes[ThisCategoryIndex].fIgnoreZ  //Disable past a certain height
                        bLastResult = FALSE
                        ePropWarning = PW_AboveGround

//                        CPRINTLN(debug_director, "Shape Test Corner 1 failed.",vHitPos.z - fGroundZ)

                        RETURN bLastResult
                    ENDIF
                    vHitPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eMover.obj, <<vModelMax.x,vModelMin.y,vModelMin.z>>)
                    GET_GROUND_Z_FOR_3D_COORD(vHitPos+<<0,0,3>>,fGroundZ)
                    fGroundZ = FMAX(fGroundZ,FMAX(fWaterZ,0))   //Don't place props under the water surface
                    IF absf(vHitPos.z - GET_SURFACE_Z_BELOW(vHitPos)) > ePropTypes[ThisCategoryIndex].fIgnoreZ  //Disable past a certain height
                        bLastResult = FALSE
                        ePropWarning = PW_AboveGround

//                        CPRINTLN(debug_director, "Shape Test Corner 2 failed.",vHitPos.z - fGroundZ)

                        RETURN bLastResult
                    ENDIF
                    vHitPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eMover.obj, <<vModelMin.x,vModelMax.y,vModelMin.z>>)
                    GET_GROUND_Z_FOR_3D_COORD(vHitPos+<<0,0,3>>,fGroundZ)
                    fGroundZ = FMAX(fGroundZ,FMAX(fWaterZ,0))   //Don't place props under the water surface
                    IF absf(vHitPos.z - GET_SURFACE_Z_BELOW(vHitPos)) > ePropTypes[ThisCategoryIndex].fIgnoreZ  //Disable past a certain height
                        bLastResult = FALSE
                        ePropWarning = PW_AboveGround

//                        CPRINTLN(debug_director, "Shape Test Corner 3 failed.",vHitPos.z - fGroundZ)

                        RETURN bLastResult
                    ENDIF
                    vHitPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eMover.obj, <<vModelMax.x,vModelMax.y,vModelMin.z>>)
                    GET_GROUND_Z_FOR_3D_COORD(vHitPos+<<0,0,3>>,fGroundZ)
                    fGroundZ = FMAX(fGroundZ,FMAX(fWaterZ,0))   //Don't place props under the water surface
    //              CPRINTLN(debug_dan,"4th test: ",vHitPos.z,"/",fGroundZ)
                    IF absf(vHitPos.z - GET_SURFACE_Z_BELOW(vHitPos)) > ePropTypes[ThisCategoryIndex].fIgnoreZ  //Disable past a certain height
                        bLastResult = FALSE
                        ePropWarning = PW_AboveGround

//                        CPRINTLN(debug_director, "Shape Test Corner 4 failed.",vHitPos.z - fGroundZ)

                        RETURN bLastResult
                    ENDIF
                    
                    //If it gets here, Ground Z check for corners must have passed
                    ePropWarning = PW_Fine
                    bLastResult = TRUE
                ELSE
                    ePropWarning = PW_Colliding
                    bLastResult = FALSE
                ENDIF
            ELIF stStatus = SHAPETEST_STATUS_NONEXISTENT
                ePropWarning = PW_NotReady
                siTest = NULL
                bLastResult = FALSE
                vLastCheckedShapeTestPos = <<0,0,-1000>>
            ENDIF
        ENDIF
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT 
   	ENDIF //Closure of stunt type condition check for feature flag.
  	#ENDIF
    
    RETURN (bLastResult AND bTimerReadToReturn)
ENDFUNC


VECTOR vWaterCheckReturn //moved scope to here so the z component can be used for creating a splash effect on placement of a water prop. 
INT iPlacementSound

FUNC BOOL DMPROP_ADD_PROP()
    INT i = 0
    BOOL bFound = FALSE
	
	//Early-out if we're spamming Add too much (for stunt props mostly)
	IF GET_GAME_TIMER() < iPropPlaceTimer
		CLOGLN(DEBUG_DIRECTOR, CHANNEL_SEVERITY_DEBUG1, "DMPROP_ADD_PROP - Trying to add props too quickly, locking out")
		RETURN FALSE
	ENDIF
		
    //Exit out if the position for placement isn't safe
    IF NOT DMPROP_IS_MOVER_CLEAR_TO_PLACE_PROP(moverProp)
        CPRINTLN(debug_director,"DMPROP_ADD_PROP - Tried to add an object at an unsafe position")
        //PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
        PLAY_SOUND_FRONTEND (-1, "Place_Prop_Fail", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
		RETURN FALSE
    ENDIF
    
    INT ThisCategoryIndex = DMPROP_GET_PROP_TYPE_INDEX(moverProp)
    //Check if this is a prop replacement
    IF iForceReplaceProp <> -1
        i = iForceReplaceProp
        DELETE_OBJECT(eProps[i].obj)
        eProps[i].mModel = DUMMY_MODEL_FOR_SCRIPT
        bFound = TRUE
        iForceReplaceProp = -1
    ELSE



         //2416882 - Check if this mover prop we are attempting to place is Dynamic and return FALSE if we have reached the Dynamic Prop limit.
         IF NOT ePropTypes[ThisCategoryIndex].bFrozen
            CPRINTLN(debug_director,"Prop to add is included in the DYNAMIC Category ", ThisCategoryIndex)
            IF iNumDynamicProps = ciMAXDYNAMICOBJECTS
                CPRINTLN(debug_director,"Can't add new DYNAMIC prop, DYNAMIC limit reached")
                PLAY_SOUND_FRONTEND (-1, "Place_Prop_Fail", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2525333

                RETURN FALSE
            ENDIF
         ENDIF
         

        //Quit out if already at object limit
        IF iNumProps = ciMAXOBJECTS
            CPRINTLN(debug_director,"Can't add new prop, limit reached")
            PLAY_SOUND_FRONTEND (-1, "Place_Prop_Fail", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2525333

            RETURN FALSE
        ENDIF
        WHILE i < ciMAXOBJECTS AND !bFound
            IF NOT DOES_ENTITY_EXIST(eProps[i].obj)
            AND DOES_ENTITY_EXIST(moverProp.obj)
                bFound = TRUE
                //Increment number of props
                iNumProps += 1

                //2416882  - also need to increment the dynamic sub-total.
                IF NOT ePropTypes[ThisCategoryIndex].bFrozen
                    CPRINTLN(debug_director,"Adding new DYNAMIC prop to dynamic total at ", i)
                    iNumDynamicProps += 1
                ENDIF   

                IF IsPropModelRestrictedUse(ePropTypes[ThisCategoryIndex].model)
                    iNumRestrictedUseProps += 1
                    CPRINTLN(debug_director,"Adding new RESTRICTED USE prop to restricted use. Total at ", iNumRestrictedUseProps)
                ENDIF

            ELSE
                i+=1
            ENDIF
        ENDWHILE
    ENDIF
        

    IF bFound
        CPRINTLN(debug_director,"Adding prop at ",i)
		
		iPropPlaceTimerMin = iPropPlaceTimerMin		//Fix for compile without the _STUNT flag active
		
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
		IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp)
			//This adds a min 200ms + 3X prop size (around 1.5 seconds max) when placing really large snapped props
			iPropPlaceTimer = GET_GAME_TIMER() + iPropPlaceTimerMin + FLOOR(FMAX(moverProp.vSize.x, moverProp.vSize.y) * 3) 
//			CPRINTLN(debug_dan,"New place timer: ",iPropPlaceTimerMin + FMAX(moverProp.vSize.x, moverProp.vSize.y) * 3)
		ENDIF
		#ENDIF
		
        //Copy mover data into props
        eProps[i] = moverProp
        moverProp.obj = NULL
        DMPROP_CLEAR_MOVER_DATA(FALSE)
        
        //Disable low lod buoyancy
        INT PropTypeIndex = DMPROP_GET_PROP_TYPE_INDEX(eProps[i])          
        IF ePropTypes[PropTypeIndex].category = CAT_Water
            SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY(eProps[i].obj,FALSE)
//            CPRINTLN(debug_dan,"Disabled low lod buoyancy")
        ENDIF
        
        //Only keep in air if it's not frozen and the zOffset !=0, place on ground otherwise
        IF ePropTypes[ThisCategoryIndex].bPlaceOnGround
            PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(eProps[i].obj)
        ENDIF

        eProps[i].vPos = GET_ENTITY_COORDS(eProps[i].obj)
        eProps[i].vRot = GET_ENTITY_ROTATION(eProps[i].obj)
        
        //Now the object has been placed, perform any ground adjustment from the prop sheet to account for any model setup weirdness. See #2412969                
        SET_ENTITY_COORDS (eProps[i].obj, <<eProps[i].vPos.x, eProps[i].vPos.y, eProps[i].vPos.z + ePropTypes[PropTypeIndex].fGroundAdjust>>)
        


        //SFX volume adjustment scaling - we may want to get the model dimensions here if we get feedback on large flat props. Currently we're just using the height as a scale guide.

        FLOAT tempPropSizeVolumeScale = 0.0

        IF ePropTypes[PropTypeIndex].fMarkerAdjust > 1.5

            tempPropSizeVolumeScale = 0.33  //Small prop

        ENDIF


        IF ePropTypes[PropTypeIndex].fMarkerAdjust > 3.2

            tempPropSizeVolumeScale = 0.66 //Medium Prop

        ENDIF

        
        IF ePropTypes[PropTypeIndex].fMarkerAdjust > 6.5

            tempPropSizeVolumeScale = 0.99 //Large Prop

        ENDIF



        IF ePropTypes[PropTypeIndex].category = CAT_Water
        OR b_MoverPropOverWater //2520972

			#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			IF DMPROP_IS_PROP_A_STUNT_PROP (PropTypeIndex) //3196186
		
				CPRINTLN(debug_director, "Mover over water but not creating splash as this prop is a stunt prop")
				
			ELSE
			#ENDIF
			
            	USE_PARTICLE_FX_ASSET("scr_mp_creator")  
            
            	//Need to check place on ground bool here
            	START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_splash", <<eProps[i].vPos.x, eProps[i].vPos.y, vWaterCheckReturn.z>>, <<0,0,0>>,
            	ePropTypes[PropTypeIndex].fPfxScale) 

            	iPlacementSound = GET_SOUND_ID()

            	CPRINTLN(debug_director, "Setting Prop_Drop_Water PropSize variable as ", tempPropSizeVolumeScale)

            	PLAY_SOUND_FROM_ENTITY(iPlacementSound, "Prop_Drop_Water", eProps[i].obj, "DLC_Dmod_Prop_Editor_Sounds")
            	SET_VARIABLE_ON_SOUND(iPlacementSound, "PropSize", tempPropSizeVolumeScale) //These need to be set AFTER the sound is played. See 2498902. 
            	RELEASE_SOUND_ID (iPlacementSound)
			#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			ENDIF
			#ENDIF

        ELSE

            //Need to check place on ground bool here to make sure that props which can be placed in the air do not generate a dust cloud.
            IF ePropTypes[PropTypeIndex].bPlaceOnGround
                USE_PARTICLE_FX_ASSET("scr_mp_creator")  

                START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_dust_cloud", <<eProps[i].vPos.x, eProps[i].vPos.y, eProps[i].vPos.z + ePropTypes[PropTypeIndex].fGroundAdjust>>, <<0,0,0>>, 
                ePropTypes[PropTypeIndex].fPfxScale) //Last parameter is the scale adjustment. If the marker is high above the prop then we can assume it's quite large.
            ENDIF
        
            iPlacementSound = GET_SOUND_ID()

            CPRINTLN(debug_director, "Setting Prop_Drop_Land PropSize variable as ", tempPropSizeVolumeScale)

            PLAY_SOUND_FROM_ENTITY(iPlacementSound, "Prop_Drop_Land", eProps[i].obj, "DLC_Dmod_Prop_Editor_Sounds")
            SET_VARIABLE_ON_SOUND(iPlacementSound, "PropSize", tempPropSizeVolumeScale) //These need to be set AFTER the sound is played. See 2498902.
            RELEASE_SOUND_ID (iPlacementSound)

        ENDIF
          
            
        //Save out the adjusted position again.
        eProps[i].vPos = GET_ENTITY_COORDS(eProps[i].obj)

        SET_ENTITY_COLLISION(eProps[i].obj,TRUE)    
        RESET_ENTITY_ALPHA(eProps[i].obj)

//        //Special mover conditions
//        IF NOT ePropTypes[ThisCategoryIndex].bFrozen
//            FREEZE_ENTITY_POSITION(eProps[i].obj,FALSE) //Only unfreeze certain objects
//            ACTIVATE_PHYSICS(eProps[i].obj)
//        ENDIF
        
//        SET_ENTITY_INVINCIBLE( eProps[i].obj, FALSE )
//        SET_ENTITY_PROOFS( eProps[i].obj, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE )
        
        //Reset shape test location
        vLastCheckedShapeTestPos = <<0,0,-1000>>

        //PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
        //Initially removed this sound in favour of distinct scalable water and splash effects from the entity as requested in 2498902 but it was requested back in again!
        PLAY_SOUND_FRONTEND (-1, "Place_Prop_Success", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537


        //Prototype Test work on disabling nearby road nodes when placing props for bug 2503514
        /*
        #if IS_DEBUG_BUILD
            IF g_Cellphone_Onscreen_State_Debug = TRUE //Just use temp widget for quickness.

                IF IS_POINT_ON_ROAD(<<eProps[i].vPos.x, eProps[i].vPos.y, eProps[i].vPos.z + ePropTypes[PropTypeIndex].fGroundAdjust>>, NULL)
                    CPRINTLN(debug_director, "Just Placed Prop returns TRUE from IS_POINT_ON_ROAD_CHECK.")
                
                    VECTOR tempRoadVec1
                    VECTOR tempRoadVec2
                    tempRoadVec1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eProps[i].obj, <<10.0, 10.0, 5.0>>) 
                    tempRoadVec2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eProps[i].obj, <<-10.0, -10.0,-5.0>>) 

                    SET_ROADS_IN_ANGLED_AREA(tempRoadVec1, tempRoadVec2, 15.0, FALSE, FALSE, FALSE)
                    
                ELSE
                    CPRINTLN(debug_director, "Just Placed Prop not on road.")
                ENDIF

            ENDIF    
        #endif 
        */

               
        eProps[i].BlipIndex = ADD_BLIP_FOR_ENTITY(eProps[i].obj) //Work on 2485910
        SET_BLIP_NAME_FROM_TEXT_FILE( eProps[i].BlipIndex, GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL( eProps[i].mModel ) )
        SET_BLIP_COLOUR (eProps[i].BlipIndex, BLIP_COLOUR_PURPLEDARK)
        SET_BLIP_SCALE (eProps[i].BlipIndex, 0.75)
        SHOW_HEIGHT_ON_BLIP (eProps[i].BlipIndex, FALSE)

    
        b_PropSceneUpdatedSinceLastSave = TRUE


        RETURN TRUE
    ELSE
        CPRINTLN(debug_director, "Tried adding a prop but no array index found")
    ENDIF
    RETURN FALSE
ENDFUNC

PROC DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()

    v_MarkerDraw = <<-99.9, -99.9, -99.9>>

    //Delete the blip
    //work on 2485910
    /*
    IF DOES_BLIP_EXIST(blSelected)
        REMOVE_BLIP(blSelected)
        DM_PROP temp = DMPROP_GET_NTH_PROP(iBlippedProp)
        IF DOES_ENTITY_EXIST(temp.obj)
            //Reset alpha
            RESET_ENTITY_ALPHA(temp.obj)
        
        ENDIF
        iBlippedProp = -1
    ENDIF
    */

    //work on 2485910
    IF iBlippedProp >= 0

        CPRINTLN(debug_director, "Unlocking minimap position")
        //UNLOCK_MINIMAP_POSITION() - Removed to fix 2498709. The map position is now only unlocked in DMPROP_UPDATE_PI_MENU when the player's selection is not on a placed prop row.
  
        CPRINTLN(debug_director, "DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP has been called on blipped prop ", iBlippedProp)
        DM_PROP temp = DMPROP_GET_NTH_PROP(iBlippedProp)

        IF DOES_BLIP_EXIST(temp.BlipIndex)
            SET_BLIP_COLOUR (temp.BlipIndex, BLIP_COLOUR_PURPLEDARK)
            SET_BLIP_SCALE (temp.BlipIndex, 0.75)

            IF DOES_ENTITY_EXIST(temp.obj)
                //Reset alpha
                RESET_ENTITY_ALPHA(temp.obj)
            
            ENDIF
            iBlippedProp = -1                                                                                                                                                       
        ENDIF

    ENDIF


ENDPROC

//Sets other nearby props safely on the ground
PROC DMPROP_SET_NEARBY_PROPS_SAFELY_ON_GROUND(VECTOR vPos, MODEL_NAMES mModel = DUMMY_MODEL_FOR_SCRIPT)
    INT i, ind, iType
    FLOAT fRadius = 10.0
    VECTOR vMin, vMax = <<0,0,0>>
    IF mModel != DUMMY_MODEL_FOR_SCRIPT
        GET_MODEL_DIMENSIONS(mModel,vMin,vMax)
        vMin = vMax - vMin
        fRadius = FMAX(vMin.x,FMAX(vMin.y,vMin.z))
    ENDIF
//    CPRINTLN(debug_director,"Checking ",vPos," range ",fRadius, " above Z ",vMax.z/2+vPos.z)
    REPEAT iNumProps i
        ind = DMPROP_GET_NTH_PROP_INDEX(i)
        IF DOES_ENTITY_EXIST(eProps[ind].obj)
//          CPRINTLN(debug_director,"Prop ",i," pos ",eProps[ind].vPos)
//            IF eProps[ind].vPos.z > vPos.z + vMax.z/2 
                vMin = eProps[ind].vPos     
                vMin.z = 0              //Ignore Z difference
                vPos.z = 0
                IF VDIST2(vPos, vMin) < fRadius*fRadius
                    iType = DMPROP_GET_PROP_TYPE_INDEX(eProps[ind])
                    IF ePropTypes[iType].bPlaceOnGround
                        SET_ENTITY_COMPLETELY_DISABLE_COLLISION(eProps[ind].obj,FALSE)
                        PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(eProps[ind].obj)
                        eProps[ind].vPos = GET_ENTITY_COORDS(eProps[ind].obj)
                        eProps[ind].vRot = GET_ENTITY_COORDS(eProps[ind].obj)
                        CPRINTLN(debug_director,"Post-delete: placed ",i," on ground")
                        SET_ENTITY_COLLISION(eProps[ind].obj, TRUE)
                    ELSE
                        ACTIVATE_PHYSICS(eProps[ind].obj)
                    ENDIF
                ENDIF
//            ENDIF
        ENDIF
    ENDREPEAT

ENDPROC

PROC DMPROP_DELETE_PROP(INT iCurrentSel)

    CPRINTLN(debug_director, "DMPROP_DELETE_PROP has been passed ", iCurrentSel,"/",iNumProps)


    INT ind = DMPROP_GET_NTH_PROP_INDEX(icurrentSel)
    
    IF ind > -1
    
    IF DOES_ENTITY_EXIST(eProps[ind].obj)
        //Delete the object and reset its properties
        INT ThisPropToDeleteIndex = DMPROP_GET_PROP_TYPE_INDEX(eProps[ind])
           
        //2416882 
        IF NOT ePropTypes[ThisPropToDeleteIndex].bFrozen
            CPRINTLN(debug_director,"Prop to DELETE is included in the DYNAMIC Category - decrementing iNumDynamicProps ")
            iNumDynamicProps -=1
        ENDIF


        IF IsPropModelRestrictedUse(ePropTypes[ThisPropToDeleteIndex].model)
            iNumRestrictedUseProps -= 1
            CPRINTLN(debug_director,"Deleting RESTRICTED USE prop. Total at ", iNumRestrictedUseProps)
        ENDIF



        
        VECTOR vOldPos = GET_ENTITY_COORDS(eProps[ind].obj)
        MODEL_NAMES mOldModel = eProps[ind].mModel
        
        CPRINTLN(debug_director,"Clear_Blip Spam Test 1")
        DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()

        DELETE_OBJECT(eProps[ind].obj)
		SAFE_RELEASE_MODEL(eProps[ind].mModel)
        eProps[ind].fZoffset = 0
        eProps[ind].vPos = <<0,0,0>>
        eProps[ind].mModel = DUMMY_MODEL_FOR_SCRIPT
        iNumProps -=1

        iLastPISelection = -99 //Force Update of blip in DMPROP_UPDATE_PI_MENU()
        iLastPropSelectionForChevronMarker = -1

        PLAY_SOUND_FRONTEND (-1, "Delete_Placed_Prop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537

        b_PropSceneUpdatedSinceLastSave = TRUE
        
        DMPROP_SET_NEARBY_PROPS_SAFELY_ON_GROUND(vOldPos, mOldModel)
        
        //Go back to the scene edit menu if that was the last prop
        IF iNumProps = 0
            DMPROP_SET_STATE(PES_SceneEdit)
        ENDIF
    ENDIF

    ELSE

        CPRINTLN(debug_director, "Warning DMPROP_DELETE_PROP was passed ", ind) //2508317

    ENDIF

ENDPROC


/// PURPOSE:
///    Edits a prop by removing the object and setting the mover to that type
/// PARAMS:
///    iCurrentSel - the currently selected prop, sets it to the type selection line
/// RETURNS: TRUE if a prop was found/edited
///    
FUNC BOOL DMPROP_EDIT_PROP(INT &iCurrentSel)
    INT ind = DMPROP_GET_NTH_PROP_INDEX(iCurrentSel)

    CPRINTLN(debug_director, "DMPROP_EDIT_PROP has been passed ", iCurrentSel,"/",iNumProps)

    IF ind > -1

        IF DOES_ENTITY_EXIST(eProps[ind].obj)
        
            bEditingProp = TRUE

            b_DoNotProcessNthLabel = TRUE

            PLAY_SOUND_FRONTEND (-1, "Select_Placed_Prop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537

            //Set category and type
            INT iTypeIndex = DMPROP_GET_PROP_TYPE_INDEX(eProps[ind])

			//Set the old prop coordinates
            DMPROP_CLEAR_MOVER_DATA()
			moverProp.vPos = eProps[ind].vPos
			moverProp.vRot = eProps[ind].vRot


            //IF iTypeIndex > ENUM_TO_INT(CAT_Null) // Steve T. removed this. You shouldn't compare the type to greater than zero. This was causing a problem with being unable
                                                // to edit the default Red Plastic Barrier prop.  
            eSelectedPropType[0] = ENUM_TO_INT(ePropTypes[iTypeIndex].category)
            eSelectedPropType[1] =  DMPROP_GET_PROP_CATEGORY_POSITION(ePropTypes[iTypeIndex].category, ePropTypes[iTypeIndex].model)

            //DMPROP_GET_NTH_TYPE_LABEL
            CPRINTLN(debug_director,"DMPROP_EDIT_PROP current prop type ",eSelectedPropType[0]," category is ",iTypeIndex,", was ", iCurrentProp)
            iCurrentProp = iTypeIndex

            
            //2416882 - Check if this prop we have chosen to Edit is Dynamic... if it is we need to also decrement the Dynamic props total tracked by iNumDynamicProps.
            IF NOT ePropTypes[iTypeIndex].bFrozen
                CPRINTLN(debug_director,"Prop to EDIT is included in the DYNAMIC Category ", iTypeIndex)
                iNumDynamicProps -=1
            ELSE
                CPRINTLN(debug_director,"Prop to EDIT is frozen ", iTypeIndex)
            ENDIF


            IF IsPropModelRestrictedUse(ePropTypes[iTypeIndex].model)
                iNumRestrictedUseProps -= 1
                CPRINTLN(debug_director,"Editing RESTRICTED USE prop. Decrementing. Restricted use total now at ",iNumRestrictedUseProps)
            ENDIF



            
            VECTOR vOldPos = GET_ENTITY_COORDS(eProps[ind].obj)
            MODEL_NAMES mOldModel = eProps[ind].mModel

            //Steve T - BEGIN: deleting the last selected prop seems better, safer and avoids confusion after choosing to move or change its type. See 2408809, 2410680, 2411036
           
            CPRINTLN(debug_director,"Clear_Blip Spam Test 6")
            DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()
            
            DELETE_OBJECT(eProps[ind].obj)
			SAFE_RELEASE_MODEL(eProps[ind].mModel)
            eProps[ind].fZoffset = 0
            eProps[ind].vPos = <<0,0,0>>
            eProps[ind].mModel = DUMMY_MODEL_FOR_SCRIPT
            iNumProps -=1 //Need this to avoid asserting on Nth prop routines We treat the moved / changed prop as a new prop.
            //Steve T - END of deleting the object to avoid confusion fix. See 2408809, 2410680


            //Mark prop for replacement
            //iForceReplaceProp = iCurrentSel  //Steve T - commenting out this line to make sure prop is added correctly. See 2408809, 2410680

            b_PropSceneUpdatedSinceLastSave = TRUE
            
            DMPROP_SET_NEARBY_PROPS_SAFELY_ON_GROUND(vOldPos, mOldModel)
            
            DMPROP_SET_STATE(PES_PropPlace)
            iCurrentSel = 1 //Selects the Type option

            RETURN TRUE
        ENDIF
        
    ELSE

        CPRINTLN(debug_director, "Warning DMPROP_EDIT_PROP was passed ", ind) //2508317

    ENDIF

    
    RETURN FALSE

ENDFUNC

//PC/Pad control selection
CONTROL_ACTION caPropMoveUp = INPUT_FRONTEND_RS, caPropMoveDown = INPUT_FRONTEND_LS, caPropAccept = INPUT_FRONTEND_ACCEPT
CONTROL_ACTION caPropDelete = INPUT_FRONTEND_X, caPropSnap =INPUT_FRONTEND_Y, caPropMoveNear = INPUT_FRONTEND_LT
CONTROL_ACTION caPropMoveFar = INPUT_FRONTEND_RT, caPropRotRight = INPUT_FRONTEND_RB, caPropRotLeft = INPUT_FRONTEND_LB
    
PROC DMPROP_SWITCH_PC_PAD_CONTROLS()
    //Defaults
    caPropMoveUp = INPUT_FRONTEND_RS
    caPropMoveDown = INPUT_FRONTEND_LS
    caPropAccept = INPUT_FRONTEND_ACCEPT
    caPropDelete = INPUT_FRONTEND_X
    caPropMoveNear = INPUT_FRONTEND_LT
    caPropMoveFar = INPUT_FRONTEND_RT
    caPropRotRight = INPUT_FRONTEND_RB
    caPropRotLeft = INPUT_FRONTEND_LB
	caPropSnap = INPUT_FRONTEND_Y
    
	//Compile fix?
	caPropSnap = caPropSnap
	
    //PC
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        caPropMoveUp = INPUT_SELECT_WEAPON
        caPropMoveDown = INPUT_FRONTEND_LS
        caPropDelete = INPUT_CONTEXT
		caPropSnap = INPUT_ENTER
    ENDIF
ENDPROC

PROC DMPROP_CHANGE_MOVER_DIST(FLOAT min, FLOAT max)
	fMoverMinDist		= min
	fMoverMaxDist		= max
	fMoverDist			= CLAMP(fMoverDist, fMoverMinDist, fMoverMaxDist)
ENDPROC


 ///Stunt mode features
 #IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
  
 /// PURPOSE:
 ///    Moves the camera with the prop by calculating the direction vector from the prop based on rotation and height
 PROC DMPROP_TOPDOWN_CAMERA_MOVE()
	 	
	//Calculate the offset vector
	IF WAS_FLY_CAM_CONSTRAINED_ON_PREVIOUS_UDPATE(ElevatedCam)
		fCamHeight += fMoverZOffsetStep * fMoverStuntZMultiplier
		vecCamOffset.z = fCamHeight			//This moves the camera up. Should be clamped by the move control
//		CPRINTLN(debug_director,"Topdown cam offset now ",vecCamOffset)
		SET_CAM_COORD(ElevatedCam, moverProp.vPos + vecCamOffset)
	ELSE
//		vecCamPreviousOffset = vecCamOffset
		vecCamOffset = <<-SIN(fCamRotation),COS(fCamRotation),0>> * fCamRadius + <<0,0,fCamHeight>>
		SET_CAM_COORD(ElevatedCam, moverProp.vPos + vecCamOffset)
	ENDIF

	//Move cam around with prop
	POINT_CAM_AT_ENTITY(ElevatedCam, moverProp.obj, <<0.0, 0.0, FMIN(fCamHeight, moverProp.vSize.z / 2)>>)
	
	//Set the player position to the mover
	SET_ENTITY_COORDS(PLAYER_PED_ID(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(moverProp.obj, <<0,-2,15>>))
	SET_ENTITY_HEADING(PLAYER_PED_ID(), moverProp.vRot.z)
	
	//Debug - print the interiors
//	TEXT_LABEL_63 ints = "Interiors:P("
//	ints += NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
//	ints += ") O("
//	ints += NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(moverProp.obj))
//	ints += ") C("
//	ints += GET_INTERIOR_FROM_PRIMARY_VIEW()
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	DRAW_DEBUG_TEXT_2D(ints, <<0.5, 0.7, 0>>,255)
	
 ENDPROC
 
  
 PROC DMPROP_TOPDOWN_CAMERA_TOGGLE(BOOL Activate)
	FLOAT fGroundz
 	IF Activate
		IF NOT DOES_CAM_EXIST(ElevatedCam)		//Create camera
			//Save current player interior and room
			intPlayer = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
			iPlayerRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
			IF IS_VALID_INTERIOR(intPlayer)
				PIN_INTERIOR_IN_MEMORY(intPlayer)
			ENDIF
		
			CPRINTLN(debug_director,"Created topdown cam for current prop: ", iCurrentProp)
			ElevatedCam = CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA", TRUE)
			SET_CAM_COORD(ElevatedCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(moverProp.obj,vecCamOffset))
			POINT_CAM_AT_ENTITY(ElevatedCam, moverProp.obj, <<0.0, 0.0, moverProp.vSize.z/2>>)
			SET_FLY_CAM_MAX_HEIGHT(ElevatedCam, fMoverStuntMaxZ)
			SET_FLY_CAM_HORIZONTAL_RESPONSE(ElevatedCam, 40, 20)
			SET_FLY_CAM_VERTICAL_RESPONSE(ElevatedCam, 40, 20)
			
			SET_CAM_ACTIVE(ElevatedCam, true)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			
			//Activate the instant fade-out
			iToggleCamFadeTimeLeft = iToggleCamFadeTime + FLOOR(VDIST(vecPlayerPositionBeforeCam, GET_CAM_COORD(ElevatedCam)) /1000)
			DO_SCREEN_FADE_OUT(0)	//Instant fade
			//DO_SCREEN_FADE_OUT(1000)//Test one second fade to tidy PS4 - 3198938
			
			vecPlayerPositionBeforeCam = GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)
			fPlayerHeadingBeforeCam = GET_ENTITY_HEADING(PLAYER_PED_ID())
			
			//If the player is in air, get the ground z of the location
			IF GET_GROUND_Z_FOR_3D_COORD(vecPlayerPositionBeforeCam, fGroundZ)
			AND vecPlayerPositionBeforeCam.z > (fGroundZ + 5)
				CPRINTLN(debug_director,"DMPROP_TOPDOWN_CAMERA_TOGGLE: Player was in the air when switching camera, setting on ground next time")
				vecPlayerPositionBeforeCam.z = fGroundZ
			ENDIF
			
			//Remove player from a vehicle, if he is in one
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehCamSwitchVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(vehCamSwitchVehicle, <<0,0,-100>>, FALSE)
//				SET_VEHICLE_ON_GROUND_PROPERLY(vehCamSwitchVehicle)
				SET_ENTITY_VISIBLE(vehCamSwitchVehicle, FALSE)
				FREEZE_ENTITY_POSITION(vehCamSwitchVehicle, TRUE)
			ENDIF
			
			//Freeze player, disable control, save player position
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			//B*3213301: Set the player to be exterior only
			SET_IS_EXTERIOR_ONLY(PLAYER_PED_ID(), TRUE)
	
		ENDIF
		
		//Set other variables
		DMPROP_CHANGE_MOVER_DIST(1,100)
	ELSE
		IF DOES_CAM_EXIST(ElevatedCam)
			//Activate the instant fade-out
			iToggleCamFadeTimeLeft = iToggleCamFadeTime + FLOOR(VDIST(vecPlayerPositionBeforeCam, GET_CAM_COORD(ElevatedCam)) / 1000)
			DO_SCREEN_FADE_OUT(0)	//Instant fade
			//DO_SCREEN_FADE_OUT(1000)//Test one second fade to tidy PS4 - 3198938
			
			CPRINTLN(debug_director,"Deleted topdown cam for current prop: ", iCurrentProp)
			DETACH_CAM(ElevatedCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)	
			DESTROY_CAM(ElevatedCam)
			
			//Ground Z check at the location, place the player above the area if it's below a bit of environment
			IF (GET_GROUND_Z_FOR_3D_COORD(vecPlayerPositionBeforeCam + <<0,0,900>>, fGroundZ))
				IF fGroundZ > vecPlayerPositionBeforeCam.z
					CPRINTLN(debug_director,"DMPROP_TOPDOWN_CAMERA_TOGGLE: Found ground Z higher than stored vector, moving player above that")
					vecPlayerPositionBeforeCam.z = fGroundZ
				ENDIF
			ENDIF

			//Unfreeze player, set control back on
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			//If player was in a vehicle, set them back in
			IF DOES_ENTITY_EXIST(vehCamSwitchVehicle) AND NOT IS_ENTITY_DEAD(vehCamSwitchVehicle)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehCamSwitchVehicle)
				FREEZE_ENTITY_POSITION(vehCamSwitchVehicle,FALSE)
				SET_ENTITY_VISIBLE(vehCamSwitchVehicle, TRUE)
				SET_ENTITY_COORDS(vehCamSwitchVehicle, vecPlayerPositionBeforeCam)
				vehCamSwitchVehicle = NULL
			ELSE
				//Else, place him where he used to be
				SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(),vecPlayerPositionBeforeCam, FALSE)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerHeadingBeforeCam )
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			//B*3213301: Set the player to be exterior only
			SET_IS_EXTERIOR_ONLY(PLAYER_PED_ID(), FALSE)
			
			IF IS_VALID_INTERIOR(intPlayer)
				FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), intPlayer, iPlayerRoomKey)
				UNPIN_INTERIOR(intPlayer)
			ENDIF
		ENDIF
		
		//Set other variables
		DMPROP_CHANGE_MOVER_DIST(3,15)
		
	ENDIF
 ENDPROC
  
 
 /// PURPOSE:
 ///    Checks the Right stick / mouse for camera up/down motion
 PROC DMPROP_UPDATE_TOPDOWN_CAM_CONTROLS()
 	//Don't update controls if constrained the last frame
	IF NOT WAS_FLY_CAM_CONSTRAINED_ON_PREVIOUS_UDPATE(ElevatedCam)
		FLOAT maxBaseSize = FMAX(moverProp.vSize.x, moverProp.vSize.y)
	
	  	FLOAT fLookX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	 	FLOAT fLookY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		
		IF fLookX != 0		//Adjust angle		
			fCamRotation = (fCamRotation - fLookX * fMoverStuntZMultiplier + 360) % 360	//Flipped rotation works better
		ENDIF
//		IF fLookY != 0		
		//Adjust cam height
//		fCamHeight = CLAMP(fCamHeight - fLookY * fMoverStuntZMultiplier, fCamMinHeight, fCamMaxHeight) //Flipped Z adjustment looks better
		VECTOR vMax, vMin
		GET_MODEL_DIMENSIONS(moverProp.mModel, vmin, vmax)
		fCamHeight = CLAMP(fCamHeight - fLookY * fMoverStuntZMultiplier, vmin.z, fCamMaxHeight) //Flipped Z adjustment looks better
		TEXT_LABEL_15 tl = "Min Z "
		tl += ROUND(vmin.z * 10)
		DRAW_DEBUG_TEXT_2D(tl, <<0.1, 0.3, 0>>)
//		ENDIF

		//Adjust radius
		fCamRadius = maxBaseSize + fMoverDist * fMoverStuntZMultiplier
	ENDIF
 ENDPROC
 
 
  /// PURPOSE:
 ///    This creates (if needed) and moves/reattaches the top-down prop camera 
 PROC DMPROP_TOPDOWN_CAMERA_UPDATE()
 	//If the prop's a stunt prop
	IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE)
	
	 	IF DOES_ENTITY_EXIST(moverProp.obj)
			IF !DOES_CAM_EXIST(ElevatedCam)
				DMPROP_TOPDOWN_CAMERA_TOGGLE(TRUE)
			ENDIF
			DMPROP_UPDATE_TOPDOWN_CAM_CONTROLS()
			DMPROP_TOPDOWN_CAMERA_MOVE()
		ENDIF
	 	
	ELSE 
		IF DOES_CAM_EXIST(ElevatedCam)
			DMPROP_TOPDOWN_CAMERA_TOGGLE(FALSE)
		ENDIF
	ENDIF
 	
 ENDPROC
 
//===============================Prop snapping area====================================
INT iSnappedBoneIdx = -1, iSnappedLocalBoneIdx = -1, iPrevSnapBoneIdx = -1
ENTITY_INDEX entSnapFrom, entSnapTo
INT iFoundPropIdx = -1, iFoundBoneIdx = -1, iFoundLocalBoneIdx = -1
INT iTempPropIdx = -1, iTempBoneIdx = -1, iTempLocalBoneIdx = -1
INT iSnapTimer, iSnapTimeOut = 1000			//Timer variable + time that snapped props should not move for
INT iCurrentPropIndex = 0, iPropsCheckedPerFrame = 10
BOOL bMovedSinceSnap = TRUE
FLOAT fPropCheckRange = 90000	//300*300
FLOAT fPropBoneRange = 400		//20*20
FLOAT iCurrDist = 2000000000
VECTOR vecSnapPosition


FUNC BOOL DMPROP_IS_BONE_VALID(ENTITY_INDEX obj, INT bone)

	IF DOES_ENTITY_EXIST(obj)
		INT iNumBones = GET_ENTITY_BONE_COUNT(obj)
		IF bone >= iNumBones OR bone <0
			RETURN FALSE
		ENDIF
		
		VECTOR vObjPos = GET_ENTITY_COORDS(obj)
		VECTOR vBonePos = GET_WORLD_POSITION_OF_ENTITY_BONE(obj, bone)
		
		IF ARE_VECTORS_ALMOST_EQUAL(vObjPos, vBonePos, 0.05) OR IS_VECTOR_ZERO(vBonePos)
			RETURN FALSE
		ENDIF
		
		//Check if the bone is one of the extras in the animated props
		MODEL_NAMES mnProp = GET_ENTITY_MODEL(obj)
		IF bone = 4
			IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_01"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_02"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_03"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_04"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Fn_05"))
				CDEBUG1LN(DEBUG_DIRECTOR,"This is a special extra on the animated props. Skipping bone 4")
				RETURN FALSE
			ENDIF
		ENDIF
		
		//B* 3198934: Disable bone usage on the Stop sign prop
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_track_stop_sign"))
			RETURN FALSE
		ENDIF
		
		//Bone passed all previous checks
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC INT DMPROP_GET_NEXT_VALID_BONE(ENTITY_INDEX prop, INT currentBone, INT iTriesLeft = 10)
	
	INT nextBone = (currentBone + 1) % GET_ENTITY_BONE_COUNT(prop)
	
	IF iTriesLeft < 0
		CASSERTLN(debug_director,"DMPROP_GET_NEXT_VALID_BONE: Ran out of tries to get a good bone. returning the current one: ",nextBone)
		RETURN nextBone
	ENDIF
	
	IF DMPROP_IS_BONE_VALID(prop, nextBone)
		RETURN nextBone
	ELSE
		RETURN DMPROP_GET_NEXT_VALID_BONE(prop, nextBone, iTriesLeft-1)
	ENDIF
ENDFUNC


/// PURPOSE:
///    This is a very manual method to get the opposite bone on a given prop
FUNC INT DMPROP_GET_OPPOSITE_BONE(ENTITY_INDEX prop, int currentBone)
	//4-direction prop
	IF GET_ENTITY_BONE_COUNT(prop) >= 6
		SWITCH currentBone
			CASE 2 RETURN 4
			CASE 3 RETURN 5
			CASE 4 RETURN 2
			CASE 5 RETURN 3
			DEFAULT 
				CASSERTLN(debug_director,"DMPROP_GET_OPPOSITE_BONE: Given bone (",currentBone,") not one of the default ones on 6+ bones")
				RETURN (currentBone + 1) % GET_ENTITY_BONE_COUNT(prop)
		ENDSWITCH
	ELSE
	//2-direction prop
		SWITCH currentBone
			CASE 2 RETURN 3
			CASE 3 RETURN 2
			DEFAULT 
				CASSERTLN(debug_director,"DMPROP_GET_OPPOSITE_BONE: Given bone (",currentBone,") not one of the default ones on <6 bones")
				RETURN (currentBone + 1) % GET_ENTITY_BONE_COUNT(prop)
		ENDSWITCH
	ENDIF
ENDFUNC


/// PURPOSE:
///    Flips the first prop by 180* on the heading if the bones are at an acute angle 
///    (Otherwise the props will snap one through the other
PROC DMPROP_FLIP_PROP_FOR_CORRECT_SNAPPING(ENTITY_INDEX prop1, ENTITY_INDEX prop2, INT bone1, INT bone2)

	VECTOR rot1 = GET_ENTITY_BONE_ROTATION(prop1, bone1)
	VECTOR rot2 = GET_ENTITY_BONE_ROTATION(prop2,bone2)
	FLOAT headingDif = (rot1.z - rot2.z + 360) % 360 
	IF headingDif > 180
		headingDif -= 360
	ENDIF
//	CPRINTLN(debug_dan,"Initial heading diff ",headingDif)
	
	//Extra 30* rotation if close to 90
	IF ABSF(headingDif) <= 90.001 AND ABSF(headingDif) >=89.001
		SET_ENTITY_HEADING(prop1, ( GET_ENTITY_HEADING(prop1) + 30) % 360)
		rot1 = GET_ENTITY_BONE_ROTATION(prop1, bone1)
		headingDif = (rot1.z - rot2.z + 360) % 360
//		CPRINTLN(debug_dan,"Angle was close to 90*, Added a bit to rotation, now it's ",headingDif)
	ENDIF

	//Flip the mover's heading if the heading difference between the props is < 90
	IF (bone1 = 2 AND bone2 != 2) OR (bone1 != 2 AND bone2 = 2)
		//Special case for bone 2, where we know it's flipped
		CPRINTLN(DEBUG_DIRECTOR, "DMPROP_SNAP_PROP_TO_PROP: We're using bone 2 here")
		headingDif = (headingDif + 180) % 360
	ENDIF
//	CPRINTLN(debug_dan,"Heading diff after bone2 ",headingDif)
	
	headingDif = (headingDif + 360) % 360
//	CPRINTLN(debug_dan,"Heading diff after normalise ",headingDif)
	
	IF headingDif > 180
		headingDif -= 360		//Wrap around o values
	ENDIF
	
//	CPRINTLN(debug_dan,"Heading diff after wrap around ",headingDif)	
	IF ABSF(headingDif) <= 90.001
		SET_ENTITY_HEADING(prop1, ( GET_ENTITY_HEADING(prop1) + 181) % 360)
	ENDIF
ENDPROC


/// PURPOSE:
///    Attaches a prop to a different prop based on bones
PROC DMPROP_SNAP_PROP_TO_PROP(ENTITY_INDEX prop1, ENTITY_INDEX prop2, int bone1, int bone2)
	CPRINTLN(debug_director,"Attaching prop1 bone ", bone1, " to prop2 bone ", bone2)
	
	DMPROP_FLIP_PROP_FOR_CORRECT_SNAPPING(prop1, prop2, bone1, bone2)
	
	//Move prop away from that bone for better snapping
	VECTOR vDif = GET_ENTITY_BONE_POSTION(prop1, bone1) - GET_ENTITY_COORDS(prop1, false)
	VECTOR vDif2 = GET_ENTITY_BONE_POSTION(prop2, bone2) - GET_ENTITY_COORDS(prop2, false)
	SET_ENTITY_COORDS(prop1, GET_ENTITY_BONE_POSTION(prop2, bone2) - vDif * 2.0 + vDif2 * 2.0, false)
	
	ATTACH_ENTITY_BONE_TO_ENTITY_BONE(prop1, prop2 , bone1, bone2)
	DETACH_ENTITY(prop1, FALSE)
	iSnapTimer = GET_GAME_TIMER()
	
	//Set global snapped parameters
	entSnapFrom = prop1
	entSnapTo = prop2
	iSnappedBoneIdx = bone2
	iSnappedLocalBoneIdx = bone1
	bMovedSinceSnap = FALSE
ENDPROC


PROC DMPROP_SNAP_NEW_MOVER()
	//If the new mover has no skeleton, break out
	IF NOT DOES_ENTITY_EXIST(moverProp.obj) OR GET_ENTITY_BONE_COUNT(moverProp.obj) <= 0	
		EXIT
	ENDIF
	
	IF DMPROP_IS_PROP_SPECIALRACE_CURVED_TRACK(iCurrentProp)
		EXIT
	ENDIF
	
	
	//Check if the local bone exists on the new mover
	IF NOT DMPROP_IS_BONE_VALID(moverProp.obj, iSnappedLocalBoneIdx)
		iSnappedLocalBoneIdx = DMPROP_GET_NEXT_VALID_BONE(moverProp.obj, iSnappedLocalBoneIdx)
		iFoundLocalBoneIdx = iSnappedLocalBoneIdx
		CPRINTLN(debug_director,"Original mover bone index out ot bounds. New index set to ",iSnappedLocalBoneIdx)
	ENDIF

	IF DOES_ENTITY_EXIST(entSnapFrom)
		//We know the current mover object, we need to use the same bone on the opposite bone of the old mover prop
		DMPROP_SNAP_PROP_TO_PROP(moverProp.obj, entSnapFrom, iSnappedLocalBoneIdx, DMPROP_GET_OPPOSITE_BONE(entSnapFrom, iSnappedLocalBoneIdx))
		
	//We've changed the mover type, snap to the current placed object		
	ELIF DOES_ENTITY_EXIST(entSnapTo)
		//We're using the same placed object we were using with the old mover, use the same bone index
		DMPROP_SNAP_PROP_TO_PROP(moverProp.obj, entSnapTo, iSnappedLocalBoneIdx, iSnappedBoneIdx)
	ENDIF
ENDPROC


PROC DMPROP_SNAP_MOVER_TO_NEXT_BONE()
	IF DOES_ENTITY_EXIST(entSnapTo) AND DOES_ENTITY_EXIST(entSnapFrom)
		iSnappedBoneIdx = DMPROP_GET_NEXT_VALID_BONE(entSnapTo, iSnappedBoneIdx)
		
		DMPROP_SNAP_PROP_TO_PROP(moverProp.obj, entSnapTo, iSnappedLocalBoneIdx, iSnappedBoneIdx)
	ENDIF
ENDPROC


/// PURPOSE:
///    Updates the prop snapping on the current prop
PROC DMPROP_UPDATE_PROP_SNAPPING()
	IF iNumProps > 0 AND DOES_ENTITY_EXIST(moverProp.obj) AND DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp)
	
		IF DMPROP_IS_PROP_SPECIALRACE_CURVED_TRACK(iCurrentProp)
			EXIT
		ENDIF
	
	
		//Check if the prop has not been snapped
		IF bMovedSinceSnap
			//get prop bones
			INT iNumBones = GET_ENTITY_BONE_COUNT(moverProp.obj)
			INT i, j
			INT iPropsLeft = iPropsCheckedPerFrame
			
			//Sanity check the prop index
			IF iCurrentPropIndex >= iNumProps OR iCurrentPropIndex < 0 
				iCurrentPropIndex = 0
			ENDIF
			
			//Reset variables if we start over
			IF iCurrentPropIndex = 0
				//Save old values to "found bones"
				iFoundPropIdx = iTempPropIdx
				iFoundBoneIdx = iTempBoneIdx
				iFoundLocalBoneIdx = iTempLocalBoneIdx
			
				iCurrDist = 200000000
				iTempPropIdx = -1
				iTempBoneIdx = -1
				iTempLocalBoneIdx = -1
			ENDIF
			
			
			//Find all props that qualify
			WHILE iPropsLeft > 0 AND iNumProps > 0
				DM_PROP prop = DMPROP_GET_NTH_PROP(iCurrentPropIndex)	//The prop we're currently checking
				
				IF DOES_ENTITY_EXIST(prop.obj) AND NOT IS_ENTITY_DEAD(prop.obj) AND GET_ENTITY_BONE_COUNT(prop.obj) > 0
					FLOAT dist = VDIST2(moverProp.vPos, prop.vPos)
					IF dist < fPropCheckRange
						//Check the bones on this prop
						
						int iNumBonesOther = GET_ENTITY_BONE_COUNT(prop.obj)
						FOR i = 1 to iNumBones - 1			//Starts at bone index 1 because 0 is the root
							FOR j = 1 to iNumBonesOther - 1
								IF DMPROP_IS_BONE_VALID(prop.obj, j) AND DMPROP_IS_BONE_VALID(moverProp.obj, i)
									dist = VDIST2(GET_WORLD_POSITION_OF_ENTITY_BONE(moverProp.obj, i), GET_WORLD_POSITION_OF_ENTITY_BONE(prop.obj, j))
									IF dist < iCurrDist AND dist < fPropBoneRange
										iCurrDist = dist
										iTempPropIdx = DMPROP_GET_NTH_PROP_INDEX(iCurrentPropIndex)
										iTempBoneIdx = j
										iTempLocalBoneIdx = i
									ELIF dist = iCurrDist AND iTempPropIdx > -1
										//Equal distance, check which prop is closest
										IF VDIST2(moverProp.vPos, prop.vPos) < VDIST2(moverProp.vPos, eProps[iTempPropIdx].vPos)
											iCurrDist = dist
											iTempPropIdx = DMPROP_GET_NTH_PROP_INDEX(iCurrentPropIndex)
											iTempBoneIdx = j
											iTempLocalBoneIdx = i
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						ENDFOR
					ENDIF
				ENDIF
				
				//Advance the checked prop
				iPropsLeft -= 1
				iCurrentPropIndex += 1
				IF iCurrentPropIndex > iNumProps -1
					iCurrentPropIndex = 0
					iPropsLeft = 0
				ENDIF
			ENDWHILE
			
			IF iFoundBoneIdx != -1 AND iFoundLocalBoneIdx != -1 
			AND iFoundPropIdx > -1 AND DOES_ENTITY_EXIST(eProps[iFoundPropIdx].obj)
							
				//Show markers on both bones
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//				DRAW_DEBUG_SPHERE(GET_WORLD_POSITION_OF_ENTITY_BONE(moverProp.obj, iFoundLocalBoneIdx), 1, 0, 255, 0)
//				DRAW_DEBUG_SPHERE(GET_WORLD_POSITION_OF_ENTITY_BONE(eProps[iFoundPropIdx].obj, iFoundBoneIdx), 1, 0, 0, 255)
				DMPROP_DRAW_MARKER(GET_WORLD_POSITION_OF_ENTITY_BONE(moverProp.obj, iFoundLocalBoneIdx) + <<0,0,1>>,DMPROP_GET_MARKER_SCALE(moverprop),0,255,0, default, true, FALSE)
				DMPROP_DRAW_MARKER(GET_WORLD_POSITION_OF_ENTITY_BONE(eProps[iFoundPropIdx].obj, iFoundBoneIdx) + <<0,0,1>>,DMPROP_GET_MARKER_SCALE(eProps[iFoundPropIdx]),0,0,255, default, true, FALSE)
					
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_7 str1 = "Bone "
				str1 += iFoundLocalBoneIdx
				
				TEXT_LABEL_7 str2 = "Bone "
				str2 += iFoundBoneIdx
				DRAW_DEBUG_TEXT(str1, GET_WORLD_POSITION_OF_ENTITY_BONE(moverProp.obj, iFoundLocalBoneIdx))
				DRAW_DEBUG_TEXT(str2, GET_WORLD_POSITION_OF_ENTITY_BONE(eProps[iFoundPropIdx].obj, iFoundBoneIdx))
				#ENDIF
				
				//If we press triangle, snap the mover to the fixed prop
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caPropSnap)
					DMPROP_SNAP_PROP_TO_PROP(moverProp.obj, eProps[iFoundPropIdx].obj, iFoundLocalBoneIdx, iFoundBoneIdx)
				ENDIF
			ENDIF
		ELSE
			IF iFoundPropIdx != -1 AND iFoundBoneIdx != -1 AND GET_ENTITY_BONE_COUNT(moverProp.obj) > 0
				//The prop's been snapped, no reason to recalculate nearest snap
//				DRAW_DEBUG_SPHERE(GET_WORLD_POSITION_OF_ENTITY_BONE(eProps[iFoundPropIdx].obj, iFoundBoneIdx), 1.2, 255, 0, 0)
				DMPROP_DRAW_MARKER(GET_WORLD_POSITION_OF_ENTITY_BONE(moverProp.obj, iSnappedLocalBoneIdx) + <<0,0,1>>,DMPROP_GET_MARKER_SCALE(moverprop),255,255,0, default, true, FALSE)
				
				//If pressing the button again, snap to next valid bone
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caPropSnap)
					DMPROP_SNAP_MOVER_TO_NEXT_BONE()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//Reset found/snapped indices
		iFoundPropIdx = -1
		iFoundBoneIdx = -1
		iFoundLocalBoneIdx = -1
	ENDIF
	
	//Reset the PI help if we've switched found bone index
	//We only do this if the bone indices have different signs, i.e., goes from "no bone found" to "some bone found"
	IF iFoundBoneIdx != iPrevSnapBoneIdx AND iFoundBoneIdx * iPrevSnapBoneIdx < 0	
		bResetMenuNow = TRUE
	ENDIF
	
	iPrevSnapBoneIdx = iFoundBoneIdx
	
ENDPROC
 
 #ENDIF
 



INT LoopedPI_SoundID
BOOL b_SoundLooping = FALSE
INT i_BitSet_ControlCheck
CONST_INT Bit_PropRotL  0
CONST_INT Bit_PropRotR  1
CONST_INT Bit_PropElevU 2
CONST_INT Bit_PropElevD 3
CONST_INT Bit_PropMoveF 4
CONST_INT Bit_PropMoveN 5
//FLOAT f_LastMoverDist





PROC DMPROP_UPDATE_MOVER_CONTROLS()
    
    //Set this to TRUE to let Director Mode.sc know we want to update the Menu in CONTROL_PLAYER_INPUT() immediately if a change in player circumstances occurs:
    //b_PropEditorRequestsImmediateMenuReset = TRUE //Update: Looks like a vehicle entering / exit check is already happening elsewhere.


    //DRAW_CANNOT_PLACE_LABEL()

    fOldMoverHeading = fMoverHeading

    IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropRotLeft)
    AND (NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropRotRight))) 

        fMoverHeading = (fMoverHeading + 2) % 360

        IF b_SoundLooping = FALSE
        
            LoopedPI_SoundID = GET_SOUND_ID()
            b_SoundLooping = TRUE

            IF NOT (IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropRotL))

                SET_BIT (i_BitSet_ControlCheck, Bit_PropRotL)

            ENDIF

        ELSE

            IF HAS_SOUND_FINISHED (LoopedPI_SoundID)
                PLAY_SOUND_FRONTEND (LoopedPI_SoundID, "Rotate_Loop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
            ENDIF

        ENDIF

    ELSE

        IF b_SoundLooping = TRUE

            IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropRotL)
            OR ePEState != PES_PropPlace

                CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropRotL)

                STOP_SOUND (LoopedPI_SoundID)
                RELEASE_SOUND_ID (LoopedPI_SoundID)

                b_SoundLooping = FALSE
        
            ENDIF

        ENDIF

    ENDIF




    IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropRotRight)
    AND (NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropRotLeft))) 
        fMoverHeading = (fMoverHeading - 2 + 360) % 360
        
        IF b_SoundLooping = FALSE
        
            LoopedPI_SoundID = GET_SOUND_ID()
            b_SoundLooping = TRUE

            IF NOT (IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropRotR))

                SET_BIT (i_BitSet_ControlCheck, Bit_PropRotR)

            ENDIF

        ELSE

            IF HAS_SOUND_FINISHED (LoopedPI_SoundID)
                PLAY_SOUND_FRONTEND (LoopedPI_SoundID, "Rotate_Loop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
            ENDIF
  
        ENDIF

    ELSE

        IF b_SoundLooping = TRUE

            IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropRotR)
            OR ePEState != PES_PropPlace


                CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropRotR)

                STOP_SOUND (LoopedPI_SoundID)
                RELEASE_SOUND_ID (LoopedPI_SoundID)

                b_SoundLooping = FALSE

            ENDIF

        ENDIF

    ENDIF



     IF NOT IS_PED_INJURED(PLAYER_PED_ID())  //Don't allow the forward and back prop movement controls whilst in a vehicle.
        IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            
            IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveNear)
            AND (NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveFar))) 

                fMoverDist = FMAX(fMoverDist - TO_FLOAT(GET_CONTROL_VALUE(FRONTEND_CONTROL, caPropMoveNear)-127)*0.1/127,fMoverMinDist)

                
                IF fMoverDist > fMoverMinDist 
                
                    IF b_SoundLooping = FALSE
                
                        LoopedPI_SoundID = GET_SOUND_ID()
                        b_SoundLooping = TRUE

                        IF NOT (IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropMoveN))

                            SET_BIT (i_BitSet_ControlCheck, Bit_PropMoveN)

                        ENDIF

                    ELSE

                        
                        IF fMoverDist > fMoverMinDist //Don't play the sound when we have reached the near limit. 
                            
                            IF HAS_SOUND_FINISHED (LoopedPI_SoundID)
							
								#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
								IF NOT (DOES_CAM_EXIST(ElevatedCam))//Potential fix 3199874. Shouldn't play this sfx as these controls manipulate the camera only in this view.
								#ENDIF
							
                                	PLAY_SOUND_FRONTEND (LoopedPI_SoundID, "Move_Loop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
								
								#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
								ENDIF
								#ENDIF
								
                            ENDIF
                        
                        ELSE

                            STOP_SOUND (LoopedPI_SoundID)

                        ENDIF

                    ENDIF

                ELSE
                    
                    IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropMoveN)
                    OR ePEState != PES_PropPlace

                        CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropMoveN)

                        STOP_SOUND (LoopedPI_SoundID)
                        RELEASE_SOUND_ID (LoopedPI_SoundID)
                        b_SoundLooping = FALSE


                    ENDIF
                ENDIF

            ELSE

                IF b_SoundLooping = TRUE

                    IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropMoveN)
                    OR ePEState != PES_PropPlace

                        CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropMoveN)

                        STOP_SOUND (LoopedPI_SoundID)
                        RELEASE_SOUND_ID (LoopedPI_SoundID)

                        b_SoundLooping = FALSE
            
                    ENDIF

                ENDIF

            ENDIF


            IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveFar)
            AND (NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveNear))) 

                fMoverDist = FMIN(fMoverDist + TO_FLOAT(GET_CONTROL_VALUE(FRONTEND_CONTROL, caPropMoveFar)-127)*0.1/127,fMoverMaxDist)
                
                
                IF fMoverDist < fMoverMaxDist 
                
                    IF b_SoundLooping = FALSE
                
                        LoopedPI_SoundID = GET_SOUND_ID()
                        b_SoundLooping = TRUE

                        IF NOT (IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropMoveF))

                            SET_BIT (i_BitSet_ControlCheck, Bit_PropMoveF)

                        ENDIF

                    ELSE

                        IF fMoverDist < fMoverMaxDist //Don't play the sound when we have reached the near limit. 
						
                            IF HAS_SOUND_FINISHED (LoopedPI_SoundID)
							
								#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
								IF NOT (DOES_CAM_EXIST(ElevatedCam))//Potential fix 3199874. Shouldn't play this sfx as these controls manipulate the camera only in this view.
								#ENDIF
							
                                	PLAY_SOUND_FRONTEND (LoopedPI_SoundID, "Move_Loop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
								
								#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
								ENDIF
								#ENDIF
								
                            ENDIF
                    
                        ELSE

                            STOP_SOUND (LoopedPI_SoundID)

                        ENDIF

                    ENDIF

                ELSE

                    IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropMoveF)
                    OR ePEState != PES_PropPlace

                        CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropMoveF)

                        STOP_SOUND (LoopedPI_SoundID)
                        RELEASE_SOUND_ID (LoopedPI_SoundID)
                        b_SoundLooping = FALSE

    
                    ENDIF

                ENDIF

            ELSE

                IF b_SoundLooping = TRUE

                    IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropMoveF)
                    OR ePEState != PES_PropPlace


                        CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropMoveF)

                        STOP_SOUND (LoopedPI_SoundID)
                        RELEASE_SOUND_ID (LoopedPI_SoundID)

                        b_SoundLooping = FALSE
            
                    ENDIF

                ENDIF

            ENDIF




       
        ELSE

            DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND) //If the player is in a vehicle whilst placing props, disable look behind and the horn
            DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HORN)        //to prevent conflicts with prop height manipulation.  
            
            //Bicycle Check here

            fMoverDist = 7.0  //Make sure the mover prop's default position cannot intersect with the bonnet of a vehicle if we block movement.
                          
        ENDIF
    ENDIF




    //Prop Height manipulation lockout for bug 2410298
    IF b_LockOut_PropHeightControls = FALSE

        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveUp)
        AND (NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveDown))) 

            moverProp.fZoffset = FMIN( moverProp.fZoffset+ fMoverZOffsetStep, fMoverMaxZOffset)
			
			#IF IS_DEBUG_BUILD
				vCurrentVec = GET_ENTITY_COORDS(moverProp.Obj)
			#ENDIF

            IF b_SoundLooping = FALSE
                
                IF eSelectedPropType[0] != ENUM_TO_INT(CAT_Water) //2506667

                    LoopedPI_SoundID = GET_SOUND_ID()
                    b_SoundLooping = TRUE

                    IF NOT (IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropElevU))

                        SET_BIT (i_BitSet_ControlCheck, Bit_PropElevU)

                    ENDIF

                ENDIF

            ELSE

                IF moverProp.fZoffset < fMoverMaxZOffset
				#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
				OR DOES_CAM_EXIST(ElevatedCam) //Fix for 3199874. Allows props in this view to still play audio at more elevated heights.
				#ENDIF
				
                    IF HAS_SOUND_FINISHED (LoopedPI_SoundID)
                        PLAY_SOUND_FRONTEND (LoopedPI_SoundID, "Elevation_Loop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
                    ENDIF

                ELSE

                    STOP_SOUND (LoopedPI_SoundID)
                
                ENDIF

            ENDIF

        ELSE

            IF b_SoundLooping = TRUE
            OR ePEState != PES_PropPlace

                IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropElevU)

                    CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropElevU)

                    STOP_SOUND (LoopedPI_SoundID)
                    RELEASE_SOUND_ID (LoopedPI_SoundID)

                    b_SoundLooping = FALSE
            
                ENDIF

            ENDIF

        ENDIF





        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveDown)
        AND (NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveUp))) 

            moverProp.fZoffset = FMAX( moverProp.fZoffset- fMoverZOffsetStep, -fMoverMaxZOffset)
			
			#IF IS_DEBUG_BUILD
				vCurrentVec = GET_ENTITY_COORDS(moverProp.Obj)
			#ENDIF

            IF b_SoundLooping = FALSE
            
                IF eSelectedPropType[0] != ENUM_TO_INT(CAT_Water) //2506667

                    
                    LoopedPI_SoundID = GET_SOUND_ID()
                    b_SoundLooping = TRUE

                    IF NOT (IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropElevD))

                        SET_BIT (i_BitSet_ControlCheck, Bit_PropElevD)

                    ENDIF

                ENDIF

            ELSE

                IF moverProp.fZoffset > -fMoverMaxZOffset

                    IF HAS_SOUND_FINISHED (LoopedPI_SoundID)
                        PLAY_SOUND_FRONTEND (LoopedPI_SoundID, "Elevation_Loop", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537
                    ENDIF

                ELSE

                    STOP_SOUND (LoopedPI_SoundID)
                
                ENDIF

            ENDIF
    
        ELSE

            IF b_SoundLooping = TRUE

                IF IS_BIT_SET (i_BitSet_ControlCheck, Bit_PropElevD)
                OR ePEState != PES_PropPlace

                    CLEAR_BIT (i_BitSet_ControlCheck, Bit_PropElevD)

                    STOP_SOUND (LoopedPI_SoundID)
                    RELEASE_SOUND_ID (LoopedPI_SoundID)

                    b_SoundLooping = FALSE
            
                ENDIF

            ENDIF

   

        ENDIF
    
    ELSE

        IF TIMERA() > 1000 //Lockout height manipulation controls for one second only after a prop height reset.

            b_LockOut_PropHeightControls = FALSE
            
            CPRINTLN(debug_director, "b_LockOut_PropHeightControls set to FALSE after lockout period elapsed.")

        ENDIF
            
    ENDIF


	#IF IS_DEBUG_BUILD	
	IF bEnableResetHeightDebug
		IF DOES_CAM_EXIST(ElevatedCam)
		DRAW_RECT( 0.75, 0.23, 0.4, 0.3, 0, 0, 0, 205 )
		DISPLAY_TEXT_WITH_LITERAL_STRING (0.65, 0.10, "STRING", "ResetZ")
		DISPLAY_TEXT_WITH_FLOAT (0.65, 0.17, "CELL_502", vResetOriginal.z, 3)
		IF vCurrentVec.z > vResetOriginal.Z
			DISPLAY_TEXT_WITH_LITERAL_STRING (0.65, 0.24, "STRING", "Adjust req +")
			DISPLAY_TEXT_WITH_FLOAT (0.65, 0.3, "CELL_502", vCurrentVec .z - vResetOriginal.z, 3)
		ELSE
			DISPLAY_TEXT_WITH_LITERAL_STRING (0.65, 0.24, "STRING", "Adjust req -")
			DISPLAY_TEXT_WITH_FLOAT (0.65, 0.3, "CELL_502", vResetOriginal.z - vCurrentVec .z, 3)
		ENDIF
		ENDIF
	ENDIF
	#ENDIF


    //Special ability for mover Z and rotation reset
    IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL,caPropMoveUp)
    AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, caPropMoveDown)
        moverProp.fZoffset = 0
		moverProp.vRot.x = 0
		moverProp.vRot.y = 0

        //Prop Height manipulation lockout for bug 2410298
        IF b_LockOut_PropHeightControls = FALSE //Make sure the sound cannot spam by linking it to the prop height control lockout timeslice
            IF eSelectedPropType[0] != ENUM_TO_INT(CAT_Water) //2506667
                PLAY_SOUND_FRONTEND (-1, "Reset_Prop_Position", "DLC_Dmod_Prop_Editor_Sounds", FALSE) //New sound for bug 2448537. Updated with new sound from George.
            ENDIF
        ENDIF

        b_LockOut_PropHeightControls = TRUE
        SETTIMERA (0)

        CPRINTLN(debug_director, "b_LockOut_PropHeightControls set to TRUE after user reset height press.")

	
		//Ground stunt pack prop height
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
		IF DOES_CAM_EXIST(ElevatedCam)
			moverProp.vPos.z = GET_SURFACE_Z_BELOW(moverProp.vPos + <<0,0,100>>, TRUE)
			
			//Steve T. - We need to tweak the reset heights of certain Stunt Props.
			INT ThisMoverIndex = DMPROP_GET_PROP_TYPE_INDEX(moverProp)
			
			moverProp.vPos.z  = moverProp.vPos.z + ePropTypes[ThisMoverIndex].fMoverAdjust
			
			#IF IS_DEBUG_BUILD
			vResetOriginal.z =moverProp.vPos.z
			#ENDIF
			
		ENDIF
		#ENDIF
    ENDIF        
ENDPROC



/// PURPOSE:
///    Calculates the basic position of the mover before ground adjustments. Based on player position or top-down camera position
PROC DMPROP_UPDATE_BASIC_MOVER_POS(VECTOR playerPos, FLOAT playerHeading)
	#IF NOT FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	moverProp.vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(playerPos,playerHeading,<<0,fMoverDist,0>>)
	#ENDIF
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	IF NOT DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE)
		moverProp.vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(playerPos,playerHeading,<<0,fMoverDist,0>>)
	ELSE
		//If the prop hasn't been snapped recently
		IF GET_GAME_TIMER() <= iSnapTimer + iSnapTimeOut
			EXIT
		ENDIF
		//Get the displacement from the sticks
		FLOAT movX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		FLOAT movY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * -1	//Reverse because up = negative y on the map
		IF movX != 0 OR movY != 0
			//Increase move rate
			fPropMoveRate = CLAMP(fPropMoveRate + fPropMoveAccel, 0, 1)
			
			//Apply the movements based on the heading
			VECTOR camRot = GET_FINAL_RENDERED_CAM_ROT()
			VECTOR fwdCam = <<-SIN(camRot.z),COS(camRot.z),0>>
			fwdCam = fwdCam / VMAG(fwdCam)
			VECTOR rightCam = CROSS_PRODUCT(fwdCam, <<0,0,1>>)
			
			moverProp.vPos += (fwdCam * movY + rightCam * movX) * fPropMoveRate
		ELSE
			fPropMoveRate = 0
		ENDIF
		
		//Get up/down adjustment
		IF NOT b_LockOut_PropHeightControls
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveUp)
			 	moverProp.vPos.z += fMoverZOffsetStep  * fMoverStuntZMultiplier
			ELIF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caPropMoveDown)
				moverProp.vPos.z -= fMoverZOffsetStep  * fMoverStuntZMultiplier
			ENDIF
		ENDIF
		moverProp.vPos.z = CLAMP(moverProp.vPos.z, -moverProp.vSize.z/2, fMoverStuntMaxZ)
		
		DMPROP_CLAMP_MOVER_POSITION()
			
	ENDIF
	#ENDIF


ENDPROC
 

PROC DMPROP_UPDATE_MOVER_PROP()       

    //Steve T. work on 2485910  - I'm removing this "Delete the selected blip" routine. 
    //In a script where DMPROP_GET_NTH_PROP_INDEX asserts if it can't find the object and DMPROP_GET_NTH_PROP is needed frequently, this is a bit ropey here.  
    //DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP should only really be called when you know the object exists rather than just checking ints.
    //Discovered a problem where editing a prop quite far down the selection list would cause it to be deleted then this clear blip routine to run below on an object that didn't exist.
    //DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP is now called in a non-spammy way from DMPROP_DELETE_PROP, DMPROP_EDIT_PROP, DMPROP_UPDATE_SELECTED_PROP_BLIP and the exit cleanup
    
    /* Don't put this back in please!.
    //Delete the selected blip if it exists
    IF iBlippedProp >= 0 AND iBlippedProp <> iForceReplaceProp
        CPRINTLN(debug_director,"Clear_Blip Spam Test 2")
        //DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()
    ENDIF
    */


    //Create mover if it doesn't exist
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
        VECTOR vCamRot = GET_GAMEPLAY_CAM_ROT()
        FLOAT fHeading = vCamRot.z
        FLOAT fGroundZ
        
        IF !bMoverCreated
			
            CPRINTLN(debug_director,"Creating mover with iCurrentProp ", iCurrentProp)

            IF iBlippedProp >= 0
                DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()
            ENDIF

            IF moverProp.mModel = DUMMY_MODEL_FOR_SCRIPT
                moverProp.mModel = ePropTypes[iCurrentProp].model
            ENDIF
            REQUEST_MODEL(moverProp.mModel)
            IF HAS_MODEL_LOADED(moverProp.mModel)
				
                GET_MODEL_DIMENSIONS(moverProp.mModel,vModelmin,vModelMax)
				moverProp.vSize = vModelMax - vModelMin
				
				//Only change the mover position if it was cleared
                IF ARE_VECTORS_EQUAL(moverProp.vPos, <<0,0,0>>)	
					moverProp.vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vplayerPos,fHeading,<<0,5,0>>)
					//CPRINTLN(debug_director,"Creating mover object at player heading")
				ENDIF
                moverProp.obj = CREATE_OBJECT(moverProp.mModel,moverProp.vPos)
				SET_ENTITY_COORDS(moverProp.obj, moverProp.vPos, FALSE)
				
				//Make sure that the mover prop's X, Y rotation is reset so it doesn't retain any exotic snapped angle of the preceding prop if not snapped. See bug 3469921
				moverProp.vRot.x = 0.0
				moverProp.vRot.y = 0.0
				SET_ENTITY_ROTATION(moverProp.obj, moverProp.vRot)
				
				//Preserve the heading of the mover from the preceding mover prop.
				SET_ENTITY_HEADING(moverProp.obj, moverProp.vRot.z)
				
				moverProp.vPos = GET_ENTITY_COORDS(moverProp.obj, false)
				bMoverCreated = TRUE
                
				//Special mover conditions
                FREEZE_ENTITY_POSITION(moverProp.obj,TRUE)
                SET_ENTITY_COMPLETELY_DISABLE_COLLISION(moverProp.obj,FALSE) 
                
                
                SET_ENTITY_INVINCIBLE( moverProp.obj, TRUE )
                SET_ENTITY_PROOFS( moverProp.obj, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE )
                IF DOES_ENTITY_HAVE_PHYSICS(moverProp.obj) AND GET_IS_ENTITY_A_FRAG(moverProp.obj)
                    SET_DISABLE_BREAKING(moverProp.obj, TRUE)
                    SET_DISABLE_FRAG_DAMAGE(moverProp.obj, TRUE)
                ENDIF
                
                //No tangible effect with this experiment - bug 2409832 
                //SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(moverProp.obj,TRUE)

                b_DoNotProcessNthLabel = FALSE  
				
				#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
				IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE)
					SET_IS_EXTERIOR_ONLY(moverProp.obj, TRUE)	//B* 3202586: We don't want big props culled by interiors.
				ENDIF
				
				iTempLocalBoneIdx = -1		//Clean this up regardless, need to recheck local bones
				IF bMovedSinceSnap
					//Clean up the currently snapped bone
					iFoundLocalBoneIdx = -1
				ELSE
					//If currently snapped, attach the current prop to the new mover
					DMPROP_SNAP_NEW_MOVER()
				ENDIF				
				#ENDIF
				bResetMenuNow = TRUE	//Reset the menu once we've created the mover to update the help buttons
            ENDIF
        ENDIF
		
		//Create PTFX for the mover
		IF bMoverCreated AND NOT bMoverPtfxCreated
			bMoverPtfxCreated = DMPROP_CREATE_PROP_PTFX(moverProp.mModel, moverProp.obj)
		ENDIF
			
		// Handle topdown camera for stunt prop placement
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT 
		DMPROP_TOPDOWN_CAMERA_UPDATE()
		#ENDIF
				
					
		
		
		//Update the mover position
        IF DOES_ENTITY_EXIST(moverProp.obj)
			
            INT ThisMoverIndex = DMPROP_GET_PROP_TYPE_INDEX(moverProp)

			DMPROP_UPDATE_BASIC_MOVER_POS(vPlayerPos, fHeading)
            //GET_GROUND_Z_FOR_3D_COORD(moverProp.vPos+<<0,0,100>>,fGroundZ) //This is the probe's original height.

            //New for bug 2411412
            fGroundZ = GET_SURFACE_Z_BELOW(moverProp.vPos, FALSE)
            TEST_VERTICAL_PROBE_AGAINST_ALL_WATER(moverProp.vPos + <<0,0,vModelMax.z>>, SCRIPT_INCLUDE_MOVER, fWaterZ)
            
            //New water test floats
            FLOAT fWaterTest1, fWaterTest2
            IF NOT GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(GET_ENTITY_COORDS(moverProp.obj) + <<0,0,vModelMax.z>>,fWaterTest1)
                fWaterTest1 = -200
            ENDIF
            IF NOT GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(GET_ENTITY_COORDS(moverProp.obj)+ <<0,0,vModelMax.z>>,fWaterTest2, TRUE)
                fWaterTest2 = -500
            ENDIF
            //VECTOR vWaterCheckReturn //moved scope so the z component can be used for creating a splash effect on placement of a water prop. 
            IF fWaterTest1 >= fWaterTest2   //If water level is below ground level
                b_MoverPropOverWater = FALSE
                fWaterZ = -1    //Reset water Z
            ELSE
                b_MoverPropOverWater = TRUE
                IF ABSF(fWaterZ - fWaterTest2) >= 0.5   //Set the water Z, will persist until mover is out of water
                OR fWaterZ = -1
                    fWaterZ = fWaterTest2
                ENDIF
            ENDIF
            /*
            #if IS_DEBUG_BUILD //Comment in to display Z of player and prop onscreen.
                DISPLAY_TEXT_WITH_FLOAT (0.4, 0.05, "CELL_500", fWaterTest1, 2)  //Player Coords
                DISPLAY_TEXT_WITH_FLOAT (0.4, 0.15, "CELL_501", fWaterTest2, 2)  //Prop Coords
                DISPLAY_TEXT_WITH_FLOAT (0.4, 0.25, "CELL_502", fWaterZ, 2)  //Prop Coords
//              DISPLAY_TEXT_WITH_FLOAT(0.4,0.5,"NUMBER",moverProp.vPos.z,2)
//              DISPLAY_TEXT_WITH_FLOAT(0.4,0.6,"NUMBER",fGroundZ,2)
//              DISPLAY_TEXT_WITH_FLOAT(0.4,0.7,"NUMBER",fWaterZ,2)
//              DRAW_DEBUG_BOX(GET_ENTITY_COORDS(moverProp.obj) + vModelMin, GET_ENTITY_COORDS(moverProp.obj) + vModelMax,0,0,255,100)
            #endif
//          */
            fGroundZ = fMax(fGroundZ, fWaterZ)

			//Special Stunt prop placement
			#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE)
				IF GET_GAME_TIMER() > iSnapTimer + iSnapTimeOut	//Only move if the prop hasn't been recently snapped
					moverProp.vRot = <<moverProp.vRot.x,moverProp.vRot.y,fMoverHeading>>
					SET_ENTITY_ROTATION(moverProp.obj, moverProp.vRot)
	                SET_ENTITY_COORDS(moverProp.obj,moverProp.vPos)
					
					//Check if the mover has moved/rotated after being snapped
					IF NOT bMovedSinceSnap AND NOT ARE_VECTORS_ALMOST_EQUAL(vecSnapPosition, moverProp.vPos + moverProp.vRot, 0.1)
					
						IF 	DMPROP_IS_PROP_SPECIALRACE_CURVED_TRACK(DMPROP_GET_PROP_TYPE_INDEX(moverProp))
							//CPRINTLN(debug_director,"Special Race Curved track detected:: ",vecSnapPosition, " vs ", moverProp.vPos + moverProp.vRot)
							bMovedSinceSnap = TRUE //This isn't causing the phantom unsnapping seen on the new special race track props.  bug 3469921.  Think that's caused by these props applying a rotation on snapping that we don't support on that axis, which is then lost,
						ELSE
							//CPRINTLN(debug_director,"No longer snapped: ",vecSnapPosition, " vs ", moverProp.vPos + moverProp.vRot)
							bMovedSinceSnap = TRUE
						ENDIF
						
					ENDIF
				ELSE
					//Save the snapped position
					vecSnapPosition = GET_ENTITY_COORDS(moverProp.obj, FALSE) + GET_ENTITY_ROTATION(moverProp.obj)
				ENDIF
				//Update the vectors to reflect the actual position
				moverProp.vPos = GET_ENTITY_COORDS(moverProp.obj)
                moverProp.vRot = GET_ENTITY_ROTATION(moverProp.obj)
				fMoverHeading = moverProp.vRot.z
			ELSE
			#ENDIF
			
            IF b_MoverPropOverWater = TRUE
            OR eSelectedPropType[0] = ENUM_TO_INT(CAT_Water)
            
//              TEXT_LABEL_15 tlTemp = "Water Z:"
//              tlTemp += ROUND(vWaterCheckReturn.z * 100)
//              DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.8,"STRING",tlTemp)
//              
                moverProp.vPos.Z = fGroundZ - vModelMin.z
                moverProp.vRot = <<0,0,fMoverHeading>>
                IF eSelectedPropType[0] != ENUM_TO_INT(CAT_Water)
                    moverProp.vPos.z += moverProp.fZoffset
                ENDIF
                SET_ENTITY_COORDS(moverProp.obj,moverProp.vPos)
                SET_ENTITY_HEADING(moverProp.obj, fMoverHeading)
                moverProp.vPos = GET_ENTITY_COORDS(moverProp.obj)
                moverProp.vRot = GET_ENTITY_ROTATION(moverProp.obj)
                
                
            ELIF NOT ARE_VECTORS_ALMOST_EQUAL(vOldMoverPosition, moverProp.vPos+<<0,0,moverProp.fZoffset>>,0.01) 
            OR fOldMoverHeading!= fMoverHeading
                moverProp.vPos.Z = fGroundZ - vModelMin.z + 0.1
                moverProp.vRot.z = fMoverHeading
                SET_ENTITY_ROTATION(moverProp.obj, moverProp.vRot)
                SET_ENTITY_COORDS(moverProp.obj,moverProp.vPos+<<0,0,moverProp.fZoffset>>)
                
                IF moverProp.fZoffset = 0
					IF ePropTypes[iCurrentProp].bPlaceOnGround
                    	PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(moverProp.obj)
					ENDIF
                    moverProp.vPos = GET_ENTITY_COORDS(moverProp.obj) + <<0,0,0.2+ePropTypes[ThisMoverIndex].fMoverAdjust>>
                    moverProp.vRot = GET_ENTITY_ROTATION(moverProp.obj)
                    moverProp.vRot.x = CLAMP(moverProp.vRot.x,-65,65)
                    moverProp.vRot.y = CLAMP(moverProp.vRot.y,-65,65)
                    
                    SET_ENTITY_ROTATION(moverProp.obj,moverProp.vRot)
                    SET_ENTITY_COORDS(moverProp.obj,moverProp.vPos)
                ENDIF
            
            ENDIF
			
			#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			ENDIF
			#ENDIF

	//ENDIF
			
			

			
            vOldMoverPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vplayerPos,fHeading,<<0,fMoverDist,0>>)+<<0,0,moverProp.fZoffset>>

            //Update the shape test, reduce alpha if can't place
            VECTOR vMarkerPos = GET_ENTITY_COORDS(moverProp.obj)+<<0,0,ePropTypes[ThisMoverIndex].fMarkerAdjust>>
			vMarkerPos.z = FMAX(vMarkerPos.z, GET_SURFACE_Z_BELOW(moverProp.vPos + <<0,0,300>>) +1)
			vMarkerPos.z = FMAX(vMarkerPos.z, moverProp.vPos.z + moverProp.vSize.z/2 + DMPROP_GET_MARKER_SCALE(moverprop) + 2 ) //Was just 2, now offset the height of the marker from the top of the prop by using the scale. Still need the extra 2 for a few small ramps.
            IF DMPROP_IS_MOVER_CLEAR_TO_PLACE_PROP(moverProp)
                IF iNumProps = ciMAXOBJECTS //Fix for 2416997. If we are at the prop limit, keep the alpha semi-transparent to show the prop can't be placed.
					DMPROP_DRAW_MARKER(vMarkerPos,DMPROP_GET_MARKER_SCALE(moverprop), default, default, default, default, true)
                    ePropWarning = PW_MaxProps
                    SET_ENTITY_ALPHA(moverProp.obj,100,FALSE)
                ELSE
                    IF NOT ePropTypes[ThisMoverIndex].bFrozen
                    AND iNumDynamicProps = ciMAXDYNAMICOBJECTS //Fix for 2416997 but specific to the dynamic prop total.
						DMPROP_DRAW_MARKER(vMarkerPos,DMPROP_GET_MARKER_SCALE(moverprop), default, default, default, default, true)
                        ePropWarning = PW_MaxDynamicProps
                        SET_ENTITY_ALPHA(moverProp.obj,100,FALSE)
                    ELSE
                        IF ePropTypes[ThisMoverIndex].bFrozen
							DMPROP_DRAW_MARKER(vMarkerPos, DMPROP_GET_MARKER_SCALE(moverprop), 70, 180, 230, default, true)
                        ELSE
							DMPROP_DRAW_MARKER(vMarkerPos, DMPROP_GET_MARKER_SCALE(moverprop), 70, 230, 140, default, true)
                        ENDIF
                        SET_ENTITY_ALPHA(moverProp.obj,220,FALSE)
                    ENDIF
                ENDIF
                
            ELSE
                IF ePropWarning = PW_NotReady OR ePropWarning = PW_Fine OR GET_GAME_TIMER() <= iLastResultTimer +100
					DMPROP_DRAW_MARKER(vMarkerPos,DMPROP_GET_MARKER_SCALE(moverprop),150,150,150, default, true)
                ELSE
					DMPROP_DRAW_MARKER(vMarkerPos,DMPROP_GET_MARKER_SCALE(moverprop), default, default, default, default, true)
                ENDIF
                SET_ENTITY_ALPHA(moverProp.obj,100,FALSE)  //Steve T Test 2485918
            ENDIF
            
            //Update mover controls
            DMPROP_UPDATE_MOVER_CONTROLS()
			
			#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			//Update snapping of props
			DMPROP_UPDATE_PROP_SNAPPING()			
			#ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC DMPROP_UPDATE_SELECTED_PROP_BLIP(INT iSel)
    //Destroy the mover if it exists
    DMPROP_CLEAR_MOVER_DATA()
    //Check if a blip already exists, delete if not for this prop
    CPRINTLN(debug_director,"Clear_Blip Spam Test 3")
    DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()

    //This was spamming - Steve T.
    //Create new blip
    IF NOT DOES_BLIP_EXIST(blSelected)
    AND iSel >= 0
        DM_PROP temp = DMPROP_GET_NTH_PROP(iSel)
        IF DOES_ENTITY_EXIST(temp.obj)
            
            //blSelected = ADD_BLIP_FOR_ENTITY(temp.obj)  //Removed for work on 2485910

            //SET_BLIP_NAME_FROM_TEXT_FILE( blSelected, GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL( temp.mModel ) ) //work on 2485910
            iBlippedProp = iSel

            //Add chevron marker for selected prop and apply z offset for that type.
            VECTOR tempVec = GET_ENTITY_COORDS(temp.obj) //Avoid any pops by using a temp.

            //When the marker isn't in the original dummy z of -99.9 then we know we need to draw it every frame in Prop_Info_Disabling_and_Deletion_Alpha_Management()
            
            INT ThisMarkerAdjustmentIndex = DMPROP_GET_PROP_TYPE_INDEX(temp)
            v_MarkerDraw = <<tempVec.x, tempVec.y, (tempVec.z + ePropTypes[ThisMarkerAdjustmentIndex].fMarkerAdjust)>>
            SETTIMERB (0) //use this to periodically update the chevron for bug 2421778 
            iLastPropSelectionForChevronMarker = iSel
       
            CPRINTLN(debug_director,"Added blip for ",iBlippedProp)
            IF DOES_BLIP_EXIST (temp.BlipIndex)

                //Context blip colouring for 2504896.
                IF ePropTypes[ThisMarkerAdjustmentIndex].bFrozen
                    CPRINTLN(debug_director,"Newly selected prop is static. Colouring blip blue ", ThisMarkerAdjustmentIndex)
                    SET_BLIP_COLOUR (temp.BlipIndex, BLIP_COLOUR_BLUE) //work on 2485910
                ELSE
                    CPRINTLN(debug_director,"Newly selected prop is DYNAMIC. Colouring blip green ", ThisMarkerAdjustmentIndex)
                    SET_BLIP_COLOUR (temp.BlipIndex, BLIP_COLOUR_GREEN) //work on 2485910
                ENDIF

                SET_BLIP_SCALE (temp.BlipIndex, 1.0)

                LOCK_MINIMAP_POSITION (tempVec.x, tempVec.y) //work on 2485910
                LOCK_MINIMAP_ANGLE(ROUND(GET_HEADING_BETWEEN_VECTORS_2D(tempVec,<<0,25000,0>>)))    //Rotate to the North blip

            ENDIF


            SET_ENTITY_ALPHA (temp.obj, c_i_BlippedPropAlphaThisFrame, FALSE) //Steve T. Make prop semi-translucent.


        ENDIF
    ENDIF
ENDPROC

PROC DMPROP_UPDATE_SCENE_LIST()
    //Clear the mover prop if it exists
    IF DOES_ENTITY_EXIST(moverProp.obj)
        DMPROP_CLEAR_MOVER_DATA()
    ENDIF
    
    //Clear the blip and alpha of the selected object
    //CPRINTLN(debug_director,"Clear_Blip Spam Test 4")
    //Steve T - This is being spammed, not by my changes, but is probably okay until the scene stuff moves into its own menu.
    DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()
ENDPROC

PROC DMPROP_UPDATE_PI_MENU(INT iSel)
    //State transition PI changes
    IF eLastPEState <> ePEState
        IF eLastPEState = PES_PropPlace //Delete mover if not in place props menu
            DMPROP_CLEAR_MOVER_DATA()
        ENDIF
        IF ePEState = PES_PropList      //Highlight prop as you're entering prop list
            iLastPISelection = -1
        ENDIF
        IF eLastPEState = PES_PropList  //Remove last prop blip
            DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()
        ENDIF
    ENDIF


    IF ePEState = PES_SceneEdit // 2507008. Keeping this out of the kind of dangerous ELIF block below.
        UNLOCK_MINIMAP_POSITION() //The minimap position should be  unlocked in DMPROP_UPDATE_PI_MENU when the player's selection is not on a placed prop row.
        UNLOCK_MINIMAP_ANGLE()
    ENDIF


    IF ePEState = PES_SceneSelect
        //Clean the mover prop and blipped object
        iSelectedScene = iSel
//        UNLOCK_MINIMAP_POSITION() //Fixes 2498709. The minimap position should be  unlocked in DMPROP_UPDATE_PI_MENU when the player's selection is not on a placed prop row.
        DMPROP_UPDATE_SCENE_LIST()
    ELIF ePEState = PES_PropPlace  //Category or type
        //Update the mover
        UNLOCK_MINIMAP_POSITION() //The minimap position should be  unlocked in DMPROP_UPDATE_PI_MENU when the player's selection is not on a placed prop row.
        UNLOCK_MINIMAP_ANGLE()
        DMPROP_UPDATE_MOVER_PROP()
    ELIF ePEState = PES_PropList
        //Update the selected prop

        //Steve T fix - Make sure we only update the blip if the player has either navigated to a new selection in the menu list or has just deleted a prop.
        //This fixes 2410561 which was happening because this proc was getting spammed every frame as a consequence of it being nested within UPDATE_PI_MENU_PROP_MOVEMENT()
        IF iSel <> iLastPISelection
            CPRINTLN(DEBUG_DIRECTOR,"Blip Update called this frame.")
            DMPROP_UPDATE_SELECTED_PROP_BLIP(iSel)
        ENDIF
    ENDIF
    iLastPISelection = iSel
    eLastPEState = ePEState

    Manage_PropMenu_Scaleform_Help(iSel)
    IF ePEState = PES_SceneSelect// AND NOT bShowCurrentSceneInfo
        IF bShowCurrentSceneInfo iSel = -1 ENDIF
        IF iSel != iPrevSelectedScene
            REFRESH_PROP_MODE_LOCATION_MINI_MAP(iSel)
            iPrevSelectedScene = iSel
        ENDIF
    ELSE
        iPrevSelectedScene = -2
        IF bPropSceneInfoMiniMapActive
            CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP(TRUE) //Was TRUE - updated for bug 2485908 - have the minimap expanded at all times. 
        ENDIF
    ENDIF
ENDPROC


PROC DMPROP_SHOW_SCENE_INFO(INT iPassedSelection, BOOL bDrawCurrentScene = TRUE)
    
    INT iNum, iNumDynamic, iScene
    STRING stSceneLabel
    
    IF IS_CUSTOM_MENU_ON_SCREEN()
        AND IS_CUSTOM_MENU_SAFE_TO_DRAW()
            
            IF bDrawCurrentScene
                iNum = iNumProps
                iNumDynamic = iNumDynamicProps
                iScene = iCurrentScene
                stSceneLabel = "DMPP_CURRSCENE"
            ELSE
                iNum = GET_NUM_PROPS_IN_SCENE(iPassedSelection)
                iNumDynamic = GET_NUM_PROPS_IN_SCENE(iPassedSelection,TRUE)
                iScene = iPassedSelection
                stSceneLabel = "DMPP_SELSCENE"
            ENDIF
            
            IF bLoadSceneRunning= FALSE //Fix for 2498501. Only draw the current scene when one isn't in the process of loading.
                DRAW_GENERIC_SCORE(iScene+1, stSceneLabel, DEFAULT, HUD_COLOUR_WHITE, HUDORDER_TOP )
            ENDIF
            
            IF iNum = ciMAXOBJECTS //Prop limit reached, draw numbers in red
                DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNum, ciMAXOBJECTS, "DMPP_PROPSPLCD", DEFAULT, HUD_COLOUR_RED)     
            ELSE
                DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNum, ciMAXOBJECTS, "DMPP_PROPSPLCD", DEFAULT, HUD_COLOUR_BLUE)
            ENDIF
                        
            IF iNumDynamic = ciMAXDYNAMICOBJECTS //Dynamic Prop limit reached, draw numbers in red
            OR iNum = ciMAXOBJECTS
                DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumDynamic, ciMAXDYNAMICOBJECTS, "DMPP_DYNPPLCD", DEFAULT, HUD_COLOUR_RED)
            ELSE
                DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumDynamic, ciMAXDYNAMICOBJECTS, "DMPP_DYNPPLCD",DEFAULT, HUD_COLOUR_GREEN )
            ENDIF

        ENDIF

ENDPROC

PROC Prop_Info_Disabling_and_Deletion_Alpha_Management(INT iPassedSelection)
         //work on 2485910
        SET_RADAR_ZOOM_TO_DISTANCE(fRadarZoom)
        
        //Scene info display - only show non-current scene on the Scene select list
        DMPROP_SHOW_SCENE_INFO(iPassedSelection, (ePEState <> PES_SceneSelect) OR bShowCurrentSceneInfo)
        
        IF ePEState != PES_PropPlace //2512814
            IF b_SoundLooping = TRUE
                STOP_SOUND (LoopedPI_SoundID)
                RELEASE_SOUND_ID (LoopedPI_SoundID)
                b_SoundLooping = FALSE
            ENDIF
        ENDIF

        THEFEED_HIDE_THIS_FRAME() // For bug 2498499. Should be safe enough to hide the feed when placing props I think. We can no longer accept invites in DM.

        SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)  //2502459

        IF ePEState = PES_PropPlace //2511941
            DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HANDBRAKE) //2409162 - remove handbrake functionality so the player can rotate a prop in a vehicle.
        ENDIF

  
        REPLAY_PREVENT_RECORDING_THIS_FRAME() //Fully prevent prop editor recording for bug 2410393.
        
        //DRAW_GENERIC_METER(iNumProps, ciMAXOBJECTS, "DMPP_PLACEP", HUD_COLOUR_WHITE, -1, HUDORDER_TOP)  //We could use this if we perhaps have model memory limits.


        IF v_MarkerDraw.z <> -99.9 //If the marker isn't in the original dummy z of -99.9 then we know we need to draw it.
            IF iLastPropSelectionForChevronMarker >= 0
                
                DM_PROP UpdateTemp = DMPROP_GET_NTH_PROP(iLastPropSelectionForChevronMarker)
                
                IF DOES_ENTITY_EXIST(UpdateTemp.obj)
                    INT UpdateMarkerAdjustmentIndex = DMPROP_GET_PROP_TYPE_INDEX(UpdateTemp)
                    INT iGreenAdjust = 80 * BOOL_TO_INT(NOT ePropTypes[UpdateMarkerAdjustmentIndex].bFrozen)
                    
                    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                        IF VDIST2 (GET_ENTITY_COORDS(PLAYER_PED_ID()), v_MarkerDraw) > (DMPROP_GET_MARKER_SCALE(UpdateTemp) + DMPROP_GET_MARKER_SCALE(UpdateTemp) ) // 0.35 // Do not draw chevron if player is very close to it. Prevents unsightly intersection. 2510942  
                           // DRAW_MARKER(MARKER_ARROW, v_MarkerDraw, <<0.0, 0.0, 0.0>>, <<0.0, 180.0, 0.0>>, <<0.5, 0.5, 0.5>>, 79, 175+iGreenAdjust, 239-iGreenAdjust, 150, FALSE, FALSE, EULER_YXZ, FALSE)	   
						   
							v_MarkerDraw = GET_ENTITY_COORDS(UpdateTemp.obj)+<<0,0,ePropTypes[UpdateMarkerAdjustmentIndex].fMarkerAdjust>>
							v_MarkerDraw.z = FMAX(v_MarkerDraw.z, GET_SURFACE_Z_BELOW(UpdateTemp.vPos + <<0,0,300>>) +1)
							v_MarkerDraw.z = FMAX(v_MarkerDraw.z, UpdateTemp.vPos.z + UpdateTemp.vSize.z/2 + DMPROP_GET_MARKER_SCALE(UpdateTemp) + 2 ) //Was just 2, now offset the height of the marker from the top of the prop by using the scale. Adding an extra 2 for a few small ramps.
						   
						   	//Steve T - adding a bit extra for the largest of props as the chevron can intersect the prop on some large hill slopes.
							IF DMPROP_GET_MARKER_SCALE(UpdateTemp) > 4			
								v_MarkerDraw.z = v_MarkerDraw.z + 7.0 					
						   	ENDIF
						   
                        	DMPROP_DRAW_MARKER(v_MarkerDraw, DMPROP_GET_MARKER_SCALE(UpdateTemp), 79, 175+iGreenAdjust, 239-iGreenAdjust)
						ENDIF
                    ENDIF
                    //When the prop radar blip alters, we get the new v_Marker vec from DMPROP_UPDATE_SELECTED_PROP_BLIP() and TIMERB is initialised.
                    //However for bug 2421778 we need to update the marker vec every second in case a dynamic prop has been moved about.
                    IF TIMERB () > 500 
                            
                            //Add chevron marker for selected prop and apply z offset for that type.
                           //VECTOR UpdateTempVec = GET_ENTITY_COORDS(UpdateTemp.obj) //Avoid any pops by using a temp.

                            //When the marker isn't in the original dummy z of -99.9 then we know we need to draw it every frame in Prop_Info_Disabling_and_Deletion_Alpha_Management()
                            //v_MarkerDraw = <<UpdateTempVec.x, UpdateTempVec.y, (UpdateTempVec.z + ePropTypes[UpdateMarkerAdjustmentIndex].fMarkerAdjust)>>
            
													   
							v_MarkerDraw = GET_ENTITY_COORDS(UpdateTemp.obj)+<<0,0,ePropTypes[UpdateMarkerAdjustmentIndex].fMarkerAdjust>>
							v_MarkerDraw.z = FMAX(v_MarkerDraw.z, GET_SURFACE_Z_BELOW(UpdateTemp.vPos + <<0,0,300>>) +1)
							v_MarkerDraw.z = FMAX(v_MarkerDraw.z, UpdateTemp.vPos.z + UpdateTemp.vSize.z/2 + DMPROP_GET_MARKER_SCALE(UpdateTemp) + 2 ) //Was just 2, now offset the height of the marker from the top of the prop by using the scale. Still need the extra 2 for a few small ramps.
						   
			
			
                            SETTIMERB (0) //use this to periodically update the chevron for bug 2421778 
                         
                    ENDIF
                ENDIF

            ENDIF

        ENDIF
                     

        IF iPassedSelection = 1//Will remove if it transpires that this function will remain simplified.
        ENDIF

        /* Remove this temporarily, as we're only altering the alpha when the prop blip is updated now.        
        IF iBlippedProp > -1

            IF DOES_ENTITY_EXIST (eProps[iBlippedProp].obj)

                IF iPassedSelection < ciPROPADDLINES

                    IF b_NeedToResetAlphaForBlippedProp
                        SET_ENTITY_ALPHA (eProps[iBlippedProp].obj, 255, FALSE)
                        b_NeedToResetAlphaForBlippedProp = FALSE
                        i_LastBlippedPropForAlpha = iBlippedProp
                    ENDIF

                ELSE
                    
                             
                 
                    IF NOT b_NeedToResetAlphaForBlippedProp
                        SET_ENTITY_ALPHA (eProps[iBlippedProp].obj, i_BlippedPropAlphaThisFrame, FALSE )
                        b_NeedToResetAlphaForBlippedProp = TRUE
                        i_LastBlippedPropForAlpha = iBlippedProp
                    ENDIF 


                    IF iBlippedProp <> i_LastBlippedPropForAlpha
                        IF DOES_ENTITY_EXIST (eProps[i_LastBlippedPropForAlpha].obj)
                            SET_ENTITY_ALPHA (eProps[i_LastBlippedPropForAlpha].obj, 255, FALSE )
                            b_NeedToResetAlphaForBlippedProp = FALSE
                        ENDIF
                    ENDIF


                ENDIF

            ELSE

                b_NeedToResetAlphaForBlippedProp = FALSE

            ENDIF

        ENDIF
        */
        
        
ENDPROC

PROC DMPROP_CLEAN_OBJECT_ARRAY(BOOL bSetAsUpdatedSinceSave = FALSE)
    IF iNumProps > 0
    AND bSetAsUpdatedSinceSave
        b_PropSceneUpdatedSinceLastSave = TRUE
    ENDIF
    iNumProps = 0
    iNumDynamicProps = 0
    iNumRestrictedUseProps = 0
    //Remove all existing props
    int i
    for i = 0 to ciMAXOBJECTS - 1
        if DOES_ENTITY_EXIST(eProps[i].obj)
            REMOVE_ALL_STICKY_BOMBS_FROM_ENTITY(eProps[i].obj) //Fix for 2513944 
            DELETE_OBJECT(eProps[i].obj)
			SAFE_RELEASE_MODEL(eProps[i].mModel)
        endif
        eProps[i].vPos = <<0,0,0>>
        eProps[i].vRot = <<0,0,0>>
        eProps[i].fZoffset = 0
        eProps[i].mModel = DUMMY_MODEL_FOR_SCRIPT
    endfor
ENDPROC

PROC DMPROP_SAVE_SCENE(INT iScene)
    b_PropSceneUpdatedSinceLastSave = FALSE
    iCurrentScene = iScene
    iSelectedScene = (iScene +1) % ciMAXSCENES

    INT i = 0   //Scene array iterator
    INT j
    REPEAT ciMAXOBJECTS j
        IF DOES_ENTITY_EXIST(eProps[j].obj)
            eScenes[iScene][i].mModel = eProps[j].mModel
            escenes[iScene][i].vPos = GET_ENTITY_COORDS(eProps[j].obj)
            escenes[iScene][i].vRot = GET_ENTITY_ROTATION(eProps[j].obj)
            i += 1
        ENDIF
    ENDREPEAT
    
    
    //Clear the rest of the objects out
    WHILE i < ciMAXOBJECTS
        eScenes[iScene][i].mModel = DUMMY_MODEL_FOR_SCRIPT
        eScenes[iScene][i].vPos = <<0,0,0>>
        eScenes[iScene][i].vRot = <<0,0,0>>
        i += 1
    ENDWHILE
    
    sdSceneData[ iScene ].iTotalProps = GET_NUM_PROPS_IN_SCENE( iScene )
    sdSceneData[ iScene ].iDynamicProps = GET_NUM_PROPS_IN_SCENE( iScene, TRUE )
    sdSceneData[ iScene ].tlModeLocation = GET_SCENE_MODE_PROP_LOCATION( iScene )
    TEXT_LABEL_23 tlTemp = "Scene "
    tlTemp += (iScene + 1) //Fix for 
    sdSceneData[ iScene ].tlSceneName = tlTemp
        
ENDPROC

PROC DMPROP_LOAD_SCENE(INT &iScene)
	IF iScene <0 OR iScene >= ciMAXSCENES
		iScene = 0								//url:bugstar:5092622 - Protect against illegal array access
	ENDIF
    iSelectedScene = (iScene +1) %ciMAXSCENES
    iCurrentScene = iScene
    
    //Reset scene info map
    IF NOT bShowCurrentSceneInfo
        CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP(TRUE)
    ENDIF

    b_PropSceneUpdatedSinceLastSave = FALSE

    //Start loading the scene
    bLoadSceneRunning= TRUE
    
    //  CPRINTLN(debug_dan,"Scene objects: ",iNumProps)
    
    //Skip fade-out if the current scene and the scene to load are empty
    IF iNumProps = 0 AND sdSceneData[iScene].iTotalProps = 0
        bSkipFadeOut = TRUE
    ENDIF
    
    
    CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP( FALSE ) //Was TRUE - updated for bug 2485908 - have the minimap expanded at all times. 
ENDPROC

PROC DMPROP_SAVE_SCENES_TO_GLOBALS()
    int iScene, jObject
    REPEAT ci_MAXPROPSCENES iScene
        REPEAT ci_MAXPropObjectsPerScene jObject
            g_savedGlobals.sDirectorModeData.vSavedPropScenesPos[iScene*ci_MAXPropObjectsPerScene + jObject] = eScenes[iScene][jObject].vPos
            g_savedGlobals.sDirectorModeData.vSavedPropScenesRot[iScene*ci_MAXPropObjectsPerScene + jObject]= eScenes[iScene][jObject].vRot
            g_savedGlobals.sDirectorModeData.vSavedPropScenesModel[iScene*ci_MAXPropObjectsPerScene + jObject] = ENUM_TO_INT(eScenes[iScene][jObject].mModel)
        ENDREPEAT
        g_savedGlobals.sDirectorModeData.vSavedPropScenesNames[iScene] = sdSceneData[iScene].tlSceneName
    ENDREPEAT
ENDPROC

PROC DMPROP_LOAD_SCENES_FROM_GLOBALS()
    int iScene, jObject
    REPEAT ci_MAXPROPSCENES iScene
        REPEAT ci_MAXPropObjectsPerScene jObject
            eScenes[iScene][jObject].vPos = g_savedGlobals.sDirectorModeData.vSavedPropScenesPos[iScene*ci_MAXPropObjectsPerScene + jObject]
            eScenes[iScene][jObject].vRot = g_savedGlobals.sDirectorModeData.vSavedPropScenesRot[iScene*ci_MAXPropObjectsPerScene + jObject]
            eScenes[iScene][jObject].mModel = INT_TO_ENUM(MODEL_NAMES, g_savedGlobals.sDirectorModeData.vSavedPropScenesModel[iScene*ci_MAXPropObjectsPerScene + jObject])
        ENDREPEAT
        
        sdSceneData[ iScene ].iTotalProps = GET_NUM_PROPS_IN_SCENE( iScene )
        sdSceneData[ iScene ].iDynamicProps = GET_NUM_PROPS_IN_SCENE( iScene, TRUE ) 
        sdSceneData[ iScene ].tlModeLocation = GET_SCENE_MODE_PROP_LOCATION( iScene )
        sdSceneData[ iScene ].tlSceneName = g_savedGlobals.sDirectorModeData.vSavedPropScenesNames[iScene]
        TEXT_LABEL_23 tlTemp = "Scene "
        tlTemp += (iScene + 1 ) //Fix for 2510837. Make sure Scenes are labelled from 1 upwards not the internal 0 designation.
        sdSceneData[ iScene ].tlSceneName = tlTemp
    
    ENDREPEAT
ENDPROC

PROC DMPROP_STORE_PLAYERS_VEHICLE()
    IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
        vPropEditVeh = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
    ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
        vPropEditVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
    ELIF DOES_ENTITY_EXIST(GET_LAST_DRIVEN_VEHICLE())
        IF NOT IS_ENTITY_DEAD(GET_LAST_DRIVEN_VEHICLE())
            vPropEditVeh = GET_LAST_DRIVEN_VEHICLE()
        ENDIF
    ENDIF
    IF DOES_ENTITY_EXIST(vPropEditVeh)
        SET_ENTITY_AS_MISSION_ENTITY(vPropEditVeh)
    ENDIF
ENDPROC


#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
PROC DMPROP_DISABLE_PROP_COLLISION_IN_INTERIOR()
	IF bLoadSceneRunning
		EXIT	//Exit if we've not loaded the scene yet
	ENDIF
	
	//Check if the player is in an interior
	INTERIOR_INSTANCE_INDEX playerInt = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	INT i
	DM_PROP tempProp
	BOOL bShouldDisable = IS_VALID_INTERIOR(playerInt)

	FOR i = 0 to iNumProps - 1
		tempProp = DMPROP_GET_NTH_PROP(i)
		IF DOES_ENTITY_EXIST(tempProp.obj)
			IF DMPROP_IS_PROP_A_STUNT_PROP(DMPROP_GET_PROP_TYPE_INDEX(tempProp), FALSE)
				IF GET_ENTITY_COLLISION_DISABLED(tempProp.obj) != bShouldDisable
					SET_ENTITY_COLLISION(tempProp.obj, NOT bShouldDisable)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC
#ENDIF


BOOL bObjectsCleaned = FALSE


BOOL b_PostLoadFailsafeRepositionRequired = FALSE
BOOL b_PostLoadSceneAdjustmentPause = FALSE
INT i_PostLoadScene_StartTime, i_PostLoadScene_CurrentTime 
INT i_PostLoadAdjustmentPauseTime = 1500
VECTOR v_CurrentPlayerPos
BOOL bPreviouslyUsingMouseControl = FALSE

//Called every frame, should process 
PROC DMPROP_PER_FRAME_PROCESSING()
    // B* 2503473 - Stop mountain lions attacking when in prop edit
    SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME( PLAYER_ID() )
    
    //Disable cinematic cam
    DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
    IF IS_CINEMATIC_CAM_RENDERING()
        SET_CINEMATIC_MODE_ACTIVE(FALSE)
    ENDIF
    
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	//Disable topdown cam if we're not placing props
	IF ePEState != PES_PropPlace AND DOES_CAM_EXIST(ElevatedCam)
		DMPROP_TOPDOWN_CAMERA_TOGGLE(FALSE)
	ENDIF
	
	//Manage the fade-in for a long warp
	IF iToggleCamFadeTimeLeft > 0
		iToggleCamFadeTimeLeft -= ROUND(GET_FRAME_TIME() * 1000)
		
		IF (iToggleCamFadeTimeLeft <= 0)
			iToggleCamFadeTimeLeft = 0
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
		ENDIF
	ENDIF
	#ENDIF
	
    //Underwater check
    IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
//        ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT, FALSE)
//        SET_CONTROL_VALUE_NEXT_FRAME(PLAYER_CONTROL, INPUT_SPRINT,1.0)
//        SET_CONTROL_VALUE_NEXT_FRAME(PLAYER_CONTROL, INPUT_MOVE_UP,1.0)
		SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVEBLENDRATIO_SPRINT)
    ENDIF
    
    //Check if the control scheme has changed
    IF bPreviouslyUsingMouseControl <> IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        DMPROP_SWITCH_PC_PAD_CONTROLS()
        bPreviouslyUsingMouseControl = IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
    ENDIF
	
   
    
    //Scene loading
    IF bLoadSceneRunning
        IF NOT IS_SCREEN_FADED_OUT()
        AND NOT bSkipFadeOut
            IF NOT IS_SCREEN_FADING_OUT()
                DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
            ENDIF
        ELSE
            IF NOT bObjectsCleaned


                IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
                    CPRINTLN(debug_director,"Clearing area of fragments and fires after load_scene fade out.") //2502454

                    CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0)
                    STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()),1000)
                ENDIF


        
                //Clean the existing objects
                DMPROP_CLEAN_OBJECT_ARRAY()
                
                //Store the model names in the array
                INT i
				REPEAT ciMAXOBJECTS i
                    IF eScenes[iCurrentScene][i].mModel != DUMMY_MODEL_FOR_SCRIPT
                        eProps[i].vPos = eScenes[iCurrentScene][i].vPos
                        eProps[i].vRot = eScenes[iCurrentScene][i].vRot
                        eProps[i].mModel = eScenes[iCurrentScene][i].mModel
                        eProps[i].fZoffset = 0
            //          CPRINTLN(debug_dan,"Added object at ",i)
                        iNumProps += 1
						
						//Model check if we're using a valid model (i.e., not allowing stunt props with the tunable turned off
						IF DMPROP_GET_PROP_TYPE_INDEX(eProps[i]) = -1
							//Prop not in list, replace with Crate :)
							eProps[i].mModel = PROP_CEMENTBAGS01
						ENDIF
                    ENDIF
                ENDREPEAT
                
                bObjectsCleaned = TRUE

                CPRINTLN(debug_director,"Resetting post load scene player adjustment values.")
                b_PostLoadFailsafeRepositionRequired = FALSE
                b_PostLoadSceneAdjustmentPause = FALSE

                IF NOT IS_PED_INJURED(PLAYER_PED_ID()) //2502846
                    v_CurrentPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
                ENDIF

            ELSE
                
                //Screen faded out, do per-frame object creation
                IF iSceneObjectToLoad < ciMAXOBJECTS
                    IF eScenes[iCurrentScene][iSceneObjectToLoad].mModel != DUMMY_MODEL_FOR_SCRIPT
                        REQUEST_MODEL(eProps[iSceneObjectToLoad].mModel)
                        IF HAS_MODEL_LOADED(eProps[iSceneObjectToLoad].mModel)
                        //Create the object

                            //2474500 - Need to check player position in case he ends up stuck under or in a newly loaded prop.
                            IF b_PostLoadFailsafeRepositionRequired = FALSE
                                IF GET_DISTANCE_BETWEEN_COORDS (v_CurrentPlayerPos, eProps[iSceneObjectToLoad].vPos) < 3.5
                                    CPRINTLN(debug_director,"<DMPROP>Player in close proximity to loaded prop in scene")
                                    b_PostLoadFailsafeRepositionRequired = TRUE
                                ENDIF
                            ENDIF


                            eProps[iSceneObjectToLoad].obj = CREATE_OBJECT(eProps[iSceneObjectToLoad].mModel,eProps[iSceneObjectToLoad].vPos)

                            IF DOES_ENTITY_EXIST (eProps[iSceneObjectToLoad].obj) //2502846
                            
                                SET_ENTITY_COORDS(eProps[iSceneObjectToLoad].obj,eProps[iSceneObjectToLoad].vPos)
                                SET_ENTITY_ROTATION(eProps[iSceneObjectToLoad].obj,eProps[iSceneObjectToLoad].vRot)
                     
                                eProps[iSceneObjectToLoad].BlipIndex = ADD_BLIP_FOR_ENTITY(eProps[iSceneObjectToLoad].obj) //Work on 2485910
                                SHOW_HEIGHT_ON_BLIP (eProps[iSceneObjectToLoad].BlipIndex, FALSE)
                                SET_BLIP_NAME_FROM_TEXT_FILE( eProps[iSceneObjectToLoad].BlipIndex, GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL( eProps[iSceneObjectToLoad].mModel ) )
                                SET_BLIP_SCALE (eProps[iSceneObjectToLoad].BlipIndex, 0.75)
                                SET_BLIP_COLOUR (eProps[iSceneObjectToLoad].BlipIndex, BLIP_COLOUR_PURPLEDARK) 
        //                      CPRINTLN(debug_dan, "created object at ",iSceneObjectToLoad)
                                //Dynamic object fix for 2416882 
                                //Need to put in routine that updates the dynamic props total in the scene.
                                INT ThisLoadedPropCategoryIndex = DMPROP_GET_PROP_TYPE_INDEX(eProps[iSceneObjectToLoad])
                                IF NOT ePropTypes[ThisLoadedPropCategoryIndex].bFrozen
                                    CPRINTLN(debug_director,"<DMPROP>This Prop  is included in the DYNAMIC Category and not frozen", ThisLoadedPropCategoryIndex)
                                    iNumDynamicProps += 1
//                                    APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(eProps[iSceneObjectToLoad].obj, APPLY_TYPE_FORCE, <<0,0,-10>>, 0, FALSE, TRUE)
                                ENDIF


                                IF IsPropModelRestrictedUse(ePropTypes[ThisLoadedPropCategoryIndex].model)
                                    CPRINTLN(debug_director,"Adding new RESTRICTED USE prop during scene load.")
                                    iNumRestrictedUseProps += 1
                                ENDIF



                                SET_ENTITY_INVINCIBLE( eProps[iSceneObjectToLoad].obj, TRUE )
                                SET_ENTITY_PROOFS( eProps[iSceneObjectToLoad].obj, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE )
                                IF DOES_ENTITY_HAVE_PHYSICS(eProps[iSceneObjectToLoad].obj) AND GET_IS_ENTITY_A_FRAG(eProps[iSceneObjectToLoad].obj)
                                    SET_DISABLE_BREAKING(eProps[iSceneObjectToLoad].obj, TRUE)
                                    SET_DISABLE_FRAG_DAMAGE(eProps[iSceneObjectToLoad].obj, TRUE)
                                ENDIF
                                
                                FREEZE_ENTITY_POSITION(eProps[iSceneObjectToLoad].obj,TRUE)
                                SET_ENTITY_COLLISION(eProps[iSceneObjectToLoad].obj,TRUE)
								
								SAFE_RELEASE_MODEL(eProps[iSceneObjectToLoad].mModel)
                            ENDIF
                            //Move to next prop when PTFX creation is done
							IF DMPROP_CREATE_PROP_PTFX(eProps[iSceneObjectToLoad].mModel, eProps[iSceneObjectToLoad].obj)
                            	iSceneObjectToLoad++
							ENDIF
                        ENDIF
                    ELSE
                        //Skip all empty objects
                        WHILE iSceneObjectToLoad < ciMAXOBJECTS
                        AND eScenes[iCurrentScene][iSceneObjectToLoad].mModel = DUMMY_MODEL_FOR_SCRIPT
                            iSceneObjectToLoad++
                        ENDWHILE
                      CPRINTLN(debug_director,"<DMPROP>Skipped all the way to ",iSceneObjectToLoad)
                    ENDIF 
                          
                ELSE
                    //We've finished prop creation, reposition player


                    IF b_PostLoadSceneAdjustmentPause = FALSE

                        VECTOR vPosTemp
                        FLOAT vHeadTemp
                        //IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
                        IF DOES_ENTITY_EXIST(vPropEditVeh)
                        AND NOT IS_ENTITY_DEAD(vPropEditVeh)
                            //Reposition the player's vehicle
                            vPosTemp = GET_ENTITY_COORDS(vPropEditVeh)
                            vHeadTemp = GET_ENTITY_HEADING(vPropEditVeh)
                            IF NOT START_UP_SHAPETEST(vPosTemp, vHeadTemp , GET_ENTITY_MODEL(vPropEditVeh),FALSE)
                                IF START_UP_ROAD_NODE_TEST(vPosTemp, vHeadTemp,GET_ENTITY_MODEL(vPropEditVeh),DEFAULT,FALSE)
                                   
                                    //Check for 2515189. Tunnel weirdness when repositioning.
                                    INTERIOR_INSTANCE_INDEX tempRepositionInterior
                                    tempRepositionInterior = GET_INTERIOR_FROM_ENTITY(vPropEditVeh)
                                    IF IS_VALID_INTERIOR(tempRepositionInterior)
                                        CPRINTLN(debug_director,"Player vehicle was in valid interior. NOT performing reposition. See 2515189 ")    
                                    ELSE
                                        SET_ENTITY_COORDS(vPropEditVeh,vPosTemp)
                                        CPRINTLN(debug_director,"Repositioning the player and vehicle after interior check to ",vPosTemp)
                                    ENDIF

                                ENDIF
                            ENDIF
                        ENDIF
                    //Reposition player if not in his vehicle
                        IF (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
                        AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
                        //Reposition the player
                        vPosTemp = GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0,0,0.1>>
                        vHeadTemp = GET_ENTITY_HEADING(PLAYER_PED_ID())
                            CPRINTLN(debug_director,"Running check...",vPosTemp)

                            IF b_PostLoadFailsafeRepositionRequired
                                vPosTemp.z = (vPosTemp.z + 2.0)    //Adjust for overhangs...
                            ENDIF
                            

                            IF NOT START_UP_SHAPETEST(vPosTemp, vHeadTemp)

                                IF START_UP_ROAD_NODE_TEST(vPosTemp, vHeadTemp)
                                    IF b_PostLoadFailsafeRepositionRequired
                                        //vPosTemp.z = (vPosTemp.z + 2.0)
                                        CPRINTLN(debug_director,"Repositioning the player to adjusted z vector",vPosTemp)
                                        SET_ENTITY_COORDS(PLAYER_PED_ID(),vPosTemp)
                                        b_PostLoadFailsafeRepositionRequired = FALSE
                                    ELSE
                                        CPRINTLN(debug_director,"Repositioning the player to non-adjusted z vector",vPosTemp)
                                        SET_ENTITY_COORDS(PLAYER_PED_ID(),vPosTemp)
                                    ENDIF
                                ENDIF

                            ELSE

                                CPRINTLN(debug_director,"Not running check...",vPosTemp)

                            ENDIF


                        ENDIF               

                        //Making sure this is consistently called if it hasn't already been adjusted above for whatever reason..
                        IF b_PostLoadFailsafeRepositionRequired = TRUE

                            i_PostLoadAdjustmentPauseTime = 3000 //If the failsafe has been triggered give the player a little more time to settle. 2497161. We need to make sure he does not get trapped under objects.
                            
                            INTERIOR_INSTANCE_INDEX checkInterior
                            BOOL bPlayerInInterior = FALSE

                            //Need this to prevent the player being warped outside of an interior if too near a prop. Nasty bug. 2507608
                            checkInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
                            IF IS_VALID_INTERIOR(checkInterior)
                                CPRINTLN(debug_director,"Player in interior - preventing adjusted vector.")
                                bPlayerInInterior = TRUE
                            ENDIF
                                

                            IF (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
                            AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
                            AND bPlayerInInterior = FALSE 

                                v_CurrentPlayerPos.z = (v_CurrentPlayerPos.z + 1.75)
                                CPRINTLN(debug_director,"Failsafe repositioning the player to adjusted z vector",vPosTemp)
                                SET_ENTITY_COORDS(PLAYER_PED_ID(),v_CurrentPlayerPos)
                                b_PostLoadFailsafeRepositionRequired = FALSE
                            
                            ENDIF
            
                        ELSE
    
                             i_PostLoadAdjustmentPauseTime = 1500

                        ENDIF               
                    
                        i_PostLoadScene_StartTime = GET_GAME_TIMER()
                        b_PostLoadSceneAdjustmentPause = TRUE //Signal that we want to give this routine a small adjustment phase to make sure the player is placed correctly.


                    ELSE

                        i_PostLoadScene_CurrentTime = GET_GAME_TIMER()

                        

                        IF (i_PostLoadScene_CurrentTime - i_PostLoadScene_StartTime) > i_PostLoadAdjustmentPauseTime

                            bObjectsCleaned = FALSE
                            bLoadSceneRunning = FALSE
                            b_PostLoadSceneAdjustmentPause = FALSE
                            bSkipFadeOut = FALSE
                            iSceneObjectToLoad = 0

                            IF NOT (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
                                DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
                            ENDIF
							
                        ELSE
                        
                            CPRINTLN(debug_director,"Post load scene pause ongoing.")

                        ENDIF

                    ENDIF //End of b_PostLoadSceneAdjustmentPause condition check.

                ENDIF
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC DMPROP_ACTIVATE_PHYSICS_ON_PROPS()
    INT i, iType
    REPEAT ciMAXOBJECTS i
        IF DOES_ENTITY_EXIST(eProps[i].obj)
            iType = DMPROP_GET_PROP_TYPE_INDEX(eProps[i])
            
            //Special mover conditions
            IF NOT ePropTypes[iType].bFrozen
                FREEZE_ENTITY_POSITION(eProps[i].obj,FALSE) //Only unfreeze certain objects
                ACTIVATE_PHYSICS(eProps[i].obj)
//                CPRINTLN(debug_dan,"Activated prop ",GET_MODEL_NAME_FOR_DEBUG(eProps[i].mModel))
            ENDIF
            SET_ENTITY_INVINCIBLE( eProps[i].obj, FALSE )
            SET_ENTITY_PROOFS( eProps[i].obj, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE )
            SET_ENTITY_COLLISION(eProps[i].obj,TRUE)
            IF DOES_ENTITY_HAVE_PHYSICS(eProps[i].obj) AND GET_IS_ENTITY_A_FRAG(eProps[i].obj)
                RESET_DISABLE_BREAKING(eProps[i].obj)
                SET_DISABLE_FRAG_DAMAGE(eProps[i].obj, FALSE)
            ENDIF
        ENDIF
        
    ENDREPEAT

ENDPROC

/// PURPOSE:
///    Removes the mover and other prop editor assets
PROC DMPROP_EXIT_PROP_EDIT()

    CPRINTLN(debug_director,"Re-enabled taxi hailing.")
    DISABLE_TAXI_HAILING(FALSE) //2530261
	
	//Exit the topdown camera
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	DMPROP_TOPDOWN_CAMERA_TOGGLE(false)
	#ENDIF


    //Remove the mover
    DMPROP_CLEAR_MOVER_DATA()
    
    //Activate all prop physics
    DMPROP_ACTIVATE_PHYSICS_ON_PROPS()
    
    //Remove the blip/alpha
    CPRINTLN(debug_director,"Clear_Blip Spam Test 5")
    DMPROP_CLEAR_BLIP_AND_ALPHA_FOR_BLIPPED_PROP()
    iForceReplaceProp = -1
    iSelectedScene = 0
//    iCurrentScene = 0         //Needs to stay uninitialised when booting up Director Mode
    iLastPISelection = -1
    
    sPropEditorHelpString = ""  //Clear the help text message on exit
    
    //Mark last vehicle as no longer needed
    IF DOES_ENTITY_EXIST(vPropEditVeh)
        SET_VEHICLE_AS_NO_LONGER_NEEDED(vPropEditVeh)
    ENDIF
    
    //Make sure any looped sounds from manipulating the prop mover are cleaned up.
    IF b_SoundLooping = TRUE
        STOP_SOUND (LoopedPI_SoundID)
        RELEASE_SOUND_ID (LoopedPI_SoundID)
    ENDIF

    b_SoundLooping = FALSE

    REMOVE_NAMED_PTFX_ASSET("scr_mp_creator")

    CPRINTLN(debug_director, "Removing Prop Editor Particle FX assets on exit.")
        
    //Clean up scene blips
    CLEAN_UP_PROP_MODE_LOCATION_MINI_MAP(TRUE)  // A full clean required to return the minimap to normal size on Prop Editor exit.
    
ENDPROC

/// PURPOSE:
///    Cleans up Prop Editor and removes the objects from the world
PROC CLEANUP_DM_PROPS()
    //Exit the prop editor
    DMPROP_EXIT_PROP_EDIT()

    CLEAR_ADDITIONAL_TEXT(ODDJOB_TEXT_SLOT,FALSE)
    
    //Clean all the objects
    DMPROP_CLEAN_OBJECT_ARRAY()
ENDPROC

FUNC BOOL DMPROP_IS_MODEL_AN_ANIMAL(model_names animalModel)
    switch animalModel
        case A_C_BOAR FALLTHRU
        CASE A_C_CAT_01 FALLTHRU
        CASE A_C_COW FALLTHRU
        CASE A_C_COYOTE FALLTHRU
        CASE A_C_DEER FALLTHRU
        CASE A_C_HUSKY FALLTHRU
        CASE A_C_MTLION FALLTHRU
        CASE A_C_PIG FALLTHRU
        CASE A_C_POODLE FALLTHRU
        CASE A_C_PUG FALLTHRU
        CASE A_C_RABBIT_01 FALLTHRU
        CASE A_C_RETRIEVER FALLTHRU
        CASE A_C_ROTTWEILER FALLTHRU
        CASE A_C_SHEPHERD FALLTHRU
        CASE A_C_WESTY FALLTHRU
        CASE A_C_CHICKENHAWK FALLTHRU
        CASE A_C_CORMORANT FALLTHRU
        CASE A_C_CROW FALLTHRU
        CASE A_C_HEN FALLTHRU
        CASE A_C_PIGEON FALLTHRU
        CASE A_C_SEAGULL FALLTHRU
        CASE IG_ORLEANS
            RETURN TRUE
        BREAK
    endswitch
    RETURN FALSE
ENDFUNC

PROC DMPROP_WARP_TO_SELECTED_PROP( DM_PROP eProp )

    VECTOR  vOriganlPos         = GET_ENTITY_COORDS( PLAYER_PED_ID() )
    FLOAT   fOriginalHeading    = GET_ENTITY_HEADING( PLAYER_PED_ID() )
    VECTOR  vMoveToCoord        = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( eProp.obj, GetPropModelWarpOffset(eProp.mModel))    //  eProp.vPos  
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT //3198244 3197196 - Fix for tire wall and wall rides. Player looks stupid when warped on them or just beside, moving him away a little for these categories on a general basis.
	IF ePropTypes[ DMPROP_GET_PROP_TYPE_INDEX( eProp ) ].category = CAT_StuntTyres
	OR ePropTypes[ DMPROP_GET_PROP_TYPE_INDEX( eProp ) ].category = CAT_StuntWalls
		CLOGLN( DEBUG_DIRECTOR, CHANNEL_SEVERITY_DEBUG1, "Warping to Stunt Wall or Stunt Tyre category. Overwriting any offset with general value. " )
		vMoveToCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( eProp.obj, <<-5.0, -5.0, 0.0>>)
	ENDIF
	#ENDIF
	
	
	
    FLOAT   fMoveToHeading      = 0.0
    
    VECTOR  vModelDimMin        = <<0.0, 0.0, 0.0>>
    VECTOR  vModelDimMax        = <<0.0, 0.0, 0.0>>
            
//  BOOL    bIsWaterProp        = FALSE
//  BOOL    bIsAnimal           = FALSE
    
//  IF ePropTypes[ DMPROP_GET_PROP_TYPE_INDEX( eProp ) ].category = CAT_Water 
//      CPRINTLN( DEBUG_DIRECTOR, "Setting water prop selected bool.")
//      bIsWaterProp = TRUE
//  ENDIF
//  
//  IF DMPROP_IS_MODEL_AN_ANIMAL( GET_PLAYER_MODEL() )
//      CPRINTLN( DEBUG_DIRECTOR, "Setting playing as animal bool.")
//      bIsAnimal = TRUE
//  ENDIF

    //Cancel if the player's entering a vehicle
    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
        EXIT
    ENDIF
    
    CLEAR_PED_TASKS(PLAYER_PED_ID())
    SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)   //Disable control when warping
    
    //  Fade screen
    DO_SCREEN_FADE_OUT( DEFAULT_FADE_TIME_SHORT)
    WHILE NOT IS_SCREEN_FADED_OUT()
        WAIT( 0 )
        PropEditorHidesPerFrame(TRUE)
		
		IF IS_SCREEN_FADED_IN()
			 DO_SCREEN_FADE_OUT( DEFAULT_FADE_TIME_SHORT)
		ENDIF
    ENDWHILE
    
    //  Do warp
    GET_MODEL_DIMENSIONS( eProp.mModel, vModelDimMin, vModelDimMax )
    
    FLOAT fModelHeight = vModelDimMax.z - vModelDimMin.z

    
    //vMoveToCoord = vMoveToCoord - <<0.3, 0.3, 0.0>> //Adjust warp position a little off centre from the prop. Player can easily slide off irregular shaped props.

    //Tried getting edge of dimensions but his occasionally moves player fully off prop to side which is dangerous.
    //vMoveToCoord = vMoveToCoord + vModelDimMax //2510942 Going to move the player to the corner of the selected prop to avoid unsightly collision with the chevron.


    vMoveToCoord.z += ( fModelHeight + 2.0 )

	//Ground Z check for bug 3194804 - player was able to warp to stunt props that may have portions under the ground z and end up under the map himself
	FLOAT fGroundCheckZ
	VECTOR vMoveToCoordStack
	
	vMoveToCoordStack = vMoveToCoord 
	vMoveToCoordStack.z = vMoveToCoordStack.z + 1200.0
	GET_GROUND_Z_FOR_3D_COORD(vMoveToCoordStack, fGroundCheckZ)

	IF fGroundCheckZ > vMoveToCoord.z // If the ground z is higher than the prospective warp position
		CPRINTLN(debug_director,"Warp position Z was lower than ground z. Altering warp position to accomodate.")
		vMoveToCoord.z = (fGroundCheckZ + 0.5) //... the warp z should become the ground z with a little extra added for safety.
	ENDIF
		
	
    IF NOT IS_PLAYER_TELEPORT_ACTIVE() //2508911
        DO_PLAYER_TELEPORT( vMoveToCoord )
    ENDIF
        
    FREEZE_ENTITY_POSITION( PLAYER_PED_ID(), TRUE )
    WAIT( 0 )
    PropEditorHidesPerFrame(TRUE)
    
    IF TEST_VERTICAL_PROBE_AGAINST_ALL_WATER( vMoveToCoord, SCRIPT_INCLUDE_ALL, vMoveToCoord.z ) != SCRIPT_WATER_TEST_RESULT_NONE
        fMoveToHeading = GET_HEADING_BETWEEN_VECTORS_2D( vMoveToCoord, eProp.vPos )
        IF NOT IS_PLAYER_TELEPORT_ACTIVE()
        DO_PLAYER_TELEPORT( vMoveToCoord, fMoveToHeading )
        ENDIF
    ELSE
        IF NOT IS_PLAYER_TELEPORT_ACTIVE()
        DO_PLAYER_TELEPORT( vOriganlPos, fOriginalHeading )
        ENDIF
        PRINT_HELP( "Can't find valid warp position." )
    ENDIF
    
    SET_PLAYER_CONTROL(player_id(), TRUE)

    SET_GAMEPLAY_CAM_RELATIVE_HEADING()
    SET_GAMEPLAY_CAM_RELATIVE_PITCH()
                                                                                                        
    DO_SCREEN_FADE_IN( DEFAULT_FADE_TIME_SHORT )

ENDPROC



//Copied from director_mode, leave all help key management to the header itself
PROC DMPROP_REBUILD_HELP_PI_KEYS(INT iSelection, BOOL bPiMouseCameraOverride)
    iSelection = iSelection
    IF ePEState = PES_SceneSelect
        IF iCurrentScene <> iSelection
            ADD_MENU_HELP_KEY_CLICKABLE(caPropAccept, "CM_BUTLOADSCENE", default, bPiMouseCameraOverride)
        ELSE
            ADD_MENU_HELP_KEY_CLICKABLE(caPropAccept, "CM_BUTCNTEDIT", default, bPiMouseCameraOverride)
            ADD_MENU_HELP_KEY_CLICKABLE(caPropDelete, "CM_BUTRLDSCENE", default, bPiMouseCameraOverride)
        ENDIF
        IF NOT bShowCurrentSceneInfo
            ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_LB, "CM_BUTSCENEINFO" , default, bPiMouseCameraOverride)
        ENDIF
    ELIF ePEState = PES_SceneEdit
        IF (iSelection = ciPIPropList OR iSelection = ciPISceneClear)
        AND iNumProps = 0
            //Dont add the Accept button
        ELIF (iSelection = ciPISceneCopy AND iCurrentScene = iSelectedScene)
            //Don't add Accept
        ELSE
            ADD_MENU_HELP_KEY_CLICKABLE(caPropAccept, "ITEM_SELECT", default, bPiMouseCameraOverride)
        ENDIF
//        IF DMPROP_GET_NUM_PROPS() > 0
//            ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_Y, "CM_BUTCLRSCENE" )
//        ENDIF
//
//        ADD_MENU_HELP_KEY_CLICKABLE(caPropAccept, "CM_BUTLOADSCENE")
//        IF iSelectedScene = iCurrentScene
//        OR sdSceneData[iSelectedScene].iTotalProps = 0
//            ADD_MENU_HELP_KEY_CLICKABLE(caPropDelete, "CM_BUTSAVESCENE")
//        ELSE
//            ADD_MENU_HELP_KEY_CLICKABLE(caPropDelete, "CM_BUTOVRWSCENE")
//        ENDIF
    ELIF ePEState = PES_PropPlace            //Add prop button
        ADD_MENU_HELP_KEY_CLICKABLE(caPropAccept,"CM_BUTADDPROP", default, bPiMouseCameraOverride)
        //N.B From Left to right onscreen, L1 should appear before L2, then L3. The order of ADD_MENU calls reflect this.
        
        IF eSelectedPropType[0] != ENUM_TO_INT( CAT_Water )
            //Just have Lower and Raise instead.
            ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("DMPP_CHEIGHT", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropMoveUp),
                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropMoveDown))
        ENDIF
        
        //Distance
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())  //Don't show the forward and back prop movement controls whilst in a vehicle.
            IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
				#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
				//Was using DMPP_CPOSITION - fixed for 3199827. Dynamic help based on stunt / not stunt prop for L2 / R2 cam or prop movement,		
				IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE)
				OR DOES_CAM_EXIST(ElevatedCam)
				 	ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("DMPP_CPOS_STUNT",GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropMoveFar),
                	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropMoveNear))
				ELSE
				#ENDIF
                ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("DMPP_CPOS_NORM",GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropMoveFar),
                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropMoveNear))
				
				#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
				ENDIF
				#ENDIF
				
            ENDIF
        ENDIF



        //Rotate
        ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("DMPP_PROPROT", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropRotRight),
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,caPropRotLeft))
		
		
		//Move Player / Move Prop when in Stunt Prop elevated cam mode. Same control for bot. Wrapping entire segment in Stunt tunable check as this is all new for Import Export.		
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE)
			OR DOES_CAM_EXIST(ElevatedCam)
				 ADD_MENU_HELP_KEY(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_MOVE), "CM_MOVESTUNT")
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //Don't show the "move player" prompt if the player is in a vehicle when placing non-stunt props.
                		 ADD_MENU_HELP_KEY(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_MOVE), "CM_MOVENORM")
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
		
		//Snap
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
		IF DMPROP_IS_PROP_A_STUNT_PROP(iCurrentProp, TRUE) AND DOES_ENTITY_EXIST(moverProp.obj)
		AND GET_ENTITY_BONE_COUNT(moverProp.obj) > 0 AND (iFoundBoneIdx > 0 OR (iSnappedBoneIdx > 0 AND NOT bMovedSinceSnap))
			IF NOT DMPROP_IS_PROP_SPECIALRACE_CURVED_TRACK(iCurrentProp)
				ADD_MENU_HELP_KEY_CLICKABLE(caPropSnap,"DMPP_PROPSNAP", default)
			ENDIF
		ENDIF			
		#ENDIF
			
		
    ELIF ePEState = PES_PropList    //Delete/Move buttons
        ADD_MENU_HELP_KEY_CLICKABLE(caPropAccept,"CM_BUTEDITPROP", default, bPiMouseCameraOverride)
        ADD_MENU_HELP_KEY_CLICKABLE(caPropDelete,"CM_BUTDELPROP", default, bPiMouseCameraOverride)

        IF NOT IS_PED_INJURED(PLAYER_PED_ID())  //Don't show the warp control whilst in a vehicle.
            IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_Y, "CM_BUTWARPTOPRP", default, bPiMouseCameraOverride)
            ENDIF
        ENDIF
    ENDIF
    
ENDPROC
