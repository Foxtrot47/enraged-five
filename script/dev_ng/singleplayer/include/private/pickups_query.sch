// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_object.sch"
USING "commands_misc.sch"

/// PURPOSE:
///    Signals to the pickup controller that we want to check 
///    whether the specified world pickup has been collected
PROC INIT_PICKUP_QUERY_REQUEST(QUERY_TYPE eType, INT iIndex)
	IF g_ePickupQuery = PC_QUERY_INACTIVE
		g_ePickupQuery = PC_QUERY_REQUEST
		g_eQueryType   = eType
		g_iPickupIndex = iIndex
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns whether a pickup query has been initiated
FUNC BOOL IS_QUERY_RESULT_INACTIVE()
	
	IF g_ePickupQuery = PC_QUERY_INACTIVE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns whether we have a result for the pickup being collected 
FUNC BOOL HAS_RETURNED_QUERY_RESULT()
	
	IF g_ePickupQuery = PC_QUERY_RESULT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns whether we have a result for the pickup being collected 
FUNC BOOL HAS_QUERIED_PICKUP_BEEN_COLLECTED()
	RETURN g_bPickupCollected
ENDFUNC

/// PURPOSE:
///    Signal to pickup controller that no longer 
PROC END_PICKUP_QUERY_REQUEST()
	g_ePickupQuery = PC_QUERY_RESET
ENDPROC
