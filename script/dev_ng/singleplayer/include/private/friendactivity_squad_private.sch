//-	commands headers	-//

//- script headers	-//

//-	public headers	-//
USING "locates_public.sch"
USING "shop_public.sch"

//-	private headers	-//
USING "friendActivity_system_private.sch"
USING "friendActivity_journey_private.sch"


#IF IS_DEBUG_BUILD
//-	debug headers	-//
#ENDIF


//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//-- Friend activity - Squad state private functions and data types
//   sam.hackett@rockstarleeds.com
//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
//-- Squad - Debug
//---------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	PROC DEBUG_DisplaySquadInfo()

		// Print soldier info
		INT iSlot
		REPEAT MAX_BATTLE_BUDDIES iSlot
			DEBUG_DisplaySquadMemberInfo(gActivity.mSoldiers[iSlot])
		ENDREPEAT
		
		// Print trapped friend info
		IF gActivity.mFriendA.eState = FRIEND_TRAPPED
		OR gActivity.mFriendB.eState = FRIEND_TRAPPED
			DEBUG_DisplayFriendInfo(gActivity.mFriendA, 4)
			DEBUG_DisplayFriendInfo(gActivity.mFriendB, 5)
		ENDIF

		// Print activity locations
		INT iLine = CONST_iActivityDebugPrintLine
		
		enumBattleBuddyBehaviourFlag eFlag
		REPEAT MAX_BATTLEBUDDY_BEHAVIOUR_FLAGS eFlag
			IF IS_BATTLEBUDDY_BEHAVIOUR_REQUESTED(eFlag)
				IF DrawFriendLiteralString(GetLabel_enumBattleBuddyBehaviourFlag(eFlag), iLine, HUD_COLOUR_WHITE)
					iLine++
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDPROC

	FUNC STRING GetLabel_HateGroup(REL_GROUP_HASH hGroup)
		SWITCH hGroup
			CASE RELGROUPHASH_HATES_PLAYER				RETURN "RELGROUPHASH_HATES_PLAYER"				BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_LOST			RETURN "RELGROUPHASH_AMBIENT_GANG_LOST"			BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_MEXICAN		RETURN "RELGROUPHASH_AMBIENT_GANG_MEXICAN"		BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_FAMILY		RETURN "RELGROUPHASH_AMBIENT_GANG_FAMILY"		BREAK
			CASE RELGROUPHASH_AMBIENT_ARMY				RETURN "RELGROUPHASH_AMBIENT_ARMY"				BREAK
			CASE RELGROUPHASH_SECURITY_GUARD			RETURN "RELGROUPHASH_SECURITY_GUARD"			BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_MARABUNTE	RETURN "RELGROUPHASH_AMBIENT_GANG_MARABUNTE"	BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_CULT			RETURN "RELGROUPHASH_AMBIENT_GANG_CULT"			BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_SALVA		RETURN "RELGROUPHASH_AMBIENT_GANG_SALVA"		BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_WEICHENG		RETURN "RELGROUPHASH_AMBIENT_GANG_WEICHENG"		BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_BALLAS		RETURN "RELGROUPHASH_AMBIENT_GANG_BALLAS"		BREAK
			CASE RELGROUPHASH_AMBIENT_GANG_HILLBILLY	RETURN "RELGROUPHASH_AMBIENT_GANG_HILLBILLY"	BREAK
			CASE RELGROUPHASH_AGGRESSIVE_INVESTIGATE	RETURN "RELGROUPHASH_AGGRESSIVE_INVESTIGATE"	BREAK
		ENDSWITCH
		
		RETURN "<unknown group>"
	ENDFUNC
#ENDIF


//---------------------------------------------------------------------------------------------------
//-- Squad - Switching
//---------------------------------------------------------------------------------------------------

PROC Private_UpdateHotswap()

	SWITCH gActivity.eHotswapStage
		CASE SWAP_STATE_Wait
			
			gActivity.bIsAnySwitchAvailable = FALSE
			
			// Register members with hotswap selector
			INT i
			REPEAT MAX_BATTLE_BUDDIES i
				IF Private_RegisterSoldierForSwapping(gActivity.mSoldiers[i], gActivity.sSelectorPeds)
					gActivity.bIsAnySwitchAvailable = TRUE
				ENDIF
			ENDREPEAT
			
			// Set default quick switch
			IF Private_CheckBehaviourFlag(BBF_SwitchDefaultTrevor)
				SET_SELECTOR_PED_PRIORITY(gActivity.sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN)
			ELSE
				CLEAR_SELECTOR_PED_PRIORITY(gActivity.sSelectorPeds)
			ENDIF

			// Check for hotswap
			IF IS_PLAYER_PLAYING(PLAYER_ID()) AND gActivity.bIsAnySwitchAvailable
				IF UPDATE_SELECTOR_HUD(gActivity.sSelectorPeds)
					IF HAS_SELECTOR_PED_BEEN_SELECTED(gActivity.sSelectorPeds, SELECTOR_PED_FRANKLIN)
					OR HAS_SELECTOR_PED_BEEN_SELECTED(gActivity.sSelectorPeds, SELECTOR_PED_MICHAEL)
					OR HAS_SELECTOR_PED_BEEN_SELECTED(gActivity.sSelectorPeds, SELECTOR_PED_TREVOR)
						
						CPRINTLN(DEBUG_FRIENDS, "BATTLEBUDDY - ** HOT SWAP STARTED **")
						gActivity.sCamDetails.pedTo = gActivity.sSelectorPeds.pedID[gActivity.sSelectorPeds.eNewSelectorPed]
						gActivity.sCamDetails.bRun  = TRUE
						gActivity.eHotswapStage = SWAP_STATE_CamIn
						
						REPEAT MAX_BATTLE_BUDDIES i
							Private_ClearSoldierExtraHealth(gActivity.mSoldiers[i])
						ENDREPEAT
								
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SWAP_STATE_CamIn
			IF gActivity.sCamDetails.bSplineActive = FALSE
				IF IS_PED_INJURED(gActivity.sCamDetails.pedTo)
					CPRINTLN(DEBUG_FRIENDS, "BATTLEBUDDY - ** HOT SWAP ABORTED BEFORE START - TARGET PED INVALID/DEAD **")
					gActivity.eHotswapStage = SWAP_STATE_Wait
					EXIT
				ENDIF
			ENDIF
			
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(gActivity.sCamDetails) 		
				IF gActivity.sCamDetails.bOKToSwitchPed
					IF NOT gActivity.sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(gActivity.sSelectorPeds, TRUE)
							CPRINTLN(DEBUG_FRIENDS, "BATTLEBUDDY - ** HOT SWAP SWITCHED CHARS **")
							gActivity.eHotswapStage = SWAP_STATE_CamOut
							
							BOOL bIsNewPlayerGettingIntoCar
							bIsNewPlayerGettingIntoCar = (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							
							IF NOT bIsNewPlayerGettingIntoCar
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF

							REPEAT MAX_BATTLE_BUDDIES i
								IF gActivity.mSoldiers[i].hPed <> PLAYER_PED_ID()
									Private_SetSoldierExtraHealth(gActivity.mSoldiers[i])
								ENDIF
							ENDREPEAT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SWAP_STATE_CamOut
			IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(gActivity.sCamDetails)
				IF DOES_ENTITY_EXIST(gActivity.sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					g_sPlayerPedRequest.sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]	= gActivity.sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
				ENDIF
				
				IF DOES_ENTITY_EXIST(gActivity.sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					g_sPlayerPedRequest.sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]	= gActivity.sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
				ENDIF
				
				IF DOES_ENTITY_EXIST(gActivity.sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					g_sPlayerPedRequest.sSelectorPeds.pedID[SELECTOR_PED_TREVOR]	= gActivity.sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
				ENDIF
				
				CPRINTLN(DEBUG_FRIENDS, "BATTLEBUDDY - ** HOT SWAP DONE **")
				gActivity.eHotswapStage = SWAP_STATE_Wait
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


//---------------------------------------------------------------------------------------------------
//-- Squad - Group helpers
//---------------------------------------------------------------------------------------------------

//PROC Private_UpdatePlayerGroupOrder()
//
//	// Get player group
//	GROUP_INDEX hGroup = GET_PLAYER_GROUP(PLAYER_ID())
//	IF hGroup = NULL
//		EXIT
//	ENDIF
//	
//	// Check if group size has changed...
//	INT iLeaderCount, iFollowerCount
//	GET_GROUP_SIZE(hGroup, iLeaderCount, iFollowerCount)
//	
//	IF iLeaderCount+iFollowerCount > 0
//		IF GET_PED_AS_GROUP_LEADER(hGroup) <> PLAYER_PED_ID()
//			SCRIPT_ASSERT("UPDATE_PLAYER_GROUP_ORDER() - Player is not leader of PLAYER_GROUP_ID()")
//		ENDIF
//	ENDIF
//	
//	IF gActivity.iPlayerGroupFollowerCount <> iFollowerCount
//	
//		// Check if mission peds need moving up list
//		BOOL bFoundBattleBuddy			= FALSE
//		BOOL bFoundDisplacedMissionPed	= FALSE
//
//		INT i
//		REPEAT iFollowerCount i 
//			PED_INDEX hPed = GET_PED_AS_GROUP_MEMBER(hGroup, i)
//			IF IS_PED_UNINJURED(hPed)
//				IF GET_PLAYER_PED_ENUM(hPed) <> NO_CHARACTER
//					bFoundBattleBuddy = TRUE
//				
//				ELIF bFoundBattleBuddy
//					bFoundDisplacedMissionPed = TRUE
//					CPRINTLN(DEBUG_FRIENDS, "BATTLEBUDDY - GroupOrder: Npc ", GetLabel_enumCharacterList(GET_NPC_PED_ENUM(hPed)), " is too far back in group order (", i, ")")
//				ENDIF
//			ENDIF
//		ENDREPEAT
//		
//		// If order needs updating -> Remove and re-add battle buddies who are already in group
//		IF bFoundDisplacedMissionPed
//		
//			REPEAT MAX_BATTLE_BUDDIES i
//				IF IS_PED_UNINJURED(gActivity.mSoldiers[i].hPed)
//					IF IS_PED_GROUP_MEMBER(gActivity.mSoldiers[i].hPed, hGroup)
//						REMOVE_PED_FROM_GROUP(gActivity.mSoldiers[i].hPed)
//						SET_PED_AS_GROUP_MEMBER(gActivity.mSoldiers[i].hPed, hGroup)
//					ENDIF
//				ENDIF
//			ENDREPEAT
//		
//			CPRINTLN(DEBUG_FRIENDS, "BATTLEBUDDY - GroupOrder: Group has been reordered (count ", iFollowerCount, ")")
//
//		ENDIF
//	
//		// Update group size check
//		gActivity.iPlayerGroupFollowerCount = iFollowerCount
//		
//	ENDIF
//	
//ENDPROC

PROC Private_InitHatedRelGroups()

	// Set group key
	gActivity.hateGroups[0].hRelGroup	= RELGROUPHASH_HATES_PLAYER
	gActivity.hateGroups[1].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_LOST
	gActivity.hateGroups[2].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_MEXICAN
	gActivity.hateGroups[3].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_FAMILY
	gActivity.hateGroups[4].hRelGroup	= RELGROUPHASH_AMBIENT_ARMY
	gActivity.hateGroups[5].hRelGroup	= RELGROUPHASH_SECURITY_GUARD
	gActivity.hateGroups[6].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_MARABUNTE
	gActivity.hateGroups[7].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_CULT
	gActivity.hateGroups[8].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_SALVA
	gActivity.hateGroups[9].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_WEICHENG
	gActivity.hateGroups[10].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_BALLAS
	gActivity.hateGroups[11].hRelGroup	= RELGROUPHASH_AMBIENT_GANG_HILLBILLY
	gActivity.hateGroups[12].hRelGroup	= RELGROUPHASH_AGGRESSIVE_INVESTIGATE
	
	// Reset stored original relationships to none
	INT i
	REPEAT MAX_HATE_GROUPS i
		// ACQUAINTANCE_TYPE_PED_HATE is used to denote group's relationship has not been changed
		gActivity.hateGroups[i].eOriginalRelType = ACQUAINTANCE_TYPE_PED_HATE
	ENDREPEAT

ENDPROC
PROC Private_AddHatedRelGroup(REL_GROUP_HASH hRelGroup, RELATIONSHIP_TYPE eOriginalRelType)

	CPRINTLN(DEBUG_FRIENDS, "Private_AddHatedRelGroup() - Rel group ped in combat with/damaged playable char, set RELGROUPHASH_PLAYER to hate the group \"", GetLabel_HateGroup(hRelGroup), "\"")
	
	// Set player group to hate this group
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, hRelGroup)
	REGISTER_HATED_TARGETS_AROUND_PED(PLAYER_PED_ID(), 100.0)

	// Backup original state
	INT i
	REPEAT MAX_HATE_GROUPS i
		IF gActivity.hateGroups[i].hRelGroup = hRelGroup
			gActivity.hateGroups[i].eOriginalRelType = eOriginalRelType
		ENDIF
	ENDREPEAT

ENDPROC
PROC Private_ResetHatedRelGroups()

	INT i
	REPEAT MAX_HATE_GROUPS i
		IF gActivity.hateGroups[i].eOriginalRelType <> ACQUAINTANCE_TYPE_PED_HATE
			CPRINTLN(DEBUG_FRIENDS, "Private_ResetHatedRelGroups() - Reseting \"", GetLabel_HateGroup(gActivity.hateGroups[i].hRelGroup), "\"")
			SET_RELATIONSHIP_BETWEEN_GROUPS(gActivity.hateGroups[i].eOriginalRelType, RELGROUPHASH_PLAYER, gActivity.hateGroups[i].hRelGroup)
			gActivity.hateGroups[i].eOriginalRelType = ACQUAINTANCE_TYPE_PED_HATE
		ENDIF
	ENDREPEAT

ENDPROC

PROC Private_UpdateHatedRelGroups()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		// Check if player is wanted
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_PLAYER, RELGROUPHASH_COP) <> ACQUAINTANCE_TYPE_PED_HATE
				CPRINTLN(DEBUG_FRIENDS, "Private_UpdateHatedRelGroups() - Player is wanted, set RELGROUPHASH_PLAYER to hate the cops")
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
				REGISTER_HATED_TARGETS_AROUND_PED(PLAYER_PED_ID(), 100.0)
			ENDIF
		ELSE
			IF GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_PLAYER, RELGROUPHASH_COP) <> ACQUAINTANCE_TYPE_PED_NONE
				CPRINTLN(DEBUG_FRIENDS, "Private_UpdateHatedRelGroups() - Player is NOT wanted, set RELGROUPHASH_PLAYER to NOT hate the cops")
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
			ENDIF			
		ENDIF

		// Check if any nearby peds are in combat with a playable character (check one playable ped every 3 frames)
		IF GET_FRAME_COUNT() % 10 = 0
		
			INT iChar = (GET_FRAME_COUNT() % 30) / 10
				
			PED_INDEX hPlayablePed = gActivity.mSoldiers[iChar].hPed											// Get playable ped
			IF IS_PED_UNINJURED(hPlayablePed)
			
				IF (COUNT_PEDS_IN_COMBAT_WITH_TARGET(hPlayablePed) > 0)										// Get nearby peds
					PED_INDEX nearbyPeds[25]
					INT iNearCount = GET_PED_NEARBY_PEDS(hPlayablePed, nearbyPeds)
					
					INT i
					REPEAT iNearCount i																		// If any nearby peds are in combat with/traded damaged with playable ped...
						IF IS_PED_UNINJURED(nearbyPeds[i])
							IF IS_PED_IN_COMBAT(nearbyPeds[i], hPlayablePed)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(nearbyPeds[i], hPlayablePed)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(hPlayablePed, nearbyPeds[i])
								REL_GROUP_HASH hAttackedRelGroup
								hAttackedRelGroup = GET_PED_RELATIONSHIP_GROUP_HASH(nearbyPeds[i])
								
								IF hAttackedRelGroup <> RELGROUPHASH_NO_RELATIONSHIP
								AND hAttackedRelGroup <> RELGROUPHASH_PLAYER									// Make sure player hates this group (unless it's a group that should be ignored)
								AND hAttackedRelGroup <> RELGROUPHASH_CIVMALE
								AND hAttackedRelGroup <> RELGROUPHASH_CIVFEMALE
								AND hAttackedRelGroup <> RELGROUPHASH_COP
									RELATIONSHIP_TYPE eRelType = GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_PLAYER, hAttackedRelGroup)
									
									IF eRelType <> ACQUAINTANCE_TYPE_PED_HATE
										
										// If one of the standard rel groups, backup players original relationship to it
										Private_AddHatedRelGroup(hAttackedRelGroup, eRelType)

									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			
			ENDIF
			
		ENDIF
		
		// Iterate through this frame's events
//		INT iEventIndex
//		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventIndex
//			
//			// Get event
//			EVENT_NAMES			eEventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventIndex)
//			STRUCT_ENTITY_ID	sEventEntityStruct
//
//			// Process event
//			SWITCH (eEventType)
//				
////				CASE EVENT_ACQUAINTANCE_PED_HATE	// Could supporting this help?
////				CASE EVENT_RESPONDED_TO_THREAT		// Could supporting this help?
//				CASE EVENT_ENTITY_DAMAGED
//					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventIndex, sEventEntityStruct, SIZE_OF(sEventEntityStruct))
//					
//					IF DOES_ENTITY_EXIST(sEventEntityStruct.EntityId) AND IS_ENTITY_A_PED(sEventEntityStruct.EntityId)
//					
//						// If ped was damaged by player, get his rel group
//						PED_INDEX hDamagedPed
//						hDamagedPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEventEntityStruct.EntityId)
//						
//						IF DOES_ENTITY_EXIST(hDamagedPed)
//							IF hDamagedPed <> PLAYER_PED_ID()
//							AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(hDamagedPed, PLAYER_PED_ID())
//								REL_GROUP_HASH hAttackedRelGroup
//								hAttackedRelGroup = GET_PED_RELATIONSHIP_GROUP_HASH(hDamagedPed)
//								
//								IF hAttackedRelGroup <> RELGROUPHASH_PLAYER
//								AND hAttackedRelGroup <> RELGROUPHASH_CIVMALE
//								AND hAttackedRelGroup <> RELGROUPHASH_CIVFEMALE
//									IF GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_PLAYER, hAttackedRelGroup) <> ACQUAINTANCE_TYPE_PED_HATE
//										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, hAttackedRelGroup)
//										REGISTER_HATED_TARGETS_AROUND_PED(PLAYER_PED_ID(), 100.0)
//
//										CPRINTLN(DEBUG_FRIENDS, "Private_UpdateHatedRelGroups() - Player damaged rel group, set RELGROUPHASH_PLAYER to hate the group")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				BREAK
//
//			ENDSWITCH
//		ENDREPEAT

	ENDIF

ENDPROC


//---------------------------------------------------------------------------------------------------
//-- Squad - Main
//---------------------------------------------------------------------------------------------------


//PROC PROCESS_SQUAD_REBOOT_STATE()				// BBUDDIES REMOVED
//
//	Private_ClearActivityFailReason()
//	Private_RequestFriendText()
//	
//	//-- Check if replay is rebooting
//	IF IS_REPLAY_BEING_PROCESSED()
//
//		IF IS_PLAYER_PLAYING(PLAYER_ID())
//
//			//-- Process activity members
//			Private_ProcessAddSoldiers()
//			Private_ProcessMembers()
//
////			IF Private_AreSoldiersFailed()
////
////				Private_SetActivityFailReason(FAF_RetryAbort)
////			
////			ENDIF
//		
//		ENDIF
//	
//	//-- Check if replay reboot is complete
//	ELSE
//
//		IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
//			Private_SetActivityFailReason(FAF_PlayerDeathArrest)
//	
//		
//		ELSE
//			Private_SetActivityState(ACTIVITY_STATE_SquadRoaming)
//
//		ENDIF
//	
//	ENDIF
//
//	// TODO: Don't forget to sort out all the CLEANUP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
//	
//
//ENDPROC


//PROC PROCESS_SQUAD_ROAMING_STATE()					// BBUDDIES REMOVED
//
//	Private_ClearActivityFailReason()
//	
//	//-- Check if replay has been initiated
//	IF IS_REPLAY_BEING_PROCESSED()
//
//		Private_ClearObjective()
//		IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
//			Private_SetActivityFailReason(FAF_RetryAbort)
//		ENDIF
//
//
//	//-- Check if player is dead
//	ELIF NOT IS_PLAYER_PLAYING(PLAYER_ID())
//	
//		Private_SetActivityFailReason(FAF_PlayerDeathArrest)
//
//	
//	//-- If player has started prep mission...
//	ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
//		
//		Private_SetActivityState(ACTIVITY_STATE_SquadMission)
//	
//
//	//-- If knockout scene has started...
//	ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_GRIEFING)
//		
//		Private_ClearObjective()
////		Private_ClearDropoffScenarioBlocking()
//
//		Private_DeleteMembersForGriefing()
//		Private_SetActivityFailReason(FAF_PlayerOnMission)
//	
//
//	//-- If player has started story mission...
//	ELIF    IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
//	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
//	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
//		
//		Private_RejectMembersForMission(FALSE)
//		Private_SetActivityFailReason(FAF_PlayerOnMission)
//	
//
//	//-- Waiting to get mission flag...
//	ELIF NOT Private_SetCurrentlyOnFriendMission(gActivity.iCandidateID)
//		
//		DISABLE_SELECTOR_THIS_FRAME()
//		EXIT
//	
//	
//	ELIF Private_RequestFriendText()
//
//		IF gActivity.iStateProgress = 0
//			IF gActivity.bRestoreLocationBlips = FALSE
//				Private_BackupLocationBlips()
//				gActivity.bRestoreLocationBlips = TRUE
//			ENDIF
//			Private_ClearLocationBlips()
//			gActivity.iStateProgress++
//		ENDIF
//		
//		//-- Process activity members
//		Private_ProcessAddSoldiers()
//		Private_ProcessMembers()
//		Private_ProcessRemoveMembers()
//		
//		Private_ProcessSystem(TRUE)
//		
//		// -- Update hotswaps, groups and display debug
//		Private_UpdateHotSwap()
////		Private_UpdatePlayerGroupOrder()
//		Private_UpdateHatedRelGroups()
//		
//		#IF IS_DEBUG_BUILD
//			DEBUG_DisplaySquadInfo()
//		#ENDIF
//		
//		IF NOT Private_AreAnyMembersValid()							// Are friends failed
//		
//			Private_ClearObjective()
//			IF Private_AreAllMembersRemoved()
//				Private_SetActivityFailReason(FAF_MemberFail)
//			ENDIF
//		
//		ENDIF
//		
//	ENDIF
//	
//
//	// TODO: Don't forget to sort out all the CLEANUP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
//	
//
//ENDPROC


//PROC INIT_SQUAD_MISSION_STATE()
//	CPRINTLN(DEBUG_FRIENDS, "INIT_SQUAD_MISSION_STATE()")			// BBUDDIES REMOVED
//
//	SET_SQUAD_MISSION_TO_CURRENT_ZONE()
//	Private_SetZoneRejection(FALSE)
//	
//	// TODO: Set squad idle dialogue state
//ENDPROC
//
//PROC PROCESS_SQUAD_MISSION_STATE()
//
//	Private_ClearActivityFailReason()
//	
//	//-- Check if replay has been initiated
//	IF IS_REPLAY_BEING_PROCESSED()
//
//		IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
//			Private_SetActivityFailReason(FAF_RetryAbort)
//		ENDIF
//
//
//	//-- Check if player is alive
//	ELIF NOT IS_PLAYER_PLAYING(PLAYER_ID())
//	
//		EXIT	// Allow replay to handle this
//
//
//	//-- If has prep mission ended successfully...
//	ELIF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
//		
///*		IF gActivity.bResumeFriendsAfterSquad
//			IF Private_TransferMembersToFriends()
//				Private_SetActivityState(ACTIVITY_STATE_Journey)
//			ELSE
//				Private_SetActivityState(ACTIVITY_STATE_DropoffSquad)	// TODO: Should this go straight to fail/success or something?
//			ENDIF
//		ELSE
//			Private_SetActivityState(ACTIVITY_STATE_DropoffSquad)
//		ENDIF
//*/
//		IF gActivity.bResumeFriendsAfterSquad AND Private_TransferMembersToFriends()
//			Private_SetActivityState(ACTIVITY_STATE_Journey)
//		ELSE
//			Private_SetActivityState(ACTIVITY_STATE_DropoffSquad)
//		ENDIF
//		
//
//	ELIF Private_RequestFriendText()
//	
//		//-- Process activity members
//		Private_ProcessAddSoldiers()
//		Private_ProcessMembers()
//		Private_ProcessRemoveMembers()
//		
//		Private_ProcessSystem(TRUE)
//
//		// -- Update hotswaps, groups and display debug
//		Private_UpdateHotSwap()
////		Private_UpdatePlayerGroupOrder()
//		Private_UpdateHatedRelGroups()
//		
//		#IF IS_DEBUG_BUILD
//			DEBUG_DisplaySquadInfo()
//		#ENDIF
//		
//	ENDIF
//
//	// TODO: Don't forget to sort out all the CLEANUP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
//	
//
//ENDPROC


//FUNC BOOL PROCESS_DROPOFF_SQUAD_STATE()					// BBUDDIES REMOVED
//	
//	// Gets rid of all the battle buddies
//	BOOL bAreAnyMembersRemaining = FALSE
//	
//	INT iSlot
//	REPEAT MAX_BATTLE_BUDDIES iSlot
//		IF gActivity.mSoldiers[iSlot].eState <> SOLDIER_NULL
//			Private_SetSoldierRejected(gActivity.mSoldiers[iSlot], SP_MISSION_NONE, TRUE, FAP_SQUAD_PASSED)
//			bAreAnyMembersRemaining = TRUE
//		ENDIF
//	ENDREPEAT
//
//	// If all are gone, cleanup
//	IF NOT bAreAnyMembersRemaining
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//
//ENDFUNC


PROC INIT_TRAPPED_STATE()
	CPRINTLN(DEBUG_FRIENDS, "INIT_SQUAD_MISSION_STATE()")			// BBUDDIES REMOVED

	SET_SQUAD_MISSION_TO_CURRENT_ZONE()
	Private_SetZoneRejection(FALSE)
	
	// TODO: Set squad idle dialogue state
ENDPROC

PROC PROCESS_TRAPPED_STATE()

	Private_ClearActivityFailReason()
	
	//-- If replay (as in playback) system requested clear entities...
	IF REPLAY_SYSTEM_HAS_REQUESTED_A_SCRIPT_CLEANUP()
	
		Private_ClearObjective()
		Private_SetActivityFailReason(FAF_PlaybackAbort)
		EXIT

	//-- Check if replay (as in retry failed mission) has been initiated
	ELIF IS_REPLAY_BEING_PROCESSED()

		IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			Private_SetActivityFailReason(FAF_RetryAbort)
		ENDIF


	//-- Check if player is dead
	ELIF NOT IS_PLAYER_PLAYING(PLAYER_ID())
	
		Private_ClearObjective()
//		Private_ClearDropoffScenarioBlocking()
		Private_SetActivityFailReason(FAF_PlayerDeathArrest)


	//-- If player has started story mission...
	ELIF    IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
		
		Private_ClearObjective()
//		Private_ClearDropoffScenarioBlocking()

		Private_RejectMembersForMission(FALSE)
		Private_SetActivityFailReason(FAF_PlayerOnMission)
	

	ELIF Private_RequestFriendText()
	
		//-- Process activity members
		Private_ProcessMembers()
		Private_ProcessRemoveMembers()
		
		Private_ProcessSystem(TRUE)

		#IF IS_DEBUG_BUILD
			DEBUG_DisplayFriendInfo(gActivity.mFriendA, 0)
			DEBUG_DisplayFriendInfo(gActivity.mFriendB, 2)
		#ENDIF
		
		Private_ClearObjective()

		IF NOT Private_AreAnyMembersValid()							// Are friends failed
			IF Private_AreAllMembersRemoved()
				Private_SetActivityFailReason(FAF_MemberFail)
			ENDIF
		ENDIF
		
	ENDIF


ENDPROC
