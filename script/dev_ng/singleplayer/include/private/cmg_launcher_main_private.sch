USING "cmg_launcher_structs_private.sch"
//USING "cmg_launcher_scenes_private.sch"

USING "cmg_launcher_public.sch"
USING "context_control_public.sch"
USING "commands_network.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	CLIFFORD MINIGAME FILE: cmg_launcher_main_private.sch
//	Sam Hackett
//
//	This is to be included in scripts that can launch the casino minigames
//	- Singleplayer casino launcher (xc_launcher.sc)
//	- Multiplayer casino launcher (clifford_mg_launcher_mp.sc)
//	- MP apartment (AMP_MP_PROPERTY_INT.sc)
//
//	It contains the main init/update/cleanup functions that these scripts should call.
//
//	See also:
//		cmg_launcher_structs_private.sch
//		cmg_launcher_scenes_private.sch
//		cmg_launcher_public.sch
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     HELPERS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#IF IS_DEBUG_BUILD
	FUNC STRING ENUM_NAME_CMGL_LOCAL_STATE(CMGL_LOCAL_STATE eState)
		SWITCH eState
			CASE CMGL_LOCAL_STATE_Init				RETURN "CMGL_LOCAL_STATE_Init"				BREAK
			CASE CMGL_LOCAL_STATE_TempCreateTables	RETURN "CMGL_LOCAL_STATE_TempCreateTables"	BREAK
			CASE CMGL_LOCAL_STATE_Idle				RETURN "CMGL_LOCAL_STATE_Idle"				BREAK
			CASE CMGL_LOCAL_STATE_Offer				RETURN "CMGL_LOCAL_STATE_Offer"				BREAK
			CASE CMGL_LOCAL_STATE_RequestMPSeat		RETURN "CMGL_LOCAL_STATE_RequestMPSeat"		BREAK
			CASE CMGL_LOCAL_STATE_ScriptLaunch		RETURN "CMGL_LOCAL_STATE_ScriptLaunch"		BREAK
			CASE CMGL_LOCAL_STATE_ScriptRun			RETURN "CMGL_LOCAL_STATE_ScriptRun"			BREAK
			CASE CMGL_LOCAL_STATE_ReleaseMPSeat		RETURN "CMGL_LOCAL_STATE_ReleaseMPSeat"		BREAK
		ENDSWITCH
		CPRINTLN(DEBUG_MINIGAME, "ENUM_NAME_CMGL_LOCAL_STATE() - Unknown state: ", eState)
		SCRIPT_ASSERT("ENUM_NAME_CMGL_LOCAL_STATE() - Unknown state")
		RETURN "CMGL_LOCAL_STATE_unknown"
	ENDFUNC
#ENDIF


//FUNC BOOL IS_ENTITY_FACING_COORD_XY(ENTITY_INDEX hEntity, VECTOR vCoord, FLOAT fAngle)
//	VECTOR vEntityToCoord	= vCoord - GET_ENTITY_COORDS(hEntity)
//	VECTOR vEntityFacing	= GET_ENTITY_FORWARD_VECTOR(hEntity)
//	IF DOT_PRODUCT_XY(vEntityToCoord, vEntityFacing) > COS(fAngle)
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC


//FUNC BOOL IS_PED_PERFORMING_TASK(PED_INDEX hPed, SCRIPT_TASK_NAME eTask)
//	SCRIPTTASKSTATUS eStatus = GET_SCRIPT_TASK_STATUS(hPed, eTask)
//	IF (eStatus = PERFORMING_TASK) OR (eStatus = WAITING_TO_START_TASK)
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC


FUNC BOOL CMGL_GetContextPromptTextRoot(CMG_TYPE eGameType, BOOL bIsApartment, TEXT_LABEL& tContextPromptRoot)
	
	IF bIsApartment = FALSE
		SWITCH (eGameType)
			CASE CMG_TYPE_Blackjack
				tContextPromptRoot = "CAS_BJACK"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Poker
				tContextPromptRoot = "CAS_POKER"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Roulette
				tContextPromptRoot = "CAS_ROULETTE"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Slot
				tContextPromptRoot = "CAS_SLOT"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Dancing
				tContextPromptRoot = "CAS_DANCE"
				RETURN TRUE
			BREAK
			DEFAULT	// ERROR
				CPRINTLN(DEBUG_MINIGAME,	"GET_CLIFFORD_MG_CONTEXT_PROMPT_ROOT() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eGameType))
				SCRIPT_ASSERT(				"GET_CLIFFORD_MG_CONTEXT_PROMPT_ROOT() Invalid minigame type")
			BREAK
		ENDSWITCH
	ELSE
		SWITCH (eGameType)
			CASE CMG_TYPE_Blackjack
				tContextPromptRoot = "CASA_BJACK"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Poker
				tContextPromptRoot = "CASA_POKER"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Roulette
				tContextPromptRoot = "CASA_ROULETTE"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Slot
				tContextPromptRoot = "CASA_SLOT"
				RETURN TRUE
			BREAK
			DEFAULT	// ERROR
				CPRINTLN(DEBUG_MINIGAME,	"GET_CLIFFORD_MG_CONTEXT_PROMPT_ROOT() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eGameType))
				SCRIPT_ASSERT(				"GET_CLIFFORD_MG_CONTEXT_PROMPT_ROOT() Invalid minigame type")
			BREAK
		ENDSWITCH
	ENDIF
	
	tContextPromptRoot = "CAS_unknown"
	RETURN FALSE
ENDFUNC


FUNC BOOL CMGL_GetWantedTextRoot(CMG_TYPE eGameType, TEXT_LABEL& tWantedRoot)
	
	SWITCH (eGameType)
		CASE CMG_TYPE_Blackjack
			tWantedRoot = "CAS_BJK_COPS"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Poker
			tWantedRoot = "CAS_PKR_COPS"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Roulette
			tWantedRoot = "CAS_RLT_COPS"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Slot
			tWantedRoot = "CAS_SLT_COPS"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Dancing
			tWantedRoot = "CAS_DAN_COPS"
			RETURN TRUE
		BREAK
		DEFAULT	// ERROR
			CPRINTLN(DEBUG_MINIGAME,	"CMGL_GetWantedTextRoot() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eGameType))
			SCRIPT_ASSERT(				"CMGL_GetWantedTextRoot() Invalid minigame type")
		BREAK
	ENDSWITCH
	
	tWantedRoot = "CAS_unknown"
	RETURN FALSE
ENDFUNC


FUNC BOOL CMGL_GetCostTextRoot(CMG_TYPE eGameType, TEXT_LABEL& tCostRoot)
	
	SWITCH (eGameType)
		CASE CMG_TYPE_Blackjack
			tCostRoot = "CAS_BJK_COST"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Poker
			tCostRoot = "CAS_PKR_COST"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Roulette
			tCostRoot = "CAS_RLT_COST"
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Slot
			tCostRoot = "CAS_SLT_COST"
			RETURN TRUE
		BREAK
		DEFAULT	// ERROR
			CPRINTLN(DEBUG_MINIGAME,	"CMGL_GetCostTextRoot() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eGameType))
			SCRIPT_ASSERT(				"CMGL_GetCostTextRoot() Invalid minigame type")
		BREAK
	ENDSWITCH
	
	tCostRoot = "CAS_unknown"
	RETURN FALSE
ENDFUNC


FUNC BOOL CMGL_GetScriptName(CMG_TYPE eGameType, BOOL bIsMultiplayer, TEXT_LABEL_63& tScriptName)
	IF bIsMultiplayer = FALSE
		SWITCH (eGameType)
			CASE CMG_TYPE_Blackjack
				tScriptName = "bjack_sp"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Poker
				tScriptName = "poker_sp"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Roulette
				tScriptName = "roulette_sp"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Slot
				tScriptName = "slot"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Dancing
				tScriptName = "dancing"
				RETURN TRUE
			BREAK
			DEFAULT	// ERROR
				CPRINTLN(DEBUG_MINIGAME,	"GET_CLIFFORD_MG_SCRIPT_NAME() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eGameType))
				SCRIPT_ASSERT(				"GET_CLIFFORD_MG_SCRIPT_NAME() Invalid minigame type")
			BREAK
		ENDSWITCH
	ELSE
		SWITCH (eGameType)
			CASE CMG_TYPE_Blackjack
				tScriptName = "blackjack_mp"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Poker
				tScriptName = "old_poker_mp"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Roulette
				tScriptName = "roulette_mp"
				RETURN TRUE
			BREAK
			CASE CMG_TYPE_Slot
				tScriptName = ""
				RETURN FALSE
			BREAK
			DEFAULT	// ERROR
				CPRINTLN(DEBUG_MINIGAME,	"GET_CLIFFORD_MG_SCRIPT_NAME() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eGameType))
				SCRIPT_ASSERT(				"GET_CLIFFORD_MG_SCRIPT_NAME() Invalid minigame type")
			BREAK
		ENDSWITCH
	ENDIF
	
	tScriptName = ""
	RETURN FALSE
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     TRIGGERS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


PROC CMGL_InitTrigger(CMGL_LOCAL& local, INT iTriggerID)

	CPRINTLN(DEBUG_MINIGAME, "CMGL_InitTrigger(", iTriggerID, ")")
	
	// Init type data
	local.mTriggers[iTriggerID].eGameType	= CMG_TYPE_INVALID
	local.mTriggers[iTriggerID].iGameID		= 0
	local.mTriggers[iTriggerID].vPos		= <<0.0, 0.0, 0.0>>
	local.mTriggers[iTriggerID].fBuyIn		= 10.0

ENDPROC


FUNC INT CMGL_SetupTrigger(CMGL_LOCAL& local, CMG_TYPE eGameType, VECTOR vPos, FLOAT fBuyIn)

	// Get unique game ID (fail if run out)
	INT iGameID = 0

	INT iTriggerID
	REPEAT CMGL_TRIGGER_MAX iTriggerID
		IF local.mTriggers[iTriggerID].eGameType = eGameType
			iGameID++
		ENDIF
	ENDREPEAT
	
	IF eGameType = CMG_TYPE_Blackjack
		IF iGameID >= CMGL_BJACK_TRIGGER_MAX
			CPRINTLN(DEBUG_MINIGAME,	"CMGL_SetupTrigger(", ENUM_NAME_CMG_TYPE(eGameType), ", vPos=<<", vPos.x, ", ", vPos.y, ", ", vPos.z, ", fBuyIn=", fBuyIn, ") - Out of unique game IDs for this game type (CMGL_BJACK_TRIGGER_MAX = ", CMGL_BJACK_TRIGGER_MAX, ")")
			SCRIPT_ASSERT(				"CMGL_SetupTrigger() - Out of unique game IDs for this game type (bjack)")
			RETURN -1
		ENDIF

	ELIF eGameType = CMG_TYPE_Poker
		IF iGameID >= CMGL_POKER_TRIGGER_MAX
			CPRINTLN(DEBUG_MINIGAME,	"CMGL_SetupTrigger(", ENUM_NAME_CMG_TYPE(eGameType), ", vPos=<<", vPos.x, ", ", vPos.y, ", ", vPos.z, ", fBuyIn=", fBuyIn, ") - Out of unique game IDs for this game type (CMGL_POKER_TRIGGER_MAX = ", CMGL_POKER_TRIGGER_MAX, ")")
			SCRIPT_ASSERT(				"CMGL_SetupTrigger() - Out of unique game IDs for this game type (poker)")
			RETURN -1
		ENDIF

	ELIF eGameType = CMG_TYPE_Roulette
		IF iGameID >= CMGL_ROULE_TRIGGER_MAX
			CPRINTLN(DEBUG_MINIGAME,	"CMGL_SetupTrigger(", ENUM_NAME_CMG_TYPE(eGameType), ", vPos=<<", vPos.x, ", ", vPos.y, ", ", vPos.z, ", fBuyIn=", fBuyIn, ") - Out of unique game IDs for this game type (CMGL_ROULE_TRIGGER_MAX = ", CMGL_ROULE_TRIGGER_MAX, ")")
			SCRIPT_ASSERT(				"CMGL_SetupTrigger() - Out of unique game IDs for this game type (poker)")
			RETURN -1
		ENDIF

	ELSE
		IF iGameID >= CMGL_GAME_ID_MAX
			CPRINTLN(DEBUG_MINIGAME,	"CMGL_SetupTrigger(", ENUM_NAME_CMG_TYPE(eGameType), ", vPos=<<", vPos.x, ", ", vPos.y, ", ", vPos.z, ", fBuyIn=", fBuyIn, ") - Out of unique game IDs for this game type (CMGL_GAME_ID_MAX = ", CMGL_GAME_ID_MAX, ")")
			SCRIPT_ASSERT(				"CMGL_SetupTrigger() - Out of unique game IDs for this game type")
			RETURN -1
		ENDIF
	ENDIF

	// Get unique trigger ID (fail if run out)
	REPEAT CMGL_TRIGGER_MAX iTriggerID
		IF local.mTriggers[iTriggerID].eGameType = CMG_TYPE_INVALID

			CPRINTLN(DEBUG_MINIGAME, "CMGL_SetupTrigger(", ENUM_NAME_CMG_TYPE(eGameType), ", vPos=<<", vPos.x, ", ", vPos.y, ", ", vPos.z, ", fBuyIn=", fBuyIn, ") = ID:", iTriggerID)
			
			local.mTriggers[iTriggerID].eGameType	= eGameType
			local.mTriggers[iTriggerID].iGameID		= iGameID
			local.mTriggers[iTriggerID].vPos		= vPos
			local.mTriggers[iTriggerID].fBuyIn		= fBuyIn
			
			RETURN iTriggerID
		
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_MINIGAME, "CMGL_SetupTrigger(", ENUM_NAME_CMG_TYPE(eGameType), ", vPos=<<", vPos.x, ", ", vPos.y, ", ", vPos.z, ", fBuyIn=", fBuyIn, ") - Out of unique trigger IDs for this game type")
	SCRIPT_ASSERT("CMGL_SetupTrigger() - Out of unique trigger IDs for this game type")
	RETURN -1
	
ENDFUNC


FUNC BOOL CMGL_CheckTriggerHotspot(CMGL_LOCAL& local, INT iTriggerID, BOOL bIsCheckingForExit = FALSE)

	VECTOR	vTriggerPos = local.mTriggers[iTriggerID].vPos
	FLOAT	fTriggerRad = 2.5
	FLOAT	fTriggerAng = 0.3826834323650897717284599840304 //COS(67.5)
	
	IF local.mTriggers[iTriggerID].eGameType = CMG_TYPE_Slot
		fTriggerRad = 5.0
	ENDIF
	
	IF bIsCheckingForExit
		fTriggerRad += 2.0
		fTriggerAng = 0.0 //COS(90.0)
	ENDIF
	
	// Check if player is in the trigger's hotspot
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTriggerPos, <<fTriggerRad, fTriggerRad, fTriggerRad>>)

		// Check if player is facing trigger
		VECTOR	vEntityToCoord	= vTriggerPos - GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR	vEntityFacing	= GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
		FLOAT	fDot			= (vEntityToCoord.x*vEntityFacing.x + vEntityToCoord.y*vEntityFacing.y)
		
		IF fDot > fTriggerAng
			RETURN TRUE
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL CMGL_CheckTriggerAvailability(CMGL_LOCAL& local, INT iTriggerID, CMGL_SERVER& server)

	OBJECT_INDEX	hTable			= NULL
	PED_INDEX		hPed			= NULL
	
	// Check table is present
	IF CLIFFORD_MG_GET_TABLE_OBJECT(local.mTriggers[iTriggerID].eGameType, local.mTriggers[iTriggerID].vPos, hTable)
			
		// Debug prints which seats are taken
		#IF IS_DEBUG_BUILD
			#IF CMGL_ENABLE_DEBUG_OUTPUT
				PED_INDEX hPed2
				IF local.mTriggers[iTriggerID].eGameType != CMG_TYPE_Slot
					TEXT_LABEL_63 tPlayersPresent = "Players: "
					INT iSeatMax2 = CLIFFORD_MG_SEAT_COUNT(local.mTriggers[iTriggerID].eGameType)
					INT iSeatID2
					REPEAT iSeatMax2 iSeatID2
						IF CLIFFORD_MG_GET_SEAT_PED(local.mTriggers[iTriggerID].eGameType, hTable, iSeatID2, hPed2)
							tPlayersPresent += iSeatID2
						ELSE
							tPlayersPresent += "_"
						ENDIF
					ENDREPEAT
					IF CLIFFORD_MG_HAS_DEALER(local.mTriggers[iTriggerID].eGameType)
						IF CLIFFORD_MG_GET_DEALER_PED(local.mTriggers[iTriggerID].eGameType, hTable, hPed2)
							tPlayersPresent += " + Dealer"
						ELSE
							tPlayersPresent += " + No dealer"
						ENDIF
					ENDIF
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.25, 0.01, "STRING", tPlayersPresent)
				ENDIF
			#ENDIF
		#ENDIF

		//-- MP apartment location ------------------------------------------------------------
		IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsApartment)
			
			// Make sure there's an MP seat free
			IF server.iClientSeatReservationCount[iTriggerID] < CLIFFORD_MG_SEAT_COUNT(local.mTriggers[iTriggerID].eGameType)
				RETURN TRUE
			ENDIF
	
		
		//-- MP casino location ---------------------------------------------------------------
		ELIF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
			
			// Make sure there's an MP seat free
			IF server.iClientSeatReservationCount[iTriggerID] < CLIFFORD_MG_SEAT_COUNT(local.mTriggers[iTriggerID].eGameType)
	
				// If game has already started, don't need to check peds
				IF server.iClientSeatReservationCount[iTriggerID] > 0
					RETURN TRUE
				ENDIF
				
				// If dealer is present (or not needed), can start
				IF NOT CLIFFORD_MG_HAS_DEALER(local.mTriggers[iTriggerID].eGameType)
				OR CLIFFORD_MG_GET_DEALER_PED(local.mTriggers[iTriggerID].eGameType, hTable, hPed)
					RETURN TRUE
				ENDIF
			
			ENDIF
		
		
		//-- SP casino location ---------------------------------------------------------------
		ELSE
		
			// Make sure dealer is present
			IF NOT CLIFFORD_MG_HAS_DEALER(local.mTriggers[iTriggerID].eGameType)
			OR CLIFFORD_MG_GET_DEALER_PED(local.mTriggers[iTriggerID].eGameType, hTable, hPed)
				
				// If not poker, don't need to check for other player peds
				IF local.mTriggers[iTriggerID].eGameType != CMG_TYPE_Poker
					RETURN TRUE
				ENDIF
				
				// If poker, If any other ped is present too, can start
				INT iSeatMax = CLIFFORD_MG_SEAT_COUNT(local.mTriggers[iTriggerID].eGameType)
				INT iSeatID
				REPEAT iSeatMax iSeatID
					IF CLIFFORD_MG_GET_SEAT_PED(local.mTriggers[iTriggerID].eGameType, hTable, iSeatID, hPed)
						RETURN TRUE
					ENDIF
				ENDREPEAT
			
			ENDIF

		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC CMGL_TRIGGER_BLOCK_CONDITION CMGL_CheckTriggerBlockCondition(CMGL_LOCAL& local, INT iTriggerLoop)

	// Check if in vehicle
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN CMGL_TRIGGER_BLOCK_CONDITION_TotalBlock
	ENDIF
	
	// Check if player has wanted level
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		RETURN CMGL_TRIGGER_BLOCK_CONDITION_WantedLevel
	ENDIF
					
	// Check if player has no money
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF (NETWORK_GET_VC_BALANCE() < CEIL(local.mTriggers[iTriggerLoop].fBuyIn))
			RETURN CMGL_TRIGGER_BLOCK_CONDITION_Money
		ENDIF
	ELSE
		IF (GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < CEIL(local.mTriggers[iTriggerLoop].fBuyIn))	
			RETURN CMGL_TRIGGER_BLOCK_CONDITION_Money
		ENDIF
	ENDIF

	RETURN CMGL_TRIGGER_BLOCK_CONDITION_None

ENDFUNC


PROC CMGL_CleanupTrigger(CMGL_LOCAL& local, INT iTriggerID)

	CPRINTLN(DEBUG_MINIGAME, "CMGL_CleanupTrigger(", iTriggerID, ")")

	// Clear type
	local.mTriggers[iTriggerID].eGameType	= CMG_TYPE_INVALID
	local.mTriggers[iTriggerID].iGameID		= 0
	local.mTriggers[iTriggerID].vPos		= <<0.0, 0.0, 0.0>>
	local.mTriggers[iTriggerID].fBuyIn		= 10.0

ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     LAUNCHER MP SERVER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


PROC CMGL_UpdateServerDataForClient(CMGL_LOCAL& local, CMGL_SERVER& server, CMGL_CLIENT& client, INT iClientID)

	// Mark this client as connected
	SET_BIT(server.iConnectedClientBits, iClientID)

	//-- Respond to client requesting a seat reservation ----------------------------------------------------
	IF IS_BIT_SET(client.iPackedRequestData, CMGL_CLIENT_BIT_RequestSeat)
		IF NOT IS_BIT_SET(server.iClientSeatRequestResponses, iClientID)
		
			// Get trigger ID from packed data
			INT iTriggerID = client.iPackedRequestData & 255
			
			// Reserve trigger seat (if not reserved yet + if there are any seats left)
			IF NOT IS_BIT_SET(server.iClientSeatReservations[iTriggerID], iClientID)
			
				IF server.iClientSeatReservationCount[iTriggerID] < CLIFFORD_MG_SEAT_COUNT(local.mTriggers[iTriggerID].eGameType)
					CPRINTLN(DEBUG_MINIGAME, "CMGL_UpdateServerDataForClient(", iClientID, ") - Reserved seat (on trigger ", iTriggerID, ")")
					SET_BIT(server.iClientSeatReservations[iTriggerID], iClientID)
					server.iClientSeatReservationCount[iTriggerID]++
				ELSE
					CPRINTLN(DEBUG_MINIGAME, "CMGL_UpdateServerDataForClient(", iClientID, ") - Failed to reserved seat (on trigger ", iTriggerID, ")")
				ENDIF
			
			ENDIF
			
			// Inform client that alloc was been attempted
			SET_BIT(server.iClientSeatRequestResponses, iClientID)
		
		ENDIF
	ELSE
		CLEAR_BIT(server.iClientSeatRequestResponses, iClientID)
	ENDIF

	//-- Respond to a client releasing a seat reservation ---------------------------------------------------
	IF IS_BIT_SET(client.iPackedRequestData, CMGL_CLIENT_BIT_ReleaseSeat)
		IF NOT IS_BIT_SET(server.iClientSeatReleaseResponses, iClientID)
		
			// Get trigger ID from packed data
			INT iTriggerID = client.iPackedRequestData & 255
			
			// Free trigger seat (if not freed yet)
			IF IS_BIT_SET(server.iClientSeatReservations[iTriggerID], iClientID)

				CPRINTLN(DEBUG_MINIGAME, "CMGL_UpdateServerDataForClient(", iClientID, ") - Released seat (on trigger ", iTriggerID, ")")
				CLEAR_BIT(server.iClientSeatReservations[iTriggerID], iClientID)
				server.iClientSeatReservationCount[iTriggerID]--
			
			ENDIF
			
			// Inform client that alloc was been attempted
			SET_BIT(server.iClientSeatReleaseResponses, iClientID)
		
		ENDIF
	ELSE
		CLEAR_BIT(server.iClientSeatReleaseResponses, iClientID)
	ENDIF
	
ENDPROC

PROC CMGL_RemoveServerDataForClient(CMGL_LOCAL& local, CMGL_SERVER& server, CMGL_CLIENT& client, INT iClientID)
UNUSED_PARAMETER(local)

	CPRINTLN(DEBUG_MINIGAME, "CMGL_RemoveServerDataForClient(", iClientID, ")")

	// Clear this client as connected
	CLEAR_BIT(server.iConnectedClientBits, iClientID)

	// Cleanup server (make sure all seat reservations are removed)
	INT iTriggerLoop
	REPEAT CMGL_TRIGGER_MAX iTriggerLoop
		IF IS_BIT_SET(server.iClientSeatReservations[iTriggerLoop], iClientID)
			CPRINTLN(DEBUG_MINIGAME, "CMGL_RemoveServerDataForClient(", iClientID, ") - Released seat (on trigger ", iTriggerLoop, ")")
			CLEAR_BIT(server.iClientSeatReservations[iTriggerLoop], iClientID)
			server.iClientSeatReservationCount[iTriggerLoop]--
		ENDIF
	ENDREPEAT
	
	// Cleanup client
	client.iPackedRequestData = 0

ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     LAUNCHER LOCAL
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


PROC CMGL_SetLocalState(CMGL_LOCAL& local, CMGL_LOCAL_STATE eNewState)
	CPRINTLN(DEBUG_MINIGAME, "CMGL_SetLocalState(", ENUM_NAME_CMGL_LOCAL_STATE(eNewState), ")")
	local.eState = eNewState
ENDPROC

PROC CMGL_InitFlags(CMGL_LOCAL& local, INT iApartmentInstID)
	
	// Packs IsMultiplayer/IsApartment and the apartment instance ID into one integer
	local.iFlags = (iApartmentInstID & 255)
	
	#IF CMGL_IS_MULTIPLAYER
		SET_BIT(local.iFlags, CMGL_FLAG_IsMultiplayer)
	#ENDIF
	#IF CMGL_IS_APARTMENT
		SET_BIT(local.iFlags, CMGL_FLAG_IsApartment)
	#ENDIF

ENDPROC

FUNC INT CMGL_GetApartmentID(CMGL_LOCAL& local)
	RETURN (local.iFlags & 255)
ENDFUNC

PROC CMGL_InitLauncher(CMGL_LOCAL& local, CMGL_SERVER& server, CMGL_CLIENT& client, INT iApartmentID = 0)
	
	// Check consts
	IF CMGL_BJACK_TRIGGER_MAX + CMGL_POKER_TRIGGER_MAX + CMGL_ROULE_TRIGGER_MAX > CMGL_TRIGGER_MAX
		CPRINTLN(DEBUG_MINIGAME,	"CMGL_InitLauncher(): CMGL_TRIGGER_MAX not large enough (given CMGL_BJACK_TRIGGER_MAX, CMGL_POKER_TRIGGER_MAX and CMGL_ROULE_TRIGGER_MAX)")
		SCRIPT_ASSERT(				"CMGL_InitLauncher(): CMGL_TRIGGER_MAX not large enough (given CMGL_BJACK_TRIGGER_MAX, CMGL_POKER_TRIGGER_MAX and CMGL_ROULE_TRIGGER_MAX)")
	ENDIF
	
	IF CMGL_IS_APARTMENT = 1 AND CMGL_IS_MULTIPLAYER = 0
		CPRINTLN(DEBUG_MINIGAME,	"CMGL_InitLauncher(): Mode is set as in apartment, but not set to multiplayer")
		SCRIPT_ASSERT(				"CMGL_InitLauncher(): Mode is set as in apartment, but not set to multiplayer")
	ENDIF

	IF iApartmentID >= CMGL_APARTMENT_ID_MAX
		CPRINTLN(DEBUG_MINIGAME,	"CMGL_InitLauncher(): iApartmentID (", iApartmentID, ") >= CMGL_APARTMENT_ID_MAX (", CMGL_APARTMENT_ID_MAX, ")")
		SCRIPT_ASSERT(				"CMGL_InitLauncher(): iApartmentID  >= CMGL_APARTMENT_ID_MAX")
	ENDIF

	// Init launcher
	CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Init)

	// Pack flags
	CMGL_InitFlags(local, iApartmentID)
	
	// Init triggers
	INT iTriggerLoop
	REPEAT CMGL_TRIGGER_MAX iTriggerLoop
		CMGL_InitTrigger(local, iTriggerLoop)
	ENDREPEAT

	// Init runtime data
	local.iTriggerID			= -1
	local.hContextPrompt		= NEW_CONTEXT_INTENTION
	local.hScriptThread			= NULL
	
	
	// Init multiplayer server+client data
	IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)

		// Init server (clear reservations)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			REPEAT CMGL_TRIGGER_MAX iTriggerLoop
				server.iClientSeatReservationCount[iTriggerLoop] = 0
				server.iClientSeatReservations[iTriggerLoop] = 0
			ENDREPEAT
			
			server.iClientSeatRequestResponses = 0
			server.iClientSeatReleaseResponses = 0
			server.iConnectedClientBits = 0
		ENDIF
		
		// Init client (clear request)
		client.iPackedRequestData = 0

	ENDIF
	
	
	// Init temp scenario test handles
	INT i
	REPEAT COUNT_OF(local.hTempTables) i
		local.hTempTables[i] = NULL
	ENDREPEAT

	REPEAT COUNT_OF(local.hTempPeds) i
		local.hTempPeds[i] = NULL
	ENDREPEAT

ENDPROC

FUNC PED_INDEX CLIFFORD_MG_TempCreateOffsetScenarioPed(MODEL_NAMES eModel, OBJECT_INDEX hTable, VECTOR vPosOffset, FLOAT fRotOffset, STRING sAnimDict, STRING sAnimName)

	PED_INDEX hPed = NULL
	IF DOES_ENTITY_EXIST(hTable)
	
		VECTOR vTablePos = GET_ENTITY_COORDS(hTable)
		FLOAT fTableRot = GET_ENTITY_HEADING(hTable)

		VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTablePos, fTableRot, vPosOffset)
		FLOAT fRot = fTableRot + fRotOffset
		
		CLEAR_AREA_OF_PEDS(vPos, 0.5)
		
		hPed = CREATE_PED(PEDTYPE_CIVMALE, eModel, vPos, fRot, FALSE)
		IF NOT IS_PED_INJURED(hPed)
		
			CPRINTLN(DEBUG_MINIGAME, "CLIFFORD_MG_TempCreateScenarioPed() - Created ped for table \"", sAnimDict, "\"")			
			TASK_PLAY_ANIM_ADVANCED(hPed, sAnimDict, sAnimName, vPos, <<0,0,fRot>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET, GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0) )

		ENDIF
	
	ENDIF
	
	RETURN hPed

ENDFUNC

PROC CLIFFORD_MG_CreateTempScenarios(CMGL_LOCAL& local, BOOL bAllowCurrentTriggerID = TRUE)

	// Only create other players when not in MP
	IF NOT IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
		
		OBJECT_INDEX hGrabTable

		// Create missing peds for each off-screen trigger
		INT iTriggerLoop
		REPEAT CMGL_TRIGGER_MAX iTriggerLoop
			IF local.mTriggers[iTriggerLoop].eGameType < CMG_TYPE_MAX
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//			AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), local.mTriggers[iTriggerLoop].vPos, <<10.0, 10.0, 10.0>>)
//			AND NOT IS_SPHERE_VISIBLE(local.mTriggers[iTriggerLoop].vPos, 5.0)
			
				// If currently offering a minigame, don't update peds for that table
				IF bAllowCurrentTriggerID
				OR local.iTriggerID != iTriggerLoop
			
					// Find table objects
					IF CLIFFORD_MG_GET_TABLE_OBJECT(local.mTriggers[iTriggerLoop].eGameType, local.mTriggers[iTriggerLoop].vPos, hGrabTable, FALSE)
					
						CLEAR_AREA_OF_PEDS(local.mTriggers[iTriggerLoop].vPos, 10.0)
						
						SWITCH local.mTriggers[iTriggerLoop].eGameType
							
							// Create poker peds
							CASE CMG_TYPE_Poker
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[0])//NOT CLIFFORD_MG_GET_DEALER_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, hGrabPed)
									local.hTempPeds[0] = CLIFFORD_MG_TempCreateOffsetScenarioPed(S_M_Y_Waiter_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,			"anim@mini@poker@dealer", "idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[1])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Poker, hGrabTable, 0, hGrabPed)
									local.hTempPeds[1] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.5, -0.25, 0.0>>, 90.0,		"anim@mini@poker@ped_c", "idle")
								ENDIF
//								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[2])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Poker, hGrabTable, 1, hGrabPed)
//									local.hTempPeds[2] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.65, 0.0, 0.0>>, 15.0,		"anim@mini@poker@ped_c", "idle")
//								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[3])//NOT CLIFFORD_MG_GET_SEAT_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, 2, hGrabPed)
									local.hTempPeds[3] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,			"anim@mini@poker@ped_c", "idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[4])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Poker, hGrabTable, 3, hGrabPed)
									local.hTempPeds[4] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<-0.7, 0.0, 0.0>>, -15.0,		"anim@mini@poker@ped_c", "idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[5])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Poker, hGrabTable, 4, hGrabPed)
									local.hTempPeds[5] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<-0.5, -0.15, 0.0>>, -90.0,	"anim@mini@poker@ped_c", "idle")
								ENDIF
							BREAK

							// Create blackjack peds
							CASE CMG_TYPE_Blackjack
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[6])//NOT CLIFFORD_MG_GET_DEALER_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, hGrabPed)
									local.hTempPeds[6] = CLIFFORD_MG_TempCreateOffsetScenarioPed(S_M_Y_Waiter_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,			"anim@mini@blackjack2@dealer", "idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[7])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Bjack, hGrabTable, 0, hGrabPed)
									local.hTempPeds[7] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.4, 0.5, 0.0>>, 110.0,		"anim@mini@blackjack@ped_b", "IDLE_E")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[8])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Bjack, hGrabTable, 1, hGrabPed)
									local.hTempPeds[8] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.2, 0.2, 0.0>>, 70.0,		"anim@mini@blackjack@ped_b", "IDLE_E")
								ENDIF
//								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[9])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Bjack, hGrabTable, 2, hGrabPed)
//									local.hTempPeds[9] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<-0.05, 0.1, 0.0>>, 6.0,		"anim@mini@blackjack@ped_b", "IDLE_E")
//								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[10])//NOT CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE_Bjack, hGrabTable, 3, hGrabPed)
									local.hTempPeds[10] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<-0.3, 0.25, 0.0>>, -40.0,	"anim@mini@blackjack@ped_b", "IDLE_E")
								ENDIF
							BREAK
						
							// Create roulette peds
							CASE CMG_TYPE_Roulette
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[11])//NOT CLIFFORD_MG_GET_DEALER_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, hGrabPed)
									local.hTempPeds[11] = CLIFFORD_MG_TempCreateOffsetScenarioPed(S_M_Y_Waiter_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,			"anim@mini@roulette@dealer", "dealer_idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[12])//NOT CLIFFORD_MG_GET_SEAT_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, 0, hGrabPed)
									local.hTempPeds[12] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,		"anim@mini@roulette@ped_a", "ped_a_idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[13])//NOT CLIFFORD_MG_GET_SEAT_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, 1, hGrabPed)
									local.hTempPeds[13] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,		"anim@mini@roulette@ped_b", "ped_b_idle")
								ENDIF
								IF NOT DOES_ENTITY_EXIST(local.hTempPeds[14])//NOT CLIFFORD_MG_GET_SEAT_PED(local.mTriggers[iTriggerLoop].eGameType, hGrabTable, 3, hGrabPed)
									local.hTempPeds[14] = CLIFFORD_MG_TempCreateOffsetScenarioPed(A_M_O_GenStreet_01, hGrabTable, <<0.0, 0.0, 0.0>>, 0.0,		"anim@mini@roulette@ped_d", "ped_d_idle")
								ENDIF
							BREAK
						
						ENDSWITCH
					ENDIF
		
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

PROC CMGL_UpdateLauncher(CMGL_LOCAL& local, CMGL_SERVER& server, CMGL_CLIENT& client, INT iClientID)
	
	CMGL_TRIGGER_BLOCK_CONDITION	eTriggerBlockCondition
	CLIFFORD_MG_LAUNCH_PARAMS		params
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		// Display seat reservations
		#IF IS_DEBUG_BUILD
			#IF CMGL_ENABLE_DEBUG_OUTPUT
				IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
					TEXT_LABEL_63 tDebugText
					FLOAT fDebugY = 0.1
					INT iDebugLoop
					REPEAT CMGL_TRIGGER_MAX iDebugLoop
						tDebugText = CMG_GET_DISPLAY_NAME(local.mTriggers[iDebugLoop].eGameType)
						tDebugText += " MP reservations: "
						tDebugText += server.iClientSeatReservationCount[iDebugLoop]
						SET_TEXT_SCALE(0.4, 0.4)
						SET_TEXT_COLOUR(150, 200, 255, 128)
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.025, fDebugY, "STRING", tDebugText)
						fDebugY += 0.05
					ENDREPEAT
				ENDIF
			#ENDIF
		#ENDIF
		
		TEXT_LABEL		tContextPromptRoot
		TEXT_LABEL_63	tScriptName
		INT				iStackSize
		
		// Update launcher state
		SWITCH local.eState
			
			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_Init
				IF NOT IS_BIT_SET(local.iFlags, CMGL_FLAG_IsApartment)
				
					REQUEST_ADDITIONAL_TEXT_FOR_DLC("DLCCAS", DLC_TEXT_SLOT2)
					IF HAS_ADDITIONAL_TEXT_LOADED(DLC_TEXT_SLOT2)
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_TempCreateTables)
					ENDIF
					
				ELSE
					CMGL_SetLocalState(local, CMGL_LOCAL_STATE_TempCreateTables)
				ENDIF
			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_TempCreateTables
				
				// Temp hack to create blackjack + poker tables (eventually artists will add these as scenarios)
				MODEL_NAMES eBlackjackTableModel
				MODEL_NAMES eRouletteTableModel
				MODEL_NAMES ePokerTableModel
				
				// Apartment
				IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsApartment)

					IF  CLIFFORD_MG_GET_TABLE_MODEL(CMG_TYPE_Blackjack, eBlackjackTableModel)
					AND CLIFFORD_MG_GET_TABLE_MODEL(CMG_TYPE_Roulette,	eRouletteTableModel)
					AND CLIFFORD_MG_GET_TABLE_MODEL(CMG_TYPE_Poker,		ePokerTableModel)
						
						REQUEST_MODEL(eBlackjackTableModel)
						REQUEST_MODEL(eRouletteTableModel)
						REQUEST_MODEL(ePokerTableModel)
						IF  HAS_MODEL_LOADED(eBlackjackTableModel)
						AND HAS_MODEL_LOADED(eRouletteTableModel)
						AND HAS_MODEL_LOADED(ePokerTableModel)
							
							INT iTriggerLoop
							REPEAT CMGL_TRIGGER_MAX iTriggerLoop
								IF local.mTriggers[iTriggerLoop].eGameType = CMG_TYPE_Blackjack
									local.hTempTables[0] = CREATE_OBJECT(eBlackjackTableModel, local.mTriggers[iTriggerLoop].vPos, FALSE, FALSE)
								
								ELIF local.mTriggers[iTriggerLoop].eGameType = CMG_TYPE_Roulette
									local.hTempTables[1] = CREATE_OBJECT(eRouletteTableModel, local.mTriggers[iTriggerLoop].vPos, FALSE, FALSE)
								
								ELIF local.mTriggers[iTriggerLoop].eGameType = CMG_TYPE_Poker
									local.hTempTables[2] = CREATE_OBJECT(ePokerTableModel, local.mTriggers[iTriggerLoop].vPos, FALSE, FALSE)
									
								ENDIF
							ENDREPEAT
							
							SET_MODEL_AS_NO_LONGER_NEEDED(eBlackjackTableModel)
							SET_MODEL_AS_NO_LONGER_NEEDED(eRouletteTableModel)
							SET_MODEL_AS_NO_LONGER_NEEDED(ePokerTableModel)
							CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
							
						ENDIF
					ENDIF
					
				// Casino (SP)
				ELIF NOT IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)

					REQUEST_MODEL(S_M_Y_Waiter_01)
					REQUEST_MODEL(A_M_O_GenStreet_01)
					
					REQUEST_ANIM_DICT("anim@mini@poker@dealer")
					REQUEST_ANIM_DICT("anim@mini@roulette@dealer")
					REQUEST_ANIM_DICT("anim@mini@blackjack2@dealer")
					
					REQUEST_ANIM_DICT("anim@mini@poker@ped_c")
					REQUEST_ANIM_DICT("anim@mini@blackjack@ped_b")
					REQUEST_ANIM_DICT("anim@mini@roulette@ped_a")
					REQUEST_ANIM_DICT("anim@mini@roulette@ped_b")
					REQUEST_ANIM_DICT("anim@mini@roulette@ped_d")
					
					IF  HAS_MODEL_LOADED(S_M_Y_Waiter_01)
					AND HAS_MODEL_LOADED(A_M_O_GenStreet_01)
					AND HAS_ANIM_DICT_LOADED("anim@mini@poker@dealer")
					AND HAS_ANIM_DICT_LOADED("anim@mini@roulette@dealer")
					AND HAS_ANIM_DICT_LOADED("anim@mini@blackjack2@dealer")
					AND HAS_ANIM_DICT_LOADED("anim@mini@poker@ped_c")
					AND HAS_ANIM_DICT_LOADED("anim@mini@blackjack@ped_b")
					AND HAS_ANIM_DICT_LOADED("anim@mini@roulette@ped_a")
					AND HAS_ANIM_DICT_LOADED("anim@mini@roulette@ped_b")
					AND HAS_ANIM_DICT_LOADED("anim@mini@roulette@ped_d")
					
						// Temp force create poker peds
//						local.hTempPeds[0] = CLIFFORD_MG_TempCreateScenarioPed(S_M_Y_Waiter_01,		<<964.60, 44.31, 80.06>>, 80.0,		"anim@mini@poker@dealer", "deal_self_1")
//						local.hTempPeds[1] = CLIFFORD_MG_TempCreateScenarioPed(A_M_O_GenStreet_01,	<<964.60, 44.31, 80.06>>, 80.0,		"anim@mini@poker@ped_c", "idle")
						
						// Temp force create blackjack peds
//						local.hTempPeds[2] = CLIFFORD_MG_TempCreateScenarioPed(S_M_Y_Waiter_01,		<<968.91, 48.64, 80.06>>, 18.31,	"anim@mini@blackjack2@dealer", "idle")
//						local.hTempPeds[3] = NULL//CLIFFORD_MG_TempCreateScenarioPed(A_M_O_GenStreet_01,	<<968.91, 48.64, 80.06>>, 18.31,	"anim@mini@blackjack@ped_c", "idle")
//
//						// Temp force create roulette peds
//						local.hTempPeds[4] = CLIFFORD_MG_TempCreateScenarioPed(S_M_Y_Waiter_01,		<<962.72, 49.61, 80.06>>, 140.0,	"anim@mini@roulette@dealer", "dealer_idle")
//						local.hTempPeds[5] = CLIFFORD_MG_TempCreateScenarioPed(A_M_O_GenStreet_01,	<<962.72, 49.61, 80.06>>, 140.0,	"anim@mini@roulette@ped_a", "ped_a_idle")
//						local.hTempPeds[6] = CLIFFORD_MG_TempCreateScenarioPed(A_M_O_GenStreet_01,	<<962.72, 49.61, 80.06>>, 140.0,	"anim@mini@roulette@ped_b", "ped_b_idle")
//						local.hTempPeds[7] = CLIFFORD_MG_TempCreateScenarioPed(A_M_O_GenStreet_01,	<<962.72, 49.61, 80.06>>, 140.0,	"anim@mini@roulette@ped_d", "ped_d_idle")

						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
						
					ENDIF

				ENDIF
			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_Idle
				
				// Create missing table peds
				CLIFFORD_MG_CreateTempScenarios(local)

				// Check hotspot of each trigger...
				local.iTriggerID = -1
				
				INT iTriggerLoop
				REPEAT CMGL_TRIGGER_MAX iTriggerLoop
					
					// If no offer has been made + this trigger is available
					IF local.iTriggerID = -1 AND local.mTriggers[iTriggerLoop].eGameType != CMG_TYPE_INVALID
					
						// Check if player is in the trigger's hotspot
						IF CMGL_CheckTriggerHotspot(local, iTriggerLoop, FALSE)
						
							// Check if table and enough peds are present
							IF CMGL_CheckTriggerAvailability(local, iTriggerLoop, server)
							
								eTriggerBlockCondition = CMGL_CheckTriggerBlockCondition(local, iTriggerLoop)
								
								// Check player doesn't have wanted level
								IF eTriggerBlockCondition = CMGL_TRIGGER_BLOCK_CONDITION_WantedLevel
									IF CMGL_GetWantedTextRoot(local.mTriggers[iTriggerLoop].eGameType, tContextPromptRoot)
										PRINT_HELP(tContextPromptRoot, 128)
									ENDIF
					
								// Check player has enough money
								ELIF eTriggerBlockCondition = CMGL_TRIGGER_BLOCK_CONDITION_Money
									IF CMGL_GetCostTextRoot(local.mTriggers[iTriggerLoop].eGameType, tContextPromptRoot)
										PRINT_HELP_WITH_NUMBER(tContextPromptRoot, CEIL(local.mTriggers[iTriggerLoop].fBuyIn), 128)
									ENDIF
								
								ELIF eTriggerBlockCondition = CMGL_TRIGGER_BLOCK_CONDITION_None
					
									// Add context intention
									CLEAR_HELP()
									IF CMGL_GetContextPromptTextRoot(local.mTriggers[iTriggerLoop].eGameType, IS_BIT_SET(local.iFlags, CMGL_FLAG_IsApartment), tContextPromptRoot)
										REGISTER_CONTEXT_INTENTION(local.hContextPrompt, CP_MAXIMUM_PRIORITY, tContextPromptRoot)
									ENDIF
									
									// Set offer state
									IF local.hContextPrompt != NEW_CONTEXT_INTENTION
										local.iTriggerID = iTriggerLoop
										CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Offer)
									ENDIF
								
								ENDIF
							
							ENDIF
							
						ENDIF
					ENDIF
					
				ENDREPEAT

			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_Offer
				
				// Create missing table peds
				CLIFFORD_MG_CreateTempScenarios(local, FALSE)

				IF local.hContextPrompt = NEW_CONTEXT_INTENTION
				OR CMGL_CheckTriggerBlockCondition(local, local.iTriggerID) != CMGL_TRIGGER_BLOCK_CONDITION_None
				OR CMGL_CheckTriggerHotspot(local, local.iTriggerID, TRUE) != TRUE
				
					// Remove context intention
					RELEASE_CONTEXT_INTENTION(local.hContextPrompt)
					CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
				
				ELIF IS_CONTEXT_INTENTION_TOP(local.hContextPrompt) AND HAS_CONTEXT_BUTTON_TRIGGERED(local.hContextPrompt)
					
					// Remove context intention
					RELEASE_CONTEXT_INTENTION(local.hContextPrompt)
					
					// Start launch process
					IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_RequestMpSeat)
					ELSE
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_ScriptLaunch)
					ENDIF
					
				ENDIF

			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_RequestMPSeat
				
				// Ask the server to reserve a seat...
				IF client.iPackedRequestData = 0
					IF IS_BIT_SET(server.iClientSeatRequestResponses, iClientID) = FALSE
						
						// Set requested trigger, and request alloc
						client.iPackedRequestData = local.iTriggerID
						SET_BIT(client.iPackedRequestData, CMGL_CLIENT_BIT_RequestSeat)
					
					ENDIF
				ELSE
					IF IS_BIT_SET(server.iClientSeatRequestResponses, iClientID) = TRUE
					
						// Respond to alloc success/failure, and clear request
						IF IS_BIT_SET(server.iClientSeatReservations[local.iTriggerID], iClientID)
							CMGL_SetLocalState(local, CMGL_LOCAL_STATE_ScriptLaunch)
						ELSE
							CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
						ENDIF
						client.iPackedRequestData = 0

					ENDIF
				ENDIF
				
			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_ScriptLaunch

				IF CMGL_GetScriptName(local.mTriggers[local.iTriggerID].eGameType, IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer), tScriptName)
					REQUEST_SCRIPT(tScriptName)
					IF HAS_SCRIPT_LOADED(tScriptName)
						
						// Setup params to pass to script
						params.minigameType				= local.mTriggers[local.iTriggerID].eGameType
						params.vLaunchLocation			= local.mTriggers[local.iTriggerID].vPos
						params.fBuyinAmountDollars		= local.mTriggers[local.iTriggerID].fBuyIn
						
						IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsApartment)
							// In apartments: There are multiple apartment instances, but only one game in each -> So the apartment instance gives a unique ID
							params.iMultiplayerInstanceID = CMGL_GetApartmentID(local)
						ELSE
							// In casino interior: There is one casino, but multiple game instances inside it -> So the game instance gives a unique ID
							params.iMultiplayerInstanceID = CMGL_APARTMENT_ID_MAX + local.mTriggers[local.iTriggerID].iGameID
						ENDIF

						// Temporary: Allow roulette to have a larger stack size
						iStackSize = MULTIPLAYER_MISSION_STACK_SIZE

						IF local.mTriggers[local.iTriggerID].eGameType = CMG_TYPE_Roulette
							iStackSize = TRANSITION_STACK_SIZE
						ENDIF
						
						// Start script
						CPRINTLN(DEBUG_MINIGAME, "CMGL_LOCAL: Launching script: \"", tScriptName, "\" stack=", iStackSize, ". ",
												 "Trigger=", local.iTriggerID, ", ",
												 "Buy in=$", params.fBuyinAmountDollars, ", ",
												 "iMultiplayerInstanceID=", params.iMultiplayerInstanceID)
						
						local.hScriptThread = START_NEW_SCRIPT_WITH_ARGS(tScriptName, params, SIZE_OF(params), iStackSize)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(tScriptName)
					
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_ScriptRun)
					
					ENDIF
				ELSE
					IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_ReleaseMPSeat)
					ELSE
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
					ENDIF
				ENDIF
			
			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_ScriptRun
				
				// If script ends, end launch process
				IF NOT IS_THREAD_ACTIVE(local.hScriptThread)
					IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_ReleaseMPSeat)
					ELSE
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
					ENDIF
				ENDIF
			
			BREAK

			// -----------------------------------------------------------------------
			CASE CMGL_LOCAL_STATE_ReleaseMPSeat

				// Ask the server to reserve a seat...
				IF client.iPackedRequestData = 0
					IF IS_BIT_SET(server.iClientSeatReleaseResponses, iClientID) = FALSE
						
						// Set requested trigger, and request alloc
						client.iPackedRequestData = local.iTriggerID
						SET_BIT(client.iPackedRequestData, CMGL_CLIENT_BIT_ReleaseSeat)
					
					ENDIF
				ELSE
					IF IS_BIT_SET(server.iClientSeatReleaseResponses, iClientID) = TRUE
					
						// Wait for release, and clear request
						CMGL_SetLocalState(local, CMGL_LOCAL_STATE_Idle)
						client.iPackedRequestData = 0

					ENDIF
				ENDIF

			BREAK

		ENDSWITCH
	
	ENDIF

ENDPROC

PROC CMGL_CleanupLauncher(CMGL_LOCAL& local, CMGL_SERVER& server, CMGL_CLIENT& client)
UNUSED_PARAMETER(server)

	CPRINTLN(DEBUG_MINIGAME, "CMGL_CleanupLauncher()")

	// Cleanup multiplayer
	IF IS_BIT_SET(local.iFlags, CMGL_FLAG_IsMultiplayer)
		
		// NOTE: Do not cleanup server data here, as it may need to migrate to next host
		
		// Cleanup client
		client.iPackedRequestData = 0
		
	ENDIF

	// Cleanup apartment test (temporary)
	MODEL_NAMES eBlackjackTableModel
	IF CLIFFORD_MG_GET_TABLE_MODEL(CMG_TYPE_Blackjack, eBlackjackTableModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(eBlackjackTableModel)
	ENDIF
	
	MODEL_NAMES ePokerTableModel
	IF CLIFFORD_MG_GET_TABLE_MODEL(CMG_TYPE_Poker, ePokerTableModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(ePokerTableModel)
	ENDIF
	
	INT i
	REPEAT COUNT_OF(local.hTempTables) i
		IF DOES_ENTITY_EXIST(local.hTempTables[i])
			DELETE_OBJECT(local.hTempTables[i])
		ENDIF
	ENDREPEAT

	// Cleanup casino test (temporary)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_Waiter_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_O_GenStreet_01)
					
	REMOVE_ANIM_DICT("anim@mini@poker@dealer")
	REMOVE_ANIM_DICT("anim@mini@roulette@dealer")
	REMOVE_ANIM_DICT("anim@mini@blackjack2@dealer")
	
	REMOVE_ANIM_DICT("anim@mini@poker@ped_c")
	REMOVE_ANIM_DICT("anim@mini@blackjack@ped_c")
	REMOVE_ANIM_DICT("anim@mini@roulette@ped_a")
	REMOVE_ANIM_DICT("anim@mini@roulette@ped_b")
	REMOVE_ANIM_DICT("anim@mini@roulette@ped_d")

	REPEAT COUNT_OF(local.hTempPeds) i
		IF DOES_ENTITY_EXIST(local.hTempPeds[i])
			DELETE_PED(local.hTempPeds[i])
		ENDIF
	ENDREPEAT

	// Cleanup launcher
	INT iTriggerLoop
	REPEAT CMGL_TRIGGER_MAX iTriggerLoop
		CMGL_CleanupTrigger(local, iTriggerLoop)
	ENDREPEAT
	
	RELEASE_CONTEXT_INTENTION(local.hContextPrompt)

ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

