//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_camera.sch										//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_camera.sch"
USING "script_maths.sch"
USING "player_scene_anim.sch"

#IF IS_DEBUG_BUILD
USING "scripted_cam_editor_public.sch"
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL SETUP_PLAYER_CAMERA_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, VECTOR &vecCamEndPos, VECTOR &vecCamEndRot, FLOAT& vecCamEndFov)
	
	vecCamEndFov = 40.0		//GET_GAMEPLAY_CAM_FOV()		//to fix 689599
	
	SWITCH eScene
		
		CASE PR_SCENE_DEAD
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDead
			sDead = "Dead eScene for player camera setup: "
			sDead += Get_String_From_Ped_Request_Scene_Enum(eScene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sDead)PRINTNL()
			SCRIPT_ASSERT(sDead)
			#ENDIF
			
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
		BREAK
		
		CASE PR_SCENE_HOSPITAL			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M_OVERRIDE		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_F_OVERRIDE		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_OVERRIDE		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		
		CASE PR_SCENE_M_DEFAULT			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_F_DEFAULT			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_DEFAULT			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3	vecCamEndPos = <<153.3026, -1308.0750, 31.1902>>		vecCamEndRot = <<2.0694, 0.0000, 68.3040>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_PHONECALL_ARM3	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_Ma_ARM3			vecCamEndPos = << -77.2636, -1107.2596, 28.7406 >>		vecCamEndRot = << -13.6138, -0.0000, -71.7254>>	RETURN TRUE BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1	vecCamEndPos = <<153.3026, -1308.0750, 31.1902>>		vecCamEndRot = <<2.0694, 0.0000, 68.3040>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM1	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3	vecCamEndPos = <<153.3026, -1308.0750, 31.1902>>		vecCamEndRot = <<2.0694, 0.0000, 68.3040>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM3	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FAMILY3		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FBI1			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FBI2			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FAMILY1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FBI4intro		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FBI4intro		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FBI5			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FBI3			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FBI4			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FBI5			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_Ma_FAMILY4_a		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_Ma_FAMILY4_b		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FAMILY4		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FINALEC		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_AGENCY1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_AGENCYprep1	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_AGENCY3B		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_CARSTEAL1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_CARSTEAL1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_AGENCY2		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_CARSTEAL2		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FBI2			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FBI4			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_DOCKS2B		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FAMILY6		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FINALEprepD	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FAMILY6		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_MARTIN1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_TREVOR3		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_TREVOR3		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FRANKLIN2		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FRANKLIN2		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FBI1end		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_Ma_MARTIN1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_Ta_MARTIN1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_RURAL2A		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_RURAL2A		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_RC_MRSP2		vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_Ma_RURAL1			vecCamEndPos = <<1393.8290, 3721.2100, 59.6646>>		vecCamEndRot = <<-77.4667, -3.4089, -29.2756>>	vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1a	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_FTa_FRANKLIN1b	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1c	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1d	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1e	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_EXILE2 		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_EXILE2			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_EXILE3			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_EXILE3			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M4_WASHFACE		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_MICHAEL3		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_MICHAEL3		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_DOCKS2A 		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_DOCKS2A		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FINALE1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FINALE1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_CARSTEAL4		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FINALE2intro	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FINALE2intro	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_DOCKS2B		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FINALE1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_AGENCY3A		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FINALE2A		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FINALE2A		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FINALE2B		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ta_FINALE2B		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FINALEA		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FINALEB		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Ma_FINALEC		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_Fa_FINALEC		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		
		CASE PR_SCENE_M_MD_FBI2
			vecCamEndPos = << 1421.369995, -1909.599976, 70.000000 >>	//<< 1363.9,-2016.8,56.5 >>
			vecCamEndRot = << -89.499535, -0.000001, 166.040573 >>		//<< -11.1,0,-165 >>
			vecCamEndFov = 50.0 RETURN TRUE BREAK
		CASE PR_SCENE_F_MD_FRANKLIN2
			vecCamEndPos = <<1375.1461, -2306.5835, 148.1897>> + <<0,0,-42.5>>
			vecCamEndRot = <<-78.9021, 0.0000, 25.2484>>
			RETURN TRUE BREAK
		
		CASE PR_SCENE_M2_BEDROOM		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b	vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a
			vecCamEndPos = <<-806.1221, 173.3714, 73.0859>>
			vecCamEndRot = <<-1.4712, -0.0000, -95.2503>>
			vecCamEndFov = 50.0000 RETURN TRUE BREAK	//v_PRM2_SAVEHOUSE1_a
		CASE PR_SCENE_M2_SAVEHOUSE1_b	vecCamEndPos = <<-777.6068, 187.4530, 74.1732>>			vecCamEndRot = <<-10.8798, -0.0000, -156.0149>>		RETURN TRUE BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF	vecCamEndPos = <<-1313.8571, 117.7842, 83.0089>>		vecCamEndRot = <<-81.4288, -0.0005, -4.4763>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M4_WAKEUPSCREAM	vecCamEndPos = <<-837.0487, 180.2459, 70.8420>>			vecCamEndRot = <<9.3514, -0.0001, -106.3141>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED	vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M4_HOUSEBED_b		vecCamEndPos = <<-837.0487, 180.2459, 70.8420>>			vecCamEndRot = <<9.3514, -0.0001, -106.3141>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSEBED		vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M4_WATCHINGTV		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_KIDS_TV		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_a		vecCamEndPos = <<-779.5107, 184.6536, 97.9522>>			vecCamEndRot = <<-83.1792, -0.0008, 33.1776>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_b		vecCamEndPos = <<-1350.3981, 355.7088, 89.1898>>		vecCamEndRot = <<-83.1793, -0.0008, 94.8576>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_POOLSIDE_a		vecCamEndPos = <<-11.4584, 509.3831, 199.7681>>			vecCamEndRot = <<-83.1793, -0.0008, 10.6550>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_POOLSIDE_b		vecCamEndPos = <<21.0263, 518.8938, 195.3684>>			vecCamEndRot = <<-83.1792, -0.0008, 10.6496>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANINGAPT	vecCamEndPos = <<-7.0679, 497.5869, 175.1577>>			vecCamEndRot = <<-8.6355, 0.0000, -18.0968>>		vecCamEndFov = 69.4280 RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONCELL			vecCamEndPos = <<25.1207, 538.1634, 176.3949>>			vecCamEndRot = <<-9.1084, 0.0000, 136.7439>>		vecCamEndFov = 60.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_SNACKING		vecCamEndPos = <<28.0599, 519.7770, 170.9287>>			vecCamEndRot = <<1.0712, 0.0000, 66.1406>>			vecCamEndFov = 62.3773 RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONLAPTOP		vecCamEndPos = <<-7.0679, 497.5869, 175.1577>>			vecCamEndRot = <<-8.6355, 0.0000, -18.0968>>		vecCamEndFov = 69.4280 RETURN TRUE BREAK
		CASE PR_SCENE_F1_IRONING		vecCamEndPos = <<28.0599, 519.7770, 170.9287>>			vecCamEndRot = <<1.0712, 0.0000, 66.1406>>			vecCamEndFov = 62.3773 RETURN TRUE BREAK
//		CASE PR_SCENE_F0_WATCHINGTV		vecCamEndPos = << -10.8925, -1442.1118, 31.6543 >>		vecCamEndRot = << -12.2422, -0.0000, -41.6801 >>	RETURN TRUE BREAK
		CASE PR_SCENE_F1_WATCHINGTV		vecCamEndPos = <<41.5060, 518.5600, 172.8830>>			vecCamEndRot = <<-3.4960, 0.0000, 80.2680>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_LUNCH_a		vecCamEndPos = <<-115.9504, 359.3368, 138.8368>>		vecCamEndRot = <<-84.1450, -0.0028, 30.8130>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M7_LUNCH_b		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT	vecCamEndPos = <<388.4338, 181.3034, 128.5381>>			vecCamEndRot = <<-82.1573, 0.0028, -133.5252>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M4_LUNCH_b		vecCamEndPos = << -1368.7029, 50.4611, 55.6791 >>		vecCamEndRot = << -25.2992, 0.0000, 39.0928 >>		RETURN TRUE BREAK
		CASE PR_SCENE_M4_CINEMA			vecCamEndPos = <<-1412.4731, -197.0279, 73.1327>>		vecCamEndRot = <<-85.8754, 0.0301, 103.5493>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_a		vecCamEndPos = <<-828.6376, 153.5095, 94.4233>>			vecCamEndRot = <<-82.4779, -87.7380, -38.5885>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b		vecCamEndPos = <<-886.2003, 130.8110, 84.4115>>			vecCamEndRot = <<-82.1702, -15.3164, 27.7905>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_a			vecCamEndPos = <<-1208.4165, -952.6558, 28.0064>>		vecCamEndRot = <<-81.4289, -0.0005, 145.6032>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_b			vecCamEndPos = <<-848.7570, 852.3610, 228.9125>>		vecCamEndRot = <<-81.4289, -0.0005, -13.8233>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_c			vecCamEndPos = <<-1267.4175, -707.9901, 48.8130>>		vecCamEndRot = <<-81.4294, -0.0010, 157.5229>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_a		vecCamEndPos = <<252.4540, 1118.0386, 245.2481>>		vecCamEndRot = <<-77.0357, 0.0002, -101.5461>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b		vecCamEndPos = <<-1817.5940, -685.8846, 35.5217>>		vecCamEndRot = <<-77.0356, 0.0002, 3.4539>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_a			vecCamEndPos = <<-1206.0880, -824.3892, 41.0638>>		vecCamEndRot = <<-84.7759, 0.0002, 88.1273>>		vecCamEndFov = 44.9926 RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_b			vecCamEndPos = <<-427.4440, -25.9635, 71.9877>>			vecCamEndRot = <<-84.7759, 0.0002, 46.2254>>		vecCamEndFov = 44.9926 RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_a	vecCamEndPos = <<815.9318, 1275.7186, 387.0281>>		vecCamEndRot = <<-76.9465, 0.0003, 175.4883>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b	vecCamEndPos = <<-1664.9277, 493.0317, 155.4287>>		vecCamEndRot = <<-76.9465, 0.0003, 149.7993>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a	vecCamEndPos = <<661.9240, 3503.3372, 60.5464>>			vecCamEndRot = <<-76.9465, -0.0009, -81.4523>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b	vecCamEndPos = <<2409.7986, 4293.2222, 61.6806>>		vecCamEndRot = <<-76.5680, -3.7101, 56.4411>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c	vecCamEndPos = <<100.7403, 3369.2800, 61.1030>>			vecCamEndRot = <<-77.9654, 2.7972, -172.4415>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d	vecCamEndPos = <<2441.2163, 3795.6873, 66.4953>>		vecCamEndRot = <<-75.6905, -1.8311, -34.0872>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e	vecCamEndPos = <<1779.8412, 4588.3213, 64.2296>>		vecCamEndRot = <<-77.2461, -2.1153, 134.3354>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_DRIVING_a		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M2_DRIVING_b		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_a		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_b		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_c		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_d		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_e		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_f		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_g		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DRIVING_h		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_RONBORING		vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_a		vecCamEndPos = << -1064.2815, -1530.4437, 4.9643 >>		vecCamEndRot = << -6.3151, 0.0000, -11.6771 >>		RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_b		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M2_CYCLING_c		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M2_PHARMACY		vecCamEndPos = <<70.2718, -1555.7793, 54.5700>>			vecCamEndRot = <<-79.8378, 0.0014, 146.7438>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M4_DOORSTUMBLE	vecCamEndPos = <<545.2649, 136.5109, 124.6734>>			vecCamEndRot = <<-71.4281, 0.0098, -40.0411>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR	vecCamEndPos = <<-611.2242, -215.1656, 69.3734>>		vecCamEndRot = <<-82.5325, -23.1336, 38.4436>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M_PIER_a			vecCamEndPos = <<-1733.7089, -1127.2845, 39.0047>>		vecCamEndRot = <<-83.9991, 0.0016, -37.0248>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_PIER_b			vecCamEndPos = <<-1926.9205, -575.5510, 37.5216>>		vecCamEndRot = <<-81.4289, -0.0005, 163.5237>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_MARINA			vecCamEndPos = <<-832.9372, -1357.8823, 31.1094>>		vecCamEndRot = <<-82.6154, 0.0027, -121.9677>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE	vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT	vecCamEndPos = <<-10.2980, -1454.6390, 30.9220>>		vecCamEndRot = <<3.3530, 0.0000, 15.6580>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_NEWHOUSE		vecCamEndPos = <<41.5060, 518.5600, 172.8830>>			vecCamEndRot = <<-3.4960, 0.0000, 80.2680>>				vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M4_PARKEDBEACH	vecCamEndPos = <<-2008.9288, -494.5522, 38.0431>>		vecCamEndRot = <<-74.6696, 3.4735, 101.1669>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_HOOKERMOTEL		vecCamEndPos = <<-1500.4567, -682.8798, 52.8456>>		vecCamEndRot = <<-82.2007, -0.0021, -20.6200>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M_HOOKERCAR		vecCamEndPos = <<852.3899, -1196.4309, 51.5570>>		vecCamEndRot = <<-85.1710, -0.0004, 166.3516>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_a		vecCamEndPos = <<-514.3963, -19.7218, 71.4872>>			vecCamEndRot = <<-85.8754, 0.0302, -142.4507>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_b		vecCamEndPos = <<-627.7501, 245.7220, 107.7731>>		vecCamEndRot = <<-85.8754, 0.0301, 148.5493>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_c		vecCamEndPos = <<-831.1096, -350.8676, 64.5357>>		vecCamEndRot = <<-85.8754, 0.0301, 73.7675>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_a		vecCamEndPos = << -466.3570, -1586.5269, 42.1308 >>		vecCamEndRot = << -24.1059, 0.0000, -157.2387 >>	RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_b		vecCamEndPos = << -1738.6078, -624.3743, 12.7827 >>		vecCamEndRot = << -22.3162, 0.0000, 105.6959 >>		RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_c		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M_BENCHCALL_a		vecCamEndPos = <<-95.4359, -413.3546, 61.9024>>			vecCamEndRot = <<-84.9814, -0.0003, 154.7523>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_b		vecCamEndPos = <<-1290.9719, -697.7042, 50.5674>>		vecCamEndRot = <<-84.9813, -0.0003, 54.7523>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_MORNING_a		vecCamEndPos = << 1969.9984, 3806.8472, 35.4940 >>		vecCamEndRot = << -14.7978, 0.0000, -11.4244 >>		RETURN TRUE BREAK
//		CASE PR_SCENE_M6_MORNING_b		vecCamEndPos =  <<1974.4213, 3817.4978, 34.1967>>		vecCamEndRot = <<-16.8570, 0.0000, -2.7419>> 		RETURN TRUE BREAK
		CASE PR_SCENE_M6_CARSLEEP		vecCamEndPos = <<672.4669, 3502.7385, 58.9099>>			vecCamEndRot = <<-83.2715, 4.5479, 61.6184>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSETV_a		vecCamEndPos = <<1975.9104, 3822.4126, 34.0088>>		vecCamEndRot = <<-10.9024, -0.0000, 154.0872>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK	//v_PRM6_HOUSETV_a
//		CASE PR_SCENE_M6_HOUSETV_b		vecCamEndPos = << 1973.4263, 3820.1006, 34.7771 >>		vecCamEndRot = << -28.5269, 0.0000, -69.4462 >>		RETURN TRUE BREAK
		CASE PR_SCENE_M6_SUNBATHING		vecCamEndPos = <<1983.7357, 3822.7627, 57.6395>>		vecCamEndRot = <<-83.4323, -0.0013, 121.7344>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRINKINGBEER	vecCamEndPos = <<1978.2292, 3820.7212, 34.0738>>		vecCamEndRot = <<-8.2785, 0.0000, 103.9586>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK	//v_PRM6_DRINKINGBEER
		CASE PR_SCENE_M6_ONPHONE		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_DEPRESSED		vecCamEndPos = <<1973.9507, 3819.0784, 33.7825>>		vecCamEndRot = <<-5.3804, 0.0000, -17.5258>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK	//v_PRM6_DEPRESSED
		CASE PR_SCENE_M6_BOATING		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M6_LIQUORSTORE	vecCamEndPos = <<1994.9846, 3057.2192, 49.8043>>		vecCamEndRot = <<-49.0498, 0.0012, 99.8958>>		vecCamEndFov = 41.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M6_PILOTSCHOOL	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_M6_TRIATHLON		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M7_RESTAURANT		vecCamEndPos = <<-113.8308, 361.9919, 138.5542>>		vecCamEndRot = <<-76.7137, -0.0014, 41.6163>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS	vecCamEndPos = <<-1349.1143, 351.6396, 100.0071>>		vecCamEndRot = <<-80.7411, -0.0078, 34.6522>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a	vecCamEndPos = <<-715.1777, 255.9193, 105.5536>>		vecCamEndRot = <<-84.0507, -0.0034, 89.2749>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b	vecCamEndPos = <<-715.3492, 256.7212, 105.5855>>		vecCamEndRot = <<-82.449600, -0.003003, 99.947533>>	vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_WIFETENNIS		vecCamEndPos = <<-774.8297, 164.9666, 104.4211>>		vecCamEndRot = <<-81.8771, -0.0817, 178.2323>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_ROUNDTABLE		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_REJECTENTRY	vecCamEndPos = <<-720.5396, 253.1793, 105.7766>>		vecCamEndRot = <<-83.2751, 3.4532, -42.6086>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_HOOKERS		vecCamEndPos = <<533.3047, 120.3593, 122.7126>>			vecCamEndRot = <<-83.8911, -0.0019, -129.3945>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITBARBER		vecCamEndPos = <<-827.4469, -191.4707, 63.4392>>		vecCamEndRot = <<-80.7034, 0.0005, -51.0296>>		vecCamEndFov = 44.8880 RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP	vecCamEndPos = <<-718.5201, -159.4091, 62.5628>>		vecCamEndRot = <<-79.3643, 0.0002, -33.1272>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_FAKEYOGA		vecCamEndPos = <<-786.2485, 187.6860, 97.9448>>			vecCamEndRot = <<-80.7232, 0.0002, 104.9069>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_COFFEE			vecCamEndPos = <<-1370.7305, -209.3841, 70.3220>>		vecCamEndRot = <<-85.8754, 0.0301, -94.4507>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_GETSREADY		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_M7_PARKEDHILLS	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_M7_READSCRIPT		vecCamEndPos = <<-778.8658, 185.7413, 98.1194>>			vecCamEndRot = <<-84.2688, 0.0243, 42.5521>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		
		#IF NOT USE_TU_CHANGES
		CASE PR_SCENE_M7_EMPLOYEECONVO	vecCamEndPos = <<-1133.2946, -455.1246, 95.9402>>		vecCamEndRot = <<-85.6256, -0.0000, 28.0351>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		#ENDIF
		#IF USE_TU_CHANGES
		CASE PR_SCENE_M7_EMPLOYEECONVO	vecCamEndPos = <<-1133.2946, -455.1246, 95.9402-30>>	vecCamEndRot = <<-85.6256, -0.0000, 28.0351>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		#ENDIF
		
		CASE PR_SCENE_M7_TALKTOGUARD	vecCamEndPos = <<-1046.6012, -478.8773, 62.6878>>		vecCamEndRot = <<-77.3255, -6.2970, 139.6857>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOT_JIMMY		vecCamEndPos = <<-1184.2911, -505.4514, 75.0146>>		vecCamEndRot = <<-82.6035, 0.0014, -31.0152>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_TV		vecCamEndPos = <<-774.6182, 189.3469, 73.1101>>			vecCamEndRot = <<4.5397, -0.0000, 113.3996>>		vecCamEndFov = 43.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_GAMING	vecCamEndPos = <<-837.0487, 180.2459, 70.8420>>			vecCamEndRot = <<9.3514, -0.0001, -106.3141>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA	vecCamEndPos = <<-657.0717, -271.9325, 61.5797>>		vecCamEndRot = <<-78.4277, 9.0072, -174.2934>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY	vecCamEndPos = <<-252.0919, -81.0714, 74.7886>>			vecCamEndRot = <<-86.5763, 49.0804, -90.1929>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY	vecCamEndPos = <<-831.4534, 178.1406, 96.6498>>			vecCamEndRot = <<-73.1145, -0.0027, -93.0056>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR	vecCamEndPos = <<-250.7753, -80.1775, 74.8044>>			vecCamEndRot = <<-85.8438, 27.4390, -107.7830>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		
		CASE PR_SCENE_M_S_FAMILY4		vecCamEndPos = <<-803.7232, 181.6804, 73.2304>>			vecCamEndRot = <<-5.3638, 0.0000, -22.9153>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK	//v_PRM_S_FAMILY4
		
	ENDSWITCH
	SWITCH eScene
		
//		CASE PR_SCENE_F0_SAVEHOUSE		vecCamEndPos = << -19.1775, -1449.9987, 33.3962 >>		vecCamEndRot = << -17.8780, 0.0000, -27.3560 >>		RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_ASLEEP		vecCamEndPos = <<-23.8601, -1456.8895, 30.8716>>		vecCamEndRot = <<2.4688, 0.0000, -32.8280>>			vecCamEndFov = 37.6481 RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_ASLEEP		vecCamEndPos = <<-7.0679, 497.5869, 175.1577>>			vecCamEndRot = <<-8.6355, 0.0000, -18.0968>>		vecCamEndFov = 69.4280 RETURN TRUE BREAK
		CASE PR_SCENE_F1_NAPPING		vecCamEndPos = <<41.5060, 518.5600, 172.8830>>			vecCamEndRot = <<-3.4960, 0.0000, 80.2680>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_GETTINGREADY	vecCamEndPos = <<41.5060, 518.5600, 172.8830>>			vecCamEndRot = <<-3.4960, 0.0000, 80.2680>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_READING		vecCamEndPos = <<-10.2980, -1454.6390, 30.9220>>		vecCamEndRot = <<3.3530, 0.0000, 15.6580>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_READING		vecCamEndPos = <<41.5060, 518.5600, 172.8830>>			vecCamEndRot = <<-3.4960, 0.0000, 80.2680>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		
		#IF NOT USE_TU_CHANGES
		CASE PR_SCENE_F0_SH_PUSHUP_a	vecCamEndPos = <<-9.8814, -1454.5720, 32.7605>>			vecCamEndRot = <<-10.1081, 0.0000, 13.7157>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		#ENDIF
		#IF USE_TU_CHANGES
		CASE PR_SCENE_F0_SH_PUSHUP_a	vecCamEndPos = <<-10.2980, -1454.6390, 30.9220>>		vecCamEndRot = <<3.3530, 0.0000, 15.6580>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		#ENDIF
		
		CASE PR_SCENE_F0_SH_PUSHUP_b	vecCamEndPos = <<-13.2465, -1425.4192, 31.1279>>		vecCamEndRot = <<-30.6822, 0.6787, 15.1437>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_PUSHUP		vecCamEndPos = <<14.9547, 525.4391, 170.6328>>			vecCamEndRot = <<-30.6819, 0.6789, -170.1687>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_a		vecCamEndPos = <<-1175.9375, -1575.7007, 30.1692>>		vecCamEndRot = <<-84.0992, -0.0012, -19.5058>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC		vecCamEndPos = <<-1175.7921, -1576.0494, 30.8335>>		vecCamEndRot = <<-80.2938, 0.0008, -26.9162>>		vecCamEndFov = 49.9377 RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_b		vecCamEndPos = << -1151.1907, -1365.4847, 9.0273 >>		vecCamEndRot = << -14.4946, 0.0000, 176.3572 >>		RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_c		vecCamEndPos = << -1171.8738, -1425.2235, 9.2445 >>		vecCamEndRot = << -17.1697, 0.0000, -111.3917 >>	RETURN TRUE BREAK
		CASE PR_SCENE_F0_GARBAGE		vecCamEndPos = <<-14.2911, -1443.6281, 56.3341>>		vecCamEndRot = <<-81.6191, 0.0002, -175.2965>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_GARBAGE		vecCamEndPos = <<18.2618, 537.6057, 201.7112>>			vecCamEndRot = <<-81.6191, 0.0002, 31.4594>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_THROW_CUP		vecCamEndPos = <<-0.9251, -1606.8064, 54.3956>>			vecCamEndRot = <<-81.9792, -0.0059, -89.5973>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND	vecCamEndPos = <<1.8601, -1609.9586, 55.0489>>			vecCamEndRot = <<-81.9213, 0.0006, -30.8316>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_GYM				vecCamEndPos = <<-1246.2660, -1614.2175, 30.1603>>		vecCamEndRot = <<-85.4036, -0.0003, -85.7066>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F0_WALKCHOP		vecCamEndPos = <<-13.0353, -1450.7229, 55.6841>>		vecCamEndRot = <<-83.5727, -0.0019, 129.2043>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F0_PLAYCHOP		vecCamEndPos = <<-11.3015, -1423.0619, 55.8104>>		vecCamEndRot = <<-83.5727, 0.0005, 102.8099>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_F1_WALKCHOP		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_F_WALKCHOP_a		vecCamEndPos = <<154.6263, 769.2573, 234.8318>>			vecCamEndRot = <<-83.5727, -0.0019, -173.7957>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_b		vecCamEndPos = <<-264.6808, 416.6765, 134.8675>>		vecCamEndRot = <<-83.5727, -0.0019, 126.6163>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_PLAYCHOP		vecCamEndPos = <<13.4706, 520.5046, 195.3417>>			vecCamEndRot = <<-83.5727, 0.0005, -15.8635>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_a		vecCamEndPos = << -452.4845, -1591.8811, 43.1702 >>		vecCamEndRot = << -15.7272, 0.0000, 116.1361 >>		RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_b
			vecCamEndPos = <<41.4086, -1491.7068, 94.2915>>
			vecCamEndRot = <<-83.0867, -0.0000, -132.4861>>
			RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_c		vecCamEndPos = << 213.7408, 224.4167, 107.2885 >>		vecCamEndRot = << -17.6158, 0.0000, 124.3368 >>		RETURN TRUE BREAK
		CASE PR_SCENE_F0_BIKE			vecCamEndPos = <<-26.0418, -1435.7556, 56.3367>>		vecCamEndRot = <<-85.6549, 0.0007, -100.9700>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F0_CLEANCAR		vecCamEndPos = <<-26.9523, -1443.4591, 56.3006>>		vecCamEndRot = <<-82.1580, 0.0001, -120.4657>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_BIKE			vecCamEndPos = <<13.4810, 547.7665, 201.6628>>			vecCamEndRot = <<-85.4529, 32.8556, -165.2088>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR
			#IF NOT IS_NEXTGEN_BUILD
			vecCamEndPos = <<9.4245, 548.1450, 201.2811>>
			vecCamEndRot = <<-83.3180, 36.2472, 157.2104>>
			#ENDIF
			#IF IS_NEXTGEN_BUILD
			vecCamEndPos = <<9.7918, 546.587, 201.411>>
			vecCamEndRot = <<-85.6569, 7.19613, 128.447>>
			#ENDIF
			vecCamEndFov = 45.0000
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_BYETAXI		vecCamEndPos = <<5.4402, 547.3710, 201.0860>>			vecCamEndRot = <<-75.7499, -5.6172, -42.2747>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_c			vecCamEndPos = <<-1150.7317, 941.2178, 224.4071>>		vecCamEndRot = <<-84.8486, -22.4675, 10.6362>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_d			vecCamEndPos = <<-1690.1348, -945.0534, 33.3591>>		vecCamEndRot = <<-85.6549, 0.0007, -128.9700>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMGRAFF		vecCamEndPos = <<-83.3090, -1457.2451, 59.1689>>		vecCamEndRot = <<-82.0413, -0.0011, 83.9391>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_CLUB			vecCamEndPos = <<-527.8450, -32.3779, 69.5368>>			vecCamEndRot = <<-77.7626, 15.8623, -13.8244>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE	vecCamEndPos = <<504.4986, -1318.4994, 29.3536>>		vecCamEndRot = <<5.2409, -0.0000, 89.8084>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPEHANDS	vecCamEndPos = <<504.4986, -1318.4994, 29.3536>>		vecCamEndRot = <<5.2409, -0.0000, 89.8084>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT	vecCamEndPos = <<504.4986, -1318.4994, 29.3536>>		vecCamEndRot = <<5.2409, -0.0000, 89.8084>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_a_01		vecCamEndPos = <<30.1511, -1357.0276, 54.6942>>			vecCamEndRot = <<-78.5097, 0.0010, 21.5880>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_b_01		vecCamEndPos = <<-380.8515, 226.4113, 109.5036>>		vecCamEndRot = <<-78.5097, 0.0010, -153.1610>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_c_02		vecCamEndPos = <<135.3769, -1305.2886, 54.2690>>		vecCamEndRot = <<-81.3478, 0.0016, 71.6158>>		vecCamEndFov = 42.2656 RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_d_02		vecCamEndPos = <<789.0312, -738.3510, 52.6819>>			vecCamEndRot = <<-81.3478, 0.0016, -42.3726>>		vecCamEndFov = 42.2656 RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_e_01		vecCamEndPos = <<-296.2211, -1326.7321, 56.6653>>		vecCamEndRot = <<-78.5097, 0.0010, 177.9219>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_F_TAUNT			vecCamEndPos = <<-45.2266, -1509.5935, 68.1322>>		vecCamEndRot = <<-85.2245, -0.0025, -161.2204>>		RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1		vecCamEndPos = <<-6.8196, -1411.3240, 55.2464>>			vecCamEndRot = <<-78.7542, -0.0029, 172.9781>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2		vecCamEndPos = <<297.0839, -1621.7515, 56.4597>>		vecCamEndRot = <<-78.7542, -0.0026, 13.8147>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3		vecCamEndPos = <<-237.5357, -1534.0734, 57.4441>>		vecCamEndRot = <<-78.2577, 0.0009, 122.6248>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5		vecCamEndPos = <<-21.2522, -1820.3120, 51.5856>>		vecCamEndRot = <<-76.7860, 0.0038, -145.6433>>		vecCamEndFov = 43.2990 RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT	vecCamEndPos = <<189.5486, -1676.4065, 54.9378>>		vecCamEndRot = <<-76.6137, 0.0002, -45.2699>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_S_EXILE2
//			VECTOR vCreateExile2Coords, vecExile2CamEndOff
//			vCreateExile2Coords = <<-2689.2244, 2368.0752, 15.7681>>
//			vecExile2CamEndOff = << -2689.2244, 2368.0752, 77.517100 >> - vCreateExile2Coords
//			
//			vecCamEndPos = vCreateExile2Coords + (vecExile2CamEndOff*0.7)
			
			vecCamEndRot = << -89.499535, -0.000001, 166.040573-180 >>
			vecCamEndFov = 50.0
			
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
			
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_a	vecCamEndPos = <<-79.1742, -1020.3333, 54.5996>>		vecCamEndRot = <<-86.8093, -0.0114, -28.8534>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_b	vecCamEndPos = <<-79.1742, -1020.3333, 54.5996>>		vecCamEndRot = <<-86.8093, -0.0114, -28.8534>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_F_S_FBI1end		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK BREAK
//		CASE PR_SCENE_F_S_AGENCY_2B
//			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING("PR_SCENE_F_S_AGENCY_2B called - no specific cam")PRINTNL()
//		RETURN FALSE
//		BREAK
//		CASE PR_SCENE_F_S_AGENCY_2C
//			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING("PR_SCENE_F_S_AGENCY_2C called - no specific cam")PRINTNL()
//		RETURN FALSE
//		BREAK
		
//		CASE PR_SCENE_T_STRIPCLUB_a		vecCamEndPos = << 110.7018, -1288.5123, 30.2271 >>		vecCamEndRot = << -27.7435, -0.0000, 169.0141 >>	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE	vecCamEndPos = <<153.3026, -1308.0750, 31.1902>>		vecCamEndRot = <<2.0694, 0.0000, 68.3040>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_BAR			vecCamEndPos = <<153.3026, -1308.0750, 31.1902>>		vecCamEndRot = <<2.0694, 0.0000, 68.3040>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_CHASE		vecCamEndPos = <<133.4546, -1306.8057, 55.1700>>		vecCamEndRot = <<-79.3860, -0.0059, 29.3671>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_STRIPCLUB_out	vecCamEndPos = <<135.0825, -1306.1569, 54.8922>>		vecCamEndRot = <<-83.3399, 0.2592, 40.1620>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_ESCORTED_OUT	vecCamEndPos = <<-50.4324, 348.2621, 157.2838>>			vecCamEndRot = <<-83.7124, 0.0772, 28.1343>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_b	vecCamEndPos = << 1534.4061, 3607.2744, 36.6934 >>		vecCamEndRot = << -12.1833, 0.0000, 9.7072 >>		RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_c	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_CR_CHATEAU_d	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT	vecCamEndPos = <<-119.1385, -444.0774, 61.0046>>		vecCamEndRot = <<-85.9063, 0.0045, -35.7580>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PARK_b		vecCamEndPos = <<-1857.7023, 2072.0447, 166.0882>>		vecCamEndRot = <<-85.9063, 0.0045, 126.2420>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_T_CN_PARK_c		vecCamEndPos = << -1870.9778, 2036.9607, 140.7903 >>	vecCamEndRot = << -8.8816, 0.0000, 129.8679 >>		RETURN TRUE BREAK
		CASE PR_SCENE_T_SMOKEMETH		vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_a			vecCamEndPos = <<287.7224, -3200.3870, 31.6541>>		vecCamEndRot = <<-83.4634, -0.0001, -168.0601>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_b			vecCamEndPos = <<-869.8748, 66.6628, 77.9598>>			vecCamEndRot = <<-83.4634, -0.0001, 62.0870>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_c			vecCamEndPos = <<-44.7279, -1473.6636, 57.8914>>		vecCamEndRot = <<-83.4634, -0.0001, 107.5896>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_d			vecCamEndPos = <<1874.6770, 2621.5630, 71.5578>>		vecCamEndRot = <<-83.4635, -0.0001, -120.0601>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_T_SWEATSHOP		vecCamEndPos = << 716.6193, -970.8576, 28.2888 >>		vecCamEndRot = << -38.9692, 0.0000, -156.3869 >>	RETURN TRUE BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD	vecCamEndPos = <<437.0992, -1462.1802, 54.8615>>		vecCamEndRot = <<-83.9471, 0.0018, 96.0313>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_THROW_FOOD		vecCamEndPos = <<442.4368, -1461.1077, 54.2539>>		vecCamEndRot = <<-72.1078, -0.0029, 104.5689>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK	vecCamEndPos = <<-1267.9402, -1104.5046, 32.9180>>		vecCamEndRot = <<-80.9687, 0.0000, -3.6111>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_ALLEYDRUNK	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		vecCamEndPos = <<-1201.4962, -1571.5861, 29.7218>>		vecCamEndRot = <<-84.4445, 0.0015, -51.2858>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK		//<<-1200.3567, -1571.1133, 5.1365>>, <<-2.2180, -0.0002, -33.6270>>, fov = 40.0000
		CASE PR_SCENE_T_ANNOYSUNBATHERS	vecCamEndPos = <<-1322.5938, -1669.3389, 27.6893>>		vecCamEndRot = <<-81.6907, 0.0005, 60.2722>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE	vecCamEndPos = <<638.9762, -1002.2932, 68.2444>>		vecCamEndRot = <<-82.3678, -0.0043, -59.3736>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_GARDEN	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_NAKED_ISLAND	vecCamEndPos = <<2786.5510, -1451.6735, 26.3083>>		vecCamEndRot = <<-81.3303, -0.0028, -117.8646>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_DUMPSTER		vecCamEndPos = <<492.7402, -1337.7001, 53.2989>>		vecCamEndRot = <<-74.0621, -0.0069, 128.6647>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEBEACH	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_CN_WAKETRASH_b	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_CN_WAKEBARN		vecCamEndPos =<<2210.5369, 4919.1538, 41.1959>>			vecCamEndRot = <<-3.5325, -0.0000, 169.6810>>		RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRAIN	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_CR_WAKEROOFTOP	vecCamEndPos = << 415.3352, -800.8342, 51.2722 >>		vecCamEndRot = << -17.2722, -0.0000, -24.5920 >>	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_CR_FUNERAL		vecCamEndPos = <<411.8938, -1481.1056, 55.0911>>		vecCamEndRot = <<-73.5286, 0.0012, 177.4479>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	vecCamEndPos = <<297.5302, 179.2290, 129.4594>>			vecCamEndRot = <<-83.7434, 0.0126, 62.1929>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN	vecCamEndPos = <<294.6928, 195.4445, 129.4204>>			vecCamEndRot = <<-77.0175, -0.0008, 155.6596>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK

		CASE PR_SCENE_T_CR_CHASECAR_a
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			vecCamEndFov = 50.0
			RETURN FALSE BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b
			vecCamEndPos = <<-930.9908, 2873.0781, 22.1187>> + <<0,0,GET_RANDOM_FLOAT_IN_RANGE(5, 20)>>
			vecCamEndRot = << -89.499535, -0.000001, 1.040573 >>
			vecCamEndFov = 50.0
			
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
			
			
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
		CASE PR_SCENE_T_CR_CHASESCOOTER
			#IF NOT USE_TU_CHANGES
			vecCamEndPos = <<1313.7350, -1149.1787, 82.2928>>
			vecCamEndRot = <<-89.4995, -0.0000, -59.5958>>
			vecCamEndFov = 50.0 RETURN TRUE BREAK
			#ENDIF
			#IF USE_TU_CHANGES
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
			#ENDIF
		CASE PR_SCENE_T_CR_POLICE_a
			#IF NOT USE_TU_CHANGES
			vecCamEndPos = << 937.3,-1195.9,49.4+30.0>>				//<<896.5768, -1198.5872, 98.7699>>
			vecCamEndRot = <<-89.4995, -0.0041, -85.9115>>
			vecCamEndFov = 55.0 RETURN TRUE BREAK
			#ENDIF
			#IF USE_TU_CHANGES
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
			#ENDIF
		CASE PR_SCENE_T_CN_POLICE_b
			#IF NOT USE_TU_CHANGES
			vecCamEndPos = <<1893.4,2303.4,54.5>>
			vecCamEndPos.z += 30.0
			vecCamEndRot = <<-89.4995, 0.0001, -15.9960>>
			vecCamEndFov = 55.0 RETURN TRUE BREAK
			#ENDIF
			#IF USE_TU_CHANGES
			vecCamEndPos = <<0,0,0>>
			vecCamEndRot = <<0,0,0>>
			RETURN FALSE
			#ENDIF
		CASE PR_SCENE_T_CN_POLICE_c
			vecCamEndPos = <<1076.9883, 2685.1528, 37.9730+30.0>>	//<<1063.6404, 2692.5437, 93.9365>>
			vecCamEndRot = <<-89.4995, 0.0796, -93.9607>>
			vecCamEndFov = 55.0 RETURN TRUE BREAK
			
		CASE PR_SCENE_T_CR_LINGERIE		vecCamEndPos = <<150.8545, -214.1522, 79.4128>>			vecCamEndRot = <<-78.7899, -0.0026, -136.2629>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
//		CASE PR_SCENE_T_CR_MACHINE		vecCamEndPos = << 329.9294, -1301.4003, 37.4339 >>		vecCamEndRot = << -10.8717, -0.2723, -71.6290 >>	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	vecCamEndPos = <<43.4637, -2055.6338, 47.3524>>			vecCamEndRot = <<-80.4436, 0.0000, -109.8209>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_a		vecCamEndPos = << -1241.5002, -1099.6058, 10.4958 >>	vecCamEndRot = << -8.2431, 0.0000, 146.4822 >>		RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_b		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_FIGHTBAR_c		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_YELLATDOORMAN	vecCamEndPos = <<-725.9904, -1305.7162, 30.7195>>		vecCamEndRot = <<-86.3376, 0.0189, -169.3006>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_FIGHTCASINO		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE	vecCamEndPos = <<-1275.4763, -1194.2114, 38.3756>>		vecCamEndRot = <<-84.3481, -0.0001, -125.4683>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE	vecCamEndPos = <<-876.4864, -849.5565, 45.7044>>		vecCamEndRot = <<-65.6340, -0.0013, 106.1902>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_UNDERPIER		vecCamEndPos = <<-1708.9696, -1055.7620, 44.8272>>		vecCamEndRot = <<-76.5326, -0.0048, -144.2845>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_KONEIGHBOUR		vecCamEndPos = <<-1153.2279, -1526.3785, 30.1087>>		vecCamEndRot = <<-87.0602, 0.0019, -17.9875>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_SCARETRAMP		vecCamEndPos = <<-562.2963, -1256.6270, 38.9716>>		vecCamEndRot = <<-80.9113, -0.0008, 143.1449>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_DRUNKHOWLING	vecCamEndPos = <<442.2141, -229.2575, 81.2032>>			vecCamEndRot = <<-83.8876, -0.0064, 45.2185>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING	vecCamEndPos = <<153.3026, -1308.0750, 31.1902>>		vecCamEndRot = <<2.0694, 0.0000, 68.3040>>			vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	vecCamEndPos = <<-1134.3420, -1497.2400, 4.7530>>		vecCamEndRot = <<4.7530, 0.0000, 151.5690>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A	vecCamEndPos = <<-1173.4980, -1515.8302, 5.1590>>		vecCamEndRot = <<9.9778, -0.0009, -96.2808>>		vecCamEndFov = 56.5538 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B	vecCamEndPos = <<-1173.4980, -1515.8302, 5.1590>>		vecCamEndRot = <<9.9778, -0.0009, -96.2808>>		vecCamEndFov = 56.5538 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B2	RETURN SETUP_PLAYER_CAMERA_FOR_SCENE(PR_SCENE_T_FLOYDSPOON_B, vecCamEndPos, vecCamEndRot, vecCamEndFov) BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A2	RETURN SETUP_PLAYER_CAMERA_FOR_SCENE(PR_SCENE_T_FLOYDSPOON_A, vecCamEndPos, vecCamEndRot, vecCamEndFov) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A	vecCamEndPos = <<-1173.4980, -1515.8302, 5.1590>>		vecCamEndRot = <<9.9778, -0.0009, -96.2808>>		vecCamEndFov = 56.5538 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0	RETURN SETUP_PLAYER_CAMERA_FOR_SCENE(PR_SCENE_T_FLOYDCRYING_E3, vecCamEndPos, vecCamEndRot, vecCamEndFov) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E1	RETURN SETUP_PLAYER_CAMERA_FOR_SCENE(PR_SCENE_T_FLOYDCRYING_E3, vecCamEndPos, vecCamEndRot, vecCamEndFov) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E2	RETURN SETUP_PLAYER_CAMERA_FOR_SCENE(PR_SCENE_T_FLOYDCRYING_E3, vecCamEndPos, vecCamEndRot, vecCamEndFov) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E3	vecCamEndPos = <<-1173.4980, -1515.8302, 5.1590>>		vecCamEndRot = <<9.9778, -0.0009, -96.2808>>		vecCamEndFov = 56.5538 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_BEAR		vecCamEndPos = <<-1134.3420, -1497.2400, 4.7530>>		vecCamEndRot = <<4.7530, 0.0000, 151.5690>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_DOLL		vecCamEndPos = <<-1134.3420, -1497.2400, 4.7530>>		vecCamEndRot = <<4.7530, 0.0000, 151.5690>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE	vecCamEndPos = <<-1134.3420, -1497.2400, 4.7530>>		vecCamEndRot = <<4.7530, 0.0000, 151.5690>>			vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T6_SMOKECRYSTAL	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_T6_EVENING		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T6_METHLAB		vecCamEndPos = <<1395.4946, 3601.7251, 39.5656>>		vecCamEndRot = <<-6.5114, -0.0000, 79.7777>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK	//v_PRT6_METHLAB
		CASE PR_SCENE_T6_HUNTING1		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T6_HUNTING2		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T6_HUNTING3		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T6_TRAF_AIR		vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
//		CASE PR_SCENE_T6_DISPOSEBODY_A	vecCamEndPos = << 780.4767, -2612.5190, 57.2638 >>		vecCamEndRot = << -35.8332, 0.0000, -98.6916 >>		RETURN TRUE BREAK
		CASE PR_SCENE_T6_DIGGING		vecCamEndPos = <<2022.2897, 3405.7341, 68.8313>>		vecCamEndRot = <<-84.7870, 0.0001, 143.9959>>		vecCamEndFov = 45.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT	vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PIER			vecCamEndPos = << -272.2570, 6628.2559, 8.3634 >>		vecCamEndRot = << -13.8262, -0.0000, 79.1812 >>		RETURN TRUE BREAK
		CASE PR_SCENE_T6_LAKE			vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		CASE PR_SCENE_T_FLYING_PLANE	vecCamEndPos = <<0,0,0>>	vecCamEndRot = <<0,0,0>>	RETURN FALSE BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT			vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		CASE PR_SCENE_T_JERKOFF			vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		#ENDIF		
		CASE PR_SCENE_T_HEADINSINK		vecCamEndPos = <<1976.5756, 3803.5605, 34.6805>>		vecCamEndRot = <<-7.8325, 0.0000, 11.5471>>		vecCamEndFov = 50.0000 RETURN TRUE BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sUnknown
			sUnknown = "unknown eScene for player camera setup: "
			sUnknown += Get_String_From_Ped_Request_Scene_Enum(eScene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sUnknown)PRINTNL()
			SCRIPT_ASSERT(sUnknown)
			#ENDIF
			
			VECTOR vUnknownCoords
			FLOAT fUnknownHead
			TEXT_LABEL_31 tUnknownRoom
			GET_PLAYER_PED_POSITION_FOR_SCENE(eScene, vUnknownCoords, fUnknownHead, tUnknownRoom)

			vecCamEndPos = vUnknownCoords + <<0,-5,0>>
			vecCamEndRot = <<0,0,-fUnknownHead>>

			RETURN TRUE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for player camera setup: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	VECTOR vCoords
	FLOAT fHead
	TEXT_LABEL_31 tRoom
	GET_PLAYER_PED_POSITION_FOR_SCENE(eScene, vCoords, fHead, tRoom)

	vecCamEndPos = vCoords + <<0,-5,0>>
	vecCamEndRot = <<0,0,0>>
	
	RETURN FALSE
ENDFUNC








FUNC BOOL GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tPlayerSceneSyncLoopCam)
	SWITCH eScene
		CASE PR_SCENE_M2_BEDROOM		tPlayerSceneSyncLoopCam = "BED_LOOP_Cam" RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b	tPlayerSceneSyncLoopCam = "BED_LOOP_Cam" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_FUNERAL		tPlayerSceneSyncLoopCam = "TRVS_IG_11_LOOP_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M4_WATCHINGTV		tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_RESTAURANT		tPlayerSceneSyncLoopCam = "001510_02_GC_MICS3_IG_1_BASE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_BAR			tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_CHASE			tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		
//		CASE PR_SCENE_F_TAUNT			tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1		tPlayerSceneSyncLoopCam = "GANG_TAUNT_LOOP_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2		tPlayerSceneSyncLoopCam = "FRAS_IG_6_P2_LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3		tPlayerSceneSyncLoopCam = "GANG_TAUNT_WITH_LAMAR_LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5		tPlayerSceneSyncLoopCam = "FRAS_IG_6_P5_LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT	tPlayerSceneSyncLoopCam = "FRAS_IG_10_P3_LOOP_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_S_AGENCY_2A_a	tPlayerSceneSyncLoopCam = "Franklin_call_Michael_IDLE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_b	tPlayerSceneSyncLoopCam = "Franklin_call_Michael_IDLE_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_FAKEYOGA		tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b	tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M2_LUNCH_a		tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_M7_LUNCH_b		tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M_VWOODPARK_a		tPlayerSceneSyncLoopCam = "PARKBENCH_SMOKE_RANGER_LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_VWOODPARK_a, tPlayerSceneSyncLoopCam) BREAK
				
		CASE PR_SCENE_F_CLUB			tPlayerSceneSyncLoopCam = "base_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT	tPlayerSceneSyncLoopCam = "002057_03_TRVS_27_FLUSHES_FOOT_IDLE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	tPlayerSceneSyncLoopCam = "001220_03_GC_TRVS_IG_7_BASE_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_GYM				tPlayerSceneSyncLoopCam = "001942_02_GC_FRAS_IG_5_BASE_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_FLOYDSPOON_A	tPlayerSceneSyncLoopCam = "Bed_Sleep_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B	tPlayerSceneSyncLoopCam = "Bed_Sleep_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B2	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_T_FLOYDSPOON_B, tPlayerSceneSyncLoopCam) BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A2	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_T_FLOYDSPOON_A, tPlayerSceneSyncLoopCam) BREAK
		
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1	tPlayerSceneSyncLoopCam = "IG_16_BASE_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3	tPlayerSceneSyncLoopCam = "IG_17_BASE_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F1_IRONING		tPlayerSceneSyncLoopCam = "001947_01_GC_FRAS_V2_IG_6_BASE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR		tPlayerSceneSyncLoopCam = "001946_01_GC_FRAS_V2_IG_5_BASE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F0_CLEANCAR		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F1_CLEANCAR, tPlayerSceneSyncLoopCam) BREAK
		
		CASE PR_SCENE_T_UNDERPIER		tPlayerSceneSyncLoopCam = "LOOP_Cam" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_LINGERIE		tPlayerSceneSyncLoopCam = "trev_exit_lingerie_shop_idle_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_BIKINGJIMMY	tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMGRAFF		tPlayerSceneSyncLoopCam = "Lamar_tagging_wall_LOOP_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M2_CARSLEEP_a		tPlayerSceneSyncLoopCam = "BASE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M2_CARSLEEP_a, tPlayerSceneSyncLoopCam) BREAK
		
		CASE PR_SCENE_M6_CARSLEEP		tPlayerSceneSyncLoopCam = "BASE_PREMIER_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M_CANAL_a			tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_b			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncLoopCam) BREAK
		CASE PR_SCENE_M_CANAL_c			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncLoopCam) BREAK
		CASE PR_SCENE_M_PIER_b			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncLoopCam) BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncLoopCam) BREAK
		
		
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_M6_SUNBATHING		tPlayerSceneSyncLoopCam = "Switch_ON_CLUBCHAIR_BASE_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_HOOKERS		tPlayerSceneSyncLoopCam = "BASE_CAM" RETURN TRUE BREAK
		
//		CASE PR_SCENE_F0_BIKE			tPlayerSceneSyncLoopCam = "BASE_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_F1_BIKE			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncLoopCam) BREAK
//		CASE PR_SCENE_F_BIKE_c			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncLoopCam) BREAK
//		CASE PR_SCENE_F_BIKE_d			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncLoopCam) BREAK
		
		CASE PR_SCENE_T_NAKED_ISLAND	tPlayerSceneSyncLoopCam = "LOOP_CAM" RETURN TRUE BREAK
		
	ENDSWITCH
	
	tPlayerSceneSyncLoopCam = "null"
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tPlayerSceneSyncExitCam, TEXT_LABEL_63 &tPlayerSceneEstabShotSuffix)
	tPlayerSceneEstabShotSuffix = ""
	SWITCH eScene
		
		CASE PR_SCENE_M_S_FAMILY4		tPlayerSceneSyncExitCam = "" tPlayerSceneEstabShotSuffix = "_2" RETURN FALSE BREAK
		
		CASE PR_SCENE_M2_BEDROOM		tPlayerSceneSyncExitCam = "BED_EXIT_Cam" RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b	tPlayerSceneSyncExitCam = "BED_EXIT_Cam" RETURN TRUE BREAK
		
		CASE PR_SCENE_M6_SUNBATHING		tPlayerSceneSyncExitCam = "Switch_ON_CLUBCHAIR_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M_VWOODPARK_a		tPlayerSceneSyncExitCam = "PARKBENCH_SMOKE_RANGER_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_VWOODPARK_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M_COFFEE_a		tPlayerSceneSyncExitCam = "Cafe_Exit_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_COFFEE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M_COFFEE_c		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_COFFEE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M4_CINEMA			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_COFFEE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M7_COFFEE			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_COFFEE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M2_KIDS_TV		tPlayerSceneSyncExitCam = "EXIT_Cam" RETURN TRUE BREAK
		
		CASE PR_SCENE_M_POOLSIDE_a		tPlayerSceneSyncExitCam = "SunLounger_GetUp_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_POOLSIDE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F1_POOLSIDE_a		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_POOLSIDE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F1_POOLSIDE_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_POOLSIDE_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK

		CASE PR_SCENE_M_PIER_a			tPlayerSceneSyncExitCam = "pier_lean_smoke_outro_CAM" RETURN TRUE BREAK

//		CASE PR_SCENE_M_BAR_a			tPlayerSceneSyncExitCam = "Drunk_Exit_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_b			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_BAR_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M6_LIQUORSTORE	tPlayerSceneSyncExitCam = "Drunk_Exit_CAM" RETURN TRUE BREAK	//RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_BAR_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_F0_GARBAGE		tPlayerSceneSyncExitCam = "Garbage_Toss_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_GARBAGE		tPlayerSceneSyncExitCam = "Garbage_Toss_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_THROW_CUP		tPlayerSceneSyncExitCam = "THROW_CUP_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND	tPlayerSceneSyncExitCam = "HIT_CUP_HAND_EXIT_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_F_GYM				tPlayerSceneSyncExitCam = "001942_02_GC_FRAS_IG_5_EXIT_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_F_CS_WIPEHANDS	tPlayerSceneSyncExitCam = "WipeHands_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT	tPlayerSceneSyncExitCam = "WipeRight_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE	tPlayerSceneSyncExitCam = "CheckShoe_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_STRIPCLUB_out	tPlayerSceneSyncExitCam = "trev_leave_stripclub_outro_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_BEAR		tPlayerSceneSyncExitCam = "BEAR_IN_FLOYDS_FACE_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_DOLL		tPlayerSceneSyncExitCam = "BEAR_FLOYDS_FACE_SMELL_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_CLUB			tPlayerSceneSyncExitCam = "switch_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CR_FUNERAL		tPlayerSceneSyncExitCam = "TRVS_IG_11_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_SMOKEMETH		tPlayerSceneSyncExitCam = "TREV_SMOKING_METH_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_Ta_RC_MRSP2		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_SMOKEMETH, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_SCARETRAMP		tPlayerSceneSyncExitCam = "TREV_SCARES_TRAMP_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_DRUNKHOWLING	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_LINGERIE		tPlayerSceneSyncExitCam = "trev_exit_lingerie_shop_outro_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	tPlayerSceneSyncExitCam = "THROW_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_YELLATDOORMAN	tPlayerSceneSyncExitCam = "001430_01_TRVS_21_YELLS_AT_DOORMAN_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE	tPlayerSceneSyncExitCam = "001218_03_TRVS_23_RUDE_AT_CAFE_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CR_RAND_TEMPLE	tPlayerSceneSyncExitCam = "TAI_CHI_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_UNDERPIER		tPlayerSceneSyncExitCam = "EXIT_Cam" RETURN TRUE BREAK
		
		CASE PR_SCENE_M2_MARINA			tPlayerSceneSyncExitCam = "EXIT_Cam" RETURN TRUE BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE	tPlayerSceneSyncExitCam = "ARGUE_WITH_AMANDA_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT	tPlayerSceneSyncExitCam = "Tanisha_Argue_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_NEWHOUSE		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_F_TAUNT			tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1		tPlayerSceneSyncExitCam = "GANG_TAUNT_EXIT_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2		tPlayerSceneSyncExitCam = "FRAS_IG_6_P2_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3		tPlayerSceneSyncExitCam = "GANG_TAUNT_WITH_LAMAR_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5		tPlayerSceneSyncExitCam = "FRAS_IG_6_P5_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT	tPlayerSceneSyncExitCam = "FRAS_IG_10_P3_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_S_AGENCY_2A_a	tPlayerSceneSyncExitCam = "Franklin_call_Michael_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_b	tPlayerSceneSyncExitCam = "Franklin_call_Michael_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_KUSH_DOC_a		tPlayerSceneSyncExitCam = "exit_dispensary_outro_CAM" RETURN TRUE BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT			tPlayerSceneSyncExitCam = "TREV_ON_TOILET_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_JERKOFF			tPlayerSceneSyncExitCam = "TREV_JERKING_OFF_EXIT_CAM" RETURN TRUE BREAK
		#ENDIF
		CASE PR_SCENE_F_MD_KUSH_DOC		tPlayerSceneSyncExitCam = "002110_04_MAGD_3_WEED_EXCHANGE_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_BYESOLOMON_a	tPlayerSceneSyncExitCam = "001400_01_MICS3_5_BYE_TO_SOLOMAN_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_FAKEYOGA		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_READSCRIPT		tPlayerSceneSyncExitCam = "001404_01_MICS3_16_READS_SCRIPT_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITBARBER		tPlayerSceneSyncExitCam = "001406_01_MICS3_7_EXITS_BARBER_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP	tPlayerSceneSyncExitCam = "001405_01_MICS3_8_EXITS_FANCYSHOP_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M2_LUNCH_a		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_M7_LUNCH_b		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_KONEIGHBOUR		tPlayerSceneSyncExitCam = "001500_03_TRVS_19_KO_NEIGHBOUR_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	tPlayerSceneSyncExitCam = "001220_03_GC_TRVS_IG_7_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN	tPlayerSceneSyncExitCam = "001370_02_TRVS_8_GUITAR_BEATDOWN_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3	tPlayerSceneSyncExitCam = "002113_02_FRAS_15_STRIPCLUB_EXIT_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1	tPlayerSceneSyncExitCam = "IG_16_EXIT_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3	tPlayerSceneSyncExitCam = "IG_17_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_FLOYDPINEAPPLE	tPlayerSceneSyncExitCam = "pineapple_exit_cam" RETURN TRUE BREAK
		
//		CASE PR_SCENE_F_LAMGRAFF		tPlayerSceneSyncExitCam = "Lamar_tagging_wall_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T6_DIGGING		tPlayerSceneSyncExitCam = "001433_01_TRVS_26_DIGGING_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS	tPlayerSceneSyncExitCam = "TREV_ANNOYS_SUNBATHERS_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		tPlayerSceneSyncExitCam = "001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR	tPlayerSceneSyncExitCam = "000606_02_MICS1_5_AMANDA_EXITS_CAR_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M6_RONBORING		tPlayerSceneSyncExitCam = "RONEX_IG5_P2_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_OPENDOORFORAMA	tPlayerSceneSyncExitCam = "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY	tPlayerSceneSyncExitCam = "001839_02_MICS3_20_DROPPING_OFF_JMY_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR	tPlayerSceneSyncExitCam = "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_CAM" RETURN TRUE BREAK
//		CASE PR_SCENE_M_HOOKERCAR		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M7_TRACEYEXITSCAR, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M_HOOKERMOTEL		tPlayerSceneSyncExitCam = "Im_A_Married_Man_CAM" RETURN TRUE BREAK

//		CASE PR_SCENE_M4_DOORSTUMBLE	tPlayerSceneSyncExitCam = "000610_03_MICS2_4_STUMBLES_THROUGH_DOORS_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_REJECTENTRY	tPlayerSceneSyncExitCam = "001396_01_MICS3_6_REJECTED_ENTRY_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO	tPlayerSceneSyncExitCam = "001387_03_MICS3_2_BAR_EMPLOYEE_CONVO_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_HOOKERS		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_TALKTOGUARD	tPlayerSceneSyncExitCam = "001393_02_MICS3_3_TALKS_TO_GUARD_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOT_JIMMY		tPlayerSceneSyncExitCam = "001513_03_GC_MICS3_IG_4_ON_SET_W_JMY_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_GAMING	tPlayerSceneSyncExitCam = "001518_02_MICS3_11_GAMING_W_JMY_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_M7_WIFETENNIS		tPlayerSceneSyncExitCam = "001833_01_MICS3_18_AMA_TENNIS_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_ROUNDTABLE		tPlayerSceneSyncExitCam = "AROUND_THE_TABLE_SELFISH_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F0_PLAYCHOP		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F1_PLAYCHOP, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F1_PLAYCHOP		tPlayerSceneSyncExitCam = "001916_01_FRAS_V2_9_PLAYS_W_DOG_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F0_WALKCHOP		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F1_PLAYCHOP, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F_WALKCHOP_a		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F1_PLAYCHOP, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F_WALKCHOP_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F1_PLAYCHOP, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CR_DUMPSTER		tPlayerSceneSyncExitCam = "002002_01_TRVS_14_DUMPSTER_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANINGAPT	tPlayerSceneSyncExitCam = "001918_01_FRAS_V2_1_CLEANING_APT_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_F1_WATCHINGTV		tPlayerSceneSyncExitCam = "001915_01_FRAS_V2_8_WATCHING_TV_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F1_SNACKING		tPlayerSceneSyncExitCam = "001922_01_FRAS_V2_3_SNACKING_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_4" RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONLAPTOP		tPlayerSceneSyncExitCam = "001927_01_FRAS_V2_4_ON_LAPTOP_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_F1_IRONING		tPlayerSceneSyncExitCam = "001947_01_GC_FRAS_V2_IG_6_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_4" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_NAKED_BRIDGE	tPlayerSceneSyncExitCam = "002055_01_TRVS_17_NAKED_ON_BRIDGE_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT	tPlayerSceneSyncExitCam = "002057_03_TRVS_27_FLUSHES_FOOT_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_BYETAXI		tPlayerSceneSyncExitCam = "001938_01_FRAS_V2_7_BYE_TAXI_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR		tPlayerSceneSyncExitCam = "001946_01_GC_FRAS_V2_IG_5_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F0_CLEANCAR		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F1_CLEANCAR, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M7_GETSREADY		tPlayerSceneSyncExitCam = "001520_02_MICS3_14_GETS_READY_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M7_RESTAURANT		tPlayerSceneSyncExitCam = "001510_02_GC_MICS3_IG_1_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M4_WAKEUPSCREAM	tPlayerSceneSyncExitCam = "001671_02_MICS2_1_WAKES_UP_SCREAMING_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED	tPlayerSceneSyncExitCam = "001672_02_MICS2_1_WAKES_UP_SCARED_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M4_WATCHINGTV		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M6_HOUSEBED		tPlayerSceneSyncExitCam = "M_GetOut_countryside_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M4_WASHFACE		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_F1_ONCELL			tPlayerSceneSyncExitCam = "001914_01_FRAS_V2_2_ON_CELL_EXIT_CAM" tPlayerSceneEstabShotSuffix = "_3" RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_TV		tPlayerSceneSyncExitCam = "001520_02_MICS3_14_TV_W_KIDS_EXIT_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_M7_BIKINGJIMMY	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M7_LOUNGECHAIRS	tPlayerSceneSyncExitCam = "001523_01_MICS3_9_LOUNGE_CHAIRS_EXIT_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_T_FLOYDSPOON_A	tPlayerSceneSyncExitCam = "Bed_GetUp_1_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B	tPlayerSceneSyncExitCam = "Bed_GetUp_2_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B2	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_FLOYDSPOON_B, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A2	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_FLOYDSPOON_A, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_T_FLOYDCRYING_A	tPlayerSceneSyncExitCam = "Console_Wasnt_Fun_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_FLOYDCRYING_E3, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E1	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_FLOYDCRYING_E3, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E2	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_FLOYDCRYING_E3, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E3	tPlayerSceneSyncExitCam = "Console_Get_Along_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_SC_MOCKLAPDANCE	tPlayerSceneSyncExitCam = "001443_01_TRVS_28_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_BAR			tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_CHASE		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_ESCORTED_OUT	tPlayerSceneSyncExitCam = "001215_02_TRVS_12_ESCORTED_OUT_EXIT_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_PUKEINTOFOUNT	tPlayerSceneSyncExitCam = "TREV_FOUNTAIN_PUKE_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PARK_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_PUKEINTOFOUNT, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
//		CASE PR_SCENE_T_CN_PARK_c		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_PUKEINTOFOUNT, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_T_HEADINSINK		tPlayerSceneSyncExitCam = "TREV_SINK_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_a			tPlayerSceneSyncExitCam = "001209_01_TRVS_3_AT_THE_DOCKS_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_b			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_DOCKS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_DOCKS_c			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_DOCKS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_T_DOCKS_d			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_T_DOCKS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_F0_SH_ASLEEP		tPlayerSceneSyncExitCam = "Sleep_GetUp_RubEyes_CAM" tPlayerSceneEstabShotSuffix = "_2" RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_ASLEEP		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_SH_ASLEEP, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F1_NAPPING		tPlayerSceneSyncExitCam = "002333_01_FRAS_V2_10_NAPPING_EXIT_CAM" tPlayerSceneEstabShotSuffix = "" RETURN TRUE BREAK
		CASE PR_SCENE_F1_GETTINGREADY	tPlayerSceneSyncExitCam = "002334_02_FRAS_V2_11_GETTING_DRESSED_EXIT_CAM" tPlayerSceneEstabShotSuffix = "" RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_READING		tPlayerSceneSyncExitCam = "Bed_Reading_GetUp_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_READING		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_SH_READING, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a	tPlayerSceneSyncExitCam = "PressUps_OUT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_b	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_SH_PUSHUP_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F1_SH_PUSHUP		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_SH_PUSHUP_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M2_CARSLEEP_a		tPlayerSceneSyncExitCam = "SLEEP_IN_CAR_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M2_CARSLEEP_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M6_CARSLEEP		tPlayerSceneSyncExitCam = "SLEEP_IN_CAR_PREMIER_CAM" RETURN TRUE BREAK
		
		CASE PR_SCENE_M_CANAL_a			tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_b			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M_CANAL_c			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M_PIER_b			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_CANAL_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_M4_EXITRESTAURANT	tPlayerSceneSyncExitCam		= "MIC_EXIT_RESTAURANT_EXIT_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_F_BAR_a_01
		CASE PR_SCENE_F_BAR_b_01
		CASE PR_SCENE_F_BAR_e_01
			tPlayerSceneSyncExitCam = "SWITCH_01_CAM" RETURN TRUE BREAK
		BREAK
		CASE PR_SCENE_F_BAR_c_02
		CASE PR_SCENE_F_BAR_d_02
			tPlayerSceneSyncExitCam = "SWITCH_02_CAM" RETURN TRUE BREAK
		BREAK

		
		CASE PR_SCENE_M2_PHARMACY		tPlayerSceneSyncExitCam		= "MICS1_IG_11_EXIT_CAM" RETURN TRUE BREAK


		CASE PR_SCENE_M_PARKEDHILLS_a	tPlayerSceneSyncExitCam		= "SITTING_ON_CAR_BONNET_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M4_PARKEDBEACH	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_Ma_RURAL1 		RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a	tPlayerSceneSyncExitCam		= "SITTING_ON_CAR_PREMIERE_EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e	RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_M6_PARKEDHILLS_a, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_T_CR_ALLEYDRUNK	tPlayerSceneSyncExitCam		= "TREV_SLOUCHED_GET_UP_EXIT_CAM" RETURN TRUE BREAK

		CASE PR_SCENE_F0_BIKE			tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_F1_BIKE			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F_BIKE_c			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		CASE PR_SCENE_F_BIKE_d			RETURN GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(PR_SCENE_F0_BIKE, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix) BREAK
		
		CASE PR_SCENE_T_GARBAGE_FOOD	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_THROW_FOOD		tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_ISLAND	tPlayerSceneSyncExitCam = "EXIT_CAM" RETURN TRUE BREAK
		
	ENDSWITCH
	
	tPlayerSceneSyncExitCam = "null"
	RETURN FALSE
ENDFUNC




FUNC BOOL SETUP_PROCEDURAL_CAMERA_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		TEXT_LABEL_23 &tPlayerSceneProceduralCam)
	SWITCH eScene

		CASE PR_SCENE_FTa_FRANKLIN1a	tPlayerSceneProceduralCam		= "v_PRFTa_FRANKLIN1a" RETURN TRUE BREAK
	//	CASE PR_SCENE_FTa_FRANKLIN1b	tPlayerSceneProceduralCam		= "v_PRFTa_FRANKLIN1b" RETURN TRUE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1c	tPlayerSceneProceduralCam		= "v_PRFTa_FRANKLIN1c" RETURN TRUE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1d	tPlayerSceneProceduralCam		= "v_PRFTa_FRANKLIN1d" RETURN TRUE BREAK
		CASE PR_SCENE_FTa_FRANKLIN1e	tPlayerSceneProceduralCam		= "v_PRFTa_FRANKLIN1e" RETURN TRUE BREAK
		
		CASE PR_SCENE_M2_SAVEHOUSE1_b	tPlayerSceneProceduralCam		= "v_PRM2_SAVEHOUSE1_b" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_FIGHTCASINO		tPlayerSceneProceduralCam		= "v_PRT_FIGHTCASINO" RETURN TRUE BREAK
		CASE PR_SCENE_T6_SMOKECRYSTAL	tPlayerSceneProceduralCam		= "v_PRT6_SMOKECRYSTAL" RETURN TRUE BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP		tPlayerSceneProceduralCam		= "v_PRT6_BLOWSHITUP" RETURN TRUE BREAK
//		CASE PR_SCENE_T6_DISPOSEBODY	tPlayerSceneProceduralCam		= "v_PRT6_DISPOSEBODY" RETURN TRUE BREAK
		CASE PR_SCENE_T6_METHLAB		tPlayerSceneProceduralCam		= "v_PRT6_METHLAB" RETURN TRUE BREAK
		
		CASE PR_SCENE_M2_DROPOFFDAU_a	tPlayerSceneProceduralCam		= "v_PRM2_DROPOFFDAU_a" RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a	tPlayerSceneProceduralCam		= "v_PRM2_DROPOFFSON_a" RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b	tPlayerSceneProceduralCam		= "v_PRM2_DROPOFFSON_b" RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b	tPlayerSceneProceduralCam		= "v_PRM2_DROPOFFDAU_b" RETURN TRUE BREAK
		
		CASE PR_SCENE_M6_DRINKINGBEER	tPlayerSceneProceduralCam		= "v_PRM6_DRINKINGBEER" RETURN TRUE BREAK
//		CASE PR_SCENE_M6_HOUSEBED		tPlayerSceneProceduralCam		= "v_PRM6_HOUSEBED" RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSETV_a		tPlayerSceneProceduralCam		= "v_PRM6_HOUSETV_a" RETURN TRUE BREAK
		CASE PR_SCENE_M6_MORNING_a		tPlayerSceneProceduralCam		= "v_PRM6_MORNING_a" RETURN TRUE BREAK
		CASE PR_SCENE_M6_DEPRESSED		tPlayerSceneProceduralCam		= "v_PRM6_DEPRESSED" RETURN TRUE BREAK
		CASE PR_SCENE_M6_ONPHONE		tPlayerSceneProceduralCam		= "v_PRM6_ONPHONE" RETURN TRUE BREAK
		CASE PR_SCENE_M4_LUNCH_b		tPlayerSceneProceduralCam		= "v_PRM4_LUNCH_b" RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a	tPlayerSceneProceduralCam		= "v_PRM2_SAVEHOUSE1_a" RETURN TRUE BREAK
		CASE PR_SCENE_M_S_FAMILY4		tPlayerSceneProceduralCam		= "v_PRM_S_FAMILY4" RETURN TRUE BREAK
//		CASE PR_SCENE_M2_KIDS_TV		tPlayerSceneProceduralCam		= "v_PRM2_KIDS_TV" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_FIGHTYAUCLUB_b	tPlayerSceneProceduralCam		= "v_PRT_FIGHTYAUCLUB_b" RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_a		tPlayerSceneProceduralCam		= "v_PRT_FIGHTBAR_a" RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_b		tPlayerSceneProceduralCam		= "v_PRT_FIGHTBAR_b" RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_c		tPlayerSceneProceduralCam		= "v_PRT_FIGHTBAR_c" RETURN TRUE BREAK
		CASE PR_SCENE_T6_TRAF_AIR		tPlayerSceneProceduralCam		= "v_PRT6_TRAF_AIR" RETURN TRUE BREAK
		CASE PR_SCENE_T6_LAKE			tPlayerSceneProceduralCam		= "v_PRT6_LAKE" RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING2		tPlayerSceneProceduralCam		= "v_PRT6_HUNTING2" RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING1		tPlayerSceneProceduralCam		= "v_PRT6_HUNTING1" RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING3		tPlayerSceneProceduralCam		= "v_PRT6_HUNTING3" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CN_PIER			tPlayerSceneProceduralCam		= "v_PRT_CN_PIER" RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRAIN	tPlayerSceneProceduralCam		= "v_PRT_CN_WAKETRAIN" RETURN TRUE BREAK
			
		CASE PR_SCENE_M_BENCHCALL_a		tPlayerSceneProceduralCam		= "v_PRM_BENCHCALL_a" RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_b		tPlayerSceneProceduralCam		= "v_PRM_BENCHCALL_b" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CN_WAKETRASH_b	tPlayerSceneProceduralCam		= "v_PRT_CN_WAKETRASH_b" RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_ALLEYDRUNK	tPlayerSceneProceduralCam		= "v_PRT_SC_ALLEYDRUNK" RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_GARDEN	tPlayerSceneProceduralCam		= "v_PRT_NAKED_GARDEN" RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CN_CHATEAU_b	tPlayerSceneProceduralCam		= "v_PRT_CN_CHATEAU_b" RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_c	tPlayerSceneProceduralCam		= "v_PRT_CN_CHATEAU_c" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHATEAU_d	tPlayerSceneProceduralCam		= "v_PRT_CR_CHATEAU_d" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEBEACH	tPlayerSceneProceduralCam		= "v_PRT_CR_WAKEBEACH" RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEBARN		tPlayerSceneProceduralCam		= "v_PRT_CN_WAKEBARN" RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEROOFTOP	tPlayerSceneProceduralCam		= "v_PRT_CR_WAKEROOFTOP" RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN	tPlayerSceneProceduralCam		= "v_PRT_CN_WAKEMOUNTAIN" RETURN TRUE BREAK
		
		CASE PR_SCENE_F_KUSH_DOC_b		tPlayerSceneProceduralCam		= "v_PRF_KUSH_DOC_b" RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_c		tPlayerSceneProceduralCam		= "v_PRF_KUSH_DOC_c" RETURN TRUE BREAK
		
	ENDSWITCH
	
	tPlayerSceneProceduralCam = "null"
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		FLOAT &gpCamPitch, FLOAT &gameplayCamSmoothRate,
		FLOAT &gpCamHead)
	
	CONST_FLOAT fNULL	999.0
	
	gameplayCamSmoothRate	= 1.0
	
	IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
		
		SWITCH eScene
			CASE PR_SCENE_F0_BIKE			//#2042646
			CASE PR_SCENE_F1_BIKE
				gpCamHead = -0.0922
				gpCamPitch = -32.8439
				
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_FLOYDSAVEHOUSE	//#2042258
				gpCamHead = -0.0920
				gpCamPitch = -3.5553
				
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_FLOYDCRYING_A	//#2045259
				gpCamHead = -0.0916
				gpCamPitch = -1.4212
				
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_FLOYDSPOON_B	//#2045349
			CASE PR_SCENE_T_FLOYDSPOON_B2
				gpCamHead = -0.0944
				gpCamPitch = -4.2087
				
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_FLOYDSPOON_A	//#2045351
			CASE PR_SCENE_T_FLOYDSPOON_A2
				gpCamHead = -0.0920
				gpCamPitch = -2.1053
				
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_CN_PIER			//#2045394
				gpCamHead = 0.0000
				gpCamPitch = -7.6775
				
				RETURN TRUE
			BREAK
		ENDSWITCH
		
		gpCamHead = 0.0
		gpCamPitch = 0.0
		RETURN TRUE
	ENDIF
	
	
	SWITCH eScene
		CASE PR_SCENE_DEAD			FALLTHRU
		CASE PR_SCENE_HOSPITAL		FALLTHRU
		
		CASE PR_SCENE_M_OVERRIDE	FALLTHRU
		CASE PR_SCENE_F_OVERRIDE	FALLTHRU
		CASE PR_SCENE_T_OVERRIDE	FALLTHRU
		
		CASE PR_SCENE_M_DEFAULT		FALLTHRU
		CASE PR_SCENE_F_DEFAULT		FALLTHRU
		CASE PR_SCENE_T_DEFAULT		
			gpCamPitch = fNULL	gpCamHead = fNULL	RETURN FALSE
		BREAK
		
		CASE PR_SCENE_M6_LIQUORSTORE	gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK
		
//		CASE PR_SCENE_M2_SMOKINGGOLF	gpCamHead = 113.7165	gpCamPitch = -11.4 RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_a		gpCamHead = -325.2390	gpCamPitch = -9.9 RETURN TRUE BREAK
		
		CASE PR_SCENE_T_NAKED_ISLAND
			IF NOT g_bMagDemoActive
			gpCamHead = -60.0			gpCamPitch = 0.0 RETURN TRUE
			ENDIF
			BREAK
		
		CASE PR_SCENE_F_CS_WIPERIGHT	RETURN SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(PR_SCENE_F_CS_CHECKSHOE, gpCamPitch, gameplayCamSmoothRate, gpCamHead) BREAK
		
		
		CASE PR_SCENE_F_KUSH_DOC_a		gpCamHead = -9.0		gpCamPitch = 0.0 RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_BAR			gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK
		
		CASE PR_SCENE_T_DRUNKHOWLING	gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING	RETURN SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(PR_SCENE_T_DRUNKHOWLING, gpCamPitch, gameplayCamSmoothRate, gpCamHead) BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE	gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK
		
		CASE PR_SCENE_T_CR_ALLEYDRUNK	gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK

//		CASE PR_SCENE_M6_DEPRESSED		gpCamHead = 180.0		gpCamPitch = 0.0 RETURN TRUE BREAK
		
		CASE PR_SCENE_T_SMOKEMETH		gpCamHead = -0.0		gpCamPitch = -0.0 RETURN TRUE BREAK
		CASE PR_SCENE_Ta_RC_MRSP2		RETURN SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(PR_SCENE_T_SMOKEMETH, gpCamPitch, gameplayCamSmoothRate, gpCamHead) BREAK
		
		CASE PR_SCENE_F_MD_KUSH_DOC		gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK

		CASE PR_SCENE_M2_CARSLEEP_a		gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b		gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK
		CASE PR_SCENE_M6_CARSLEEP		gpCamHead = 0.0			gpCamPitch = 0.0 RETURN TRUE BREAK

		
	ENDSWITCH
	
	gpCamPitch = fNULL
	gpCamHead = fNULL
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HasQuitCamPhasePassed(PED_REQUEST_SCENE_ENUM eScene, CAMERA_INDEX &camIndex, FLOAT fPlayerSceneAnimOutTime,
		INT &iDrawSceneRot)
	
	IF NOT DOES_CAM_EXIST(camIndex)
		RETURN FALSE
	ENDIF
	IF NOT IS_CAM_RENDERING(camIndex)
		RETURN FALSE
	ENDIF
	
	FLOAT fEventPhase = -1
	SWITCH eScene
//		CASE PR_SCENE_T_SHIT			fEventPhase = 0.736					BREAK
		CASE PR_SCENE_T_HEADINSINK		fEventPhase = 0.856					BREAK
//		CASE PR_SCENE_F_MD_KUSH_DOC		fEventPhase = 0.780					BREAK
		
		CASE PR_SCENE_T6_DIGGING		fEventPhase = 0.999					BREAK
//		CASE PR_SCENE_T_CR_DUMPSTER		fEventPhase = 0.780					BREAK
		CASE PR_SCENE_T_ESCORTED_OUT	fEventPhase = 0.880					BREAK
		
		DEFAULT							fEventPhase = -1	RETURN FALSE	BREAK
	ENDSWITCH 
		
	IF (fEventPhase >= 0 AND fEventPhase <= 1)
		
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str  = ("quit cam phase [")
		str += GET_STRING_FROM_FLOAT(fEventPhase)
		str += ("]")
		#ENDIF
		
		IF fPlayerSceneAnimOutTime < fEventPhase
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			#ENDIF
			
			iDrawSceneRot++
		ELSE
			
			#IF IS_DEBUG_BUILD
			str += " break"
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			#ENDIF
			
			iDrawSceneRot++
			
			RETURN TRUE
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HasFirstPersonQuitCamPhasePassed(PED_REQUEST_SCENE_ENUM eScene, CAMERA_INDEX &camIndex, FLOAT fPlayerSceneAnimOutTime,
		INT &iDrawSceneRot)
	
	IF NOT DOES_CAM_EXIST(camIndex)
		RETURN FALSE
	ENDIF
	IF NOT IS_CAM_RENDERING(camIndex)
		RETURN FALSE
	ENDIF
	IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
		RETURN FALSE
	ENDIF
	
	FLOAT fEventPhase = -1
	SWITCH eScene
		CASE PR_SCENE_M_CANAL_a			//#2041075
		CASE PR_SCENE_M_CANAL_b
		CASE PR_SCENE_M_CANAL_c
		CASE PR_SCENE_M_PIER_b
		CASE PR_SCENE_M2_SMOKINGGOLF
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_M_COFFEE_a		//#2041078
		CASE PR_SCENE_M_COFFEE_b
		CASE PR_SCENE_M_COFFEE_c
		CASE PR_SCENE_M4_CINEMA
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_M_HOOKERMOTEL		//#2041083
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_a	//#2041088
		CASE PR_SCENE_M_PARKEDHILLS_b
		CASE PR_SCENE_M6_PARKEDHILLS_a
		CASE PR_SCENE_M6_PARKEDHILLS_b
		CASE PR_SCENE_M6_PARKEDHILLS_c
		CASE PR_SCENE_M6_PARKEDHILLS_d
		CASE PR_SCENE_M6_PARKEDHILLS_e
		CASE PR_SCENE_M4_PARKEDBEACH
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_M_PIER_a			//#2041092
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M_POOLSIDE_a		//#2041095
		CASE PR_SCENE_M_POOLSIDE_b
			fEventPhase = 0.55
		BREAK
		CASE PR_SCENE_M_VWOODPARK_a		//#2041128
		CASE PR_SCENE_M_VWOODPARK_b
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M2_KIDS_TV		//#2041156
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_M2_LUNCH_a		//#2041164
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_M2_MARINA			//#2041166
			fEventPhase = 0.65
		BREAK
		CASE PR_SCENE_M2_PHARMACY 		//#2041179
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b	//#2041188
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_M2_BEDROOM		//#2041200
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE	//#2041215
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_M2_CYCLING_a		//#2041238
		CASE PR_SCENE_M2_CYCLING_b
		CASE PR_SCENE_M2_CYCLING_c
		//	The camera isn't cutting into 1st person at all for me
			fEventPhase = -1	RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_COFFEE			//#2042319
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a	//#2042317
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b	//#2042316
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY	//#2042316
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M6_RONBORING		//#2042314
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M6_SUNBATHING		//#2042309
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M6_HOUSEBED		//#2042307
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	//#2042258
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT	//#2042249
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_M4_WATCHINGTV		//#2042247
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED	//#2042243
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M4_WAKEUPSCREAM	//#2042241
			fEventPhase = 0.75
		BREAK

		CASE PR_SCENE_M7_KIDS_TV		//#2042386
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING	//#2042384
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_M7_HOOKERS		//#2042377
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_M7_GETSREADY		//#2042374
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE		//#2042372
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA		//#2042366
			fEventPhase = 1.0
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP	//#2042362
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_M7_EXITBARBER		//#2042353
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO	//#2042351
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_M7_LOT_JIMMY		//#2042523
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS	//#2042531
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_M7_READSCRIPT		//#2042569
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_M7_REJECTENTRY	//#2042600
			fEventPhase = 1.0
		BREAK
		CASE PR_SCENE_M7_RESTAURANT		//#2042605
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS		//#2042629
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_F0_GARBAGE		//#2042643
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F0_BIKE			//#2042646
		CASE PR_SCENE_F1_BIKE
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F0_CLEANCAR		//#2042653
		CASE PR_SCENE_F1_CLEANCAR
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F0_PLAYCHOP		//#2042661
		CASE PR_SCENE_F1_PLAYCHOP
		CASE PR_SCENE_F0_WALKCHOP
		CASE PR_SCENE_F_WALKCHOP_a
		CASE PR_SCENE_F_WALKCHOP_b
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F1_GARBAGE		//#2042668
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F0_SH_ASLEEP		//#2042675
		CASE PR_SCENE_F1_SH_ASLEEP
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a	//#2042692
		CASE PR_SCENE_F0_SH_PUSHUP_b
		CASE PR_SCENE_F1_SH_PUSHUP
			fEventPhase = 0.6
		BREAK
		
		CASE PR_SCENE_F1_WATCHINGTV	//#2042781
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_F1_GETTINGREADY	//#2042758
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_F1_NAPPING		//#2042754
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_F1_POOLSIDE_a		//#2042751
		CASE PR_SCENE_F1_POOLSIDE_b
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F1_ONCELL			//#2042745
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP		//#2042744
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_F1_BYETAXI		//#2042741
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F1_IRONING		//#2042740
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_F1_CLEANINGAPT	//#2042736
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F1_SNACKING		//#2042728
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_F0_SH_READING		//#2042706
		CASE PR_SCENE_F1_SH_READING
			fEventPhase = 0.8
		BREAK
		
		
		CASE PR_SCENE_F0_TANISHAFIGHT	//#2045137
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3	//#2045131
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND	//#2045125
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F_THROW_CUP		//#2045123
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5		//#2045121
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3		//#2045117
			fEventPhase = 0.25
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1		//#2045112
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a		//#2045108
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_F_GYM				//#2045104
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT	//#2045101
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F_CS_WIPEHANDS	//#2045100
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE	//#2045099
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_F_CLUB			//#2045096
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F_BIKE_c			//#2045085
		CASE PR_SCENE_F_BIKE_d
			fEventPhase = 0.6
		BREAK
		CASE PR_SCENE_F_BAR_a_01		//#2045082
		CASE PR_SCENE_F_BAR_b_01
		CASE PR_SCENE_F_BAR_e_01
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC		//#2045073
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F1_NEWHOUSE		//#2045142
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_a	//#2045173
		CASE PR_SCENE_F_S_AGENCY_2A_b
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT	//#2068827
			fEventPhase = 0.55
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT			//#2045173
			fEventPhase = 0.65
		BREAK
		#ENDIF
		CASE PR_SCENE_T_DOCKS_a			//#2045240
		CASE PR_SCENE_T_DOCKS_b
		CASE PR_SCENE_T_DOCKS_c
		CASE PR_SCENE_T_DOCKS_d
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A	//#2045259
			fEventPhase = 0.5
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0	//#2045266
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR		//#2045292
			fEventPhase = 0.45
		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL		//#2045338
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B	//#2045349
		CASE PR_SCENE_T_FLOYDSPOON_B2
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A	//#2045351
		CASE PR_SCENE_T_FLOYDSPOON_A2
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE	//#2045374
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_HEADINSINK		//#2045378
			fEventPhase = 0.55
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_JERKOFF			//#2045380
			fEventPhase = 0.9
		BREAK
		#ENDIF
		CASE PR_SCENE_T_SMOKEMETH		//#2045381
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE	//#2045383
			fEventPhase = 1.0
		BREAK
		CASE PR_SCENE_T_SC_BAR			//#2045386
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_SC_CHASE		//#2045387
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING	//#2045389
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_STRIPCLUB_OUT	//#2045390
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T6_DIGGING		//#2045391
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT	//#2045393
			fEventPhase = 1.0
		BREAK
//		CASE PR_SCENE_T_CN_PIER			//#2045394
//			Just needs to have the gun aimed at the guy when in 1st person, thanks
//		BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK	//#2045641
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	//#2045644
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_T_NAKED_ISLAND	//#2045662
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT	//#2045672
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_CR_FUNERAL		//#2045676
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_DRUNKHOWLING	//#2045677
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS	//#2045683
			fEventPhase = 0.95
		BREAK
		CASE PR_SCENE_T_CR_LINGERIE		//#2045685
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT	//#2045690
		CASE PR_SCENE_T_CN_PARK_b
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE	//#2045706
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD	//#2045713
			fEventPhase = 0.7
		BREAK
		CASE PR_SCENE_T_THROW_FOOD		//#2045717
			fEventPhase = 0.75
		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	//#2045720
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN	//#2045723
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_UNDERPIER		//#2045726
			fEventPhase = 0.85
		BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE	//#2045729
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_CR_DUMPSTER		//#2045731
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		//#2045755
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_KONEIGHBOUR		//#2045760
			fEventPhase = 0.9
		BREAK
		CASE PR_SCENE_T_SCARETRAMP		//#2045763
			fEventPhase = 0.8
		BREAK
		CASE PR_SCENE_T_YELLATDOORMAN	//#2045768
			fEventPhase = 0.85
		BREAK
		
		
		DEFAULT
			TEXT_LABEL_63 tPlayerSceneSyncLoopCam
			IF GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(eScene, tPlayerSceneSyncLoopCam)
				CWARNINGLN(DEBUG_SWITCH, "HasFirstPersonQuitCamPhasePassed called on ", Get_String_From_Ped_Request_Scene_Enum(eScene), " without a set phase")
				fEventPhase = 0.95
			ELSE
				fEventPhase = -1	RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH 
		
	IF (fEventPhase >= 0 AND fEventPhase <= 1)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str  = ("1st person phase [")
		str += GET_STRING_FROM_FLOAT(fEventPhase)
		str += ("]")
		#ENDIF
		
		IF fPlayerSceneAnimOutTime < fEventPhase
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			#ENDIF
			
			iDrawSceneRot++
		ELSE
			
			#IF IS_DEBUG_BUILD
			str += " break"
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			#ENDIF
			
			iDrawSceneRot++
			
			RETURN TRUE
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_PUSH_IN_DIRECTION_MODIFIER_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, VECTOR &vDirectionMod)
	SWITCH eScene
		CASE PR_SCENE_M_VWOODPARK_a		vDirectionMod = <<20.0, 0.0, 0.0>> RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	vDirectionMod = <<20.0, 0.0, 0.0>> RETURN TRUE BREAK
		
		DEFAULT
			vDirectionMod = <<20.0, 0.0, 0.0>>
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

