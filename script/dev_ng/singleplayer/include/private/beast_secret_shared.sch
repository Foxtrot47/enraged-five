USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_physics.sch"

#IF FEATURE_SP_DLC_BEAST_SECRET

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════    The Beast Secret      ════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC Beast_Display_Onscreen_Text(FLOAT px, FLOAT py,STRING literalString, FLOAT scale = 1.0)
	SET_TEXT_SCALE(scale, scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(px, py, "STRING", literalString)
	SET_TEXT_SCALE(1,1)
ENDPROC
#ENDIF

PROC Do_White_Fade(INT iAlpha, INT iTime)
	CPRINTLN(DEBUG_HUNTING, "Requested white fade: Alpha:", iAlpha, " Time:", iTime, ".")
	FLOAT fAlpha = TO_FLOAT(iAlpha)

	IF iTime <= 0
		g_sWhiteFade.fDelta= 0
		g_sWhiteFade.fTargetAlpha = fAlpha
		g_sWhiteFade.fCurrentAlpha = fAlpha
		CPRINTLN(DEBUG_HUNTING, "Fade completed instantly.")
	ELSE
		FLOAT fDiff = fAlpha - g_sWhiteFade.fCurrentAlpha
		FLOAT fFrameCount = TO_FLOAT(iTime) / (GET_FRAME_TIME() * 1000)
		g_sWhiteFade.fDelta = fDiff / fFrameCount
		g_sWhiteFade.fTargetAlpha = fAlpha
		CPRINTLN(DEBUG_HUNTING, "Set up gradual fade: Diff:", fDiff, " Frames:", fFrameCount, " Delta: ", g_sWhiteFade.fDelta, ".")
	ENDIF
	
ENDPROC


PROC Update_White_Fade()
	//Check if we've not updated this frame already
	IF g_sWhiteFade.iLastFrameUpdate = GET_FRAME_COUNT()
		CDEBUG1LN(debug_hunting, "Already performed white fade update this frame, skipping")
		EXIT
	ENDIF
	g_sWhiteFade.iLastFrameUpdate = GET_FRAME_COUNT()
	
	//Early out if alphas are 0.
	IF g_sWhiteFade.fCurrentAlpha = 0.0
	AND g_sWhiteFade.fTargetAlpha = 0.0
		EXIT
	ENDIF

	//Interp between target and current.
	IF g_sWhiteFade.fCurrentAlpha != g_sWhiteFade.fTargetAlpha
		g_sWhiteFade.fCurrentAlpha += g_sWhiteFade.fDelta
		IF g_sWhiteFade.fDelta <= 0
			IF g_sWhiteFade.fCurrentAlpha < g_sWhiteFade.fTargetAlpha
				g_sWhiteFade.fCurrentAlpha = g_sWhiteFade.fTargetAlpha
			ENDIF
		ELSE
			IF g_sWhiteFade.fCurrentAlpha > g_sWhiteFade.fTargetAlpha
				g_sWhiteFade.fCurrentAlpha = g_sWhiteFade.fTargetAlpha
			ENDIF
		ENDIF
	ENDIF

	//Draw the fade.
	DRAW_RECT(0.5,0.5, 1, 1, 255, 255, 255, ROUND(g_sWhiteFade.fCurrentAlpha))
ENDPROC


FUNC BOOL Is_White_Faded_Out(BOOL orFading = FALSE)
	IF orFading 
		RETURN g_sWhiteFade.fCurrentAlpha > 0.5
	ELSE
		RETURN g_sWhiteFade.fCurrentAlpha >= 255
	ENDIF
ENDFUNC


FUNC MODEL_NAMES Get_Sasquatch_Model()
	RETURN INT_TO_ENUM(MODEL_NAMES, SASQUATCH_MODEL_BASE + SASQUATCH_MODEL_OFFSET) //HASH("IG_ORLEANS")
ENDFUNC


FUNC BOOL Is_Sasquatch_Model(MODEL_NAMES model)
	RETURN model = Get_Sasquatch_Model()
ENDFUNC


FUNC MODEL_NAMES Get_Beast_Model()
	RETURN INT_TO_ENUM(MODEL_NAMES, BEAST_MODEL_BASE + BEAST_MODEL_OFFSET) //HASH("MP_M_FREEMODE_01")
ENDFUNC
	
	
FUNC BOOL Is_Beast_Model(MODEL_NAMES model)
	RETURN model = Get_Beast_Model()
ENDFUNC


FUNC MODEL_NAMES Get_Rottweiler_Model()
	RETURN INT_TO_ENUM(MODEL_NAMES, ROTTWEILER_MODEL_BASE + ROTTWEILER_MODEL_OFFSET) //HASH("A_C_ROTTWEILER")
ENDFUNC


//Beast Hint Types
CONST_INT BHT_INVALID			-1
CONST_INT BHT_RABBIT			12
CONST_INT BHT_BOAR				109
CONST_INT BHT_DEER				43
CONST_INT BHT_CAR_WRECK			35
CONST_INT BHT_MOUNTAIN_BIKER	17
CONST_INT BHT_LION				28
CONST_INT BHT_HUNTER			49
CONST_INT BHT_HIKER				25
CONST_INT BHT_HIPPIE			14
CONST_INT BHT_REDNECK			86


FUNC MODEL_NAMES Get_Beast_Hint_Model_A(INT iHintType)
	SWITCH iHintType
		CASE BHT_RABBIT
			//HASH("A_C_RABBIT_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, RABBIT_MODEL_BASE + RABBIT_MODEL_OFFSET)
		CASE BHT_BOAR
			//HASH("A_C_BOAR")
			RETURN INT_TO_ENUM(MODEL_NAMES, BOAR_MODEL_BASE + BOAR_MODEL_OFFSET)
		CASE BHT_DEER
			//HASH("A_C_DEER")
			RETURN INT_TO_ENUM(MODEL_NAMES, DEER_MODEL_BASE + DEER_MODEL_OFFSET)
		CASE BHT_CAR_WRECK
			//HASH("EMPEROR2")
			RETURN INT_TO_ENUM(MODEL_NAMES, CAR_WRECK_MODEL_BASE + CAR_WRECK_MODEL_OFFSET)
		CASE BHT_MOUNTAIN_BIKER
			//HASH("SCORCHER")
			RETURN INT_TO_ENUM(MODEL_NAMES, MOUNTAIN_BIKE_MODEL_BASE + MOUNTAIN_BIKE_MODEL_OFFSET)
		CASE BHT_LION
			//HASH("A_C_MTLION")
			RETURN INT_TO_ENUM(MODEL_NAMES, LION_MODEL_BASE + LION_MODEL_OFFSET)
		CASE BHT_HUNTER
			//HASH("IG_HUNTER")
			RETURN INT_TO_ENUM(MODEL_NAMES, HUNTER_MODEL_BASE + HUNTER_MODEL_OFFSET)
		CASE BHT_HIKER
			//HASH("A_M_Y_HIKER_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, HIKER_MODEL_BASE + HIKER_MODEL_OFFSET)
		CASE BHT_HIPPIE
			//HASH("A_F_Y_HIPPIE_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, HIPPIE_MODEL_BASE + HIPPIE_MODEL_OFFSET)
		CASE BHT_REDNECK
			//HASH("A_M_M_HILLBILLY_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, REDNECK_MODEL_BASE + REDNECK_MODEL_OFFSET)
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


FUNC MODEL_NAMES Get_Beast_Hint_Model_B(INT iHintType)
	SWITCH iHintType
		CASE BHT_MOUNTAIN_BIKER
			//HASH("U_M_Y_CYCLIST_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, MOUNTAIN_BIKER_MODEL_BASE + MOUNTAIN_BIKER_MODEL_OFFSET)
		CASE BHT_HUNTER
			//HASH("DUNE")
			RETURN INT_TO_ENUM(MODEL_NAMES, BUGGY_MODEL_BASE + BUGGY_MODEL_OFFSET)
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


PROC Set_Male_Beast_Outfit_On_Ped(PED_INDEX ped)
	CDEBUG1LN(DEBUG_HUNTING, "Applying male beast outfit.")

	scrShopPedComponent componentItem

	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_M_BERD_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)

	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_M_JBIB_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	
	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_M_TORSO_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	
	SET_PED_COMPONENT_VARIATION(ped,PED_COMP_LEG, 1, 5)
	SET_PED_COMPONENT_VARIATION(ped,PED_COMP_FEET, 0, 10)
	SET_PED_COMPONENT_VARIATION(ped,PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(ped,PED_COMP_SPECIAL, 15, 0)
	SET_PED_COMPONENT_VARIATION(ped,PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(ped,PED_COMP_DECL, 0, 0)
ENDPROC


PROC Maintain_Apply_Beast_Punches_To_Ped(PED_INDEX pedTarget, PED_INDEX pedBeast, BOOL bInstantKill = FALSE, FLOAT fForceMult = 50.0, INT iExtraDamage = 0)
	IF DOES_ENTITY_EXIST(pedTarget) AND NOT IS_ENTITY_DEAD(pedBeast)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedTarget, pedBeast)
		AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedTarget, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYMELEE)
			IS_ENTITY_DEAD(pedTarget)		//Fixes an assert
			
			VECTOR vHandBone = GET_PED_BONE_COORDS(pedBeast, BONETAG_R_HAND, <<0,0,0>>)
			VECTOR vArmBone = GET_PED_BONE_COORDS(pedBeast, BONETAG_R_FOREARM, <<0,0,0>>)
			VECTOR vForce = NORMALISE_VECTOR(vHandBone - vArmBone) * (fForceMult / 2)		//Local multiplier
			VECTOR vImpact = <<0,0,0>>
			GET_PED_LAST_WEAPON_IMPACT_COORD(pedTarget, vImpact)
			CPRINTLN(DEBUG_HUNTING, "Ped damaged by beast, force ",vForce, " at ", vImpact)
			
			IF NOT IS_PED_INJURED(pedTarget)		//Fixes an assert
				SET_PED_TO_RAGDOLL(pedTarget, 0, 2000, TASK_NM_BALANCE, FALSE, FALSE)
			ENDIF
			APPLY_FORCE_TO_ENTITY(pedTarget, APPLY_TYPE_EXTERNAL_IMPULSE, vForce, vImpact, 0, FALSE, FALSE, TRUE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedTarget)
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(pedTarget)
			IF bInstantKill
				SET_ENTITY_HEALTH(pedTarget, 0)
			ELIF iExtraDamage != 0
				APPLY_DAMAGE_TO_PED(pedTarget, iExtraDamage, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL Maintain_Beast_Cheating_Check()
	//Dead check
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF (GET_PLAYER_INVINCIBLE(PLAYER_ID()))
//		CPRINTLN(debug_dan,"Player invincible, cheat!")
		RETURN TRUE
	ENDIF
	
	INT iHealth = GET_ENTITY_HEALTH(PLAYER_PED_ID())
	
	IF (iHealth > 1)
		APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), 1, FALSE)
		INT iHealth2 = GET_ENTITY_HEALTH(PLAYER_PED_ID())
		
		IF (iHealth2 >= iHealth)
			RETURN TRUE
		ENDIF
		APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), -1, FALSE)
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC Bloody_Up_Ped(PED_INDEX ped)
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_HEAD), 		0.431, 0.667,  179.593, 0.889, 2, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_HEAD), 		0.556, 0.292,  161.805, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_HEAD), 		0.708, 0.688,  167.897, 0.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_ARM), 	0.472, 0.347,  146.133, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_ARM), 	0.799, 0.451,  158.294, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.681, 0.507,  158.707, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.819, 0.813,  174.811, 0.056, 1, 0.00, "ShotgunLargeMonolithic")
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.174, 0.438,  211.521, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.174, 0.486,  213.237, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.174, 0.542,  215.168, 1.000, 0, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.236, 0.542,  213.752, 1.000, 0, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.278, 0.542,  212.851, 1.000, 0, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_TORSO), 	0.313, 0.542,  212.127, 1.000, 0, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_RIGHT_LEG), 0.639, 0.313,  149.248, 1.000, 1, 0.00, "stab") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_RIGHT_LEG), 0.792, 0.424,  182.625, 1.000, 1, 0.00, "stab") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_RIGHT_LEG), 0.792, 0.424,  182.625, 1.000, 1, 0.00, "BasicSlash")
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_RIGHT_ARM), 0.618, 0.451,  204.207, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_RIGHT_ARM), 0.785, 0.597,  206.103, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.688, 0.438,  202.910, 1.000, 1, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.688, 0.424,  202.492, 1.000, 1, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.688, 0.347,  200.181, 1.000, 1, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.688, 0.278,  198.043, 1.000, 1, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.688, 0.222,  196.275, 1.000, 1, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.653, 0.222,  196.609, 1.000, 3, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.750, 0.222,  195.716, 1.000, 2, 0.00, "BasicSlash") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	1.000, 0.326,  196.621, 1.000, 0, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.930, 0.451,  200.584, 1.000, 1, 0.00, "ShotgunLargeMonolithic") 
	APPLY_PED_BLOOD_SPECIFIC(ped,	ENUM_TO_INT(PDZ_LEFT_LEG), 	0.618, 0.451,  204.207, 1.000, 0, 0.00, "ShotgunLargeMonolithic") 
ENDPROC


//Spawns on day 0 (Sunday)
PROC Create_Beast_Rabbit_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	MODEL_NAMES eRabbitModel = Get_Beast_Hint_Model_A(BHT_RABBIT)

	IF HAS_MODEL_LOADED(eRabbitModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast rabbit hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_ANIMAL, eRabbitModel, vPosition, fHeading)
//		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eRabbitModel)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		
		//vPosition = <<-1592.6420, 4592.2207, 35.9522>>
		
		VECTOR vOffset
		vOffset = <<0.04, 0.02, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.6, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.01, 0.05, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.03, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		
		IF bDrawFootPrints
			vOffset = <<0.2417, -0.3975, -0.2282>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<-0.9471, -0.3209, 0.0000>>, 0.1900, 0.3300, 0.1060, 0.0000, 0.0000, 1.0000, -1, TRUE)
			vOffset = <<0.0747, -0.8188, -0.2576>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<-0.9471, -0.3209, 0.0000>>, 0.1900, 0.3300, 0.1060, 0.0000, 0.0000, 1.0000, -1, TRUE)
			vOffset = <<0.4322, -1.1548, -0.4902>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<-0.9471, -0.3209, 0.0000>>, 0.1900, 0.3300, 0.1060, 0.0000, 0.0000, 0.8020, -1, TRUE)
		ENDIF
		
		Bloody_Up_Ped(pedHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Rabbit_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


//Spawns on day 1 (Monday)
PROC Create_Beast_Deer_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	MODEL_NAMES eDeerModel = Get_Beast_Hint_Model_A(BHT_DEER)

	IF HAS_MODEL_LOADED(eDeerModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast deer hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_ANIMAL, eDeerModel, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eDeerModel)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		
		//vPosition =<<-1573.5006, 4459.3745, 13.5490>>
		
		VECTOR vOffset
		vOffset = <<0.12, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.0, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.03, 0.04, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.20, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.05, -0.15, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.7, 0.196, 0, 0, 1.0, -1, TRUE)
		
		IF bDrawFootPrints
			vOffset = <<0.4125, -0.5815, -0.2056>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<-0.5358, -0.8443, 0.0000>>, 0.1900, 0.3300, 0.2340, 0.0000, 0.0000, 0.9, -1, TRUE)
			vOffset = <<0.6332, -1.1758, -0.7106>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<-0.7026, -0.7115, 0.0000>>, 0.1900, 0.3300, 0.2600, 0.0000, 0.0000, 0.75, -1, TRUE)
		ENDIF
		
//		Bloody_Up_Ped(pedHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Deer_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


//Spawns on day 2 (Tueday)
PROC Create_Beast_Car_Wreck_Hint(VEHICLE_INDEX &vehHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	MODEL_NAMES eCarModel = Get_Beast_Hint_Model_A(BHT_CAR_WRECK)

	IF HAS_MODEL_LOADED(eCarModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast wrecked vehicle hint at ", vPosition, ".")
		vehHint = CREATE_VEHICLE(eCarModel, vPosition, fHeading)
		SET_MODEL_AS_NO_LONGER_NEEDED(eCarModel)
		
		SET_ENTITY_ROTATION(vehHint, <<0, 170.0, 0>>)
		SET_VEHICLE_ENGINE_HEALTH(vehHint, 1.0)
		SET_VEHICLE_BODY_HEALTH(vehHint, 1.0)
		SET_VEHICLE_DIRT_LEVEL(vehHint, 15.0)
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_FRONT_LEFT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_FRONT_RIGHT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_REAR_LEFT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_REAR_RIGHT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_BONNET, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_BOOT, GET_RANDOM_BOOL())
		SET_VEHICLE_HEADLIGHT_SHADOWS(vehHint, HEADLIGHTS_CAST_FULL_SHADOWS)
		SET_VEHICLE_LIGHTS(vehHint, FORCE_VEHICLE_LIGHTS_ON)
		SET_VEHICLE_UNDRIVEABLE(vehHint, TRUE)
		ACTIVATE_PHYSICS(vehHint)
		
		//vPosition = <<-1459.3585, 4473.0684, 18.1095>>

		IF bDrawFootPrints
			VECTOR vOffset
			vOffset = <<0.4947, 2.3632, 0.1294>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.3798, -0.9251, 0.0000>>, 0.1900, 0.3300, 0.0000, 0.0000, 0.0000, 0.7, -1, TRUE, FALSE, FALSE)
			vOffset = <<0.9243, 2.6606, 0.1951>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.3798, -0.9251, 0.0000>>, 0.1900, 0.3300, 0.0000, 0.0000, 0.0000, 0.66, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.5467, 2.5044, 0.2418>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.3798, -0.9251, 0.0000>>, 0.1900, 0.3300, 0.0000, 0.0000, 0.0000, 0.58, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.357, 3.8779, 0.3259>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.3798, -0.9251, 0.0000>>, 0.1900, 0.3300, 0.0000, 0.0000, 0.0000, 0.48, -1, TRUE, FALSE, FALSE)
		ENDIF
		
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Car_Wreck_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


//Spawns on day 3 (Wednesday)
PROC Create_Beast_Boar_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	MODEL_NAMES eBoarModel = Get_Beast_Hint_Model_A(BHT_BOAR)

	IF HAS_MODEL_LOADED(eBoarModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast boar hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_ANIMAL, eBoarModel, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
//		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eBoarModel)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		
		//vPosition = <<21.1005, 4317.9458, 42.2091>>
		
		VECTOR vOffset
		vOffset = <<0.12, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.0, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.03, 0.04, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.2, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.05, -0.15, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.7, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<0.01, -0.06, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		
		IF bDrawFootPrints
			vOffset = <<0.4936, -0.1763, -0.0522>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.0000, -1.0000, 0.0000>>, 0.1900, 0.3300, 0.1440, 0.0000, 0.0000, 1.0000, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.0645, 0.0483, -0.0698>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.0000, -1.0000, 0.0000>>, 0.1900, 0.3300, 0.1440, 0.0000, 0.0000, 1.0000, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.6011, -0.1704, -0.1473>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.0000, -1.0000, 0.0000>>, 0.1900, 0.3300, 0.1440, 0.0000, 0.0000, 0.8080, -1, TRUE, FALSE, FALSE)
		ENDIF
		
		Bloody_Up_Ped(pedHint)
		EXIT
	ENDIF
	
	
	SCRIPT_ASSERT("Create_Beast_Boar_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


//Spawns on day 4 (Thursday)
PROC Create_Beast_Lion_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	MODEL_NAMES eLionModel = Get_Beast_Hint_Model_A(BHT_LION)

	IF HAS_MODEL_LOADED(eLionModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast lion hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_ANIMAL, eLionModel, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eLionModel)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		
		//vPosition = <<262.9409, 4285.7988, 41.0045>>
		
		VECTOR vOffset
		vOffset = <<0.02, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.6, 0.4, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.03, 0.04, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 0.6, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.05, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.1, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<0.01, -0.06, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		
		IF bDrawFootPrints
			vOffset = <<0.5206, 0.0, 0.003>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.3090, -0.9511, 0.0000>>, 0.1900, 0.3300, 0.1130, 0.0000, 0.0000, 1.0000, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.1258, 0.0362, -0.0837>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.5036, -0.8639, 0.0000>>, 0.1900, 0.3300, 0.1190, 0.0000, 0.0000, 1.0, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.6107, 0.4678, -0.0815>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.5036, -0.8639, 0.0000>>, 0.1900, 0.3300, 0.1190, 0.0000, 0.0000, 0.8, -1, TRUE, FALSE, FALSE)
		ENDIF
		
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Lion_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


//Spawns on day 5 (Friday)
PROC Create_Beast_Mountain_Biker_Hint(PED_INDEX &pedHint, VEHICLE_INDEX &vehHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	MODEL_NAMES eBikeModel = Get_Beast_Hint_Model_A(BHT_MOUNTAIN_BIKER)
	MODEL_NAMES eBikerModel = Get_Beast_Hint_Model_B(BHT_MOUNTAIN_BIKER)
	
	IF HAS_MODEL_LOADED(eBikeModel) AND HAS_MODEL_LOADED(eBikerModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast mountain biker hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_CIVMALE, eBikerModel, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eBikerModel)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		
		//vPosition = <<1091.0703, 4500.7588, 50.1141>>
		
		VECTOR vOffset
		vOffset = <<0.02, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.5, 0.4, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<0.23, 0.04, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.05, -0.01, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.1, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<-0.05, -0.11, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.6, 0.196, 0, 0, 1.0, -1, TRUE)
		vOffset = <<0.09, -0.06, 0>>
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vPosition + vOffset, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.1, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		
		IF bDrawFootPrints
			vOffset = <<0.5783, 0.1357, -0.0683>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.4484, -0.8938, 0.0000>>, 0.1900, 0.3300, 0.0900, 0.0000, 0.0000, 1.0000, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.0477, 0.1103, -0.1388>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.4484, -0.8938, 0.0000>>, 0.1900, 0.3300, 0.0900, 0.0000, 0.0000, 1.0000, -1, TRUE, FALSE, FALSE)
			vOffset = <<1.5048, 0.4595, -0.1953>>
			ADD_DECAL(DECAL_RSID_FOOTPRINT_SAND, vPosition + vOffset, <<0.0, 0.0, -1.0>>, <<0.4484, -0.8938, 0.0000>>, 0.1900, 0.3300, 0.0900, 0.0000, 0.0000, 1.0000, -1, TRUE, FALSE, FALSE)
		ENDIF
		
		Bloody_Up_Ped(pedHint)

		vehHint = CREATE_VEHICLE(eBikeModel, <<vPosition.X - 0.4, vPosition.Y - 0.5, vPosition.Z>>, fHeading)
		SET_MODEL_AS_NO_LONGER_NEEDED(eBikeModel)
		
		SET_ENTITY_ROTATION(vehHint, <<0, 85.0, 0>>)
		SET_VEHICLE_BODY_HEALTH(vehHint, 20.5)
		SET_VEHICLE_DIRT_LEVEL(vehHint, 15.0)
		ACTIVATE_PHYSICS(vehHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Mountain_Biker_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


//Spawns on day 6 (Saturday)
PROC Create_Beast_Hunter_Hint(PED_INDEX &pedHint, VEHICLE_INDEX &vehHint, VECTOR vPosition, FLOAT fHeading = 0.0)
	MODEL_NAMES eHunterModel = Get_Beast_Hint_Model_A(BHT_HUNTER)
	MODEL_NAMES eBuggyModel = Get_Beast_Hint_Model_B(BHT_HUNTER)

	IF HAS_MODEL_LOADED(eHunterModel) AND HAS_MODEL_LOADED(eBuggyModel)
		CPRINTLN(DEBUG_HUNTING, "Creating beast hunter hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_CIVMALE, eHunterModel, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eHunterModel)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		
		//Offset:
		//<< 1677.69, 5141.08, 149.13>>
		//<< 1678.09, 5141.01, 149.05 >>
		
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.12-0.3, vPosition.Y-0.01-0.07, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.0, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.03-0.3, vPosition.Y+0.04-0.07, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.20-0.3, vPosition.Y-0.01-0.07, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.9, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.05-0.3, vPosition.Y-0.15-0.07, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.7, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.01-0.3, vPosition.Y-0.06-0.07, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		
		Bloody_Up_Ped(pedHint)
		
		VECTOR vBuggyOffset = <<9.9929, 4.6946, -7.1469>>
		vehHint = CREATE_VEHICLE(eBuggyModel, vPosition + vBuggyOffset, fHeading)
		SET_MODEL_AS_NO_LONGER_NEEDED(eBuggyModel)

		SET_VEHICLE_ENGINE_HEALTH(vehHint, 2.0)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehHint)
		SET_VEHICLE_BODY_HEALTH(vehHint, 2.0)
		SET_VEHICLE_BRAKE(vehHint, 1.0)
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_FRONT_LEFT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_FRONT_RIGHT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_REAR_LEFT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_REAR_RIGHT, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_BONNET, GET_RANDOM_BOOL())
		SET_VEHICLE_DOOR_BROKEN(vehHint, SC_DOOR_BOOT, GET_RANDOM_BOOL())
		SET_VEHICLE_DIRT_LEVEL(vehHint, 15.0)
		SET_VEHICLE_HEADLIGHT_SHADOWS(vehHint, HEADLIGHTS_CAST_FULL_SHADOWS)
		SET_VEHICLE_LIGHTS(vehHint, FORCE_VEHICLE_LIGHTS_ON)
		SET_VEHICLE_UNDRIVEABLE(vehHint, TRUE)
		ACTIVATE_PHYSICS(vehHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Hunter_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


PROC Create_Beast_Hiker_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0)
	MODEL_NAMES eHiker = Get_Beast_Hint_Model_A(BHT_HIKER)

	IF HAS_MODEL_LOADED(eHiker)
		CPRINTLN(DEBUG_HUNTING, "Creating beast hiker hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_CIVMALE, eHiker, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eHiker)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.02, vPosition.Y-0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.5, 0.4, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.23, vPosition.Y+0.04, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.05, vPosition.Y-0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.1, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.05, vPosition.Y-0.11, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.6, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.09, vPosition.Y-0.06, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.1, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		Bloody_Up_Ped(pedHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Hiker_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


PROC Create_Beast_Hippie_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0)
	MODEL_NAMES eHippie = Get_Beast_Hint_Model_A(BHT_HIPPIE)

	IF HAS_MODEL_LOADED(eHippie)
		CPRINTLN(DEBUG_HUNTING, "Creating beast hippie hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_CIVFEMALE, eHippie, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eHippie)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.1, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.5, 0.4, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.03, vPosition.Y + 0.3, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.2, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.1, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.5, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.6, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.01, vPosition.Y - 0.4, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.4, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.01, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.1, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.6, vPosition.Y - 0.35, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.5, 1.2, 0.196, 0, 0, 1.0, -1, TRUE)
		Bloody_Up_Ped(pedHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Hippie_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


PROC Create_Beast_Redneck_Hint(PED_INDEX &pedHint, VECTOR vPosition, FLOAT fHeading = 0.0)
	MODEL_NAMES eRedneck = Get_Beast_Hint_Model_A(BHT_REDNECK)

	IF HAS_MODEL_LOADED(eRedneck)
		CPRINTLN(DEBUG_HUNTING, "Creating beast redneck hint at ", vPosition, ".")
		pedHint = CREATE_PED(PEDTYPE_CIVFEMALE, eRedneck, vPosition, fHeading)
		DISABLE_PED_PAIN_AUDIO(pedHint,TRUE)
		SET_PED_CONFIG_FLAG(pedHint,PCF_ForceRagdollUponDeath,TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(eRedneck)
		SET_ENTITY_HEALTH(pedHint, 0)
		ACTIVATE_PHYSICS(pedHint)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.1, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.5, 0.4, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.03, vPosition.Y + 0.3, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.9, 0.8, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.2, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.8, 1.1, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.5, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 0.7, 0.6, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.01, vPosition.Y - 0.4, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.4, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X + 0.01, vPosition.Y - 0.01, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.1, 1.0, 0.196, 0, 0, 1.0, -1, TRUE)
		ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<vPosition.X - 0.6, vPosition.Y - 0.35, vPosition.Z>>, <<0.0, 0.0, -1.0>>, NORMALISE_VECTOR(<<0.0, 1.0, 0.0>>), 1.5, 1.2, 0.196, 0, 0, 1.0, -1, TRUE)
		Bloody_Up_Ped(pedHint)
		EXIT
	ENDIF
	
	SCRIPT_ASSERT("Create_Beast_Redneck_Hint: Tried to create a beast hint when the required models weren't loaded.")
ENDPROC


PROC Create_Beast_Hint_Type(INT iHintType, PED_INDEX &pedHint, VEHICLE_INDEX &vehHint, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bDrawFootPrints = FALSE)
	SWITCH iHintType
		CASE BHT_RABBIT
			Create_Beast_Rabbit_Hint(pedHint, vPosition, fHeading, bDrawFootPrints)
		BREAK
		CASE BHT_BOAR
			Create_Beast_Boar_Hint(pedHint, vPosition, fHeading, bDrawFootPrints)
		BREAK
		CASE BHT_DEER
			Create_Beast_Deer_Hint(pedHint, vPosition, fHeading, bDrawFootPrints)
		BREAK
		CASE BHT_CAR_WRECK
			Create_Beast_Car_Wreck_Hint(vehHint, vPosition, fHeading, bDrawFootPrints)
		BREAK
		CASE BHT_LION
			Create_Beast_Lion_Hint(pedHint, vPosition, fHeading, bDrawFootPrints)
		BREAK
		CASE BHT_MOUNTAIN_BIKER
			Create_Beast_Mountain_Biker_Hint(pedHint, vehHint, vPosition, fHeading, bDrawFootPrints)
		BREAK
		CASE BHT_HUNTER
			Create_Beast_Hunter_Hint(pedHint, vehHint, vPosition, fHeading)
		BREAK
		CASE BHT_HIKER
			Create_Beast_Hiker_Hint(pedHint, vPosition, fHeading)
		BREAK
		CASE BHT_HIPPIE
			Create_Beast_Hippie_Hint(pedHint, vPosition, fHeading)
		BREAK
		CASE BHT_REDNECK
			Create_Beast_Redneck_Hint(pedHint, vPosition, fHeading)
		BREAK
	ENDSWITCH
ENDPROC


FUNC BOOL Is_Player_In_Bad_Hunt_Vehicle(VEHICLE_INDEX &vehReturnIndex)
	VEHICLE_INDEX vehPlayer = NULL
	BOOL bIsInBadVehicle = FALSE
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF NOT IS_ENTITY_DEAD(vehPlayer)
					MODEL_NAMES mVehicle = GET_ENTITY_MODEL(vehPlayer)
					//Check vehicle type
					IF NOT (IS_THIS_MODEL_A_BICYCLE(mVehicle) OR mVehicle = VOLTIC OR mVehicle = DILETTANTE OR mVehicle = DILETTANTE2)
						bIsInBadVehicle = TRUE
						
						#IF IS_DEBUG_BUILD
							Beast_Display_Onscreen_Text(0.5,0.8, "Bad vehicle!", 0.3)
						#ENDIF
					ENDIF
				ENDIF		
			ENDIF
		ENDIF
	ENDIF

	vehReturnIndex = vehPlayer
	RETURN bIsInBadVehicle
ENDFUNC


FUNC INT Get_Int_From_Bitset(INT iBitset, INT iMask, INT iShift)
	RETURN SHIFT_RIGHT(iBitset & iMask, iShift)
ENDFUNC


PROC Set_Int_In_Bitset(INT &iBitset, INT iValue, INT iMask, INT iShift)
	iBitset -= iBitset & iMask
	iBitset |= SHIFT_LEFT(iValue, iShift)
ENDPROC


/// PURPOSE:
///    Checks if the Duck button has been pressed. Should be called every frame for a constant check.
FUNC BOOL Has_Beast_Call_Been_Made(BOOL bClearAfterCheck = FALSE, BOOL bDisplayText = FALSE)
	IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DUCK)
		SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_Call_Made)
	ENDIF
	
	BOOL made = IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_Call_Made)
	IF bClearAfterCheck and made
		CLEAR_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_Call_Made)
	ENDIF
	
	bDisplayText = bDisplayText		//Release compile
	#IF IS_DEBUG_BUILD
	IF made AND bDisplayText
		Beast_Display_Onscreen_Text(0.4, 0.8, "Call made", 0.3) 
	ENDIF
	#ENDIF
	
	RETURN made
ENDFUNC


FUNC BOOL Has_Beast_Call_Just_Been_Made()
	BOOL bMadeLastFrame = IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_Call_Made)
	RETURN Has_Beast_Call_Been_Made() AND NOT bMadeLastFrame
ENDFUNC


PROC Set_Beast_Call_Made(BOOL bMade = TRUE)
	IF bMade
		SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_Call_Made)
	ELSE
		CLEAR_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_Call_Made)
	ENDIF
ENDPROC


FUNC TEXT_LABEL_23 Build_Beast_String_23(STRING namePart2, STRING namePart1, STRING namePart4, STRING namePart3)
	TEXT_LABEL_23 tlString = namePart1
	tlString += namePart2
	tlString += namePart3
	tlString += namePart4
	
	RETURN tlString
ENDFUNC


FUNC TEXT_LABEL_31 Build_Beast_String_31(STRING namePart2, STRING namePart1, STRING namePart4, STRING namePart3)
	TEXT_LABEL_31 tlString = namePart1
	tlString += namePart2
	tlString += namePart3
	tlString += namePart4
	
	RETURN tlString
ENDFUNC


FUNC TEXT_LABEL_63 Build_Beast_String_63(STRING namePart2, STRING namePart1, STRING namePart4, STRING namePart3)
	TEXT_LABEL_63 tlString = namePart1
	tlString += namePart2
	tlString += namePart3
	tlString += namePart4
	
	RETURN tlString
ENDFUNC


FUNC TEXT_LABEL_63 Build_Long_Beast_String_63(STRING namePart7, STRING namePart2, STRING namePart6, STRING namePart8, STRING namePart1, STRING namePart4, STRING namePart5, STRING namePart3)
	TEXT_LABEL_63 tlString = namePart1
	tlString += namePart2
	tlString += namePart3
	tlString += namePart4
	tlString += namePart5
	tlString += namePart6
	tlString += namePart7
	tlString += namePart8
	
	RETURN tlString
ENDFUNC


FUNC BOOL Has_Beast_Secret_Data_Setup_Finished()
	//Well, IS_BITMASK_SET is broken (just checks if results >0), so I'll just reimplement it here
	RETURN (g_iBeastSetupInt & BH_Bit_Data_Finished) = BH_Bit_Data_Finished
ENDFUNC


PROC Set_Beast_Peyote_Vectors_X()
	g_vPeyoteDaySfxPosition[0].x = -1567.3816	//<<-1567.3816, 4464.3052, 17.4783>>
	g_vPeyoteDaySfxPosition[1].x = -1436.3053   //<<-1436.3053, 4428.8013, 44.8536>>
	g_vPeyoteDaySfxPosition[2].x = 31.2408		//<<31.2408, 4328.0522, 43.9517>>	
	g_vPeyoteDaySfxPosition[3].x = 278.1924		//<<278.1924, 4276.5039, 39.3595>>
	g_vPeyoteDaySfxPosition[4].x = 1116.0020	//<<1116.0020, 4506.7651, 64.8542>>
	g_vPeyoteDaySfxPosition[5].x = 1676.1929	//<<1676.1929, 5139.8701, 149.3976>>
	
	g_vPeyoteDayHintPosition[0].x =	-1592.6420	//<<-1592.6420, 4592.2207, 35.9522>>
	g_vPeyoteDayHintPosition[1].x =	-1573.5006	//<<-1573.5006, 4459.3745, 13.5490>>
	g_vPeyoteDayHintPosition[2].x =	-1459.3585	//<<-1459.3585, 4473.0684, 18.1095>>
	g_vPeyoteDayHintPosition[3].x =	21.1005		//<<21.1005, 4317.9458, 42.2091>>
	g_vPeyoteDayHintPosition[4].x =	262.9409	//<<262.9409, 4285.7988, 41.0045>>
	g_vPeyoteDayHintPosition[5].x =	1091.0703	//<<1091.0703, 4500.7588, 50.1141>>
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Peyote_x)
	CPRINTLN(debug_hunting, "BEAST: Set peyote vector X data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Peyote_Vectors_Y()
	g_vPeyoteDaySfxPosition[0].y = 4464.3052	//<<-1567.3816, 4464.3052, 17.4783>>
	g_vPeyoteDaySfxPosition[1].y = 4428.8013  	//<<-1436.3053, 4428.8013, 44.8536>>
	g_vPeyoteDaySfxPosition[2].y = 4328.0522	//<<31.2408, 4328.0522, 43.9517>>	
	g_vPeyoteDaySfxPosition[3].y = 4276.5039	//<<278.1924, 4276.5039, 39.3595>>
	g_vPeyoteDaySfxPosition[4].y = 4506.7651	//<<1116.0020, 4506.7651, 64.8542>>
	g_vPeyoteDaySfxPosition[5].y = 5139.8701	//<<1676.1929, 5139.8701, 149.3976>>
	
	g_vPeyoteDayHintPosition[0].y = 4592.2207	//<<-1592.6420, 4592.2207, 35.9522>>
	g_vPeyoteDayHintPosition[1].y =	4459.3745	//<<-1573.5006, 4459.3745, 13.5490>>
	g_vPeyoteDayHintPosition[2].y =	4473.0684	//<<-1459.3585, 4473.0684, 18.1095>>
	g_vPeyoteDayHintPosition[3].y =	4317.9458	//<<21.1005, 4317.9458, 42.2091>>
	g_vPeyoteDayHintPosition[4].y =	4285.7988	//<<262.9409, 4285.7988, 41.0045>>
	g_vPeyoteDayHintPosition[5].y =	4500.7588	//<<1091.0703, 4500.7588, 50.1141>>
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Peyote_y)
	CPRINTLN(debug_hunting, "BEAST: Set peyote vector Y data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Peyote_Vectors_Z()
	g_vPeyoteDaySfxPosition[0].z = 17.4783	//<<-1567.3816, 4464.3052, 17.4783>>
	g_vPeyoteDaySfxPosition[1].z = 44.8536  //<<-1436.3053, 4428.8013, 44.8536>>
	g_vPeyoteDaySfxPosition[2].z = 43.9517	//<<31.2408, 4328.0522, 43.9517>>	
	g_vPeyoteDaySfxPosition[3].z = 39.3595	//<<278.1924, 4276.5039, 39.3595>>
	g_vPeyoteDaySfxPosition[4].z = 64.8542	//<<1116.0020, 4506.7651, 64.8542>>
	g_vPeyoteDaySfxPosition[5].z = 149.3976	//<<1676.1929, 5139.8701, 149.3976>>
	
	g_vPeyoteDayHintPosition[0].z =	35.9522	//<<-1592.6420, 4592.2207, 35.9522>>
	g_vPeyoteDayHintPosition[1].z =	13.5490	//<<-1573.5006, 4459.3745, 13.5490>>
	g_vPeyoteDayHintPosition[2].z =	18.1095	//<<-1459.3585, 4473.0684, 18.1095>>
	g_vPeyoteDayHintPosition[3].z =	42.2091	//<<21.1005, 4317.9458, 42.2091>>
	g_vPeyoteDayHintPosition[4].z =	41.0045	//<<262.9409, 4285.7988, 41.0045>>
	g_vPeyoteDayHintPosition[5].z =	50.1141	//<<1091.0703, 4500.7588, 50.1141>>
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Peyote_z)
	CPRINTLN(debug_hunting, "BEAST: Set peyote vector Z data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Hunt_Vectors_X()
	//Checkpoints
	g_vBHCheckpoints[0].x 	=1678.09 //<< 1678.09, 5141.01, 149.05 >>
	g_vBHCheckpoints[1].x 	=2135.74 //<< 2135.74, 6048.71, 44.4 >>  
	g_vBHCheckpoints[2].x 	=2565.06 //<< 2565.06,6185.32,161.66 >> 
	g_vBHCheckpoints[3].x 	=3318.81 //<< 3318.81, 5180.99, 17.5 >>
	g_vBHCheckpoints[4].x 	=1849.53 //<< 1849.53,4582.72,30.69 >> 
	g_vBHCheckpoints[5].x 	=2692.14 //<< 2692.14, 4831.82, 33.12 >>
	g_vBHCheckpoints[6].x 	=2915.64 //<< 2915.64, 4322.66, 49.3 >>
	g_vBHCheckpoints[7].x 	=2972.94 //<< 2972.94, 3503.92, 70.38 >>
	g_vBHCheckpoints[8].x 	=2512.64 //<< 2512.64, 3777.54, 51.87 >>
	g_vBHCheckpoints[9].x 	=1970.29 //<< 1970.29, 3810.8, 31.3737 >>
	g_vBHCheckpoints[10].x 	=2408.71 //<< 2408.71, 3095.88, 47.1529 >>
	
	//Nodes
	g_vBHNodes[0 ][0].x =2302.76 	g_vBHNodes[0 ][1].x =2430.40 	g_vBHNodes[0 ][2].x =2466.59	g_vBHNodes[0][3].x =2316.91		//2302.76, 5222.39, 58.42, 	2430.40, 5333.68, 76.64, 	2466.59, 5534.22, 44.19, 	2316.91, 5843.98, 46.37
	g_vBHNodes[1 ][0].x =1969.33 	g_vBHNodes[1 ][1].x =2522.25 	g_vBHNodes[1 ][2].x =3057.61								//1969.33, 5178.94, 46.86, 	2522.25, 4956.4, 43.71, 	3057.61, 5063.24, 17.70)
	g_vBHNodes[2 ][0].x =1428.54 	g_vBHNodes[2 ][1].x =1626.60 	g_vBHNodes[2 ][2].x =1801.79								//1428.54, 5027.97, 107.71,	1626.60, 4854.23, 43.23, 	1801.79, 4770.31, 32.98)
	g_vBHNodes[3 ][0].x =2334.89 	g_vBHNodes[3 ][1].x =2393.25 	g_vBHNodes[3 ][2].x =2445.89								//2334.89, 6052.6, 100.3, 	2393.25, 6086.12, 132.91, 	2445.89, 6165.88, 178.98)
	g_vBHNodes[4 ][0].x =2311.40 	g_vBHNodes[4 ][1].x =2604.71 	g_vBHNodes[4 ][2].x =2720.39								//2311.40, 5880.13, 51.39, 	2604.71, 5515.67, 51.14, 	2720.39, 5195.4, 44.52)
	g_vBHNodes[5 ][0].x =2711.54 	g_vBHNodes[5 ][1].x =3085.80 	g_vBHNodes[5 ][2].x =3269.95								//2711.54, 6380.15, 117.41,	3085.8, 6193.92, 120.29, 	3269.95, 5909.13, 87.93)
	g_vBHNodes[6 ][0].x =3152.87 	g_vBHNodes[6 ][1].x =2881.11 	g_vBHNodes[6 ][2].x =2780.61								//3152.87, 5072.56, 4.14, 	2881.11, 5062.55, 27.65, 	2780.61, 4964.13, 29.15)
	g_vBHNodes[7 ][0].x =3198.90 	g_vBHNodes[7 ][1].x =3134.58 	g_vBHNodes[7 ][2].x =3036.24								//3198.90, 4956.69, 43.77, 	3134.58, 4767.8, 153.94, 	3036.24, 4588.79, 51.37)
	g_vBHNodes[8 ][0].x =2155.17 	g_vBHNodes[8 ][1].x =2429.00 	g_vBHNodes[8 ][2].x =2552.63								//2155.17, 4783.28, 39.95, 	2429.0, 4613.93, 28.11, 	2552.63, 4737.46, 28.92)
	g_vBHNodes[9 ][0].x =2440.40 	g_vBHNodes[9 ][1].x =2561.90 	g_vBHNodes[9 ][2].x =2896.60								//2440.40, 4624.70, 28.51, 	2561.9, 4286.39, 40.56, 	2896.60, 4500.57, 46.99)
	g_vBHNodes[10][0].x =2938.81 	g_vBHNodes[10][1].x =2868.55 	g_vBHNodes[10][2].x =2919.73								//2938.81, 4618.13, 47.73, 	2868.55, 4534.44, 44.34, 	2919.73, 4493.64, 47.8)
	g_vBHNodes[11][0].x =2734.24 	g_vBHNodes[11][1].x =2524.12 	g_vBHNodes[11][2].x =2630.76	g_vBHNodes[11][3].x =2501.67		//2734.24, 4282.70, 47.38, 	2524.12, 4111.71, 37.63, 	2630.76, 3914.81, 41.22, 	2501.67, 3894.7, 39.2)
	g_vBHNodes[12][0].x =3082.36 	g_vBHNodes[12][1].x =2980.31 	g_vBHNodes[12][2].x =2885.25								//3082.36, 4038.96, 65.44, 	2980.31, 3849.33, 47.92, 	2885.25, 3699.37, 44.36)
	g_vBHNodes[13][0].x =2812.63 	g_vBHNodes[13][1].x =2648.99 	g_vBHNodes[13][2].x =2496.60								//2812.63, 3970.96, 44.20, 	2648.99, 3926.75, 40.62, 	2496.60, 3874.89, 39.2)
	g_vBHNodes[14][0].x =2670.26 	g_vBHNodes[14][1].x =2483.62 	g_vBHNodes[14][2].x =2370.73								//2670.26, 3291.26, 54.24, 	2483.62, 3436.60, 48.99, 	2370.73, 3592.12, 56.11)
	g_vBHNodes[15][0].x =2886.82 	g_vBHNodes[15][1].x =2786.55 	g_vBHNodes[15][2].x =2453.63	g_vBHNodes[15][3].x =2282.27		//2886.82, 3710.49, 43.32, 	2786.55, 3945.38, 43.41, 	2453.63, 3854.20, 37.95, 	2282.27, 3757.30, 37.52)
	g_vBHNodes[16][0].x =2675.75 	g_vBHNodes[16][1].x =2384.53 	g_vBHNodes[16][2].x =2573.34								//2675.75, 3528.6, 51.39, 	2384.53, 3328.93, 46.46, 	2573.34, 3157.76, 49.66)
	g_vBHNodes[17][0].x =2331.89 	g_vBHNodes[17][1].x =2073.61 	g_vBHNodes[17][2].x =1903.3									//2331.89, 3809.12, 33.41, 	2073.61, 3826.24, 30.81, 	1903.3, 3727.81, 31.73)
	g_vBHNodes[18][0].x =2187.61 	g_vBHNodes[18][1].x =2183.83 	g_vBHNodes[18][2].x =1983.37								//2187.61, 3610.22, 42.25, 	2183.83, 3314.26, 45.23, 	1983.37, 3034.58, 46.6)
	g_vBHNodes[19][0].x =1565.57 	g_vBHNodes[19][1].x =1867.24 	g_vBHNodes[19][2].x =2051.61								//1565.57, 3572.72, 32.58, 	1867.24, 3261.69, 45.15, 	2051.61, 3184.28, 44.17)
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Hunt_x)
	CPRINTLN(debug_hunting, "BEAST: Set hunt vector X data, bitset: ", g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Hunt_Vectors_Y()
	g_vBHCheckpoints[0].y 	=5141.01 //<< 1678.09, 5141.01, 149.05 >>
	g_vBHCheckpoints[1].y 	=6048.71 //<< 2135.74, 6048.71, 44.4 >>
	g_vBHCheckpoints[2].y 	=6185.32 //<< 2565.06,6185.32,161.66 >> 
	g_vBHCheckpoints[3].y 	=5180.99 //<< 3318.81, 5180.99, 17.5 >>
	g_vBHCheckpoints[4].y 	=4582.72 //<< 1849.53,4582.72,30.69 >> 
	g_vBHCheckpoints[5].y 	=4831.82 //<< 2692.14, 4831.82, 33.12 >>
	g_vBHCheckpoints[6].y 	=4322.66 //<< 2915.64, 4322.66, 49.3 >>
	g_vBHCheckpoints[7].y 	=3503.92 //<< 2972.94, 3503.92, 70.38 >>
	g_vBHCheckpoints[8].y 	=3777.54 //<< 2512.64, 3777.54, 51.87 >>
	g_vBHCheckpoints[9].y 	=3810.8 //<< 1970.29, 3810.8, 31.3737 >>
	g_vBHCheckpoints[10].y 	=3095.88 //<< 2408.71, 3095.88, 47.1529 >>
	
	//Nodes
	g_vBHNodes[0 ][0].y =5222.39 	g_vBHNodes[0 ][1].y =5333.68 	g_vBHNodes[0 ][2].y =5534.22		g_vBHNodes[0][3].y =5843.98		//2302.76, 5222.39, 58.42, 	2430.40, 5333.68, 76.64, 	2466.59, 5534.22, 44.19, 	2316.91, 5843.98, 46.37
	g_vBHNodes[1 ][0].y =5178.94 	g_vBHNodes[1 ][1].y =4956.40 	g_vBHNodes[1 ][2].y =5063.24										//1969.33, 5178.94, 46.86, 	2522.25, 4956.4, 43.71, 	3057.61, 5063.24, 17.70)
	g_vBHNodes[2 ][0].y =5027.97 	g_vBHNodes[2 ][1].y =4854.23 	g_vBHNodes[2 ][2].y =4770.31										//1428.54, 5027.97, 107.71,	1626.60, 4854.23, 43.23, 	1801.79, 4770.31, 32.98)
	g_vBHNodes[3 ][0].y =6052.60 	g_vBHNodes[3 ][1].y =6086.12 	g_vBHNodes[3 ][2].y =6165.88										//2334.89, 6052.6, 100.3, 	2393.25, 6086.12, 132.91, 	2445.89, 6165.88, 178.98)
	g_vBHNodes[4 ][0].y =5880.13 	g_vBHNodes[4 ][1].y =5515.67 	g_vBHNodes[4 ][2].y =5195.4											//2311.40, 5880.13, 51.39, 	2604.71, 5515.67, 51.14, 	2720.39, 5195.4, 44.52)
	g_vBHNodes[5 ][0].y =6380.15 	g_vBHNodes[5 ][1].y =6193.92 	g_vBHNodes[5 ][2].y =5909.13										//2711.54, 6380.15, 117.41,	3085.8, 6193.92, 120.29, 	3269.95, 5909.13, 87.93)
	g_vBHNodes[6 ][0].y =5072.56 	g_vBHNodes[6 ][1].y =5062.55 	g_vBHNodes[6 ][2].y =4964.13										//3152.87, 5072.56, 4.14, 	2881.11, 5062.55, 27.65, 	2780.61, 4964.13, 29.15)
	g_vBHNodes[7 ][0].y =4956.69 	g_vBHNodes[7 ][1].y =4767.80 	g_vBHNodes[7 ][2].y =4588.79										//3198.90, 4956.69, 43.77, 	3134.58, 4767.8, 153.94, 	3036.24, 4588.79, 51.37)
	g_vBHNodes[8 ][0].y =4783.28 	g_vBHNodes[8 ][1].y =4613.93 	g_vBHNodes[8 ][2].y =4737.46										//2155.17, 4783.28, 39.95, 	2429.0, 4613.93, 28.11, 	2552.63, 4737.46, 28.92)
	g_vBHNodes[9 ][0].y =4624.70 	g_vBHNodes[9 ][1].y =4286.39 	g_vBHNodes[9 ][2].y =4500.57										//2440.40, 4624.70, 28.51, 	2561.9, 4286.39, 40.56, 	2896.60, 4500.57, 46.99)
	g_vBHNodes[10][0].y =4618.13 	g_vBHNodes[10][1].y =4534.44 	g_vBHNodes[10][2].y =4493.64										//2938.81, 4618.13, 47.73, 	2868.55, 4534.44, 44.34, 	2919.73, 4493.64, 47.8)
	g_vBHNodes[11][0].y =4282.70 	g_vBHNodes[11][1].y =4111.71 	g_vBHNodes[11][2].y =3914.81		g_vBHNodes[11][3].y =3894.70	//2734.24, 4282.70, 47.38, 	2524.12, 4111.71, 37.63, 	2630.76, 3914.81, 41.22, 	2501.67, 3894.7, 39.2)
	g_vBHNodes[12][0].y =4038.96 	g_vBHNodes[12][1].y =3849.33 	g_vBHNodes[12][2].y =3699.37										//3082.36, 4038.96, 65.44, 	2980.31, 3849.33, 47.92, 	2885.25, 3699.37, 44.36)
	g_vBHNodes[13][0].y =3970.96 	g_vBHNodes[13][1].y =3926.75 	g_vBHNodes[13][2].y =3874.89										//2812.63, 3970.96, 44.20, 	2648.99, 3926.75, 40.62, 	2496.60, 3874.89, 39.2)
	g_vBHNodes[14][0].y =3291.26 	g_vBHNodes[14][1].y =3436.60 	g_vBHNodes[14][2].y =3592.12										//2670.26, 3291.26, 54.24, 	2483.62, 3436.60, 48.99, 	2370.73, 3592.12, 56.11)
	g_vBHNodes[15][0].y =3710.49 	g_vBHNodes[15][1].y =3945.38 	g_vBHNodes[15][2].y =3854.20		g_vBHNodes[15][3].y =3757.30	//2886.82, 3710.49, 43.32, 	2786.55, 3945.38, 43.41, 	2453.63, 3854.20, 37.95, 	2282.27, 3757.30, 37.52)
	g_vBHNodes[16][0].y =3528.60 	g_vBHNodes[16][1].y =3328.93 	g_vBHNodes[16][2].y =3157.76										//2675.75, 3528.6, 51.39, 	2384.53, 3328.93, 46.46, 	2573.34, 3157.76, 49.66)
	g_vBHNodes[17][0].y =3809.12 	g_vBHNodes[17][1].y =3826.24 	g_vBHNodes[17][2].y =3727.81										//2331.89, 3809.12, 33.41, 	2073.61, 3826.24, 30.81, 	1903.3, 3727.81, 31.73)
	g_vBHNodes[18][0].y =3610.22 	g_vBHNodes[18][1].y =3314.26 	g_vBHNodes[18][2].y =3034.58										//2187.61, 3610.22, 42.25, 	2183.83, 3314.26, 45.23, 	1983.37, 3034.58, 46.6)
	g_vBHNodes[19][0].y =3572.72 	g_vBHNodes[19][1].y =3261.69 	g_vBHNodes[19][2].y =3184.28										//1565.57, 3572.72, 32.58, 	1867.24, 3261.69, 45.15, 	2051.61, 3184.28, 44.17)
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Hunt_y)
	CPRINTLN(debug_hunting, "BEAST: Set hunt vector Y data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Hunt_Vectors_Z()
	g_vBHCheckpoints[0].z 	=149.05 //<< 1678.09, 5141.01, 149.05 >>
	g_vBHCheckpoints[1].z 	=44.4 //<< 2135.74, 6048.71, 44.4 >>
	g_vBHCheckpoints[2].z 	=161.66 //<< 2565.06,6185.32,161.66 >> 
	g_vBHCheckpoints[3].z 	=17.5 //<< 3318.81, 5180.99, 17.5 >>
	g_vBHCheckpoints[4].z 	=30.69 //<< 1849.53,4582.72,30.69 >> 
	g_vBHCheckpoints[5].z 	=33.12 //<< 2692.14, 4831.82, 33.12 >>
	g_vBHCheckpoints[6].z 	=49.3 //<< 2915.64, 4322.66, 49.3 >>
	g_vBHCheckpoints[7].z 	=70.38 //<< 2972.94, 3503.92, 70.38 >>
	g_vBHCheckpoints[8].z 	=51.87 //<< 2512.64, 3777.54, 51.87 >>
	g_vBHCheckpoints[9].z 	=31.3737 //<< 1970.29, 3810.8, 31.3737 >>
	g_vBHCheckpoints[10].z 	=47.1529 //<< 2408.71, 3095.88, 47.1529 >>
	
	//Nodes
	g_vBHNodes[0 ][0].z =58.420 	g_vBHNodes[0 ][1].z =76.640 	g_vBHNodes[0 ][2].z =44.19		g_vBHNodes[0][3].z =46.37	//2302.76, 5222.39, 58.42, 	2430.40, 5333.68, 76.64, 	2466.59, 5534.22, 44.19, 	2316.91, 5843.98, 46.37
	g_vBHNodes[1 ][0].z =46.860 	g_vBHNodes[1 ][1].z =43.710 	g_vBHNodes[1 ][2].z =17.70									//1969.33, 5178.94, 46.86, 	2522.25, 4956.4, 43.71, 	3057.61, 5063.24, 17.70)
	g_vBHNodes[2 ][0].z =107.71 	g_vBHNodes[2 ][1].z =43.230 	g_vBHNodes[2 ][2].z =32.98									//1428.54, 5027.97, 107.71,	1626.60, 4854.23, 43.23, 	1801.79, 4770.31, 32.98)
	g_vBHNodes[3 ][0].z =100.30 	g_vBHNodes[3 ][1].z =132.91 	g_vBHNodes[3 ][2].z =178.98									//2334.89, 6052.6, 100.3, 	2393.25, 6086.12, 132.91, 	2445.89, 6165.88, 178.98)
	g_vBHNodes[4 ][0].z =51.390 	g_vBHNodes[4 ][1].z =51.140 	g_vBHNodes[4 ][2].z =44.52									//2311.40, 5880.13, 51.39, 	2604.71, 5515.67, 51.14, 	2720.39, 5195.4, 44.52)
	g_vBHNodes[5 ][0].z =117.41 	g_vBHNodes[5 ][1].z =120.29 	g_vBHNodes[5 ][2].z =87.93									//2711.54, 6380.15, 117.41,	3085.8, 6193.92, 120.29, 	3269.95, 5909.13, 87.93)
	g_vBHNodes[6 ][0].z =4.1400 	g_vBHNodes[6 ][1].z =27.650 	g_vBHNodes[6 ][2].z =29.15									//3152.87, 5072.56, 4.14, 	2881.11, 5062.55, 27.65, 	2780.61, 4964.13, 29.15)
	g_vBHNodes[7 ][0].z =43.770 	g_vBHNodes[7 ][1].z =153.94 	g_vBHNodes[7 ][2].z =51.37									//3198.90, 4956.69, 43.77, 	3134.58, 4767.8, 153.94, 	3036.24, 4588.79, 51.37)
	g_vBHNodes[8 ][0].z =39.950 	g_vBHNodes[8 ][1].z =28.110 	g_vBHNodes[8 ][2].z =28.92									//2155.17, 4783.28, 39.95, 	2429.0, 4613.93, 28.11, 	2552.63, 4737.46, 28.92)
	g_vBHNodes[9 ][0].z =28.510 	g_vBHNodes[9 ][1].z =40.560 	g_vBHNodes[9 ][2].z =46.99									//2440.40, 4624.70, 28.51, 	2561.9, 4286.39, 40.56, 	2896.60, 4500.57, 46.99)
	g_vBHNodes[10][0].z =47.730 	g_vBHNodes[10][1].z =44.340 	g_vBHNodes[10][2].z =47.8									//2938.81, 4618.13, 47.73, 	2868.55, 4534.44, 44.34, 	2919.73, 4493.64, 47.8)
	g_vBHNodes[11][0].z =47.380 	g_vBHNodes[11][1].z =37.630 	g_vBHNodes[11][2].z =41.22		g_vBHNodes[11][3].z =39.20	//2734.24, 4282.70, 47.38, 	2524.12, 4111.71, 37.63, 	2630.76, 3914.81, 41.22, 	2501.67, 3894.7, 39.2)
	g_vBHNodes[12][0].z =65.440 	g_vBHNodes[12][1].z =47.920 	g_vBHNodes[12][2].z =44.36									//3082.36, 4038.96, 65.44, 	2980.31, 3849.33, 47.92, 	2885.25, 3699.37, 44.36)
	g_vBHNodes[13][0].z =44.200 	g_vBHNodes[13][1].z =40.620 	g_vBHNodes[13][2].z =39.20									//2812.63, 3970.96, 44.20, 	2648.99, 3926.75, 40.62, 	2496.60, 3874.89, 39.2)
	g_vBHNodes[14][0].z =54.240 	g_vBHNodes[14][1].z =48.990 	g_vBHNodes[14][2].z =56.11									//2670.26, 3291.26, 54.24, 	2483.62, 3436.60, 48.99, 	2370.73, 3592.12, 56.11)
	g_vBHNodes[15][0].z =43.320 	g_vBHNodes[15][1].z =43.410 	g_vBHNodes[15][2].z =37.95		g_vBHNodes[15][3].z =37.52	//2886.82, 3710.49, 43.32, 	2786.55, 3945.38, 43.41, 	2453.63, 3854.20, 37.95, 	2282.27, 3757.30, 37.52)
	g_vBHNodes[16][0].z =51.390 	g_vBHNodes[16][1].z =46.460 	g_vBHNodes[16][2].z =49.66									//2675.75, 3528.6, 51.39, 	2384.53, 3328.93, 46.46, 	2573.34, 3157.76, 49.66)
	g_vBHNodes[17][0].z =33.410 	g_vBHNodes[17][1].z =30.810 	g_vBHNodes[17][2].z =31.73									//2331.89, 3809.12, 33.41, 	2073.61, 3826.24, 30.81, 	1903.3, 3727.81, 31.73)
	g_vBHNodes[18][0].z =42.250 	g_vBHNodes[18][1].z =45.230 	g_vBHNodes[18][2].z =46.60									//2187.61, 3610.22, 42.25, 	2183.83, 3314.26, 45.23, 	1983.37, 3034.58, 46.6)
	g_vBHNodes[19][0].z =32.580 	g_vBHNodes[19][1].z =45.150 	g_vBHNodes[19][2].z =44.17									//1565.57, 3572.72, 32.58, 	1867.24, 3261.69, 45.15, 	2051.61, 3184.28, 44.17)
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Hunt_z)
	CPRINTLN(debug_hunting, "BEAST: Set hunt vector Z data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Fight_Vectors_x()

	g_vBeastFinaleRootPosition.x = 2405.8494 		//<<2405.8494, 3029.5410, 47.1526>>
	g_vBeastFinaleCheckPos.x = 2408.22				//<<2408.22, 3031.50, 47.15>>
	g_vBeastFinaleArenaPos.x = 2386.0961			//<<2386.0961,3094.0444,58.0271>> 
	g_vBeastFinaleArenaSize.x = 69.1875 			//<<69.1875,86.3125,15.8750>>
	
	//IS_ENTITY_AT_COORD(PLAYER_PED_ID(), 			<<2408.22, 3031.50, 47.15>>
	//IS_SPHERE_VISIBLE(							<<2405.59, 3028.55, 48.00>>
	//IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,		<<2405.750, 3029.633, 48.109>>
	
	g_vBeastFinaleAnimalSpawnVec[0].x = 2417.1050	//<<2417.1050, 3113.5032, 47.1662>>
	g_vBeastFinaleAnimalSpawnVec[1].x =	2407.0581	//<<2407.0581, 3116.8059, 47.2119>>
	g_vBeastFinaleAnimalSpawnVec[2].x =	2371.9197	//<<2371.9197, 3080.0127, 47.1530>>
	g_vBeastFinaleAnimalSpawnVec[3].x =	2399.5835	//<<2399.5835, 3091.8440, 47.1531>>
	g_vBeastFinaleAnimalSpawnVec[4].x =	2416.5190	//<<2416.5190, 3045.7954, 47.1523>>
	g_vBeastFinaleAnimalSpawnVec[5].x =	2430.3450	//<<2430.3450, 3103.7461, 47.1531>>
	g_vBeastFinaleAnimalSpawnVec[6].x =	2385.8186	//<<2385.8186, 3031.4382, 47.1529>>
	
	g_vBeastFinaleAnimalSpawnHead[0] =	157.8636
	g_vBeastFinaleAnimalSpawnHead[1] =	174.7226
	g_vBeastFinaleAnimalSpawnHead[2] =	270.7562
	g_vBeastFinaleAnimalSpawnHead[3] =	190.7495
	g_vBeastFinaleAnimalSpawnHead[4] =	27.6506
	g_vBeastFinaleAnimalSpawnHead[5] =	358.1218
	g_vBeastFinaleAnimalSpawnHead[6] =	339.4363
	
	g_vBeastFinaleStartPoint.x = 2399.82	//<<2399.82, 3058.12, 53.42>>
	g_vBeastFinaleJumpOffPoint.x = 2414.7	//<<2412.1, 3046.7, 48.2>>

	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Fight_x)
	CPRINTLN(debug_hunting, "BEAST: Set fight vector X data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Fight_Vectors_y()

	g_vBeastFinaleRootPosition.y = 3029.5410	//<<2405.8494, 3029.5410, 47.1526>>
	g_vBeastFinaleCheckPos.y =	3031.50			//<<2408.22, 3031.50, 47.15>>
	g_vBeastFinaleArenaPos.y =	3094.0444		//<<2386.0961,3094.0444,58.0271>> 
	g_vBeastFinaleArenaSize.y = 86.3125			//<<69.1875,86.3125,15.8750>>
	
	g_vBeastFinaleAnimalSpawnVec[0].y =	3113.5032	//<<2417.1050, 3113.5032, 47.1662>>
	g_vBeastFinaleAnimalSpawnVec[1].y =	3116.8059	//<<2407.0581, 3116.8059, 47.2119>>
	g_vBeastFinaleAnimalSpawnVec[2].y =	3080.0127	//<<2371.9197, 3080.0127, 47.1530>>
	g_vBeastFinaleAnimalSpawnVec[3].y =	3091.8440	//<<2399.5835, 3091.8440, 47.1531>>
	g_vBeastFinaleAnimalSpawnVec[4].y =	3045.7954	//<<2416.5190, 3045.7954, 47.1523>>
	g_vBeastFinaleAnimalSpawnVec[5].y =	3103.7461	//<<2430.3450, 3103.7461, 47.1531>>
	g_vBeastFinaleAnimalSpawnVec[6].y =	3031.4382	//<<2385.8186, 3031.4382, 47.1529>>
	
	g_vBeastFinaleStartPoint.y = 3058.12	//<<2399.82, 3058.12, 53.42>>
	g_vBeastFinaleJumpOffPoint.y = 3052.8	//<<2412.1, 3046.7, 48.2>>

	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Fight_y)
	CPRINTLN(debug_hunting, "BEAST: Set fight vector Y data, bitset: ",g_iBeastSetupInt)
ENDPROC


PROC Set_Beast_Fight_Vectors_z()

	g_vBeastFinaleRootPosition.z = 47.1526		//<<2405.8494, 3029.5410, 47.1526>>
	g_vBeastFinaleCheckPos.z =	47.15			//<<2408.22, 3031.50, 47.15>>
	g_vBeastFinaleArenaPos.z =	58.0271			//<<2386.0961,3094.0444,58.0271>> 
	g_vBeastFinaleArenaSize.z = 15.8750			//<<69.1875,86.3125,15.8750>>
	
	g_vBeastFinaleAnimalSpawnVec[0].z =	47.1662	//<<2417.1050, 3113.5032, 47.1662>>
	g_vBeastFinaleAnimalSpawnVec[1].z =	47.2119	//<<2407.0581, 3116.8059, 47.2119>>
	g_vBeastFinaleAnimalSpawnVec[2].z =	47.1530	//<<2371.9197, 3080.0127, 47.1530>>
	g_vBeastFinaleAnimalSpawnVec[3].z =	47.1531	//<<2399.5835, 3091.8440, 47.1531>>
	g_vBeastFinaleAnimalSpawnVec[4].z =	47.1523	//<<2416.5190, 3045.7954, 47.1523>>
	g_vBeastFinaleAnimalSpawnVec[5].z =	47.1531	//<<2430.3450, 3103.7461, 47.1531>>
	g_vBeastFinaleAnimalSpawnVec[6].z =	47.1529	//<<2385.8186, 3031.4382, 47.1529>>
	
	g_vBeastFinaleStartPoint.z = 53.42	//<<2399.82, 3058.12, 53.42>>
	g_vBeastFinaleJumpOffPoint.z = 48.2	//<<2412.1, 3046.7, 48.2>>
	
	SET_BITMASK(g_iBeastSetupInt,BH_Bit_Fight_z)
	CPRINTLN(debug_hunting, "BEAST: Set fight vector Z data, bitset: ",g_iBeastSetupInt)
ENDPROC



#IF IS_DEBUG_BUILD

	PROC Create_Decal_Widget(DecalPlacementData &sData, WIDGET_GROUP_ID parentGroup = NULL)
		IF parentGroup != NULL
			SET_CURRENT_WIDGET_GROUP(parentGroup)
		ENDIF
		
		//Set up defaults.
		sData.fHeading = 0.0
		sData.fWidth = 1.0
		sData.fHeight = 1.0
		sData.fColorR = 0.0
		sData.fColorG = 0.0
		sData.fColorB = 0.0
		sData.fColorA = 1.0
		sData.bLongRange = TRUE
		sData.bDynamic = FALSE
		sData.bComplexCollision = FALSE

		//Construct widget.
		START_WIDGET_GROUP("Decal Placement")
			ADD_WIDGET_STRING("Enable \"Script Debug Tools\" to use this feature")
			ADD_WIDGET_BOOL("Position with Mouse", sData.bUseMousePosition)
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Decal Settings")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("BLOOD_SPLATTER")
				ADD_TO_WIDGET_COMBO("BLOOD_DIRECTIONAL")
				ADD_TO_WIDGET_COMBO("FOOTPRINT_SAND")
				ADD_TO_WIDGET_COMBO("BULLET_IMPACT_METAL_SHOTGUN")
				ADD_TO_WIDGET_COMBO("FLOOR_SCUFF_1")
				ADD_TO_WIDGET_COMBO("FLOOR_SCUFF_2")
				ADD_TO_WIDGET_COMBO("FLOOR_SCUFF_3")
				ADD_TO_WIDGET_COMBO("FLOOR_SCUFF_4")
				ADD_TO_WIDGET_COMBO("LOGO_LOST_HOLLOW")
				ADD_TO_WIDGET_COMBO("LOGO_LOST_GREY")
				ADD_TO_WIDGET_COMBO("LOGO_LOST_WHITE")
				ADD_TO_WIDGET_COMBO("LOGO_COPS_WHITE")
				ADD_TO_WIDGET_COMBO("LOGO_VAGOS_WHITE")
				ADD_TO_WIDGET_COMBO("RACE_LOCATE_GROUND")
				ADD_TO_WIDGET_COMBO("RACE_LOCATE_AIR")
				ADD_TO_WIDGET_COMBO("RACE_LOCATE_WATER")
				ADD_TO_WIDGET_COMBO("TRIATHLON_CYCLING")
				ADD_TO_WIDGET_COMBO("TRIATHLON_RUNNING")
				ADD_TO_WIDGET_COMBO("TRIATHLON_SWIMMING")
				ADD_TO_WIDGET_COMBO("HELIPAD")
				ADD_TO_WIDGET_COMBO("PARACHUTE_LANDING")
				ADD_TO_WIDGET_COMBO("SHADOW")
				ADD_TO_WIDGET_COMBO("HALO")
				ADD_TO_WIDGET_COMBO("HALO_PT")
				ADD_TO_WIDGET_COMBO("MP_LOCATE_1")
				ADD_TO_WIDGET_COMBO("MP_LOCATE_2")
				ADD_TO_WIDGET_COMBO("MP_LOCATE_3")
				ADD_TO_WIDGET_COMBO("MP_CREATOR_TRIGGER")
				ADD_TO_WIDGET_COMBO("OIL_POOL")
			STOP_WIDGET_COMBO("Type", sData.iType)
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Position X", sData.vPosition.X, -6000.0, 6000.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Position Y", sData.vPosition.Y, -6000.0, 6000.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Position Z", sData.vPosition.Z, -6000.0, 6000.0, 0.001)

			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Heading", sData.fHeading, -360.0, 360.0, 0.001)
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Width", sData.fWidth, 0.001, 100.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Height", sData.fHeight, 0.001, 100.0, 0.001)
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Color R", sData.fColorR, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Color G", sData.fColorG, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Color B", sData.fColorB, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Color A", sData.fColorA, 0.0, 1.0, 0.001)

			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("LongRange", sData.bLongRange)
			ADD_WIDGET_BOOL("Dynamic", sData.bDynamic)
			ADD_WIDGET_BOOL("ComplexCollision", sData.bComplexCollision)
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Create", sData.bCreate)
			ADD_WIDGET_BOOL("Delete", sData.bDelete)
			ADD_WIDGET_BOOL("Write To File", sData.bWriteToFile)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Decal Created? (Readonly)", sData.bCreated)
		STOP_WIDGET_GROUP()

		IF parentGroup != NULL
			CLEAR_CURRENT_WIDGET_GROUP(parentGroup)
		ENDIF
	ENDPROC
	
	
	FUNC DECAL_RENDERSETTING_ID Get_Decal_Type_From_Index(INT index)
		SWITCH index
			CASE 0	RETURN DECAL_RSID_BLOOD_SPLATTER
			CASE 1	RETURN DECAL_RSID_BLOOD_DIRECTIONAL
			CASE 2	RETURN DECAL_RSID_FOOTPRINT_SAND
			CASE 3	RETURN DECAL_RSID_BULLET_IMPACT_METAL_SHOTGUN
			CASE 4	RETURN DECAL_RSID_FLOOR_SCUFF_1
			CASE 5	RETURN DECAL_RSID_FLOOR_SCUFF_2
			CASE 6	RETURN DECAL_RSID_FLOOR_SCUFF_3
			CASE 7	RETURN DECAL_RSID_FLOOR_SCUFF_4
			CASE 8	RETURN DECAL_RSID_LOGO_LOST_HOLLOW
			CASE 9	RETURN DECAL_RSID_LOGO_LOST_GREY
			CASE 10	RETURN DECAL_RSID_LOGO_LOST_WHITE
			CASE 11	RETURN DECAL_RSID_LOGO_COPS_WHITE
			CASE 12	RETURN DECAL_RSID_LOGO_VAGOS_WHITE
			CASE 13	RETURN DECAL_RSID_RACE_LOCATE_GROUND
			CASE 14	RETURN DECAL_RSID_RACE_LOCATE_AIR
			CASE 15	RETURN DECAL_RSID_RACE_LOCATE_WATER
			CASE 16	RETURN DECAL_RSID_TRIATHLON_CYCLING
			CASE 17	RETURN DECAL_RSID_TRIATHLON_RUNNING
			CASE 18	RETURN DECAL_RSID_TRIATHLON_SWIMMING
			CASE 19	RETURN DECAL_RSID_HELIPAD
			CASE 20	RETURN DECAL_RSID_PARACHUTE_LANDING
			CASE 21	RETURN DECAL_RSID_SHADOW
			CASE 22	RETURN DECAL_RSID_HALO
			CASE 23	RETURN DECAL_RSID_HALO_PT
			CASE 24	RETURN DECAL_RSID_MP_LOCATE_1
			CASE 25	RETURN DECAL_RSID_MP_LOCATE_2
			CASE 26	RETURN DECAL_RSID_MP_LOCATE_3
			CASE 27	RETURN DECAL_RSID_MP_CREATOR_TRIGGER
			CASE 28	RETURN DECAL_RSID_OIL_POOL
		ENDSWITCH
		
		SCRIPT_ASSERT("Get_Decal_Type_From_Index: Failed to find a type for passed index.")
		RETURN DECAL_RSID_BLOOD_SPLATTER
	ENDFUNC
	
	
	FUNC STRING Get_Decal_Type_String(DECAL_RENDERSETTING_ID type)
		SWITCH type
			CASE DECAL_RSID_BLOOD_SPLATTER				RETURN "DECAL_RSID_BLOOD_SPLATTER"
			CASE DECAL_RSID_BLOOD_DIRECTIONAL			RETURN "DECAL_RSID_BLOOD_DIRECTIONAL"
			CASE DECAL_RSID_FOOTPRINT_SAND				RETURN "DECAL_RSID_FOOTPRINT_SAND"
			CASE DECAL_RSID_BULLET_IMPACT_METAL_SHOTGUN	RETURN "DECAL_RSID_BULLET_IMPACT_METAL_SHOTGUN"
			CASE DECAL_RSID_FLOOR_SCUFF_1				RETURN "DECAL_RSID_FLOOR_SCUFF_1"
			CASE DECAL_RSID_FLOOR_SCUFF_2				RETURN "DECAL_RSID_FLOOR_SCUFF_2"
			CASE DECAL_RSID_FLOOR_SCUFF_3				RETURN "DECAL_RSID_FLOOR_SCUFF_3"
			CASE DECAL_RSID_FLOOR_SCUFF_4				RETURN "DECAL_RSID_FLOOR_SCUFF_4"
			CASE DECAL_RSID_LOGO_LOST_HOLLOW			RETURN "DECAL_RSID_LOGO_LOST_HOLLOW"
			CASE DECAL_RSID_LOGO_LOST_GREY				RETURN "DECAL_RSID_LOGO_LOST_GREY"
			CASE DECAL_RSID_LOGO_LOST_WHITE				RETURN "DECAL_RSID_LOGO_LOST_WHITE"
			CASE DECAL_RSID_LOGO_COPS_WHITE				RETURN "DECAL_RSID_LOGO_COPS_WHITE"
			CASE DECAL_RSID_LOGO_VAGOS_WHITE			RETURN "DECAL_RSID_LOGO_VAGOS_WHITE"
			CASE DECAL_RSID_RACE_LOCATE_GROUND			RETURN "DECAL_RSID_RACE_LOCATE_GROUND"
			CASE DECAL_RSID_RACE_LOCATE_AIR				RETURN "DECAL_RSID_RACE_LOCATE_AIR"
			CASE DECAL_RSID_RACE_LOCATE_WATER			RETURN "DECAL_RSID_RACE_LOCATE_WATER"
			CASE DECAL_RSID_TRIATHLON_CYCLING			RETURN "DECAL_RSID_TRIATHLON_CYCLING"
			CASE DECAL_RSID_TRIATHLON_RUNNING			RETURN "DECAL_RSID_TRIATHLON_RUNNING"
			CASE DECAL_RSID_TRIATHLON_SWIMMING			RETURN "DECAL_RSID_TRIATHLON_SWIMMING"
			CASE DECAL_RSID_HELIPAD						RETURN "DECAL_RSID_HELIPAD"
			CASE DECAL_RSID_PARACHUTE_LANDING			RETURN "DECAL_RSID_PARACHUTE_LANDING"
			CASE DECAL_RSID_SHADOW						RETURN "DECAL_RSID_SHADOW"
			CASE DECAL_RSID_HALO						RETURN "DECAL_RSID_HALO"
			CASE DECAL_RSID_HALO_PT						RETURN "DECAL_RSID_HALO_PT"
			CASE DECAL_RSID_MP_LOCATE_1					RETURN "DECAL_RSID_MP_LOCATE_1"
			CASE DECAL_RSID_MP_LOCATE_2					RETURN "DECAL_RSID_MP_LOCATE_2"
			CASE DECAL_RSID_MP_LOCATE_3					RETURN "DECAL_RSID_MP_LOCATE_3"
			CASE DECAL_RSID_MP_CREATOR_TRIGGER			RETURN "DECAL_RSID_MP_CREATOR_TRIGGER"
			CASE DECAL_RSID_OIL_POOL					RETURN "DECAL_RSID_OIL_POOL"
		ENDSWITCH
		
		SCRIPT_ASSERT("Get_Decal_Type_String: Failed to find a string for passed type.")
		RETURN "ERROR!"
	ENDFUNC
	
	
	FUNC VECTOR Get_Side_Vector_From_Heading(FLOAT fHeading)
		RETURN <<SIN(fHeading), -COS(fHeading), 0.0>>
	ENDFUNC
	
	
	PROC Update_Decal_Widget(DecalPlacementData &sData)
	
		//Report if a decal currently exists.
		sData.bCreated = (sData.id != NULL)

		//Create decal with current settings.
		IF sData.bCreate
			sData.bCreate = FALSE
			IF sData.id != NULL
				REMOVE_DECAL(sData.id)
				sData.id = NULL
			ENDIF
			sData.id = ADD_DECAL(	
				Get_Decal_Type_From_Index(sData.iType),
				sData.vPosition,
				<<0.0, 0.0, -1.0>>,
				Get_Side_Vector_From_Heading(sData.fHeading),
				sData.fWidth,
				sData.fHeight,
				sData.fColorR,
				sData.fColorG,
				sData.fColorB,
				sData.fColorA,
				-1,
				sData.bLongRange,
				sData.bDynamic,
				sData.bComplexCollision)
		ENDIF

		//Delete current decal.
		IF sData.bDelete
			sData.bDelete = FALSE
			IF sData.id != NULL
				REMOVE_DECAL(sData.id)
				sData.id = NULL
			ENDIF
		ENDIF
		
		IF sData.bUseMousePosition
			sData.vPosition = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
		ENDIF
		
		//Write to file.
		IF sData.bWriteToFile
			VECTOR vSide = Get_Side_Vector_From_Heading(sData.fHeading)
		
			OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("ADD_DECAL(")
				SAVE_STRING_TO_DEBUG_FILE(Get_Decal_Type_String(Get_Decal_Type_From_Index(sData.iType)))
				SAVE_STRING_TO_DEBUG_FILE(", <<")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.vPosition.X)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.vPosition.Y)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.vPosition.Z)
				SAVE_STRING_TO_DEBUG_FILE(">>, <<0.0, 0.0, -1.0>>, <<")
				SAVE_FLOAT_TO_DEBUG_FILE(vSide.X)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(vSide.Y)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(vSide.Z)
				SAVE_STRING_TO_DEBUG_FILE(">>, ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.fWidth)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.fHeight)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.fColorR)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.fColorG)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.fColorB)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sData.fColorA)
				SAVE_STRING_TO_DEBUG_FILE(", -1, ")
				SAVE_STRING_TO_DEBUG_FILE(PICK_STRING(sData.bLongRange, "TRUE", "FALSE"))
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_STRING_TO_DEBUG_FILE(PICK_STRING(sData.bDynamic, "TRUE", "FALSE"))
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_STRING_TO_DEBUG_FILE(PICK_STRING(sData.bComplexCollision, "TRUE", "FALSE"))
				SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
			sData.bWriteToFile = FALSE
		ENDIF
	
	ENDPROC

#ENDIF //IS_DEBUG_BUILD

#ENDIF //FEATURE_SP_DLC_BEAST_SECRET
