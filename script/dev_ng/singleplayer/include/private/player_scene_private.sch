USING "player_ped_scenes.sch"

///private header for player scene scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//  SCENE PRIVATE COORD FUNCTIONS
// *******************************************************************************************

FUNC BOOL Should_Switch_Cleanup_Be_Forced()

    IF g_bMagDemoActive
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                VEHICLE_INDEX switch_scene_veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
                IF IS_VEHICLE_DRIVEABLE(switch_scene_veh)
                    IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(switch_scene_veh)
                        
                        CPRINTLN(DEBUG_SWITCH, "!!! IS_PLAYBACK_GOING_ON_FOR_VEHICLE !!!")
                        
                        RETURN FALSE
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
    OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
    OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
        IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
        AND NOT Is_Player_Timetable_Scene_In_Progress()
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> Should_Switch_Cleanup_Be_Forced: mission is launched/mission flag true ")
            #ENDIF
            
            RETURN TRUE
        ENDIF
    ENDIF
    
    IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
        
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> Should_Switch_Cleanup_Be_Forced: mp hud is on screen ")
        #ENDIF
        
        RETURN TRUE
    ENDIF
    
    #IF USE_TU_CHANGES
    IF MPGlobals.g_bEndSingleplayerNow
        
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> Should_Switch_Cleanup_Be_Forced: g_bEndSingleplayerNow")
        
//      CASSERTLN(DEBUG_SWITCH, "Should_Switch_Cleanup_Be_Forced: g_bEndSingleplayerNow")
        #ENDIF
        
        RETURN TRUE
    ENDIF
    #ENDIF
    
//  IF (g_sPlayerPedRequest.eType = PR_TYPE_AMBIENT)
        IF (g_sPlayerPedRequest.eState = PR_STATE_PROCESSING)
            IF (g_sPlayerPedRequest.iStage = 0)
        
                #IF IS_DEBUG_BUILD
                CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> Should_Switch_Cleanup_Be_Forced: new SCRIPT switch has started ")
                #ENDIF
                
                RETURN TRUE
            ENDIF
        ENDIF
//  ENDIF
    
    IF IS_PLAYER_SWITCH_IN_PROGRESS()
        IF (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
            IF (GET_PLAYER_SWITCH_STATE() <= SWITCH_STATE_PREP_FOR_CUT)
            AND (GET_PLAYER_SWITCH_STATE() <> SWITCH_STATE_PREP_DESCENT)
        
                #IF IS_DEBUG_BUILD
                CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> Should_Switch_Cleanup_Be_Forced: new CODE switch has started ")
                #ENDIF
                
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    
    RETURN FALSE
ENDFUNC

FUNC BOOL ProgressScene(INT iPedBit, ENTITY_INDEX entityID)
    
    
    IF IS_PLAYER_SWITCH_IN_PROGRESS()
        IF (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
            IF (GET_PLAYER_SWITCH_STATE() <= SWITCH_STATE_PREP_FOR_CUT)
            AND (GET_PLAYER_SWITCH_STATE() <> SWITCH_STATE_PREP_DESCENT)
        
                #IF IS_DEBUG_BUILD
                CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ProgressScene: new CODE switch has started ")
                #ENDIF
                
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
        
        IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
        
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> STOP ProgressScene: currently on mission type director ")
            #ENDIF
            
            RETURN FALSE
        ENDIF
        
        IF GET_IS_PLAYER_IN_ANIMAL_FORM()
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> STOP ProgressScene: currently is in animal form.")
            #ENDIF
            RETURN FALSE
        ENDIF
        
        IF NOT IS_BITMASK_SET(iPedBit, GET_CURRENT_PLAYER_PED_BIT())
            #IF IS_DEBUG_BUILD
            CDEBUG3LN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> STOP ProgressScene: current ped changed ", GET_CURRENT_PLAYER_PED_STRING(), " <> ")
            
            BOOL bCharBitFound = FALSE
            IF IS_BITMASK_SET(iPedBit, BIT_MICHAEL)
                CDEBUG3LN(DEBUG_SWITCH, "BIT_MICHAEL")
                bCharBitFound = TRUE
            ENDIF
            IF IS_BITMASK_SET(iPedBit, BIT_FRANKLIN)
                IF bCharBitFound
                    CDEBUG3LN(DEBUG_SWITCH, "|")
                ENDIF
                CDEBUG3LN(DEBUG_SWITCH, "BIT_FRANKLIN")
                bCharBitFound = TRUE
            ENDIF
            IF IS_BITMASK_SET(iPedBit, BIT_TREVOR)
                IF bCharBitFound
                    CDEBUG3LN(DEBUG_SWITCH, "|")
                ENDIF
                CDEBUG3LN(DEBUG_SWITCH, "BIT_TREVOR")
                bCharBitFound = TRUE
            ENDIF
            IF NOT bCharBitFound
                CDEBUG3LN(DEBUG_SWITCH, "BIT_unknown")
            ENDIF
            #ENDIF
            
            RETURN FALSE
        ENDIF
        
        IF DOES_ENTITY_EXIST(entityID)
            
            VECTOR vEntityCoord = GET_ENTITY_COORDS(entityID, FALSE)
            FLOAT fDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEntityCoord)
            
            IF fDist > 250.0
                IF NOT IS_ENTITY_DEAD(entityID)
                    IF NOT IS_ENTITY_ON_SCREEN(entityID)
                        
                        #IF IS_DEBUG_BUILD
                        CDEBUG3LN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> STOP ProgressScene: current entity distance ", fDist)
                        #ENDIF
                        
                        RETURN FALSE
                    ENDIF
                ELSE
                    IF NOT IS_SPHERE_VISIBLE(vEntityCoord, 1.5)
                        
                        #IF IS_DEBUG_BUILD
                        CDEBUG3LN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> STOP ProgressScene: current entity distance ", fDist)
                        #ENDIF
                        
                        RETURN FALSE
                    ENDIF
                ENDIF
            ENDIF
        ELSE
        
            IF (entityID = GET_ENTITY_FROM_PED_OR_VEHICLE(g_pScene_buddy))
            AND (entityID != NULL)
                CASSERTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ProgressScene: clean up non existant pedID for #2327327 (entityID: ", NATIVE_TO_INT(entityID), ")")
                g_pScene_buddy = NULL
            ENDIF
        ENDIF
    ENDIF
    
    RETURN TRUE
ENDFUNC 



// *******************************************************************************************
//  SCENE PRIVATE EVENT FUNCTIONS
// *******************************************************************************************


FUNC BOOL IsPlayerMovingLeftStick(INT &iDrawSceneRot, INT iLEFT_STICK_THRESHOLD = 64)
    
    INT ReturnLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_X) -128
    INT ReturnLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_Y) -128
    
    IF ReturnLeftX < iLEFT_STICK_THRESHOLD
    AND ReturnLeftX > -iLEFT_STICK_THRESHOLD
    AND ReturnLeftY < iLEFT_STICK_THRESHOLD
    AND ReturnLeftY > -iLEFT_STICK_THRESHOLD
        
        //
        #IF IS_DEBUG_BUILD
        TEXT_LABEL_63 strStick
        strStick  = "left stick("
        strStick += ReturnLeftX
        strStick += ", "
        strStick += ReturnLeftY
        strStick += ")"
        
        DrawLiteralSceneString(strStick, iDrawSceneRot, HUD_COLOUR_BLUEDARK)
        #ENDIF
        iDrawSceneRot++
        //
        
    ELSE
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL IsPlayerUsingAccelerateOrBrake(VEHICLE_INDEX vehIndex, INT &iDrawSceneRot)
    
    IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
        RETURN FALSE
    ENDIF
    
    CONTROL_ACTION eInputVehAccelerate = INPUT_VEH_ACCELERATE
    IF (IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehIndex)))
        eInputVehAccelerate = INPUT_SPRINT
    ENDIF
    
    IF IS_CONTROL_PRESSED(PLAYER_CONTROL, eInputVehAccelerate)
    OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
        RETURN TRUE
    ELSE
        
        //
        #IF IS_DEBUG_BUILD
        
        INT ReturnLeftX = GET_CONTROL_VALUE(PLAYER_CONTROL,eInputVehAccelerate)
        INT ReturnLeftY = GET_CONTROL_VALUE(PLAYER_CONTROL,INPUT_VEH_BRAKE)
        
        TEXT_LABEL_63 strStick
        strStick  = "acc:"
        strStick += ReturnLeftX
        strStick += " brake:"
        strStick += ReturnLeftY
        
        DrawLiteralSceneString(strStick, iDrawSceneRot, HUD_COLOUR_BLUEDARK)
        #ENDIF
        iDrawSceneRot++
        //
        
    ENDIF
    
    RETURN FALSE
ENDFUNC

// *******************************************************************************************
//  SCENE PRIVATE PAUSE FUNCTIONS
// *******************************************************************************************

//PAUSE_BEFORE_PAN 
PROC ar_ALLOW_PLAYER_SWITCH_PAN()
    
    IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_PAN() if player switch not in progress")
        #ENDIF
        
        EXIT
    ENDIF
    
    IF (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_PAN() during SWITCH_TYPE_SHORT")
        #ENDIF
        
        EXIT
    ENDIF
    
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ar_ALLOW_PLAYER_SWITCH_PAN()")
    #ENDIF

    ALLOW_PLAYER_SWITCH_PAN()
ENDPROC

//PAUSE_BEFORE_OUTRO
PROC ar_ALLOW_PLAYER_SWITCH_OUTRO()
    
    IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
        IF IS_SCREEN_FADED_OUT()
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") if player switch not in progress [g_iPauseOnOutro:", g_iPauseOnOutro, "] - screen faded out")
//          CASSERTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") if player switch not in progress [g_iPauseOnOutro:", g_iPauseOnOutro, "] - screen faded out")
            #ENDIF
        
            CLEAR_Pause_Outro_Count()
        ELSE
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") if player switch not in progress [g_iPauseOnOutro:", g_iPauseOnOutro, "] - screen faded in")
            //Steve T removed this assert only for bug 2484303. Looks like all similar asserts in this proc had been previously removed, so safe enough to remove this one too.
            //CASSERTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") if player switch not in progress [g_iPauseOnOutro:", g_iPauseOnOutro, "] - screen faded in")
            #ENDIF
        ENDIF
        
        EXIT
    ENDIF
    
    IF (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") during SWITCH_TYPE_SHORT [g_iPauseOnOutro:", g_iPauseOnOutro, "]")
//      CASSERTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") during SWITCH_TYPE_SHORT [g_iPauseOnOutro:", g_iPauseOnOutro, "]")
        #ENDIF
        
        CLEAR_Pause_Outro_Count()
        
        EXIT
    ENDIF
    
    DECRAMENT_Pause_Outro_Count()
    IF (g_iPauseOnOutro > 0)
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") g_iPauseOnOutro is still greater than zero [g_iPauseOnOutro:", g_iPauseOnOutro, "]")
//      CASSERTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> cannot call ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") g_iPauseOnOutro is still greater than zero [g_iPauseOnOutro:", g_iPauseOnOutro, "]")
        #ENDIF
        
        EXIT
    ENDIF
    
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> ar_ALLOW_PLAYER_SWITCH_OUTRO(", Get_String_From_Ped_Request_Scene_Enum(g_eRecentlySelectedScene), ") //g_iPauseOnOutro:", g_iPauseOnOutro)
    #ENDIF

    ALLOW_PLAYER_SWITCH_OUTRO()
ENDPROC

// *******************************************************************************************
//  SCENE PRIVATE OVERRIDE FUNCTIONS
// *******************************************************************************************

PROC SET_PED_SWITCH_OVERRIDECLF(enumCharacterList ePed, VECTOR vPos, FLOAT fHeading)
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> SET_PED_SWITCH_OVERRIDE(", GET_PLAYER_PED_STRING(ePed), ", ", vPos, ", ", fHeading, ")")
    #ENDIF
    
    SWITCH ePed
        CASE CHAR_MICHAEL
            g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_M_OVERRIDE
        BREAK
        CASE CHAR_FRANKLIN
            g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_F_OVERRIDE
        BREAK
        CASE CHAR_TREVOR
            g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_T_OVERRIDE
        BREAK
        
        DEFAULT
            
            #IF IS_DEBUG_BUILD
            TEXT_LABEL_63 str
            str = "invalid ePed in SET_PED_SWITCH_OVERRIDE: "
            str += GET_PLAYER_PED_STRING(ePed)
            
            CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
            CASSERTLN(DEBUG_SWITCH, str)
            #ENDIF
            
            EXIT
        BREAK
    ENDSWITCH
    
    g_sOverrideScene[ePed].ePed = ePed
    g_sOverrideScene[ePed].eScene = g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed]
    g_sOverrideScene[ePed].vCreateCoords = vPos
    g_sOverrideScene[ePed].fCreateHead = fHeading
    
ENDPROC
PROC SET_PED_SWITCH_OVERRIDENRM(enumCharacterList ePed, VECTOR vPos, FLOAT fHeading)
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> SET_PED_SWITCH_OVERRIDE(", GET_PLAYER_PED_STRING(ePed), ", ", vPos, ", ", fHeading, ")")
    #ENDIF
    
    SWITCH ePed
        CASE CHAR_MICHAEL
            g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_M_OVERRIDE
        BREAK
        CASE CHAR_FRANKLIN
            g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_F_OVERRIDE
        BREAK
        CASE CHAR_TREVOR
            g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_T_OVERRIDE
        BREAK
        
        DEFAULT
            
            #IF IS_DEBUG_BUILD
            TEXT_LABEL_63 str
            str = "invalid ePed in SET_PED_SWITCH_OVERRIDE: "
            str += GET_PLAYER_PED_STRING(ePed)
            
            CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
            CASSERTLN(DEBUG_SWITCH, str)
            #ENDIF
            
            EXIT
        BREAK
    ENDSWITCH
    
    g_sOverrideScene[ePed].ePed = ePed
    g_sOverrideScene[ePed].eScene = g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed]
    g_sOverrideScene[ePed].vCreateCoords = vPos
    g_sOverrideScene[ePed].fCreateHead = fHeading
    
ENDPROC
PROC SET_PED_SWITCH_OVERRIDE(enumCharacterList ePed, VECTOR vPos, FLOAT fHeading)
#if USE_CLF_DLC
    if g_bLoadedClifford
        SET_PED_SWITCH_OVERRIDECLF(ePed,vPos,fHeading)
        EXIT
    endif
#endif
#if USE_NRM_DLC
    if g_bLoadedNorman
        SET_PED_SWITCH_OVERRIDENRM(ePed)
        EXIT
    endif
#endif  
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> SET_PED_SWITCH_OVERRIDE(", GET_PLAYER_PED_STRING(ePed), ", ", vPos, ", ", fHeading, ")")
    #ENDIF
    
    SWITCH ePed
        CASE CHAR_MICHAEL
            g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_M_OVERRIDE
        BREAK
        CASE CHAR_FRANKLIN
            g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_F_OVERRIDE
        BREAK
        CASE CHAR_TREVOR
            g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_T_OVERRIDE
        BREAK
        
        DEFAULT
            
            #IF IS_DEBUG_BUILD
            TEXT_LABEL_63 str
            str = "invalid ePed in SET_PED_SWITCH_OVERRIDE: "
            str += GET_PLAYER_PED_STRING(ePed)
            
            CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
            CASSERTLN(DEBUG_SWITCH, str)
            #ENDIF
            
            EXIT
        BREAK
    ENDSWITCH
    
    g_sOverrideScene[ePed].ePed = ePed
    g_sOverrideScene[ePed].eScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
    g_sOverrideScene[ePed].vCreateCoords = vPos
    g_sOverrideScene[ePed].fCreateHead = fHeading
    
ENDPROC
PROC CLEAR_PED_SWITCH_OVERRIDECLF(enumCharacterList ePed)
    
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> CLEAR_PED_SWITCH_OVERRIDE(", GET_PLAYER_PED_STRING(ePed), ")")
    #ENDIF
    
    IF (g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_M_OVERRIDE)
    AND (g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_F_OVERRIDE)
    AND (g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_T_OVERRIDE)
        
        #IF IS_DEBUG_BUILD
        TEXT_LABEL_63 str
        str = "invalid ePed in CLEAR_PED_SWITCH_OVERRIDE: "
        str += GET_PLAYER_PED_STRING(ePed)
        
        CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str, ", ", Get_String_From_Ped_Request_Scene_Enum(g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed]))
        
        CASSERTLN(DEBUG_SWITCH, str)
        #ENDIF
        
        EXIT
    ENDIF
    
    g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
    
    g_sOverrideScene[ePed].ePed = NO_CHARACTER
    g_sOverrideScene[ePed].eScene = PR_SCENE_INVALID
    g_sOverrideScene[ePed].vCreateCoords = <<0,0,0>>
    g_sOverrideScene[ePed].fCreateHead = 0
    
ENDPROC
PROC CLEAR_PED_SWITCH_OVERRIDENRM(enumCharacterList ePed)
    
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> CLEAR_PED_SWITCH_OVERRIDE(", GET_PLAYER_PED_STRING(ePed), ")")
    #ENDIF
    
    IF (g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_M_OVERRIDE)
    AND (g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_F_OVERRIDE)
    AND (g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_T_OVERRIDE)
        
        #IF IS_DEBUG_BUILD
        TEXT_LABEL_63 str
        str = "invalid ePed in CLEAR_PED_SWITCH_OVERRIDE: "
        str += GET_PLAYER_PED_STRING(ePed)
        
        CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str, ", ", Get_String_From_Ped_Request_Scene_Enum(g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed]))
        
        CASSERTLN(DEBUG_SWITCH, str)
        #ENDIF
        
        EXIT
    ENDIF
    
    g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
    
    g_sOverrideScene[ePed].ePed = NO_CHARACTER
    g_sOverrideScene[ePed].eScene = PR_SCENE_INVALID
    g_sOverrideScene[ePed].vCreateCoords = <<0,0,0>>
    g_sOverrideScene[ePed].fCreateHead = 0
    
ENDPROC
PROC CLEAR_PED_SWITCH_OVERRIDE(enumCharacterList ePed)

    #IF USE_CLF_DLC
        IF g_bLoadedClifford
            CLEAR_PED_SWITCH_OVERRIDECLF(ePed)
            EXIT
        ENDIF
    #ENDIF
    #IF USE_NRM_DLC
        IF g_bLoadedNorman
            CLEAR_PED_SWITCH_OVERRIDENRM(ePed)
            EXIT
        ENDIF
    #ENDIF  

    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> CLEAR_PED_SWITCH_OVERRIDE(", GET_PLAYER_PED_STRING(ePed), ")")
    #ENDIF
    
    IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_M_OVERRIDE)
    AND (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_F_OVERRIDE)
    AND (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] <> PR_SCENE_T_OVERRIDE)
        
        #IF IS_DEBUG_BUILD
        TEXT_LABEL_63 str
        str = "invalid ePed in CLEAR_PED_SWITCH_OVERRIDE: "
        str += GET_PLAYER_PED_STRING(ePed)
        
        CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str, ", ", Get_String_From_Ped_Request_Scene_Enum(g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]))
        
        CASSERTLN(DEBUG_SWITCH, str)
        #ENDIF
        
        EXIT
    ENDIF
    
    g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
    
    g_sOverrideScene[ePed].ePed = NO_CHARACTER
    g_sOverrideScene[ePed].eScene = PR_SCENE_INVALID
    g_sOverrideScene[ePed].vCreateCoords = <<0,0,0>>
    g_sOverrideScene[ePed].fCreateHead = 0
    
ENDPROC

#IF IS_DEBUG_BUILD
PROC DrawLiteralSceneTitle(PED_REQUEST_SCENE_ENUM eScene, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE, FLOAT fAlphaMult = 1.0)
    
    STRING sScene = Get_String_From_Ped_Request_Scene_Enum(eScene)
    DrawLiteralSceneString(GET_STRING_FROM_STRING(sScene,
                GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
                GET_LENGTH_OF_LITERAL_STRING(sScene)),
                iColumn, eColour, fAlphaMult)
    
ENDPROC
#ENDIF

