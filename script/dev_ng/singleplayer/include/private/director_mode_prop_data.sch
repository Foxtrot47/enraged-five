USING "globals.sch"
USING "rage_builtins.sch"

//enums
ENUM enum_propedit_state
    PES_SceneSelect,
    PES_SceneEdit,
    PES_PropPlace,
    PES_PropList
ENDENUM

#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT //You can't include feature flag conditions inside the enum, has to wrap it.
ENUM enum_Category
    CAT_Null,
    CAT_Barrier,
    CAT_Benches,
    CAT_Cabin,
    CAT_Construction,
    CAT_Containers,
    CAT_Crates,
    CAT_Dumpsters,
    CAT_Explosive,
    CAT_Machinery,
    CAT_Ramp,
    CAT_Natural,
    CAT_Roadside,

	CAT_StuntRamps,
	CAT_StuntTubes,
	CAT_StuntDynamic,
	CAT_StuntTrack,
	CAT_StuntTyres,
	CAT_StuntMisc,
	CAT_StuntWalls,
	CAT_StuntExtraTrk,
	CAT_StuntBlocks,


    CAT_Trailers,
    CAT_Water,
    CAT_Wreckage,
    CAT_MAX
ENDENUM
#ENDIF

#IF NOT FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT // You can't include feature flag conditions inside the enum, has to wrap it.
ENUM enum_Category
    CAT_Null,
    CAT_Barrier,
    CAT_Benches,
    CAT_Cabin,
    CAT_Construction,
    CAT_Containers,
    CAT_Crates,
    CAT_Dumpsters,
    CAT_Explosive,
    CAT_Machinery,
    CAT_Ramp,
    CAT_Natural,
    CAT_Roadside,
    CAT_Trailers,
    CAT_Water,
    CAT_Wreckage,
    CAT_MAX
ENDENUM
#ENDIF

ENUM enum_PropWarning
    PW_NotReady,
    PW_Fine,
    PW_Colliding,
    PW_WaterPropOnLand,
    PW_PropInInterior,
    PW_LandPropOnWater,
    PW_AboveGround,
    PW_MaxDynamicProps,
    PW_MaxProps,
    PW_PropBiggerThanWaterDepth,
    PW_RestrictedArea,
    PW_RestrictedUse,
    PW_MAX
ENDENUM

//Consts
CONST_INT ciMAXOBJECTS          50      //Max number of objects - this was at 100 but have decreased this to 50 at the request of John Whyte in bug 2416882
                                        //THIS NEEDS TO BE SYNCHED TO ci_MAXPropObjectsPerScene IN director_mode_globals.sch
										
#IF	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT

#IF NOT FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	
	CONST_INT ciMAXTYPES           511
	//THIS CONST SHOULD NO LONGER BE ALTERED. Increment or decrement the const wrapped in the special races additions feature flag below.
	//Was 512 - removed stt_prop_stunt_tube_jmp2 for 3199133
	//Was 513 - removed dynamic stop sign for 3199133
	//Was 518 - removed rotating dynamic tube types for 3199133
	//Was 519 - removed prop for 3199647//Was 520 - removed stt_prop_stunt_target
	//Was 521 - removed dupe for bug 3194627
	//Was 525 but removed four dupes for bug 3192435
	//Was 527 but removed two dupes for bug 3191366 
#ENDIF

#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS		
	//Important! Special Race Increment or decrement this const for for every prop added or removed.
	CONST_INT ciMAXTYPES        600
	//Was 601 - removed duplicate Neon Block Extra Large sr_Prop_Special_BBlock_XL3 for 3471218
	//Was 602 - removed sr_prop_sr_tube_wall for 3471805
	//Was 599 - Add three new landing strip targest
	//Was 602 - Removed the bullseye targets. No labels associated and add very little value other than being annoying to scroll through!
	//Was 599 - Added 3 new landing bullseye targets. 
	//Was 594 - Added 5 new landing pad points targets.
	//Was 596 - Removed the Blimp sign and the Take Flight sign that were just added for technical reasons.
	//Was 592 - Added 4 new signs to stuntMisc.
	//Was 587 - Added three track, refills, a tube refill and a track jump wall.
	//Was 579 - Added 8 new track pieces.
	//Was 564 - Added fifteen new corners tubes
	//Was 560 - Added four new L tubes
	//Was 556 - Added four new M tubes
	//Was 552 - Added four new S tubes
	//Was 548 - Added four new XS tubes
	//Was 544 - Added four new XXS tubes
	//Was 534 - Added ten corner tubes.
	//Was 529 - Added five new special tubes.
	//Was 520 - Added another nine neon blocks
	//Was 516 - Added four neon blocks.
	//Was 511 - added sr_prop_sr_boxpile_02, sr_prop_sr_boxpile_03, sr_prop_sr_track_wall, sr_prop_sr_tube_wall, and sr_mp_spec_races_ammu_sign to stunt misc category for feature flag test.
	
#ENDIF

#ENDIF





#IF NOT FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
CONST_INT ciMAXTYPES            231		//231 was the number of prop types available before the stunt props were included. Leave as is!
#ENDIF

CONST_INT ciMAXSCENES           4       //THIS ALSO NEEDS SYNCHING WITH ci_MAXPROPSCENES
CONST_INT ciPROPADDLINES        3       //1st n lines which add new props
CONST_INT ciPROPSCENELINES      1
CONST_INT ciMAXDYNAMICOBJECTS   25

//PI menu consts
CONST_INT ciPIPropPlace         0
CONST_INT ciPIPropList          1
CONST_INT ciPISceneSave         2
CONST_INT ciPISceneCopy         3
CONST_INT ciPISceneClear        4

CONST_INT ciPIPlaceCat          0
CONST_INT ciPIPlaceType         1

//Structs
STRUCT DM_PROP
    VECTOR          vPos
    VECTOR          vRot
	VECTOR			vSize
    FLOAT           fZoffset
    MODEL_NAMES     mModel
    OBJECT_INDEX    obj
    BLIP_INDEX      BlipIndex   //Work on 2485910
ENDSTRUCT




STRUCT DM_SCENE
    VECTOR          vPos
    VECTOR          vRot
    MODEL_NAMES     mModel
ENDSTRUCT

STRUCT DM_SCENE_DATA
    INT             iTotalProps
    INT             iDynamicProps
    TEXT_LABEL_15   tlModeLocation
    TEXT_LABEL_23   tlSceneName
ENDSTRUCT

STRUCT DM_PROPTYPE
    TEXT_LABEL_15   name
    enum_Category   category
    MODEL_NAMES     model
    BOOL            bFrozen         //If prop should be fixed to the ground or is mobile
    BOOL            bPlaceOnGround  //Place prop safely on the ground
	BOOL			bIgnoreCollision//Ignores all collision checks
    FLOAT           fIgnoreZ        //Allow prop placement up to a fixed distance off the ground
    FLOAT           fMoverAdjust    //Certain prop types require some ground adjustment before placing to account for any model setup weirdness. See #2412969 
    FLOAT           fGroundAdjust   //Certain prop types require some ground adjustment to account for any model setup weirdness. See #2412969 
    FLOAT           fMarkerAdjust   //Also add height adjustment for chevron marker.
    FLOAT           fGroundScale    //Adjustment for the Z check on the base
    FLOAT           fPfxScale       //Adjustment for the scale of dust cloud or splash when the prop is placed
ENDSTRUCT

//Prop editor state
enum_propedit_state ePEState, eLastPEState
//Prop type arrays
TEXT_LABEL_15       eCategoryNames[CAT_MAX]
INT                 ePropTypesPerCategory[CAT_MAX]
DM_PROPTYPE         ePropTypes[ciMAXTYPES]
INT                 eSelectedPropType[2]
INT                 iCurrentProp
BOOL                bEditingProp            = FALSE

//Object array
DM_PROP             eProps[ciMAXOBJECTS]
INT                 iNumProps
INT                 iNumDynamicProps
BLIP_INDEX          blSelected
INT                 iBlippedProp            = -1
INT                 iForceReplaceProp       = -1        //If this is != -1, next added prop will be at that index


// Restricting the number of problematic log and pipe pile props for 2499306 and 2503897
INT                 iNumRestrictedUseProps  = 0
CONST_INT           c_i_RestrictedUsePropLimit 5//10 - Steve T - Should be okay to alter this for 3190881, legacy saved scenes with 10 props should still be able to be loaded but future scenes will only allow 5.

CONST_INT           c_i_BlippedPropAlphaThisFrame 160

VECTOR              v_MarkerDraw            = <<-99.9, -99.9, -99.9>> //Used for drawing chevron.
INT                 iLastPropSelectionForChevronMarker  //keeps track of the last prop selection for updating the chevron periodically.

//Scene arrays
INT                 iSelectedScene          = 0
INT                 iCurrentScene           = -1
DM_SCENE            eScenes[ciMAXSCENES][ciMAXOBJECTS]
DM_SCENE_DATA       sdSceneData[ ciMAXSCENES ]
BOOL                bLoadSceneRunning       = FALSE
BOOL                bSkipFadeOut            = FALSE
INT                 iSceneObjectToLoad      = 0

//Mover vars
DM_Prop             moverProp
BOOL                bMoverCreated           = FALSE
BOOL				bMoverPtfxCreated		= FALSE
FLOAT               fMoverHeading
FLOAT               fMoverDist              = 3
FLOAT               fMoverMinDist           = 3
FLOAT               fMoverMaxDist           = 15
BOOL                b_MoverPropOverWater    = FALSE
FLOAT				fMoverMaxZOffset		= 30
FLOAT				fMoverZOffsetStep		= 0.1


//Shapetest vars
SHAPETEST_INDEX     siTest = NULL
INT                 iSTResult
INT                 iLastResultTimer
VECTOR              vLastCheckedShapeTestPos, vTestLastPos, vOldMoverPosition
BOOL                bLastResult
VECTOR              vModelMin
VECTOR              vModelMax
FLOAT               fWaterZ, fOldMoverHeading

//Other vars
BOOL                b_LockOut_PropHeightControls            = FALSE   //Prop Height manipulation lockout for bug 2410298
BOOL                b_PropSceneUpdatedSinceLastSave         = FALSE//Used to decide if the exclamation mark should be displayed beside "Scene" in the menu to indicate a change has been made to the current scene. See 2409831
BOOL                b_PropEditorRequestsImmediateMenuReset  = FALSE
STRING              sPropEditorHelpString                   = "RemovePropMenuHelp"
enum_PropWarning    ePropWarning                            = PW_NotReady
INT                 iLastPISelection                        = -1
VEHICLE_INDEX       vPropEditVeh                            = NULL
INT					iPropPlaceTimer							= 0		//Timer to lock-out placing long props in a series
INT 				iPropPlaceTimerMin						= 200	//Min time to lock out chain prop placement

//Scene overview vars
INT                 iPrevSelectedScene                      = -2    //Now using -2 for "no scene"
FLOAT               fRadarZoom                              = 100.0
FLOAT               fDefaultRadarZoom                       = 100.0
BOOL                bPropSceneInfoMiniMapActive             = FALSE
BLIP_INDEX          biScenePropBlips[ ciMAXOBJECTS ]

BOOL                b_DoNotProcessNthLabel                  = FALSE

#IF IS_DEBUG_BUILD
BOOL    bRefreshValues  = FALSE
FLOAT   fChevronY       = 2.0
FLOAT   fGroundY        = 0.0
#ENDIF

FUNC STRING GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL(MODEL_NAMES mn)

    IF mn = PROP_CONST_FENCE02B                                             RETURN "FMMC_PR_0"          ENDIF /*Construction Fence*/            
    IF mn = PROP_OFFROAD_BALE03                                             RETURN "FMMC_PR_1"          ENDIF /*Left Bale*/                 
    IF mn = PROP_OFFROAD_BALE02                                             RETURN "FMMC_PR_2"          ENDIF /*Right Bale*/                    
    IF mn = PROP_OFFROAD_BALE01                                             RETURN "FMMC_PR_3"          ENDIF /*Single Bale*/                   
    IF mn = PROP_OFFROAD_TYRES01                                            RETURN "FMMC_PR_4"          ENDIF /*Tire Line*/                 
    IF mn = PROP_OFFROAD_TYRES02                                            RETURN "FMMC_PR_5"          ENDIF /*Tire Stack*/                    
    IF mn = Prop_Barier_Conc_02a                                            RETURN "FMMC_PR_6"          ENDIF /*Med Concrete*/              
    IF mn = Prop_Barier_Conc_05c                                            RETURN "FMMC_PR_7"          ENDIF /*Med Striped Concrete*/      
    IF mn = Prop_Barier_Conc_05a                                            RETURN "FMMC_PR_8"          ENDIF /*Long Concrete*/             
    IF mn = Prop_Barier_Conc_05b                                            RETURN "FMMC_PR_9"          ENDIF /*Long Striped Concrete*/     
    IF mn = prop_barier_conc_01a                                            RETURN "FMMC_PR_10"         ENDIF /*Short Concrete*/
    IF mn = prop_bench_05                                                   RETURN "FMMC_PR_12"         ENDIF /*Concrete Bench*/                
    IF mn = prop_bench_07                                                   RETURN "FMMC_PR_13"         ENDIF /*Wooden Bench*/              
    IF mn = prop_bench_01a                                                  RETURN "FMMC_PR_14"         ENDIF /*Metal Bench*/                   
    IF mn = PROP_BENCH_08                                                   RETURN "FMMC_PR_BNBLUE"     ENDIF /*Blue Bench*/                    
    IF mn = Prop_Dock_Bouy_1                                                RETURN "FMMC_PR_15"         ENDIF /*Red Buoy*/                  
    IF mn = PROP_DOCK_BOUY_2                                                RETURN "FMMC_PR_16"         ENDIF /*Green Buoy*/                    
    IF mn = prop_elecbox_24                                                 RETURN "FMMC_PR_17"         ENDIF /*Large Corrugated Cabin*/    
    IF mn = prop_elecbox_24b                                                RETURN "FMMC_PR_18"         ENDIF /*Small Corrugated Cabin*/    
    IF mn = prop_portacabin01                                               RETURN "FMMC_PR_19"         ENDIF /*Cabin with Windows*/            
    IF mn = PROP_CEMENTBAGS01                                               RETURN "FMMC_PR_20"         ENDIF /*Cement Bags*/                   
    IF mn = prop_conc_blocks01a                                             RETURN "FMMC_PR_21"         ENDIF /*Concrete Blocks*/               
    IF mn = PROP_CONS_CRATE                                                 RETURN "FMMC_PR_22"         ENDIF /*Construction Bin*/          
    IF mn = prop_jyard_block_01a                                            RETURN "FMMC_PR_23"         ENDIF /*Large Concrete Block*/      
    IF mn = prop_conc_sacks_02a                                             RETURN "FMMC_PR_24"         ENDIF /*Sandbags*/                  
    IF mn = prop_byard_sleeper01                                            RETURN "FMMC_PR_25"         ENDIF /*Sleepers*/                  
    IF mn = Prop_Shuttering01                                               RETURN "FMMC_PR_26"         ENDIF /*Low Med Shuttering*/            
    IF mn = Prop_Shuttering02                                               RETURN "FMMC_PR_27"         ENDIF /*High Med Shuttering*/           
    IF mn = Prop_Shuttering03                                               RETURN "FMMC_PR_28"         ENDIF /*High Long Shuttering*/      
    IF mn = Prop_Shuttering04                                               RETURN "FMMC_PR_29"         ENDIF /*Low Long Shuttering*/           
    IF mn = prop_woodpile_01a                                               RETURN "FMMC_PR_30"         ENDIF /*Flat Wood Pile*/                
    IF mn = prop_woodpile_01c                                               RETURN "FMMC_PR_31"         ENDIF /*Uneven Wood Pile*/  
    IF mn = prop_rub_cont_01b                                               RETURN "FMMC_PR_32"         ENDIF /*Empty Container*/               
    IF mn = prop_rail_boxcar4                                               RETURN "FMMC_PR_33"         ENDIF /*Open Yellow Carriage*/      
    IF mn = prop_rub_railwreck_2                                            RETURN "FMMC_PR_34"         ENDIF /*Open Brown Carriage*/           
    IF mn = Prop_Contr_03b_LD                                               RETURN "FMMC_PR_35"         ENDIF /*Small Closed Container*/    
    IF mn = prop_container_ld2                                              RETURN "FMMC_PR_36"         ENDIF /*Large Closed Container*/    
    IF mn = prop_rail_boxcar                                                RETURN "FMMC_PR_37"         ENDIF /*Closed Yellow Carriage*/    
    IF mn = prop_rail_boxcar3                                               RETURN "FMMC_PR_38"         ENDIF /*Closed Brown Carriage*/     
    IF mn = prop_byard_floatpile                                            RETURN "FMMC_PR_39"         ENDIF /*Float Pile*/                    
    IF mn = prop_boxpile_07A                                                RETURN "DMPP_FRAGILE"       ENDIF /*Fragile Marked box pile*/ //2500497 - Duplicate name removed by Steve T.        
    IF mn = prop_watercrate_01                                              RETURN "FMMC_PR_41"         ENDIF /*Water Crate*/                   
    IF mn = PROP_BOX_WOOD01A                                                RETURN "FMMC_PR_42"         ENDIF /*Small Wooden Box*/          
    IF mn = PROP_BOX_WOOD03A                                                RETURN "FMMC_PR_43"         ENDIF /*Med Wooden Box*/                
    IF mn = prop_box_wood04a                                                RETURN "FMMC_PR_44"         ENDIF /*Large Wooden Box*/          
    IF mn = PROP_CASH_CRATE_01                                              RETURN "FMMC_PR_45"         ENDIF /*Cash Crate*/                    
    IF mn = prop_bin_13a                                                    RETURN "FMMC_PR_46"         ENDIF /*Wheelie Dumpster*/          
    IF mn = prop_bin_14a                                                    RETURN "FMMC_PR_47"         ENDIF /*Open Dumpster*/             
    IF mn = prop_dumpster_3a                                                RETURN "FMMC_PR_48"         ENDIF /*Med Metal Dumpster*/            
    IF mn = prop_dumpster_4b                                                RETURN "FMMC_PR_49"         ENDIF /*Large Metal Dumpster*/      
    IF mn = PROP_DUMPSTER_01A                                               RETURN "FMMC_PR_50"         ENDIF /*Small Metal Dumpster*/      
    IF mn = prop_skip_06a                                                   RETURN "FMMC_PR_51"         ENDIF /*Large Dumpster*/                
    IF mn = prop_elecbox_02a                                                RETURN "FMMC_PR_52"         ENDIF /*Electrics Box*/             
    IF mn = prop_elecbox_16                                                 RETURN "FMMC_PR_53"         ENDIF /*Electrics Container*/           
    IF mn = prop_elecbox_14                                                 RETURN "FMMC_PR_54"         ENDIF /*Generator*/                 
    IF mn = PROP_IND_DEISELTANK                                             RETURN "FMMC_PR_55"         ENDIF /*Diesel Tank*/                   
    IF mn = prop_ind_mech_02a                                               RETURN "FMMC_PR_56"         ENDIF /*Mechanical Box Set*/            
    IF mn = prop_ind_mech_02b                                               RETURN "FMMC_PR_57"         ENDIF /*Mechanical Box*/                
    IF mn = prop_sub_trans_01a                                              RETURN "FMMC_PR_58"         ENDIF /*Large Transformer*/         
    IF mn = prop_sub_trans_02a                                              RETURN "FMMC_PR_59"         ENDIF /*Small Transformer*/         
    IF mn = prop_sub_trans_04a                                              RETURN "FMMC_PR_60"         ENDIF /*Med Transformer*/
    IF mn = prop_skip_04                                                    RETURN "FMMC_PR_62"         ENDIF /*Small Dumpster Ramp*/
    IF mn = Prop_MP_Ramp_01 OR mn = LTS_PROP_LTS_RAMP_01                    RETURN "FMMC_PR_61"         ENDIF /*Small Ramp*/
    IF mn = Prop_MP_Ramp_02 OR mn = LTS_PROP_LTS_RAMP_02                    RETURN "FMMC_PR_63"         ENDIF /*Medium Ramp*/
    IF mn = Prop_MP_Ramp_03 OR mn = LTS_PROP_LTS_RAMP_03                    RETURN "FMMC_PR_64"         ENDIF /*Large Ramp*/
    IF mn = prop_skip_08a                                                   RETURN "FMMC_PR_65"         ENDIF /*Large Dumpster Ramp*/
    IF mn = PROP_JETSKI_RAMP_01                                             RETURN "FMMC_PR_66"         ENDIF /*Water Ramp*/
    IF mn = Prop_TrafficDiv_01                                              RETURN "FMMC_PR_67"         ENDIF /*Diversion Sign*/
    IF mn = prop_sign_road_09a                                              RETURN "FMMC_PR_68"         ENDIF /*Right Chevron Road Sign*/
    IF mn = prop_sign_road_09b                                              RETURN "FMMC_PR_69"         ENDIF /*Left Chevron  Road Sign*/
    IF mn = prop_sign_road_09c                                              RETURN "FMMC_PR_70"         ENDIF /*Small Go Left Road Sign*/
    IF mn = prop_sign_road_09d                                              RETURN "FMMC_PR_71"         ENDIF /*Small Go Right Road Sign*/
    IF mn = prop_sign_road_06q                                              RETURN "FMMC_PR_72"         ENDIF /*Large Go Left Road Sign*/
    IF mn = prop_sign_road_06r                                              RETURN "FMMC_PR_73"         ENDIF /*Large Go Right Road Sign*/
    IF mn = prop_sign_road_05c                                              RETURN "FMMC_PR_74"         ENDIF /*Left Bend Road Sign*/
    IF mn = prop_sign_road_05d                                              RETURN "FMMC_PR_75"         ENDIF /*Right Bend Road Sign*/
    IF mn = prop_sign_road_05e                                              RETURN "FMMC_PR_76"         ENDIF /*Next Left Road Sign*/
    IF mn = prop_sign_road_05f                                              RETURN "FMMC_PR_77"         ENDIF /*Next Right Road Sign*/
    IF mn = Prop_food_van_01                                                RETURN "FMMC_PR_78"         ENDIF /*Hotdog Trailer*/
    IF mn = Prop_food_van_02                                                RETURN "FMMC_PR_79"         ENDIF /*Snacks Trailer*/
    IF mn = prop_tanktrailer_01a                                            RETURN "FMMC_PR_80"         ENDIF /*Tanker Trailer*/
    IF mn = prop_truktrailer_01a                                            RETURN "FMMC_PR_81"         ENDIF /*Truck Trailer*/
    IF mn = prop_rub_buswreck_01                                            RETURN "FMMC_PR_82"         ENDIF /*Half Wrecked Bus*/          
    IF mn = prop_rub_buswreck_06                                            RETURN "FMMC_PR_83"         ENDIF /*Full Wrecked Bus*/          
    IF mn = prop_rub_carwreck_11                                            RETURN "FMMC_PR_84"         ENDIF /*Wrecked Red Car*/               
    IF mn = prop_rub_carwreck_12                                            RETURN "FMMC_PR_85"         ENDIF /*Wrecked White Car*/         
    IF mn = prop_rub_carwreck_13                                            RETURN "FMMC_PR_86"         ENDIF /*Wrecked Flipped Car*/           
    IF mn = prop_rub_carwreck_9                                             RETURN "FMMC_PR_87"         ENDIF /*Wrecked Van*/
    
    IF mn = PROP_FNCLINK_03GATE5                                            RETURN "FMMC_PR_FNCMGTS"    ENDIF //Single Metal Fence Gate
    IF mn = PROP_FNCLINK_02GATE3                                            RETURN "FMMC_PR_FNCMGTD"    ENDIF //Double Metal Fence Gate
    IF mn = PROP_PLAS_BARIER_01A                                            RETURN "FMMC_PR_BARPRED"    ENDIF //Red Plastic Barrier
    IF mn = PROP_BARIER_CONC_02B                                            RETURN "FMMC_PR_BARCANW"    ENDIF //High Concrete Barrier
    IF mn = PROP_BARRIER_WORK06A                                            RETURN "FMMC_PR_BARWRKP"    ENDIF //Plastic Works Barrier
    IF mn = PROP_BARRIER_WORK04A                                            RETURN "FMMC_PR_BARWRKW"    ENDIF //Wooden Works Barrier 
    IF mn = PROP_FNCLINK_06A                                                RETURN "FMMC_PR_FNCBWSG"    ENDIF //Single Barbed Wire Fence
    IF mn = PROP_FNCLINK_06B                                                RETURN "FMMC_PR_FNCBWLN"    ENDIF //Barbed Wire Fence Link
    IF mn = PROP_FNCLINK_06C                                                RETURN "FMMC_PR_FNCBWWD"    ENDIF //Double Barbed Wire Fence
    IF mn = PROP_FNCLINK_06D                                                RETURN "FMMC_PR_FNCBWWL"    ENDIF //Double Barbed Wire Link
    IF mn = PROP_FNCCORGM_03A                                               RETURN "FMMC_PR_FNCCDCF"    ENDIF //Double Corrugated Fence
    IF mn = PROP_FNCCORGM_03B                                               RETURN "FMMC_PR_FNCCPDF"    ENDIF //Patched Double Fence
    IF mn = PROP_FNCCORGM_03C                                               RETURN "FMMC_PR_FNCCSCF"    ENDIF //Single Corrugated Fence
    IF mn = PROP_FNCCORGM_02A                                               RETURN "FMMC_PR_FNCCRDF"    ENDIF //Rusty Double Fence
    IF mn = PROP_FNCCORGM_02B                                               RETURN "FMMC_PR_FNCCPRF"    ENDIF //Patched Rusty Fence
    IF mn = PROP_FNCCORGM_02C                                               RETURN "FMMC_PR_FNCCBPF"    ENDIF //Broken Patched Fence
    IF mn = PROP_FNCCORGM_02D                                               RETURN "FMMC_PR_FNCCTCF"    ENDIF //Triple Corrugated Fence
    IF mn = PROP_FNCCORGM_02E                                               RETURN "FMMC_PR_FNCCSRF"    ENDIF //Single Rusty Fence
    //IF mn = PROP_GATE_CULT_01_L                                             RETURN "FMMC_PR_FNCGTAL"    ENDIF //Left Altruist Fence
    //IF mn = PROP_GATE_CULT_01_R                                             RETURN "FMMC_PR_FNCGTAR"    ENDIF //Right Altruist Fence
    IF mn = PROP_CONST_FENCE03B_CR                                          RETURN "FMMC_PR_BARQADB"    ENDIF //Long Blue Barrier
    IF mn = PROP_CONST_FENCE02A                                             RETURN "FMMC_PR_BARDUBU"    ENDIF //Double Unpainted Barrier
    IF mn = PROP_CONST_FENCE01B_CR                                          RETURN "FMMC_PR_BARSINB"    ENDIF //Single Blue Barrier
    IF mn = PROP_FNCWOOD_16B                                                RETURN "FMMC_PR_FNCPKOD"    ENDIF //Double Old Picket Fence
    IF mn = PROP_FNCWOOD_16C                                                RETURN "FMMC_PR_FNCPKOS"    ENDIF //Single Old Picket Fence
    IF mn = PROP_FNC_FARM_01B                                               RETURN "FMMC_PR_FNCFMS"     ENDIF //Single Farm Fence
    IF mn = PROP_FNC_FARM_01C                                               RETURN "FMMC_PR_FNCFMSL"    ENDIF //Single Farm Fence Link
    IF mn = PROP_FNC_FARM_01D                                               RETURN "FMMC_PR_FNCFMD"     ENDIF //Double Farm Fence
    IF mn = PROP_FNC_FARM_01E                                               RETURN "FMMC_PR_FNCFMT"     ENDIF //Triple Farm Fence
    IF mn = PROP_FNC_FARM_01F                                               RETURN "FMMC_PR_FNCFMSX"    ENDIF //Long Farm Fence     
    IF mn = PROP_IND_BARGE_01_CR                                            RETURN "FMMC_PR_BARGE"      ENDIF //Floating Platform           
    IF mn = PROP_TOLLBOOTH_1                                                RETURN "FMMC_PR_CABTBTH"    ENDIF //Toll Booth
    IF mn = PROP_PARKING_HUT_2                                              RETURN "FMMC_PR_CABPHUT"    ENDIF //Parking Hut
    IF mn = PROP_WOODPILE_01B                                               RETURN "FMMC_PR_WODPLSM"    ENDIF //Small Wood Pile
    IF mn = PROP_WOODPILE_03A                                               RETURN "FMMC_PR_WODPLUT"    ENDIF //Unsecured Wood Pile
    IF mn = PROP_CONC_BLOCKS01C                                             RETURN "FMMC_PR_CONCCND"    ENDIF //Cinder Blocks
    IF mn = PROP_CONS_CEMENTS01                                             RETURN "FMMC_PR_CONCSAK"    ENDIF //Cement Sacks
    IF mn = PROP_CONTAINER_01MB                                             RETURN "FMMC_PR_CNTLNGG"    ENDIF //Long Green Container
    IF mn = PROP_CONTAINER_03MB                                             RETURN "FMMC_PR_CNTSHTG"    ENDIF //Short Green Container
    IF mn = PROP_PALLET_PILE_02                                             RETURN "FMMC_PR_PLTPILL"    ENDIF //Large Pallet Pile
    IF mn = PROP_PALLET_PILE_01                                             RETURN "FMMC_PR_PLTPILS"    ENDIF //Small Pallet Pile
    //IF mn = PROP_CRATEPILE_07A                                              RETURN "FMMC_PR_CRTPILL"    ENDIF //Crate Pile  Behave weirdly during deletion 2504295
    IF mn = PROP_BOXPILE_04A                                                RETURN "FMMC_PR_BOXPILW"    ENDIF //Wrapped Box Pile
    IF mn = PROP_DUMPSTER_02A                                               RETURN "FMMC_PR_DMPCLDB"    ENDIF //Closed Blue Dumpster
    IF mn = PROP_DUMPSTER_02B                                               RETURN "FMMC_PR_DMPCLDM"    ENDIF //Closed Brown Dumpster
    IF mn = PROP_ELECBOX_10_CR                                              RETURN "FMMC_PR_ELECBXW"    ENDIF //Wide Electrics Box
    IF mn = PROP_ELECBOX_15_CR                                              RETURN "FMMC_PR_ELCBXGN"    ENDIF //Green Electrics Box
    IF mn = PROP_ELECBOX_17_CR                                              RETURN "FMMC_PR_ELCBXGY"    ENDIF //Grey Electrics Box
    IF mn = PROP_GENERATOR_03B                                              RETURN "FMMC_PR_GANDLMP"    ENDIF //Generator & Lamp
    IF mn = PROP_FEEDER1_CR                                                 RETURN "FMMC_PR_FEEDER"     ENDIF //Cattle Feeder       
    IF mn = PROP_SKIP_03                                                    RETURN "FMMC_PR_RMPDMPM"    ENDIF //Medium Dumpster Ramp
    //IF mn = PROP_BYARD_RAMPOLD_CR                                           RETURN "FMMC_PR_RMPOLD"     ENDIF //Old Wooden Ramp - Removed for PROP_BYARD_RAMPOLD_CR
    IF mn = PROP_SKATE_HALFPIPE_CR                                          RETURN "FMMC_PR_RMPHP"      ENDIF //Half Pipe
    IF mn = PROP_SKATE_QUARTPIPE_CR                                         RETURN "FMMC_PR_RMPQP"      ENDIF //Quarter Pipe
    IF mn = PROP_SKATE_SPINER_CR                                            RETURN "FMMC_PR_RMPFRS"     ENDIF //Flat Ramp With Spine
    IF mn = PROP_SKATE_FLATRAMP_CR                                          RETURN "FMMC_PR_RMPLFR"     ENDIF //Flat Ramp
    IF mn = PROP_SKATE_KICKERS_CR                                           RETURN "FMMC_PR_RMPFRK"     ENDIF //Flat Ramp With Kicker
    IF mn = PROP_SKATE_FUNBOX_CR                                            RETURN "FMMC_PR_RMPFBP"     ENDIF //Pyramid Fun Box
    IF mn = PROP_PILE_DIRT_07_CR                                            RETURN "FMMC_PR_RMPPILE"    ENDIF //Dirt Pile Ramp
    IF mn = PROP_OLD_FARM_03                                                RETURN "FMMC_PR_FRMOLDT"    ENDIF //Old Farm Trailer
    IF mn = PROP_RUB_CARWRECK_14                                            RETURN "FMMC_PR_WRKCRRD"    ENDIF //Red Car Wreck
    IF mn = PROP_RUB_COUCH02                                                RETURN "FMMC_PR_WRKCCH"     ENDIF //Old Couch
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_TREE_OLIVE_CR2"))           RETURN "FMMC_PR_NTROLVT"    ENDIF //Olive Tree
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_TREE_ENG_OAK_CR2"))         RETURN "FMMC_PR_NTROAKT"    ENDIF //Oak Tree
    IF mn = PROP_TREE_FALLEN_02                                             RETURN "FMMC_PR_NTRFLNT"    ENDIF //Fallen Tree
    
    
    IF mn = PROP_HAYB_ST_01_CR                                              RETURN "FMMC_PR_HBSTK"      ENDIF //Hay Bale Stack
    IF mn = PROP_HAYBALE_03                                                 RETURN "FMMC_PR_HBRND"      ENDIF //Hay Bale Round
    IF mn = PROP_HAYBALE_02                                                 RETURN "FMMC_PR_HBSSTK"     ENDIF //Hay Stack
    IF mn = PROP_HAYBALE_01                                                 RETURN "FMMC_PR_HBSML"      ENDIF //Small Hay Stack
    IF mn = PROP_BYARD_FLOAT_02                                             RETURN "FMMC_PR_FLOATD"     ENDIF //Double Float
    
    IF mn = PROP_TYRE_WALL_01                                               RETURN "FMMC_PR_TYR1"       ENDIF //Small Tire Wall (Stripe)
    IF mn = PROP_TYRE_WALL_02                                               RETURN "FMMC_PR_TYR2"       ENDIF //Medium Tire Wall (Stripe)
    IF mn = PROP_TYRE_WALL_03                                               RETURN "FMMC_PR_TYR3"       ENDIF //Large Tire Wall (Stripe)
    IF mn = PROP_TYRE_WALL_04                                               RETURN "FMMC_PR_TYR4"       ENDIF //Small Tire Wall (Arrows)
    IF mn = PROP_TYRE_WALL_05                                               RETURN "FMMC_PR_TYR5"       ENDIF //Medium Tire Wall (Arrows)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Tyre_Wall_01b"))            RETURN "FMMC_PR_TYR1B"      ENDIF //Small Tire Wall (Black)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Tyre_Wall_02b"))            RETURN "FMMC_PR_TYR2B"      ENDIF //Medium Tire Wall (Black)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Tyre_Wall_03b"))            RETURN "FMMC_PR_TYR3B"      ENDIF //Large Tire Wall (Black)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Tyre_Wall_01c"))            RETURN "FMMC_PR_TYR1C"      ENDIF //Small Tire Wall (Yellow)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Tyre_Wall_02c"))            RETURN "FMMC_PR_TYR2C"      ENDIF //Medium Tire Wall (Yellow)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Tyre_Wall_03c"))            RETURN "FMMC_PR_TYR3C"      ENDIF //Large Tire Wall (Yellow)
    
    IF mn = PROP_OFFROAD_TYRES01                                            RETURN "FMMC_PR_ORT1"       ENDIF //Offroad Tyres Stack
    IF mn = PROP_OFFROAD_TYRES02                                            RETURN "FMMC_PR_ORT2"       ENDIF //Offroad Tyres Stack 2
    
    IF mn = PROP_PIPES_CONC_01                                              RETURN "FMMC_PR_CNPPE"      ENDIF /*Pipe*/
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_START_GATE_01B"))           RETURN "FMMC_PR_SGTE"       ENDIF //Starting Gate
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Makeup_Trail_01_CR"))       RETURN "FMMC_PR_SMTR"       ENDIF //Makeup Trailer  
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Makeup_Trail_02_CR"))       RETURN "FMMC_PR_MUTR"       ENDIF //Makeup Trailer(extended)
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_bleachers_04_CR"))          RETURN "FMMC_PR_SBLE"       ENDIF //Bleachers (small)   
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_bleachers_05_CR"))          RETURN "FMMC_PR_BLEA"       ENDIF //Bleachers
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Beachf_01_CR"))             RETURN "FMMC_PR_BEAF"       ENDIF //Beach Flag
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_SET_Generator_01_CR"))      RETURN "DMPP_LRGEGEN"       ENDIF //Generator     //2500509 Duplicate name fixed by Steve T
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_BLASTFENCE_01"))        RETURN  "FMMC_PR_FNCBLST"   ENDIF //Blast Fence
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_01"))          RETURN  "FMMC_PR_SNDBKSI"   ENDIF //Single Sand Block
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_02"))          RETURN  "FMMC_PR_SNDBKTR"   ENDIF //Triple Sand Block
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_05_CR"))       RETURN  "FMMC_PR_SNDBKED"   ENDIF //Sand Block Edge
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_04"))          RETURN  "FMMC_PR_SNDBKCR"   ENDIF //Sand Block Corner
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_03_CR"))       RETURN  "FMMC_PR_SNDBKST"   ENDIF //Sand Block Stack
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_HESCO_06"))              RETURN  "FMMC_PR_SNDBKFT"   ENDIF //Sand Block Fort 
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_CONST_FENCE03A_CR"))        RETURN  "FMMC_PR_BARWDQU"   ENDIF //Long Unpainted Barrier
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_MONHUT_03_CR"))         RETURN  "FMMC_PR_CABAPHT"   ENDIF //Runway Hut
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_SECHUT_01"))            RETURN  "FMMC_PR_CABSCHT"   ENDIF //Security Hut
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_03a"))             RETURN  "FMMC_PR_CRG01"     ENDIF //Cargo 1
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))             RETURN  "FMMC_PR_CRG02"     ENDIF //Cargo 2
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))            RETURN  "FMMC_PR_CRG03"     ENDIF //Cargo 3
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))             RETURN  "FMMC_PR_CRG04"     ENDIF //Cargo 4
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_01a"))            RETURN  "FMMC_PR_CRGAIR"    ENDIF //Air Cargo
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_CARGO_04B"))             RETURN  "FMMC_PR_CRG05"     ENDIF //Cargo 5
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_CARGO_02A"))             RETURN  "FMMC_PR_CRG06"     ENDIF //Cargo 6
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_TAXISIGN_01A"))         RETURN  "FMMC_PR_SINAPTX"   ENDIF //Runway Sign
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_STAIR_01"))             RETURN  "FMMC_PR_STRFLY"    ENDIF //FlyUs Stair Trailer
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_STAIR_04A_CR"))         RETURN  "FMMC_PR_STRRSD"    ENDIF //Raised Stair Trailer
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_STAIR_04B_CR"))         RETURN  "FMMC_PR_STRLWR"    ENDIF //Lowered Stair Trailer
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_BAGLOADER"))            RETURN  "FMMC_PR_BAGLDL"    ENDIF //Low Bag Loader      
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_BAGLOADER2_CR"))        RETURN  "FMMC_PR_BAGLDH"    ENDIF //High Bag Loader
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_PLANT_GROUP_04_CR"))        RETURN  "FMMC_PR_PLNTGP"    ENDIF //Plant Group
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BUSH_LRG_01C_CR"))          RETURN  "FMMC_PR_BUSHLD"    ENDIF //Large Bush
    //IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BUSH_LRG_01E_CR2"))         RETURN  "FMMC_PR_BUSHM"     ENDIF //Medium Bush  //Causing too many problems See bug 2485860.
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BUSH_MED_03_CR2"))          RETURN  "FMMC_PR_BUSHS"     ENDIF //Small Bush
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_JOSHUA_TREE_01A"))          RETURN  "FMMC_PR_JOTREE"    ENDIF //Joshua Tree
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_CACTUS_01E"))               RETURN  "FMMC_PR_CACTUS"    ENDIF //Cactus
    //IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_LOG_BREAK_01"))             RETURN  "FMMC_PR_TREFLN"    ENDIF //Fallen Tree - Removed for PROP_LOG_BREAK_01
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_05C"))            RETURN  "FMMC_PR_PLNTAP"    ENDIF //Potted Aloe
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_04C"))            RETURN  "FMMC_PR_PLNTPP"    ENDIF //Potted Palm
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_05D"))            RETURN  "FMMC_PR_PLNTFW"    ENDIF //Round Grey Planter
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_03B_CR2"))        RETURN  "FMMC_PR_PLNTCF"    ENDIF //Round Red Planter
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_04B"))            RETURN  "FMMC_PR_PLNTTC"    ENDIF //Tall Cactus Planter     
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_ROCK_4_BIG2"))              RETURN  "FMMC_PR_RCKBGR"    ENDIF //Large Round Rock
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_ROCK_4_C_2"))               RETURN  "FMMC_PR_RCKMDF"    ENDIF //Medium Rock
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_ROCK_4_BIG"))               RETURN  "FMMC_PR_RCKBGF"    ENDIF //Large Flat Rock
    
    //dynamic
    IF mn = PROP_OFFROAD_BARREL01                                           RETURN "FMMC_DPR_BARREL"    ENDIF   //Barrel
    IF mn = PROP_OFFROAD_BARREL02                                           RETURN "FMMC_DPR_BRLLNE"    ENDIF   //Barrel Line
    IF mn = PROP_BARREL_EXP_01A                                             RETURN "FMMC_DPR_EXPBRL"    ENDIF   //Explosive Barrel
    IF mn = PROP_FIRE_EXTING_1B                                             RETURN "FMMC_DPR_FIREXT"    ENDIF   //Fire Extinguisher
    IF mn = PROP_ROADCONE02C                                                RETURN "FMMC_DPR_CONE"      ENDIF   //Cone
    IF mn = PROP_ROADCONE02A                                                RETURN "FMMC_DPR_TRFCNE"    ENDIF   //Traffic Cone
    IF mn = PROP_ROADCONE01A                                                RETURN "FMMC_DPR_LTRFCN"    ENDIF   //Large Traffic Cone
    IF mn = PROP_ROADPOLE_01A                                               RETURN "FMMC_DPR_TRFPLE"    ENDIF   //Traffic Pole
    IF mn = PROP_POSTBOX_01A                                                RETURN "FMMC_DPR_MALBOX"    ENDIF   //Mail Box
    IF mn = PROP_NEWS_DISP_02D                                              RETURN "FMMC_DPR_NPVND"     ENDIF   //Newspaper Machine
    IF mn = PROP_VEND_WATER_01                                              RETURN "FMMC_DPR_WVND"      ENDIF   //Water Machine
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_vend_snak_01_TU"))          RETURN "FMMC_DPR_MCHSNK"    ENDIF   //Snack Machine
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Train_ticket_02_TU"))       RETURN "FMMC_DPR_MCHTCK"    ENDIF   //Ticket Machine
    IF mn = PROP_BOXPILE_02B                                                RETURN "FMMC_PR_40"         ENDIF   //Pile of Boxes   //2515890 - fixed Uppercase "Of".
    IF mn = PROP_MC_CONC_BARRIER_01                                         RETURN "FMMC_DPR_DESBAR"    ENDIF   //Destructible Barrier
    IF mn = PROP_FNCSEC_03B                                                 RETURN "FMMC_DPR_SECFEN"    ENDIF   //Security Fence
    IF mn = PROP_TABLE_08_SIDE                                              RETURN "FMMC_DPR_UPTTBL"    ENDIF   //Upturned Table
    IF mn = PROP_CONTAINER_LD_PU                                            RETURN "FMMC_DPR_AMOCRT"    ENDIF   //Ammunition Crate
    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_ORDNANCE_02"))           RETURN "FMMC_DPR_ORDNAN"    ENDIF   //Ordnance Pile
    IF mn = PROP_STORAGETANK_02B                                            RETURN "FMMC_DPR_TKRDEX"    ENDIF   //Explosive Red Tank
    IF mn = PROP_LOGPILE_01                                                 RETURN "FMMC_DPR_LGPLEL"    ENDIF   //Large Log Pile
    IF mn = PROP_LOGPILE_03                                                 RETURN "FMMC_DPR_LGPLES"    ENDIF   //Small Log Pile
    IF mn = PROP_PIPES_02B                                                  RETURN "FMMC_DPR_PIPPLE"    ENDIF   //Pipe Pile
    IF mn = PROP_BARREL_PILE_01                                             RETURN "FMMC_DPR_BRLPLE"    ENDIF   //Barrel Pile
    IF mn = PROP_BARREL_EXP_01B                                             RETURN "FMMC_DPR_BRGOEX"    ENDIF   //Globe Oil Barrel
    IF mn = PROP_BARREL_EXP_01C                                             RETURN "FMMC_DPR_BRFLEX"    ENDIF   //Banded Explosive Barrel
    IF mn = PROP_GAS_TANK_02A                                               RETURN "FMMC_DPR_TKLRGW"    ENDIF   //Large White Gas Tank
    IF mn = PROP_GAS_TANK_04A                                               RETURN "FMMC_DPR_TKLRGG"    ENDIF   //Large Green Gas Tank
    IF mn = PROP_GAS_TANK_02B                                               RETURN "FMMC_DPR_TKLRGY"    ENDIF   //Large Grey Gas Tank
    IF mn = PROP_JERRYCAN_01A                                               RETURN "FMMC_DPR_JRYCAN"    ENDIF   //Jerry Can
    IF mn = prop_gascyl_01a                                                 RETURN "FMMC_DPR_SGC"       ENDIF   //Small Gas Cylinder
    IF mn = Prop_GasCyl_04A                                                 RETURN "FMMC_DPR_LGC"       ENDIF   //Large Gas Cylinder
    IF mn = Prop_GasCyl_03A                                                 RETURN "FMMC_DPR_TBGAS"     ENDIF   //Narrow Blue Top Gas Cylinder
    IF mn = PROP_GASCYL_03B                                                 RETURN "FMMC_DPR_TRGAS"     ENDIF   //Narrow Red Top Gas Cylinder
    IF mn = PROP_GASCYL_02A                                                 RETURN "FMMC_DPR_OLGAS"     ENDIF   //Large Old Gas Cyclinder
    IF mn = PROP_GASCYL_02B                                                 RETURN "FMMC_DPR_OSGAS"     ENDIF   //Medium Old Gas Cylinder
    IF mn = PROP_FRUITSTAND_B                                               RETURN "FMMC_DPR_FSTND"     ENDIF   //Fruit Stand
    IF mn = PROP_RUB_TYRE_03                                                RETURN "FMMC_DPR_RBRTYR"    ENDIF   //Rubber Tire
    IF mn = PROP_BARREL_02B                                                 RETURN "FMMC_DPR_BAR2"      ENDIF   //Barrel 2
    IF mn = PROP_BARREL_01A                                                 RETURN "FMMC_DPR_BAR3"      ENDIF   //Barrel 3
    
    IF mn = PROP_LD_ALARM_01                                                RETURN "FMMC_PR_ALARM"      ENDIF   //Alarm
    IF mn = PROP_FLARE_01                                                   RETURN "FMMC_PR_FLARE"      ENDIF   //Flare
    IF mn = PROP_BARRIER_WORK05                                             RETURN "FMMC_PR_PBARR"      ENDIF   //Police Barrier
    IF mn = IND_PROP_DLC_FLAG_02                                            RETURN "FMMC_PR_AMFLG"      ENDIF   //American Flag
    IF mn = HEI_PROP_BANK_PLUG                                              RETURN "FMMC_PR_BPLUG"      ENDIF   //Bank Plug
    IF mn = HEI_PROP_WALL_ALARM_ON                                          RETURN "FMMC_PR_AMLG1"      ENDIF   //Alarm Light (On)
    IF mn = HEI_PROP_WALL_ALARM_OFF                                         RETURN "FMMC_PR_AMLG2"      ENDIF   //Alarm Light (Off)
    IF mn = HEI_PROP_HEI_CASH_TROLLY_03                                     RETURN "FMMC_PR_CASHT"      ENDIF   //Cash Trolley (Empty)
    IF mn = HEI_PROP_CARRIER_DOCKLIGHT_01                                   RETURN "FMMC_PR_CARL2"      ENDIF   //Carrier Lights (Set of 2)
    IF mn = HEI_PROP_CARRIER_DOCKLIGHT_02                                   RETURN "FMMC_PR_CARL5"      ENDIF   //Carrier Lights (Set of 5)
    IF mn = HEI_PROP_WALL_LIGHT_10A_CR                                      RETURN "FMMC_PR_WALLL"      ENDIF   //Wall Light
    IF mn = HEI_PROP_HEIST_APECRATE                                         RETURN "FMMC_PR_MNKCR"      ENDIF   //Monkey Crate
    IF mn = HEI_PROP_CC_METALCOVER_01                                       RETURN "FMMC_PR_METCV"      ENDIF   //Metal Cover
    IF mn = HEI_PROP_BANK_ALARM_01                                          RETURN "FMMC_PR_BNKAL"      ENDIF   //Bank Alarm
    IF mn = PROP_ROAD_MEMORIAL_02                                           RETURN "FMMC_PR_RDMEM"      ENDIF   //Road Memorial


    IF mn = prop_inflategate_01                                             RETURN "DMPP_INFLAT"        ENDIF   //Inflated Race Gates. This label is new and from AmericanReplay.txt.
	
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT // Taken from mulitplayer\include\public\freemode\fmmc_vars.sch
	
	
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_m"))                               RETURN "MC_PR_STNT173"         ENDIF   //Ramp Flip Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_mb"))                             RETURN "MC_PR_STNT32"          ENDIF   //Ramp Flip Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_s"))                                RETURN "MC_PR_STNT193"         ENDIF   //Ramp Flip Small Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_sb"))                              RETURN "MC_PR_STNT31"          ENDIF   //Ramp Flip Small Left
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_xs"))                                             RETURN "MC_PR_STNT25"         ENDIF   //Ramp XS
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_s"))                                               RETURN "MC_PR_STNT26"         ENDIF   //Ramp Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_m"))                                             RETURN "MC_PR_STNT27"         ENDIF   //Ramp Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_l"))                                                RETURN "MC_PR_STNT28"         ENDIF   //Ramp Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_xl"))                                              RETURN "MC_PR_STNT29"         ENDIF   //Ramp XL
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_xxl"))                                RETURN "MC_PR_STNT30"          ENDIF   //Ramp XXL

			

	
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_hloop"))		RETURN "MC_PR_STNT19"	ENDIF	//Half Loop
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_loop"))			RETURN "MC_PR_STNT20"	ENDIF	//Full Loop
	
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_xs"))			RETURN "MC_PR_STNT37"	ENDIF	//Tube Short
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_s"))				RETURN "MC_PR_STNT38"	ENDIF	//Tube Medium
	
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_lball"))		RETURN "MC_PR_STNT65"		ENDIF	//Football Large
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_sball"))    	RETURN "MC_PR_STNT66"      ENDIF   //Football Small
   			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_ball"))    	 RETURN "MC_PR_STNT68"     	ENDIF   //Football Medium
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowling_pin"))   		RETURN "MC_PR_STNT63"    	ENDIF   //Bowling Pin
    		IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowling_ball"))   	RETURN "MC_PR_STNT261"     ENDIF   //Bowling Ball
			

	
	
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_multi_loop_rb"))             RETURN "MC_PR_STNT21"          ENDIF   //Multi Loop 
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_loop"))                              RETURN "MC_PR_STNT319"         ENDIF   //Ramp Loop


            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_01a"))                                RETURN "MC_PR_STNT156"         ENDIF   //Ramp Large Wide
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_01b"))                               RETURN "MC_PR_STNT157"         ENDIF   //Ramp XL Wide
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_01c"))                                RETURN "MC_PR_STNT158"         ENDIF   //Ramp XXL Wide
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_02a"))                                RETURN "MC_PR_STNT159"         ENDIF   //Ramp Stunt Large Wide
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_02b"))                               RETURN "MC_PR_STNT160"         ENDIF   //Ramp Stunt XL Wide
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_02c"))                                RETURN "MC_PR_STNT161"         ENDIF   //Ramp Stunt XXL Wide

		
			//End of Tuesday work.
			
			
			//Begin Wednesday work.
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_start"))                                       RETURN "MC_PR_STNT59"          ENDIF   //Track Start Grid 16
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_start_02"))                                  RETURN "MC_PR_STNT2"            ENDIF   //Track Start Grid 30
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_constraction_01a"))        RETURN "MC_PR_STNT189"         ENDIF   //Hoop of Fire Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_tannoy"))                                    RETURN "MC_PR_STNT255"         ENDIF   //Alarm
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_01"))                                     RETURN "MC_PR_STNT213"         ENDIF   //Tyre Wall
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_02"))                                     RETURN "MC_PR_STNT214"         ENDIF   //Tyre Wall Med
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_03"))                                     RETURN "MC_PR_STNT215"         ENDIF //Tyre Wall         Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_04"))                                     RETURN "MC_PR_STNT274"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_05"))                                     RETURN "MC_PR_STNT275"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_06"))                                     RETURN "MC_PR_STNT276"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_07"))                                     RETURN "MC_PR_STNT277"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_08"))                                     RETURN "MC_PR_STNT278"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_09"))                                     RETURN "MC_PR_STNT279"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_010"))                                   RETURN "MC_PR_STNT280"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_011"))                                   RETURN "MC_PR_STNT281"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_012"))                                   RETURN "MC_PR_STNT282"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_013"))                                   RETURN "MC_PR_STNT283"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_014"))                                   RETURN "MC_PR_STNT316"         ENDIF   //Tyre Wall        Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_015"))                                   RETURN "MC_PR_STNT284"         ENDIF   //Tyre Wall        Large

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r1"))                                    RETURN "MC_PR_STNT216"         ENDIF   //Tyre Wall Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r2"))                                    RETURN "MC_PR_STNT217"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r06"))                                  RETURN "MC_PR_STNT285"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r07"))                                  RETURN "MC_PR_STNT286"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r011"))                                RETURN "MC_PR_STNT287"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r012"))                                RETURN "MC_PR_STNT288"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r013"))                                RETURN "MC_PR_STNT289"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r014"))                                RETURN "MC_PR_STNT290"         ENDIF   //Tyre Wall Med Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r019"))                                RETURN "MC_PR_STNT291"         ENDIF   //Tyre Wall Med Right

			
			
			

		    IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_bar_m"))                            RETURN "MC_PR_STNT52"          ENDIF   //Track Bend Bar Medium
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_5D_bar"))               RETURN "MC_PR_STNT264"         ENDIF   //Track Bend Bar 5
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_15D_bar"))             RETURN "MC_PR_STNT265"         ENDIF   //Track Bend Bar 15
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_30D_bar"))             RETURN "MC_PR_STNT266"         ENDIF   //Track Bend Bar 30
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_180D_bar"))                       RETURN "MC_PR_STNT267"         ENDIF   //Track Bend Bar 180
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_m"))                                              RETURN "MC_PR_STNT48"         ENDIF   //Track Bend Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_5d"))                                 RETURN "MC_PR_STNT222"         ENDIF   //Track Bend 5d
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_15d"))                               RETURN "MC_PR_STNT223"         ENDIF   //Track Bend 15d
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_30d"))                               RETURN "MC_PR_STNT224"         ENDIF   //Track Bend 30d
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_180d"))                              RETURN "MC_PR_STNT225"         ENDIF   //Track Bend 180d

			//No longer required - bug 3192435
			//IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_l"))                                                RETURN "MC_PR_STNT239"       ENDIF   //Track Bend Large DEV
            //IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_l"))                                  RETURN "MC_PR_STNT240"         ENDIF   //Track Bend Extra Large DEV
			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_l_b"))                                 RETURN "MC_PR_STNT49"          ENDIF   //Track Bend Large Public
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_l_b"))                               RETURN "MC_PR_STNT141"         ENDIF   //Track Bend Extra Large Public
			
           	//No longer required - bug 3192435
		   	//IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_bar_l"))                              RETURN "MC_PR_STNT241"         ENDIF   //Track Bend Bar Large DEV
            //IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_bar_l"))                RETURN "MC_PR_STNT259"         ENDIF   //Track Bend Bar Extra Large DEV
			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_bar_l_b"))               RETURN "MC_PR_STNT53"          ENDIF   //Track Bend Bar Large Public
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_bar_l_b"))             RETURN "MC_PR_STNT142"         ENDIF   //Track Bend Bar Extra Large Public
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_fork"))                                        RETURN "MC_PR_STNT137"         ENDIF   //Track Fork
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_cross"))                                      RETURN "MC_PR_STNT56"          ENDIF   //Track Cross

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_s"))                                   RETURN "MC_PR_STNT24"          ENDIF   //Spiral Small 
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_s"))                                 RETURN "MC_PR_STNT150"         ENDIF   //Spiral Small Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_m"))                                 RETURN "MC_PR_STNT23"          ENDIF   //Spiral Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_m"))                               RETURN "MC_PR_STNT151"         ENDIF   //Spiral Medium Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l"))                                   RETURN "MC_PR_STNT22"          ENDIF   //Spiral Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_l"))                                 RETURN "MC_PR_STNT152"         ENDIF   //Spiral Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_xxl"))                                RETURN "MC_PR_STNT155"         ENDIF   //Spiral XXL
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_xxl"))                  RETURN "MC_PR_STNT153"         ENDIF   //Spiral XXL Left

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_speakerstack_01a"))                             RETURN "MC_PR_STNT263"         ENDIF   //Audio Speaker
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_small_01"))                                 RETURN "MC_PR_STNT188"         ENDIF   //Hoop of Fire 
			// Wed afternoon.
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowlpin_stand"))                        RETURN "MC_PR_STNT64"          ENDIF   //Bowling Pin Stand
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_goal"))               				 RETURN "MC_PR_STNT67"          ENDIF   //Football Goal
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_stop_sign"))                                RETURN "MC_PR_STNT101"         ENDIF   //Stop Sign
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_01"))                                 RETURN "MC_PR_STNT198"         ENDIF   //Bend Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_02"))                                 RETURN "MC_PR_STNT199"         ENDIF   //Weave Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_03"))                                 RETURN "MC_PR_STNT200"         ENDIF   //Caution Slow Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_04"))                                 RETURN "MC_PR_STNT201"         ENDIF   //Slippery Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_05"))                                 RETURN "MC_PR_STNT202"         ENDIF   //Ramp Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_06"))                                 RETURN "MC_PR_STNT203"         ENDIF   //Loop Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_07"))                                 RETURN "MC_PR_STNT204"         ENDIF   //No Cars Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_08"))                                 RETURN "MC_PR_STNT205"         ENDIF   //Bowling Pin Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_09"))                                 RETURN "MC_PR_STNT206"         ENDIF   //Cliff Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_10"))                                 RETURN "MC_PR_STNT207"         ENDIF   //Steep Slope Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_11"))                                 RETURN "MC_PR_STNT208"         ENDIF   //Hard Left Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_12"))                                 RETURN "MC_PR_STNT209"         ENDIF   //Bumps Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_13"))                                 RETURN "MC_PR_STNT210"         ENDIF   //U-Turn Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_14"))                                 RETURN "MC_PR_STNT211"         ENDIF   //Tilted Road Sign Post
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_15"))                                 RETURN "MC_PR_STNT212"         ENDIF   //! Sign Post

			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_01"))                                 RETURN "MC_PR_STNT174"         ENDIF   //Corner Sign 5
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_02"))                                 RETURN "MC_PR_STNT175"         ENDIF   //Corner Sign 10
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_03"))                                 RETURN "MC_PR_STNT176"         ENDIF   //Corner Sign 50
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_04"))                                 RETURN "MC_PR_STNT177"         ENDIF   //Corner Sign 100
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_05"))                                 RETURN "MC_PR_STNT178"         ENDIF   //Corner Sign Caution
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_06"))                                 RETURN "MC_PR_STNT179"         ENDIF   //Corner Sign Left Pin
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_07"))                                 RETURN "MC_PR_STNT180"         ENDIF   //Corner Sign Right Pin
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_08"))                                 RETURN "MC_PR_STNT181"         ENDIF   //Corner Sign Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_09"))                                 RETURN "MC_PR_STNT182"         ENDIF   //Corner Sign Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_10"))                                 RETURN "MC_PR_STNT183"         ENDIF   //Corner Sign Chicane Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_11"))                                 RETURN "MC_PR_STNT184"         ENDIF   //Corner Sign Chicane Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_12"))                                 RETURN "MC_PR_STNT185"         ENDIF   //Corner Sign !
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_13"))                                 RETURN "MC_PR_STNT186"         ENDIF   //Corner Sign >
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_14"))                                 RETURN "MC_PR_STNT187"         ENDIF   //Corner Sign <
			

			

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r3"))                                    RETURN "MC_PR_STNT218"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r04"))                                  RETURN "MC_PR_STNT292"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r05"))                                  RETURN "MC_PR_STNT293"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r08"))                                  RETURN "MC_PR_STNT294"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r09"))                                  RETURN "MC_PR_STNT295"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r010"))                                RETURN "MC_PR_STNT296"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r015"))                                RETURN "MC_PR_STNT297"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r016"))                                RETURN "MC_PR_STNT298"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r017"))                                RETURN "MC_PR_STNT299"         ENDIF   //Tyre Wall        Large Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r018"))                                RETURN "MC_PR_STNT300"         ENDIF   //Tyre Wall        Large Right
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l1"))                                    RETURN "MC_PR_STNT219"         ENDIF   //Tyre Wall Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l2"))                                    RETURN "MC_PR_STNT220"         ENDIF   //Tyre Wall Med Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l06"))                                  RETURN "MC_PR_STNT301"         ENDIF   //Tyre Wall        Med Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l07"))                                  RETURN "MC_PR_STNT302"         ENDIF   //Tyre Wall        Med Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l013"))                                 RETURN "MC_PR_STNT303"         ENDIF   //Tyre Wall        Med Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l014"))                                 RETURN "MC_PR_STNT304"         ENDIF   //Tyre Wall        Med Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l015"))                                 RETURN "MC_PR_STNT305"         ENDIF   //Tyre Wall Med Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l020"))                                 RETURN "MC_PR_STNT306"         ENDIF   //Tyre Wall Med Left

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l3"))                                    RETURN "MC_PR_STNT221"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l04"))                                  RETURN "MC_PR_STNT307"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l05"))                                  RETURN "MC_PR_STNT308"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l08"))                                  RETURN "MC_PR_STNT309"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l010"))                                 RETURN "MC_PR_STNT310"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l012"))                                 RETURN "MC_PR_STNT311"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l16"))                                  RETURN "MC_PR_STNT312"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l17"))                                  RETURN "MC_PR_STNT313"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l018"))                                 RETURN "MC_PR_STNT314"         ENDIF   //Tyre Wall        Large Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l019"))                                 RETURN "MC_PR_STNT315"         ENDIF   //Tyre Wall        Large Left
            
			
			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_s"))                                               RETURN "MC_PR_STNT88"         ENDIF   //Jump Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_m"))                                             RETURN "MC_PR_STNT89"         ENDIF   //Jump Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_l"))                                                RETURN "MC_PR_STNT90"         ENDIF   //Jump Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_sb"))                                 RETURN "MC_PR_STNT110"         ENDIF   //Smooth Jump Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_mb"))                               RETURN "MC_PR_STNT111"         ENDIF   //Smooth Jump Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_lb"))                                  RETURN "MC_PR_STNT112"         ENDIF   //Smooth Jump Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_ramp"))                                     RETURN "MC_PR_STNT33"          ENDIF   //Giant Stunt Ramp
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_wideramp"))                              RETURN "MC_PR_STNT135"         ENDIF   //Giant Stunt Ramp Wide
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_qp3"))                            RETURN "MC_PR_STNT162"         ENDIF   //Quarter Pipe Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_qp2"))                            RETURN "MC_PR_STNT163"         ENDIF   //Quarter Pipe Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_qp"))                              RETURN "MC_PR_STNT164"         ENDIF   //Quarter Pipe Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_hump_01"))                    RETURN "MC_PR_STNT163s"       ENDIF   //Jump Hump Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_hump_02"))                    RETURN "MC_PR_STNT163m"      ENDIF   //Jump Hump Medium


			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_fork_bar"))                                 RETURN "MC_PR_STNT138"         ENDIF   //Track Fork Bar
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_cross_bar"))                                RETURN "MC_PR_STNT57"          ENDIF   //Track Cross Bar 
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_L"))                                RETURN "MC_PR_STNT167"         ENDIF   //Track Chicane Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_tube_01"))                                  RETURN "MC_PR_STNT169"         ENDIF   //Track Tube Blend
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_R"))                               RETURN "MC_PR_STNT168"         ENDIF   //Track Chicane Right
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_L_02"))               RETURN "MC_PR_STNT196"         ENDIF   //Track Chicane Left Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_R_02"))               RETURN "MC_PR_STNT197"         ENDIF   //Track Chicane Right Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_block_01"))                                 RETURN "MC_PR_STNT236"         ENDIF   //Track Blocker 01
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_block_02"))                                 RETURN "MC_PR_STNT237"         ENDIF   //Track Blocker 02
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_block_03"))                                 RETURN "MC_PR_STNT238"         ENDIF   //Track Blocker 03
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_gantry_01"))                                RETURN "MC_PR_STNT242"         ENDIF   //Gantry
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel"))                                                RETURN "MC_PR_STNT58"         ENDIF   //Track Funnel
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel_ads_01a"))                       RETURN "MC_PR_STNT235"         ENDIF   //Track Funnel ads 01
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel_ads_01b"))                       RETURN "MC_PR_STNT253"         ENDIF   //Track Funnel ads 02
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel_ads_01c"))                       RETURN "MC_PR_STNT254"         ENDIF   //Track Funnel ads 03
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_link"))                                         RETURN "MC_PR_STNT115"         ENDIF   //Track Link
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_bar_l"))              RETURN "MC_PR_STNT55"          ENDIF   //Track Straight Bar Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_lm_bar"))           RETURN "MC_PR_STNT55a"         ENDIF   //Track Straight Bar Large-Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_bar_m"))                        RETURN "MC_PR_STNT54"          ENDIF   //Track Straight Bar Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_bar_s"))             RETURN "MC_PR_STNT106"         ENDIF   //Track Straight Bar Short

            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_l"))                                RETURN "MC_PR_STNT51"          ENDIF   //Track Straight Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_lm"))                 RETURN "MC_PR_STNT51a"         ENDIF   //Track Straight Large-Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_m"))                              RETURN "MC_PR_STNT50"          ENDIF   //Track Straight Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_s"))                               RETURN "MC_PR_STNT105"         ENDIF   //Track Straight Short


			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_01"))                 RETURN "MC_PR_STNT190"         ENDIF   //Start Line 01
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_01b"))               RETURN "MC_PR_STNT190b"       ENDIF   //Start Line 01b
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_02"))                 RETURN "MC_PR_STNT191"         ENDIF   //Start Line 02
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_02b"))               RETURN "MC_PR_STNT243"         ENDIF   //Start Line 02b
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_03"))                 RETURN "MC_PR_STNT192"         ENDIF   //Start Line 03
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_03b"))               RETURN "MC_PR_STNT244"         ENDIF   //Start Line 03b
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_target_small"))               RETURN "MC_PR_STNT320"         ENDIF   //Stunt Target Small
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_target"))                                                RETURN "MC_PR_STNT34"         ENDIF   //Stunt Target
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_landing_zone_01"))         RETURN "MC_PR_STNT171"         ENDIF   //Stunt Landing Zone
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_tyre_01a"))                                 RETURN "MC_PR_STNT194"         ENDIF   //Hoop Tyre

            
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn"))                                RETURN "MC_PR_STNT46"          ENDIF   //Tube Corner
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn2"))                               RETURN "MC_PR_STNT102"         ENDIF   //Tube Corner 45
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn_5d"))               RETURN "MC_PR_STNT226"         ENDIF   //Tube Corner 5
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn_15d"))              RETURN "MC_PR_STNT227"         ENDIF   //Tube Corner 15
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn_30d"))              RETURN "MC_PR_STNT228"         ENDIF   //Tube Corner 30
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fork"))                               RETURN "MC_PR_STNT134"         ENDIF   //Tube Fork 
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_gap_01"))               RETURN "MC_PR_STNT165"         ENDIF   //Tube Gap 
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_gap_02"))               RETURN "MC_PR_STNT166"         ENDIF   //Tube Gap Double
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_gap_03"))               RETURN "MC_PR_STNT262"         ENDIF   //Tube Gap Triple Section
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_cross"))                             RETURN "MC_PR_STNT40"          ENDIF   //Tube Cross 
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_end"))                               RETURN "MC_PR_STNT47"          ENDIF   //Tube End
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speed"))                            RETURN "MC_PR_STNT248"         ENDIF   //Tube Speed Boost
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_tube_02"))                                  RETURN "MC_PR_STNT170"         ENDIF   //Stunt Track Tube Blend
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_qg"))                                 RETURN "MC_PR_STNT41"          ENDIF   //Tube Quarter Gate
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_hg"))                                 RETURN "MC_PR_STNT42"          ENDIF   //Tube Half Gate
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_xxs"))                                RETURN "MC_PR_STNT104"         ENDIF   //Tube Extra Short
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_xs"))                                  RETURN "MC_PR_STNT37"          ENDIF   //Tube Short
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_s"))                                                RETURN "MC_PR_STNT38"         ENDIF   //Tube Medium
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_m"))                                              RETURN "MC_PR_STNT39"         ENDIF   //Tube Long
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_l"))                                    RETURN "MC_PR_STNT100"         ENDIF   //Tube Extra Long

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_jmp"))                               RETURN "MC_PR_STNT44"          ENDIF   //Tube Jump
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_jmp2"))                              RETURN "MC_PR_STNT82"          ENDIF   //Tube Jump 2
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_ent"))                                RETURN "MC_PR_STNT93"          ENDIF   //Tube Entrance
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_01"))                             RETURN "MC_PR_STNT83"          ENDIF   //Tube Anim 1
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_02"))                             RETURN "MC_PR_STNT84"          ENDIF   //Tube Anim 2
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_03"))                             RETURN "MC_PR_STNT85"          ENDIF   //Tube Anim 3
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_04"))                             RETURN "MC_PR_STNT86"          ENDIF   //Tube Anim 4
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_05"))                             RETURN "MC_PR_STNT87"          ENDIF   //Tube Anim 5

			//New categories Thursday.
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_01"))                                      RETURN "MC_PR_STNT256"         ENDIF   //Wall Ride Curve (Launch Ramp) Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_01b"))                                    RETURN "MC_PR_STNT91"          ENDIF   //Wall Ride Curve (Launch Ramp) Right
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_04"))                                      RETURN "MC_PR_STNT60"          ENDIF   //Wall Ride Curve
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45r"))                                     RETURN "MC_PR_STNT143"         ENDIF   //Wall Ride Curve 45 R
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45ra"))                                   RETURN "MC_PR_STNT249"         ENDIF   //Wall Ride Curve 45 R
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45l"))                                      RETURN "MC_PR_STNT144"         ENDIF   //Wall Ride Curve 45 L
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45la"))                                    RETURN "MC_PR_STNT250"         ENDIF   //Wall Ride Curve 45 L
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90r"))                                     RETURN "MC_PR_STNT145"         ENDIF   //Wall Ride Curve 90 R
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90rb"))                                   RETURN "MC_PR_STNT251"         ENDIF   //Wall Ride Curve 90 R
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90l"))                                      RETURN "MC_PR_STNT146"         ENDIF   //Wall Ride Curve 90 L
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90lb"))                                    RETURN "MC_PR_STNT252"         ENDIF   //Wall Ride Curve 90 L
			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_02"))                                      RETURN "MC_PR_STNT257"         ENDIF   //Wall Ride Spiral L(Launch Ramp)
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_02b"))                                    RETURN "MC_PR_STNT92"          ENDIF   //Wall Ride Spiral R (Launch Ramp)
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_05"))                                      RETURN "MC_PR_STNT258"         ENDIF   //Wall Ride Spiral Left
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_05b"))                                    RETURN "MC_PR_STNT61"          ENDIF   //Wall Ride Spiral Right

			
			
			
			
			
			
			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_exshort"))             RETURN "MC_PR_STNT154"         ENDIF   //Stunt Track Short
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_short"))                 RETURN "MC_PR_STNT103"         ENDIF   //Stunt Track Short

			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_straight"))             RETURN "MC_PR_STNT80"          ENDIF   //Stunt Track Straight
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_cutout"))               RETURN "MC_PR_STNT113"         ENDIF   //Stunt Track Bridge
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_otake"))                RETURN "MC_PR_STNT69"          ENDIF   //Stunt Track Overtake
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_fork"))                  RETURN "MC_PR_STNT139"         ENDIF   //Stunt Track Fork
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_funnel"))               RETURN "MC_PR_STNT70"          ENDIF   //Stunt Track Funnel
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_funlng"))               RETURN "MC_PR_STNT140"         ENDIF   //Stunt Track Funnel Large
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_slope15"))             RETURN "MC_PR_STNT74"          ENDIF   //Stunt Track Slope 15
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_slope30"))             RETURN "MC_PR_STNT75"          ENDIF   //Stunt Track Slope 30
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_slope45"))             RETURN "MC_PR_STNT76"          ENDIF   //Stunt Track Slope 45
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_hill"))                    RETURN "MC_PR_STNT77"          ENDIF   //Stunt Track Steep Hill

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_hill2"))                   RETURN "MC_PR_STNT78"          ENDIF   //Stunt Track Small Hills
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_bumps"))              RETURN "MC_PR_STNT136"         ENDIF   //Stunt Track Bumps
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_jump"))                RETURN "MC_PR_STNT79"          ENDIF   //Stunt Track Jump
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump15"))                    	RETURN "MC_PR_STNT116"       ENDIF   //Stunt Track Jump 15
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump30"))                     RETURN "MC_PR_STNT117"       ENDIF   //Stunt Track Jump 30
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump45"))                       RETURN "MC_PR_STNT118"       ENDIF   //Stunt Track Jump 45
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_start"))                  RETURN "MC_PR_STNT71"          ENDIF   //Stunt Track Start Grid 30
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_start_02"))             RETURN "MC_PR_STNT1"            ENDIF   //Stunt Track Start Grid 16
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_st_01"))                 RETURN "MC_PR_STNT246"         ENDIF   //Stunt Track Start Line 30
           	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_st_02"))                 RETURN "MC_PR_STNT245"         ENDIF   //Stunt Track Start Line 16

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_turn"))                              RETURN "MC_PR_STNT73"          ENDIF   //Stunt Track Turn
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_sh15"))                              RETURN "MC_PR_STNT107"         ENDIF   //Stunt Track Turn 15
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_sh30"))                              RETURN "MC_PR_STNT108"         ENDIF   //Stunt Track Turn 30
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_sh45"))                              RETURN "MC_PR_STNT109"         ENDIF   //Stunt Track Turn 45
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Track_SH45_a"))             RETURN "MC_PR_STNT109A"      ENDIF   //Stunt Track Turn 45a

			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_uturn"))                RETURN "MC_PR_STNT81"          ENDIF   //Stunt Track U-Turn
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_link"))                               RETURN "MC_PR_STNT114"         ENDIF   //Stunt Track Link
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwlink"))               RETURN "MC_PR_STNT3"            ENDIF   //Stunt Track Wide Link
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwlink_02"))          RETURN "MC_PR_STNT247"         ENDIF   //Stunt Track Wide Link 02
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwshort"))                        RETURN "MC_PR_STNT4"            ENDIF   //Stunt Track Wide Straight

		 	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwsh15"))             RETURN "MC_PR_STNT5"            ENDIF   //Stunt Track Wide Turn 15
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwturn"))              RETURN "MC_PR_STNT6"            ENDIF   //Stunt Track Wide Turn
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwuturn"))                        RETURN "MC_PR_STNT7"            ENDIF   //Stunt Track Wide U-Turn
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwslope15"))         RETURN "MC_PR_STNT8"            ENDIF   //Stunt Track Wide Slope 15
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwslope30"))         RETURN "MC_PR_STNT9"            ENDIF   //Stunt Track Wide Slope 30
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwslope45"))         RETURN "MC_PR_STNT10"          ENDIF   //Stunt Track Wide Slope 45
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp"))                                RETURN "MC_PR_STNT132"         ENDIF   //Speed Up
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T1"))                            RETURN "MC_PR_STNT231"         ENDIF   //Speed Up
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T2"))                            RETURN "MC_PR_STNT232"         ENDIF   //Speed Up
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))                              RETURN "MC_PR_STNT133"         ENDIF   //Speed Down
			
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))             RETURN "MC_PR_STNT233"         ENDIF   //Speed Down
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2"))             RETURN "MC_PR_STNT234"         ENDIF   //Speed Down
			//Had problems with these two. Missing model?
            //IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_trail_CylinderRun"))                 RETURN "MC_PR_STNT6"            ENDIF   //Trail Cylinder Run
            //IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_trail_LogJump_01"))                             RETURN "MC_PR_STNT7"            ENDIF   //Trail Ring of Fire Run












			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_sml1"))               RETURN "MC_PR_STNT120"         ENDIF   //Block 1x2
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_sml2"))               RETURN "MC_PR_STNT121"         ENDIF   //Block 2x2
			IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_sml3"))               RETURN "MC_PR_STNT122"         ENDIF   //Block 3x2
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_mdm1"))            RETURN "MC_PR_STNT123"         ENDIF   //Block 1x3
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_mdm2"))            RETURN "MC_PR_STNT124"         ENDIF   //Block 2x3
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_mdm3"))            RETURN "MC_PR_STNT125"         ENDIF   //Block 3x3
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_lrg1"))                RETURN "MC_PR_STNT126"         ENDIF   //Block 1x4
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_lrg2"))                RETURN "MC_PR_STNT127"         ENDIF   //Block 2x4
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_lrg3"))                RETURN "MC_PR_STNT128"         ENDIF   //Block 3x4
			
			
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_xl1"))                              RETURN "MC_PR_STNT129"         ENDIF   //Block 1x5
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_xl2"))                              RETURN "MC_PR_STNT130"         ENDIF   //Block 2x5
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_xl3"))                              RETURN "MC_PR_STNT131"         ENDIF   //Block 3x5
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_01"))                      RETURN "MC_PR_STNT147"         ENDIF   //Block 9x11
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_02"))                      RETURN "MC_PR_STNT148"         ENDIF   //Block 9x21
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_03"))                      RETURN "MC_PR_STNT149"         ENDIF   //Block 18x21
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_04"))                      RETURN "MC_PR_STNT229"         ENDIF   //Block 23x28
            IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_05"))                      RETURN "MC_PR_STNT230"         ENDIF   //Block 28x49




			//End Thursday work.
			
			//Special Race Labels
			#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	
			
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_Spec_Races_Blimp_Sign"))		RETURN "MC_SR_PROP_45"	ENDIF	//Xero Sign
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Take_Flight_Sign"))	RETURN "MC_SR_PROP_46"	ENDIF	//Take Sign
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Ron_Sign"))			RETURN "MC_SR_PROP_47"	ENDIF	//Ron Sign
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Xero_Sign"))		RETURN "MC_SR_PROP_48"	ENDIF	//Xero Sign
					
					
					
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_1_01a"))			RETURN "MC_SR_PROP_61"	ENDIF	//Landing Pad Target 1
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_2_04a"))			RETURN "MC_SR_PROP_62"	ENDIF	//Landing Pad Target 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_3_03a"))			RETURN "MC_SR_PROP_63"	ENDIF	//Landing Pad Target 3
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_4_01a"))			RETURN "MC_SR_PROP_64"	ENDIF	//Landing Pad Target 4
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_5_01a"))			RETURN "MC_SR_PROP_65"	ENDIF	//Landing Pad Target 5
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_s_01a"))		RETURN "FMMC_PR_BJT1"		ENDIF	//Target Small
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_m_01a"))		RETURN "FMMC_PR_BJT2"		ENDIF	//Target Medium
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_b_01a"))		RETURN "FMMC_PR_BJT3"		ENDIF	//Target Big
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_long_01a"))		RETURN "MC_SR_PROP_67"	ENDIF	//Landing Pad Target Long
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_trap_01a"))		RETURN "MC_SR_PROP_70"	ENDIF	//Landing Pad Target Trap 1
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_trap_02a"))		RETURN "MC_SR_PROP_71"	ENDIF	//Landing Pad Target Trap 2
					
			
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Refill"))						RETURN "MC_SR_PROP_39"	ENDIF	//Boost Refiller
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Refill_T2"))				RETURN "MC_SR_PROP_40"	ENDIF	//Boost Refiller Raised Track
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Refill_T1"))				RETURN "MC_SR_PROP_41"	ENDIF	//Boost Refiller Track
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_SR_Track_JumpWall"))		RETURN "MC_SR_PROP_42"	ENDIF	//Track Hurdle
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Refill"))			RETURN "MC_SR_PROP_44"	ENDIF	//Tube Boost Refiller
			
			
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))	RETURN "FMMC_PR_116"		ENDIF /*Destructible Ammunation Sign*/
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_02"))			RETURN "FMMC_PR_111"		ENDIF /*Pile of Boxes 1 (Special Races)*/
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_03"))			RETURN "FMMC_PR_112"		ENDIF /*Pile of Boxes 2 (Special Races)*/
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall"))			RETURN "FMMC_PR_114"		ENDIF /*Destructible Wall*/
					//IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_tube_wall"))			RETURN "FMMC_PR_115"		ENDIF /*Destructible Tube End*/
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml1"))		RETURN "MC_SR_PROP_49"	ENDIF	//Neon Block Small 1
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm1"))		RETURN "MC_SR_PROP_50"	ENDIF	//Neon Block Medium 1
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg11"))		RETURN "MC_SR_PROP_51"	ENDIF	//Neon Block Large 1
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL1"))		RETURN "MC_SR_PROP_52"	ENDIF	//Neon Block Extra Large 1
	
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml2"))		RETURN "MC_SR_PROP_53"	ENDIF	//Neon Block Small 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm2"))		RETURN "MC_SR_PROP_54"	ENDIF	//Neon Block Medium 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg2"))		RETURN "MC_SR_PROP_55"	ENDIF	//Neon Block Large 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL2"))		RETURN "MC_SR_PROP_56"	ENDIF	//Neon Block Extra Large 2
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml3"))		RETURN "MC_SR_PROP_57"	ENDIF	//Neon Block Small 3
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm3"))		RETURN "MC_SR_PROP_58"	ENDIF	//Neon Block Medium 3
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg3"))		RETURN "MC_SR_PROP_59"	ENDIF	//Neon Block Large 3
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL3"))		RETURN "MC_SR_PROP_60x"	ENDIF	//Neon Block Extra Large 3 (d)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL3_Fixed"))	RETURN "MC_SR_PROP_60"	ENDIF	//Neon Block Extra Large 3
				

		
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_01a"))			RETURN "MC_SR_PROP_01"	ENDIF 	//Neon Tube Corner
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_02a"))			RETURN "MC_SR_PROP_01"	ENDIF 	//Neon Tube Corner
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_03a"))			RETURN "MC_SR_PROP_01"	ENDIF 	//Neon Tube Corner
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_04a"))			RETURN "MC_SR_PROP_01"	ENDIF 	//Neon Tube Corner
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_05a"))			RETURN "MC_SR_PROP_01"	ENDIF 	//Neon Tube Corner
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_01a"))		RETURN "MC_SR_PROP_02"	ENDIF 	//Neon Tube Corner 30D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_02a"))		RETURN "MC_SR_PROP_02"	ENDIF 	//Neon Tube Corner 30D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_03a"))		RETURN "MC_SR_PROP_02"	ENDIF 	//Neon Tube Corner 30D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_04a"))		RETURN "MC_SR_PROP_02"	ENDIF 	//Neon Tube Corner 30D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_05a"))		RETURN "MC_SR_PROP_02"	ENDIF 	//Neon Tube Corner 30D
					
					
					
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_01a"))			RETURN "MC_SR_PROP_03"	ENDIF 	//Neon Tube Medium
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_02a"))			RETURN "MC_SR_PROP_03"	ENDIF 	//Neon Tube Medium
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_03a"))			RETURN "MC_SR_PROP_03"	ENDIF 	//Neon Tube Medium
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_04a"))			RETURN "MC_SR_PROP_03"	ENDIF 	//Neon Tube Medium
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_05a"))			RETURN "MC_SR_PROP_03"	ENDIF 	//Neon Tube Medium
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_01a"))			RETURN "MC_SR_PROP_04"	ENDIF 	//Neon Tube Long
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_02a"))			RETURN "MC_SR_PROP_04"	ENDIF 	//Neon Tube Long
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_03a"))			RETURN "MC_SR_PROP_04"	ENDIF 	//Neon Tube Long
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_04a"))			RETURN "MC_SR_PROP_04"	ENDIF 	//Neon Tube Long
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_05a"))			RETURN "MC_SR_PROP_04"	ENDIF 	//Neon Tube Long
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_01a"))			RETURN "MC_SR_PROP_05"	ENDIF 	//Neon Tube Extra Large
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_02a"))			RETURN "MC_SR_PROP_05"	ENDIF 	//Neon Tube Extra Large
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_03a"))			RETURN "MC_SR_PROP_05"	ENDIF 	//Neon Tube Extra Large
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_04a"))			RETURN "MC_SR_PROP_05"	ENDIF 	//Neon Tube Extra Large
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_05a"))			RETURN "MC_SR_PROP_05"	ENDIF 	//Neon Tube Extra Large
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_01a"))			RETURN "MC_SR_PROP_06"	ENDIF	//Neon Tube Extra Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_02a"))			RETURN "MC_SR_PROP_06"	ENDIF	//Neon Tube Extra Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_03a"))			RETURN "MC_SR_PROP_06"	ENDIF	//Neon Tube Extra Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_04a"))			RETURN "MC_SR_PROP_06"	ENDIF	//Neon Tube Extra Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_05a"))			RETURN "MC_SR_PROP_06"	ENDIF	//Neon Tube Extra Short
					
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_01a"))		RETURN "MC_SR_PROP_07"	ENDIF	//Neon Tube Corner 5D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_02a"))		RETURN "MC_SR_PROP_07"	ENDIF	//Neon Tube Corner 5D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_03a"))		RETURN "MC_SR_PROP_07"	ENDIF	//Neon Tube Corner 5D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_04a"))		RETURN "MC_SR_PROP_07"	ENDIF	//Neon Tube Corner 5D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_05a"))		RETURN "MC_SR_PROP_07"	ENDIF	//Neon Tube Corner 5D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_01a"))	RETURN "MC_SR_PROP_08"	ENDIF	//Neon Tube Corner 15D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_02a"))	RETURN "MC_SR_PROP_08"	ENDIF	//Neon Tube Corner 15D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_03a"))	RETURN "MC_SR_PROP_08"	ENDIF	//Neon Tube Corner 15D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_04a"))	RETURN "MC_SR_PROP_08"	ENDIF	//Neon Tube Corner 15D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_05a"))	RETURN "MC_SR_PROP_08"	ENDIF	//Neon Tube Corner 15D
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_01a"))		RETURN "MC_SR_PROP_09"	ENDIF	//Neon Tube Corner 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_02a"))		RETURN "MC_SR_PROP_09"	ENDIF	//Neon Tube Corner 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_03a"))		RETURN "MC_SR_PROP_09"	ENDIF	//Neon Tube Corner 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_04a"))		RETURN "MC_SR_PROP_09"	ENDIF	//Neon Tube Corner 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_05a"))		RETURN "MC_SR_PROP_09"	ENDIF	//Neon Tube Corner 2
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_01a"))			RETURN "MC_SR_PROP_10"	ENDIF	//Neon Tube Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_02a"))			RETURN "MC_SR_PROP_10"	ENDIF	//Neon Tube Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_03a"))			RETURN "MC_SR_PROP_10"	ENDIF	//Neon Tube Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_04a"))			RETURN "MC_SR_PROP_10"	ENDIF	//Neon Tube Short
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_05a"))			RETURN "MC_SR_PROP_10"	ENDIF	//Neon Tube Short
						
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D5"))		RETURN "MC_SR_PROP_31"	ENDIF 	//Track Down (5)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D15"))		RETURN "MC_SR_PROP_32"	ENDIF 	//Track Down (15)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D30"))		RETURN "MC_SR_PROP_33"	ENDIF 	//Track Down (30)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D45"))		RETURN "MC_SR_PROP_34"	ENDIF 	//Track Down (45)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U5"))		RETURN "MC_SR_PROP_35"	ENDIF 	//Track Up (5)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U15"))		RETURN "MC_SR_PROP_36"	ENDIF 	//Track Up (15)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U30"))		RETURN "MC_SR_PROP_37"	ENDIF 	//Track Up (30)
					IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U45"))		RETURN "MC_SR_PROP_38"	ENDIF 	//Track Up (45)
					
			#ENDIF
			
		

	#ENDIF
    
    ASSERTLN( "director_mode_prop_data - GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL - Invalid model passed in ( ", mn," ) -returning NULL label.")
    RETURN ""
    
ENDFUNC

PROC Add_Prop_To_Prop_Sheet(INT &PassedIndex, enum_Category PassedCategory, MODEL_NAMES PassedModel,
	FLOAT PassedMoverAdjustment = 0.0, FLOAT PassedMarkerAdjustment = 2.0, FLOAT PassedGroundAdjustment = 0.0, FLOAT fPfxScale = 1.0,
	BOOL bIsFrozen = TRUE, BOOL bPlaceOnGround = TRUE, FLOAT fIgnoreZ = 0.5, FLOAT fGroundScale = 1.0, BOOL bIgnoreCollision = FALSE)
     
    ePropTypes[PassedIndex].name                    = GET_DM_PROP_TEXT_LABEL_FOR_PROP_MODEL( PassedModel )
    ePropTypes[PassedIndex].category                = PassedCategory
    ePropTypes[PassedIndex].model                   = PassedModel

    ePropTypes[PassedIndex].fMoverAdjust            = PassedMoverAdjustment
    ePropTypes[PassedIndex].fGroundAdjust           = PassedGroundAdjustment
    ePropTypes[PassedIndex].fMarkerAdjust           = PassedMarkerAdjustment
    ePropTypes[passedIndex].fPfxScale               = fPfxScale

    ePropTypes[passedIndex].bFrozen                 = bIsFrozen
    ePropTypes[passedIndex].bPlaceOnGround          = bPlaceOnGround
    ePropTypes[passedIndex].fIgnoreZ                = fIgnoreZ
    ePropTypes[passedIndex].fGroundScale            = fGroundScale
	ePropTypes[passedIndex].bIgnoreCollision		= bIgnoreCollision

    ePropTypesPerCategory[PassedCategory] += 1
    CDEBUG3LN(debug_director, "Prop added to sheet at index ", PassedIndex,", types in category ", ePropTypesPerCategory[PassedCategory])
    PassedIndex++

ENDPROC

FUNC BOOL GET_IS_DM_PROP_DYNAMIC( MODEL_NAMES eModel )
    INT iIndex
    REPEAT ciMAXTYPES iIndex
        IF eModel = ePropTypes[ iIndex ].model
            RETURN !ePropTypes[ iIndex ].bFrozen
        ENDIF
    ENDREPEAT
    CPRINTLN( DEBUG_DIRECTOR, " - GET_IS_DM_PROP_DYNAMIC - Invalid modeltype passed in. Not in prop editor prop list.")
	//Assert removed - stunt props might be removed via tunable now
//    ASSERTLN( " - GET_IS_DM_PROP_DYNAMIC - Invalid modeltype passed in (", eModel, ") - Not in prop editor prop list. ( director_mode_prop_data.sch )")
    RETURN FALSE
ENDFUNC

PROC FILL_IN_PROP_DATA()

    eCategoryNames[ CAT_Barrier ]       = "FMMC_PLIB_0"     //  Barriers
    eCategoryNames[ CAT_Benches ]       = "FMMC_PLIB_1"     //  Benches
    eCategoryNames[ CAT_Dumpsters ]     = "FMMC_PLIB_7"     //  Dumpsters
    eCategoryNames[ CAT_Cabin ]         = "FMMC_PLIB_3"     //  Cabins
    eCategoryNames[ CAT_Construction ]  = "FMMC_PLIB_4"     //  Construction
    eCategoryNames[ CAT_Containers ]    = "FMMC_PLIB_5"     //  Containers
    eCategoryNames[ CAT_Crates ]        = "FMMC_PLIB_6"     //  Crates and Boxes
    eCategoryNames[ CAT_Explosive ]     = "FMMC_PLIB_15"    //  Explosive
    eCategoryNames[ CAT_Machinery ]     = "FMMC_PLIB_8"     //  Machinery
    eCategoryNames[ CAT_Natural ]       = "FMMC_PLIB_TNR"   //  Rocks and Trees
    eCategoryNames[ CAT_Ramp ]          = "FMMC_PLIB_9"     //  Ramps
    eCategoryNames[ CAT_Roadside ]      = "FMMC_PLIB_10"    //  Signs
    eCategoryNames[ CAT_Trailers ]      = "FMMC_PLIB_11"    //  Trailers
    eCategoryNames[ CAT_Wreckage ]      = "FMMC_PLIB_12"    //  Wreckage
    eCategoryNames[ CAT_Water ]         = "DMPP_WTRCAT"     //  From AmericanReplay rather than Creator text. I want to put the water ramp into the water category.
	
	#IF	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	eCategoryNames[CAT_StuntRamps] =  "DMPP_SCAT1"
	eCategoryNames[CAT_StuntTubes] = "DMPP_SCAT2"
	eCategoryNames[CAT_StuntDynamic] = "DMPP_SCAT3"
	eCategoryNames[CAT_StuntTrack] = "DMPP_SCAT4"
	eCategoryNames[CAT_StuntTyres] = "DMPP_SCAT5"
	eCategoryNames[CAT_StuntMisc] = "DMPP_SCAT6"
	eCategoryNames[CAT_StuntWalls] = "DMPP_SCAT7"
	eCategoryNames[CAT_StuntExtraTrk] = "DMPP_SCAT8"
	eCategoryNames[CAT_StuntBlocks] = "DMPP_SCAT9"
	#ENDIF

    INT iID = 0

    //  Barriers
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_PLAS_BARIER_01A                                        ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARRIER_WORK04A                                        ,0.0    , 2.0,  0.0,    0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARRIER_WORK05                                         ,0.0    , 2.0,  0.0,    0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARRIER_WORK06A                                        ,0.0    , 2.0,  0.0,    0.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_BALE01                                         ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_BALE02                                         ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_BALE03                                         ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       Prop_HayB_St_01_CR                                          ,0.0    , 3.5,  -0.05,  4.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_HAYBALE_01                                             ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )  
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_HAYBALE_02                                             ,0.0    , 2.0,  -0.1,   1.5,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_HAYBALE_03                                             ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_TYRES01                                        ,0.25   , 2.0,  0.0,    1.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_TYRES02                                        ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )  
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_RUB_TYRE_03                                            ,0.0    , 2.0,  0.0,    0.5,   FALSE,  TRUE )  
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       Prop_Tyre_Wall_01                                           ,0.0    , 2.5,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       Prop_Tyre_Wall_02                                           ,0.0    , 2.5,  0.0,    3.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       Prop_Tyre_Wall_03                                           ,0.0    , 2.5,  0.0,    5.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       Prop_Tyre_Wall_04                                           ,0.0    , 2.5,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       Prop_Tyre_Wall_05                                           ,0.0    , 2.5,  0.0,    3.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_BARREL01                                       ,0.0    , 2.0,  0.0,    0.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_OFFROAD_BARREL02                                       ,0.0    , 2.0,  0.0,    1.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_MC_CONC_BARRIER_01                                     ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARIER_CONC_01A                                        ,0.0    , 2.0,  -0.05,  1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARIER_CONC_02A                                        ,0.0    , 2.0,  -0.05,  2.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARIER_CONC_02B                                        ,0.0    , 3.0,  -0.05,  2.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARIER_CONC_05A                                        ,0.0    , 2.0,  -0.05,  2.7,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARIER_CONC_05B                                        ,0.0    , 2.0,  -0.05,  2.7,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_BARIER_CONC_05C                                        ,0.0    , 2.0,  -0.05,  2.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_01"))      ,0.0    , 2.5,  -0.2,   2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_02"))      ,0.0    , 2.5,  -0.2,   4.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_05_CR"))   ,0.0    , 2.5,  -0.2,   4.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_04"))      ,0.0    , 2.5,  -0.2,   4.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_SANDBLOCK_03_CR"))   ,0.0    , 4.0,  -0.2,   4.5,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_HESCO_06"))          ,0.0    , 5.0,  0.0,    6.0,   TRUE,   TRUE )
    //Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,     PROP_FNCLINK_02GATE3                                        ,0.0    , 3.5,  0.0,      FALSE,  TRUE )  //Removing this doors as it can cause big trouble. See 2488343
    //Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,     PROP_FNCLINK_03GATE5                                        ,0.0    , 3.5,  0.0,      FALSE,  TRUE )  //Removing this doors as it can cause big trouble. See 2488343
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCLINK_06A                                            ,0.0    , 3.75, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCLINK_06B                                            ,0.0    , 3.75, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCLINK_06C                                            ,0.0    , 3.75, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCLINK_06D                                            ,0.0    , 3.75, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCSEC_03B                                             ,0.0    , 2.5,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_03A                                           ,0.0    , 3.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_03B                                           ,0.0    , 3.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_03C                                           ,0.0    , 3.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_02A                                           ,0.0    , 3.25, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_02B                                           ,0.0    , 3.25, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_02C                                           ,0.0    , 3.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_02D                                           ,0.0    , 3.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCCORGM_02E                                           ,0.0    , 3.25, 0.0,    1.0,   FALSE,  TRUE )
//    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,     PROP_GATE_CULT_01_L                                         ,0.0    , 3.0,  0.0,      TRUE,  TRUE )   //Still gives a "door not in level" assert as these cult gates are technically doors but freezing to prevent problems. Bug 2488343
//    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,     PROP_GATE_CULT_01_R                                         ,0.0    , 3.0,  0.0,      TRUE,  TRUE )   //Still gives a "door not in level" assert as these cult gates are technically doors but freezing to prevent problems. Bug 2488343
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_CONST_FENCE02A                                         ,0.0    , 4.0,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_CONST_FENCE03A_CR                                      ,0.0    , 4.0,  0.0,    3.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_CONST_FENCE01B_CR                                      ,0.0    , 4.0,  0.0,    1.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_CONST_FENCE02B                                         ,0.0    , 4.0,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_CONST_FENCE03B_CR                                      ,0.0    , 4.0,  0.0,    3.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCWOOD_16B                                            ,0.0    , 3.0,  0.0,    0.8,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNCWOOD_16C                                            ,0.0    , 3.0,  0.0,    0.6,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNC_FARM_01B                                           ,0.0    , 2.5,  -0.4,   0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNC_FARM_01C                                           ,0.0    , 2.5,  -0.4,   0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNC_FARM_01D                                           ,0.0    , 2.5,  -0.4,   1.7,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNC_FARM_01E                                           ,0.0    , 2.5,  -0.4,   2.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_FNC_FARM_01F                                           ,0.0    , 2.5,  -0.4,   3.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       PROP_TABLE_08_SIDE                                          ,0.0    , 2.0,  0.0,    1.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_START_GATE_01B"))       ,0.0    , 10.0, 0.0,    7.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Beachf_01_CR"))         ,0.0    , 7.5,  0.0,    0.5,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Barrier,       prop_inflategate_01                                         ,0.0    , 17.5,  -0.2,  4.0,   TRUE,   TRUE )
                                                                
    //  Benches
    Add_Prop_To_Prop_Sheet( iID, CAT_Benches,       PROP_BENCH_01A                                              ,0.0    , 2.0,  0.0,    0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Benches,       PROP_BENCH_05                                               ,0.0    , 2.0,  0.0,    1.1,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Benches,       PROP_BENCH_07                                               ,0.0    , 2.0,  0.0,    0.7,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Benches,       PROP_BENCH_08                                               ,0.0    , 2.0,  0.0,    1.1,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Benches,       INT_TO_ENUM(MODEL_NAMES, HASH("Prop_bleachers_04_CR"))      ,0.0    , 3.0,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Benches,       INT_TO_ENUM(MODEL_NAMES, HASH("Prop_bleachers_05_CR"))      ,0.0    , 3.0,  0.0,    2.1,   TRUE,   TRUE )
                                                            
    //  Cabins
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         PROP_ELECBOX_24                                             ,0.0    , 4.0,  0.0,    4.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         PROP_ELECBOX_24B                                            ,0.0    , 4.5,  0.0,    2.5,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         PROP_PORTACABIN01                                           ,0.0    , 4.5,  0.0,    3.6,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_MONHUT_03_CR"))     ,0.0    , 5.0,  0.0,    2.5,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_SECHUT_01"))        ,0.0    , 4.5,  0.0,    2.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         PROP_TOLLBOOTH_1                                            ,0.0    , 4.0,  0.0,    1.9,   FALSE,  TRUE )  //2x props not cleaned
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         PROP_PARKING_HUT_2                                          ,0.0    , 4.25, 0.0,    2.1,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Makeup_Trail_02_CR"))   ,0.0    , 4.0,  0.0,    3.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Makeup_Trail_01_CR"))   ,0.0    , 4.0,  0.0,    3.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Cabin,         PROP_FRUITSTAND_B                                           ,0.0    , 4.25, -0.1,   3.6,   FALSE,  TRUE )
                   
                    
    //  Construction
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_CEMENTBAGS01                                           ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_CONS_CEMENTS01                                         ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_CONC_BLOCKS01A                                         ,0.0    , 2.0,  0.0,    1.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_CONC_BLOCKS01C                                         ,0.0    , 2.5,  0.0,    1.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_CONS_CRATE                                             ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_JYARD_BLOCK_01A                                        ,0.0    , 2.0,  0.0,    1.4,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_CONC_SACKS_02A                                         ,0.0    , 2.0,  -0.025, 1.4,   TRUE,   TRUE )

    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_BYARD_SLEEPER01                                        ,0.0    , 2.0,  0.0,    1.1,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_SHUTTERING01                                           ,0.0    , 2.0,  0.0,    0.8,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_SHUTTERING04                                           ,0.0    , 2.0,  0.0,    1.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_SHUTTERING02                                           ,0.0    , 3.0,  0.0,    1.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_SHUTTERING03                                           ,0.0    , 3.0,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_WOODPILE_01A                                           ,0.0    , 2.0,  0.0,    2.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_WOODPILE_01B                                           ,0.0    , 2.0,  0.0,    1.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_WOODPILE_01C                                           ,0.0    , 2.0,  0.0,    1.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_WOODPILE_03A                                           ,0.0    , 3.0,  0.0,    2.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_LOGPILE_03                                             ,0.0    , 2.75, 0.0,    2.7,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_LOGPILE_01                                             ,0.0    , 2.75, 0.0,    3.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_PIPES_CONC_01                                          ,0.0    , 1.5,  0.0,    1.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,  PROP_PIPES_02B                                              ,0.0    , 3.5,  0.0,    4.2,   FALSE,  TRUE )
//    Add_Prop_To_Prop_Sheet( iID, CAT_Construction,PROP_BARREL_PILE_01                                         ,0.0    , 1.5,  0.0,      FALSE,  TRUE )
    
    //  Containers
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_RUB_CONT_01B                                           ,0.0    , 3.75, 0.0,    4.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_CONTAINER_LD2                                          ,0.0    , 3.75, 0.0,    4.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_CONTAINER_01MB                                         ,0.0    , 3.75, 0.0,    4.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_CONTR_03B_LD                                           ,0.0    , 3.75, 0.0,    3.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_CONTAINER_03MB                                         ,0.0    , 3.75, 0.0,    3.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_RAIL_BOXCAR                                            ,0.0    , 5.0,  0.0,    4.4,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_RAIL_BOXCAR3                                           ,0.0    , 5.0,  0.0,    4.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_RAIL_BOXCAR4                                           ,0.0    , 5.0,  0.0,    4.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Containers,    PROP_RUB_RAILWRECK_2                                        ,0.0    , 4.5,  0.0,    4.2,   TRUE,   TRUE )
                                                            
    // Crates
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BOXPILE_02B                                            ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BOXPILE_04A                                            ,0.0    , 2.0,  0.0,    1.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BOXPILE_07A                                            ,0.0    , 3.0,  0.0,    1.6,   FALSE,  TRUE )
    //Add_Prop_To_Prop_Sheet( iID, CAT_Crates,      PROP_CRATEPILE_07A                                          ,0.0    , 2.0,  0.0,      FALSE,  TRUE ) //Behave weirdly during deletion 2504295
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BOX_WOOD01A                                            ,0.0    , 2.0,  0.0,    0.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BOX_WOOD03A                                            ,0.0    , 2.0,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BOX_WOOD04A                                            ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_PALLET_PILE_01                                         ,0.0    , 2.0,  0.0,    1.1,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_PALLET_PILE_02                                         ,0.0    , 3.0,  0.0,    1.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_03a"))         ,0.0    , 3.0,  0.0,    2.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))         ,0.0    , 2.5,  0.0,    1.6,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))        ,0.0    , 2.5,  0.0,    1.8,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))         ,0.0    , 2.5,  0.0,    1.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_CARGO_04B"))         ,0.0    , 2.5,  0.0,    2.4,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_CARGO_02A"))         ,0.0    , 3.25, 0.0,    2.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_01a"))        ,0.0    , 2.5,  0.0,    1.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_WATERCRATE_01                                          ,0.0    , 2.0,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_CASH_CRATE_01                                          ,0.0    , 1.5,  0.0,    0.4,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        HEI_PROP_HEIST_APECRATE                                     ,0.0    , 2.5,  0.0,    1.0,   FALSE,  TRUE )  
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        HEI_PROP_HEI_CASH_TROLLY_03                                 ,0.0    , 1.75, 0.0,    0.4,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_POSTBOX_01A                                            ,0.0    , 2.25, 0.0,    0.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_NEWS_DISP_02D                                          ,0.0    , 2.25, 0.0,    0.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_VEND_WATER_01                                          ,0.0    , 2.5,  0.0,    0.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("Prop_vend_snak_01_TU"))      ,0.0    , 2.0,  0.0,    0.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Train_ticket_02_TU"))   ,0.0    , 3.0,  0.0,    0.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Crates,        PROP_BYARD_FLOATPILE                                        ,0.0    , 3.0,  0.0,    1.7,   TRUE,   TRUE )
    
    //  Dumpsters
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_BIN_13A                                                ,0.0    , 2.0,  0.0,    0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_BIN_14A                                                ,0.0    , 2.5,  0.0,    1.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_DUMPSTER_3A                                            ,0.0    , 3.0,  0.0,    1.6,   FALSE,  TRUE )  //litter
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_DUMPSTER_4B                                            ,0.0    , 3.0,  0.0,    1.9,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_DUMPSTER_01A                                           ,0.0    , 2.75, 0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_DUMPSTER_02A                                           ,0.0    , 2.75, 0.0,    1.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_DUMPSTER_02B                                           ,0.0    , 2.75, 0.0,    1.5,   FALSE,  TRUE )  //litter
    Add_Prop_To_Prop_Sheet( iID, CAT_Dumpsters,     PROP_SKIP_06A                                               ,0.0    , 3.5,  0.0,    3.1,   TRUE,   TRUE )
        
    //  Explosive
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_BARREL_EXP_01A                                         ,0.0    , 1.75, 0.0,    0.7,   FALSE,  FALSE, 30 )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_BARREL_EXP_01B                                         ,0.0    , 1.75, 0.0,    0.7,   FALSE,  FALSE, 30 )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_BARREL_EXP_01C                                         ,0.0    , 1.75, 0.0,    0.7,   FALSE,  FALSE, 30 )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_JERRYCAN_01A                                           ,0.0    , 1.75, 0.0,    0.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GASCYL_01A                                             ,0.0    , 2.0,  0.0,    0.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GASCYL_02A                                             ,0.0    , 2.75, 0.0,    0.6,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GASCYL_02B                                             ,0.0    , 2.0,  0.0,    0.5,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GASCYL_03A                                             ,0.0    , 2.75, 0.0,    0.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GASCYL_03B                                             ,0.0    , 2.75, 0.0,    0.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GASCYL_04A                                             ,0.0    , 2.0,  0.0,    0.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_FIRE_EXTING_1B                                         ,0.0    , 1.75, 0.0,    0.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MB_ORDNANCE_02"))       ,0.0    , 2.0,  0.0,    1.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_STORAGETANK_02B                                        ,0.0    , 3.5,  0.0,    2.7,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GAS_TANK_02A                                           ,0.0    , 3.5,  0.0,    3.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GAS_TANK_04A                                           ,0.0    , 3.5,  0.0,    3.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Explosive,     PROP_GAS_TANK_02B                                           ,0.0    , 3.5,  -0.05,  3.3,   FALSE,  TRUE )
                                                        
    //  Machinery
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_ELECBOX_02A                                            ,0.0    , 2.5,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_ELECBOX_16                                             ,0.0    , 3.0,  0.0,    2.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_ELECBOX_14                                             ,0.0    , 2.5,  0.0,    1.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_ELECBOX_10_CR                                          ,0.0    , 3.25, 0.0,    2.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_ELECBOX_15_CR                                          ,0.0    , 2.75, 0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_ELECBOX_17_CR                                          ,0.0    , 3.0,  0.0,    1.8,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_IND_MECH_02A                                           ,0.0    , 2.75, 0.0,    1.7,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_IND_MECH_02B                                           ,0.0    , 3.0,  0.0,    1.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_SUB_TRANS_01A                                          ,0.0    , 5.0,  0.0,    3.2,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_SUB_TRANS_02A                                          ,0.0    , 4.0,  0.0,    1.9,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_SUB_TRANS_04A                                          ,0.0    , 3.5,  0.0,    1.8,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_GENERATOR_03B                                          ,0.0    , 4.5,  0.0,    1.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     INT_TO_ENUM(MODEL_NAMES, HASH("Prop_SET_Generator_01_CR"))  ,0.0    , 3.75, 0.0,    2.4,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_FEEDER1_CR                                             ,0.0    , 2.75, 0.0,    1.7,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Machinery,     PROP_IND_DEISELTANK                                         ,0.0    , 5.0,  -0.05,  3.7,   TRUE,   TRUE )
    
    //  Natural
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_JOSHUA_TREE_01A"))      ,0.0    , 4.5,  -0.05,  0.3,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_CACTUS_01E"))           ,0.0    , 7.0,  -0.25,  0.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_05C"))        ,0.0    , 2.5,  0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_04C"))        ,0.0    , 3.25, 0.0,    1.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_05D"))        ,0.0    , 2.25, 0.0,    1.0,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_03B_CR2"))    ,0.0    , 2.25, -0.125, 0.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_POT_PLANT_04B"))        ,0.0    , 2.25, 0.0,    0.7,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_ROCK_4_C_2"))           ,0.0    , 2.0,  0.0,    1.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_ROCK_4_BIG"))           ,0.0    , 2.25, -0.125, 2.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_ROCK_4_BIG2"))          ,0.0    , 2.75, -0.05,  2.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_TREE_OLIVE_CR2"))       ,0.0    , 18.5, -0.1,   2.1,   TRUE,   TRUE ,DEFAULT, 0.2)
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_TREE_ENG_OAK_CR2"))     ,0.0    , 18.5, -0.1,   1.6,   TRUE,   TRUE ,DEFAULT, 0.2)
    //Add_Prop_To_Prop_Sheet( iID, CAT_Natural,     INT_TO_ENUM(MODEL_NAMES, HASH("PROP_LOG_BREAK_01"))         ,0.0    , 2.75, 0.0,      FALSE,  TRUE ) //Removed for 2503656
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BUSH_MED_03_CR2"))      ,0.75    , 2.75, 0.0,   0.6,   TRUE,   TRUE)
    //Add_Prop_To_Prop_Sheet( iID, CAT_Natural,     INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BUSH_LRG_01E_CR2"))     ,0.0    , 2.5,  0.0,      TRUE,   TRUE )      //  remove? - thought ground needed - Update: Removed by Steve T. Causes problems and not worth a special case fix. Bug 2485860
    Add_Prop_To_Prop_Sheet( iID, CAT_Natural,       INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BUSH_LRG_01C_CR"))      ,0.0    , 2.75, -0.85,  1.0,   TRUE,   TRUE )      //  remove? - thought ground needed
    
    //  Ramps
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          LTS_PROP_LTS_RAMP_01                                        ,0.0    , 2.0,  0.0,    1.8,   TRUE,   TRUE)
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          LTS_PROP_LTS_RAMP_02                                        ,0.0    , 2.5,  0.0,    2.0,   TRUE,   TRUE)
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          LTS_PROP_LTS_RAMP_03                                        ,0.0    , 3.0,  0.0,    2.2,   TRUE,   TRUE)
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          PROP_SKIP_04                                                ,0.0    , 3.0,  0.0,    2.0,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          PROP_SKIP_03                                                ,0.0    , 2.5,  0.0,    2.1,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          PROP_SKIP_08A                                               ,0.0    , 3.0,  0.0,    2.4,   TRUE,   TRUE )
    //Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,        PROP_BYARD_RAMPOLD_CR                                       ,0.0    , 2.0,  0.0,      TRUE,   TRUE )  //Removed for 2506973
    Add_Prop_To_Prop_Sheet( iID, CAT_Ramp,          PROP_PILE_DIRT_07_CR                                        ,0.0    , 2.5,  -0.325, 2.6,   TRUE,   TRUE )
                                                                                                                            
    //  Roadside                                                                                                            
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_TRAFFICDIV_01                                          ,0.0    , 4.0,  0.0,    1.1,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_09A                                          ,0.0    , 2.75, 0.0,    0.2,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_09B                                          ,0.0    , 2.75, 0.0,    0.2,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_09C                                          ,0.0    , 2.75, 0.0,    0.1,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_09D                                          ,0.0    , 2.75, 0.0,    0.1,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_06Q                                          ,0.0    , 2.75, 0.0,    0.2,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_06R                                          ,0.0    , 2.75, 0.0,    0.2,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_05C                                          ,0.0    , 3.5,  0.0,    0.15,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_05D                                          ,0.0    , 3.5,  0.0,    0.15,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_05E                                          ,0.0    , 3.5,  0.0,    0.15,   FALSE,  TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_SIGN_ROAD_05F                                          ,0.0    , 3.5,  0.0,    0.15,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_ROADCONE02C                                            ,0.0    , 2.0,  0.0,    0.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_ROADCONE02A                                            ,0.0    , 2.0,  0.0,    0.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_ROADCONE01A                                            ,0.0    , 2.0,  0.0,    0.25,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_ROADPOLE_01A                                           ,0.0    , 2.0,  0.0,    0.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      IND_PROP_DLC_FLAG_02                                        ,0.0    , 2.75, 0.0,    0.15,   TRUE,   TRUE )                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      PROP_ROAD_MEMORIAL_02                                       ,0.0    , 2.25, -0.19,  1.3,   TRUE,   TRUE )                                                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_TAXISIGN_01A"))     ,0.0    , 2.0,  0.0,    2.1,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      HEI_PROP_CARRIER_DOCKLIGHT_01                               ,0.0    , 2.0,  0.0,    0.05,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Roadside,      HEI_PROP_CARRIER_DOCKLIGHT_02                               ,0.0    , 2.0,  0.0,    0.05,   TRUE,   TRUE )          
                                                                                                                            
    //  Trailers                                                                                                                        
    Add_Prop_To_Prop_Sheet( iID, CAT_Trailers,      PROP_FOOD_VAN_01                                            ,0.0    , 2.5,  0.0,    2.1,   TRUE,   TRUE )                                                  
    Add_Prop_To_Prop_Sheet( iID, CAT_Trailers,      PROP_FOOD_VAN_02                                            ,0.0    , 2.5,  0.0,    2.1,   TRUE,   TRUE )                                          
    Add_Prop_To_Prop_Sheet( iID, CAT_Trailers,      PROP_TANKTRAILER_01A                                        ,0.0    , 5.0,  -0.1,   4.0,   FALSE,  TRUE )                                                 
    Add_Prop_To_Prop_Sheet( iID, CAT_Trailers,      PROP_TRUKTRAILER_01A                                        ,0.0    , 5.75, 0.0,    4.0,   TRUE,   TRUE )                      
    Add_Prop_To_Prop_Sheet( iID, CAT_Trailers,      PROP_OLD_FARM_03                                            ,0.0    , 3.0,  0.0,    2.8,   FALSE,  TRUE )
	
	
    //  Wreckage
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_BUSWRECK_01                                        ,0.0    , 4.0,  -0.1,   2.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_BUSWRECK_06                                        ,0.0    , 4.0,  -0.05,  3.3,   TRUE,   TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_CARWRECK_11                                        ,0.0    , 2.0,  0.0,    1.9,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_CARWRECK_12                                        ,0.0    , 2.0,  0.0,    1.9,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_CARWRECK_13                                        ,0.0    , 2.0,  0.0,    1.9,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_CARWRECK_14                                        ,0.0    , 2.0,  -0.15,  1.8,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_CARWRECK_9                                         ,0.0    , 3.0,  0.0,    2.2,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Wreckage,      PROP_RUB_COUCH02                                            ,0.0    , 2.0,  0.0,    1.5,   FALSE,  TRUE )
                                                                                                                        
    //  Water
    Add_Prop_To_Prop_Sheet( iID, CAT_Water,         PROP_DOCK_BOUY_1                                            ,0.0    , 6.0,  0.0,    1.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Water,         PROP_DOCK_BOUY_2                                            ,0.0    , 6.0,  0.0,    1.4,   FALSE,  TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Water,         PROP_BYARD_FLOAT_02                                         ,0.0    , 3.0,  0.0,    1.1,   FALSE,  TRUE )
    //Add_Prop_To_Prop_Sheet( iID, CAT_Water,       PROP_IND_BARGE_01_CR                                        ,0.0    , 2.0,  0.0, FALSE,     TRUE )
    Add_Prop_To_Prop_Sheet( iID, CAT_Water,         PROP_JETSKI_RAMP_01                                         ,0.0    , 3.0,  0.0,    1.4,   FALSE,  TRUE )



	//Special Race Props pack additions added to various stunt categories
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	
	

	
	
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))		 ,0.05    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)	//Destructible - Submerged check needed
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall"))						 ,0.05    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1) 	//Destructible - Submerged check needed
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_tube_wall"))						 ,13.0    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)	//Destructible - Removed for 3471805
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_03"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			
			
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_Spec_Races_Blimp_Sign"))		 ,20.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Removing after discussion.  This would need a rendertarget to get the display working, text entry with profanity checks AND new save field for the text.
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Take_Flight_Sign"))		 ,4.5    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Might need to watch out for this one. Has moving parts. Causes problems with bones! Removing.
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Ron_Sign"))		 ,0.0    , 12.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Mp_Spec_Races_Xero_Sign"))		 ,0.0    , 12.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			//Above all tested.
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_1_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_2_04a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_3_03a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_4_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_5_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			
			//Removing these, they're essentially just a few concentric circles to denote a bullseye with no text label attached.
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_s_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_m_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_b_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

			/*Not including these as they are merely constinuent parts of a target which we already have...
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_03a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_02a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_06a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_07a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_04a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_small_05a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			*/


			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_long_01a"))		 ,1.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			
			//Removing this one. Just a blank target we already have a variation of.
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_large_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_trap_01a"))		 ,1.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_target_trap_02a"))		 ,1.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg11"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			

			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			//Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) Removed for 3471218
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL3_Fixed"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

			
			//These tubes might  need neon applied? Checked. Orange or Cyan Tint didn't work. They strobe independently depending on prop type.
			//DMPROP_IS_PROP_A_STUNT_PROP(ThisCategoryIndex) have something similar for stuntTubes.
		
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_XXS_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			
		
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_XS_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 

			
			
			
			
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_S_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 

			
			
			
			
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_M_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			
			
			
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_L_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			//Tested Done.
			
			
			
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_stunt_tube_crn2_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 

			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Crn_30D_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_15D_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_01a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_02a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_03a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_04a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_stunt_Tube_Crn_5D_05a"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 

			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Spec_Tube_Refill"))		 ,13.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
	
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U5"))		 	,1.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U15"))		 ,1.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U30"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_U45"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
	
	
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D5"))			 ,8.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D15"))		 ,21.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D30"))		 ,41.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Straight_L_D45"))		 ,59.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) 

							
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Refill"))		 ,0.7    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Refill_T2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Track_Refill_T1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			
			Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_SR_Track_JumpWall"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
							
	#ENDIF







	//Stunt Categories.
	#IF	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT // !! Remember to increment const ciMAXTYPES in the FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT wrapper for every new prop added. !
	IF NOT g_sMPTunables.bDISABLE_STUNT_PROPS_DIRECTOR
		//Ramps / Loops / Spirals
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_loop"))	 	,6.6    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_loop"))			,0.3    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_hloop"))	 		,19.7    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_multi_loop_rb"))		,0.1    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)


		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_xs"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_s"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_m"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_l"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_xl"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_jump_xxl"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //This and the previous 5 ramps seemed to have a sensible setup that didn't require mover adjusting.
		
		
		//Moved Flip Ramps after normal ramps and put small before medium for bug 3192031
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_s"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_sb"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_m"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_adj_flip_mb"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)//This and the previous 3 ramps seemed to have a sensible setup that didn't require mover adjusting.

		
		
		//These are tricky to embed.
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_01a"))	 	,3.4    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_01b"))	 	,5.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_01c"))	 	,8.4    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_02a"))	 	,3.3    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_02b"))	 	,5.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_jump_02c"))	 	,8.2    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Stunt Track Ramp XXL Wide - recheck. All previous ramps fine.
		
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_s"))	 		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_s"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_m"))	 		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_m"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l"))	 		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_l"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_xxl"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_ramp_spiral_l_xxl"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //All spirals look okay.
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_s"))	 			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_m"))	 			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_l"))	 			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_sb"))	 			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_mb"))	 			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump_lb"))	 			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //All of these standard six ramps seem fine.
		
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_ramp"))	 				,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Giant stunt Ramp thin
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_wideramp"))	 			,123.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_qp3"))		 	,21.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_qp2"))		 	,27.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_qp"))		 	,31.3    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_hump_01"))	 	,21.6    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntRamps,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_hump_02"))	 	,22.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//All items above tested.
		
		
		
		
		//Tubes.
		/* Removed for bug 3191366 - these are duplicates.
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_xs"))	 		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_s"))			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		*/
		
		//Moved tube straights to the beginning of the category and put the _crn2 last as per request of bug 3191633
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_xxs"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_xs"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_s"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_m"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_l"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn"))		 	,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn_5d"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn_15d"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn_30d"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn2"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fork"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_gap_01"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_gap_02"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_gap_03"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_cross"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//Removing for A class bug 3199647 - Bones causes fatal crash when placed after snapping to other tube section.
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_end"))			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speed"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_tube_02"))			,61.5    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)  //This is the really tall blend prop.
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_qg"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_hg"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_jmp"))			,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_jmp2"))		,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Removing for 3199133
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_ent"))		 	,12.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		/*Removed after discussion with Ben due to crazy physical behaviour on peds.
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_01"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_02"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_03"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_04"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTubes,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_fn_05"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		*/
		
		//
		
		//Dynamic. //These have mover adjustments for 3199778
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_sball"))		 ,1.7    , 2.0,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_ball"))		 ,2.5    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_lball"))		 ,2.5    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)  
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowling_pin"))		 ,1.0    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntDynamic,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowling_ball"))		 ,3.5    , 2.5,  0.0,    2.1,   FALSE,   FALSE, 1000, 1)
		//All dynamic stunt props above tested.
		

		
		//Track
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_start"))	 		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_start_02"))			,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_bar_m"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_5D_bar"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_15D_bar"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_30D_bar"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_180D_bar"))	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_m"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_5d"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_15d"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_30d"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_180d"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		/*Removed for 3192435
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_l"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_l"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		*/
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_l_b"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_l_b"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		/*Removed for 3192435
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_bar_l"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_bar_l"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		*/
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend_bar_l_b"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_bend2_bar_l_b"))	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_fork"))		 		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_cross"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_fork_bar"))		 	,1.2    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_cross_bar"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_tube_01"))		 	,12.3    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Moved up for 3194732
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_L"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_R"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_L_02"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_chicane_R_02"))		,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_block_01"))		 	,9.3    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)  //Obstacle
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_block_02"))		 	,9.3    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_block_03"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_gantry_01"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Moved to Stunt Misc for 3194789
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel"))		 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel_ads_01a"))	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel_ads_01b"))	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_funnel_ads_01c"))	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_link"))		 		,0.0	, 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_bar_l"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_lm_bar"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_bar_m"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_bar_s"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_l"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_lm"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_m"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTrack,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_straight_s"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//Tyres
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_01"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_03"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_04"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_05"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_06"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_07"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_08"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_09"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_010"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_011"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_012"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_013"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_014"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_014"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) - Removed dupe for 3194627.
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r06"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r07"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r011"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r012"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r013"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r014"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r019"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r04"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r05"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r08"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r09"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r010"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r015"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r016"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r017"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0r018"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l06"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l07"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l013"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l014"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l015"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l020"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l04"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l05"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l08"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l010"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l012"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l16"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)//Check weird numbering!
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l17"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)//Check weird numbering!
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l018"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntTyres,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_tyre_wall_0l019"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		
		//Misc
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_01"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_01b"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_02"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_02b"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_03"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_start_line_03b"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_gantry_01"))	   ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Moved from Stunt Track for 3194789
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_target_small"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_target"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) - Removing - causes a lot of issues 3196045, 3195934, 3196075
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_landing_zone_01"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_tyre_01a"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

		
		
	
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_small_01"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_constraction_01a"))	 	,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_tannoy"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_speakerstack_01a"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bowlpin_stand"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_soccer_goal"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//Removed dynamic stop sign for bug 3199133
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_stop_sign"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_01"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_03"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_04"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_05"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_06"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_07"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_08"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_09"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_10"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_11"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_12"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_13"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_14"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_sign_circuit_15"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_01"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_03"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_04"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_05"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_06"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_07"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_08"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_09"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//Re-ordered for 3198144
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_11"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_10"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_12"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_13"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_14"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		

		
		
		//Walls
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_01"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_01b"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_04"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45l"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45la"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45r"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_45ra"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90l"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90lb"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90r"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_90rb"))		 ,8.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)

		//These need restricted for 3201251
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_02"))		 ,66.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_02b"))		 ,66.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_05"))		 ,66.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntWalls,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_wallride_05b"))		 ,66.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		
		
		
		//Extra Tracks
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_start_02"))		 ,50.0   , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_start"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_st_02"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_st_01"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_exshort"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_short"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_straight"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_cutout"))		 ,51.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Stunt track Bridge was slightly more embedded on reset than the previous Extra Track props.
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_otake"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_fork"))		 ,50.0.    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_funnel"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_funlng"))		 ,50.4    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_slope15"))		 ,69.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_slope30"))		 ,79.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_slope45"))		 ,76.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_hill"))		 ,49.5    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Checked movers up to this point.
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_hill2"))		 ,53.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_bumps"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_jump"))		 ,61.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump15"))		 ,53.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump30"))		 ,55.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
			
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_jump45"))		 ,57.9    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		/*Moved and reordered for 3198281
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_start_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_start"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_st_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_st_01"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		*/
				
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_turn"))		 ,91.2    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_sh15"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_sh30"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_sh45"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Track_SH45_a"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_uturn"))		 ,102.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_link"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwlink"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwlink_02"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwshort"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwsh15"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwturn"))		 ,50.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwuturn"))		 ,57.8    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE) //Dodgy!
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwslope15"))		 ,66.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwslope30"))		 ,66.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_track_dwslope45"))		 ,66.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//3194423 requested that these speed up and slow down props be moved to the stunt misc category from the original CAT_StuntExtraTrk. Agreed,
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)	
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntMisc,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		//Had problems with these two. Missing model?
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_trail_CylinderRun"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE) 
		//Add_Prop_To_Prop_Sheet( iID, CAT_StuntExtraTrk,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_trail_LogJump_01"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE)	
		
		
		
			
		//Blocks
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_sml1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_sml2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_sml3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_mdm1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_mdm2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_mdm3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_lrg1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_lrg2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_lrg3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_xl1"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_xl2"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_xl3"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_01"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_02"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_03"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_04"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		Add_Prop_To_Prop_Sheet( iID, CAT_StuntBlocks,  INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_bblock_huge_05"))		 ,0.0    , 2.5,  0.0,    2.1,   TRUE,   FALSE, 1000, 1, TRUE)
		
		ENDIF
		// ! Remember to increment const ciMAXTYPES in the FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT wrapper for every new prop added. !
		
	#ENDIF
	








    moverProp.vPos      = moverProp.vPos
    moverProp.mModel    = DUMMY_MODEL_FOR_SCRIPT
    moverProp.obj       = NULL
    
    //Steve T removed this. Not very clear and the number of types in a category can easily be incremented in Fill_Prop_Sheet...
    //Calculate the max number of types per category
    /*
    int i
    FOR i = 0 to ciMAXTYPES -1
        IF eTypeModels[i] <> DUMMY_MODEL_FOR_SCRIPT
            ePropTypesPerCategory[eTypeCategory[i]] += 1
        endif
    ENDFOR
    */

    eSelectedPropType[0] = 1    //1st Category name
    
    ePropTypesPerCategory[0]    = ePropTypesPerCategory[0]
    ePropTypes[0].name          = ePropTypes[0].name
    eProps[0]                   = eProps[0]
ENDPROC


//===============Function lifted from fmmc_creation.sch
FUNC BOOL DMPROP_CREATE_PROP_PTFX(MODEL_NAMES mnProp, OBJECT_INDEX oiProp)
	UNUSED_PARAMETER(mnProp)
	UNUSED_PARAMETER(oiProp)
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	//Hoop ptfx for stunt props
	IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_hoop_constraction_01a"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Hoop_Small_01"))
	
		//Get the ptfx variables
		STRING strPTFXAsset	= "scr_stunts"
		STRING strPTFXName =  "scr_stunts_fire_ring"
		VECTOR vOffset = <<0.0,0.0,25.0>>
		VECTOR vRotation = <<-12.5,0.0,0.0>>
		FLOAT fScale = 1.0
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Hoop_Small_01"))
			strPTFXAsset = "core"
			strPTFXName = "ent_amb_fire_ring"
			vOffset = <<0.0,0.0,4.5>>
			vRotation = <<0.0,0.0,90.0>>
			fScale = 3.5
		ENDIF
		
		REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
		IF HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
			USE_PARTICLE_FX_ASSET(strPTFXAsset) 
			START_PARTICLE_FX_LOOPED_ON_ENTITY(strPTFXName, oiProp, vOffset, vRotation, fScale)
			RETURN TRUE
		ENDIF

		RETURN FALSE
	ENDIF
	#ENDIF
	
	
	//Add tint for neon blocks.
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml1"))
			SET_OBJECT_TINT_INDEX(oiProp, 1)	//Red
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm1"))
			SET_OBJECT_TINT_INDEX(oiProp, 2)	//Cyan
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg11"))
			SET_OBJECT_TINT_INDEX(oiProp, 3)	//Magenta
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL1"))
			SET_OBJECT_TINT_INDEX(oiProp, 5)	//Bright White //4 is Dull White so not using that.
		ENDIF
		
		//-
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml2"))
			SET_OBJECT_TINT_INDEX(oiProp, 9)	//Green //Bright White
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm2"))
			SET_OBJECT_TINT_INDEX(oiProp, 1)	//Red //6 is Dull White
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg2"))
			SET_OBJECT_TINT_INDEX(oiProp, 7)	//Yellow
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL2"))
			SET_OBJECT_TINT_INDEX(oiProp, 8)	//Orange
		ENDIF
		
		
		//-
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Sml3"))
			SET_OBJECT_TINT_INDEX(oiProp, 9)	//Green
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Mdm3"))
			SET_OBJECT_TINT_INDEX(oiProp, 10)	//Magenta
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_Lrg3"))
			SET_OBJECT_TINT_INDEX(oiProp, 11)	//Cyan
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL3"))
			SET_OBJECT_TINT_INDEX(oiProp, 5)	//Bright White //12 is Dull White
		ENDIF
		
		IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_Special_BBlock_XL3_Fixed"))
			SET_OBJECT_TINT_INDEX(oiProp, 13)	//Orange
		ENDIF
		
	#ENDIF
	
	RETURN TRUE		//By default, props don't need PTFX
ENDFUNC



#IF IS_DEBUG_BUILD

PROC INIT_PROP_DATA_DEBUG_WIDGET()
    START_WIDGET_GROUP( "Director Mode Prop Data." )
        ADD_WIDGET_FLOAT_SLIDER( "Chevron Y", fChevronY, 0.0, 30.0, 0.05 )
        ADD_WIDGET_FLOAT_SLIDER( "Ground Y", fGroundY, -10.0, 10.0, 0.05 )
        ADD_WIDGET_BOOL( "Refresh data", bRefreshValues )
    STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_PROP_DATA_DEBUG_WIDGET()
    IF bRefreshValues
        FILL_IN_PROP_DATA()
        bRefreshValues = FALSE
    ENDIF
ENDPROC

#ENDIF
