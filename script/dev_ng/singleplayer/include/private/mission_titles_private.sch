USING "globals.sch"
USING "rage_builtins.sch"
USING "candidate_public.sch"
USING "player_ped_public.sch"

// *****************************************************************************************
//		SCRIPT NAME		:	mission_titles_private.sch
//		AUTHOR			:	Andy Minghella
//		DESCRIPTION		:	Handles displaying the mission title for story + RC missions
// *****************************************************************************************

// display positions
// default
CONST_FLOAT	FLOW_MISSION_NAME_POS_X					0.0 
CONST_FLOAT	FLOW_MISSION_NAME_POS_Y					-0.014

// races (test with fanatic 3)
CONST_FLOAT	FLOW_MISSION_NAME_POS_X_RACE			0.0  
CONST_FLOAT	FLOW_MISSION_NAME_POS_Y_RACE			-0.07 

// rampages
CONST_FLOAT	FLOW_MISSION_NAME_POS_X_RAMPAGE			0.0  
CONST_FLOAT	FLOW_MISSION_NAME_POS_Y_RAMPAGE			-0.077

// time limit (test with SP_HEIST_FINALE_1 or SP_MISSION_ASSASSIN_2)
CONST_FLOAT	FLOW_MISSION_NAME_POS_X_TIME_LIMIT		0.0  
CONST_FLOAT	FLOW_MISSION_NAME_POS_Y_TIME_LIMIT		-0.05 

// timer bar (test with Michael Event Amanda)
CONST_FLOAT	FLOW_MISSION_NAME_POS_X_TIMER_BAR		0.0  
CONST_FLOAT	FLOW_MISSION_NAME_POS_Y_TIMER_BAR		-0.035 // this was -0.03 but korean text over spills

// other display constants
CONST_FLOAT	FLOW_MISSION_NAME_SCALE_X				0.67 
CONST_FLOAT	FLOW_MISSION_NAME_SCALE_Y				0.67  

CONST_FLOAT FLOW_MISSION_NAME_SIZE_X				0.0
CONST_FLOAT FLOW_MISSION_NAME_SIZE_Y				0.01

CONST_INT	FLOW_MISSION_NAME_DISPLAY_TIME			9000 
CONST_INT	FLOW_MISSION_NAME_DISPLAY_FADE_TIME		1000

CONST_FLOAT	FLOW_MAX_MISSION_TITLE_WIDTH			0.17
CONST_FLOAT	FLOW_MAX_MISSION_TITLE_WIDTH_4_3		0.14

FLOAT	fSafeToDisplayTimer = 0
VECTOR 	vOriginalSubtitleTextPos
FLOAT 	fSubtitleTextPosOffsetY = -0.0375	
FLOAT 	fMaxMissionTitleWidth = FLOW_MAX_MISSION_TITLE_WIDTH		// if the mission title is longer than this we move the text up

// TIMERS
CONST_FLOAT MISSION_TITLE_DELAY		2.5
CONST_FLOAT MINIGAME_TITLE_DELAY	1.5

/*
// debug stuff for repositioning text
FLOAT fTitleDisplayX = FLOW_MISSION_NAME_POS_X_RACE
FLOAT fTitleDisplayY = FLOW_MISSION_NAME_POS_Y_RACE

FLOAT fTitleTextScaleX = FLOW_MISSION_NAME_SCALE_X
FLOAT fTitleTextScaleY = FLOW_MISSION_NAME_SCALE_Y

FLOAT fTitleSizeX = FLOW_MISSION_NAME_SIZE_X
FLOAT fTitleSizeY = FLOW_MISSION_NAME_SIZE_Y

BOOL bWidgetsSetup = FALSE
BOOL bPrintTitleCoords = FALSE
*/

/// PURPOSE:
///    Returns the Course name Label for this BJ Course
/// PARAMS:
///    iBJCourse - Course ID
/// RETURNS:
///    TEXT_LABEL_7 Course name Label for this BJ Course
FUNC TEXT_LABEL_7 GET_BJ_COURSE_NAME_LABEL(INT iBJCourse)

	TEXT_LABEL_7 tCourseNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = iBJCourse
	IF IS_STRING_NULL_OR_EMPTY(tStatID)
		SCRIPT_ASSERT("ERROR: GET_BJ_COURSE_NAME_LABEL has been passed an illegal BJ COURSE ID")
	ELSE
		tCourseNameLabel = "MGBJ_"
		tCourseNameLabel += tStatID
	ENDIF
	
	RETURN tCourseNameLabel
ENDFUNC

/// PURPOSE:
///    Returns the Lesson name Label for this flight school lesson
/// PARAMS:
///    iFSLesson - Lesson ID
/// RETURNS:
///    TEXT_LABEL_7 Lesson name Label for this FS Lesson
FUNC TEXT_LABEL_7 GET_FS_LESSON_NAME_LABEL(INT iFSLesson)

	TEXT_LABEL_7 tLessonNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = iFSLesson
	IF IS_STRING_NULL_OR_EMPTY(tStatID)
		SCRIPT_ASSERT("ERROR: GET_FS_LESSON_NAME_LABEL has been passed an illegal FS LESSON ID")
	ELSE
		tLessonNameLabel = "MGFS_"
		tLessonNameLabel += tStatID
	ENDIF
	
	RETURN tLessonNameLabel
ENDFUNC

/// PURPOSE:
///    Returns the course name Label for this Stunt plane time trial course
/// PARAMS:
///    iSPTTLesson - Course ID
/// RETURNS:
///    TEXT_LABEL_7 Course name Label for this SPTT Course
FUNC TEXT_LABEL_7 GET_SP_COURSE_NAME_LABEL(INT iSPTTCourse)

	TEXT_LABEL_7 tCourseNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = iSPTTCourse
	IF IS_STRING_NULL_OR_EMPTY(tStatID)
		SCRIPT_ASSERT("ERROR: GET_SP_COURSE_NAME_LABEL has been passed an illegal SP COURSE ID")
	ELSE
		tCourseNameLabel = "MGSP_"
		tCourseNameLabel += tStatID
	ENDIF
	
	RETURN tCourseNameLabel
ENDFUNC

/// PURPOSE:
///    Returns the course name Label for this Offroad race course
/// PARAMS:
///    iORRCourse - Course ID
/// RETURNS:
///    TEXT_LABEL_7 Course name Label for this ORR Course
FUNC TEXT_LABEL_7 GET_OR_COURSE_NAME_LABEL(INT iORRCourse)

	TEXT_LABEL_7 tCourseNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = iORRCourse
	IF IS_STRING_NULL_OR_EMPTY(tStatID)
		SCRIPT_ASSERT("ERROR: GET_OR_COURSE_NAME_LABEL has been passed an illegal OR COURSE ID")
	ELSE
		tCourseNameLabel = "MGOR_"
		tCourseNameLabel += tStatID
	ENDIF
	
	RETURN tCourseNameLabel
ENDFUNC

/// PURPOSE:
///    Returns the course name for this city race
/// PARAMS:
///    iCRCourse - Course ID
/// RETURNS:
///    TEXT_LABEL_7 Course name for this city race
FUNC TEXT_LABEL_7 GET_CR_COURSE_NAME_LABEL(INT iCRCourse)

	TEXT_LABEL_7 tCourseNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = iCRCourse
	IF IS_STRING_NULL_OR_EMPTY(tStatID) OR iCRCourse = 3	// City Race 3 no longer exists
		SCRIPT_ASSERT("ERROR: GET_CR_COURSE_NAME_LABEL has been passed an illegal CR COURSE ID")
	ELSE
		tCourseNameLabel = "MGCR_"
		tCourseNameLabel += tStatID
	ENDIF
	
	RETURN tCourseNameLabel
ENDFUNC

/// PURPOSE:
///    Returns the course name for this sea race
/// PARAMS:
///    iCRCourse - Course ID
/// RETURNS:
///    TEXT_LABEL_7 Course name for this sea race
FUNC TEXT_LABEL_7 GET_SR_COURSE_NAME_LABEL(INT iSRCourse)

	TEXT_LABEL_7 tCourseNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = iSRCourse
	IF IS_STRING_NULL_OR_EMPTY(tStatID)
		SCRIPT_ASSERT("ERROR: GET_SR_COURSE_NAME_LABEL has been passed an illegal SR COURSE ID")
	ELSE
		tCourseNameLabel = "MGSR_"
		tCourseNameLabel += tStatID
	ENDIF
	
	RETURN tCourseNameLabel
ENDFUNC

#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    	Returns the string ID of the story or RC mission
	/// RETURNS:
	///    STRING id of the mission
	FUNC STRING GET_MISSION_DISPLAY_STRING_FROM_ID(enumGrouping eMissionType, INT iMissionID)
		SWITCH eMissionType
		
			CASE CP_GROUP_MISSIONS
				RETURN GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionID))
			
			CASE CP_GROUP_RANDOMCHARS
				RETURN GET_RC_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(g_eRC_MissionIDs, iMissionID))
		
			CASE CP_GROUP_MINIGAMES
				RETURN "Minigame" //GET_BJ_COURSE_NAME_LABEL(iMissionID)
			BREAK
		
		ENDSWITCH
		RETURN "Unknown"
	ENDFUNC
	
/*
	// debug stuff for repositioning text
	PROC SETUP_TITLE_WIDGET()
		IF bWidgetsSetup = FALSE
			START_WIDGET_GROUP("Mission Titles- Debug")
				ADD_WIDGET_FLOAT_SLIDER("title x", fTitleDisplayX, -1.0, 1.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("title y", fTitleDisplayY, -1.0, 1.0, 0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("scale x", fTitleTextScaleX, 0.0, 1.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("scale y", fTitleTextScaleY, 0.0, 1.0, 0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("fSizeX ", fTitleSizeX, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fSizeY ", fTitleSizeY, 0.0, 1.0, 0.01)
				
				ADD_WIDGET_FLOAT_SLIDER("subtitle shift y", fSubtitleTextPosOffsetY, -1.0, 1.0, 0.1)
				ADD_WIDGET_INT_SLIDER("max title length", iMaxMissionTitleLength, 1, 32, 1)
				ADD_WIDGET_BOOL("print", bPrintTitleCoords)
			STOP_WIDGET_GROUP()
			bWidgetsSetup= TRUE
		ELSE
			IF bPrintTitleCoords = TRUE
				CPRINTLN(DEBUG_FLOW, "pos x = ", fTitleDisplayX, "  y = ", fTitleDisplayY)
				CPRINTLN(DEBUG_FLOW, "scale x = ", fTitleTextScaleX, "  y = ", fTitleTextScaleY)
				CPRINTLN(DEBUG_FLOW, "size x = ", fTitleSizeX, "  y = ", fTitleSizeY)
				bPrintTitleCoords = FALSE
			ENDIF
		ENDIF
	ENDPROC
*/
	
#ENDIF

/// PURPOSE:
///    Returns the text label of the mission name for selected mission
/// PARAMS:
///    iMissionIndex - index of appropriate array
///    eMissionType - CP_GROUP_MISSIONS / CP_GROUP_RANDOMCHARS / CP_GROUP_MINIGAMES
/// RETURNS:
///    the text label of the mission name
FUNC TEXT_LABEL_7 GET_MISSION_TITLE(INT iMissionIndex, enumGrouping eMissionType, INT iVariation)

	TEXT_LABEL_7 tMissionNameLabel = ""
			
	SWITCH eMissionType
		CASE CP_GROUP_MISSIONS
			tMissionNameLabel = GET_SP_MISSION_NAME_LABEL(INT_TO_ENUM(SP_MISSIONS, iMissionIndex))
		BREAK
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		CASE CP_GROUP_RANDOMCHARS
			tMissionNameLabel = GET_RC_MISSION_NAME_LABEL(INT_TO_ENUM(g_eRC_MissionIDs, iMissionIndex))
		BREAK
		#endif
		#endif
		
		CASE CP_GROUP_MINIGAMES
			SP_MINIGAMES eMiniGame
			eMiniGame = INT_TO_ENUM(SP_MINIGAMES, iMissionIndex)
		
			SWITCH eMiniGame
				// minigames with multiple variations
				CASE MINIGAME_BASEJUMPING
					tMissionNameLabel = GET_BJ_COURSE_NAME_LABEL(iVariation)
				BREAK
				
				CASE MINIGAME_STREET_RACES
					tMissionNameLabel = GET_CR_COURSE_NAME_LABEL(iVariation)
				BREAK
				
				CASE MINIGAME_SEA_RACES
					tMissionNameLabel = GET_SR_COURSE_NAME_LABEL(iVariation)
				BREAK
				
				CASE MINIGAME_STUNT_PLANES
					tMissionNameLabel = GET_SP_COURSE_NAME_LABEL(iVariation)
				BREAK
				
				CASE MINIGAME_PILOT_SCHOOL
					tMissionNameLabel = GET_FS_LESSON_NAME_LABEL(iVariation)
				BREAK
				    
				CASE MINIGAME_OFFROAD_RACES
					tMissionNameLabel = GET_OR_COURSE_NAME_LABEL(iVariation)
				BREAK
			
				// other minigames only have 1 variation
				DEFAULT
					tMissionNameLabel = GET_MINIGAME_NAME_LABEL(eMiniGame)
				BREAK
			
			ENDSWITCH
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_FLOW,"GET_MISSION_TITLE passed an invalid mission type: ", eMissionType)
		BREAK
	ENDSWITCH
	
	RETURN tMissionNameLabel
ENDFUNC

/// PURPOSE:
///    Stops mission title displaying and resets the variables it uses
PROC MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	IF g_eMissionTitleState <> MTS_DONE
		CPRINTLN(DEBUG_FLOW, "MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE() - CALLED BY ", GET_THIS_SCRIPT_NAME())
	ENDIF
	
	IF g_bMissionTitleMovedSubtitles
		RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
		g_bMissionTitleMovedSubtitles = FALSE
		// Force the menu system to set the position..
		g_sMenuData.bSubtitlesMoved = FALSE
	ENDIF
	g_eMissionTitleState = MTS_DONE
	g_iFlowTimeBeganDisplayMissionTitle = -1
	g_iFlowDisplayMissionTitle = -1
ENDPROC

/// PURPOSE:
///    Checks if we can now start displaying the mission name
/// PARAMS:
///    bWaitingToDisplay - TRUE if we are waiting to display title, FALSE if we've already started displaying it
/// RETURNS:
///    TRUE if we can now start displaying the mission name, FALSE otherwise
FUNC BOOL IS_IT_SAFE_TO_DISPLAY_MISSION_NAME(BOOL bWaitingToDisplay, TEXT_LABEL_23 tIntroMocap)

	// Some checks that we do in all states
	
	// not on mission, or screen hasn't fade in
	IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
	OR g_flowUnsaved.bUpdatingGameflow
	OR (g_bMissionTitleBlocked = TRUE)
	OR NOT IS_SCREEN_FADED_IN()
		//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: Not on mission, not updating flow, or faded out, or title display blocked.")
		RETURN FALSE
	ENDIF

	// State only checks
	SWITCH g_eMissionTitleState
	
		CASE MTS_CHECK_INTRO
			//Work out if flagged mission has a cutscene.
			IF ARE_STRINGS_EQUAL(tIntroMocap, "NONE")
			OR IS_STRING_NULL_OR_EMPTY(tIntroMocap)
				// this mission doesn't have an intro, skip to post intro stage
				g_eMissionTitleState = MTS_POST_INTRO
			ELSE
				//This mission has an intro cut-scene. Wait for it to load
				g_eMissionTitleState = MTS_LOAD_INTRO
			ENDIF
		BREAK
		
		CASE MTS_LOAD_INTRO
			// wait for intro to load
			IF HAS_CUTSCENE_LOADED()
				g_eMissionTitleState = MTS_PLAY_INTRO
			ELSE
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: Mission waiting for intro to load.")
			ENDIF
		BREAK
		
		CASE MTS_PLAY_INTRO
			//Show title part way through intro cut-scene
			IF IS_CUTSCENE_PLAYING()
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: Intro has started playing.")
				g_eMissionTitleState = MTS_DISPLAY_CUTSCENE
				RETURN TRUE
			ELSE
				IF NOT IS_CUTSCENE_ACTIVE()
					//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: Intro skipped.")
					// cut-scene has been skipped- show title now
					g_eMissionTitleState = MTS_POST_INTRO
				ENDIF
			ENDIF
		BREAK
		
		CASE MTS_POST_INTRO
			// Mission has no intro, or it is finished
			// safe to display as long as there isn't another cut-scene playing
			IF IS_CUTSCENE_PLAYING()
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: Post intro, waiting for a cut-scene to end.")
			ELSE
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: Post intro + no cut-scene playing..")
				g_eMissionTitleState = MTS_DISPLAY_GAMEPLAY
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE MTS_DISPLAY_CUTSCENE
			IF IS_CUTSCENE_PLAYING()
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: MTS_DISPLAY_CUTSCENE, cut-scene playing, still safe.")
				RETURN TRUE
			ELSE
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: MTS_DISPLAY_CUTSCENE. cut-scene ended")
				IF bWaitingToDisplay = TRUE
					// If we're not already displaying the title, we need to change state
					// and try to display it over gameplay instead
					g_eMissionTitleState = MTS_DISPLAY_GAMEPLAY
				ENDIF
			ENDIF
		BREAK
		
		CASE MTS_DISPLAY_GAMEPLAY
			// displaying title over gameplay...
			//safe to do so unless a cut-scene starts or the phone is onscreen
			
			IF IS_CUTSCENE_PLAYING()
			OR IS_PHONE_ONSCREEN()
			OR IS_SELECTOR_ONSCREEN()
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: MTS_DISPLAY_GAMEPLAY, cut-scene started / phone / seelctor on screen.")
			ELSE
				//CPRINTLN(DEBUG_FLOW, "IS_IT_SAFE_TO_DISPLAY_MISSION_NAME: MTS_DISPLAY_GAMEPLAY. still safe")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the flow has requested a mission title be displayed on screen.
///    Waits for any display-only-when conditions are met and then fades in the title,
///    leaves it onscreen for a predetermined amount fo time, and then fades it out.   
/// PARAMS:
///    tIntroMocap - the text label of the mocap for this mission
///    bUsesRaceHUD - does this mission use the race HUD? (like Fanatic 1-3)
///    bRampage - Is this mission a rampage? (they also have a HUD display in bottom right)
///    iVariation - used by minigames to specify which variation is running, so we get correct title
PROC UPDATE_MISSION_NAME_DISPLAYING(TEXT_LABEL_23 tIntroMocap, BOOL bUsesRaceHUD = FALSE, BOOL bRampage = FALSE, BOOL bTimeLimit = FALSE, BOOL bTimerBar = FALSE, INT iVariation=0)

	//Check for new display requests.
	IF g_eMissionTitleState <>  MTS_DONE
	
		// we are still waiting to display the title
		IF g_iFlowTimeBeganDisplayMissionTitle = -1 // (this is reset to -1 when we start a mission)
	
			IF IS_IT_SAFE_TO_DISPLAY_MISSION_NAME(TRUE, tIntroMocap)
				
				FLOAT fTitleDelay
				IF g_eMissionTitleType = CP_GROUP_MINIGAMES
					fTitleDelay = MINIGAME_TITLE_DELAY
				ELSE
					fTitleDelay = MISSION_TITLE_DELAY
				ENDIF

				IF fSafeToDisplayTimer > fTitleDelay
					// it has been safe to display title for long enough now, show the title
					CPRINTLN(DEBUG_FLOW, "Started displaying mission title for ", GET_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionTitleType, g_iFlowDisplayMissionTitle), ".")
					g_iFlowTimeBeganDisplayMissionTitle = GET_GAME_TIMER()
					vOriginalSubtitleTextPos = GET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT)
					fSafeToDisplayTimer = 0.0
				ELSE
					// not been safe to display long enough yet, increase timer
					fSafeToDisplayTimer += GET_FRAME_TIME()
				ENDIF
			ELSE
				// not safe to start displaying title, reset safe timer
				fSafeToDisplayTimer = 0.0
			ENDIF
		ELSE
			// we are currently displaying the title
			INT iTimeDisplayed
			INT iAlpha
			INT iTimeLeft
			TEXT_LABEL_7 txtMissionTitle
			FLOAT fDisplayX, fDisplayY

			IF NOT IS_IT_SAFE_TO_DISPLAY_MISSION_NAME(FALSE, tIntroMocap)
				CPRINTLN(DEBUG_FLOW, "No longer safe to display mission title for ", GET_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionTitleType, g_iFlowDisplayMissionTitle), ". Clearing early.")
				g_iFlowTimeBeganDisplayMissionTitle = GET_GAME_TIMER() - FLOW_MISSION_NAME_DISPLAY_TIME
			ENDIF
		
			//Block area name from displaying as this will clash.
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		
			iTimeDisplayed = GET_GAME_TIMER() - g_iFlowTimeBeganDisplayMissionTitle
			
			IF iTimeDisplayed < FLOW_MISSION_NAME_DISPLAY_TIME 
			AND NOT IS_SCREEN_FADED_OUT()
			
				iAlpha = 255
				IF iTimeDisplayed < FLOW_MISSION_NAME_DISPLAY_FADE_TIME
					//Fade text in over the first second.
					iAlpha = CEIL(TO_FLOAT(iTimeDisplayed)/1000.0*255.0)
				ELSE
					iTimeLeft = FLOW_MISSION_NAME_DISPLAY_TIME - iTimeDisplayed
					IF iTimeLeft < FLOW_MISSION_NAME_DISPLAY_FADE_TIME
						//Fade text out over the last second.
						iAlpha = CEIL(TO_FLOAT(iTimeLeft)/1000.0*255.0)
					ENDIF
				ENDIF

				// set position of text
				SWITCH g_eMissionTitleState
					CASE MTS_POST_INTRO
					CASE MTS_DISPLAY_GAMEPLAY
						// if we are displaying over gameplay we may need to adjust y value
						IF bUsesRaceHUD = TRUE
							fDisplayX = FLOW_MISSION_NAME_POS_X_RACE
							fDisplayY = FLOW_MISSION_NAME_POS_Y_RACE
						ELIF bRampage = TRUE
							fDisplayX = FLOW_MISSION_NAME_POS_X_RAMPAGE
							fDisplayY = FLOW_MISSION_NAME_POS_Y_RAMPAGE
						ELIF bTimeLimit = TRUE
							fDisplayX= FLOW_MISSION_NAME_POS_X_TIME_LIMIT	
							fDisplayY= FLOW_MISSION_NAME_POS_Y_TIME_LIMIT
						ELIF bTimerBar = TRUE
							fDisplayX= FLOW_MISSION_NAME_POS_X_TIMER_BAR	
							fDisplayY= FLOW_MISSION_NAME_POS_Y_TIMER_BAR
						ELSE
							// use default
							fDisplayX= FLOW_MISSION_NAME_POS_X
							fDisplayY= FLOW_MISSION_NAME_POS_Y
						ENDIF
					BREAK
					
					DEFAULT
						// otherwise use default value
						fDisplayX= FLOW_MISSION_NAME_POS_X
						fDisplayY= FLOW_MISSION_NAME_POS_Y
					BREAK
				ENDSWITCH
				
				//Render title.
//				SETUP_TITLE_WIDGET()
//				UNUSED_PARAMETER(fDisplayX)
//				UNUSED_PARAMETER(fDisplayY)

				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
				SET_TEXT_FONT(FONT_CURSIVE)
				SET_TEXT_JUSTIFICATION(FONT_RIGHT)
				
				INT red, green, blue, alpha_value
				
				enumCharacterList ePlayerCharForColour = GET_CURRENT_PLAYER_PED_ENUM()
				
				#IF NOT USE_SP_DLC
					// Fix for 1993570: For Trevor1 use the previous player's colour.
					IF g_eMissionTitleType = CP_GROUP_MISSIONS
					AND g_iFlowDisplayMissionTitle = ENUM_TO_INT(SP_MISSION_TREVOR_1)
						ePlayerCharForColour = g_savedGlobals.sPlayerData.sInfo.ePreviousPed
					ENDIF
				#ENDIF
				
				// this is the default color - if we aren't a player character use this			
				SWITCH ePlayerCharForColour
					CASE CHAR_MICHAEL
						GET_HUD_COLOUR(HUD_COLOUR_MICHAEL, red, green, blue, alpha_value)
					BREAK
					CASE CHAR_FRANKLIN
						GET_HUD_COLOUR(HUD_COLOUR_FRANKLIN, red, green, blue, alpha_value)
					BREAK
					CASE CHAR_TREVOR
						GET_HUD_COLOUR(HUD_COLOUR_TREVOR, red, green, blue, alpha_value)
					BREAK
					DEFAULT
						red = 240
						green = 200
						blue = 80
				ENDSWITCH

				SET_TEXT_COLOUR(red, green, blue, iAlpha) // yellow text
				SET_TEXT_DROP_SHADOW()
				txtMissionTitle = GET_MISSION_TITLE(g_iFlowDisplayMissionTitle ,g_eMissionTitleType, iVariation)
	
				// debug version- for tweaking values
//				SET_SCRIPT_GFX_ALIGN_PARAMS(fTitleDisplayX, fTitleDisplayY, fTitleSizeX, fTitleSizeY) 
//				SET_TEXT_SCALE(fTitleTextScaleX, fTitleTextScaleY)
//				
//				BEGIN_TEXT_COMMAND_DISPLAY_TEXT(txtMissionTitle)
//				END_TEXT_COMMAND_DISPLAY_TEXT(fTitleDisplayX, fTitleDisplayY)
				
				// B*1396018 - Shift the subtitle if we using the default mission name pos
				IF (fDisplayY = FLOW_MISSION_NAME_POS_Y) 
					/*
					IF (GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(txtMissionTitle) > iMaxMissionTitleLength)
						IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
							SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, vOriginalSubtitleTextPos.x, vOriginalSubtitleTextPos.y + fSubtitleTextPosOffsetY)	
						ENDIF
					ENDIF
					*/
				ENDIF
				
				// in game version
				SET_SCRIPT_GFX_ALIGN_PARAMS(fDisplayX, fDisplayY, FLOW_MISSION_NAME_SIZE_X, FLOW_MISSION_NAME_SIZE_Y)
				SET_TEXT_SCALE(FLOW_MISSION_NAME_SCALE_X, FLOW_MISSION_NAME_SCALE_Y)

				// if 4:3 use smaller value
				IF (NOT GET_IS_WIDESCREEN() AND NOT GET_IS_HIDEF())
					fMaxMissionTitleWidth = FLOW_MAX_MISSION_TITLE_WIDTH_4_3
					//CPRINTLN(DEBUG_ACHIEVEMENT, "[4:3] MISSION TITLE:", txtMissionTitle, " WIDTH:", GET_STRING_WIDTH(txtMissionTitle), " MAX:", fMaxMissionTitleWidth)
				ELSE
					fMaxMissionTitleWidth = FLOW_MAX_MISSION_TITLE_WIDTH
					//CPRINTLN(DEBUG_ACHIEVEMENT, "MISSION TITLE:", txtMissionTitle, " WIDTH:", GET_STRING_WIDTH(txtMissionTitle), " MAX:", fMaxMissionTitleWidth)
				ENDIF
				
				IF (fDisplayY = FLOW_MISSION_NAME_POS_Y) 
					IF (GET_STRING_WIDTH(txtMissionTitle) > fMaxMissionTitleWidth)
						IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
							SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, vOriginalSubtitleTextPos.x, vOriginalSubtitleTextPos.y + fSubtitleTextPosOffsetY)	
							g_bMissionTitleMovedSubtitles = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT(txtMissionTitle)
				END_TEXT_COMMAND_DISPLAY_TEXT(fDisplayX, fDisplayY)
				
				RESET_SCRIPT_GFX_ALIGN()
				
				// if mission title is blocked clear the state
				IF (g_bMissionTitleBlocked = TRUE)
					CPRINTLN(DEBUG_FLOW, " Mission Title Display has been blocked")
					MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
					fSafeToDisplayTimer = 0.0
				ENDIF
			ELSE
				//Deactive completed display request.
				CPRINTLN(DEBUG_FLOW, "Finished displaying mission title for ", GET_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionTitleType, g_iFlowDisplayMissionTitle), ".")
				MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
				fSafeToDisplayTimer = 0.0
			ENDIF
		ENDIF
	ELSE
		// done, wait for new request
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the mission title system that a story mission is launching
/// PARAMS:
///    paramMissionID - the RC mission that is launching
PROC MISSION_FLOW_DISPLAY_MISSION_TITLE(SP_MISSIONS paramMissionID)
	CPRINTLN(DEBUG_FLOW, GET_THIS_SCRIPT_NAME(), " requested to display mission title for mission ", GET_MISSION_DISPLAY_STRING_FROM_ID(CP_GROUP_MISSIONS, ENUM_TO_INT(paramMissionID)) ,".")
		
	IF ENUM_TO_INT(paramMissionID) < ENUM_TO_INT(SP_MISSION_MAX)
		MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
		g_iFlowDisplayMissionTitle = ENUM_TO_INT(paramMissionID)
		g_eMissionTitleState = MTS_CHECK_INTRO
		g_eMissionTitleType = CP_GROUP_MISSIONS
	ELSE
		SCRIPT_ASSERT("MISSION_FLOW_DISPLAY_MISSION_TITLE: Tried to start displaying a mission intro name for an invalid mission enum.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the mission title system that an RC mission is launching
/// PARAMS:
///    eMissionID - the RC mission that is launching
PROC RANDOM_CHAR_DISPLAY_MISSION_TITLE(g_eRC_MissionIDs eMissionID)
	CPRINTLN(DEBUG_FLOW, GET_THIS_SCRIPT_NAME(), " requested to display mission title for mission ", GET_MISSION_DISPLAY_STRING_FROM_ID(CP_GROUP_RANDOMCHARS, ENUM_TO_INT(eMissionID)) ,".")
		
	IF ENUM_TO_INT(eMissionID) < ENUM_TO_INT(MAX_RC_MISSIONS)
		MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
		g_iFlowDisplayMissionTitle = ENUM_TO_INT(eMissionID)
		g_eMissionTitleState = MTS_CHECK_INTRO
		g_eMissionTitleType = CP_GROUP_RANDOMCHARS
	ELSE
		SCRIPT_ASSERT("RANDOM_CHAR_DISPLAY_MISSION_TITLE: Tried to start displaying a mission intro name for an invalid mission enum.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the mission title system that a minigame is launching
/// PARAMS:
///   eMissionID- the minigame that is launching
PROC MINIGAME_DISPLAY_MISSION_TITLE(INT iMinigame)
	CPRINTLN(DEBUG_FLOW, GET_THIS_SCRIPT_NAME(), " requested to display mission title for mission ", GET_MISSION_DISPLAY_STRING_FROM_ID(CP_GROUP_MINIGAMES, iMinigame) ,".")
		
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	g_iFlowDisplayMissionTitle = iMinigame
	g_eMissionTitleState = MTS_CHECK_INTRO
	g_eMissionTitleType = CP_GROUP_MINIGAMES
ENDPROC

/// PURPOSE:
///    determine when the mission title has gone away
/// RETURNS:
///    g_eMissionTitleState = MTS_DONE
FUNC BOOL IS_MISSION_TITLE_FINISHED()
	RETURN g_eMissionTitleState = MTS_DONE
ENDFUNC

/// PURPOSE:
///    Tells the mission title system to go into block state
/// PARAMS:
///    bBlock - block or unblock
PROC BLOCK_MISSION_TITLE(BOOL bBlock = TRUE)
	CPRINTLN(DEBUG_FLOW, GET_THIS_SCRIPT_NAME(), " has set the mission title blocking to ", bBlock)
	g_bMissionTitleBlocked = bBlock
ENDPROC

FUNC BOOL IS_MISSION_TITLE_BLOCKED()
	RETURN g_bMissionTitleBlocked
ENDFUNC
