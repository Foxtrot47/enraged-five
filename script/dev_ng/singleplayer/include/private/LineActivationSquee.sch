


///////////////////////////////////////////////////////////////////////////////
///////////////////////////New Test For Les//////////////////////////////
///////////////////////////////////////////////////////////////////////////


// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_ped.sch"
USING "commands_vehicle.sch"
USING "commands_object.sch"
USING "commands_hud.sch"
USING "commands_player.sch"
USING "commands_task.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "commands_path.sch"
USING "types.sch"
USING "commands_camera.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_interiors.sch"
USING "script_player.sch"
USING "Minigame_private.sch"
USING "LineActivation.sch"


STRUCT BENCHASSETS
	STRING sAnimFolder
	STRING sHandcuffAnimFolder
	MODEL_NAMES mnChairModel
	MODEL_NAMES mnGuardModel
	MODEL_NAMES mnDoorModel
ENDSTRUCT

STRUCT ANIMSTRINGS
	STRING sAnimNameToSitDown 
	STRING sAnimNameSittingLoop 
	STRING sAnimNameGetUp
	STRING sHandcuffGuard 
	STRING sHandcuffPlayer
ENDSTRUCT

ENUM EUpdate
	EUpdate_Start = 0,
	EUpdate_Listen,
	EUpdate_RunEvent,
	EUpdate_RunEvent2,
	EUpdate_RunEvent3,
	EUpdate_Cleanup
ENDENUM
	
ENUM ANIMSTATES
	ANIMSTATES_PLAY_INTRO = 0,
	ANIMSTATES_WAIT_INTRO,
	ANIMSTATES_PLAY_LOOP,
	ANIMSTATES_WAIT_LOOP,
	ANIMSTATES_PLAY_EXIT,
	ANIMSTATES_WAIT_EXIT
ENDENUM

ANIMSTATES RunSitting_Stages = ANIMSTATES_PLAY_INTRO

ANIMSTATES RunHandcuff_Stages = ANIMSTATES_PLAY_INTRO
	
SEQUENCE_INDEX iSeqTask

FUNC BOOL LeftStickPulled()

	INT LeftX
	INT LeftY
	INT RightX
	INT RightY

	GET_POSITION_OF_ANALOGUE_STICKS(PAD1, LeftX, LeftY, RightX, RightY)
	
	IF LeftX > 60 OR LeftX < -60
	OR LeftY > 60 OR LeftY < -60
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


//FUNC BOOL RunAnims(OBJECT_INDEX aChair, STRING AnimFolder, STRING AnimSitDown, STRING AnimLoop, STRING AnimGetup)
FUNC BOOL RunAnims(OBJECT_INDEX aChair)
	

	
	SWITCH  RunSitting_Stages
	
		CASE ANIMSTATES_PLAY_INTRO
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			RunSitting_Stages = ANIMSTATES_WAIT_INTRO
		BREAK
		
		CASE ANIMSTATES_WAIT_INTRO
			TASK_SIT_DOWN_ON_OBJECT(PLAYER_PED_ID(), aChair, -1, SF_LOOPANIM)
			RunSitting_Stages = ANIMSTATES_WAIT_EXIT
		BREAK
		
		CASE ANIMSTATES_WAIT_EXIT
			
			IF IS_BUTTON_JUST_PRESSED(PAD1, TRIANGLE) OR LeftStickPulled()
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				RunSitting_Stages = ANIMSTATES_PLAY_INTRO
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH


	RETURN FALSE
ENDFUNC
	
FUNC BOOL RunHandcuffAnimsLES(PED_INDEX aPed, STRING HandcuffFolder, STRING PlayerAnim, STRING GuardAnim)

//////	VECTOR animScenePosition = << 324.858, -977.465, 28.283 >> 
//	VECTOR animScenePosition = <<-1044.9077, -2914.9951, 12.8198>>
//////	VECTOR animSceneRotation = << 0.000, 0.000, 180.000 >>	
//	VECTOR animSceneRotation = << 0.000, 0.000, 0.000 >>
	
	VECTOR animScenePosition = <<154.5272, -976.1486, 29.0919>>
	VECTOR animSceneRotation = << 0.000, 0.000, 0.000 >>

	VECTOR SittingPos
	FLOAT SittingHeading
	IF NOT IS_ENTITY_DEAD(aPed)
		SittingPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aPed, <<0, 1, 0>>)
		SittingHeading = GET_ENTITY_HEADING(aPed)
		SittingHeading -= 180
	ENDIF
	

	IF NOT IS_PED_INJURED(aPed)
	
		animScenePosition = GET_ENTITY_COORDS(aPed)
		GET_GROUND_Z_FOR_3D_COORD(animScenePosition, animScenePosition.z)
		animSceneRotation = <<0,0,GET_ENTITY_HEADING(aPed)-180>>
	
		SWITCH  RunHandcuff_Stages
		
			CASE ANIMSTATES_PLAY_INTRO
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(aPed)
				/*SET_ENTITY_COORDS*/ SET_PED_COORDS_KEEP_VEHICLE(aPed, animScenePosition)
//				SET_ENTITY_HEADING(aPed, animSceneRotation-180)
				RunHandcuff_Stages = ANIMSTATES_WAIT_INTRO
			BREAK
			
			CASE ANIMSTATES_WAIT_INTRO

//				TASK_PLAY_ANIM_ADVANCED (aPed, HandcuffFolder, GuardAnim, animScenePosition, animSceneRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET)
//				TASK_PLAY_ANIM_ADVANCED (PLAYER_PED_ID(), HandcuffFolder, PlayerAnim, animScenePosition, animSceneRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET )
				OPEN_SEQUENCE_TASK(iSeqTask)
					TASK_PED_SLIDE_TO_COORD(NULL, SittingPos, SittingHeading, 0.5)
	//				TASK_PED_SLIDE_TO_COORD_AND_PLAY_ANIM(NULL, SittingPos, SittingHeading, 2.0, AnimSitDown, AnimFolder,8.0, FALSE, FALSE, FALSE, FALSE,-1 )
					TASK_ACHIEVE_HEADING(NULL,SittingHeading)
				CLOSE_SEQUENCE_TASK(iSeqTask)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), iSeqTask)
				CLEAR_SEQUENCE_TASK(iSeqTask)
			
			
			
			
				RunHandcuff_Stages = ANIMSTATES_PLAY_LOOP
			BREAK
			
			CASE ANIMSTATES_PLAY_LOOP
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
					CLEAR_PED_TASKS_IMMEDIATELY(aPed)
//					TASK_PLAY_ANIM(aPed, HandcuffFolder, GuardAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
//					TASK_PLAY_ANIM(PLAYER_PED_ID(), HandcuffFolder, PlayerAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
					TASK_PLAY_ANIM_ADVANCED (aPed, HandcuffFolder, GuardAnim, animScenePosition, animSceneRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET)
					TASK_PLAY_ANIM_ADVANCED (PLAYER_PED_ID(), HandcuffFolder, PlayerAnim, animScenePosition, animSceneRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET )

					RunHandcuff_Stages = ANIMSTATES_WAIT_EXIT
				ENDIF
			BREAK
			
			CASE ANIMSTATES_WAIT_LOOP
				
				RunHandcuff_Stages = ANIMSTATES_WAIT_EXIT
			BREAK
			
			CASE ANIMSTATES_WAIT_EXIT
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
					RunHandcuff_Stages = ANIMSTATES_PLAY_INTRO
					RETURN TRUE
				ENDIF
			BREAK
		
		ENDSWITCH
	ENDIF


	RETURN FALSE

ENDFUNC	
	

	
FUNC BOOL UPDATE_EVENT_LES(EUpdate &curState,OBJECT_INDEX ChairObj,PED_INDEX aPed, BENCHASSETS Asset, ANIMSTRINGS AnimNames)


	SWITCH curState
	
		CASE EUpdate_Start
		
			curState = EUpdate_Listen
		BREAK
		
		CASE EUpdate_Listen
			IF IS_PLAYER_ACTIVATING_OBJECT(ChairObj)		
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE,  SPC_LEAVE_CAMERA_CONTROL_ON)
				curState = EUpdate_RunEvent		
			ENDIF
			
			IF IS_PLAYER_ACTIVATING_PED(aPed)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE,  SPC_LEAVE_CAMERA_CONTROL_ON)
				curState = EUpdate_RunEvent2	
			ENDIF
			
		
		BREAK
		
		CASE EUpdate_RunEvent
//			IF RunAnims(ChairObj, Asset.sAnimFolder , AnimNames.sAnimNameToSitDown, AnimNames.sAnimNameSittingLoop, AnimNames.sAnimNameGetUp)
			IF RunAnims(ChairObj)

				curState = EUpdate_Cleanup	
			ENDIF

		BREAK
		
		CASE EUpdate_RunEvent2
			IF RunHandcuffAnimsLES(aPed, Asset.sHandcuffAnimFolder, AnimNames.sHandcuffPlayer, AnimNames.sHandcuffGuard)
				curState = EUpdate_Cleanup	
			ENDIF
		
		BREAK
		
		CASE EUpdate_RunEvent3
			curState = EUpdate_Cleanup	
		BREAK
		
		CASE EUpdate_Cleanup
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			curState = EUpdate_Listen
		BREAK
	
	
	ENDSWITCH
	


	RETURN FALSE
ENDFUNC	
	
	
FUNC BOOL LoadUnloadAllAssets(BOOL Load, BENCHASSETS Asset)

	IF load = TRUE
		REQUEST_MODEL(Asset.mnChairModel)
		REQUEST_MODEL(Asset.mnGuardModel)
	
		REQUEST_ANIM_DICT(Asset.sAnimFolder)
		REQUEST_ANIM_DICT(Asset.sHandcuffAnimFolder)
		
		
		IF HAS_MODEL_LOADED(Asset.mnChairModel)	
			IF HAS_MODEL_LOADED(Asset.mnGuardModel)
				IF HAS_ANIM_DICT_LOADED(Asset.sAnimFolder)
					IF HAS_ANIM_DICT_LOADED(Asset.sHandcuffAnimFolder)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ELSE
		
		REMOVE_ANIM_DICT(Asset.sAnimFolder)
		REMOVE_ANIM_DICT(Asset.sHandcuffAnimFolder)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnChairModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnGuardModel)
		
	ENDIF


	RETURN FALSE
	
ENDFUNC
 
 







