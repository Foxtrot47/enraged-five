USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	drunkNoticeboard.sc
//		AUTHOR			:	Keith / Alwyn
//		DESCRIPTION		:	Allows the various scripts in the drunk routines to pass info.
//
//		NOTES			:	The idea is that each frame a drunk script clears out any
//							notices it posted in the previous frame, checks for any notices
//							posted by other scripts that are relevant to this script, and
//							posts any new messages it needs to for other drunk scripts.
//
//							An example of a message would be a drunk ped getting into a car
//							and posting a message to any follower peds that that should
//							also get into the car.
//
//		USAGE			:	Each poster should clear up any notices posted by it in previous
//							frames.
//
//							Each reader should clear up any notices aimed specifically for
//							them.
//
//							Control script should clear up any messages from posters that
//							no longer exist.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// PURPOSE:	Find an empty slot in the Drunk Notices array.

// RETURN VALUE:	INT			Array Index of the free Drunk Notices slot, or UNKNOWN_DRUNK_ARRAY_INDEX
FUNC INT Get_An_Empty_Drunk_Notice_Slot()
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_NOTICES tempLoop
		IF (g_drunkNotices[tempLoop].notice = DNID_NO_DRUNK_NOTICE)
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX

ENDFUNC


// PURPOSE:	Find the array position of a specific notice ID from a specific poster to a specifi reader
//
// INPUT PARAMS:	paramPoster		Unique Drunk Ped ID of required poster
//					paramReaderID	Unique Drunk Ped ID for readers of the notice
//					paramNoticeID	Notice ID being searched for
// RETURN VALUE:	INT				Array Position for this drunk notice, or NO_DRUNK_NOTICES
FUNC INT Find_Position_Of_This_Drunk_Notice_From_Poster_To_Reader(INT paramPoster, INT paramReader, g_eDrunkNoticeIDs paramNoticeID)
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_NOTICES tempLoop
		IF (paramNoticeID = g_drunkNotices[tempLoop].notice)
			IF (paramPoster = g_drunkNotices[tempLoop].poster)
				IF (paramReader = g_drunkNotices[tempLoop].reader)
					RETURN tempLoop
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN NO_DRUNK_NOTICES

ENDFUNC


// PURPOSE:	Find the array position of a specific notice ID from a specific poster
//
// INPUT PARAMS:	paramPoster		Unique Drunk Ped ID of required poster
//					paramNoticeID	Notice ID being searched for
// RETURN VALUE:	INT				Array Position for this drunk notice, or NO_DRUNK_NOTICES
FUNC INT Find_Position_Of_This_Drunk_Notice_From_Poster(INT paramPoster, g_eDrunkNoticeIDs paramNoticeID)
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_NOTICES tempLoop
		IF (paramNoticeID = g_drunkNotices[tempLoop].notice)
			IF (paramPoster = g_drunkNotices[tempLoop].poster)
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN NO_DRUNK_NOTICES

ENDFUNC


// PURPOSE:	Check if the exact details of this Drunk Notice already exist
//
// INPUT PARAMS:	paramPosterID		Unique Drunk Ped ID for notice poster
//					paramReaderID		Unique Drunk Ped ID for readers of the notice
//					paramNoticeID		Contents of the notice
// RETURN VALUE:	BOOL				TRUE if this exact notice exists, otherwise FALSE
FUNC BOOL Does_This_Drunk_Notice_Already_Exist(INT paramPosterID, INT paramReaderID, g_eDrunkNoticeIDs paramNoticeID)

	IF (Find_Position_Of_This_Drunk_Notice_From_Poster_To_Reader(paramPosterID, paramReaderID, paramNoticeID) = NO_DRUNK_NOTICES)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// PURPOSE:	Post a new notice with all parameters on the Drunk Noticeboard
//
// INPUT PARAMS:	paramPosterID		Unique Drunk Ped ID for notice poster
//					paramReaderID		Unique Drunk Ped ID for readers of the notice
//					paramNoticeID		Contents of the notice
//					paramPed			PED_INDEX for this notice
//					paramInt			INT for this notice
PROC Post_Drunk_Notice_With_Full_Parameters(INT paramPosterID, INT paramReaderID, g_eDrunkNoticeIDs paramNoticeID, PED_INDEX paramPed, INT paramInt, INT iHitCount = 1)
	
	IF (paramPosterID = NO_UNIQUE_DRUNK_PED_ID)
		SCRIPT_ASSERT("Post_Notice: Unique Drunk Ped ID for poster is unknown")
		EXIT
	ENDIF
	
	IF (paramReaderID = NO_UNIQUE_DRUNK_PED_ID)
		SCRIPT_ASSERT("Post_Notice: Unique Drunk Ped ID for reader is unknown")
		EXIT
	ENDIF
	
	IF (paramNoticeID = DNID_NO_DRUNK_NOTICE)
		SCRIPT_ASSERT("Post_Notice: Notice ID is unknown")
		EXIT
	ENDIF
	
	// Don't add duplicate messages
	IF (Does_This_Drunk_Notice_Already_Exist(paramPosterID, paramReaderID, paramNoticeID))
		EXIT
	ENDIF
	
	INT freeNoticeSlot = Get_An_Empty_Drunk_Notice_Slot()
	IF (freeNoticeSlot = UNKNOWN_DRUNK_ARRAY_INDEX)
		SCRIPT_ASSERT("All drunk notice slots are full - Tell Alwyn to increase MAX_NUMBER_OF_DRUNK_NOTICES")
		EXIT
	ENDIF
	
	g_drunkNotices[freeNoticeSlot].poster	= paramPosterID
	g_drunkNotices[freeNoticeSlot].reader	= paramReaderID
	g_drunkNotices[freeNoticeSlot].notice	= paramNoticeID
	g_drunkNotices[freeNoticeSlot].pedIndex	= paramPed
	g_drunkNotices[freeNoticeSlot].intval	= paramInt
	g_drunkNotices[freeNoticeSlot].inthit	= iHitCount

ENDPROC


// PURPOSE:	Post a new notice with an Int parameter on the Drunk Noticeboard
//
// INPUT PARAMS:	paramPosterID		Unique Drunk Ped ID for notice poster
//					paramReaderID		Unique Drunk Ped ID for readers of the notice
//					paramNoticeID		Contents of the notice
//					paramInt			INT value for this notice
PROC Post_Drunk_Notice_With_Int(INT paramPosterID, INT paramReaderID, g_eDrunkNoticeIDs paramNoticeID, INT paramInt)

	Post_Drunk_Notice_With_Full_Parameters(paramPosterID, paramReaderID, paramNoticeID, NULL, paramInt)

ENDPROC


// PURPOSE:	Post a new notice with a ped index parameter on the Drunk Noticeboard
//
// INPUT PARAMS:	paramPosterID		Unique Drunk Ped ID for notice poster
//					paramReaderID		Unique Drunk Ped ID for readers of the notice
//					paramNoticeID		Contents of the notice
//					paramPedIndex		PED_INDEX for this notice
PROC Post_Drunk_Notice_With_Ped_Index(INT paramPosterID, INT paramReaderID, g_eDrunkNoticeIDs paramNoticeID, PED_INDEX paramPed, INT iHitCount = 1)

	Post_Drunk_Notice_With_Full_Parameters(paramPosterID, paramReaderID, paramNoticeID, paramPed, 0, iHitCount)

ENDPROC


// PURPOSE:	Post a new notice with no parameters on the Drunk Noticeboard
//
// INPUT PARAMS:	paramPosterID		Unique Drunk Ped ID for notice poster
//					paramReaderID		Unique Drunk Ped ID for readers of the notice
//					paramNoticeID		Contents of the notice
PROC Post_Drunk_Notice(INT paramPosterID, INT paramReaderID, g_eDrunkNoticeIDs paramNoticeID)

	Post_Drunk_Notice_With_Full_Parameters(paramPosterID, paramReaderID, paramNoticeID, NULL, 0)

ENDPROC


// PURPOSE:	Clear the details of the specified Drunk Notices array position
//
// INPUT PARAMS:	paramArrayIndex		Index into the drunk notices array
PROC Clear_One_Drunk_Notice(INT paramArrayIndex)
	
	IF (paramArrayindex < 0)
	OR (paramArrayIndex >= MAX_NUMBER_OF_DRUNK_NOTICES)
		SCRIPT_ASSERT("Clear_One_Drunk_Notice: array index out of bounds")
		EXIT
	ENDIF

	g_drunkNotices[paramArrayIndex].poster		= NO_UNIQUE_DRUNK_PED_ID
	g_drunkNotices[paramArrayIndex].reader		= NO_UNIQUE_DRUNK_PED_ID
	g_drunkNotices[paramArrayIndex].notice		= DNID_NO_DRUNK_NOTICE
	g_drunkNotices[paramArrayIndex].pedIndex	= NULL
	g_drunkNotices[paramArrayIndex].intval		= 0
	
ENDPROC


// PURPOSE:	Clear all Drunk Notices where the Unique Drunk Ped ID is the poster of the notice
//
// INPUT PARAMS:	paramUniqueID		Unique Drunk Ped ID of poster being searched for
PROC Clear_All_Notices_From_This_Unique_Drunk_Ped_ID(INT paramPoster)
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_NOTICES tempLoop
		IF (paramPoster = g_drunkNotices[tempLoop].poster)
			Clear_One_Drunk_Notice(tempLoop)
		ENDIF
	ENDREPEAT

ENDPROC


// PURPOSE:	Clear all Drunk Notices where the Unique Drunk Ped ID is the reader of the notice
//
// INPUT PARAMS:	paramUniqueID		Unique Drunk Ped ID of reader being searched for
PROC Clear_All_Notices_To_This_Unique_Drunk_Ped_ID(INT paramReader)
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_NOTICES tempLoop
		IF (paramReader = g_drunkNotices[tempLoop].reader)
			Clear_One_Drunk_Notice(tempLoop)
		ENDIF
	ENDREPEAT

ENDPROC


// PURPOSE:	Clear all Drunk Notices where the Unique Drunk Ped ID is the poster or the reader of the notices
//
// INPUT PARAMS:	paramUniqueID		Unique Drunk Ped ID being searched for
PROC Clear_All_Notices_With_This_Unique_Drunk_Ped_ID(INT paramUniqueID)

	Clear_All_Notices_From_This_Unique_Drunk_Ped_ID(paramUniqueID)
	Clear_All_Notices_To_This_Unique_Drunk_Ped_ID(paramUniqueID)

ENDPROC


// PURPOSE:	Clear every slot in the Drunk Notices array
PROC Clear_All_Drunk_Notices()

	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_NOTICES tempLoop
		Clear_One_Drunk_Notice(tempLoop)
	ENDREPEAT

ENDPROC




