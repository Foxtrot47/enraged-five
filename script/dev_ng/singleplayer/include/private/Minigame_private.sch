

USING "commands_camera.sch"
USING "commands_ped.sch"
USING "Script_player.sch"
USING "Cellphone_public.sch"
USING "Commands_path.sch"


CONST_INT ACTIVATION_DISTANCE_SMALL (2)

ENUM AMBIENT_EVENT_STATE
	EVENT_INIT = 0,
	EVENT_CONTINUE,
	EVENT_WAIT_TO_START,
	EVENT_LOADING,
	EVENT_STARTING,
	EVENT_RUNNING,
	EVENT_ENDING,
	EVENT_OVER
ENDENUM


ENUM RUNINTRO
	RUNINTRO_SLIDEINTOPOSITION = 0,
	RUNINTRO_WAITFORSLIDE,
	RUNINTRO_PLAYINTROANIM,
	RUNINTRO_FINISHED
ENDENUM


CONST_INT Large_Reward (500)
CONST_INT Medium_Reward (100)
CONST_INT Small_Reward (30)

//STRUCT sMINIGAME
//	OBJECT_INDEX anObject
//ENDSTRUCT

FUNC VECTOR MAKE_VECTOR_ZERO()
	RETURN <<0,0,0>>
ENDFUNC

PROC Set_Ped_Position(PED_INDEX aPed, VECTOR aPosition, FLOAT aHeading = -1.0)

	FLOAT ActualGroundPosition
	GET_GROUND_Z_FOR_3D_COORD(aPosition, ActualGroundPosition)
	IF NOT IS_ENTITY_DEAD(aPed)	
		/*SET_ENTITY_COORDS*/ SET_PED_COORDS_KEEP_VEHICLE(aPed, <<aPosition.x, aPosition.y, ActualGroundPosition>>)
		IF aHeading > -1
			SET_ENTITY_HEADING(aPed, aHeading)
		ENDIF
	ENDIF

ENDPROC

PROC Set_Veh_Position(VEHICLE_INDEX aVeh, VECTOR aPosition, FLOAT aHeading = -1.0)

	FLOAT ActualGroundPosition
	GET_GROUND_Z_FOR_3D_COORD(aPosition, ActualGroundPosition)
//	ActualGroundPosition = aPosition.z
	IF NOT IS_ENTITY_DEAD(aVeh)	
		SET_ENTITY_COORDS(aVeh, <<aPosition.x, aPosition.y, ActualGroundPosition>>)
		IF aHeading > -1
			SET_ENTITY_HEADING(aVeh, aHeading)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL HAVE_ALL_MODELS_LOADED(MODEL_NAMES& Models[])
	
	INT I
	FOR I = 0 TO COUNT_OF(Models)-1
		IF NOT HAS_MODEL_LOADED(Models[I])
			RETURN FALSE
		ENDIF
	ENDFOR
RETURN TRUE
ENDFUNC

//
//FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_INITIAL_ENTITY(ENTITY_INDEX anEntity)
//
//	BLIP_INDEX theBlip
//	IF NOT IS_ENTITY_DEAD(anEntity)
//		theBlip = ADD_BLIP_FOR_ENTITY(anEntity)
//		SET_BLIP_SPRITE(theBlip, RADAR_TRACE_RANDOM_CHARACTER)
//	ENDIF
//	RETURN TheBlip
//
//ENDFUNC
//
//FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_INITIAL_COORD(VECTOR vecCoord)
//
//	BLIP_INDEX theBlip
//
//	theBlip = ADD_BLIP_FOR_COORD(vecCoord)
//	SET_BLIP_SPRITE(theBlip, RADAR_TRACE_RANDOM_CHARACTER)
//	
//	RETURN TheBlip
//
//ENDFUNC
//
//FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_VEHICLE(VEHICLE_INDEX vehicle, BOOL IsEnemyVehicle = FALSE)
//
//	BLIP_INDEX theBlip
//	IF NOT IS_ENTITY_DEAD(vehicle)
//		
//		TheBlip = ADD_BLIP_FOR_ENTITY(vehicle)
//		SET_BLIP_SCALE(TheBlip, BLIP_SIZE_VEHICLE)
//		
//		IF IsEnemyVehicle = FALSE
//			SET_BLIP_COLOUR(TheBlip, BLIP_COLOUR_BLUE)
//		ELSE
//			SET_BLIP_COLOUR(TheBlip, BLIP_COLOUR_RED)
//		ENDIF
//	ENDIF
//	RETURN TheBlip
//
//ENDFUNC
//
//FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_PED(PED_INDEX ped, BOOL IsEnemyPed = FALSE)
//
//	BLIP_INDEX theBlip
//	IF NOT IS_ENTITY_DEAD(ped)
//		
//		TheBlip = ADD_BLIP_FOR_ENTITY(ped)
//		SET_BLIP_SCALE(TheBlip, BLIP_SIZE_PED)
//		
//		IF IsEnemyPed = FALSE
//			SET_BLIP_COLOUR(TheBlip, BLIP_COLOUR_BLUE)
//		ELSE
//			SET_BLIP_COLOUR(TheBlip, BLIP_COLOUR_RED)
//		ENDIF
//	ENDIF
//	RETURN TheBlip
//
//ENDFUNC
//
//FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_COORD(VECTOR vecCoord, BOOL setBlipRoute = FALSE)
//
//	BLIP_INDEX theBlip
//
//	TheBlip = ADD_BLIP_FOR_COORD(vecCoord)
//	SET_BLIP_SCALE(TheBlip, BLIP_SIZE_COORD)
//	SET_BLIP_COLOUR(TheBlip, BLIP_COLOUR_YELLOW)
//	
//	SET_BLIP_ROUTE(TheBlip, setBlipRoute)
//	
//	RETURN TheBlip
//
//ENDFUNC
//
//FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_OBJECT(OBJECT_INDEX anObject)
//
//	BLIP_INDEX theBlip
//	IF DOES_ENTITY_EXIST(anObject)
//		TheBlip = ADD_BLIP_FOR_ENTITY(anObject)
//		SET_BLIP_SCALE(TheBlip, BLIP_SIZE_OBJECT)
//		SET_BLIP_COLOUR(TheBlip, BLIP_COLOUR_GREEN)
//	ENDIF
//	RETURN TheBlip
//
//ENDFUNC

FUNC BOOL IS_PED_HOLDING_ONE_HANDED_WEAPON(PED_INDEX aPed)

	WEAPON_TYPE CurrentWeapon
	IF NOT IS_PED_INJURED(aPed)
		IF GET_CURRENT_PED_WEAPON(aPed, CurrentWeapon)
			IF CurrentWeapon = WEAPONTYPE_PISTOL
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_SMG
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_STUNGUN
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_STICKYBOMB
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_SMOKEGRENADE
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_GRENADE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_HOLDING_TWO_HANDED_WEAPON(PED_INDEX aPed)

	WEAPON_TYPE CurrentWeapon
	IF NOT IS_PED_INJURED(aPed)
		IF GET_CURRENT_PED_WEAPON(aPed, CurrentWeapon)
			IF CurrentWeapon = WEAPONTYPE_ASSAULTRIFLE
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_RPG
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_PUMPSHOTGUN
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_HEAVYSNIPER
				RETURN TRUE
			ENDIF
			IF CurrentWeapon = WEAPONTYPE_REMOTESNIPER
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RemoveBlip(BLIP_INDEX aBlip)
	IF DOES_BLIP_EXIST(aBlip)
		REMOVE_BLIP(aBlip)
	ENDIF
ENDPROC

PROC RemoveBlips(BLIP_INDEX& Blips[])
	INT I 
	FOR I = 0 TO COUNT_OF(Blips)-1
		IF DOES_BLIP_EXIST(Blips[I])
			REMOVE_BLIP(Blips[I])
		ENDIF
	ENDFOR
ENDPROC

FUNC BLIP_INDEX AddCoordBlip(VECTOR Location, INT Colour, FLOAT Scale = 1.0)

	BLIP_INDEX TheBlip = ADD_BLIP_FOR_COORD(Location)
	SET_BLIP_COLOUR(TheBlip, Colour)	
	SET_BLIP_SCALE(TheBlip, Scale)
	RETURN TheBlip
ENDFUNC


PROC SHOOT_PED(PED_INDEX aPed, PED_BONETAG aBone, FLOAT ShooterDirection, INT DamageCaused)

	IF NOT IS_ENTITY_DEAD(aPed)
		
		IF WAS_PED_SKELETON_UPDATED(aPed)
			VECTOR vPedCoords = GET_PED_BONE_COORDS(aPed, aBone,MAKE_VECTOR_ZERO())
			FLOAT fPedHeading = GET_ENTITY_HEADING(aPed)
			
			FLOAT fDistAwayXY = 30.0  // change to whatever distances you
			FLOAT fDistAwayAB = 30.0  // want XY and AB to be from the player

			FLOAT fAngle1 = fPedHeading + ShooterDirection
			FLOAT fAngle2 = fPedHeading + ShooterDirection + 180.0

			FLOAT x = - SIN(fAngle1)
			FLOAT y = COS(fAngle1)
			x *= fDistAwayXY
			y *= fDistAwayXY
			x += vPedCoords.x
			y += vPedCoords.y

			FLOAT a = - SIN(fAngle2)
			FLOAT b = COS(fAngle2)
			a *= fDistAwayAB
			b *= fDistAwayAB
			a += vPedCoords.x
			b += vPedCoords.y

			PRINTSTRING("a = ")
			PRINTFLOAT(a)
			PRINTNL()
			PRINTSTRING("b = ")
			PRINTFLOAT(b)
			PRINTNL()
			
			PRINTNL()
			PRINTSTRING("x = ")
			PRINTFLOAT(x)
			PRINTNL()
			PRINTSTRING("y = ")
			PRINTFLOAT(y)
			PRINTNL()
			
	//		WAIT(0)
			VECTOR FirstVec = <<x,y,vPedCoords.z>>
			VECTOR SecondVec = <<0, 0, 0>>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(FirstVec,SecondVec, DamageCaused, TRUE)
		ENDIF
	ENDIF

ENDPROC

PROC PutPlayerWeaponAway()

	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)

ENDPROC

FUNC BOOL DoAllEntitiesExist(ENTITY_INDEX& ArrayOfEntities[])

	INT I 
	FOR I = 0 TO COUNT_OF(ArrayOfEntities)-1
		IF NOT DOES_ENTITY_EXIST(ArrayOfEntities[I])
			RETURN FALSE
		ENDIF
	ENDFOR

	RETURN TRUE
ENDFUNC

//PURPOSE:checks to see if the vehicle is stuck or undrivable
FUNC BOOL AMBIENT_VEHICLE_FAIL_CHECKS(VEHICLE_INDEX VEHICLE)
	
	IF DOES_ENTITY_EXIST(VEHICLE)
		IF IS_VEHICLE_DRIVEABLE(VEHICLE)
			IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_JAMMED, JAMMED_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_SIDE, SIDE_TIME)
				RETURN TRUE
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF 
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_VEHICLE_BEEN_DAMAGED_BY_ANY_HAND_WEAPON(VEHICLE_INDEX aVehicle)

	IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aVehicle, WEAPONTYPE_PISTOL)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aVehicle, WEAPONTYPE_ASSAULTRIFLE)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aVehicle, WEAPONTYPE_RPG)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aVehicle, WEAPONTYPE_PUMPSHOTGUN)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aVehicle, WEAPONTYPE_SMG)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aVehicle, WEAPONTYPE_HEAVYSNIPER)
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC


FUNC BOOL ARE_PEDS_INJURED_OR_IN_VEHICLE(PED_INDEX& thePeds[],VEHICLE_INDEX TheVehicle, FLOAT Distance = -1.0)

	INT I 
	FOR I = 0 TO COUNT_OF(thePeds)-1
		
		IF NOT IS_PED_INJURED(thePeds[I])
			IF NOT IS_PED_IN_VEHICLE(thePeds[I], TheVehicle)
				IF Distance > 0
					IF IS_ENTITY_AT_ENTITY(thePeds[I], PLAYER_PED_ID(), <<Distance,Distance, Distance>>)
						RETURN FALSE
					ENDIF
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF 
		ENDIF

	ENDFOR
				
RETURN TRUE

ENDFUNC


CONST_INT	RANGE_SMALL		50
CONST_INT	RANGE_MEDIUM	100
CONST_INT	RANGE_LARGE		150

FUNC BOOL HAS_PLAYER_LEFT_RANDOM_EVENT(VECTOR Location, INT Range)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), Location) > Range
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC


FUNC BOOL IS_PED_INJURED_OR_NOT_IN_VEHICLE(PED_INDEX thePed,VEHICLE_INDEX TheVehicle, FLOAT Distance = -1.0 )

		IF DOES_ENTITY_EXIST(thePed)
			IF DOES_ENTITY_EXIST(TheVehicle)
				IF IS_VEHICLE_DRIVEABLE(TheVehicle)
					IF NOT IS_PED_INJURED(thePed)
						IF IS_PED_IN_VEHICLE(thePed, TheVehicle)
							RETURN FALSE
						ENDIF
						IF Distance > 0
							IF IS_ENTITY_AT_ENTITY(thePed, PLAYER_PED_ID(), <<Distance,Distance, Distance>>)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					RETURN FALSE //Don't care about the car not existing, that isn't for this check.
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
				
RETURN TRUE

ENDFUNC

FUNC BOOL ARE_PEDS_INJURED_OR_NOT_IN_VEHICLE(PED_INDEX& thePeds[],VEHICLE_INDEX TheVehicle, FLOAT Distance = -1.0 )

	INT I 
	FOR I = 0 TO COUNT_OF(thePeds)-1
		IF DOES_ENTITY_EXIST(thePeds[I])
			IF DOES_ENTITY_EXIST(TheVehicle)
				IF IS_VEHICLE_DRIVEABLE(TheVehicle)
					IF NOT IS_PED_INJURED(thePeds[I])
						IF IS_PED_IN_VEHICLE(thePeds[I], TheVehicle)
							RETURN FALSE
						ENDIF
						IF Distance > 0
							IF IS_ENTITY_AT_ENTITY(thePeds[I], PLAYER_PED_ID(), <<Distance,Distance, Distance>>)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					RETURN FALSE //Don't care about the car not existing, that isn't for this check.
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDFOR
				
RETURN TRUE

ENDFUNC

FUNC BOOL IS_A_PED_IN_VEHICLE(PED_INDEX& thePeds[],VEHICLE_INDEX TheVehicle, INT& IndexOfPedInVehicle)

	INT I 
	FOR I = 0 TO COUNT_OF(thePeds)-1
		
		IF NOT IS_PED_INJURED(thePeds[I])
			IF IS_PED_IN_VEHICLE(thePeds[I], TheVehicle)
				IndexOfPedInVehicle = I
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
				
RETURN TRUE

ENDFUNC


// ===========================================================================================================
//		Minigames assert messages
// ===========================================================================================================

// PURPOSE:	Output a Minigame assert information on the console.
// 
PROC Display_Minigame_Assert_Message_Header()

	PRINTNL()
	PRINTSTRING("---MINIGAME SCRIPT ASSERT-----------------------------------------")
	PRINTNL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output a Mission Flow Assert Footer Message
// 
PROC Display_Minigame_Assert_Message_Footer()

	PRINTSTRING("---------------------------------------------------------------")
	PRINTNL()

ENDPROC



FUNC BOOL DO_TIMER(INT StartTime, INT Timer)

	INT DoTimerEnd, DoTimerDiff
	DoTimerEnd = GET_GAME_TIMER()
	DoTimerDiff = DoTimerEnd - StartTime
	IF DoTimerDiff > Timer
		RETURN TRUE
	ENDIF
RETURN FALSE
ENDFUNC

PROC CappingValue(FLOAT& Value, FLOAT Min, FLOAT Max)

	IF Value > Max
		Value = Max
	ELIF Value < Min
		Value = Min
	ENDIF

ENDPROC

PROC SwitchTo_Minigame(BOOL StartMinigame, BOOL bIgnorePlayerControl = TRUE)

	DISABLE_CELLPHONE(StartMinigame)
	
	IF StartMinigame = TRUE
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	ENDIF
	
	IF bIgnorePlayerControl = FALSE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT StartMinigame))
	ENDIF
	
ENDPROC

FUNC VECTOR Normalise(VECTOR VEntity, VECTOR VCamera)

	VECTOR PositionFromOrigin = VEntity-VCamera
	VECTOR NormalisedVector 

	FLOAT Length = SQRT((PositionFromOrigin.x*PositionFromOrigin.x)+(PositionFromOrigin.y*PositionFromOrigin.y)+(PositionFromOrigin.z*PositionFromOrigin.z))

	NormalisedVector.x = PositionFromOrigin.x/Length
	NormalisedVector.y = PositionFromOrigin.y/Length
	NormalisedVector.z = PositionFromOrigin.z/Length

	RETURN NormalisedVector

ENDFUNC

/// PURPOSE:
///    For the issue of working out euler exactly on the ATAN wrap (0.0,0.0,1.0) or (0.0,0.0,-1.0)
/// PARAMS:
///    aFloat - The Z coord of the forward vector.
/// RETURNS:
///    
FUNC FLOAT VERY_SMALL_FLOAT(FLOAT aFloat)
	//
	IF aFloat = 1.0
		RETURN 0.999999
	ENDIF
	IF aFloat = -1.0
		RETURN -0.999999
	ENDIF
RETURN aFloat

ENDFUNC

FUNC VECTOR ComputeEulersFromVector(VECTOR vForwardVector)
	
	VECTOR RotationEulers
	
//	RotationEulers.x = ATAN2(vForwardVector.x, vForwardVector.y)
//	RotationEulers.z = ATAN2(vForwardVector.z, SQRT((vForwardVector.x*vForwardVector.x)+(vForwardVector.y*vForwardVector.y)))

	vForwardVector.z = VERY_SMALL_FLOAT(vForwardVector.z)
	RotationEulers.z = ATAN2(-vForwardVector.x, vForwardVector.y)
	RotationEulers.x = ASIN(vForwardVector.z)
	RotationEulers.y = 0.0
	
	RETURN RotationEulers
ENDFUNC


ENUM DOORFLAGS
	DF_NODOOR = 0,
	DF_DRIVER = 1,
	DF_FRONTRIGHT = 2,
	DF_BACKRIGHT = 4,
	DF_BACKLEFT = 8
ENDENUM

FUNC BOOL GET_CLOSEST_VEHICLE_DOOR(PED_INDEX aPed, VEHICLE_INDEX aVehicle, INT IgnoreDoorFlags, DOORFLAGS& DoorPicked, VECTOR& WinningDoor)

	VECTOR PositionDoor 
	FLOAT Distance = 999	
	VECTOR player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())	
	
	IF NOT IS_PED_INJURED(aPed)
		IF NOT IS_ENTITY_DEAD(aVehicle)
			IF IgnoreDoorFlags = 1
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = DF_FRONTRIGHT
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked =  (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked =  (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 2
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 3
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 4
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_FRONTRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 5
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_FRONTRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 6
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 7
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, -1.5, 0>>) //Back Left
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKLEFT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 8
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_FRONTRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 9
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_FRONTRIGHT)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 10
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 11
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, -1.5, 0>>) //Back Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_BACKRIGHT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 12
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_FRONTRIGHT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 13
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<1.5, 1.5, 0>>) //Front Right
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_FRONTRIGHT)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 14
				PositionDoor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(aVehicle, <<-1.5, 1.5, 0>>) //Front Left (driver)
				IF VDIST(PositionDoor, player_pos) < Distance
					Distance = VDIST(PositionDoor, player_pos)
					DoorPicked = (DF_DRIVER)
					WinningDoor = PositionDoor
				ENDIF
			ELIF IgnoreDoorFlags = 15
				SCRIPT_ASSERT("EnterClosestVehicleDoor: You've set all doors to be ignored")
			ENDIF
		ENDIF
	ENDIF



RETURN TRUE

ENDFUNC


PROC SET_PED_ENTER_CLOSEST_VEHICLE_DOOR(PED_INDEX aPed, VEHICLE_INDEX aVehicle, INT IgnoreDoorFlags, DOORFLAGS& DoorPicked, FLOAT MoveSpeed,  BOOL bAddDelay = TRUE, ENTER_EXIT_VEHICLE_FLAGS ECF_Flags = ECF_RESUME_IF_INTERRUPTED, ENTITY_INDEX AimAtEntity = NULL)


	VECTOR WinnningDoor
	SEQUENCE_INDEX SeqNPC
	
	IF NOT IS_PED_INJURED(aPed)
		IF NOT IS_ENTITY_DEAD(aVehicle)
		
			GET_CLOSEST_VEHICLE_DOOR(aPed, aVehicle, IgnoreDoorFlags, DoorPicked, WinnningDoor)
	
			VEHICLE_SEAT VehSeat
			IF DoorPicked = DF_DRIVER	
				VehSeat = VS_DRIVER
			ELIF DoorPicked = DF_FRONTRIGHT	
				VehSeat = VS_FRONT_RIGHT
			ELIF DoorPicked = DF_BACKLEFT
				VehSeat = VS_BACK_LEFT
			ELIF DoorPicked = DF_BACKRIGHT	
				VehSeat = VS_BACK_RIGHT
			ENDIF	


			IF AimAtEntity = NULL
				OPEN_SEQUENCE_TASK(SeqNPC)
					IF bAddDelay = TRUE
						TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(500, 3000))
					ENDIF
					TASK_ENTER_VEHICLE(NULL, aVehicle, DEFAULT_TIME_NEVER_WARP, VehSeat, MoveSpeed, ECF_Flags)
				CLOSE_SEQUENCE_TASK(SeqNPC)
				TASK_PERFORM_SEQUENCE(aPed, SeqNPC)
				CLEAR_SEQUENCE_TASK(SeqNPC)
				SeqNPC = NULL
				SET_PED_KEEP_TASK(aPed, TRUE)
			ELSE
				OPEN_SEQUENCE_TASK(SeqNPC)
					
					IF NOT IS_ENTITY_DEAD(AimAtEntity)
						IF bAddDelay = TRUE
							TASK_AIM_GUN_AT_ENTITY(NULL, AimAtEntity, GET_RANDOM_INT_IN_RANGE(500, 3000))
						ENDIF
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, WinnningDoor, AimAtEntity, MoveSpeed, FALSE)
					ENDIF
					TASK_ENTER_VEHICLE(NULL, aVehicle, -1, VehSeat, MoveSpeed, ECF_Flags)
				CLOSE_SEQUENCE_TASK(SeqNPC)
				TASK_PERFORM_SEQUENCE(aPed, SeqNPC)
				CLEAR_SEQUENCE_TASK(SeqNPC)
				SeqNPC = NULL
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

PROC SET_PEDS_ENTER_CLOSEST_VEHICLE_DOOR(PED_INDEX& SomePeds[], VEHICLE_INDEX aVehicle, INT IgnoreDoorFlags, FLOAT MoveSpeed, BOOL bAddDelay = TRUE, ENTER_EXIT_VEHICLE_FLAGS ECF_Flags = ECF_RESUME_IF_INTERRUPTED, ENTITY_INDEX AimAtEntity = NULL)

	INT I
	INT DoorTaken = IgnoreDoorFlags
//	INT DoorUsed
	DOORFLAGS DF_Door_Used 

	FOR I = 0 TO COUNT_OF(SomePeds)-1
		
		IF NOT IS_PED_INJURED(SomePeds[I])
			DF_Door_Used = INT_TO_ENUM(DOORFLAGS,DoorTaken)
			SET_PED_ENTER_CLOSEST_VEHICLE_DOOR(SomePeds[I], aVehicle,DoorTaken, DF_Door_Used, MoveSpeed, bAddDelay, ECF_Flags, AimAtEntity)
			DoorTaken +=  ENUM_TO_INT(DF_Door_Used)
		ENDIF
	ENDFOR
	

ENDPROC


FUNC VECTOR GET_POSITION_IN_FRONT_OF_PLAYER(FLOAT HowFarInFront, BOOL OnlyOnPavement = FALSE)

VECTOR Result

	Result = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0, HowFarInFront, 0>>)
	GET_SAFE_COORD_FOR_PED(Result, OnlyOnPavement, Result)
	
RETURN Result 
ENDFUNC

		
		
	

// ===========================================================================================================
//		Camera Controls
// ===========================================================================================================

STRUCT INTERPCAMS
	VECTOR StartCamPos
	VECTOR StartCamRot
	VECTOR EndCamPos
	VECTOR EndCamRot
	FLOAT CamFov
	INT ShotTime
	CAMERA_INDEX StartCam
	CAMERA_INDEX EndCam
	CAMERA_GRAPH_TYPE GraphPosition
	CAMERA_GRAPH_TYPE GraphRotation
	BOOL AttachToEntity
	BOOL AttachPlayer
ENDSTRUCT

//STRUCT SYNCHCAM
//	VECTOR StartCamPos
//	VECTOR StartCamRot
//	FLOAT CamFov
//	INT ShotTime
//	CAMERA_INDEX StartCam
//	CAMERA_GRAPH_TYPE GraphPosition
//	CAMERA_GRAPH_TYPE GraphRotation
//ENDSTRUCT

//PURPOSE: Starts or stops a cutscene. If TRUE is passed TIMERA() is set to Zero
PROC SET_CUTSCENE_RUNNING(BOOL isRunning, BOOL doGameCamInterp = FALSE, INT durationFromInterp = 2000, BOOL turnOffGadgets = TRUE, BOOL bIgnorePlayerControl = FALSE)

//	SET_USE_HIGHDOF(isRunning)removed
	SET_WIDESCREEN_BORDERS(isRunning,0)
	DISPLAY_RADAR((NOT isRunning))
	IF bIgnorePlayerControl = FALSE
		IF turnOffGadgets = TRUE
			SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning),  SPC_DEACTIVATE_GADGETS)
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
		ENDIF
	ENDIF
	IF isRunning 
		PutPlayerWeaponAway()
	ENDIF
	RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	
ENDPROC


PROC Create_Animated_camera(INTERPCAMS& CameraSet, STRING AnimName, STRING AnimDict, VECTOR StartPos, VECTOR StartRot)

	CameraSet.StartCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
	SET_CAM_COORD(CameraSet.StartCam,CameraSet.StartCamPos)
	SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
	SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)

	PLAY_CAM_ANIM(CameraSet.StartCam, AnimName, AnimDict, StartPos, StartRot)
	SET_CAM_ACTIVE(CameraSet.StartCam, true)


ENDPROC

PROC Create_Syncronised_camera(INTERPCAMS& CameraSet, INT SceneID, STRING AnimName, STRING AnimDict)

	CameraSet.StartCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
	SET_CAM_COORD(CameraSet.StartCam,CameraSet.StartCamPos)
	SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
	SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
	PLAY_SYNCHRONIZED_CAM_ANIM( CameraSet.StartCam, sceneId, AnimName, AnimDict) 
	SET_CAM_ACTIVE(CameraSet.StartCam, true)


ENDPROC

//PROC Create_Small_Syncronised_camera(SYNCHCAM& CameraSet, INT SceneID, STRING AnimName, STRING AnimDict)
//
//	CameraSet.StartCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
//	SET_CAM_COORD(CameraSet.StartCam,CameraSet.StartCamPos)
//	SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
//	SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
//	PLAY_SYNCHRONIZED_CAM_ANIM( CameraSet.StartCam, sceneId, AnimName, AnimDict) 
//	SET_CAM_ACTIVE(CameraSet.StartCam, true)
//
//ENDPROC

PROC Run_camera_Interp(INTERPCAMS& CameraSet)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
	IF DOES_CAM_EXIST(CameraSet.EndCam)
		DESTROY_CAM(CameraSet.EndCam)
	ENDIF
	
		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(CameraSet.StartCam,CameraSet.StartCamPos)
		SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		
		CameraSet.EndCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(CameraSet.EndCam,CameraSet.EndCamPos)
		SET_CAM_ROT(CameraSet.EndCam, CameraSet.EndCamRot)
		SET_CAM_FOV(CameraSet.EndCam, CameraSet.CamFov)

		SET_CAM_ACTIVE_WITH_INTERP(CameraSet.EndCam, CameraSet.StartCam, CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation)
	
		//Attaching the player invisibly to the camera for the streaming issues
		IF CameraSet.AttachPlayer = TRUE
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			/*SET_ENTITY_COORDS*/ SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), CameraSet.StartCamPos)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF
	
ENDPROC


PROC Run_camera_static(INTERPCAMS& CameraSet, BOOL FollowPed = FALSE, PED_INDEX aPed = NULL, FLOAT Pan = 0.5)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
	
	CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	SET_CAM_COORD(CameraSet.StartCam,CameraSet.StartCamPos)
	
	IF FollowPed = FALSE
		SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
	ELSE
		IF NOT IS_ENTITY_DEAD(aPed)
			POINT_CAM_AT_ENTITY(CameraSet.StartCam, aPed, <<0,0,Pan>>)
		ENDIF
	ENDIF
	SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
	SET_CAM_ACTIVE(CameraSet.StartCam, TRUE)

ENDPROC

/// PURPOSE:
///    Runs a camera attached to a car
/// PARAMS:
///    CameraSet - the INTERPCAMS 
///    WhichCar - the car to attach camera to
///    offset - The offset the camera is placed at relative to the car
///    a_rotation - the rotation or the point at coords
///    isRotation - if you pass in a rotation, make this true. IF you pass in a point at coord, make this false
PROC Run_camera_Attach_car(INTERPCAMS& CameraSet, VEHICLE_INDEX WhichCar, VECTOR offset,VECTOR a_rotation, BOOL isRotation)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
				
	IF NOT IS_ENTITY_DEAD(WhichCar)
		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		SET_CAM_COORD(CameraSet.StartCam, GET_ENTITY_COORDS(WhichCar))
		
		ATTACH_CAM_TO_ENTITY(CameraSet.StartCam, WhichCar, offset)
		
		
		IF isRotation
			SET_CAM_ROT(CameraSet.StartCam, a_rotation)
		ELSE
			POINT_CAM_AT_COORD(CameraSet.StartCam, a_rotation)
		ENDIF
		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		
		SET_CAM_ACTIVE(CameraSet.StartCam,  TRUE)
//		SET_CAM_PROPAGATE(CameraSet.StartCam, TRUE)
	ENDIF

ENDPROC

/// PURPOSE:
///    Runs a camera created to an offset of a car
/// PARAMS:
///    CameraSet - the INTERPCAMS 
///    WhichCar -  the entity that the camera is placed in relation to
///    offset - The offset the camera is placed at relative to the car
///    a_rotation - the rotation or the point at coords
///    isRotation - if you pass in a rotation, make this true. IF you pass in a point at coord, make this false
PROC Run_camera_Offset_car(INTERPCAMS& CameraSet, VEHICLE_INDEX WhichCar, VECTOR offset,VECTOR a_rotation, BOOL isRotation)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(WhichCar)
		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")

		SET_CAM_COORD(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichCar, offset))	
				
		IF isRotation
			SET_CAM_ROT(CameraSet.StartCam, a_rotation)
		ELSE
			POINT_CAM_AT_COORD(CameraSet.StartCam, a_rotation)
		ENDIF
		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		
		SET_CAM_ACTIVE(CameraSet.StartCam,  TRUE)
//		SET_CAM_PROPAGATE(CameraSet.StartCam, TRUE)
	ENDIF

ENDPROC



//// PURPOSE:
 ///     Runs a camera attached to a car and interps
 /// PARAMS:
 ///    CameraSet - the INTERPCAMS
 ///    WhichEntity - the entity to attach camera to
 ///    isRotation - the rotation or the point at coords
PROC Run_camera_Attach_Entity_Static(INTERPCAMS& CameraSet, ENTITY_INDEX WhichEntity, BOOL isRotation = TRUE)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
	IF DOES_CAM_EXIST(CameraSet.EndCam)
		DESTROY_CAM(CameraSet.EndCam)
	ENDIF
				
	//IF NOT IS_ENTITY_DEAD(WhichEntity)
	IF DOES_ENTITY_EXIST(WhichEntity)
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> Run_camera_Attach_Entity_Static, DOES_ENTITY_EXIST")
		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))
		IF CameraSet.AttachToEntity = TRUE
			ATTACH_CAM_TO_ENTITY(CameraSet.StartCam, WhichEntity, CameraSet.StartCamPos)
		ENDIF
		IF isRotation
			SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
		ELSE
//			POINT_CAM_AT_ENTITY(CameraSet.StartCam, WhichEntity, CameraSet.StartCamRot, FALSE)
			VECTOR vForward = Normalise(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamRot), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))
			VECTOR vRotationEulers = ComputeEulersFromVector(vForward)
			SET_CAM_ROT(CameraSet.StartCam, vRotationEulers)
		ENDIF
		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		
		SET_CAM_ACTIVE(CameraSet.StartCam,  TRUE)	
		
	ENDIF

ENDPROC



//// PURPOSE:
 ///     Runs a camera attached to a car and interps
 /// PARAMS:
 ///    CameraSet - the INTERPCAMS
 ///    WhichEntity - the entity to attach camera to
 ///    isRotation - the rotation or the point at coords
PROC Run_camera_Attach_Entity_interp(INTERPCAMS& CameraSet, ENTITY_INDEX WhichEntity, BOOL isRotation = TRUE)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
	IF DOES_CAM_EXIST(CameraSet.EndCam)
		DESTROY_CAM(CameraSet.EndCam)
	ENDIF

	IF NOT IS_ENTITY_DEAD(WhichEntity)
		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))	
		IF CameraSet.AttachToEntity = TRUE
			ATTACH_CAM_TO_ENTITY(CameraSet.StartCam, WhichEntity, CameraSet.StartCamPos)
		ENDIF
		IF isRotation
			SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
		ELSE
//			POINT_CAM_AT_ENTITY(CameraSet.StartCam, WhichEntity, CameraSet.StartCamRot)
			VECTOR vForward = Normalise(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamRot), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))
			VECTOR vRotationEulers = ComputeEulersFromVector(vForward)
			SET_CAM_ROT(CameraSet.StartCam, vRotationEulers)
		ENDIF
		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		SET_CAM_ACTIVE(CameraSet.StartCam, TRUE)
		
		CameraSet.EndCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(CameraSet.EndCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamPos))
		
		IF CameraSet.AttachToEntity = TRUE
			ATTACH_CAM_TO_ENTITY(CameraSet.EndCam, WhichEntity, CameraSet.EndCamPos)	
		ENDIF
		
		IF isRotation
			SET_CAM_ROT(CameraSet.EndCam, CameraSet.EndCamRot)
		ELSE
//			POINT_CAM_AT_ENTITY(CameraSet.EndCam, WhichEntity, CameraSet.EndCamRot)
			VECTOR vForward = Normalise(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamRot), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamPos))
			VECTOR vRotationEulers = ComputeEulersFromVector(vForward)
			SET_CAM_ROT(CameraSet.EndCam, vRotationEulers)
		ENDIF
		
		SET_CAM_FOV(CameraSet.EndCam, CameraSet.CamFov)
		
		SET_CAM_ACTIVE_WITH_INTERP(CameraSet.EndCam, CameraSet.StartCam, CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation)
		
	ENDIF

ENDPROC



//// PURPOSE:
 ///     Runs a camera attached to a car and interps
 /// PARAMS:
 ///    CameraSet - the INTERPCAMS
 ///    WhichEntity - the entity to attach camera to
 ///    isRotation - the rotation or the point at coords
PROC Run_camera_Attach_Entity_interp_Params(INTERPCAMS& CameraSet, ENTITY_INDEX WhichEntity, BOOL isRotation = TRUE)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF

	IF NOT IS_ENTITY_DEAD(WhichEntity)
//		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		SET_CAM_COORD(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))	
		
		IF isRotation
			CameraSet.StartCam  = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos), CameraSet.StartCamRot, CameraSet.CamFov, TRUE)

		ELSE
//			POINT_CAM_AT_ENTITY(CameraSet.StartCam, WhichEntity, CameraSet.StartCamRot)
			VECTOR vForward = Normalise(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamRot), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))
			VECTOR vRotationEulers = ComputeEulersFromVector(vForward)
			CameraSet.StartCam  = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos), vRotationEulers, CameraSet.CamFov, TRUE)

		ENDIF

		IF CameraSet.AttachToEntity = TRUE
			ATTACH_CAM_TO_ENTITY(CameraSet.StartCam, WhichEntity, CameraSet.StartCamPos)
		ENDIF
		
		IF isRotation
			SET_CAM_PARAMS(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamPos), CameraSet.EndCamRot,CameraSet.CamFov,CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation )
		ELSE
			VECTOR vForward = Normalise(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamRot), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamPos))
			VECTOR vRotationEulers = ComputeEulersFromVector(vForward)
			SET_CAM_PARAMS(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.EndCamPos), vRotationEulers ,CameraSet.CamFov,CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation)
		ENDIF

	ENDIF

ENDPROC

PROC Run_camera_Attach_Entity_Params(INTERPCAMS& CameraSet, ENTITY_INDEX WhichEntity, BOOL isRotation = TRUE)


	IF NOT IS_ENTITY_DEAD(WhichEntity)

		IF isRotation
			SET_CAM_PARAMS(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos), CameraSet.StartCamRot,CameraSet.CamFov,CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation )
		ELSE
			VECTOR vForward = Normalise(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamRot), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos))
			VECTOR vRotationEulers = ComputeEulersFromVector(vForward)
			SET_CAM_PARAMS(CameraSet.StartCam, GET_CAM_COORD(CameraSet.StartCam), GET_CAM_ROT(CameraSet.StartCam) ,GET_CAM_FOV(CameraSet.StartCam),0, CameraSet.GraphPosition, CameraSet.GraphRotation)
			SET_CAM_PARAMS(CameraSet.StartCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, CameraSet.StartCamPos), vRotationEulers ,CameraSet.CamFov,CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation)
		ENDIF

	ENDIF

ENDPROC


PROC RunCameraShooter(ENTITY_INDEX Vehicle)

	VECTOR VehiclePos = GET_ENTITY_COORDS(Vehicle)
	FLOAT VehicleHeading = GET_ENTITY_HEADING(Vehicle)
	
	VECTOR BulletStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VehiclePos, VehicleHeading, <<0, 2, 0>>)
	
	VECTOR CameraPos = GET_GAMEPLAY_CAM_COORD()
	Vector CameraRotation = GET_GAMEPLAY_CAM_ROT()

	FLOAT CameraHeading = CameraRotation.z
	
	VECTOR BulletEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CameraPos, CameraHeading, <<0,250,0>>)
	
	DRAW_DEBUG_LINE(BulletStart, BulletEnd, 0, 250)

ENDPROC


//// PURPOSE:
 ///     Runs a camera created to an offset of a vehicle and interps
 /// PARAMS:
 ///    CameraSet - the INTERPCAMS
 ///    WhichEntity - the entity that the camera is placed in relation to
 ///    isRotation - the rotation or the point at coords
 ///    offset2 - The offset the camera will end up at relative to the entity
PROC Run_camera_Entity_Offset_interp_from_Current_camera(INTERPCAMS& CameraSet, ENTITY_INDEX WhichEntity, VECTOR offset2, VECTOR a_rotation, BOOL isRotation = TRUE)

	IF DOES_CAM_EXIST(CameraSet.EndCam)
		DESTROY_CAM(CameraSet.EndCam)
	ENDIF
				
	IF NOT IS_ENTITY_DEAD(WhichEntity)
	//	CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		SET_CAM_COORD(CameraSet.StartCam, startCamPos)	
//		IF isRotation
//			SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
//		ELSE
//			POINT_CAM_AT_ENTITY(CameraSet.StartCam, WhichEntity, a_rotation)
//		ENDIF
//		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		
		CameraSet.EndCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(CameraSet.EndCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(WhichEntity, offset2))	
		IF isRotation
			SET_CAM_ROT(CameraSet.EndCam, a_rotation)
		ELSE
			POINT_CAM_AT_ENTITY(CameraSet.EndCam, WhichEntity, a_rotation)
		ENDIF
		SET_CAM_FOV(CameraSet.EndCam, CameraSet.CamFov)
		
		CameraSet.ShotTime = 7000
		
		CameraSet.GraphPosition = GRAPH_TYPE_DECEL 
		CameraSet.GraphRotation = GRAPH_TYPE_DECEL
					
		SET_CAM_ACTIVE_WITH_INTERP(CameraSet.EndCam, CameraSet.StartCam, CameraSet.ShotTime, CameraSet.GraphPosition, CameraSet.GraphRotation)
		
	ENDIF

ENDPROC

////
//
PROC  Run_camera_Attach_ped(INTERPCAMS &CameraSet, PED_INDEX WhichPed, VECTOR offset,VECTOR a_rotation, PED_INDEX PointAtChar = NULL)

	IF DOES_CAM_EXIST(CameraSet.StartCam)
		DESTROY_CAM(CameraSet.StartCam)
	ENDIF
				
	IF NOT IS_ENTITY_DEAD(WhichPed)
		CameraSet.StartCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		ATTACH_CAM_TO_ENTITY(CameraSet.StartCam, WhichPed, offset)

		IF PointAtChar = NULL
			SET_CAM_ROT(CameraSet.StartCam, a_rotation)
		ELSE
			IF NOT IS_ENTITY_DEAD(PointAtChar)
				POINT_CAM_AT_ENTITY(CameraSet.StartCam, PointAtChar, MAKE_VECTOR_ZERO())
			ELSE
				PRINTNL()
				PRINTSTRING("------Pointing Camera at Dead Char's last position---------")
				PRINTNL()
				POINT_CAM_AT_COORD(CameraSet.StartCam, /*WITH DEADCHECK=FALSE*/ GET_ENTITY_COORDS(PointAtChar, FALSE))
			ENDIF
		ENDIF
		SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
		SET_CAM_ACTIVE(CameraSet.StartCam,  TRUE)

	ENDIF

ENDPROC




FUNC BOOL CreateDebugCar(VECTOR Position, FLOAT Heading, BOOL DebugCheck)

//	BOOL DebugCheck
	#IF IS_DEBUG_BUILD
//		DebugCheck = TRUE
	#ENDIF
	
	MODEL_NAMES mnDebugCar = SULTAN
	VEHICLE_INDEX viDebugCar

	IF DebugCheck = TRUE
		REQUEST_MODEL(mnDebugCar)
		IF HAS_MODEL_LOADED(mnDebugCar)
			IF NOT DOES_ENTITY_EXIST(viDebugCar)
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CLEAR_AREA(Position, 10.0, TRUE)
					viDebugCar = CREATE_VEHICLE(mnDebugCar, Position, Heading)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viDebugCar)
	//				SCRIPT_ASSERT("Put player in car")
				ENDIF	
			ENDIF
			RETURN TRUE
		ENDIF
	
	ELSE
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC SET_FREE_PARACHUTE_ENABLED(BOOL bActivate = TRUE)
	PRINTLN("Setting free parachute refill to ",bActivate)
	g_bGiveFreeParachute = bActivate
ENDPROC

FUNC BOOL IS_FREE_PARACHUTE_ON()
	RETURN g_bGiveFreeParachute
ENDFUNC

PROC MAINTAIN_FREE_PLAYER_PARACHUTE()
	#if USE_CLF_DLC
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			PRINTLN("MAINTAIN_FREE_PLAYER_PARACHUTE - Giving player free parachute")
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 0, FALSE, FALSE)
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			PRINTLN("MAINTAIN_FREE_PLAYER_PARACHUTE - Giving player free parachute")
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 0, FALSE, FALSE)
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_EXTREME4_BJUMPS_FINISHED]
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
			AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			AND NOT GET_IS_PLAYER_IN_ANIMAL_FORM() //B* 2434711: Stop parachutes being given to chickens
				PRINTLN("MAINTAIN_FREE_PLAYER_PARACHUTE - Giving player free parachute")
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 0, FALSE, FALSE)
			ENDIF
		ENDIF
	#endif	
	#endif
ENDPROC


