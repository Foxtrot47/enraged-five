//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 10/02/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	  Jewel Heist Planning Board Data Header					│
//│																				│
//│			This header contains lookups to all data required by the Jewel		│
//│			Store Job planning board.											│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "heist_board_data_generic.sch"


//Jewelry Heist Assets.
ENUM BoardAssetJewel
	PBA_JEWEL_DOC_BLUEPRINT = 0,		
	PBA_JEWEL_PHOTO_EXTERIOR,			
	PBA_JEWEL_PHOTO_JEWELS,				
	PBA_JEWEL_PHOTO_CCTV,			
	PBA_JEWEL_VENT_MAP,			
	PBA_JEWEL_PHOTO_TUNNEL,							
	PBA_JEWEL_BROCHURE,              	
	PBA_JEWEL_EXTEMINATOR_SUIT,      	
	PBA_JEWEL_BIKE,                 	
	PBA_JEWEL_ALARM,                  	
	PBA_JEWEL_GAS,
	PBA_JEWEL_PHOTO_VENT
ENDENUM


PROC Setup_Jewel_Heist_Board_Data(HeistBoardData &paramBoardData, INT paramMaxCrewSize)
	
	// Jewel Heist display groups.
	// PBDG_0 - Lester put up main map and labels.
	// PBDG_1 - Lester put up roof photo and map.
	// PBDG_2 - Lester put up tunnel photo and TODO list and approach choices.
	// PBDG_3 - Lester put up vent, alarm and jewelry photos.
	// PBDG_4 - Lester put up CCTV photo.
	// PBDG_5 - Gameplay approach chosen.
	// PBDG_6 - Crew chosen.
	// PBDG_7 - High impact approach chosen.
	// PBDG_8 - Stealth approach chosen.
	// PBDG_9 - Bugstar van obtained.
	// PBDG_10 - BZ gas obtained.
	// PBDG_11 - Carbine rifles obtained.
	
	//												Planning Location	Board Scaleform		Board Text	Board Dialogue	Crew Dialogue	Choice Group	Todo List Group		Chosen A	Chosen B	Max Crew Size		Blocking Area Center						Blocking Area Dimensions
	Store_Board_Core_Data(paramBoardData,			PLN_SWEATSHOP,		"HEIST_JEWELLERY",	"BOARD0",	"JHFAUD",		"CRWAUD",		PBDG_2,			PBDG_2,				PBDG_7,		PBDG_8,		paramMaxCrewSize,	<<708.995667,-965.256042,31.395329>>,		<<5.500000,6.250000,4.000000>>)
	
	//												SF Pixel Dim.		SF Dim.				SF Scale		Cam Dist		Default Focus	Crew 1 Pos		Crew 2 Pos		Crew 3 Pos		Crew 4 Pos	   	Crew 5 Pos	   	Choice Pos		Choice CamOff	List Pos		
	Store_Board_Main_Layout_Data(paramBoardData,	750, 540,			2.10, 1.47,			3.60, 2.05,		2.0,			375,250,		116,420,		248,425,		380,425,		510,430,		0,0,			650,300,		0,70,			684,106		)
		
	//												Max Zoom			Choice Zoom			POI Zoom
	Store_Board_Zoom_Levels(paramBoardData,			20.0,				22.0,				28.0)
	
	//												Choice Help 1		Choice Help 2		Crew Help 1		Crew Help 2		Choice Diag		Crew Diag		Confirm Diag	Too Slow Diag
	Store_Board_Text_Data(paramBoardData,			"BRD_H_01",			"BRD_H_02",			"BRD_H_03",		"BRD_H_04",		"JHFP1",		"JHFP4",		"JHP11",		"JHHUR")

	//Load crew member dialogue.									Crew Member					Pick Diag		Met Diag		
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_GUSTAV,			"CRW_GM",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_NORM,			"CRW_NR",		""			)		
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_G_PAIGE,			"CRW_PH",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_M_CHRIS,			"CRW_CF",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_G_EDDIE,			"CRW_ET",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_B_KARIM,			"CRW_KD",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_PACKIE_UNLOCK,	"CRW_PM",		"CRM_PM"  	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_B_RICKIE_UNLOCK,	"CRW_RL",		"CRM_RL"   	)	

	//Jewel heist static items.
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_DOC_BLUEPRINT),		PBDG_0,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_BROCHURE),			PBDG_0,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_PHOTO_EXTERIOR),		PBDG_0,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_PHOTO_CCTV),			PBDG_1,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_PHOTO_VENT),			PBDG_2,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_PHOTO_TUNNEL),		PBDG_3,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_ALARM),				PBDG_3,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_PHOTO_JEWELS),		PBDG_4,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_EXTEMINATOR_SUIT),	PBDG_9,		0,0		)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_JEWEL_GAS),					PBDG_10,	0,0		) 
	
	//Jewel heist todo items.				
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_2,		PBDG_2,		"H_TD_SEC")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_2,		PBDG_2,		"H_TD_PHOTO")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_2,		PBDG_5,		"H_TD_PLAN")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_2,		PBDG_6,		"H_TD_CREW")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_7,		PBDG_11,	"H_TD_CARB")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_8,		PBDG_9,		"H_TD_BUGS")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_8,		PBDG_10,	"H_TD_GAS")
	
	//Jewel heist pins.
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_0,		203,74		) 	// Blueprint
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_1,		368,181		) 	// Brochure
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_1,		556,189		) 	// Escape map
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_2,		690,63		) 	// TODO List
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_2,		673,308		) 	// Approach A
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_2,		671,406		) 	// Approach B
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_3,		245,244		) 	// Alarm
	Store_Board_Pin_Layout_Data(paramBoardData,		PBDG_4,		70,68		) 	// CCTV
	
	//Jewel heist points of interest.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"JHP12",		214,165		)		// Floor
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"JHP13",		254,46		)		// Vent photo
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"JHP14",		70,97		)		// CCTV photo
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"JHP15",		254,275		)		// Alarm photo
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"JHP16",		457,165		)		// Tunnel photo
ENDPROC
