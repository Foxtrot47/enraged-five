//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 22/02/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Heist Planning Board Header						│
//│																				│
//│		This header contains the script that creates/maintains/destroys			│
//│		planning boards.														│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_streaming.sch"
USING "commands_entity.sch"
USING "commands_object.sch"
USING "commands_recording.sch"

USING "blip_control_public.sch"
USING "cellphone_public.sch"
USING "context_control_public.sch"
USING "flow_public_core_override.sch"
USING "mission_control_public.sch"

USING "script_heist.sch"
USING "heist_private.sch"
USING "flow_help_public.sch"
USING "dialogue_public.sch"
USING "net_fps_cam.sch"
USING "director_mode_public.sch"
USING "script_misc.sch"


//═══════════════════════════════╡ Constants ╞═════════════════════════════════

//Analogue stick axis indexes.
CONST_INT PB_LEFT_X		0
CONST_INT PB_LEFT_Y		1
CONST_INT PB_RIGHT_X	2
CONST_INT PB_RIGHT_Y	3

//Button bindings.
CONTROL_ACTION PB_CONTROL_EXIT 			= INPUT_FRONTEND_CANCEL
CONTROL_ACTION PB_CONTROL_SELECT 		= INPUT_FRONTEND_ACCEPT
CONTROL_ACTION PB_CONTROL_SELECT_M 		= INPUT_ATTACK
CONTROL_ACTION PB_CONTROL_UNDO 			= INPUT_FRONTEND_CANCEL
CONTROL_ACTION PB_CONTROL_UNDO_M		= INPUT_AIM

//Selection change rate.
CONST_INT PB_SELECTION_CHANGE_TIME		200
CONST_INT PB_NO_INPUT_COMMENT_TIME		25000

//Camera look swing angles.
CONST_INT PB_CAM_LOOK_X					18
CONST_INT PB_CAM_LOOK_Y					13
CONST_INT PB_CAM_LOOK_ROLL				3

//Camera FOV limits.
CONST_FLOAT PB_CAM_FOV_DEFAULT				45.0
CONST_FLOAT PB_CAM_FOCUS_PIXEL_RANGE_SCALE	0.85

//The pacing delay between describing each POI in a board's POI overveiw.
CONST_INT	PB_TIME_BETWEEN_POIS 		500

//The stare duration for each POI before free look is activated
CONST_INT PB_DISABLE_FREE_LOOK_TIME		1000

//How close does the player need to be to the board to activate the board view.
CONST_FLOAT PB_ACTIVATION_RANGE			0.8

//Most conversations that can be queued up at a time.
CONST_INT	PB_MAX_CONV_QUEUE			3

//How many frames a board view has to be active before it can be exited again.
CONST_INT WAIT_FRAMES_FOR_BOARD_EXIT 	15

//State bit indexes.
CONST_INT BIT_BOARD_LOADED						0
CONST_INT BIT_BOARD_CREATED						1
CONST_INT BIT_BOARD_VIEW_INITIALISED			2
CONST_INT BIT_BOARD_HAS_MISS_CONTROL			3
CONST_INT BIT_BOARD_ZOOM_CONTROL_ON				4
CONST_INT BIT_BOARD_LOOK_CONTROL_ON				5
CONST_INT BIT_BOARD_LEAVE_AREA					6
CONST_INT BIT_BOARD_PLAYER_SETUP				7
CONST_INT BIT_BOARD_AUTO_EXIT					8
CONST_INT BIT_BOARD_WAIT_FOR_DIR_INPUT			9
CONST_INT BIT_BOARD_WAIT_FOR_DIALOGUE			10
CONST_INT BIT_BOARD_DIALOGUE_LOCKED				11
CONST_INT BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP	12
CONST_INT BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE	13
CONST_INT BIT_BOARD_REFRESH_TODO_LIST			14
CONST_INT BIT_BOARD_REFRESH_PINS				15
CONST_INT BIT_BOARD_REFRESH_STATIC_ITEMS		16
CONST_INT BIT_BOARD_NO_REFOCUS_AFTER_LOCK		17
CONST_INT BIT_BOARD_HIDE_HELP					18
CONST_INT BIT_BOARD_DELAY_NEXT_POI				19


//═══════════════════════════════╡ Structures ╞════════════════════════════════

STRUCT BoardDialogue
	TEXT_LABEL_7 	txtBlock
	TEXT_LABEL_7 	txtConversation
	BOOL 			bSkippable
ENDSTRUCT

//This is a very heavyweight structure. However there will only ever be one 
//per active heist in existence at any given time.
STRUCT PlanningBoard
	//═══════Linked heist══════
	INT iLinkedHeist
	
	//═══════Board data════════
	HeistBoardData sData

	//═════Board placement═════
	VECTOR vPosition
	FLOAT fHeading
	
	//══════Board camera═══════
	VECTOR vCamPos
	VECTOR vCamBaseRot
	BoardPixel sFocusPixel

	//══Board scaleform indexes══
	SCALEFORM_INDEX siBoardMovie
	SCALEFORM_INDEX siHelpTextMovie
	
	//══Scaleform view indexes═
	INT iGameplayChoiceStartIndex
	INT iGameplayChoiceHighlighted
	
	//═════Crew selection══════
	INT iCrewSlotSelected
	CrewMember eCrewSlotLayout[8]
	CrewMember eHoverCrewMember = CM_EMPTY
	
	//═══════Board pins════════
	OBJECT_INDEX oPin[MAX_BOARD_PINS]
	
	//═══════Board state═══════
	INT iState
	INT iSelectedView = 0
	INT iContextID = NEW_CONTEXT_INTENTION
	INT iPOITimer = -1
	INT iCancelOutCooldownTimer
	INT iDisableFreeLookTime = 0
	INT iFramesWithoutDialogue = 0
	INT iSelectionChangeTimer
	INT iChoiceNoInputTimer
	INT iStickPosition[4]
	INT iCrewDialoguePlayedBitset
	g_eBoardModes eMode
	CrewMemberType eLastCrewType
	STRING eLookInputLastFrame
	
	//═══Conversation Queue════
	BoardDialogue	sConvQueue[PB_MAX_CONV_QUEUE]
	INT 			iNoConvQueued
	
	structPedsForConversation sPedsForConversation
	FIRST_PERSON_CAM_STRUCT sCam
	
	SCALEFORM_RETURN_INDEX sriGameplayChoice 	= NULL
	SCALEFORM_RETURN_INDEX sriCrewChoice 		= NULL
	SCALEFORM_RETURN_INDEX sriCrewHover 		= NULL
ENDSTRUCT



//═════════════════════╡ Utility Procedures/Functions ╞════════════════════════


PROC Set_Board_Help_Hidden(PlanningBoard &sPlanningBoard, BOOL bHelpBlocked)
	IF bHelpBlocked
		CPRINTLN(DEBUG_HEIST, "Hiding board help.")
		DEBUG_PRINTCALLSTACK()
		SET_BIT(sPlanningBoard.iState, BIT_BOARD_HIDE_HELP)
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tChoiceHelp[0])
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tChoiceHelp[1])
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tCrewHelp[0])
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tCrewHelp[1])
			CLEAR_HELP(FALSE)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_HEIST, "Unhiding board help.")
		DEBUG_PRINTCALLSTACK()
		CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_HIDE_HELP)
	ENDIF
ENDPROC


PROC Queue_Board_Conversation(PlanningBoard &sPlanningBoard, TEXT_LABEL_7 txtBlock, TEXT_LABEL_7 txtConversation, BOOL bSkippable = TRUE)
	IF sPlanningBoard.iNoConvQueued = PB_MAX_CONV_QUEUE
		SCRIPT_ASSERT("Queue_Board_Conversation: The board's queue was full. Increase PB_MAX_CONV_QUEUE? Tell BenR.")
		EXIT
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(txtConversation)
		sPlanningBoard.sConvQueue[sPlanningBoard.iNoConvQueued].txtBlock 		= txtBlock
		sPlanningBoard.sConvQueue[sPlanningBoard.iNoConvQueued].txtConversation = txtConversation
		sPlanningBoard.sConvQueue[sPlanningBoard.iNoConvQueued].bSkippable 		= bSkippable
		sPlanningBoard.iNoConvQueued++
		CPRINTLN(DEBUG_HEIST, "Planning board queued dialogue. Block:",txtBlock," Conv:",txtConversation,".")
#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_HEIST, "Tired to queue dialogue with an empty conversation root. No dialogue queued.")
#ENDIF
	ENDIF
ENDPROC


PROC Update_Board_Conversations(PlanningBoard &sPlanningBoard)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND IS_SCREEN_FADED_IN()
	AND NOT IS_CUTSCENE_PLAYING()
		
		IF sPlanningBoard.iNoConvQueued > 0
			IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE)
				IF CREATE_CONVERSATION(	sPlanningBoard.sPedsForConversation,
										sPlanningBoard.sConvQueue[0].txtBlock,
										sPlanningBoard.sConvQueue[0].txtConversation,
										CONV_PRIORITY_AMBIENT_HIGH)
					CPRINTLN(DEBUG_HEIST, "Planning board started playing dialogue. Block:",sPlanningBoard.sConvQueue[0].txtBlock," Conv:",sPlanningBoard.sConvQueue[0].txtConversation,".")
					
					sPlanningBoard.iFramesWithoutDialogue = 0

					IF sPlanningBoard.sConvQueue[0].bSkippable
						CPRINTLN(DEBUG_HEIST, "New board dialogue isn't locked.")
						CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
					ELSE
						CPRINTLN(DEBUG_HEIST, "New board dialogue is locked.")
						SET_BIT(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
					ENDIF
					
					//Remove conversation from queue.
					INT iConvIndex
					REPEAT (PB_MAX_CONV_QUEUE-1) iConvIndex
						sPlanningBoard.sConvQueue[iConvIndex].txtBlock 			= sPlanningBoard.sConvQueue[iConvIndex+1].txtBlock
						sPlanningBoard.sConvQueue[iConvIndex].txtConversation	= sPlanningBoard.sConvQueue[iConvIndex+1].txtConversation
						sPlanningBoard.sConvQueue[iConvIndex].bSkippable		= sPlanningBoard.sConvQueue[iConvIndex+1].bSkippable
					ENDREPEAT
					sPlanningBoard.iNoConvQueued--
				ENDIF
			ELSE
				CPRINTLN(DEBUG_HEIST, "Skipping conversation as we were flagged to skip conversations this frame.")
				CPRINTLN(DEBUG_HEIST, "Block:",sPlanningBoard.sConvQueue[0].txtBlock," Conv:",sPlanningBoard.sConvQueue[0].txtConversation,".")
				
				//Remove conversation from queue.
				INT iConvIndex
				REPEAT (PB_MAX_CONV_QUEUE-1) iConvIndex
					sPlanningBoard.sConvQueue[iConvIndex].txtBlock 			= sPlanningBoard.sConvQueue[iConvIndex+1].txtBlock
					sPlanningBoard.sConvQueue[iConvIndex].txtConversation	= sPlanningBoard.sConvQueue[iConvIndex+1].txtConversation
					sPlanningBoard.sConvQueue[iConvIndex].bSkippable		= sPlanningBoard.sConvQueue[iConvIndex+1].bSkippable
				ENDREPEAT
				sPlanningBoard.iNoConvQueued--
				
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE)
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
			ENDIF
		ELSE
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
				CPRINTLN(DEBUG_HEIST, "Clearing previous dialogue lock.")
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
			ENDIF
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE)
				CPRINTLN(DEBUG_HEIST, "Clearing multi skip dialogue flag.")
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE)
			ENDIF
			sPlanningBoard.iFramesWithoutDialogue++
		ENDIF
	ENDIF
ENDPROC


PROC Play_Board_Dialogue_From_Slot_If_Unplayed(PlanningBoard &sPlanningBoard, BoardDialogueSlot paramSlot, BOOL paramSkippable = FALSE)
	IF NOT IS_BIT_SET(sPlanningBoard.sData.iBitsetBoardDialoguePlayed, ENUM_TO_INT(paramSlot))
		IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tBoardDialogueLabels[paramSlot], "")
			Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tBoardDialogueLabels[paramSlot], paramSkippable)
			SET_BIT(sPlanningBoard.sData.iBitsetBoardDialoguePlayed, ENUM_TO_INT(paramSlot))
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_HEIST, "Play_Board_Dialogue_From_Slot_If_Unplayed, Queueing: ",sPlanningBoard.sData.tBoardDialogueLabels[paramSlot], ".")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC Play_Heist_Choice_Dialogue_From_Slot_If_Unplayed(PlanningBoard &sPlanningBoard, INT paramHeistChoice, HeistChoiceDialogueSlot paramSlot)

	IF NOT IS_BIT_SET(g_sHeistChoiceData[paramHeistChoice].iBitsetChoiceDialoguePlayed, ENUM_TO_INT(paramSlot))
		IF NOT ARE_STRINGS_EQUAL(g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[paramSlot], "")
			Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[paramSlot], TRUE)
			SET_BIT(g_sHeistChoiceData[paramHeistChoice].iBitsetChoiceDialoguePlayed, ENUM_TO_INT(paramSlot))
			INT iOtherChoicesIndex
			REPEAT MAX_HEIST_CHOICES iOtherChoicesIndex
				IF ARE_STRINGS_EQUAL(g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[paramSlot], g_sHeistChoiceData[iOtherChoicesIndex].tChoiceDialogueLabels[paramSlot])
					SET_BIT(g_sHeistChoiceData[iOtherChoicesIndex].iBitsetChoiceDialoguePlayed, ENUM_TO_INT(paramSlot))
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC


PROC Play_Crew_Member_Dialogue(PlanningBoard &sPlanningBoard, CrewMember paramCrewMember)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_HEIST, "Playing crew picked dialogue for ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ".")
	#ENDIF
	
	IF NOT IS_BIT_SET(sPlanningBoard.iCrewDialoguePlayedBitset, ENUM_TO_INT(paramCrewMember))
		//Play dialogue the first time a crew member is selected on a board.
		IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(paramCrewMember))
			IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewDialogueLabels[paramCrewMember], "")
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing FIRST PICK dialogue.")
				#ENDIF
				Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tCrewDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[paramCrewMember], TRUE)
				SET_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(paramCrewMember))
			ENDIF
		
		//Play dialogue related to previous heists.
		ELSE
			SWITCH sPlanningBoard.iLinkedHeist
				
				//Paleto Score specific dialogue.
				CASE HEIST_RURAL_BANK
					SWITCH paramCrewMember
						CASE CM_GUNMAN_G_GUSTAV
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_GUSTAV)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
							ENDIF
						BREAK
						CASE CM_GUNMAN_G_PACKIE_UNLOCK
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PACKIE)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				//Bureau Raid specific dialogue.
				CASE HEIST_AGENCY
					SWITCH paramCrewMember
						CASE CM_HACKER_B_RICKIE_UNLOCK
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_RICKIE)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing RICKIE_LAST_ONE dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_RICKIE_LAST_ONE], TRUE)
							ENDIF
						BREAK
						CASE CM_DRIVER_B_KARIM
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_KARIM)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing KARIM_LAST_ONE dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_KARIM_LAST_ONE], TRUE)
							ENDIF
						BREAK
						CASE CM_GUNMAN_G_GUSTAV
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_GUSTAV)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_GUSTAV)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL_PALETO dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL_PALETO], TRUE)
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
								ENDIF
							ELIF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_GUSTAV)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_PALETO dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_PALETO], TRUE)
							ENDIF
						BREAK
						CASE CM_GUNMAN_G_PACKIE_UNLOCK
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PACKIE)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_PACKIE)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL_PALETO dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL_PALETO], TRUE)
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
								ENDIF
							ELIF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_PACKIE)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_PALETO dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_PALETO], TRUE)
							ENDIF
						BREAK
						CASE CM_HACKER_G_PAIGE
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PAIGE)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
							ENDIF
						BREAK
						CASE CM_HACKER_M_CHRIS
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_CHRIS)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
							ENDIF
						BREAK
						CASE CM_DRIVER_G_EDDIE
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_EDDIE)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL], TRUE)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				//The Big Score specific dialogue.
				CASE HEIST_FINALE
					SWITCH paramCrewMember
						CASE CM_HACKER_B_RICKIE_UNLOCK
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_RICKIE)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_RICKIE)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_RICKIE_LAST_TWO dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_RICKIE_LAST_TWO], TRUE)
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_RICKIE_LAST_ONE dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_RICKIE_LAST_ONE], TRUE)
								ENDIF
							ENDIF
						BREAK
						CASE CM_HACKER_M_CHRIS
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_CHRIS)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_CHRIS)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_CHRIS_JEWEL_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_CHRIS_JEWEL_AGENCY], TRUE)
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_CHRIS_JEWEL dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_CHRIS_JEWEL], TRUE)
								ENDIF
							ELIF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_CHRIS)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_CHRIS_AGENCY dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_CHRIS_AGENCY], TRUE)
							ENDIF
						BREAK
						CASE CM_DRIVER_B_KARIM
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_KARIM)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_KARIM)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_KARIM_LAST_TWO dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_KARIM_LAST_TWO], TRUE)
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_KARIM_LAST_ONE dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_KARIM_LAST_ONE], TRUE)
								ENDIF
							ENDIF
						BREAK
						CASE CM_GUNMAN_B_NORM
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_NORM)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_BADG_AGENCY dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_BADG_AGENCY], TRUE)
							ENDIF
						BREAK
						CASE CM_GUNMAN_B_DARYL
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_DARYL)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_BADG_AGENCY dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_BADG_AGENCY], TRUE)
							ENDIF
						BREAK
						CASE CM_GUNMAN_G_GUSTAV
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_GUSTAV)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_GUSTAV)
									IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_GUSTAV)
										#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_ALL dialogue.")
										#ENDIF
										Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_ALL], TRUE)	
									ELSE
										#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL_AGENCY dialogue.")
										#ENDIF
										Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL_AGENCY], TRUE)	
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_AGENCY], TRUE)	
								ENDIF
							ENDIF
						BREAK
						CASE CM_GUNMAN_G_PACKIE_UNLOCK
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_PACKIE)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PACKIE)
									IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_PACKIE)
										#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_ALL dialogue.")
										#ENDIF
										Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_ALL], TRUE)	
									ELSE
										#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL_AGENCY dialogue.")
										#ENDIF
										Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL_AGENCY], TRUE)	
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_AGENCY], TRUE)	
								ENDIF
							ENDIF
						BREAK
						CASE CM_GUNMAN_M_HUGH
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_HUGH)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_AGENCY dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_AGENCY], TRUE)
							ENDIF
						BREAK
						CASE CM_HACKER_G_PAIGE
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_PAIGE)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PAIGE)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL_AGENCY], TRUE)	
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_AGENCY], TRUE)	
								ENDIF
							ENDIF
						BREAK
						CASE CM_DRIVER_G_EDDIE
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_EDDIE)
								IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_EDDIE)
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_JEWEL_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_JEWEL_AGENCY], TRUE)	
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_AGENCY dialogue.")
									#ENDIF
									Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_AGENCY], TRUE)	
								ENDIF
							ENDIF
						BREAK
						CASE CM_DRIVER_G_TALINA_UNLOCK
							IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_TALINA)
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_HEIST, GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ": Queing USED_AGENCY dialogue.")
								#ENDIF
								Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_USED_AGENCY], TRUE)	
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
			ENDSWITCH
		ENDIF
		SET_BIT(sPlanningBoard.iCrewDialoguePlayedBitset, ENUM_TO_INT(paramCrewMember))
	ENDIF
	
ENDPROC


PROC Play_Crew_Member_Hover_Dialogue(PlanningBoard &sPlanningBoard, CrewMember paramCrewMember)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF sPlanningBoard.iNoConvQueued = 0
			SWITCH paramCrewMember
				CASE CM_GUNMAN_G_PACKIE_UNLOCK
					IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_PACKIE))
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_HEIST, "Attempting to play just met dialogue for ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ".")
						#ENDIF
						CPRINTLN(DEBUG_HEIST, "Bit set.")
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_PACKIE], "")
							CPRINTLN(DEBUG_HEIST, "String not empty: ", sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_PACKIE])
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
							Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tCrewDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_PACKIE], TRUE)
							CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_PACKIE))
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP)
							EXIT
						ENDIF
					ENDIF
				BREAK
				CASE CM_DRIVER_G_TALINA_UNLOCK
					IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_TALINA))
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_HEIST, "Attempting to play just met dialogue for ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ".")
						#ENDIF
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_TALINA], "")
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
							Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tCrewDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_TALINA], TRUE)
							CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_TALINA))
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP)
							EXIT
						ENDIF
					ENDIF
				BREAK
				CASE CM_HACKER_B_RICKIE_UNLOCK
					IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_RICKIE))
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_HEIST, "Attempting to play just met dialogue for ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ".")
						#ENDIF
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_RICKIE], "")
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
							Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tCrewDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_RICKIE], TRUE)
							CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_RICKIE))
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP)
							EXIT
						ENDIF
					ENDIF
				BREAK
				CASE CM_GUNMAN_G_CHEF_UNLOCK
					IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_CHEF))
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_HEIST, "Attempting to play just met dialogue for ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(paramCrewMember)), ".")
						#ENDIF
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_CHEF], "")
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
							Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tCrewDialogueSlot, sPlanningBoard.sData.tCrewDialogueLabels[CRDS_MET_CHEF], TRUE)
							CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_CHEF))
							SET_BIT(sPlanningBoard.iState, BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP)
							EXIT
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC


PROC Update_Board_Text(PlanningBoard &sPlanningBoard)
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_PLAYER_SETUP)
		IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_HIDE_HELP)
			SWITCH (sPlanningBoard.eMode)
				CASE PBM_CHOICE_SELECT
					IF NOT g_savedGlobals.sHeistData.bChoiceHelpDisplayed[sPlanningBoard.iLinkedHeist]
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tChoiceHelp[0], "")
							ADD_HELP_TO_FLOW_QUEUE(sPlanningBoard.sData.tChoiceHelp[0], FHP_VERY_HIGH)
						ENDIF
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tChoiceHelp[1], "")
							ADD_HELP_TO_FLOW_QUEUE(sPlanningBoard.sData.tChoiceHelp[1], FHP_VERY_HIGH, 1000)
						ENDIF
						g_savedGlobals.sHeistData.bChoiceHelpDisplayed[sPlanningBoard.iLinkedHeist] = TRUE
					ELIF IS_FLOW_HELP_QUEUE_EMPTY() AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF (NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED) 
						AND NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE) 
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND sPlanningBoard.iNoConvQueued = 0)
							BEGIN_TEXT_COMMAND_DISPLAY_HELP("PB_H_CHOICE")
							END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, FALSE)
						ENDIF
					ENDIF
				BREAK
			
				CASE PBM_CREW_SELECT
					IF NOT g_savedGlobals.sHeistData.bCrewHelpDisplayed[sPlanningBoard.iLinkedHeist]
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewHelp[0], "")
							ADD_HELP_TO_FLOW_QUEUE(sPlanningBoard.sData.tCrewHelp[0], FHP_VERY_HIGH)
						ENDIF
						IF NOT ARE_STRINGS_EQUAL(sPlanningBoard.sData.tCrewHelp[1], "")
							ADD_HELP_TO_FLOW_QUEUE(sPlanningBoard.sData.tCrewHelp[1], FHP_VERY_HIGH, 1000)
						ENDIF
						g_savedGlobals.sHeistData.bCrewHelpDisplayed[sPlanningBoard.iLinkedHeist] = TRUE
					ELIF IS_FLOW_HELP_QUEUE_EMPTY() AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PB_H_GUNM")
							AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PB_H_HACK") 
							AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PB_H_DRIV") 
								INT iHeistChoice
								iHeistChoice = Get_Mission_Flow_Int_Value(Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist))
								IF sPlanningBoard.iCrewSlotSelected < MAX_CREW_SIZE
									IF (NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED) 
									AND NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE) 
									AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									AND sPlanningBoard.iNoConvQueued = 0)
										SWITCH g_sHeistChoiceData[iHeistChoice].eCrewType[sPlanningBoard.iCrewSlotSelected]
											CASE CMT_GUNMAN	
												BEGIN_TEXT_COMMAND_DISPLAY_HELP("PB_H_GUNM")
												END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, FALSE)
											BREAK
											CASE CMT_HACKER	
												BEGIN_TEXT_COMMAND_DISPLAY_HELP("PB_H_HACK")
												END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, FALSE)
											BREAK
											CASE CMT_DRIVER	
												BEGIN_TEXT_COMMAND_DISPLAY_HELP("PB_H_DRIV")
												END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, FALSE)
											BREAK
										ENDSWITCH
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
ENDPROC


PROC Get_Local_Board_Position_For_Scaleform_Pixel(PlanningBoard &sPlanningBoard, BoardPixel &sPixel, FLOAT &fScaleformX, FLOAT &fScaleformY)
	INT iPixelWidth = sPlanningBoard.sData.iScaleformPixelWidth
	INT iPixelHeight = sPlanningBoard.sData.iScaleformPixelHeight
	
	//Check passed pixels are in the valid range.
	IF sPixel.x < 0 OR sPixel.x > iPixelWidth
		SCRIPT_ASSERT("Get_Board_Position_For_Scaleform_Pixel: Passed pixel X coord was not in a valid range")
		EXIT
	ENDIF
	IF sPixel.y < 0 OR sPixel.y > iPixelHeight
		SCRIPT_ASSERT("Get_Board_Position_For_Scaleform_Pixel: Passed pixel Y coord was not in a valid range")
		EXIT
	ENDIF

	//Convert pixel positions to board screen positions.
	fScaleformX = TO_FLOAT(sPixel.x) / TO_FLOAT(iPixelWidth)
	fScaleformY = TO_FLOAT(sPixel.y) / TO_FLOAT(iPixelHeight)
ENDPROC


FUNC VECTOR Get_World_Board_Position_For_Scaleform_Pixel(PlanningBoard &sPlanningBoard, BoardPixel &sPixel)

	FLOAT fScaleformWidth = sPlanningBoard.sData.fScaleformWidth
	FLOAT fScaleformHeight = sPlanningBoard.sData.fScaleformHeight

	//Get the local board position for this pixel.
	FLOAT fLocalBoardXPos
	FLOAT fLocalBoardYPos
	Get_Local_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixel, fLocalBoardXPos, fLocalBoardYPos)
	
	//Get this as an offset from the center of the board.
	FLOAT fBoardXOffset = fScaleformWidth * (fLocalBoardXPos - 0.5)
	FLOAT fBoardYOffset = -fScaleformHeight * (fLocalBoardYPos - 0.5)
	FLOAT fHeading = sPlanningBoard.fHeading
	
	//Translate this into the board's world position.
	VECTOR vWorldPosition = sPlanningBoard.vPosition
	
	vWorldPosition += <<fBoardXOffset*SIN(90-fHeading), fBoardXOffset*COS(90-fHeading), fBoardYOffset>>
	
	RETURN vWorldPosition
ENDFUNC


FUNC VECTOR Get_Board_Camera_Position_For_Scaleform_Pixel(PlanningBoard &sPlanningBoard, BoardPixel &sPixel)

	//Translate pixel position into the board's world position.
	VECTOR vWorldPosition = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixel)
	
	//Add camera offset and return.
	RETURN vWorldPosition + <<-sPlanningBoard.sData.fCameraDistance*SIN(360 - sPlanningBoard.fHeading),-sPlanningBoard.sData.fCameraDistance*COS(360 - sPlanningBoard.fHeading),0.0>>
ENDFUNC


FUNC VECTOR Get_Board_Camera_Rotation_For_Scaleform_Pixel(PlanningBoard &sPlanningBoard, BoardPixel &sPixel)

	FLOAT fScaleformWidth = sPlanningBoard.sData.fScaleformWidth
	FLOAT fScaleformHeight = sPlanningBoard.sData.fScaleformHeight

	//Get the local board position for this pixel.
	FLOAT fLocalBoardXPos
	FLOAT fLocalBoardYPos
	Get_Local_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixel, fLocalBoardXPos, fLocalBoardYPos)
	
	//Get this as an offset from the center of the board.
	FLOAT fBoardXOffset = fScaleformWidth * (fLocalBoardXPos - 0.5)
	FLOAT fBoardYOffset = -fScaleformHeight * (fLocalBoardYPos - 0.5)
	FLOAT fHeading = sPlanningBoard.fHeading
	
	//Translate this into the board's world position.
	VECTOR vWorldPixelPos = sPlanningBoard.vPosition
	vWorldPixelPos += <<fBoardXOffset*SIN(90-fHeading), fBoardXOffset*COS(90-fHeading), fBoardYOffset>>
	
	FLOAT fXYDifference = fBoardXOffset
	
	//Get XY angle between board normal and cam-pixel vector.
	FLOAT fXYAngle = ATAN(fXYDifference/sPlanningBoard.sData.fCameraDistance)

	//Get Z dist from camera to board center.
	FLOAT fDirCamToBoardZ = sPlanningBoard.vPosition.Z - sPlanningBoard.sCam.vInitCamPos.Z
	//Get Z dist from camera to pixel.
	FLOAT fDirCamToPixelZ = vWorldPixelPos.Z - sPlanningBoard.sCam.vInitCamPos.Z
	//Get difference between Z distances.
	FLOAT fZDifference = fDirCamToPixelZ - fDirCamToBoardZ
	
	//Get Z angle between board normal and cam-pixel vector.
	FLOAT fZAngle = ATAN(fZDifference/sPlanningBoard.sData.fCameraDistance)
	
	FLOAT fRollAngle = fXYAngle*PB_CAM_LOOK_ROLL/PB_CAM_LOOK_X
	
	//Return rotation vector.
	RETURN <<fZAngle*0.95, fRollAngle, -fXYAngle*0.85>>
ENDFUNC


FUNC BoardPixel Get_Board_Focus_Pixel_From_Stick_Positions(PlanningBoard &sPlanningBoard)
	BoardPixel sNewPixel
	
	IF sPlanningBoard.iStickPosition[PB_RIGHT_X] != 0
		FLOAT fLookRatioX = TO_FLOAT(sPlanningBoard.iStickPosition[PB_RIGHT_X]) / 127.0
		FLOAT fPixelLookRangeX
		IF sPlanningBoard.iStickPosition[PB_RIGHT_X] < 0
			fPixelLookRangeX = TO_FLOAT(sPlanningBoard.sFocusPixel.x) * PB_CAM_FOCUS_PIXEL_RANGE_SCALE
		ELIF sPlanningBoard.iStickPosition[PB_RIGHT_X] > 0
			fPixelLookRangeX = TO_FLOAT(sPlanningBoard.sData.iScaleformPixelWidth - sPlanningBoard.sFocusPixel.x) * PB_CAM_FOCUS_PIXEL_RANGE_SCALE		ENDIF
		sNewPixel.x = sPlanningBoard.sFocusPixel.x + FLOOR(fPixelLookRangeX*fLookRatioX)
	ELSE
		sNewPixel.x = sPlanningBoard.sFocusPixel.x
	ENDIF
	
	IF sPlanningBoard.iStickPosition[PB_RIGHT_Y] != 0
		FLOAT fLookRatioY = TO_FLOAT(sPlanningBoard.iStickPosition[PB_RIGHT_Y]) / 127.0
		FLOAT fPixelLookRangeY
		IF sPlanningBoard.iStickPosition[PB_RIGHT_Y] < 0
			fPixelLookRangeY = TO_FLOAT(sPlanningBoard.sFocusPixel.y) * PB_CAM_FOCUS_PIXEL_RANGE_SCALE
		ELIF sPlanningBoard.iStickPosition[PB_RIGHT_Y] > 0
			fPixelLookRangeY = TO_FLOAT(sPlanningBoard.sData.iScaleformPixelHeight - sPlanningBoard.sFocusPixel.y) * PB_CAM_FOCUS_PIXEL_RANGE_SCALE
		ENDIF
		sNewPixel.y = sPlanningBoard.sFocusPixel.y + FLOOR(fPixelLookRangeY*fLookRatioY)
	ELSE
		sNewPixel.y = sPlanningBoard.sFocusPixel.y
	ENDIF

	RETURN sNewPixel
ENDFUNC


//═══════════════════════════╡ Board Configuring ╞═════════════════════════════

FUNC CrewMember Get_CrewMember_In_Slot_With_Type(CrewMemberType eType, INT iSlotIndex)

	SWITCH eType
		CASE CMT_GUNMAN
			SWITCH iSlotIndex
				CASE 0
					RETURN CM_GUNMAN_G_GUSTAV
				BREAK
				CASE 1
					RETURN CM_GUNMAN_G_KARL
				BREAK
				CASE 2
					RETURN CM_GUNMAN_G_PACKIE_UNLOCK
				BREAK
				CASE 3
					RETURN CM_GUNMAN_G_CHEF_UNLOCK
				BREAK
				CASE 4
					RETURN CM_GUNMAN_M_HUGH
				BREAK
				CASE 5
					RETURN CM_GUNMAN_B_NORM
				BREAK
				CASE 6
					RETURN CM_GUNMAN_B_DARYL
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CMT_HACKER
			SWITCH iSlotIndex
				CASE 0
					RETURN CM_HACKER_G_PAIGE
				BREAK
				CASE 1
					RETURN CM_HACKER_M_CHRIS
				BREAK
				CASE 2
					RETURN CM_HACKER_B_RICKIE_UNLOCK
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CMT_DRIVER
			SWITCH iSlotIndex
				CASE 0
					RETURN CM_DRIVER_G_EDDIE
				BREAK
				CASE 1
					RETURN CM_DRIVER_G_TALINA_UNLOCK
				BREAK
				CASE 2
					RETURN CM_DRIVER_B_KARIM
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN CM_ERROR
ENDFUNC


FUNC BOOL Is_Crew_Member_Already_Selected_On_Board(PlanningBoard &sPlanningBoard, CrewMember crewMem)
	INT iHeistChoice = Get_Mission_Flow_Int_Value(Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist))
	INT index
	
	REPEAT sPlanningBoard.iCrewSlotSelected index
		IF g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][index] = crewMem
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


PROC Setup_Planning_Board_Labels(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Setting up board labels.")
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_LABELS")
	
		//Pass in crew member stat text.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_CRW_NAME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_CRW_TYPE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_CRW_SKILLS")
		
		//Pass in heist name text.
		SWITCH sPlanningBoard.iLinkedHeist
			CASE HEIST_JEWEL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_JWL")
			BREAK
			CASE HEIST_DOCKS
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_DOC")
			BREAK
			CASE HEIST_RURAL_BANK
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_RUR")
			BREAK
			CASE HEIST_AGENCY
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_AGN")
			BREAK
			CASE HEIST_FINALE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_FA")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_FB")
			BREAK
		ENDSWITCH
		
		//Pass in crew/todo/approach text.
		IF sPlanningBoard.iLinkedHeist != HEIST_DOCKS
			//Port of LS heist doesn't have a crew to choose.
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_CRW")
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_TODO")
		IF sPlanningBoard.iLinkedHeist != HEIST_RURAL_BANK
			//Paleto Score doesn't have an approach to choose.
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_APP") 
		ENDIF
		
		//Pass in heist specific text.
		SWITCH sPlanningBoard.iLinkedHeist
			CASE HEIST_JEWEL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_J1")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_J2")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_J3")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_J4")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_J_IMPACT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_J_STEALTH")
			BREAK
			CASE HEIST_DOCKS
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_D_BLOW_UP")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_D_DEEP_SEA")
			BREAK
			CASE HEIST_RURAL_BANK
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R1")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R2")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R3")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R4")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R5")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R6")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R7")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R8")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R9")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R10")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R11")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_R12")
			BREAK
			CASE HEIST_AGENCY
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_A1")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_A2")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_A3")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_A4")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_A5")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_A_FIRETRUCK")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_A_HELICOPTER")
			BREAK
			CASE HEIST_FINALE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_F1")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_F2")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_F3")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_F4")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_LBL_F5")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_F_TRAFFCONT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HC_F_HELI")
			BREAK
		ENDSWITCH
		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC Setup_Crew_Data_Slot_For_Crew_Member(SCALEFORM_INDEX siBoardMovie, CrewMember eCrewMember, INT iSlotIndex, INT iDataIndex, INT iCrewDatandex)

	BEGIN_SCALEFORM_MOVIE_METHOD(siBoardMovie, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDataIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrewDatandex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_CREW_MEMBER_NAME_LABEL(eCrewMember))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_CREW_MEMBER_TYPE_LABEL(eCrewMember))
		SWITCH GET_CREW_MEMBER_TYPE(eCrewMember)
			CASE CMT_GUNMAN
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_GUNMAN_STAT_NAME_LABEL(CGS_MAX_HEALTH))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_GUNMAN_STAT_AS_PERCENTAGE(eCrewMember, CGS_MAX_HEALTH))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_GUNMAN_STAT_NAME_LABEL(CGS_ACCURACY))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_GUNMAN_STAT_AS_PERCENTAGE(eCrewMember, CGS_ACCURACY))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_GUNMAN_STAT_NAME_LABEL(CGS_SHOOT_RATE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_GUNMAN_STAT_AS_PERCENTAGE(eCrewMember, CGS_SHOOT_RATE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_GUNMAN_STAT_NAME_LABEL(CGS_WEAPON_CHOICE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_GUNMAN_STAT_AS_PERCENTAGE(eCrewMember, CGS_WEAPON_CHOICE))
			BREAK
			CASE CMT_HACKER
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_HACKER_STAT_NAME_LABEL(CHS_SYS_KNOWLEDGE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_HACKER_STAT_AS_PERCENTAGE(eCrewMember, CHS_SYS_KNOWLEDGE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_HACKER_STAT_NAME_LABEL(CHS_DECRYPT_SKILL))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_HACKER_STAT_AS_PERCENTAGE(eCrewMember, CHS_DECRYPT_SKILL))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_HACKER_STAT_NAME_LABEL(CHS_ACCESS_SPEED))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_HACKER_STAT_AS_PERCENTAGE(eCrewMember, CHS_ACCESS_SPEED))
			BREAK
			CASE CMT_DRIVER
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_DRIVER_STAT_NAME_LABEL(CDS_DRIVING_SKILL))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_DRIVER_STAT_AS_PERCENTAGE(eCrewMember, CDS_DRIVING_SKILL))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_DRIVER_STAT_NAME_LABEL(CDS_COMPOSURE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_DRIVER_STAT_AS_PERCENTAGE(eCrewMember, CDS_COMPOSURE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_DRIVER_STAT_NAME_LABEL(CDS_VEHICLE_CHOICE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_DRIVER_STAT_AS_PERCENTAGE(eCrewMember, CDS_VEHICLE_CHOICE))
			BREAK
		ENDSWITCH
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_CRW_CUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CREW_MEMBER_JOB_CUT(eCrewMember))
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


PROC Setup_Planning_Board_Crew_Slot(PlanningBoard &sPlanningBoard, INT iSlotIndex)
	CPRINTLN(DEBUG_HEIST, "Setting up crew view ", iSlotIndex, ".")

	FLOW_INT_IDS eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
	INT iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
	CrewMemberType eCrewType = g_sHeistChoiceData[iHeistChoice].eCrewType[iSlotIndex]
	
	//Create scaleform crew memeber view.
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "CREATE_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[iSlotIndex].x))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[iSlotIndex].y))
	END_SCALEFORM_MOVIE_METHOD()
	
	Setup_Planning_Board_Labels(sPlanningBoard)
					
	INT iDataIndex = 0
	
	//Has a crew member for this slot already been chosen?
	IF g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iSlotIndex] <> CM_EMPTY
		CPRINTLN(DEBUG_HEIST, "Crew member already selected in this view.")
		CrewMember eSelectedCrew = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iSlotIndex]
		Setup_Crew_Data_Slot_For_Crew_Member(sPlanningBoard.siBoardMovie, eSelectedCrew, iSlotIndex, iDataIndex, ENUM_TO_INT(eSelectedCrew))
	ELSE
		CPRINTLN(DEBUG_HEIST, "Crew member not selected in this view. Repopulating.")
		INT iCrewMemberIndex
		REPEAT CM_MAX_CREW_MEMBERS iCrewMemberIndex
			CrewMember eCrewMem = INT_TO_ENUM(CrewMember, iCrewMemberIndex)
			IF GET_CREW_MEMBER_TYPE(eCrewMem) = eCrewType
				IF IS_HEIST_CREW_MEMBER_UNLOCKED(eCrewMem)
					IF NOT IS_HEIST_CREW_MEMBER_DEAD(eCrewMem)
						IF NOT Is_Crew_Member_Already_Selected_On_Board(sPlanningBoard, eCrewMem)
							IF NOT (eCrewMem = CM_GUNMAN_G_CHEF_UNLOCK AND sPlanningBoard.iLinkedHeist = HEIST_AGENCY)
								CPRINTLN(DEBUG_HEIST, "Adding crew member to view.")
								Setup_Crew_Data_Slot_For_Crew_Member(sPlanningBoard.siBoardMovie, eCrewMem, iSlotIndex, iDataIndex, iCrewMemberIndex)

								sPlanningBoard.eCrewSlotLayout[iDataIndex] = eCrewMem
								iDataIndex++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iSlotIndex] <> CM_EMPTY
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotIndex)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotIndex)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "DISPLAY_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotIndex)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


PROC Setup_Planning_Board_Gameplay_Choice(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Setting up new gameplay choice view.")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "CREATE_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sChoiceBoardPos.x))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sChoiceBoardPos.y))
	END_SCALEFORM_MOVIE_METHOD()
	
	Setup_Planning_Board_Labels(sPlanningBoard)
	
	//Setup the gameplay choice scaleform view.
	SWITCH(sPlanningBoard.iLinkedHeist)
		CASE HEIST_JEWEL
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_JEWEL_HIGH_IMPACT)
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_JEWEL_STEALTH)
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE HEIST_DOCKS
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_DOCKS_BLOW_UP_BOAT)
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_DOCKS_DEEP_SEA)
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE HEIST_AGENCY
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_AGENCY_FIRETRUCK)
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_AGENCY_HELICOPTER)
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE HEIST_FINALE
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_FINALE_TRAFFCONT)
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(HEIST_CHOICE_FINALE_HELI)
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "DISPLAY_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(Is_Heist_Display_Group_Active(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.eChoiceGroup))
	END_SCALEFORM_MOVIE_METHOD()
	
	//Automatically step to the previously selected element.
	INT iHeistChoice = Get_Mission_Flow_Int_Value(Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist))
	IF iHeistChoice != HEIST_CHOICE_EMPTY
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
		END_SCALEFORM_MOVIE_METHOD()
		
		IF iHeistChoice = Get_Choice_In_Heist_Choice_Slot(sPlanningBoard.iLinkedHeist, 0)
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
			END_SCALEFORM_MOVIE_METHOD()
		ELIF iHeistChoice = Get_Choice_In_Heist_Choice_Slot(sPlanningBoard.iLinkedHeist, 1)
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99) //99 is a special value that will focus on nothing.
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC


PROC Setup_Planning_Board_Todo_List(PlanningBoard &sPlanningBoard)
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "CREATE_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sListBoardPos.x))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sListBoardPos.y))
	END_SCALEFORM_MOVIE_METHOD()
	
	INT iHeist = sPlanningBoard.iLinkedHeist
	INT iNoTODOItemsAdded = 0
	
	//Fill todo list data slots for this heist.
	INT index
	REPEAT sPlanningBoard.sData.iNoTodoItems index
		//Work out if this item should have appeared yet.
		IF Is_Heist_Display_Group_Active(iHeist, INT_TO_ENUM(g_eBoardDisplayGroups,sPlanningBoard.sData.eTodoItemAppearGroup[index]))
			//Work out if this item should be ticked.
			INT iTicked = 0
			IF Is_Heist_Display_Group_Active(iHeist, INT_TO_ENUM(g_eBoardDisplayGroups,sPlanningBoard.sData.eTodoItemTickGroup[index]))
				iTicked = 1
			ENDIF
				
			//Add this todo item to the view with correct tick state.
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNoTODOItemsAdded)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTicked)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlanningBoard.sData.tTodoItemText[index])
			END_SCALEFORM_MOVIE_METHOD()
			iNoTODOItemsAdded++
		ENDIF
	ENDREPEAT

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "DISPLAY_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
	END_SCALEFORM_MOVIE_METHOD()
		
	IF Is_Heist_Display_Group_Active(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.eTodoListGroup)
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


PROC Setup_Planning_Board_Static_Items(PlanningBoard &sPlanningBoard)
	INT index
	
	//Get the heist linked to this board.
	INT iHeist = sPlanningBoard.iLinkedHeist
	
	//Display each static item element if its display group is visible.
	REPEAT sPlanningBoard.sData.iNoStaticItems index
	
		//Get the display group for this static item.
		g_eBoardDisplayGroups eDisplayGroup = sPlanningBoard.sData.eStaticItemGroup[index]
		
		//Is this display group is visible?
		BOOL bVisible = IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], ENUM_TO_INT(eDisplayGroup))
		
		//Update the state of this static item in the scaleform.
		IF sPlanningBoard.sData.sStaticItemPos[index].x = 0 AND sPlanningBoard.sData.sStaticItemPos[index].y = 0
			//No custom position defined. Use scaleform movie's default position.
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_HEIST_ASSET")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.sData.iStaticItemAsset[index])
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
			END_SCALEFORM_MOVIE_METHOD()
		ELSE
			//Custom position defined. Draw static item at custom position.
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_HEIST_ASSET")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.sData.iStaticItemAsset[index])
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sStaticItemPos[index].x))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sStaticItemPos[index].y))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDREPEAT

ENDPROC


//*** Configures the settings of a planning board. ***
PROC Setup_Planning_Board(	PlanningBoard &sPlanningBoard,
							INT iLinkedHeist)					
	CPRINTLN(DEBUG_HEIST, "Setting up a new planning board.")

	//Set the linked heist.
	sPlanningBoard.iLinkedHeist = iLinkedHeist
							
	//Set board position.
	VECTOR vPosition = g_sPlanningLocationData[sPlanningBoard.sData.ePlanningLocation].vPlanningLocationBoard
	FLOAT fHeading = g_sPlanningLocationData[sPlanningBoard.sData.ePlanningLocation].fPlanningLocationBoardHeading
	sPlanningBoard.vPosition = vPosition
	sPlanningBoard.fHeading = fHeading
	
	//Calculate base camera values.
	sPlanningBoard.vCamPos = vPosition + <<-sPlanningBoard.sData.fCameraDistance*SIN(360.0-fHeading),-sPlanningBoard.sData.fCameraDistance*COS(360.0-fHeading),0.0>>
	sPlanningBoard.vCamBaseRot = <<-0.85,0,fHeading>>
	SET_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)

	//Setup scaleform view indexes for different board items.
	sPlanningBoard.iGameplayChoiceStartIndex = sPlanningBoard.sData.iMaxCrewSize
	sPlanningBoard.iGameplayChoiceHighlighted = -1
	
	//Initialise board state.
	CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_LOADED)
	CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_CREATED)
	CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)

	sPlanningBoard.eMode = PBM_VIEW
	sPlanningBoard.iContextID = NEW_CONTEXT_INTENTION
ENDPROC


PROC Create_Planning_Board_Pins(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Creating planning board pins.")
	
	INT iPinIndex
	MODEL_NAMES ePinModel
	REPEAT sPlanningBoard.sData.iNoBoardPins iPinIndex
		IF Is_Heist_Display_Group_Active(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.ePinGroup[iPinIndex])
			//Cycle the model we're using.
			SWITCH (iPinIndex%3)
				CASE 0 	ePinModel = PROP_LD_PLANNING_PIN_01
				BREAK
				CASE 1	ePinModel = PROP_LD_PLANNING_PIN_02
				BREAK
				CASE 2	ePinModel = PROP_LD_PLANNING_PIN_03
				BREAK
			ENDSWITCH
			
			FLOAT fRandomXRot = TO_FLOAT(GET_RANDOM_INT_IN_RANGE() - 32767) / 4000
			FLOAT fRandomYRot = TO_FLOAT(GET_RANDOM_INT_IN_RANGE() - 32767) / 4000

			//Create the pin
			VECTOR vPinWorldPos = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sData.sPinPos[iPinIndex])
			sPlanningBoard.oPin[iPinIndex] = CREATE_OBJECT(ePinModel, vPinWorldPos)
			SET_ENTITY_ROTATION(sPlanningBoard.oPin[iPinIndex], <<fRandomXRot, 0.0, sPlanningBoard.fHeading + fRandomYRot>>)
			SET_ENTITY_AS_MISSION_ENTITY(sPlanningBoard.oPin[iPinIndex])
			SET_ENTITY_INVINCIBLE(sPlanningBoard.oPin[iPinIndex], TRUE)
			SET_ENTITY_COLLISION(sPlanningBoard.oPin[iPinIndex], FALSE)
			SET_ENTITY_HAS_GRAVITY(sPlanningBoard.oPin[iPinIndex], FALSE)
			FREEZE_ENTITY_POSITION(sPlanningBoard.oPin[iPinIndex], TRUE)
		ENDIF
	ENDREPEAT
	
	CLEAR_BIT(g_iBitsetHeistBoardAllowCreation, sPlanningBoard.iLinkedHeist)
ENDPROC


//════════════════════════════╡ Board Streaming ╞══════════════════════════════

//*** Requests that assets are streamed in for a planning baord. ***
PROC Request_Planning_Board_Assets(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Requested planning board assets.")
	
	//Check if the SP dataset has loaded, wait if now
	IF NOT HAS_LOADED_SP_DATA_SET()
		EXIT
	ENDIF
	
	//Load board text.
	REQUEST_ADDITIONAL_TEXT(sPlanningBoard.sData.tBoardText, OBJECT_TEXT_SLOT)
		
	//Load audio.
	REQUEST_SCRIPT_AUDIO_BANK("HEIST_BULLETIN_BOARD")
	
	//Load the pin models.
	IF sPlanningBoard.iLinkedHeist != HEIST_DOCKS
		REQUEST_MODEL(PROP_LD_PLANNING_PIN_01)
		REQUEST_MODEL(PROP_LD_PLANNING_PIN_02)
		REQUEST_MODEL(PROP_LD_PLANNING_PIN_03)
	ENDIF

	//Request scaleform movies.
	sPlanningBoard.siBoardMovie = REQUEST_SCALEFORM_MOVIE_INSTANCE(sPlanningBoard.sData.tBoardScaleform)
	CPRINTLN(DEBUG_HEIST, "Scaleform movie set to index ", NATIVE_TO_INT(sPlanningBoard.siBoardMovie), " for ", GET_HEIST_INDEX_DEBUG_STRING(sPlanningBoard.iLinkedHeist), ".")
	
	
	sPlanningBoard.siHelpTextMovie = REQUEST_SCALEFORM_MOVIE_INSTANCE("INSTRUCTIONAL_BUTTONS")
	CPRINTLN(DEBUG_HEIST, "Instructional buttons help text movie requested with ID ", NATIVE_TO_INT(sPlanningBoard.siHelpTextMovie), ".")
	

	SET_BIT(g_iBitsetHeistBoardUpdated, sPlanningBoard.iLinkedHeist)
	SET_BIT(sPlanningBoard.iState, BIT_BOARD_LOADED)
ENDPROC


//*** Checks if assets have streamed in for a planning baord. ***
FUNC BOOL Have_Planning_Board_Assets_Loaded(PlanningBoard &sPlanningBoard)
	//Check if the planning board movie has loaded.
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sPlanningBoard.siBoardMovie)
		RETURN FALSE
	ENDIF
	
	//Check if the help text movie has loaded.
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sPlanningBoard.siHelpTextMovie)
		RETURN FALSE
	ENDIF

	//Check planning board text has loaded.
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(OBJECT_TEXT_SLOT)
		RETURN FALSE
	ENDIF
	
	//Check if the SP dataset has loaded
	IF HAS_LOADED_SP_DATA_SET()
		//Check if the planning board audio has loaded.
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("HEIST_BULLETIN_BOARD")
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	//Check if the pin models have loaded.
	IF sPlanningBoard.iLinkedHeist != HEIST_DOCKS
		IF NOT HAS_MODEL_LOADED(PROP_LD_PLANNING_PIN_01)
		OR NOT HAS_MODEL_LOADED(PROP_LD_PLANNING_PIN_02)
		OR NOT HAS_MODEL_LOADED(PROP_LD_PLANNING_PIN_03)
			RETURN FALSE
		ENDIF
	ENDIF
	
	SET_BIT(g_iBitsetHeistBoardLoaded, sPlanningBoard.iLinkedHeist)
	
	RETURN TRUE
ENDFUNC


//*** Streams out all assets associated with a planning board except the fonts scaleform. ***
PROC Release_Planning_Board_Assets(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Releasing planning board assets for ", GET_HEIST_INDEX_DEBUG_STRING(sPlanningBoard.iLinkedHeist),".")
	
	//Release audio.
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("HEIST_BULLETIN_BOARD")
	
	//Release pin models.
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_PLANNING_PIN_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_PLANNING_PIN_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_PLANNING_PIN_03)
	
	//Release scaleform movies.
	IF sPlanningBoard.siBoardMovie <> NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sPlanningBoard.siBoardMovie)
	ENDIF
	IF sPlanningBoard.siHelpTextMovie <> NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sPlanningBoard.siHelpTextMovie)
			
		CPRINTLN(DEBUG_HEIST, "Released instructional button scaleform.")
	ENDIF
	
	//Release loaded text slot.
	CLEAR_ADDITIONAL_TEXT(OBJECT_TEXT_SLOT, FALSE)

	CLEAR_BIT(g_iBitsetHeistBoardLoaded, sPlanningBoard.iLinkedHeist)
	CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_LOADED)
ENDPROC


//*** Creates an entire planning board in game. ***
PROC Create_Planning_Board(PlanningBoard &sPlanningBoard)
	IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
		CPRINTLN(DEBUG_HEIST, "Creating planning board.")
		
		INT index
		REPEAT sPlanningBoard.sData.iMaxCrewSize index
			Setup_Planning_Board_Crew_Slot(sPlanningBoard, index)
		ENDREPEAT
		
		//Setup the labels for the text that is rendered on the board.
		Setup_Planning_Board_Labels(sPlanningBoard)
		
		//Create scaleform gameplay choice view.
		Setup_Planning_Board_Gameplay_Choice(sPlanningBoard)
		
		//Create Todo List view.
		Setup_Planning_Board_Todo_List(sPlanningBoard)

		//Create planning board static items.
		Setup_Planning_Board_Static_Items(sPlanningBoard)
		
		//Create the 3D pin objects for this board.
		Create_Planning_Board_Pins(sPlanningBoard)

		SET_BIT(sPlanningBoard.iState, BIT_BOARD_CREATED)
	ENDIF
ENDPROC


//══════════════════════════╡ Board Mode Control ╞═════════════════════════════


PROC Clean_Up_Crew_View(PlanningBoard &sPlanningBoard, INT iCrewSlot)
	CPRINTLN(DEBUG_HEIST, "Cleaning up crew view ", iCrewSlot, ".")

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrewSlot)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC Clean_Up_Gameplay_View(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Cleaning up gameplay choice view.")

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC Clean_Up_Todo_View(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Cleaning up gameplay choice view.")

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC Set_Focus_On_Pixel(PlanningBoard &sPlanningBoard, BoardPixel &sBoardPixel, FLOAT fFieldOfView)
	//Save a reference to the pixel we are looking at.
	sPlanningBoard.sFocusPixel.x = sBoardPixel.x
	sPlanningBoard.sFocusPixel.y = sBoardPixel.y
	
	//Update the camera rotation to point at this pixel.
	sPlanningBoard.iDisableFreeLookTime = GET_GAME_TIMER()+ PB_DISABLE_FREE_LOOK_TIME
	VECTOR vNewCamRotation = Get_Board_Camera_Rotation_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sFocusPixel)
	REFOCUS_FIRST_PERSON_CAMERA_WITH_ROTATION(sPlanningBoard.sCam, vNewCamRotation, fFieldOfView)
ENDPROC
	

PROC Set_Focus_On_Default_View(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Focusing on board center position.")
	
	Set_Focus_On_Pixel(sPlanningBoard, sPlanningBoard.sData.sDefaultFocusPos, PB_CAM_FOV_DEFAULT)

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99) //99 is a special value that will focus on nothing.
	END_SCALEFORM_MOVIE_METHOD()
	
	Update_Board_Text(sPlanningBoard)
ENDPROC


PROC Set_Focus_On_Crew_View(PlanningBoard &sPlanningBoard, INT iCrewSlot, CrewMember eCrewToHighlight = CM_EMPTY)
	CPRINTLN(DEBUG_HEIST, "Focusing on crew slot ", iCrewSlot, ".")

	//Check the crew slot is in the valid range.
	IF iCrewSlot < 0 
	OR iCrewSlot >= sPlanningBoard.sData.iMaxCrewSize
		SCRIPT_ASSERT("Set_Focus_On_Crew_View: Tried to focus on a crew slot that was not in the valid range (0 -> iNoCrew-1).")
		EXIT
	ENDIF
	
	//Play crew select dialogue if not already played.
	Play_Board_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, BDS_CREW_SELECT, TRUE)
	
	//Play dialogue if we've changed slot type.
	INT iHeistChoice = Get_Mission_Flow_Int_Value(Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist))
	CrewMemberType eNewSlotType = g_sHeistChoiceData[iHeistChoice].eCrewType[iCrewSlot]
	SWITCH eNewSlotType
		CASE CMT_GUNMAN
			Play_Heist_Choice_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, iHeistChoice, HCDS_GUNMAN)
		BREAK
		CASE CMT_HACKER
			Play_Heist_Choice_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, iHeistChoice, HCDS_HACKER)
		BREAK
		CASE CMT_DRIVER
			Play_Heist_Choice_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, iHeistChoice, HCDS_DRIVER)
		BREAK
	ENDSWITCH
	
	Clean_Up_Crew_View(sPlanningBoard, sPlanningBoard.iCrewSlotSelected)
	Setup_Planning_Board_Crew_Slot(sPlanningBoard, sPlanningBoard.iCrewSlotSelected)
	
	sPlanningBoard.iSelectedView = iCrewSlot
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrewSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrewSlot)
	END_SCALEFORM_MOVIE_METHOD()
	
	//Update camera focus and zoom.
	Set_Focus_On_Pixel(sPlanningBoard, sPlanningBoard.sData.sCrewBoardPos[iCrewSlot], sPlanningBoard.sData.fChoiceZoom)

	//Automatically step to the previously selected element.
	IF eCrewToHighlight != CM_EMPTY
		INT iCurrentElement = 0
		WHILE sPlanningBoard.eCrewSlotLayout[iCurrentElement] != eCrewToHighlight
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
			END_SCALEFORM_MOVIE_METHOD()
			iCurrentElement++
			IF iCurrentElement > 7
				SCRIPT_ASSERT("Set_Focus_On_Crew_View: Tried to auto scroll to a crew member that was out of the valid range.")
				EXIT
			ENDIF
		ENDWHILE
	ENDIF
	
	//Refresh the help text.
	Set_Board_Help_Hidden(sPlanningBoard, FALSE)
	Update_Board_Text(sPlanningBoard)
ENDPROC


PROC Set_Focus_On_Choice_View(PlanningBoard &sPlanningBoard)
	Clean_Up_Gameplay_View(sPlanningBoard)
	Setup_Planning_Board_Gameplay_Choice(sPlanningBoard)

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
	END_SCALEFORM_MOVIE_METHOD()
	
	INT iHeistChoice = Get_Mission_Flow_Int_Value(Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist))
	
	//Automatically step to the previously selected element.
	IF iHeistChoice != HEIST_CHOICE_EMPTY
		IF iHeistChoice = Get_Choice_In_Heist_Choice_Slot(sPlanningBoard.iLinkedHeist, 0)
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
			END_SCALEFORM_MOVIE_METHOD()
		ELIF iHeistChoice = Get_Choice_In_Heist_Choice_Slot(sPlanningBoard.iLinkedHeist, 1)
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
		
	//Update camera focus and zoom.
	BoardPixel sChoiceFocus
	sChoiceFocus.x = sPlanningBoard.sData.sChoiceBoardPos.x + sPlanningBoard.sData.sChoiceCameraOffset.x
	sChoiceFocus.y = sPlanningBoard.sData.sChoiceBoardPos.y + sPlanningBoard.sData.sChoiceCameraOffset.y 
	Set_Focus_On_Pixel(sPlanningBoard, sChoiceFocus, sPlanningBoard.sData.fChoiceZoom)
	
	//Play choice select dialogue if not already played.
	Play_Board_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, BDS_CHOICE_SELECT, TRUE)
	
	//Refresh the help text.
	Set_Board_Help_Hidden(sPlanningBoard, FALSE)
	Update_Board_Text(sPlanningBoard)
ENDPROC


PROC Set_Focus_On_POI(PlanningBoard &sPlanningBoard, INT iPOIIndex)
	CPRINTLN(DEBUG_HEIST, "Focusing on POI ", iPOIIndex, ".")

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99)
	END_SCALEFORM_MOVIE_METHOD()
			
	//Update camera focus and zoom.
	Set_Focus_On_Pixel(sPlanningBoard, sPlanningBoard.sData.sPOIPos[iPOIIndex], sPlanningBoard.sData.fPOIZoom)
	
	//Play point of interest dialogue.
	Play_Board_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, INT_TO_ENUM(BoardDialogueSlot, iPOIIndex + ENUM_TO_INT(BDS_POI_1)), TRUE)

	sPlanningBoard.iPOITimer = -1
ENDPROC


PROC Clear_Board_Modes(PlanningBoard &sPlanningBoard)
	SWITCH(sPlanningBoard.eMode)
		CASE PBM_POI_OVERVIEW
		CASE PBM_CREW_SELECT
		CASE PBM_CHOICE_SELECT
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99) //99 is a special value that will focus on nothing.
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH

	CLEAR_HELP(FALSE)
ENDPROC


PROC Update_Instructional_Buttons(PlanningBoard &sPlanningBoard)

	STRING eSelectAllInput = 	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_GENERIC_ALL)
	STRING eSelectUDInput = 	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_GENERIC_UD)
	STRING eZoomInput = 		GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
	STRING eLookInput = 		GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK)
	STRING eAcceptInput = 		GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	STRING eExitInput = 		GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		eSelectAllInput = 	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_ALL)
		eSelectUDInput = 	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD)
		eZoomInput = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_CURSOR_SCROLL)
	ENDIF

	BOOL bNoExit = Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, sPlanningBoard.iLinkedHeist)

	BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_MAX_WIDTH")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.6)
	END_SCALEFORM_MOVIE_METHOD()

	SWITCH(sPlanningBoard.eMode)
		
		CASE PBM_VIEW
			CPRINTLN(DEBUG_HEIST, "Updating instructional buttons for VIEW mode.")
			
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				IF bNoExit
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eZoomInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_ZOOM")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eZoomInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_ZOOM")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eExitInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_EXIT")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "DRAW_INSTRUCTIONAL_BUTTONS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		BREAK
		
		CASE PBM_POI_OVERVIEW
			CPRINTLN(DEBUG_HEIST, "Updating instructional buttons for POI_OVERVIEW mode.")

			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "DRAW_INSTRUCTIONAL_BUTTONS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE PBM_CREW_SELECT
			CPRINTLN(DEBUG_HEIST, "Updating instructional buttons for CREW_SELECT mode.")

			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
				END_SCALEFORM_MOVIE_METHOD()
				
				IF sPlanningBoard.iLinkedHeist = HEIST_RURAL_BANK
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eSelectAllInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_SELCT")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eAcceptInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_TRIG")
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eSelectAllInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_SELCT")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eExitInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_UNDO")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eAcceptInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_TRIG")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "DRAW_INSTRUCTIONAL_BUTTONS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		BREAK
		
		CASE PBM_CHOICE_SELECT
			CPRINTLN(DEBUG_HEIST, "Updating instructional buttons for CHOICE_SELECT mode.")
			
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eSelectUDInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_SELCT")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eSelectUDInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_SELCT")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eAcceptInput)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_TRIG")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF

				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "DRAW_INSTRUCTIONAL_BUTTONS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		BREAK
		
		CASE PBM_CONFIRM
			CPRINTLN(DEBUG_HEIST, "Updating instructional buttons for CONFIRM mode.")
			
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT_EMPTY")
				END_SCALEFORM_MOVIE_METHOD()

				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eZoomInput)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_ZOOM")
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eLookInput)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_LOOK")
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eExitInput)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_UNDO")
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(eAcceptInput)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PB_H_CONF")
				END_SCALEFORM_MOVIE_METHOD()
				
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "DRAW_INSTRUCTIONAL_BUTTONS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC Initialise_Board_Mode(PlanningBoard &sPlanningBoard, g_eBoardModes ePBM, CrewMember eCrewToHighlight = CM_EMPTY)
	SWITCH(ePBM)
		CASE PBM_VIEW
			CPRINTLN(DEBUG_HEIST, "Board switching to VIEW mode.")
			
			Set_Focus_On_Default_View(sPlanningBoard)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON)
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
		BREAK
		
		CASE PBM_POI_OVERVIEW
			CPRINTLN(DEBUG_HEIST, "Board switching to POI_OVERVIEW mode.")
			
			Set_Focus_On_Default_View(sPlanningBoard)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON)
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
			sPlanningBoard.iPOITimer = -1
		BREAK
		
		CASE PBM_CREW_SELECT
			CPRINTLN(DEBUG_HEIST, "Board switching to CREW_SELECT mode.")
		
			Set_Focus_On_Crew_View(sPlanningBoard, sPlanningBoard.iCrewSlotSelected, eCrewToHighlight)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON)
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)

			sPlanningBoard.iChoiceNoInputTimer = GET_GAME_TIMER()
		BREAK
		
		CASE PBM_CHOICE_SELECT
			CPRINTLN(DEBUG_HEIST, "Board switching to CHOICE_SELECT mode.")
			
			Set_Focus_On_Choice_View(sPlanningBoard)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON)
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
			sPlanningBoard.iChoiceNoInputTimer = GET_GAME_TIMER()
		BREAK
		
		CASE PBM_CONFIRM
			CPRINTLN(DEBUG_HEIST, "Board switching to CONFIRM mode.")

			//Clean up old crew select help.
			REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tCrewHelp[0])
			REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tCrewHelp[1])
			g_savedGlobals.sHeistData.bCrewHelpDisplayed[sPlanningBoard.iLinkedHeist] = TRUE
			CLEAR_HELP(FALSE)
			
			Set_Board_Help_Hidden(sPlanningBoard, FALSE)
			
			Set_Focus_On_Default_View(sPlanningBoard)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
			sPlanningBoard.iChoiceNoInputTimer = GET_GAME_TIMER()
		BREAK
	ENDSWITCH
	
	Update_Instructional_Buttons(sPlanningBoard)
ENDPROC


//*** Handles switching between board modes. ***
PROC Switch_Board_Mode(PlanningBoard &sPlanningBoard, g_eBoardModes ePBM, CrewMember eCrewToHighlight = CM_EMPTY)
	
	//Clean up the previous mode.
	IF ePBM <> sPlanningBoard.eMode
		IF sPlanningBoard.sriGameplayChoice = NULL
			Clear_Board_Modes(sPlanningBoard)
			sPlanningBoard.eMode = ePBM
			
			//Initialise the new mode.
			Initialise_Board_Mode(sPlanningBoard, ePBM, eCrewToHighlight)
		#IF IS_DEBUG_BUILD
		ELIF GET_FRAME_COUNT() % 240 = 0
			CPRINTLN(DEBUG_HEIST, "Board waiting for gameplay choice to return before swiching mode.")
		#ENDIF
		ENDIF
	ENDIF
ENDPROC


//*** Sets-up the board view. ***
PROC Initalise_Board_View(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Initialising planning board view.")
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	#ENDIF
	
	sPlanningBoard.iCancelOutCooldownTimer = 0
	sPlanningBoard.iCrewDialoguePlayedBitset = 0
	SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
	Set_Board_Help_Hidden(sPlanningBoard, FALSE)

	//Configure player.
	IF NOT IS_CUTSCENE_PLAYING()
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			// Sober the player up
			IF g_isPlayerDrunk = TRUE
				Make_Ped_Sober(PLAYER_PED_ID())
			ENDIF
			
			// other setup
			VECTOR vViewPos = sPlanningBoard.vPosition
			vViewPos += <<2.9*SIN(180-sPlanningBoard.fHeading),2.9*COS(180-sPlanningBoard.fHeading),0.5>>
			GET_GROUND_Z_FOR_3D_COORD(vViewPos,vViewPos.Z)
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH|SPC_AMBIENT_SCRIPT|SPC_CLEAR_TASKS)
			SET_ENTITY_COORDS(PLAYER_PED_ID(),vViewPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlanningBoard.fHeading)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_HEIST, "Player configured for planning board view.")
		#ENDIF
		SET_BIT(sPlanningBoard.iState, BIT_BOARD_PLAYER_SETUP)
	ENDIF
	
	//Deactivate hud and phone.
	DISPLAY_RADAR(FALSE)
	DISABLE_CELLPHONE(TRUE)
	DISABLE_SELECTOR()
	
	//Pause the feed queue.
	THEFEED_PAUSE()
	
	//Clear area to ensure explosives are gone.
	CLEAR_AREA(sPlanningBoard.vPosition, 5.0, TRUE, TRUE)
	REMOVE_PARTICLE_FX_IN_RANGE(sPlanningBoard.vPosition, 5.0)
	
	CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(TRUE)
	
	//Move the save game spinner out of the way of the help text.
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT,UI_ALIGN_BOTTOM)
	SET_HUD_COMPONENT_POSITION(NEW_HUD_SAVING_GAME, 0.612, 0.818)
	RESET_SCRIPT_GFX_ALIGN()
	
	//Setup our first person camera.
	INIT_FIRST_PERSON_CAMERA(	sPlanningBoard.sCam, 
								sPlanningBoard.vCamPos,
								sPlanningBoard.vCamBaseRot,
								PB_CAM_FOV_DEFAULT,
								PB_CAM_LOOK_X,
								PB_CAM_LOOK_Y,
								PB_CAM_LOOK_ROLL,
								sPlanningBoard.sData.fMaxZoom)

	//Get the planning location room name.
	INTERIOR_INSTANCE_INDEX theInterior = GET_INTERIOR_AT_COORDS(sPlanningBoard.vPosition)
	IF NATIVE_TO_INT(theInterior) <> 0
		PIN_INTERIOR_IN_MEMORY(theInterior)
	ENDIF
	
	//Ensure the camera cuts into the interior correctly.
	SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME(g_sPlanningLocationData[sPlanningBoard.sData.ePlanningLocation].tPlanningLocationRoomName)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(GET_KEY_FOR_ENTITY_IN_ROOM(PLAYER_PED_ID()))
	ENDIF
	
	//Give planning board access to the help queue system.
	PAUSE_FLOW_HELP_QUEUE(FALSE)
	PRIVATE_Clear_Flow_Help_Queue()
	
	//Give script access to audio commands.
	REGISTER_SCRIPT_WITH_AUDIO()
	
	//Turn off PC clickable instructional buttons. (2285274)
	IF IS_PC_VERSION()
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siHelpTextMovie, "TOGGLE_MOUSE_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	//Store an enum to use to look for control scheme changes while the board view is active
	sPlanningBoard.eLookInputLastFrame = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL)
	
	SET_BIT(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
	g_bHeistBoardViewActive = TRUE
	
	//Initialise the current board mode.
	Initialise_Board_Mode(sPlanningBoard, sPlanningBoard.eMode)
ENDPROC


//*** Cleans up and exits a board view. ***
PROC Exit_Board_View(PlanningBoard &sPlanningBoard, BOOL bDoingExitCutscene)

	CPRINTLN(DEBUG_HEIST, "Leaving planning board view.")
	
	sPlanningBoard.iCancelOutCooldownTimer = 0
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		//Configure player.
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		IF NOT bDoingExitCutscene
			// not playing cut-scene, so move the player to suitable exit posiiton
		 	VECTOR vViewPos = sPlanningBoard.vPosition
			vViewPos += <<1.0*SIN(180-sPlanningBoard.fHeading),1.0*COS(180-sPlanningBoard.fHeading),0.0>>
			GET_GROUND_Z_FOR_3D_COORD(vViewPos,vViewPos.Z)
			SET_ENTITY_COORDS(PLAYER_PED_ID(),vViewPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlanningBoard.fHeading)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
			CPRINTLN(DEBUG_HEIST, "Player configured for leaving planning board view.")
		ELSE
			CPRINTLN(DEBUG_HEIST, "Doing board exit cut-scene, leaving player where they are.")
		ENDIF
	ENDIF
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE,SPC_REENABLE_CONTROL_ON_DEATH|SPC_AMBIENT_SCRIPT|SPC_CLEAR_TASKS)
	ENDIF
	
	//Activate hud and phone.
	DISPLAY_RADAR(TRUE)
	DISABLE_CELLPHONE(FALSE)
	ENABLE_SELECTOR()
	
	//Unpause the feed queue.
	THEFEED_RESUME()
	
	//Reset the savegame spinner to its default position.
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SAVING_GAME)
	
	CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(FALSE)
		
	//Deactivate our first person camera.
	CLEAR_FIRST_PERSON_CAMERA(sPlanningBoard.sCam)
	
	//Clear help text.
	REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tChoiceHelp[0])
	REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tChoiceHelp[1])
	REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tCrewHelp[0])
	REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tCrewHelp[1])
	CLEAR_HELP()
	WHILE IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
		WAIT(0)
	ENDWHILE
	
	INTERIOR_INSTANCE_INDEX theInterior = GET_INTERIOR_AT_COORDS(sPlanningBoard.vPosition)
	IF NATIVE_TO_INT(theInterior) <> 0
		UNPIN_INTERIOR(theInterior)
	ENDIF
	
	//Release script from audio control.
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	//Clear the current board mode.
	Clear_Board_Modes(sPlanningBoard)
	
	CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_PLAYER_SETUP)
	CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
	g_bHeistBoardViewActive = FALSE
ENDPROC


//*** Deletes all pin objects on a planning board. ***
PROC Delete_Planning_Board_Pins(PlanningBoard &sPlanningBoard)
	CPRINTLN(DEBUG_HEIST, "Deleting planning board pins.")
	
	INT iPinIndex
	REPEAT sPlanningBoard.sData.iNoBoardPins iPinIndex
		IF DOES_ENTITY_EXIST(sPlanningBoard.oPin[iPinIndex])
			DELETE_OBJECT(sPlanningBoard.oPin[iPinIndex])
		ENDIF
	ENDREPEAT	
ENDPROC


//*** Deletes all objects that make up a planning board in game. ***
PROC Delete_Planning_Board(PlanningBoard &sPlanningBoard)
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
		CPRINTLN(DEBUG_HEIST, "Deleting planning board.")
		
		//If we're currently on the planning board view, exit out cleanly before deleting anything.
		IF NOT IS_CUTSCENE_PLAYING()
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				Exit_Board_View(sPlanningBoard, FALSE)
			ENDIF
		ENDIF
		
		//Delete all the pins.
		Delete_Planning_Board_Pins(sPlanningBoard)

		CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_CREATED)
		CLEAR_BIT(g_iBitsetHeistBoardPinned, sPlanningBoard.iLinkedHeist)
	ENDIF
ENDPROC


//*** Completely deletes a planning board and streams out all assets associated with it. ***
FUNC BOOL Cleanup_Planning_Board(PlanningBoard &sPlanningBoard)
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
		Delete_Planning_Board(sPlanningBoard)
	ENDIF
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_LOADED)
		Release_Planning_Board_Assets(sPlanningBoard)
	ENDIF
	RETURN TRUE
ENDFUNC


//*** Look for requests to refresh the elements that are displaying on the board. ***
PROC Update_Board_Item_Refreshing(PlanningBoard &sPlanningBoard)
	INT index
	
	IF IS_BIT_SET(g_iBitsetHeistBoardUpdated, sPlanningBoard.iLinkedHeist)
		SET_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_STATIC_ITEMS)
		SET_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_PINS)
		SET_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_TODO_LIST)
		CLEAR_BIT(g_iBitsetHeistBoardUpdated, sPlanningBoard.iLinkedHeist)
	ENDIF
	
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_REFRESH_STATIC_ITEMS)
		CPRINTLN(DEBUG_HEIST, "Refreshing board static items.")
		REPEAT sPlanningBoard.sData.iNoStaticItems index
			g_eBoardDisplayGroups eDisplayGroup = sPlanningBoard.sData.eStaticItemGroup[index]
			BOOL bVisible = Is_Heist_Display_Group_Active(sPlanningBoard.iLinkedHeist, eDisplayGroup)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_HEIST_ASSET")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.sData.iStaticItemAsset[index])
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()
		ENDREPEAT
		CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_STATIC_ITEMS)
	ENDIF
	
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_REFRESH_PINS)
		CPRINTLN(DEBUG_HEIST, "Refreshing board pins.")
		MODEL_NAMES ePinModel
		REPEAT sPlanningBoard.sData.iNoBoardPins index
			IF Is_Heist_Display_Group_Active(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.ePinGroup[index])
				IF NOT DOES_ENTITY_EXIST(sPlanningBoard.oPin[index])
					SWITCH (index%3)
						CASE 0 	ePinModel = PROP_LD_PLANNING_PIN_01
						BREAK
						CASE 1	ePinModel = PROP_LD_PLANNING_PIN_02
						BREAK
						CASE 2	ePinModel = PROP_LD_PLANNING_PIN_03
						BREAK
					ENDSWITCH
				
					FLOAT fRandomXRot = TO_FLOAT(GET_RANDOM_INT_IN_RANGE() - 32767) / 4000
					FLOAT fRandomYRot = TO_FLOAT(GET_RANDOM_INT_IN_RANGE() - 32767) / 4000

					//Create a new pin
					VECTOR vPinWorldPos = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sData.sPinPos[index])
					sPlanningBoard.oPin[index] = CREATE_OBJECT(ePinModel, vPinWorldPos)
					SET_ENTITY_ROTATION(sPlanningBoard.oPin[index], <<fRandomXRot, 0.0, sPlanningBoard.fHeading + fRandomYRot>>)
					SET_ENTITY_AS_MISSION_ENTITY(sPlanningBoard.oPin[index])
					SET_ENTITY_INVINCIBLE(sPlanningBoard.oPin[index], TRUE)
					SET_ENTITY_COLLISION(sPlanningBoard.oPin[index], FALSE)
					SET_ENTITY_HAS_GRAVITY(sPlanningBoard.oPin[index], FALSE)
					FREEZE_ENTITY_POSITION(sPlanningBoard.oPin[index], TRUE)
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(sPlanningBoard.oPin[index])
					DELETE_OBJECT(sPlanningBoard.oPin[index])
				ENDIF
			ENDIF
		ENDREPEAT
		CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_PINS)
	ENDIF
	
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_REFRESH_TODO_LIST)
		CPRINTLN(DEBUG_HEIST, "Refreshing board TODO list.")
		Clean_Up_Todo_View(sPlanningBoard)
		Setup_Planning_Board_Labels(sPlanningBoard)
		Setup_Planning_Board_Todo_List(sPlanningBoard)
		CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_TODO_LIST)
	ENDIF
	
ENDPROC


//*** Displays the boards scaleform movie. ***
PROC Draw_Board(PlanningBoard &sPlanningBoard)
	
	//Only draw the board after the first display group has been activated.
	IF Is_Heist_Display_Group_Active(sPlanningBoard.iLinkedHeist, PBDG_0)
	
		//Handle board being requested to update the displaying of items on it.
		Update_Board_Item_Refreshing(sPlanningBoard)
	
		//Draw the board movie.
		FLOAT fOffsetX = -sPlanningBoard.sData.fScaleformWidth*0.5
		FLOAT fOffsetY = sPlanningBoard.sData.fScaleformHeight*0.5
		
		DRAW_SCALEFORM_MOVIE_3D(sPlanningBoard.siBoardMovie, 
								sPlanningBoard.vPosition + <<fOffsetX*SIN(90-sPlanningBoard.fHeading),fOffsetX*COS(90-sPlanningBoard.fHeading),fOffsetY>>, 
								<<180.0, 0.0, sPlanningBoard.fHeading>>, 
								<<1.0, 1.0, 1.0>>, 
								<<sPlanningBoard.sData.fScaleformXScale, sPlanningBoard.sData.fScaleformYScale,1.0>>)

		#IF IS_DEBUG_BUILD
			//Toggle on and off debug drawing when needed.
			IF sPlanningBoard.sData.bDebugDrawCenter
			OR sPlanningBoard.sData.bDebugDrawDefaultFocus
			OR sPlanningBoard.sData.bDebugDrawStickFocus
			OR sPlanningBoard.sData.bDebugDrawGrid
			OR sPlanningBoard.sData.bDebugDrawPins
			OR sPlanningBoard.sData.bDebugDrawPOIs
			OR sPlanningBoard.sData.bDebugDrawStaticItems
				IF NOT sPlanningBoard.sData.bDebugDrawingEnabled
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					sPlanningBoard.sData.bDebugDrawingEnabled = TRUE
				ENDIF
			ELSE
				IF sPlanningBoard.sData.bDebugDrawingEnabled
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
					sPlanningBoard.sData.bDebugDrawingEnabled = FALSE
				ENDIF
			ENDIF
			
			IF sPlanningBoard.sData.bDebugDrawCenter
				DRAW_DEBUG_SPHERE(sPlanningBoard.vPosition, 0.035, 0, 255, 0, 255)
				DRAW_DEBUG_LINE(sPlanningBoard.vPosition, sPlanningBoard.vPosition + <<-0.25*SIN(360 - sPlanningBoard.fHeading),-0.25*COS(360 - sPlanningBoard.fHeading),0.0>>, 0, 255, 0, 255) 
			ENDIF

			IF sPlanningBoard.sData.bDebugDrawDefaultFocus
				VECTOR vDefaultFocusWorldPos = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sData.sDefaultFocusPos)
				DRAW_DEBUG_TEXT("Default Focus", vDefaultFocusWorldPos)
				DRAW_DEBUG_SPHERE(vDefaultFocusWorldPos, 0.01, 255, 0, 0)
			ENDIF
			
			IF sPlanningBoard.sData.bDebugDrawStickFocus
				BoardPixel sFocusPixel = Get_Board_Focus_Pixel_From_Stick_Positions(sPlanningBoard)
				VECTOR vStickFocusWorldPos = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sFocusPixel)
				DRAW_DEBUG_TEXT("Stick Focus", vStickFocusWorldPos)
				DRAW_DEBUG_SPHERE(vStickFocusWorldPos, 0.01, 75, 100, 200)
			ENDIF
				
			IF sPlanningBoard.sData.bDebugDrawPins
				INT iPinIndex
				REPEAT sPlanningBoard.sData.iNoBoardPins iPinIndex
					TEXT_LABEL_7 txtPin = "Pin"
					txtPin += iPinIndex
					VECTOR vPinWorldPosition = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sData.sPinPos[iPinIndex])
					DRAW_DEBUG_TEXT(txtPin, vPinWorldPosition)
					DRAW_DEBUG_SPHERE(vPinWorldPosition, 0.01, 255, 0, 0)
				ENDREPEAT
			ENDIF
			
			IF sPlanningBoard.sData.bDebugDrawPOIs
				INT iPOIIndex
				REPEAT sPlanningBoard.sData.iNoPointsOfInterest iPOIIndex
					TEXT_LABEL_7 txtPOI = "POI"
					txtPOI += iPOIIndex
					VECTOR vPOIWorldPosition = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sData.sPOIPos[iPOIIndex])
					DRAW_DEBUG_TEXT(txtPOI, vPOIWorldPosition)
					DRAW_DEBUG_SPHERE(vPOIWorldPosition, 0.025, 255, 255, 0)
				ENDREPEAT
			ENDIF
			
			IF sPlanningBoard.sData.bDebugDrawStaticItems
				INT iStaticItemIndex
				REPEAT sPlanningBoard.sData.iNoStaticItems iStaticItemIndex
					IF NOT (sPlanningBoard.sData.sStaticItemPos[iStaticItemIndex].x = 0 AND sPlanningBoard.sData.sStaticItemPos[iStaticItemIndex].y = 0)
						TEXT_LABEL_7 txtStaticItem = "SI"
						txtStaticItem += iStaticItemIndex
						VECTOR vStaticItemWorldPosition = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPlanningBoard.sData.sStaticItemPos[iStaticItemIndex])
						DRAW_DEBUG_TEXT(txtStaticItem, vStaticItemWorldPosition)
						DRAW_DEBUG_SPHERE(vStaticItemWorldPosition, 0.025, 255, 255, 0)
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF sPlanningBoard.sData.bDebugDrawGrid
				INT x, y, iLineAlpha
				BoardPixel sPixelA, sPixelB
				TEXT_LABEL_7 tGridCoord = ""
				INT iMaxX = sPlanningBoard.sData.iScaleformPixelWidth
				INT iMaxY = sPlanningBoard.sData.iScaleformPixelHeight
				//Draw X grid.
				sPixelA.y = 0
				sPixelB.y = iMaxY
				FOR x = 0 TO iMaxX STEP 25
					sPixelA.x = x
					sPixelB.x = x
					iLineAlpha = 5
					IF (x%100 = 0) 	
						iLineAlpha = 160
					ELIF (x%50 = 0) 
						iLineAlpha = 40
					ENDIF
					VECTOR vLineStart = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixelA)
					IF iLineAlpha > 40
						tGridCoord = ""
						tGridCoord += x
						DRAW_DEBUG_TEXT(tGridCoord, vLineStart, 0, 255, 0, 180)
					ENDIF
					DRAW_DEBUG_LINE(vLineStart, Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixelB),
									0, 255, 0, iLineAlpha)
				ENDFOR
				//Draw Y grid.
				sPixelA.x = 0
				sPixelB.x = iMaxX
				FOR y = 0 TO iMaxY STEP 25
					sPixelA.y = y
					sPixelB.y = y
					iLineAlpha = 5
					IF (y%100 = 0) 	
						iLineAlpha = 160
					ELIF (y%50 = 0) 
						iLineAlpha = 40
					ENDIF
					VECTOR vLineStart = Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixelA)
					IF iLineAlpha > 40
						tGridCoord = ""
						tGridCoord += y
						DRAW_DEBUG_TEXT(tGridCoord, vLineStart, 0, 255, 0, 180)
					ENDIF
					DRAW_DEBUG_LINE(vLineStart, Get_World_Board_Position_For_Scaleform_Pixel(sPlanningBoard, sPixelB),
									0, 255, 0, iLineAlpha)
				ENDFOR
			ENDIF
		#ENDIF
	
	ENDIF
	
ENDPROC


//*** Handles the streaming and creation/deletion of a planning board. ***
PROC Manage_Board_Streaming(PlanningBoard &sPlanningBoard, FLOAT fLoadDist, FLOAT fUnloadDist)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		FLOAT fDistSquaredFromBoard = VDIST2(sPlanningBoard.vPosition, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
		//Check to see if the board needs to be streamed in and created.
		IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
			Draw_Board(sPlanningBoard)
		ELSE
			IF fDistSquaredFromBoard < (fLoadDist*fLoadDist) OR IS_BIT_SET(g_iBitsetHeistBoardPinned, sPlanningBoard.iLinkedHeist)
				IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_LOADED)
					IF Have_Planning_Board_Assets_Loaded(sPlanningBoard)
						IF IS_BIT_SET(g_iBitsetHeistBoardPinned, sPlanningBoard.iLinkedHeist) 
							IF IS_BIT_SET(g_iBitsetHeistBoardAllowCreation, sPlanningBoard.iLinkedHeist)
								Create_Planning_Board(sPlanningBoard)
							ENDIF
						ELSE
							Create_Planning_Board(sPlanningBoard)
						ENDIF
					ENDIF
				ELSE
					Request_Planning_Board_Assets(sPlanningBoard)
				ENDIF
			ENDIF
		ENDIF

		//Check to see if the board needs to be deleted and streamed out.
		IF fDistSquaredFromBoard > (fUnloadDist*fUnloadDist) AND NOT IS_BIT_SET(g_iBitsetHeistBoardPinned, sPlanningBoard.iLinkedHeist)
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_LOADED)
				IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
					Delete_Planning_Board(sPlanningBoard)
				ENDIF
				Release_Planning_Board_Assets(sPlanningBoard)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//════════════════════════╡ Board Crew Selection Control ╞═══════════════════════════

//*** Manages player input for the scaleform board movie. ***
PROC Manage_Board_Input(PlanningBoard &sPlanningBoard)
	++sPlanningBoard.iCancelOutCooldownTimer
	
	// Refresh the instructional buttons if the control scheme changes.
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		Update_Instructional_Buttons(sPlanningBoard)
	ENDIF
	
	FLOW_INT_IDS eHeistChoiceFlowInt
	INT iHeistChoice
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
				IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
					IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
						CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
						Update_Instructional_Buttons(sPlanningBoard)
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
			ENDIF
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
			ENDIF
		ENDIF
	#ENDIF
	
	//Allow skipping of dialogue with the X and O buttons.
	IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, PB_CONTROL_SELECT) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_UNDO)
		//M&K options
		OR ( IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND 
			(IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, PB_CONTROL_SELECT_M) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_UNDO_M)))
		#IF IS_DEBUG_BUILD
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		#ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP)
				SET_BIT(sPlanningBoard.iState, BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE)
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
				CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
			ENDIF
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_ALLOW_DIALOGUE_MULTI_SKIP)
		ENDIF
	ENDIF
	
	//Get analogue stick positions.
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(sPlanningBoard.iStickPosition[PB_LEFT_X],
									 	 sPlanningBoard.iStickPosition[PB_LEFT_Y],
									 	 sPlanningBoard.iStickPosition[PB_RIGHT_X],
									 	 sPlanningBoard.iStickPosition[PB_RIGHT_Y])
									
	// Make PC controls less sensitive
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		sPlanningBoard.iStickPosition[PB_RIGHT_X] /= 10
		sPlanningBoard.iStickPosition[PB_RIGHT_Y] /= 10
		
		//Clamp PC controls to min/max stick values
		sPlanningBoard.iStickPosition[PB_RIGHT_X] = CLAMP_INT(sPlanningBoard.iStickPosition[PB_RIGHT_X] + sPlanningBoard.sCam.iOldStickRX,-127,127)
		sPlanningBoard.iStickPosition[PB_RIGHT_Y] = CLAMP_INT(sPlanningBoard.iStickPosition[PB_RIGHT_Y] + sPlanningBoard.sCam.iOldStickRY,-127,127)

	ENDIF
		
	//Store old stick/position values
	sPlanningBoard.sCam.iOldStickRX = sPlanningBoard.iStickPosition[PB_RIGHT_X]
	sPlanningBoard.sCam.iOldStickRY = sPlanningBoard.iStickPosition[PB_RIGHT_Y]

	#IF IS_DEBUG_BUILD	
		sPlanningBoard.sCam.bZoomEnabled = IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
		sPlanningBoard.sCam.bLookEnabled = IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON)
	#ENDIF			
	
	IF IS_LOOK_INVERTED()
		sPlanningBoard.iStickPosition[PB_RIGHT_Y] = -sPlanningBoard.iStickPosition[PB_RIGHT_Y]
	ENDIF
		
	//Check if free look should be disabled for now
	IF sPlanningBoard.iDisableFreeLookTime > GET_GAME_TIMER()
		sPlanningBoard.iStickPosition[PB_RIGHT_X] = 0
		sPlanningBoard.iStickPosition[PB_RIGHT_Y] = 0
	ENDIF
	
	//Lock zoom control on entering view mode so the camera
	//doesn't jump if the player was walking forward.
	IF sPlanningBoard.eMode = PBM_VIEW
	OR sPlanningBoard.eMode = PBM_CONFIRM
		IF sPlanningBoard.iCancelOutCooldownTimer > WAIT_FRAMES_FOR_BOARD_EXIT 
		//AND sPlanningBoard.iStickPosition[PB_LEFT_Y] < 0
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
		ELSE
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON)
		ENDIF
	ENDIF

	//Are we waiting for dialogue to finish?
	IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
		IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED)
			
			//Selection events.
			IF (GET_GAME_TIMER() - sPlanningBoard.iSelectionChangeTimer) > PB_SELECTION_CHANGE_TIME
			
				IF sPlanningBoard.eMode = PBM_CREW_SELECT
				OR sPlanningBoard.eMode = PBM_CHOICE_SELECT
				OR sPlanningBoard.eMode = PBM_CONFIRM
					IF (GET_GAME_TIMER() - sPlanningBoard.iChoiceNoInputTimer) > PB_NO_INPUT_COMMENT_TIME
						CPRINTLN(DEBUG_HEIST, "Board input inactive for too long. Playing \"Hurry up!\" dialogue.")
						Queue_Board_Conversation(sPlanningBoard, sPlanningBoard.sData.tBoardDialogueSlot, sPlanningBoard.sData.tBoardDialogueLabels[BDS_TOO_SLOW], TRUE)
						sPlanningBoard.iChoiceNoInputTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0, 8000)
					ENDIF
				ENDIF
			
				//Directional input events.
				IF sPlanningBoard.eMode = PBM_CREW_SELECT
					IF sPlanningBoard.iStickPosition[PB_LEFT_Y] < -85
					OR sPlanningBoard.iStickPosition[PB_LEFT_X] < -85
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
					//M&K options
					OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP ))
						
					
						BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
						END_SCALEFORM_MOVIE_METHOD()

						CPRINTLN(DEBUG_HEIST, "Polling for new crew member hover return value.")
						BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "GET_CURRENT_SELECTION")
						sPlanningBoard.sriCrewHover = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
						
						sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
						sPlanningBoard.iChoiceNoInputTimer = sPlanningBoard.iSelectionChangeTimer
						
					ELIF sPlanningBoard.iStickPosition[PB_LEFT_Y] > 85 
					OR sPlanningBoard.iStickPosition[PB_LEFT_X] > 85
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
					//M&K options
					OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN))
					
						BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
						END_SCALEFORM_MOVIE_METHOD()

						CPRINTLN(DEBUG_HEIST, "Polling for new crew member hover return value.")
						BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "GET_CURRENT_SELECTION")
						sPlanningBoard.sriCrewHover = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

						sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
						sPlanningBoard.iChoiceNoInputTimer = sPlanningBoard.iSelectionChangeTimer
					ENDIF
				
				ENDIF
				IF sPlanningBoard.eMode = PBM_CHOICE_SELECT
					
					IF sPlanningBoard.iStickPosition[PB_LEFT_Y] < -85
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
					//M&K options
					OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ONLY ))
					
						BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
						END_SCALEFORM_MOVIE_METHOD()
						
						//Play selection scroll SFX.
						IF sPlanningBoard.iGameplayChoiceHighlighted != 0
							PLAY_SOUND_FRONTEND(-1, "MARKER_ERASE", "HEIST_BULLETIN_BOARD_SOUNDSET")
						ENDIF
						sPlanningBoard.iGameplayChoiceHighlighted = 0
						
						IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
							CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
							Update_Instructional_Buttons(sPlanningBoard)
						ENDIF
						
						sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
						sPlanningBoard.iChoiceNoInputTimer = sPlanningBoard.iSelectionChangeTimer
					ELIF sPlanningBoard.iStickPosition[PB_LEFT_Y] > 85 
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
					//M&K options
					OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED( PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ONLY))
					
						BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_INPUT_EVENT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
						END_SCALEFORM_MOVIE_METHOD()

						IF sPlanningBoard.iGameplayChoiceHighlighted != 1
							PLAY_SOUND_FRONTEND(-1, "MARKER_ERASE", "HEIST_BULLETIN_BOARD_SOUNDSET")
						ENDIF
						sPlanningBoard.iGameplayChoiceHighlighted = 1
						
						IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
							CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
							Update_Instructional_Buttons(sPlanningBoard)
						ENDIF
				
						sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
						sPlanningBoard.iChoiceNoInputTimer = sPlanningBoard.iSelectionChangeTimer
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				OR IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_MULTI_SKIP_NEXT_DIALOGUE)
				
					//Selection input events.
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, PB_CONTROL_SELECT)
					//M&K option
					OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_SELECT_M))
					#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					#ENDIF

						SWITCH sPlanningBoard.eMode
							CASE PBM_CHOICE_SELECT
								//Gameplay choice selection made.
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIR_INPUT)
										sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
										eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
										
										IF eHeistChoiceFlowInt <> FLOWINT_NONE
											//Update the flow int with the selected value.
											BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "GET_CURRENT_SELECTION")
											sPlanningBoard.sriGameplayChoice = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
											
											Set_Board_Help_Hidden(sPlanningBoard, TRUE)
											REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tChoiceHelp[0])
											REMOVE_HELP_FROM_FLOW_QUEUE(sPlanningBoard.sData.tChoiceHelp[1])
											g_savedGlobals.sHeistData.bChoiceHelpDisplayed[sPlanningBoard.iLinkedHeist] = TRUE
											CLEAR_HELP(FALSE)
											
											CPRINTLN(DEBUG_HEIST, "Requested a return value for a gameplay choice.")
										ENDIF
									ENDIF
								ENDIF
							BREAK

							CASE PBM_CREW_SELECT
								//Crew choice selection made.
								IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, sPlanningBoard.iLinkedHeist)
									eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
									iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
									
									IF sPlanningBoard.iSelectedView < g_sHeistChoiceData[iHeistChoice].iCrewSize
										BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "GET_CURRENT_SELECTION")
										sPlanningBoard.sriCrewChoice = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
										CPRINTLN(DEBUG_HEIST, "Requested a return value for a crew choice.")
										Set_Board_Help_Hidden(sPlanningBoard, TRUE)
									ENDIF
								ENDIF
							BREAK
							
							CASE PBM_CONFIRM
								CPRINTLN(DEBUG_HEIST, "Planning board choices confirmed. Playing board exit dialogue.")
							
								IF sPlanningBoard.iLinkedHeist != HEIST_DOCKS
									//Board choices confirmed.
									Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_CREW_SELECTED, sPlanningBoard.iLinkedHeist, TRUE)
									
									//Set bits to modify dialogue later.
									IF NOT IS_REPEAT_PLAY_ACTIVE()
										CPRINTLN(DEBUG_HEIST, "Updating selected crew member bits.")
										PRIVATE_Update_Crew_Used_Flags_For_Heist(sPlanningBoard.iLinkedHeist)
									ENDIF
								ELSE
									//Gameplay choice confirmed.
									Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_CHOICE_SELECTED, sPlanningBoard.iLinkedHeist, TRUE)
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
								//Play board exit dialogue.
								eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
								iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
								
								//Extra safeguard to ensure a valid crew has been selected.
								INT iCrewIndex
								CrewMember eCrewTesting
								REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
									IF g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex] = CM_EMPTY
									OR GET_CREW_MEMBER_TYPE(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]) != g_sHeistChoiceData[iHeistChoice].eCrewType[iCrewIndex]
										SCRIPT_ASSERT("MANAGE_BOARD_INPUT: Confirming a planning board and a crew member slot is set to empty or wrong type. Picking failsafe crew.")
	
										CPRINTLN(DEBUG_HEIST, "Looking for failsafe crew of type ", GET_HEIST_CREW_TYPE_DEBUG_STRING(g_sHeistChoiceData[iHeistChoice].eCrewType[iCrewIndex]), ".")
										
										//Search for a random crew member of the correct type to fill this slot.
										BOOL bFoundMatch
										bFoundMatch = FALSE
										
										INT iCrewTestIndex
										REPEAT CM_MAX_CREW_MEMBERS iCrewTestIndex
											IF NOT bFoundMatch
												bFoundMatch = TRUE
												eCrewTesting = INT_TO_ENUM(CrewMember, iCrewTestIndex)
												IF eCrewTesting != CM_EMPTY
													CPRINTLN(DEBUG_HEIST, "Testing crew member ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_NAME_LABEL(eCrewTesting)), " for selection.")
													
													//Is this random crew member the correct type?
													IF g_sCrewMemberStaticData[eCrewTesting].type != g_sHeistChoiceData[iHeistChoice].eCrewType[iCrewIndex]
														CPRINTLN(DEBUG_HEIST, "Failed type check.")
														bFoundMatch = FALSE
													ENDIF
													//Is this crew member unlocked?
													IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUnlockedBitset, ENUM_TO_INT(eCrewTesting))
														CPRINTLN(DEBUG_HEIST, "Failed unlock check.")
														bFoundMatch = FALSE
													ENDIF
													//Is this crew member dead?
													IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDeadBitset, ENUM_TO_INT(eCrewTesting))
														CPRINTLN(DEBUG_HEIST, "Failed dead check.")
														bFoundMatch = FALSE
													ENDIF
													//Special case. Block using Chef on the Agency Heist.
													IF eCrewTesting = CM_GUNMAN_G_CHEF_UNLOCK AND sPlanningBoard.iLinkedHeist = HEIST_AGENCY
														CPRINTLN(DEBUG_HEIST, "Failed chef check.")
														bFoundMatch = FALSE
													ENDIF
													//Is this random crew member already used?
													IF Is_Crew_Member_Already_Selected_On_Board(sPlanningBoard, eCrewTesting)
														CPRINTLN(DEBUG_HEIST, "Failed already selected check.")
														bFoundMatch = FALSE
													ENDIF
												ELSE
													bFoundMatch = FALSE
												ENDIF
											ENDIF
										ENDREPEAT
							
										//Save selected crew member into relevant slot.
										g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex] = eCrewTesting
										#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_HEIST, "Picked failsafe crew member ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_NAME_LABEL(eCrewTesting)), " verified as valid as board is locked in.")
										#ENDIF
#IF IS_DEBUG_BUILD
									ELSE
										CPRINTLN(DEBUG_HEIST, "Crew member at index ", iCrewIndex, " verified as valid as board is locked in.")
#ENDIF
									ENDIF
								ENDREPEAT
								
								Set_Board_Help_Hidden(sPlanningBoard, TRUE)
								
								Play_Heist_Choice_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, iHeistChoice, HCDS_EXIT)
								
							BREAK
						ENDSWITCH
						
						sPlanningBoard.eHoverCrewMember = CM_EMPTY
					ENDIF
					
					//IF (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_UNDO))
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_UNDO)
					//M&K Option
					OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_UNDO_M))
						SWITCH sPlanningBoard.eMode
							CASE PBM_CREW_SELECT
								eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
								iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
							
								IF sPlanningBoard.iSelectedView > 0
									Set_Board_Help_Hidden(sPlanningBoard, TRUE)
								
									//Step back to previous crew slot.
									sPlanningBoard.iSelectedView--
									sPlanningBoard.iCrewSlotSelected = sPlanningBoard.iSelectedView
									CrewMember eOldSelected
									eOldSelected = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView]
									g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView] = CM_EMPTY
									sPlanningBoard.eLastCrewType = g_sHeistChoiceData[iHeistChoice].eCrewType[sPlanningBoard.iSelectedView]
									Set_Focus_On_Crew_View(sPlanningBoard, sPlanningBoard.iSelectedView, eOldSelected)
									PLAY_SOUND_FRONTEND(-1, "UNDO", "HEIST_BULLETIN_BOARD_SOUNDSET")
									sPlanningBoard.eHoverCrewMember = CM_EMPTY
									sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
									
							#if not USE_CLF_DLC
							#if not USE_NRM_DLC
								ELIF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_HEIST_BOARD_UNDO)
									// Hack fix to 1106678. Paleto Score has no choice selection to back up to.
									// By far the easiest solution to a bug that is tricky to fix inside
									// this generalised system.
									IF sPlanningBoard.iLinkedHeist != HEIST_RURAL_BANK 
										INT index
										REPEAT sPlanningBoard.sData.iMaxCrewSize index
											BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT_EMPTY")
												SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
											END_SCALEFORM_MOVIE_METHOD()
											BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SET_DATA_SLOT_EMPTY")
												SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
												SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
											END_SCALEFORM_MOVIE_METHOD()
										ENDREPEAT
										PLAY_SOUND_FRONTEND(-1, "UNDO", "HEIST_BULLETIN_BOARD_SOUNDSET")
										
										//Clear display groups for chosen approach as we jump back to
										//the approach selector.
										REPEAT 2 index
											IF sPlanningBoard.sData.eChoiceSelectedGroup[index] != PBDG_INVALID
												SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.eChoiceSelectedGroup[index], FALSE)
											ENDIF
										ENDREPEAT
										
										sPlanningBoard.eHoverCrewMember = CM_EMPTY

										Set_Board_Help_Hidden(sPlanningBoard, TRUE)
										
										sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
										
										sPlanningBoard.eLastCrewType = CMT_EMPTY
										Set_Mission_Flow_Flag_State(FLOWFLAG_HEIST_BOARD_UNDO, TRUE)
									ENDIF
							#endif
							#endif
								ENDIF	
								
							BREAK
							
							CASE PBM_CONFIRM
								CPRINTLN(DEBUG_HEIST, "Board choice confirmation rejected. Backing up to last crew selection.")
								IF sPlanningBoard.iLinkedHeist != HEIST_DOCKS
									eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
									iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
									sPlanningBoard.iSelectedView--
									sPlanningBoard.iCrewSlotSelected = sPlanningBoard.iSelectedView
									CrewMember eOldSelected
									eOldSelected = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView]
									g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView] = CM_EMPTY
									sPlanningBoard.eLastCrewType = g_sHeistChoiceData[iHeistChoice].eCrewType[sPlanningBoard.iSelectedView]
									
									Switch_Board_Mode(sPlanningBoard, PBM_CREW_SELECT, eOldSelected)
								ELSE
									//Clear display groups for chosen approach as we jump back to
									//the approach selector.
									INT index
									REPEAT 2 index
										IF sPlanningBoard.sData.eChoiceSelectedGroup[index] != PBDG_INVALID
											SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.eChoiceSelectedGroup[index], FALSE)
										ENDIF
									ENDREPEAT
									
									//Make sure the choice is cleared as we initialise the interaction.
									SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_DOCKS, HEIST_CHOICE_EMPTY)
									SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, HEIST_DOCKS, FALSE)
									
									sPlanningBoard.eHoverCrewMember = CM_EMPTY
									Set_Board_Help_Hidden(sPlanningBoard, TRUE)
									sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
									Switch_Board_Mode(sPlanningBoard, PBM_CHOICE_SELECT)
								ENDIF
								PLAY_SOUND_FRONTEND(-1, "UNDO", "HEIST_BULLETIN_BOARD_SOUNDSET")
								
								//Set_Board_Help_Hidden(sPlanningBoard, TRUE)	//	Removed for B* 2170546
							BREAK
						ENDSWITCH

					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	//We are waiting for dialogue to finish. 
	//If it has, refocus the view on the next crew slot.
	ELIF sPlanningBoard.iFramesWithoutDialogue > 5
		eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
		iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
		
		IF sPlanningBoard.iSelectedView >= g_sHeistChoiceData[iHeistChoice].iCrewSize
		OR (sPlanningBoard.iLinkedHeist = HEIST_DOCKS AND sPlanningBoard.iSelectedView > sPlanningBoard.iGameplayChoiceStartIndex)
			//Unfocus from this view.
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99) 	//99 is a special value that will focus on nothing.
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)	//-1 is a special value that will clear all darkening effects.
			END_SCALEFORM_MOVIE_METHOD()
		
			Play_Board_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, BDS_CONFIRM, TRUE)
			Switch_Board_Mode(sPlanningBoard, PBM_CONFIRM)
		ELSE
			IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
				CrewMember eOldSelected
				eOldSelected = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView]
				g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView] = CM_EMPTY
				sPlanningBoard.eLastCrewType = g_sHeistChoiceData[iHeistChoice].eCrewType[sPlanningBoard.iSelectedView]
				Set_Focus_On_Crew_View(sPlanningBoard, sPlanningBoard.iSelectedView, eOldSelected)
			ENDIF
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
		ENDIF
		
		sPlanningBoard.iChoiceNoInputTimer = GET_GAME_TIMER()
		CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
		
	ELIF IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tCrewHelp[0])
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tCrewHelp[1])
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tChoiceHelp[0])
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tChoiceHelp[1])
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
	
	IF sPlanningBoard.sriGameplayChoice != NULL
		CPRINTLN(DEBUG_HEIST, "Waiting for gameplay choice return...")
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sPlanningBoard.sriGameplayChoice)
			CPRINTLN(DEBUG_HEIST, "Gameplay choice returned.")
			
			INT iSlotIndex = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sPlanningBoard.sriGameplayChoice)
			iHeistChoice = Get_Choice_In_Heist_Choice_Slot(sPlanningBoard.iLinkedHeist, iSlotIndex)
			eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
			Set_Mission_Flow_Int_Value(eHeistChoiceFlowInt, iHeistChoice)
			
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				PRIVATE_Update_Heist_Choice_Stat_From_FlowInt(sPlanningBoard.iLinkedHeist)
			ENDIF
				
			//Play choice selection SFX.
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
			//Play choice selection dialogue.
			Play_Heist_Choice_Dialogue_From_Slot_If_Unplayed(sPlanningBoard, iHeistChoice, HCDS_CHOICE)
			
			//Turn on display group for the choice we've picked.
			IF sPlanningBoard.sData.eChoiceSelectedGroup[iSlotIndex] != PBDG_INVALID
				SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(sPlanningBoard.iLinkedHeist, sPlanningBoard.sData.eChoiceSelectedGroup[iSlotIndex], TRUE)
			ENDIF
			
			//Make correct number of crew select views visible.
			INT index
			REPEAT sPlanningBoard.sData.iMaxCrewSize index
				Clean_Up_Crew_View(sPlanningBoard, index)
				IF index < g_sHeistChoiceData[iHeistChoice].iCrewSize
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "SHOW_VIEW")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDREPEAT
			
			IF sPlanningBoard.iLinkedHeist != HEIST_DOCKS
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, sPlanningBoard.iLinkedHeist, TRUE)
			ELSE
				SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
				sPlanningBoard.iSelectedView++
			ENDIF

			CLEAR_HELP(FALSE)
			sPlanningBoard.sriGameplayChoice = NULL
		ENDIF
	ENDIF
	
	IF sPlanningBoard.sriCrewChoice != NULL
		CPRINTLN(DEBUG_HEIST, "Waiting for crew choice return...")
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sPlanningBoard.sriCrewChoice)
			CPRINTLN(DEBUG_HEIST, "Crew choice returned.")
			
			INT iCurrentSelectedIndex = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sPlanningBoard.sriCrewChoice)
			eHeistChoiceFlowInt = Get_Heist_Choice_FlowInt_For_Heist(sPlanningBoard.iLinkedHeist)
			iHeistChoice = Get_Mission_Flow_Int_Value(eHeistChoiceFlowInt)
				
			//Save the selected crew member to the heist globals.
			g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView] = sPlanningBoard.eCrewSlotLayout[iCurrentSelectedIndex]
			
			//Rebuild the crew view with only the chosen crew member in it.
			INT iSelectedCrew = ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][sPlanningBoard.iSelectedView])

			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "UPDATE_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iSelectedView)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentSelectedIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedCrew)
			END_SCALEFORM_MOVIE_METHOD()
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_HEIST, "Setting selected crew member asset to crew member ", GET_STRING_FROM_TEXT_FILE(GET_CREW_MEMBER_FIRST_NAME_LABEL(INT_TO_ENUM(CrewMember, iSelectedCrew))) , ".")
			#ENDIF
			
			//Unfocus from this view.
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "FOCUS_VIEW")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99) //99 is a special value that will focus on nothing.
			END_SCALEFORM_MOVIE_METHOD()

			//Play crew selection SFX.
			PLAY_SOUND_FRONTEND(-1, "PERSON_SELECT", "HEIST_BULLETIN_BOARD_SOUNDSET")
			
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_NO_REFOCUS_AFTER_LOCK)
													
			//Start playing crew member selection dialogue.
			Play_Crew_Member_Dialogue(sPlanningBoard, INT_TO_ENUM(CrewMember, iSelectedCrew))
			
			//Update the view/crew slot we have selected.
			sPlanningBoard.iSelectedView++
			sPlanningBoard.iCrewSlotSelected = sPlanningBoard.iSelectedView //Save current selected crew slot to return to if we exit the board.
			sPlanningBoard.iSelectionChangeTimer = GET_GAME_TIMER()
			sPlanningBoard.iChoiceNoInputTimer = sPlanningBoard.iSelectionChangeTimer
			
			//Flag that we want to block all input until any current dialgoue has finished.
			//at which point we will refocus the view on the next crew slot.
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE)
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tCrewHelp[0])
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPlanningBoard.sData.tCrewHelp[1])
				CLEAR_HELP(FALSE)
			ENDIF
			sPlanningBoard.sriCrewChoice = NULL
		ENDIF
	ENDIF
	
	IF sPlanningBoard.sriCrewHover != NULL
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sPlanningBoard.sriCrewHover)
			INT iCrewSelection = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sPlanningBoard.sriCrewHover)
			IF iCrewSelection != -1
				CPRINTLN(DEBUG_HEIST, "Got new crew member hover return value: ", iCrewSelection)
				
				CrewMember eNewHoverCrew = sPlanningBoard.eCrewSlotLayout[iCrewSelection]
				IF eNewHoverCrew != sPlanningBoard.eHoverCrewMember
					//Play selection scroll SFX.
					PLAY_SOUND_FRONTEND(-1, "PERSON_SCROLL", "HEIST_BULLETIN_BOARD_SOUNDSET")
				ENDIF
				sPlanningBoard.eHoverCrewMember = eNewHoverCrew
				CPRINTLN(DEBUG_HEIST, "Set crew member hover value to ", GET_CREW_MEMBER_NAME_LABEL(sPlanningBoard.eHoverCrewMember),".")
			ENDIF
			sPlanningBoard.sriCrewHover = NULL	
		ENDIF
	ENDIF
	
	IF sPlanningBoard.eHoverCrewMember != CM_EMPTY
		Play_Crew_Member_Hover_Dialogue(sPlanningBoard, sPlanningBoard.eHoverCrewMember)
	ENDIF
ENDPROC


PROC Update_Board_POI_Overview(PlanningBoard &sPlanningBoard)
	IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, sPlanningBoard.iLinkedHeist)
	
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				sPlanningBoard.iPOITimer = GET_GAME_TIMER()
			ENDIF
		#ENDIF
		
		IF sPlanningBoard.iNoConvQueued = 0 AND sPlanningBoard.iFramesWithoutDialogue > 3
			IF sPlanningBoard.iPOITimer = -1
				CPRINTLN(DEBUG_HEIST, "Setting new POI overview time delay.")
				IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DELAY_NEXT_POI)
					sPlanningBoard.iPOITimer = GET_GAME_TIMER() + PB_TIME_BETWEEN_POIS
				ELSE
					sPlanningBoard.iPOITimer = GET_GAME_TIMER()
				ENDIF
			ELIF GET_GAME_TIMER() > sPlanningBoard.iPOITimer
				BOOL bFoundNewPOI = FALSE
			
				INT iPOIIndex
				REPEAT sPlanningBoard.sData.iNoPointsOfInterest iPOIIndex
					IF NOT bFoundNewPOI
						IF NOT IS_BIT_SET(sPlanningBoard.sData.iBitsetBoardDialoguePlayed, iPOIIndex + ENUM_TO_INT(BDS_POI_1)) 
							Set_Focus_On_POI(sPlanningBoard, iPOIIndex)
							IF IS_BIT_SET(sPlanningBoard.sData.iPOIDelayBeforeNextBitset, iPOIIndex)
								SET_BIT(sPlanningBoard.iState, BIT_BOARD_DELAY_NEXT_POI)
							ELSE
								CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_DELAY_NEXT_POI)
							ENDIF
							bFoundNewPOI = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF NOT bFoundNewPOI
					CPRINTLN(DEBUG_HEIST, "All point of interests processed. Flagging POI overview as completed.")
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, sPlanningBoard.iLinkedHeist, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//══════════════════════════════════╡ Debug ╞═══════════════════════════════════

#IF IS_DEBUG_BUILD
//*** Sets-up and maintains a widget for a specific planning board. ***
PROC Debug_Run_Planning_Board_Widget(PlanningBoard &sPlanningBoard)
	INT index

	//Create the widget.
	IF NOT sPlanningBoard.sData.bBoardWidgetCreated
		TEXT_LABEL_31 tWidgetTitle

		START_WIDGET_GROUP("Planning Board Widget")
			
			ADD_WIDGET_BOOL("Refresh Views", sPlanningBoard.sData.bRefreshViews)
			ADD_WIDGET_BOOL("Refresh Board", sPlanningBoard.sData.bRefreshBoard)
			ADD_WIDGET_BOOL("Draw Pixel Coord Grid", sPlanningBoard.sData.bDebugDrawGrid)
			ADD_WIDGET_BOOL("Draw Center", sPlanningBoard.sData.bDebugDrawCenter)
			ADD_WIDGET_BOOL("Draw Default Focus", sPlanningBoard.sData.bDebugDrawDefaultFocus)
			ADD_WIDGET_BOOL("Draw Stick Focus", sPlanningBoard.sData.bDebugDrawStickFocus)
			ADD_WIDGET_BOOL("Draw Pins", sPlanningBoard.sData.bDebugDrawPins)
			ADD_WIDGET_BOOL("Draw POIs", sPlanningBoard.sData.bDebugDrawPOIs)
			ADD_WIDGET_BOOL("Draw Static Items", sPlanningBoard.sData.bDebugDrawStaticItems)
			
			
			START_WIDGET_GROUP("Camera attributes")
				
				ADD_WIDGET_BOOL("Zoom", sPlanningBoard.sCam.bZoomEnabled)			
				ADD_WIDGET_BOOL("Look",sPlanningBoard.sCam.bLookEnabled)
				ADD_WIDGET_INT_READ_ONLY("Stick X",sPlanningBoard.iStickPosition[PB_RIGHT_X])
				ADD_WIDGET_INT_READ_ONLY("Stick Y",sPlanningBoard.iStickPosition[PB_RIGHT_Y])
				ADD_WIDGET_FLOAT_READ_ONLY("Off cur X",sPlanningBoard.sCam.vCamRotOffsetCurrent.x)
				ADD_WIDGET_FLOAT_READ_ONLY("Off cur Y",sPlanningBoard.sCam.vCamRotOffsetCurrent.y)
				ADD_WIDGET_FLOAT_READ_ONLY("Off cur Z",sPlanningBoard.sCam.vCamRotOffsetCurrent.z)
				ADD_WIDGET_INT_READ_ONLY("FX limit",sPlanningBoard.sCam.iLookXLimit)
				ADD_WIDGET_INT_READ_ONLY("FY limit",sPlanningBoard.sCam.iLookYLimit)
				ADD_WIDGET_INT_READ_ONLY("Roll limit",sPlanningBoard.sCam.iRollLimit)
				
			STOP_WIDGET_GROUP()
			
			

			//Allow base board object to be positioned.
			START_WIDGET_GROUP("Base Positioning")
				ADD_WIDGET_FLOAT_SLIDER("Position X",sPlanningBoard.vPosition.X,-4000.0,4000.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Position Y",sPlanningBoard.vPosition.Y,-4000.0,4000.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Position Z",sPlanningBoard.vPosition.Z,-4000.0,4000.0,0.01)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_FLOAT_SLIDER("Heading",sPlanningBoard.fHeading,-360.0,360.0,0.01)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_FLOAT_SLIDER("Camera Distance",sPlanningBoard.sData.fCameraDistance,0.1,4.0,0.01)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Def. Focus X",sPlanningBoard.sData.sDefaultFocusPos.x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
				ADD_WIDGET_INT_SLIDER("Def. Focus Y",sPlanningBoard.sData.sDefaultFocusPos.y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
			STOP_WIDGET_GROUP()
			
			//Allow scaleform to be repositioned.
			START_WIDGET_GROUP("Scaleform Data")
				ADD_WIDGET_FLOAT_SLIDER("Width", sPlanningBoard.sData.fScaleformWidth, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Height", sPlanningBoard.sData.fScaleformHeight, -5.0, 5.0, 0.01)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_FLOAT_SLIDER("Scale X", sPlanningBoard.sData.fScaleformXScale, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Scale Y", sPlanningBoard.sData.fScaleformYScale, -5.0, 5.0, 0.01)
			STOP_WIDGET_GROUP()
			
			//Allow elements on the board to be repositioned.
			START_WIDGET_GROUP("Element Positioning")
			
				//Create sub-widgets for each crew element.
				REPEAT sPlanningBoard.sData.iMaxCrewSize index
					tWidgetTitle = "Crew Element "
					tWidgetTitle += index
					START_WIDGET_GROUP(tWidgetTitle)
						ADD_WIDGET_INT_SLIDER("X Pos", sPlanningBoard.sData.sCrewBoardPos[index].x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
						ADD_WIDGET_INT_SLIDER("Y Pos", sPlanningBoard.sData.sCrewBoardPos[index].y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
					STOP_WIDGET_GROUP()
				ENDREPEAT
				
				//Create a sub-widget for the gameplay choice element.
				START_WIDGET_GROUP("Gameplay Choice Element")
					ADD_WIDGET_INT_SLIDER("X Pos", sPlanningBoard.sData.sChoiceBoardPos.x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
					ADD_WIDGET_INT_SLIDER("Y Pos", sPlanningBoard.sData.sChoiceBoardPos.y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
					ADD_WIDGET_INT_SLIDER("X Offset", sPlanningBoard.sData.sChoiceCameraOffset.x, -250, 250, 1)
					ADD_WIDGET_INT_SLIDER("Y Offset", sPlanningBoard.sData.sChoiceCameraOffset.y, -250, 250, 1)
				STOP_WIDGET_GROUP()
				
				//Create a sub-widget for the gameplay choice element.
				START_WIDGET_GROUP("Todo List Element")
					ADD_WIDGET_INT_SLIDER("X Pos", sPlanningBoard.sData.sListBoardPos.x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
					ADD_WIDGET_INT_SLIDER("Y Pos", sPlanningBoard.sData.sListBoardPos.y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
				STOP_WIDGET_GROUP()
				
				//Create sub-widgets for the static elements.
				REPEAT sPlanningBoard.sData.iNoStaticItems index
					tWidgetTitle = "Static Element "
					tWidgetTitle += index
					START_WIDGET_GROUP(tWidgetTitle)
						ADD_WIDGET_INT_SLIDER("X Pos", sPlanningBoard.sData.sStaticItemPos[index].x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
						ADD_WIDGET_INT_SLIDER("Y Pos", sPlanningBoard.sData.sStaticItemPos[index].y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
					STOP_WIDGET_GROUP()
				ENDREPEAT
				
				//Create sub-widgets for each board point of interest.
				REPEAT sPlanningBoard.sData.iNoPointsOfInterest index
					tWidgetTitle = "POI "
					tWidgetTitle += index
					START_WIDGET_GROUP(tWidgetTitle)
						ADD_WIDGET_INT_SLIDER("X Pos", sPlanningBoard.sData.sPOIPos[index].x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
						ADD_WIDGET_INT_SLIDER("Y Pos", sPlanningBoard.sData.sPOIPos[index].y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
					STOP_WIDGET_GROUP()
				ENDREPEAT
				
				//Create sub-widgets for each board pin.
				REPEAT sPlanningBoard.sData.iNoBoardPins index
					TEXT_LABEL_31 tPinWidgetTitle = "Pin "
					tPinWidgetTitle += index
					START_WIDGET_GROUP(tPinWidgetTitle)
						ADD_WIDGET_INT_SLIDER("X Pos", sPlanningBoard.sData.sPinPos[index].x, 0, sPlanningBoard.sData.iScaleformPixelWidth, 1)
						ADD_WIDGET_INT_SLIDER("Y Pos", sPlanningBoard.sData.sPinPos[index].y, 0, sPlanningBoard.sData.iScaleformPixelHeight, 1)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Debug Info")
				//Display board mode info.
				ADD_WIDGET_INT_READ_ONLY("Board Mode",sPlanningBoard.sData.iModeWidget)
				ADD_WIDGET_BOOL("Board View Active Global", g_bHeistBoardViewActive)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	
		sPlanningBoard.sData.bBoardWidgetCreated = TRUE
	ENDIF
	
	//Update board mode INTs for debug info.
	sPlanningBoard.sData.iModeWidget = ENUM_TO_INT(sPlanningBoard.eMode)
ENDPROC


//*** Monitors Planning Board Widget variables and updates the board where appropriate. ***
PROC Maintain_Planning_Board_Widget(PlanningBoard &sPlanningBoard)
	IF sPlanningBoard.sData.bRefreshViews
		//Reposition crew views.
		INT index
		REPEAT sPlanningBoard.sData.iMaxCrewSize index
			BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "REPOSITION_VIEW")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[index].x))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[index].y))
			END_SCALEFORM_MOVIE_METHOD()
		ENDREPEAT
		
		//Reposition gameplay choice view.
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "REPOSITION_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[index].x))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[index].y))
		END_SCALEFORM_MOVIE_METHOD()
												
		//Reposition todo list view.
		BEGIN_SCALEFORM_MOVIE_METHOD(sPlanningBoard.siBoardMovie, "REPOSITION_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPlanningBoard.iGameplayChoiceStartIndex+1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[index].x))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(sPlanningBoard.sData.sCrewBoardPos[index].y))
		END_SCALEFORM_MOVIE_METHOD()
												
		sPlanningBoard.sData.bRefreshViews = FALSE
	ENDIF


	IF sPlanningBoard.sData.bRefreshBoard
		//Force cleanup of board so that it is rebuilt.
		WHILE NOT Cleanup_Planning_Board(sPlanningBoard)
			WAIT(0)
		ENDWHILE
		sPlanningBoard.sData.bRefreshBoard = FALSE
	ENDIF
ENDPROC

#ENDIF


//══════════════════════════════╡ Board Viewing ╞═══════════════════════════════

//*** Maintains a board view for each frame. ***
PROC Update_Board_View(PlanningBoard &sPlanningBoard)

	//Check for control scheme changes.
	STRING eLookInputThisFrame = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK)
	IF NOT ARE_STRINGS_EQUAL(eLookInputThisFrame, sPlanningBoard.eLookInputLastFrame)
		CPRINTLN(DEBUG_HEIST, "Detected control input mapping change. Refreshing instructional buttons.")
		Update_Instructional_Buttons(sPlanningBoard)
	ENDIF
	sPlanningBoard.eLookInputLastFrame = eLookInputThisFrame
	
	// #1182074 Don't allow the video recording system record footage of a planning board.
	// #1966073 Only prevent while the player is interacting with the board.
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	//Update board player input.
	Manage_Board_Input(sPlanningBoard)
	
	//Handle dialogue queuing.
	Update_Board_Conversations(sPlanningBoard)

	IF sPlanningBoard.eMode = PBM_POI_OVERVIEW
		Update_Board_POI_Overview(sPlanningBoard)
	ENDIF
	
	IF IS_FLOW_HELP_QUEUE_EMPTY()
		Update_Board_Text(sPlanningBoard)
	ENDIF
	
	//Update board's first person camera.
	Bool bBlockLookControl = (sPlanningBoard.iDisableFreeLookTime > GET_GAME_TIMER())
	UPDATE_FIRST_PERSON_CAMERA(	sPlanningBoard.sCam, 
								IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_ZOOM_CONTROL_ON), 
								IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_LOOK_CONTROL_ON) AND NOT bBlockLookControl,
								TRUE, FALSE, DEFAULT, TRUE)

	//Draw the scaleform help text.
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_PLAYER_SETUP)
		IF NOT IS_CUTSCENE_PLAYING() 
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_PAUSE_MENU_ACTIVE()
			IF sPlanningBoard.eMode != PBM_POI_OVERVIEW
				IF sPlanningBoard.eMode = PBM_VIEW
				OR (NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_DIALOGUE_LOCKED) 
					AND NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_WAIT_FOR_DIALOGUE) 
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND sPlanningBoard.iNoConvQueued = 0)
					IF (NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_HIDE_HELP)) OR sPlanningBoard.eMode = PBM_CREW_SELECT							
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(sPlanningBoard.siHelpTextMovie, 255,255,255,0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Block various HUD elements while the board view is enabled.
	DISPLAY_RADAR(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	THEFEED_HIDE_THIS_FRAME()
	
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT,UI_ALIGN_BOTTOM)
	SET_HUD_COMPONENT_POSITION(NEW_HUD_SAVING_GAME, 0.612, 0.818)
	RESET_SCRIPT_GFX_ALIGN()
	
	IF NOT IS_CUTSCENE_PLAYING()
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
//	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//	ENDIF
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)

	IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_PLAYER_SETUP)
		IF NOT IS_CUTSCENE_PLAYING()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_HEIST, "Player configured for planning board view.")
			#ENDIF
			VECTOR vViewPos = sPlanningBoard.vPosition
			vViewPos += <<2.9*SIN(180-sPlanningBoard.fHeading),2.9*COS(180-sPlanningBoard.fHeading),0.5>>
			GET_GROUND_Z_FOR_3D_COORD(vViewPos,vViewPos.Z)
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH|SPC_AMBIENT_SCRIPT|SPC_CLEAR_TASKS)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_COORDS(PLAYER_PED_ID(),vViewPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlanningBoard.fHeading)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_PLAYER_SETUP)
		ENDIF
	ENDIF
ENDPROC


//*** Starts, updates, and cleans up a board view. ***
PROC Do_Board_View(PlanningBoard &sPlanningBoard)

	#IF IS_DEBUG_BUILD
		Maintain_Planning_Board_Widget(sPlanningBoard)
	#ENDIF
	
	BOOL bInTriggerArea = FALSE

	IF NOT IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PHONE_ONSCREEN()
				IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_PLANNING_BOARD)
				
					#IF IS_DEBUG_BUILD
						IF g_bTriggerDebugOn
							DRAW_DEBUG_TEXT("Planning Board", <<sPlanningBoard.vPosition.X-2.0, sPlanningBoard.vPosition.Y, sPlanningBoard.vPosition.Z>>, 0, 0, 255, 255)
							DRAW_DEBUG_SPHERE(sPlanningBoard.vPosition, 2, 255, 255, 0, 50) 
						ENDIF
					#ENDIF
				
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), sPlanningBoard.vPosition) < 4
						// Calculate heading of player to the board.
						// Looks lengthy but fairly optimised.
						VECTOR vDirPlayerToBoard = sPlanningBoard.vPosition - GET_ENTITY_COORDS(PLAYER_PED_ID())
						FLOAT fHeadPlayerToBoard = GET_HEADING_FROM_VECTOR_2D(vDirPlayerToBoard.X, vDirPlayerToBoard.Y)
						FLOAT fHeadingDiff = ABSF(sPlanningBoard.fHeading - fHeadPlayerToBoard)

						IF fHeadingDiff <= 70
							bInTriggerArea = TRUE
						ELSE
							FLOAT fHeadBoardTranslated = sPlanningBoard.fHeading
							IF fHeadBoardTranslated > 180.0
								fHeadBoardTranslated -= 360.0
							ELIF fHeadBoardTranslated < -180.0
								fHeadBoardTranslated += 360.0
							ENDIF
							IF (fHeadBoardTranslated - fHeadPlayerToBoard) < fHeadingDiff
								fHeadingDiff = ABSF(fHeadBoardTranslated - fHeadPlayerToBoard)
							ENDIF
							IF fHeadingDiff <= 70
								bInTriggerArea = TRUE
							ELSE
								IF fHeadPlayerToBoard > 180.0
									fHeadPlayerToBoard -= 360.0
								ELIF fHeadPlayerToBoard < -180.0
									fHeadPlayerToBoard += 360.0
								ENDIF
								IF (sPlanningBoard.fHeading - fHeadPlayerToBoard) < fHeadingDiff
									fHeadingDiff = ABSF(sPlanningBoard.fHeading - fHeadPlayerToBoard)
								ENDIF
								IF fHeadingDiff <= 70
									bInTriggerArea = TRUE
								ELSE
									IF (fHeadBoardTranslated - fHeadPlayerToBoard) < fHeadingDiff
										fHeadingDiff = ABSF(fHeadBoardTranslated - fHeadPlayerToBoard)
									ENDIF
									IF fHeadingDiff <= 70
										bInTriggerArea = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Check player is in a triggerable position.
		IF bInTriggerArea
			IF sPlanningBoard.iContextID = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(sPlanningBoard.iContextID, CP_MEDIUM_PRIORITY, "PB_H_ENT")
			ELIF HAS_CONTEXT_BUTTON_TRIGGERED(sPlanningBoard.iContextID)
				RELEASE_CONTEXT_INTENTION(sPlanningBoard.iContextID)
				Initalise_Board_View(sPlanningBoard)
			ENDIF
		ELIF sPlanningBoard.iContextID != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(sPlanningBoard.iContextID)
		ENDIF
	ELSE
		BOOL bNoExit = Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, sPlanningBoard.iLinkedHeist)
	
		Update_Board_View(sPlanningBoard)
		
		IF (NOT bNoExit
		AND (sPlanningBoard.iCancelOutCooldownTimer > WAIT_FRAMES_FOR_BOARD_EXIT)
		AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,PB_CONTROL_EXIT) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)))
		OR IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_AUTO_EXIT)
			CLEAR_BIT(sPlanningBoard.iState, BIT_BOARD_AUTO_EXIT)
			Exit_Board_View(sPlanningBoard, FALSE)
		ENDIF
	ENDIF
ENDPROC
