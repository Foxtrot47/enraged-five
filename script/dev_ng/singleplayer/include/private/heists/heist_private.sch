//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/05/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						    Generic Heist Control Header						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "script_buttons.sch"
USING "flow_help_public.sch"

CONST_INT	CREW_STAT1_BITMASK		(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7|BIT8|BIT9|BIT10|BIT11|BIT12|BIT13|BIT14)
CONST_INT	CREW_STAT2_BITMASK		(BIT15|BIT16|BIT17|BIT18|BIT19|BIT20|BIT21|BIT22|BIT23|BIT24|BIT25|BIT26|BIT27|BIT28|BIT29)
CONST_INT	CREW_STAT2_SHIFT		15

#IF IS_DEBUG_BUILD
	STRUCT DebugCrewSelector
		INT iLinkedHeistChoice
		INT iSelectedSlot
		INT iTimeLastSelectionChange
		BOOL bFinalised
	ENDSTRUCT
	
	FUNC STRING PRIVATE_Get_Crew_Dialogue_Debug_String(CrewDialogueSlot eCrewDialogueSlot)
		SWITCH eCrewDialogueSlot
			CASE CRDS_PICK_GUSTAV					RETURN "PICK_GUSTAV"				BREAK
			CASE CRDS_PICK_KARL						RETURN "PICK_KARL"					BREAK
			CASE CRDS_PICK_HUGH						RETURN "PICK_HUGH"					BREAK
			CASE CRDS_PICK_NORM						RETURN "PICK_NORM"					BREAK
			CASE CRDS_PICK_DARYL					RETURN "PICK_DARYL"					BREAK
			CASE CRDS_PICK_PAIGE					RETURN "PICK_PAIGE"					BREAK
			CASE CRDS_PICK_CHRIS					RETURN "PICK_CHRIS"					BREAK
			CASE CRDS_PICK_EDDIE					RETURN "PICK_EDDIE"					BREAK
			CASE CRDS_PICK_KARIM					RETURN "PICK_KARIM"					BREAK
			CASE CRDS_PICK_PACKIE					RETURN "PICK_PACKIE"				BREAK
			CASE CRDS_PICK_CHEF						RETURN "PICK_CHEF"					BREAK
			CASE CRDS_PICK_RICKIE					RETURN "PICK_RICKIE"				BREAK
			CASE CRDS_PICK_TALINA					RETURN "PICK_TALINA"				BREAK
			CASE CRDS_MET_PACKIE					RETURN "MET_PACKIE"					BREAK
			CASE CRDS_MET_RICKIE					RETURN "MET_RICKIE"					BREAK
			CASE CRDS_MET_TALINA					RETURN "MET_TALINA"					BREAK
			CASE CRDS_MET_CHEF						RETURN "MET_CHEF"					BREAK
			CASE CRDS_USED_JEWEL					RETURN "USED_JEWEL"					BREAK
			CASE CRDS_USED_PALETO					RETURN "USED_PALETO"				BREAK
			CASE CRDS_USED_AGENCY					RETURN "USED_AGENCY"				BREAK
			CASE CRDS_USED_JEWEL_PALETO				RETURN "USED_JEWEL_PALETO"			BREAK
			CASE CRDS_USED_JEWEL_AGENCY				RETURN "USED_JEWEL_AGENCY"			BREAK
			CASE CRDS_USED_ALL						RETURN "USED_ALL"					BREAK
			CASE CRDS_USED_RICKIE_LAST_ONE			RETURN "USED_RICKIE_LAST_ONE"		BREAK
			CASE CRDS_USED_RICKIE_LAST_TWO			RETURN "USED_RICKIE_LAST_TWO"		BREAK
			CASE CRDS_USED_KARIM_LAST_ONE			RETURN "USED_KARIM_LAST_ONE"		BREAK
			CASE CRDS_USED_KARIM_LAST_TWO			RETURN "USED_KARIM_LAST_TWO"		BREAK
			CASE CRDS_USED_CHRIS_JEWEL				RETURN "USED_CHRIS_JEWEL"			BREAK
			CASE CRDS_USED_CHRIS_AGENCY				RETURN "USED_CHRIS_AGENCY"			BREAK
			CASE CRDS_USED_CHRIS_JEWEL_AGENCY		RETURN "USED_CHRIS_JEWEL_AGENCY"	BREAK
			CASE CRDS_USED_BADG_AGENCY				RETURN "USED_BADG_AGENCY"			BREAK
		ENDSWITCH
		RETURN "ERROR"
	ENDFUNC  
	
	FUNC STRING PRIVATE_Get_Crew_Used_Bit_Debug_String(INT iCrewUsedBit)
		SWITCH iCrewUsedBit
			CASE CREW_USED_JEWEL_GUSTAV		RETURN 	"JEWEL_GUSTAV" 		BREAK
			CASE CREW_USED_JEWEL_PACKIE		RETURN 	"JEWEL_PACKIE" 		BREAK		
			CASE CREW_USED_JEWEL_CHRIS		RETURN 	"JEWEL_CHRIS" 		BREAK		
			CASE CREW_USED_JEWEL_RICKIE		RETURN 	"JEWEL_RICKIE" 		BREAK		
			CASE CREW_USED_JEWEL_PAIGE		RETURN 	"JEWEL_PAIGE" 		BREAK		
			CASE CREW_USED_JEWEL_EDDIE		RETURN 	"JEWEL_EDDIE" 		BREAK		
			CASE CREW_USED_JEWEL_KARIM		RETURN 	"JEWEL_KARIM" 		BREAK		
			CASE CREW_USED_PALETO_GUSTAV	RETURN 	"PALETO_GUSTAV" 	BREAK		
			CASE CREW_USED_PALETO_PACKIE	RETURN 	"PALETO_PACKIE" 	BREAK		
			CASE CREW_USED_PALETO_CHEF		RETURN 	"PALETO_CHEF" 		BREAK		
			CASE CREW_USED_AGENCY_GUSTAV	RETURN 	"AGENCY_GUSTAV" 	BREAK		
			CASE CREW_USED_AGENCY_PACKIE	RETURN 	"AGENCY_PACKIE" 	BREAK		
			CASE CREW_USED_AGENCY_DARYL		RETURN 	"AGENCY_DARYL" 		BREAK		
			CASE CREW_USED_AGENCY_HUGH		RETURN 	"AGENCY_HUGH" 		BREAK		
			CASE CREW_USED_AGENCY_CHRIS		RETURN 	"AGENCY_CHRIS" 		BREAK		
			CASE CREW_USED_AGENCY_RICKIE	RETURN 	"AGENCY_RICKIE" 	BREAK		
			CASE CREW_USED_AGENCY_EDDIE		RETURN 	"AGENCY_EDDIE" 		BREAK		
			CASE CREW_USED_AGENCY_NORM		RETURN 	"AGENCY_NORM" 		BREAK	
			CASE CREW_USED_AGENCY_KARIM		RETURN 	"AGENCY_KARIM" 		BREAK		
			CASE CREW_USED_AGENCY_PAIGE		RETURN 	"AGENCY_PAIGE" 		BREAK	
			CASE CREW_USED_AGENCY_TALINA	RETURN 	"AGENCY_TALINA" 	BREAK	
		ENDSWITCH
		RETURN "ERROR"
	ENDFUNC  
	
	FUNC STRING PRIVATE_Get_Heist_Choice_Debug_String(INT paramHeistChoice)

	SWITCH paramHeistChoice
		CASE HEIST_CHOICE_JEWEL_STEALTH			RETURN "JEWEL_STEALTH"		BREAK
		CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT		RETURN "JEWEL_HIGH_IMPACT"	BREAK
		CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT	RETURN "DOCKS_BLOW_UP_BOAT"	BREAK
		CASE HEIST_CHOICE_DOCKS_DEEP_SEA		RETURN "DOCKS_DEEP_SEA"		BREAK
		CASE HEIST_CHOICE_RURAL_NO_TANK			RETURN "RURAL_NO_TANK"		BREAK
		CASE HEIST_CHOICE_AGENCY_FIRETRUCK		RETURN "AGENCY_FIRETRUCK"	BREAK
		CASE HEIST_CHOICE_AGENCY_HELICOPTER		RETURN "AGENCY_HELICOPTER"	BREAK
		CASE HEIST_CHOICE_FINALE_TRAFFCONT		RETURN "FINALE_TRAFFCONT"	BREAK
		CASE HEIST_CHOICE_FINALE_HELI			RETURN "FINALE_HELI"		BREAK
		CASE HEIST_CHOICE_EMPTY					RETURN "EMPTY"				BREAK
	ENDSWITCH
	
	RETURN "INVALID!"
ENDFUNC

#ENDIF


FUNC INT PRIVATE_Get_Heist_Index_From_Finale_Mission(SP_MISSIONS paramMission)
	#IF NOT USE_SP_DLC
		SWITCH paramMission
			CASE SP_HEIST_JEWELRY_2
				RETURN HEIST_JEWEL
			BREAK
			CASE SP_HEIST_DOCKS_2A
			CASE SP_HEIST_DOCKS_2B
				RETURN HEIST_DOCKS
			BREAK
			CASE SP_HEIST_RURAL_2
				RETURN HEIST_RURAL_BANK
			BREAK
			CASE SP_HEIST_AGENCY_3A
			CASE SP_HEIST_AGENCY_3B
				RETURN HEIST_AGENCY
			BREAK
			CASE SP_HEIST_FINALE_2A
			CASE SP_HEIST_FINALE_2B
				RETURN HEIST_FINALE
			BREAK
		ENDSWITCH
	#ENDIF
	#IF USE_SP_DLC
		UNUSED_PARAMETER(paramMission)
	#ENDIF
	RETURN -1
ENDFUNC


FUNC FLOW_INT_IDS Get_Heist_Choice_FlowInt_For_Heist(INT iHeistIndex)

#IF USE_CLF_DLC
	iHeistIndex = iHeistIndex
	FLOW_INT_IDS eHeistChoiceFlowInt = FLOWINT_NONE
	RETURN eHeistChoiceFlowInt
#ENDIF
#IF USE_NRM_DLC
	iHeistIndex = iHeistIndex
	FLOW_INT_IDS eHeistChoiceFlowInt = FLOWINT_NONE
	RETURN eHeistChoiceFlowInt
#ENDIF

#IF NOT USE_SP_DLC
	FLOW_INT_IDS eHeistChoiceFlowInt
				
	SWITCH(iHeistIndex)
		CASE HEIST_JEWEL
			eHeistChoiceFlowInt = FLOWINT_HEIST_CHOICE_JEWEL
		BREAK
		CASE HEIST_DOCKS
			eHeistChoiceFlowInt = FLOWINT_HEIST_CHOICE_DOCKS
		BREAK		
		CASE HEIST_RURAL_BANK
			eHeistChoiceFlowInt = FLOWINT_HEIST_CHOICE_RURAL
		BREAK
		CASE HEIST_AGENCY
			eHeistChoiceFlowInt = FLOWINT_HEIST_CHOICE_AGENCY
		BREAK	
		CASE HEIST_FINALE
			eHeistChoiceFlowInt = FLOWINT_HEIST_CHOICE_FINALE
		BREAK		
	ENDSWITCH
	
	RETURN eHeistChoiceFlowInt
#ENDIF
	
ENDFUNC


FUNC INT Get_Choice_In_Heist_Choice_Slot(INT iHeist, INT iSlotIndex)

	SWITCH iHeist
		CASE HEIST_JEWEL
			SWITCH iSlotIndex
				CASE 0
					RETURN HEIST_CHOICE_JEWEL_HIGH_IMPACT
				BREAK
				CASE 1
					RETURN HEIST_CHOICE_JEWEL_STEALTH
				BREAK
				DEFAULT
					SCRIPT_ASSERT("Get_Choice_In_Heist_Choice_Slot: Invalid slot index for Jewelry Heist.")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_DOCKS
			SWITCH iSlotIndex
				CASE 0
					RETURN HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
				BREAK
				CASE 1
					RETURN HEIST_CHOICE_DOCKS_DEEP_SEA
				BREAK
				DEFAULT
					SCRIPT_ASSERT("Get_Choice_In_Heist_Choice_Slot: Invalid slot index for Docks Heist.")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_RURAL_BANK
			SWITCH iSlotIndex
				CASE 0
					RETURN HEIST_CHOICE_RURAL_NO_TANK
				BREAK
				DEFAULT
					SCRIPT_ASSERT("Get_Choice_In_Heist_Choice_Slot: Invalid slot index for Rural Bank Heist.")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_AGENCY
			SWITCH iSlotIndex
				CASE 0
					RETURN HEIST_CHOICE_AGENCY_FIRETRUCK
				BREAK
				CASE 1
					RETURN HEIST_CHOICE_AGENCY_HELICOPTER
				BREAK
				DEFAULT
					SCRIPT_ASSERT("Get_Choice_In_Heist_Choice_Slot: Invalid slot index for Agency Heist.")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_FINALE
			SWITCH iSlotIndex
				CASE 0
					RETURN HEIST_CHOICE_FINALE_TRAFFCONT
				BREAK
				CASE 1
					RETURN HEIST_CHOICE_FINALE_HELI
				BREAK
				DEFAULT
					SCRIPT_ASSERT("Get_Choice_In_Heist_Choice_Slot: Invalid slot index for Finale Heist.")
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Choice_In_Heist_Choice_Slot: Invalid heist index.")
	RETURN HEIST_CHOICE_EMPTY
ENDFUNC


FUNC INT Get_Heist_Choice_Base_Index(INT iHeistChoice)

	SWITCH iHeistChoice
		CASE HEIST_CHOICE_JEWEL_STEALTH
		CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
		CASE HEIST_CHOICE_RURAL_NO_TANK
		CASE HEIST_CHOICE_AGENCY_FIRETRUCK
		CASE HEIST_CHOICE_FINALE_TRAFFCONT
			RETURN 0
		BREAK
		
		CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT		
		CASE HEIST_CHOICE_DOCKS_DEEP_SEA			
		CASE HEIST_CHOICE_AGENCY_HELICOPTER
		CASE HEIST_CHOICE_FINALE_HELI
			RETURN 1
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Heist_Choice_Base_Index: The INT passed was not a valid heist choice. Tell BenR.")
	RETURN -1
	
ENDFUNC


FUNC FLOW_INT_IDS Get_Heist_Choice_FlowInt_ID(INT iHeist)
#if USE_CLF_DLC
iHeist = iHeist
#endif
#if USE_NRM_DLC
iHeist = iHeist
#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	SWITCH(iHeist)
		CASE HEIST_JEWEL
			RETURN FLOWINT_HEIST_CHOICE_JEWEL
		BREAK
		CASE HEIST_DOCKS
			RETURN FLOWINT_HEIST_CHOICE_DOCKS
		BREAK
		CASE HEIST_RURAL_BANK
			RETURN FLOWINT_HEIST_CHOICE_RURAL
		BREAK
		CASE HEIST_AGENCY
			RETURN FLOWINT_HEIST_CHOICE_AGENCY
		BREAK
		CASE HEIST_FINALE
			RETURN FLOWINT_HEIST_CHOICE_FINALE
		BREAK
	ENDSWITCH
	#endif
	#endif
	SCRIPT_ASSERT("Get_Heist_Choice_FlowInt_ID: Invalid heist index.")
	RETURN FLOWINT_NONE
ENDFUNC


FUNC FLOW_INT_IDS Get_Heist_Board_Mode_FlowInt_ID(INT iHeist)
#if USE_CLF_DLC
iHeist = iHeist
#endif
#if USE_NRM_DLC
iHeist = iHeist
#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	SWITCH(iHeist)
		CASE HEIST_JEWEL
			RETURN FLOWINT_HEIST_BOARD_MODE_JEWEL
		BREAK
		CASE HEIST_DOCKS
			RETURN FLOWINT_HEIST_BOARD_MODE_DOCKS
		BREAK
		CASE HEIST_RURAL_BANK
			RETURN FLOWINT_HEIST_BOARD_MODE_RURAL
		BREAK
		CASE HEIST_AGENCY
			RETURN FLOWINT_HEIST_BOARD_MODE_AGENCY
		BREAK
		CASE HEIST_FINALE
			RETURN FLOWINT_HEIST_BOARD_MODE_FINALE
		BREAK
	ENDSWITCH
	#endif
	#endif
	SCRIPT_ASSERT("Get_Heist_Board_Mode_FlowInt_ID: Invalid heist index.")
	RETURN FLOWINT_NONE
ENDFUNC


FUNC BOOL Is_Heist_Display_Group_Active(INT iHeist, g_eBoardDisplayGroups eDisplayGroup)
	RETURN IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], ENUM_TO_INT(eDisplayGroup))
ENDFUNC


FUNC STRING PRIVATE_Get_Crew_Member_Name_Label(CrewMember crewMem)
	SWITCH crewMem
		CASE CM_GUNMAN_G_GUSTAV
			RETURN "HC_N_GUS"
		BREAK
		CASE CM_GUNMAN_G_KARL
			RETURN "HC_N_KAR"
		BREAK
		CASE CM_GUNMAN_G_PACKIE_UNLOCK
			RETURN "HC_N_PAC"
		BREAK
		CASE CM_GUNMAN_G_CHEF_UNLOCK
			RETURN "HC_N_CHE"
		BREAK
		CASE CM_GUNMAN_M_HUGH
			RETURN "HC_N_HUG"
		BREAK
		CASE CM_GUNMAN_B_NORM
			RETURN "HC_N_NOR"
		BREAK
		CASE CM_GUNMAN_B_DARYL
			RETURN "HC_N_DAR"
		BREAK
		CASE CM_HACKER_G_PAIGE
			RETURN "HC_N_PAI"
		BREAK
		CASE CM_HACKER_M_CHRIS
			RETURN "HC_N_CHR"
		BREAK
		CASE CM_HACKER_B_RICKIE_UNLOCK
			RETURN "HC_N_RIC"
		BREAK
		CASE CM_DRIVER_G_EDDIE
			RETURN "HC_N_EDD"
		BREAK
		CASE CM_DRIVER_G_TALINA_UNLOCK
			RETURN "HC_N_TAL"
		BREAK
		CASE CM_DRIVER_B_KARIM
			RETURN "HC_N_KRM"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("PRIVATE_Get_Crew_Member_Name_Label: Invalid crew member enum. Bug BenR.")
	RETURN "ERROR!"
ENDFUNC


FUNC STRING PRIVATE_Get_Crew_Member_First_Name_Label(CrewMember crewMem)
	SWITCH crewMem
		CASE CM_GUNMAN_G_GUSTAV
			RETURN "HC_FN_GUS"
		BREAK
		CASE CM_GUNMAN_G_KARL
			RETURN "HC_FN_KAR"
		BREAK
		CASE CM_GUNMAN_G_PACKIE_UNLOCK
			RETURN "HC_FN_PAC"
		BREAK
		CASE CM_GUNMAN_G_CHEF_UNLOCK
			RETURN "HC_FN_CHE"
		BREAK
		CASE CM_GUNMAN_M_HUGH
			RETURN "HC_FN_HUG"
		BREAK
		CASE CM_GUNMAN_B_NORM
			RETURN "HC_FN_NOR"
		BREAK
		CASE CM_GUNMAN_B_DARYL
			RETURN "HC_FN_DAR"
		BREAK
		
		CASE CM_HACKER_G_PAIGE
			RETURN "HC_FN_PAI"
		BREAK
		CASE CM_HACKER_M_CHRIS
			RETURN "HC_FN_CHR"
		BREAK
		CASE CM_HACKER_B_RICKIE_UNLOCK
			RETURN "HC_FN_RIC"
		BREAK
		CASE CM_DRIVER_G_EDDIE
			RETURN "HC_FN_EDD"
		BREAK
		CASE CM_DRIVER_G_TALINA_UNLOCK
			RETURN "HC_FN_TAL"
		BREAK
		CASE CM_DRIVER_B_KARIM
			RETURN "HC_FN_KRM"
		BREAK
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_Get_Crew_Member_First_Name_Label: Invalid crew member enum. Bug BenR.")
	RETURN "ERROR!"
ENDFUNC


FUNC STRING PRIVATE_Get_Heist_Choice_Name_Label(INT iHeistChoice)
	SWITCH iHeistChoice
		CASE HEIST_CHOICE_JEWEL_STEALTH			RETURN "HC_J_STEALTH"		BREAK
		CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT		RETURN "HC_J_IMPACT"		BREAK	
		CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT	RETURN "HC_D_BLOW_UP"		BREAK
		CASE HEIST_CHOICE_DOCKS_DEEP_SEA		RETURN "HC_D_DEEP_SEA"		BREAK
		CASE HEIST_CHOICE_RURAL_NO_TANK			RETURN "HC_R_NO_TANK"		BREAK
		CASE HEIST_CHOICE_AGENCY_FIRETRUCK		RETURN "HC_A_FIRETRUCK"		BREAK
		CASE HEIST_CHOICE_AGENCY_HELICOPTER		RETURN "HC_A_HELICOPTER"	BREAK
		CASE HEIST_CHOICE_FINALE_TRAFFCONT		RETURN "HC_F_TRAFFCONT"		BREAK
		CASE HEIST_CHOICE_FINALE_HELI			RETURN "HC_F_HELI"			BREAK
	ENDSWITCH

	SCRIPT_ASSERT("PRIVATE_Get_Heist_Choice_Name_Label: Invalid heist choice int. Bug BenR.")
	RETURN "ERROR!"
ENDFUNC


PROC PRIVATE_Clear_Crew_Used_Flags_For_Heist(INT iHeist)
	SWITCH iHeist
		CASE HEIST_JEWEL
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_GUSTAV)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PACKIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_CHRIS)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_RICKIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PAIGE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_EDDIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_KARIM)
		BREAK
		
		CASE HEIST_RURAL_BANK
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_GUSTAV)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_PACKIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_CHEF)
		BREAK
		
		CASE HEIST_AGENCY
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_GUSTAV)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_PACKIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_DARYL)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_HUGH)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_CHRIS)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_RICKIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_EDDIE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_NORM)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_KARIM)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_PAIGE)
			CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_TALINA)
		BREAK
	ENDSWITCH
ENDPROC


PROC PRIVATE_Update_Crew_Used_Flags_For_Heist(INT iHeist)
#if USE_CLF_DLC
iHeist = iHeist
#endif
#if USE_NRM_DLC
iHeist = iHeist
#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
	INT iCrewIndex
	INT iHeistChoice
	
	PRIVATE_Clear_Crew_Used_Flags_For_Heist(iHeist)
	
	SWITCH iHeist
		CASE HEIST_JEWEL
			CDEBUG1LN(DEBUG_HEIST, "Updating used crew member flags for HEIST_JEWEL.")
			iHeistChoice = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_JEWEL]
			REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
				SWITCH g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]
					CASE CM_GUNMAN_G_GUSTAV
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_GUSTAV)
					BREAK
					CASE CM_GUNMAN_G_PACKIE_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PACKIE)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_PACKIE))
					BREAK
					CASE CM_HACKER_M_CHRIS
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_CHRIS)
					BREAK
					CASE CM_HACKER_B_RICKIE_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_RICKIE)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_RICKIE))
					BREAK
					CASE CM_HACKER_G_PAIGE
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_PAIGE)
					BREAK
					CASE CM_DRIVER_G_EDDIE
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_EDDIE)
					BREAK
					CASE CM_DRIVER_B_KARIM
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_JEWEL_KARIM)
					BREAK
				ENDSWITCH
				SET_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]))
			ENDREPEAT
		BREAK
		
		CASE HEIST_RURAL_BANK
			CDEBUG1LN(DEBUG_HEIST, "Updating used crew member flags for HEIST_RURAL_BANK.")
			iHeistChoice = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_RURAL]
			REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
				SWITCH g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]
					CASE CM_GUNMAN_G_GUSTAV
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_GUSTAV)
					BREAK
					CASE CM_GUNMAN_G_PACKIE_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_PACKIE)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_PACKIE))
					BREAK
					CASE CM_GUNMAN_G_CHEF_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_PALETO_CHEF)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_CHEF))
					BREAK
				ENDSWITCH
				SET_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]))
			ENDREPEAT
		BREAK
		
		CASE HEIST_AGENCY
			CDEBUG1LN(DEBUG_HEIST, "Updating used crew member flags for HEIST_AGENCY.")
			iHeistChoice = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_AGENCY]
			REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
				SWITCH g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]
					CASE CM_GUNMAN_G_GUSTAV
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_GUSTAV)
					BREAK
					CASE CM_GUNMAN_G_PACKIE_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_PACKIE)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_PACKIE))
					BREAK
					CASE CM_GUNMAN_B_DARYL
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_DARYL)
					BREAK
					CASE CM_GUNMAN_M_HUGH
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_HUGH)
					BREAK
					CASE CM_GUNMAN_B_NORM
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_NORM)
					BREAK
					CASE CM_HACKER_M_CHRIS
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_CHRIS)
					BREAK
					CASE CM_HACKER_B_RICKIE_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_RICKIE)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_RICKIE))
					BREAK
					CASE CM_HACKER_G_PAIGE
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_PAIGE)
					BREAK
					CASE CM_DRIVER_G_EDDIE
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_EDDIE)
					BREAK
					CASE CM_DRIVER_B_KARIM
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_KARIM)
					BREAK
					CASE CM_DRIVER_G_TALINA_UNLOCK
						SET_BIT(g_savedGlobals.sHeistData.iCrewUsedBitset, CREW_USED_AGENCY_TALINA)
						CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(CRDS_MET_TALINA))
					BREAK
				ENDSWITCH
				SET_BIT(g_savedGlobals.sHeistData.iCrewDialogueBitset, ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]))
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CDEBUG3LN(DEBUG_HEIST, "CREW DIALOGUE BITS")
		INT iBitIndex
		REPEAT CRDS_MAX_SLOTS iBitIndex
			IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDialogueBitset, iBitIndex)
				CDEBUG3LN(DEBUG_HEIST, "Bit ", PRIVATE_Get_Crew_Dialogue_Debug_String(INT_TO_ENUM(CrewDialogueSlot, iBitIndex)), " TRUE.")
			ELSE
				CDEBUG3LN(DEBUG_HEIST, "Bit ", PRIVATE_Get_Crew_Dialogue_Debug_String(INT_TO_ENUM(CrewDialogueSlot, iBitIndex)), " FALSE.")
			ENDIF
		ENDREPEAT
		
		CDEBUG3LN(DEBUG_HEIST, "CREW USED BITS")
		REPEAT CREW_USED_MAX iBitIndex
			IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUsedBitset, iBitIndex)
				CDEBUG3LN(DEBUG_HEIST, "Bit ", PRIVATE_Get_Crew_Used_Bit_Debug_String(iBitIndex), " TRUE.")
			ELSE
				CDEBUG3LN(DEBUG_HEIST, "Bit ", PRIVATE_Get_Crew_Used_Bit_Debug_String(iBitIndex), " FALSE.")
			ENDIF
		ENDREPEAT
	#ENDIF

	#endif //DLC check
	#endif
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡  Heist Social Club Stats ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
	FUNC STRING PRIVATE_Get_Heist_Choice_Stat_Debug_String(STATSENUM paramHeistChoiceStat)
		SWITCH paramHeistChoiceStat
			CASE HCS_JEWEL_GAMEPLAY_CHOICE		RETURN "HCS_JEWEL_GAMEPLAY_CHOICE"	BREAK
			CASE HCS_JEWEL_CREW1_CHOICE			RETURN "HCS_JEWEL_CREW1_CHOICE"		BREAK
			CASE HCS_JEWEL_CREW2_CHOICE			RETURN "HCS_JEWEL_CREW2_CHOICE"		BREAK
			CASE HCS_JEWEL_CREW3_CHOICE			RETURN "HCS_JEWEL_CREW3_CHOICE"		BREAK
			CASE HCS_PORT_GAMEPLAY_CHOICE		RETURN "HCS_PORT_GAMEPLAY_CHOICE"	BREAK
			CASE HCS_PALETO_CREW1_CHOICE		RETURN "HCS_PALETO_CREW1_CHOICE"	BREAK
			CASE HCS_BUREAU_GAMEPLAY_CHOICE		RETURN "HCS_BUREAU_GAMEPLAY_CHOICE"	BREAK
			CASE HCS_BUREAU_CREW1_CHOICE		RETURN "HCS_BUREAU_CREW1_CHOICE"	BREAK
			CASE HCS_BUREAU_CREW2_CHOICE		RETURN "HCS_BUREAU_CREW2_CHOICE"	BREAK
			CASE HCS_BUREAU_CREW3_CHOICE		RETURN "HCS_BUREAU_CREW3_CHOICE"	BREAK
			CASE HCS_BIGS_GAMEPLAY_CHOICE		RETURN "HCS_BIGS_GAMEPLAY_CHOICE"	BREAK
			CASE HCS_BIGS_CREW1_CHOICE			RETURN "HCS_BIGS_CREW1_CHOICE"		BREAK
			CASE HCS_BIGS_CREW2_CHOICE			RETURN "HCS_BIGS_CREW2_CHOICE"		BREAK
			CASE HCS_BIGS_CREW3_CHOICE			RETURN "HCS_BIGS_CREW3_CHOICE"		BREAK
			CASE HCS_BIGS_CREW4_CHOICE			RETURN "HCS_BIGS_CREW4_CHOICE"		BREAK
			CASE HCS_BIGS_CREW5_CHOICE			RETURN "HCS_BIGS_CREW5_CHOICE"		BREAK
		ENDSWITCH
		SCRIPT_ASSERT("PRIVATE_Get_Heist_Choice_Stat_Debug_String: Passed stat was not a heist choice stat.")
		RETURN "ERROR!"
	ENDFUNC
#ENDIF


FUNC STATSENUM PRIVATE_Get_Heist_Gameplay_Choice_Stat(INT paramHeist)
	SWITCH paramHeist
		CASE HEIST_JEWEL	RETURN HCS_JEWEL_GAMEPLAY_CHOICE	BREAK
		CASE HEIST_DOCKS	RETURN HCS_PORT_GAMEPLAY_CHOICE		BREAK
		CASE HEIST_AGENCY	RETURN HCS_BUREAU_GAMEPLAY_CHOICE	BREAK
		CASE HEIST_FINALE	RETURN HCS_BIGS_GAMEPLAY_CHOICE		BREAK
	ENDSWITCH
	RETURN CITIES_PASSED //Using this as a NULL value.
ENDFUNC


FUNC STATSENUM PRIVATE_Get_Heist_Crew_Choice_Stat_For_Index(INT paramHeist, INT paramCrewIndex)
	SWITCH paramHeist
		CASE HEIST_JEWEL	
			SWITCH paramCrewIndex
				CASE 0	RETURN HCS_JEWEL_CREW1_CHOICE	BREAK
				CASE 1	RETURN HCS_JEWEL_CREW2_CHOICE	BREAK
				CASE 2	RETURN HCS_JEWEL_CREW3_CHOICE	BREAK
			ENDSWITCH
		BREAK
		CASE HEIST_RURAL_BANK
			SWITCH paramCrewIndex
				CASE 0	RETURN HCS_PALETO_CREW1_CHOICE	BREAK
			ENDSWITCH
		BREAK
		CASE HEIST_AGENCY
			SWITCH paramCrewIndex
				CASE 0	RETURN HCS_BUREAU_CREW1_CHOICE	BREAK
				CASE 1	RETURN HCS_BUREAU_CREW2_CHOICE	BREAK
				CASE 2	RETURN HCS_BUREAU_CREW3_CHOICE	BREAK
			ENDSWITCH
		BREAK
		CASE HEIST_FINALE
			SWITCH paramCrewIndex
				CASE 0	RETURN HCS_BIGS_CREW1_CHOICE	BREAK
				CASE 1	RETURN HCS_BIGS_CREW2_CHOICE	BREAK
				CASE 2	RETURN HCS_BIGS_CREW3_CHOICE	BREAK
				CASE 3	RETURN HCS_BIGS_CREW4_CHOICE	BREAK
				CASE 4	RETURN HCS_BIGS_CREW5_CHOICE	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN CITIES_PASSED //Using this as a NULL value.
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡  Crew Stat Management ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC STRING PRIVATE_Get_Crew_Type_Stat_Name_Label_At_Index(CrewMemberType paramCrewType, INT paramStatIndex)
	SWITCH paramCrewType
		CASE CMT_GUNMAN
			SWITCH paramStatIndex
				CASE 0	RETURN "HC_STA_G1"	BREAK
				CASE 1	RETURN "HC_STA_G2"	BREAK
				CASE 2	RETURN "HC_STA_G3"	BREAK
				CASE 3	RETURN "HC_STA_G4"	BREAK
			ENDSWITCH
		BREAK
		CASE CMT_HACKER
			SWITCH paramStatIndex
				CASE 0	RETURN "HC_STA_H1"	BREAK
				CASE 1	RETURN "HC_STA_H2"	BREAK
				CASE 2	RETURN "HC_STA_H3"	BREAK
			ENDSWITCH
		BREAK
		CASE CMT_DRIVER
			SWITCH paramStatIndex
				CASE 0	RETURN "HC_STA_D1"	BREAK
				CASE 1	RETURN "HC_STA_D2"	BREAK
				CASE 2	RETURN "HC_STA_D3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("PRIVATE_Get_Crew_Type_Stat_Name_Label_At_Index: Crew type doesn't have a stat at the specified index.")
	RETURN "ERROR!"
ENDFUNC


PROC PRIVATE_Store_Crew_Member_Stat_At_Index_Local(INT paramStatIndex, INT paramStatValue, INT &paramStatsA, INT &paramStatsB)
	SWITCH paramStatIndex
		CASE 0
			paramStatsA -= paramStatsA & CREW_STAT1_BITMASK
			paramStatsA |= paramStatValue
			EXIT
		BREAK
		CASE 1
			paramStatsA -= paramStatsA & CREW_STAT2_BITMASK
			paramStatsA |= SHIFT_LEFT(paramStatValue, CREW_STAT2_SHIFT)
			EXIT	
		BREAK
		CASE 2
			paramStatsB -= paramStatsB & CREW_STAT1_BITMASK
			paramStatsB |= paramStatValue
			EXIT
		BREAK
		CASE 3
			paramStatsB -= paramStatsB & CREW_STAT2_BITMASK
			paramStatsB |= SHIFT_LEFT(paramStatValue, CREW_STAT2_SHIFT)
			EXIT
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("PRIVATE_Store_Crew_Member_Stat_At_Index_Local: Invalid stat index passed. Index must be in range 0->3.")
ENDPROC


PROC PRIVATE_Store_Crew_Member_Stat_At_Index(CrewMember paramCrewMember, INT paramStatIndex, INT paramStatValue)
	PRIVATE_Store_Crew_Member_Stat_At_Index_Local(	paramStatIndex,
													paramStatValue,
													g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].statsA,
													g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].statsB)
ENDPROC


FUNC INT PRIVATE_Get_Crew_Member_Stat_At_Index_Local(INT paramStatIndex, INT paramStatsA, INT paramStatsB)
	SWITCH paramStatIndex
		CASE 0
		CASE 1
			RETURN SHIFT_RIGHT(paramStatsA, 15*paramStatIndex) & CREW_STAT1_BITMASK
		BREAK
		CASE 2
		CASE 3
			RETURN SHIFT_RIGHT(paramStatsB, 15*(paramStatIndex-2)) & CREW_STAT1_BITMASK
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("PRIVATE_Get_Crew_Member_Stat_At_Index_Local: Invalid stat index passed. Index must be in range 0->3.")
	RETURN -1
ENDFUNC


FUNC INT PRIVATE_Get_Crew_Member_Stat_At_Index(CrewMember paramCrewMember, INT paramStatIndex)
	RETURN PRIVATE_Get_Crew_Member_Stat_At_Index_Local(	paramStatIndex, 
														g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].statsA,
														g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].statsB)
ENDFUNC


FUNC INT PRIVATE_Get_Gunman_Stat_Max_Value(CrewGunmanStat paramStat)
	SWITCH paramStat
		CASE CGS_MAX_HEALTH			RETURN 1000		BREAK
		CASE CGS_ACCURACY			RETURN 100		BREAK	
		CASE CGS_SHOOT_RATE			RETURN 100		BREAK	
		CASE CGS_WEAPON_CHOICE		RETURN 100		BREAK	
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_Get_Gunman_Stat_Max_Value: Undefined gunman stat passed.")
	RETURN 0
ENDFUNC


FUNC INT PRIVATE_Get_Hacker_Stat_Max_Value(CrewHackerStat paramStat)
	SWITCH paramStat
		CASE CHS_SYS_KNOWLEDGE			RETURN 100		BREAK
		CASE CHS_DECRYPT_SKILL			RETURN 100		BREAK	
		CASE CHS_ACCESS_SPEED			RETURN 100		BREAK	
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_Get_Hacker_Stat_Max_Value: Undefined hacker stat passed.")
	RETURN 0
ENDFUNC


FUNC INT PRIVATE_Get_Driver_Stat_Max_Value(CrewDriverStat paramStat)
	SWITCH paramStat
		CASE CDS_DRIVING_SKILL			RETURN 100		BREAK
		CASE CDS_COMPOSURE				RETURN 100		BREAK	
		CASE CDS_VEHICLE_CHOICE			RETURN 100		BREAK	
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_Get_Driver_Stat_Max_Value: Undefined driver stat passed.")
	RETURN 0
ENDFUNC


PROC PRIVATE_Update_Crew_Member_Skill_Level_From_Stats(CrewMember paramCrewMember)

	//If the crew member is already at the highest skill level, no point in recalculating.
	IF g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].skill = CMSK_GOOD
		EXIT
	ENDIF
	
	FLOAT fAverageStatPercentage = 0.0
	INT iStat
	
	SWITCH g_sCrewMemberStaticData[paramCrewMember].type
		CASE CMT_GUNMAN
			REPEAT 4 iStat
				fAverageStatPercentage += (TO_FLOAT(PRIVATE_Get_Crew_Member_Stat_At_Index(paramCrewMember, iStat)) / TO_FLOAT(PRIVATE_Get_Gunman_Stat_Max_Value(INT_TO_ENUM(CrewGunmanStat, iStat))) * 100.0) 
			ENDREPEAT
			fAverageStatPercentage /= 4.0
		BREAK
		
		CASE CMT_HACKER
			REPEAT 3 iStat
				fAverageStatPercentage += (TO_FLOAT(PRIVATE_Get_Crew_Member_Stat_At_Index(paramCrewMember, iStat)) / TO_FLOAT(PRIVATE_Get_Hacker_Stat_Max_Value(INT_TO_ENUM(CrewHackerStat, iStat))) * 100.0) 
			ENDREPEAT
			fAverageStatPercentage /= 3.0
		BREAK
		
		CASE CMT_DRIVER
			REPEAT 3 iStat
				fAverageStatPercentage += (TO_FLOAT(PRIVATE_Get_Crew_Member_Stat_At_Index(paramCrewMember, iStat)) / TO_FLOAT(PRIVATE_Get_Driver_Stat_Max_Value(INT_TO_ENUM(CrewDriverStat, iStat))) * 100.0) 
			ENDREPEAT
			fAverageStatPercentage /= 3.0
		BREAK
	ENDSWITCH
	CPRINTLN(DEBUG_HEIST, "<CREWSTAT> ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_First_Name_Label(paramCrewMember)), "'s average stat percentage is now ", fAverageStatPercentage, ".")
	
	IF fAverageStatPercentage > 66.6 
		IF g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].skill < CMSK_GOOD
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_First_Name_Label(paramCrewMember)), "'s base skill level increased to CMSK_GOOD.")
			g_savedGlobals.sHeistData.sCrewActiveData[paramCrewMember].skill = CMSK_GOOD
		ENDIF
	ELIF fAverageStatPercentage > 33.3 
		IF g_SavedGlobals.sHeistData.sCrewActiveData[paramCrewMember].skill < CMSK_MEDIUM
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_First_Name_Label(paramCrewMember)), "'s base skill level increased to CMSK_MEDIUM.")
			g_savedGlobals.sHeistData.sCrewActiveData[paramCrewMember].skill = CMSK_MEDIUM
		ENDIF
	ENDIF
ENDPROC


PROC PRIVATE_Update_Heist_Choice_Stat_From_FlowInt(INT paramHeist)
#if USE_CLF_DLC
paramHeist = paramHeist
#endif
#if USE_NRM_DLC
paramHeist = paramHeist
#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	INT iChoiceValue
	SWITCH paramHeist
		CASE HEIST_JEWEL
			iChoiceValue = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_JEWEL]
			IF iChoiceValue = HEIST_CHOICE_JEWEL_STEALTH
				CPRINTLN(DEBUG_HEIST, "Setting SP_HEIST_CHOSE_JEWEL_STEALTH stat to TRUE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_JEWEL_STEALTH, TRUE)
			ELSE
				CPRINTLN(DEBUG_HEIST, "Setting SP_HEIST_CHOSE_JEWEL_STEALTH stat to FALSE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_JEWEL_STEALTH, FALSE)
			ENDIF
		BREAK
		
		CASE HEIST_DOCKS
			iChoiceValue = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_DOCKS]
			IF iChoiceValue = HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
				CPRINTLN(DEBUG_HEIST, "Setting SP_HEIST_CHOSE_DOCKS_SINK_SHIP stat to TRUE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_DOCKS_SINK_SHIP, TRUE)
			ELSE
				CPRINTLN(DEBUG_HEIST, "Setting SP_HEIST_CHOSE_DOCKS_SINK_SHIP stat to FALSE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_DOCKS_SINK_SHIP, FALSE)
			ENDIF
		BREAK
		
		CASE HEIST_AGENCY
			iChoiceValue = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_AGENCY]
			IF iChoiceValue = HEIST_CHOICE_AGENCY_FIRETRUCK
				CPRINTLN(DEBUG_HEIST, "Setting SP_HEIST_CHOSE_BUREAU_FIRECREW stat to TRUE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_BUREAU_FIRECREW, TRUE)
			ELSE
				CPRINTLN(DEBUG_HEIST, "Setting SP_HEIST_CHOSE_BUREAU_FIRECREW stat to FALSE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_BUREAU_FIRECREW, FALSE)
			ENDIF
		BREAK
		
		CASE HEIST_FINALE
			iChoiceValue = g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_FINALE]
			IF iChoiceValue = HEIST_CHOICE_FINALE_TRAFFCONT
				CPRINTLN(DEBUG_HEIST, "Setting HEIST_CHOICE_FINALE_TRAFFCONT stat to TRUE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_BIGS_TRAFFIC, TRUE)
			ELSE
				CPRINTLN(DEBUG_HEIST, "Setting HEIST_CHOICE_FINALE_TRAFFCONT stat to FALSE.")
				STAT_SET_BOOL(SP_HEIST_CHOSE_BIGS_TRAFFIC, FALSE)
			ENDIF
		BREAK
	ENDSWITCH
	#endif
	#endif
ENDPROC


PROC PRIVATE_Display_Feed_For_All_Crew_Member_Stat_Increases(INT iHeistChoice, INT iAliveCrewCount)

	IF iAliveCrewCount != 0
		INT iCrewIndex
		TEXT_LABEL_23 txtCrewFeedLabel = "FEED_CREW_S"
		txtCrewFeedLabel += iAliveCrewCount
		
		BEGIN_TEXT_COMMAND_THEFEED_POST(txtCrewFeedLabel)
		REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
			CrewMember crewMem = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]
			IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDeadBitset, ENUM_TO_INT(crewMem))
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(PRIVATE_Get_Crew_Member_Name_Label(crewMem))
			ENDIF
		ENDREPEAT
		END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
	
		#IF NOT USE_CLF_DLC
		#IF NOT USE_NRM_DLC
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_HEIST_CREW_SKILLUP)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("AM_H_CRWLVL")
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_HEIST_CREW_SKILLUP)
				ENDIF
			ENDIF
		#ENDIF
		#ENDIF
			
	ENDIF
	
ENDPROC


PROC PRIVATE_Display_Feed_For_Crew_Member_Stat_Increase(CrewMember paramCrewMember, CrewMemberType paramCrewType, INT paramCrewIndex)
	STRING strCrewName = PRIVATE_Get_Crew_Member_Name_Label(paramCrewMember)
	
	FEED_TEXT_ICON eIcon
	SWITCH paramCrewType
		CASE CMT_GUNMAN		eIcon = TEXT_ICON_SHOOTER	BREAK
		CASE CMT_DRIVER		eIcon = TEXT_ICON_DRIVER	BREAK
		CASE CMT_HACKER		eIcon = TEXT_ICON_HACKER	BREAK
	ENDSWITCH
	
	UNUSED_PARAMETER(paramCrewIndex)
	
	BEGIN_TEXT_COMMAND_THEFEED_POST("FEED_CREW_S1")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strCrewName)
	END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(strCrewName, strCrewName, TRUE, eIcon, "")

	#IF NOT USE_CLF_DLC
	#IF NOT USE_NRM_DLC	
		IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_HEIST_CREW_SKILLUP)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("AM_H_CRWLVL")
				SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_HEIST_CREW_SKILLUP)
			ENDIF
		ENDIF
	#ENDIF
	#ENDIF
		
ENDPROC


PROC PRIVATE_Display_Feed_For_Gunman_Stat_Increases(CrewMember paramCrewMember, INT paramCrewIndex)
	PRIVATE_Display_Feed_For_Crew_Member_Stat_Increase(paramCrewMember, CMT_GUNMAN, paramCrewIndex)
ENDPROC


PROC PRIVATE_Display_Feed_For_Driver_Stat_Increases(CrewMember paramCrewMember, INT paramCrewIndex)
	PRIVATE_Display_Feed_For_Crew_Member_Stat_Increase(paramCrewMember, CMT_DRIVER, paramCrewIndex)
ENDPROC


PROC PRIVATE_Display_Feed_For_Hacker_Stat_Increases(CrewMember paramCrewMember, INT paramCrewIndex)
	PRIVATE_Display_Feed_For_Crew_Member_Stat_Increase(paramCrewMember, CMT_HACKER, paramCrewIndex)
ENDPROC


PROC PRIVATE_Prime_Heist_Crew_Stat_Tracking_On_Mission_Start(SP_MISSIONS paramMission)
#if USE_CLF_DLC
paramMission = paramMission
#endif
#if USE_NRM_DLC
paramMission = paramMission
#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF NOT g_bHeistTempCrewStatsPrimed
	
		INT iHeist = PRIVATE_Get_Heist_Index_From_Finale_Mission(paramMission)
		FLOW_INT_IDS eHeistChoiceID = Get_Heist_Choice_FlowInt_For_Heist(iHeist)
		
		//Double check the Paleto Score choice has been set.
		IF paramMission = SP_HEIST_RURAL_2
			IF g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_RURAL] != HEIST_CHOICE_RURAL_NO_TANK
				g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_RURAL] = HEIST_CHOICE_RURAL_NO_TANK
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_sMissionStaticData[paramMission].settingsBitset, MF_INDEX_IS_HEIST)
			
			IF iHeist != -1
				CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Priming heist crew member stat tracking as a heist starts.")
				INT iHeistChoice = g_savedGlobals.sFlow.controls.intIDs[eHeistChoiceID]
				INT iCrewIndex
				REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
					CrewMember eCrewMember = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]
					g_sHeistTempStats[iCrewIndex].statsIncreased = FALSE
					g_sHeistTempStats[iCrewIndex].statsA = g_SavedGlobals.sHeistData.sCrewActiveData[eCrewMember].statsA
					g_sHeistTempStats[iCrewIndex].statsB = g_SavedGlobals.sHeistData.sCrewActiveData[eCrewMember].statsB
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Starting stats stored for ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_First_Name_Label(eCrewMember)), ".")
						INT iTemp
						REPEAT 4 iTemp
							IF (iTemp < 3) OR (g_sCrewMemberStaticData[eCrewMember].type = CMT_GUNMAN)
								CPRINTLN(DEBUG_HEIST, "<CREWSTAT> ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Type_Stat_Name_Label_At_Index(g_sCrewMemberStaticData[eCrewMember].type, iTemp)), " = ", PRIVATE_Get_Crew_Member_Stat_At_Index_Local(iTemp, g_sHeistTempStats[iCrewIndex].statsA, g_sHeistTempStats[iCrewIndex].statsB))
							ENDIF
						ENDREPEAT
					#ENDIF
				ENDREPEAT
				CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Heist crew member stats flagged as primed.")
				g_bHeistTempCrewStatsPrimed = TRUE
			ELSE
				SCRIPT_ASSERT("FINALISE_HEIST_CREW_STAT_INCREASES_ON_MISSION_PASS: A mission is flagged with MF_IS_HEIST when it isn't a heist finale.")
			ENDIF
		ENDIF
	ENDIF
	
	#endif //DLC check
	#endif
ENDPROC


PROC PRIVATE_Finalise_Heist_Crew_Stat_Increments_On_Mission_Pass(SP_MISSIONS paramMission)
	IF IS_BIT_SET(g_sMissionStaticData[paramMission].settingsBitset, MF_INDEX_IS_HEIST)
		INT iHeist = PRIVATE_Get_Heist_Index_From_Finale_Mission(paramMission)

		IF iHeist != -1
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Finalising heist crew member stats as a heist passes.")
			INT iHeistChoice = g_savedGlobals.sFlow.controls.intIDs[Get_Heist_Choice_FlowInt_For_Heist(iHeist)]
			INT iCrewIndex
			REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
				CrewMember eCrewMember = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]
				IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDeadBitset, ENUM_TO_INT(eCrewMember))
					CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Finalising stats for ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_First_Name_Label(eCrewMember)), ".")
					g_SavedGlobals.sHeistData.sCrewActiveData[eCrewMember].statsA = g_sHeistTempStats[iCrewIndex].statsA
					g_SavedGlobals.sHeistData.sCrewActiveData[eCrewMember].statsB = g_sHeistTempStats[iCrewIndex].statsB
					PRIVATE_Update_Crew_Member_Skill_Level_From_Stats(eCrewMember)
				ENDIF
			ENDREPEAT
		ELSE
			SCRIPT_ASSERT("FINALISE_HEIST_CREW_STAT_INCREASES_ON_MISSION_PASS: A mission is flagged with MF_IS_HEIST when it isn't a heist finale.")
		ENDIF
	ENDIF
	IF g_bHeistTempCrewStatsPrimed
		CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Clearing heist crew member stat prime as we finalise stats.")
		g_bHeistTempCrewStatsPrimed = FALSE
	ENDIF
ENDPROC


PROC PRIVATE_Increment_Single_Gunman_Stat_During_Heist(INT heistCrewIndex, CrewGunmanStat stat)
	INT iOldValue = PRIVATE_Get_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
	INT iMaxValue = PRIVATE_Get_Gunman_Stat_Max_Value(stat)
	
	IF iOldValue < iMaxValue
		INT iIncrement = ROUND(TO_FLOAT(iMaxValue) * 0.25)
		INT iNewValue = iOldValue + iIncrement
		
		IF iNewValue > iMaxValue
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Old:", iOldValue, " Adding:", iIncrement, " New:", iMaxValue, ". (Maxed)")
			PRIVATE_Store_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), iMaxValue, g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
		ELSE
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Old:", iOldValue, " Adding:", iIncrement, " New:", iNewValue, ".")
			PRIVATE_Store_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), iNewValue, g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
		ENDIF
	ENDIF
ENDPROC


PROC PRIVATE_Increment_Single_Hacker_Stat_During_Heist(INT heistCrewIndex, CrewHackerStat stat)
	INT iOldValue = PRIVATE_Get_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
	INT iMaxValue = PRIVATE_Get_Hacker_Stat_Max_Value(stat)
	
	IF iOldValue < iMaxValue
		INT iIncrement = ROUND(TO_FLOAT(iMaxValue) * 0.25)
		INT iNewValue = iOldValue + iIncrement
		
		IF iNewValue > iMaxValue
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Old:", iOldValue, " Adding:", iIncrement, " New:", iMaxValue, ". (Maxed)")
			PRIVATE_Store_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), iMaxValue, g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
		ELSE
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Old:", iOldValue, " Adding:", iIncrement, " New:", iNewValue, ".")
			PRIVATE_Store_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), iNewValue, g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
		ENDIF
	ENDIF
ENDPROC


PROC PRIVATE_Increment_Single_Driver_Stat_During_Heist(INT heistCrewIndex, CrewDriverStat stat)
	INT iOldValue = PRIVATE_Get_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
	INT iMaxValue = PRIVATE_Get_Driver_Stat_Max_Value(stat)
	
	IF iOldValue < iMaxValue
		INT iIncrement = ROUND(TO_FLOAT(iMaxValue) * 0.25)
		INT iNewValue = iOldValue + iIncrement
		
		IF iNewValue > iMaxValue
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Old:", iOldValue, " Adding:", iIncrement, " New:", iMaxValue, ". (Maxed)")
			PRIVATE_Store_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), iMaxValue, g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
		ELSE
			CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Old:", iOldValue, " Adding:", iIncrement, " New:", iNewValue, ".")
			PRIVATE_Store_Crew_Member_Stat_At_Index_Local(ENUM_TO_INT(stat), iNewValue, g_sHeistTempStats[heistCrewIndex].statsA, g_sHeistTempStats[heistCrewIndex].statsB)
		ENDIF
	ENDIF
ENDPROC


PROC PRIVATE_Set_Valid_Crew_Member_For_Heist_Choice_Slot(INT heistChoice, INT slot)

	CrewMember eCrewRandom
	BOOL bFoundMatch = FALSE

	//Search for a random crew member of the correct type to fill this slot.
	WHILE NOT bFoundMatch
		eCrewRandom = INT_TO_ENUM(CrewMember, GET_RANDOM_INT_IN_RANGE(1, ENUM_TO_INT(CM_MAX_CREW_MEMBERS)))
		bFoundMatch = TRUE
		
		//Is this crew member the correct type?
		IF g_sCrewMemberStaticData[eCrewRandom].type != g_sHeistChoiceData[heistChoice].eCrewType[slot]
			bFoundMatch = FALSE
		ENDIF
		
		//Is this crew member unlocked?
		IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUnlockedBitset, ENUM_TO_INT(eCrewRandom))
			bFoundMatch = FALSE
		ENDIF
		
		//Is this crew member dead?
		IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDeadBitset, ENUM_TO_INT(eCrewRandom))
			bFoundMatch = FALSE
		ENDIF
		
		//Special case. Block using Chef on the Agency Heist.
		IF eCrewRandom = CM_GUNMAN_G_CHEF_UNLOCK
			IF heistChoice = HEIST_CHOICE_AGENCY_FIRETRUCK
			OR heistChoice = HEIST_CHOICE_AGENCY_HELICOPTER
				bFoundMatch = FALSE
			ENDIF
		ENDIF
		
		//Is this random crew member already used?
		INT index
		REPEAT g_sHeistChoiceData[heistChoice].iCrewSize index
			IF index != slot
				IF eCrewRandom = g_savedGlobals.sHeistData.eSelectedCrew[heistChoice][index]
					bFoundMatch = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDWHILE
	
	//Save selected crew member into relevant slot.
	g_savedGlobals.sHeistData.eSelectedCrew[heistChoice][slot] = eCrewRandom
	
	CPRINTLN(DEBUG_HEIST, "<CREW-VALID> Repaired crew member for ", PRIVATE_Get_Heist_Choice_Debug_String(heistChoice), " slot ", slot, " to ", GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_Name_Label(eCrewRandom)), ".")
ENDPROC


PROC PRIVATE_Reset_Heist_End_Screen_Crew_Member_Statuses(INT heistIndex)
	
	INT index
	REPEAT MAX_CREW_SIZE index
		g_savedGlobals.sHeistData.sEndScreenData[heistIndex].eCrewStatus[index] = CMST_NOT_SET
		g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iCrewMemberTake[index] = 0
	ENDREPEAT
	
ENDPROC


#IF IS_DEBUG_BUILD

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ Debug Crew Selector ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC PRIVATE_Configure_Crew_Selector_Heading_Text(INT r, INT g, INT b, INT a)
	SET_TEXT_SCALE (0.33, 0.37)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_DROPSHADOW (0,0,0,0,0)
	SET_TEXT_COLOUR(r,g,b,a)
	SET_TEXT_EDGE (1,0,0,0,a)
	SET_TEXT_PROPORTIONAL (TRUE)                                              
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC PRIVATE_Configure_Crew_Selector_Normal_Text(INT r, INT g, INT b, INT a)
	SET_TEXT_SCALE (0.26, 0.28)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_DROPSHADOW (0,0,0,0,0)
	SET_TEXT_COLOUR(r,g,b,a)
	SET_TEXT_EDGE (1,0,0,0,a)
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


FUNC CrewMember PRIVATE_Get_Next_Member_In_Slot(DebugCrewSelector &selector, BOOL bScanForwards)
	INT iCurrent = ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[selector.iLinkedHeistChoice][selector.iSelectedSlot])
	
	WHILE TRUE
		IF bScanForwards
			iCurrent++
			IF iCurrent > (ENUM_TO_INT(CM_MAX_CREW_MEMBERS)-1)
				iCurrent = ENUM_TO_INT(CM_GUNMAN_G_GUSTAV)
			ENDIF
		ELSE
			iCurrent --
			IF iCurrent <= 0
				iCurrent = ENUM_TO_INT(CM_MAX_CREW_MEMBERS)-1
			ENDIF
		ENDIF
		
		//Is this crew member unlocked?
		IF IS_BIT_SET(g_savedGlobals.sHeistData.iCrewUnlockedBitset, iCurrent) 		
			//Is this crew member still alive?
			IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iCrewDeadBitset, iCurrent)
				//Special case. Block using Chef on the Agency Heist.
				IF NOT (INT_TO_ENUM(CrewMember, iCurrent) = CM_GUNMAN_G_CHEF_UNLOCK AND (selector.iLinkedHeistChoice = HEIST_CHOICE_AGENCY_FIRETRUCK OR selector.iLinkedHeistChoice = HEIST_CHOICE_AGENCY_HELICOPTER))
					//Is this crew member the correct type?
					IF g_sCrewMemberStaticData[iCurrent].type = g_sHeistChoiceData[selector.iLinkedHeistChoice].eCrewType[selector.iSelectedSlot]
						//Is this crew member selected in another slot?
						BOOL bAlreadySelected = FALSE
						INT index
						REPEAT MAX_CREW_SIZE index
							IF index <> selector.iSelectedSlot
								IF ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[selector.iLinkedHeistChoice][index]) = iCurrent
									bAlreadySelected = TRUE
								ENDIF
							ENDIF
						ENDREPEAT
						IF NOT bAlreadySelected
							RETURN INT_TO_ENUM(CrewMember,iCurrent)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iCurrent = ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[selector.iLinkedHeistChoice][selector.iSelectedSlot])
			RETURN INT_TO_ENUM(CrewMember,iCurrent)
		ENDIF
	ENDWHILE
	
	//Will never reach here.
	RETURN CM_ERROR
ENDFUNC



PROC PRIVATE_Manage_Crew_Selector_Input(DebugCrewSelector &selector)

	IF (GET_GAME_TIMER() - selector.iTimeLastSelectionChange) > 250
		INT iStickLeftX, iStickLeftY, iTemp
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iStickLeftX, iStickLeftY, iTemp, iTemp)
	
		IF IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
		OR IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
		OR iStickLeftX > 85
			IF selector.iSelectedSlot >= (g_sHeistChoiceData[selector.iLinkedHeistChoice].iCrewSize - 1)
				selector.bFinalised = TRUE
			ELSE
				selector.iSelectedSlot++
				selector.iTimeLastSelectionChange = GET_GAME_TIMER()
			ENDIF
		ELIF IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
		OR IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_BACK)
		OR iStickLeftX < -85
			selector.iSelectedSlot--
			selector.iTimeLastSelectionChange = GET_GAME_TIMER()
			IF selector.iSelectedSlot < 0
				selector.iSelectedSlot = 0
			ENDIF
		ENDIF
		IF IS_BUTTON_JUST_PRESSED(PAD1, TRIANGLE)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
			selector.bFinalised = TRUE
		ENDIF
		
		IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
		OR iStickLeftY > 85
			g_savedGlobals.sHeistData.eSelectedCrew[selector.iLinkedHeistChoice][selector.iSelectedSlot] = PRIVATE_Get_Next_Member_In_Slot(selector, TRUE)
			selector.iTimeLastSelectionChange = GET_GAME_TIMER()
		ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)
		OR iStickLeftY < -85
			g_savedGlobals.sHeistData.eSelectedCrew[selector.iLinkedHeistChoice][selector.iSelectedSlot] = PRIVATE_Get_Next_Member_In_Slot(selector, FALSE)
			selector.iTimeLastSelectionChange = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
ENDPROC


PROC PRIVATE_Draw_Crew_Selector_Slot(INT slotIndex, DebugCrewSelector &selector)

	INT iHighlightIncrease = 0
	FLOAT fSlotXPos = 0.58 - (g_sHeistChoiceData[selector.iLinkedHeistChoice].iCrewSize*0.16*0.5) + (slotIndex*0.16)
	FLOAT fHalfStringWidth
	TEXT_LABEL_31 tStringData
	
	//Draw selector slot background.
	IF selector.iSelectedSlot = slotIndex
		iHighlightIncrease = 25
	ENDIF
	
	DRAW_RECT(fSlotXPos, 0.51, 0.12, 0.24, 10 + iHighlightIncrease, 10 + iHighlightIncrease, 10 + iHighlightIncrease, 200)
	
	//Display slot type.
	STRING strSlotType
	SWITCH g_sHeistChoiceData[selector.iLinkedHeistChoice].eCrewType[slotIndex]
		CASE CMT_GUNMAN
			strSlotType = "Gunman"
		BREAK
		CASE CMT_DRIVER
			strSlotType = "Driver"
		BREAK
		CASE CMT_HACKER
			strSlotType = "Hacker"
		BREAK
			
		DEFAULT
			SCRIPT_ASSERT("PRIVATE_Draw_Crew_Selector_Slot: Invalid crew member type set for heist choice slot. Bug BenR")
		BREAK
	ENDSWITCH
	PRIVATE_Configure_Crew_Selector_Heading_Text(225,225,225,180)
	fHalfStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING", strSlotType) * 0.5
	DISPLAY_TEXT_WITH_LITERAL_STRING(fSlotXPos-fHalfStringWidth, 0.635, "STRING", strSlotType)
	
	CrewMember eSelectedCrew = g_savedGlobals.sHeistData.eSelectedCrew[selector.iLinkedHeistChoice][slotIndex]
	
	//Display selected crew member info.
	IF eSelectedCrew = CM_ERROR
		SCRIPT_ASSERT("PRIVATE_Draw_Crew_Selector_Slot: Slot has an invalid crew member selected. Bug BenR")
	ELIF eSelectedCrew = CM_EMPTY
		//Display empty slot info.
		PRIVATE_Configure_Crew_Selector_Normal_Text(200+iHighlightIncrease,200+iHighlightIncrease,200+iHighlightIncrease,180)
		fHalfStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING", "-EMPTY-") * 0.5
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSlotXPos-fHalfStringWidth, 0.50, "STRING", "-EMPTY-")
	ELSE
		//Display crewmember specific info.
		//Name.
		PRIVATE_Configure_Crew_Selector_Normal_Text(200+iHighlightIncrease,200+iHighlightIncrease,200+iHighlightIncrease,180)
		tStringData = "NAME: "
		tStringData += GET_STRING_FROM_TEXT_FILE(PRIVATE_Get_Crew_Member_Name_Label(eSelectedCrew))
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSlotXPos-0.05, 0.435, "STRING", tStringData)
	
		//Skill.
		PRIVATE_Configure_Crew_Selector_Normal_Text(200+iHighlightIncrease,200+iHighlightIncrease,200+iHighlightIncrease,180)
		tStringData = "SKILL: "
		SWITCH g_savedGlobals.sHeistData.sCrewActiveData[eSelectedCrew].skill
			CASE CMSK_BAD
				tStringData += "Poor"
			BREAK
			CASE CMSK_MEDIUM
				tStringData += "Average"
			BREAK
			CASE CMSK_GOOD
				tStringData += "Good"
			BREAK
		ENDSWITCH
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSlotXPos-0.05, 0.475, "STRING", tStringData)
		
		//Cut.
		PRIVATE_Configure_Crew_Selector_Normal_Text(200+iHighlightIncrease,200+iHighlightIncrease,200+iHighlightIncrease,180)
		tStringData = "JOB CUT: "
		tStringData += g_sCrewMemberStaticData[eSelectedCrew].jobCut
		tStringData += "%"
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSlotXPos-0.05, 0.555, "STRING", tStringData)
		
//		//Accuracy.
//		PRIVATE_Configure_Crew_Selector_Normal_Text(200+iHighlightIncrease,200+iHighlightIncrease,200+iHighlightIncrease,180)
//		tStringData = "ACCURACY: "
//		tStringData += g_sCrewMemberData[eSelectedCrew].accuracy
//		tStringData += "%"
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fSlotXPos-0.05, 0.515, "STRING", tStringData)
	ENDIF
	
ENDPROC


PROC PRIVATE_Draw_Crew_Selector_Foreground(INT heistIndex, DebugCrewSelector &selector)
	INT iSlotIndex
	FLOAT fHalfStringWidth
	TEXT_LABEL_63 tTitle
	
	SWITCH heistIndex
		CASE HEIST_JEWEL
			tTitle = "DEBUG CREW - The Jewel Store Job"
		BREAK
		CASE HEIST_DOCKS
			tTitle = "DEBUG CREW - The Port of LS Heist"
		BREAK
		CASE HEIST_RURAL_BANK
			tTitle = "DEBUG CREW - The Paleto Score"
		BREAK
		CASE HEIST_AGENCY
			tTitle = "DEBUG CREW - The Agency Heist"
		BREAK
		CASE HEIST_FINALE
			tTitle = "DEBUG CREW - The Big Score"
		BREAK
	ENDSWITCH
	
	//Draw debug selector title.
	PRIVATE_Configure_Crew_Selector_Heading_Text(245,245,245,180)
	fHalfStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING", tTitle)*0.5
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.5-fHalfStringWidth, 0.345, "STRING", tTitle)
	
	IF g_sHeistChoiceData[selector.iLinkedHeistChoice].iCrewSize = 0
		PRIVATE_Configure_Crew_Selector_Heading_Text(200,200,200,160)
		fHalfStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING", "-No crew to select-")*0.5
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5-fHalfStringWidth, 0.5, "STRING", "-No crew to select-")
	ELSE
		REPEAT g_sHeistChoiceData[selector.iLinkedHeistChoice].iCrewSize iSlotIndex
			PRIVATE_Draw_Crew_Selector_Slot(iSlotIndex, selector)
		ENDREPEAT
	ENDIF
	
ENDPROC


PROC PRIVATE_Draw_Crew_Selector_Background(DebugCrewSelector &selector)
	//Wash screen black.
	DRAW_RECT(0.5,0.5,1.0,1.0,0,0,0,255)
	
	//Get screen aspect ratio.
	INT iScreenX
	INT iScreenY
	GET_SCREEN_RESOLUTION(iScreenX,iScreenY)
	FLOAT fAspectRatio = TO_FLOAT(iScreenY) / TO_FLOAT(iScreenX)
	
	//Calculate width required to fit in the required number of selector slots.
	FLOAT fSelectorWidth = 0.16*g_sHeistChoiceData[selector.iLinkedHeistChoice].iCrewSize
	
	IF fSelectorWidth < 0.22
		fSelectorWidth = 0.22
	ENDIF
	
	DRAW_RECT(0.5,0.5,fSelectorWidth,0.35,25,25,25,220)
	DRAW_RECT(	0.5,0.5,
				fSelectorWidth + (0.012 * fAspectRatio),
				0.35 + 0.012,
				25,25,25,100)
ENDPROC

#ENDIF

