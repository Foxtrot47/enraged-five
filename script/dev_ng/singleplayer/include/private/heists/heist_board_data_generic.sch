//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 03/03/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	  Heist Planning Board Data Generic Header					│
//│																				│
//│			This header contains fucntions for storing data in heist			│
//│			board data structures.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

PROC Store_Board_Core_Data(	HeistBoardData &paramBoardData,
							PlanningLocationName paramPlanningLocation,
							STRING paramBoardScaleform,
							STRING paramBoardText,
							STRING paramBoardDialogue,
							STRING paramCrewDialogue,
							g_eBoardDisplayGroups paramChoiceGroup,
							g_eBoardDisplayGroups paramListGroup,
							g_eBoardDisplayGroups paramChosenAGroup,
							g_eBoardDisplayGroups paramChosenBGroup,
							INT paramMaxCrewSize,
							VECTOR paramBlockingCenter,
							VECTOR paramBlockingDimensions)
	
	paramBoardData.ePlanningLocation 		= paramPlanningLocation
	paramBoardData.tBoardScaleform 			= paramBoardScaleform
	paramBoardData.tBoardText 				= paramBoardText
	paramBoardData.tBoardDialogueSlot 		= paramBoardDialogue
	paramBoardData.tCrewDialogueSlot 		= paramCrewDialogue
	paramBoardData.eChoiceGroup 			= paramChoiceGroup
	paramBoardData.eChoiceSelectedGroup[0] 	= paramChosenAGroup
	paramBoardData.eChoiceSelectedGroup[1] 	= paramChosenBGroup	
	paramBoardData.eTodoListGroup 			= paramListGroup
	paramBoardData.iMaxCrewSize				= paramMaxCrewSize
	paramBoardData.vAreaBlockCenter			= paramBlockingCenter
	paramBoardData.vAreaBlockDimensions		= paramBlockingDimensions
ENDPROC
							
PROC Store_Board_Main_Layout_Data(	HeistBoardData &paramBoardData,
									INT paramScaleformPixelWidth,
									INT paramScaleformPixelHeight,
									FLOAT paramScaleformWidth,
									FLOAT paramScaleformHeight,
									FLOAT paramScaleformScaleX,
									FLOAT paramScaleformScaleY,
									FLOAT paramCameraDistance,
									INT paramDefaultFocusX, INT paramDefaultFocusY,
									INT paramCrewPositionX1, INT paramCrewPositionY1,
									INT paramCrewPositionX2, INT paramCrewPositionY2,
									INT paramCrewPositionX3, INT paramCrewPositionY3,
									INT paramCrewPositionX4, INT paramCrewPositionY4,
									INT paramCrewPositionX5, INT paramCrewPositionY5,
									INT paramChoicePositionX, INT paramChoicePositionY,
									INT paramChoiceCameraOffsetX, INT paramChoiceCameraOffsetY,
									INT paramListPositionX, INT paramListPositionY)
															
	paramBoardData.iScaleformPixelWidth 	= paramScaleformPixelWidth
	paramBoardData.iScaleformPixelHeight 	= paramScaleformPixelHeight
	paramBoardData.fScaleformWidth 			= paramScaleformWidth
	paramBoardData.fScaleformHeight 		= paramScaleformHeight
	paramBoardData.fScaleformXScale			= paramScaleformScaleX
	paramBoardData.fScaleformYScale			= paramScaleformScaleY
	paramBoardData.fCameraDistance			= paramCameraDistance

	paramBoardData.sDefaultFocusPos.x		= paramDefaultFocusX
	paramBoardData.sDefaultFocusPos.y		= paramDefaultFocusY
	
	paramBoardData.sCrewBoardPos[0].x		= paramCrewPositionX1
	paramBoardData.sCrewBoardPos[0].y		= paramCrewPositionY1
	paramBoardData.sCrewBoardPos[1].x		= paramCrewPositionX2
	paramBoardData.sCrewBoardPos[1].y		= paramCrewPositionY2
	paramBoardData.sCrewBoardPos[2].x		= paramCrewPositionX3
	paramBoardData.sCrewBoardPos[2].y		= paramCrewPositionY3
	paramBoardData.sCrewBoardPos[3].x		= paramCrewPositionX4
	paramBoardData.sCrewBoardPos[3].y		= paramCrewPositionY4
	paramBoardData.sCrewBoardPos[4].x		= paramCrewPositionX5
	paramBoardData.sCrewBoardPos[4].y		= paramCrewPositionY5

	paramBoardData.sChoiceBoardPos.x 		= paramChoicePositionX
	paramBoardData.sChoiceBoardPos.y 		= paramChoicePositionY
	paramBoardData.sChoiceCameraOffset.x	= paramChoiceCameraOffsetX
	paramBoardData.sChoiceCameraOffset.y	= paramChoiceCameraOffsetY
	
	paramBoardData.sListBoardPos.x 			= paramListPositionX
	paramBoardData.sListBoardPos.y 			= paramListPositionY
ENDPROC


PROC Store_Board_Zoom_Levels( 	HeistBoardData &paramBoardData,
								FLOAT paramMaxZoom,
								FLOAT paramChoiceZoom,
								FLOAT paramPOIZoom)
								
	paramBoardData.fMaxZoom = paramMaxZoom
	paramBoardData.fChoiceZoom = paramChoiceZoom
	paramBoardData.fPOIZoom = paramPOIZoom
								
ENDPROC


CONST_FLOAT PB_CAM_FOV_MAX_ZOOM				20.0
CONST_FLOAT PB_CAM_FOV_CHOICE_ZOOM			25.5
CONST_FLOAT PB_CAM_FOV_CREW_ZOOM			25.5
CONST_FLOAT PB_CAM_FOV_POI					28.0


PROC Store_Board_Text_Data(	HeistBoardData &paramBoardData,
							STRING paramChoiceHelpLabel1,
							STRING paramChoiceHelpLabel2,
							STRING paramCrewHelpLabel1,
							STRING paramCrewHelpLabel2,
							STRING paramChoiceDiaLabel,
							STRING paramCrewDiaLabel,
							STRING paramConfirmDiaLabel,
							STRING paramTooSlowDiaLabel)
							
	TEXT_LABEL_15 tChoiceHelp1 = 		paramChoiceHelpLabel1
	TEXT_LABEL_15 tChoiceHelp2 =		paramChoiceHelpLabel2
	TEXT_LABEL_15 tCrewHelp1 = 			paramCrewHelpLabel1
	TEXT_LABEL_15 tCrewHelp2 = 			paramCrewHelpLabel2
	TEXT_LABEL_7  tChoiceDialogue = 	paramChoiceDiaLabel
	TEXT_LABEL_7  tCrewDialogue = 		paramCrewDiaLabel
	TEXT_LABEL_7  tConfirmDialogue = 	paramConfirmDiaLabel
	TEXT_LABEL_7  tTooSlowDialogue = 	paramTooSlowDiaLabel
									
	paramBoardData.tChoiceHelp[0] 							= tChoiceHelp1
	paramBoardData.tChoiceHelp[1] 							= tChoiceHelp2
	paramBoardData.tCrewHelp[0] 							= tCrewHelp1
	paramBoardData.tCrewHelp[1] 							= tCrewHelp2
	paramBoardData.tBoardDialogueLabels[BDS_CHOICE_SELECT] 	= tChoiceDialogue
	paramBoardData.tBoardDialogueLabels[BDS_CREW_SELECT] 	= tCrewDialogue
	paramBoardData.tBoardDialogueLabels[BDS_CONFIRM] 		= tConfirmDialogue
	paramBoardData.tBoardDialogueLabels[BDS_TOO_SLOW] 		= tTooSlowDialogue
ENDPROC


PROC Store_Board_Static_Item_Data(	HeistBoardData &paramBoardData,
									INT paramAssetIndex,
									g_eBoardDisplayGroups paramDisplayGroup,
									INT paramBoardPositionX, INT paramBoardPositionY)
	
	IF paramBoardData.iNoStaticItems < MAX_BOARD_STATIC_ITEMS
		paramBoardData.iStaticItemAsset[paramBoardData.iNoStaticItems] = paramAssetIndex
		paramBoardData.eStaticItemGroup[paramBoardData.iNoStaticItems] = paramDisplayGroup
		
		paramBoardData.sStaticItemPos[paramBoardData.iNoStaticItems].x = paramBoardPositionX
		paramBoardData.sStaticItemPos[paramBoardData.iNoStaticItems].y = paramBoardPositionY
		
		paramBoardData.iNoStaticItems++
	ELSE
		SCRIPT_ASSERT("Store_Board_Static_Item_Data: Tried to add too many static items to a board. Increase MAX_BOARD_STATIC_ITEMS? -BenR")
	ENDIF
ENDPROC


PROC Store_Board_Todo_List_Item_Data( 	HeistBoardData &paramBoardData,
										g_eBoardDisplayGroups paramAppearGroup,
										g_eBoardDisplayGroups paramTickGroup,
										STRING paramTodoText)
	
	IF paramBoardData.iNoTodoItems < MAX_BOARD_TODO_ITEMS
		TEXT_LABEL_31 tTodoText = paramTodoText									
		paramBoardData.eTodoItemAppearGroup[paramBoardData.iNoTodoItems] = paramAppearGroup
		paramBoardData.eTodoItemTickGroup[paramBoardData.iNoTodoItems] = paramTickGroup
		paramBoardData.tTodoItemText[paramBoardData.iNoTodoItems] = tTodoText
		paramBoardData.iNoTodoItems++
	ELSE
		SCRIPT_ASSERT("Store_Board_Todo_List_Item_Data: Tried to add too many TODO items to a board. Increase MAX_BOARD_TODO_ITEMS? -BenR")
	ENDIF
ENDPROC


PROC Store_Board_Pin_Layout_Data(	HeistBoardData &paramBoardData,
									g_eBoardDisplayGroups paramDisplayGroup,
									INT paramPinPosX, INT paramPinPosY)
									
	IF paramBoardData.iNoBoardPins < MAX_BOARD_PINS
		paramBoardData.ePinGroup[paramBoardData.iNoBoardPins] = paramDisplayGroup
		
		paramBoardData.sPinPos[paramBoardData.iNoBoardPins].x = paramPinPosX
		paramBoardData.sPinPos[paramBoardData.iNoBoardPins].y = paramPinPosY
		
		paramBoardData.iNoBoardPins++
	ELSE
		SCRIPT_ASSERT("Store_Board_Pin_Layout_Data: Tried to add too many pins to a board. Increase MAX_BOARD_PINS? -BenR")
	ENDIF
ENDPROC


PROC Store_Board_Point_Of_Interest_Data(HeistBoardData &paramBoardData,
										g_eBoardDisplayGroups paramPOIDisplayGroup,
										STRING paramPOIDiag,
										INT paramPOIPosX, INT paramPOIPosY,
										BOOL paramDelayBeforeNext = TRUE)
									
	IF paramBoardData.iNoPointsOfInterest < MAX_BOARD_POIS
		TEXT_LABEL_7 tPOILabel = paramPOIDiag
	
		paramBoardData.ePOIGroup[paramBoardData.iNoPointsOfInterest] = paramPOIDisplayGroup
		paramBoardData.sPOIPos[paramBoardData.iNoPointsOfInterest].x = paramPOIPosX
		paramBoardData.sPOIPos[paramBoardData.iNoPointsOfInterest].y = paramPOIPosY
		paramBoardData.tBoardDialogueLabels[paramBoardData.iNoPointsOfInterest + ENUM_TO_INT(BDS_POI_1)] = tPOILabel
		IF paramDelayBeforeNext
			SET_BIT(paramBoardData.iPOIDelayBeforeNextBitset, paramBoardData.iNoPointsOfInterest)
		ENDIF
		paramBoardData.iNoPointsOfInterest++
	ELSE
		SCRIPT_ASSERT("Store_Board_Point_Of_Interest_Data: Tried to add too many POIs to a board. Increase MAX_BOARD_POIS? -BenR")
	ENDIF
ENDPROC


PROC Store_Heist_Board_Crew_Member_Dialogue_Data(	HeistBoardData &paramBoardData,
													CrewMember paramCrewMember,
													STRING paramPickLabel,
													STRING paramMetLabel)
									
	TEXT_LABEL_7 tCrewPick = paramPickLabel
	TEXT_LABEL_7 tCrewMet = paramMetLabel
			
	paramBoardData.tCrewDialogueLabels[paramCrewMember] = tCrewPick
	
	IF NOT ARE_STRINGS_EQUAL(paramMetLabel, "")
		SWITCH paramCrewMember
			CASE CM_GUNMAN_G_PACKIE_UNLOCK
				paramBoardData.tCrewDialogueLabels[CRDS_MET_PACKIE] = tCrewMet
				CDEBUG3LN(DEBUG_HEIST, "Stored Packie's MET dialogue label as [", tCrewMet, "].")
			BREAK
			CASE CM_DRIVER_G_TALINA_UNLOCK
				paramBoardData.tCrewDialogueLabels[CRDS_MET_TALINA] = tCrewMet
				CDEBUG3LN(DEBUG_HEIST, "Stored Talina's MET dialogue label as [", tCrewMet, "].")
			BREAK
			CASE CM_HACKER_B_RICKIE_UNLOCK
				paramBoardData.tCrewDialogueLabels[CRDS_MET_RICKIE] = tCrewMet
				CDEBUG3LN(DEBUG_HEIST, "Stored Rickie's MET dialogue label as [", tCrewMet, "].")
			BREAK
			CASE CM_GUNMAN_G_CHEF_UNLOCK
				paramBoardData.tCrewDialogueLabels[CRDS_MET_CHEF] = tCrewMet
				CDEBUG3LN(DEBUG_HEIST, "Stored Chef's MET dialogue label as [", tCrewMet, "].")
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Store_Heist_Board_Crew_Member_Dialogue_Data: Tried to store MET dialogue for a none unlockable crew member.")
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC


PROC Store_Heist_Board_Crew_Member_Dialogue_Slot(	HeistBoardData &paramBoardData,
													CrewDialogueSlot paramSlot,
													STRING paramLabel)					
	TEXT_LABEL_7 tLabel = paramLabel
			
	IF ARE_STRINGS_EQUAL(paramLabel, "")
		SCRIPT_ASSERT("Store_Heist_Board_Crew_Member_Dialogue_Slot: Blank label passed. Failed to store.")
		EXIT
	ENDIF
	
	paramBoardData.tCrewDialogueLabels[paramSlot] = tLabel
ENDPROC
