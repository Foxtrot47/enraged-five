//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 10/02/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	  Finale Heist Planning Board Data Header					│
//│																				│
//│			This header contains lookups to all data required by the Big		│
//│			Score planning board.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "heist_board_data_generic.sch"

ENUM BoardAssetFinale
	//Part A
	PBA_FINALE_NOTE_GETAWAY = 0,
	PBA_FINALE_SECURITY_NOTES,        
	PBA_FINALE_SECURITY,              
	PBA_FINALE_TUNNEL,                
	PBA_FINALE_VAN,                   
	PBA_FINALE_CARS,                  
	PBA_FINALE_TRUCK,                 
	PBA_FINALE_MAPSAT1,               
	PBA_FINALE_MAP_ROUTE,             
	PBA_FINALE_BANK_LETTER,           

	//Part B
	PBA_FINALE_BOMB,                  
	PBA_FINALE_BANK_LOBBY,            
	PBA_FINALE_NOTE_DROPOFF,          
	PBA_FINALE_NOTE_HOLE,             
	PBA_FINALE_DRILLING,              
	PBA_FINALE_PAPERWORK,             
	PBA_FINALE_HELI,                  
	PBA_FINALE_FREIGHT,               
	PBA_FINALE_HOLE,                  
	PBA_FINALE_MAPSAT2,               
	PBA_FINALE_MAPB,                  
	PBA_FINALE_GOLDRESERVE           
ENDENUM


PROC Setup_Finale_Heist_Board_Data(HeistBoardData &paramBoardData, INT paramMaxCrewSize)
	
	// Finale Heist display groups.
	// PBDG_1 - Approach selected.
	// PBDG_2 - Crew selected.
	// PBDG_3 - Chose to do the Traffic Control approach.
	// PBDG_4 - Chose to do the Heli lift approach.
	// PBDG_5 - Completed the obtain stingers prep.
	// PBDG_6 - Completed the modded cars prep.
	// PBDG_7 - Completed the get driller prep.
	// PBDG_8 - Completed the get a train prep.
	
	//												Planning Location	Board Scaleform		Board Text	Board Dialogue	Crew Dialogue	Choice Group	Todo List Group		Chosen A		Chosen B		Max Crew Size		Blocking Area Center					Blocking Area Dimensions 
	Store_Board_Core_Data(paramBoardData,			PLN_STRIPCLUB,		"HEIST_FINALE",		"BOARD6",	"FHFAUD",		"CRWAUD",		PBDG_1,			PBDG_1,				PBDG_3,			PBDG_4,			paramMaxCrewSize, 	<<96.478249,-1292.352905,29.768753>>,	<<5.000000,5.500000,6.000000>>)
	
	//												SF Pixel Dim.		SF Dim.				SF Scale		Cam Dist	Default Focus	Crew 1 Pos		Crew 2 Pos		Crew 3 Pos		Crew 4 Pos	   	Crew 5 Pos	   	Choice Pos		Choice CamOff	List Pos
	Store_Board_Main_Layout_Data(paramBoardData, 	1500, 540,			3.45, 1.34,			3.18, 1.79,		2.58,		747,260,		620,442,		490,445,		360,438,		230,440,		100,435,		872,398,		20,40,			1150,415	)

	//												Max Zoom			Choice Zoom			POI Zoom
	Store_Board_Zoom_Levels(paramBoardData,			18.0,				17.0,				19.0)

	//												Choice Help 1		Choice Help 2		Crew Help 1		Crew Help 2		Choice Diag		Crew Diag	Confirm Diag	Too Slow Diag
	Store_Board_Text_Data(paramBoardData,			"",					"",					"",				"",				"FHP16",		"",			"FHP17",		"FHP19")

	//Load crew member dialogue.									Crew Member					Pick Diag		Met Diag	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_GUSTAV,			"CRW_GM",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_KARL,			"CRW_KA",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_M_HUGH,			"CRW_HW",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_NORM,			"CRW_NR",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_DARYL,			"CRW_DJ",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_G_PAIGE,			"CRW_PH",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_M_CHRIS,			"CRW_CF",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_G_EDDIE,			"CRW_ET",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_B_KARIM,			"CRW_KD",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_PACKIE_UNLOCK,	"CRW_PM",		"CRM_PM"	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_CHEF_UNLOCK,	"",				"CRM_CH"	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_B_RICKIE_UNLOCK,	"CRW_RL",		"CRM_RL"   	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_G_TALINA_UNLOCK,	"CRW_TM",		"CRM_TM"   	)	

	//Load more specific lines of dialogue for this heist.
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_RICKIE_LAST_ONE,		"FHP18")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_RICKIE_LAST_TWO,		"FHP18b")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_CHRIS_JEWEL,			"FHP18c")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_CHRIS_AGENCY,			"FHP18d")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_CHRIS_JEWEL_AGENCY,	"FHP18e")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_KARIM_LAST_ONE,		"FHP18f")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_KARIM_LAST_TWO,		"FHP18g")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_BADG_AGENCY,			"FHP18h")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_JEWEL_AGENCY,			"FHP18i")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_ALL,					"FHP18j")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_AGENCY,				"FHP18k")

	//Finale heist static items.								
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_NOTE_GETAWAY),       PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_SECURITY_NOTES),     PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_SECURITY),      		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_TUNNEL),   			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_VAN),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_CARS),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_TRUCK),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_MAPSAT1),   			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_MAP_ROUTE),          PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_BANK_LETTER),        PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_BOMB),          		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_BANK_LOBBY),         PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_NOTE_DROPOFF),      	PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_NOTE_HOLE),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_DRILLING),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_PAPERWORK),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_HELI),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_FREIGHT),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_HOLE),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_MAPSAT2),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_MAPB),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FINALE_GOLDRESERVE),		PBDG_1,		0,0	)
	
	//Finale heist todo items.
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_0,		"H_TD_RES")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_0,		"H_TD_FLY")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_2,		"H_TD_PLAN")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_10,	"H_TD_CREW")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_3,		PBDG_5,		"H_TD_STNG")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_3,		PBDG_6,		"H_TD_CARS")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_4,		PBDG_7,		"H_TD_DRIL")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_4,		PBDG_8,		"H_TD_TRAI")
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_4,		PBDG_9,		"H_TD_GETA")
	
	//Finale heist pins.
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		142,72		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		134,244		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		262,54		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		334,206		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		547,65		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		550,200		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		453,54		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		492,242		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		731,179		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		685,81		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		828,79		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		982,167		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		1155,70		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		1293,60		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		1254,191	)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		1433,84		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		1469,172	)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		952,367		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		967,469		)
	Store_Board_Pin_Layout_Data(paramBoardData, 	PBDG_1,		1246,374	)
	
	//Finale heist points of interest.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP9",		777,165				) // Union repository.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP10",	970,179				) // City tunneling machine.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP11",	1210,184			) // Roadworks hole.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP12",	537,168,	FALSE	) // Armored cars.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP13",	579,229				) // Stingers.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP14",	355,228				) // Getaway vehicles. Gauntlets/Lorry.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,	"FHP15",	350,117				) // Ambush point.
	
ENDPROC
