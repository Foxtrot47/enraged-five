//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 10/02/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	  Jewel Heist Planning Board Data Header					│
//│																				│
//│			This header contains lookups to all data required by the Jewel		│
//│			Store Job planning board.											│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "heist_board_data_generic.sch"


//Agency Heist Assets.
ENUM BoardAssetAgency
	PBA_FBI_BLUEPRINT01 = 0,	
	PBA_FBI_BLUEPRINT02,
	PBA_FBI_JANITOR_LICENSE,
	PBA_FBI_LICENSE_PLATE,
	PBA_FBI_FIRE_STATION,
	PBA_FBI_FBI_RECEPTION,
	PBA_FBI_FBI_BUILDING,
	PBA_FBI_ARCHITECT_NEWSPAPER,
	PBA_FBI_ARCHITECT_BUSINESS_CARD,
	PBA_FBI_MAP_HELICOPTER
ENDENUM


PROC Setup_Agency_Heist_Board_Data(HeistBoardData &paramBoardData, INT paramMaxCrewSize)
	
	// Agency Heist display groups.
	// PBDG_0 - Janitor mission complete.
	// PBDG_1 - Lester ticks Janitor ID and writes about getting plans.
	// PBDG_2 - Lester set up board.
	// PBDG_3 - Blueprint posted.
	// PBDG_4 - Approach selected.
	// PBDG_5 - Crew selected.
	// PBDG_6 - Firecrew approach was chosen.
	// PBDG_7 - Fire truck and equipment obtained.
	
	//												Planning Location	Board Scaleform		Board Text	Board Dialogue	Crew Dialogue	Choice Group	Todo List Group		Chosen A		Chosen B		Max Crew Size		Blocking Area Center						Blocking Area Dimensions
	Store_Board_Core_Data(paramBoardData,			PLN_SWEATSHOP,		"HEIST_AGENCY",		"BOARD5",	"AHFAUD",		"CRWAUD",		PBDG_3,			PBDG_0,				PBDG_6, 		PBDG_INVALID,	paramMaxCrewSize	,<<708.995667,-965.256042,31.395329>>,		<<5.500000,6.250000,2.000000>>)
	
	//												SF Pixel Dim.		SF Dim.				SF Scale		Cam Dist	Default Focus	Crew 1 Pos		Crew 2 Pos		Crew 3 Pos		Crew 4 Pos	   	Crew 5 Pos	   	Choice Pos		Choice CamOff	List Pos
	Store_Board_Main_Layout_Data(paramBoardData, 	750, 540,			2.10, 1.47,			3.60, 2.05,		2.0,		375,250,		136,411,		268,415,		401,418,		482,413,		0,0,			670,350,		0,85,			684,106		)

	//												Max Zoom			Choice Zoom			POI Zoom
	Store_Board_Zoom_Levels(paramBoardData,			20.0,				22.0,				28.0)

	//												Choice Help 1		Choice Help 2		Crew Help 1		Crew Help 2		Choice Diag		Crew Diag		Confirm Diag	Too Slow Diag
	Store_Board_Text_Data(paramBoardData,			"",					"",					"",				"",				"AHP12",		"",				"AHP13",		"AHLONG")

	//Load crew member dialogue.									Crew Member					Pick Diag		Met Diag		
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_GUSTAV,			"CRW_GM",		""			)		
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_M_HUGH,			"CRW_HW",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_NORM,			"CRW_NR",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_DARYL,			"CRW_DJ",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_G_PAIGE,			"CRW_PH",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_M_CHRIS,			"CRW_CF",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_G_EDDIE,			"CRW_ET",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_B_KARIM,			"CRW_KD",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_PACKIE_UNLOCK,	"CRW_PM",		"CRM_PM"   	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_HACKER_B_RICKIE_UNLOCK,	"CRW_RL",		"CRM_RL"  	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_DRIVER_G_TALINA_UNLOCK,	"CRW_TM",		"CRM_TM"   	)	
	
	//Load more specific lines of dialogue for this heist.
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_RICKIE_LAST_ONE,	"AHRIC1")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_KARIM_LAST_ONE,	"AHKAR1")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_JEWEL,			"AHJH")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_PALETO,			"AHPAL")
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_JEWEL_PALETO,		"AHBOTH")

	//Agency heist static items.
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_LICENSE_PLATE),         	PBDG_0,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_JANITOR_LICENSE),     		PBDG_0,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_ARCHITECT_NEWSPAPER),   	PBDG_0,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_ARCHITECT_BUSINESS_CARD),	PBDG_0,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_FBI_RECEPTION),       		PBDG_2,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_FBI_BUILDING),        		PBDG_2,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_FIRE_STATION),       		PBDG_2,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_MAP_HELICOPTER),      		PBDG_2,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_BLUEPRINT01),				PBDG_3,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_FBI_BLUEPRINT02),        		PBDG_3,		0,0	)
	
	//Agency heist todo items.
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_0,		PBDG_1,		"H_TD_JANI")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_1,		PBDG_3,		"H_TD_BLUP")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_2,		PBDG_4,		"H_TD_PLAN")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_2,		PBDG_5,		"H_TD_CREW")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_6,		PBDG_7,		"H_TD_FIRE")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_6,		PBDG_8,		"H_TD_GETA")
	
	//Agency heist pins.
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	70,71	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	44,174	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	45,209	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_3,	178,68	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_3,	321,65	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	538,52	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	470,82	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	455,207	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	566,207	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	673,339	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	679,442	)
	Store_Board_Pin_Layout_Data(paramBoardData,		 PBDG_2,	689,63	)
	
	//Agency heist points of interest.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_2,		"AHP8",		55,197		)	// Janitor info.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_2,		"AHP9",		252,150		)	// Bureau building plans.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_2,		"AHP10",	70,97		)	// Davis fire station.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_2,		"AHP11",	504,106		)	// Skylight on roof.
ENDPROC
