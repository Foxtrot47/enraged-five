//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 10/02/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	  Docks Heist Planning Board Data Header					│
//│																				│
//│			This header contains lookups to all data required by the Port		│
//│			of LS Heist planning board.											│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "heist_board_data_generic.sch"

//Docks Heist Assets.
ENUM BoardAssetDocks
	PBA_DOCKS_MAP  = 0,
	PBA_DOCKS_PHOTO_BOW,
	PBA_DOCKS_PHOTO_STERN,
	PBA_DOCKS_PHOTO_SECURITY,
	PBA_DOCKS_WEBSITE,
	PBA_DOCKS_MANIFESTO
ENDENUM


PROC Setup_Docks_Heist_Board_Data(HeistBoardData &paramBoardData, INT paramMaxCrewSize)
	
	// Docks Heist display groups.
	// PBDG_1 - Finished scouting mission.
	// PBDG_2 - Picked approach
	// PBDG_3 - Sink tanker option chosen.
	// PBDG_4 - Deep sea dive option chosen.
	// PBDG_5 - Stole submersible.
	// PBDG_6 - Stole helicopter.
	
	//												Planning Location	Board Scaleform		Board Text	Board Dialogue	Crew Dialogue	Choice Group	Todo List Group		Chosen A		Chosen B		Max Crew Size		Blocking Area Center							Blocking Area Dimensions
	Store_Board_Core_Data(paramBoardData,			PLN_TREV_CITY, 		"HEIST_DOCKS", 		"BOARD1", 	"DHFAUD",		"CRWAUD",		PBDG_1,			PBDG_1,				PBDG_3, 		PBDG_4,			paramMaxCrewSize,	<<-1157.126465,-1520.959229,11.133599>>,		<<5.000000,5.500000,4.000000>>)

	
	//												SF Pixel Dim.		SF Dim.				SF Scale		Cam Dist	Default Focus	Crew 1 Pos		Crew 2 Pos		Crew 3 Pos		Crew 4 Pos	   	Crew 5 Pos	   	Choice Pos		Choice CamOff	List Pos
	Store_Board_Main_Layout_Data(paramBoardData, 	544, 424,			1.41, 0.93,			2.75, 1.55,		1.45,		272,206,		122,380,		270,362,		0,0,			0,0,			0,0,			481,265,		0,85,			544,115		)

	//												Max Zoom			Choice Zoom			POI Zoom
	Store_Board_Zoom_Levels(paramBoardData,			20.0,				22.0,				28.0)

	//												Choice Help 1		Choice Help 2		Crew Help 1		Crew Help 2		Choice Diag		Crew Diag		Confirm Diag	Too Slow Diag
	Store_Board_Text_Data(paramBoardData,			"",					"",					"",				"",				"DHP1",			"",				"",				"DHP10")

	// Docks heist static items.
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_DOCKS_MAP),				PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_DOCKS_PHOTO_BOW),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_DOCKS_PHOTO_STERN),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_DOCKS_PHOTO_SECURITY),	PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_DOCKS_WEBSITE),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_DOCKS_MANIFESTO),		PBDG_1,		0,0	)
	
	// Docks heist todo items.
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_1,		"H_TD_PHSH"	)
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_1,		"H_TD_INVE"	)
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_1,		"H_TD_SECU"	)
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_0,		PBDG_2,		"H_TD_PLAN"	)	
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_3,		PBDG_5,		"H_TD_SUB"	)
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_4,		PBDG_7,		"H_TD_SUB"	)
	Store_Board_Todo_List_Item_Data(paramBoardData,		PBDG_4,		PBDG_6,		"H_TD_AIR"	)
	
	// Docks heist points of interest.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"DHP4a",		227,154				)	// Port diagram.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"DHP4b",		264,237,	FALSE	)	// Merryweather website.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"DHP5",			59,134				)	// Photos of the freighter.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"DHP6",			227,154				)	// Port diagram.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_0,		"DHP7",			346,39				)	// Manifest page.
ENDPROC
