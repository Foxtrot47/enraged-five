//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/05/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						    Generic Heist Control Header						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "heist_planning_board.sch"
USING "flow_public_core_override.sch"

USING "Flow_Mission_Data_Public.sch"
USING "mission_control_public.sch"
USING "heist_private.sch"
USING "heist_end_screen.sch"
USING "dialogue_public.sch"



PROC HEIST_TIMER_UPDATE()

	//check for conditions that would pause the timer
	IF g_bResultScreenDisplaying //stop counting as soon as the end screen is about to pop
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX()) //don't count while the player doesn't have control
		EXIT
	ENDIF

	//otherwise update the timer
	//PRINTLN("Htimer is now : ", sEndScreen.iHeistTimer)
	INT t = ROUND(1 +@ 1000)
	sEndScreen.iHeistTimer += t

ENDPROC


//Allow the player to interact with a board if it is created.
PROC Manage_Board_Viewing(PlanningBoard &sPlanningBoard)
	IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
		Do_Board_View(sPlanningBoard)
	ENDIF
ENDPROC


//Check for flow requesting a crew be selected.
PROC Manage_Board_Mode_Switching(PlanningBoard &sPlanningBoard)

	//Read the FlowInt value for this heist.
	INT iFlaggedMode = Get_Mission_Flow_Int_Value(Get_Heist_Board_Mode_FlowInt_ID(sPlanningBoard.iLinkedHeist))
	
	//Check it is a valid value.
	IF iFlaggedMode < 0
	OR iFlaggedMode >= ENUM_TO_INT(PBM_INVALID)
		SCRIPT_ASSERT("Manage_Board_Mode_Switching: A flagged board mode was set to an invalid value. Failed to switch board mode.")
		EXIT
	ENDIF
	
	//Flagged mode is valid. Convert to an enum.
	g_eBoardModes eFlaggedMode = INT_TO_ENUM(g_eBoardModes, iFlaggedMode)

	//Is the board already in this mode?
	IF eFlaggedMode <> sPlanningBoard.eMode
	//Special case hack. Allow the board to go into confirm mode while the flow thinks it is in crew select.
	AND NOT (eFlaggedMode = PBM_CREW_SELECT AND sPlanningBoard.eMode = PBM_CONFIRM) 
	AND NOT (sPlanningBoard.iLinkedHeist = HEIST_DOCKS AND eFlaggedMode = PBM_CHOICE_SELECT AND sPlanningBoard.eMode = PBM_CONFIRM)
		//No switch it.
		IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
			Switch_Board_Mode(sPlanningBoard, eFlaggedMode)
		ELSE
			sPlanningBoard.eMode = eFlaggedMode
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Checks appropriate flowflag to see if the heist will be doing the board exit cut-scene
/// PARAMS:
///    iHeist - the heist we are playing
/// RETURNS:
///    TRUE if flowlflag is set. FALSE otherwise
FUNC BOOL IS_HEIST_DOING_BOARD_EXIT_CUTSCENE(INT iHeist)
	SWITCH iHeist
		CASE HEIST_JEWEL
			RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE)
		BREAK

		CASE HEIST_DOCKS	
			RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE)
		BREAK
		
		CASE HEIST_RURAL_BANK	
			RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE)
		BREAK
		
		CASE HEIST_AGENCY	
			RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE)
		BREAK
		
		CASE HEIST_FINALE	
			RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE)
		BREAK
		
		DEFAULT // not a heist
		BREAK
	ENDSWITCH

	RETURN FALSE // shouldn't reach here
ENDFUNC


//Update whether or not this planning board should auto quit the board view when a mission is selected.
PROC Manage_Board_View_Auto_Toggling(PlanningBoard &sPlanningBoard)
	IF Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_TOGGLE_BOARD, sPlanningBoard.iLinkedHeist)
		IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
			IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				Exit_Board_View(sPlanningBoard, IS_HEIST_DOING_BOARD_EXIT_CUTSCENE(sPlanningBoard.iLinkedHeist))
				Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_TOGGLE_BOARD, sPlanningBoard.iLinkedHeist, FALSE)
			ELSE
				Initalise_Board_View(sPlanningBoard)
				Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_HEIST_TOGGLE_BOARD, sPlanningBoard.iLinkedHeist, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
					
					
//Check for flow requesting a group of items be displayed.
PROC Update_Board_Display_Groups(PlanningBoard &sPlanningBoard)
	IF IS_BIT_SET(g_iBitsetHeistBoardUpdated, sPlanningBoard.iLinkedHeist)
		IF IS_BIT_SET(sPlanningBoard.iState, BIT_BOARD_CREATED)
			CPRINTLN(DEBUG_HEIST, "Updating board display groups.")
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_PINS)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_STATIC_ITEMS)
			SET_BIT(sPlanningBoard.iState, BIT_BOARD_REFRESH_TODO_LIST)
			
			CLEAR_BIT(g_iBitsetHeistBoardUpdated, sPlanningBoard.iLinkedHeist)
		ENDIF
	ENDIF
ENDPROC


PROC Manage_Heist_Cutscene_Request(INT &paramRequestID, STRING paramCutscene, CUTSCENE_SECTION paramSections = CS_SECTION_8)
	IF paramRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
		CPRINTLN(DEBUG_HEIST, "Registering off mission cutscene request for heist controller cutscene ", paramCutscene, ".")
		REGISTER_OFFMISSION_CUTSCENE_REQUEST(paramRequestID, OCT_MUST_LOAD)
	ELSE
		SWITCH GET_OFFMISSION_CUTSCENE_REQURIED_ACTION(paramRequestID)
			CASE OCA_LOAD
				IF paramSections = CS_SECTION_8
					CPRINTLN(DEBUG_HEIST, "Loading full cutscene.")
					REQUEST_CUTSCENE(paramCutscene)
				ELSE
					CPRINTLN(DEBUG_HEIST, "Loading cutscene with specific sections.")
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(paramCutscene, paramSections)
				ENDIF
				SET_OFFMISSION_CUTSCENE_ACTIVE(paramRequestID, TRUE)
			BREAK
			CASE OCA_UNLOAD
				CPRINTLN(DEBUG_HEIST, "Unloading cutscene.")
				REMOVE_CUTSCENE()
				SET_OFFMISSION_CUTSCENE_ACTIVE(paramRequestID, FALSE)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


PROC Reset_Heist_Selections()
	IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_1)
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, HEIST_JEWEL, FALSE)
		INT iCrewIndex
		REPEAT MAX_CREW_SIZE iCrewIndex
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_JEWEL_STEALTH][iCrewIndex] = CM_EMPTY
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_JEWEL_HIGH_IMPACT][iCrewIndex] = CM_EMPTY
		ENDREPEAT
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, HEIST_JEWEL, FALSE)
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_JEWEL, HEIST_CHOICE_EMPTY)
		g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_JEWEL] = 0
		CPRINTLN(DEBUG_HEIST, "Reset Jewelry heist planning board selections.")
	ENDIF
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_1)
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, HEIST_DOCKS, FALSE)
		INT iCrewIndex
		REPEAT MAX_CREW_SIZE iCrewIndex
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_DOCKS_BLOW_UP_BOAT][iCrewIndex] = CM_EMPTY
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_DOCKS_DEEP_SEA][iCrewIndex] = CM_EMPTY
		ENDREPEAT
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, HEIST_DOCKS, FALSE)
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_DOCKS, HEIST_CHOICE_EMPTY)
		g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_DOCKS] = 0
		CPRINTLN(DEBUG_HEIST, "Reset Docks heist planning board selections.")
	ENDIF
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_1)
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, HEIST_RURAL_BANK, FALSE)
		INT iCrewIndex
		REPEAT MAX_CREW_SIZE iCrewIndex
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_RURAL_NO_TANK][iCrewIndex] = CM_EMPTY
		ENDREPEAT
		g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_RURAL_BANK] = 0
		CPRINTLN(DEBUG_HEIST, "Reset Rural Bank heist planning board selections.")
	ENDIF
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2)
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, HEIST_AGENCY, FALSE)
		INT iCrewIndex
		REPEAT MAX_CREW_SIZE iCrewIndex
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_AGENCY_FIRETRUCK][iCrewIndex] = CM_EMPTY
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_AGENCY_HELICOPTER][iCrewIndex] = CM_EMPTY
		ENDREPEAT
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, HEIST_AGENCY, FALSE)
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY, HEIST_CHOICE_EMPTY)
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_2))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_3))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_4))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_5))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_6))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_7))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_8))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_9))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_10))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_11))
		CPRINTLN(DEBUG_HEIST, "Reset Agency heist planning board selections.")
	ENDIF
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2_INTRO)
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, HEIST_FINALE, FALSE)
		INT iCrewIndex
		REPEAT MAX_CREW_SIZE iCrewIndex
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_FINALE_HELI][iCrewIndex] = CM_EMPTY
			g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_FINALE_TRAFFCONT][iCrewIndex] = CM_EMPTY
		ENDREPEAT
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, HEIST_FINALE, FALSE)
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE, HEIST_CHOICE_EMPTY)
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_2))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_3))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_4))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_5))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_6))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_7))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_8))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_9))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_10))
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_11))
		CPRINTLN(DEBUG_HEIST, "Reset Finale heist planning board selections.")
	ENDIF
ENDPROC
