//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 18/06/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Heist End Screen Header							│
//│																				│
//│			This header contains all the backend script to display and 			│
//│			control the heist end screen.										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_public_core.sch"

USING "Screen_placements.sch"
USING "hud_drawing.sch"
USING "heist_private.sch"

USING "commands_graphics.sch"

USING "flow_public_game.sch"
USING "script_heist.sch"

CONST_FLOAT ES_CENTER_X		0.290
CONST_FLOAT ES_CENTER_Y		0.500
CONST_FLOAT ES_BG_BORDER	0.012
CONST_INT 	ES_TARGET_ALPHA	205	

CONST_FLOAT 	ES_TEXT_LINEBREAK_SMALL		0.03
CONST_FLOAT 	ES_TEXT_LINEBREAK_LARGE		0.045
CONST_FLOAT		ES_TEXT_LEFT_ALIGN			0.338
CONST_FLOAT		ES_TEXT_BREAK				0.010

CONST_INT	ES_CREW_HURT_COST		3000
CONST_INT	ES_CREW_CRITICAL_COST	9000
CONST_INT	ES_CREW_KIA_COST		17000


ENUM EndScreenRectangles
	ESR_TAKE_HEADER,
	ESR_TAKE_HIGHLIGHT,
	ESR_TAKE_BODY,
	ESR_BREAKDOWN_HEADER,
	ESR_BREAKDOWN_HIGHLIGHT,
	ESR_BREAKDOWN_BODY,
	ESR_BREAKDOWN_BODY1,
	ESR_BREAKDOWN_BODY2,
	ESR_BREAKDOWN_BODY3,
	ESR_INFO_HEADER,
	ESR_INFO_HIGHLIGHT,
	ESR_CREW_COLOUR_KEY_BASE,
	ESR_CREW_BODY_BASE,
	ESR_TAKE_GREYLINE
ENDENUM

ENUM EndScreenSprites
	ESS_CREW_PHOTO_BASE,
	ESS_PIE_CHART_TEST,
	ESS_KIA_TEXT_BASE,
	ESS_DEBUG_OVERLAY
ENDENUM

ENUM EndScreenText
	EST_TITLE,
	
	EST_HEADING_TAKE,
	EST_HEADING_BREAKDOWN,
	EST_HEADING_INFO,
	
	EST_SUBHEAD_BREAKDOWN,
	EST_SUBHEAD_PERSONAL_TAKE,
	EST_SUBHEAD_EXPENSES,
	EST_SUBHEAD_SUMMARY,
	EST_SUBHEAD_STOLEN_ITEMS,
	EST_SUBHEAD_VERDICT,
	EST_SUBHEAD_POTENTIAL_TAKE,
	
	EST_SYMBOL_DOLLAR_ACT_TAKE,
	EST_SYMBOL_DOLLAR_EXPENSES,
	EST_SYMBOL_DOLLAR_PER_TAKE,
	
	EST_VALUE_POTENTIAL_TAKE,
	EST_VALUE_ACTUAL_TAKE,
	EST_VALUE_EXPENSES,
	EST_VALUE_SUMMARY,
	EST_VALUE_STOLEN_ITEMS,
	EST_VALUE_VERDICT,
	EST_VALUE_COMPLETE_PERCENTAGE,
	EST_VALUE_MISSION_TIME,
	
	EST_CREW_NAME_BASE,
	EST_CREW_MONEY_BASE,
	EST_CREW_CUT_BASE,
	EST_CREW_PERCENTAGE_BASE,
	EST_CREW_NUMBERING
ENDENUM


ENUM HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_ENUM
		HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_SUMMARY = 0,
		HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_OVERVIEW,
		HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_TAKE,
		HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_CREW,
		
		
		HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_COMPLETE
ENDENUM



PROC PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA(HEIST_END_SCREEN_RAW_DATA &data, 
												INT take, FLOAT percentage, 
												STRING name, STRING image,
												bool kia,
												HEIST_END_SCREEN_CREW_ENTRY_TYPE type)
	//put them in the list
	IF data.iCrewTotal = HEIST_SCREEN_MAX_CREW
		PRINTLN("PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA: no space to add new participant")
		EXIT
	ENDIF
	data.iCrewDollars[data.iCrewTotal] = take
	data.fCrewPercentages[data.iCrewTotal] = percentage
	data.iCrewNames[data.iCrewTotal] = name
	data.iCrewImages[data.iCrewTotal] = image
	data.iCrewStatIndices[data.iCrewTotal] = -1
	data.eCrewTypes[data.iCrewTotal] = type
	data.bKia[data.iCrewTotal] = kia
	
	++data.iCrewTotal
ENDPROC


PROC PRIVATE_ADD_LAST_END_SCREEN_PARTICIPANT_STAT_TO_DATA(HEIST_END_SCREEN_RAW_DATA &data, 
														  INT nth,
														  //INT statEnum,
														  INT statCurrent,
														  INT statIncrement,
														  INT max)
	//
	
	// iCrewStatsForThisMember[HEIST_SCREEN_MAX_CREW]
	// iCrewStatTypeEnum[HEIST_SCREEN_MAX_CREW][5]
	// iCrewStatCurrentPerc[HEIST_SCREEN_MAX_CREW][5]
	// iCrewStatAdditionalPerc[HEIST_SCREEN_MAX_CREW][5]
	
	IF data.iCrewTotal = 0
		EXIT //this command has to interleave with PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA
	ENDIF
	
	IF data.iCrewStatsForThisMember[data.iCrewTotal-1] = 5
		PRINTLN("PRIVATE_ADD_LAST_END_SCREEN_PARTICIPANT_STAT_TO_DATA: no space at index ",data.iCrewTotal)
		EXIT
	ENDIF
	
	IF data.iCrewStatsForThisMember[data.iCrewTotal-1] = 0
		++data.iCrewStatTotal
	ENDIF

	data.iCrewStatIndices[nth] = data.iCrewTotal-1
	INT iindex = data.iCrewStatsForThisMember[data.iCrewTotal-1]
	//data.iCrewStatTypeEnum[nth][iindex] = statEnum
	data.fCrewStatCurrentPerc[nth][iindex] = TO_FLOAT(statCurrent)/TO_FLOAT(max)
	data.fCrewStatAdditionalPerc[nth][iindex] = TO_FLOAT(statIncrement)/TO_FLOAT(max)
	IF data.fCrewStatCurrentPerc[nth][iindex] > 1.0
		data.fCrewStatCurrentPerc[nth][iindex] = 1.0
	ENDIF
	IF data.fCrewStatAdditionalPerc[nth][iindex]+data.fCrewStatCurrentPerc[nth][iindex] > 1.0
		data.fCrewStatAdditionalPerc[nth][iindex] = 1.0 - data.fCrewStatCurrentPerc[nth][iindex]
		
	ENDIF
	++data.iCrewStatsForThisMember[data.iCrewTotal-1]
ENDPROC

FUNC INT PRIVATE_GET_NTH_CREW_INDEX_WITH_STATS(HEIST_END_SCREEN_RAW_DATA &data, INT nth)

	IF data.iCrewTotal < 1
		RETURN -1
	ENDIF
	
		
	INT i = 0
	INT ndown = nth
	REPEAT data.iCrewTotal i
		IF data.eCrewTypes[i] != HENDSCR_PLAYER 
		AND data.eCrewTypes[i] != HENDSCR_EXTRA
		AND (NOT data.bKIA[i])
			IF ndown = 0
				RETURN i
			ENDIF
			--ndown
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC


PROC ADD_PENALTY_TO_HEIST(INT iHeistIndex, HEIST_ENDSCREEN_PENALTY_ENUM item, INT value) 
		PRINTLN("ADD_PENALTY_TO_HEIST : for heist ", iHeistIndex," adding item ", item," of value ",value)
		IF value = 0
			EXIT
		ENDIF
		
		INT ival = value
		
		if ival < 0
			ival *= -1
		ENDIF

		BOOL bNoSpace = FALSE
		IF g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged = MAX_HEIST_PENALTY_ENUM_ENTRIES
			//SCRIPT_ASSERT("ADD_PENALTY_TO_HEIST: failed no space")
			bNoSpace = TRUE
		ENDIF
		
		INT ilogged = g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged		
		
		//check it isn't in the list already
		INT i = 0
		REPEAT ilogged i
			IF g_HeistNoneSavedDatas[iHeistIndex].Penalties[i] = item
				PRINTLN("ADD_PENALTY_TO_HEIST : Overwriting value")
				//found our writable index, increase it
				g_HeistNoneSavedDatas[iHeistIndex].Penalties[i] = item
				g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[i] = ival
				EXIT
			ENDIF
		ENDREPEAT
		

		//wasn't already in the log, make a new entry if possible
		IF bNoSpace
			PRINTLN("ADD_PENALTY_TO_HEIST : Heist penalty wasn't in the log and there was no space to add new")
			SCRIPT_ASSERT("Heist penalty wasn't in the log and there was no space to add new")
			EXIT
		ENDIF
		
		//Set penalty stats for social club to read.
		SWITCH item
			CASE HEIST_PENALTY_MONEY_DROPPED
				SWITCH iHeistIndex
					CASE HEIST_JEWEL 		STAT_SET_INT(HCS_PENALTY_JEWEL_DROP_MONEY, value)	BREAK
					CASE HEIST_RURAL_BANK 	STAT_SET_INT(HCS_PENALTY_PALETO_DROP_MONEY, value)	BREAK
					CASE HEIST_FINALE 		STAT_SET_INT(HCS_PENALTY_BIGS_DROP_MONEY, value)	BREAK
				ENDSWITCH
			BREAK
			CASE HEIST_PENALTY_JCVAN
				STAT_SET_INT(HCS_PENALTY_JEWEL_DROP_MONEY, value)
			BREAK
			CASE HEIST_PENALTY_JCHOUSE
				STAT_SET_INT(HCS_PENALTY_JEWEL_MADR_HOUSE, value)
			BREAK
			CASE HEIST_PENALTY_UNSELLABLE_WEAPON
				STAT_SET_INT(HCS_PENALTY_DOCKS_UNSELL_WPN, value)
			BREAK
			CASE HEIST_PENALTY_AGENTCUT
				STAT_SET_INT(HCS_PENALTY_PALETO_AGENT_CUT, value)
			BREAK
			CASE HEIST_PENALTY_BIGS_SLOW_LOADING
				STAT_SET_INT(HCS_PENALTY_BIGS_SLOW_LOADING, value)
			BREAK
			CASE HEIST_PENALTY_HOSTAGE_GIFT
				STAT_SET_INT(HCS_PENALTY_BIGS_HOSTAGE_GIFT, value)
			BREAK
		ENDSWITCH
		
		PRINTLN("ADD_PENALTY_TO_HEIST : Wrote new value ", ival, " to index ", g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged)
		g_HeistNoneSavedDatas[iHeistIndex].Penalties[g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged] = item
		g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged] = ival
		++g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged
ENDPROC

FUNC BOOL UPDATE_HEIST_END_SCREEN_HELP(INT &iHeistFirstTimeTutorialStageTimer,HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_ENUM  &eHeistCurrentStage)
	
	PRINTLN("Timer: ", iHeistFirstTimeTutorialStageTimer, " Mode: ", eHeistCurrentStage)
	/*
	IF HAS_ONE_TIME_HELP_DISPLAYED(FHM_FIRST_HEIST_PASS)
		//Start displaying help text.
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ES_HELP")
			PRINT_HELP_FOREVER("ES_HELP")
		ENDIF
		PRINTLN("UPDATE_HEIST_END_SCREEN_HELP: Help already done, bail")
		EXIT
	ENDIF
	*/
	
	IF eHeistCurrentStage =  HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_COMPLETE
		//SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_HEIST_PASS)
		//DRAW_SCALEFORM_MOVIE_FULLSCREEN(sEndScreen.continueButton,255,255,255,255)
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sEndScreen.continueButton,255,255,255,255)
		RESET_SCRIPT_GFX_ALIGN()
		//DRAW_SCALEFORM_MOVIE(sEndScreen.continueButton, 0.5,0.5,1.0,1.0,255,255,255,0)
		PRINTLN("UPDATE_HEIST_END_SCREEN_HELP: drawing helpmovie")
		RETURN TRUE
	ENDIF
	

		
		IF iHeistFirstTimeTutorialStageTimer > DEFAULT_GOD_TEXT_TIME
			iHeistFirstTimeTutorialStageTimer = 0
			eHeistCurrentStage = INT_TO_ENUM(HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_ENUM,ENUM_TO_INT(eHeistCurrentStage)+1)
		ENDIF

		IF iHeistFirstTimeTutorialStageTimer = 0
				SWITCH eHeistCurrentStage
				CASE HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_SUMMARY
					//HELP_AT_SCREEN_LOCATION("HEITUTO1", 0.27, 0.5, HELP_TEXT_WEST)
					PRINT_HELP("HEITUTO1")
					BREAK
				CASE HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_OVERVIEW
					//HELP_AT_SCREEN_LOCATION("HEITUTO2", 0.25, 0.79, HELP_TEXT_NORTH)
					PRINT_HELP("HEITUTO2")
					BREAK
				CASE HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_TAKE
					//HELP_AT_SCREEN_LOCATION("HEITUTO3", 0.46, 0.79, HELP_TEXT_NORTH)
					PRINT_HELP("HEITUTO3")
					BREAK
				CASE HEIST_ENDSCREEN_FIRST_TIME_TUTORIAL_CREW
					//HELP_AT_SCREEN_LOCATION("HEITUTO4", 0.65, 0.79, HELP_TEXT_NORTH)
					PRINT_HELP("HEITUTO4")
					BREAK
					
			ENDSWITCH
		ENDIF
		
		
	RETURN FALSE

ENDFUNC



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Utility Procedures ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


FUNC TEXT_LABEL_31 PRIVATE_Comma_Separate_Value(INT iValue)
	//Convert value to text.
	TEXT_LABEL_31 txtValue = iValue
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 txtCommaSeparated
		TEXT_LABEL_31 txtTemp
	
		//Comma separate groups of 3 digits starting from end of value.
		INT iLength = GET_LENGTH_OF_LITERAL_STRING(txtValue)
		WHILE iLength > 3
			txtTemp = txtCommaSeparated
			txtCommaSeparated = GET_STRING_FROM_STRING(txtValue, iLength-3, iLength)
			txtCommaSeparated += txtTemp
			txtTemp = txtCommaSeparated
			txtCommaSeparated = ","
			txtCommaSeparated += txtTemp
			iLength -= 3
		ENDWHILE
		
		//Add remaining digits to start of value.
		txtTemp = txtCommaSeparated
		txtCommaSeparated = GET_STRING_FROM_STRING(txtValue, 0, iLength)
		txtCommaSeparated += txtTemp
		
		RETURN txtCommaSeparated
	#ENDIF
	
	RETURN txtValue
ENDFUNC

/// PURPOSE:
///    Takes the percentage score for this heist and returns the string of the overall verdict
/// PARAMS:
///    fPercentage - percentage score for this heist
/// RETURNS:
///    STRING of the verdict
FUNC STRING GET_HEIST_VERDICT_FROM_PERCENTAGE(FLOAT fPercentage)
	
	IF fPercentage < 20.0
		RETURN "ES_V_VBAD"
		
	ELIF fPercentage < 50.0
		RETURN "ES_V_BAD"
		
	ELIF fPercentage < 80.0
		RETURN "ES_V_MED"
		
	ELIF fPercentage < 90.0
		RETURN "ES_V_GOOD"
		
	ELSE
		RETURN  "ES_V_VGD"
	ENDIF
ENDFUNC

/// PURPOSE:
///    Calculates the percentage score for the heist based on actual take and potential take
/// PARAMS:
///    fPercentage - the percentage (up to the decimal point)
///    fPoint - the percentage (after the decimal point)
///    iHeistIndex - the heist we are checking
PROC GET_PERCENTAGE_SCORE_FOR_HEIST(FLOAT &fPercentage, FLOAT &fPoint, INT iHeistIndex)
	fPercentage = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)/TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake))* 100.0
	fPoint = (fPercentage - TO_FLOAT(FLOOR(fPercentage)))*10.0
ENDPROC



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ End Screen Asset Streaming ╞═══════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC PRIVATE_Request_Heist_ES_Assets()
	REQUEST_MISSION_AUDIO_BANK("HEIST_SCREEN")
	REQUEST_STREAMED_TEXTURE_DICT("HeistHUD")
	REQUEST_ADDITIONAL_TEXT("MISHSTA",OBJECT_TEXT_SLOT)
	
	
	//SCRIPT_ASSERT("Loading heist end screen assets")
	sEndScreen.pieChart = REQUEST_SCALEFORM_MOVIE("heist_endscreen")
	sEndScreen.continueButton = REQUEST_SCALEFORM_MOVIE_INSTANCE("INSTRUCTIONAL_BUTTONS")

	
	WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(sEndScreen.pieChart))
	OR (NOT HAS_SCALEFORM_MOVIE_LOADED(sEndScreen.continueButton))
		PRINTLN("PRIVATE_Request_Heist_ES_Assets waiting for scaleforms to finish loading.")
		WAIT(5)
	ENDWHILE
	
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sEndScreen.pieChart,"SHOW_PIECHART_NUMBERS")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	
	
	
	//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(FALSE)
	sEndScreen.bSformeUploadDone = FALSE
ENDPROC


FUNC BOOL PRIVATE_Have_Heist_ES_Assets_Loaded()
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("HeistHUD")
	AND HAS_ADDITIONAL_TEXT_LOADED(OBJECT_TEXT_SLOT)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC PRIVATE_Set_Heist_ES_Assets_As_No_Longer_Needed()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("HeistHUD")

ENDPROC


PROC PRIVATE_Configure_Heist_ES_Specifics(INT iHeistIndex, MEGA_PLACEMENT_TOOLS &sPlacement)
	IF iHeistIndex = 0
	ENDIF
	IF sPlacement.iHudButtonPressed = 0
	ENDIF
ENDPROC


PROC PRIVATE_Release_Heist_ES_Assets()
	//SCRIPT_ASSERT("Releasing heist end screen assets")
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sEndScreen.pieChart)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sEndScreen.continueButton)
	
	
	RELEASE_MISSION_AUDIO_BANK()
ENDPROC


PROC PRIVATE_Heist_ES_CALCULATE_TOTALS_AND_UPLOAD_TO_SCALEFORM(INT iHeistIndex)//,
																//HEIST_END_SCREEN_RAW_DATA &rawData,
																//HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)
	//uses the base values in 
	//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake
	//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPersonalTake 
	//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake
	//to work out the values that will populate the heist screen

	IF sEndScreen.bSformeUploadDone
		PRINTLN("PRIVATE_Heist_ES_CALCULATE_TOTALS_AND_UPLOAD_TO_SCALEFORM: sEndScreen.bSformeUploadDone is true")
		EXIT 
	ENDIF
		
	PRINTLN("PRIVATE_Heist_ES_CALCULATE_TOTALS_AND_UPLOAD_TO_SCALEFORM: <STAT WATCHER> triggered")
	
	//Set base potential/actual values.
	SWITCH iHeistIndex
		CASE HEIST_FINALE
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake = 201600000
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake = 201600000
			BREAK
		CASE HEIST_AGENCY
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake = 331985
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake = 331985
			BREAK
		CASE HEIST_DOCKS
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake = 20000000
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake = 20000000
			BREAK
	ENDSWITCH
	
	INT iHeistChoice = GET_MISSION_FLOW_INT_VALUE(Get_Heist_Choice_FlowInt_ID(iHeistIndex))
	
	INT iTakeTotal = g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake
	
	PRINTLN("Actual take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " is: ", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
	PRINTLN("Potential take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " is: ", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake)
	
	
	//Add any hardcoded penalties for specific heists.
	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			ADD_PENALTY_TO_HEIST(iHeistIndex, HEIST_PENALTY_JCHOUSE, 2500000)
		BREAK
		CASE HEIST_DOCKS
			ADD_PENALTY_TO_HEIST(iHeistIndex, HEIST_PENALTY_UNSELLABLE_WEAPON, 20000000)
		BREAK
	ENDSWITCH
	
	//Zero crew takes.
	INT index = 0
	REPEAT MAX_CREW_SIZE index
		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iCrewMemberTake[index] = 0
	ENDREPEAT
	
	//Remove penalties from the total, but also from the actual
	index = 0
	INT penalties =0
	REPEAT g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged index
		IF g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[index] > 0
			penalties += g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[index]
			g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake -= g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[index]
		ENDIF
	ENDREPEAT
	iTakeTotal -= penalties
	
	//Calculate the FIB cut for the Paleto Score after other penalties. This is because it's a cut.
	IF iHeistIndex = HEIST_RURAL_BANK
		INT iFIBCut = FLOOR(TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)*0.78)
		ADD_PENALTY_TO_HEIST(iHeistIndex, HEIST_PENALTY_AGENTCUT, iFIBCut)
		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake -= iFIBCut
		iTakeTotal -= iFIBCut
	ENDIF
	
	PRINTLN("Actual take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " after penalties is: ", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
	
	//Calculate crew member takes.
	REPEAT MAX_CREW_SIZE index
		CrewMember eMember = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][index]
		SWITCH g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].eCrewStatus[index]
			//Crew member survived, give them their pre-determied cut.
			CASE CMST_FINE
			CASE CMST_INJURED
			CASE CMST_NOT_SET
				g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iCrewMemberTake[index] = CEIL((TO_FLOAT(g_sCrewMemberStaticData[eMember].jobCut)*0.01)*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
				//PRINTLN("Set crew member take value to ", CEIL((TO_FLOAT(g_sCrewMemberStaticData[eMember].jobCut)*0.01)*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)," ")
				BREAK
				
			//Crew member died, pay a funeral bill that is 1.5x their pre-arranged cut.
			CASE CMST_KILLED
				g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iCrewMemberTake[index] = CEIL((TO_FLOAT(g_sCrewMemberStaticData[eMember].jobCut)*0.015)*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
				BREAK
		ENDSWITCH
	ENDREPEAT
	
	
	//Deduct hired crew cuts from the amount left for the player characters.
	index = 0
	REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize index
		iTakeTotal -= g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iCrewMemberTake[index]
	ENDREPEAT
	
	PRINTLN("Total take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " after crew pay is: ", iTakeTotal)
	
	
	//Zero the player character's and Lester's takes.
	g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].iPlayerTake[0] = 0
	g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].iPlayerTake[1] = 0
	g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].iPlayerTake[2] = 0
	sEndScreen.iLesterTotal = 0
	
	
	//Calculate the player character's takes and Lester's take.
	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			//Involved // MIKE, FRANK, LESTER
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].fPlayerPercentage[0] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].fPlayerPercentage[1] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].fPlayerPercentage[2] = -1 //no trev
			
			//Franklin takes 12%
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].iPlayerTake[1] = CEIL(0.12*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
			iTakeTotal -= g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].iPlayerTake[1]
			
			//Lester takes 14%
			sEndScreen.iLesterTotal = CEIL(0.14*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
			iTakeTotal -= sEndScreen.iLesterTotal
			sEndScreen.fLesterPercentage = 14
			
			//Mike takes the remainder
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_JEWEL].iPlayerTake[0] = iTakeTotal
		BREAK
		
		CASE HEIST_DOCKS
			
			//Involved // MIKE, FRANK, TREV
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_DOCKS].fPlayerPercentage[0] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_DOCKS].fPlayerPercentage[1] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_DOCKS].fPlayerPercentage[2] = 0

			g_savedGlobals.sHeistData.sEndScreenData[HEIST_DOCKS].iPlayerTake[0] = 0//iTakeTotal/3
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_DOCKS].iPlayerTake[1] = 0//iTakeTotal/3
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_DOCKS].iPlayerTake[2] = 0//iTakeTotal/3
			
			sEndScreen.fLesterPercentage = -1//no lester
			//sEndScreen.iSpecificSpecialCharPercentage = -1//no specific char
		BREAK

		CASE HEIST_RURAL_BANK
		
			//involved extra // MIKE, FRANK, TREV, LESTER
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_RURAL_BANK].fPlayerPercentage[0] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_RURAL_BANK].fPlayerPercentage[1] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_RURAL_BANK].fPlayerPercentage[2] = 0
			
			//Lester takes a flat 12%
			sEndScreen.iLesterTotal = CEIL(0.12*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
			iTakeTotal -= sEndScreen.iLesterTotal
			sEndScreen.fLesterPercentage = 12

			g_savedGlobals.sHeistData.sEndScreenData[HEIST_RURAL_BANK].iPlayerTake[0] = iTakeTotal/3 
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_RURAL_BANK].iPlayerTake[1] = iTakeTotal/3
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_RURAL_BANK].iPlayerTake[2] = iTakeTotal/3
			
			sEndScreen.fLesterPercentage = 0
		BREAK
		
		CASE HEIST_AGENCY

			//involved extra // MIKE, FRANK, TREV, LESTER, AGENCY
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_AGENCY].fPlayerPercentage[0] = -1
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_AGENCY].fPlayerPercentage[1] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_AGENCY].fPlayerPercentage[2] = 0
			//lester
			
			//lester takes a flat 12
			sEndScreen.iLesterTotal = CEIL(0.12*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
			iTakeTotal -= sEndScreen.iLesterTotal
			sEndScreen.fLesterPercentage = 12
			
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_AGENCY].iPlayerTake[1] = iTakeTotal
		BREAK
		
		CASE HEIST_FINALE
		
			//involved extra // MIKE, FRANK, TREV, LESTER
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_FINALE].fPlayerPercentage[0] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_FINALE].fPlayerPercentage[1] = 0
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_FINALE].fPlayerPercentage[2] = 0
			
			//lester takes a flat 12
			sEndScreen.iLesterTotal = CEIL(0.12*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
			iTakeTotal -= sEndScreen.iLesterTotal
			sEndScreen.fLesterPercentage = 12
			
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_FINALE].iPlayerTake[0] = iTakeTotal/3
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_FINALE].iPlayerTake[1] = iTakeTotal/3
			g_savedGlobals.sHeistData.sEndScreenData[HEIST_FINALE].iPlayerTake[2] = iTakeTotal/3
			
			sEndScreen.fLesterPercentage = 0
		BREAK
	ENDSWITCH 
	
	PRINTLN("Total take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " for Michael is: 	", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0])
	PRINTLN("Total take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " for Franklin is: 	", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1])
	PRINTLN("Total take for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " for Trevor is: 	", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2])
	
	//PRINTLN("IPBLAH : " ,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0] ," " ,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1] , " " ,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2])
	//work out player char percentage splits mike takes 2 shares trev and frank 1 share each

	INT totalDollarsincludingCrewCut = g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake

	//Calculate player's percentages.
	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0] != -1
		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0] = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0])/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
	ENDIF
	
	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1] != -1
		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1] = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1])/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
	ENDIF

	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2] != -1
		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2] = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2])/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
	ENDIF

	IF sEndScreen.fLesterPercentage != -1
		sEndScreen.fLesterPercentage = (TO_FLOAT(sEndScreen.iLesterTotal)/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
	ENDIF
	

	//Do heist specific raw stat upload
	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake
			STAT_SET_INT(JEWEL_HEIST_RAW_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake)
		BREAK
	ENDSWITCH

	INT sum_of_player_cuts = 0
	INT i = 0
	REPEAT 3 i
		sum_of_player_cuts += g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[i]
	ENDREPEAT
	
	PRINTLN("sum of player cuts for heist ", GET_HEIST_INDEX_DEBUG_STRING(iHeistIndex), " is: ", sum_of_player_cuts)
	
	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			//SWITCH Get_Heist_Choice_FlowInt_ID(FLOWINT_HEIST_CHOICE_JEWEL)
			//	HEIST_CHOICE_JEWEL_STEALTH
			//	HEIST_CHOICE_JEWEL_HIGH_IMPACT
			//ENDSWITCH
			//jewel only has one mission script
			//so the variants have to currently use the same stats

			//JH2A_ACTUAL_TAKE
			//JH2A_PERSONAL_TAKE
			INFORM_MISSION_STATS_OF_INCREMENT(JH2A_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
			INFORM_MISSION_STATS_OF_INCREMENT(JH2A_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
		BREAK
		CASE HEIST_DOCKS
			SWITCH  GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_DOCKS)
				CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
					//DH2A_ACTUAL_TAKE
					//DH2A_PERSONAL_TAKE
					INFORM_MISSION_STATS_OF_INCREMENT(DH2A_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
					INFORM_MISSION_STATS_OF_INCREMENT(DH2A_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
					BREAK
				CASE HEIST_CHOICE_DOCKS_DEEP_SEA
					//DH2B_ACTUAL_TAKE
					//DH2B_PERSONAL_TAKE
					INFORM_MISSION_STATS_OF_INCREMENT(DH2B_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
					INFORM_MISSION_STATS_OF_INCREMENT(DH2B_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
					BREAK
			ENDSWITCH
		BREAK
		CASE HEIST_RURAL_BANK
			//SWITCH  Get_Heist_Choice_FlowInt_ID(FLOWINT_HEIST_CHOICE_RURAL)
			//ENDSWITCH
			//no variance 
			//RH2_ACTUAL_TAKE
			//RH2_PERSONAL_TAKE
			
			PRINTLN("sum of player cuts from rural heist is ", sum_of_player_cuts)
			
			INFORM_MISSION_STATS_OF_INCREMENT(RH2_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
			INFORM_MISSION_STATS_OF_INCREMENT(RH2_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
		BREAK
		CASE HEIST_AGENCY
			SWITCH  GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY)
				CASE HEIST_CHOICE_AGENCY_FIRETRUCK
					//AH3A_ACTUAL_TAKE
					//AH3A_PERSONAL_TAKE
					//INFORM_MISSION_STATS_OF_INCREMENT(AH3A_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
					//INFORM_MISSION_STATS_OF_INCREMENT(AH3A_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
					BREAK
				CASE HEIST_CHOICE_AGENCY_HELICOPTER
					//AH3B_ACTUAL_TAKE
					//AH3B_PERSONAL_TAKE
					//INFORM_MISSION_STATS_OF_INCREMENT(AH3B_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
					//INFORM_MISSION_STATS_OF_INCREMENT(AH3B_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
					BREAK
			ENDSWITCH
		BREAK
		CASE HEIST_FINALE
			SWITCH  GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE)
				CASE HEIST_CHOICE_FINALE_TRAFFCONT
					//FH2A_ACTUAL_TAKE
					//FH2A_PERSONAL_TAKE
					INFORM_MISSION_STATS_OF_INCREMENT(FH2A_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
					INFORM_MISSION_STATS_OF_INCREMENT(FH2A_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
					BREAK
				CASE HEIST_CHOICE_FINALE_HELI
					//FH2B_ACTUAL_TAKE
					//FH2B_PERSONAL_TAKE
					INFORM_MISSION_STATS_OF_INCREMENT(FH2B_ACTUAL_TAKE,g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake,TRUE)
					INFORM_MISSION_STATS_OF_INCREMENT(FH2B_PERSONAL_TAKE,sum_of_player_cuts,TRUE)
					BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH 

	sEndScreen.bSformeUploadDone = TRUE
	g_bMissionStatSystemBlocker = FALSE//unblocked upload
	
	
//	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0] != -1
//		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0] = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0])/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
//		PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA(rawData,
//						g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0],
//						g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0],
//													"ACCNA_MIKE",
//													"HC_MICHAEL",
//													FALSE,
//													HENDSCR_PLAYER)
//	ENDIF
//	
//	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1] != -1
//		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1] = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1])/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
//		PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA(rawData,
//								g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1],
//								g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1],
//													"ACCNA_FRANKLIN",
//													"HC_FRANKLIN",
//													FALSE,
//													HENDSCR_PLAYER)
//	ENDIF
//
//	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2] != -1
//		g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2] = (TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2])/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
//		PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA(rawData,
//					g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2],
//					g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2],
//													"ACCNA_TREVOR",
//													"HC_TREVOR",
//													FALSE,
//													HENDSCR_PLAYER)
//	ENDIF
//
//	IF sEndScreen.fLesterPercentage != -1
//		sEndScreen.fLesterPercentage = (TO_FLOAT(sEndScreen.iLesterTotal)/TO_FLOAT(totalDollarsincludingCrewCut))*100.0
//		PRIVATE_ADD_END_SCREEN_PARTICIPANT_TO_DATA(rawData,
//								sEndScreen.iLesterTotal,
//								sEndScreen.fLesterPercentage,
//													"ACCNA_LES",
//													"HC_LESTER",
//													FALSE,
//													HENDSCR_EXTRA)
//	ENDIF
		
	
	//rawData.iActual = g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake
	//rawData.iTotal = g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ End Screen HUD Configuring ╞═══════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC PRIVATE_Configure_Heist_ES_Rectangles(MEGA_PLACEMENT_TOOLS &sPlacement)
		EndScreenRectangles eRectIndex
		
		eRectIndex = ESR_TAKE_HEADER
		sPlacement.RectPlacement[eRectIndex].x = 0.300
		sPlacement.RectPlacement[eRectIndex].y = 0.187
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.043
		sPlacement.RectPlacement[eRectIndex].r = 255	
		sPlacement.RectPlacement[eRectIndex].g = 255	
		sPlacement.RectPlacement[eRectIndex].b = 255
		sPlacement.RectPlacement[eRectIndex].a = 255
		
		
		eRectIndex = ESR_TAKE_HIGHLIGHT
		sPlacement.RectPlacement[eRectIndex].x = 0.300
		sPlacement.RectPlacement[eRectIndex].y = 0.169
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.007
		sPlacement.RectPlacement[eRectIndex].r = 103	
		sPlacement.RectPlacement[eRectIndex].g = 153		
		sPlacement.RectPlacement[eRectIndex].b = 104
		sPlacement.RectPlacement[eRectIndex].a = 255
		
		
		eRectIndex = ESR_TAKE_BODY
		sPlacement.RectPlacement[eRectIndex].x = 0.300
		sPlacement.RectPlacement[eRectIndex].y = 0.522
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.599
		sPlacement.RectPlacement[eRectIndex].r = 0	
		sPlacement.RectPlacement[eRectIndex].g = 0	
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 128
		

		eRectIndex = ESR_TAKE_GREYLINE 
		sPlacement.RectPlacement[eRectIndex].x = 0.300
		sPlacement.RectPlacement[eRectIndex].y = 0.522
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.025
		sPlacement.RectPlacement[eRectIndex].r = 255		
		sPlacement.RectPlacement[eRectIndex].g = 255		
		sPlacement.RectPlacement[eRectIndex].b = 255
		sPlacement.RectPlacement[eRectIndex].a = 8


		eRectIndex = ESR_BREAKDOWN_HEADER
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.187
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.043
		sPlacement.RectPlacement[eRectIndex].r = 255		
		sPlacement.RectPlacement[eRectIndex].g = 255		
		sPlacement.RectPlacement[eRectIndex].b = 255
		sPlacement.RectPlacement[eRectIndex].a = 255
		
		eRectIndex = ESR_BREAKDOWN_HIGHLIGHT
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.169
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.007
		sPlacement.RectPlacement[eRectIndex].r = 103	
		sPlacement.RectPlacement[eRectIndex].g = 153		
		sPlacement.RectPlacement[eRectIndex].b = 104
		sPlacement.RectPlacement[eRectIndex].a = 255
		

		eRectIndex = ESR_BREAKDOWN_BODY
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.2472
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.051
		sPlacement.RectPlacement[eRectIndex].r = 0		
		sPlacement.RectPlacement[eRectIndex].g = 0		
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 128

		/*
			//TODO
			ESR_BREAKDOWN_BODY1,
			ESR_BREAKDOWN_BODY2,
			ESR_BREAKDOWN_BODY3,
		*/
		eRectIndex = ESR_BREAKDOWN_BODY1
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.437
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.32
		sPlacement.RectPlacement[eRectIndex].r = 0		
		sPlacement.RectPlacement[eRectIndex].g = 0		
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 128
		
		eRectIndex = ESR_BREAKDOWN_BODY2
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.626
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.051
		sPlacement.RectPlacement[eRectIndex].r = 0		
		sPlacement.RectPlacement[eRectIndex].g = 0		
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 128
		
		eRectIndex = ESR_BREAKDOWN_BODY3
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.737
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.165
		sPlacement.RectPlacement[eRectIndex].r = 0		
		sPlacement.RectPlacement[eRectIndex].g = 0		
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 128
		
		
		
		
		
		eRectIndex = ESR_INFO_HEADER
		sPlacement.RectPlacement[eRectIndex].x = 0.700
		sPlacement.RectPlacement[eRectIndex].y = 0.187
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.043
		sPlacement.RectPlacement[eRectIndex].r = 255		
		sPlacement.RectPlacement[eRectIndex].g = 255		
		sPlacement.RectPlacement[eRectIndex].b = 255
		sPlacement.RectPlacement[eRectIndex].a = 255
		
		eRectIndex = ESR_INFO_HIGHLIGHT
		sPlacement.RectPlacement[eRectIndex].x = 0.700
		sPlacement.RectPlacement[eRectIndex].y = 0.169
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.007
		sPlacement.RectPlacement[eRectIndex].r = 103	
		sPlacement.RectPlacement[eRectIndex].g = 153		
		sPlacement.RectPlacement[eRectIndex].b = 104
		sPlacement.RectPlacement[eRectIndex].a = 255
		/* //verdict bar is gone
		eRectIndex = ESR_VERDICT_BAR_BACKGROUND
		sPlacement.RectPlacement[eRectIndex].x = 0.500
		sPlacement.RectPlacement[eRectIndex].y = 0.274
		sPlacement.RectPlacement[eRectIndex].w = 0.178
		sPlacement.RectPlacement[eRectIndex].h = 0.010
		sPlacement.RectPlacement[eRectIndex].r = 76			
		sPlacement.RectPlacement[eRectIndex].g = 77			
		sPlacement.RectPlacement[eRectIndex].b = 76
		sPlacement.RectPlacement[eRectIndex].a = 255
		*/
		eRectIndex = ESR_CREW_COLOUR_KEY_BASE
		sPlacement.RectPlacement[eRectIndex].x = 0.607
		sPlacement.RectPlacement[eRectIndex].y = 0.259
		sPlacement.RectPlacement[eRectIndex].w = 0.012
		sPlacement.RectPlacement[eRectIndex].h = 0.072
		sPlacement.RectPlacement[eRectIndex].r = 203			
		sPlacement.RectPlacement[eRectIndex].g = 0			
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 255
		
		eRectIndex = ESR_CREW_BODY_BASE
		sPlacement.RectPlacement[eRectIndex].x = 0.700
		sPlacement.RectPlacement[eRectIndex].y = 0.259
		sPlacement.RectPlacement[eRectIndex].w = 0.198
		sPlacement.RectPlacement[eRectIndex].h = 0.073
		sPlacement.RectPlacement[eRectIndex].r = 0			
		sPlacement.RectPlacement[eRectIndex].g = 0			
		sPlacement.RectPlacement[eRectIndex].b = 0
		sPlacement.RectPlacement[eRectIndex].a = 128

ENDPROC


PROC PRIVATE_Configure_Heist_ES_Sprites(MEGA_PLACEMENT_TOOLS &sPlacement)
	EndScreenSprites eSpriteIndex
	
	eSpriteIndex = ESS_CREW_PHOTO_BASE
	sPlacement.SpritePlacement[eSpriteIndex].x = 0.629
	sPlacement.SpritePlacement[eSpriteIndex].y = 0.259
	sPlacement.SpritePlacement[eSpriteIndex].w = 0.040
	sPlacement.SpritePlacement[eSpriteIndex].h = 0.071
	sPlacement.SpritePlacement[eSpriteIndex].r = 255
	sPlacement.SpritePlacement[eSpriteIndex].g = 255
	sPlacement.SpritePlacement[eSpriteIndex].b = 255
	sPlacement.SpritePlacement[eSpriteIndex].a = 255
	
	eSpriteIndex = ESS_PIE_CHART_TEST
	sPlacement.SpritePlacement[eSpriteIndex].x = 0.501
	sPlacement.SpritePlacement[eSpriteIndex].y = 0.437
	sPlacement.SpritePlacement[eSpriteIndex].w = 0.183
	sPlacement.SpritePlacement[eSpriteIndex].h = 0.324
	sPlacement.SpritePlacement[eSpriteIndex].r = 255
	sPlacement.SpritePlacement[eSpriteIndex].g = 0
	sPlacement.SpritePlacement[eSpriteIndex].b = 0
	sPlacement.SpritePlacement[eSpriteIndex].a = 255
	
	eSpriteIndex = ESS_KIA_TEXT_BASE
	sPlacement.SpritePlacement[eSpriteIndex].x = 0.768
	sPlacement.SpritePlacement[eSpriteIndex].y = 0.259
	sPlacement.SpritePlacement[eSpriteIndex].w = 0.060
	sPlacement.SpritePlacement[eSpriteIndex].h = 0.071
	sPlacement.SpritePlacement[eSpriteIndex].r = 255
	sPlacement.SpritePlacement[eSpriteIndex].g = 255
	sPlacement.SpritePlacement[eSpriteIndex].b = 255
	sPlacement.SpritePlacement[eSpriteIndex].a = 255
	
	
	eSpriteIndex = ESS_DEBUG_OVERLAY
	sPlacement.SpritePlacement[eSpriteIndex].x = 0.5
	sPlacement.SpritePlacement[eSpriteIndex].y = 0.5
	sPlacement.SpritePlacement[eSpriteIndex].w = 1.0
	sPlacement.SpritePlacement[eSpriteIndex].h = 1.0
	sPlacement.SpritePlacement[eSpriteIndex].r = 255
	sPlacement.SpritePlacement[eSpriteIndex].g = 255
	sPlacement.SpritePlacement[eSpriteIndex].b = 255
ENDPROC

PROC PRIVATE_Configure_Heist_ES_Text(MEGA_PLACEMENT_TOOLS &sPlacement)
	EndScreenText eTextIndex
	
	eTextIndex = EST_TITLE
	sPlacement.TextPlacement[eTextIndex].x = 0.198
	sPlacement.TextPlacement[eTextIndex].y = 0.1


	eTextIndex = EST_HEADING_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.207
	sPlacement.TextPlacement[eTextIndex].y = 0.178

	eTextIndex = EST_HEADING_BREAKDOWN
	sPlacement.TextPlacement[eTextIndex].x = 0.407
	sPlacement.TextPlacement[eTextIndex].y = 0.178
	
	eTextIndex = EST_HEADING_INFO
	sPlacement.TextPlacement[eTextIndex].x = 0.607
	sPlacement.TextPlacement[eTextIndex].y = 0.178

	eTextIndex = EST_SUBHEAD_BREAKDOWN
	sPlacement.TextPlacement[eTextIndex].x = 0.206
	sPlacement.TextPlacement[eTextIndex].y = 0.230
	
	eTextIndex = EST_SUBHEAD_EXPENSES
	sPlacement.TextPlacement[eTextIndex].x = 0.206
	sPlacement.TextPlacement[eTextIndex].y = 0.328
	
	eTextIndex = EST_SUBHEAD_PERSONAL_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.206
	sPlacement.TextPlacement[eTextIndex].y = 0.426
	
	eTextIndex = EST_SUBHEAD_SUMMARY
	sPlacement.TextPlacement[eTextIndex].x = 0.409
	sPlacement.TextPlacement[eTextIndex].y = 0.651
	
	eTextIndex = EST_SUBHEAD_STOLEN_ITEMS
	sPlacement.TextPlacement[eTextIndex].x = 0.206
	sPlacement.TextPlacement[eTextIndex].y = 0.592
	
	eTextIndex = EST_SUBHEAD_VERDICT
	sPlacement.TextPlacement[eTextIndex].x = 0.409
	sPlacement.TextPlacement[eTextIndex].y = 0.231
	
	eTextIndex = EST_SUBHEAD_POTENTIAL_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.459
	sPlacement.TextPlacement[eTextIndex].y = 0.433
	
	eTextIndex = EST_SYMBOL_DOLLAR_ACT_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.208
	sPlacement.TextPlacement[eTextIndex].y = 0.260
	
	eTextIndex = EST_SYMBOL_DOLLAR_EXPENSES
	sPlacement.TextPlacement[eTextIndex].x = 0.208
	sPlacement.TextPlacement[eTextIndex].y = 0.358
	
	eTextIndex = EST_SYMBOL_DOLLAR_PER_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.208
	sPlacement.TextPlacement[eTextIndex].y = 0.454
	
	eTextIndex = EST_VALUE_ACTUAL_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.403
	sPlacement.TextPlacement[eTextIndex].y = 0.218
	
	eTextIndex = EST_VALUE_EXPENSES
	sPlacement.TextPlacement[eTextIndex].x = 0.219
	sPlacement.TextPlacement[eTextIndex].y = 0.354
	
	eTextIndex = EST_VALUE_POTENTIAL_TAKE
	sPlacement.TextPlacement[eTextIndex].x = 0.403
	sPlacement.TextPlacement[eTextIndex].y = 0.596
	
	eTextIndex = EST_VALUE_SUMMARY
	sPlacement.TextPlacement[eTextIndex].x = 0.411
	sPlacement.TextPlacement[eTextIndex].y = 0.652
	
	eTextIndex = EST_VALUE_STOLEN_ITEMS
	sPlacement.TextPlacement[eTextIndex].x = 0.208
	sPlacement.TextPlacement[eTextIndex].y = 0.618
	
	eTextIndex = EST_VALUE_VERDICT
	sPlacement.TextPlacement[eTextIndex].x = 0.460
	sPlacement.TextPlacement[eTextIndex].y = 0.233
	
	eTextIndex = EST_VALUE_COMPLETE_PERCENTAGE
	sPlacement.TextPlacement[eTextIndex].x = 0.556
	sPlacement.TextPlacement[eTextIndex].y = 0.233
	
	eTextIndex = EST_VALUE_MISSION_TIME
	sPlacement.TextPlacement[eTextIndex].x = 0.411
	sPlacement.TextPlacement[eTextIndex].y = 0.289
	
	
	eTextIndex = EST_CREW_NAME_BASE
	sPlacement.TextPlacement[eTextIndex].x = 0.651
	sPlacement.TextPlacement[eTextIndex].y = 0.232
	
	eTextIndex = EST_CREW_MONEY_BASE
	sPlacement.TextPlacement[eTextIndex].x = 0.651
	sPlacement.TextPlacement[eTextIndex].y = 0.260
	
	eTextIndex = EST_CREW_CUT_BASE
	sPlacement.TextPlacement[eTextIndex].x = 0.756
	sPlacement.TextPlacement[eTextIndex].y = 0.260//0.227
	
	eTextIndex = EST_CREW_PERCENTAGE_BASE
	sPlacement.TextPlacement[eTextIndex].x = 0.780
	sPlacement.TextPlacement[eTextIndex].y = 0.226

	eTextIndex =  EST_CREW_NUMBERING
	sPlacement.TextPlacement[eTextIndex].x = 0.600
	sPlacement.TextPlacement[eTextIndex].y = 0.226
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ End Screen Rendering ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC PRIVATE_Draw_Rect_Column(RECT sBase, INT iCount, FLOAT fOffset)
	INT index
	REPEAT iCount index
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		DRAW_RECTANGLE(sBase)
		sBase.y += fOffset
	ENDREPEAT
ENDPROC


PROC PRIVATE_Draw_Heist_ES_Background(MEGA_PLACEMENT_TOOLS &sPlacement)


		
	UNUSED_PARAMETER(sPlacement)
	/*
	DRAW_SPRITE("HeistHUD", "tempbla", 
	0.5, 0.5, 
	1.0, 1.0, 
	0, 
	255, 255, 255, 128)
	
	
	
	

	
	SET_TEXT_FONT(FONT_STANDARD)

	//SET_TEXT_WRAP(Style.WrapStartX, Style.WrapEndX)

	SET_TEXT_SCALE(1.0, 0.70)
	//SET_TEXT_COLOUR(Style.r, Style.g, Style.b, Style.a)

	//SET_TEXT_OUTLINE()
	//SET_TEXT_DROP_SHADOW()
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	TEXT_LABEL test = "Crew Name Here"
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
	ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(test)
	END_TEXT_COMMAND_DISPLAY_TEXT(0.1984, 0.1000)
*/
	
	//DRAW_RECT(0.5,0.5,0.5,0.5,255,255,255,255)
	
	/*
	// Draw main background gradient.
	SPRITE_PLACEMENT MainBackground
	MainBackground.x = 0.500 
	MainBackground.y = 0.511
	MainBackground.w = 0.678
	MainBackground.h = 1.568
	MainBackground.r = 0
	MainBackground.g = 0
	MainBackground.b = 0
	MainBackground.a = 200	
	DRAW_2D_SPRITE("HeistHUD", "Main_Gradient", MainBackground, FALSE,HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_BEFORE_HUD)
	
	// Draw individual background rectangles.
	INT index
	REPEAT ESR_CREW_COLOUR_KEY_BASE index
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		DRAW_RECTANGLE(sPlacement.RectPlacement[index])
	ENDREPEAT
	
	// Draw columns of background rectangles.
	PRIVATE_Draw_Rect_Column(sPlacement.RectPlacement[ESR_CREW_BODY_BASE], 	8, 	0.075)
	*/
ENDPROC


PROC PRIVATE_Draw_Heist_ES_Base_Text(MEGA_PLACEMENT_TOOLS &sPlacement, BOOL bAgencyHeaders)
	
	BOOL temp = bAgencyHeaders
	temp = temp
	
	// Draw section headings.
	SET_TEXT_BLACK(sPlacement.aStyle.TS_STANDARDSMALLHUD)
	DRAW_TEXT(sPlacement.TextPlacement[EST_HEADING_TAKE], 			sPlacement.aStyle.TS_STANDARDSMALLHUD, "ES_H_TAKE",TRUE)
	DRAW_TEXT(sPlacement.TextPlacement[EST_HEADING_BREAKDOWN],		sPlacement.aStyle.TS_STANDARDSMALLHUD, "ES_H_BRKD",TRUE)
	DRAW_TEXT(sPlacement.TextPlacement[EST_HEADING_INFO],			sPlacement.aStyle.TS_STANDARDSMALLHUD, "ES_H_INFO",TRUE)



	//CEIL( 100.0*(TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)/TO_FLOAT(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake)))
	


	// Draw sub headings.

		/*
	IF NOT bAgencyHeaders
		DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_ACTUAL_TAKE], 	sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_ACTU")
		//DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_PERSONAL_TAKE], 	sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_PSNL")
		DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_EXPENSES], 		sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_PSNL")//"ES_S_EXPN")
		
		
		DRAW_TEXT(sPlacement.TextPlacement[EST_SYMBOL_DOLLAR_EXPENSES], sPlacement.aStyle.TS_UI_DASHDOUBLE,	"DOLLR")
	ELSE
		DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_ACTUAL_TAKE], 	sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_ACTU_AG")
		//DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_PERSONAL_TAKE], 	sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_PSNL_AG")
		//DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_EXPENSES], 		sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_PSNL_AG")//"ES_S_EXPN_AG")
	ENDIF
	*/
	/*
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_SUMMARY], 		sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_SUMM")
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_STOLEN_ITEMS], 	sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_STLN")
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_VERDICT], 		sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_VERD")
	
	//scaleform text now
	//DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_POTENTIAL_TAKE], sPlacement.aStyle.TS_STANDARDMEDIUM, "ES_S_POTN")
	
	
	
	// Draw symbols.
	DRAW_TEXT(sPlacement.TextPlacement[EST_SYMBOL_DOLLAR_ACT_TAKE], sPlacement.aStyle.TS_UI_DASHDOUBLE, "DOLLR")
	
	//DRAW_TEXT(sPlacement.TextPlacement[EST_SYMBOL_DOLLAR_PER_TAKE], sPlacement.aStyle.TS_UI_DASHDOUBLE, "DOLLR")
	*/
	
	
	
ENDPROC



PROC PRIVATE_Draw_Heist_ES_Summary(INT iHeistIndex, MEGA_PLACEMENT_TOOLS &sPlacement)
	
	STRING strTitleLabel
	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			strTitleLabel ="ES_SUMM1"
		BREAK
		CASE HEIST_DOCKS
			strTitleLabel ="ES_T_DOCK"
		BREAK
		CASE HEIST_RURAL_BANK
			strTitleLabel ="ES_T_RURL"
		BREAK
		CASE HEIST_AGENCY
			strTitleLabel ="ES_T_AGEN"
		BREAK
		CASE HEIST_FINALE
			strTitleLabel ="ES_T_FIN"
		BREAK
	ENDSWITCH 
	
	//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].tLastSummary
	

		
	//SET_TEXT_WRAP(//ESR_TAKE_BODY
	
	//FLOAT hw = sPlacement.RectPlacement[ESR_TAKE_BODY].x + sPlacement.RectPlacement[ESR_TAKE_BODY].w
	//FLOAT l = sPlacement.RectPlacement[ESR_TAKE_BODY].x  - hw

	//SET_TEXT_WRAP(l,l+sPlacement.RectPlacement[ESR_TAKE_BODY].w)
	SET_TEXT_WRAP(0.0,0.600)
	///SET_TEXT_LINE_HEIGHT_MULT(1.5)
	//
	
	
	//SET_TEXT_WRAPPED_TO_RECT(sPlacement.aStyle.TS_STANDARDSMALLHUD,sPlacement.RectPlacement[ESR_TAKE_BODY])
	DRAW_TEXT(sPlacement.TextPlacement[EST_VALUE_SUMMARY], sPlacement.aStyle.TS_STANDARDSMALLHUD, strTitleLabel,TRUE)
	//SCRIPT_ASSERT("PRIVATE_Draw_Heist_ES_Summary triggered testing longer assert because shenanigens suspected")
	
	/*SET_TEXT_STYLE(sPlacement.aStyle.TS_STANDARDSMALLHUD)	
	
	SET_TEXT_RIGHT_JUSTIFY(bAlignRight)
	SET_TEXT_CENTRE(bAlignCentre)	
	*/


	//DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), strTitleLabel)
	/*BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
	END_TEXT_COMMAND_DISPLAY_TEXT(0.0, 0.0)*/
ENDPROC


PROC PRIVATE_Draw_Heist_ES_Values(INT iHeistIndex, MEGA_PLACEMENT_TOOLS &sPlacement)
	//SCRIPT_ASSERT("PRIVATE_Draw_Heist_ES_Values triggered testing longer assert because shenanigens suspected")
	// Title.
	STRING strTitleLabel
	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			strTitleLabel ="ES_T_JEWL"
		BREAK
		CASE HEIST_DOCKS
			strTitleLabel ="ES_T_DOCK"
		BREAK
		CASE HEIST_RURAL_BANK
			strTitleLabel ="ES_T_RURL"
		BREAK
		CASE HEIST_AGENCY
			strTitleLabel ="ES_T_AGEN"
		BREAK
		CASE HEIST_FINALE
			strTitleLabel ="ES_T_FIN"
		BREAK
	ENDSWITCH 
	
	
	SET_MEGA_HUD_TEXT_DETAILS(sPlacement)
	DRAW_TEXT(sPlacement.TextPlacement[EST_TITLE] , sPlacement.aStyle.TS_TITLEHUD, strTitleLabel,TRUE)

	

	// Potential take.
	TEXT_LABEL_31 txtValue = "$"


	// Actual take.
	
	 //old actual
	 TEXT_LABEL_31 tmptxt = PRIVATE_Comma_Separate_Value(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake)
	txtValue +=  tmptxt
	
	
	DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_VALUE_ACTUAL_TAKE],
	sPlacement.aStyle.TS_STANDARDSMALLHUD, "Take", "STRING")
	SET_TEXT_WRAP(0.0,0.600)
	DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_VALUE_ACTUAL_TAKE],
	sPlacement.aStyle.TS_TITLE, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)

	//DRAW_LINE_2D(0.477,0.275,0.517,0.262,0.004,255,255,255,255) //removed
	
	txtValue = "$"
	DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_VALUE_POTENTIAL_TAKE], sPlacement.aStyle.TS_STANDARDSMALLHUD, "Target", "STRING",HUD_COLOUR_GREY)
	tmptxt = PRIVATE_Comma_Separate_Value(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake)
	txtValue += tmptxt
	SET_TEXT_WRAP(0.0,0.600)
	DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_VALUE_POTENTIAL_TAKE], sPlacement.aStyle.TS_TITLE, txtValue, "STRING",HUD_COLOUR_GREY,FONT_RIGHT)
	// Expenses
	
	
	
	
	//do new wrapped details column // maybe move this out later
	
	//txtValue = PRIVATE_Comma_Separate_Value(sEndScreen.iExpenses)
	/*
	IF NOT (iHeistIndex = HEIST_AGENCY)
		txtValue = PRIVATE_Comma_Separate_Value(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPotentialTake)
		DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_VALUE_EXPENSES], sPlacement.aStyle.TS_TITLE, txtValue, "STRING")
	ENDIF
	*/
	
	/*
	// Crew take.
	 removed for now
	txtValue = PRIVATE_Comma_Separate_Value(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPersonalTake)
	DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_VALUE_PERSONAL_TAKE], sPlacement.aStyle.TS_TITLE, txtValue, "STRING")
	*/
	// Summary.
/*
	SET_TEXT_WRAP(0.208, 0.388)
	SET_TEXT_LINE_HEIGHT_MULT(1.5)
	DRAW_TEXT(sPlacement.TextPlacement[EST_VALUE_SUMMARY], sPlacement.aStyle.TS_STANDARDSMALLHUD, )//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].tLastSummary
	*/
	
	
	
	// Items stolen.
	//DRAW_TEXT_WITH_2_NUMBERS(sPlacement.TextPlacement[EST_VALUE_STOLEN_ITEMS], sPlacement.aStyle.TS_HUDNUMBER, "ES_STLN", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iItemCount, g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iMaxItems)

	// Complete percentage.
	// TODO: Currently hardcoded.
	/*
	FLOAT fPercentage
	FLOAT fPoint
	GET_PERCENTAGE_SCORE_FOR_HEIST(fPercentage, fPoint, iHeistIndex)
	DRAW_TEXT_WITH_2_NUMBERS(sPlacement.TextPlacement[EST_VALUE_COMPLETE_PERCENTAGE], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ES_PERC", FLOOR(fPercentage), FLOOR(fPoint))
	*/
	// Verdict.
	//DRAW_TEXT(sPlacement.TextPlacement[EST_VALUE_VERDICT], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, GET_HEIST_VERDICT_FROM_PERCENTAGE(fPercentage))
	
	// Mission time.

	//sEndScreen.iHeistTimer = 5568444 // 1 hour 32 minutes and 48 seconds for testing with extra ms to round down
	
	
	/* //Old time display
	INT hours = (sEndScreen.iHeistTimer/(3600000))*3600000
	INT minutes = ((sEndScreen.iHeistTimer - (hours))/(60000))*60000
	INT seconds  = ((sEndScreen.iHeistTimer - (minutes) - (hours))/(1000))*1000
	
	PRINTLN("Heist time total MS: ", sEndScreen.iHeistTimer , ", Hours: ", hours, "  Minutes: ", minutes, " Seconds: ", seconds ) 
	
	// Store the time in the saved global
	g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iTimeTaken = sEndScreen.iHeistTimer
	
	DRAW_TEXT_TIMER(sPlacement.TextPlacement[EST_VALUE_MISSION_TIME], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, hours, TIME_FORMAT_HOURS, "STRING")
	sPlacement.TextPlacement[EST_VALUE_MISSION_TIME].x += 0.014
	DRAW_TEXT(sPlacement.TextPlacement[EST_VALUE_MISSION_TIME], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ES_COLON")
	sPlacement.TextPlacement[EST_VALUE_MISSION_TIME].x += 0.003
	DRAW_TEXT_TIMER(sPlacement.TextPlacement[EST_VALUE_MISSION_TIME], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, minutes, TIME_FORMAT_MINUTES, "STRING")
	sPlacement.TextPlacement[EST_VALUE_MISSION_TIME].x += 0.014
	DRAW_TEXT(sPlacement.TextPlacement[EST_VALUE_MISSION_TIME], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ES_COLON")
	sPlacement.TextPlacement[EST_VALUE_MISSION_TIME].x += 0.003
	DRAW_TEXT_TIMER(sPlacement.TextPlacement[EST_VALUE_MISSION_TIME], sPlacement.aStyle.TS_STANDARDMEDIUMHUD, seconds, TIME_FORMAT_SECONDS, "STRING")
	sPlacement.TextPlacement[EST_VALUE_MISSION_TIME].x -= 0.034
	*/
	// Pie chart.
	// TODO: Currently hardcoded. Should probably be scaleform.
	//DRAW_2D_SPRITE("HeistHUD", "Pie_Chart_Test", sPlacement.SpritePlacement[ESS_PIE_CHART_TEST])
	
	DRAW_SCALEFORM_MOVIE(sEndScreen.pieChart,sPlacement.SpritePlacement[ESS_PIE_CHART_TEST].x,
						sPlacement.SpritePlacement[ESS_PIE_CHART_TEST].y,
						sPlacement.SpritePlacement[ESS_PIE_CHART_TEST].w,
						sPlacement.SpritePlacement[ESS_PIE_CHART_TEST].h,
						255,255,255,255)
	
	
	//FLOAT x, y
	//FLOAT w, h
	//INT r,g,b,a
	
	
ENDPROC








PROC PRIVATE_Draw_Heist_ES_Crew_Details(INT iHeistIndex, MEGA_PLACEMENT_TOOLS &sPlacement)


	INT bargb[15][3]
	INT coli = 0
			//255 255 255
		bargb[0][0] = 255
		bargb[0][1] = 255
		bargb[0][2] = 255
		//165 212 166
		bargb[1][0] = 165
		bargb[1][1] = 212
		bargb[1][2] = 166
		//104 154 105
		bargb[2][0] = 104
		bargb[2][1] = 154
		bargb[2][2] = 105
		// 77 115  78
		bargb[3][0] = 77
		bargb[3][1] = 115
		bargb[3][2] = 78
		// 52  76  52
		bargb[4][0] = 52
		bargb[4][1] = 76
		bargb[4][2] = 52
		// 36  53  35
		bargb[5][0] = 36
		bargb[5][1] = 53
		bargb[5][2] = 35
		// 21  31  22
		bargb[6][0] = 21
		bargb[6][1] = 31
		bargb[6][2] = 22
		//  0   0   0
		bargb[7][0] = 0
		bargb[7][1] = 0
		bargb[7][2] = 0
		//  0   0   0
		bargb[8][0] = 0
		bargb[8][1] = 0
		bargb[8][2] = 0
		//  0   0   0
		bargb[9][0] = 0
		bargb[9][1] = 0
		bargb[9][2] = 0
		
		

	
	
	
	INT iHeistChoice = GET_MISSION_FLOW_INT_VALUE(Get_Heist_Choice_FlowInt_ID(iHeistIndex))
	//INT nth = 1
	FLOAT yat = sPlacement.TextPlacement[EST_CREW_NUMBERING].y
	
	
	// Draw mike's rectangle
	TEXT_LABEL_31 txtValue
	TEXT_LABEL_31 tmptxt
	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0] != -1

		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	= bargb[coli][0]
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].g	= bargb[coli][1]
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].b	= bargb[coli][2]
		++coli
		// Draw player's section. // this is now mike's since he is the main in each heist
		//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPersonalTake
		DRAW_TEXT(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ACCNA_MIKE")
		
		
		//DRAW_TEXT_WITH_NUMBER(sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		
		//sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH",
		//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0])
		txtValue = "$"
		tmptxt = PRIVATE_Comma_Separate_Value(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[0])
		txtValue +=  tmptxt
		SET_TEXT_WRAP(0.0,0.720)
		DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_CREW_MONEY_BASE], 
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)

		
		SET_TEXT_WRAP(0.0,0.794)
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC",  	ROUND(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[0]),FONT_RIGHT)
		
		//DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT",TRUE)
		
		DRAW_2D_SPRITE(				"HeistHUD", "HC_MICHAEL", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
		
		
		
		sPlacement.aStyle.TS_STANDARDSMALL.r = 0
		sPlacement.aStyle.TS_STANDARDSMALL.g = 0
		sPlacement.aStyle.TS_STANDARDSMALL.b = 0
		
		/*
		DRAW_TEXT_WITH_NUMBER(		
		sPlacement.TextPlacement[EST_CREW_NUMBERING],		
		sPlacement.aStyle.TS_STANDARDSMALL, 		
		"NUMBER", 	
		nth)
		*/
		//++nth
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y += 0.075
		
		sPlacement.aStyle.TS_STANDARDSMALL.r = 255
		sPlacement.aStyle.TS_STANDARDSMALL.g = 255
		sPlacement.aStyle.TS_STANDARDSMALL.b = 255
	
	ELSE
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			-= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			-= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			-= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	-= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	-= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		-= 0.075
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y -= 0.075
	ENDIF
		
	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1] != -1
		// Increment x position.
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
		// Decrease red shade.
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	= bargb[coli][0]
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].g	= bargb[coli][1]
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].b	= bargb[coli][2]
	++coli
		
		
		DRAW_TEXT(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ACCNA_FRANKLIN")
		
		//DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1])
		
		txtValue = "$"
		tmptxt = PRIVATE_Comma_Separate_Value(  g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[1] )
		txtValue +=  tmptxt
		SET_TEXT_WRAP(0.0,0.720)
		DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_CREW_MONEY_BASE], 
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)
		
		SET_TEXT_WRAP(0.0,0.794)
		//DRAW_TEXT_WITH_FLOAT(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC", 	,2,FONT_RIGHT)
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC",  	ROUND(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[1]),FONT_RIGHT)
		
		/*
		DRAW_TEXT_WITH_NUMBER(		
		sPlacement.TextPlacement[EST_CREW_NUMBERING],		
		sPlacement.aStyle.TS_STANDARDSMALL, 		
		"NUMBER", 	
		nth)
		
		++nth*/
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y += 0.075
		
		//DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT")
		
		DRAW_2D_SPRITE(				"HeistHUD", "HC_FRANKLIN", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
	ENDIF
	
	
	IF g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2] != -1
		// Increment x position.
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
	
		
		
	
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	= bargb[coli][0]
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].g	= bargb[coli][1]
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].b	= bargb[coli][2]
	++coli
		
		
		DRAW_TEXT(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ACCNA_TREVOR")

		
		/*
		DRAW_TEXT_WITH_NUMBER(		
		sPlacement.TextPlacement[EST_CREW_NUMBERING],		
		sPlacement.aStyle.TS_STANDARDSMALL, 		
		"NUMBER", 	
		nth)
		++nth
		*/
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y += 0.075
		
		
		//DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2])
		txtValue = "$"
		tmptxt = PRIVATE_Comma_Separate_Value(  g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iPlayerTake[2] )
		txtValue +=  tmptxt
		SET_TEXT_WRAP(0.0,0.720)
		DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_CREW_MONEY_BASE], 
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)
		SET_TEXT_WRAP(0.0,0.794)
		//DRAW_TEXT_WITH_FLOAT(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC", 	,2,FONT_RIGHT)
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC",  	ROUND(g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].fPlayerPercentage[2]),FONT_RIGHT)
		//DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT")
		
		DRAW_2D_SPRITE(				"HeistHUD", "HC_TREVOR", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
	ENDIF




	// Draw crew sections.
	INT index
	REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize index
		CrewMember eMember = g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][index]
		
		// Increment x position.
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
		
		sPlacement.SpritePlacement[ESS_KIA_TEXT_BASE].y = sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 
		
	
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	= bargb[coli][0]
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].g	= bargb[coli][1]
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].b	= bargb[coli][2]
	++coli

		HUD_COLOURS hucu = HUD_COLOUR_WHITE
		BOOL bKIA = FALSE
		SWITCH g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].eCrewStatus[index]
			//CASE CMST_FINE
			CASE CMST_INJURED
				hucu = HUD_COLOUR_GREYDARK
					sPlacement.aStyle.TS_STANDARDMEDIUMHUD.r = 128
					sPlacement.aStyle.TS_STANDARDMEDIUMHUD.g = 128
					sPlacement.aStyle.TS_STANDARDMEDIUMHUD.b = 128
					
					sPlacement.aStyle.TS_UI_DASHDOUBLE.r = 128
					sPlacement.aStyle.TS_UI_DASHDOUBLE.g = 128
					sPlacement.aStyle.TS_UI_DASHDOUBLE.b = 128
				BREAK
			CASE CMST_KILLED
				hucu = HUD_COLOUR_GREYDARK
				bKIA = TRUE
					sPlacement.aStyle.TS_STANDARDMEDIUMHUD.r = 128
					sPlacement.aStyle.TS_STANDARDMEDIUMHUD.g = 128
					sPlacement.aStyle.TS_STANDARDMEDIUMHUD.b = 128
					
					
					sPlacement.aStyle.TS_UI_DASHDOUBLE.r = 128
					sPlacement.aStyle.TS_UI_DASHDOUBLE.g = 128
					sPlacement.aStyle.TS_UI_DASHDOUBLE.b = 128
				BREAK
		ENDSWITCH
		
		/*
		DRAW_TEXT_WITH_NUMBER(		
		sPlacement.TextPlacement[EST_CREW_NUMBERING],		
		sPlacement.aStyle.TS_STANDARDSMALL, 		
		"NUMBER", 	
		nth)
		++nth
		*/
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y += 0.075
		
		
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
		
		
		DRAW_TEXT(				sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, PRIVATE_Get_Crew_Member_Name_Label(eMember))
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
		//DRAW_TEXT_WITH_NUMBER(	sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", CEIL(g_sCrewMemberData[eMember].jobCut*0.01*g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iActualTake))
		

		
		IF !bKIA
		
			txtValue = "$"
			tmptxt = PRIVATE_Comma_Separate_Value( g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].iCrewMemberTake[index]  )
			txtValue +=  tmptxt
			SET_TEXT_WRAP(0.0,0.720)
			DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_CREW_MONEY_BASE], 
			sPlacement.aStyle.TS_STANDARDMEDIUMHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)
			
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
			SET_TEXT_WRAP(0.0,0.794)
			//DRAW_TEXT_WITH_FLOAT(	sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC", 	TO_FLOAT(),2,FONT_RIGHT)
			DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC",  g_sCrewMemberStaticData[eMember].jobCut	,FONT_RIGHT)
		ENDIF
		//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
		//DRAW_TEXT(				sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT")
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
		DRAW_2D_SPRITE(			"HeistHUD", PRIVATE_Get_Crew_Member_Name_Label(eMember), sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		DRAW_RECTANGLE(			sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
		
		IF bKIA
			//do KIA text
			DRAW_2D_SPRITE(			"HeistHUD", "kiaoverlay", sPlacement.SpritePlacement[ESS_KIA_TEXT_BASE])
		ENDIF
		
		
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD.r = 255
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD.g = 255
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD.b = 255
		
		sPlacement.aStyle.TS_UI_DASHDOUBLE.r = 255
		sPlacement.aStyle.TS_UI_DASHDOUBLE.g = 255
		sPlacement.aStyle.TS_UI_DASHDOUBLE.b = 255
		
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
	ENDREPEAT
	
	

	/*
	// Draw story character sections.
	REPEAT 2 index
		// Increment x position.
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
		// Decrease red shade.
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	-= 28
		
		SWITCH index
			CASE 0
				DRAW_TEXT_WITH_PLAYER_NAME(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "Franklin", "STRING")
			BREAK
			CASE 1
				DRAW_TEXT_WITH_PLAYER_NAME(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "Lester", "STRING")
			BREAK
		ENDSWITCH
		
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", sEndScreen.iFTDollarTake)
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"NUMBER", 	sEndScreen.iFTCutPercentage)
		DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT")
		DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
		DRAW_2D_SPRITE(				"HeistHUD", "HC_PLAYER", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
	ENDREPEAT
	*/
	

	
	
	IF sEndScreen.fLesterPercentage != -1
		// Increment x position.
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
	

		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	= bargb[coli][0]
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].g	= bargb[coli][1]
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].b	= bargb[coli][2]
		++coli
			
			
		/*
		DRAW_TEXT_WITH_NUMBER(		
		sPlacement.TextPlacement[EST_CREW_NUMBERING],		
		sPlacement.aStyle.TS_STANDARDSMALL, 		
		"NUMBER", 	
		nth)
		++nth
		*/
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y += 0.075
		
		DRAW_TEXT(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "ACCNA_LES")	
		
		//DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", sEndScreen.iLesterTotal)
		
		txtValue = "$"
		tmptxt = PRIVATE_Comma_Separate_Value(  sEndScreen.iLesterTotal )
		txtValue +=  tmptxt
		SET_TEXT_WRAP(0.0,0.720)
		DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_CREW_MONEY_BASE], 
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)
		
		
		
		SET_TEXT_WRAP(0.0,0.794)
		//DRAW_TEXT_WITH_FLOAT(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC", 	,2,FONT_RIGHT)
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"ES_PERC",  	ROUND(sEndScreen.fLesterPercentage),FONT_RIGHT)
		//DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT",TRUE)
		
		DRAW_2D_SPRITE(				"HeistHUD", "HC_LESTER", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
	ENDIF
	
	
	
	
	
	IF iHeistIndex = HEIST_JEWEL 
		
		
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
	

		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	= bargb[coli][0]
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].g	= bargb[coli][1]
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].b	= bargb[coli][2]
		++coli
			
			
		/*
		DRAW_TEXT_WITH_NUMBER(		
		sPlacement.TextPlacement[EST_CREW_NUMBERING],		
		sPlacement.aStyle.TS_STANDARDSMALL, 		
		"NUMBER", 	
		nth)
		++nth
		sPlacement.TextPlacement[EST_CREW_NUMBERING].y += 0.075
		*/
		DRAW_TEXT(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "JCHMDRAZN")	
		
		//DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", 2000000)
		
		txtValue = "$"
		tmptxt = PRIVATE_Comma_Separate_Value(  2000000 )
		txtValue +=  tmptxt
		SET_TEXT_WRAP(0.0,0.794)
		DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_CREW_MONEY_BASE], 
		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)

		//DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"NUMBER", 	sEndScreen.iLesterPercentage)
		//DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT",TRUE)
		
		//DRAW_2D_SPRITE(				"HeistHUD", "HC_LESTER", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		//DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
		
		
	ENDIF
	
	
	
	/*
	IF sEndScreen.iSpecificSpecialCharPercentage != -1
		// Increment x position.
		sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y			+= 0.075
		sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			+= 0.075
		sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	+= 0.075
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	+= 0.075
		sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		+= 0.075
		// Decrease red shade.
		sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].r	-= 20
		
		
		
		DRAW_TEXT_WITH_PLAYER_NAME(	sPlacement.TextPlacement[EST_CREW_NAME_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, sEndScreen.sSpecificSpecialChar, "STRING")
	
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_MONEY_BASE],		sPlacement.aStyle.TS_STANDARDMEDIUMHUD, "HUD_CASH", sEndScreen.iSpecificSpecialCharTotal)
		DRAW_TEXT_WITH_NUMBER(		sPlacement.TextPlacement[EST_CREW_CUT_BASE],		sPlacement.aStyle.TS_HUDNUMBER, 		"NUMBER", 	sEndScreen.iSpecificSpecialCharPercentage)
		DRAW_TEXT(					sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE],	sPlacement.aStyle.TS_UI_DASHDOUBLE, 	"ES_H_PCNT")
		DRAW_RECTANGLE(				sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE])
		DRAW_2D_SPRITE(				"HeistHUD", "HC_PLAYER", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
	ENDIF
	*/
	
	
	// Reset.
	sPlacement.TextPlacement[EST_CREW_NAME_BASE].y 			= 0.232
	sPlacement.TextPlacement[EST_CREW_MONEY_BASE].y 		= 0.260
	sPlacement.TextPlacement[EST_CREW_CUT_BASE].y 			= 0.250
	sPlacement.TextPlacement[EST_CREW_PERCENTAGE_BASE].y 	= 0.226
	sPlacement.RectPlacement[ESR_CREW_COLOUR_KEY_BASE].y	= 0.259
	sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE].y 		= 0.259
	


	sPlacement.TextPlacement[EST_CREW_NUMBERING].y = yat
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ Heist list item control calls ╞═══════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


PROC PRIVATE_RESET_HEIST_TIME_WINDOWS(INT iHeistIndex)
	INT i = 0
	REPEAT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES i
		g_HeistNoneSavedDatas[iHeistIndex].bTimeWindowOpen[i] = FALSE
		g_HeistNoneSavedDatas[iHeistIndex].eTimeWindowSource[i] = UNSET_HEIST_TIME_WINDOW
		g_HeistNoneSavedDatas[iHeistIndex].iTimeWindowMSValue[i] = 0
	ENDREPEAT
	PRINTLN("PRIVATE_RESET_HEIST_TIME_WINDOWS: All heist window time buffers reset for ",iHeistIndex)
ENDPROC



PROC PRIVATE_RESET_HEIST_GEAR_BUFFER(INT iHeistIndex)
	g_HeistNoneSavedDatas[iHeistIndex].iGearLogged = 0
	PRINTLN("PRIVATE_RESET_HEIST_GEAR_BUFFER: All gear buffers reset for ",iHeistIndex)
ENDPROC

/*
PROC PRIVATE_RESET_HEIST_PENALTY_BUFFER(INT iHeistIndex)
	g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged = 0
	PRINTLN("PRIVATE_RESET_HEIST_GEAR_BUFFER: All penalty buffers reset for ",iHeistIndex)
ENDPROC
*/
//heist time window open/close
PROC OPEN_HEIST_TIME_WINDOW(INT iHeistIndex,HEIST_TIME_WINDOW_ENUM section)
	INT i = 0
	REPEAT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES i
		IF g_HeistNoneSavedDatas[iHeistIndex].eTimeWindowSource[i] = section
			g_HeistNoneSavedDatas[iHeistIndex].bTimeWindowOpen[i] = TRUE
			EXIT
		ENDIF
	ENDREPEAT
	//try to add a new entry
	REPEAT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES i
		IF g_HeistNoneSavedDatas[iHeistIndex].eTimeWindowSource[i] = UNSET_HEIST_TIME_WINDOW
			g_HeistNoneSavedDatas[iHeistIndex].eTimeWindowSource[i] = section
			g_HeistNoneSavedDatas[iHeistIndex].bTimeWindowOpen[i] = TRUE
			g_HeistNoneSavedDatas[iHeistIndex].iTimeWindowMSValue[i] = 0
			EXIT
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("OPEN_HEIST_TIME_WINDOW : couldn't open window as it wasn't in the buffers")
	//g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].bTimeWindowOpen[i] = FALSE
ENDPROC



PROC CLOSE_HEIST_TIME_WINDOW(INT iHeistIndex,HEIST_TIME_WINDOW_ENUM section)
	INT i = 0
	REPEAT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES i
		IF g_HeistNoneSavedDatas[iHeistIndex].eTimeWindowSource[i] = section
			g_HeistNoneSavedDatas[iHeistIndex].bTimeWindowOpen[i] = FALSE
			EXIT
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("CLOSE_HEIST_TIME_WINDOW : couldn't close window as it wasn't in the buffers")
ENDPROC



PROC RESET_SPECIFIC_HEIST_TIME_WINDOW(INT iHeistIndex,HEIST_TIME_WINDOW_ENUM section)
	INT i = 0
	REPEAT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES i
		IF g_HeistNoneSavedDatas[iHeistIndex].eTimeWindowSource[i] = section
			g_HeistNoneSavedDatas[iHeistIndex].bTimeWindowOpen[i] = FALSE
			g_HeistNoneSavedDatas[iHeistIndex].iTimeWindowMSValue[i] = 0
			EXIT
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("RESET_SPECIFIC_HEIST_TIME_WINDOW : couldn't reset window as it wasn't in the buffers")
ENDPROC


PROC ADD_USED_GEAR_TO_HEIST(INT iHeistIndex,HEIST_ENDSCREEN_GEAR_ENUM item)
	INT i = 0

	REPEAT g_HeistNoneSavedDatas[iHeistIndex].iGearLogged i
		IF g_HeistNoneSavedDatas[iHeistIndex].GearUsed[i] = item
			//if its already been used then bail
			EXIT
		ENDIF
	ENDREPEAT
	
	IF g_HeistNoneSavedDatas[iHeistIndex].iGearLogged = MAX_HEIST_GEAR_ENUM_ENTRIES
		SCRIPT_ASSERT("ADD_USED_GEAR_TO_HEIST : could not add, no space left to log")
		EXIT	
	ENDIF
	
	
	g_HeistNoneSavedDatas[iHeistIndex].GearUsed[g_HeistNoneSavedDatas[iHeistIndex].iGearLogged] = item
	++g_HeistNoneSavedDatas[iHeistIndex].iGearLogged
	
	
	
ENDPROC
		


/// PURPOSE:
///    Returns the string name of a specific bit of heist gear
/// PARAMS:
///    gear - 
/// RETURNS:
///    
FUNC STRING PRIVATE_GET_HEIST_GEAR_STRING(HEIST_ENDSCREEN_GEAR_ENUM gear)
	SWITCH gear
		CASE HEIST_GEAR_GAS_GRENADE
			RETURN "MISBZGASGREN"
		CASE HEIST_GEAR_REMOTE_RIFLE
			RETURN "MISREMRIFLE"
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_GET_HEIST_GEAR_STRING: no matching string for enum" )
	RETURN "UNSET_HEISTGEAR"
ENDFUNC

/// PURPOSE:
///    Returns a heist time window results string with 2 entries for minutes and seconds
/// PARAMS:
///    window - 
/// RETURNS:
///    
FUNC STRING PRIVATE_GET_HEIST_WINDOW_STRING(HEIST_TIME_WINDOW_ENUM window)
	SWITCH window
		CASE HEIST_TIME_WINDOW_JC_FRANKLIN_TO_ROOF
			RETURN "MISTAFRARO"
		CASE HEIST_TIME_WINDOW_JC_DRIVE_TO_JOB_TIME
			RETURN "MISTADRAJO"
		CASE HEIST_TIME_WINDOW_JC_LOOTING_TIME
			RETURN "MISTALOOTIM"
		CASE HEIST_TIME_WINDOW_JC_MOTORBIKE_CHASE_TIME
			RETURN "MISTAMOTCHA"		
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_GET_HEIST_WINDOW_STRING: no matching string for enum" )
	RETURN "UNSET_HEISTTIMEWINDOW"
ENDFUNC

FUNC STRING PRIVATE_GET_HEIST_PENALTY_STRING(HEIST_ENDSCREEN_PENALTY_ENUM penalty)
	SWITCH penalty
		CASE HEIST_PENALTY_JCVAN
			RETURN "JCSLMVANR"	
		CASE HEIST_PENALTY_JCHOUSE
			RETURN "JCHMDRAZ"
		CASE HEIST_PENALTY_AGENTCUT
			RETURN "HSTAGNCUT"
		CASE HEIST_PENALTY_UNSELLABLE_WEAPON
			RETURN "UNSELLWEAP"
		CASE HEIST_PENALTY_MONEY_DROPPED
			RETURN "HSTECUT"
		CASE HEIST_PENALTY_BIGS_SLOW_LOADING
			RETURN "HSTSLWLD"
		CASE HEIST_PENALTY_HOSTAGE_GIFT
			RETURN "HSTCASECUT"
	ENDSWITCH
	SCRIPT_ASSERT("PRIVATE_GET_HEIST_PENALTY_STRING: no matching string for enum" )
	RETURN "UNSET_HEISTPENALTY"
ENDFUNC


FUNC STRING GET_HEIST_CHOICE_END_SCREEN_STRING(INT choiceID)
	SWITCH choiceID
		CASE HEIST_CHOICE_JEWEL_STEALTH	
			RETURN "MISCHO1"
		CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT	
			RETURN "MISCHO2"
		CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
			RETURN "MISCHO3"
		CASE HEIST_CHOICE_DOCKS_DEEP_SEA
			RETURN "MISCHO4"
		CASE HEIST_CHOICE_AGENCY_FIRETRUCK
			RETURN "MISCHO5"
		CASE HEIST_CHOICE_AGENCY_HELICOPTER
			RETURN "MISCHO6"
		CASE HEIST_CHOICE_FINALE_TRAFFCONT
			RETURN "MISCHO7"
		CASE HEIST_CHOICE_FINALE_HELI
			RETURN "MISCHO8"
	ENDSWITCH
		SCRIPT_ASSERT("GET_HEIST_CHOICE_END_SCREEN_STRING: no matching string for constant" )
	RETURN "UNSET_HEIST_CHOICE_STRING"
ENDFUNC


PROC PRIVATE_UPDATE_HEIST_TIMER_DELTAS(INT MSstep)
	INT i = 0
	INT j = 0
	REPEAT NO_HEISTS j
		REPEAT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES i
			IF NOT (g_HeistNoneSavedDatas[j].eTimeWindowSource[i] = UNSET_HEIST_TIME_WINDOW)
				IF g_HeistNoneSavedDatas[j].bTimeWindowOpen[i]
					g_HeistNoneSavedDatas[j].iTimeWindowMSValue[i] += MSstep
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC



PROC PRIVATE_ALTERNATE_GREYLINE(BOOL &alternator,float lineskip,MEGA_PLACEMENT_TOOLS &sPlacement)

	IF alternator = TRUE
		DRAW_RECTANGLE(sPlacement.RectPlacement[ESR_TAKE_GREYLINE])
	ENDIF
	sPlacement.RectPlacement[ESR_TAKE_GREYLINE].y += lineskip
	alternator = NOT alternator

ENDPROC

/*
PROC PRIVATE_WRITE_STAT_REGISTER_TO_LEADERBOARD(INT i , MEGA_PLACEMENT_TOOLS &sPlacement,FLOAT lineskip, BOOL &alternator)
	
	ENUM_MISSION_STATS t = g_MissionStatTrackingArray[i].target
	
	IF  t = UNSET_MISSION_STAT_ENUM
		EXIT
	ENDIF
	
	IF g_MissionStatTrackingPrototypes[t].bHidden
		EXIT
	ENDIF
	
	
	
	
	//HUD_COLOURS hucu = HUD_COLOUR_RED
	sPlacement.aStyle.TS_STANDARDSMALLHUD.r = 150
	sPlacement.aStyle.TS_STANDARDSMALLHUD.g = 150
	sPlacement.aStyle.TS_STANDARDSMALLHUD.b = 150
	
	BOOL bSuccess = FALSE
	
	IF g_MissionStatTrackingPrototypes[t].less_than_threshold
		IF g_MissionStatTrackingArray[i].ivalue < g_MissionStatTrackingPrototypes[t].success_threshold
			//hucu = HUD_COLOUR_WHITE
		sPlacement.aStyle.TS_STANDARDSMALLHUD.r = 255
		sPlacement.aStyle.TS_STANDARDSMALLHUD.g = 255
		sPlacement.aStyle.TS_STANDARDSMALLHUD.b = 255
		bSuccess = TRUE
		ENDIF
	ELSE
		IF g_MissionStatTrackingArray[i].ivalue >= g_MissionStatTrackingPrototypes[t].success_threshold
			//hucu = HUD_COLOUR_WHITE
		sPlacement.aStyle.TS_STANDARDSMALLHUD.r = 255
		sPlacement.aStyle.TS_STANDARDSMALLHUD.g = 255
		sPlacement.aStyle.TS_STANDARDSMALLHUD.b = 255
		bSuccess = TRUE
		ENDIF
	ENDIF
	
	
	//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)	
	
	SET_TEXT_WRAP(0.0,0.400)
	
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD, GET_MISSION_STAT_NAME(t))
	
	
	
	
	
	INT tval = g_MissionStatTrackingPrototypes[t].success_threshold
	INT sval = g_MissionStatTrackingArray[i].ivalue
	
	
	SET_TEXT_WRAP(0.0,0.400)
	
	SWITCH g_MissionStatTrackingPrototypes[t].type
		
		CASE MISSION_STAT_TYPE_UNIQUE_BOOL
			
				IF bSuccess
					DRAW_TEXT_WITH_ALIGNMENT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
											 sPlacement.aStyle.TS_STANDARDSMALLHUD,
											 "CHECKMARK",FALSE,TRUE)
				ELSE
					DRAW_TEXT_WITH_ALIGNMENT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
											 sPlacement.aStyle.TS_STANDARDSMALLHUD,
											 "CROSSMARK",FALSE,TRUE)
				ENDIF
		
			BREAK
		
		CASE MISSION_STAT_TYPE_TOTALTIME
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE
		CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
			DRAW_TEXT_TIMER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
							sPlacement.aStyle.TS_STANDARDSMALLHUD,
							tval,
							 TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES,
							 "STRING",
							FALSE,TRUE)
			BREAK
		
		CASE MISSION_STAT_TYPE_FRACTION //x/y
		CASE MISSION_STAT_TYPE_HEADSHOTS//ES_STLN
			DRAW_TEXT_WITH_2_NUMBERS(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
									 sPlacement.aStyle.TS_STANDARDSMALLHUD,
									 "FRCTIONSPL",sval,tval,FONT_RIGHT)
		
			BREAK
		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE // target is overridden to 100 for display
			tval = 100
			FALLTHRU			
		CASE MISSION_STAT_TYPE_ACCURACY //percentage
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD

			DRAW_TEXT_WITH_NUMBER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
									 sPlacement.aStyle.TS_STANDARDSMALLHUD,
									 "ES_PERC", CEIL(TO_FLOAT(sval)/TO_FLOAT(tval)),FONT_RIGHT)

			BREAK

		
		DEFAULT
			
			DRAW_TEXT_WITH_2_NUMBERS(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
									 sPlacement.aStyle.TS_STANDARDSMALLHUD,
									 "ES_STLN",sval,tval,FONT_RIGHT)
			BREAK
		
	ENDSWITCH
	
	
	PRIVATE_ALTERNATE_GREYLINE(alternator,lineskip,sPlacement)
	
	
	
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
	
	sPlacement.aStyle.TS_STANDARDSMALLHUD.r = 255
	sPlacement.aStyle.TS_STANDARDSMALLHUD.g = 255
	sPlacement.aStyle.TS_STANDARDSMALLHUD.b = 255
ENDPROC
*/

/*
PROC PRIVATE_HEIST_ENDSCREEN_STAT_REGISTER_WRITE(MEGA_PLACEMENT_TOOLS &sPlacement,FLOAT lineskip,BOOL &alternator)

	FLOAT tabskip = 0.02
	FLOAT otab = sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].x
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].x += tabskip
		
	//PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		//g_MissionStatTrackingArray[i].
		PRIVATE_WRITE_STAT_REGISTER_TO_LEADERBOARD(i , sPlacement,lineskip, alternator)
	ENDREPEAT

	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].x = otab

ENDPROC
*/

/*
PROC PRIVATE_Draw_Heist_ES_Breakdown(INT iHeistIndex, MEGA_PLACEMENT_TOOLS &sPlacement)
	FLOAT lineskip = 0.0277
	FLOAT tabskip = 0.02

	FLOAT ygry = sPlacement.RectPlacement[ESR_TAKE_GREYLINE].y
	BOOL greyLineAlternator = FALSE

	//heistArt_JS
	//do heist image
	SPRITE_PLACEMENT img
	img.x = 0.30
	img.y = 0.35
	img.w = 0.189 
	img.h = 0.236
	img.r = 255
	img.g = 255
	img.b = 255
	img.a = 255	

	SWITCH iHeistIndex
		CASE HEIST_JEWEL
			DRAW_2D_SPRITE("HeistHUD", "heistArt_JS", img, FALSE,HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_BEFORE_HUD)
		BREAK
		CASE HEIST_DOCKS
			DRAW_2D_SPRITE("HeistHUD", "heistArt_DO", img, FALSE,HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_BEFORE_HUD)
		BREAK
		CASE HEIST_RURAL_BANK
			DRAW_2D_SPRITE("HeistHUD", "heistArt_RB", img, FALSE,HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_BEFORE_HUD)
		BREAK
		CASE HEIST_AGENCY
			DRAW_2D_SPRITE("HeistHUD", "heistArt_AG", img, FALSE,HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_BEFORE_HUD)
		BREAK
		CASE HEIST_FINALE
			DRAW_2D_SPRITE("HeistHUD", "heistArt_FI", img, FALSE,HIGHLIGHT_OPTION_NORMAL,GFX_ORDER_BEFORE_HUD)
		BREAK
	ENDSWITCH 
	
	
		
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y = 0.48
	sPlacement.RectPlacement[ESR_TAKE_GREYLINE].y = 0.49
		
	IF iHeistIndex != HEIST_RURAL_BANK	 //rural bank has no choice line
	
		INT choice = -1
		SWITCH iHeistIndex
			CASE HEIST_JEWEL
				choice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_JEWEL)
			BREAK
			CASE HEIST_DOCKS
				choice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_DOCKS)
			BREAK
			CASE HEIST_AGENCY
				choice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY)
			BREAK
			CASE HEIST_FINALE
				choice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE)
			BREAK
		ENDSWITCH 
		
		PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

		SET_TEXT_WRAP(0.0,0.400)
		DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD, 
			 GET_HEIST_CHOICE_END_SCREEN_STRING(choice))
		sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
	ENDIF
	
	
	
	
	//TODO, do stat write here
	
	PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)
	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD, "ES_STATS")
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
	
	
	
	///////////////////////////////
	PRIVATE_HEIST_ENDSCREEN_STAT_REGISTER_WRITE(sPlacement,lineskip,greyLineAlternator)
	//////////////////////////////////////
	
	//TODO remove penaties that are zero
	
	
	
	//do penalties

	INT i = 0
	IF g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged > 0
		
		PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

		//do the gear heading
		SET_TEXT_WRAP(0.0,0.400)
		DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], 
		sPlacement.aStyle.TS_STANDARDSMALLHUD,
		"PENPLIESHA")
		
		sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip-0.005
		
		FLOAT otab = sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].x
		sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].x += tabskip
		REPEAT g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged i
			IF g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[i] > 0
				PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

				SET_TEXT_WRAP(0.0,0.400)
				DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], 
				sPlacement.aStyle.TS_STANDARDSMALLHUD,
				PRIVATE_GET_HEIST_PENALTY_STRING(g_HeistNoneSavedDatas[iHeistIndex].Penalties[i]))
				//g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[iHeistIndex])
				
				SET_TEXT_WRAP(0.0,0.400)
				//DRAW_TEXT_WITH_NUMBER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], 
				//sPlacement.aStyle.TS_STANDARDSMALLHUD,
				//"HSTENSCRP",
				//g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[iHeistIndex],
				//FONT_RIGHT)
				TEXT_LABEL_31 txtValue = "-$"
				TEXT_LABEL_31 tmptxt = PRIVATE_Comma_Separate_Value( g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[i]  )
				txtValue +=  tmptxt
				SET_TEXT_WRAP(0.0,0.400)
				DRAW_TEXT_WITH_PLAYER_NAME(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], 
				sPlacement.aStyle.TS_STANDARDSMALLHUD, txtValue, "STRING",HUD_COLOUR_WHITE,FONT_RIGHT)
				sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
			ENDIF
		ENDREPEAT
		sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].x = otab
		
	ELSE
	
		//do no penalties entry
		
	ENDIF
	
	

	//are there any casualties
	INT injured = 0
	INT deaths = 0

	i = 0
	REPEAT MAX_CREW_SIZE i
		IF NOT (g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].eCrewStatus[i] = CMST_NOT_SET)
			IF  g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].eCrewStatus[i] = CMST_INJURED
				//SET_TEXT_WRAP(0.0,0.400)
				//DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD, "CREWINJUREDTEST")
				//sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
				++injured
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	//are there any deaths
	i = 0
	REPEAT MAX_CREW_SIZE i
		IF NOT (g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].eCrewStatus[i] = CMST_NOT_SET)
			IF  g_savedGlobals.sHeistData.sEndScreenData[iHeistIndex].eCrewStatus[i] = CMST_KILLED
				//SET_TEXT_WRAP(0.0,0.400)
				//DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD, "CREWDEADTEST")
				//sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
				
				
				
				
				++deaths
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	//Shift this section down the screen a bit
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y = 0.74
	sPlacement.RectPlacement[ESR_TAKE_GREYLINE].y = 0.75

			
	PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
					sPlacement.aStyle.TS_STANDARDSMALLHUD,
					"OVTIMEVAL")
	
	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT_TIMER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
							sPlacement.aStyle.TS_STANDARDSMALLHUD,
							g_LeaderboardLastMissionBestTime,
							 TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES ,
							 "STRING",
							FALSE,TRUE)	
	
	
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip



		
	PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD,
	"OVCASSEC")
	//injured)
	
	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT_WITH_NUMBER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD,
	"NUMBER",
	injured,FONT_RIGHT)
	
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
	
		
		
		
		
	PRIVATE_ALTERNATE_GREYLINE(greyLineAlternator,lineskip,sPlacement)

	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD,
	"OVDECSEC")
	
	
	SET_TEXT_WRAP(0.0,0.400)
	DRAW_TEXT_WITH_NUMBER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN], sPlacement.aStyle.TS_STANDARDSMALLHUD,
	"NUMBER",
	deaths,FONT_RIGHT)
	
	
	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y+=lineskip
	
	


	
	sPlacement.RectPlacement[ESR_TAKE_GREYLINE].y = ygry
	
	
ENDPROC
*/






////////////////////////////////////////////////////////////////////////////
////////////////////////New and slightly less shit end screen///////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

PROC PRIVATE_SET_DEFAULT_HEIST_END_SCREEN_POSITIONS(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)

	rawPositions.bSet = TRUE
	
	FLOAT scrw = 1280
	FLOAT scrh = 720
	
	rawPositions.fAnchorCrewNameX = 258/scrw
	rawPositions.fAnchorCrewNameY = 70/scrh
	
	rawPositions.fSeperationX = 2.0/scrw
	rawPositions.fSeperationY = 2.0/scrh
	
	
	rawPositions.fHeadersX = 258/scrw
	rawPositions.fHeadersY = 119/scrh
	
	rawPositions.fHeadersW = 253/scrw
	rawPositions.fHeadersH = 31/scrh
	
	rawPositions.fHeadersToContentY = 9/scrh
	
	rawPositions.fContentCrewAdvanceY = (430.0/HEIST_SCREEN_MAX_CREW)/scrh
	rawPositions.fContentStatAdvanceY = (430.0/4)/scrh
	
	rawPositions.fContentOverviewY = 108.0/scrh
	rawPositions.fContentOverviewLineY = ((430.0-108.0)/12)/scrh

	//subsection values
	rawPositions.fTopLinerY = 5.0/scrh
	
	rawPositions.fTopCrewListColourBarX = 9/scrw

	rawPositions.fCrewStatTitleY = 27/scrh
	
	rawPositions.fCrewStatBarW = 113/scrw
	rawPositions.fCrewStatBarH = 7/scrh
	
	
	rawPositions.fCrewStatScrollBarH = 25/scrh
	
	
	
	//INT bargb[HEIST_SCREEN_MAX_CREW][3]
	rawPositions.bargb[0][0] = 214
	rawPositions.bargb[0][1] = 235
	rawPositions.bargb[0][2] = 222
	rawPositions.bargb[1][0] = 173
	rawPositions.bargb[1][1] = 211
	rawPositions.bargb[1][2] = 173
	rawPositions.bargb[2][0] = 101
	rawPositions.bargb[2][1] = 155
	rawPositions.bargb[2][2] = 99
	rawPositions.bargb[3][0] = 82
	rawPositions.bargb[3][1] = 109
	rawPositions.bargb[3][2] = 82
	rawPositions.bargb[4][0] = 52
	rawPositions.bargb[4][1] = 76
	rawPositions.bargb[4][2] = 52
	rawPositions.bargb[5][0] = 27
	rawPositions.bargb[5][1] = 38
	rawPositions.bargb[5][2] = 30
	rawPositions.bargb[6][0] = 21
	rawPositions.bargb[6][1] = 31
	rawPositions.bargb[6][2] = 22
	rawPositions.bargb[7][0] = 0
	rawPositions.bargb[7][1] = 0
	rawPositions.bargb[7][2] = 0
	rawPositions.bargb[8][0] = 0
	rawPositions.bargb[8][1] = 0
	rawPositions.bargb[8][2] = 0
	
	
	//top bar
	//104/151/107
	//titlebars
	//49/48/49
	//alternator colours
	//light 74/73/74
	//dark 66/60/66
	rawPositions.topcol[0] = 104
	rawPositions.topcol[1] = 151
	rawPositions.topcol[2] = 107
	
	rawPositions.titlecol[0] = 49
	rawPositions.titlecol[1] = 48
	rawPositions.titlecol[2] = 49
	

	
	rawPositions.lightAlpha = 102
	rawPositions.darkAlpha = 128
	
	//0.03888
	rawPositions.fontSizeHeading = 18*0.03888
	rawPositions.fontSizeLarge = 16*0.03888
	rawPositions.fontSizeMedium = 13*0.03888
	rawPositions.fontSizeTiny = 9*0.03888
ENDPROC

PROC PRIVATE_HEIST_END_DRAW_ORDER()
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
ENDPROC 

PROC PRIVATE_DRAW_HEIST_END_SCREEN_HEADERS(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)

	FLOAT xto = rawPositions.fHeadersX
	FLOAT yto = rawPositions.fHeadersY
	
	//rawPositions.rawPositionsfSeperationX
	
	INT i = 0
	REPEAT 3 i
		//white bar
		PRIVATE_HEIST_END_DRAW_ORDER()
		DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
				  yto+(rawPositions.fHeadersH*0.5),
				  rawPositions.fHeadersW,rawPositions.fHeadersH,
				  255,255,255,255)
		
		
		//green bar
		PRIVATE_HEIST_END_DRAW_ORDER()
		DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
				  yto+(rawPositions.fTopLinerY*0.5),
				  rawPositions.fHeadersW,rawPositions.fTopLinerY,
				  rawPositions.topcol[0],rawPositions.topcol[1],rawPositions.topcol[2],
				  255)
		
		xto += rawPositions.fHeadersW + rawPositions.fSeperationX
		
	ENDREPEAT
ENDPROC


PROC PRIVATE_DRAW_HEIST_END_CHARACTER_KEY_HEADERS(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions,HEIST_END_SCREEN_RAW_DATA &rawData )

	
	FLOAT xto = rawPositions.fHeadersX
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY
	
	INT iChars = rawData.iCrewTotal
	FLOAT iconW = rawPositions.fContentCrewAdvanceY*0.5625
	BOOL bDark = TRUE
	INT i = 0
	REPEAT iChars i
		
		//background
		IF bDark
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
					  yto+(rawPositions.fContentCrewAdvanceY*0.5),
					  rawPositions.fHeadersW,
					  rawPositions.fContentCrewAdvanceY,
					  0,
					  0,
					  0,
					  rawPositions.darkAlpha)
					  bDark = FALSE
		ELSE
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
					  yto+(rawPositions.fContentCrewAdvanceY*0.5),
					  rawPositions.fHeadersW,
					  rawPositions.fContentCrewAdvanceY,
					  0,0,0,
					  rawPositions.lightAlpha)
					  bDark = TRUE
		ENDIF
		
		
		
		//colour key
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fTopCrewListColourBarX*0.5),
					  yto+(rawPositions.fContentCrewAdvanceY*0.5),
					  rawPositions.fTopCrewListColourBarX,
					  rawPositions.fContentCrewAdvanceY,
					  rawPositions.bargb[i][0],
					  rawPositions.bargb[i][1],
					  rawPositions.bargb[i][2],
					  255)
		
		
		
		//DRAW_2D_SPRITE(				"HeistHUD", "HC_MICHAEL", sPlacement.SpritePlacement[ESS_CREW_PHOTO_BASE])
		DRAW_SPRITE("HeistHUD", rawData.iCrewImages[i], 
		xto+(rawPositions.fTopCrewListColourBarX)+(iconW*0.5),
		yto+(rawPositions.fContentCrewAdvanceY*0.5), 
		iconW, rawPositions.fContentCrewAdvanceY, 
		0, 255,255,255,255)
		
		//TEXT_LABEL iCrewNames[HEIST_SCREEN_MAX_CREW]
		//TEXT_LABEL iCrewImages[HEIST_SCREEN_MAX_CREW]
		
		yto += rawPositions.fContentCrewAdvanceY
	
	ENDREPEAT
	


ENDPROC


PROC PRIVATE_DRAW_HEIST_END_CREW_STAT_ENTRIES(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions,HEIST_END_SCREEN_RAW_DATA &rawData )

/*
FUNC STRING GET_GUNMAN_STAT_NAME_LABEL(CrewGunmanStat stat)
FUNC STRING GET_HACKER_STAT_NAME_LABEL(CrewHackerStat stat)
FUNC STRING GET_DRIVER_STAT_NAME_LABEL(CrewDriverStat stat)

*/
	//rawPositions.
	
	INT iStatEntries = rawData.iCrewStatTotal
	
	FLOAT xto = rawPositions.fHeadersX + rawPositions.fHeadersW + rawPositions.fSeperationX
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY
	
	FLOAT contentAdvance = rawPositions.fContentStatAdvanceY - rawPositions.fCrewStatTitleY
	
	INT i = 0
	INT iDone = 0
	
	REPEAT iStatEntries i
		IF iDone != 4
	

			IF NOT rawData.bKIA[rawData.iCrewStatIndices[i]]
			//heading
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
					  yto+(rawPositions.fCrewStatTitleY*0.5),
					  rawPositions.fHeadersW,
					  rawPositions.fCrewStatTitleY,
					  0,0,0,
					  rawPositions.darkAlpha)
		
			yto += rawPositions.fCrewStatTitleY
		
			//background
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
					  yto+(contentAdvance*0.5),
					  rawPositions.fHeadersW,
					  contentAdvance,
					  0,0,0,
					  rawPositions.lightAlpha)
		
		
				yto += contentAdvance
		
			ENDIF
		++iDone
		ENDIF
		
		
	ENDREPEAT
	
	
	
	IF iDone > 4 
		yto += rawPositions.fSeperationY
		//render scroll bar
		PRIVATE_HEIST_END_DRAW_ORDER()
		DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
				  yto+(rawPositions.fCrewStatScrollBarH*0.5),
				  rawPositions.fHeadersW,
				  rawPositions.fCrewStatScrollBarH,
				  0,0,0,
				  rawPositions.darkAlpha)
		
		
		
	ENDIF

ENDPROC



PROC PRIVATE_DRAW_HEIST_END_OVERVIEW_SECTION(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)

	
	
	FLOAT xto = rawPositions.fHeadersX + rawPositions.fHeadersW*2 + rawPositions.fSeperationX*2
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY
	
	
	FLOAT piHalfW =(rawPositions.fContentOverviewY*0.5625)*0.5
	PRIVATE_HEIST_END_DRAW_ORDER()
	DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
			  yto+(rawPositions.fContentOverviewY*0.5),
			  rawPositions.fHeadersW,
			  rawPositions.fContentOverviewY,
			  0,0,0,
			  rawPositions.darkAlpha)
	
	
	PRIVATE_HEIST_END_DRAW_ORDER()
	DRAW_SCALEFORM_MOVIE(sEndScreen.pieChart,
						xto+(rawPositions.fHeadersW)- piHalfW,
						yto+(rawPositions.fContentOverviewY*0.5),
						piHalfW*1.8,
						rawPositions.fContentOverviewY*0.9,
						255,255,255,255)
	
	
	yto+=rawPositions.fContentOverviewY
	

	
	

	//FLOAT fContentOverviewY
	//FLOAT fContentOverviewLineY
	INT iLines = 12
	BOOL bDark = FALSE
	INT i=0
	REPEAT iLines i
		//background
		IF bDark
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
					  yto+(rawPositions.fContentOverviewLineY*0.5),
					  rawPositions.fHeadersW,
					  rawPositions.fContentOverviewLineY,
					  0,0,0,
					  rawPositions.darkAlpha)
					  bDark = FALSE
		ELSE
			PRIVATE_HEIST_END_DRAW_ORDER()
			DRAW_RECT(xto+(rawPositions.fHeadersW*0.5),
					  yto+(rawPositions.fContentOverviewLineY*0.5),
					  rawPositions.fHeadersW,
					  rawPositions.fContentOverviewLineY,
					  0,0,0,
					  rawPositions.lightAlpha)
					  bDark = TRUE
		ENDIF
		
		
		
		yto+=rawPositions.fContentOverviewLineY
	ENDREPEAT

ENDPROC


PROC RENDER_PLACEHOLDER_TEXT( FLOAT x, FLOAT y,BOOL c = FALSE)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()
	TEXT_LABEL test = "PLACEHOLDER"
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
	ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(test)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC

PROC PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(STRING tl, FLOAT x, FLOAT y,BOOL c = FALSE)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()

	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl)
	//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
	//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC

PROC PRIVATE_RENDER_HEIST_END_SCREEN_PERCENTAGE(INT percentage, FLOAT x, FLOAT y,BOOL c = FALSE)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ES_PERC")
	ADD_TEXT_COMPONENT_INTEGER(percentage)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC 

PROC PRIVATE_RENDER_HEIST_END_SCREEN_DOLLARS(INT dollars, FLOAT x, FLOAT y,BOOL c = FALSE,BOOL minus = FALSE)

	TEXT_LABEL_31 txtValue = "$"
	IF minus
		txtValue = "-$"
	ENDIF
	TEXT_LABEL_31 tmptxt = PRIVATE_Comma_Separate_Value(dollars)
	txtValue +=  tmptxt
	
	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
	ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(txtValue)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)

ENDPROC 


PROC PRIVATE_RENDER_HEIST_END_SCREEN_TIMER(INT msval, FLOAT x, FLOAT y,BOOL c = FALSE)
	
	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
	ADD_TEXT_COMPONENT_SUBSTRING_TIME(msval, TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC


PROC PRIVATE_RENDER_HEIST_END_SCREEN_FRACTION(INT l, INT r, FLOAT x, FLOAT y,BOOL c = FALSE)
	//"FRCTIONSPL"
	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FRCTIONSPL")
	ADD_TEXT_COMPONENT_INTEGER(l)
	ADD_TEXT_COMPONENT_INTEGER(r)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
	
ENDPROC

PROC PRIVATE_RENDER_HEIST_END_SCREEN_TEXTWITH_NUMBER(STRING label,INT number, FLOAT x, FLOAT y,BOOL c = FALSE)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_HEIST_END_DRAW_ORDER()

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(label)
	ADD_TEXT_COMPONENT_INTEGER(number)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
	
ENDPROC

PROC PRIVATE_DRAW_HEIST_END_SCREEN_HEADER_TEXTS(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)


	
	
	//SET_TEXT_WRAP(0.0,0.600)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeHeading)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_N_HEADER",rawPositions.fAnchorCrewNameX,rawPositions.fAnchorCrewNameY)

	FLOAT xto = rawPositions.fHeadersX + rawPositions.fHeadersW *0.5
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH*0.25
	
	//rawPositions.rawPositionsfSeperationX
	
	INT i = 0
	REPEAT 3 i
		
		SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
		SET_TEXT_COLOUR(0, 0, 0, 255)
		
		SWITCH i
			CASE 0
				PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_H_INFO",xto,yto,TRUE)
				BREAK
			CASE 1
				PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_H_BRKD",xto,yto,TRUE)
				BREAK
			CASE 2
				PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_H_TAKE",xto,yto,TRUE)
				BREAK
		
		ENDSWITCH
		
		xto += rawPositions.fHeadersW + rawPositions.fSeperationX
		
	ENDREPEAT
ENDPROC


PROC PRIVATE_DRAW_HEIST_END_SCREEN_CREW_TEXTS(HEIST_END_SCREEN_RAW_DATA &rawData,
											  HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)
	//FLOAT xto = rawPositions.fHeadersX
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY
	INT iChars = rawData.iCrewTotal
	FLOAT portraitW = (rawPositions.fContentCrewAdvanceY*0.5625)
	FLOAT wrapLeft = rawPositions.fHeadersX + portraitW + rawPositions.fTopCrewListColourBarX
	FLOAT wrapRight = rawPositions.fHeadersX + rawPositions.fHeadersW
	
	wrapLeft+= CUSTOM_MENU_TEXT_INDENT_X
	wrapRight -= CUSTOM_MENU_TEXT_INDENT_X
	
	INT i = 0
	REPEAT iChars i

		IF rawData.bKIA[i]
			FLOAT kiww = (rawPositions.fContentCrewAdvanceY*0.5625)*1.50 - CUSTOM_MENU_TEXT_INDENT_X
			DRAW_SPRITE("HeistHUD", "kiaoverlay", 
			rawPositions.fHeadersX + rawPositions.fHeadersW - kiww*0.5, 
			yto+rawPositions.fContentCrewAdvanceY*0.5, 
			kiww, rawPositions.fContentCrewAdvanceY, 
			0, 255,255,255,155)
			
			SET_TEXT_COLOUR(155,155,155,255)
		ENDIF

		yto += rawPositions.fContentCrewAdvanceY *0.1
		
		//name
		SET_TEXT_WRAP(wrapLeft, wrapRight)
		SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(rawData.iCrewNames[i], wrapLeft,yto)

		IF rawData.bKIA[i]
			
			SET_TEXT_COLOUR(255,255,255,255)
			yto += rawPositions.fContentCrewAdvanceY *0.9
				
		ELSE		
		
			yto += rawPositions.fContentCrewAdvanceY *0.1
			//percentage 
			SET_TEXT_WRAP(wrapLeft, wrapRight)
			SET_TEXT_SCALE(1.0, rawPositions.fontSizeLarge)
			SET_TEXT_JUSTIFICATION(FONT_RIGHT)
			SET_TEXT_RIGHT_JUSTIFY(TRUE)
			
			PRIVATE_RENDER_HEIST_END_SCREEN_PERCENTAGE(ROUND(rawData.fCrewPercentages[i]),wrapRight,yto)
			
				
			yto += rawPositions.fContentCrewAdvanceY *0.3
			
			//money
			SET_TEXT_WRAP(wrapLeft, wrapRight)
			SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
			SET_TEXT_JUSTIFICATION(FONT_LEFT)
			//RENDER_PLACEHOLDER_TEXT(wrapLeft,yto)
			
			PRIVATE_RENDER_HEIST_END_SCREEN_DOLLARS(rawData.iCrewDollars[i],wrapLeft,yto)
			
			
		
			yto += rawPositions.fContentCrewAdvanceY *0.5
		
		
		ENDIF
		
		
		
	ENDREPEAT
	

	


	UNUSED_PARAMETER(rawData)

ENDPROC


PROC PRIVATE_DRAW_HEIST_END_SCREEN_CREW_STAT_TEXTS(HEIST_END_SCREEN_RAW_DATA &rawData,
													HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions,
													INT &focus)

	//rawPositions.
	INT iStatEntries = rawData.iCrewStatTotal - rawData.iCrewDead // do check for this from data
	
	//FLOAT xto = rawPositions.fHeadersX + rawPositions.fHeadersW + rawPositions.fSeperationX
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY
	FLOAT portraitW = rawPositions.fCrewStatTitleY*0.5625
	
	
	FLOAT wrapLeft = (rawPositions.fHeadersX*2.0) + rawPositions.fSeperationX 
	FLOAT wrapRight = wrapLeft + rawPositions.fHeadersW
	wrapLeft += portraitW
	wrapLeft+= CUSTOM_MENU_TEXT_INDENT_X
	wrapRight -= CUSTOM_MENU_TEXT_INDENT_X
	
	FLOAT headerDown = rawPositions.fCrewStatTitleY*0.1
	
	
	
	IF focus < 0
		focus = 0
	ENDIF
	IF iStatEntries < 5
		focus = 0
	ENDIF
	
	IF focus > iStatEntries-4
		focus = iStatEntries-4
	ENDIF
	
	INT i = 0
	INT j = 0
	INT iDone = 0
	REPEAT iStatEntries i
	
		INT nthCrewStatIndice = PRIVATE_GET_NTH_CREW_INDEX_WITH_STATS(rawData,i+focus)
	
		IF iDone != 4 AND nthCrewStatIndice > -1
			
			
			//TEXT_LABEL iCrewImages[HEIST_SCREEN_MAX_CREW]
			//portrait
			DRAW_SPRITE("HeistHUD", rawData.iCrewImages[nthCrewStatIndice], 
			wrapLeft-(portraitW*0.5), yto+ (rawPositions.fCrewStatTitleY*0.5), 
			portraitW, rawPositions.fCrewStatTitleY, 
			0, 255,255,255,255)
			
			
			//name
			SET_TEXT_WRAP(wrapLeft, wrapRight)
			SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
			SET_TEXT_JUSTIFICATION(FONT_LEFT)
			//RENDER_PLACEHOLDER_TEXT(wrapLeft,yto+headerDown)
			PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(rawData.iCrewNames[nthCrewStatIndice],wrapLeft,yto+headerDown)
			
			
			
			//TEXT_LABEL iCrewNames[HEIST_SCREEN_MAX_CREW]
			
			
			
			
			//role
			SET_TEXT_WRAP(wrapLeft, wrapRight)
			SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
			SET_TEXT_JUSTIFICATION(FONT_RIGHT)
			SET_TEXT_RIGHT_JUSTIFY(TRUE)
			
			INT iStatbars = 3
			SWITCH rawData.eCrewTypes[nthCrewStatIndice]
				CASE HENDSCR_GUNMAN//(wrapRight,yto+headerDown)
					PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_S_GUNM",wrapRight,yto+headerDown)
					iStatbars = 4
					BREAK
				CASE HENDSCR_HACKER
					PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_S_HACK",wrapRight,yto+headerDown)
					BREAK
				CASE HENDSCR_DRIVER
					PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_S_DRIV",wrapRight,yto+headerDown)
					BREAK
			ENDSWITCH
			
			
			
			
			yto += rawPositions.fCrewStatTitleY
			
			//do the bars for the stats// either 4 (gunners) or 3 (hacker/drivers)
			
			
			
			//rawData.eCrewStatType[]
			
			FLOAT statshift = (1.0/iStatbars)*(((rawPositions.fContentStatAdvanceY) - rawPositions.fCrewStatTitleY)*0.8)
			FLOAT staty = yto + CUSTOM_MENU_TEXT_INDENT_Y
			wrapLeft -= portraitW
			REPEAT iStatBars j

				SET_TEXT_WRAP(wrapLeft, wrapRight)
				SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
				SET_TEXT_JUSTIFICATION(FONT_LEFT)
				SWITCH rawData.eCrewTypes[nthCrewStatIndice]//(wrapLeft,staty)
					CASE HENDSCR_GUNMAN
						PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(PRIVATE_Get_Crew_Type_Stat_Name_Label_At_Index(CMT_GUNMAN,j),
																wrapLeft,staty)
						BREAK
					CASE HENDSCR_HACKER
						PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(PRIVATE_Get_Crew_Type_Stat_Name_Label_At_Index(CMT_HACKER,j),
																wrapLeft,staty)
						BREAK
					CASE HENDSCR_DRIVER
						PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(PRIVATE_Get_Crew_Type_Stat_Name_Label_At_Index(CMT_DRIVER,j),
																wrapLeft,staty)
						BREAK
				ENDSWITCH

					//draw the stat bars
					FLOAT bary = staty+(statshift*0.5)//-rawPositions.fCrewStatBarH*0.5
					PRIVATE_HEIST_END_DRAW_ORDER()
					DRAW_RECT(wrapRight-(rawPositions.fCrewStatBarW*0.5)-CUSTOM_MENU_TEXT_INDENT_X,
							  bary,
							  rawPositions.fCrewStatBarW,
							  rawPositions.fCrewStatBarH,
							  0,0,0,
							  rawPositions.lightAlpha)
							  
					//FLOAT iCrewStatCurrentPerc[HEIST_SCREEN_MAX_CREW][5]
					//FLOAT iCrewStatAdditionalPerc[HEIST_SCREEN_MAX_CREW][5]
							  
					FLOAT whitewidth = rawPositions.fCrewStatBarW* rawData.fCrewStatCurrentPerc[i][j]
										
					FLOAT greenwidth = whitewidth + (rawData.fStatPercBlend * (rawPositions.fCrewStatBarW*rawData.fCrewStatAdditionalPerc[i][j]))
					
					
					
					DRAW_RECT(wrapRight-rawPositions.fCrewStatBarW+(greenwidth*0.5)-CUSTOM_MENU_TEXT_INDENT_X,
							  bary,
							  greenwidth,
							  rawPositions.fCrewStatBarH,
							  rawPositions.topcol[0],
							  rawPositions.topcol[1],
							  rawPositions.topcol[2],
							  255)
					
					DRAW_RECT(wrapRight-rawPositions.fCrewStatBarW+(whitewidth*0.5)-CUSTOM_MENU_TEXT_INDENT_X,
							  bary,
							  whitewidth,
							  rawPositions.fCrewStatBarH,
							  255,
							  255,
							  255,
							  255)
					

					
					
				staty += statshift
			ENDREPEAT
			wrapLeft += portraitW
			
		

			yto += rawPositions.fContentStatAdvanceY - rawPositions.fCrewStatTitleY
			++iDone
		ENDIF
		
		
	ENDREPEAT


	UNUSED_PARAMETER(rawData)
ENDPROC


FUNC BOOL PRIVATE_NEW_HEIST_ENDSCREEN_STAT_REGISTER_WRITE(HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions, INT statindex,FLOAT yto,FLOAT wrapLeft, FLOAT wrapRight)
	
	ENUM_MISSION_STATS t = g_MissionStatTrackingArray[statindex].target
	
	IF  t = UNSET_MISSION_STAT_ENUM
		RETURN FALSE
	ENDIF
	
	IF g_MissionStatTrackingPrototypes[t].bHidden
		RETURN FALSE
	ENDIF
	
	BOOL bSuccess = FALSE
	
	IF g_MissionStatTrackingPrototypes[t].less_than_threshold
		IF g_MissionStatTrackingArray[statindex].ivalue < g_MissionStatTrackingPrototypes[t].success_threshold
			bSuccess = TRUE
		ENDIF
	ELSE
		IF g_MissionStatTrackingArray[statindex].ivalue >= g_MissionStatTrackingPrototypes[t].success_threshold
			bSuccess = TRUE
		ENDIF
	ENDIF
	bSuccess = bSuccess

	SET_TEXT_WRAP(wrapLeft, wrapRight)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(GET_MISSION_STAT_NAME(t),wrapLeft,yto)



	//RENDER_PLACEHOLDER_TEXT
		
	
	INT tval = g_MissionStatTrackingPrototypes[t].success_threshold
	INT sval = g_MissionStatTrackingArray[statindex].ivalue
	
	SET_TEXT_WRAP(wrapLeft, wrapRight)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
	SET_TEXT_JUSTIFICATION(FONT_RIGHT)
	SET_TEXT_RIGHT_JUSTIFY(TRUE)//(wrapLeft,yto)
	SWITCH g_MissionStatTrackingPrototypes[t].type
		
		CASE MISSION_STAT_TYPE_UNIQUE_BOOL
			
				IF bSuccess
					//DRAW_TEXT_WITH_ALIGNMENT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
											 //sPlacement.aStyle.TS_STANDARDSMALLHUD,
											// "CHECKMARK",FALSE,TRUE)
					PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("CHECKMARK",wrapLeft,yto)
											
											
				ELSE
					//DRAW_TEXT_WITH_ALIGNMENT(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
											 //sPlacement.aStyle.TS_STANDARDSMALLHUD,
											// "CROSSMARK",FALSE,TRUE)
					PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("CROSSMARK",wrapLeft,yto)
				ENDIF
		
			BREAK
		
		CASE MISSION_STAT_TYPE_TOTALTIME
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE
		CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
			//DRAW_TEXT_TIMER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
							//sPlacement.aStyle.TS_STANDARDSMALLHUD,
							//tval,
							// TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES,
							// "STRING",
							//FALSE,TRUE)
			PRIVATE_RENDER_HEIST_END_SCREEN_TIMER(sval,wrapLeft,yto)
							
			BREAK
		
		CASE MISSION_STAT_TYPE_FRACTION //x/y
		CASE MISSION_STAT_TYPE_HEADSHOTS//ES_STLN
			//DRAW_TEXT_WITH_2_NUMBERS(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
									 //sPlacement.aStyle.TS_STANDARDSMALLHUD,
									 //"FRCTIONSPL",sval,tval,FONT_RIGHT)
			PRIVATE_RENDER_HEIST_END_SCREEN_FRACTION(sval,tval,wrapLeft,yto)
			
			BREAK
		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE // target is overridden to 100 for display
			tval = 100
			FALLTHRU			
		CASE MISSION_STAT_TYPE_ACCURACY //percentage
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD

			//DRAW_TEXT_WITH_NUMBER(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
									 //sPlacement.aStyle.TS_STANDARDSMALLHUD,
									// "ES_PERC", CEIL(TO_FLOAT(sval)/TO_FLOAT(tval)),FONT_RIGHT)
			PRIVATE_RENDER_HEIST_END_SCREEN_TEXTWITH_NUMBER("ES_PERC",CEIL(TO_FLOAT(sval)/TO_FLOAT(tval)*100),wrapLeft,yto)

			BREAK
			

		DEFAULT
			
			//DRAW_TEXT_WITH_2_NUMBERS(sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN],
									 //sPlacement.aStyle.TS_STANDARDSMALLHUD,
									 //"ES_STLN",sval,tval,FONT_RIGHT)
			
			BREAK
		
	ENDSWITCH
	
	
	


	RETURN TRUE
ENDFUNC


PROC PRIVATE_DO_HEIST_END_SCREEN_STATLINES(HEIST_END_SCREEN_RAW_DATA &rawData,HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)
	
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY + rawPositions.fContentOverviewY + CUSTOM_MENU_TEXT_INDENT_Y
	//- rawPositions.fContentOverviewLineY*0.25
	FLOAT wrapLeft = rawPositions.fHeadersX + rawPositions.fHeadersW*2 + rawPositions.fSeperationX*2
	FLOAT wrapRight = wrapLeft + rawPositions.fHeadersW
	//max of 12 lines
	INT iLineCount = 12
	wrapLeft+= CUSTOM_MENU_TEXT_INDENT_X
	wrapRight -= CUSTOM_MENU_TEXT_INDENT_X
	
	
	IF g_iMissionStatsBeingTracked > 0
		
		SET_TEXT_WRAP(wrapLeft, wrapRight)
		SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_STATS",wrapLeft,yto)
		
		yto += rawPositions.fContentOverviewLineY
		--iLineCount
		
		INT i = 0
		REPEAT g_iMissionStatsBeingTracked i

			IF PRIVATE_NEW_HEIST_ENDSCREEN_STAT_REGISTER_WRITE(rawPositions,i,yto,wrapLeft+CUSTOM_MENU_TEXT_INDENT_X,wrapRight)
				--iLineCount
				yto += rawPositions.fContentOverviewLineY
			ENDIF
		
			IF iLineCount = 0
				EXIT
			ENDIF
		ENDREPEAT

	ENDIF
	

	

	//we need at least g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged + 1 lines to display penalties
	INT iHeistIndex = rawData.iHeistIndex
	INT penalties = g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesLogged
	IF iLineCount < penalties + 1
		PRINTLN("HEIST_END_SCREEN_STATLINES: Not enough space to render penalties")
		EXIT
	ENDIF
	
	IF penalties > 0
		
		SET_TEXT_WRAP(wrapLeft, wrapRight)
		SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("PENPLIESHA",wrapLeft,yto)
		
		yto += rawPositions.fContentOverviewLineY
		--iLineCount
		
		
		INT i = 0
		REPEAT penalties i

			
			SET_TEXT_WRAP(wrapLeft, wrapRight)
			SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
			SET_TEXT_JUSTIFICATION(FONT_LEFT)
			PRIVATE_RENDER_HEIST_END_SCREEN_LABEL(PRIVATE_GET_HEIST_PENALTY_STRING(g_HeistNoneSavedDatas[iHeistIndex].Penalties[i]),
												  wrapLeft+CUSTOM_MENU_TEXT_INDENT_X,yto)		
				
				
			SET_TEXT_WRAP(wrapLeft, wrapRight)
			SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
			SET_TEXT_JUSTIFICATION(FONT_RIGHT)
			SET_TEXT_RIGHT_JUSTIFY(TRUE)
			PRIVATE_RENDER_HEIST_END_SCREEN_DOLLARS(g_HeistNoneSavedDatas[iHeistIndex].iPenaltiesValues[i],
													wrapLeft,yto,FALSE,TRUE)
				
			yto += rawPositions.fContentOverviewLineY
			--iLineCount
			//no need to check linecount since it was checked before this
		ENDREPEAT
		
		
	ENDIF
	

	
	
	
	
ENDPROC


PROC PRIVATE_DRAW_HEIST_END_SCREEN_OVERVIEW_AND_STATLINES(HEIST_END_SCREEN_RAW_DATA &rawData,
								HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions)


	//FLOAT xto = rawPositions.fHeadersX + rawPositions.fHeadersW*2 + rawPositions.fSeperationX*2
	FLOAT yto = rawPositions.fHeadersY + rawPositions.fHeadersH + rawPositions.fHeadersToContentY
	
	FLOAT wrapLeft = rawPositions.fHeadersX + rawPositions.fHeadersW*2 + rawPositions.fSeperationX*2
	FLOAT wrapRight = wrapLeft + rawPositions.fHeadersW
	wrapLeft+= CUSTOM_MENU_TEXT_INDENT_X
	wrapRight -= CUSTOM_MENU_TEXT_INDENT_X
	
	//Take
	SET_TEXT_WRAP(wrapLeft, wrapRight)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_S_ACTU",wrapLeft,yto+CUSTOM_MENU_TEXT_INDENT_Y)
	
	//take dollars
	SET_TEXT_WRAP(wrapLeft, wrapRight)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeMedium)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	//RENDER_PLACEHOLDER_TEXT(wrapLeft,yto+suby*2.0)
	PRIVATE_RENDER_HEIST_END_SCREEN_DOLLARS(rawData.iActual,wrapLeft,yto+CUSTOM_MENU_TEXT_INDENT_Y+rawPositions.fContentOverviewLineY*0.5)
	
	
	yto+= rawPositions.fContentOverviewY*0.5

	//Target
	SET_TEXT_WRAP(wrapLeft, wrapRight)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	PRIVATE_RENDER_HEIST_END_SCREEN_LABEL("ES_S_TARG",wrapLeft,yto+CUSTOM_MENU_TEXT_INDENT_Y)
	
	
	//target dollars
	SET_TEXT_WRAP(wrapLeft, wrapRight)
	SET_TEXT_SCALE(1.0, rawPositions.fontSizeMedium)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	//RENDER_PLACEHOLDER_TEXT(wrapLeft,yto+suby*2.0)
	PRIVATE_RENDER_HEIST_END_SCREEN_DOLLARS(rawData.iTotal,wrapLeft,yto+CUSTOM_MENU_TEXT_INDENT_Y+rawPositions.fContentOverviewLineY*0.5)
	
	
	yto+= rawPositions.fContentOverviewY*0.500
	
	
	
	/*
	//do the statlines
	yto = rawPositions.fHeadersY 
			+ rawPositions.fHeadersH 
			+ rawPositions.fHeadersToContentY 
			+ rawPositions.fContentOverviewY
			- rawPositions.fContentOverviewLineY*0.25
	
	
	INT i = 0
	
	
	REPEAT 12 i
		SET_TEXT_WRAP(wrapLeft, wrapRight)
		SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		RENDER_PLACEHOLDER_TEXT(wrapLeft,yto+suby)
		
		SET_TEXT_WRAP(wrapLeft, wrapRight)
		SET_TEXT_SCALE(1.0, rawPositions.fontSizeTiny)
		SET_TEXT_JUSTIFICATION(FONT_RIGHT)
		SET_TEXT_RIGHT_JUSTIFY(TRUE)
		RENDER_PLACEHOLDER_TEXT(wrapLeft,yto+suby)
		
		yto += rawPositions.fContentOverviewLineY
		
	ENDREPEAT
	*/
	
	
	PRIVATE_DO_HEIST_END_SCREEN_STATLINES(rawData,rawPositions)
	
	
	
	UNUSED_PARAMETER(rawData)
ENDPROC


PROC PRIVATE_DRAW_NEW_HEIST_END_SCREEN(HEIST_END_SCREEN_RAW_DATA &rawData,
										HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositions,
										INT &focus)

	//Render white heading bars
	PRIVATE_DRAW_HEIST_END_SCREEN_HEADERS(rawPositions)

	//render character keys
	PRIVATE_DRAW_HEIST_END_CHARACTER_KEY_HEADERS(rawPositions,rawData)

	//render stat entries//render scroll bar if needed
	PRIVATE_DRAW_HEIST_END_CREW_STAT_ENTRIES(rawPositions,rawData)

	//render overview top section//render stat lines (max 12)
	PRIVATE_DRAW_HEIST_END_OVERVIEW_SECTION(rawPositions)
	
	//now render the texts
	PRIVATE_DRAW_HEIST_END_SCREEN_HEADER_TEXTS(rawPositions)
	
	SET_TEXT_COLOUR(255, 255, 255, 255) //white text for all the rest
	
	PRIVATE_DRAW_HEIST_END_SCREEN_CREW_TEXTS(rawData,rawPositions)

	PRIVATE_DRAW_HEIST_END_SCREEN_CREW_STAT_TEXTS(rawData,rawPositions,focus)
	
	PRIVATE_DRAW_HEIST_END_SCREEN_OVERVIEW_AND_STATLINES(rawData,rawPositions)

ENDPROC




////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////








PROC PRIVATE_Draw_Heist_ES_Screen(INT iHeistIndex, 
									MEGA_PLACEMENT_TOOLS &sPlacement,
									HEIST_END_SCREEN_RAW_DATA &rawData,
									HEIST_END_SCREEN_ELEMENT_POSITIONS &rawPositioning,
									INT &focus)
	g_bHeistEndscreenDisplaying = TRUE
	
	
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	DISABLE_ALL_CONTROL_ACTIONS(FRONTEND_CONTROL)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	INVALIDATE_IDLE_CAM()

	sPlacement.TextPlacement[EST_SUBHEAD_BREAKDOWN].y = 0.230


	IF(!rawPositioning.bSet)
		PRIVATE_SET_DEFAULT_HEIST_END_SCREEN_POSITIONS(rawPositioning)
	ENDIF

	PRIVATE_Heist_ES_CALCULATE_TOTALS_AND_UPLOAD_TO_SCALEFORM(iHeistIndex,rawData,rawPositioning)
	
	
	//SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	//PRIVATE_Draw_Heist_ES_Background(sPlacement)
	
	
	
	PRIVATE_DRAW_NEW_HEIST_END_SCREEN(rawData,rawPositioning,focus)
	//RESET_SCRIPT_GFX_ALIGN()
	
	
	//IF iHeistIndex = HEIST_AGENCY
	//	PRIVATE_Draw_Heist_ES_Base_Text(sPlacement,TRUE)
	//ELSE
	//	PRIVATE_Draw_Heist_ES_Base_Text(sPlacement,FALSE)
	//ENDIF
	//PRIVATE_Draw_Heist_ES_Values(iHeistIndex, sPlacement)
	//PRIVATE_Draw_Heist_ES_Crew_Details(iHeistIndex, sPlacement)
	//PRIVATE_Draw_Heist_ES_Summary(iHeistIndex, sPlacement)
	
	//PRIVATE_Draw_Heist_ES_Breakdown(iHeistIndex, sPlacement)
	
	
ENDPROC





//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ End Screen Control ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
/*
PROC PRIVATE_Reset_Heist_End_Screen_Stolen_Items(INT heistIndex)
	
	INT index
	REPEAT MAX_END_SCREEN_STOLEN_ITEMS index
		g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iItemValue[index] = -1
		g_savedGlobals.sHeistData.sEndScreenData[heistIndex].tItemDescription[index] = ""
	ENDREPEAT
	g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iItemCount = 0
	
ENDPROC
*/



PROC PRIVATE_Validate_Heist_End_Screen(INT heistIndex)
	INT index
	
	//Get which heist variation was chosen.
	INT iHeistChoice = Get_Mission_Flow_Int_Value(Get_Heist_Choice_FlowInt_ID(heistIndex))
	
	//Check that the potential take value is set.
	IF g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iPotentialTake = 0
		SCRIPT_ASSERT("PRIVATE_Validate_Heist_End_Screen: Heist potential take value not set.")
	ENDIF
	
	
	//Check that the status of all crew members are set.
	REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize index
		IF g_savedGlobals.sHeistData.sEndScreenData[heistIndex].eCrewStatus[index] = CMST_NOT_SET
			SCRIPT_ASSERT("PRIVATE_Validate_Heist_End_Screen: Status of all heist crew members not set.")
		ENDIF
	ENDREPEAT
	
	//Check that at least one stolen item has been added.
	/*
	IF g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iItemCount <= 0
		SCRIPT_ASSERT("PRIVATE_Validate_Heist_End_Screen: No stolen items set.")
	ENDIF
	*/
ENDPROC




//INFO:			Author - Ben Rollinson
//PARAM NOTES:	
//				heistIndex - An index referencing a specific heist.
//PURPOSE:		Resets any data that has already been setup for a heist end screen.
/*
PROC RESET_HEIST_END_SCREEN(INT heistIndex)

	//Reset all heist "take" values.
	g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iPotentialTake = 0
	g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iActualTake = 0
	//g_savedGlobals.sHeistData.sEndScreenData[heistIndex].iPersonalTake = 0
	
	//PRIVATE_Reset_Heist_End_Screen_Stolen_Items(heistIndex)
	PRIVATE_Reset_Heist_End_Screen_Crew_Member_Statuses(heistIndex)
	
	
	PRIVATE_RESET_HEIST_TIME_WINDOWS(heistIndex)
	PRIVATE_RESET_HEIST_GEAR_BUFFER(heistIndex)
	//PRIVATE_RESET_HEIST_PENALTY_BUFFER(heistIndex)
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_HEIST, GET_THIS_SCRIPT_NAME(), " reset heist end screen for ", GET_HEIST_INDEX_DEBUG_STRING(heistIndex), ".")
	#ENDIF
ENDPROC
*/










//INFO:			Author - Ben Rollinson
//PARAM NOTES:	
//				heistIndex - An index referencing a specific heist.
//PURPOSE:		Call this to display a heist end screen. The screen will transition in, wait for a button press, and then tranisiton out. The command will hang a script until the screen has transitioned out.
/*
PROC DISPLAY_HEIST_END_SCREEN(INT heistIndex)
	PRIVATE_Validate_Heist_End_Screen(heistIndex)
	PRIVATE_Heist_ES_CALCULATE_TOTALS_AND_UPLOAD_TO_SCALEFORM(heistIndex)
ENDPROC
*/









