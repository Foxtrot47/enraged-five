//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 10/02/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	  Rural Bank Heist Planning Board Data Header				│
//│																				│
//│			This header contains lookups to all data required by the Paleto		│
//│			Score planning board.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "heist_board_data_generic.sch"


//Rural Bank Heist Assets.
ENUM BoardAssetRural
	PBA_RURAL_WEAPONS_AMMO = 0,
	PBA_RURAL_ESCAPE_PLAN_MAP,
	PBA_RURAL_SPEED_BOAT,
	PBA_RURAL_NOTE_COPS,
	PBA_RURAL_NOTE_COPCARS,
	PBA_RURAL_NOTE_TIME,
	PBA_RURAL_NOTE_PLANA,
	PBA_RURAL_NOTE_PLANB,
	PBA_RURAL_NOTE_VAN,
	PBA_RURAL_NOTE_BANKALARM,
	PBA_RURAL_NOTE_BANK,
	PBA_RURAL_LIFE_INVADER,
	PBA_RURAL_CITY_MAP,
	PBA_RURAL_NOTE_BANK2,
	PBA_RURAL_DOLLAR,
	PBA_RURAL_HARDWARE,
	PBA_RURAL_HOURS
ENDENUM


PROC Setup_Rural_Bank_Heist_Board_Data(HeistBoardData &paramBoardData, INT paramMaxCrewSize)
	
	// Rural Bank Heist display groups.
	// PBDG_1 - Lester put up board after setup.
	// PBDG_2 - Crew selected.
	// PBDG_3 - Military hardware obtained.
	
	//												Planning Location	Board Scaleform		Board Text	Board Dialogue	Crew Dialogue	Choice Group	Todo List Group		Chosen A		Chosen B		Max Crew Size		Blocking Area Center					Blocking Area Dimensions 
	Store_Board_Core_Data(paramBoardData,			PLN_METH_LAB,		"HEIST_PALETO",		"BOARD3",	"RHFAUD",		"CRWAUD",		PBDG_INVALID,	PBDG_1,				PBDG_INVALID,	PBDG_INVALID,	paramMaxCrewSize,	<<1394.510986,3607.037109,36.941910>>,	<<7.500000,7.750000,6.750000>>)
	
	//												SF Pixel Dim.		SF Dim.				SF Scale		Cam Dist	Default Focus	Crew 1 Pos		Crew 2 Pos		Crew 3 Pos		Crew 4 Pos	   	Crew 5 Pos	   	Choice Pos		Choice CamOff	List Pos
	Store_Board_Main_Layout_Data(paramBoardData, 	750, 540,			2.03, 1.30,			3.35, 1.88,		1.85,		375,252,		150,400,		0,0,			0,0,			0,0,			0,0,			620,370,		0,85,			695,215	)

	//												Max Zoom			Choice Zoom			POI Zoom
	Store_Board_Zoom_Levels(paramBoardData,			20.0,				22.0,				28.0)

	//												Choice Help 1		Choice Help 2		Crew Help 1		Crew Help 2		Choice Diag		Crew Diag		Confirm Diag	Too Slow Diag
	Store_Board_Text_Data(paramBoardData,			"",					"",					"",				"",				"",				"RHP1",			"RHP8",			"RHFP11"	)

	//Load crew member dialogue.									Crew Member					Pick Diag		Met Diag		
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_GUSTAV,			"CRW_GM",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_NORM,			"CRW_NR",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_B_DARYL,			"CRW_DJ",		""			)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_PACKIE_UNLOCK,	"CRW_PM",		"CRM_PM"   	)	
	Store_Heist_Board_Crew_Member_Dialogue_Data(paramBoardData,		CM_GUNMAN_G_CHEF_UNLOCK,	"",				"CRM_CH"	)	

	//Load more specific lines of dialogue for this heist.
	Store_Heist_Board_Crew_Member_Dialogue_Slot(paramBoardData, 	CRDS_USED_JEWEL,			"RHP10B")

	//Rural Bank heist static items.
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_WEAPONS_AMMO),	PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_ESCAPE_PLAN_MAP),	PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_SPEED_BOAT),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_COPS),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_COPCARS),	PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_TIME),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_PLANA),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_PLANB),		PBDG_1,		0,0	)	
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_VAN),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_BANKALARM),	PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_NOTE_BANK),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_CITY_MAP),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_DOLLAR),			PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_HARDWARE),		PBDG_1,		0,0	)
	Store_Board_Static_Item_Data(paramBoardData,	ENUM_TO_INT(PBA_RURAL_HOURS),			PBDG_1,		0,0	)

	//Rural Bank heist todo items.
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_1,	PBDG_1,	"H_TD_TOWN")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_1,	PBDG_1,	"H_TD_ALAR")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_1,	PBDG_1,	"H_TD_COPS")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_1,	PBDG_2,	"H_TD_CREW")
	Store_Board_Todo_List_Item_Data(paramBoardData,	PBDG_1,	PBDG_3,	"H_TD_MILI")
	
	//Rural Bank pins.
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	67,78		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	204,61		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	217,256		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	337,24		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	466,32		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	570,20		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	565,150		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	671,184		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	379,311		)
	Store_Board_Pin_Layout_Data(paramBoardData,	 PBDG_1,	50,192		)
	
	//Rural Bank heist points of interest.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,		"RHP5",	396,232		)	// Police response times.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,		"RHP6",	175,150		)	// Military hardware.
	Store_Board_Point_Of_Interest_Data(paramBoardData,	PBDG_1,		"RHP7",	455,85		)	// Quick getaway.
ENDPROC
