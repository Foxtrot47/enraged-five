//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	vehicle_gen_private.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains all the data for each vehicle 	//
//							that can be generated via script.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"
USING "player_ped_public.sch"

/// PURPOSE: Returns TRUE if the specified flag is set
FUNC BOOL GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHICLE_GEN_NAME_ENUM eName, VEHICLE_GEN_SAVED_FLAG_ENUM eFlag)

	IF eName = VEHGEN_NONE
		RETURN FALSE
	ENDIF
	#if USE_CLF_DLC
		RETURN IS_BIT_SET(g_savedGlobalsClifford.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
	#endif
	#if USE_NRM_DLC
		RETURN IS_BIT_SET(g_savedGlobalsnorman.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN IS_BIT_SET(g_savedGlobals.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
	#endif
	#endif
ENDFUNC

/// PURPOSE: Sets the state for the specified flag
PROC SET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHICLE_GEN_NAME_ENUM eName, VEHICLE_GEN_SAVED_FLAG_ENUM eFlag, BOOL bState)
	
	IF eName = VEHGEN_NONE
		EXIT
	ENDIF
	
	#if USE_CLF_DLC
		IF bState
			SET_BIT(g_savedGlobalsClifford.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF bState
			SET_BIT(g_savedGlobalsnorman.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF bState
			SET_BIT(g_savedGlobals.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
		ELSE
			CLEAR_BIT(g_savedGlobals.sVehicleGenData.iProperties[eName], ENUM_TO_INT(eFlag))
		ENDIF
	#endif
	#endif
	

ENDPROC

/// PURPOSE: Fills the struct with all the vehicle gen data
FUNC BOOL GET_VEHICLE_GEN_DATA(VEHICLE_GEN_DATA_STRUCT &sData, VEHICLE_GEN_NAME_ENUM eName)

	BOOL bDataFound
	
	INT iOffset
	
	sData.coords = <<0,0,0>>
	sData.heading = 0.0
	sData.model = DUMMY_MODEL_FOR_SCRIPT
	sData.help = ""
	sData.flags = 0
	sData.colour1 = 0
	sData.colour2 = 0
	sData.ped = NO_CHARACTER
	sData.blip = RADAR_TRACE_INVALID
	sData.dynamicSlotIndex = 0
	sData.scenario_block_minXYZ = <<0,0,0>>
	sData.scenario_block_maxXYZ = <<0,0,0>>
	#IF IS_DEBUG_BUILD
		sData.dbg_name = ""
	#ENDIF
	
	// Note: Use the VEHICLE_GEN_NON_SAVED_FLAG_ENUM enums when setting the
	// 		 bits in the sData.flags

	SWITCH eName
		CASE VEHGEN_MICHAEL_SAVEHOUSE
			sData.coords 	= << -831.8538, 172.1154, 69.9058 >>
			sData.heading 	= 157.5705
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_MICHAEL
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MICHAEL_SAVEHOUSE"
			#ENDIF
		BREAK
		CASE VEHGEN_MICHAEL_SAVEHOUSE_COUNTRY
			sData.coords 	= <<1970.9434, 3801.6838, 31.1396>>
			sData.heading 	= 301.3964
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_MICHAEL
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MICHAEL_SAVEHOUSE_COUNTRY"
			#ENDIF
		BREAK
		CASE VEHGEN_FRANKLIN_SAVEHOUSE_CAR
			sData.coords 	= <<-22.6297, -1439.1368, 29.6549>>
			sData.heading 	= 180.0808
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_FRANKLIN
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_FRANKLIN_SAVEHOUSE_CAR"
			#ENDIF
		BREAK
		CASE VEHGEN_FRANKLIN_SAVEHOUSE_BIKE
			sData.coords 	= <<-22.5229, -1434.6986, 29.6552>>
			sData.heading 	= 141.6114
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_BIKE)
			sData.ped 		= CHAR_FRANKLIN
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_FRANKLIN_SAVEHOUSE_BIKE"
			#ENDIF
		BREAK
		CASE VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_CAR
			sData.coords 	= <<10.9281, 545.6690, 174.7951>>
			sData.heading 	= 61.3920
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_FRANKLIN
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_F_SAVEHOUSE_HILLS_CAR"
			#ENDIF
		BREAK
		CASE VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_BIKE
			sData.coords 	= <<6.1093, 544.9742, 174.2835>>
			sData.heading 	= 92.1548
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_BIKE)
			sData.ped 		= CHAR_FRANKLIN
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_F_SAVEHOUSE_HILLS_BIKE"
			#ENDIF
		BREAK
		CASE VEHGEN_TREVOR_SAVEHOUSE_COUNTRY
			sData.coords 	= << 1981.4163, 3808.1313, 31.1384 >>
			sData.heading 	= 117.2557
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_TREVOR_SAVEHOUSE_COUNTRY"
			#ENDIF
		BREAK
		CASE VEHGEN_TREVOR_SAVEHOUSE_CITY
			sData.coords 	= << -1158.4875, -1529.3673, 3.8995 >>
			sData.heading 	= 35.7505
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_TREVOR_SAVEHOUSE_CITY"
			#ENDIF
		BREAK
		CASE VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB
			sData.coords 	= <<148.2868, -1270.5695, 28.2252>>
			sData.heading 	= 208.4685
			sData.model 	= GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB"
			#ENDIF
		BREAK
		CASE VEHGEN_MOUNTAIN_BIKE_CH
			sData.coords 	= << 1459.5085, -1380.4500, 78.3259 >>
			sData.heading 	= 99.6211
			sData.model 	= SCORCHER
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MOUNTAIN_BIKE_CH"
			#ENDIF
		BREAK
		CASE VEHGEN_SEASHARK_SM
			sData.coords 	= << -1518.9470, -1387.8655, -0.5134 >>
			sData.heading 	= 98.3867
			sData.model 	= SEASHARK
			bDataFound = TRUE
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_SEASHARK_SM"
			#ENDIF
		BREAK
		CASE VEHGEN_DUSTER
			sData.coords 	= << 353.0926, 3577.5925, 32.3510 >>
			sData.heading 	= 16.6205
			sData.model 	= DUSTER
			bDataFound = TRUE
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_DUSTER"
			#ENDIF
		BREAK
		/*
		CASE VEHGEN_IMPOUND_TOW_TRUCK
			sData.coords 	= << 419.1216, -1630.1923, 29.2580 >>
			sData.heading 	= 140.37
			sData.model 	= TOWTRUCK
			sData.ped 		= CHAR_FRANKLIN
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_IMPOUND_TOW_TRUCK"
			#ENDIF
		BREAK
		*/
		
		////////////////////////////////////////////////////////////////////////////////
		///      HANGARS
		///      
		CASE VEHGEN_WEB_HANGAR_MICHAEL
			sData.dynamicSlotIndex = 0
			#IF USE_TU_CHANGES
				sData.coords 	= <<-1652.0043, -3142.3484, 12.9921>>
				sData.heading 	= 329.1082
			#ENDIF
			#IF NOT USE_TU_CHANGES
				sData.coords 	= <<-1636.7456, -3152.9697, 12.9928>>
				sData.heading 	= 329.5191
			#ENDIF
			//sData.help		= "Hangar_Mike"
			sData.ped 		= CHAR_MICHAEL
			sData.blip 		= RADAR_TRACE_HANGAR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_HANGAR_MICHAEL"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_HANGAR_FRANKLIN
			sData.dynamicSlotIndex = 1
			#IF USE_TU_CHANGES
				sData.coords 	= <<-1271.6487, -3380.6853, 12.9451>>
				sData.heading 	= 329.5137
			#ENDIF
			#IF NOT USE_TU_CHANGES
				sData.coords 	= <<-1255.2875, -3395.9893, 12.9453>>
			#ENDIF
			//sData.help		= "Hangar_Frank"
			sData.ped 		= CHAR_FRANKLIN
			sData.blip 		= RADAR_TRACE_HANGAR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_HANGAR_FRANKLIN"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_HANGAR_TREVOR
			sData.dynamicSlotIndex = 2
			sData.coords 	= <<1735.5863, 3294.5305, 40.1651>>
			sData.heading 	= 194.9525
			//sData.help		= "Hangar_Trevor"
			sData.ped 		= CHAR_TREVOR
			sData.blip 		= RADAR_TRACE_HANGAR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_HANGAR_TREVOR"
			#ENDIF
		BREAK
		
		////////////////////////////////////////////////////////////////////////////////
		///      MARINAS
		///      
		CASE VEHGEN_WEB_MARINA_MICHAEL
			sData.dynamicSlotIndex = 3
			sData.coords 	= <<-846.27, -1363.19, 0.22>>
			sData.heading 	= 108.78
			//sData.help		= "Marina_01_VB"
			sData.ped 		= CHAR_MICHAEL
			sData.blip 		= RADAR_TRACE_DOCK
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_MARINA_MICHAEL"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_MARINA_FRANKLIN
			sData.dynamicSlotIndex = 4
			sData.coords 	= <<-849.47, -1354.99, 0.24>>
			sData.heading 	= 109.84
			//sData.help		= "Marina_02_VB"
			sData.ped 		= CHAR_FRANKLIN
			sData.blip 		= RADAR_TRACE_DOCK
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_MARINA_FRANKLIN"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_MARINA_TREVOR
			sData.dynamicSlotIndex = 5
			sData.coords 	= <<-852.47, -1346.20, 0.21>>
			sData.heading 	= 108.76
			//sData.help		= "Marina_03_VB"
			sData.ped 		= CHAR_TREVOR
			sData.blip 		= RADAR_TRACE_DOCK
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_MARINA_TREVOR"
			#ENDIF
		BREAK
		
		////////////////////////////////////////////////////////////////////////////////
		///      HELIPADS
		///      
		CASE VEHGEN_WEB_HELIPAD_MICHAEL
			sData.dynamicSlotIndex = 6
			sData.coords	= <<-745.8570, -1433.9036, 4.0005>>
			//sData.help		= "Helipad_01_VB"
			sData.ped 		= CHAR_MICHAEL
			sData.blip 		= RADAR_TRACE_HELIPAD
			sData.scenario_block_minXYZ = <<-756.2952, -1441.6093, 2.9184>>
			sData.scenario_block_maxXYZ = <<-738.0606, -1423.0676, 8.2835>>
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_HELIPAD_MICHAEL"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_HELIPAD_FRANKLIN
			sData.dynamicSlotIndex = 7
			sData.coords 	 = <<-761.8486, -1453.8293, 4.0005>>
			//sData.help		 = "Helipad_02_VB"
			sData.ped 		 = CHAR_FRANKLIN
			sData.blip 		 = RADAR_TRACE_HELIPAD
			sData.scenario_block_minXYZ = <<-772.8158, -1459.9572, 3.2894>>
			sData.scenario_block_maxXYZ = <<-754.3353, -1440.8361, 8.3334>>
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_HELIPAD_FRANKLIN"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY
			sData.dynamicSlotIndex = 8
			sData.coords 	 = <<1769.3, 3244.0, 41.1>>
			//sData.help		 = "Helipad_04_VB"
			sData.ped 		 = CHAR_TREVOR
			sData.blip 		 = RADAR_TRACE_HELIPAD
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY"
			#ENDIF
		BREAK
		
		////////////////////////////////////////////////////////////////////////////////
		///      CAR GARAGE
		///      
		CASE VEHGEN_WEB_CAR_MICHAEL
			sData.dynamicSlotIndex = 9
			sData.coords 	= <<192.7897, -1020.5385, -99.98>>
			sData.heading 	= 180.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			sData.ped 		= CHAR_MICHAEL
			sData.blip 		= RADAR_TRACE_GARAGE
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_CAR_MICHAEL"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_CAR_FRANKLIN
			sData.dynamicSlotIndex = 10
			sData.coords 	= <<192.7897, -1020.5385, -99.98>>
			sData.heading 	= 180.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			sData.ped 		= CHAR_FRANKLIN
			sData.blip 		= RADAR_TRACE_GARAGE
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_CAR_FRANKLIN"
			#ENDIF
		BREAK
		CASE VEHGEN_WEB_CAR_TREVOR
			sData.dynamicSlotIndex = 11
			sData.coords 	= <<192.7897, -1020.5385, -99.98>>
			sData.heading 	= 180.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			sData.ped 		= CHAR_TREVOR
			sData.blip 		= RADAR_TRACE_GARAGE
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WEB_CAR_TREVOR"
			#ENDIF
		BREAK
		
		CASE VEHGEN_MICHAEL_GARAGE_1
		CASE VEHGEN_FRANKLIN_GARAGE_1
		CASE VEHGEN_TREVOR_GARAGE_1
			iOffset = (ENUM_TO_INT(eName) - ENUM_TO_INT(VEHGEN_MICHAEL_GARAGE_1))
			sData.dynamicSlotIndex = 12 + iOffset
			sData.coords 	= <<196.2794, -1020.4791, -99.98>>
			sData.heading 	= 180.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			sData.ped 		= INT_TO_ENUM(enumCharacterList, ENUM_TO_INT(CHAR_MICHAEL)+iOffset)
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLOCK_CLEANUP_ON_ENTRY))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_GARAGE_1"
			#ENDIF
		BREAK
		CASE VEHGEN_MICHAEL_GARAGE_2
		CASE VEHGEN_FRANKLIN_GARAGE_2
		CASE VEHGEN_TREVOR_GARAGE_2
			iOffset = (ENUM_TO_INT(eName) - ENUM_TO_INT(VEHGEN_MICHAEL_GARAGE_2))
			sData.dynamicSlotIndex = 15 + iOffset
			sData.coords 	= <<199.8872, -1020.0480, -99.98>>
			sData.heading 	= 180.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			sData.ped 		= INT_TO_ENUM(enumCharacterList, ENUM_TO_INT(CHAR_MICHAEL)+iOffset)
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLOCK_CLEANUP_ON_ENTRY))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_GARAGE_2"
			#ENDIF
		BREAK
		CASE VEHGEN_MICHAEL_GARAGE_3
		CASE VEHGEN_FRANKLIN_GARAGE_3
		CASE VEHGEN_TREVOR_GARAGE_3
			iOffset = (ENUM_TO_INT(eName) - ENUM_TO_INT(VEHGEN_MICHAEL_GARAGE_3))
			sData.dynamicSlotIndex = 18 + iOffset
			sData.coords 	= <<203.6006, -1019.7762, -99.98>>
			sData.heading 	= 180.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			sData.ped 		= INT_TO_ENUM(enumCharacterList, ENUM_TO_INT(CHAR_MICHAEL)+iOffset)
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLOCK_CLEANUP_ON_ENTRY))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_GARAGE_3"
			#ENDIF
		BREAK
		
		CASE VEHGEN_MISSION_VEH
			sData.dynamicSlotIndex = 21
			sData.coords 	= << 0.0, 0.0, 0.0 >>
			sData.heading 	= 0.0
			sData.model 	= DUMMY_MODEL_FOR_SCRIPT
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_COORDS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MISSION_VEH"
			#ENDIF
		BREAK
		CASE VEHGEN_MISSION_VEH_FBI4_PREP
			sData.dynamicSlotIndex = 22
			sData.coords 	= <<723.2515, -632.0496, 27.1484>>
			sData.heading 	= 12.9316
			sData.model 	= TAILGATER
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_COORDS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_IGNORE_HANDOVER_MODEL_CHECKS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MISSION_VEH_FBI4_PREP"
			#ENDIF
		BREAK
		CASE VEHGEN_TREV1_SMASHED_TRAILER
			sData.coords 	= <<-51.23, 3111.9, 24.95>>
			sData.heading 	= 46.78
			sData.model 	= PROPTRAILER
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_TREV1_SMASHED_TRAILER"
			#ENDIF
		BREAK
		CASE VEHGEN_BJXL_CRASH_POST_ARM3
			sData.coords 	= << -55.7984, -1096.5856, 25.4223 >>
			sData.heading 	= 308.0596
			sData.model 	= BJXL
			sData.colour1	= 126
			sData.colour2	= 126
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_FORCE_COLOURS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_BJXL_CRASH_POST_ARM3"
			#ENDIF
		BREAK
		CASE VEHGEN_EPSILON6_PLANE
			sData.coords 	= <<-2892.93,3192.37,11.66>>
			sData.heading 	= -132.35
			sData.model 	= VELUM
			sData.colour1	= 157
			sData.colour2	= 157
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_FORCE_COLOURS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPSILON6_PLANE"
			#ENDIF
		BREAK
		CASE VEHGEN_DOCKSP2B_CHINOOK
			#IF USE_TU_CHANGES
				sData.coords 	= <<1744.3083, 3270.6731, 40.2076>>
				sData.heading 	= 125
			#ENDIF
			#IF NOT USE_TU_CHANGES
				sData.coords 	= <<1692.4996, 3270.3042, 39.9374>>
				sData.heading 	= 289.8685
			#ENDIF
			sData.model 	= CARGOBOB3
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_DOCKSP2B_CHINOOK"
			#ENDIF
		BREAK
		CASE VEHGEN_DOCKSP2B_SUB
			sData.coords 	= <<1751.43970, 3322.64307, 42.1855>>
			sData.heading 	= 268.1340
			sData.model 	= SUBMERSIBLE
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_DOCKSP2B_SUB"
			#ENDIF
		BREAK
		CASE VEHGEN_FBI4_TOWING
			sData.coords 	= <<1377.1045, -2076.2000, 52.0000>>
			sData.heading 	= 37.5000
			sData.model 	= TOWTRUCK
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_FBI4_TOWING"
			#ENDIF
		BREAK
		CASE VEHGEN_FBI4_TRASH
			sData.coords 	= <<1380.4200, -2072.7695, 51.7607>>
			sData.heading 	= 37.5000
			sData.model 	= TRASH
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_FBI4_TRASH"
			#ENDIF
		BREAK
		CASE VEHGEN_RURALH_MILITARY_TRUCK
			sData.coords 	= <<1359.3892, 3618.4407, 33.8907>>
			sData.heading 	= 108.2337
			sData.model 	= BARRACKS
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_RURALH_MILITARY_TRUCK"
			#ENDIF
		BREAK
		CASE VEHGEN_AGENCY_PREP_FIRETRUCK
			sData.coords 	= <<693.1154, -1018.1551, 21.6387>>
			sData.heading 	= 177.6454
			sData.model 	= FIRETRUK
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_AGENCY_PREP_FIRETRUCK"
			#ENDIF
		BREAK
		CASE VEHGEN_EPS3_VACCA
			sData.coords 	= <<-73.6963, 495.1240, 143.5226>>
			sData.heading 	= 155.5994
			sData.model 	= VACCA
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_VACCA"
			#ENDIF
		BREAK
		CASE VEHGEN_EPS3_SURANO
			sData.coords 	= <<-67.6314, 891.8266, 234.5348>>
			sData.heading 	= 294.9930
			sData.model 	= SURANO
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_SURANO"
			#ENDIF
		BREAK
		CASE VEHGEN_EPS3_TORNADO2
			sData.coords 	= <<533.9048, -169.2469, 53.7005>>
			sData.heading 	= 1.2998
			sData.model 	= TORNADO2
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_TORNADO2"
			#ENDIF
		BREAK 
		CASE VEHGEN_EPS3_SUPERD
			sData.coords 	= <<-726.8914, -408.6952, 34.0416>>
			sData.heading 	= 267.7392
			sData.model 	= SUPERD
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_SUPERD"
			#ENDIF
		BREAK
		CASE VEHGEN_EPS3_DOUBLE
			sData.coords 	= <<-1321.5186, 261.3993, 61.5709>>
			sData.heading 	= 350.7697
			sData.model 	= DOUBLE
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_DOUBLE"
			#ENDIF
		BREAK
		CASE VEHGEN_EPS3_DOUBLE_2
			sData.coords 	= <<-1267.9991, 451.6463, 93.7071>>
			sData.heading 	= 48.9311
			sData.model 	= DOUBLE
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_DOUBLE_2"
			#ENDIF
		BREAK
		CASE VEHGEN_EPS3_DOUBLE_3
			sData.coords 	= <<-1062.0762, -226.7637, 37.1570>>
			sData.heading 	= 234.2767
			sData.model 	= DOUBLE
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_EPS3_DOUBLE_3"
			#ENDIF
		BREAK
		CASE VEHGEN_MRSP_PHARMACY1
			sData.coords 	= <<68.169144,-1558.958130,29.469042>>
			sData.heading 	= 49.905754
			sData.model 	= RUMPO2
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MRSP_PHARMACY1"
			#ENDIF
		BREAK
		CASE VEHGEN_MRSP_PHARMACY2
			sData.coords 	= <<589.439880,2736.707764,42.033165>>
			sData.heading 	= -175.710495
			sData.model 	= RUMPO2
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MRSP_PHARMACY2"
			#ENDIF
		BREAK
		CASE VEHGEN_MRSP_HOSPITAL1
			sData.coords 	= <<-488.773956,-344.572052,34.363564>>
			sData.heading 	= 82.404198
			sData.model 	= RUMPO2
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MRSP_HOSPITAL1"
			#ENDIF
		BREAK		
		CASE VEHGEN_MRSP_HOSPITAL2
			sData.coords 	= <<288.880829,-585.472839,43.154282>>
			sData.heading 	= -20.807068
			sData.model 	= RUMPO2
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MRSP_HOSPITAL2"
			#ENDIF
		BREAK	
		CASE VEHGEN_MRSP_HOSPITAL3
			sData.coords 	= <<304.829376,-1383.674194,31.677443>>
			sData.heading 	= -41.116028
			sData.model 	= RUMPO2
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MRSP_HOSPITAL3"
			#ENDIF
		BREAK	
		CASE VEHGEN_MRSP_HOSPITAL4
			sData.coords 	= <<1126.194336,-1481.485962,34.701603>>
			sData.heading 	= -91.433693
			sData.model 	= RUMPO2
			sData.ped 		= CHAR_TREVOR
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_MRSP_HOSPITAL4"
			#ENDIF
		BREAK
		CASE VEHGEN_PROPERTY_MARINA_SUB
			sData.coords 	= <<-1598.36, 5252.84, 0>>
			sData.heading 	= 28.14
			sData.model 	= SUBMERSIBLE
			sData.blip 		= RADAR_TRACE_SUB
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_PROPERTY_MARINA_SUB"
			#ENDIF
		BREAK
		CASE VEHGEN_PROPERTY_MARINA_ZODIAC
			sData.coords 	= <<-1602.62, 5260.37, 0.86>>
			sData.heading 	= 25.32
			sData.model 	= DINGHY
			sData.blip 		= RADAR_TRACE_DINGHY
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_PROPERTY_MARINA_ZODIAC"
			#ENDIF
		BREAK
		CASE VEHGEN_AIRPORT_VEHICLE
			sData.coords 	= <<2116.5710, 4763.2793, 40.1596>>
			sData.heading 	= 198.7230
			sData.model 	= BFINJECTION
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_AIRPORT_VEHICLE"
			#ENDIF
		BREAK
		CASE VEHGEN_BLIMP_CASINO
			sData.coords 	=  << 1133.21, 120.20, 80.9 >>
			sData.heading 	= 134.40
			IF IS_LAST_GEN_PLAYER()
				sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("BLIMP2"))
			ELSE
				sData.model 	= BLIMP
			ENDIF
			sData.blip 		= RADAR_TRACE_BLIMP
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_LONG_RANGE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_BLIMP_CASINO"
			#ENDIF
		BREAK
		CASE VEHGEN_BLIMP_DOCKS
			sData.coords 	= << -806.31, -2679.65, 13.9 >>
			sData.heading 	= 150.54
			IF IS_LAST_GEN_PLAYER()
				sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("BLIMP2"))
			ELSE
				sData.model 	= BLIMP
			ENDIF
			sData.blip 		= RADAR_TRACE_BLIMP
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_LONG_RANGE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_BLIMP_DOCKS"
			#ENDIF
		BREAK
		CASE VEHGEN_TREVOR_BLAZER3
			sData.coords 	= <<1985.85, 3828.96, 31.98>>  
			sData.heading 	= -16.58
			sData.model 	= BLAZER3
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_TREVOR_BLAZER3"
			#ENDIF
		BREAK
		
		// Wildlife Photography sub spawn.
		CASE VEHGEN_WILDPHOTO_SUB
			sData.coords 	= <<3870.75, 4464.67, 0.0>>
			sData.heading 	= 0.0
			sData.model 	= SUBMERSIBLE2
			sData.blip 		= RADAR_TRACE_SUB
			//SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_ONCE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
			//SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_LONG_RANGE))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
			//SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED)) //VEHGEN_NS_FLAG_ANCHORED
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_WILDPHOTO_SUB"
			#ENDIF
		BREAK
		
		// RE Duel car city spawn.
		CASE VEHGEN_RE_DUEL_CITY
			sData.coords 	= <<1257.7295, -2564.4741, 41.7170>>  
			sData.heading 	= 284.5561
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("DUKES2"))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_RE_DUEL_CITY"
			#ENDIF
		BREAK
		
		// RE Duel car country spawn.
		CASE VEHGEN_RE_DUEL_COUNTRY
			sData.coords 	= <<643.2823, 3014.1521, 42.2733>>
			sData.heading 	= 128.0554
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("DUKES2"))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_RE_DUEL_COUNTRY"
			#ENDIF
		BREAK
		
		// RE Seaplane city spawn.
		CASE VEHGEN_RE_SEAPLANE_CITY
			sData.coords 	= <<38.9368, 850.8677, 196.3000>>
			sData.heading 	= 311.6813
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("DODO"))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			//SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED)) //VEHGEN_NS_FLAG_ANCHORED
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_RE_SEAPLANE_CITY"
			#ENDIF
		BREAK
		
		// RE Seaplane country spawn.
		CASE VEHGEN_RE_SEAPLANE_COUNTRY
			sData.coords 	= <<1333.8752, 4262.2256, 30.7800>>
			sData.heading 	= 262.5293
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("DODO"))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))
			SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
			//SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED)) //VEHGEN_NS_FLAG_ANCHORED
			bDataFound = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VEHGEN_RE_SEAPLANE_COUNTRY"
			#ENDIF
		BREAK
		
	ENDSWITCH

	// Update model name if using dynamic data
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
		#if USE_CLF_DLC
			sData.model = g_savedGlobalsClifford.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].eModel	
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobalsClifford.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex], <<0,0,0>>)
				sData.coords = g_savedGlobalsClifford.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex]
			ENDIF
			IF g_savedGlobalsClifford.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex] != -1.0
				sData.heading = g_savedGlobalsClifford.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex]
			ENDIF
		#endif
		#if USE_NRM_DLC
			sData.model = g_savedGlobalsnorman.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].eModel	
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobalsnorman.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex], <<0,0,0>>)
				sData.coords = g_savedGlobalsnorman.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex]
			ENDIF
			IF g_savedGlobalsnorman.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex] != -1.0
				sData.heading = g_savedGlobalsnorman.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex]
			ENDIF
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			sData.model = g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].eModel	
			
			#IF USE_TU_CHANGES
				// Fix for bug # 1876673 - Override coords for miljet
				// Fix for bug # 1876654 - Override coords for besra
				// Fix for bug # 1924889 - Michael and Trevor clip through parked Western Besra during final cutscene of Exile 2
				// Fix for bug # 2059214 - Titan explodes instantly when spawned from Trevors hanger
				// Fix for bug # 2322104 - Luxor Deluxe doesn't fit inside Trevor's Sandy Shores Hangar. The vehicle's back wing hits the ceiling and can spawn with the front wheel in the air.
				IF eName = VEHGEN_WEB_HANGAR_TREVOR
					IF sData.model = MILJET
					OR sData.model = BESRA
					OR sData.model = LUXOR
					OR sData.model = SHAMAL
					OR sData.model = TITAN
					OR sData.model = LUXOR2
						sData.coords 	= <<1678.8, 3229.6, 41.8>>
						sData.heading 	= 106.0906
					ENDIF
				ENDIF
			#ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobals.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex], <<0,0,0>>)
				sData.coords = g_savedGlobals.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex]
			ENDIF
			IF g_savedGlobals.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex] != -1.0
				sData.heading = g_savedGlobals.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex]
			ENDIF
		#endif	
		#endif
	ENDIF
	
	#if USE_CLF_DLC
	// Update coords if we have override.
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][sData.ped], <<0,0,0>>)
				sData.coords = g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][sData.ped]
				sData.heading = g_savedGlobalsClifford.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][sData.ped]
			ENDIF
		ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][sData.ped], <<0,0,0>>)
				sData.coords = g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][sData.ped]
				sData.heading = g_savedGlobalsClifford.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][sData.ped]
			ENDIF
		ENDIF
	#endif
	#if USE_NRM_DLC
	// Update coords if we have override.
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][sData.ped], <<0,0,0>>)
				sData.coords = g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][sData.ped]
				sData.heading = g_savedGlobalsnorman.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][sData.ped]
			ENDIF
		ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][sData.ped], <<0,0,0>>)
				sData.coords = g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][sData.ped]
				sData.heading = g_savedGlobalsnorman.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][sData.ped]
			ENDIF
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Update coords if we have override.
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][sData.ped], <<0,0,0>>)
				sData.coords = g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][sData.ped]
				sData.heading = g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][sData.ped]
			ENDIF
		ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			IF NOT ARE_VECTORS_EQUAL(g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][sData.ped], <<0,0,0>>)
				sData.coords = g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][sData.ped]
				sData.heading = g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][sData.ped]
			ENDIF
		ENDIF
	#endif	
	#endif
	
	RETURN bDataFound
ENDFUNC

/// PURPOSE: Fills the struct with all the vehicle gen data
FUNC BOOL GET_PURCHASABLE_GARAGE_DATA(PURCHASABLE_GARAGE_DATA_STRUCT &sData, VEHICLE_GEN_NAME_ENUM eName)

	// Note: The VEHICLE_GEN_NAME_ENUM param must have the save garage flag set
	sData.bDataSet = FALSE
	sData.bWarpToGarage = FALSE
	sData.tl15BlipLabel = ""
	
	SWITCH eName
		/////////////////////////////////////////////////////////////////
		///      HANGARS
		///      
		CASE VEHGEN_WEB_HANGAR_MICHAEL
			sData.vBuyPos1 = <<-952.882324,-2795.896484,12.132326>>
			sData.vBuyPos2 = <<-966.730835,-2787.974854,16.132324>> 
			sData.fBuyWidth = 17.562500
			
			sData.vPlayerCoords = <<-966.1285, -2794.7891, 12.9648>>
			sData.fPlayerHeading = 146.6324
			
			sData.mPans1.mStart.vPos = <<-1652.4537, -3059.1494, 24.9320>>
			sData.mPans1.mStart.vRot = <<-2.5815, 0.0000, -176.2374>>
			sData.mPans1.fFov = 38.2303
			
			sData.mPans1.mEnd.vPos = <<-1651.5469, -3060.4783, 23.8077>>
			sData.mPans1.mEnd.vRot = <<-2.5815, 0.0000, -176.2374>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-1650.4305, -3177.2292, 16.9898>>
			sData.mPans2.mStart.vRot = <<-4.9147, -0.0000, -11.6415>>
			sData.mPans2.fFov = 38.2303
			
			sData.mPans2.mEnd.vPos = <<-1647.4950, -3173.7278, 16.6439>>
			sData.mPans2.mEnd.vRot = <<-4.9147, -0.0000, -11.6415>>
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vMenuPos1 = <<-1625.910522,-3167.555664,11.114552>>
			sData.vMenuPos2 = <<-1688.580078,-3130.740723,20.135382>> 
			sData.fMenuWidth = 60.187500
			
			sData.vStorePos1 = <<-1625.910522,-3167.555664,11.114552>>
			sData.vStorePos2 = <<-1688.580078,-3130.740723,20.135382>> 
			sData.fStoreWidth = 60.187500
			
			sData.vBlipCoords = <<-966.0840, -2793.6130, 12.9648>>
			sData.eBlipSprite = RADAR_TRACE_HANGAR_FOR_SALE
			sData.tl15BlipLabel = "HANGAR_NAME"
			
			sData.iCost = 1378600
			sData.tl15HelpText = "HANGAR"
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_HANGAR_FRANKLIN
			sData.vBuyPos1 = <<-952.882324,-2795.896484,12.132326>>
			sData.vBuyPos2 = <<-966.730835,-2787.974854,16.132324>> 
			sData.fBuyWidth = 17.562500
			
			sData.vPlayerCoords = <<-966.1285, -2794.7891, 12.9648>>
			sData.fPlayerHeading = 146.6324
			
			sData.mPans1.mStart.vPos = <<-1184.2, -3345, 17.5>>
			sData.mPans1.mStart.vRot = <<3.5, 0, 109>>   
			sData.mPans1.fFov = 30.3
			
			sData.mPans1.mEnd.vPos = <<-1184.3, -3345, 16.6>>
			sData.mPans1.mEnd.vRot = <<3.5, 0, 109>>   
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-1272.6, -3414.7, 15.6>>
			sData.mPans2.mStart.vRot = <<-2.1, 0, -9.9>>
			sData.mPans2.fFov = 37.8
			
			sData.mPans2.mEnd.vPos = <<-1274, -3414.4, 15.6>>
			sData.mPans2.mEnd.vRot = <<-2.1, 0, -9.9>>
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vMenuPos1 = <<-1247.687744,-3408.546387,20.504175>>
			sData.vMenuPos2 = <<-1309.774048,-3372.294434,11.368781>> 
			sData.fMenuWidth = 66.187500
			
			sData.vStorePos1 = <<-1247.687744,-3408.546387,20.504175>>
			sData.vStorePos2 = <<-1309.774048,-3372.294434,11.368781>> 
			sData.fStoreWidth = 66.187500
			
			sData.vBlipCoords = <<-966.0840, -2793.6130, 12.9648>>
			sData.eBlipSprite = RADAR_TRACE_HANGAR_FOR_SALE
			sData.tl15BlipLabel = "HANGAR_NAME"
			
			sData.iCost = 1378600
			sData.tl15HelpText = "HANGAR"
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_HANGAR_TREVOR
			sData.vBuyPos1 = <<1727.301758,3291.453125,39.619114>>
			sData.vBuyPos2 = <<1744.107666,3296.214600,44.171993>> 
			sData.fBuyWidth = 4.687500
			
			sData.vPlayerCoords = <<1737.5295, 3289.2388, 40.1448>>
			sData.fPlayerHeading = 14.8763
			
			sData.mPans1.mStart.vPos = <<1739.7485, 3280.4453, 45.8124>>
			sData.mPans1.mStart.vRot = <<-12.3854, -0.0000, 14.8379>>
			sData.mPans1.fFov = 50.0
			
			sData.mPans1.mEnd.vPos = <<1739.7485, 3280.4453, 45.8124>>
			sData.mPans1.mEnd.vRot = <<-12.3854, -0.0000, 14.8379>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<1739.7485, 3280.4453, 45.8124>>
			sData.mPans2.mStart.vRot = <<-12.3854, -0.0000, 14.8379>>
			sData.mPans2.fFov = 50.0
			
			sData.mPans2.mEnd.vPos = <<1739.7485, 3280.4453, 45.8124>>
			sData.mPans2.mEnd.vRot = <<-12.3854, -0.0000, 14.8379>>
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vMenuPos1 = <<1718.055542,3305.018799,45.709221>>
			sData.vMenuPos2 = <<1745.705933,3313.101074,39.757996>> 
			sData.fMenuWidth = 28.125000
			
			sData.vStorePos1 = <<1718.055542,3305.018799,45.709221>>
			sData.vStorePos2 = <<1745.705933,3313.101074,39.757996>> 
			sData.fStoreWidth = 28.125000
			
			sData.vBlipCoords = <<1735.5863, 3294.5305, 40.1651>>
			sData.eBlipSprite = RADAR_TRACE_HANGAR_FOR_SALE
			sData.tl15BlipLabel = "HANGAR_NAME2"
			
			sData.iCost = 1378600
			sData.tl15HelpText = "HANGAR"
			
			sData.bDataSet = TRUE
		BREAK
		
		/////////////////////////////////////////////////////////////////
		///      HELIPADS
		///      
		CASE VEHGEN_WEB_HELIPAD_MICHAEL
			sData.vBuyPos1 = <<-709.094971,-1414.229492,3.188014>>
			sData.vBuyPos2 = <<-712.122986,-1411.654541,6.938014>> 
			sData.fBuyWidth = 3.250000
			
			sData.vMenuPos1 = <<-739.153076,-1439.509277,3.188024>>
			sData.vMenuPos2 = <<-753.779175,-1426.755615,7.188024>> 
			sData.fMenuWidth = 17.562500
			
			sData.vPlayerCoords = <<-700.9434, -1418.1694, 4.0005>>
			sData.fPlayerHeading = 142.6382
			
			sData.mPans1.mStart.vPos = <<-738, -1440, 6.3>>
			sData.mPans1.mStart.vRot = <<-2.5, 0, 48.3>>
			sData.mPans1.fFov = 45
			
			sData.mPans1.mEnd.vPos = <<-738, -1440, 6>>
			sData.mPans1.mEnd.vRot = <<-2.5, 0, 48.3>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-749.3, -1425.7, 5.9>>
			sData.mPans2.mStart.vRot = <<-9.9, 0.2, -152.8>>
			sData.mPans2.fFov = 40
			 
			sData.mPans2.mEnd.vPos = <<-749.9, -1426, 5.9>>
			sData.mPans2.mEnd.vRot = <<-9.9, 0.2, -152.8>>
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vStorePos1 = <<-738.779053,-1439.377197,9.125515>>
			sData.vStorePos2 = <<-755.911072,-1425.005859,1.688014>> 
			sData.fStoreWidth = 18.062500
			
			sData.vBlipCoords = <<-708.48, -1414.66, 4.00>>
			sData.eBlipSprite = RADAR_TRACE_HELIPAD_FOR_SALE
			sData.tl15BlipLabel = "HELIPAD_NAME"
			
			sData.iCost = 419850
			sData.tl15HelpText = "HELIPAD"
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_HELIPAD_FRANKLIN
			sData.vBuyPos1 = <<-709.094971,-1414.229492,3.188014>>
			sData.vBuyPos2 = <<-712.122986,-1411.654541,6.938014>> 
			sData.fBuyWidth = 3.250000
			
			sData.vMenuPos1 = <<-755.681152,-1459.233521,3.188023>>
			sData.vMenuPos2 = <<-770.767090,-1446.866577,7.188024>> 
			sData.fMenuWidth = 17.562500
			
			sData.vPlayerCoords = <<-700.9434, -1418.1694, 4.0005>>
			sData.fPlayerHeading = 142.6382
			
			sData.mPans1.mStart.vPos = <<-754.6, -1460.5, 6.4>>
			sData.mPans1.mStart.vRot = <<-4.8, 0, 48.5>>
			sData.mPans1.fFov = 45
			
			sData.mPans1.mEnd.vPos = <<-754.6, -1460.6, 6.1>>
			sData.mPans1.mEnd.vRot = <<-2.2, 0, 48.5>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-771.8, -1452.6, 5.7>>
			sData.mPans2.mStart.vRot = <<-4.1, 0, -98.6>>
			sData.mPans2.fFov = 40.0
			
			sData.mPans2.mEnd.vPos = <<-771.8, -1452.1, 5.7>>
			sData.mPans2.mEnd.vRot = <<-4.1, 0, -98.6>>
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vStorePos1 = <<-755.571533,-1459.453003,9.125514>>
			sData.vStorePos2 = <<-771.420410,-1446.234741,1.688017>> 
			sData.fStoreWidth = 18.062500
			
			sData.vBlipCoords = <<-708.48, -1414.66, 4.00>>
			sData.eBlipSprite = RADAR_TRACE_HELIPAD_FOR_SALE
			sData.tl15BlipLabel = "HELIPAD_NAME"
			
			sData.iCost = 419850
			sData.tl15HelpText = "HELIPAD"
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY
			sData.vBuyPos1 = <<1763.424438,3238.282227,40.320221>>
			sData.vBuyPos2 = <<1786.588745,3239.015869,47.185337>> 
			sData.fBuyWidth = 17.750000
			
			sData.vMenuPos1 = <<1763.424438,3238.282227,40.320221>>
			sData.vMenuPos2 = <<1786.588745,3239.015869,47.185337>> 
			sData.fMenuWidth = 17.750000
			
			sData.vPlayerCoords = <<1761.1222, 3250.1250, 40.7330>>
			sData.fPlayerHeading = 236.5858
			
			sData.mPans1.mStart.vPos = <<1779.2452, 3222.4773, 48.5795>>
			sData.mPans1.mStart.vRot = <<-31.2473, 0.0000, 7.2108>>
			sData.mPans1.fFov = 40
			
			sData.mPans1.mEnd.vPos = <<1779.2452, 3222.4773, 48.5795>>
			sData.mPans1.mEnd.vRot = <<-31.2473, 0.0000, 7.2108>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<1779.2452, 3222.4773, 48.5795>>
			sData.mPans2.mStart.vRot = <<-31.2473, 0.0000, 7.2108>>
			sData.mPans2.fFov = 40
			
			sData.mPans2.mEnd.vPos = <<1779.2452, 3222.4773, 48.5795>>
			sData.mPans2.mEnd.vRot = <<-31.2473, 0.0000, 7.2108>>
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vStorePos1 = <<1802.326782,3245.164551,46.955437>>
			sData.vStorePos2 = <<1755.039673,3237.170410,38.693703>> 
			sData.fStoreWidth = 28.125000
			
			sData.vBlipCoords = <<1769.3, 3244.0, 41.1>>
			sData.eBlipSprite = RADAR_TRACE_HELIPAD_FOR_SALE
			sData.tl15BlipLabel = "HELIPAD_NAME2"
			
			sData.iCost = 0
			sData.tl15HelpText = "HELIPAD"
			
			sData.bDataSet = TRUE
		BREAK
		
		/////////////////////////////////////////////////////////////////
		///      MARINAS
		///      
		CASE VEHGEN_WEB_MARINA_MICHAEL
			sData.vBuyPos1 = <<-826.575867,-1368.474976,3.750513>>
			sData.vBuyPos2 = <<-827.867981,-1364.866943,6.750514>> 
			sData.fBuyWidth = 3.312500
			
			sData.vMenuPos1 = <<-845.221558,-1356.039917,-0.212093>>
			sData.vMenuPos2 = <<-841.645142,-1365.776733,3.787907>> 
			sData.fMenuWidth = 17.562500
			
			sData.vPlayerCoords = <<-835.8600, -1358.3470, 0.6102>>
			sData.fPlayerHeading = 112.3787
			
			sData.mPans1.mStart.vPos = <<-859.6, -1374.0, 4.3>>
			sData.mPans1.mStart.vRot = <<-3, 0.0, -48.3>>  
			sData.mPans1.fFov = 32.9
			
			sData.mPans1.mEnd.vPos = <<-859.7, -1374.0, 3.0>>
			sData.mPans1.mEnd.vRot = <<-3, 0.0, -48.3>>  
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-837.2, -1350.3, 2.4>>
			sData.mPans2.mStart.vRot = <<-7, 0, 156.1>>  
			sData.mPans2.fFov = 47.5
			
			sData.mPans2.mEnd.vPos = <<-837.4, -1350.8, 2.3>>
			sData.mPans2.mEnd.vRot = <<-7, 0, 156.1>>  
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vStorePos1 = <<-860.069275,-1314.887085,-3.331177>>
			sData.vStorePos2 = <<-826.646057,-1410.446777,4.787533>> 
			sData.fStoreWidth = 19.625000
			
			sData.vBlipCoords 	= <<-827.9120, -1366.7362, 4.0005>>
			sData.eBlipSprite = RADAR_TRACE_DOCK_FOR_SALE
			sData.tl15BlipLabel = "MARINA_NAME"
			
			sData.iCost = 75000
			sData.tl15HelpText = "MARINA"
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_MARINA_FRANKLIN
			sData.vBuyPos1 = <<-826.575867,-1368.474976,3.750513>>
			sData.vBuyPos2 = <<-827.867981,-1364.866943,6.750514>> 
			sData.fBuyWidth = 3.312500
			
			sData.vMenuPos1 = <<-848.817749,-1347.785767,-0.212093>>
			sData.vMenuPos2 = <<-845.248230,-1357.935791,3.787908>> 
			sData.fMenuWidth = 17.562500
			
			sData.vPlayerCoords = <<-839.2461, -1349.5249, 0.6102>>
			sData.fPlayerHeading = 110.6762
			
			sData.mPans1.mStart.vPos = <<-866.1, -1365.6, 4.3>>
			sData.mPans1.mStart.vRot = <<-3.6, 0.0, -54.6>>
			sData.mPans1.fFov = 30.0
			
			sData.mPans1.mEnd.vPos = <<-866.1, -1365.6, 3.0>>
			sData.mPans1.mEnd.vRot = <<-3.6, 0.0, -54.6>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-838.7, -1343.3, 2.9>>
			sData.mPans2.mStart.vRot = <<-7.5, 0, 142.6>>  
			sData.mPans2.fFov = 47.5
			
			sData.mPans2.mEnd.vPos = <<-838.8, -1343.2, 2.9>>
			sData.mPans2.mEnd.vRot = <<-7.5, 0, 142.6>>  
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vStorePos1 = <<-860.069275,-1314.887085,-3.331177>>
			sData.vStorePos2 = <<-826.646057,-1410.446777,4.787533>> 
			sData.fStoreWidth = 19.625000
			
			sData.vBlipCoords 	= <<-827.9120, -1366.7362, 4.0005>>
			sData.eBlipSprite = RADAR_TRACE_DOCK_FOR_SALE
			sData.tl15BlipLabel = "MARINA_NAME"
			
			sData.iCost = 75000
			sData.tl15HelpText = "MARINA"
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_MARINA_TREVOR
			sData.vBuyPos1 = <<-826.575867,-1368.474976,3.750513>>
			sData.vBuyPos2 = <<-827.867981,-1364.866943,6.750514>> 
			sData.fBuyWidth = 3.312500
			
			sData.vMenuPos1 = <<-851.698669,-1339.127808,-0.212129>>
			sData.vMenuPos2 = <<-848.127991,-1349.168091,3.787905>> 
			sData.fMenuWidth = 17.562500
			
			sData.vPlayerCoords = <<-842.0763, -1341.4015, 0.6102>>
			sData.fPlayerHeading = 109.8916
			
			sData.mPans1.mStart.vPos = <<-866.3, -1357.9, 4.3>>
			sData.mPans1.mStart.vRot = <<-4.4, 0.0, -46.6>>
			sData.mPans1.fFov = 31.2
			
			sData.mPans1.mEnd.vPos = <<-866.3, -1357.9, 3.0>>
			sData.mPans1.mEnd.vRot = <<-4.4, 0.0, -46.6>>
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7500/1000
			
			sData.mPans2.mStart.vPos = <<-841.6, -1336.3, 2.5>>
			sData.mPans2.mStart.vRot = <<-3.6, 0, 137.7>>  
			sData.mPans2.fFov = 47.5
			
			sData.mPans2.mEnd.vPos = <<-841.8, -1336.1, 2.5>>
			sData.mPans2.mEnd.vRot = <<-3.6, 0, 137.7>> 
			sData.mPans2.fShake = 0.2
			sData.mPans1.fDuration = 11500/1000
			
			sData.vStorePos1 = <<-860.069275,-1314.887085,-3.331177>>
			sData.vStorePos2 = <<-826.646057,-1410.446777,4.787533>> 
			sData.fStoreWidth = 19.625000
			
			sData.vBlipCoords 	= <<-827.9120, -1366.7362, 4.0005>>
			sData.eBlipSprite	= RADAR_TRACE_DOCK_FOR_SALE
			sData.tl15BlipLabel = "MARINA_NAME"
			
			sData.iCost = 75000
			sData.tl15HelpText = "MARINA"
			
			sData.bDataSet = TRUE
		BREAK
		
		
		/////////////////////////////////////////////////////////////////
		///      CAR
		///      
		CASE VEHGEN_WEB_CAR_MICHAEL
			sData.vBuyPos1 = <<-63.381466,84.059402,70.521385>>
			sData.vBuyPos2 = <<-66.177742,77.959129,74.053719>>
			sData.fBuyWidth = 5.937500
			
			sData.vPlayerCoords = <<-65.2041, 81.0524, 70.5666>>
			sData.fPlayerHeading = 243.8699
			
			sData.vMenuPos1 = <<189.707352,-1017.569031,-104.999962>>
			sData.vMenuPos2 = <<207.832504,-1017.774292,-96.635757>> 
			sData.fMenuWidth = 23.000000
			
			sData.mPans1.mStart.vPos = <<191.0,-1026.9,-98.3>>
			sData.mPans1.mStart.vRot = <<-2.4,0.0,-77.0>>
			sData.mPans1.mEnd.vPos = <<190.0,-1026.9,-98.3>>
			sData.mPans1.mEnd.vRot = <<-4.3,0.0,-57.6>>
			sData.mPans1.fFov = 37.0
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7.500
			
			sData.mPans2.mStart.vPos = <<207.3,-1013.0,-98.2>>
			sData.mPans2.mStart.vRot = <<-2.4,-0.0,112.3>>
			sData.mPans2.mEnd.vPos = <<207.3,-1013.0,-98.2>>
			sData.mPans2.mEnd.vRot = <<-2.4,-0.0,141.0>>
			sData.mPans2.fFov = 37.0
			sData.mPans2.fShake = 0.2
			sData.mPans2.fDuration = 11.500
			
			sData.vStorePos1 = <<221.997864,-981.663452,-100.187424>>
			sData.vStorePos2 = <<234.201019,-981.758240,-97.624924>> 
			sData.fStoreWidth = 6.250000
			
			sData.vBlipCoords 	= <<-62.62, 80.03, 70.62>>
			sData.eBlipSprite	= RADAR_TRACE_GARAGE_FOR_SALE
			sData.tl15BlipLabel = "GARAGE_NAME1"
			
			sData.iCost = 30000
			sData.tl15HelpText = "CAR_GAR"
			
			sData.fDoorHeading = 243.8699
			
			sData.bWarpToGarage = TRUE
			
			sData.vGarageExitCoords[0] = <<-67.9068, 82.2664, 70.5153>>
			sData.fGarageExitHeading[0] = 66.2020
			
			sData.vGarageExitCoords[1] = <<-65.1234, 81.2517, 70.5644>>
			sData.fGarageExitHeading[1] = 71.6237
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_CAR_FRANKLIN
			sData.vBuyPos1 = <<-72.411659,-1824.142090,25.817038>>
			sData.vBuyPos2 = <<-68.702538,-1819.642090,29.379538>>
			sData.fBuyWidth = 5.937500
			
			sData.vPlayerCoords = <<-70.1992, -1823.2252, 25.9420>>
			sData.fPlayerHeading = 46.1535
			
			sData.vMenuPos1 = <<189.707352,-1017.569031,-104.999962>>
			sData.vMenuPos2 = <<207.832504,-1017.774292,-96.635757>> 
			sData.fMenuWidth = 23.000000
			
			sData.mPans1.mStart.vPos = <<191.0,-1026.9,-98.3>>
			sData.mPans1.mStart.vRot = <<-2.4,0.0,-77.0>>
			sData.mPans1.mEnd.vPos = <<190.0,-1026.9,-98.3>>
			sData.mPans1.mEnd.vRot = <<-4.3,0.0,-57.6>>
			sData.mPans1.fFov = 37.0
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7.500
			
			sData.mPans2.mStart.vPos = <<207.3,-1013.0,-98.2>>
			sData.mPans2.mStart.vRot = <<-2.4,-0.0,112.3>>
			sData.mPans2.mEnd.vPos = <<207.3,-1013.0,-98.2>>
			sData.mPans2.mEnd.vRot = <<-2.4,-0.0,141.0>>
			sData.mPans2.fFov = 37.0
			sData.mPans2.fShake = 0.2
			sData.mPans2.fDuration = 11.500
			
			sData.vStorePos1 = <<221.997864,-981.663452,-100.187424>>
			sData.vStorePos2 = <<234.201019,-981.758240,-97.624924>> 
			sData.fStoreWidth = 6.250000
			
			sData.vBlipCoords 	= <<-71.29, -1821.68, 25.94>>
			sData.eBlipSprite	= RADAR_TRACE_GARAGE_FOR_SALE
			sData.tl15BlipLabel = "GARAGE_NAME2"
			
			sData.iCost = 30000
			sData.tl15HelpText = "CAR_GAR"
			
			sData.fDoorHeading = 53.0985
			
			sData.bWarpToGarage = TRUE
			
			sData.vGarageExitCoords[0] = <<-64.2268, -1832.5975, 25.8666>>
			sData.fGarageExitHeading[0] = 274.6339
			
			sData.vGarageExitCoords[1] = <<-68.5531, -1824.3774, 25.9424>>
			sData.fGarageExitHeading[1] = 215.8295
			
			sData.bDataSet = TRUE
		BREAK
		CASE VEHGEN_WEB_CAR_TREVOR
			sData.vBuyPos1 = <<-220.779358,-1159.279785,21.903023>>
			sData.vBuyPos2 = <<-220.727295,-1165.265259,25.450535>>
			sData.fBuyWidth = 5.937500
			
			sData.vPlayerCoords = <<-220.7592, -1162.2775, 22.0242>>
			sData.fPlayerHeading = 271.2097
			
			sData.vMenuPos1 = <<189.707352,-1017.569031,-104.999962>>
			sData.vMenuPos2 = <<207.832504,-1017.774292,-96.635757>> 
			sData.fMenuWidth = 23.000000
			
			sData.mPans1.mStart.vPos = <<191.0,-1026.9,-98.3>>
			sData.mPans1.mStart.vRot = <<-2.4,0.0,-77.0>>
			sData.mPans1.mEnd.vPos = <<190.0,-1026.9,-98.3>>
			sData.mPans1.mEnd.vRot = <<-4.3,0.0,-57.6>>
			sData.mPans1.fFov = 37.0
			sData.mPans1.fShake = 0.2
			sData.mPans1.fDuration = 7.500
			
			sData.mPans2.mStart.vPos = <<207.3,-1013.0,-98.2>>
			sData.mPans2.mStart.vRot = <<-2.4,-0.0,112.3>>
			sData.mPans2.mEnd.vPos = <<207.3,-1013.0,-98.2>>
			sData.mPans2.mEnd.vRot = <<-2.4,-0.0,141.0>>
			sData.mPans2.fFov = 37.0
			sData.mPans2.fShake = 0.2
			sData.mPans2.fDuration = 11.500
			
			sData.vStorePos1 = <<221.997864,-981.663452,-100.187424>>
			sData.vStorePos2 = <<234.201019,-981.758240,-97.624924>> 
			sData.fStoreWidth = 6.250000
			
			sData.vBlipCoords 	= <<-218.35, -1162.18, 22.02>>
			sData.eBlipSprite	= RADAR_TRACE_GARAGE_FOR_SALE
			sData.tl15BlipLabel = "GARAGE_NAME3"
			
			sData.iCost = 30000
			sData.tl15HelpText = "CAR_GAR"
			
			sData.fDoorHeading = 271.2097
			
			sData.bWarpToGarage = TRUE
			
			sData.vGarageExitCoords[0] = <<-222.1935, -1162.1134, 22.0204>>
			sData.fGarageExitHeading[0] = 358.5703
			
			sData.vGarageExitCoords[1] = <<-220.8189, -1162.3016, 22.0242>>
			sData.fGarageExitHeading[1] = 70.2711
			
			sData.bDataSet = TRUE
		BREAK
		
		
	ENDSWITCH

	RETURN sData.bDataSet
ENDFUNC

/// PURPOSE: Sets the availability of the specified vehicle gen
PROC SET_VEHICLE_GEN_AVAILABLE(VEHICLE_GEN_NAME_ENUM eName, BOOL bAvailable)
	IF eName = VEHGEN_NONE
		EXIT
	ENDIF
	
	IF bAvailable
		IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
		
			#IF IS_DEBUG_BUILD
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					GET_VEHICLE_GEN_DATA(g_sVehicleGenNSData.sRuntimeStruct[0], eName)
					PRINTLN("SET_VEHICLE_GEN_AVAILABLE(", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name, ", TRUE) called by ", GET_THIS_SCRIPT_NAME())
				ENDIF
			#ENDIF
		
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_HELP_PROCESSED, FALSE)
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_BLIP_PROCESSED, FALSE)
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_TXT_PROCESSED, FALSE)
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_PLAYER_USED_VEH, FALSE)
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE, TRUE)
			
			// If a vehicle gen is activated when the player is near, it will be created instantly.
			// To prevent this pop we need to wait for the player to leave the area.
			// If there are any special cases where this behaviour is unsuitable then 
			// CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(eVehicleGen) should be used.
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		ENDIF
	ELSE
	
		#IF IS_DEBUG_BUILD
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				GET_VEHICLE_GEN_DATA(g_sVehicleGenNSData.sRuntimeStruct[0], eName)
				PRINTLN("SET_VEHICLE_GEN_AVAILABLE(", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name, ", FALSE) called by ", GET_THIS_SCRIPT_NAME())
			ENDIF
		#ENDIF
	
		SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE, FALSE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the position of a given vehicle gen 
FUNC VECTOR GET_VEHICLE_GEN_COORDS(VEHICLE_GEN_NAME_ENUM eVehicleGen)
	IF eVehicleGen = VEHGEN_NONE
		RETURN <<0, 0, 0>>
	ENDIF
	
	IF NOT GET_VEHICLE_GEN_DATA(g_sVehicleGenNSData.sRuntimeStruct[0], eVehicleGen)
		RETURN <<0, 0, 0>>
	ENDIF
	
	RETURN g_sVehicleGenNSData.sRuntimeStruct[0].coords
ENDFUNC

/// PURPOSE:
///    Gets the heading of a given vehicle gen 
FUNC FLOAT GET_VEHICLE_GEN_HEADING(VEHICLE_GEN_NAME_ENUM eVehicleGen)
	IF eVehicleGen = VEHGEN_NONE
		RETURN 0.0
	ENDIF
	
	IF NOT GET_VEHICLE_GEN_DATA(g_sVehicleGenNSData.sRuntimeStruct[0], eVehicleGen)
		RETURN 0.0
	ENDIF
	
	RETURN g_sVehicleGenNSData.sRuntimeStruct[0].heading
ENDFUNC

/// PURPOSE:
///    Gets the saved vehicle setup data for the vehicle gen 
FUNC BOOL GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(VEHICLE_SETUP_STRUCT &sVehicleSetupData, VEHICLE_GEN_NAME_ENUM eVehicleGen)
	
	IF eVehicleGen = VEHGEN_NONE
		RETURN FALSE
	ENDIF
	
	IF NOT GET_VEHICLE_GEN_DATA(g_sVehicleGenNSData.sRuntimeStruct[0], eVehicleGen)
		RETURN FALSE
	ELSE
		#if USE_CLF_DLC
			CLONE_VEHICLE_SETUP_STRUCT(g_savedGlobalsClifford.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex], sVehicleSetupData)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "<clf>GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA() - Getting this data for this - ", GET_THIS_SCRIPT_NAME())
				CPRINTLN(DEBUG_MISSION, "...vehicle gen = ", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name)
				CPRINTLN(DEBUG_MISSION, "...vehicle model = ", GET_MODEL_NAME_FOR_DEBUG(g_savedGlobalsClifford.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex].eModel))
				CPRINTLN(DEBUG_MISSION, "...dynamic slot = ", g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex)
				CPRINTLN(DEBUG_MISSION, "...dynamic coords = ", g_savedGlobalsClifford.sVehicleGenData.vDynamicCoords[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex])
				CPRINTLN(DEBUG_MISSION, "...dynamic heading = ", g_savedGlobalsClifford.sVehicleGenData.fDynamicHeading[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex])
				CPRINTLN(DEBUG_MISSION, "...Set by script = ", GET_THIS_SCRIPT_NAME())
			#ENDIF
			CPRINTLN(DEBUG_MISSION, "<clf>Cloned vehicle gen date for  ", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name, " called by - ", GET_THIS_SCRIPT_NAME())
		#endif
		
		#if USE_NRM_DLC
			CLONE_VEHICLE_SETUP_STRUCT(g_savedGlobalsnorman.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex], sVehicleSetupData)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "<clf>GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA() - Getting this data for this - ", GET_THIS_SCRIPT_NAME())
				CPRINTLN(DEBUG_MISSION, "...vehicle gen = ", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name)
				CPRINTLN(DEBUG_MISSION, "...vehicle model = ", GET_MODEL_NAME_FOR_DEBUG(g_savedGlobalsnorman.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex].eModel))
				CPRINTLN(DEBUG_MISSION, "...dynamic slot = ", g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex)
				CPRINTLN(DEBUG_MISSION, "...dynamic coords = ", g_savedGlobalsnorman.sVehicleGenData.vDynamicCoords[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex])
				CPRINTLN(DEBUG_MISSION, "...dynamic heading = ", g_savedGlobalsnorman.sVehicleGenData.fDynamicHeading[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex])
				CPRINTLN(DEBUG_MISSION, "...Set by script = ", GET_THIS_SCRIPT_NAME())
			#ENDIF
			CPRINTLN(DEBUG_MISSION, "<clf>Cloned vehicle gen date for  ", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name, " called by - ", GET_THIS_SCRIPT_NAME())
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			CLONE_VEHICLE_SETUP_STRUCT(g_savedGlobals.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex], sVehicleSetupData)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA() - Getting this data for this - ", GET_THIS_SCRIPT_NAME())				
				CPRINTLN(DEBUG_MISSION, "...vehicle gen = ", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name)
				if g_savedGlobals.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex].eModel != DUMMY_MODEL_FOR_SCRIPT
					CPRINTLN(DEBUG_MISSION, "...vehicle model = ", GET_MODEL_NAME_FOR_DEBUG(g_savedGlobals.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex].eModel))
				else
					CPRINTLN(DEBUG_MISSION, "...vehicle model = DUMMY_MODEL_FOR_SCRIPT ")
				endif
				CPRINTLN(DEBUG_MISSION, "...dynamic slot = ", g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex)
				CPRINTLN(DEBUG_MISSION, "...dynamic coords = ", g_savedGlobals.sVehicleGenData.vDynamicCoords[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex])
				CPRINTLN(DEBUG_MISSION, "...dynamic heading = ", g_savedGlobals.sVehicleGenData.fDynamicHeading[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex])
				CPRINTLN(DEBUG_MISSION, "...Set by script = ", GET_THIS_SCRIPT_NAME())
			#ENDIF
			CPRINTLN(DEBUG_MISSION, "Cloned vehicle gen date for  ", g_sVehicleGenNSData.sRuntimeStruct[0].dbg_name, " called by - ", GET_THIS_SCRIPT_NAME())
		#endif
		#endif
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the height to anchor a vehicle gen	
FUNC FLOAT GET_ANCHOR_HEIGHT_FOR_VEHICLE_GEN(VEHICLE_GEN_NAME_ENUM eName)
	SWITCH eName
		CASE VEHGEN_PROPERTY_MARINA_SUB
			RETURN -0.7
		BREAK
		CASE VEHGEN_WILDPHOTO_SUB
			RETURN -0.8
		BREAK
	ENDSWITCH

	RETURN 0.0
ENDFUNC

