
//////////////////////////////////////////////////////////////////////
/* shrink_phone.sch													*/
/* Author: DJ Jones, Yomal Perera									*/
/* Functionality for shrink phone sessions.							*/
//////////////////////////////////////////////////////////////////////

// Script entry point.
SCRIPT
	INT iCurrentLine
	BOOL bBroke, bPlayBrokeConversation, bReachedDecision
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		SHRINK_PHONE_CLEANUP()
	ENDIF
	
	thisSession = SHRINK_GET_CURRENT_MISSION_SESSION()
	SHRINK_GET_SESSION_PROPERTIES(sessionProperties, thisSession)
	
	// See if we have enough money.
	bBroke = GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL) < SHRINK_GET_SESSION_FEE(thisSession)
	
	// Main loop.
	WHILE TRUE
		WAIT(0)
		
		// Check debug commands.
		#IF IS_DEBUG_BUILD
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FLOW_MISSION_FAILED()
				SHRINK_PHONE_CLEANUP()
			ENDIF
		#ENDIF
		
		// Shrink state machine.
		SWITCH phoneState
			// Perform init.
			CASE SHRINKPHONESTATE_INIT
				IF bBroke
					IF SHRINK_PHONE_INIT_BROKE(convPeds, sRoots, sSpecifics, iLineCounter)
						POINT_STRINGS_AT_TEXT_LABEL_15(sRootStrings, sRoots, iLineCounter)
						POINT_STRINGS_AT_TEXT_LABEL_15(sSpecificStrings, sSpecifics, iLineCounter)
						bPlayBrokeConversation = TRUE
					ENDIF
				ELSE
					SHRINK_PHONE_INIT(convPeds, sRoots, sSpecifics, iLineCounter, sYesRoots, sYesSpecifics, iYesLineCounter,
						sNoRoots, sNoSpecifics, iNoLineCounter, sessionProperties, thisSession)
					
					g_iShrinkBrokeConvTimestamp = 0
					
					POINT_STRINGS_AT_TEXT_LABEL_15(sRootStrings, sRoots, iLineCounter)
					POINT_STRINGS_AT_TEXT_LABEL_15(sSpecificStrings, sSpecifics, iLineCounter)
					POINT_STRINGS_AT_TEXT_LABEL_15(sYesRootStrings, sYesRoots, iYesLineCounter)
					POINT_STRINGS_AT_TEXT_LABEL_15(sYesSpecificStrings, sYesSpecifics, iYesLineCounter)
					POINT_STRINGS_AT_TEXT_LABEL_15(sNoRootStrings, sNoRoots, iNoLineCounter)
					POINT_STRINGS_AT_TEXT_LABEL_15(sNoSpecificStrings, sNoSpecifics, iNoLineCounter)
					
					bReachedDecision = FALSE
				ENDIF
				
				phoneState = SHRINKPHONESTATE_START_CONV
			BREAK
			
			// Start the phone conversation.
			CASE SHRINKPHONESTATE_START_CONV
				IF bBroke
					IF bPlayBrokeConversation
						PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES(iLineCounter, convPeds, CHAR_DR_FRIEDLANDER, "DrfAud", sRootStrings, sSpecificStrings, CONV_PRIORITY_VERY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
						phoneState = SHRINKPHONESTATE_CONV
					ELSE
						phoneState = SHRINKPHONESTATE_BUSY
					ENDIF
				ELIF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES_REQUIRES_RESPONSE(iLineCounter, convPeds, CHAR_DR_FRIEDLANDER, "DrfAud", sRootStrings, sSpecificStrings, "SHRINK_CELL_Q", CONV_PRIORITY_VERY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
					SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES(iYesLineCounter, iNoLineCounter, sYesRootStrings, sYesSpecificStrings, sNoRootStrings, sNoSpecificStrings)
					phoneState = SHRINKPHONESTATE_CONV
				ENDIF
			BREAK
			
			// Wait for the conversation to end (voicemail/busy signal).
			CASE SHRINKPHONESTATE_BUSY
				IF NOT CAN_PHONE_BE_SEEN_ON_SCREEN()
					WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
						WAIT(0)
					ENDWHILE
					
					PRINT_HELP("SHRINK_BROKE")
					phoneState = SHRINKPHONESTATE_FAIL
				ENDIF
			BREAK
			
			// Wait for the phone conversation to end.
			CASE SHRINKPHONESTATE_CONV
				IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > iCurrentLine
					iCurrentLine = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
				ENDIF
				
				 IF IS_CELLPHONE_CALL_WITH_REPLIES_WAITING_ON_USER_INPUT()
					START_TIMER_NOW(timerReply)
					bReachedDecision = TRUE
					iCurrentLine = -1
					IF TIMER_DO_ONCE_WHEN_READY(timerReply, 30.0)
						FORCE_NEGATIVE_RESPONSE_TO_CALL_WITH_REPLIES (TRUE)
//					ELSE
//					 	IF NOT IS_TIMER_STARTED(timerReplyPrompt)
//							START_TIMER_NOW(timerReplyPrompt)
//						ELIF GET_TIMER_IN_SECONDS(timerReplyPrompt) > 10.5
//							Trigger prompt line here
//							RESTART_TIMER_NOW(timerReplyPrompt)
//						ENDIF
					ENDIF
				 ENDIF
				 
				 IF IS_PED_JUMPING_OUT_OF_VEHICLE(PLAYER_PED_ID())
				 OR IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				 	HANG_UP_AND_PUT_AWAY_PHONE()
				 ENDIF
				
				//PRINTLN("Current shrink line ", iCurrentLine)
				
				IF HAS_CELLPHONE_CALL_FINISHED()
					IF (NOT bBroke)
					AND bReachedDecision
					AND ((SHRINK_GET_CURRENT_MISSION_SESSION() = SHRINKSESSION_PHONE_NEGATIVITY AND iCurrentLine >= 8)
					OR (SHRINK_GET_CURRENT_MISSION_SESSION() = SHRINKSESSION_PHONE_FUCKED_UP AND iCurrentLine >= 10))
						phoneState = SHRINKPHONESTATE_PASS
					ELSE
						IF bBroke
							WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
								WAIT(0)
							ENDWHILE
							PRINT_HELP("SHRINK_BROKE")
						ENDIF
						phoneState = SHRINKPHONESTATE_FAIL
					ENDIF
				ENDIF
			BREAK
			
			// Call completed successfully or was interrupted late.
			CASE SHRINKPHONESTATE_PASS
				DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_SHRINK, SHRINK_GET_SESSION_FEE(thisSession))
				MISSION_FLOW_MISSION_PASSED()
				SHRINK_PHONE_CLEANUP()			
			BREAK
			
			// Call was interrupted too early, or we were broke.
			CASE SHRINKPHONESTATE_FAIL
				IF (NOT bBroke) OR (NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHRINK_BROKE"))
					MISSION_FLOW_MISSION_FAILED()
					SHRINK_PHONE_CLEANUP()
				ENDIF
			BREAK
		ENDSWITCH
	ENDWHILE
ENDSCRIPT
