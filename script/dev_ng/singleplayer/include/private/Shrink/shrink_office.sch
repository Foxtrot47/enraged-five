
//////////////////////////////////////////////////////////////////////
/* shrink_office.sch												*/
/* Author: DJ Jones, Yomal Perera									*/
/* Functionality for shrink office sessions.						*/
//////////////////////////////////////////////////////////////////////


USING "flow_public_game.sch"
USING "clubs_public.sch"
USING "shrink_kill.sch"
USING "rc_helper_functions.sch"


// Disable player controls for the current frame.  To be used for Accept/Deny scripted cam.
PROC SHRINK_DISABLE_PLAYER_CONTROLS()
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE, FALSE) // only disable alternate pause not real pause
	ENDIF
ENDPROC

// Script entry point.
SCRIPT
	SET_MISSION_FLAG(TRUE)
	#IF IS_DEBUG_BUILD
		g_b_CellDialDebugTextToggle = FALSE
	#ENDIF
	
	BOOl bShrinkEnteredVeh, bChoice, bHasPlayerMadeChoice, bDrPrompt, bIntroSetup, bVariationsRequested
	structTimer cameraPanTimer, tmrClothingRestore
	CAMERA_INDEX introCam1, introCam2
	TEXT_LABEL_15 sAudioBlock = "DrfAud"
	TEXT_LABEL_15 sBase
	//TEXT_LABEL_15 sLine
	FLOAT fRejectDelay
	BOOL bChaseStarted
	BOOL bSkip
	BOOL bEventsAllowed
	
	iUpperVariation = PICK_INT(GET_RANDOM_BOOL(), 0, 1)
	iUpperTexture = PICK_INT(GET_RANDOM_BOOL(), 0, 1)
	iLowerTexture = PICK_INT(GET_RANDOM_BOOL(), 0, 1)
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		ENABLE_SELECTOR()
		IF bChaseStarted
			IF DOES_ENTITY_EXIST(shrinkIGPed) AND IS_PED_INJURED(shrinkIGPed)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SHRINK_KILLED, TRUE)
				STAT_SET_BOOL(SP_KILLED_DR_FRIEDLANDER, TRUE)
			ENDIF
			MISSION_FLOW_MISSION_PASSED()
		ELSE
			MISSION_FLOW_MISSION_FORCE_CLEANUP()
		ENDIF
		SHRINK_OFFICE_CLEANUP(shrinkCSPed, playerVehicle, shrinkMainCam, officeState)
	ENDIF
	thisSession = SHRINK_GET_CURRENT_MISSION_SESSION()
	SHRINK_GET_SESSION_PROPERTIES(sessionProperties, thisSession)
	STOP_FIRE_IN_RANGE(<< -1899.75635, -561.36877, 10.78784>>, 100.0)
	
	//Fix for 2300669. Shrink sessions can be auto-completed by the flow. This looks to
	//Director Mode like they played and passed the session. Set a saved bit to record when the 
	//player actually attended a session.
	SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_SHRINK_SESSION_ATTENDED)
	
	SWITCH SHRINK_GET_CURRENT_MISSION_SESSION()
		CASE SHRINKSESSION_OFFICE_CHAOS       fRejectDelay = 3.6 BREAK
		CASE SHRINKSESSION_OFFICE_EVIL        fRejectDelay = 3.6 BREAK
		CASE SHRINKSESSION_OFFICE_ABANDONMENT fRejectDelay = 2.5 BREAK
	ENDSWITCH
	
	IF NOT IS_PLAYER_DEAD(PLAYER_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	// Debug mode - force variations
	#IF IS_DEBUG_BUILD
		INT iCounter, iInnerCounter
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			/*parentWidgets =*/ START_WIDGET_GROUP("DRF")
				ADD_WIDGET_BOOL("Run Shrink Session", bLaunchVisit)
				
				/*violenceWidgets =*/ START_WIDGET_GROUP("Violence Variations")
					ADD_WIDGET_BOOL("Force DRF_OWR_1",                bForceViolence[ 0])
					ADD_WIDGET_BOOL("Force DRF_OWR_2",                bForceViolence[ 1])
					ADD_WIDGET_BOOL("Force DRF_OWR_3",                bForceViolence[ 2])
					ADD_WIDGET_BOOL("Force DRF_OWR_4_A",              bForceViolence[ 3])
					ADD_WIDGET_BOOL("Force DRF_OWR_4_B",              bForceViolence[ 4])
					ADD_WIDGET_BOOL("Force DRF_OWR_4_C",              bForceViolence[ 5])
					ADD_WIDGET_BOOL("Force DRF_OWR_7_A",              bForceViolence[ 6])
					ADD_WIDGET_BOOL("Force DRF_OWR_7_B",              bForceViolence[ 7])
					ADD_WIDGET_BOOL("Force DRF_OWR_7_C",              bForceViolence[ 8])
					ADD_WIDGET_BOOL("Force DRF_OWR_7_D",              bForceViolence[ 9])
					ADD_WIDGET_BOOL("Force DRF_OWR_7_E",              bForceViolence[10])
					ADD_WIDGET_BOOL("Don't force violence variation", bForceViolenceOff)
				STOP_WIDGET_GROUP()
				
				/*sexWidgets =*/ START_WIDGET_GROUP("Sex Variations")
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_1_A",           bForceSex[ 0])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_1_B",           bForceSex[ 1])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_1_C",           bForceSex[ 2])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_1_D",           bForceSex[ 3])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_1_STRIP_A",     bForceSex[ 4])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_TAKE_01_B",   bForceSex[ 5])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_TAKE_01_C",   bForceSex[ 6])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_PROS_A",      bForceSex[ 7])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_PROS_B",      bForceSex[ 8])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_STRIPPROS_A", bForceSex[ 9])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_STRIPPROS_B", bForceSex[10])
					ADD_WIDGET_BOOL("Force DRF_PAIR_RESPONSE_5_NONE",        bForceSex[11])
					ADD_WIDGET_BOOL("Don't force sex variation",             bForceSexOff)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Debug Mode: See widgets in Script -> DRF", 60000, 0)
			
			WHILE NOT bLaunchVisit
				WAIT(0)
				
				// See if we need to clear violence variations
				IF bForceViolenceOff
					REPEAT 11 iCounter
						bForceViolence[iCounter] = FALSE
						bLastForceViolence[iCounter] = FALSE
					ENDREPEAT
					bForceViolenceOff = FALSE
				
				// Look for newly-selected variations.
				ELSE
					REPEAT 11 iCounter
						IF bForceViolence[iCounter] AND NOT bLastForceViolence[iCounter]
							
							// Turn off all other variations.
							REPEAT 11 iInnerCounter
								IF iInnerCounter <> iCounter
									bForceViolence[iInnerCounter] = FALSE
									bLastForceViolence[iInnerCounter] = FALSE
								ENDIF
							ENDREPEAT
							bLastForceViolence[iCounter] = TRUE
							iInnerCounter = 11
							iCounter = 11
						ENDIF
					ENDREPEAT
				ENDIF
				
				// See if we need to clear sex variations
				IF bForceSexOff
					REPEAT 12 iCounter
						bForceSex[iCounter] = FALSE
						bLastForceSex[iCounter] = FALSE
					ENDREPEAT
					bForceSexOff = FALSE
				
				// Look for newly-selected variations.
				ELSE
					REPEAT 12 iCounter
						IF bForceSex[iCounter] AND NOT bLastForceSex[iCounter]
							// Turn off all other variations.
							REPEAT 11 iInnerCounter
								IF iInnerCounter <> iCounter
									bForceSex[iInnerCounter] = FALSE
									bLastForceSex[iInnerCounter] = FALSE
								ENDIF
							ENDREPEAT
							bLastForceSex[iCounter] = TRUE
							iInnerCounter = 12
							iCounter = 12
						ENDIF
					ENDREPEAT
				ENDIF
			ENDWHILE
		ENDIF
	#ENDIF
	
	PRINTLN("Start shrink office")
	// Main loop.
	WHILE TRUE
		WAIT(0)
		
		// Check debug commands.
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FLOW_MISSION_FAILED()
				SHRINK_OFFICE_CLEANUP(shrinkCSPed, playerVehicle, shrinkMainCam, officeState)
			ENDIF
		#ENDIF
		
		IF officeState <> SHRINKOFFICESTATE_KILL_DR_FRIEDLANDER
		AND officeState <> SHRINKOFFICESTATE_KILL_CLEANUP
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		
		// Shrink state machine.
		SWITCH officeState
			// Pre-streaming init.
			CASE SHRINKOFFICESTATE_INIT
				IF PLAYER_HAS_WEAPON_EQUIPPED()	
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, INFINITE_AMMO)
				ENDIF

				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				
				SHRINK_OFFICE_INIT(shrinkModels, shrinkCSModelName, shrinkIGModelName, shrinkVehModelName, nextDoorCarModel, chairModel, SHRINK_GET_CURRENT_MISSION_SESSION())
				
				// Turn off UI elements.
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				DISABLE_SELECTOR()
				
				WHILE IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					WAIT(0)
				ENDWHILE
				
				// Player walks toward the office.
				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1918.9437, -577.8114, 10.8764>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				
				//WAIT(2000)
				
				introCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1896.5294, -545.7853, 18.7228>>, <<3.1482, 0.0036, 170.2056>>, 44.9736)
				introCam2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1896.8657, -547.7337, 18.8316>>, <<4.9529, 0.0036, 170.2056>>, 44.9736)
				SET_CAM_ACTIVE_WITH_INTERP(introCam2, introCam1, 5000)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				START_TIMER_NOW(cameraPanTimer)
				
				//LOAD_SHRINK_AUDIO_EVENT(previousAnim)
				officeState = SHRINKOFFICESTATE_STREAMING
			BREAK
			
			// Wait for streaming, then setup.
			CASE SHRINKOFFICESTATE_STREAMING
				IF NOT bIntroSetup
					IF SHRINK_IS_OFFICE_STREAMING_COMPLETE(shrinkModels)
						SHRINK_OFFICE_SETUP_INTRO(shrinkCSPed, shrinkCSModelName, vProcConvShrinkPos, fProcConvShrinkHeading, iUpperVariation, iUpperTexture, iLowerTexture)
						SHRINK_OFFICE_REQUEST_INTRO_CUTSCENE(sessionProperties.sIntroConvName, SHRINK_GET_CURRENT_MISSION_SESSION())
						START_TIMER_NOW(controlTimer)
						bIntroSetup = TRUE
					ENDIF
				ENDIF
				
				IF bIntroSetup
					IF NOT bVariationsRequested
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sMichaelID, PLAYER_PED_ID())
							ENDIF
							IF NOT IS_ENTITY_DEAD(shrinkCSPed)
								SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sFriedlanderID, shrinkCSPed)
							ENDIF
							bVariationsRequested = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				// Waiting for setup to complete.
				IF GET_TIMER_IN_SECONDS(cameraPanTimer) > 6.0 AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					IF (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())) AND (NOT IS_ENTITY_DEAD(shrinkCSPed))
						PRINTLN("Register entities for intro cutscene")
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sMichaelID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(shrinkCSPed, sFriedlanderID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					START_CUTSCENE()
					PLAY_STREAM_FRONTEND()
					
					NEW_LOAD_SCENE_STOP()
					NEW_LOAD_SCENE_START(<<-1906.8422, -572.3835, 19.1046>>, <<-1.5254, 0.0003, 171.4657>>, 12.0)
					
					CANCEL_TIMER(controlTimer)
					CANCEL_TIMER(cameraPanTimer)
					officeState = SHRINKOFFICESTATE_INTRO_CUTSCENE
				ENDIF
			BREAK
			
			// Play the intro cutscene.
			CASE SHRINKOFFICESTATE_INTRO_CUTSCENE
				IF DOES_CAM_EXIST(introCam1)
					DESTROY_CAM(introCam1)
				ENDIF
				IF DOES_CAM_EXIST(introCam2)
					DESTROY_CAM(introCam2)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sMichaelID)
					PLAY_IDLE_ANIMS(shrinkCSPed, TRUE, FALSE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sFriedlanderID)
					PLAY_IDLE_ANIMS(shrinkCSPed, FALSE, TRUE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SHRINK_SETUP_OFFICE_PROC_CONV(shrinkCSPed, shrinkVehicle, playerVehicle, shrinkMainCam, convPeds, vShrinkVehPos, fShrinkVehHeading, shrinkVehModelName)
					RELEASE_SHRINK_TRIGGER_ASSETS(SHRINK_GET_CURRENT_MISSION_SESSION())
					START_TIMER_NOW(convTimer)
					INIT_SIMPLE_USE_CONTEXT(useContext, FALSE, FALSE, FALSE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(useContext, "SHRINK_ACCEPT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					ADD_SIMPLE_USE_CONTEXT_INPUT(useContext, "SHRINK_REJECT", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					bVariationsRequested = FALSE
					
					// This needs to be called again because the cutscene automatically enables controls on exit.
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					officeState = SHRINKOFFICESTATE_PROCEDURAL
					IF IS_SCREEN_FADED_OUT()
						bSkip = TRUE
					ENDIF
				ENDIF
			BREAK
			
			// Play procedural dialogue.
			CASE SHRINKOFFICESTATE_PROCEDURAL
				SHRINK_DISABLE_PLAYER_CONTROLS()
				
				IF IS_SCREEN_FADED_OUT() OR bSkip
					IF bSkip
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					
					RESTART_TIMER_NOW(convTimer)
					WHILE GET_TIMER_IN_SECONDS(convTimer) < 0.4
						WAIT(0)
						SHRINK_DISABLE_PLAYER_CONTROLS()
					ENDWHILE
					
					WHILE IS_NEW_LOAD_SCENE_ACTIVE() AND NOT IS_NEW_LOAD_SCENE_LOADED()
						WAIT(0)
						SHRINK_DISABLE_PLAYER_CONTROLS()
					ENDWHILE
					
					RESTART_TIMER_NOW(convTimer)
					bSkip = FALSE
					CERRORLN(DEBUG_MISSION, "Fading in (procedural)")
					DO_SCREEN_FADE_IN(400)
				ENDIF
				
				IF NOT bHasPlayerMadeChoice
			
					IF (NOT bOutroRequested) AND (NOT IS_CUTSCENE_ACTIVE())
						PRINTLN("Requesting cutscene ", sessionProperties.sOutroConvName)
						sessionProperties.shrinkDecision = SHRINK_ACCEPT
						SHRINK_OFFICE_REQUEST_OUTRO_CUTSCENE(sessionProperties.sOutroConvName, sessionProperties, SHRINK_GET_CURRENT_MISSION_SESSION())
						bOutroRequested = TRUE
					ENDIF
					
					IF HAS_PLAYER_REACTED_TO_SHRINK_ADVICE_OFFICE(convTimer, useContext, bChoice)
						PRINTLN("Player made choice")
						
						IF bChoice
							sessionProperties.shrinkDecision = SHRINK_ACCEPT 
						ELSE
							sessionProperties.shrinkDecision = SHRINK_REJECT
							IF bOutroRequested
								REMOVE_CUTSCENE()
							ENDIF
							
							SHRINK_OFFICE_REQUEST_OUTRO_CUTSCENE(sessionProperties.sOutroConvName, sessionProperties, SHRINK_GET_CURRENT_MISSION_SESSION())
							bVariationsRequested = FALSE
							
							RESTART_TIMER_NOW(convTimer)
						ENDIF
						
						CLEAR_HELP()
						bHasPlayerMadeChoice = TRUE
					ELSE
						IF GET_TIMER_IN_SECONDS(convTimer) >= 14.0
							sessionProperties.shrinkDecision = SHRINK_SILENT
							bHasPlayerMadeChoice = TRUE
							CLEAR_HELP()
						
						ELIF (NOT bDrPrompt) AND GET_TIMER_IN_SECONDS(convTimer) >= 7.0
							sBase = "DRF_PROMPT"
//							sLine = sBase
//							IF thisSession = SHRINKSESSION_OFFICE_CHAOS
//								sLine += "_1"
//							ELIF thisSession = SHRINKSESSION_OFFICE_EVIL
//								sLine += "_2"
//							ELIF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
//								sLine += "_3"
//							ENDIF
							CREATE_CONVERSATION(convPeds, sAudioBlock, sBase, CONV_PRIORITY_VERY_HIGH)
							//PLAY_SINGLE_LINE_FROM_CONVERSATION(convPeds, sAudioBlock, sBase, sLine, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
							bDrPrompt = TRUE
						ENDIF
					ENDIF
				ELSE
				
					IF (GET_TIMER_IN_SECONDS(convTimer) > 5.0 AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE())
					OR (HAS_CUTSCENE_LOADED()
					AND (sessionProperties.shrinkDecision <> SHRINK_REJECT OR GET_TIMER_IN_SECONDS(convTimer) > fRejectDelay OR PLAY_FILLER_ANIMS(thisSession, shrinkCSPed, shrinkMainCam)))
						PRINTLN("Register entities for outro cutscene")
						SHRINK_CLEANUP_PROC_CONV()
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sMichaelID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_ENTITY_DEAD(shrinkCSPed)
							REGISTER_ENTITY_FOR_CUTSCENE(shrinkCSPed, sFriedlanderID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						START_CUTSCENE()
						SET_MODEL_AS_NO_LONGER_NEEDED(chairModel)
						officeState = SHRINKOFFICESTATE_OUTRO_CUTSCENE
					ENDIF
				ENDIF
				
				IF (NOT bVariationsRequested) AND bOutroRequested
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sMichaelID, PLAYER_PED_ID())
						ENDIF
						IF NOT IS_ENTITY_DEAD(shrinkCSPed)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sFriedlanderID, shrinkCSPed)
						ENDIF
						bVariationsRequested = TRUE
					ENDIF
				ENDIF
			BREAK
			
			// Play the outro cutscene.
			CASE SHRINKOFFICESTATE_OUTRO_CUTSCENE
				IF DOES_CAM_EXIST(shrinkMainCam)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(shrinkMainCam)
				ENDIF
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() AND IS_CUTSCENE_PLAYING()
					AND NOT IS_SCREEN_FADING_OUT() AND NOT IS_SCREEN_FADED_OUT()
					//DO_SCREEN_FADE_OUT(250)
					STOP_CUTSCENE()
				ENDIF
//				
//				IF IS_SCREEN_FADED_OUT()
//					STOP_CUTSCENE_IMMEDIATELY()
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				ENDIF

				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
						STOP_STREAM()
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
								DETACH_ENTITY(PLAYER_PED_ID())
							ENDIF							
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						ENDIF
						
						SHRINK_SETUP_KILL_SCENARIO()
						START_TIMER_NOW(tmrKillControl)
						START_TIMER_NOW(tmrPlayerGait)
						START_TIMER_NOW(tmrObjective)
						
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						officeState = SHRINKOFFICESTATE_KILL_DR_FRIEDLANDER
					ELSE
						STOP_STREAM()
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
								DETACH_ENTITY(PLAYER_PED_ID())
							ENDIF
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<-1902.6094, -559.9267, 10.7916>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fPostConvPlayerHeading)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
							
							//B*1177720 - Fix for parking large vehicle in front of the office
							playerVehicle = GET_PLAYERS_LAST_VEHICLE()
							IF DOES_ENTITY_EXIST(playerVehicle)
								IF NOT IS_ENTITY_DEAD(playerVehicle)
									SET_ENTITY_COORDS(playerVehicle, SHRINK_GET_PLAYER_VEHICLE_POSITION(playerVehicle))
									SET_ENTITY_HEADING(playerVehicle, 231.0000)
								ENDIF
							ENDIF
							
							IF IS_SCREEN_FADED_OUT()
								IF NOT bSceneRequested
									NEW_LOAD_SCENE_START(<<-1903.8698, -562.9188, 12.8354>>, NORMALISE_VECTOR(<<-1901.3314, -560.0015, 12.8015>> - <<-1903.8698, -562.9188, 12.8354>>), 100)
									bSceneRequested = TRUE
									WAIT(0)
								ENDIF
								
								IF NOT bClothingRestore
									bClothingRestore = TRUE
								ENDIF
								
								WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
									WAIT(0)
								ENDWHILE
							ENDIF
							
							//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vPostConvPlayerPos, PEDMOVE_WALK)
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
						ENDIF
						
						officeState = SHRINKOFFICESTATE_CLEANUP
					ENDIF
				ELSE
					IF NOT bSceneRequested
						IF GET_CUTSCENE_TIME() > 60000
							IF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
								NEW_LOAD_SCENE_START(<<-1905.4998, -557.8768, 12.4125>>, NORMALISE_VECTOR(<<-1903.8552, -560.4745, 11.9712>> - <<-1905.4998, -557.8768, 12.4125>>), 100)
							ELSE
								NEW_LOAD_SCENE_START(<<-1903.8698, -562.9188, 12.8354>>, NORMALISE_VECTOR(<<-1901.3314, -560.0015, 12.8015>> - <<-1903.8698, -562.9188, 12.8354>>), 100)
							ENDIF
							bSceneRequested = TRUE
						ENDIF
					ENDIF
					
					IF NOT bClothingRestore
						IF GET_CUTSCENE_CONCAT_SECTION_PLAYING() >= 16
							IF NOT IS_TIMER_STARTED(tmrClothingRestore)
								START_TIMER_NOW(tmrClothingRestore)
							ELIF GET_TIMER_IN_SECONDS(tmrClothingRestore) > SHRINK_GET_ATTIRE_RESTORE_DELAY(SHRINK_GET_CURRENT_MISSION_SESSION())
								IF SHRINK_GET_CURRENT_MISSION_SESSION() = SHRINKSESSION_OFFICE_ABANDONMENT
									shrinkIGPed = CREATE_PED(PEDTYPE_MISSION, shrinkIGModelName, <<-1899.7000, -562.2000, 10.7945>>, 217.9453)
									SET_PED_COMPONENT_VARIATION(shrinkIGPed, PED_COMP_TORSO, iUpperVariation, iUpperTexture)
									SET_PED_COMPONENT_VARIATION(shrinkIGPed, PED_COMP_LEG, 0, iLowerTexture)
									SET_PED_MONEY(shrinkIGPed, iSHRINK_FEE_ABANDONMENT + GET_RANDOM_INT_IN_RANGE(5, 301))
									SET_ENTITY_LOAD_COLLISION_FLAG(shrinkIGPed, TRUE)
									ADD_PED_FOR_DIALOGUE(convPeds, 1, shrinkIGPed, "FRIEDLANDER")
								ENDIF
								
								bClothingRestore = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Kill Dr. Friedlander or let him go.
			CASE SHRINKOFFICESTATE_KILL_DR_FRIEDLANDER
				IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				// Doctor dead?
				IF IS_ENTITY_DEAD(shrinkIGPed)
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SHRINK_KILLED, TRUE)
					STAT_SET_BOOL(SP_KILLED_DR_FRIEDLANDER, TRUE)
					CREATE_CONVERSATION(convPeds, "DrfAud", "DRF_KILL", CONV_PRIORITY_VERY_HIGH)
					IF DOES_BLIP_EXIST(shrinkBlip)
						SET_BLIP_COLOUR(shrinkBlip, BLIP_COLOUR_RED)
					ENDIF
					officeState = SHRINKOFFICESTATE_KILL_CLEANUP
				
				// Doctor escaped?
				ELIF (NOT DOES_ENTITY_EXIST(shrinkIGPed)) 
				OR GET_DISTANCE_BETWEEN_ENTITIES(shrinkIGPed, PLAYER_PED_ID()) > 300.0
					CREATE_CONVERSATION(convPeds, "DrfAud", "DRF_AWAY", CONV_PRIORITY_VERY_HIGH)
					IF DOES_BLIP_EXIST(shrinkBlip)
						SET_BLIP_COLOUR(shrinkBlip, BLIP_COLOUR_BLUE)
					ENDIF
					IF DOES_ENTITY_EXIST(shrinkIGPed)
						CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(shrinkIGPed), 10.0)
					ENDIF
					officeState = SHRINKOFFICESTATE_KILL_CLEANUP
				
				// Doctor still alive and fleeing.
				ELSE
					SHRINK_UPDATE_FLIGHT_DIALOGUE()
					SET_PED_CONFIG_FLAG(shrinkIGPed, PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
					
					IF IS_TIMER_STARTED(tmrPlayerGait)
						IF GET_TIMER_IN_SECONDS(tmrPlayerGait) > 2.5
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 1000)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							CANCEL_TIMER(tmrPlayerGait)
						ENDIF
					ENDIF
					
					IF NOT bPanStarted
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						
						// Notes:
						// The interp to game cam now looks bad because of the camera clipping the wall as Michael
						//  comes around the corner. This didn't seem to be a problem on last gen. The desired
						//  end positions have been pre-computed; we'll now lerp to this hard-coded position and
						//  do a fast lerp to gameplay from there.
						IF GET_TIMER_IN_SECONDS(tmrKillControl) > 2.0
							VECTOR vCamPos, vCamRot
							SWITCH GET_FOLLOW_PED_CAM_VIEW_MODE()
								CASE CAM_VIEW_MODE_THIRD_PERSON_NEAR
									vCamPos = <<-1905.2889, -560.9047, 12.5379>>
									vCamRot = <<-8.9999, -0.0000, -99.8165>>
								BREAK
								CASE CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
									vCamPos = <<-1908.0061, -560.4351, 12.6592>>
									vCamRot = <<-3.0000, 0.0000, -99.8148>>
								BREAK
								CASE CAM_VIEW_MODE_THIRD_PERSON_FAR
									vCamPos = <<-1908.7588, -558.6420, 13.4942>>
									vCamRot = <<-12.0007, -0.0000, -115.8697>>
								BREAK
								CASE CAM_VIEW_MODE_FIRST_PERSON
									vCamPos = <<-1904.6583, -560.0228, 12.4975>>
									vCamRot = <<-10.0948, 0.0099, -128.3421>>
								BREAK
								DEFAULT
									CASSERTLN(DEBUG_MISSION, "Unsupported follow cam view mode! (lerp betw scripted cams)")
									vCamPos = <<-1908.0061, -560.4351, 12.6592>>
									vCamRot = <<-3.0000, 0.0000, -99.8148>>
								BREAK
							ENDSWITCH
							
							shrinkMainCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, vCamRot, 50)
							SET_CAM_ACTIVE_WITH_INTERP(shrinkMainCam, shrinkHelperCam, 2660)
							
							IF DOES_ENTITY_EXIST(playerVehicle)
							AND NOT IS_ENTITY_DEAD(playerVehicle)
								SET_VEHICLE_FIXED(playerVehicle)
								SET_ENTITY_INVINCIBLE(playerVehicle, FALSE)
								SET_VEHICLE_CAN_BREAK(playerVehicle, TRUE)
							ENDIF
							
							RESTART_TIMER_NOW(tmrKillControl)
							bPanStarted = TRUE
						ENDIF
					
					ELIF NOT bChaseStarted
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						
						IF GET_TIMER_IN_SECONDS(tmrKillControl) > 2.0
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							DISPLAY_RADAR(TRUE)
							DISPLAY_HUD(TRUE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
							bChaseStarted = TRUE
						ELSE
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						ENDIF
					
					ELSE
						IF DOES_CAM_EXIST(shrinkHelperCam)
							IF GET_TIMER_IN_SECONDS(tmrKillControl) >= 2.66
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(PICK_FLOAT(GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_THIRD_PERSON_FAR, -16.0000, 0.0))
								RENDER_SCRIPT_CAMS(FALSE, TRUE, 340, FALSE)
								DESTROY_CAM(shrinkHelperCam)
							ELSE
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							ENDIF
						ENDIF
					ENDIF
					
					IF bEscDlgCleared AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF TIMER_DO_ONCE_WHEN_READY(timerBlipFlicker, 0.5)
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								NEW_LOAD_SCENE_STOP()
							ENDIF
							
							IF NOT DOES_BLIP_EXIST(shrinkBlip)
								IF NOT IS_ENTITY_DEAD(shrinkIGPed)
									shrinkBlip = ADD_BLIP_FOR_ENTITY(shrinkIGPed)
									SET_BLIP_COLOUR(shrinkBlip, BLIP_COLOUR_RED)
									RESTART_TIMER_NOW(timerBlipFlicker)
								ENDIF
							
							ELIF GET_BLIP_COLOUR(shrinkBlip) <> BLIP_COLOUR_BLUE
								SET_BLIP_COLOUR(shrinkBlip, BLIP_COLOUR_BLUE)
								IF IS_TIMER_STARTED(tmrObjective)
									IF GET_TIMER_IN_SECONDS(tmrObjective) < DEFAULT_GOD_TEXT_TIME / 1000.0
										PRINT_NOW("SHRINK_KILL_B", DEFAULT_GOD_TEXT_TIME, 1)
									ELSE
										CLEAR_SMALL_PRINTS()
										CANCEL_TIMER(tmrObjective)
									ENDIF
								ENDIF
							
							ELSE
								SET_BLIP_COLOUR(shrinkBlip, BLIP_COLOUR_RED)
								IF IS_TIMER_STARTED(tmrObjective)
									IF GET_TIMER_IN_SECONDS(tmrObjective) < DEFAULT_GOD_TEXT_TIME / 1000.0
										PRINT_NOW("SHRINK_KILL_R", DEFAULT_GOD_TEXT_TIME, 1)
									ELSE
										CLEAR_SMALL_PRINTS()
										CANCEL_TIMER(tmrObjective)
									ENDIF
								ENDIF
							ENDIF
							RESTART_TIMER_NOW(timerBlipFlicker)
						ENDIF
					ELSE
						RESTART_TIMER_AT(timerBlipFlicker, 0.3)
					ENDIF
					
					// Update fleeing behavior.
					IF NOT IS_PED_INJURED(shrinkIGPed) 
						IF DOES_ENTITY_EXIST(shrinkVehicle)
							IF IS_PED_IN_ANY_VEHICLE(shrinkIGPed)
								bShrinkEnteredVeh = TRUE //shrink entered vehicle at one point
							ENDIF
							
							IF (NOT IS_PED_FLEEING(shrinkIGPed))
								IF (NOT IS_VEHICLE_DRIVEABLE(shrinkVehicle))
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), shrinkVehicle, TRUE)
								OR IS_VEHICLE_STUCK_TIMER_UP(shrinkVehicle, VEH_STUCK_ON_ROOF, 2000)
								OR IS_VEHICLE_STUCK_TIMER_UP(shrinkVehicle, VEH_STUCK_ON_SIDE, 2000)
								OR IS_VEHICLE_STUCK_TIMER_UP(shrinkVehicle, VEH_STUCK_HUNG_UP, 2000)
								OR IS_VEHICLE_STUCK_TIMER_UP(shrinkVehicle, VEH_STUCK_JAMMED,  2000)
								OR (bShrinkEnteredVeh AND NOT IS_PED_IN_ANY_VEHICLE(shrinkIGPed)) //shrink was in the vehicle but is no longer, run
								OR IS_PLAYER_TOWING_VEHICLE(shrinkVehicle)
								OR IS_PLAYER_FLYING_WITH_ATTACHED_VEHICLE(shrinkVehicle)
									TASK_SMART_FLEE_PED(shrinkIGPed, PLAYER_PED_ID(), 3000.0, -1)
								ENDIF
							ENDIF
						ELIF NOT IS_PED_FLEEING(shrinkIGPed)
							TASK_SMART_FLEE_PED(shrinkIGPed, PLAYER_PED_ID(), 3000.0, -1)
						ENDIF
						
						IF NOT bEventsAllowed
						AND NOT IS_ENTITY_IN_ANGLED_AREA(shrinkIGPed, <<-1892.13904, -567.19403, 10.5000>>, <<-1904.93494, -556.44226, 13.5000>>, 4.73)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(shrinkIGPed, FALSE)
							bEventsAllowed = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE SHRINKOFFICESTATE_CLEANUP
				IF NOT IS_TIMER_STARTED(cleanupTimer)
					NEW_LOAD_SCENE_STOP()
					START_TIMER_NOW(cleanupTimer)
				ELIF GET_TIMER_IN_SECONDS(cleanupTimer) > 2.0

					#IF IS_DEBUG_BUILD
						g_b_CellDialDebugTextToggle = TRUE
					#ENDIF
					
					DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_SHRINK, SHRINK_GET_SESSION_FEE(thisSession))
					ENABLE_SELECTOR()
					MISSION_FLOW_MISSION_PASSED()
					SHRINK_OFFICE_CLEANUP(shrinkCSPed, playerVehicle, shrinkMainCam, officeState)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
			BREAK
			
			// Cleanup specific to the kill scenario.
			CASE SHRINKOFFICESTATE_KILL_CLEANUP
			
				IF NOT IS_TIMER_STARTED(cleanupTimer)
					START_TIMER_NOW(cleanupTimer)
				ENDIF
				
				IF GET_TIMER_IN_SECONDS(cleanupTimer) > 4.0
				OR IS_SCREEN_FADED_OUT()
					REMOVE_BLIP(shrinkBlip)
					
					IF DOES_CAM_EXIST(shrinkMainCam)
						DESTROY_CAM(shrinkMainCam)
					ENDIF
					ENABLE_SELECTOR()
					MISSION_FLOW_MISSION_PASSED()
					
					// Delete Dr. F and car if we didn't kill him.
					IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SHRINK_KILLED)
						IF DOES_ENTITY_EXIST(shrinkIGPed)
							IF DOES_ENTITY_EXIST(shrinkVehicle) AND IS_PED_IN_VEHICLE(shrinkIGPed, shrinkVehicle)
								DELETE_VEHICLE(shrinkVehicle)
							ENDIF
						ENDIF
						
						DELETE_PED(shrinkIGPed)
					ENDIF

					#IF IS_DEBUG_BUILD
						g_b_CellDialDebugTextToggle = TRUE
					#ENDIF
					// End the script.
					TERMINATE_THIS_THREAD()
				ENDIF
			BREAK
		ENDSWITCH
	ENDWHILE
ENDSCRIPT
