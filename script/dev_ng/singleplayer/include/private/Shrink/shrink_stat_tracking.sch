
//////////////////////////////////////////////////////////////////////
/* shrink_proc_conv.sch												*/
/* Author: DJ Jones													*/
/* Procedural conversation functionality for shrink activity.		*/
//////////////////////////////////////////////////////////////////////


USING "shrink.sch"


//Set time stamp for sex act
PROC SHRINK_ADD_SEX_TIMESTAMP()
	INT iCounter
	REPEAT SHRINK_SEX_ACTS iCounter
		IF iCounter < SHRINK_SEX_ACTS - 1
			g_iShrinkSexTimestamps[iCounter] = g_iShrinkSexTimestamps[iCounter + 1]
		ELSE
			g_iShrinkSexTimestamps[iCounter] = GET_GAME_TIMER()
			PRINTLN("Add to shrink sex timer ", GET_GAME_TIMER())
		ENDIF
	ENDREPEAT
ENDPROC

//Set time stamp for lap dance
PROC SHRINK_ADD_LAP_DANCE_TIMESTAMP()
	INT iCounter
	REPEAT SHRINK_LAP_DANCES iCounter
		IF iCounter < SHRINK_LAP_DANCES - 1
			g_iShrinkLapDanceTimestamps[iCounter] = g_iShrinkLapDanceTimestamps[iCounter + 1]
		ELSE
			g_iShrinkLapDanceTimestamps[iCounter] = GET_GAME_TIMER()
		ENDIF
	ENDREPEAT
ENDPROC

//Set timestamp for strip club visit
PROC SHRINK_ADD_STRIP_CLUB_VISIT_TIMESTAMP()
	g_iShrinkStripClubTimestamp = GET_GAME_TIMER()
ENDPROC

//discard time stamps that are older than 1 hour (sex and lap dances only)
PROC SHRINK_REMOVE_OLD_SEX_ACT_TIMESTAMPS()
	INT iCounter
	REPEAT SHRINK_SEX_ACTS iCounter
		IF g_iShrinkSexTimestamps[iCounter] > 0
			IF GET_GAME_TIMER() - g_iShrinkSexTimestamps[iCounter] > SHRINK_SEX_MEMORY_TIME
				PRINTLN("Removing Shrink sex act")
				g_iShrinkSexTimestamps[iCounter] = 0
			ELSE
				iCounter = SHRINK_SEX_ACTS
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//discard time stamps that are older than 1 hour (sex and lap dances only)
PROC SHRINK_REMOVE_OLD_LAP_DANCE_TIMESTAMPS()
	INT iCounter
	REPEAT SHRINK_LAP_DANCES iCounter
		IF g_iShrinkLapDanceTimestamps[iCounter] > 0
			IF GET_GAME_TIMER() - g_iShrinkLapDanceTimestamps[iCounter] > SHRINK_SEX_MEMORY_TIME
				g_iShrinkLapDanceTimestamps[iCounter] = 0
			ELSE
				iCounter = SHRINK_LAP_DANCES
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//discard time stamp that are older than 1 hour (strip club visit)
PROC SHRINK_REMOVE_OLD_STRIP_CLUB_TIMESTAMP()
	IF g_iShrinkStripClubTimestamp > 0 AND GET_GAME_TIMER() - g_iShrinkStripClubTimestamp > SHRINK_SEX_MEMORY_TIME
		g_iShrinkStripClubTimestamp = 0
	ENDIF	
ENDPROC

FUNC INT SHRINK_GET_SEX_ACT_TIMESTAMP_COUNT()
	INT iCounter, iTimestamps
	REPEAT SHRINK_SEX_ACTS iCounter
		IF g_iShrinkSexTimestamps[iCounter] > 0
			iTimestamps += 1
		ENDIF
	ENDREPEAT
	RETURN iTimestamps
ENDFUNC

FUNC INT SHRINK_GET_LAP_DANCE_TIMESTAMP_COUNT()
	INT iCounter, iTimestamps
	REPEAT SHRINK_LAP_DANCES iCounter
		IF g_iShrinkLapDanceTimestamps[iCounter] > 0
			iTimestamps += 1
		ENDIF
	ENDREPEAT
	RETURN iTimestamps
ENDFUNC

FUNC BOOL SHRINK_WAS_STRIP_CLUB_VISITED_RECENTLY()
	RETURN g_iShrinkStripClubTimestamp > 0
ENDFUNC
PROC SHRINK_CLEAR_ALL_SEX_TIMESTAMPS()
	INT iCounter
	REPEAT SHRINK_LAP_DANCES iCounter
		g_iShrinkLapDanceTimestamps[iCounter] = 0
	ENDREPEAT

	REPEAT SHRINK_SEX_ACTS iCounter
		g_iShrinkSexTimestamps[iCounter] = 0
	ENDREPEAT	
ENDPROC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// OPEN WORLD VIOLENCE ACCESSORS AND MUTATORS////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PROC SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(INT& thisTimestamps[], INT iMaxViolenceActs)
	INT iCounter
	REPEAT iMaxViolenceActs iCounter
		thisTimestamps[iCounter] = 0
	ENDREPEAT
ENDPROC 
PROC SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(INT& thisTimestamps[], INT iMaxViolenceActs)
	INT iCounter
	REPEAT iMaxViolenceActs iCounter
		IF iCounter < iMaxViolenceActs - 1
			thisTimestamps[iCounter] = thisTimestamps[iCounter + 1]
		ELSE
			thisTimestamps[iCounter] = GET_GAME_TIMER()
		ENDIF
	ENDREPEAT
ENDPROC
PROC SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(INT& thisTimestamps[], INT iMaxViolenceActs, INT iTime = SHRINK_VIOLENCE_MEMORY_TIME)
	INT iCounter
	REPEAT iMaxViolenceActs iCounter
		IF thisTimestamps[iCounter] > 0
			IF GET_GAME_TIMER() - thisTimestamps[iCounter] > iTime
				thisTimestamps[iCounter] = 0
			ELSE
				iCounter = iMaxViolenceActs
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
//helper for getting violence time stamp count
FUNC INT SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(INT& thisTimestamps[], INT iMaxViolenceActs)
	INT iCounter, iTimestamps
	REPEAT iMaxViolenceActs iCounter
		IF thisTimestamps[iCounter] > 0
			iTimestamps += 1
		ENDIF
	ENDREPEAT
	RETURN iTimestamps
ENDFUNC
//get the count of violence timestamps by type
FUNC INT SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_OW_VIOLENCE_TYPES thisViolenceType)
	SWITCH thisViolenceType
		CASE SHRINK_4STAR_WANTED
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrink4StarWantedTimestamps, iSHRINK_4STAR_WANTED)
		BREAK
		CASE SHRINK_RANDOM_KILLS
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkRandomKillsTimestamps, iSHRINK_RANDOM_KILLS)
		BREAK
		CASE SHRINK_2STAR_WANTED
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrink2StarWantedTimestamps, iSHRINK_2STAR_WANTED)
		BREAK
		CASE SHRINK_VEHICLE_KILLS
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkVehKillsTimestamps, iSHRINK_VEHICLE_KILLS)
		BREAK
		CASE SHRINK_KILLS
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkKillsTimestamps, iSHRINK_KILLS)
		BREAK
		CASE SHRINK_SHOP_ROBBERY
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkShopRobTimestamps, iSHRINK_SHOP_ROBBERY)
		BREAK
		CASE SHRINK_PED_ROBBERY
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkPedRobTimestamps, iSHRINK_PED_ROBBERY)
		BREAK
		CASE SHRINK_STOLEN_CARS
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkStolenCarTimestamps, iSHRINK_STOLEN_CARS)
		BREAK
		CASE SHRINK_WANTED_IN_CAR
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkWantedInCarTimestamps, iSHRINK_WANTED_IN_CAR)
		BREAK
		CASE SHRINK_DANGEROUS_DRIVING
			RETURN SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT_HELPER(g_iShrinkDangerousDriveTimestamps, iSHRINK_DANGEROUS_DRIVING)
		BREAK		
	ENDSWITCH
	RETURN 0
ENDFUNC
//
//Set time stamp for violence
PROC SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_OW_VIOLENCE_TYPES thisViolenceType)	
	SWITCH thisViolenceType
		CASE SHRINK_4STAR_WANTED
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrink4StarWantedTimestamps, iSHRINK_4STAR_WANTED)
		BREAK
		CASE SHRINK_RANDOM_KILLS
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkRandomKillsTimestamps, iSHRINK_RANDOM_KILLS)
		BREAK
		CASE SHRINK_2STAR_WANTED
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrink2StarWantedTimestamps, iSHRINK_2STAR_WANTED)
		BREAK
		CASE SHRINK_VEHICLE_KILLS
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkVehKillsTimestamps, iSHRINK_VEHICLE_KILLS)
		BREAK
		CASE SHRINK_KILLS
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkKillsTimestamps, iSHRINK_KILLS)
		BREAK
		CASE SHRINK_SHOP_ROBBERY
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkShopRobTimestamps, iSHRINK_SHOP_ROBBERY)
		BREAK
		CASE SHRINK_PED_ROBBERY
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkPedRobTimestamps, iSHRINK_PED_ROBBERY)
		BREAK
		CASE SHRINK_STOLEN_CARS
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkStolenCarTimestamps, iSHRINK_STOLEN_CARS)
		BREAK
		CASE SHRINK_WANTED_IN_CAR
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkWantedInCarTimestamps, iSHRINK_WANTED_IN_CAR)
		BREAK
		CASE SHRINK_DANGEROUS_DRIVING
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP_HELPER(g_iShrinkDangerousDriveTimestamps, iSHRINK_DANGEROUS_DRIVING)
		BREAK		
	ENDSWITCH
ENDPROC
//Interate through all the violence types and remove old time stamps
PROC SHRINK_REMOVE_OW_VIOLENCE_TIMESTAMPS()	
	SHRINK_OW_VIOLENCE_TYPES eViolenceType
	INT iCounter
	REPEAT SHRINK_NUM_EVENTS iCounter
		eViolenceType = INT_TO_ENUM(SHRINK_OW_VIOLENCE_TYPES,iCounter)
		SWITCH eViolenceType
			CASE SHRINK_4STAR_WANTED
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrink4StarWantedTimestamps, iSHRINK_4STAR_WANTED)
			BREAK
			CASE SHRINK_RANDOM_KILLS
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkRandomKillsTimestamps, iSHRINK_RANDOM_KILLS)
			BREAK
			CASE SHRINK_2STAR_WANTED
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrink2StarWantedTimestamps, iSHRINK_2STAR_WANTED)
			BREAK
			CASE SHRINK_VEHICLE_KILLS
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkVehKillsTimestamps, iSHRINK_VEHICLE_KILLS, SHRINK_VIOLENCE_VEH_KILLS_MEMORY_TIME)
			BREAK
			CASE SHRINK_KILLS
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkKillsTimestamps, iSHRINK_KILLS)
			BREAK
			CASE SHRINK_SHOP_ROBBERY
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkShopRobTimestamps, iSHRINK_SHOP_ROBBERY)
			BREAK
			CASE SHRINK_PED_ROBBERY
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkPedRobTimestamps, iSHRINK_PED_ROBBERY)
			BREAK
			CASE SHRINK_STOLEN_CARS
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkStolenCarTimestamps, iSHRINK_STOLEN_CARS)
			BREAK
			CASE SHRINK_WANTED_IN_CAR
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkWantedInCarTimestamps, iSHRINK_WANTED_IN_CAR)
			BREAK
			CASE SHRINK_DANGEROUS_DRIVING
				SHRINK_REMOVE_OLD_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkDangerousDriveTimestamps, iSHRINK_DANGEROUS_DRIVING)
			BREAK		
		ENDSWITCH		
	ENDREPEAT
ENDPROC
//Interate through all the violence types and CLEAR ALL time stamps
PROC SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS()	
	SHRINK_OW_VIOLENCE_TYPES eViolenceType
	INT iCounter
	REPEAT SHRINK_NUM_EVENTS iCounter
		eViolenceType = INT_TO_ENUM(SHRINK_OW_VIOLENCE_TYPES,iCounter)
		SWITCH eViolenceType
			CASE SHRINK_4STAR_WANTED
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrink4StarWantedTimestamps, iSHRINK_4STAR_WANTED)
			BREAK
			CASE SHRINK_RANDOM_KILLS
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkRandomKillsTimestamps, iSHRINK_RANDOM_KILLS)
			BREAK
			CASE SHRINK_2STAR_WANTED
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrink2StarWantedTimestamps, iSHRINK_2STAR_WANTED)
			BREAK
			CASE SHRINK_VEHICLE_KILLS
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkVehKillsTimestamps, iSHRINK_VEHICLE_KILLS)
			BREAK
			CASE SHRINK_KILLS
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkKillsTimestamps, iSHRINK_KILLS)
			BREAK
			CASE SHRINK_SHOP_ROBBERY
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkShopRobTimestamps, iSHRINK_SHOP_ROBBERY)
			BREAK
			CASE SHRINK_PED_ROBBERY
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkPedRobTimestamps, iSHRINK_PED_ROBBERY)
			BREAK
			CASE SHRINK_STOLEN_CARS
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkStolenCarTimestamps, iSHRINK_STOLEN_CARS)
			BREAK
			CASE SHRINK_WANTED_IN_CAR
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkWantedInCarTimestamps, iSHRINK_WANTED_IN_CAR)
			BREAK
			CASE SHRINK_DANGEROUS_DRIVING
				SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS_HELPER(g_iShrinkDangerousDriveTimestamps, iSHRINK_DANGEROUS_DRIVING)
			BREAK		
		ENDSWITCH		
	ENDREPEAT
ENDPROC




