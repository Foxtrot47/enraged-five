
//////////////////////////////////////////////////////////////////////
/* shrink_properties.sch											*/
/* Author: DJ Jones													*/
/* Per-rank information for shrink activity.						*/
//////////////////////////////////////////////////////////////////////


// Get the properties for a session with Dr. Friedlander based on the current rank.
PROC SHRINK_GET_SESSION_PROPERTIES(SHRINK_SESSION_PROPERTIES& sessionProperties, SHRINK_SESSION eSession)
	
	SWITCH eSession
		CASE SHRINKSESSION_OFFICE_CHAOS
			sessionProperties.sIntroConvName = "DRF_MIC_1_CS_1"
			sessionProperties.sStoryConvName = "OBTNR1"
			sessionProperties.sOutroConvName = "DRF_MIC_1_CS_2"
			sessionProperties.iOutroDuration = 293200
		BREAK
		
		CASE SHRINKSESSION_OFFICE_EVIL
			sessionProperties.sIntroConvName = "DRF_MIC_2_CS_1"
			sessionProperties.sStoryConvName = "OBTNR2"
			sessionProperties.sOutroConvName = "DRF_MIC_2_CS_2"
			sessionProperties.iOutroDuration = 243600
		BREAK
		
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			sessionProperties.sIntroConvName = "PBTINT1"
			sessionProperties.sStoryConvName = "PBTNR2"
			sessionProperties.sOutroConvName = "PBTOUT1"
		BREAK
		
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			sessionProperties.sIntroConvName = "PBTINT2"
			sessionProperties.sStoryConvName = "PBTNR3"
			sessionProperties.sOutroConvName = "PBTOUT2"
		BREAK
		
		CASE SHRINKSESSION_OFFICE_ABANDONMENT
			sessionProperties.sIntroConvName = "DRF_MIC_3_CS_1"
			sessionProperties.sStoryConvName = "OBTNR1"
			sessionProperties.sOutroConvName = "DRF_MIC_3_CS_2"
			sessionProperties.iOutroDuration = 252700
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("SHRINK_GET_SESSION_PROPERTIES called with invalid SHRINK_SESSION!")
		BREAK
	ENDSWITCH
ENDPROC
