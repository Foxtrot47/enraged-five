
//////////////////////////////////////////////////////////////////////
/* shrink_phone_defs.sch											*/
/* Author: DJ Jones, Yomal Perera									*/
/* definitions for shrink phone sessions.							*/
//////////////////////////////////////////////////////////////////////


CONST_INT iSHRINK_CUTSCENE 0


USING "rgeneral_include.sch"
USING "minigames_helpers.sch"
USING "cutscene_public.sch"
USING "shrink.sch"

USING "shrink_properties.sch"

USING "shrink_proc_conv.sch"
USING "shrink_core.sch"

SHRINK_PHONE_STATE phoneState
SHRINK_SESSION_PROPERTIES sessionProperties
SHRINK_SESSION thisSession

structTimer timerReply

structPedsForConversation convPeds

STRING sRootStrings[iSHRINK_LONGEST_CONV_PRE_DECISION]
STRING sSpecificStrings[iSHRINK_LONGEST_CONV_PRE_DECISION]
TEXT_LABEL_15 sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION]
TEXT_LABEL_15 sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION]

STRING sYesRootStrings[iSHRINK_LONGEST_CONV_POST_DECISION]
STRING sYesSpecificStrings[iSHRINK_LONGEST_CONV_POST_DECISION]
TEXT_LABEL_15 sYesRoots[iSHRINK_LONGEST_CONV_POST_DECISION]
TEXT_LABEL_15 sYesSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION]

STRING sNoRootStrings[iSHRINK_LONGEST_CONV_POST_DECISION]
STRING sNoSpecificStrings[iSHRINK_LONGEST_CONV_POST_DECISION]
TEXT_LABEL_15 sNoRoots[iSHRINK_LONGEST_CONV_POST_DECISION]
TEXT_LABEL_15 sNoSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION]

INT iLineCounter, iYesLineCounter, iNoLineCounter
