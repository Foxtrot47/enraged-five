
//////////////////////////////////////////////////////////////////////
/* shrink_kill.sch													*/
/* Author: DJ Jones													*/
/* Updates related to killing (or letting go) Dr. Friedlander.		*/
//////////////////////////////////////////////////////////////////////


CONST_INT iSHRINK_ESC_DRF_VARIATIONS 7
CONST_INT iSHRINK_ESC_PLR_VARIATIONS 8


// Determine good placement for vehicle based on its length & width.
FUNC VECTOR SHRINK_GET_PLAYER_VEHICLE_POSITION(VEHICLE_INDEX vehPlayer)
	VECTOR vFront, vSide, vUp, vPos, vMin, vMax
	
	IF IS_ENTITY_DEAD(vehPlayer)
		RETURN <<0,0,0>>
	ENDIF
	
	GET_ENTITY_MATRIX(vehPlayer, vFront, vSide, vUp, vPos)
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehPlayer), vMin, vMax)
	
	RETURN <<-1900.2925, -555.7014, 10.7548>> - vMax.x * <<-0.6293, -0.7771, 0.0>> - vMax.y * <<0.7771, -0.6293, 0.0>>
ENDFUNC

// Get Dr. Friedlander and Michael ready for some killing (unless the player is a pussy).
PROC SHRINK_SETUP_KILL_SCENARIO()
	IF NOT IS_VEHICLE_DRIVEABLE(shrinkVehicle)
		DELETE_VEHICLE(shrinkVehicle)
		shrinkVehicle = CREATE_VEHICLE(shrinkVehModelName, vShrinkVehPos, fShrinkVehHeading)
		SET_VEHICLE_COLOUR_COMBINATION(shrinkVehicle, 6)
	ENDIF
	
	//VECTOR vShrinkPos
	
	IF DOES_ENTITY_EXIST(shrinkCSPed)
		//vShrinkPos = GET_ENTITY_COORDS(shrinkCSPed, FALSE)
		DELETE_PED(shrinkCSPed)
		//shrinkCSPed = CREATE_PED(PEDTYPE_MISSION, shrinkIGModelName, vShrinkPos, 0)
	ENDIF
	
	IF (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())) AND (NOT IS_ENTITY_DEAD(shrinkVehicle)) AND IS_VEHICLE_DRIVEABLE(shrinkVehicle)
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1907.8273, -560.0641, 10.8064>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 260.1950)
		
		SET_ENTITY_COORDS(shrinkVehicle, vShrinkVehPos)
		SET_ENTITY_HEADING(shrinkVehicle, fShrinkVehHeading)
		
		IF DOES_ENTITY_EXIST(playerVehicle)
			IF NOT IS_ENTITY_DEAD(playerVehicle)
				//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA
				SET_VEHICLE_FIXED(playerVehicle)
				SET_ENTITY_INVINCIBLE(playerVehicle, TRUE)
				SET_VEHICLE_CAN_BREAK(playerVehicle, FALSE)
				SET_ENTITY_COORDS(playerVehicle, SHRINK_GET_PLAYER_VEHICLE_POSITION(playerVehicle))
				SET_ENTITY_HEADING(playerVehicle, 231.0000)
			ENDIF
		ENDIF
		
//		IF IS_ENTITY_ATTACHED(shrinkIGPed)
//			DETACH_ENTITY(shrinkIGPed)
//		ENDIF
//		SET_ENTITY_COLLISION(shrinkIGPed, TRUE)
//		FREEZE_ENTITY_POSITION(shrinkIGPed, FALSE)
		
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		TASK_STAND_STILL(PLAYER_PED_ID(), -1)
		
		IF NOT bClothingRestore OR NOT DOES_ENTITY_EXIST(shrinkIGPed) OR IS_ENTITY_DEAD(shrinkIGPed)
			shrinkIGPed = CREATE_PED(PEDTYPE_MISSION, shrinkIGModelName, <<-1899.7000, -562.2000, 10.7945>>, 217.9453)
			SET_PED_COMPONENT_VARIATION(shrinkIGPed, PED_COMP_TORSO, iUpperVariation, iUpperTexture)
			SET_PED_COMPONENT_VARIATION(shrinkIGPed, PED_COMP_LEG, 0, iLowerTexture)
			SET_PED_MONEY(shrinkIGPed, iSHRINK_FEE_ABANDONMENT + GET_RANDOM_INT_IN_RANGE(5, 301))
			SET_ENTITY_LOAD_COLLISION_FLAG(shrinkIGPed, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(shrinkIGPed, TRUE)
			ADD_PED_FOR_DIALOGUE(convPeds, 1, shrinkIGPed, "FRIEDLANDER")
			bClothingRestore = TRUE
			
		ELSE
			SET_ENTITY_COORDS(shrinkIGPed,<<-1899.7000, -562.2000, 10.7945>>)
			SET_ENTITY_HEADING(shrinkIGPed, 217.9453)
			CLEAR_PED_TASKS_IMMEDIATELY(shrinkIGPed)
			TASK_STAND_STILL(shrinkIGPed, -1)
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			IF NOT bSceneRequested
				NEW_LOAD_SCENE_START(<<-1905.4998, -557.8768, 12.4125>>, NORMALISE_VECTOR(<<-1904.7095, -560.2881, 12.4748>> - <<-1905.4998, -557.8768, 12.4125>>), 100)
				bSceneRequested = TRUE
				WAIT(0)
			ENDIF
			
			WHILE (NOT IS_NEW_LOAD_SCENE_LOADED())
			OR ((NOT IS_PED_INJURED(shrinkIGPed)) AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(shrinkIGPed))
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			
			DO_SCREEN_FADE_IN(500)
		ENDIF
		
		FLOAT fMoveRatio = PEDMOVEBLENDRATIO_RUN
		
//		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			OPEN_SEQUENCE_TASK(seqShrinkCar)
//				TASK_STAND_STILL(NULL, 2000)
//				SIMULATE_PLAYER_INPUT_GAIT(NULL, <<-1904.4763, -560.7510, 10.7980>>, 0.5 * (PEDMOVE_WALK + PEDMOVE_RUN), DEFAULT_TIME_NEVER_WARP)
//			CLOSE_SEQUENCE_TASK(seqShrinkCar)
//			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqShrinkCar)
//			CLEAR_SEQUENCE_TASK(seqShrinkCar)
//		ENDIF
		
		IF (NOT IS_PED_INJURED(shrinkIGPed)) AND NOT IS_ENTITY_DEAD(shrinkVehicle)
			OPEN_SEQUENCE_TASK(seqShrinkCar)
				//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1902.2, -559.8, 11.8>>, fMoveRatio, DEFAULT_TIME_NEVER_WARP, 2.0)
				TASK_ENTER_VEHICLE(NULL, shrinkVehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, fMoveRatio)
				//TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL, shrinkVehicle, "Shrink_escape", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, shrinkVehicle, <<-1783.7303, -629.1945, 9.8900>>, 20.0, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(shrinkVehicle), DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds, 10.0, 12.0)
				TASK_VEHICLE_DRIVE_WANDER(NULL, shrinkVehicle, 19.0, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds)
			CLOSE_SEQUENCE_TASK(seqShrinkCar)
			TASK_PERFORM_SEQUENCE(shrinkIGPed, seqShrinkCar)
			
			FORCE_PED_MOTION_STATE(shrinkIGPed, MS_ON_FOOT_RUN, TRUE, FAUS_CUTSCENE_EXIT, TRUE)
			
		//ELSE
			// Fallback plan?
		ENDIF
		
		shrinkBlip = ADD_BLIP_FOR_ENTITY(shrinkIGPed)
		SET_BLIP_COLOUR(shrinkBlip, BLIP_COLOUR_RED)
		RESTART_TIMER_NOW(timerBlipFlicker)
		
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(shrinkIGPed)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		
		shrinkHelperCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1904.6146, -558.8920, 12.8339>>, <<-9.0794, -0.0000, -127.4455>>, 50.0, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		DISPLAY_RADAR(FALSE)
		DISPLAY_HUD(FALSE)
	ENDIF
	
	// This is a car created next door to the office, so the player has something to drive if nothing else is available.
	CLEAR_AREA(<<-1886.8839, -571.1942, 11.3828>>, 2.5, TRUE, TRUE)
	CREATE_VEHICLE(nextDoorCarModel, <<-1886.8839, -571.1942, 11.3828>>, -40.83)
ENDPROC

PROC SHRINK_UPDATE_FLIGHT_DIALOGUE()
	TEXT_LABEL_15 sLabel
	INT iLineToPlay, iIndex
	
	// Everyone alive?
	IF (NOT DOES_ENTITY_EXIST(shrinkIGPed)) OR IS_ENTITY_DEAD(shrinkIGPed) OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF NOT IS_TIMER_STARTED(tmrDrfKillDialogue)
		START_TIMER_NOW(tmrDrfKillDialogue)
	ENDIF
	IF NOT IS_TIMER_STARTED(tmrPlrKillDialogue)
		START_TIMER_NOW(tmrPlrKillDialogue)
	ENDIF
	
	FLOAT fDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(shrinkIGPed))
	
	// Dr. Friedlander's first dialogue.
	IF iEscUsedDrfCount < iSHRINK_ESC_DRF_VARIATIONS
		IF iEscUsedDrfCount <= 0
			IF GET_TIMER_IN_SECONDS(tmrDrfKillDialogue) > 0.2
				iLineToPlay = GET_RANDOM_INT_IN_RANGE(0, iSHRINK_ESC_DRF_VARIATIONS)
				SET_BIT(iEscDlgShrinkBits, iLineToPlay)
				sLabel = "DRF_RUNOFF_"
				sLabel += iLineToPlay + 1
				//CERRORLN(DEBUG_MISSION, "Playing first line: ", sLabel)
				//CERRORLN(DEBUG_MISSION, "Playing conversation DRF_RUNOFF")
				//CREATE_CONVERSATION(convPeds, "DrfAud", "DRF_RUNOFF", CONV_PRIORITY_VERY_HIGH)
				PLAY_SINGLE_LINE_FROM_CONVERSATION(convPeds, "DrfAud", "DRF_RUNOFF", sLabel, CONV_PRIORITY_HIGH)
				iEscUsedDrfCount = 1
				RESTART_TIMER_NOW(tmrDrfKillDialogue)
			ENDIF
		
		// Dr. Friedlander's repeating dialogue.
		ELIF GET_TIMER_IN_SECONDS(tmrDrfKillDialogue) > 12.0
			IF fDist2 <= 144.0
			AND NOT IS_SCRIPTED_SPEECH_PLAYING(PLAYER_PED_ID())
				iLineToPlay = GET_RANDOM_INT_IN_RANGE(0, iSHRINK_ESC_DRF_VARIATIONS - iEscUsedDrfCount)
				
				WHILE iIndex >= 0 AND iIndex < iSHRINK_ESC_DRF_VARIATIONS
					IF NOT IS_BIT_SET(iEscDlgShrinkBits, iIndex)
						IF iLineToPlay = 0
							SET_BIT(iEscDlgShrinkBits, iIndex)
							iLineToPlay = iIndex
							iIndex = -2
						ELSE
							iLineToPlay -= 1
						ENDIF
					ENDIF
					
					iIndex += 1
				ENDWHILE
				
				sLabel = "DRF_RUNOFF_"
				sLabel += iLineToPlay + 1
				//CERRORLN(DEBUG_MISSION, "Playing line: ", sLabel)
				//CREATE_CONVERSATION(convPeds, "DrfAud", "DRF_RUNOFF", CONV_PRIORITY_VERY_HIGH)
				PLAY_SINGLE_LINE_FROM_CONVERSATION(convPeds, "DrfAud", "DRF_RUNOFF", sLabel, CONV_PRIORITY_HIGH)
				iEscUsedDrfCount += 1
				RESTART_TIMER_NOW(tmrDrfKillDialogue)
			ELSE
				RESTART_TIMER_AT(tmrDrfKillDialogue, 6.0)
			ENDIF
		ENDIF
	ENDIF
	
	// Michael's first dialogue.
	IF iEscUsedPlrCount < iSHRINK_ESC_PLR_VARIATIONS
		IF iEscUsedPlrCount <= 0
			IF GET_TIMER_IN_SECONDS(tmrPlrKillDialogue) > 1.0
			AND NOT IS_SCRIPTED_SPEECH_PLAYING(shrinkIGPed)
				iLineToPlay = GET_RANDOM_INT_IN_RANGE(0, iSHRINK_ESC_PLR_VARIATIONS)
				SET_BIT(iEscDlgPlayerBits, iLineToPlay)
				sLabel = "DRF_CHASE_"
				sLabel += iLineToPlay + 1
				//CERRORLN(DEBUG_MISSION, "Playing first line: ", sLabel)
				//CERRORLN(DEBUG_MISSION, "Playing conversation DRF_CHASE")
				//CREATE_CONVERSATION(convPeds, "DrfAud", "DRF_CHASE", CONV_PRIORITY_VERY_HIGH)
				PLAY_SINGLE_LINE_FROM_CONVERSATION(convPeds, "DrfAud", "DRF_CHASE", sLabel, CONV_PRIORITY_HIGH)
				iEscUsedPlrCount = 1
				RESTART_TIMER_NOW(tmrPlrKillDialogue)
			ENDIF
		
		// Michael's repeating dialogue.
		ELIF GET_TIMER_IN_SECONDS(tmrPlrKillDialogue) > 10.3
			IF fDist2 <= 2500.0
			AND NOT IS_SCRIPTED_SPEECH_PLAYING(shrinkIGPed)
				iLineToPlay = GET_RANDOM_INT_IN_RANGE(0, iSHRINK_ESC_PLR_VARIATIONS - iEscUsedPlrCount)
				
				iIndex = 0
				WHILE(iIndex >= 0)
					IF NOT IS_BIT_SET(iEscDlgPlayerBits, iIndex)
						IF iLineToPlay = 0
							SET_BIT(iEscDlgPlayerBits, iIndex)
							iLineToPlay = iIndex
							iIndex = -2
						ELSE
							iLineToPlay -= 1
						ENDIF
					ENDIF
					
					iIndex += 1
				ENDWHILE
				
				sLabel = "DRF_CHASE_"
				sLabel += iLineToPlay + 1
				//CERRORLN(DEBUG_MISSION, "Playing line: ", sLabel)
				//CREATE_CONVERSATION(convPeds, "DrfAud", "DRF_CHASE", CONV_PRIORITY_VERY_HIGH)
				PLAY_SINGLE_LINE_FROM_CONVERSATION(convPeds, "DrfAud", "DRF_CHASE", sLabel, CONV_PRIORITY_HIGH)
				iEscUsedPlrCount += 1
				RESTART_TIMER_NOW(tmrPlrKillDialogue)
			ELSE
				RESTART_TIMER_AT(tmrPlrKillDialogue, 5.0)
			ENDIF
		ENDIF
	ENDIF
	
	IF (NOT bEscDlgCleared) AND iEscUsedDrfCount >= 1 AND iEscUsedPlrCount >= 1 AND NOT IS_MESSAGE_BEING_DISPLAYED()
		bEscDlgCleared = TRUE
	ENDIF
ENDPROC
