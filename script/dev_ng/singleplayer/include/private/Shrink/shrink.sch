
//////////////////////////////////////////////////////////////////////
/* shrink.sch														*/
/* Author: DJ Jones													*/
/* Definitions for shrink activity (sessions with Dr. Friedlander).	*/
//////////////////////////////////////////////////////////////////////
 
USING "rage_builtins.sch"
USING "globals.sch"

CONST_INT iSHRINK_OFFICE_MODELS 6
CONST_INT iMAX_PROC_CAMERAS 10
CONST_INT iSHRINK_LONGEST_CONV_PRE_DECISION 47
CONST_INT iSHRINK_LONGEST_CONV_POST_DECISION 27

//Shrink session fees
CONST_INT iSHRINK_FEE_CHAOS			500
CONST_INT iSHRINK_FEE_EVIL 			1000
CONST_INT iSHRINK_FEE_NEGATIVITY 	1500
CONST_INT iSHRINK_FEE_FUCKED_UP 	1500
CONST_INT iSHRINK_FEE_ABANDONMENT 	2000

// State definitions for office visit.
ENUM SHRINK_OFFICE_STATE
	SHRINKOFFICESTATE_INIT,
	SHRINKOFFICESTATE_STREAMING,
	SHRINKOFFICESTATE_INTRO_CUTSCENE,
	
	SHRINKOFFICESTATE_PROCEDURAL,
	SHRINKOFFICESTATE_PROCEDURAL_INTRO, 	//The shrink ask Michael how he is doing. 
	SHRINKOFFICESTATE_PROCEDURAL_REPLY, 	//Michael replies to the doctor either good, okay, or bad and then the doctor replies.
	SHRINKOFFICESTATE_PROCEDURAL_MISSION, 	//Michael talks about the mission he last did.
	SHRINKOFFICESTATE_PROCEDURAL_OW_INTRO,  //Dr.Friedlander questions Michael about his open world activities.
	SHRINKOFFICESTATE_PROCEDURAL_OW,		//Michael talks about his recent activities and the doctor comments.
	SHRINKOFFICESTATE_PROCEDURAL_CHOOSE,	//Player chosses to either accept or reject the doctors advice
	SHRINKOFFICESTATE_PROCEDURAL_ACCEPT,    //Michael accepts advice
	SHRINKOFFICESTATE_PROCEDURAL_DENY,   	//Michael rejects advice
	SHRINKOFFICESTATE_PROCEDURAL_SEX,		//Michael talks about his sex life and the doctor comments
	SHRINKOFFICESTATE_PROCEDURAL_SUM,		//Michael sums up his experience
	
	SHRINKOFFICESTATE_OUTRO_CUTSCENE,
	SHRINKOFFICESTATE_KILL_DR_FRIEDLANDER,
	SHRINKOFFICESTATE_CLEANUP,
	SHRINKOFFICESTATE_KILL_CLEANUP,
	SHRINKOFFICESTATES
ENDENUM

// State definitions for phone call.
ENUM SHRINK_PHONE_STATE
	SHRINKPHONESTATE_INIT,
	SHRINKPHONESTATE_START_CONV,
	SHRINKPHONESTATE_BUSY,
	SHRINKPHONESTATE_CONV,
	SHRINKPHONESTATE_PASS,
	SHRINKPHONESTATE_FAIL,
	SHRINKPHONESTATES
ENDENUM

// State definitions for procedural dialogue (office or phone).
ENUM SHRINK_PROC_CONV_STATE
	SHRINKPROCCONVSTATE_DR_INTRO,
	SHRINKPROCCONVSTATE_MIC_INTRO,
	SHRINKPROCCONVSTATE_DR_TELL_MORE,
	SHRINKPROCCONVSTATE_BOTH_NARRATIVE,
	SHRINKPROCCONVSTATE_DR_OPEN_WORLD,
	SHRINKPROCCONVSTATE_BOTH_OW_BEHAVIOR,
	SHRINKPROCCONVSTATE_MIC_ACCEPT_DENY,
	SHRINKPROCCONVSTATE_FILLER,
	SHRINKPROCCONVSTATE_DR_SEX,
	SHRINKPROCCONVSTATE_BOTH_SEX,
	SHRINKPROCCONVSTATE_MIC_SUMMARY,
	SHRINKPROCCONVSTATES
ENDENUM

ENUM SHRINK_DECISION_RESULT
	SHRINK_ACCEPT,
	SHRINK_REJECT,
	SHRINK_SILENT
ENDENUM

// Flags to change behavior of the camera controller.
ENUM SHRINK_PROC_CAM_FLAG
	SHRINKPROCCAMFLAG_RANDOM,
	SHRINKPROCCAMFLAG_REPEAT,
	SHRINKPROCCAMFLAGS
ENDENUM

// Michael's possible conditions.
ENUM SHRINK_CONDITION
	SHRINKCONDITION_GOOD,
	SHRINKCONDITION_OKAY,
	SHRINKCONDITION_BAD,
	SHRINKCONDITIONS
ENDENUM

//Shrink Open world violence
ENUM SHRINK_OW_VIOLENCE_TYPES
	SHRINK_OW_VIOLENCE_TYPE_INVALID = -1,
	SHRINK_4STAR_WANTED,	
	SHRINK_RANDOM_KILLS,	
	SHRINK_2STAR_WANTED,	
	SHRINK_VEHICLE_KILLS,
	SHRINK_KILLS,		
	SHRINK_SHOP_ROBBERY,
	SHRINK_PED_ROBBERY,	
	SHRINK_STOLEN_CARS,	
	SHRINK_WANTED_IN_CAR,
	SHRINK_DANGEROUS_DRIVING,
	SHRINK_NUM_EVENTS
ENDENUM

// Information for remembering what Michael was wearing when he started the session.
STRUCT SHRINK_PLAYER_ATTIRE
	INT iHair
	INT iHairTexture
	INT iBeard
	INT iBrdTexture
	INT iAccessory
	INT iAccTexture
	INT iTaskItem
	INT iTskTexture
	INT iDecal
	INT iDclTexture
	INT iHeadProp
	INT iHeadTexture
	INT iEyeProp
	INT iEyeTexture
	INT iEarProp
ENDSTRUCT

// Information for a procedural conversation camera script. See shrink_camera.sch for accessors/mutators.
STRUCT SHRINK_PROC_CAM_SCRIPT
	VECTOR vCamPos[iMAX_PROC_CAMERAS]
	VECTOR vCamRot[iMAX_PROC_CAMERAS]
	FLOAT fCamFOV[iMAX_PROC_CAMERAS]
	FLOAT fCamDur[iMAX_PROC_CAMERAS]
	INT iValidCameras
	INT iFlags
ENDSTRUCT

// Info for a conversation with the shrink.
//  sIntroConvName - Name of intro cutscene (for office) or conversation (for phone).
//  sStoryConvName - Name of the story-specific conversation in D*.
//  sOutroConvName - Name of outro cutscene (for office) or conversation (for phone).
STRUCT SHRINK_SESSION_PROPERTIES
	TEXT_LABEL_23 sIntroConvName
	TEXT_LABEL_23 sStoryConvName
	TEXT_LABEL_23 sOutroConvName
	INT iOutroDuration
	INT iConvSexIndex
	SHRINK_DECISION_RESULT shrinkDecision
ENDSTRUCT

FUNC INT SHRINK_GET_SESSION_FEE(SHRINK_SESSION thisSession)
	SWITCH thisSession
		CASE SHRINKSESSION_OFFICE_CHAOS
			RETURN iSHRINK_FEE_CHAOS
		BREAK
		CASE SHRINKSESSION_OFFICE_EVIL
			RETURN iSHRINK_FEE_EVIL
		BREAK
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			RETURN iSHRINK_FEE_NEGATIVITY
		BREAK
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			RETURN iSHRINK_FEE_FUCKED_UP
		BREAK
		CASE SHRINKSESSION_OFFICE_ABANDONMENT
			RETURN iSHRINK_FEE_ABANDONMENT
		BREAK		
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC BOOL IS_SP_MISSION_ID_A_SHRINK_MISSION(SP_MISSIONS paramMissionID)
	SWITCH paramMissionID
		CASE SP_MISSION_SHRINK_1
		CASE SP_MISSION_SHRINK_2
		CASE SP_MISSION_SHRINK_3
		CASE SP_MISSION_SHRINK_4
		CASE SP_MISSION_SHRINK_5
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC SHRINK_SESSION GET_SHRINK_SESSION_ID_FOR_SP_MISSION_ID(SP_MISSIONS paramMissionID)
	SWITCH paramMissionID
		CASE SP_MISSION_SHRINK_1
			RETURN SHRINKSESSION_OFFICE_CHAOS
		BREAK
		CASE SP_MISSION_SHRINK_2
			RETURN SHRINKSESSION_OFFICE_EVIL
		BREAK
		CASE SP_MISSION_SHRINK_3
			RETURN SHRINKSESSION_PHONE_NEGATIVITY
		BREAK
		CASE SP_MISSION_SHRINK_4
			RETURN SHRINKSESSION_PHONE_FUCKED_UP
		BREAK
		CASE SP_MISSION_SHRINK_5
			RETURN SHRINKSESSION_OFFICE_ABANDONMENT
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_SHRINK_SESSION_ID_FOR_SP_MISSION_ID: The ID passed was not for a shrink session mission. Failed to return a valid session ENUM.")
	RETURN SHRINKSESSIONS
ENDFUNC

FUNC SP_MISSIONS GET_SP_MISSION_ID_FOR_SHRINK_SESSION_ID(SHRINK_SESSION paramSession)
	SWITCH paramSession
		CASE SHRINKSESSION_OFFICE_CHAOS
			RETURN SP_MISSION_SHRINK_1
		BREAK
		CASE SHRINKSESSION_OFFICE_EVIL
			RETURN SP_MISSION_SHRINK_2
		BREAK
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			RETURN SP_MISSION_SHRINK_3
		BREAK
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			RETURN SP_MISSION_SHRINK_4
		BREAK
		CASE SHRINKSESSION_OFFICE_ABANDONMENT
			RETURN SP_MISSION_SHRINK_5
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_SP_MISSION_ID_FOR_SHRINK_SESSION_ID: Invalid session ID passed. Could not return a valid SP mission ID.")
	RETURN SP_MISSION_NONE
ENDFUNC

