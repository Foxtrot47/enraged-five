
//////////////////////////////////////////////////////////////////////
/* shrink_core.sch													*/
/* Author: DJ Jones													*/
/* Core init & cleanup for shrink activity.							*/
//////////////////////////////////////////////////////////////////////


// Pre-streaming initialization.
PROC SHRINK_OFFICE_INIT(STREAMED_MODEL& modelStreams[], MODEL_NAMES shrinkModel, MODEL_NAMES shrinkIGModelName, MODEL_NAMES carModel, MODEL_NAMES nextDoorCarModel, MODEL_NAMES chairModel, SHRINK_SESSION eSession)
	// Streaming requests.
	ADD_STREAMED_MODEL(modelStreams, shrinkModel)
	ADD_STREAMED_MODEL(modelStreams, carModel)
	ADD_STREAMED_MODEL(modelStreams, shrinkIGModelName)
	ADD_STREAMED_MODEL(modelStreams, chairModel)
	
	IF eSession = SHRINKSESSION_OFFICE_ABANDONMENT
		ADD_STREAMED_MODEL(modelStreams, nextDoorCarModel)
	ENDIF
	
	REQUEST_ALL_MODELS(modelStreams)
	REQUEST_ADDITIONAL_TEXT("SHRINK", MISSION_TEXT_SLOT)
	LOAD_STREAM("Office_Background_Stream", "SHRINK_SOUNDS")
	
	// Approximate first shot of cutscenes
	NEW_LOAD_SCENE_START(<<-1904.2, -573.2, 19.6>>, <<-0.2, 0.0, -59.4>>, 10.0)
	
	#IF iSHRINK_CUTSCENE
	REQUEST_SHRINK_IDLE_ANIMS()
	#ENDIF
ENDPROC

// Build the conversation and get the player ready. Returns false if we should just do a busy signal instead.
FUNC BOOL SHRINK_PHONE_INIT_BROKE(structPedsForConversation& convPeds, TEXT_LABEL_15& sThisRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15& sThisSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iThisLineCounter)
	IF g_iShrinkBrokeConvTimestamp <> 0 AND GET_GAME_TIMER() - g_iShrinkBrokeConvTimestamp < 30*60*1000 // 30 minutes
		RETURN FALSE
	ENDIF
	
	REQUEST_ADDITIONAL_TEXT("SHRINK", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("DrfAud", MISSION_DIALOGUE_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	// Phone conversation intro.
	IF NOT SHRINK_ADD_PHONE_CONV_BROKE(sThisRoots, sThisSpecifics, iThisLineCounter)
		RETURN FALSE
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(convPeds, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(convPeds, 1, NULL, "FRIEDLANDER")
	
	g_iShrinkBrokeConvTimestamp = GET_GAME_TIMER()
	
	RETURN TRUE
ENDFUNC

// Build the conversation and get the player ready.
PROC SHRINK_PHONE_INIT(structPedsForConversation& convPeds, TEXT_LABEL_15& sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION],    TEXT_LABEL_15& sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION],    INT& iLineCounter, 
															TEXT_LABEL_15& sYesRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sYesSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], INT& iYesLineCounter, 
															TEXT_LABEL_15& sNoRoots[iSHRINK_LONGEST_CONV_POST_DECISION],  TEXT_LABEL_15& sNoSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION],  INT& iNoLineCounter, 
															SHRINK_SESSION_PROPERTIES &thisSessionProperties, SHRINK_SESSION thisSession)
	
	SHRINK_CONDITION myCondition
	TEXT_LABEL_15 sConvLabel
	INT iStartLine, iLinesInConv
	
	REQUEST_ADDITIONAL_TEXT("SHRINK", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("DrfAud", MISSION_DIALOGUE_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	// Add dialogue peds (player only; he's on a cellphone).
	ADD_PED_FOR_DIALOGUE(convPeds, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(convPeds, 1, NULL, "FRIEDLANDER")
	
	// Phone conversation intro.
	SHRINK_ADD_PHONE_CONV_INTRO(sRoots, sSpecifics, iLineCounter, thisSession)
	
	// Michael responds that he is good, okay, or bad. Dr. Friedlander responds and asks for more details.
	sConvLabel = SHRINK_GET_NARRATIVE_CONVERSATION(thisSession, iStartLine, iLinesInConv)
	myCondition = SHRINK_GET_NARRATIVE_CONDITION(sConvLabel)
	SHRINK_ADD_CONDITIONAL_EXCHANGE(sRoots, sSpecifics, iLineCounter, myCondition)
	
	// Michael describes the current situation in the main story and Dr. Friedlander responds.
	SHRINK_ADD_PHONE_CONV_NARRATIVE(sRoots, sSpecifics, iLineCounter, thisSession)
	
	// Michael talks about his open world behavior and Dr. Friedlander gives him some advice.
	SHRINK_ADD_PHONE_CONV_OPEN_WORLD(sRoots, sSpecifics, iLineCounter, thisSession)
	
	// Michael accepts or rejects Dr. Friedlander's advice.
	SHRINK_ADD_ACCEPT_DENY_LINES(sYesRoots, sYesSpecifics, sNoRoots, sNoSpecifics, iYesLineCounter, iNoLineCounter, thisSession)
	
	// Michael describes his recent sexual behavior and Dr. Friedlander responds.
	SHRINK_ADD_PHONE_CONV_SEX(sYesRoots, sYesSpecifics, sNoRoots, sNoSpecifics, iYesLineCounter, iNoLineCounter, thisSessionProperties, thisSession)
	
	// Michael summarizes his condition (primarily based on sexual behavior).
	SHRINK_ADD_PHONE_CONV_SUMMARY(sYesRoots, sYesSpecifics, sNoRoots, sNoSpecifics, iYesLineCounter, iNoLineCounter, thisSessionProperties)
	
	// Phone conversation outro.
	SHRINK_ADD_PHONE_CONV_OUTRO(sYesRoots, sYesSpecifics, sNoRoots, sNoSpecifics, iYesLineCounter, iNoLineCounter, thisSession)
	
	// Debug print the conversation.
	CERRORLN(DEBUG_MISSION, "Conversation has been determined.")
	CERRORLN(DEBUG_MISSION, "FIRST HALF:")
	
	INT iCounter
	REPEAT iSHRINK_LONGEST_CONV_PRE_DECISION iCounter
		IF IS_STRING_NULL_OR_EMPTY(sRoots[iCounter])
			iCounter = iSHRINK_LONGEST_CONV_PRE_DECISION
		ELSE
			CERRORLN(DEBUG_MISSION, "sRoots[", iCounter, "]: ", sRoots[iCounter], ", sSpecifics[", iCounter, "]: ", sSpecifics[iCounter])
		ENDIF
	ENDREPEAT
	
	CERRORLN(DEBUG_MISSION, "ACCEPT ADVICE:")
	REPEAT iSHRINK_LONGEST_CONV_POST_DECISION iCounter
		IF IS_STRING_NULL_OR_EMPTY(sYesRoots[iCounter])
			iCounter = iSHRINK_LONGEST_CONV_POST_DECISION
		ELSE
			CERRORLN(DEBUG_MISSION, "sYesRoots[", iCounter, "]: ", sYesRoots[iCounter], ", sYesSpecifics[", iCounter, "]: ", sYesSpecifics[iCounter])
		ENDIF
	ENDREPEAT
	
	CERRORLN(DEBUG_MISSION, "REJECT ADVICE:")
	REPEAT iSHRINK_LONGEST_CONV_POST_DECISION iCounter
		IF IS_STRING_NULL_OR_EMPTY(sNoRoots[iCounter])
			iCounter = iSHRINK_LONGEST_CONV_POST_DECISION
		ELSE
			CERRORLN(DEBUG_MISSION, "sNoRoots[", iCounter, "]: ", sNoRoots[iCounter], ", sNoSpecifics[", iCounter, "]: ", sNoSpecifics[iCounter])
		ENDIF
	ENDREPEAT
ENDPROC

// Check for streaming and related init.
FUNC BOOL SHRINK_IS_OFFICE_STREAMING_COMPLETE(STREAMED_MODEL& modelStreams[])
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 sWaitReason
	#ENDIF
	
	IF NOT LOAD_STREAM("Office_Background_Stream", "SHRINK_SOUNDS")
		RETURN FALSE
	ENDIF
	
	IF ARE_MODELS_STREAMED(modelStreams)
		//IF HAS_CUTSCENE_LOADED() // Use failsafe as fallback if this ever needs to go back in
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
//			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
//				IF IS_INTERIOR_READY(shrinkInterior)
//					
//					PIN_INTERIOR_IN_MEMORY(shrinkInterior)
					DEBUG_MESSAGE("Streaming complete.")
					RETURN TRUE
//					
//				#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
//					sWaitReason = "interior" #ENDIF	
//				ENDIF
			#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
				sWaitReason = "mission text" #ENDIF
			ENDIF 
		//#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
		//	sWaitReason = "cutscene" #ENDIF
		//ENDIF
	#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
		sWaitReason = "models" #ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
		DEBUG_MESSAGE("Streaming: Waiting for ", sWaitReason, ".")
	ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

// Returns true if a given accessory would create problems with cutscene animations.
FUNC BOOL SHRINK_IS_ACCESSORY_PROBLEMATIC(INT iAccessory)
	SWITCH iAccessory
		CASE  1
		CASE  3
		CASE  5
		CASE  7
		CASE  9
		CASE 12
		CASE 14
		CASE 15
		CASE 21
		CASE 22
		CASE 23
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Returns true if a given task item would create problems with cutscene animations.
FUNC BOOL SHRINK_IS_TASK_ITEM_PROBLEMATIC(INT iTaskItem)
	SWITCH iTaskItem
		CASE  1
		CASE  2
		CASE  5
		CASE  7
		CASE  8
		CASE  9
		CASE 10
		CASE 11
		CASE 12
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Returns true if a given decal would create problems with cutscene animations.
FUNC BOOL SHRINK_IS_DECAL_PROBLEMATIC(INT iDecal)
	RETURN iDecal = 11
ENDFUNC

// Get delay before restoring Michael's attire from the start of the last cutscene section.
FUNC FLOAT SHRINK_GET_ATTIRE_RESTORE_DELAY(SHRINK_SESSION eSession)
	SWITCH eSession
		CASE SHRINKSESSION_OFFICE_CHAOS       RETURN 103.3
		CASE SHRINKSESSION_OFFICE_EVIL        RETURN  55.7
		CASE SHRINKSESSION_OFFICE_ABANDONMENT RETURN  62.2
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

// Get ready to start the intro cutscene.
PROC SHRINK_OFFICE_SETUP_INTRO(PED_INDEX& shrinkCSPed, MODEL_NAMES shrinkCSModelName, VECTOR vShrinkPos, FLOAT fShrinkHeading, INT iUpperVariation, INT iUpperTexture, INT iLowerTexture)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(shrinkCSPed) OR IS_ENTITY_DEAD(shrinkCSPed)
		shrinkCSPed = CREATE_PED(PEDTYPE_MISSION, shrinkCSModelName, vShrinkPos, fShrinkHeading)
	ENDIF
	
	SET_PED_COMPONENT_VARIATION(shrinkCSPed, PED_COMP_TORSO, iUpperVariation, iUpperTexture)
	SET_PED_COMPONENT_VARIATION(shrinkCSPed, PED_COMP_LEG, 0, iLowerTexture)
	
	SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, FALSE)
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
ENDPROC

// Clean up assets and place the player on the street.
PROC SHRINK_OFFICE_CLEANUP(PED_INDEX& shrinkCSPed, VEHICLE_INDEX& playerVehicle, CAMERA_INDEX shrinkMainCam, SHRINK_OFFICE_STATE eOfficeState)
	// Restore UI.
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	// Stop the current conversation.
	//STOP_SCRIPTED_CONVERSATION(FALSE)
	KILL_ANY_CONVERSATION()
	
	IF DOES_ENTITY_EXIST(playerVehicle)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(playerVehicle)
	ENDIF
	
	// Clean up the doctor.
	IF DOES_ENTITY_EXIST(shrinkCSPed) AND (NOT IS_ENTITY_DEAD(shrinkCSPed))
		CLEAR_PED_TASKS(shrinkCSPed)
		FREEZE_ENTITY_POSITION(shrinkCSPed, FALSE)
	ENDIF
	
	// Restore game camera.
	//RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	// Return player control to normal.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		ENDIF
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS()
	SHRINK_CLEAR_ALL_SEX_TIMESTAMPS()
	
	IF eOfficeState >= SHRINKOFFICESTATE_KILL_DR_FRIEDLANDER
		SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, TRUE)
		IF DOES_CAM_EXIST(shrinkMainCam)
			DESTROY_CAM(shrinkMainCam)
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF
	
	// End the script.
	TERMINATE_THIS_THREAD()
ENDPROC

// Clean up assets and put away the phone.
PROC SHRINK_PHONE_CLEANUP()
	// Stop the current conversation.
	//STOP_SCRIPTED_CONVERSATION(FALSE)
	KILL_ANY_CONVERSATION()
	//Clear all stored timestamps
	SHRINK_CLEAR_ALL_OW_VIOLENCE_TIMESTAMPS()
	SHRINK_CLEAR_ALL_SEX_TIMESTAMPS()
	// End the script.
	TERMINATE_THIS_THREAD()
ENDPROC
