PROC REQUEST_SHRINK_IDLE_ANIMS()
	REQUEST_ANIM_DICT("missdrfriedlanderdrf_idles")
	REQUEST_ANIM_DICT("facials@drf")
ENDPROC

FUNC BOOL HAS_SHRINK_IDLE_ANIMS_LOADED()
	RETURN HAS_ANIM_DICT_LOADED("missdrfriedlanderdrf_idles") AND HAS_ANIM_DICT_LOADED("facials@drf")
ENDFUNC

PROC REMOVE_SHRINK_IDLE_ANIMS()
	REMOVE_ANIM_DICT("missdrfriedlanderdrf_idles")
	REMOVE_ANIM_DICT("facials@drf")
ENDPROC

FUNC TEXT_LABEL_31 PICK_FILLER_ANIM(SHRINK_SESSION thisSession)
	
	TEXT_LABEL_31 textReturn
	
	SWITCH thisSession
		CASE SHRINKSESSION_OFFICE_CHAOS
			textReturn = "hand2chin_michael"
		BREAK
		
		CASE SHRINKSESSION_OFFICE_EVIL
			textReturn = "hand2chin_michael"
		BREAK
		
		CASE SHRINKSESSION_OFFICE_ABANDONMENT
			textReturn = "leanback_michael"
		BREAK
		
		DEFAULT
			textReturn = "uneasy_michael"
		BREAK
	ENDSWITCH

	RETURN textReturn
ENDFUNC

FUNC BOOL PLAY_FILLER_ANIMS(SHRINK_SESSION thisSession, PED_INDEX shrinkCSPed, CAMERA_INDEX& shrinkMainCam)
	
	ANIMATION_FLAGS animFlags = AF_NOT_INTERRUPTABLE | AF_TURN_OFF_COLLISION | AF_HOLD_LAST_FRAME
	TEXT_LABEL_31 sAnimDict = "missdrfriedlanderdrf_idles"
	TEXT_LABEL_15 sAnimDictFace = "facials@drf"
	
	TEXT_LABEL_31 sMichaelIdle = PICK_FILLER_ANIM(thisSession)
	
	TEXT_LABEL_15 sShrinkIdle = "drf_idle_drf"
	TEXT_LABEL_23 sMichaelIdleFace = "michael_concerned_01"
	TEXT_LABEL_23 sShrinkIdleFace = "drfriedlander_idle_01"
	
	IF HAS_SHRINK_IDLE_ANIMS_LOADED()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(shrinkCSPed)
			IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sMichaelIdle)
				
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1907.39, -577.33, 17.64>>)
//				SET_ENTITY_COORDS(shrinkCSPed, <<-1907.69, -574.77, 17.67>>)
	//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 318.6574)
	//			SET_ENTITY_HEADING(shrinkCSPed, -3.0)
				
				TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, sMichaelIdle, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, animFlags)
				TASK_PLAY_ANIM(shrinkCSPed,       sAnimDict, sShrinkIdle,  REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, animFlags)
				
				TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDictFace, sMichaelIdleFace, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | animFlags, DEFAULT, DEFAULT, AIK_DISABLE_HEAD_IK)
				TASK_PLAY_ANIM(shrinkCSPed,       sAnimDictFace, sShrinkIdleFace,  REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | animFlags)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				FORCE_PED_AI_AND_ANIMATION_UPDATE(shrinkCSPed)
				
				IF DOES_CAM_EXIST(shrinkMainCam)
					DESTROY_CAM(shrinkMainCam)
				ENDIF
				IF NOT DOES_CAM_EXIST(shrinkMainCam)
					shrinkMainCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
				ENDIF
				
				SET_CAM_COORD(shrinkMainCam, <<-1906.8422, -572.3835, 19.1046>>)
				SET_CAM_ROT(shrinkMainCam, <<-1.5254, 0.0003, 171.4657>>)
				SET_CAM_FOV(shrinkMainCam, 17.0)
				SET_CAM_ACTIVE(shrinkMainCam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, sMichaelIdle) >= 0.99
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_IDLE_ANIMS(PED_INDEX shrinkCSPed, BOOL bMichael, BOOL bFriedlander)
	ANIMATION_FLAGS animFlags = AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_TURN_OFF_COLLISION
	
	TEXT_LABEL_31 sAnimDict = "missdrfriedlanderdrf_idles"
	TEXT_LABEL_15 sAnimDictFace = "facials@drf"
	TEXT_LABEL_15 sMichaelIdle = "idle_michael"
	TEXT_LABEL_15 sShrinkIdle = "drf_idle_drf"
	TEXT_LABEL_15 sMichaelIdleFace = "michael_idle_01"
	TEXT_LABEL_23 sShrinkIdleFace = "drfriedlander_idle_01"
	
	IF bMichael AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1907.39, -577.33, 17.64>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 318.6574)
		TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, sMichaelIdle, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, animFlags)
		TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDictFace, sMichaelIdleFace, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY | animFlags, DEFAULT, DEFAULT, AIK_DISABLE_HEAD_IK)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	ENDIF
	
	IF bFriedlander AND NOT IS_PED_INJURED(shrinkCSPed)
		SET_ENTITY_COORDS(shrinkCSPed, <<-1907.69, -574.77, 17.67>>)
		SET_ENTITY_HEADING(shrinkCSPed, 177.0)
		TASK_PLAY_ANIM(shrinkCSPed, sAnimDict, sShrinkIdle, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, animFlags)
		TASK_PLAY_ANIM(shrinkCSPed, sAnimDictFace, sShrinkIdleFace, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY | animFlags)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(shrinkCSPed)
	ENDIF
ENDPROC
