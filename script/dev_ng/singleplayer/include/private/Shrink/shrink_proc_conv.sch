
//////////////////////////////////////////////////////////////////////
/* shrink_proc_conv.sch												*/
/* Author: DJ Jones													*/
/* Procedural conversation functionality for shrink activity.		*/
//////////////////////////////////////////////////////////////////////


//USING "commands_misc.sch"
USING "flow_mission_data_public.sch"
USING "shrink_stat_tracking.sch"
USING "script_usecontext.sch"
USING "minigames_helpers.sch"


CONST_INT DRF_USE_OLD_DIALOGUE 0
CONST_INT DRF_USE_NEW_DIALOGUE 1

#IF NOT DEFINED(iSHRINK_CUTSCENE)
	CONST_INT iSHRINK_CUTSCENE 0
#ENDIF

#IF iSHRINK_CUTSCENE
#IF IS_DEBUG_BUILD
//	WIDGET_GROUP_ID parentWidgets
//	WIDGET_GROUP_ID violenceWidgets
//	WIDGET_GROUP_ID sexWidgets
	BOOL bForceViolence[11], bLastForceViolence[11], bForceViolenceOff
	BOOL bForceSex[12], bLastForceSex[12], bForceSexOff
	BOOL bLaunchVisit
#ENDIF
#ENDIF


//get narrative conversation
FUNC CUTSCENE_SECTION SHRINK_GET_NARRATIVE_CUTSCENE_SECTION(SHRINK_SESSION thisSession, BOOL bForceActiveReturn = FALSE)
	SWITCH thisSession
		CASE SHRINKSESSION_OFFICE_CHAOS
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1)
					RETURN CS_SECTION_6
				ELSE
					RETURN CS_SECTION_5
				ENDIF
			ENDIF
		BREAK
		
		CASE SHRINKSESSION_OFFICE_EVIL
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_SOLOMON_1)
				// Special rule: Unavailable between Family 5 and Port of LS Heist Setup
				IF (NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5))
				OR GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_PREP_1)
				OR GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_PREP_2B)
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
						RETURN CS_SECTION_7
					ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_1)
						RETURN CS_SECTION_6
					ELSE
						RETURN CS_SECTION_5
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SHRINKSESSION_OFFICE_ABANDONMENT
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_3)
			AND GET_MISSION_COMPLETE_STATE(SP_MISSION_SOLOMON_3)
			AND GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_C2)
					RETURN CS_SECTION_7
				ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_A)
					RETURN CS_SECTION_6	
				// Special rule: Unavailable between Big One finale and Story Finale
				ELIF (NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2A)
				AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2B))
					RETURN CS_SECTION_5
				ENDIF
			ENDIF
		BREAK		
	ENDSWITCH
	
	IF bForceActiveReturn
		CASSERTLN(DEBUG_MISSION, "Shrink office visit has been forced to use invalid narrative dialogue! This is bad!")
		RETURN CS_SECTION_5
	ENDIF
	
	RETURN CS_SECTION_1
ENDFUNC

FUNC TEXT_LABEL_15 SHRINK_GET_NARRATIVE_CONVERSATION(SHRINK_SESSION thisSession, INT& iStartLine, INT& iNumLines, BOOL bUpdateLastLine = TRUE)
	TEXT_LABEL_15 sConversation
	
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_SOLOMON_1)
	AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_MARTIN_1)
		IF thisSession = SHRINKSESSION_PHONE_NEGATIVITY
			sConversation = "DRF_SOL1"
			iStartLine = 0
			iNumLines = 19
			IF bUpdateLastLine
				SHRINK_SET_LAST_NARRATIVE(g_savedGlobals.sShrinkData, 6)
			ENDIF
		ENDIF
	
	ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_1)
	AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_3)
		sConversation = "DRF_EXILE"
		iStartLine = 0
		iNumLines = 9
		IF bUpdateLastLine
			SHRINK_SET_LAST_NARRATIVE(g_savedGlobals.sShrinkData, 7)
		ENDIF
	
	ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_3)
	AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_5)
		sConversation = "DRF_EXILE"
		iStartLine = 9
		iNumLines = 10
		IF bUpdateLastLine
			SHRINK_SET_LAST_NARRATIVE(g_savedGlobals.sShrinkData, 8)
		ENDIF
	
	ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_5)
	AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_2)
	AND NOT IS_COMMUNICATION_REGISTERED(CHAT_SOL02)
		sConversation = "DRF_SOL2"
		iStartLine = 0
		iNumLines = 15
		IF bUpdateLastLine
			SHRINK_SET_LAST_NARRATIVE(g_savedGlobals.sShrinkData, 9)
		ENDIF
	
	ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_2)
	AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
		sConversation = "DRF_MIC2"
		iStartLine = 0
		iNumLines = 13
		IF bUpdateLastLine
			SHRINK_SET_LAST_NARRATIVE(g_savedGlobals.sShrinkData, 10)
		ENDIF
	ENDIF
	
	RETURN sConversation
ENDFUNC

FUNC INT SHRINK_GET_NARRATIVE_INDEX(STRING sConvRoot, INT iStartLine)
	INT iRet = -1
	
	IF IS_STRING_NULL_OR_EMPTY(sConvRoot)
		RETURN iRet
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sConvRoot, "DRF_SOL1")
		iRet = 6
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "DRF_EXILE")
		IF iStartLine > 0
			iRet = 8
		ELSE
			iRet = 7
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "DRF_SOL2")
		iRet = 9
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "DRF_MIC2")
		iRet = 10
	ENDIF
	RETURN iRet
ENDFUNC

FUNC SHRINK_CONDITION SHRINK_GET_NARRATIVE_CONDITION(STRING sConvRoot)
	SHRINK_CONDITION eRet = SHRINKCONDITION_BAD
	IF IS_STRING_NULL_OR_EMPTY(sConvRoot)
		RETURN eRet
	ENDIF
	IF ARE_STRINGS_EQUAL(sConvRoot, "DRF_SOL1")
		eRet = SHRINKCONDITION_OKAY
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "DRF_EXILE")
		eRet = SHRINKCONDITION_BAD
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "DRF_SOL2")
		eRet = SHRINKCONDITION_OKAY
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "DRF_MIC2")
		eRet = SHRINKCONDITION_OKAY
	ENDIF
	RETURN eRet
ENDFUNC

PROC APPEND_SHRINK_LINE_TO_CONVO_LABEL(TEXT_LABEL_15 &sLine, SHRINK_SESSION thisSession)
	IF thisSession = SHRINKSESSION_OFFICE_CHAOS
		sLine += "_1"
	ELIF thisSession = SHRINKSESSION_OFFICE_EVIL
		sLine += "_2"
	ELIF thisSession = SHRINKSESSION_PHONE_NEGATIVITY
		sLine += "_3"
	ELIF thisSession = SHRINKSESSION_PHONE_FUCKED_UP
		sLine += "_4"
	ELIF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
		sLine += "_5"
	ENDIF
ENDPROC

PROC APPEND_SHRINK_LINE_TO_CONVO_LABEL_GOOD_BAD(TEXT_LABEL_15 &sLine, SHRINK_SESSION thisSession)
	IF thisSession = SHRINKSESSION_OFFICE_CHAOS
		sLine += "_1"
	ELIF thisSession = SHRINKSESSION_OFFICE_EVIL
		sLine += "_2"
	ELIF thisSession = SHRINKSESSION_PHONE_NEGATIVITY
		sLine += "_3"
	ELIF thisSession = SHRINKSESSION_PHONE_FUCKED_UP
		sLine += "_1"
	ELIF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
		sLine += "_2"
	ENDIF
ENDPROC

PROC APPEND_SHRINK_LINE_TO_CONVO_LABEL_ACCEPT_REJECT(TEXT_LABEL_15 &sLine, SHRINK_SESSION thisSession)
	IF thisSession = SHRINKSESSION_OFFICE_CHAOS
		sLine += "_1"
	ELIF thisSession = SHRINKSESSION_OFFICE_EVIL
		sLine += "_2"
	ELIF thisSession = SHRINKSESSION_PHONE_NEGATIVITY
		sLine += "_5"
	ELIF thisSession = SHRINKSESSION_PHONE_FUCKED_UP
		sLine += "_4"
	ELIF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
		sLine += "_3"
	ENDIF
ENDPROC

PROC APPEND_SHRINK_LINE_TO_CONVO_LABEL_MIC_SUM(TEXT_LABEL_15 &sLine, SHRINK_SESSION thisSession)
	IF thisSession = SHRINKSESSION_OFFICE_CHAOS
		sLine += "_1"
	ELIF thisSession = SHRINKSESSION_OFFICE_EVIL
		sLine += "_2"
	ELIF thisSession = SHRINKSESSION_PHONE_NEGATIVITY
		sLine += "_3"
	ELIF thisSession = SHRINKSESSION_PHONE_FUCKED_UP
		sLine += "_4"
	ELIF thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
		sLine += "_9"
	ENDIF
ENDPROC

#IF NOT DEFINED(iSHRINK_CUTSCENE)
CONST_INT iSHRINK_CUTSCENE	0
#ENDIF

// Set dlg sex dialog index that was picked randomly to determine response
PROC SHRINK_SET_SEXUAL_CONVERSATION_INDEX(SHRINK_SESSION_PROPERTIES &thisSessionProperties, INT iSexConvIndex)
	thisSessionProperties.iConvSexIndex = iSexConvIndex
ENDPROC

// Get dlg sex dialog index that was picked randomly to determine response
FUNC INT SHRINK_GET_SEXUAL_CONVERSATION_INDEX(SHRINK_SESSION_PROPERTIES thisSessionProperties)
	RETURN thisSessionProperties.iConvSexIndex
ENDFUNC

FUNC INT SHRINK_GET_UNUSED_SEX_CONVERSATION(INT iCandidate1, INT iCandidate2, INT iCandidate3 = 0, INT iCandidate4 = 0, INT iCandidate5 = 0)
	INT iCounter, iRet
	INT iMask[13]
	
	REPEAT 13 iCounter
		iMask[iCounter] = iCounter
	ENDREPEAT
	IF iCandidate1 > 0 AND iCandidate1 < 13 AND NOT SHRINK_IS_SEX_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate1)
		iMask[iCandidate1] =  0
	ELIF iCandidate2 > 0 AND iCandidate2 < 13 AND NOT SHRINK_IS_SEX_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate2)
		iMask[iCandidate2] =  0
	ELIF iCandidate3 > 0 AND iCandidate3 < 13 AND NOT SHRINK_IS_SEX_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate3)
		iMask[iCandidate3] =  0
	ELIF iCandidate4 > 0 AND iCandidate4 < 13 AND NOT SHRINK_IS_SEX_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate4)
		iMask[iCandidate4] =  0
	ELIF iCandidate5 > 0 AND iCandidate5 < 13 AND NOT SHRINK_IS_SEX_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate5)
		iMask[iCandidate5] =  0
	ENDIF
	
	iRet = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(iMask, 13, 1, 13)
	
	IF iRet > 0 
		RETURN iRet
	ENDIF
	
	IF iCandidate3 <= 0 OR iCandidate3 >= 13
		iRet = GET_RANDOM_INT_IN_RANGE(1,3)
	ELIF iCandidate4 <= 0 OR iCandidate4 >= 13
		iRet = GET_RANDOM_INT_IN_RANGE(1,4)
	ELIF iCandidate5 <= 0 OR iCandidate5 >= 13
		iRet = GET_RANDOM_INT_IN_RANGE(1,5)
	ELSE
		iRet = GET_RANDOM_INT_IN_RANGE(1,6)
	ENDIF
	
	SWITCH iRet
		CASE 1 RETURN iCandidate1
		CASE 2 RETURN iCandidate2
		CASE 3 RETURN iCandidate3
		CASE 4 RETURN iCandidate4
		CASE 5 RETURN iCandidate5
	ENDSWITCH
	
	RETURN 0
ENDFUNC
FUNC INT SHRINK_GET_UNUSED_VIOLENCE_CONVERSATION(INT iCandidate1, INT iCandidate2, INT iCandidate3 = 0, INT iCandidate4 = 0, INT iCandidate5 = 0)
	INT iCounter, iRet
	INT iMask[12]
	BOOL bFoundUnused
	
	REPEAT 12 iCounter
		iMask[iCounter] = iCounter
	ENDREPEAT
	
	IF iCandidate1 > 0 AND iCandidate1 < 12 AND NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate1)
		PRINTLN("Bit ", iCandidate1, " has not been used")
		iMask[iCandidate1] =  0
		bFoundUnused = TRUE
	ENDIF
	
	IF iCandidate2 > 0 AND iCandidate2 < 12 AND NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate2)
		PRINTLN("Bit ", iCandidate2, " has not been used")
		iMask[iCandidate2] =  0
		bFoundUnused = TRUE
	ENDIF
	
	IF iCandidate3 > 0 AND iCandidate3 < 12 AND NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate3)
		PRINTLN("Bit ", iCandidate3, " has not been used")
		iMask[iCandidate3] =  0
		bFoundUnused = TRUE
	ENDIF
	
	IF iCandidate4 > 0 AND iCandidate4 < 12 AND NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate4)
		PRINTLN("Bit ", iCandidate4, " has not been used")
		iMask[iCandidate4] =  0
		bFoundUnused = TRUE
	ENDIF
	
	IF iCandidate5 > 0 AND iCandidate5 < 12 AND NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, iCandidate5)
		PRINTLN("Bit ", iCandidate5, " has not been used")
		iMask[iCandidate5] =  0
		bFoundUnused = TRUE
	ENDIF
	
	IF NOT bFoundUnused
		IF iCandidate1 > 0 AND iCandidate1 < 12
			iMask[iCandidate1] =  0
		ENDIF
		
		IF iCandidate2 > 0 AND iCandidate2 < 12
			iMask[iCandidate2] =  0
		ENDIF
		
		IF iCandidate3 > 0 AND iCandidate3 < 12
			iMask[iCandidate3] =  0
		ENDIF
		
		IF iCandidate4 > 0 AND iCandidate4 < 12
			iMask[iCandidate4] =  0
		ENDIF
		
		IF iCandidate5 > 0 AND iCandidate5 < 12
			iMask[iCandidate5] =  0
		ENDIF
	ENDIF
	
	iRet = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(iMask, 12, 1, 12)
	
	PRINTLN("iRet ", iRet)
	IF iRet > 0 
		RETURN iRet
	ENDIF
	
	iRet = GET_RANDOM_INT_IN_RANGE(1,3)
	
	
	SWITCH iRet
		CASE 1 RETURN iCandidate1
		CASE 2 RETURN iCandidate2
	ENDSWITCH
	
	RETURN 0
ENDFUNC
// Get an appropriate response for Michael based on his recent sexual behavior (strippers & prostitutes).
FUNC TEXT_LABEL_15 SHRINK_GET_SEXUAL_CONVERSATION(SHRINK_SESSION_PROPERTIES &thisSessionProperties, INT& iNumLines, SHRINK_SESSION thisSession, BOOL bSetUsed = TRUE)
	TEXT_LABEL_15 sConversation
	INT iConversation
	
	IF SHRINK_GET_SEX_ACT_TIMESTAMP_COUNT() <= 0
		IF NOT SHRINK_WAS_STRIP_CLUB_VISITED_RECENTLY()
			iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(1, 2, 3, 4, 12)
		ELIF SHRINK_GET_LAP_DANCE_TIMESTAMP_COUNT() >= 1
			//use sex 5,6 OR 7
			iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(5, 6, 7)
		ELSE //just visiting	
			//use 5 OR 7
			iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(5, 7)
		ENDIF	
	ELIF SHRINK_GET_SEX_ACT_TIMESTAMP_COUNT() >= 2
		//sex 10 OR 11
		iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(10, 11)
	ELIF NOT SHRINK_WAS_STRIP_CLUB_VISITED_RECENTLY()
		IF thisSession <> SHRINKSESSION_OFFICE_ABANDONMENT
			//sex 8 or 9
			iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(8, 9)
		ELSE
			iConversation = 9
		ENDIF
	ELSE
		IF thisSession <> SHRINKSESSION_OFFICE_ABANDONMENT
			//sex 8,9, OR 10	
			iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(8, 9, 10)
		ELSE
			iConversation = SHRINK_GET_UNUSED_SEX_CONVERSATION(9, 10)
		ENDIF
	ENDIF
	
	SHRINK_SET_SEXUAL_CONVERSATION_INDEX(thisSessionProperties, iConversation)
	IF bSetUsed
		SHRINK_SET_SEX_RESPONSE_USED(g_savedGlobals.sShrinkData, iConversation, TRUE)
	ENDIF
	
	#IF iSHRINK_CUTSCENE
		sConversation = "OBTSX"
		IF iConversation < 10
			sConversation += "0"
		ENDIF
		sConversation += iConversation
		UNUSED_PARAMETER(iNumLines)
	#ENDIF
	
	#IF NOT iSHRINK_CUTSCENE
		sConversation = "DRF_PR_"
		SWITCH iConversation
			CASE  1 sConversation += "1_A"   iNumLines = 3 BREAK
			CASE  2 sConversation += "1_B"   iNumLines = 5 BREAK
			CASE  3 sConversation += "1_C"   iNumLines = 3 BREAK
			CASE  4 sConversation += "1_D"   iNumLines = 3 BREAK
			CASE  5 sConversation += "1_STR" iNumLines = 3 BREAK
			CASE  6 sConversation += "5_T1B" iNumLines = 5 BREAK
			CASE  7 sConversation  = "5_T1C" iNumLines = 4 BREAK
			CASE  8 sConversation += "5PROA" iNumLines = 5 BREAK
			CASE  9 sConversation += "5PROB" iNumLines = 5 BREAK
			CASE 10 sConversation += "5STRA" iNumLines = 7 BREAK
			CASE 11 sConversation += "5STRB" iNumLines = 4 BREAK
			CASE 12 sConversation += "5_NON" iNumLines = 5 BREAK
		ENDSWITCH
	#ENDIF
	
	RETURN sConversation
ENDFUNC

FUNC CUTSCENE_SECTION SHRINK_GET_SEXUAL_SECTION(SHRINK_SESSION_PROPERTIES &thisSessionProperties, SHRINK_SESSION thisSession)
	INT iNumLines
	TEXT_LABEL_15 sConvRoot = SHRINK_GET_SEXUAL_CONVERSATION(thisSessionProperties, iNumLines, thisSession)
	
	IF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX01")
		RETURN CS_SECTION_4
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX02")
		RETURN CS_SECTION_5
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX03")
		RETURN CS_SECTION_6
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX04")
		RETURN CS_SECTION_7
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX05")
		RETURN CS_SECTION_8
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX06")
		RETURN CS_SECTION_9
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX07")
		RETURN CS_SECTION_10
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX08")
		RETURN CS_SECTION_11
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX09")
		RETURN CS_SECTION_12
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX10")
		RETURN CS_SECTION_13
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX11")
		RETURN CS_SECTION_14
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTSX12")
		RETURN CS_SECTION_15
	ENDIF
	
	RETURN CS_SECTION_4
ENDFUNC

// Get an appropriate response for Michael based on his recent open world behavior (theft & violence).
FUNC TEXT_LABEL_15 SHRINK_GET_OPEN_WORLD_CONVERSATION(SHRINK_SESSION thisSession, BOOL bOfficeSession, BOOL bSetConvoUsed, INT& iLines)
	TEXT_LABEL_15 sConversation
	INT iConversation
	INT iResponseFlags
	INT iValidResponses
	INT iSelection
	INT iCounter
	
	// Vehicular homicide special check
	IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 8)
	AND bOfficeSession
	AND thisSession <> SHRINKSESSION_OFFICE_ABANDONMENT
	AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_VEHICLE_KILLS) > 0
	AND GET_RANDOM_FLOAT_IN_RANGE() < 0.75
		iConversation = 8
		iLines = 4
	
	ELSE
		// Shop robbery
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 1)
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_SHOP_ROBBERY) > 0		
			SET_BIT(iResponseFlags, 1)
			iValidResponses += 1
		ENDIF
		
		// Stole 2 or more cars
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 2)
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_STOLEN_CARS) >= 2
			SET_BIT(iResponseFlags, 2)
			iValidResponses += 1
		ENDIF
		
		// At least one stolen car + dangerous driving (wanted in car or major accidents)
		IF (NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 3))
		AND ((SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_WANTED_IN_CAR) > 0
		OR SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_DANGEROUS_DRIVING) > 0)
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_STOLEN_CARS) > 0
		AND bOfficeSession AND thisSession <> SHRINKSESSION_OFFICE_ABANDONMENT)
			SET_BIT(iResponseFlags, 3)
			iValidResponses += 1
		ENDIF
		
		// 1 or 2 innocent kills
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 4)
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_RANDOM_KILLS) > 0	
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_RANDOM_KILLS) < 3
			SET_BIT(iResponseFlags, 4)
			iValidResponses += 1
		ENDIF
		
		// 3 or more innocent kills
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 5)
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_RANDOM_KILLS) >= 3
			SET_BIT(iResponseFlags, 5)
			iValidResponses += 1
		ENDIF
		
		// 4-star or higher wanted level
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 6)
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_4STAR_WANTED) > 0
			SET_BIT(iResponseFlags, 6)
			iValidResponses += 1
		ENDIF
		
		// Fallback
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 7)
			SET_BIT(iResponseFlags, 7)
			iValidResponses += 1
		ENDIF
		
		// Vehicular homicide
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 8)
		AND bOfficeSession
		AND SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_VEHICLE_KILLS) > 0
		AND thisSession <> SHRINKSESSION_OFFICE_ABANDONMENT
			SET_BIT(iResponseFlags, 8)
			iValidResponses += 1
		ENDIF
		
		// Fallback
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 9)
			SET_BIT(iResponseFlags, 9)
			iValidResponses += 1
		ENDIF
		
		// Fallback
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 10)
			SET_BIT(iResponseFlags, 10)
			iValidResponses += 1
		ENDIF
		
		// Fallback
		IF NOT SHRINK_IS_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, 11)
			SET_BIT(iResponseFlags, 11)
			iValidResponses += 1
		ENDIF
		
		IF iValidResponses <= 0
			IF thisSession <> SHRINKSESSION_OFFICE_ABANDONMENT
				iConversation = SHRINK_GET_UNUSED_VIOLENCE_CONVERSATION(7, 9, 10, 11)
			ELSE
				iConversation = SHRINK_GET_UNUSED_VIOLENCE_CONVERSATION(7, 9, 10)
			ENDIF
		
		ELSE
			iSelection = GET_RANDOM_INT_IN_RANGE(1, iValidResponses + 1)
			
			iCounter = 1
			WHILE iSelection > 0
				IF IS_BIT_SET(iResponseFlags, iCounter)
					iSelection -= 1
					IF iSelection = 0
						iConversation = iCounter
					ENDIF
				ENDIF
				
				iCounter += 1
			ENDWHILE
		ENDIF
		
		SWITCH iConversation
			CASE  1 iLines = 5 BREAK
			CASE  2 iLines = 5 BREAK
			CASE  3 iLines = 7 BREAK
			CASE  4 iLines = 4 BREAK
			CASE  5 iLines = 4 BREAK
			CASE  6 iLines = 5 BREAK
			CASE  7 iLines = 4 BREAK
			CASE  8 iLines = 4 BREAK
			CASE  9 iLines = 4 BREAK
			CASE 10 iLines = 5 BREAK
			CASE 11 iLines = 7 BREAK
		ENDSWITCH
	ENDIF
	
	IF bSetConvoUsed
		SHRINK_SET_OPEN_WORLD_RESPONSE_USED(g_savedGlobals.sShrinkData, iConversation, TRUE)
	ENDIF
	
	#IF iSHRINK_CUTSCENE
		sConversation = "OBTOW"
		IF iConversation < 10
			sConversation += "0"
		ENDIF
		sConversation += iConversation
	#ENDIF
	#IF NOT iSHRINK_CUTSCENE
		sConversation = "DRF_OWR_"
		
		IF iConversation >= 1 AND iConversation <= 3
			sConversation += iConversation
		ELIF iConversation < 7
			sConversation += 4
			IF iConversation = 4
				sConversation += "A"
			ELIF iConversation = 5
				sConversation += "B"
			ELSE
				sConversation += "C"
			ENDIF
		ELSE
			sConversation += 7
			IF iConversation = 7
				sConversation += "A"
			ELIF iConversation = 8
				sConversation += "B"
			ELIF iConversation = 9
				sConversation += "C"
			ELIF iConversation = 10
				sConversation += "D"
			ELSE
				sConversation += "E"
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN sConversation
ENDFUNC

FUNC CUTSCENE_SECTION SHRINK_GET_OPEN_WORLD_SECTION(SHRINK_SESSION thisSession)
	INT iLines
	TEXT_LABEL_15 sConvRoot = SHRINK_GET_OPEN_WORLD_CONVERSATION(thisSession, TRUE, TRUE, iLines)
	
	IF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW01")
		RETURN CS_SECTION_8
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW02")
		RETURN CS_SECTION_9
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW03")
		RETURN CS_SECTION_10
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW04")
		RETURN CS_SECTION_11
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW05")
		RETURN CS_SECTION_12
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW06")
		RETURN CS_SECTION_13
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW07")
		RETURN CS_SECTION_14
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW08")
		RETURN CS_SECTION_15
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW09")
		RETURN CS_SECTION_16
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW10")
		RETURN CS_SECTION_17
	ELIF ARE_STRINGS_EQUAL(sConvRoot, "OBTOW11")
		RETURN CS_SECTION_18
	ENDIF
	
	RETURN CS_SECTION_8
ENDFUNC

// Use this to select one line randomly from a conversation.
PROC SHRINK_ADD_RANDOM_PHONE_LINE(TEXT_LABEL_15 &sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15 &sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iLineCounter, STRING sBase, INT iLines)
	INT iLineIndex = GET_RANDOM_INT_IN_RANGE(1, iLines + 1)
	
	sRoots[iLineCounter] = sBase
	sSpecifics[iLineCounter] = sBase
	sSpecifics[iLineCounter] += "_"
	IF iLineIndex < 10
		sSpecifics[iLineCounter] += "0"
	ENDIF
	sSpecifics[iLineCounter] += iLineIndex
	
	iLineCounter += 1
ENDPROC

// Given a root, start line and number of lines, generate all labels for a conversation.
PROC SHRINK_FILL_CONVERSATION(TEXT_LABEL_15& sRoots[], TEXT_LABEL_15& sSpecifics[], INT& iLineCounter, STRING sConvRoot, INT iStartLine, INT iLinesInConv, BOOL bRandomConv = FALSE)
	TEXT_LABEL_15 sCurrentSpecific
	INT iLoopCounter
	INT iCurrentLine
	INT iLinesAdded
	
	// Add all lines in the conversation.
	FOR iLoopCounter = iStartLine TO iStartLine + iLinesInConv - 1
		sCurrentSpecific = sConvRoot
		sCurrentSpecific += "_"
		IF iLoopCounter < 9 AND bRandomConv
			sCurrentSpecific += "0"
		ENDIF
		sCurrentSpecific += (iLoopCounter + 1)
		
		IF DOES_TEXT_LABEL_EXIST(sCurrentSpecific)
			iCurrentLine = iLineCounter + iLinesAdded
			CERRORLN(DEBUG_MISSION, "Adding line ", sCurrentSpecific, " at location ", iCurrentLine)
			sRoots[iCurrentLine] = sConvRoot
			sSpecifics[iCurrentLine] = sCurrentSpecific
			iLinesAdded += 1
		ELSE
			CERRORLN(DEBUG_MISSION, "Text label doesn't exist: ", sCurrentSpecific)
		ENDIF
	ENDFOR
	
	iLineCounter += iLinesAdded
ENDPROC

// Michael doesn't have enough money; cue up the conversation if there are any remaining.
FUNC BOOL SHRINK_ADD_PHONE_CONV_BROKE(TEXT_LABEL_15& sThisRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15& sThisSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iThisLineCounter)
	INT iLastBroke = SHRINK_GET_LAST_BROKE_CONV(g_savedGlobals.sShrinkData)
	
	SWITCH iLastBroke
		CASE 0
			SHRINK_FILL_CONVERSATION(sThisRoots, sThisSpecifics, iThisLineCounter, "PBTNM", 0, 11)
			SHRINK_SET_LAST_BROKE_CONV(g_savedGlobals.sShrinkData, 1)
		BREAK
		
		CASE 1
			SHRINK_FILL_CONVERSATION(sThisRoots, sThisSpecifics, iThisLineCounter, "PBTNM2", 0, 10)
			SHRINK_SET_LAST_BROKE_CONV(g_savedGlobals.sShrinkData, 2)
		BREAK
		
		CASE 2
			SHRINK_FILL_CONVERSATION(sThisRoots, sThisSpecifics, iThisLineCounter, "PBTNM3", 0, 12)
			SHRINK_SET_LAST_BROKE_CONV(g_savedGlobals.sShrinkData, 3)
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

// Add the appropriate lines for our phone conversation intro based on the current rank.
PROC SHRINK_ADD_PHONE_CONV_INTRO(TEXT_LABEL_15& sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15& sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iLineCounter, SHRINK_SESSION eCurSession)
	TEXT_LABEL_15 sConv
	INT iLinesInConv
	
	// Set up the root name and number of lines based on the rank.
	SWITCH eCurSession
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			sConv = "DRF_3"
			iLinesInConv = 17
		BREAK
		
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			sConv = "DRF_4"
			iLinesInConv = 6
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Called SHRINK_ADD_PHONE_CONV_INTRO but we are not on a phone conversation!")
		BREAK
	ENDSWITCH
	
	// Add all lines in the conversation.
	SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, sConv, 0, iLinesInConv)
	
	#IF DRF_USE_OLD_DIALOGUE
		// Add a hard-coded response for this phone call.
		SWITCH eCurSession
			CASE SHRINKSESSION_PHONE_NEGATIVITY
				sConv = "PDROVR_02"
			BREAK
			
			CASE SHRINKSESSION_PHONE_FUCKED_UP
				sConv = "PDROVR_05"
			BREAK
		ENDSWITCH
		
		sRoots[iLineCounter] = "PDROVR"
	#ENDIF
	
	#IF DRF_USE_NEW_DIALOGUE
		// Add a hard-coded response for this phone call.
		SWITCH eCurSession
			CASE SHRINKSESSION_PHONE_NEGATIVITY
				sConv = "DRF_OVR_2"
			BREAK
			
			CASE SHRINKSESSION_PHONE_FUCKED_UP
				sConv = "DRF_OVR_5"
			BREAK
		ENDSWITCH
		
		sRoots[iLineCounter] = "DRF_OVR"
	#ENDIF
	
	sSpecifics[iLineCounter] = sConv
	iLineCounter += 1
ENDPROC

// Add the appropriate lines based on how Michael is feeling overall.
PROC SHRINK_ADD_CONDITIONAL_EXCHANGE(TEXT_LABEL_15& sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15& sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iLineCounter, SHRINK_CONDITION myCondition)
	#IF DRF_USE_OLD_DIALOGUE
		IF myCondition = SHRINKCONDITION_GOOD
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "PMCOVG", GET_RANDOM_INT_IN_RANGE(1, 3), 1, TRUE)
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "PDRTMG", PICK_INT(GET_RANDOM_BOOL(), 0, 2), 1, TRUE)
		ELIF myCondition = SHRINKCONDITION_OKAY
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "PMCOVO", GET_RANDOM_INT_IN_RANGE(1, 3), 1, TRUE)
			SHRINK_ADD_RANDOM_PHONE_LINE(sRoots, sSpecifics, iLineCounter, "PDRTMO", 3)
		ELSE
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "PMCOVB", 0, 1, TRUE)
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "PDRTMB", 2, 1, TRUE)
		ENDIF
	#ENDIF
	
	#IF DRF_USE_NEW_DIALOGUE
		IF myCondition = SHRINKCONDITION_GOOD
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "DRF_OVG", GET_RANDOM_INT_IN_RANGE(1, 3), 1)
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "DRF_TMG", PICK_INT(GET_RANDOM_BOOL(), 0, 2), 1)
		ELIF myCondition = SHRINKCONDITION_OKAY
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "DRF_OVO", GET_RANDOM_INT_IN_RANGE(1, 3), 1)
			SHRINK_ADD_RANDOM_PHONE_LINE(sRoots, sSpecifics, iLineCounter, "PDRTMO", 3)
		ELSE
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "DRF_OVB", 0, 1)
			SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "DRF_TMB", 2, 1)
		ENDIF
	#ENDIF
ENDPROC

// Add the appropriate lines for our phone conversation narrative based on the position in flow.
PROC SHRINK_ADD_PHONE_CONV_NARRATIVE(TEXT_LABEL_15& sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15& sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iLineCounter, SHRINK_SESSION thisSession)
	TEXT_LABEL_15 sConvRoot
	INT iStartLine, iLinesInConv
	
	sConvRoot = SHRINK_GET_NARRATIVE_CONVERSATION(thisSession, iStartLine, iLinesInConv)
	
	// Add all lines in the conversation.
	SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, sConvRoot, iStartLine, iLinesInConv)
ENDPROC

// Add the appropriate lines for our phone conversation based on Michael's open world behavior.
PROC SHRINK_ADD_PHONE_CONV_OPEN_WORLD(TEXT_LABEL_15& sRoots[iSHRINK_LONGEST_CONV_PRE_DECISION], TEXT_LABEL_15& sSpecifics[iSHRINK_LONGEST_CONV_PRE_DECISION], INT& iLineCounter, SHRINK_SESSION eCurSession)
	TEXT_LABEL_15 sConvRoot
	INT iQuestionIndex
	INT iLines
	
	SWITCH eCurSession
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			iQuestionIndex = 2
		BREAK
		
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			iQuestionIndex = 3
		BREAK
	ENDSWITCH
	
	#IF DRF_USE_OLD_DIALOGUE
		SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "PDROPW", iQuestionIndex, 1, TRUE)
	#ENDIF
	
	#IF DRF_USE_NEW_DIALOGUE
		SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, "DRF_OPW", iQuestionIndex, 1)
	#ENDIF
	
	sConvRoot = SHRINK_GET_OPEN_WORLD_CONVERSATION(eCurSession, FALSE, TRUE, iLines)
	SHRINK_FILL_CONVERSATION(sRoots, sSpecifics, iLineCounter, sConvRoot, 0, iLines)
ENDPROC

// Add lines for Michael accepting or denying Dr. Friedlander's advice.
PROC SHRINK_ADD_ACCEPT_DENY_LINES(TEXT_LABEL_15& sYesRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sYesSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], INT& iYesLineCounter, INT& iNoLineCounter, SHRINK_SESSION eCurSession)
	INT iAcceptLine, iDenyLine
	
	SWITCH eCurSession
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			iAcceptLine = 3
			iDenyLine = 1
		BREAK
		
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			iAcceptLine = 0
			iDenyLine = 3
		BREAK
	ENDSWITCH
	
	#IF DRF_USE_OLD_DIALOGUE
		SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, "PMCACC", iAcceptLine, 1, TRUE)
		SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, "PMCDEN", iDenyLine, 1, TRUE)
	#ENDIF
	
	#IF DRF_USE_NEW_DIALOGUE
		SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, "DRF_ACC", iAcceptLine, 1)
		SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, "DRF_DEN", iDenyLine, 1)
	#ENDIF
ENDPROC

// Add the appropriate lines for our phone conversation based on Michael's sexual behavior.
PROC SHRINK_ADD_PHONE_CONV_SEX(TEXT_LABEL_15& sYesRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sYesSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], INT& iYesLineCounter, INT& iNoLineCounter, SHRINK_SESSION_PROPERTIES &thisSessionProperties, SHRINK_SESSION eCurSession)
	TEXT_LABEL_15 sConvRoot
	INT iQuestionIndex
	INT iNumLines
	
	SWITCH eCurSession
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			iQuestionIndex = 1
		BREAK
		
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			iQuestionIndex = 3
		BREAK
	ENDSWITCH
	
	#IF DRF_USE_OLD_DIALOGUE
		SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, "PDRSEX", iQuestionIndex, 1, TRUE)
		SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, "PDRSEX", iQuestionIndex, 1, TRUE)
	#ENDIF
	
	#IF DRF_USE_NEW_DIALOGUE
		SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, "DRF_SEX", iQuestionIndex, 1)
		SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, "DRF_SEX", iQuestionIndex, 1)
	#ENDIF
	
	sConvRoot = SHRINK_GET_SEXUAL_CONVERSATION(thisSessionProperties, iNumLines, eCurSession)
	SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, sConvRoot, 0, iNumLines)
	SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, sConvRoot, 0, iNumLines)
ENDPROC

// Add an appropriate summary to Michael's condition. Should follow from Dr. Friedlander's sexual response.
PROC SHRINK_ADD_PHONE_CONV_SUMMARY(TEXT_LABEL_15& sYesRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sYesSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], INT& iYesLineCounter, INT& iNoLineCounter, SHRINK_SESSION_PROPERTIES &thisSessionProperties)
	INT iSummary
	
	SWITCH thisSessionProperties.iConvSexIndex
		CASE 1
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
				CASE 0 iSummary = 6 BREAK
				CASE 1 iSummary = 7 BREAK
				CASE 2 iSummary = 8 BREAK
				CASE 3 iSummary = 9 BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
		CASE 4
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0 iSummary = 6 BREAK
				CASE 1 iSummary = 7 BREAK
				CASE 2 iSummary = 9 BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
				CASE 0 iSummary = 2 BREAK
				CASE 1 iSummary = 6 BREAK
				CASE 2 iSummary = 7 BREAK
				CASE 3 iSummary = 9 BREAK
			ENDSWITCH
		BREAK
		
		CASE 5
		CASE 6
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
				CASE 0 iSummary = 3 BREAK
				CASE 1 iSummary = 6 BREAK
				CASE 2 iSummary = 7 BREAK
				CASE 3 iSummary = 9 BREAK
			ENDSWITCH
		BREAK
		
		CASE 7
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 6)
				CASE 0 iSummary =  2 BREAK
				CASE 1 iSummary =  4 BREAK
				CASE 2 iSummary =  6 BREAK
				CASE 3 iSummary =  7 BREAK
				CASE 4 iSummary =  8 BREAK
				CASE 5 iSummary =  9 BREAK
			ENDSWITCH
		BREAK
		
		CASE 8
		CASE 9
		CASE 11
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 8)
				CASE 0 iSummary =  2 BREAK
				CASE 1 iSummary =  3 BREAK
				CASE 2 iSummary =  4 BREAK
				CASE 3 iSummary =  6 BREAK
				CASE 4 iSummary =  7 BREAK
				CASE 5 iSummary =  8 BREAK
				CASE 6 iSummary =  9 BREAK
				CASE 7 iSummary = 11 BREAK
			ENDSWITCH
		BREAK
		
		CASE 10
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 7)
				CASE 0 iSummary =  3 BREAK
				CASE 1 iSummary =  4 BREAK
				CASE 2 iSummary =  6 BREAK
				CASE 3 iSummary =  7 BREAK
				CASE 4 iSummary =  8 BREAK
				CASE 5 iSummary =  9 BREAK
				CASE 6 iSummary = 11 BREAK
			ENDSWITCH
		BREAK
		
		CASE 12
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
				CASE 0 iSummary =  6 BREAK
				CASE 1 iSummary =  7 BREAK
				CASE 2 iSummary =  9 BREAK
				CASE 3 iSummary = 11 BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, "PMCSUM", iSummary - 1, 1)
	SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, "PMCSUM", iSummary - 1, 1)
ENDPROC

// Add the appropriate lines for our phone conversation outro based on the current rank.
PROC SHRINK_ADD_PHONE_CONV_OUTRO(TEXT_LABEL_15& sYesRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sYesSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoRoots[iSHRINK_LONGEST_CONV_POST_DECISION], TEXT_LABEL_15& sNoSpecifics[iSHRINK_LONGEST_CONV_POST_DECISION], INT& iYesLineCounter, INT& iNoLineCounter, SHRINK_SESSION eCurSession)
	TEXT_LABEL_15 sConvRoot
	INT iStartLine, iLinesInConv
	
	// Set up the root name and number of lines based on the rank.
	SWITCH eCurSession
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			sConvRoot = "DRF_3"
			iStartLine = 17
			iLinesInConv = 7
		BREAK
		
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			sConvRoot = "DRF_4"
			iStartLine = 6
			iLinesInConv = 17
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Called SHRINK_ADD_PHONE_CONV_OUTRO but we are not on a phone conversation!")
		BREAK
	ENDSWITCH
	
	// Add all lines in the conversation.
	SHRINK_FILL_CONVERSATION(sYesRoots, sYesSpecifics, iYesLineCounter, sConvRoot, iStartLine, iLinesInConv)
	SHRINK_FILL_CONVERSATION(sNoRoots, sNoSpecifics, iNoLineCounter, sConvRoot, iStartLine, iLinesInConv)
ENDPROC

#IF iSHRINK_CUTSCENE
PROC RELEASE_SHRINK_TRIGGER_ASSETS(SHRINK_SESSION thisSession)
	//Vehcile has been grabbed from trigger scene now. We can release the scene's assets.
	SWITCH thisSession
		CASE SHRINKSESSION_OFFICE_CHAOS
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SHRINK_1)
		BREAK
		CASE SHRINKSESSION_OFFICE_EVIL
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SHRINK_2)
		BREAK
		CASE SHRINKSESSION_PHONE_NEGATIVITY
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SHRINK_3)
		BREAK
		CASE SHRINKSESSION_PHONE_FUCKED_UP
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SHRINK_4)
		BREAK
		CASE SHRINKSESSION_OFFICE_ABANDONMENT
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SHRINK_5)
		BREAK
	ENDSWITCH
ENDPROC

// Grab references to vehicles for later and set up peds for conversation.
PROC SHRINK_SETUP_OFFICE_PROC_CONV(PED_INDEX shrinkCSPed, VEHICLE_INDEX& shrinkVehicle, VEHICLE_INDEX& playerVehicle, CAMERA_INDEX& shrinkMainCam, structPedsForConversation& convPeds, VECTOR vShrinkVehPos, FLOAT fShrinkVehHeading, MODEL_NAMES shrinkVehModelName)
	playerVehicle = GET_PLAYERS_LAST_VEHICLE()
	
	IF DOES_ENTITY_EXIST(playerVehicle)
		IF NOT IS_VEHICLE_DRIVEABLE(playerVehicle)
			playerVehicle = NULL
		ELSE
			// Make sure it's a proper type of vehicle to put in the street.
			MODEL_NAMES vehModel = GET_ENTITY_MODEL(playerVehicle)
			IF IS_THIS_MODEL_A_BOAT(vehModel)
			OR IS_THIS_MODEL_A_TRAIN(vehModel)
				playerVehicle = NULL
			
			// Make sure it isn't a taxi or a vehicle owned by someone else.
			ELSE
				PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(playerVehicle, VS_DRIVER)
				IF pedDriver <> NULL AND pedDriver <> PLAYER_PED_ID()
					playerVehicle = NULL
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Check if the trigger scene vehicle exists. If so steal it.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		shrinkVehicle = g_sTriggerSceneAssets.veh[0]
		SET_ENTITY_AS_MISSION_ENTITY(shrinkVehicle, TRUE, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(shrinkVehicle)
		IF shrinkVehicle = playerVehicle
			shrinkVehicle = NULL
		ELSE
			SET_VEHICLE_FIXED(shrinkVehicle)
			//SET_VEHICLE_DOORS_LOCKED(shrinkVehicle, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			FREEZE_ENTITY_POSITION(shrinkVehicle, FALSE)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(shrinkVehicle)
		shrinkVehicle = CREATE_VEHICLE(shrinkVehModelName, vShrinkVehPos, fShrinkVehHeading)
		SET_VEHICLE_COLOUR_COMBINATION(shrinkVehicle, 6)
	ENDIF
	
	IF DOES_ENTITY_EXIST(playerVehicle)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(playerVehicle)
			SET_ENTITY_AS_MISSION_ENTITY(playerVehicle)
		ENDIF
	ENDIF
	
//	IF NOT DOES_ENTITY_EXIST(shrinkVehicle)
//		shrinkVehicle = CREATE_VEHICLE(shrinkVehModelName, vShrinkVehPos, fShrinkVehHeading)
//		SET_VEHICLE_COLOUR_COMBINATION(shrinkVehicle, 0)
//	ENDIF
	
	// Populate our conversation ped data.
	ADD_PED_FOR_DIALOGUE(convPeds, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(convPeds, 1, shrinkCSPed, "FRIEDLANDER")
	
	IF DOES_CAM_EXIST(shrinkMainCam)
		DESTROY_CAM(shrinkMainCam)
	ENDIF
	IF NOT DOES_CAM_EXIST(shrinkMainCam)
		shrinkMainCam = CREATE_CAMERA(CAMTYPE_SCRIPTED)
	ENDIF
	
	SET_CAM_COORD(shrinkMainCam, <<-1906.8422, -572.3835, 19.1046>>)
	SET_CAM_ROT(shrinkMainCam, <<-1.5254, 0.0003, 171.4657>>)
	SET_CAM_FOV(shrinkMainCam, 17.0)
	SET_CAM_ACTIVE(shrinkMainCam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	NEW_LOAD_SCENE_STOP()
ENDPROC

// Clean up Michael and Dr. Friedlander from the office procedural conversation tasks.
PROC SHRINK_CLEANUP_PROC_CONV()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
ENDPROC

//Handle player accepting/rejecting the shrinks advice
FUNC BOOL HAS_PLAYER_REACTED_TO_SHRINK_ADVICE_OFFICE(structTimer& tmrConv, SIMPLE_USE_CONTEXT& useContext, BOOL& bChoice)
	IF (IS_TIMER_STARTED(tmrConv)) AND GET_TIMER_IN_SECONDS(tmrConv) > 1.3
		BOOL bAcceptPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		BOOL bRejectPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		
		IF bAcceptPressed OR bRejectPressed
			PRINTLN("Player chose something")
			IF bAcceptPressed
				PRINTLN("ACCEPT!")
				bChoice = TRUE
			ELSE
				PRINTLN("REJECT!")
				bChoice = FALSE
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHRINK_ACCEPT")
				CLEAR_HELP()
			ENDIF
			
			CLEANUP_SIMPLE_USE_CONTEXT(useContext)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	UPDATE_SIMPLE_USE_CONTEXT(useContext)
	
//	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHRINK_ACCEPT")
//		PRINT_HELP_FOREVER("SHRINK_ACCEPT")
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC CUTSCENE_SECTION SHRINK_GET_REPLY_SECTION(SHRINK_DECISION_RESULT PlayerDecision)
	IF PlayerDecision = SHRINK_ACCEPT
		RETURN CS_SECTION_1
	ELIF PlayerDecision = SHRINK_REJECT
		RETURN CS_SECTION_2
	ELIF PlayerDecision = SHRINK_SILENT
		RETURN CS_SECTION_3
	ENDIF
	
	RETURN CS_SECTION_3
ENDFUNC

PROC SHRINK_OFFICE_REQUEST_INTRO_CUTSCENE(STRING sCutsceneName, SHRINK_SESSION thisSession)
	INT iSectionOffset = 0
	
	IF thisSession = SHRINKSESSION_OFFICE_EVIL
	OR thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
		iSectionOffset = 1
	ENDIF
	
	CUTSCENE_SECTION sceneBranches = CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4
	sceneBranches |= SHRINK_GET_NARRATIVE_CUTSCENE_SECTION(thisSession, TRUE)
	sceneBranches |= INT_TO_ENUM(CUTSCENE_SECTION, SHIFT_LEFT(ENUM_TO_INT(CS_SECTION_7), iSectionOffset))
	
	#IF IS_DEBUG_BUILD
		INT iCounter
		REPEAT 11 iCounter
			IF bForceViolence[iCounter]
				CUTSCENE_SECTION owSection = INT_TO_ENUM(CUTSCENE_SECTION, SHIFT_LEFT(ENUM_TO_INT(CS_SECTION_8), iSectionOffset))
				owSection = INT_TO_ENUM(CUTSCENE_SECTION, SHIFT_LEFT(ENUM_TO_INT(owSection), iCounter))
				sceneBranches |= owSection
				iCounter = 20
			ENDIF
		ENDREPEAT
		
		IF iCounter < 20
			sceneBranches |= INT_TO_ENUM(CUTSCENE_SECTION, SHIFT_LEFT(ENUM_TO_INT(SHRINK_GET_OPEN_WORLD_SECTION(thisSession)), iSectionOffset))
		ENDIF
	#ENDIF
	
	#IF NOT IS_DEBUG_BUILD
		sceneBranches |= INT_TO_ENUM(CUTSCENE_SECTION, SHIFT_LEFT(ENUM_TO_INT(SHRINK_GET_OPEN_WORLD_SECTION(thisSession)), iSectionOffset))
	#ENDIF
	
	REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sCutsceneName, sceneBranches)	
ENDPROC

PROC SHRINK_OFFICE_REQUEST_OUTRO_CUTSCENE(STRING sCutsceneName, SHRINK_SESSION_PROPERTIES &thisSessionProperties, SHRINK_SESSION thisSession)
	CUTSCENE_SECTION sceneBranches = SHRINK_GET_REPLY_SECTION(thisSessionProperties.shrinkDecision)
	sceneBranches |= CS_SECTION_3
	
	#IF IS_DEBUG_BUILD
		INT iCounter
		REPEAT 12 iCounter
			IF bForceSex[iCounter]
				sceneBranches |= INT_TO_ENUM(CUTSCENE_SECTION, SHIFT_LEFT(ENUM_TO_INT(CS_SECTION_4), iCounter))
				iCounter = 20
			ENDIF
		ENDREPEAT
		
		IF iCounter < 20
			sceneBranches |= SHRINK_GET_SEXUAL_SECTION(thisSessionProperties, thisSession)
		ENDIF
	#ENDIF
	
	#IF NOT IS_DEBUG_BUILD
		sceneBranches |= SHRINK_GET_SEXUAL_SECTION(thisSessionProperties, thisSession)
	#ENDIF
	
	sceneBranches |= (CS_SECTION_16 | CS_SECTION_17)
	
	REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sCutsceneName, sceneBranches)	
ENDPROC

#ENDIF
