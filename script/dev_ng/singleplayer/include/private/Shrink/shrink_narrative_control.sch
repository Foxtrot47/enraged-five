
//////////////////////////////////////////////////////////////////////
/* shrink_narrative_control.sch										*/
/* Author: DJ Jones, Yomal Perera									*/
/* See if there is valid narrative dialogue to play for the current	*/
/*  session based on flow status and what's already been used.		*/
//////////////////////////////////////////////////////////////////////


USING "shrink.sch"
USING "shrink_proc_conv.sch"
USING "sp_globals_saved.sch"


FUNC BOOL SHRINK_IS_NARRATIVE_VALID_FOR_SESSION(SHRINK_SESSION thisSession)
	CUTSCENE_SECTION cutSection
	TEXT_LABEL_15 sConv
	INT iDesiredNarrative, iLastNarrative
	INT iStartLine, iLinesInConv
	
	// For office sessions, see if we can find a valid cutscene section to play.
	IF thisSession = SHRINKSESSION_OFFICE_CHAOS
	OR thisSession = SHRINKSESSION_OFFICE_EVIL
	OR thisSession = SHRINKSESSION_OFFICE_ABANDONMENT
		cutSection = SHRINK_GET_NARRATIVE_CUTSCENE_SECTION(thisSession)
		RETURN cutSection <> CS_SECTION_1
	ENDIF
	
	// Make sure we're in a valid position in flow for a phone call to Dr. Friedlander.
	sConv = SHRINK_GET_NARRATIVE_CONVERSATION(thisSession, iStartLine, iLinesInConv, FALSE)
	IF IS_STRING_NULL_OR_EMPTY(sConv)
		RETURN FALSE
	ENDIF
	
	// For the first phone call, any valid narrative result is ok.
	IF thisSession = SHRINKSESSION_PHONE_NEGATIVITY
		RETURN TRUE
	ENDIF
	
	// For the second call, need to verify that we have new story stuff to talk about.
	iDesiredNarrative = SHRINK_GET_NARRATIVE_INDEX(sConv, iStartLine)
	IF iDesiredNarrative <= 0
		CASSERTLN(DEBUG_FLOW, "Did not get a valid desired narrative! This is bad!")
		RETURN FALSE
	ENDIF
	
	iLastNarrative = SHRINK_GET_LAST_NARRATIVE(g_savedGlobals.sShrinkData)
	RETURN iDesiredNarrative > iLastNarrative
ENDFUNC
