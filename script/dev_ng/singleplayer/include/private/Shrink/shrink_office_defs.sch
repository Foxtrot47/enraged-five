
//////////////////////////////////////////////////////////////////////
/* shrink_office_defs.sch										    */
/* Author: DJ Jones, Yomal Perera									*/
/* Definitions for shrink office visits.							*/
//////////////////////////////////////////////////////////////////////


CONST_INT iSHRINK_CUTSCENE 1


USING "rgeneral_include.sch"
USING "cutscene_public.sch"
USING "minigames_helpers.sch"
USING "shrink.sch"
USING "shrink_anim.sch"

USING "shrink_properties.sch"

USING "shrink_proc_conv.sch"
USING "shrink_core.sch"


STREAMED_MODEL shrinkModels[iSHRINK_OFFICE_MODELS]
MODEL_NAMES shrinkCSModelName = CS_DRFRIEDLANDER
MODEL_NAMES shrinkIGModelName = IG_DRFRIEDLANDER
MODEL_NAMES shrinkVehModelName = COMET2
MODEL_NAMES nextDoorCarModel = EXEMPLAR
MODEL_NAMES chairModel = V_ILEV_P_EASYCHAIR

SHRINK_OFFICE_STATE officeState = SHRINKOFFICESTATE_INIT
SHRINK_SESSION_PROPERTIES sessionProperties
//SHRINK_PROC_CAM_SCRIPT procCamScript
//SHRINK_PROC_CAM_STATUS procCamStatus
SHRINK_SESSION	thisSession

structPedsForConversation convPeds
structTimer controlTimer
structTimer convTimer
structTimer cleanupTimer
structTimer timerBlipFlicker
structTimer tmrObjective
structTimer tmrKillControl
structTimer tmrPlayerGait
structTimer tmrDrfKillDialogue
structTimer tmrPlrKillDialogue

SIMPLE_USE_CONTEXT useContext

PED_INDEX shrinkCSPed
PED_INDEX shrinkIGPed
VEHICLE_INDEX shrinkVehicle
VEHICLE_INDEX playerVehicle
BLIP_INDEX shrinkBlip
CAMERA_INDEX shrinkMainCam
CAMERA_INDEX shrinkHelperCam
SEQUENCE_INDEX seqShrinkCar

TEXT_LABEL_23 sMichaelID     = "Michael"
TEXT_LABEL_23 sFriedlanderID = "Therapist" //"cs_drfriedlander"

VECTOR vProcConvShrinkPos = <<-1907.785, -574.800, 18.616>>
//VECTOR vPostConvPlayerPos = <<-1900.5593, -558.0717, 10.7720>>
VECTOR vShrinkVehPos = <<-1895.5337, -564.2134, 11.3497>>
FLOAT fProcConvShrinkHeading = 179.000
FLOAT fPostConvPlayerHeading = 314.381
FLOAT fShrinkVehHeading = 54.08
INT iEscDlgShrinkBits
INT iEscUsedDrfCount
INT iEscDlgPlayerBits
INT iEscUsedPlrCount
INT iUpperVariation
INT iUpperTexture
INT iLowerTexture
BOOL bOutroRequested
BOOL bSceneRequested
BOOL bClothingRestore
BOOL bEscDlgCleared
BOOL bPanStarted
