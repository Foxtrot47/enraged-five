//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 		 Load Queue Private Header							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			23/06/14												│
//│		DESCRIPTION: 	The private commands for queuing up load				│
//│						requests to be scheduled over multiple frames.			│								
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_audio.sch"
USING "commands_script.sch"

// Queue sizes.
CONST_INT 	LOAD_QUEUE_SIZE_SMALL	5
CONST_INT 	LOAD_QUEUE_SIZE_MEDIUM	8
CONST_INT 	LOAD_QUEUE_SIZE_LARGE	15
CONST_INT 	LOAD_QUEUE_SIZE_LIGHT	15

// LoadQueueData struct state bitflags. 
// Count down from 30 as the first X bits are used to store the load type.
CONST_INT	BIT_LQD_USED			30
CONST_INT	BIT_LQD_REQUESTED		29
CONST_INT	BIT_LQD_PRIORITY_TD		28		// Priority texture dictionary.
CONST_INT	BIT_LQD_OVER_NETWORK	27		// Load audio bank over network.
CONST_INT	BIT_LQD_CLEAR_BRIEF		26		// Clear brief when releasing additional text.


// Different types of items that can exist in the load queue.
ENUM LoadQueueType
	LQT_INVALID = -1,

	LQT_MODEL,					// 0
	LQT_ANIM_DICT,				// 1
	LQT_CLIP_SET,				// 2
	LQT_TEXT_DICT,				// 3
	LQT_VEH_REC,				// 4
	LQT_WAYPOINT_REC,			// 5
	LQT_AUDIO_BANK,				// 6
	LQT_SCRIPT,					// 7
	LQT_ADDIT_TEXT,				// 8
	LQT_PTFX_ASSET,				// 9
	//Note: The values of these musn't extend into the BIT_LQD_ bit range!
	
	MAX_LQT_PTFX
ENDENUM


// Struct definiton for a single load request in a load queue with a text field.
STRUCT LoadQueueData
	INT iState
	INT iData
	TEXT_LABEL_63 txtData
ENDSTRUCT

// Struct definiton for a single load request in a load queue without a text field to save on stack sizes.
STRUCT LoadQueueDataLight
	INT iState
	INT iData
ENDSTRUCT


// Load queue data struct definitions. To be instantiated
// by scripts that require load queue support.
STRUCT LoadQueueSmall
	LoadQueueData sQueue[LOAD_QUEUE_SIZE_SMALL]
	BOOL bLoading
	INT iLastFrame
	INT iFrameDelay = 1
ENDSTRUCT

STRUCT LoadQueueMedium
	LoadQueueData sQueue[LOAD_QUEUE_SIZE_MEDIUM]
	BOOL bLoading
	INT iLastFrame
	INT iFrameDelay = 1
ENDSTRUCT

STRUCT LoadQueueLarge
	LoadQueueData sQueue[LOAD_QUEUE_SIZE_LARGE]
	BOOL bLoading
	INT iLastFrame
	INT iFrameDelay = 1
ENDSTRUCT

STRUCT LoadQueueLight
	LoadQueueDataLight sQueue[LOAD_QUEUE_SIZE_LIGHT]
	BOOL bLoading
	INT iLastFrame
	INT iFrameDelay = 1
ENDSTRUCT


#IF IS_DEBUG_BUILD
	FUNC STRING PRIVATE_Get_Load_Queue_Type_Debug_String(LoadQueueType paramType)
		SWITCH paramType
			CASE LQT_INVALID		RETURN "INVALID"		BREAK
			CASE LQT_MODEL			RETURN "MODEL"			BREAK
			CASE LQT_ANIM_DICT		RETURN "ANIM_DICT"		BREAK
			CASE LQT_CLIP_SET		RETURN "CLIP_SET"		BREAK
			CASE LQT_TEXT_DICT		RETURN "TEXT_DICT"		BREAK
			CASE LQT_VEH_REC		RETURN "VEH_REC"		BREAK
			CASE LQT_WAYPOINT_REC	RETURN "WAYPOINT_REC"	BREAK
			CASE LQT_AUDIO_BANK		RETURN "AUDIO_BANK"		BREAK
			CASE LQT_SCRIPT			RETURN "SCRIPT"			BREAK
			CASE LQT_ADDIT_TEXT		RETURN "ADDIT_TEXT"		BREAK
			CASE LQT_PTFX_ASSET		RETURN "PTFX_ASSET"		BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("PRIVATE_Get_Load_Queue_Type_Debug_String: Missing debug string for load queue type. Bug BenR.")
		RETURN "ERROR!"
	ENDFUNC
#ENDIF

FUNC BOOL PRIVATE_Is_Load_Queue_Type_Supported_In_Light_Queue(LoadQueueType paramType)
	SWITCH paramType
		CASE LQT_INVALID		RETURN FALSE		BREAK
		CASE LQT_MODEL			RETURN TRUE			BREAK
		CASE LQT_ANIM_DICT		RETURN FALSE		BREAK
		CASE LQT_CLIP_SET		RETURN FALSE		BREAK
		CASE LQT_TEXT_DICT		RETURN FALSE		BREAK
		CASE LQT_VEH_REC		RETURN FALSE		BREAK
		CASE LQT_WAYPOINT_REC	RETURN FALSE		BREAK
		CASE LQT_AUDIO_BANK		RETURN FALSE		BREAK
		CASE LQT_SCRIPT			RETURN TRUE			BREAK
		CASE LQT_ADDIT_TEXT		RETURN FALSE		BREAK
		CASE LQT_PTFX_ASSET		RETURN TRUE			BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("PRIVATE_Is_Load_Queue_Type_Supported_In_Light_Queue: Missing type definition in function. Bug BenR.")
	RETURN FALSE
ENDFUNC


FUNC LoadQueueType PRIVATE_Get_Type_From_Bitset(INT paramStateBitset)
	INT i
	REPEAT MAX_LQT_PTFX i
		IF IS_BIT_SET(paramStateBitset, i)
			RETURN INT_TO_ENUM(LoadQueueType, i)
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("PRIVATE_Get_Type_From_Bitset: No type bits were set for the load queue data passed. Bug BenR.")
	RETURN LQT_INVALID
ENDFUNC


PROC PRIVATE_Clean_Up_Load_Queue_Data(LoadQueueData &paramData)
	paramData.iState = 0
	paramData.iData = -1
	paramData.txtData = "NULL"
ENDPROC


PROC PRIVATE_Clean_Up_Load_Queue_Data_Light(LoadQueueDataLight &paramData)
	paramData.iState = 0
	paramData.iData = -1
ENDPROC


PROC PRIVATE_Copy_Load_Queue_Data(LoadQueueData &paramFrom, LoadQueueData &paramTo)
	paramTo.iState = paramFrom.iState
	paramTo.iData = paramFrom.iData
	paramTo.txtData = paramFrom.txtData
ENDPROC


PROC PRIVATE_Copy_Load_Queue_Data_Light(LoadQueueDataLight &paramFrom, LoadQueueDataLight &paramTo)
	paramTo.iState = paramFrom.iState
	paramTo.iData = paramFrom.iData
ENDPROC


PROC PRIVATE_Make_Queued_Load_Generic(INT &paramState, STRING paramTextData, INT paramIntData)
	IF IS_BIT_SET(paramState, BIT_LQD_USED)
		CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Making queued load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(PRIVATE_Get_Type_From_Bitset(paramState)), "|", paramIntData, "|", paramTextData, "] on frame ", GET_FRAME_COUNT(), ".")
	
		SWITCH PRIVATE_Get_Type_From_Bitset(paramState)
			CASE LQT_MODEL			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, paramIntData))										BREAK
			CASE LQT_ANIM_DICT		REQUEST_ANIM_DICT(paramTextData)															BREAK
			CASE LQT_CLIP_SET		REQUEST_CLIP_SET(paramTextData)																BREAK
			CASE LQT_TEXT_DICT		REQUEST_STREAMED_TEXTURE_DICT(paramTextData, IS_BIT_SET(paramState, BIT_LQD_PRIORITY_TD))	BREAK
			CASE LQT_VEH_REC		REQUEST_VEHICLE_RECORDING(paramIntData, paramTextData)										BREAK
			CASE LQT_WAYPOINT_REC	REQUEST_WAYPOINT_RECORDING(paramTextData)													BREAK
			CASE LQT_AUDIO_BANK		REQUEST_SCRIPT_AUDIO_BANK(paramTextData, IS_BIT_SET(paramState, BIT_LQD_OVER_NETWORK))		BREAK
			CASE LQT_SCRIPT			REQUEST_SCRIPT_WITH_NAME_HASH(paramIntData)													BREAK
			CASE LQT_ADDIT_TEXT		REQUEST_ADDITIONAL_TEXT(paramTextData, INT_TO_ENUM(TEXT_BLOCK_SLOTS, paramIntData))			BREAK
			CASE LQT_PTFX_ASSET		REQUEST_PTFX_ASSET()																		BREAK
			
			DEFAULT	SCRIPT_ASSERT("PRIVATE_Make_Queued_Load: Unrecognised request type. Bug BenR.")								BREAK
		ENDSWITCH
		SET_BIT(paramState, BIT_LQD_REQUESTED)
	ELSE
		SCRIPT_ASSERT("PRIVATE_Make_Queued_Load: Tried to make a request from a queue slot that is not in use. Bug BenR.")
	ENDIF
ENDPROC


PROC PRIVATE_Make_Queued_Load(LoadQueueData &paramData)
	PRIVATE_Make_Queued_Load_Generic(paramData.iState, paramData.txtData, paramData.iData)
ENDPROC


PROC PRIVATE_Make_Queued_Load_Light(LoadQueueDataLight &paramData)
	PRIVATE_Make_Queued_Load_Generic(paramData.iState, "NULL", paramData.iData)
ENDPROC


FUNC BOOL PRIVATE_Has_Queued_Load_Loaded_Generic(INT paramState, STRING paramTextData, INT paramIntData)
	IF IS_BIT_SET(paramState, BIT_LQD_USED)
		IF IS_BIT_SET(paramState, BIT_LQD_REQUESTED)
			SWITCH PRIVATE_Get_Type_From_Bitset(paramState)
				CASE LQT_MODEL			RETURN HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, paramIntData))									BREAK
				CASE LQT_ANIM_DICT		RETURN HAS_ANIM_DICT_LOADED(paramTextData)														BREAK
				CASE LQT_CLIP_SET		RETURN HAS_CLIP_SET_LOADED(paramTextData)														BREAK
				CASE LQT_TEXT_DICT		RETURN HAS_STREAMED_TEXTURE_DICT_LOADED(paramTextData)											BREAK
				CASE LQT_VEH_REC		RETURN HAS_VEHICLE_RECORDING_BEEN_LOADED(paramIntData, paramTextData)							BREAK
				CASE LQT_WAYPOINT_REC	RETURN GET_IS_WAYPOINT_RECORDING_LOADED(paramTextData)											BREAK
				CASE LQT_AUDIO_BANK		RETURN REQUEST_SCRIPT_AUDIO_BANK(paramTextData, IS_BIT_SET(paramState, BIT_LQD_OVER_NETWORK))	BREAK
				CASE LQT_SCRIPT			RETURN HAS_SCRIPT_WITH_NAME_HASH_LOADED(paramIntData)											BREAK
				CASE LQT_ADDIT_TEXT		RETURN HAS_ADDITIONAL_TEXT_LOADED(INT_TO_ENUM(TEXT_BLOCK_SLOTS, paramIntData))					BREAK
				CASE LQT_PTFX_ASSET		RETURN HAS_PTFX_ASSET_LOADED()																	BREAK
				
				DEFAULT	SCRIPT_ASSERT("PRIVATE_Has_Queued_Load_Loaded: Unrecognised request type. Bug BenR.")							BREAK
			ENDSWITCH
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		SCRIPT_ASSERT("PRIVATE_Has_Queued_Load_Loaded: Tried to check if an empty queue index has loaded. Bug BenR.")
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Has_Queued_Load_Loaded(LoadQueueData &paramData)
	RETURN PRIVATE_Has_Queued_Load_Loaded_Generic(paramData.iState, paramData.txtData, paramData.iData)
ENDFUNC


FUNC BOOL PRIVATE_Has_Queued_Load_Loaded_Light(LoadQueueDataLight &paramData)
	RETURN PRIVATE_Has_Queued_Load_Loaded_Generic(paramData.iState, "NULL", paramData.iData)
ENDFUNC


PROC PRIVATE_Set_Queued_Load_As_No_Longer_Needed_Generic(INT paramState, STRING paramTextData, INT paramIntData)
	CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting queued load as no longer needed [", PRIVATE_Get_Load_Queue_Type_Debug_String(PRIVATE_Get_Type_From_Bitset(paramState)), "|", paramIntData, "|", paramTextData, "].")

	IF IS_BIT_SET(paramState, BIT_LQD_USED)
		SWITCH PRIVATE_Get_Type_From_Bitset(paramState)
			CASE LQT_MODEL			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, paramIntData))				BREAK
			CASE LQT_ANIM_DICT		REMOVE_ANIM_DICT(paramTextData)														BREAK
			CASE LQT_CLIP_SET		REMOVE_CLIP_SET(paramTextData)														BREAK
			CASE LQT_TEXT_DICT		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(paramTextData)						BREAK
			CASE LQT_VEH_REC		REMOVE_VEHICLE_RECORDING(paramIntData, paramTextData)								BREAK
			CASE LQT_WAYPOINT_REC	REMOVE_WAYPOINT_RECORDING(paramTextData)											BREAK
			CASE LQT_AUDIO_BANK		RELEASE_SCRIPT_AUDIO_BANK()															BREAK
			CASE LQT_SCRIPT			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(paramIntData)							BREAK
			CASE LQT_ADDIT_TEXT		CLEAR_ADDITIONAL_TEXT(	INT_TO_ENUM(TEXT_BLOCK_SLOTS, paramIntData), 
															IS_BIT_SET(paramState, BIT_LQD_CLEAR_BRIEF))				BREAK
			CASE LQT_PTFX_ASSET		REMOVE_PTFX_ASSET()																	BREAK
			
			DEFAULT	SCRIPT_ASSERT("PRIVATE_Set_Queued_Load_As_No_Longer_Needed: Unrecognised request type. Bug BenR.")	BREAK
		ENDSWITCH
	ELSE
		SCRIPT_ASSERT("PRIVATE_Set_Queued_Load_As_No_Longer_Needed: Tried to set an empty queue index as no longer needed. Bug BenR.")
	ENDIF
ENDPROC


PROC PRIVATE_Set_Queued_Load_As_No_Longer_Needed(LoadQueueData &paramData)
	PRIVATE_Set_Queued_Load_As_No_Longer_Needed_Generic(paramData.iState, paramData.txtData, paramData.iData)
ENDPROC


PROC PRIVATE_Set_Queued_Load_As_No_Longer_Needed_Light(LoadQueueDataLight &paramData)
	PRIVATE_Set_Queued_Load_As_No_Longer_Needed_Generic(paramData.iState, "NULL", paramData.iData)
ENDPROC


PROC PRIVATE_Load_Queue_Small_Add_Data(LoadQueueSmall &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, INT paramBitSettings = 0)
	CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to queue load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in small queue.")

	//First check this item isn't already queued.
	INT i
	REPEAT LOAD_QUEUE_SIZE_SMALL i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[i].iData = paramIntData
						CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
						EXIT
					ENDIF
				ENDIF
				IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
					IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
						IF ARE_STRINGS_EQUAL(paramQueue.sQueue[i].txtData, paramStringData)
							CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
							EXIT
						ENDIF
					ENDIF
				ENDIF
				IF paramType = LQT_PTFX_ASSET
					CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Flag the queue as loading if it isn't already.
	IF NOT paramQueue.bLoading
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Flagged queue as loading.")
		paramQueue.bLoading = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queuing load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in small queue.")

	//Find a free slot in the queue and add the load request.
	REPEAT LOAD_QUEUE_SIZE_SMALL i
		IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found slot ", i, " as free.")
			paramQueue.sQueue[i].txtData = paramStringData
			paramQueue.sQueue[i].iData = paramIntData
			paramQueue.sQueue[i].iState = paramBitSettings
			SET_BIT(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
			SET_BIT(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			EXIT
		ENDIF
	ENDREPEAT
	
	//No free slots.
	SCRIPT_ASSERT("PRIVATE_Request_Queue_Small_Add_Data: The queue was full. Use a larger queue size?")
ENDPROC


PROC PRIVATE_Load_Queue_Medium_Add_Data(LoadQueueMedium &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, INT paramBitSettings = 0)
	CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to queue load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in medium queue.")

	//First check this item isn't already queued.
	INT i
	REPEAT LOAD_QUEUE_SIZE_MEDIUM i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[i].iData = paramIntData
						CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
						EXIT
					ENDIF
				ENDIF
				IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
					IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
						IF ARE_STRINGS_EQUAL(paramQueue.sQueue[i].txtData, paramStringData)
							CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
							EXIT
						ENDIF
					ENDIF
				ENDIF
				IF paramType = LQT_PTFX_ASSET
					CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Flag the queue as loading if it isn't already.
	IF NOT paramQueue.bLoading
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Flagged queue as loading.")
		paramQueue.bLoading = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queuing load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in medium queue.")

	//Find a free slot in the queue and add the load request.
	REPEAT LOAD_QUEUE_SIZE_MEDIUM i
		IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found slot ", i, " as free.")
			paramQueue.sQueue[i].txtData = paramStringData
			paramQueue.sQueue[i].iData = paramIntData
			paramQueue.sQueue[i].iState = paramBitSettings
			SET_BIT(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
			SET_BIT(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			EXIT
		ENDIF
	ENDREPEAT
	
	//No free slots.
	SCRIPT_ASSERT("PRIVATE_Request_Queue_Medium_Add_Data: The queue was full. Use a larger queue size?")
ENDPROC


PROC PRIVATE_Load_Queue_Large_Add_Data(LoadQueueLarge &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, INT paramBitSettings = 0)
	CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to queue load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in large queue.")

	//First check this item isn't already queued.
	INT i
	REPEAT LOAD_QUEUE_SIZE_LARGE i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[i].iData = paramIntData
						CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
						EXIT
					ENDIF
				ENDIF
				IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
					IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
						IF ARE_STRINGS_EQUAL(paramQueue.sQueue[i].txtData, paramStringData)
							CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
							EXIT
						ENDIF
					ENDIF
				ENDIF
				IF paramType = LQT_PTFX_ASSET
					CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Flag the queue as loading if it isn't already.
	IF NOT paramQueue.bLoading
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Flagged queue as loading.")
		paramQueue.bLoading = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queuing load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in large queue.")

	//Find a free slot in the queue and add the load request.
	REPEAT LOAD_QUEUE_SIZE_LARGE i
		IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found slot ", i, " as free.")
			paramQueue.sQueue[i].txtData = paramStringData
			paramQueue.sQueue[i].iData = paramIntData
			paramQueue.sQueue[i].iState = paramBitSettings
			SET_BIT(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
			SET_BIT(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			EXIT
		ENDIF
	ENDREPEAT
	
	//No free slots.
	SCRIPT_ASSERT("PRIVATE_Request_Queue_Large_Add_Data: The queue was full. Do we need to create a larger queue size?")
ENDPROC


PROC PRIVATE_Load_Queue_Light_Add_Data(LoadQueueLight &paramQueue, LoadQueueType paramType, INT paramIntData, INT paramBitSettings = 0)
	CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to queue load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "] in light queue.")

	IF NOT PRIVATE_Is_Load_Queue_Type_Supported_In_Light_Queue(paramType)
		CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Type ", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), " is not supported in light load queues. Please use LoadQueueSmall/LoadQueueMedium/LoadQueueLarge for loads requiring text data.")
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 tlError = "No support for "
			tlError += PRIVATE_Get_Load_Queue_Type_Debug_String(paramType)
			tlError += " in light queues. Ask BenR"
			SCRIPT_ASSERT(tlError)
		#ENDIF
		EXIT
	ENDIF

	//First check this item isn't already queued.
	INT i
	REPEAT LOAD_QUEUE_SIZE_LIGHT i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[i].iData = paramIntData
						CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
						EXIT
					ENDIF
				ENDIF
				IF paramType = LQT_PTFX_ASSET
					CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Flag the queue as loading if it isn't already.
	IF NOT paramQueue.bLoading
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Flagged queue as loading.")
		paramQueue.bLoading = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queuing load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "] in light queue.")

	//Find a free slot in the queue and add the load request.
	REPEAT LOAD_QUEUE_SIZE_LIGHT i
		IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found slot ", i, " as free.")
			paramQueue.sQueue[i].iData = paramIntData
			paramQueue.sQueue[i].iState = paramBitSettings
			SET_BIT(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
			SET_BIT(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			EXIT
		ENDIF
	ENDREPEAT
	
	//No free slots.
	SCRIPT_ASSERT("PRIVATE_Request_Queue_Light_Add_Data: The queue was full. Do we need to create a larger queue size?")
ENDPROC


PROC PRIVATE_Load_Queue_Small_Remove_Data(LoadQueueSmall &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, BOOL paramSetAsNoLongerNeeded = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to remove load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] from small queue.")

	//Find the array index this item is at.
	INT iSearchIndex = 0
	BOOL bFoundItem = FALSE
	WHILE NOT bFoundItem AND iSearchIndex < LOAD_QUEUE_SIZE_SMALL
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[iSearchIndex].iData = paramIntData
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
						IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
							IF ARE_STRINGS_EQUAL(paramQueue.sQueue[iSearchIndex].txtData, paramStringData)
								CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
								bFoundItem = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType = LQT_PTFX_ASSET
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bFoundItem
			iSearchIndex++
		ENDIF
	ENDWHILE
	
	IF NOT bFoundItem
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> The item was not found in the queue. Nothing to remove.")
		EXIT
	ENDIF
	
	//Remove the item from memory if requested.
	IF paramSetAsNoLongerNeeded
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Unloading item from memory as we remove it.")
		PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[iSearchIndex])
	ENDIF
	
	//Iterate through items at higher indexes in the queue and move them up one space.
	BOOL bFoundQueueEnd = FALSE
	WHILE NOT bFoundQueueEnd AND iSearchIndex < (LOAD_QUEUE_SIZE_SMALL - 1)
		iSearchIndex++
		
		//Is the next queue item in use?
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			//Yes, copy it down one space.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Copying queue index ", iSearchIndex, " over index ", iSearchIndex - 1, ".")
			PRIVATE_Copy_Load_Queue_Data(paramQueue.sQueue[iSearchIndex], paramQueue.sQueue[iSearchIndex-1])
		ELSE
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was not in use. Clearing index ", iSearchIndex -1, ".")
			PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex-1])
			bFoundQueueEnd = TRUE
		ENDIF
	ENDWHILE
	
	IF NOT bFoundQueueEnd
		// clear the index we just removed at the queue end
		CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was the last index in queue. Clearing index ", iSearchIndex , ".")
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex])
	ENDIF
ENDPROC


PROC PRIVATE_Load_Queue_Medium_Remove_Data(LoadQueueMedium &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, BOOL paramSetAsNoLongerNeeded = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to remove load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] from medium queue.")

	//Find the array index this item is at.
	INT iSearchIndex = 0
	BOOL bFoundItem = FALSE
	WHILE NOT bFoundItem AND iSearchIndex < LOAD_QUEUE_SIZE_MEDIUM
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[iSearchIndex].iData = paramIntData
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
						IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
							IF ARE_STRINGS_EQUAL(paramQueue.sQueue[iSearchIndex].txtData, paramStringData)
								CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
								bFoundItem = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType = LQT_PTFX_ASSET
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bFoundItem
			iSearchIndex++
		ENDIF
	ENDWHILE
	
	IF NOT bFoundItem
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> The item was not found in the queue. Nothing to remove.")
		EXIT
	ENDIF
	
	//Remove the item from memory if requested.
	IF paramSetAsNoLongerNeeded
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Unloading item from memory as we remove it.")
		PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[iSearchIndex])
	ENDIF
	
	//Iterate through items at higher indexes in the queue and move them up one space.
	BOOL bFoundQueueEnd = FALSE
	WHILE NOT bFoundQueueEnd AND iSearchIndex < (LOAD_QUEUE_SIZE_MEDIUM - 1)
		iSearchIndex++
		
		//Is the next queue item in use?
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			//Yes, copy it down one space.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Copying queue index ", iSearchIndex, " over index ", iSearchIndex - 1, ".")
			PRIVATE_Copy_Load_Queue_Data(paramQueue.sQueue[iSearchIndex], paramQueue.sQueue[iSearchIndex-1])
		ELSE
			//No, we've found the end of the used queue.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was not in use. Clearing index ", iSearchIndex -1, ".")
			PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex-1])
			bFoundQueueEnd = TRUE
		ENDIF
	ENDWHILE
	
	IF NOT bFoundQueueEnd
		// clear the index we just removed at the queue end
		CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was the last index in queue. Clearing index ", iSearchIndex , ".")
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex])
	ENDIF
ENDPROC


PROC PRIVATE_Load_Queue_Large_Remove_Data(LoadQueueLarge &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, BOOL paramSetAsNoLongerNeeded = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to remove load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] from large queue.")

	//Find the array index this item is at.
	INT iSearchIndex = 0
	BOOL bFoundItem = FALSE
	WHILE NOT bFoundItem AND iSearchIndex < LOAD_QUEUE_SIZE_LARGE
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[iSearchIndex].iData = paramIntData
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
						IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
							IF ARE_STRINGS_EQUAL(paramQueue.sQueue[iSearchIndex].txtData, paramStringData)
								CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
								bFoundItem = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType = LQT_PTFX_ASSET
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bFoundItem
			iSearchIndex++
		ENDIF
	ENDWHILE
	
	IF NOT bFoundItem
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> The item was not found in the queue. Nothing to remove.")
		EXIT
	ENDIF
	
	//Remove the item from memory if requested.
	IF paramSetAsNoLongerNeeded
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Unloading item from memory as we remove it.")
		PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[iSearchIndex])
	ENDIF
	
	//Iterate through items at higher indexes in the queue and move them up one space.
	BOOL bFoundQueueEnd = FALSE
	WHILE NOT bFoundQueueEnd AND iSearchIndex < (LOAD_QUEUE_SIZE_LARGE - 1)
		iSearchIndex++
		
		//Is the next queue item in use?
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			//Yes, copy it down one space.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Copying queue index ", iSearchIndex, " over index ", iSearchIndex - 1, ".")
			PRIVATE_Copy_Load_Queue_Data(paramQueue.sQueue[iSearchIndex], paramQueue.sQueue[iSearchIndex-1])
		ELSE
			//No, we've found the end of the used queue.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was not in use. Clearing index ", iSearchIndex -1, ".")
			PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex-1])
			bFoundQueueEnd = TRUE
		ENDIF
	ENDWHILE
	
	IF NOT bFoundQueueEnd
		// clear the index we just removed at the queue end
		CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was the last index in queue. Clearing index ", iSearchIndex , ".")
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex])
	ENDIF
ENDPROC


PROC PRIVATE_Load_Queue_Light_Remove_Data(LoadQueueLight &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, BOOL paramSetAsNoLongerNeeded = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to remove load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] from light queue.")
	paramIntData = paramIntData
	paramStringData = paramStringData
	
	//Find the array index this item is at.
	INT iSearchIndex = 0
	BOOL bFoundItem = FALSE
	WHILE NOT bFoundItem AND iSearchIndex < LOAD_QUEUE_SIZE_LIGHT
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[iSearchIndex].iData = paramIntData
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType = LQT_PTFX_ASSET
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bFoundItem
			iSearchIndex++
		ENDIF
	ENDWHILE
	
	IF NOT bFoundItem
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> The item was not found in the queue. Nothing to remove.")
		EXIT
	ENDIF
	
	//Remove the item from memory if requested.
	IF paramSetAsNoLongerNeeded
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Unloading item from memory as we remove it.")
		PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[iSearchIndex])
	ENDIF
	
	//Iterate through items at higher indexes in the queue and move them up one space.
	BOOL bFoundQueueEnd = FALSE
	WHILE NOT bFoundQueueEnd AND iSearchIndex < (LOAD_QUEUE_SIZE_LIGHT - 1)
		iSearchIndex++
		
		//Is the next queue item in use?
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			//Yes, copy it down one space.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Copying queue index ", iSearchIndex, " over index ", iSearchIndex - 1, ".")
			PRIVATE_Copy_Load_Queue_Data_Light(paramQueue.sQueue[iSearchIndex], paramQueue.sQueue[iSearchIndex-1])
		ELSE
			//No, we've found the end of the used queue.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was not in use. Clearing index ", iSearchIndex -1, ".")
			PRIVATE_Clean_Up_Load_Queue_Data_Light(paramQueue.sQueue[iSearchIndex-1])
			bFoundQueueEnd = TRUE
		ENDIF
	ENDWHILE
	
	IF NOT bFoundQueueEnd
		// clear the index we just removed at the queue end
		CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was the last index in queue. Clearing index ", iSearchIndex , ".")
		PRIVATE_Clean_Up_Load_Queue_Data_Light(paramQueue.sQueue[iSearchIndex])
	ENDIF
ENDPROC
