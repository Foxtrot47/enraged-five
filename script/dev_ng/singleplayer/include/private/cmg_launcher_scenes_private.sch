USING "cmg_launcher_structs_private.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	CLIFFORD MINIGAME FILE: cmg_launcher_scenes_private.sch
//	Sam Hackett
//
//	This is to be included by cmg_launcher_main_private.sch
//
//	It defines the functions for playing looped ambient scenes for each game type.
//
//	See also:
//		cmg_launcher_structs_private.sch
//		cmg_launcher_main_private.sch
//		cmg_launcher_public.sch
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     CMG SCENE - GENERAL
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

STRUCT CMG_SCENE_POSITION
	VECTOR	vPos
	FLOAT	fRot
ENDSTRUCT

FUNC MODEL_NAMES CMG_SCENE_GET_RANDOM_PED_MODEL()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
		CASE 0		RETURN A_M_M_BevHills_01
		CASE 1		RETURN A_M_M_Malibu_01
		CASE 3		RETURN A_M_Y_Business_03
		DEFAULT		RETURN A_M_O_KTown_01
	ENDSWITCH
ENDFUNC

PROC CMG_SCENE_PLAY_ANIM(PED_INDEX hPed, CMG_SCENE_POSITION origin, CMG_SCENE_POSITION offset, STRING sAnimDict, STRING sAnimName, BOOL bLoop = FALSE, FLOAT fStartPhase = 0.0)

	VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(origin.vPos, origin.fRot, offset.vPos)
	VECTOR vRot = <<0.0, 0.0, origin.fRot + offset.fRot>>

	ANIMATION_FLAGS	eTaskFlags = AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS
	
	IF bLoop
		eTaskFlags = eTaskFlags | AF_LOOPING
	ELSE
		eTaskFlags = eTaskFlags | AF_HOLD_LAST_FRAME
	ENDIF

	TASK_PLAY_ANIM_ADVANCED(hPed, sAnimDict, sAnimName, vPos, vRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
							eTaskFlags, fStartPhase, DEFAULT, AIK_DISABLE_LEG_IK)

ENDPROC

FUNC BOOL CMG_SCENE_IS_PED_PLAYING_ANY_ANIM(PED_INDEX hPed, STRING sAnimDict, STRING sAnimName)

//	TEXT_LABEL_63 tAnimDict = sAnimDict
//	TEXT_LABEL_63 tAnimName = sAnimName
//	CPRINTLN(DEBUG_MINIGAME, "CMG_AMB_IS_ANIM_PLAYING(", tAnimDict, ", ", tAnimName, ")")
	
	IF IS_ENTITY_PLAYING_ANIM(hPed, sAnimDict, sAnimName)
	AND GET_ENTITY_ANIM_CURRENT_TIME(hPed, sAnimDict, sAnimName) < 1.0
	
		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CASINO_SUSPEND_SCENE_MP(CASINO_SERVER& server, CASINO_LOCAL& local, INT iTriggerID)

	INT iSceneID = local.mTriggers[iTriggerID].iSceneID
	
	SWITCH local.mTriggers[iTriggerID].eGameType

		CASE CMG_TYPE_Blackjack
			IF iSceneID >= 0 AND iSceneID < CASINO_BJACK_SCENE_MP_MAX
				CASINO_SUSPEND_SCENE_MP_BJACK( server.mBjackScenes[iSceneID] )
			ENDIF
		BREAK
		
		CASE CMG_TYPE_Poker
			IF iSceneID >= 0 AND iSceneID < CASINO_POKER_SCENE_MP_MAX
				CASINO_SUSPEND_SCENE_MP_POKER( server.mPokerScenes[iSceneID] )
			ENDIF
		BREAK

	ENDSWITCH

ENDPROC

PROC CASINO_SUSPEND_SCENE_SP(CASINO_LOCAL& local, INT iTriggerID)

	INT iSceneID = local.mTriggers[iTriggerID].iSceneID
	
	SWITCH local.mTriggers[iTriggerID].eGameType

		CASE CMG_TYPE_Blackjack
			IF iSceneID >= 0 AND iSceneID < CASINO_BJACK_SCENE_SP_MAX
				CASINO_SUSPEND_SCENE_SP_BJACK( local.mBjackScenes[iSceneID] )
			ENDIF
		BREAK
		
		CASE CMG_TYPE_Poker
			IF iSceneID >= 0 AND iSceneID < CASINO_POKER_SCENE_SP_MAX
				CASINO_SUSPEND_SCENE_SP_POKER( local.mPokerScenes[iSceneID] )
			ENDIF
		BREAK

	ENDSWITCH

ENDPROC

PROC CASINO_UPDATE_SCENES_SP(CASINO_LOCAL& local, INT iTriggerID)

	INT iTriggerID
	REPEAT CMGL_TRIGGER_MAX iTriggerID
	
		IF  local.iTriggerID = iTriggerID
		AND local.eState >= CMGL_LOCAL_STATE_RequestMPSeat
		AND local.eState <= CMGL_LOCAL_STATE_ReleaseMPSeat
		
			IF NOT CASINO_IS_SCENE_WAITING(local, iTriggerID)
				CASINO_SET_SCENE_WAITING(local, iTriggerID)
			ENDIF
		
		ELSE
		
			IF CASINO_IS_SCENE_WAITING(local, iTriggerID)
				CASINO_SET_SCENE_CONTINUE(local, iTriggerID)
			ENDIF
			
			CASINO_UPDATE_SCENE(local, iTrigger)
		
		ENDIF
	
	ENDREPEAT

//	INT iTriggerID
//	REPEAT CMGL_TRIGGER_MAX iTriggerID
//	
//		IF CASINO_IS_TRIGGER_ACTIVE(local, iTriggerID)
//		
//			IF NOT CASINO_IS_SCENE_WAITING(local, iTriggerID)
//				CASINO_SET_SCENE_WAITING(local, iTriggerID)
//			ENDIF
//		
//		ELSE
//		
//			IF CASINO_IS_SCENE_WAITING(local, iTriggerID)
//				CASINO_SET_SCENE_CONTINUE(local, iTriggerID)
//			ENDIF
//			
//			CASINO_UPDATE_SCENE(local, iTrigger)
//		
//		ENDIF
//	
//	ENDREPEAT

ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     CMG SCENE - BJACK SP
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CONST_INT CMG_BJACK_SP_SCENE_MaxPlayers 4

ENUM CMG_BJACK_SP_SCENE_CONFIG
	CMG_BJACK_SP_SCENE_CONFIG_Full = 0,
	CMG_BJACK_SP_SCENE_CONFIG_SeatFree
ENDENUM

ENUM CMG_BJACK_SP_SCENE_STATE
	CMG_BJACK_SP_SCENE_STATE_Null = 0,
	CMG_BJACK_SP_SCENE_STATE_Init,
	CMG_BJACK_SP_SCENE_STATE_Suspended,
	
	CMG_BJACK_SP_SCENE_STATE_DealPlayer0,
	CMG_BJACK_SP_SCENE_STATE_DealPlayer1,
	CMG_BJACK_SP_SCENE_STATE_DealPlayer2,
	CMG_BJACK_SP_SCENE_STATE_DealPlayer3
ENDENUM

STRUCT CMG_BJACK_SP_SCENE
	CMG_BJACK_SP_SCENE_CONFIG	eConfig
	CMG_BJACK_SP_SCENE_STATE	eState
	PED_INDEX					hDealerPed
	PED_INDEX					hPlayerPeds[CMG_BJACK_SP_SCENE_MaxPlayers]
	MODEL_NAMES					ePlayerModels[CMG_BJACK_SP_SCENE_MaxPlayers]
ENDSTRUCT


PROC CMG_BJACK_SP_SCENE_SET_STATE(CMG_BJACK_SP_SCENE& scene, CMG_BJACK_SP_SCENE_STATE eNewState)
	CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_SP_SCENE_SET_STATE(", eNewState, ")")
	scene.eState = eNewState
ENDPROC

PROC CMG_BJACK_SP_SCENE_INIT(CMG_BJACK_SP_SCENE& scene)
	scene.eState		= CMG_BJACK_SP_SCENE_STATE_Null
	scene.hDealerPed	= NULL
	INT i
	REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
		scene.hPlayerPeds[i]	= NULL
		scene.ePlayerModels[i]	= DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
ENDPROC

PROC CMG_BJACK_SP_SCENE_CREATE(CMG_BJACK_SP_SCENE& scene, CMG_BJACK_SP_SCENE_CONFIG eConfig = CMG_BJACK_SP_SCENE_CONFIG_SeatFree)
	
	IF scene.eState != CMG_BJACK_SP_SCENE_STATE_Null
		SCRIPT_ASSERT("CMG_BJACK_SP_SCENE_CREATE() - Scene already in use")
	ELSE
	
		CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_SP_SCENE_CREATE(eConfig=", eConfig, ")")
		
		CMG_BJACK_SP_SCENE_SET_STATE(scene, CMG_BJACK_SP_SCENE_STATE_Init)
		scene.eConfig = eConfig
		
		// Set random models
		INT i
		REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
			scene.ePlayerModels[i] = CMG_SCENE_GET_RANDOM_PED_MODEL()
		ENDREPEAT
	
	ENDIF

ENDPROC

PROC CMG_BJACK_SP_SCENE_SUSPEND(CMG_BJACK_SP_SCENE& scene)
	CMG_BJACK_SP_SCENE_SET_STATE(scene, CMG_BJACK_SP_SCENE_STATE_Suspended)
ENDPROC

//PROC CMG_BJACK_SP_SCENE_RESTART(CMG_BJACK_SP_SCENE& scene)
//	CMG_BJACK_SP_SCENE_SET_STATE(scene, CMG_BJACK_SP_SCENE_STATE_Suspended)	// TODO
//ENDPROC

PROC CMG_BJACK_SP_SCENE_UPDATE(CMG_BJACK_SP_SCENE& scene, CMG_SCENE_POSITION origin)
	
	IF scene.eState = CMG_BJACK_SP_SCENE_STATE_Null
		EXIT
	ENDIF

	// Setup ped offsets
	CMG_SCENE_POSITION ped_offsets[CMG_BJACK_SP_SCENE_MaxPlayers]
	
	ped_offsets[0].vPos	= <<0.4, 0.5, 0.0>>
	ped_offsets[0].fRot = 110.0
	ped_offsets[1].vPos	= <<0.2, 0.2, 0.0>>
	ped_offsets[1].fRot = 70.0
	ped_offsets[2].vPos	= <<-0.05, 0.1, 0.0>>
	ped_offsets[2].fRot = 6.0
	ped_offsets[3].vPos	= <<-0.3, 0.25, 0.0>>
	ped_offsets[3].fRot = -40.0
	
	CMG_SCENE_POSITION dealer_offset
	dealer_offset.vPos = <<0.0, 0.0, 0.0>>
	dealer_offset.fRot = 0.0

	// Update state
	INT i
	SWITCH scene.eState
		CASE CMG_BJACK_SP_SCENE_STATE_Init
			
			// Requets ped models
			REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
				REQUEST_MODEL(scene.ePlayerModels[i])
			ENDREPEAT

			REQUEST_MODEL(S_M_M_StrPreach_01)
			
			// Request anim dictionaries
			REQUEST_ANIM_DICT("anim@mini@blackjack2@dealer")
			REQUEST_ANIM_DICT("anim@mini@blackjack@ped_b")
			
			// Wait for ped models to load
			REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
				IF NOT HAS_MODEL_LOADED(scene.ePlayerModels[i])
					EXIT
				ENDIF
			ENDREPEAT
			IF NOT HAS_MODEL_LOADED(S_M_M_StrPreach_01)
				EXIT
			ENDIF
			
			// Wait for anim dictionaries to load
			IF NOT HAS_ANIM_DICT_LOADED("anim@mini@blackjack2@dealer")
			OR NOT HAS_ANIM_DICT_LOADED("anim@mini@blackjack@ped_b")
				EXIT
			ENDIF
			
			// Create dealer
			scene.hDealerPed = CREATE_PED(PEDTYPE_CIVMALE, S_M_M_StrPreach_01, origin.vPos, origin.fRot)
			IF DOES_ENTITY_EXIST(scene.hDealerPed)
				CMG_SCENE_PLAY_ANIM(scene.hDealerPed, origin, dealer_offset, "anim@mini@blackjack2@dealer", "DEAL_IDLE_LOOP", TRUE, GET_RANDOM_FLOAT_IN_RANGE(0,1))
			ENDIF

			// Create players
			REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
				IF scene.eConfig = CMG_BJACK_SP_SCENE_CONFIG_Full
				OR i != 2
				
					scene.hPlayerPeds[i] = CREATE_PED(PEDTYPE_CIVMALE, scene.ePlayerModels[i], origin.vPos, origin.fRot)
					IF DOES_ENTITY_EXIST(scene.hPlayerPeds[i])
						CMG_SCENE_PLAY_ANIM(scene.hPlayerPeds[i], origin, ped_offsets[i], "anim@mini@blackjack@ped_b", "IDLE_E", TRUE, GET_RANDOM_FLOAT_IN_RANGE(0,1))
					ENDIF
				
				ENDIF
			ENDREPEAT
			
			// Remove ped models
			REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
				SET_MODEL_AS_NO_LONGER_NEEDED(scene.ePlayerModels[i])
			ENDREPEAT
		
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_StrPreach_01)
			
			// Set state
			CMG_BJACK_SP_SCENE_SET_STATE(scene, CMG_BJACK_SP_SCENE_STATE_DealPlayer0)
			
		BREAK
	ENDSWITCH
ENDPROC

PROC CMG_BJACK_SP_SCENE_RESUME(CMG_BJACK_SP_SCENE& scene, CMG_SCENE_POSITION origin)

	IF scene.eState != CMG_BJACK_SP_SCENE_STATE_Suspended
		SCRIPT_ASSERT("CMG_BJACK_SP_SCENE_RESUME() - Scene not in suspension")
	ELSE
	
		CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_SP_SCENE_RESUME()")
		
		// Setup ped offsets
		CMG_SCENE_POSITION ped_offsets[CMG_BJACK_SP_SCENE_MaxPlayers]
		
		ped_offsets[0].vPos	= <<0.4, 0.5, 0.0>>
		ped_offsets[0].fRot = 110.0
		ped_offsets[1].vPos	= <<0.2, 0.2, 0.0>>
		ped_offsets[1].fRot = 70.0
		ped_offsets[2].vPos	= <<-0.05, 0.1, 0.0>>
		ped_offsets[2].fRot = 6.0
		ped_offsets[3].vPos	= <<-0.3, 0.25, 0.0>>
		ped_offsets[3].fRot = -40.0
		
		CMG_SCENE_POSITION dealer_offset
		dealer_offset.vPos = <<0.0, 0.0, 0.0>>
		dealer_offset.fRot = 0.0

		// Grab dealer
		IF DOES_ENTITY_EXIST(scene.hDealerPed)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scene.hDealerPed, FALSE)
				SET_ENTITY_AS_MISSION_ENTITY(scene.hDealerPed, TRUE, TRUE)
			ENDIF
		ENDIF

		// Grab players
		INT i
		REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
			IF DOES_ENTITY_EXIST(scene.hPlayerPeds[i])
				IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scene.hPlayerPeds[i], FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(scene.hPlayerPeds[i], TRUE, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Reset dealer anim
		IF NOT IS_PED_INJURED(scene.hDealerPed)
			CMG_SCENE_PLAY_ANIM(scene.hDealerPed, origin, dealer_offset, "anim@mini@blackjack2@dealer", "DEAL_IDLE_LOOP", TRUE, GET_RANDOM_FLOAT_IN_RANGE(0,1))
		ENDIF

		// Reset players anim
		REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
			IF NOT IS_PED_INJURED(scene.hPlayerPeds[i])
				CMG_SCENE_PLAY_ANIM(scene.hPlayerPeds[i], origin, ped_offsets[i], "anim@mini@blackjack@ped_b", "IDLE_E", TRUE, GET_RANDOM_FLOAT_IN_RANGE(0,1))
			ENDIF
		ENDREPEAT
		
		// Set state
		CMG_BJACK_SP_SCENE_SET_STATE(scene, CMG_BJACK_SP_SCENE_STATE_DealPlayer0)

	ENDIF

ENDPROC

PROC CMG_BJACK_SP_SCENE_DELETE(CMG_BJACK_SP_SCENE& scene)

	IF scene.eState = CMG_BJACK_SP_SCENE_STATE_Null
		SCRIPT_ASSERT("CMG_BJACK_SP_SCENE_DELETE() - Scene not in use")
	ELSE
	
		CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_SP_SCENE_DELETE()")
		
		// Delete dealer
		IF DOES_ENTITY_EXIST(scene.hDealerPed)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scene.hDealerPed, FALSE)
				SET_ENTITY_AS_MISSION_ENTITY(scene.hDealerPed, TRUE, TRUE)
			ENDIF
			DELETE_PED(scene.hDealerPed)
		ENDIF

		// Delete players
		INT i
		REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
			IF DOES_ENTITY_EXIST(scene.hPlayerPeds[i])
				IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scene.hPlayerPeds[i], FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(scene.hPlayerPeds[i], TRUE, TRUE)
				ENDIF
				DELETE_PED(scene.hPlayerPeds[i])
			ENDIF
		ENDREPEAT
		
		// Remove ped models
//		REPEAT CMG_BJACK_SP_SCENE_MaxPlayers i
//			SET_MODEL_AS_NO_LONGER_NEEDED(scene.ePlayerModels[i])
//		ENDREPEAT
//	
//		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_StrPreach_01)
//
//		// Remove anim dictionaries
//		REMOVE_ANIM_DICT("anim@mini@blackjack2@dealer")
//		REMOVE_ANIM_DICT("anim@mini@blackjack@ped_b")
		
		// Reset data
		CMG_BJACK_SP_SCENE_INIT(scene)	
	
	ENDIF

ENDPROC

PROC CMG_BJACK_SP_SCENE_CLEANUP()

	CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_SP_SCENE_CLEANUP()")
	
	// Remove ped models
//	SET_MODEL_AS_NO_LONGER_NEEDED(scene.ePlayerModels[i])
//	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_StrPreach_01)

	// Remove anim dictionaries
	REMOVE_ANIM_DICT("anim@mini@blackjack2@dealer")
	REMOVE_ANIM_DICT("anim@mini@blackjack@ped_b")
	
ENDPROC

//FUNC BOOL CMG_BJACK_SP_SCENE_IS_VALID(CMG_BJACK_SP_SCENE& scene)
//ENDFUNC
//
//FUNC BOOL CMG_BJACK_SP_SCENE_IS_INITING(CMG_BJACK_SP_SCENE& scene)
//ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     CMG SCENE - BJACK MP
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ENUM CMG_BJACK_MP_SCENE_CONFIG
	CMG_BJACK_MP_SCENE_CONFIG_Normal = 0
ENDENUM

ENUM CMG_BJACK_MP_SCENE_STATE
	CMG_BJACK_MP_SCENE_STATE_Null = 0,
	CMG_BJACK_MP_SCENE_STATE_Init,
	CMG_BJACK_MP_SCENE_STATE_Suspended,
	
	CMG_BJACK_MP_SCENE_STATE_Idle
ENDENUM

STRUCT CMG_BJACK_MP_SCENE
	CMG_BJACK_MP_SCENE_CONFIG	eConfig
	CMG_BJACK_MP_SCENE_STATE	eState
	NETWORK_INDEX				hDealerPed
ENDSTRUCT


PROC CMG_BJACK_MP_SCENE_SET_STATE(CMG_BJACK_MP_SCENE& scene, CMG_BJACK_MP_SCENE_STATE eNewState)
	CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_MP_SCENE_SET_STATE(", eNewState, ")")
	scene.eState = eNewState
ENDPROC

PROC CMG_BJACK_MP_SCENE_INIT(CMG_BJACK_MP_SCENE& scene)
	scene.eState		= CMG_BJACK_MP_SCENE_STATE_Null
	scene.hDealerPed	= NULL
ENDPROC

PROC CMG_BJACK_MP_SCENE_CREATE(CMG_BJACK_MP_SCENE& scene, CMG_BJACK_MP_SCENE_CONFIG eConfig = CMG_BJACK_MP_SCENE_CONFIG_Normal)
	
	IF scene.eState != CMG_BJACK_MP_SCENE_STATE_Null
		SCRIPT_ASSERT("CMG_BJACK_MP_SCENE_CREATE() - Scene already in use")
	ELSE
	
		CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_MP_SCENE_CREATE(eConfig=", eConfig, ")")
		
		CMG_BJACK_MP_SCENE_SET_STATE(scene, CMG_BJACK_MP_SCENE_STATE_Init)
		scene.eConfig = eConfig
		
	ENDIF

ENDPROC

PROC CMG_BJACK_MP_SCENE_SUSPEND(CMG_BJACK_MP_SCENE& scene)
	CMG_BJACK_MP_SCENE_SET_STATE(scene, CMG_BJACK_MP_SCENE_STATE_Suspended)
ENDPROC

//PROC CMG_BJACK_MP_SCENE_RESTART(CMG_BJACK_MP_SCENE& scene)
//	CMG_BJACK_MP_SCENE_SET_STATE(scene, CMG_BJACK_MP_SCENE_STATE_Suspended)	// TODO
//ENDPROC

PROC CMG_BJACK_MP_SCENE_UPDATE(CMG_BJACK_MP_SCENE& scene, CMG_SCENE_POSITION origin)
	
	IF scene.eState = CMG_BJACK_MP_SCENE_STATE_Null
		EXIT
	ENDIF

	// Setup ped offsets
	CMG_SCENE_POSITION dealer_offset
	dealer_offset.vPos = <<0.0, 0.0, 0.0>>
	dealer_offset.fRot = 0.0

	// Update state
	SWITCH scene.eState
		CASE CMG_BJACK_MP_SCENE_STATE_Init
			
			// Request ped model
			REQUEST_MODEL(S_M_M_StrPreach_01)
			
			// Request anim dictionary
			REQUEST_ANIM_DICT("anim@mini@blackjack2@dealer")
			
			// Wait for assets to load
			IF NOT HAS_MODEL_LOADED(S_M_M_StrPreach_01)
			OR NOT HAS_ANIM_DICT_LOADED("anim@mini@blackjack2@dealer")
				EXIT
			ENDIF
			
			// Create dealer
			scene.hDealerPed = PED_TO_NET( CREATE_PED(PEDTYPE_CIVMALE, S_M_M_StrPreach_01, origin.vPos, origin.fRot) )
			IF NETWORK_DOES_NETWORK_ID_EXIST(scene.hDealerPed)
				CMG_SCENE_PLAY_ANIM(NET_TO_PED(scene.hDealerPed), origin, dealer_offset, "anim@mini@blackjack2@dealer", "DEAL_IDLE_LOOP", TRUE, GET_RANDOM_FLOAT_IN_RANGE(0,1))
			ENDIF

			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_StrPreach_01)
			
			// Set state
			CMG_BJACK_MP_SCENE_SET_STATE(scene, CMG_BJACK_MP_SCENE_STATE_Idle)
			
		BREAK
	ENDSWITCH
ENDPROC

PROC CMG_BJACK_MP_SCENE_RESUME(CMG_BJACK_MP_SCENE& scene, CMG_SCENE_POSITION origin)

	IF scene.eState != CMG_BJACK_MP_SCENE_STATE_Suspended
		SCRIPT_ASSERT("CMG_BJACK_MP_SCENE_RESUME() - Scene not in suspension")
	ELSE
	
		CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_MP_SCENE_RESUME()")
		
		// Setup ped offsets
		CMG_SCENE_POSITION dealer_offset
		dealer_offset.vPos = <<0.0, 0.0, 0.0>>
		dealer_offset.fRot = 0.0

		// Grab dealer
		IF DOES_ENTITY_EXIST(NET_TO_PED(scene.hDealerPed))
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(NET_TO_PED(scene.hDealerPed), FALSE)
				SET_ENTITY_AS_MISSION_ENTITY(NET_TO_PED(scene.hDealerPed), TRUE, TRUE)
			ENDIF
		ENDIF

		// Reset dealer anim
		IF NOT IS_PED_INJURED(NET_TO_PED(scene.hDealerPed))
			CMG_SCENE_PLAY_ANIM(NET_TO_PED(scene.hDealerPed), origin, dealer_offset, "anim@mini@blackjack2@dealer", "DEAL_IDLE_LOOP", TRUE, GET_RANDOM_FLOAT_IN_RANGE(0,1))
		ENDIF

		// Set state
		CMG_BJACK_MP_SCENE_SET_STATE(scene, CMG_BJACK_MP_SCENE_STATE_Idle)

	ENDIF

ENDPROC

PROC CMG_BJACK_MP_SCENE_DELETE(CMG_BJACK_MP_SCENE& scene)

	IF scene.eState = CMG_BJACK_MP_SCENE_STATE_Null
		SCRIPT_ASSERT("CMG_BJACK_MP_SCENE_DELETE() - Scene not in use")
	ELSE
	
		CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_MP_SCENE_DELETE()")
		
		// Delete dealer
		IF DOES_ENTITY_EXIST(scene.hDealerPed)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scene.hDealerPed, FALSE)
				SET_ENTITY_AS_MISSION_ENTITY(scene.hDealerPed, TRUE, TRUE)
			ENDIF
			DELETE_PED(scene.hDealerPed)
		ENDIF

		// Remove ped models
//		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_StrPreach_01)
//
//		// Remove anim dictionaries
//		REMOVE_ANIM_DICT("anim@mini@blackjack2@dealer")
//		REMOVE_ANIM_DICT("anim@mini@blackjack@ped_b")
		
		// Reset data
		CMG_BJACK_MP_SCENE_INIT(scene)	
	
	ENDIF

ENDPROC

PROC CMG_BJACK_MP_SCENE_CLEANUP()

	CPRINTLN(DEBUG_MINIGAME, "CMG_BJACK_MP_SCENE_CLEANUP()")
	
	// Remove ped models
//	SET_MODEL_AS_NO_LONGER_NEEDED(scene.ePlayerModels[i])
//	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_StrPreach_01)

	// Remove anim dictionaries
	REMOVE_ANIM_DICT("anim@mini@blackjack2@dealer")
	REMOVE_ANIM_DICT("anim@mini@blackjack@ped_b")
	
ENDPROC

//FUNC BOOL CMG_BJACK_MP_SCENE_IS_VALID(CMG_BJACK_MP_SCENE& scene)
//ENDFUNC
//
//FUNC BOOL CMG_BJACK_MP_SCENE_IS_INITING(CMG_BJACK_MP_SCENE& scene)
//ENDFUNC

















PROC CLIFFORD_MG_UpdateScenes(CLIFFORD_MG_LAUNCHER_LOCAL& local)

	INT iTriggerID
	REPEAT CMGL_TRIGGER_MAX iTriggerID
	
		INT iSceneID = local.mTriggers[iTriggerID].iGameID

		// If trigger is active
		IF  local.eState >= CMGL_LOCAL_STATE_RequestMPSeat
		AND local.eState <= CMGL_LOCAL_STATE_ReleaseMPSeat
		AND local.iTriggerID = iTriggerID
		
			// Make sure scene is suspended
			IF NOT CLIFFORD_MG_BJACK_SCENE_IS_SUSPENDED(local.mBjackScenes[iSceneID])
				CLIFFORD_MG_BJACK_SCENE_SUSPEND(local.mBjackScenes[iSceneID])
			ENDIF
		
		ELSE
		
			// Wake up if suspended (or not running)
			IF NOT CLIFFORD_MG_BJACK_SCENE_IS_VALID(local.mBjackScenes[iSceneID])
			OR CLIFFORD_MG_BJACK_SCENE_IS_SUSPENDED(local.mBjackScenes[iSceneID])
			
				CLIFFORD_MG_BJACK_SCENE_WAKE_UP(local.mBjackScenes[iSceneID])
			
			ENDIF
		
		ENDIF
		
		// Update scene
		IF CLIFFORD_MG_BJACK_SCENE_IS_VALID(local.mBjackScenes[iSceneID])
			CLIFFORD_MG_BJACK_SCENE_UPDATE(local.mBjackScenes[iSceneID])
		ENDIF
	
	ENDREPEAT
	
	// Manage scenes
	IF local.eConfig = CLIFFORD_MG_LAUNCHER_CONFIG_AddOffCamera
	
		iTriggerID = GET_RANDOM_INT_IN_RANGE(0, CMGL_TRIGGER_MAX)
		
		IF NOT CLIFFORD_MG_BJACK_SCENE_IS_VALID(local.mBjackScenes[iSceneID])
			IF CLIFFORD_MG_BJACK_SCENE_IS_HIDDEN(local.mBjackScenes[iSceneID])
				CLIFFORD_MG_BJACK_SCENE_CREATE(local.mBjackScenes[iSceneID])
			ENDIF
		ENDIF
		
	ENDIF
			
ENDPROC
