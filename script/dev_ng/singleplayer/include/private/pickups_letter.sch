
/// PURPOSE: Returns coords of letter scrap pickup
FUNC VECTOR GET_LETTERSCRAP_PICKUP_COORDS(INT iIndex, BOOL bForChop=FALSE)
		
	// If any of these have a bForChop vector set, Chop will lead the player to the bForChop vector then orientate himself towards the actual pickup vector.
	// Small alterations are fine but if you move a pickup more than 1-2 metres away from its current position you must update the bForChop vector too or else we'll have Chop leading the player to nothing.
	// The bForChop vector must be on a section of navmesh that is connected to the main navmesh. See Kev E if unsure.
	SWITCH iIndex

		CASE 0
			IF bForChop = TRUE
				RETURN << 1034.27429, -3026.27539, 4.90197 >>
			ELSE
				RETURN << 1026.7053, -3026.0515, 13.3323 >>
			ENDIF
		BREAK
		CASE 1
			IF bForChop = TRUE
				RETURN << -1040.97888, -2743.50928, 12.94983 >>
			ELSE
				RETURN <<-1048.6035, -2734.2180, 12.8895>>
			ENDIF
		BREAK
		CASE 2
			IF bForChop = TRUE
				RETURN << -93.90120, -2711.31445, 5.01752 >>
			ELSE
				RETURN << -81.1199, -2726.5112, 7.7400 >>
			ENDIF
		BREAK
		CASE 3   RETURN <<-917.6909, -2527.3843, 22.3218>> 				BREAK
		CASE 4
			IF bForChop = TRUE
				RETURN << 746.45, -2310.32, 26.03 >>
			ELSE
				RETURN << 748.922, -2298.114, 19.624 >>
			ENDIF
		BREAK
		CASE 5
			IF bForChop = TRUE
				RETURN << 1509.74207, -2126.03760, 75.21973 >>
			ELSE
				RETURN << 1509.0994, -2120.5510, 75.61 >>
			ENDIF
		BREAK
		CASE 6   RETURN << 76.0032, -1970.4752, 20.1302 >> 				BREAK
		CASE 7
			IF bForChop = TRUE
				RETURN << -1.82327, -1732.61438, 28.29367 >> 
			ELSE
				RETURN << 0.067, -1734.027, 30.606 >> 
			ENDIF
		BREAK
		CASE 8
			IF bForChop = TRUE
				RETURN << -1377.76563, -1409.83704, 4.63205 >>
			ELSE
				RETURN << -1380.4912, -1404.3735, 1.7273 >>
			ENDIF
		BREAK
		CASE 9   RETURN <<2864.8079, -1372.8402, 1.3151>> 				BREAK
		CASE 10  RETURN << -1035.8115, -1273.0769, 0.8919 >> 			BREAK
		CASE 11  RETURN << -1821.1364, -1201.3599, 18.1698 >> 			BREAK
		CASE 12  RETURN << 643.0116, -1035.6504, 35.8891 >> 			BREAK
		CASE 13  RETURN <<-119.0616, -977.2228, 303.23 >> 				BREAK
		CASE 14
			IF bForChop = TRUE
				RETURN << -1243.10364, -507.80569, 30.10775 >>
			ELSE
				RETURN << -1238.7655, -506.7138, 37.6019 >>
			ENDIF
		BREAK
		CASE 15
			IF bForChop = TRUE
				RETURN << 83.79993, -431.93021, 36.55315 >>
			ELSE
				RETURN << 86.4, -433.9, 36.0 >>
			ENDIF
		BREAK
		CASE 16  RETURN << 1095.9534, -210.4642, 54.9477 >> 			BREAK
		CASE 17  RETURN << -1724.5217, -196.0, 57.2387 >> 				BREAK
		CASE 18  RETURN << 265.374, -199.546, 60.795 >> 				BREAK
		CASE 19
			IF bForChop = TRUE
				RETURN << -3020.47485, 36.55431, 9.11777 >>
			ELSE
				RETURN << -3021.4, 38.0, 10.2945 >>
			ENDIF
		BREAK
		CASE 20
			IF bForChop = TRUE
				RETURN << -347.52768, 53.37161, 52.97814 >>
			ELSE
				RETURN << -347.3, 54.865, 53.921 >>
			ENDIF
		BREAK
		CASE 21  RETURN << 1052.2484, 167.6110, 87.7406 >> 				BREAK
		CASE 22  RETURN << -2303.7976, 217.4301, 166.6017 >> 			BREAK
		CASE 23  RETURN << -138.9423, 868.3885, 231.6956 >> 			BREAK
		CASE 24
			IF bForChop = TRUE
				RETURN << 688.1073, 1204.6713, 323.3438 >>
			ELSE
				RETURN << 682.4505, 1204.9277, 344.3322 >>
			ENDIF
		BREAK
		CASE 25  RETURN <<-1548.7627, 1380.1727, 125.3728>> 			BREAK
		CASE 26
			IF bForChop = TRUE
				RETURN << -432.14, 1598.46, 355.73 >>
			ELSE
				RETURN << -432.0034, 1597.1292, 356.613 >>
			ENDIF
		BREAK
		CASE 27  RETURN << 3081.93, 1648.29, 2.42 >> 					BREAK
		CASE 28  RETURN << -594.38, 2092.00, 130.57 >> 					BREAK
		CASE 29
			IF bForChop = TRUE
				RETURN << 3069.21289, 2160.98828, 1.13270 >>
			ELSE
				RETURN << 3063.5828, 2212.6299, 2.5863 >>
			ENDIF
		BREAK
		CASE 30  RETURN << 180.21, 2263.83, 91.87 >> 					BREAK
		CASE 31
			IF bForChop = TRUE
				RETURN << 926.96, 2445.36, 49.09 >>
			ELSE
				RETURN << 929.6946, 2444.1155, 48.4300 >>
			ENDIF
		BREAK
		CASE 32
			IF bForChop = TRUE
				RETURN <<-2380.21240, 2655.17578, 0.83200>>
			ELSE
				RETURN <<-2379.9482, 2656.9534, 1.4906>>
			ENDIF
		BREAK
		CASE 33  RETURN << -861.38, 2753.30, 12.867 >>  				BREAK
		CASE 34  RETURN << -289.0195, 2848.8533, 53.3310 >> 			BREAK
		CASE 35
			IF bForChop = TRUE
				RETURN << 288.84085, 2871.91162, 42.64220 >>
			ELSE
				RETURN << 265.7415, 2866.4160, 73.19 >>
			ENDIF
		BREAK
		CASE 36
			IF bForChop = TRUE
				RETURN << 1297.37805, 2988.71021, 40.11787 >>
			ELSE
				RETURN << 1294.2, 3001.9, 57.7 >>
			ENDIF
		BREAK
		CASE 37  RETURN << 1568.65, 3572.8, 32.294 >> 					BREAK
		CASE 38  RETURN << -1608.62, 4274.25, 102.95 >> 				BREAK
		CASE 39
			IF bForChop = TRUE
				RETURN << -3.51812, 4332.45068, 31.21602 >>
			ELSE
				RETURN << -1.9585, 4334.7871, 32.3702 >>
			ENDIF
		BREAK
		CASE 40  RETURN << 1336.7367, 4307.1997, 37.1325 >> 			BREAK
		CASE 41
			IF bForChop = TRUE
				RETURN << -1007.10284, 4836.93604, 268.54880 >>
			ELSE
				RETURN << -1001.4800, 4851.3218, 273.6112 >>
			ENDIF
		BREAK
		CASE 42  RETURN << 1877.090, 5078.980, 50.490 >> 				BREAK
		CASE 43
			IF bForChop = TRUE
				RETURN << 3366.09863, 5182.46143, 0.68317 >>
			ELSE
				RETURN << 3436.4526, 5176.9111, 6.3860 >>
			ENDIF
		BREAK
		CASE 44
			IF bForChop = TRUE
				RETURN << -576.12, 5472.24, 59.28 >>
			ELSE
				RETURN <<-578.8057, 5470.1641, 59.0295>>
			ENDIF
		BREAK
		CASE 45  RETURN << 444.6518, 5571.7813, 780.1888 >> 			BREAK
		CASE 46  RETURN << -402.9948, 6319.2793, 31.2256 >> 			BREAK
		CASE 47  RETURN << 1439.5989, 6335.2075, 22.9485 >> 			BREAK
		CASE 48
			IF bForChop = TRUE
				RETURN << 1466.10901, 6552.26563, 12.95773 >>
			ELSE
				RETURN << 1469.6321, 6552.1743, 13.6854 >>
			ENDIF
		BREAK
		CASE 49
			IF bForChop = TRUE
				RETURN << 66.19278, 6668.88770, 30.80633 >>
			ELSE
				RETURN << 66.7136, 6663.1978, 30.7821 >>
			ENDIF
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid index passed to GET_LETTERSCRAP_PICKUP_COORDS")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Returns true if Chop can reach the scrap, else false
/// PARAMS:
///    index - the index of the pickup
FUNC BOOL CAN_CHOP_REACH_LETTERSCRAP(INT index)
	
	SWITCH (index)
		CASE 3	FALLTHRU	// On a roof
		CASE 9	FALLTHRU	// On island
		CASE 10	FALLTHRU	// Surrounded by canal
		CASE 13 FALLTHRU	// On crane
		CASE 27	FALLTHRU	// On island
		CASE 38	FALLTHRU	// On rock plateau
			RETURN FALSE
		BREAK
		CASE 14
			IF IS_BIT_SET(g_iRestrictedAreaBitSet, 7) //AC_MOVIE_STUDIO
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
