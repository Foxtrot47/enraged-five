
USING "rage_builtins.sch"
USING "globals.sch"
USING "friends_public.sch"
USING "selector_public.sch"
USING "comms_control_public.sch"

USING "script_clock.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "properties_public.sch"
#endif
#endif

USING "player_scene_coords.sch"
USING "player_scene_vehicle.sch"
USING "player_scene_assets.sch"

#IF IS_DEBUG_BUILD
USING "flow_debug_GAME.sch"
USING "player_ped_debug.sch"
USING "respawn_location_private.sch"
USING "vector_id_public.sch"
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_schedule.sch									//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


SP_MISSIONS eTREV_CITY_SAFEHOUSE_REMOVED	= SP_MISSION_TREVOR_4	//SP_MISSION_TREVOR_5

SP_MISSIONS eMichHasMetSolomon				= SP_MISSION_MICHAEL_4	//SP_MISSION_SOLOMON_4
SP_MISSIONS old_sp_mission_trevor_4			= SP_MISSION_TREVOR_3


// *******************************************************************************************
//	PED REQUEST BIT FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL GET_ONE_OFF_BIT_FOR_PED_REQUEST_SCENE(PED_REQUEST_SCENE_ENUM eScene, INT &iOneOffBit)

	SWITCH eScene
		CASE PR_SCENE_M2_PHARMACY		iOneOffBit = 1 RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_a		iOneOffBit = 2 RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b		iOneOffBit = 3 RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_a 	iOneOffBit = 4 RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1		iOneOffBit = 5 RETURN TRUE BREAK
		CASE PR_SCENE_F_CLUB			iOneOffBit = 6 RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONCELL			iOneOffBit = 7 RETURN TRUE BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT	iOneOffBit = 8 RETURN TRUE BREAK
		CASE PR_SCENE_F1_NEWHOUSE		iOneOffBit = 9 RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		iOneOffBit = 10 RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE	iOneOffBit = 11 RETURN TRUE BREAK
		CASE PR_SCENE_M4_WASHFACE		iOneOffBit = 12 RETURN TRUE BREAK
		
		//#1568441
		CASE PR_SCENE_Fa_PHONECALL_ARM3	iOneOffBit = 13 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM1	iOneOffBit = 13 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM3	iOneOffBit = 13 RETURN TRUE BREAK
		
		#IF IS_DEBUG_BUILD
		CASE NUM_OF_PED_REQUEST_SCENES	iOneOffBit = 14 RETURN FALSE BREAK
		#ENDIF
	ENDSWITCH
	
	iOneOffBit = -1
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	GENERAL SCHEDULE FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL HasHourPassedSincePedTimeStruct(TIMEOFDAY sLastTimeActive)
	RETURN HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 1)
ENDFUNC
FUNC BOOL HasHourPassedSinceCharLastTimeActive(enumCharacterList sceneCharID)
	TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[sceneCharID]
	RETURN HasHourPassedSincePedTimeStruct(sLastTimeActive)
ENDFUNC

FUNC BOOL IsCharPresentInWorld(enumCharacterList sceneCharID)
	
	SELECTOR_SLOTS_ENUM eSceneSlot = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(sceneCharID)
	PED_INDEX scenePedID = g_sPlayerPedRequest.sSelectorPeds.pedID[eSceneSlot]
	
	IF DOES_ENTITY_EXIST(scenePedID)
		IF NOT IS_PED_INJURED(scenePedID)
//			VECTOR vPlayerCurrentCoord			= GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
//			VECTOR vScenePedCurrentCoord		= GET_ENTITY_COORDS(scenePedID)
//			
//			FLOAT fDistanceFromPlayerToScenePed	= VDIST(vPlayerCurrentCoord, vScenePedCurrentCoord)
//			
//			CONST_FLOAT		fRADIUS_FROM_SCENEPED_TO_CHECK				4.0
//			
//			CONST_FLOAT		fDIST_FROM_SCENEPED_VISIBLE_FOR_DEFAULT		140.0
//			CONST_FLOAT		fDIST_FROM_SCENEPED_HIDDEN_FOR_DEFAULT		40.0
//			
//			IF IS_SPHERE_VISIBLE(vScenePedCurrentCoord, fRADIUS_FROM_SCENEPED_TO_CHECK)
//				#IF IS_DEBUG_BUILD
//				DrawDebugSceneLineBetweenEntities(PLAYER_PED_ID(), scenePedID, HUD_COLOUR_BLUELIGHT)
//				
//				TEXT_LABEL_63 str = GET_PLAYER_PED_STRING(sceneCharID)
//				str += " visible "
//				str += GET_STRING_FROM_FLOAT(fDistanceFromPlayerToScenePed)
//				str += " < "
//				str += (ROUND(fDIST_FROM_SCENEPED_VISIBLE_FOR_DEFAULT))
//				DrawDebugSceneTextWithOffset(str, vPlayerCurrentCoord, 0, HUD_COLOUR_BLUELIGHT)
//				#ENDIF
//				
//				IF fDistanceFromPlayerToScenePed < fDIST_FROM_SCENEPED_VISIBLE_FOR_DEFAULT
//					RETURN TRUE
//				ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD
//				DrawDebugSceneLineBetweenEntities(PLAYER_PED_ID(), scenePedID, HUD_COLOUR_BLUEDARK)
//				
//				TEXT_LABEL_63 str = GET_PLAYER_PED_STRING(sceneCharID)
//				str += " hidden "
//				str += GET_STRING_FROM_FLOAT(fDistanceFromPlayerToScenePed)
//				str += " < "
//				str += ROUND(fDIST_FROM_SCENEPED_HIDDEN_FOR_DEFAULT)
//				DrawDebugSceneTextWithOffset(str, vPlayerCurrentCoord, 0, HUD_COLOUR_BLUEDARK)
//				#ENDIF
//				
//				IF fDistanceFromPlayerToScenePed < fDIST_FROM_SCENEPED_HIDDEN_FOR_DEFAULT
//					RETURN TRUE
//				ENDIF
//			ENDIF
			
			RETURN TRUE
			
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugSceneLineBetweenEntities(PLAYER_PED_ID(), scenePedID, HUD_COLOUR_REDLIGHT)
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HasDayPassedSincePedTimeStruct(TIMEOFDAY sLastTimeActive)
	IF NOT Is_TimeOfDay_Valid(sLastTimeActive)
		RETURN FALSE
	ENDIF
	
	INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
	GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
	
	IF (iYears	> 0)
	OR (iMonths	> 0)
	OR (iDays	> 0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_IsRandomCharFlagSet(g_eRC_MissionIDs eRCMission, g_eRC_FlagIDs eRCFlagID)
	#if USE_CLF_DLC
		unused_parameter(eRCMission)
		unused_parameter(eRCFlagID)
		RETURN FALSE
	#endif
	#if USE_NRM_DLC
		unused_parameter(eRCMission)
		unused_parameter(eRCFlagID)
		RETURN FALSE
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[eRCMission].rcFlags, ENUM_TO_INT(eRCFlagID))
		
		#IF IS_DEBUG_BUILD
		IF g_bDebugPrint_SceneScheduleInfo
		
		g_structRCMissionsStatic sRCMissionDetails
		Retrieve_Random_Character_Static_Mission_Details(eRCMission, sRCMissionDetails)
		
		CPRINTLN(DEBUG_SWITCH, "PRIVATE_IsRandomCharFlagSet(", sRCMissionDetails.rcScriptName, ", ", ENUM_TO_INT(eRCFlagID), ")")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
	#endif
	#endif	
ENDFUNC

FUNC BOOL PRIVATE_DoPedRequestScenesMatch(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene,
		PED_REQUEST_SCENE_ENUM eTestSceneOne,
		PED_REQUEST_SCENE_ENUM eTestSceneTwo,
		PED_REQUEST_SCENE_ENUM eTestSceneThree = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneFour = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneFive = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneSix = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneSeven = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneEight = PR_SCENE_INVALID)
	
	
	IF ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneOne)		AND (eTestSceneOne <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneTwo)		AND (eTestSceneTwo <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneThree)	AND (eTestSceneThree <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneFour)		AND (eTestSceneFour <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneFive)		AND (eTestSceneFive <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneSix)		AND (eTestSceneSix <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneSeven)	AND (eTestSceneSeven <> PR_SCENE_INVALID))
	OR ((g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eTestSceneEight)	AND (eTestSceneEight <> PR_SCENE_INVALID))
		IF ((eReqScene = eTestSceneOne)		AND (eTestSceneOne <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneTwo)		AND (eTestSceneTwo <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneThree)	AND (eTestSceneThree <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneFour)	AND (eTestSceneFour <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneFive)	AND (eTestSceneFive <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneSix)		AND (eTestSceneSix <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneSeven)	AND (eTestSceneSeven <> PR_SCENE_INVALID))
		OR ((eReqScene = eTestSceneEight)	AND (eTestSceneEight <> PR_SCENE_INVALID))
			
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by match [last known ", Get_String_From_Ped_Request_Scene_Enum(g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]), "]")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IsPedRequestSceneDupeOfLastKnownScene(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
	IF (eReqScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed])
		RETURN TRUE
	ENDIF
	
	SWITCH ePed
	
		/* michael's overlapping scenes */
		CASE CHAR_MICHAEL
		
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M2_SAVEHOUSE1_a, PR_SCENE_M2_SAVEHOUSE1_b, PR_SCENE_M2_SMOKINGGOLF)
				RETURN TRUE
			ENDIF
		
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M2_BEDROOM, PR_SCENE_M2_SAVEHOUSE0_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M4_WAKEUPSCREAM, PR_SCENE_M4_WAKESUPSCARED)
				RETURN TRUE
			ENDIF
			
//			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
//					PR_SCENE_M6_HOUSETV_a, PR_SCENE_M6_HOUSETV_b)
//				RETURN TRUE
//			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_POOLSIDE_a, PR_SCENE_M_POOLSIDE_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_CANAL_a, PR_SCENE_M_CANAL_b, PR_SCENE_M_CANAL_c)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_PARKEDHILLS_a, PR_SCENE_M_PARKEDHILLS_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M6_PARKEDHILLS_a, PR_SCENE_M6_PARKEDHILLS_b, PR_SCENE_M6_PARKEDHILLS_c, PR_SCENE_M6_PARKEDHILLS_d, PR_SCENE_M6_PARKEDHILLS_e)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M2_WIFEEXITSCAR, PR_SCENE_M2_DROPOFFDAU_a, PR_SCENE_M2_DROPOFFDAU_b, PR_SCENE_M2_DROPOFFSON_a, PR_SCENE_M2_DROPOFFSON_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M4_EXITRESTAURANT, PR_SCENE_M4_LUNCH_b, PR_SCENE_M4_CINEMA)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_COFFEE_a, PR_SCENE_M_COFFEE_b, PR_SCENE_M_COFFEE_c)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_TRAFFIC_a, PR_SCENE_M_TRAFFIC_b, PR_SCENE_M_TRAFFIC_c)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M2_CYCLING_a, PR_SCENE_M2_CYCLING_b, PR_SCENE_M2_CYCLING_c)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_PIER_a, PR_SCENE_M_PIER_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_VWOODPARK_a, PR_SCENE_M_VWOODPARK_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M_BENCHCALL_a, PR_SCENE_M_BENCHCALL_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M2_CARSLEEP_a, PR_SCENE_M2_CARSLEEP_b)
				RETURN TRUE
			ENDIF
			
//			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
//					PR_SCENE_M_BAR_a, PR_SCENE_M_BAR_b)
//				RETURN TRUE
//			ENDIF
			
//			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
//					PR_SCENE_M_HOOKERMOTEL, PR_SCENE_M_HOOKERCAR)
//				RETURN TRUE
//			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M7_EMPLOYEECONVO, PR_SCENE_M7_TALKTOGUARD, PR_SCENE_M7_LOT_JIMMY)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_M7_OPENDOORFORAMA, PR_SCENE_M7_DROPPINGOFFJMY, PR_SCENE_M7_TRACEYEXITSCAR)
				RETURN TRUE
			ENDIF
		
		BREAK
		
		/* franklins's overlapping scenes */
		CASE CHAR_FRANKLIN
		
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_F_MD_KUSH_DOC, PR_SCENE_F_KUSH_DOC_a, PR_SCENE_F_KUSH_DOC_b, PR_SCENE_F_KUSH_DOC_c)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_F_THROW_CUP, PR_SCENE_F_HIT_CUP_HAND)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_F_CS_CHECKSHOE, PR_SCENE_F_CS_WIPEHANDS, PR_SCENE_F_CS_WIPERIGHT)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_F0_BIKE, PR_SCENE_F1_BIKE, PR_SCENE_F_BIKE_c, PR_SCENE_F_BIKE_d)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_F_BAR_a_01, PR_SCENE_F_BAR_b_01, PR_SCENE_F_BAR_c_02, PR_SCENE_F_BAR_d_02, PR_SCENE_F_BAR_e_01)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_F1_POOLSIDE_a, PR_SCENE_F1_POOLSIDE_b)
				RETURN TRUE
			ENDIF
		
		BREAK
		
		/* trevor's overlapping scenes */
		CASE CHAR_TREVOR
		
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_DOCKS_a, PR_SCENE_T_DOCKS_b, PR_SCENE_T_DOCKS_c, PR_SCENE_T_DOCKS_d)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_CN_CHATEAU_b, PR_SCENE_T_CN_CHATEAU_c, PR_SCENE_T_CR_CHATEAU_d)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_PUKEINTOFOUNT, PR_SCENE_T_CN_PARK_b)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_CR_POLICE_a, PR_SCENE_T_CN_POLICE_b, PR_SCENE_T_CN_POLICE_c)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_FLOYDSPOON_A, PR_SCENE_T_FLOYDSPOON_B, PR_SCENE_T_FLOYDSPOON_B2, PR_SCENE_T_FLOYDSPOON_A2)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_FLOYD_BEAR, PR_SCENE_T_FLOYD_DOLL, PR_SCENE_T_FLOYDPINEAPPLE)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_FLOYDCRYING_A, PR_SCENE_T_FLOYDCRYING_E0, PR_SCENE_T_FLOYDCRYING_E1, PR_SCENE_T_FLOYDCRYING_E2, PR_SCENE_T_FLOYDCRYING_E3)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_CR_DUMPSTER, PR_SCENE_T_CN_WAKETRASH_b, PR_SCENE_T_CR_WAKEBEACH, PR_SCENE_T_CN_WAKEBARN, PR_SCENE_T_CN_WAKETRAIN, PR_SCENE_T_CR_WAKEROOFTOP, PR_SCENE_T_CN_WAKEMOUNTAIN)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_CR_CHASECAR_a, PR_SCENE_T_CN_CHASECAR_b, PR_SCENE_T_CR_CHASEBIKE, PR_SCENE_T_CR_CHASESCOOTER)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_FIGHTBAR_a, PR_SCENE_T_FIGHTBAR_b, PR_SCENE_T_FIGHTBAR_c,
					PR_SCENE_T_FIGHTBBUILD, PR_SCENE_T_ESCORTED_OUT,
					PR_SCENE_T_YELLATDOORMAN, PR_SCENE_T_FIGHTYAUCLUB_b, PR_SCENE_T_FIGHTCASINO)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_NAKED_BRIDGE, PR_SCENE_T_NAKED_GARDEN)
				RETURN TRUE
			ENDIF
			
			IF PRIVATE_DoPedRequestScenesMatch(ePed, eReqScene,
					PR_SCENE_T_GARBAGE_FOOD, PR_SCENE_T_THROW_FOOD)
				RETURN TRUE
			ENDIF
		
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IsPedRequestScenesBlockedForFriend(PED_REQUEST_SCENE_ENUM eReqScene)
	
	SWITCH eReqScene
		// Lamar scenes
//		CASE PR_SCENE_F_LAMGRAFF		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_P1		FALLTHRU
//		CASE PR_SCENE_F_LAMTAUNT_P2		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_P3		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_P5		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_NIGHT	
			IF HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY(CHAR_FRANKLIN, CHAR_LAMAR)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by connection ", GetLabel_enumFriendConnection(FC_FRANKLIN_LAMAR))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			IF HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY(CHAR_TREVOR, CHAR_LAMAR)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by connection ", GetLabel_enumFriendConnection(FC_TREVOR_LAMAR))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
		// Jimmy scenes
		CASE PR_SCENE_M2_KIDS_TV		FALLTHRU
		CASE PR_SCENE_M2_DROPOFFSON_a	FALLTHRU
		CASE PR_SCENE_M2_DROPOFFSON_b	FALLTHRU
		CASE PR_SCENE_M7_DROPPINGOFFJMY	FALLTHRU
		CASE PR_SCENE_M7_BIKINGJIMMY
			IF HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY(CHAR_MICHAEL, CHAR_JIMMY)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by connection ", GetLabel_enumFriendConnection(FC_MICHAEL_JIMMY))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			IF HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY(CHAR_FRANKLIN, CHAR_JIMMY)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by connection ", GetLabel_enumFriendConnection(FC_FRANKLIN_JIMMY))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			IF HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY(CHAR_TREVOR, CHAR_JIMMY)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by connection ", GetLabel_enumFriendConnection(FC_TREVOR_JIMMY))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK

		// Amanda scenes
		CASE PR_SCENE_M2_BEDROOM			FALLTHRU
		CASE PR_SCENE_M2_SAVEHOUSE0_b		FALLTHRU
		CASE PR_SCENE_M2_SAVEHOUSE1_a		FALLTHRU
		CASE PR_SCENE_M2_LUNCH_a			FALLTHRU
		CASE PR_SCENE_M2_WIFEEXITSCAR		FALLTHRU
		CASE PR_SCENE_M2_ARGUEWITHWIFE		FALLTHRU
		CASE PR_SCENE_M7_RESTAURANT
			IF HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY(CHAR_MICHAEL, CHAR_AMANDA)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by connection ", GetLabel_enumFriendConnection(FC_MICHAEL_AMANDA))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_3)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(SP_MISSION_FAMILY_3), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK

		// Floyd scenes
		CASE PR_SCENE_T_FLOYDSAVEHOUSE
		CASE PR_SCENE_T_FLOYD_BEAR
		CASE PR_SCENE_T_FLOYD_DOLL
		CASE PR_SCENE_T_FLOYDPINEAPPLE
		
		CASE PR_SCENE_T_FLOYDCRYING_A
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
		
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			
			IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2A)
			OR IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2B)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"")
				
				
				IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2A)
					CPRINTLN(DEBUG_SWITCH, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(SP_HEIST_DOCKS_2A))
				ENDIF
				IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2B)
					IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2A)
						CPRINTLN(DEBUG_SWITCH, "\" and \"")
					ENDIF
					
					CPRINTLN(DEBUG_SWITCH, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(SP_HEIST_DOCKS_2B))
				ENDIF
				
				CPRINTLN(DEBUG_SWITCH, "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IsPedRequestScenesBlockedForBuddyInAvailableMission(PED_REQUEST_SCENE_ENUM eReqScene,
		PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	sPedScene.iStage = 0
	sPedScene.eScene = eReqScene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	
	IF SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
		
		IF (sPassedScene.eSceneBuddy <> NO_CHARACTER)
		AND (sPassedScene.eSceneBuddy <> CHAR_BLANK_ENTRY)
			INT iBIT_SCENE_BUDDY = BIT_NOBODY
			INT iMissionID
			SP_MISSIONS eMissionID
			
			SWITCH sPassedScene.eSceneBuddy
				CASE CHAR_LAMAR
					iBIT_SCENE_BUDDY = BIT_LAMAR
				BREAK
				CASE CHAR_JIMMY
					iBIT_SCENE_BUDDY = BIT_JIMMY
				BREAK
				CASE CHAR_AMANDA
					iBIT_SCENE_BUDDY = BIT_AMANDA
				BREAK
				
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			
			INT availableMissionIndex
			#IF USE_CLF_DLC
			REPEAT COUNT_OF(g_availableMissionsTU) availableMissionIndex
			//Is a valid mission saved in this slot?
				IF g_availableMissionsTU[availableMissionIndex].index != ILLEGAL_ARRAY_POSITION
					//Yes. Check the coreVars index it points to.
					iMissionID = g_flowUnsaved.coreVars[g_availableMissionsTU[availableMissionIndex].index].iValue1
			#ENDIF
			#IF USE_NRM_DLC
			REPEAT COUNT_OF(g_availableMissionsTU) availableMissionIndex
				//Is a valid mission saved in this slot?
				IF g_availableMissionsTU[availableMissionIndex].index != ILLEGAL_ARRAY_POSITION
					//Yes. Check the coreVars index it points to.
					iMissionID = g_flowUnsaved.coreVars[g_availableMissionsTU[availableMissionIndex].index].iValue1
			#ENDIF
			#IF NOT USE_CLF_DLC
			#IF NOT USE_NRM_DLC
			REPEAT COUNT_OF(g_availableMissions) availableMissionIndex
				//Is a valid mission saved in this slot?
				IF g_availableMissions[availableMissionIndex].index != ILLEGAL_ARRAY_POSITION
					//Yes. Check the coreVars index it points to.
					iMissionID = g_flowUnsaved.coreVars[g_availableMissions[availableMissionIndex].index].iValue1
			#ENDIF
			#ENDIF
				
					eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionID)
					
					IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, iBIT_SCENE_BUDDY)
						#IF IS_DEBUG_BUILD
						IF g_bDebugPrint_SceneScheduleInfo
						CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by buddy ", GET_PLAYER_PED_STRING(sPassedScene.eSceneBuddy), " on mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
						ENDIF
						#ENDIF
						
						RETURN TRUE
					ENDIF
					
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IsPedRequestScenesBlockedForAvailableMission(PED_REQUEST_SCENE_ENUM eReqScene)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	SP_MISSIONS eMissionID = SP_MISSION_NONE
	SWITCH eReqScene
		CASE PR_SCENE_M2_BEDROOM		FALLTHRU	// #379457
		CASE PR_SCENE_M2_SAVEHOUSE0_b	FALLTHRU	// #379457
		CASE PR_SCENE_M2_SAVEHOUSE1_a	FALLTHRU
		CASE PR_SCENE_M2_KIDS_TV		FALLTHRU
		CASE PR_SCENE_M_POOLSIDE_a		FALLTHRU
		CASE PR_SCENE_M2_ARGUEWITHWIFE	FALLTHRU
		CASE PR_SCENE_M2_CARSLEEP_a					//#1381663
			eMissionID = SP_MISSION_FAMILY_1
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			eMissionID = SP_MISSION_FAMILY_3
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			eMissionID = SP_MISSION_TREVOR_1
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED		FALLTHRU		//#1570367
		CASE PR_SCENE_M4_WAKEUPSCREAM		FALLTHRU
		CASE PR_SCENE_M4_WATCHINGTV			FALLTHRU
		CASE PR_SCENE_M4_WASHFACE
			
			eMissionID = SP_MISSION_FAMILY_6
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY		FALLTHRU		//#1573264
		CASE PR_SCENE_M7_DROPPINGOFFJMY		FALLTHRU
		CASE PR_SCENE_M7_FAKEYOGA			FALLTHRU
		CASE PR_SCENE_M7_GETSREADY			FALLTHRU
		CASE PR_SCENE_M7_KIDS_GAMING		FALLTHRU
		CASE PR_SCENE_M7_KIDS_TV			FALLTHRU
		CASE PR_SCENE_M7_LOT_JIMMY			FALLTHRU
		CASE PR_SCENE_M7_LOUNGECHAIRS		FALLTHRU
		CASE PR_SCENE_M7_OPENDOORFORAMA		FALLTHRU
		CASE PR_SCENE_M7_RESTAURANT			FALLTHRU
		CASE PR_SCENE_M7_ROUNDTABLE			FALLTHRU
		CASE PR_SCENE_M7_TRACEYEXITSCAR		FALLTHRU
		CASE PR_SCENE_M7_WIFETENNIS
			
			eMissionID = SP_MISSION_MICHAEL_4
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_F0_CLEANCAR		FALLTHRU
		CASE PR_SCENE_F0_BIKE			FALLTHRU
//		CASE PR_SCENE_F_LAMGRAFF		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_P1		FALLTHRU
//		CASE PR_SCENE_F_LAMTAUNT_P2		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_P3		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_P5		FALLTHRU
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			eMissionID = SP_MISSION_FRANKLIN_1
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	FALLTHRU
		CASE PR_SCENE_T_FLOYD_BEAR		FALLTHRU
		CASE PR_SCENE_T_FLOYD_DOLL		FALLTHRU
		CASE PR_SCENE_T_FLOYDPINEAPPLE	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_A	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E0	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E1	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E2	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E3	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_A	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B2	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_A2	
			eMissionID = old_sp_mission_trevor_4
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			eMissionID = eTREV_CITY_SAFEHOUSE_REMOVED		//614152
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			eMissionID = SP_HEIST_DOCKS_1		//1348541
			IF IS_MISSION_AVAILABLE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
			eMissionID = SP_HEIST_DOCKS_1
			IF NOT GET_MISSION_COMPLETE_STATE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by uncompleted mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_CN_WAKEBARN
			eMissionID = SP_HEIST_FINALE_2_INTRO
			IF GET_MISSION_COMPLETE_STATE(eMissionID)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by completed mission \"", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IsPedRequestScenesBlockedForLastPassedMission(PED_REQUEST_SCENE_ENUM eReqScene)
	
	
	SWITCH eReqScene
		CASE PR_SCENE_T_HEADINSINK
		CASE PR_SCENE_T6_FLUSHESFOOT
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
		CASE PR_SCENE_T_JERKOFF
		#ENDIF
		CASE PR_SCENE_T_SMOKEMETH
			
			IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
			AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
				//
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by completed mission \"", "RC_MRS_PHILIPS_1", "\"")
				
//				CASSERTLN(DEBUG_SWITCH, "trevor scene blocked for RC_MRS_PHILIPS_1!!!")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	SWITCH g_eLastMissionPassed
//		CASE SP_HEIST_AGENCY_3A
//			IF (eReqScene = PR_SCENE_T_SWEATSHOP)
//				
//				#IF IS_DEBUG_BUILD
//				IF g_bDebugPrint_SceneScheduleInfo
//				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by last passed mission \"", "agency_heist3A", "\" id:", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eLastMissionPassed))
//				ENDIF
//				#ENDIF
//				
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE SP_MISSION_FRANKLIN_2
			IF (eReqScene = PR_SCENE_M2_CYCLING_a)
				
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by last passed mission \"", "franklin2", "\" id:", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eLastMissionPassed))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Is_PedRequest_In_Queued_VectorID(PED_REQUEST_SCENE_ENUM eReqScene)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	INT iQueueGameTimer = GET_GAME_TIMER()
	CONST_INT iCONST_QueueGameTimerMs 180000	//3min
	
	VECTOR vRequestCoord
	FLOAT fHead
	TEXT_LABEL_31 tRoom
	IF GET_PLAYER_PED_POSITION_FOR_SCENE(eReqScene, vRequestCoord, fHead, tRoom)
		
		INT index
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			
			VectorID eVectorID = GET_QUEUED_CALL_VECTOR_ID(index)
			IF eVectorID <> VID_BLANK
				IF ((eVectorID <> VID_BLANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_HARBOR) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RACE_TRACK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_WINDMILLS) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_NORTH_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_MAZE_BANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CRANE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RIVER_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RUNAWAY_TRAIN) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_GOLF_COURSE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1_5K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_ROCK_CLIFF))
					
					INT iQueuedCallsDelay = g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedCallsDelay < iCONST_QueueGameTimerMs)
						IF IS_COORD_IN_VECTOR_ID_AREA(vRequestCoord, eVectorID)
							
							#IF IS_DEBUG_BUILD
							IF g_bDebugPrint_SceneScheduleInfo
							CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by call eVectorID ", GET_DEBUG_STRING_FOR_VECTOR_ID(eVectorID), " [", TO_FLOAT(iQueuedCallsDelay) / 1000.0, "s]")
							ENDIF
							#ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
			
			VectorID eVectorID = GET_QUEUED_TEXT_VECTOR_ID(index)
			IF eVectorID <> VID_BLANK
				IF ((eVectorID <> VID_BLANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_HARBOR) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RACE_TRACK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_WINDMILLS) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_NORTH_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_MAZE_BANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CRANE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RIVER_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RUNAWAY_TRAIN) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_GOLF_COURSE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1_5K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_ROCK_CLIFF))
					
					INT iQueuedTextsDelay = g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedTextsDelay < iCONST_QueueGameTimerMs)
						IF IS_COORD_IN_VECTOR_ID_AREA(vRequestCoord, eVectorID)
						
							#IF IS_DEBUG_BUILD
							IF g_bDebugPrint_SceneScheduleInfo
							CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by text eVectorID ", GET_DEBUG_STRING_FOR_VECTOR_ID(eVectorID), " [", TO_FLOAT(iQueuedTextsDelay) / 1000.0, "s]")
							ENDIF
							#ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
			
			VectorID eVectorID = GET_QUEUED_EMAIL_VECTOR_ID(index)
			IF eVectorID <> VID_BLANK
				IF ((eVectorID <> VID_BLANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_HARBOR) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RACE_TRACK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_WINDMILLS) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_NORTH_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_MAZE_BANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CRANE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RIVER_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RUNAWAY_TRAIN) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_GOLF_COURSE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1_5K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_ROCK_CLIFF))
					
					INT iQueuedEmailsDelay = g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedEmailsDelay < iCONST_QueueGameTimerMs)
						IF IS_COORD_IN_VECTOR_ID_AREA(vRequestCoord, eVectorID)
						
							#IF IS_DEBUG_BUILD
							IF g_bDebugPrint_SceneScheduleInfo
							CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by email eVectorID ", GET_DEBUG_STRING_FOR_VECTOR_ID(eVectorID), " [", TO_FLOAT(iQueuedEmailsDelay) / 1000.0, "s]")
							ENDIF
							#ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Is_PedRequest_Close_To_Blip_Coord(PED_REQUEST_SCENE_ENUM eReqScene, 
													VECTOR vRequestCoord, 
													VECTOR vBlipCoord,
													FLOAT &fCloseDist2,
													STATIC_BLIP_NAME_ENUM eBlipName, 
													INT iBlipCoord = 0)
	INT iBlip = ENUM_TO_INT(eBlipName)
													
	//Range check the blip before accessing arrays.											
	IF ENUM_TO_INT(iBlip) < 0 OR ENUM_TO_INT(iBlip) >= g_iTotalStaticBlips
	OR eBlipName = STATIC_BLIP_NAME_DUMMY_FINAL
		CASSERTLN(DEBUG_BLIP, "PRIVATE_Is_PedRequest_Close_To_Blip_Coord: Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(iBlip), " was out of range to query. Bug BenR.")
		RETURN FALSE
	ENDIF
	
	IF g_GameBlips[iBlip].eCategory = STATIC_BLIP_CATEGORY_MINIGAME
	OR g_GameBlips[iBlip].eCategory = STATIC_BLIP_CATEGORY_SAVEHOUSE	//#1490906
	OR g_GameBlips[iBlip].eCategory = STATIC_BLIP_CATEGORY_PROPERTY		//#1490906
	OR g_GameBlips[iBlip].eCategory = STATIC_BLIP_CATEGORY_SHOP			//#2093111
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT	fCONST_MIN_DIST_FROM_BLIP						7.5
	CONST_FLOAT	fCONST_MIN_DIST_FROM_MISSION_BLIP				10.0		//min:9.251859	url:bugstar:958564
	CONST_FLOAT	fCONST_MIN_DIST_FROM_RCM_BLIP					20.0		//
	CONST_FLOAT	fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE			50.0		//min:46.611153	url:bugstar:887427
	CONST_FLOAT	fCONST_MIN_SCENE_DIST_FROM_SAFEHOUSE			30.0
	IF VDIST2(vRequestCoord, vBlipCoord) < (fCONST_MIN_DIST_FROM_BLIP*fCONST_MIN_DIST_FROM_BLIP)
		fCloseDist2 = VDIST2(vRequestCoord, vBlipCoord)
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugPrint_SceneScheduleInfo
	CDEBUG1LN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene),
		" allows blipcheck, nearest mission ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(iBlip),
		" [dist:", VDIST(vRequestCoord, vBlipCoord), "m]")
	ENDIF
	#ENDIF
	
	FLOAT fMin_dist_from_blip = -1
	IF (g_GameBlips[iBlip].eCategory = STATIC_BLIP_CATEGORY_MISSION)
		fMin_dist_from_blip = fCONST_MIN_DIST_FROM_MISSION_BLIP
		
		// #1420453 //
		INT iTriggerIndex
		REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
			IF g_TriggerableMissions[iTriggerIndex].bUsed
				//Is this trigger registered to use the blip we are interested in?
				IF g_TriggerableMissions[iTriggerIndex].eBlip = eBlipName
					fMin_dist_from_blip = g_sMissionActiveData[g_TriggerableMissions[iTriggerIndex].eMissionID].leaveAreaDistance
				ENDIF
			ENDIF
		ENDREPEAT
		// // // // //
	ELIF (g_GameBlips[iBlip].eCategory = STATIC_BLIP_CATEGORY_RANDOMCHAR)
		fMin_dist_from_blip = fCONST_MIN_DIST_FROM_RCM_BLIP
	ENDIF
	
	IF fMin_dist_from_blip > 0
		IF VDIST2(vRequestCoord, vBlipCoord) < (fMin_dist_from_blip*fMin_dist_from_blip)
			fCloseDist2 = VDIST2(vRequestCoord, vBlipCoord)
			RETURN TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_bDebugPrint_SceneScheduleInfo
		CDEBUG1LN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene),
			" allows extra blipcheck, nearest mission ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(iBlip),
			" [dist:", VDIST(vRequestCoord, vBlipCoord),
			" <= ", fMin_dist_from_blip,
			"m]")
		ENDIF
		#ENDIF
		
		IF (eReqScene <> PR_SCENE_M2_CARSLEEP_a)	//#1021775
			
			/* SAFEHOUSE BLIPS #782819 */
			SAVEHOUSE_NAME_ENUM iSavehouse
			REPEAT NUMBER_OF_SAVEHOUSE_LOCATIONS iSavehouse
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iSavehouse], REPAWN_FLAG_AVAILABLE_BIT)					
					IF VDIST2(g_sSavehouses[iSavehouse].vBlipCoords, vBlipCoord) < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
						IF VDIST2(g_sSavehouses[iSavehouse].vBlipCoords, vRequestCoord) < (fCONST_MIN_SCENE_DIST_FROM_SAFEHOUSE*fCONST_MIN_SCENE_DIST_FROM_SAFEHOUSE)
							fCloseDist2 = VDIST2(g_sSavehouses[iSavehouse].vBlipCoords, vRequestCoord)
							eReqScene = eReqScene
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDREPEAT
		ENDIF
	ENDIF
	/* * * * * * * * * * * * * */
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugPrint_SceneScheduleInfo
	CDEBUG1LN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene),
		" allows blipcheck, nearest mission ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(iBlip),
		" [dist:", VDIST(vRequestCoord, vBlipCoord), "m]")
	ENDIF
	#ENDIF
	
	iBlipCoord = iBlipCoord
	fCloseDist2 = 0.0
	RETURN FALSE	
			
ENDFUNC


FUNC BOOL PRIVATE_Is_PedRequest_In_Active_Blip(PED_REQUEST_SCENE_ENUM eReqScene)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	VECTOR vRequestCoord
	FLOAT fHead
	TEXT_LABEL_31 tRoom
	IF GET_PLAYER_PED_POSITION_FOR_SCENE(eReqScene, vRequestCoord, fHead, tRoom)
		
		INT iTriggerIndex
		REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
			IF g_TriggerableMissions[iTriggerIndex].bUsed AND g_TriggerableMissions[iTriggerIndex].eType != MISSION_TRIGGER_TYPE_SWITCH
				// B*-2266498 MISSION_TRIGGER_TYPE_SWITCH - don't run.  This type doesn't have an eBlip. (Causes array overrun)
				//Is this trigger registered to use the blip we are interested in?
				STATIC_BLIP_NAME_ENUM eTriggerableBlip = g_TriggerableMissions[iTriggerIndex].eBlip
				
				// B*-2344951 Ensure the blip is in range to be extra safe.
				IF ENUM_TO_INT(eTriggerableBlip) >= 0 AND ENUM_TO_INT(eTriggerableBlip) < g_iTotalStaticBlips
			
					VECTOR vBlipCoord = GET_STATIC_BLIP_POSITION(eTriggerableBlip)
					FLOAT fCloseDist2 = 0.0
					IF PRIVATE_Is_PedRequest_Close_To_Blip_Coord(eReqScene, vRequestCoord, vBlipCoord, fCloseDist2, eTriggerableBlip)
						#IF IS_DEBUG_BUILD
						IF g_bDebugPrint_SceneScheduleInfo
						CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by eTriggerableBlip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eTriggerableBlip)),
								" [", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iTriggerIndex].eMissionID, TRUE),
								", dist:", SQRT(fCloseDist2), "m]")
						ENDIF
						#ENDIF
						
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						IF g_bDebugPrint_SceneScheduleInfo
						CDEBUG1LN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " falls through eTriggerableBlip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eTriggerableBlip)),
								" [", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iTriggerIndex].eMissionID, TRUE), "]")
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CDEBUG1LN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " unused by eTriggerableBlip ",
						" [", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iTriggerIndex].eMissionID, TRUE), "]")
				ENDIF
				#ENDIF
			ENDIF
		ENDREPEAT
		
		STATIC_BLIP_NAME_ENUM eBlip
		REPEAT g_iTotalStaticBlips eBlip
			IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_ACTIVE)
				
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CDEBUG1LN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene),
						" needs to check for mission ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eBlip)))
				ENDIF
				#ENDIF
				
				VECTOR vBlipCoord = GET_STATIC_BLIP_POSITION(eBlip)
				FLOAT fCloseDist2 = 0.0
				IF PRIVATE_Is_PedRequest_Close_To_Blip_Coord(eReqScene, vRequestCoord, vBlipCoord, fCloseDist2, eBlip)
					
					#IF IS_DEBUG_BUILD
					IF g_bDebugPrint_SceneScheduleInfo
					CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by eBlip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eBlip)), " [dist:", SQRT(fCloseDist2), "m]")
					ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
				
				//for multi-char missions
				IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eBlip].bMultiCoordAndSprite
					
					INT iMultiBlip, iMULTI_BLIP_COUNT = 3
					REPEAT iMULTI_BLIP_COUNT iMultiBlip
						VECTOR vMultiBlipCoord = GET_STATIC_BLIP_POSITION(eBlip, iMultiBlip)
						IF NOT IS_VECTOR_ZERO(vMultiBlipCoord)
							IF PRIVATE_Is_PedRequest_Close_To_Blip_Coord(eReqScene, vRequestCoord, vMultiBlipCoord, fCloseDist2, eBlip, iMultiBlip)
							
								#IF IS_DEBUG_BUILD
								IF g_bDebugPrint_SceneScheduleInfo
								CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by eMultiBlip[", iMultiBlip, "] ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eBlip)), " [dist:", SQRT(fCloseDist2), "m]")
								ENDIF
								#ENDIF
								
								RETURN TRUE
							ENDIF
							
						ENDIF
						
					ENDREPEAT
					
				ENDIF
			ENDIF
			
		ENDREPEAT
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Does_PedRequest_Have_Queued_Char(PED_REQUEST_SCENE_ENUM eReqScene,
		PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	
	IF bDescentOnlyScene
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	sPedScene.iStage = 0
	sPedScene.eScene = eReqScene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	
	IF SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
		
		IF (eReqScene = PR_SCENE_M6_DEPRESSED)				//#989626
			sPassedScene.eSceneBuddy = CHAR_DR_FRIEDLANDER
		ENDIF
		
		IF (sPassedScene.eSceneBuddy <> NO_CHARACTER)
			IF DOES_CHAR_HAVE_COMMUNICATION_QUEUED(sPassedScene.eSceneBuddy)
						
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by queued comm from char ", GET_PLAYER_PED_STRING(sPassedScene.eSceneBuddy))
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_PedRequest_In_City(PED_REQUEST_SCENE_ENUM eReqScene)
	VECTOR vRequestCoord
	FLOAT fHead
	TEXT_LABEL_31 tRoom
	IF GET_PLAYER_PED_POSITION_FOR_SCENE(eReqScene, vRequestCoord, fHead, tRoom)
		IF vRequestCoord.y < 400.0
			IF vRequestCoord.x < 1400.0
				IF vRequestCoord.x > -1900.0
					IF vRequestCoord.y > -3500.0
						RETURN TRUE
					ENDIF 
				ENDIF
			ENDIF  
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#if USE_CLF_DLC
FUNC BOOL PRIVATE_Is_PedRequest_Blocked_To_CityCLF(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
	
	unused_parameter(eReqScene)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	SWITCH ePed
		CASE CHAR_MICHAEL		
			
			RETURN FALSE
		BREAK
		CASE CHAR_TREVOR
						
			RETURN FALSE
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL PRIVATE_Is_PedRequest_Blocked_To_CityNRM(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
	
	unused_parameter(eReqScene)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	SWITCH ePed
		CASE CHAR_MICHAEL
						
			RETURN FALSE
		BREAK
		CASE CHAR_NRM_JIMMY
			
			RETURN FALSE
		BREAK
		CASE CHAR_NRM_TRACEY
			
			RETURN FALSE
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#endif
FUNC BOOL PRIVATE_Is_PedRequest_Blocked_To_City(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
#if  USE_CLF_DLC
	return PRIVATE_Is_PedRequest_Blocked_To_CityCLF(ePed,eReqScene)
#endif
#if USE_NRM_DLC
	return PRIVATE_Is_PedRequest_Blocked_To_CityNRM(ePed,eReqScene)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			IF Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
			AND NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
				IF PRIVATE_Is_PedRequest_In_City(eReqScene)
					#IF IS_DEBUG_BUILD
					IF g_bDebugPrint_SceneScheduleInfo
					CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked exile [", "michael in exile", "]")
					ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE CHAR_TREVOR
			
			IF Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY)
				IF PRIVATE_Is_PedRequest_In_City(eReqScene)
					#IF IS_DEBUG_BUILD
					IF g_bDebugPrint_SceneScheduleInfo
					CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked [", "trevor restricted to country", "]")
					ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
				
			ENDIF
			
			IF Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_CITY)
				IF NOT PRIVATE_Is_PedRequest_In_City(eReqScene)
					#IF IS_DEBUG_BUILD
					IF g_bDebugPrint_SceneScheduleInfo
					CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked [", "trevor restricted to city", "]")
					ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
				
			ENDIF
			
			IF Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
			AND NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
				IF PRIVATE_Is_PedRequest_In_City(eReqScene)
					#IF IS_DEBUG_BUILD
					IF g_bDebugPrint_SceneScheduleInfo
					CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked exile [", "trevor in exile", "]")
					ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
#endif
#endif
ENDFUNC

FUNC BOOL PRIVATE_Is_PedRequest_In_LastSceneQueue(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
	
	INT iQueue
	REPEAT g_iCOUNT_OF_LAST_SCENE_QUEUE iQueue
		IF (eReqScene = g_SavedGlobals.sPlayerSceneData.g_eLastSceneQueue[ePed][iQueue])
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked last scene queue [", iQueue, "]")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC



FUNC BOOL PRIVATE_Has_One_Off_PedRequest_Been_Seen(PED_REQUEST_SCENE_ENUM eReqScene)
	
	INT iOneOffBit
	IF GET_ONE_OFF_BIT_FOR_PED_REQUEST_SCENE(eReqScene, iOneOffBit)
		
		//#1574416
		IF eReqScene = PR_SCENE_F1_NEWHOUSE
			IF NOT IS_BIT_SET(g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit, iOneOffBit)
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_2)
					SET_BIT(g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit, iOneOffBit)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit, iOneOffBit)
			
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked one-off seen [", g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit, "]")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT PRIVATE_Get_Switch_Clock_Hours()
	IF IS_TIMEOFDAY_VALID(g_sMultiplayerTransitionTOD)
		RETURN GET_TIMEOFDAY_HOUR(g_sMultiplayerTransitionTOD)
	ENDIF
	
	RETURN GET_CLOCK_HOURS()
ENDFUNC

FUNC INT PRIVATE_Get_Switch_Clock_Minutes()
	IF IS_TIMEOFDAY_VALID(g_sMultiplayerTransitionTOD)
		RETURN GET_TIMEOFDAY_MINUTE(g_sMultiplayerTransitionTOD)
	ENDIF
	
	RETURN GET_CLOCK_MINUTES()
ENDFUNC

/// PURPOSE:
///    X:\gta5\build\dev\common\data\levels\gta5\weather.dat
/// PARAMS:
///    eReqScene - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Is_PedRequest_Blocked_For_Weather(PED_REQUEST_SCENE_ENUM eReqScene, BOOL bDescentOnlyScene)
	/*
	EXTRASUNNY CLEAR CLOUDS
	SMOG CLOUDY OVERCAST
	RAIN THUNDER CLEARING
	NEUTRAL SNOW MULTIPLAYER
	*/
	
//	GET_WEATHER_STRING_FROM_HASH()

	
	CONST_INT iEXTRASUNNY	HASH("EXTRASUNNY")
	CONST_INT iCLEAR		HASH("CLEAR")
	CONST_INT iCLOUDS		HASH("CLOUDS")
	CONST_INT iSMOG			HASH("SMOG")
	CONST_INT iCLOUDY		HASH("CLOUDY")
	CONST_INT iOVERCAST		HASH("OVERCAST")
	CONST_INT iRAIN			HASH("RAIN")
	CONST_INT iTHUNDER		HASH("THUNDER")
	CONST_INT iCLEARING		HASH("CLEARING")
	CONST_INT iNEUTRAL		HASH("NEUTRAL")
	CONST_INT iSNOW			HASH("SNOW")
	CONST_INT iMULTIPLAYER	HASH("MULTIPLAYER")
	
	INT iPREV_WEATHER_TYPE_HASH_NAME, iNEXT_WEATHER_TYPE_HASH_NAME
	INT iCLOCK_HOURS
	
	SWITCH eReqScene
		// Sunny scenes
		CASE PR_SCENE_M_POOLSIDE_a
		CASE PR_SCENE_M_POOLSIDE_b
		CASE PR_SCENE_M2_SAVEHOUSE1_b
		CASE PR_SCENE_M7_READSCRIPT
		CASE PR_SCENE_M7_LOUNGECHAIRS
		CASE PR_SCENE_F1_POOLSIDE_a
		CASE PR_SCENE_F1_POOLSIDE_b
		CASE PR_SCENE_T_CN_CHATEAU_c
		CASE PR_SCENE_T_ANNOYSUNBATHERS
		CASE PR_SCENE_T_CR_RUDEATCAFE
	
			iPREV_WEATHER_TYPE_HASH_NAME = GET_PREV_WEATHER_TYPE_HASH_NAME()
			iNEXT_WEATHER_TYPE_HASH_NAME = GET_NEXT_WEATHER_TYPE_HASH_NAME()
			
			BOOL bInvalidPrevWeather, bInvalidNextWeather
			bInvalidPrevWeather = FALSE
			bInvalidNextWeather = FALSE
			
			IF NOT bDescentOnlyScene
				IF (iPREV_WEATHER_TYPE_HASH_NAME = iRAIN)
				OR (iPREV_WEATHER_TYPE_HASH_NAME = iTHUNDER)
				OR (iPREV_WEATHER_TYPE_HASH_NAME = iSNOW)
					bInvalidPrevWeather = TRUE
				ENDIF
			ENDIF
			IF (iNEXT_WEATHER_TYPE_HASH_NAME = iRAIN)
			OR (iNEXT_WEATHER_TYPE_HASH_NAME = iTHUNDER)
			OR (iNEXT_WEATHER_TYPE_HASH_NAME = iSNOW)
				bInvalidPrevWeather = TRUE
			ENDIF
			
			IF bInvalidPrevWeather OR bInvalidNextWeather
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				TEXT_LABEL str1, str2
				
				IF NOT bDescentOnlyScene
					SWITCH iPREV_WEATHER_TYPE_HASH_NAME
						CASE iRAIN		str1 = ("iRAIN") BREAK
						CASE iTHUNDER	str1 = ("iTHUNDER") BREAK
						CASE iSNOW		str1 = ("iSNOW") BREAK
						DEFAULT
							str1  = ("\"")
							str1 += (iPREV_WEATHER_TYPE_HASH_NAME)
							str1 += ("\"")
						BREAK
					ENDSWITCH
				ELSE
					str1 += ("DescentOnly")
				ENDIF
				
				SWITCH iNEXT_WEATHER_TYPE_HASH_NAME
					CASE iRAIN		str2 += ("iRAIN") BREAK
					CASE iTHUNDER	str2 += ("iTHUNDER") BREAK
					CASE iSNOW		str2 += ("iSNOW") BREAK
					DEFAULT
						str2  = ("\"")
						str2 += (iNEXT_WEATHER_TYPE_HASH_NAME)
						str2 += ("\"")
					BREAK
				ENDSWITCH
				
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked for weather [prev:", str1, ", next:", str2, "]")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			iCLOCK_HOURS = PRIVATE_Get_Switch_Clock_Hours()
			IF (iCLOCK_HOURS < 9 OR iCLOCK_HOURS >= 14)	/*	09:00 - 13:59	*/
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				TEXT_LABEL str
				str  = iCLOCK_HOURS
				str += ":"
				IF (PRIVATE_Get_Switch_Clock_Minutes() < 10) str += "0" ENDIF
				str += PRIVATE_Get_Switch_Clock_Minutes()
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked for weather TOD [", str, "]")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC
		CASE PR_SCENE_F_KUSH_DOC_a
		CASE PR_SCENE_F_GYM
			iCLOCK_HOURS = PRIVATE_Get_Switch_Clock_Hours()
			IF (iCLOCK_HOURS < 9 OR iCLOCK_HOURS >= 21)	/*	09:00 - 20:59	*/
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				TEXT_LABEL str
				str  = iCLOCK_HOURS
				str += ":"
				IF (PRIVATE_Get_Switch_Clock_Minutes() < 10) str += "0" ENDIF
				str += PRIVATE_Get_Switch_Clock_Minutes()
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked for weather TOD [", str, "]")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
		BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF
			iCLOCK_HOURS = PRIVATE_Get_Switch_Clock_Hours()
			IF NOT IS_GOLF_CLUB_OPEN_AT_TIME_OF_DAY(iCLOCK_HOURS)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				TEXT_LABEL str
				str  = iCLOCK_HOURS
				str += ":"
				IF (PRIVATE_Get_Switch_Clock_Minutes() < 10) str += "0" ENDIF
				str += PRIVATE_Get_Switch_Clock_Minutes()
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked for weather TOD [", str, "]")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			IF g_eCurrentBuildingState[BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM] <> BUILDINGSTATE_DESTROYED
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked for building ex [", "FLOYDS_APPARTMENT_RASPBERRY_JAM", "]")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE PR_SCENE_M2_CARSLEEP_a
		CASE PR_SCENE_M2_CARSLEEP_b
		CASE PR_SCENE_M_PARKEDHILLS_a
		CASE PR_SCENE_M_PARKEDHILLS_b
		CASE PR_SCENE_M4_PARKEDBEACH
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT]
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked by available mission \"", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT), "\"")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			#endif
			#endif
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_PedRequest_Blocked_For_Sleeptime(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene, BOOL bDescentOnlyScene)
	
	IF bDescentOnlyScene
		RETURN FALSE
	ENDIF
	
	TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed]
	IF NOT Is_TIMEOFDAY_Valid(sLastTimeActive)
		RETURN FALSE
	ENDIF
	
	SWITCH eReqScene
		CASE PR_SCENE_M2_BEDROOM
		CASE PR_SCENE_M2_SAVEHOUSE0_b
		CASE PR_SCENE_M2_CARSLEEP_a
		CASE PR_SCENE_M2_CARSLEEP_b
		CASE PR_SCENE_M4_WAKESUPSCARED
		CASE PR_SCENE_M4_WAKEUPSCREAM
		CASE PR_SCENE_M6_HOUSEBED
		CASE PR_SCENE_M7_GETSREADY
		
		CASE PR_SCENE_F0_SH_ASLEEP
		CASE PR_SCENE_F1_SH_ASLEEP
		
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			
			IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, g_iCONST_GAME_HOURS_FOR_ASLEEP_SWITCH)
				#IF IS_DEBUG_BUILD
				IF g_bDebugPrint_SceneScheduleInfo
				INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
				GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
				CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked for sleeptime [", iSeconds, "s ", iMinutes, "m ", iHours, "h ", iDays, "d ", iMonths+(iYears*12), "m", "]")
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_PedRequest_Blocked_For_SafehouseTutorial(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
	
	IF (eReqScene = PR_SCENE_F1_NEWHOUSE)		//#1501744
	
		#IF IS_DEBUG_BUILD
		IF g_bDebugPrint_SceneScheduleInfo
		CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " ignores safehouse tutorial scene")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	VECTOR vCentreProperty = <<0,0,0>>
	FLOAT fPropertyDistChec = -1
	
	FLOW_BITSET_IDS eFlowBitsetToQuery 
	INT iOffsetBitIndex 
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			//	For Michael's Family Home -
		/*	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("SH_Intro_M_Home")) > 0
				vCentreProperty = << -808.79742, 169.31934, 70.95580 >>
				fPropertyDistChec = 85.0
			ENDIF	*/
						
			// Michael's house 
			//Is this bit in our first or second bitset? 
			IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) <= 31 
				eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1 
				iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) 
			ELSE 
				eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2 
				iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) - 31 
			ENDIF
			
			IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex) 
				// cutscene script is due to relaunch or is active
				vCentreProperty = << -808.79742, 169.31934, 70.95580 >>
				fPropertyDistChec = 85.0
			ENDIF 
		BREAK
		CASE CHAR_FRANKLIN
			//	For Franklin's hills safehouse -
		/*	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("SH_Intro_F_Hills")) > 0
				vCentreProperty = << -2.62564, 528.32562, 178.39198 >>
				fPropertyDistChec = 65.0
			ENDIF */
			
			// Franklin's appartment        
			//Is this bit in our first or second bitset? 
			IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) <= 31 
				eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1 
				iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) 
			ELSE 
				eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2 
				iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) - 31 
			ENDIF 

			IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex) 
				// cutscene script is due to relaunch or is active 
				vCentreProperty = << -2.62564, 528.32562, 178.39198 >>
				fPropertyDistChec = 65.0
			ENDIF
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF (fPropertyDistChec < 0)
		RETURN FALSE
	ENDIF
	
	VECTOR vRequestCoord
	FLOAT fHead
	TEXT_LABEL_31 tRoom
	IF GET_PLAYER_PED_POSITION_FOR_SCENE(eReqScene, vRequestCoord, fHead, tRoom)
		FLOAT fPropertyDist2 = VDIST2(vRequestCoord, vCentreProperty)
		IF (fPropertyDist2 < (fPropertyDistChec*fPropertyDistChec))
		
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked too close to a safehouse tutorial scene [", SQRT(fPropertyDist2), "m]")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_PedRequest_TooCloseToCurrentPlayer(PED_REQUEST_SCENE_ENUM eReqScene, BOOL bDescentOnlyScene,
		FLOAT fCONST_DIST_FROM_CURRENT_PLAYER = 400.0)	//750.0	//150.0)
	IF bDescentOnlyScene
		#IF IS_DEBUG_BUILD
		IF g_bDebugPrint_SceneScheduleInfo
		CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " ignores TooCloseToCurrentPlayer for descent only scenes")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	VECTOR vRequestCoord
	FLOAT fHead
	TEXT_LABEL_31 tRoom
	IF GET_PLAYER_PED_POSITION_FOR_SCENE(eReqScene, vRequestCoord, fHead, tRoom)
//		IF vRequestCoord.y < 400.0
//			IF vRequestCoord.x < 1400.0
//				IF vRequestCoord.x > -1900.0
//					IF vRequestCoord.y > -3500.0
//						RETURN TRUE
//					ENDIF 
//				ENDIF
//			ENDIF  
//		ENDIF
		
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		FLOAT fPlayerDist2 = VDIST2(vRequestCoord, vPlayerCoord)
		
		//1492523
		IF fPlayerDist2 < (fCONST_DIST_FROM_CURRENT_PLAYER*fCONST_DIST_FROM_CURRENT_PLAYER)
		
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked too close to the player [", SQRT(fPlayerDist2), "m < ", ROUND(fCONST_DIST_FROM_CURRENT_PLAYER), "m]")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IsPedRequestScenesAcceptable(PED_REQUEST_SCENE_ENUM eReqScene, enumCharacterList ePed, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene, BOOL bIgnoreBlipDistCheck = FALSE)
	IF (eReqScene = PR_SCENE_INVALID)
	OR (eReqScene = PR_SCENE_DEAD)
		RETURN FALSE
	ENDIF
	
	IF bDescentOnlyScene
		IF (eReqScene = PR_SCENE_F0_TANISHAFIGHT)		//#1522036
		OR (eReqScene = PR_SCENE_F1_NEWHOUSE)
		
		OR (eReqScene = PR_SCENE_T6_METHLAB)
		OR (eReqScene = PR_SCENE_T6_LAKE)
		OR (eReqScene = PR_SCENE_T_FLYING_PLANE)
		
		OR (eReqScene = PR_SCENE_M_TRAFFIC_a)
		OR (eReqScene = PR_SCENE_M_TRAFFIC_b)
		OR (eReqScene = PR_SCENE_M_TRAFFIC_c)
		
		OR (eReqScene = PR_SCENE_F_TRAFFIC_a)
		OR (eReqScene = PR_SCENE_F_TRAFFIC_b)
		OR (eReqScene = PR_SCENE_F_TRAFFIC_c)
		
		OR (eReqScene = PR_SCENE_F_MD_KUSH_DOC)
		
		OR (eReqScene = PR_SCENE_F_CS_CHECKSHOE)
		OR (eReqScene = PR_SCENE_F_CS_WIPEHANDS)
		OR (eReqScene = PR_SCENE_F_CS_WIPERIGHT)
		
		OR (eReqScene = PR_SCENE_T_CR_CHASECAR_a)
		OR (eReqScene = PR_SCENE_T_CN_CHASECAR_b)
		OR (eReqScene = PR_SCENE_T_CR_CHASEBIKE)
		OR (eReqScene = PR_SCENE_T_CR_CHASESCOOTER)
		
		OR (eReqScene = PR_SCENE_T_CR_POLICE_a)
		OR (eReqScene = PR_SCENE_T_CN_POLICE_b)
		OR (eReqScene = PR_SCENE_T_CN_POLICE_c)
		
		OR (eReqScene = PR_SCENE_F_LAMTAUNT_P1)			//#1820715
		OR (eReqScene = PR_SCENE_F_LAMTAUNT_P3)
		OR (eReqScene = PR_SCENE_F_LAMTAUNT_P5)
		OR (eReqScene = PR_SCENE_F_LAMTAUNT_NIGHT)
		
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked bDescentOnlyScene [", "descent only", "]")
			ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
		
		TEXT_LABEL_31 RoomName = ""
		FLOAT fTurnOffTVPhase = 0.0
		TV_LOCATION eRoomTVLocation
		TVCHANNELTYPE eTVChannelType
		TV_CHANNEL_PLAYLIST eTVPlaylist

		IF CONTROL_PLAYER_WATCHING_TV(eReqScene, RoomName, fTurnOffTVPhase, eRoomTVLocation, eTVChannelType, eTVPlaylist)
		
			#IF IS_DEBUG_BUILD
			IF g_bDebugPrint_SceneScheduleInfo
			CPRINTLN(DEBUG_SWITCH, "eReqScene ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " blocked bDescentOnlyScene [", "descent only/tv script!", "]")
			ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	
	IF PRIVATE_Has_One_Off_PedRequest_Been_Seen(eReqScene)
		RETURN FALSE
	ENDIF
	
	IF IsPedRequestSceneDupeOfLastKnownScene(ePed, eReqScene)
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Is_PedRequest_In_LastSceneQueue(ePed, eReqScene)
		RETURN FALSE
	ENDIF
	
	IF IsPedRequestScenesBlockedForFriend(eReqScene)
		RETURN FALSE
	ENDIF
	
	
	IF IsPedRequestScenesBlockedForBuddyInAvailableMission(eReqScene, sPedScene, sPassedScene)
		RETURN FALSE
	ENDIF
	
	IF NOT bIgnoreBlipDistCheck		//pre-mission switches
		IF IsPedRequestScenesBlockedForAvailableMission(eReqScene)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IsPedRequestScenesBlockedForLastPassedMission(eReqScene)
		RETURN FALSE
	ENDIF
	
	
	IF PRIVATE_Is_PedRequest_In_Queued_VectorID(eReqScene)
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Does_PedRequest_Have_Queued_Char(eReqScene, sPedScene, sPassedScene, bDescentOnlyScene)
		RETURN FALSE
	ENDIF
	
	
	IF PRIVATE_Is_PedRequest_Blocked_To_City(ePed, eReqScene)
		RETURN FALSE
	ENDIF
	
	IF NOT bIgnoreBlipDistCheck		//pre-mission switches
		IF PRIVATE_Is_PedRequest_In_Active_Blip(eReqScene)
			RETURN FALSE
		ENDIF
	ENDIF
	
	
	IF PRIVATE_Is_PedRequest_Blocked_For_Weather(eReqScene, bDescentOnlyScene)
		RETURN FALSE
	ENDIF
	
	
	IF PRIVATE_Is_PedRequest_Blocked_For_Sleeptime(ePed, eReqScene, bDescentOnlyScene)
		RETURN FALSE
	ENDIF
	
	
	IF PRIVATE_Is_PedRequest_Blocked_For_SafehouseTutorial(ePed, eReqScene)
		RETURN FALSE
	ENDIF
	
	IF NOT bIgnoreBlipDistCheck
		IF PRIVATE_Is_PedRequest_TooCloseToCurrentPlayer(eReqScene, bDescentOnlyScene)
			RETURN FALSE
		ENDIF
	ELSE
		IF PRIVATE_Is_PedRequest_TooCloseToCurrentPlayer(eReqScene, bDescentOnlyScene, 15)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC FLOAT GetDistanceFromTimeOfDayDifference(TIMEOFDAY sDiffTimeOfDay)
	CONST_FLOAT fMIN_DISTANCE	 1000.0
	CONST_FLOAT fMAX_DISTANCE	10000.0
	
	FLOAT fMIN_HOURS			=	1.0 * 60.0 * 60.0
	FLOAT fMAX_HOURS			=  24.0 * 60.0 * 60.0
	
	IF (GET_TIMEOFDAY_YEAR(sDiffTimeOfDay)	> 0)
	OR (GET_TIMEOFDAY_MONTH(sDiffTimeOfDay)	> INT_TO_ENUM(MONTH_OF_YEAR,0))
	OR (GET_TIMEOFDAY_DAY(sDiffTimeOfDay)	> 0)
 /*	OR (sDifferenceTimeActive.iDayOfWeek			> 0) */		//	never needed (mon-sun?)
	
 /*	OR (sDifferenceTimeActive.iHours				> 0)		//		only need to check if year, month or 
	OR (sDifferenceTimeActive.iMinutes				> 0)		//	DOM is greater than zero to see if more 
	OR (sDifferenceTimeActive.iSeconds				> 0) */		//	than a day has passed
		RETURN fMAX_DISTANCE
	ENDIF
	
	IF (GET_TIMEOFDAY_HOUR(sDiffTimeOfDay)	<= 0)
		RETURN 0.0
	ENDIF
	
	INT iDiffTimeInSeconds = (((GET_TIMEOFDAY_HOUR(sDiffTimeOfDay) * 60) + GET_TIMEOFDAY_MINUTE(sDiffTimeOfDay)) * 60) + GET_TIMEOFDAY_SECOND(sDiffTimeOfDay)
	
//	CPRINTLN(DEBUG_SWITCH, "   iMAX_TimeInSeconds:  ")
//	CPRINTLN(DEBUG_SWITCH, iMAX_TimeInSeconds)
//	CPRINTLN(DEBUG_SWITCH, ", iDiffTimeInSeconds:  ")
//	CPRINTLN(DEBUG_SWITCH, iDiffTimeInSeconds)
	
	//
	FLOAT m = (fMAX_distance - fMIN_distance) / (fMAX_hours - fMIN_hours)
	FLOAT c = fMIN_distance - (m*fMIN_hours)
	
	FLOAT d = (m*iDiffTimeInSeconds) + c
	
	IF d < fMIN_DISTANCE
		d = fMIN_DISTANCE
	ENDIF
	IF d > fMAX_DISTANCE
		d = fMAX_DISTANCE
	ENDIF
	
	RETURN d
ENDFUNC

FUNC BOOL GetDesiredPedRequestSceneFromArray(enumCharacterList ePed,
		PED_REQUEST_SCENE_ENUM &eRequestArray[], FLOAT &fPercentArray[],
		PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent,
		PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene, BOOL bIgnoreBlipDistCheck = FALSE)
	
	//get a percentage...
	FLOAT fMaxPercent = 0
	
	INT iLoop
	REPEAT COUNT_OF(fPercentArray) iLoop
		IF IsPedRequestScenesAcceptable(eRequestArray[iLoop], ePed, sPedScene, sPassedScene, bDescentOnlyScene, bIgnoreBlipDistCheck)
			fMaxPercent += fPercentArray[iLoop]
		ELSE
			eRequestArray[iLoop] = PR_SCENE_INVALID
			fPercentArray[iLoop] = 0.0
		ENDIF
	ENDREPEAT
	
	
	
	// // // // //
//	#IF IS_DEBUG_BUILD
	IF NOT bDescentOnlyScene
	AND NOT bIgnoreBlipDistCheck
		IF (g_iAvailablePlayerMissions[ePed] = 1)
			
			SP_MISSIONS eMission = g_eFirstAvailablePlayerMission[ePed]
			STATIC_BLIP_NAME_ENUM eBlip = g_sMissionStaticData[eMission].blip
			
	//		IF (eMission = SP_HEIST_FINALE_2B)
	//			SAVE_STRING_TO_DEBUG_FILE("g_iAvailablePlayerMissions[")
	//			SAVE_STRING_TO_DEBUG_FILE(GET_PLAYER_PED_STRING(ePed))
	//			SAVE_STRING_TO_DEBUG_FILE("]: ")
	//			SAVE_INT_TO_DEBUG_FILE(g_iAvailablePlayerMissions[ePed])
	//			SAVE_STRING_TO_DEBUG_FILE(" mission: ")
	//			SAVE_STRING_TO_DEBUG_FILE(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
	//			SAVE_NEWLINE_TO_DEBUG_FILE()
	//		ENDIF
			
			VECTOR vBlipCoords
			IF NOT IS_STATIC_BLIP_MULTIMODE(eBlip)
				vBlipCoords = GET_STATIC_BLIP_POSITION(eBlip)
			ELSE
				vBlipCoords = GET_STATIC_BLIP_POSITION(eBlip, ENUM_TO_INT(ePed))
			ENDIF
			
			VECTOR vCreateCoords = <<0,0,0>>
			FLOAT fCreateHead = 0
			TEXT_LABEL_31 tRoom = ""
			REPEAT COUNT_OF(fPercentArray) iLoop
				IF (eRequestArray[iLoop] <> PR_SCENE_INVALID)
					IF GET_PLAYER_PED_POSITION_FOR_SCENE(eRequestArray[iLoop],
							vCreateCoords, fCreateHead, tRoom)
							
						FLOAT fSwitchToMissionDist2 = VDIST2(vBlipCoords, vCreateCoords)
						
	//					IF (eMission = SP_HEIST_FINALE_2B)
	//						#IF IS_DEBUG_BUILD
	//						SAVE_STRING_TO_DEBUG_FILE("	switch ")
	//						SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(eRequestArray[iLoop]))
	//						SAVE_STRING_TO_DEBUG_FILE(" dist: ")
	//						SAVE_FLOAT_TO_DEBUG_FILE(SQRT(fSwitchToMissionDist2))
	//						SAVE_NEWLINE_TO_DEBUG_FILE()
	//						#ENDIF
	//					ENDIF
						
						IF (fSwitchToMissionDist2 < (150*150))
							eReqScene = eRequestArray[iLoop]
							fScenePercent = 100	//fPercentArray[iLoop]
							
							CPRINTLN(DEBUG_SWITCH, "will this ever fire - check url:bugstar:944315")
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			//
		ENDIF
	ENDIF
//	#ENDIF
	// // // // //
	
	
	
	FLOAT fPercent = GET_RANDOM_FLOAT_IN_RANGE(0, fMaxPercent)
	REPEAT COUNT_OF(fPercentArray) iLoop
		
		IF (eRequestArray[iLoop] <> PR_SCENE_INVALID)
			IF fPercent < fPercentArray[iLoop]
				eReqScene = eRequestArray[iLoop]
				fScenePercent = fPercentArray[iLoop]
				RETURN TRUE
			ENDIF
			
			fPercent -= fPercentArray[iLoop]
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "invalid fPercent in GetDesiredPedRequestSceneFromArray(", GET_PLAYER_PED_STRING(ePed), ",", PRIVATE_Get_Switch_Clock_Hours(), ":00)")
	
	g_bDebugPrint_SceneScheduleInfo = TRUE
	#ENDIF
	
	eReqScene = PR_SCENE_INVALID
	fScenePercent = -1
	RETURN FALSE
ENDFUNC


//ENUM TIME_SAMPLE
//	TS_NIGHT = 0,
//	TS_MORNING,
//	TS_AFTERNOON,
//	TS_EVENING
//ENDENUM
FUNC TIME_SAMPLE PRIVATE_GetPlayerTriggerPrioritySwitch(enumCharacterList ePed)
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			SWITCH PRIVATE_Get_Switch_Clock_Hours()
				/*	00:00 - 08:59	*/
		        CASE 0 CASE 1 CASE 2
		        CASE 3 CASE 4 CASE 5
				CASE 6 CASE 7 CASE 8
					RETURN TS_MORNING
				BREAK
				
				/*	09:00 - 13:59	*/
		        CASE 9 CASE 10 CASE 11
		        CASE 12 CASE 13
					RETURN TS_AFTERNOON
				BREAK
				
				/*	14:00 - 19:59	*/
		        CASE 14 CASE 15 CASE 16
				CASE 17 CASE 18 CASE 19
					RETURN TS_EVENING
				BREAK
				
				/*	20:00 - 23:59	*/
		        CASE 20 CASE 21 CASE 22
				CASE 23
					RETURN TS_NIGHT
				BREAK
		    ENDSWITCH
		BREAK
		CASE CHAR_FRANKLIN
			SWITCH PRIVATE_Get_Switch_Clock_Hours()
				/*	03:00 - 08:59	*/
				CASE 3 CASE 4 CASE 5
				CASE 6 CASE 7 CASE 8
					RETURN TS_NIGHT
				BREAK
				
				/*	09:00 - 14:59	*/
				CASE 9 CASE 10 CASE 11
				CASE 12 CASE 13 CASE 14
					RETURN TS_MORNING
				BREAK
				
				/*	15:00 - 20:59	*/
				CASE 15 CASE 16 CASE 17
				CASE 18 CASE 19 CASE 20
					RETURN TS_AFTERNOON
				BREAK
				
				/*	21:00 - 02:59	*/
				CASE 21 CASE 22 CASE 23
				CASE 0 CASE 1 CASE 2
					RETURN TS_EVENING
				BREAK
			ENDSWITCH
		BREAK
		CASE CHAR_TREVOR
			SWITCH PRIVATE_Get_Switch_Clock_Hours()
				/*	00:00 - 5:59	*/
		        CASE 0 CASE 1 CASE 2
		        CASE 3 CASE 4 CASE 5
					RETURN TS_MORNING
				BREAK
				
				/*	06:00 - 14:59	*/
		        CASE 6 CASE 7 CASE 8
		        CASE 9 CASE 10 CASE 11
		        CASE 12 CASE 13 CASE 14
					RETURN TS_AFTERNOON
				BREAK
				
				/*	15:00 - 23:59	*/
		        CASE 15 CASE 16 CASE 17
		        CASE 18 CASE 19 CASE 20
		        CASE 21 CASE 22 CASE 23
					RETURN TS_EVENING
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN INT_TO_ENUM(TIME_SAMPLE, -1)
ENDFUNC

FUNC BOOL PRIVATE_ShouldPlayerTriggerPrioritySwitch(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, 
		PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		TIME_SAMPLE eSample, BOOL bDescentOnlyScene,
		PED_REQUEST_SCENE_ENUM eTestSceneOne = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneTwo = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneThree = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneFour = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneFive = PR_SCENE_INVALID,
		PED_REQUEST_SCENE_ENUM eTestSceneSix = PR_SCENE_INVALID)
	
	// for url:bugstar:944315 and url:bugstar:965982, ignore priority
	IF (g_iAvailablePlayerMissions[ePed] = 1)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF g_SavedGlobals.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[ePed][eSample]
		
		INT iSceneIndex
		PED_REQUEST_SCENE_ENUM	eRequestArray[6]
		FLOAT 					fPercentArray[6]
		
		IF (eTestSceneOne <> PR_SCENE_INVALID)
			eRequestArray[iSceneIndex] = eTestSceneOne
			fPercentArray[iSceneIndex] = 1.0
			iSceneIndex++
		ENDIF
		IF (eTestSceneTwo <> PR_SCENE_INVALID)
			eRequestArray[iSceneIndex] = eTestSceneTwo
			fPercentArray[iSceneIndex] = 1.0
			iSceneIndex++
		ENDIF
		IF (eTestSceneThree <> PR_SCENE_INVALID)
			eRequestArray[iSceneIndex] = eTestSceneThree
			fPercentArray[iSceneIndex] = 1.0
			iSceneIndex++
		ENDIF
		IF (eTestSceneFour <> PR_SCENE_INVALID)
			eRequestArray[iSceneIndex] = eTestSceneFour
			fPercentArray[iSceneIndex] = 1.0
			iSceneIndex++
		ENDIF
		IF (eTestSceneFive <> PR_SCENE_INVALID)
			eRequestArray[iSceneIndex] = eTestSceneFive
			fPercentArray[iSceneIndex] = 1.0
			iSceneIndex++
		ENDIF
		IF (eTestSceneSix <> PR_SCENE_INVALID)
			eRequestArray[iSceneIndex] = eTestSceneSix
			fPercentArray[iSceneIndex] = 1.0
			iSceneIndex++
		ENDIF
		
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		g_SavedGlobals.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[ePed][eSample] = FALSE
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "reset g_bPlayerTriggeredPrioritySwitch[", GET_PLAYER_PED_STRING(ePed), "][", ENUM_TO_INT(eSample), "]")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_ClearPrioritySwitchFlag(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene)
	IF (eReqScene = PR_SCENE_M_DEFAULT)
	OR (eReqScene = PR_SCENE_F_DEFAULT)
	OR (eReqScene = PR_SCENE_T_DEFAULT)
	
	OR (eReqScene = PR_SCENE_HOSPITAL)
	
	OR (eReqScene = PR_SCENE_M_OVERRIDE)
	OR (eReqScene = PR_SCENE_F_OVERRIDE)
	OR (eReqScene = PR_SCENE_T_OVERRIDE)
		RETURN FALSE
	ENDIF
	
	VECTOR vLastKnownCoords
	FLOAT fLastKnownHead
	IF GetLastKnownPedInfoPostMission(eReqScene, vLastKnownCoords, fLastKnownHead)
		RETURN FALSE
	ENDIF
	
	TIME_SAMPLE eSample = PRIVATE_GetPlayerTriggerPrioritySwitch(ePed)
	g_SavedGlobals.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[ePed][eSample] = FALSE
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "clear g_bPlayerTriggeredPrioritySwitch[", GET_PLAYER_PED_STRING(ePed), "][sample_", ENUM_TO_INT(eSample), "]")
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_TriggerThisPrioritySwitch(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eReqScene, TIME_SAMPLE eSample)
	IF (ePed <> NO_CHARACTER)
	ENDIF
	IF (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF (eSample <> TS_NIGHT)
	ENDIF
	
	RETURN TRUE
ENDFUNC


TYPEDEF FUNC BOOL SceneScheduleFunc(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
FUNC BOOL BlankSceneScheduleFunc(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	eReqScene = PR_SCENE_INVALID
	fScenePercent = -1
	sPedScene.eScene = sPedScene.eScene
	sPassedScene.eSceneBuddy = sPassedScene.eSceneBuddy
	bDescentOnlyScene = bDescentOnlyScene
	
	RETURN (eReqScene <> PR_SCENE_INVALID)
ENDFUNC



/// PURPOSE: Cycles through the list of weapons and checks to see if the player has any ammo or mods
FUNC BOOL DoesPlayerPedHaveWeaponStored(enumCharacterList ePed, WEAPON_TYPE eWeaponType)
	WEAPON_SLOT eWeaponSlot = GET_WEAPONTYPE_SLOT(eWeaponType)
	
	// Grab the weapon type and reset ammo/mod counts
    IF g_savedGlobals.sPlayerData.sInfo.sWeapons[ePed].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eWeaponSlot)].eWeaponType = eWeaponType
		IF (g_savedGlobals.sPlayerData.sInfo.sWeapons[ePed].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eWeaponSlot)].iAmmoCount > 0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	FUNCTIONS FOR MISSION SPECIFIC SWITCH SCHEDULING
// *******************************************************************************************


FUNC BOOL GetPedRequestSceneForAvailableMission(enumCharacterList ePed,
		PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent,
		PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
	
	PED_REQUEST_SCENE_ENUM	eRequestArray[3]
	FLOAT 					fPercentArray[3]
	SP_MISSIONS				ePostMissionSwitch = SP_MISSION_NONE
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_3)
	//	Michael should have a switch near his safehouse
		IF (ePed = CHAR_MICHAEL)
			eRequestArray[0] = PR_SCENE_M2_DRIVING_a		fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_M2_CARSLEEP_b		fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_M_COFFEE_b			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_MISSION_FAMILY_3
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
	//	Franklin near safehouse Block Franklin switch scenes in countryside or Drunk etc
		IF (ePed = CHAR_FRANKLIN)
			eRequestArray[0] = PR_SCENE_F_HIT_CUP_HAND		fPercentArray[0] = 40.0
			eRequestArray[1] = PR_SCENE_F_THROW_CUP			fPercentArray[1] = 40.0
			eRequestArray[2] = PR_SCENE_F_TRAFFIC_b			fPercentArray[2] = 20.0
			ePostMissionSwitch = SP_MISSION_FRANKLIN_1
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_MARTIN_1)
	//	Michael should be on one of his hills switches
		IF (ePed = CHAR_MICHAEL)
			eRequestArray[0] = PR_SCENE_M_PARKEDHILLS_a		fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_M_PARKEDHILLS_b		fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_M_COFFEE_a			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_MISSION_MARTIN_1
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_MICHAEL_1)
	//	Michael should have a switch near his safehouse
		IF (ePed = CHAR_MICHAEL)
			eRequestArray[0] = PR_SCENE_M4_CINEMA			fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_M4_EXITRESTAURANT	fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_M_COFFEE_a			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_MISSION_MICHAEL_1
		ENDIF
		
	// #1539580 - Trevor should be near Michaels 
		IF (ePed = CHAR_TREVOR)
			eRequestArray[0] = PR_SCENE_T_DOCKS_b			fPercentArray[0] = 50.0
			eRequestArray[1] = PR_SCENE_T_FIGHTYAUCLUB_b	fPercentArray[1] = 25.0
			eRequestArray[2] = PR_SCENE_T_ESCORTED_OUT		fPercentArray[2] = 25.0
			ePostMissionSwitch = SP_MISSION_MICHAEL_1
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_MICHAEL_2)
	//	Franklin should have a switch scene near (but not too near!) city safehouse
		IF (ePed = CHAR_FRANKLIN)
			eRequestArray[0] = PR_SCENE_F_LAMTAUNT_P1		fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_F_TRAFFIC_b			fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_F_BAR_a_01			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_MISSION_MICHAEL_2
		ENDIF
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_HEIST_FINALE_1)
	//	No countryside or hills for Michael – city switches only.
		IF (ePed = CHAR_MICHAEL)
			eRequestArray[0] = PR_SCENE_M4_CINEMA			fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_M4_EXITRESTAURANT	fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_M_PIER_b			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_FINALE_1
		ENDIF
		
	//	No countryside for Trevor or too random
		IF (ePed = CHAR_TREVOR)
			eRequestArray[0] = PR_SCENE_T_SC_DRUNKHOWLING	fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_T_SC_BAR			fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_T_SC_ALLEYDRUNK		fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_FINALE_1
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_HEIST_FINALE_2_INTRO)
	//	No countryside or hills for Michael – city switches only.
		IF (ePed = CHAR_MICHAEL)
			eRequestArray[0] = PR_SCENE_M4_CINEMA			fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_M4_EXITRESTAURANT	fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_M_PIER_b			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_FINALE_2_INTRO
		ENDIF
		
	//	No countryside for Trevor or too random
		IF (ePed = CHAR_TREVOR)
			eRequestArray[0] = PR_SCENE_T_SC_DRUNKHOWLING	fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_T_SC_BAR			fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_T_SC_ALLEYDRUNK		fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_FINALE_2_INTRO
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_HEIST_RURAL_1)
	//	Trevor on a switch that is nearby to Meth lab planning location
		IF (ePed = CHAR_TREVOR)
			eRequestArray[0] = PR_SCENE_T_CN_CHATEAU_b		fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_T6_DIGGING			fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_T_CN_POLICE_c		fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_RURAL_1
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2B)
	//	Michael on a switch in venice area, near marina, pier?
		IF (ePed = CHAR_MICHAEL)
			eRequestArray[0] = PR_SCENE_M_CANAL_a			fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_M2_MARINA			fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_INVALID				fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_DOCKS_2B
		ENDIF
		
	//	Trevor on a switch nearby to Floyds apartment
		IF (ePed = CHAR_TREVOR)
			eRequestArray[0] = PR_SCENE_T_FIGHTBBUILD		fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_T_FLOYDSAVEHOUSE	fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_T_FLOYDPINEAPPLE	fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_HEIST_DOCKS_2B
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_CARSTEAL_4)			//#1535040
	//	Trevor on a switch nearby to Franklins safehouse (the stripclub)
		IF (ePed = CHAR_TREVOR)
			eRequestArray[0] = PR_SCENE_T_SC_MOCKLAPDANCE	fPercentArray[0] = 33.3
			eRequestArray[1] = PR_SCENE_T_SC_CHASE			fPercentArray[1] = 33.3
			eRequestArray[2] = PR_SCENE_T_SC_BAR			fPercentArray[2] = 33.3
			ePostMissionSwitch = SP_MISSION_CARSTEAL_4
		ENDIF
	ENDIF
	
	IF (ePostMissionSwitch <> SP_MISSION_NONE)
		IF GetDesiredPedRequestSceneFromArray(ePed, eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, FALSE, TRUE)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "GetPedRequestSceneForAvailableMission(", GET_PLAYER_PED_STRING(ePed), ", ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(ePostMissionSwitch), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), " dist: ", VDIST(GET_STATIC_BLIP_POSITION(g_sMissionStaticData[ePostMissionSwitch].blip),g_sPedSceneData[eReqScene].vCreateCoords), "m")
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "GetPedRequestSceneForAvailableMission(", GET_PLAYER_PED_STRING(ePed), ", ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(ePostMissionSwitch), ") FAILED AT ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
//		CASSERTLN(DEBUG_SWITCH, "GetPedRequestSceneForAvailableMission failed!!!")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SchedulePostMissionSpecificScene(enumCharacterList ePed,
		PED_REQUEST_SCENE_ENUM eCurrentScene, PED_REQUEST_SCENE_ENUM &eReqScene,
		FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene,
		PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		FLOAT fPostMissSwitchTimeMult = 1.0)
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eCurrentScene)
		
		#IF IS_DEBUG_BUILD
		STRING sFile = "player_switch_scene.log"
		STRING sPath = "X:/gta5/build/dev/"
		#ENDIF
		
		IF NOT HasNumOfHoursPassedSincePedTimeStruct(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed], ROUND(TO_FLOAT(g_iCONST_GAME_HOURS_FOR_POST_MISSION_SWITCH)*fPostMissSwitchTimeMult))
			
			#IF IS_DEBUG_BUILD
			SAVE_STRING_TO_NAMED_DEBUG_FILE("NOT HasHourPassedSincePedTimeStruct(", sPath, sFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(eCurrentScene), sPath, sFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sPath, sFile)
			SAVE_TIMEOFDAY_TO_NAMED_DEBUG_FILE(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed], sPath, sFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(")", sPath, sFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
			#ENDIF
			
			fScenePercent = 100
			eReqScene = eCurrentScene
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		SAVE_STRING_TO_NAMED_DEBUG_FILE("HasHourPassedSincePedTimeStruct(", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(eCurrentScene), sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(")", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
		#ENDIF
		
		CLEAR_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
		g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
	eReqScene = PR_SCENE_INVALID
	fScenePercent = -1
	sPedScene.eScene = sPedScene.eScene
	sPassedScene.eSceneBuddy = sPassedScene.eSceneBuddy
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	FUNCTIONS TO RETURN NEXT SCENE FOR MICHAEL
// *******************************************************************************************

ENUM enumMichaelScheduleStage
	MSS_M2_WithFamily = 0,				//Michael with Family [1,2,3,4]
	MSS_M4_WithoutFamily,				//Michael without Family [4,5,7]
	MSS_M6_Exiled,						//Michael Exiled
	MSS_M7_ReunitedWithFamily,			//Michael Re-united with family
	
	MSS_M9_killedMichael				//
	
ENDENUM
FUNC enumMichaelScheduleStage GetMichaelScheduleStage()
	
	IF (g_eCurrentBuildingState[BUILDINGNAME_IPL_KILLED_MICHAEL] = BUILDINGSTATE_DESTROYED)
		RETURN MSS_M9_killedMichael
	ENDIF
	
	IF NOT (GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5))
		RETURN MSS_M2_WithFamily
	ELIF NOT (Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED))
		RETURN MSS_M4_WithoutFamily
	ELIF NOT (Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED))
		RETURN MSS_M6_Exiled
	ELIF NOT (GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6))
		RETURN MSS_M4_WithoutFamily
		
		// #1476433	//
	ELIF NOT (GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4))
		RETURN MSS_M7_ReunitedWithFamily
	ELIF NOT (GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_CREDITS))
		RETURN MSS_M4_WithoutFamily
		//  //  //  //
		
	ELSE
		RETURN MSS_M7_ReunitedWithFamily
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    /*	00:00 - 08:59	*/
/// PARAMS:
///    eReqScene - 
///    ePed - 
/// RETURNS:
///    
FUNC BOOL GetMichaelSceneForMorning(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_MICHAEL
	eReqScene = PR_SCENE_INVALID		fScenePercent = -1
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_M2_BEDROOM							25.0
		CONST_FLOAT fCHANCE_M2_SAVEHOUSE0_b						25.0
		CONST_FLOAT fCHANCE_M2_CARSLEEP_a						7.0
		CONST_FLOAT fCHANCE_M2_CARSLEEP_b						3.0
		CONST_FLOAT fCHANCE_M4_WATCHINGTV						15.0
		CONST_FLOAT fCHANCE_M2_SAVEHOUSE1_a						12.0
		CONST_FLOAT fCHANCE_M2_DRIVING_a						2.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_a						2.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_b						1.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[10]
		FLOAT 					fPercentArray[10]
		
		eRequestArray[0] = PR_SCENE_M2_BEDROOM					fPercentArray[0] = fCHANCE_M2_BEDROOM
		eRequestArray[1] = PR_SCENE_M2_SAVEHOUSE0_b				fPercentArray[1] = fCHANCE_M2_SAVEHOUSE0_b
		eRequestArray[2] = PR_SCENE_M2_CARSLEEP_a				fPercentArray[2] = fCHANCE_M2_CARSLEEP_a
		eRequestArray[3] = PR_SCENE_M2_CARSLEEP_b				fPercentArray[3] = fCHANCE_M2_CARSLEEP_b
		eRequestArray[4] = PR_SCENE_M4_WATCHINGTV				fPercentArray[4] = fCHANCE_M4_WATCHINGTV
		eRequestArray[5] = PR_SCENE_M2_SAVEHOUSE1_a				fPercentArray[5] = fCHANCE_M2_SAVEHOUSE1_a
		eRequestArray[6] = PR_SCENE_M2_DRIVING_a				fPercentArray[6] = fCHANCE_M2_DRIVING_a / 2.0
		eRequestArray[7] = PR_SCENE_M2_DRIVING_b				fPercentArray[7] = fCHANCE_M2_DRIVING_a / 2.0
		
		
		IF PRIVATE_Get_Switch_Clock_Hours() > 4		//#1561291
			eRequestArray[8] = PR_SCENE_M_VWOODPARK_a			fPercentArray[8] = fCHANCE_M_VWOODPARK_a
			eRequestArray[9] = PR_SCENE_M_VWOODPARK_b			fPercentArray[9] = fCHANCE_M_VWOODPARK_b
		ELSE
			eRequestArray[8] = PR_SCENE_INVALID					fPercentArray[8] = 0.0
			eRequestArray[9] = PR_SCENE_INVALID					fPercentArray[9] = 0.0
		ENDIF
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING, bDescentOnlyScene)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		CONST_FLOAT fCHANCE_M4_WAKEUPSCREAM						30.0
		CONST_FLOAT fCHANCE_M4_WAKESUPSCARED					30.0
		CONST_FLOAT fCHANCE_M4_WATCHINGTV						30.0
		CONST_FLOAT fCHANCE_M_CANAL_a							3.0
		CONST_FLOAT fCHANCE_M_CANAL_b							3.0
		CONST_FLOAT fCHANCE_M_CANAL_c							3.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_a						3.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_b						3.0
		CONST_FLOAT fCHANCE_M_BAR_a								10.0
		CONST_FLOAT fCHANCE_M_BAR_b								10.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[10-2+1]
		FLOAT 					fPercentArray[10-2+1]
		
		eRequestArray[0] = PR_SCENE_M4_WAKEUPSCREAM				fPercentArray[0] = fCHANCE_M4_WAKEUPSCREAM
		eRequestArray[1] = PR_SCENE_M4_WAKESUPSCARED			fPercentArray[1] = fCHANCE_M4_WAKESUPSCARED
		eRequestArray[2] = PR_SCENE_M4_WATCHINGTV				fPercentArray[2] = fCHANCE_M4_WATCHINGTV
		eRequestArray[3] = PR_SCENE_M_CANAL_a					fPercentArray[3] = fCHANCE_M_CANAL_a
		eRequestArray[4] = PR_SCENE_M_CANAL_b					fPercentArray[4] = fCHANCE_M_CANAL_b
		eRequestArray[5] = PR_SCENE_M_CANAL_c					fPercentArray[5] = fCHANCE_M_CANAL_c
		eRequestArray[6] = PR_SCENE_M_PARKEDHILLS_a				fPercentArray[6] = fCHANCE_M_PARKEDHILLS_a
		eRequestArray[7] = PR_SCENE_M_PARKEDHILLS_b				fPercentArray[7] = fCHANCE_M_PARKEDHILLS_b
//		eRequestArray[8] = PR_SCENE_M_BAR_a						fPercentArray[8] = fCHANCE_M_BAR_a
//		eRequestArray[9] = PR_SCENE_M_BAR_b						fPercentArray[9] = fCHANCE_M_BAR_b
		eRequestArray[7+1] = PR_SCENE_M4_WASHFACE				fPercentArray[7+1] = fCHANCE_M_PARKEDHILLS_b
		
		
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M6_Exiled)
		CONST_FLOAT fCHANCE_M6_MORNING_a						20.0
		CONST_FLOAT fCHANCE_M6_MORNING_b						20.0
		CONST_FLOAT fCHANCE_M6_CARSLEEP							10.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_a					2.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_b					2.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_c					2.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_d					2.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_e					2.0
		CONST_FLOAT fCHANCE_M6_HOUSETV_a						10.0
		CONST_FLOAT fCHANCE_M6_HOUSETV_b						10.0
		CONST_FLOAT fCHANCE_M6_DRIVING_c						2.5
		CONST_FLOAT fCHANCE_M6_DRIVING_d						2.5
		CONST_FLOAT fCHANCE_M6_RONBORING						10.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[13-2]
		FLOAT 					fPercentArray[13-2]
		
		eRequestArray[0] = PR_SCENE_M6_MORNING_a				fPercentArray[0] = fCHANCE_M6_MORNING_a					
//		eRequestArray[1] = PR_SCENE_M6_MORNING_b				fPercentArray[1] = fCHANCE_M6_MORNING_b					
		eRequestArray[2-1] = PR_SCENE_M6_CARSLEEP				fPercentArray[2-1] = fCHANCE_M6_CARSLEEP					
		eRequestArray[3-1] = PR_SCENE_M6_PARKEDHILLS_a			fPercentArray[3-1] = fCHANCE_M6_PARKEDHILLS_a
		eRequestArray[4-1] = PR_SCENE_M6_PARKEDHILLS_b			fPercentArray[4-1] = fCHANCE_M6_PARKEDHILLS_b
		eRequestArray[5-1] = PR_SCENE_M6_PARKEDHILLS_c			fPercentArray[5-1] = fCHANCE_M6_PARKEDHILLS_c
		eRequestArray[6-1] = PR_SCENE_M6_PARKEDHILLS_d			fPercentArray[6-1] = fCHANCE_M6_PARKEDHILLS_d
		eRequestArray[7-1] = PR_SCENE_M6_PARKEDHILLS_e			fPercentArray[7-1] = fCHANCE_M6_PARKEDHILLS_e
		eRequestArray[8-1] = PR_SCENE_M6_HOUSETV_a				fPercentArray[8-1] = fCHANCE_M6_HOUSETV_a					
//		eRequestArray[9-1] = PR_SCENE_M6_HOUSETV_b				fPercentArray[9-1] = fCHANCE_M6_HOUSETV_b
		eRequestArray[10-1-1] = PR_SCENE_M6_DRIVING_c			fPercentArray[10-1-1] = fCHANCE_M6_DRIVING_c
		eRequestArray[11-1-1] = PR_SCENE_M6_DRIVING_d			fPercentArray[11-1-1] = fCHANCE_M6_DRIVING_d
		eRequestArray[12-1-1] = PR_SCENE_M6_RONBORING			fPercentArray[12-1-1] = fCHANCE_M6_RONBORING
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		CONST_FLOAT fCHANCE_M4_WATCHINGTV						15.0
		CONST_FLOAT fCHANCE_M7_GETSREADY						25.0
		CONST_FLOAT fCHANCE_M7_KIDS_GAMING						20.0
		CONST_FLOAT fCHANCE_M7_KIDS_TV							20.0
		CONST_FLOAT fCHANCE_M7_HOOKERS							20.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[10]
		FLOAT 					fPercentArray[10]
		
		eRequestArray[0] = PR_SCENE_M4_WATCHINGTV				fPercentArray[0] = fCHANCE_M4_WATCHINGTV		
		eRequestArray[1] = PR_SCENE_M7_GETSREADY				fPercentArray[1] = fCHANCE_M7_GETSREADY	
		eRequestArray[2] = PR_SCENE_M7_KIDS_GAMING				fPercentArray[2] = fCHANCE_M7_KIDS_GAMING	
		eRequestArray[3] = PR_SCENE_M7_KIDS_TV					fPercentArray[3] = fCHANCE_M7_KIDS_TV		
		eRequestArray[4] = PR_SCENE_M7_HOOKERS					fPercentArray[4] = fCHANCE_M7_HOOKERS		
		
		eRequestArray[5] = PR_SCENE_M6_PARKEDHILLS_a			fPercentArray[5] = 1.0
		eRequestArray[6] = PR_SCENE_M6_PARKEDHILLS_b			fPercentArray[6] = 1.0
		eRequestArray[7] = PR_SCENE_M6_PARKEDHILLS_c			fPercentArray[7] = 1.0
		eRequestArray[8] = PR_SCENE_M6_PARKEDHILLS_d			fPercentArray[8] = 1.0
		eRequestArray[9] = PR_SCENE_M6_PARKEDHILLS_e			fPercentArray[9] = 1.0
		
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING, bDescentOnlyScene,
				PR_SCENE_M7_KIDS_GAMING, PR_SCENE_M7_KIDS_TV)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELSE
		eReqScene = PR_SCENE_INVALID		fScenePercent = -1
		CASSERTLN(DEBUG_SWITCH, "invalid thisMichaelScheduleStage in GetMichaelSceneForMorning()")
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
ENDFUNC
FUNC BOOL GetMichaelSceneForMorningParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetMichaelSceneForMorning(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

/// PURPOSE:
///    /*	09:00 - 13:59	*/
/// PARAMS:
///    eReqScene - 
/// RETURNS:
///    
FUNC BOOL GetMichaelSceneForAfternoon(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent,
		PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_MICHAEL
	eReqScene = PR_SCENE_INVALID		fScenePercent = -1
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_M_POOLSIDE_a						1.5
		CONST_FLOAT fCHANCE_M_POOLSIDE_b						1.5
		CONST_FLOAT fCHANCE_M2_SMOKINGGOLF						2.0
		CONST_FLOAT fCHANCE_M_PIER_a							1.0
		CONST_FLOAT fCHANCE_M_PIER_b							1.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_a							1.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_b							1.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_c							1.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_a						2.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_b						1.0
		
		CONST_FLOAT fCHANCE_M2_WIFEEXITSCAR						10.5
		CONST_FLOAT fCHANCE_M2_DROPOFFDAU_a						10.5
		CONST_FLOAT fCHANCE_M2_DROPOFFSON_a						10.5
		CONST_FLOAT fCHANCE_M2_DROPOFFSON_b						10.5
		
		CONST_FLOAT fCHANCE_M_BENCHCALL_a						3.0
		CONST_FLOAT fCHANCE_M_BENCHCALL_b						2.0
		CONST_FLOAT fCHANCE_M_COFFEE_a							1.0
		CONST_FLOAT fCHANCE_M_COFFEE_b							1.0
		CONST_FLOAT fCHANCE_M_COFFEE_c							1.0
		CONST_FLOAT fCHANCE_M2_CYCLING_a						2.0
		CONST_FLOAT fCHANCE_M2_CYCLING_b						2.0
		CONST_FLOAT fCHANCE_M2_CYCLING_c						2.0
		CONST_FLOAT fCHANCE_M2_LUNCH_a							10.0
		CONST_FLOAT fCHANCE_M2_PHARMACY							5.0
		CONST_FLOAT fCHANCE_M2_MARINA							1.0
		CONST_FLOAT fCHANCE_M2_ARGUEWITHWIFE					5.0
		CONST_FLOAT fCHANCE_M2_DRIVING_a						2.0
		CONST_FLOAT fCHANCE_M2_SAVEHOUSE1_b						10.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[30]
		FLOAT					fPercentArray[30]
		
		eRequestArray[0] = PR_SCENE_M_POOLSIDE_a				fPercentArray[0] = fCHANCE_M_POOLSIDE_a
		eRequestArray[1] = PR_SCENE_M_POOLSIDE_b				fPercentArray[1] = fCHANCE_M_POOLSIDE_b
		
		IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_GOLF))
			eRequestArray[2] = PR_SCENE_M2_SMOKINGGOLF			fPercentArray[2] = fCHANCE_M2_SMOKINGGOLF
		ELSE
			eRequestArray[2] = PR_SCENE_INVALID					fPercentArray[2] = 0.0
		ENDIF
		
		eRequestArray[3] = PR_SCENE_M_PIER_a					fPercentArray[3] = fCHANCE_M_PIER_a
		eRequestArray[4] = PR_SCENE_M_PIER_b					fPercentArray[4] = fCHANCE_M_PIER_b
		
		eRequestArray[5] = PR_SCENE_M_TRAFFIC_a					fPercentArray[5] = fCHANCE_M_TRAFFIC_a
		eRequestArray[6] = PR_SCENE_M_TRAFFIC_b					fPercentArray[6] = fCHANCE_M_TRAFFIC_b
		eRequestArray[7] = PR_SCENE_M_TRAFFIC_c					fPercentArray[7] = fCHANCE_M_TRAFFIC_c
		
		eRequestArray[8] = PR_SCENE_M_VWOODPARK_a				fPercentArray[8] = fCHANCE_M_VWOODPARK_a
		eRequestArray[9] = PR_SCENE_M_VWOODPARK_b				fPercentArray[9] = fCHANCE_M_VWOODPARK_b
		
		eRequestArray[10] = PR_SCENE_M2_WIFEEXITSCAR			fPercentArray[10] = fCHANCE_M2_WIFEEXITSCAR
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2)
			IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)		//tracey leaves after fam4
				eRequestArray[11] = PR_SCENE_M2_DROPOFFDAU_a	fPercentArray[11] = fCHANCE_M2_DROPOFFDAU_a
			ELSE
				eRequestArray[11] = PR_SCENE_INVALID			fPercentArray[11] = 0.0
			ENDIF
			
			eRequestArray[12] = PR_SCENE_M2_DROPOFFSON_a		fPercentArray[12] = fCHANCE_M2_DROPOFFSON_a
			eRequestArray[13] = PR_SCENE_M2_DROPOFFSON_b		fPercentArray[13] = fCHANCE_M2_DROPOFFSON_b
		ELSE
			eRequestArray[11] = PR_SCENE_INVALID				fPercentArray[11] = 0.0
			eRequestArray[12] = PR_SCENE_INVALID				fPercentArray[12] = 0.0
			eRequestArray[13] = PR_SCENE_INVALID				fPercentArray[13] = 0.0
		ENDIF
		
		eRequestArray[14] = PR_SCENE_M_BENCHCALL_a				fPercentArray[14] = fCHANCE_M_BENCHCALL_a
		eRequestArray[15] = PR_SCENE_M_BENCHCALL_b				fPercentArray[15] = fCHANCE_M_BENCHCALL_b
		
		eRequestArray[16] = PR_SCENE_M_COFFEE_a					fPercentArray[16] = fCHANCE_M_COFFEE_a
		eRequestArray[17] = PR_SCENE_M_COFFEE_b					fPercentArray[17] = fCHANCE_M_COFFEE_b
		eRequestArray[18] = PR_SCENE_M_COFFEE_c					fPercentArray[18] = fCHANCE_M_COFFEE_c
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)		//1080407
			eRequestArray[19] = PR_SCENE_M2_CYCLING_a			fPercentArray[19] = fCHANCE_M2_CYCLING_a
			eRequestArray[20] = PR_SCENE_M2_CYCLING_b			fPercentArray[20] = fCHANCE_M2_CYCLING_b
			eRequestArray[21] = PR_SCENE_M2_CYCLING_c			fPercentArray[21] = fCHANCE_M2_CYCLING_c
		ELSE
			eRequestArray[19] = PR_SCENE_INVALID				fPercentArray[19] = 0.0
			eRequestArray[20] = PR_SCENE_INVALID				fPercentArray[20] = 0.0
			eRequestArray[21] = PR_SCENE_INVALID				fPercentArray[21] = 0.0
		ENDIF
		
		eRequestArray[22] = PR_SCENE_M2_LUNCH_a					fPercentArray[22] = fCHANCE_M2_LUNCH_a
		
		eRequestArray[24] = PR_SCENE_M2_PHARMACY				fPercentArray[24] = fCHANCE_M2_PHARMACY
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
			eRequestArray[25] = PR_SCENE_M2_MARINA				fPercentArray[25] = fCHANCE_M2_MARINA
		ELSE
			eRequestArray[25] = PR_SCENE_INVALID				fPercentArray[25] = 0.0
		ENDIF
		eRequestArray[26] = PR_SCENE_M2_ARGUEWITHWIFE			fPercentArray[26] = fCHANCE_M2_ARGUEWITHWIFE
		
		eRequestArray[27] = PR_SCENE_M2_DRIVING_a				fPercentArray[27] =  fCHANCE_M2_DRIVING_a / 2.0
		eRequestArray[28] = PR_SCENE_M2_DRIVING_b				fPercentArray[28] =  fCHANCE_M2_DRIVING_a / 2.0
		
		eRequestArray[29] = PR_SCENE_M2_SAVEHOUSE1_b			fPercentArray[29] = fCHANCE_M2_SAVEHOUSE1_b
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2)
			
			IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)		//tracey leaves after fam4
			
				IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
						PR_SCENE_M2_WIFEEXITSCAR, PR_SCENE_M2_DROPOFFDAU_a, PR_SCENE_M2_DROPOFFSON_a, PR_SCENE_M2_DROPOFFSON_b,
						PR_SCENE_M2_ARGUEWITHWIFE)
					RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
				ENDIF
			ELSE
				IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
						PR_SCENE_M2_WIFEEXITSCAR, PR_SCENE_M2_DROPOFFSON_a, PR_SCENE_M2_DROPOFFSON_b,
						PR_SCENE_M2_ARGUEWITHWIFE)
					RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
				ENDIF
				
			ENDIF
		ELSE
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
					PR_SCENE_M2_WIFEEXITSCAR,
					PR_SCENE_M2_ARGUEWITHWIFE)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
			ENDIF
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		CONST_FLOAT fCHANCE_M4_WAKEUPSCREAM						10.5
		CONST_FLOAT fCHANCE_M4_WAKESUPSCARED					10.5
		CONST_FLOAT fCHANCE_M_POOLSIDE_a						5.0
		CONST_FLOAT fCHANCE_M_POOLSIDE_b						5.0
		CONST_FLOAT fCHANCE_M4_HOUSEBED_b						2.5
		CONST_FLOAT fCHANCE_M_CANAL_a							2.5
		CONST_FLOAT fCHANCE_M_CANAL_b							2.5
		CONST_FLOAT fCHANCE_M_PIER_a							2.5
		CONST_FLOAT fCHANCE_M_PIER_b							2.5
		
		CONST_FLOAT fCHANCE_M_TRAFFIC_a							2.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_b							2.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_c							2.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_a						2.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_b						1.0
		
		CONST_FLOAT fCHANCE_M_BENCHCALL_a						2.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_a						2.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_b						2.0
		
		CONST_FLOAT fCHANCE_M_COFFEE_a							3.0
		CONST_FLOAT fCHANCE_M_COFFEE_b							3.0
		CONST_FLOAT fCHANCE_M_COFFEE_c							2.0
		
		CONST_FLOAT fCHANCE_M4_EXITRESTAURANT					10.0
		CONST_FLOAT fCHANCE_M4_LUNCH_b							5.0
		CONST_FLOAT fCHANCE_M4_CINEMA							10.0
		CONST_FLOAT fCHANCE_M_BAR_a								2.5
		CONST_FLOAT fCHANCE_M_BAR_b								2.5
		CONST_FLOAT fCHANCE_M4_DOORSTUMBLE						11.0

		PED_REQUEST_SCENE_ENUM	eRequestArray[24-3+2]
		FLOAT					fPercentArray[24-3+2]
		
		eRequestArray[0] = PR_SCENE_M4_WAKEUPSCREAM				fPercentArray[0] = fCHANCE_M4_WAKEUPSCREAM
		eRequestArray[1] = PR_SCENE_M4_WAKESUPSCARED			fPercentArray[1] = fCHANCE_M4_WAKESUPSCARED
		eRequestArray[2] = PR_SCENE_M_POOLSIDE_a				fPercentArray[2] = fCHANCE_M_POOLSIDE_a
		eRequestArray[3] = PR_SCENE_M_POOLSIDE_b				fPercentArray[3] = fCHANCE_M_POOLSIDE_b
//		eRequestArray[4] = PR_SCENE_M4_HOUSEBED_b				fPercentArray[4] = fCHANCE_M4_HOUSEBED_b
		
		eRequestArray[3+1] = PR_SCENE_M_CANAL_a					fPercentArray[3+1] = fCHANCE_M_CANAL_a
		eRequestArray[4+1] = PR_SCENE_M_CANAL_b					fPercentArray[4+1] = fCHANCE_M_CANAL_b
		eRequestArray[5+1] = PR_SCENE_M_PIER_a					fPercentArray[5+1] = fCHANCE_M_PIER_a
		eRequestArray[6+1] = PR_SCENE_M_PIER_b					fPercentArray[6+1] = fCHANCE_M_PIER_b
		eRequestArray[7+1] = PR_SCENE_M_TRAFFIC_a				fPercentArray[7+1] = fCHANCE_M_TRAFFIC_a
		eRequestArray[10-2+1] = PR_SCENE_M_TRAFFIC_b			fPercentArray[10-2+1] = fCHANCE_M_TRAFFIC_b
		eRequestArray[11-2+1] = PR_SCENE_M_TRAFFIC_c			fPercentArray[11-2+1] = fCHANCE_M_TRAFFIC_c
		eRequestArray[10+1] = PR_SCENE_M_VWOODPARK_a			fPercentArray[10+1] = fCHANCE_M_VWOODPARK_a
		eRequestArray[11+1] = PR_SCENE_M_VWOODPARK_b			fPercentArray[11+1] = fCHANCE_M_VWOODPARK_b
		eRequestArray[12+1] = PR_SCENE_M_BENCHCALL_a			fPercentArray[12+1] = fCHANCE_M_BENCHCALL_a
		eRequestArray[13+1] = PR_SCENE_M_PARKEDHILLS_a			fPercentArray[13+1] = fCHANCE_M_PARKEDHILLS_a
		eRequestArray[14+1] = PR_SCENE_M_PARKEDHILLS_b			fPercentArray[14+1] = fCHANCE_M_PARKEDHILLS_b
		eRequestArray[15+1] = PR_SCENE_M_COFFEE_a				fPercentArray[15+1] = fCHANCE_M_COFFEE_a
		eRequestArray[16+1] = PR_SCENE_M_COFFEE_b				fPercentArray[16+1] = fCHANCE_M_COFFEE_b
		eRequestArray[17+1] = PR_SCENE_M_COFFEE_c				fPercentArray[17+1] = fCHANCE_M_COFFEE_c
		eRequestArray[20-2+1] = PR_SCENE_M4_EXITRESTAURANT		fPercentArray[20-2+1] = fCHANCE_M4_EXITRESTAURANT
		eRequestArray[21-2+1] = PR_SCENE_M4_LUNCH_b				fPercentArray[21-2+1] = fCHANCE_M4_LUNCH_b
		eRequestArray[20+1] = PR_SCENE_M4_CINEMA				fPercentArray[20+1] = fCHANCE_M4_CINEMA
//		eRequestArray[21+1] = PR_SCENE_M_BAR_a					fPercentArray[21+1] = fCHANCE_M_BAR_a
//		eRequestArray[22+1] = PR_SCENE_M_BAR_b					fPercentArray[22+1] = fCHANCE_M_BAR_b
//		eRequestArray[23+1] = PR_SCENE_M4_DOORSTUMBLE			fPercentArray[23+1] = fCHANCE_M4_DOORSTUMBLE
		eRequestArray[20+1+1] = PR_SCENE_M4_WASHFACE			fPercentArray[20+1+1] = fCHANCE_M4_CINEMA
		
//		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
//				PR_SCENE_M4_DOORSTUMBLE)
//			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
//		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M6_Exiled)
		CONST_FLOAT fCHANCE_M6_SUNBATHING						20.0
		CONST_FLOAT fCHANCE_M6_ONPHONE							20.0
		CONST_FLOAT fCHANCE_M6_DEPRESSED						20.0
		CONST_FLOAT fCHANCE_M6_BOATING							15.0
		CONST_FLOAT fCHANCE_M6_LIQUORSTORE						15.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_a					3.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_b					3.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_c					3.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_d					3.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_e					3.0
		CONST_FLOAT fCHANCE_M6_DRIVING_e						1.5
		CONST_FLOAT fCHANCE_M6_DRIVING_f						1.5
		CONST_FLOAT fCHANCE_M6_HOUSEBED							2.5
		CONST_FLOAT fCHANCE_M6_RONBORING						15.0


		PED_REQUEST_SCENE_ENUM	eRequestArray[14]
		FLOAT 					fPercentArray[14]
		
		eRequestArray[0] = PR_SCENE_M6_SUNBATHING
		IF IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sLimitedWardrobeItems[LPW_MICHAEL_COUNTRYSIDE].iItemBitset[COMP_TYPE_OUTFIT][(ENUM_TO_INT(OUTFIT_P0_YOGA_FLIP_FLOPS)/32)], (ENUM_TO_INT(OUTFIT_P0_YOGA_FLIP_FLOPS)%32))
			fPercentArray[0] = fCHANCE_M6_SUNBATHING * 2.0
		ELSE
			fPercentArray[0] = fCHANCE_M6_SUNBATHING
		ENDIF
		
		eRequestArray[1] = PR_SCENE_M6_ONPHONE					fPercentArray[1] = fCHANCE_M6_ONPHONE
		eRequestArray[2] = PR_SCENE_M6_DEPRESSED				fPercentArray[2] = fCHANCE_M6_DEPRESSED
		eRequestArray[3] = PR_SCENE_M6_BOATING					fPercentArray[3] = fCHANCE_M6_BOATING			
		eRequestArray[4] = PR_SCENE_M6_LIQUORSTORE				fPercentArray[4] = fCHANCE_M6_LIQUORSTORE		
		eRequestArray[5] = PR_SCENE_M6_PARKEDHILLS_a			fPercentArray[5] = fCHANCE_M6_PARKEDHILLS_a	
		eRequestArray[6] = PR_SCENE_M6_PARKEDHILLS_b			fPercentArray[6] = fCHANCE_M6_PARKEDHILLS_b	
		eRequestArray[7] = PR_SCENE_M6_PARKEDHILLS_c			fPercentArray[7] = fCHANCE_M6_PARKEDHILLS_c	
		eRequestArray[8] = PR_SCENE_M6_PARKEDHILLS_d			fPercentArray[8] = fCHANCE_M6_PARKEDHILLS_d	
		eRequestArray[9] = PR_SCENE_M6_PARKEDHILLS_e			fPercentArray[9] = fCHANCE_M6_PARKEDHILLS_e
		eRequestArray[10] = PR_SCENE_M6_DRIVING_e				fPercentArray[10] = fCHANCE_M6_DRIVING_e
		eRequestArray[11] = PR_SCENE_M6_DRIVING_f				fPercentArray[11] = fCHANCE_M6_DRIVING_f
		eRequestArray[12] = PR_SCENE_M6_HOUSEBED				fPercentArray[12] = fCHANCE_M6_HOUSEBED
		eRequestArray[13] = PR_SCENE_M6_RONBORING				fPercentArray[13] = fCHANCE_M6_RONBORING
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		CONST_FLOAT fCHANCE_M7_RESTAURANT						10.0
		CONST_FLOAT fCHANCE_M7_COFFEE							15.0
		CONST_FLOAT fCHANCE_M7_READSCRIPT						15.0
		CONST_FLOAT fCHANCE_M7_OPENDOORFORAMA					3.33
		CONST_FLOAT fCHANCE_M7_DROPPINGOFFJMY					3.33
		CONST_FLOAT fCHANCE_M7_TRACEYEXITSCAR					3.33
		CONST_FLOAT fCHANCE_M7_BYESOLOMON						5.0
		CONST_FLOAT fCHANCE_M7_EXITBARBER						10.0
		CONST_FLOAT fCHANCE_M7_EXITFANCYSHOP					10.0
		CONST_FLOAT fCHANCE_M7_FAKEYOGA							10.0
		CONST_FLOAT fCHANCE_M7_WIFETENNIS						15.0
		CONST_FLOAT fCHANCE_M7_LUNCH_b							10.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[13-1]
		FLOAT 					fPercentArray[13-1]
		
		eRequestArray[0] = PR_SCENE_M7_RESTAURANT				fPercentArray[0] = fCHANCE_M7_RESTAURANT
		eRequestArray[1] = PR_SCENE_M7_COFFEE					fPercentArray[1] = fCHANCE_M7_COFFEE		
		eRequestArray[2] = PR_SCENE_M7_READSCRIPT				fPercentArray[2] = fCHANCE_M7_READSCRIPT		
		eRequestArray[3] = PR_SCENE_M7_OPENDOORFORAMA			fPercentArray[3] = fCHANCE_M7_OPENDOORFORAMA	
		eRequestArray[4] = PR_SCENE_M7_DROPPINGOFFJMY			fPercentArray[4] = fCHANCE_M7_DROPPINGOFFJMY	
		eRequestArray[5] = PR_SCENE_M7_TRACEYEXITSCAR			fPercentArray[5] = fCHANCE_M7_TRACEYEXITSCAR	
		eRequestArray[6] = PR_SCENE_M7_BYESOLOMON_a				fPercentArray[6] = fCHANCE_M7_BYESOLOMON / 2.0		
		eRequestArray[7] = PR_SCENE_M7_BYESOLOMON_b				fPercentArray[7] = fCHANCE_M7_BYESOLOMON / 2.0
		
		IF HAS_SHOP_RUN_ENTRY_INTRO(HAIRDO_SHOP_01_BH)
			eRequestArray[8] = PR_SCENE_M7_EXITBARBER			fPercentArray[8] = fCHANCE_M7_EXITBARBER
		ELSE
			eRequestArray[8] = PR_SCENE_INVALID					fPercentArray[8] = 0.0
		ENDIF
		
		eRequestArray[9] = PR_SCENE_M7_EXITFANCYSHOP			fPercentArray[9] = fCHANCE_M7_EXITFANCYSHOP	
		eRequestArray[10] = PR_SCENE_M7_FAKEYOGA				fPercentArray[10] = fCHANCE_M7_FAKEYOGA		
		eRequestArray[11] = PR_SCENE_M7_WIFETENNIS				fPercentArray[11] = fCHANCE_M7_WIFETENNIS
//		eRequestArray[12] = PR_SCENE_M7_LUNCH_b					fPercentArray[12] = fCHANCE_M7_LUNCH_b
		
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
				PR_SCENE_M7_BYESOLOMON_a, PR_SCENE_M7_BYESOLOMON_b,
				PR_SCENE_M7_FAKEYOGA, PR_SCENE_M7_WIFETENNIS,
				PR_SCENE_M7_ROUNDTABLE, PR_SCENE_M7_KIDS_TV)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ELSE
		eReqScene = PR_SCENE_INVALID		fScenePercent = -1
		CASSERTLN(DEBUG_SWITCH, "invalid thisMichaelScheduleStage in GetMichaelSceneForAfternoon()")
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
ENDFUNC
FUNC BOOL GetMichaelSceneForAfternoonParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetMichaelSceneForAfternoon(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

/// PURPOSE:
///    /*	14:00 - 19:59	*/
/// PARAMS:
///    eReqScene - 
/// RETURNS:
///    
FUNC BOOL GetMichaelSceneForEvening(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_MICHAEL
	eReqScene = PR_SCENE_INVALID		fScenePercent = -1
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_M_PIER_a							9.0
		CONST_FLOAT fCHANCE_M_PIER_b							5.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_a							7.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_b							9.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_c							7.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_a						5.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_b						1.0
		CONST_FLOAT fCHANCE_M_BENCHCALL_a						5.0
		CONST_FLOAT fCHANCE_M_BENCHCALL_b						5.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_a						5.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_b						5.0
		CONST_FLOAT fCHANCE_M2_MARINA							10.0
		CONST_FLOAT fCHANCE_M2_ARGUEWITHWIFE					16.0
		CONST_FLOAT fCHANCE_M2_LUNCH_a							5.0
		CONST_FLOAT fCHANCE_M2_DRIVING_a						2.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[16]						
		FLOAT 					fPercentArray[16]
		
		eRequestArray[0] = PR_SCENE_M_PIER_a					fPercentArray[0] = fCHANCE_M_PIER_a
		eRequestArray[1] = PR_SCENE_M_PIER_b					fPercentArray[1] = fCHANCE_M_PIER_b
		eRequestArray[2] = PR_SCENE_M_TRAFFIC_a					fPercentArray[2] = fCHANCE_M_TRAFFIC_a
		eRequestArray[3] = PR_SCENE_M_TRAFFIC_b					fPercentArray[3] = fCHANCE_M_TRAFFIC_b
		eRequestArray[4] = PR_SCENE_M_TRAFFIC_c					fPercentArray[4] = fCHANCE_M_TRAFFIC_c
		eRequestArray[5] = PR_SCENE_M_VWOODPARK_a				fPercentArray[5] = fCHANCE_M_VWOODPARK_a
		eRequestArray[6] = PR_SCENE_M_VWOODPARK_b				fPercentArray[6] = fCHANCE_M_VWOODPARK_b
		eRequestArray[7] = PR_SCENE_M_BENCHCALL_a				fPercentArray[7] = fCHANCE_M_BENCHCALL_a
		eRequestArray[8] = PR_SCENE_M_BENCHCALL_b				fPercentArray[8] = fCHANCE_M_BENCHCALL_b
		eRequestArray[9] = PR_SCENE_M_PARKEDHILLS_a				fPercentArray[9] = fCHANCE_M_PARKEDHILLS_a
		eRequestArray[10] = PR_SCENE_M_PARKEDHILLS_b			fPercentArray[10] = fCHANCE_M_PARKEDHILLS_b
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
			eRequestArray[11] = PR_SCENE_M2_MARINA				fPercentArray[11] = fCHANCE_M2_MARINA
		ELSE
			eRequestArray[11] = PR_SCENE_INVALID				fPercentArray[11] = 0.0
		ENDIF
		
		eRequestArray[12] = PR_SCENE_M2_ARGUEWITHWIFE			fPercentArray[12] = fCHANCE_M2_ARGUEWITHWIFE
		eRequestArray[13] = PR_SCENE_M2_LUNCH_a					fPercentArray[13] = fCHANCE_M2_LUNCH_a
		
		eRequestArray[14] = PR_SCENE_M2_DRIVING_a				fPercentArray[14] =  fCHANCE_M2_DRIVING_a / 2.0
		eRequestArray[15] = PR_SCENE_M2_DRIVING_b				fPercentArray[15] =  fCHANCE_M2_DRIVING_a / 2.0
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
			//	PR_SCENE_M_TRAFFIC_a, PR_SCENE_M_TRAFFIC_b, PR_SCENE_M_TRAFFIC_c,
				PR_SCENE_M2_ARGUEWITHWIFE,
				PR_SCENE_M2_LUNCH_a)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		CONST_FLOAT fCHANCE_M4_WAKEUPSCREAM					10.0
		CONST_FLOAT fCHANCE_M4_WAKESUPSCARED				10.5
		CONST_FLOAT fCHANCE_M_CANAL_a						3.0
		CONST_FLOAT fCHANCE_M_CANAL_b						3.0
		CONST_FLOAT fCHANCE_M_CANAL_c						3.0
		CONST_FLOAT fCHANCE_M_PIER_a						3.0
		CONST_FLOAT fCHANCE_M_PIER_b						3.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_a						3.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_b						3.0
		CONST_FLOAT fCHANCE_M_TRAFFIC_c						4.0
		CONST_FLOAT fCHANCE_M_VWOODPARK_a					1.5
		CONST_FLOAT fCHANCE_M_VWOODPARK_b					1.0
		CONST_FLOAT fCHANCE_M_BENCHCALL_a					1.0
		CONST_FLOAT fCHANCE_M_BENCHCALL_b					1.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_a					1.5
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_b					1.5
		CONST_FLOAT fCHANCE_M_COFFEE_a						4.0
		CONST_FLOAT fCHANCE_M_COFFEE_b						3.0
		CONST_FLOAT fCHANCE_M_COFFEE_c						3.0
		CONST_FLOAT fCHANCE_M_BAR_a							6.0
		CONST_FLOAT fCHANCE_M_BAR_b							6.0
		CONST_FLOAT fCHANCE_M4_PARKEDBEACH					4.0
		CONST_FLOAT fCHANCE_M4_DOORSTUMBLE					5.0
		CONST_FLOAT fCHANCE_M4_EXITRESTAURANT				2.5
		CONST_FLOAT fCHANCE_M4_LUNCH_b						2.5
		CONST_FLOAT fCHANCE_M4_CINEMA						5.0
		CONST_FLOAT fCHANCE_M_HOOKERMOTEL					8.0
		CONST_FLOAT fCHANCE_M_HOOKERCAR						5.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[28-3-1+1]
		FLOAT 					fPercentArray[28-3-1+1]
		
		eRequestArray[0] = PR_SCENE_M4_WAKEUPSCREAM			fPercentArray[0] = fCHANCE_M4_WAKEUPSCREAM
		eRequestArray[1] = PR_SCENE_M4_WAKESUPSCARED		fPercentArray[1] = fCHANCE_M4_WAKESUPSCARED
		eRequestArray[2] = PR_SCENE_M_CANAL_a				fPercentArray[2] = fCHANCE_M_CANAL_a						
		eRequestArray[3] = PR_SCENE_M_CANAL_b				fPercentArray[3] = fCHANCE_M_CANAL_b						
		eRequestArray[4] = PR_SCENE_M_CANAL_c				fPercentArray[4] = fCHANCE_M_CANAL_c						
		eRequestArray[5] = PR_SCENE_M_PIER_a				fPercentArray[5] = fCHANCE_M_PIER_a						
		eRequestArray[6] = PR_SCENE_M_PIER_b				fPercentArray[6] = fCHANCE_M_PIER_b						
		eRequestArray[7] = PR_SCENE_M_TRAFFIC_a				fPercentArray[7] = fCHANCE_M_TRAFFIC_a					
		eRequestArray[8] = PR_SCENE_M_TRAFFIC_b				fPercentArray[8] = fCHANCE_M_TRAFFIC_b					
		eRequestArray[9] = PR_SCENE_M_TRAFFIC_c				fPercentArray[9] = fCHANCE_M_TRAFFIC_c					
		eRequestArray[10] = PR_SCENE_M_VWOODPARK_a			fPercentArray[10] = fCHANCE_M_VWOODPARK_a					
		eRequestArray[11] = PR_SCENE_M_VWOODPARK_b			fPercentArray[11] = fCHANCE_M_VWOODPARK_b					
		eRequestArray[12] = PR_SCENE_M_BENCHCALL_a			fPercentArray[12] = fCHANCE_M_BENCHCALL_a					
		eRequestArray[13] = PR_SCENE_M_BENCHCALL_b			fPercentArray[13] = fCHANCE_M_BENCHCALL_b					
		eRequestArray[14] = PR_SCENE_M_PARKEDHILLS_a		fPercentArray[14] = fCHANCE_M_PARKEDHILLS_a
		eRequestArray[15] = PR_SCENE_M_PARKEDHILLS_b		fPercentArray[15] = fCHANCE_M_PARKEDHILLS_b
		eRequestArray[16] = PR_SCENE_M_COFFEE_a				fPercentArray[16] = fCHANCE_M_COFFEE_a						
		eRequestArray[17] = PR_SCENE_M_COFFEE_b				fPercentArray[17] = fCHANCE_M_COFFEE_b						
		eRequestArray[18] = PR_SCENE_M_COFFEE_c				fPercentArray[18] = fCHANCE_M_COFFEE_c						
//		eRequestArray[19] = PR_SCENE_M_BAR_a				fPercentArray[19] = fCHANCE_M_BAR_a
//		eRequestArray[20] = PR_SCENE_M_BAR_b				fPercentArray[20] = fCHANCE_M_BAR_b
		eRequestArray[21-2] = PR_SCENE_M4_PARKEDBEACH		fPercentArray[21-2] = fCHANCE_M4_PARKEDBEACH					
//		eRequestArray[22-2] = PR_SCENE_M4_DOORSTUMBLE		fPercentArray[22-2] = fCHANCE_M4_DOORSTUMBLE				
		eRequestArray[23-2-1] = PR_SCENE_M4_EXITRESTAURANT	fPercentArray[23-2-1] = fCHANCE_M4_EXITRESTAURANT						
		eRequestArray[24-2-1] = PR_SCENE_M4_LUNCH_b			fPercentArray[24-2-1] = fCHANCE_M4_LUNCH_b						
		eRequestArray[25-2-1] = PR_SCENE_M4_CINEMA			fPercentArray[25-2-1] = fCHANCE_M4_CINEMA						
		eRequestArray[26-2-1] = PR_SCENE_M_HOOKERMOTEL		fPercentArray[26-2-1] = fCHANCE_M_HOOKERMOTEL
//		eRequestArray[27-2-1] = PR_SCENE_M_HOOKERCAR		fPercentArray[27-2-1] = fCHANCE_M_HOOKERCAR
		eRequestArray[25-2-1+1] = PR_SCENE_M4_WASHFACE		fPercentArray[25-2-1+1] = fCHANCE_M_HOOKERMOTEL
		
		
//		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
//				PR_SCENE_M4_DOORSTUMBLE)
//			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
//		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M6_Exiled)
		CONST_FLOAT fCHANCE_M6_DRINKINGBEER		20.0
		CONST_FLOAT fCHANCE_M6_ONPHONE			15.0
		CONST_FLOAT fCHANCE_M6_DEPRESSED		15.0
		CONST_FLOAT fCHANCE_M6_BOATING			20.0
		CONST_FLOAT fCHANCE_M6_LIQUORSTORE		15.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_a	4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_b	4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_c	4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_d	4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_e	4.0
		CONST_FLOAT fCHANCE_M6_DRIVING_g		1.5
		CONST_FLOAT fCHANCE_M6_DRIVING_h		1.5
		CONST_FLOAT fCHANCE_M6_RONBORING		7.5
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[13]
		FLOAT 					fPercentArray[13]
		
		eRequestArray[0] = PR_SCENE_M6_DRINKINGBEER			fPercentArray[0] = fCHANCE_M6_DRINKINGBEER
		eRequestArray[1] = PR_SCENE_M6_ONPHONE				fPercentArray[2] = fCHANCE_M6_ONPHONE
		eRequestArray[2] = PR_SCENE_M6_DEPRESSED			fPercentArray[2] = fCHANCE_M6_DEPRESSED
		eRequestArray[3] = PR_SCENE_M6_BOATING				fPercentArray[3] = fCHANCE_M6_BOATING
		eRequestArray[4] = PR_SCENE_M6_LIQUORSTORE			fPercentArray[4] = fCHANCE_M6_LIQUORSTORE
		eRequestArray[5] = PR_SCENE_M6_PARKEDHILLS_a		fPercentArray[5] = fCHANCE_M6_PARKEDHILLS_a
		eRequestArray[6] = PR_SCENE_M6_PARKEDHILLS_b		fPercentArray[6] = fCHANCE_M6_PARKEDHILLS_b
		eRequestArray[7] = PR_SCENE_M6_PARKEDHILLS_c		fPercentArray[7] = fCHANCE_M6_PARKEDHILLS_c
		eRequestArray[8] = PR_SCENE_M6_PARKEDHILLS_d		fPercentArray[8] = fCHANCE_M6_PARKEDHILLS_d
		eRequestArray[9] = PR_SCENE_M6_PARKEDHILLS_e		fPercentArray[9] = fCHANCE_M6_PARKEDHILLS_e
		eRequestArray[10] = PR_SCENE_M6_DRIVING_g			fPercentArray[10] = fCHANCE_M6_DRIVING_g
		eRequestArray[11] = PR_SCENE_M6_DRIVING_h			fPercentArray[11] = fCHANCE_M6_DRIVING_h
		eRequestArray[12] = PR_SCENE_M6_RONBORING			fPercentArray[12] = fCHANCE_M6_RONBORING
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		CONST_FLOAT fCHANCE_M7_RESTAURANT						10.0
		CONST_FLOAT fCHANCE_M7_BYESOLOMON						5.0
		CONST_FLOAT fCHANCE_M7_READSCRIPT						5.0
		CONST_FLOAT fCHANCE_M7_EMPLOYEECONVO					5.0
		CONST_FLOAT fCHANCE_M7_TALKTOGUARD						5.0
		CONST_FLOAT fCHANCE_M7_LOT_JIMMY						5.0
		
		CONST_FLOAT fCHANCE_M7_KIDS_TV							5.0
		CONST_FLOAT fCHANCE_M7_KIDS_GAMING						5.0
		
		CONST_FLOAT fCHANCE_M7_OPENDOORFORAMA					5.0
		CONST_FLOAT fCHANCE_M7_DROPPINGOFFJMY					5.0
		CONST_FLOAT fCHANCE_M7_TRACEYEXITSCAR					5.0
		
		CONST_FLOAT fCHANCE_M7_REJECTENTRY						5.0
		CONST_FLOAT fCHANCE_M7_LOUNGECHAIRS						10.0
		CONST_FLOAT fCHANCE_M7_BIKINGJIMMY						5.0
		
		CONST_FLOAT fCHANCE_M7_FAMARGUE							10.0
		CONST_FLOAT fCHANCE_M7_FAKEYOGA							5.0
		CONST_FLOAT fCHANCE_M7_WIFETENNIS						5.0
		
		CONST_FLOAT fCHANCE_M7_LUNCH_b							5.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[19+1-1]
		FLOAT 					fPercentArray[19+1-1]
		
		eRequestArray[0] = PR_SCENE_M7_RESTAURANT				fPercentArray[0] = fCHANCE_M7_RESTAURANT
		eRequestArray[3] = PR_SCENE_M7_READSCRIPT				fPercentArray[3] = fCHANCE_M7_READSCRIPT			
		eRequestArray[4] = PR_SCENE_M7_EMPLOYEECONVO			fPercentArray[4] = fCHANCE_M7_EMPLOYEECONVO			
		eRequestArray[5] = PR_SCENE_M7_TALKTOGUARD				fPercentArray[5] = fCHANCE_M7_TALKTOGUARD			
		eRequestArray[6] = PR_SCENE_M7_LOT_JIMMY				fPercentArray[6] = fCHANCE_M7_LOT_JIMMY			
		eRequestArray[7] = PR_SCENE_M7_KIDS_TV					fPercentArray[7] = fCHANCE_M7_KIDS_TV			
		eRequestArray[8] = PR_SCENE_M7_KIDS_GAMING				fPercentArray[8] = fCHANCE_M7_KIDS_GAMING
		eRequestArray[9] = PR_SCENE_M7_OPENDOORFORAMA			fPercentArray[9] = fCHANCE_M7_OPENDOORFORAMA	
		eRequestArray[10] = PR_SCENE_M7_DROPPINGOFFJMY			fPercentArray[10] = fCHANCE_M7_DROPPINGOFFJMY
		eRequestArray[11] = PR_SCENE_M7_TRACEYEXITSCAR			fPercentArray[11] = fCHANCE_M7_TRACEYEXITSCAR
		
		IF GET_MISSION_COMPLETE_STATE(eMichHasMetSolomon)
			eRequestArray[1] = PR_SCENE_M7_BYESOLOMON_a			fPercentArray[1] = fCHANCE_M7_BYESOLOMON / 2.0
			eRequestArray[2] = PR_SCENE_M7_BYESOLOMON_b			fPercentArray[2] = fCHANCE_M7_BYESOLOMON / 2.0
			eRequestArray[12] = PR_SCENE_M7_REJECTENTRY			fPercentArray[12] = fCHANCE_M7_REJECTENTRY
		ELSE
			eRequestArray[1] = PR_SCENE_INVALID					fPercentArray[1] = 0.0
			eRequestArray[2] = PR_SCENE_INVALID					fPercentArray[2] = 0.0
			eRequestArray[12] = PR_SCENE_INVALID				fPercentArray[12] = 0.0
		ENDIF
		
		eRequestArray[13] = PR_SCENE_M7_LOUNGECHAIRS			fPercentArray[13] = fCHANCE_M7_LOUNGECHAIRS		
		eRequestArray[14] = PR_SCENE_M7_BIKINGJIMMY				fPercentArray[14] = fCHANCE_M7_BIKINGJIMMY	
		eRequestArray[15] = PR_SCENE_M7_ROUNDTABLE				fPercentArray[15] = fCHANCE_M7_FAMARGUE
		eRequestArray[16] = PR_SCENE_M7_FAKEYOGA				fPercentArray[16] = fCHANCE_M7_FAKEYOGA			
		eRequestArray[17] = PR_SCENE_M7_WIFETENNIS				fPercentArray[17] = fCHANCE_M7_WIFETENNIS	
		
		IF GET_MISSION_COMPLETE_STATE(eMichHasMetSolomon)
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
					PR_SCENE_M7_OPENDOORFORAMA, PR_SCENE_M7_DROPPINGOFFJMY, PR_SCENE_M7_TRACEYEXITSCAR,
					PR_SCENE_M7_KIDS_TV)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
			ENDIF
		ELSE
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
					PR_SCENE_M7_OPENDOORFORAMA, PR_SCENE_M7_DROPPINGOFFJMY, PR_SCENE_M7_TRACEYEXITSCAR,
					PR_SCENE_M7_ROUNDTABLE)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
			ENDIF
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELSE
		eReqScene = PR_SCENE_INVALID		fScenePercent = -1
		CASSERTLN(DEBUG_SWITCH, "invalid thisMichaelScheduleStage in GetMichaelSceneForEvening()")
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
ENDFUNC
FUNC BOOL GetMichaelSceneForEveningParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetMichaelSceneForEvening(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

/// PURPOSE:
///    /*	20:00 - 23:59	*/
/// PARAMS:
///    eReqScene - 
/// RETURNS:
///    
FUNC BOOL GetMichaelSceneForNight(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_MICHAEL
	eReqScene = PR_SCENE_INVALID		fScenePercent = -1
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_M_CANAL_a							8.0
		CONST_FLOAT fCHANCE_M2_KIDS_TV							14.0
		CONST_FLOAT fCHANCE_M_PIER_a							1.0
		CONST_FLOAT fCHANCE_M_PIER_b							1.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_a						1.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_b						1.0
		CONST_FLOAT fCHANCE_M_BAR_a								10.0
		CONST_FLOAT fCHANCE_M_BAR_b								10.0
		CONST_FLOAT fCHANCE_M2_MARINA							8.0
		CONST_FLOAT fCHANCE_M2_ARGUEWITHWIFE					10.0
		CONST_FLOAT fCHANCE_M2_DROPOFFDAU_b						21.0
		CONST_FLOAT fCHANCE_M_HOOKERMOTEL						7.0
		CONST_FLOAT fCHANCE_M_HOOKERCAR							8.0
		CONST_FLOAT fCHANCE_M2_DRIVING_a						5.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[15-2-1]
		FLOAT 					fPercentArray[15-2-1]
		
		eRequestArray[0] = PR_SCENE_M_CANAL_a					fPercentArray[0] = fCHANCE_M_CANAL_a							
		eRequestArray[1] = PR_SCENE_M2_KIDS_TV					fPercentArray[1] = fCHANCE_M2_KIDS_TV
		eRequestArray[2] = PR_SCENE_M_PIER_a					fPercentArray[2] = fCHANCE_M_PIER_a
		eRequestArray[3] = PR_SCENE_M_PIER_b					fPercentArray[3] = fCHANCE_M_PIER_b							
		eRequestArray[4] = PR_SCENE_M_PARKEDHILLS_a				fPercentArray[4] = fCHANCE_M_PARKEDHILLS_a						
		eRequestArray[5] = PR_SCENE_M_PARKEDHILLS_b				fPercentArray[5] = fCHANCE_M_PARKEDHILLS_b						
//		eRequestArray[6] = PR_SCENE_M_BAR_a						fPercentArray[6] = fCHANCE_M_BAR_a
//		eRequestArray[7] = PR_SCENE_M_BAR_b						fPercentArray[7] = fCHANCE_M_BAR_b
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
			eRequestArray[8-2] = PR_SCENE_M2_MARINA				fPercentArray[8-2] = fCHANCE_M2_MARINA
		ELSE
			eRequestArray[8-2] = PR_SCENE_INVALID				fPercentArray[8-2] = 0.0
		ENDIF
		
		eRequestArray[9-2] = PR_SCENE_M2_ARGUEWITHWIFE			fPercentArray[9-2] = fCHANCE_M2_ARGUEWITHWIFE
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)	//tracey leaves after fam4
			eRequestArray[10-2] = PR_SCENE_M2_DROPOFFDAU_b		fPercentArray[10-2] = fCHANCE_M2_DROPOFFDAU_b						
		ELSE
			eRequestArray[10-2] = PR_SCENE_INVALID				fPercentArray[10-2] = 0.0
		ENDIF
		
		eRequestArray[11-2] = PR_SCENE_M_HOOKERMOTEL				fPercentArray[11-2] = fCHANCE_M_HOOKERMOTEL							
//		eRequestArray[12-2] = PR_SCENE_M_HOOKERCAR				fPercentArray[12-2] = fCHANCE_M_HOOKERCAR						
		
		eRequestArray[13-2-1] = PR_SCENE_M2_DRIVING_a				fPercentArray[13-2-1] =  fCHANCE_M2_DRIVING_a / 2.0
		eRequestArray[14-2-1] = PR_SCENE_M2_DRIVING_b				fPercentArray[14-2-1] =  fCHANCE_M2_DRIVING_a / 2.0
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)		//tracey leaves after fam4
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_NIGHT, bDescentOnlyScene,
					PR_SCENE_M2_ARGUEWITHWIFE, PR_SCENE_M2_DROPOFFDAU_b,
					PR_SCENE_M_HOOKERMOTEL)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_NIGHT)
			ENDIF
		ELSE
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_NIGHT, bDescentOnlyScene,
					PR_SCENE_M2_ARGUEWITHWIFE,
					PR_SCENE_M_HOOKERMOTEL)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_NIGHT)
			ENDIF
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		CONST_FLOAT fCHANCE_M_CANAL_a						4.0
		CONST_FLOAT fCHANCE_M_CANAL_b						3.0
		CONST_FLOAT fCHANCE_M_CANAL_c						3.0
		CONST_FLOAT fCHANCE_M_PIER_a						5.0
		CONST_FLOAT fCHANCE_M_PIER_b						5.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_a					5.0
		CONST_FLOAT fCHANCE_M_PARKEDHILLS_b					5.0
		CONST_FLOAT fCHANCE_M_BAR_a							10.0
		CONST_FLOAT fCHANCE_M_BAR_b							10.0
		CONST_FLOAT fCHANCE_M4_PARKEDBEACH					10.0
		CONST_FLOAT fCHANCE_M4_DOORSTUMBLE					20.0
		CONST_FLOAT fCHANCE_M_HOOKERMOTEL					10.0
		CONST_FLOAT fCHANCE_M_HOOKERCAR						10.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[13-3-1+1]
		FLOAT 					fPercentArray[13-3-1+1]
		
		eRequestArray[0] = PR_SCENE_M_CANAL_a			fPercentArray[0] = fCHANCE_M_CANAL_a						
		eRequestArray[1] = PR_SCENE_M_CANAL_b			fPercentArray[1] = fCHANCE_M_CANAL_b						
		eRequestArray[2] = PR_SCENE_M_CANAL_c			fPercentArray[2] = fCHANCE_M_CANAL_c						
		eRequestArray[3] = PR_SCENE_M_PIER_a			fPercentArray[3] = fCHANCE_M_PIER_a						
		eRequestArray[4] = PR_SCENE_M_PIER_b			fPercentArray[4] = fCHANCE_M_PIER_b						
		eRequestArray[5] = PR_SCENE_M_PARKEDHILLS_a		fPercentArray[5] = fCHANCE_M_PARKEDHILLS_a
		eRequestArray[6] = PR_SCENE_M_PARKEDHILLS_b		fPercentArray[6] = fCHANCE_M_PARKEDHILLS_b
//		eRequestArray[7] = PR_SCENE_M_BAR_a				fPercentArray[7] = fCHANCE_M_BAR_a
//		eRequestArray[8] = PR_SCENE_M_BAR_b				fPercentArray[8] = fCHANCE_M_BAR_b
		eRequestArray[9-2] = PR_SCENE_M4_PARKEDBEACH		fPercentArray[9-2] = fCHANCE_M4_PARKEDBEACH
//		eRequestArray[10-2] = PR_SCENE_M4_DOORSTUMBLE		fPercentArray[10-2] = fCHANCE_M4_DOORSTUMBLE					
		eRequestArray[11-2-1] = PR_SCENE_M_HOOKERMOTEL		fPercentArray[11-2-1] = fCHANCE_M_HOOKERMOTEL					
//		eRequestArray[12-2-1] = PR_SCENE_M_HOOKERCAR		fPercentArray[12-2-1] = fCHANCE_M_HOOKERCAR
		
		eRequestArray[11-2-1+1] = PR_SCENE_M4_WASHFACE		fPercentArray[11-2-1+1] = fCHANCE_M_HOOKERMOTEL					
		
		
//		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_NIGHT, bDescentOnlyScene,
//				PR_SCENE_M4_DOORSTUMBLE)
//			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_NIGHT)
//		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELIF (thisMichaelScheduleStage = MSS_M6_Exiled)
		CONST_FLOAT fCHANCE_M6_DRINKINGBEER						20.0
		CONST_FLOAT fCHANCE_M6_ONPHONE							30.0
		CONST_FLOAT fCHANCE_M6_DEPRESSED						30.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_a					4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_b					4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_c					4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_d					4.0
		CONST_FLOAT fCHANCE_M6_PARKEDHILLS_e					4.0
		CONST_FLOAT fCHANCE_M6_DRIVING_a						5.0
		CONST_FLOAT fCHANCE_M6_DRIVING_b						5.0
		CONST_FLOAT fCHANCE_M6_RONBORING						20.0
		
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[11]
		FLOAT 					fPercentArray[11]
		
		eRequestArray[0] = PR_SCENE_M6_DRINKINGBEER				fPercentArray[0] =  fCHANCE_M6_DRINKINGBEER
		eRequestArray[1] = PR_SCENE_M6_ONPHONE					fPercentArray[1] =  fCHANCE_M6_ONPHONE
		eRequestArray[2] = PR_SCENE_M6_DEPRESSED				fPercentArray[2] =  fCHANCE_M6_DEPRESSED
		eRequestArray[3] = PR_SCENE_M6_PARKEDHILLS_a			fPercentArray[3] =  fCHANCE_M6_PARKEDHILLS_a	
		eRequestArray[4] = PR_SCENE_M6_PARKEDHILLS_b			fPercentArray[4] =  fCHANCE_M6_PARKEDHILLS_b	
		eRequestArray[5] = PR_SCENE_M6_PARKEDHILLS_c			fPercentArray[5] =  fCHANCE_M6_PARKEDHILLS_c
		eRequestArray[6] = PR_SCENE_M6_PARKEDHILLS_d			fPercentArray[6] =  fCHANCE_M6_PARKEDHILLS_d
		eRequestArray[7] = PR_SCENE_M6_PARKEDHILLS_e			fPercentArray[7] =  fCHANCE_M6_PARKEDHILLS_e	
		eRequestArray[8] = PR_SCENE_M6_DRIVING_a				fPercentArray[8] =  fCHANCE_M6_DRIVING_a		
		eRequestArray[9] = PR_SCENE_M6_DRIVING_b				fPercentArray[9] =  fCHANCE_M6_DRIVING_b
		eRequestArray[10] = PR_SCENE_M6_RONBORING				fPercentArray[10] = fCHANCE_M6_RONBORING
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		CONST_FLOAT fCHANCE_M7_RESTAURANT							10.0
		CONST_FLOAT fCHANCE_M7_REJECTENTRY						20.0
		CONST_FLOAT fCHANCE_M7_GETSREADY						10.0
		CONST_FLOAT fCHANCE_M7_EMPLOYEECONVO					5.0
		CONST_FLOAT fCHANCE_M7_TALKTOGUARD						5.0
		CONST_FLOAT fCHANCE_M7_LOT_JIMMY						5.0
		CONST_FLOAT fCHANCE_M7_KIDS_TV							15.0
		CONST_FLOAT fCHANCE_M7_KIDS_GAMING						15.0
		CONST_FLOAT fCHANCE_M7_HOOKERS							15.0
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[14]
		FLOAT 					fPercentArray[14]
		
		eRequestArray[0] = PR_SCENE_M7_RESTAURANT				fPercentArray[0] = fCHANCE_M7_RESTAURANT		
		
		IF GET_MISSION_COMPLETE_STATE(eMichHasMetSolomon)
			eRequestArray[1] = PR_SCENE_M7_REJECTENTRY		fPercentArray[1] = fCHANCE_M7_REJECTENTRY
		ELSE
			eRequestArray[1] = PR_SCENE_INVALID				fPercentArray[1] = 0.0
		ENDIF
		
		eRequestArray[2] = PR_SCENE_M7_GETSREADY			fPercentArray[2] = fCHANCE_M7_GETSREADY	
		eRequestArray[3] = PR_SCENE_M7_EMPLOYEECONVO		fPercentArray[3] = fCHANCE_M7_EMPLOYEECONVO		
		eRequestArray[4] = PR_SCENE_M7_TALKTOGUARD			fPercentArray[4] = fCHANCE_M7_TALKTOGUARD		
		eRequestArray[5] = PR_SCENE_M7_LOT_JIMMY			fPercentArray[5] = fCHANCE_M7_LOT_JIMMY		
		eRequestArray[6] = PR_SCENE_M7_KIDS_TV				fPercentArray[6] = fCHANCE_M7_KIDS_TV		
		eRequestArray[7] = PR_SCENE_M7_KIDS_GAMING			fPercentArray[7] = fCHANCE_M7_KIDS_GAMING	
		eRequestArray[8] = PR_SCENE_M7_HOOKERS				fPercentArray[8] = fCHANCE_M7_HOOKERS		
		eRequestArray[9] = PR_SCENE_M6_PARKEDHILLS_a 		fPercentArray[9] = 1.0
		eRequestArray[10] = PR_SCENE_M6_PARKEDHILLS_b 		fPercentArray[10] = 1.0
		eRequestArray[11] = PR_SCENE_M6_PARKEDHILLS_c 		fPercentArray[11] = 1.0
		eRequestArray[12] = PR_SCENE_M6_PARKEDHILLS_d 		fPercentArray[12] = 1.0
		eRequestArray[13] = PR_SCENE_M6_PARKEDHILLS_e 		fPercentArray[13] = 1.0
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_NIGHT, bDescentOnlyScene,
				PR_SCENE_M7_EMPLOYEECONVO,
				PR_SCENE_M7_ROUNDTABLE, PR_SCENE_M7_KIDS_TV)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_NIGHT)
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
		
	ELSE
		eReqScene = PR_SCENE_INVALID		fScenePercent = -1
		CASSERTLN(DEBUG_SWITCH, "invalid thisMichaelScheduleStage in GetMichaelSceneForNight()")
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
ENDFUNC
FUNC BOOL GetMichaelSceneForNightParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetMichaelSceneForNight(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL GetMichaelSceneForCurrentTime(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	eReqScene = PR_SCENE_INVALID
	fScenePercent = -1
	
	IF NOT bDescentOnlyScene
		IF GetPedRequestSceneForAvailableMission(CHAR_MICHAEL, eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	
	SWITCH PRIVATE_Get_Switch_Clock_Hours()
		/*	00:00 - 08:59	*/
        CASE 0 CASE 1 CASE 2
        CASE 3 CASE 4 CASE 5
		CASE 6 CASE 7 CASE 8
			GetMichaelSceneForMorning(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetMichaelSceneForAfternoonParam, bDescentOnlyScene)
		BREAK
		
		/*	09:00 - 13:59	*/
        CASE 9 CASE 10 CASE 11
        CASE 12 CASE 13
			GetMichaelSceneForAfternoon(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetMichaelSceneForEveningParam, bDescentOnlyScene)
		BREAK
		
		/*	14:00 - 19:59	*/
        CASE 14 CASE 15 CASE 16
		CASE 17 CASE 18 CASE 19
			GetMichaelSceneForEvening(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetMichaelSceneForNightParam, bDescentOnlyScene)
		BREAK
		
		/*	20:00 - 23:59	*/
        CASE 20 CASE 21 CASE 22
		CASE 23
			GetMichaelSceneForNight(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetMichaelSceneForMorningParam, bDescentOnlyScene)
		BREAK
    ENDSWITCH
	
	RETURN (eReqScene <> PR_SCENE_INVALID)
ENDFUNC

// *******************************************************************************************
//	FUNCTIONS TO RETURN NEXT SCENE FOR FRANKLIN
// *******************************************************************************************
FUNC BOOL GetFranklinSceneForNight(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_FRANKLIN
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		PED_REQUEST_SCENE_ENUM	eRequestArray[7-1]
		FLOAT 					fPercentArray[7-1]
		
		CONST_FLOAT fCHANCE_F0_SH_ASLEEP							30.0
		CONST_FLOAT fCHANCE_F0_SH_READING							10.0
		CONST_FLOAT fCHANCE_F0_SH_PUSHUP_a							10.0
		CONST_FLOAT fCHANCE_F0_SH_PUSHUP_b							10.0
		
		CONST_FLOAT fCHANCE_F_LAMTAUNT_NIGHT						10.0
		CONST_FLOAT fCHANCE_F_CLUB									10.0
		CONST_FLOAT fCHANCE_F0_WATCHINGTV							20.0

		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_NIGHT, bDescentOnlyScene,
				PR_SCENE_F0_TANISHAFIGHT)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_NIGHT)
		ENDIF
		
		eRequestArray[0] = PR_SCENE_F0_SH_ASLEEP					fPercentArray[0] = fCHANCE_F0_SH_ASLEEP
		eRequestArray[1] = PR_SCENE_F0_SH_READING					fPercentArray[1] = fCHANCE_F0_SH_READING
		eRequestArray[2] = PR_SCENE_F0_SH_PUSHUP_a					fPercentArray[2] = fCHANCE_F0_SH_PUSHUP_a
		eRequestArray[3] = PR_SCENE_F0_SH_PUSHUP_b					fPercentArray[3] = fCHANCE_F0_SH_PUSHUP_b
//		eRequestArray[4] = PR_SCENE_F0_WATCHINGTV					fPercentArray[4] = fCHANCE_F0_WATCHINGTV
		eRequestArray[5-1] = PR_SCENE_F_LAMTAUNT_NIGHT				fPercentArray[5-1] = fCHANCE_F_LAMTAUNT_NIGHT
		eRequestArray[6-1] = PR_SCENE_F_CLUB						fPercentArray[6-1] = fCHANCE_F_CLUB
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ELSE
		PED_REQUEST_SCENE_ENUM	eRequestArray[6+2]
		FLOAT 					fPercentArray[6+2]
		
		CONST_FLOAT fCHANCE_F1_SH_ASLEEP							50.0
		CONST_FLOAT fCHANCE_F1_SH_READING							10.0
		CONST_FLOAT fCHANCE_F1_WATCHINGTV							10.0
		CONST_FLOAT fCHANCE_F1_BYETAXI								10.0
		CONST_FLOAT fCHANCE_F1_SNACKING								10.0
		CONST_FLOAT fCHANCE_F1_SH_PUSHUP							10.0
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_NIGHT, bDescentOnlyScene,
				PR_SCENE_F1_NEWHOUSE)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_NIGHT)
		ENDIF
		
		eRequestArray[0] = PR_SCENE_F1_SH_ASLEEP					fPercentArray[0] = fCHANCE_F1_SH_ASLEEP / 3
		eRequestArray[1] = PR_SCENE_F1_NAPPING						fPercentArray[1] = fCHANCE_F1_SH_ASLEEP / 3
		eRequestArray[2] = PR_SCENE_F1_GETTINGREADY					fPercentArray[2] = fCHANCE_F1_SH_ASLEEP / 3
		eRequestArray[1+2] = PR_SCENE_F1_SH_READING					fPercentArray[1+2] = fCHANCE_F1_SH_READING
		eRequestArray[2+2] = PR_SCENE_F1_WATCHINGTV					fPercentArray[2+2] = fCHANCE_F1_WATCHINGTV
		eRequestArray[3+2] = PR_SCENE_F1_BYETAXI					fPercentArray[3+2] = fCHANCE_F1_BYETAXI
		eRequestArray[4+2] = PR_SCENE_F1_SNACKING					fPercentArray[4+2] = fCHANCE_F1_SNACKING
		eRequestArray[5+2] = PR_SCENE_F1_SH_PUSHUP					fPercentArray[5+2] = fCHANCE_F1_SH_PUSHUP
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ENDIF
ENDFUNC
FUNC BOOL GetFranklinSceneForNightParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetFranklinSceneForNight(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

FUNC BOOL GetFranklinSceneForMorning(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_FRANKLIN
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		PED_REQUEST_SCENE_ENUM	eRequestArray[21-1]
		FLOAT 					fPercentArray[21-1]
		
		CONST_FLOAT fCHANCE_F0_WATCHINGTV					6.6
		CONST_FLOAT fCHANCE_F_MD_KUSH_DOC					6.2
		CONST_FLOAT fCHANCE_F_KUSH_DOC_a					6.2
		CONST_FLOAT fCHANCE_F_KUSH_DOC_b					6.2
		CONST_FLOAT fCHANCE_F_KUSH_DOC_c					6.2
		CONST_FLOAT fCHANCE_F0_GARBAGE						9.6
		CONST_FLOAT fCHANCE_F_THROW_CUP						3.3		
		CONST_FLOAT fCHANCE_F_HIT_CUP_HAND					3.3		
		CONST_FLOAT fCHANCE_F_TRAFFIC_a						1.6
		CONST_FLOAT fCHANCE_F_TRAFFIC_b						1.6
		CONST_FLOAT fCHANCE_F_TRAFFIC_c						1.6
		
		CONST_FLOAT fCHANCE_F0_BIKE							1.6
		CONST_FLOAT fCHANCE_F_BIKE_c						1.6
		CONST_FLOAT fCHANCE_F_BIKE_d						1.6
		
		CONST_FLOAT fCHANCE_F_CS_CHECKSHOE					7.2		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPEHANDS					7.2		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPERIGHT					7.2		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		
		CONST_FLOAT fCHANCE_F_GYM							6.6
		
		CONST_FLOAT fCHANCE_F0_WALKCHOP						6.6		//(only if chop app enabled)	
		CONST_FLOAT fCHANCE_F0_PLAYCHOP						6.6		//(only if chop app enabled)	
		
		CONST_FLOAT fCHANCE_F0_CLEANCAR						6.6
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING, bDescentOnlyScene,
				PR_SCENE_F0_TANISHAFIGHT)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
		ENDIF
		
//		eRequestArray[0] = PR_SCENE_F0_WATCHINGTV			fPercentArray[0] = fCHANCE_F0_WATCHINGTV
		eRequestArray[1-1] = PR_SCENE_F_MD_KUSH_DOC			fPercentArray[1-1] = fCHANCE_F_MD_KUSH_DOC
		eRequestArray[1+1-1] = PR_SCENE_F_KUSH_DOC_a			fPercentArray[1+1-1] = fCHANCE_F_KUSH_DOC_a
		eRequestArray[2+1-1] = PR_SCENE_F_KUSH_DOC_b			fPercentArray[2+1-1] = fCHANCE_F_KUSH_DOC_b
		eRequestArray[3+1-1] = PR_SCENE_F_KUSH_DOC_c			fPercentArray[3+1-1] = fCHANCE_F_KUSH_DOC_c
		eRequestArray[4+1-1] = PR_SCENE_F0_GARBAGE				fPercentArray[4+1-1] = fCHANCE_F0_GARBAGE
		eRequestArray[5+1-1] = PR_SCENE_F_THROW_CUP				fPercentArray[5+1-1] = fCHANCE_F_THROW_CUP
		eRequestArray[6+1-1] = PR_SCENE_F_HIT_CUP_HAND			fPercentArray[6+1-1] = fCHANCE_F_HIT_CUP_HAND
		eRequestArray[7+1-1] = PR_SCENE_F_TRAFFIC_a				fPercentArray[7+1-1] = fCHANCE_F_TRAFFIC_a
		eRequestArray[8+1-1] = PR_SCENE_F_TRAFFIC_b				fPercentArray[8+1-1] = fCHANCE_F_TRAFFIC_b
		eRequestArray[9+1-1] = PR_SCENE_F_TRAFFIC_c				fPercentArray[9+1-1] = fCHANCE_F_TRAFFIC_c
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE)
			eRequestArray[10+1-1] = PR_SCENE_F0_BIKE			fPercentArray[10+1-1] = fCHANCE_F0_BIKE
			eRequestArray[11+1-1] = PR_SCENE_F_BIKE_c			fPercentArray[11+1-1] = fCHANCE_F_BIKE_c
			eRequestArray[12+1-1] = PR_SCENE_F_BIKE_d			fPercentArray[12+1-1] = fCHANCE_F_BIKE_d
		ELSE
			eRequestArray[10+1-1] = PR_SCENE_INVALID			fPercentArray[10+1-1] = 0.0						
			eRequestArray[11+1-1] = PR_SCENE_INVALID			fPercentArray[11+1-1] = 0.0						
			eRequestArray[12+1-1] = PR_SCENE_INVALID			fPercentArray[12+1-1] = 0.0
		ENDIF
		
		eRequestArray[13+1-1] = PR_SCENE_F_GYM					fPercentArray[13+1-1] = fCHANCE_F_GYM
		eRequestArray[14+1-1] = PR_SCENE_F0_CLEANCAR			fPercentArray[14+1-1] = fCHANCE_F0_CLEANCAR
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4)
			eRequestArray[15+1-1] = PR_SCENE_F_CS_CHECKSHOE		fPercentArray[15+1-1] = fCHANCE_F_CS_CHECKSHOE
			eRequestArray[16+1-1] = PR_SCENE_F_CS_WIPEHANDS		fPercentArray[16+1-1] = fCHANCE_F_CS_WIPEHANDS
			eRequestArray[17+1-1] = PR_SCENE_F_CS_WIPERIGHT		fPercentArray[17+1-1] = fCHANCE_F_CS_WIPERIGHT
		ELSE
			eRequestArray[15+1-1] = PR_SCENE_INVALID			fPercentArray[15+1-1] = 0.0
			eRequestArray[16+1-1] = PR_SCENE_INVALID			fPercentArray[16+1-1] = 0.0
			eRequestArray[17+1-1] = PR_SCENE_INVALID			fPercentArray[17+1-1] = 0.0
		ENDIF
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED)
			eRequestArray[18+1-1] = PR_SCENE_F0_WALKCHOP		fPercentArray[18+1-1] = fCHANCE_F0_WALKCHOP
			eRequestArray[19+1-1] = PR_SCENE_F0_PLAYCHOP		fPercentArray[19+1-1] = fCHANCE_F0_PLAYCHOP
		ELSE
			eRequestArray[18+1-1] = PR_SCENE_INVALID			fPercentArray[18+1-1] = 0.0
			eRequestArray[19+1-1] = PR_SCENE_INVALID			fPercentArray[19+1-1] = 0.0
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ELSE
		PED_REQUEST_SCENE_ENUM	eRequestArray[29-2]
		FLOAT 					fPercentArray[29-2]
		
		CONST_FLOAT fCHANCE_F_MD_KUSH_DOC             	1.5
		CONST_FLOAT fCHANCE_F_KUSH_DOC_a             	1.5
		CONST_FLOAT fCHANCE_F_KUSH_DOC_b             	1.5
		CONST_FLOAT fCHANCE_F_KUSH_DOC_c             	1.5
		
		CONST_FLOAT fCHANCE_F1_GARBAGE        	     	4.5
		
		CONST_FLOAT fCHANCE_F_GYM						4.5
		CONST_FLOAT fCHANCE_F_TRAFFIC_a             	1.5
		CONST_FLOAT fCHANCE_F_TRAFFIC_b             	1.5
		CONST_FLOAT fCHANCE_F_TRAFFIC_c             	1.5
		CONST_FLOAT fCHANCE_F1_BIKE             		1.5
		CONST_FLOAT fCHANCE_F_BIKE_c            	 	1.5
		CONST_FLOAT fCHANCE_F_BIKE_d          	 	  	1.5
		
		CONST_FLOAT fCHANCE_F_CS_CHECKSHOE             	1.5		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPEHANDS             	1.5		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPERIGHT             	1.5		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F1_POOLSIDE_a             	6.5
		CONST_FLOAT fCHANCE_F1_POOLSIDE_b            	6.5
		CONST_FLOAT fCHANCE_F1_WALKCHOP         	   	1.5		//(only if chop app enabled)	
		CONST_FLOAT fCHANCE_F1_PLAYCHOP             	1.5		//(only if chop app enabled)
		
		CONST_FLOAT fCHANCE_F1_CLEANINGAPT             	8.5
		CONST_FLOAT fCHANCE_F1_ONCELL        	     	8.5
		
		CONST_FLOAT fCHANCE_F1_SNACKING             	8.5
		CONST_FLOAT fCHANCE_F1_ONLAPTOP             	8.5
		CONST_FLOAT fCHANCE_F1_CLEANCAR             	4.5
		CONST_FLOAT fCHANCE_F1_IRONING        	     	8.5
		
		CONST_FLOAT fCHANCE_F1_WATCHINGTV             	8.5
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING, bDescentOnlyScene,
				PR_SCENE_F1_NEWHOUSE)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
		ENDIF
		
		eRequestArray[0] = PR_SCENE_F_MD_KUSH_DOC		fPercentArray[0] = fCHANCE_F_MD_KUSH_DOC
		eRequestArray[0+1] = PR_SCENE_F_KUSH_DOC_a		fPercentArray[0+1] = fCHANCE_F_KUSH_DOC_a
		eRequestArray[1+1] = PR_SCENE_F_KUSH_DOC_b		fPercentArray[1+1] = fCHANCE_F_KUSH_DOC_b        
		eRequestArray[2+1] = PR_SCENE_F_KUSH_DOC_c		fPercentArray[2+1] = fCHANCE_F_KUSH_DOC_c        
		
		eRequestArray[3+1] = PR_SCENE_F1_GARBAGE			fPercentArray[3+1] = fCHANCE_F1_GARBAGE       
		eRequestArray[6-2+1] = PR_SCENE_F_GYM				fPercentArray[6-2+1] = fCHANCE_F_GYM				
		
		eRequestArray[7-2+1] = PR_SCENE_F_TRAFFIC_a			fPercentArray[7-2+1] = fCHANCE_F_TRAFFIC_a         
		eRequestArray[8-2+1] = PR_SCENE_F_TRAFFIC_b			fPercentArray[8-2+1] = fCHANCE_F_TRAFFIC_b         
		eRequestArray[9-2+1] = PR_SCENE_F_TRAFFIC_c			fPercentArray[9-2+1] = fCHANCE_F_TRAFFIC_c         
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE)
			eRequestArray[10-2+1] = PR_SCENE_F1_BIKE		fPercentArray[10-2+1] = fCHANCE_F1_BIKE             
			eRequestArray[11-2+1] = PR_SCENE_F_BIKE_c		fPercentArray[11-2+1] = fCHANCE_F_BIKE_c            
			eRequestArray[12-2+1] = PR_SCENE_F_BIKE_d		fPercentArray[12-2+1] = fCHANCE_F_BIKE_d
		ELSE
			eRequestArray[10-2+1] = PR_SCENE_INVALID		fPercentArray[10-2+1] = 0.0      
			eRequestArray[11-2+1] = PR_SCENE_INVALID		fPercentArray[11-2+1] = 0.0
			eRequestArray[12-2+1] = PR_SCENE_INVALID		fPercentArray[12-2+1] = 0.0
		ENDIF
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4)
			eRequestArray[13-2+1] = PR_SCENE_F_CS_CHECKSHOE	fPercentArray[13-2+1] = fCHANCE_F_CS_CHECKSHOE      
			eRequestArray[14-2+1] = PR_SCENE_F_CS_WIPEHANDS	fPercentArray[14-2+1] = fCHANCE_F_CS_WIPEHANDS      
			eRequestArray[15-2+1] = PR_SCENE_F_CS_WIPERIGHT	fPercentArray[15-2+1] = fCHANCE_F_CS_WIPERIGHT      
		ELSE
			eRequestArray[13-2+1] = PR_SCENE_INVALID		fPercentArray[13-2+1] = 0.0      
			eRequestArray[14-2+1] = PR_SCENE_INVALID		fPercentArray[14-2+1] = 0.0
			eRequestArray[15-2+1] = PR_SCENE_INVALID		fPercentArray[15-2+1] = 0.0
		ENDIF
		
		eRequestArray[16-2+1] = PR_SCENE_F1_POOLSIDE_a		fPercentArray[16-2+1] = fCHANCE_F1_POOLSIDE_a       
		eRequestArray[17-2+1] = PR_SCENE_F1_POOLSIDE_b		fPercentArray[17-2+1] = fCHANCE_F1_POOLSIDE_b       
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED)
//			eRequestArray[18-2+1] = PR_SCENE_F1_WALKCHOP	fPercentArray[18-2+1] = fCHANCE_F1_WALKCHOP         
			eRequestArray[19-2] = PR_SCENE_F1_PLAYCHOP		fPercentArray[19-2] = fCHANCE_F1_PLAYCHOP         
		ELSE
//			eRequestArray[18-2+1] = PR_SCENE_INVALID		fPercentArray[18-2+1] = 0.0
			eRequestArray[19-2] = PR_SCENE_INVALID			fPercentArray[19-2] = 0.0
		ENDIF
		
		eRequestArray[20-2] = PR_SCENE_F1_WATCHINGTV		fPercentArray[20-2] = fCHANCE_F1_WATCHINGTV       
		eRequestArray[21-2] = PR_SCENE_F1_SNACKING			fPercentArray[21-2] = fCHANCE_F1_SNACKING      
		eRequestArray[22-2] = PR_SCENE_F1_ONLAPTOP			fPercentArray[22-2] = fCHANCE_F1_ONLAPTOP         
		eRequestArray[23-2] = PR_SCENE_F1_CLEANCAR			fPercentArray[23-2] = fCHANCE_F1_CLEANCAR         
		eRequestArray[24-2] = PR_SCENE_F1_IRONING			fPercentArray[24-2] = fCHANCE_F1_IRONING        	
		eRequestArray[25-2] = PR_SCENE_F1_CLEANINGAPT		fPercentArray[25-2] = fCHANCE_F1_CLEANINGAPT       
		eRequestArray[26-2] = PR_SCENE_F1_ONCELL			fPercentArray[26-2] = fCHANCE_F1_ONCELL          
		
		eRequestArray[27-2] = PR_SCENE_F_THROW_CUP				fPercentArray[27-2] = 1.0
		eRequestArray[28-2] = PR_SCENE_F_HIT_CUP_HAND			fPercentArray[28-2] = 1.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ENDIF
ENDFUNC
FUNC BOOL GetFranklinSceneForMorningParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetFranklinSceneForMorning(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

FUNC BOOL GetFranklinSceneForAfternoon(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_FRANKLIN
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		PED_REQUEST_SCENE_ENUM	eRequestArray[28+3-1-1]
		FLOAT 					fPercentArray[28+3-1-1]
		
		CONST_FLOAT fCHANCE_F_TAUNT							1.5
		
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P1					3.8	//(ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P2					3.8	//(ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P3					3.8 //(ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P5					3.8	//(ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMGRAFF						6.0	//(ONLY UP TO JEWEL HEIST)
		
		CONST_FLOAT fCHANCE_F_TRAFFIC_a						5.0
		CONST_FLOAT fCHANCE_F_TRAFFIC_b						5.0
		CONST_FLOAT fCHANCE_F_TRAFFIC_c						5.0
		CONST_FLOAT fCHANCE_F0_BIKE							2.0
		CONST_FLOAT fCHANCE_F_BIKE_c						2.0
		CONST_FLOAT fCHANCE_F_BIKE_d						2.0
		
		CONST_FLOAT fCHANCE_F_MD_KUSH_DOC					2.0
		CONST_FLOAT fCHANCE_F_KUSH_DOC_a					2.0
		CONST_FLOAT fCHANCE_F_KUSH_DOC_b					2.0
		CONST_FLOAT fCHANCE_F_KUSH_DOC_c					2.0
		
		CONST_FLOAT fCHANCE_F0_GARBAGE						6.0
		CONST_FLOAT fCHANCE_F_THROW_CUP						2.0
		CONST_FLOAT fCHANCE_F_HIT_CUP_HAND					2.0
		
		CONST_FLOAT fCHANCE_F0_WATCHINGTV					3.0
		CONST_FLOAT fCHANCE_F0_CLEANCAR						6.0
		
		CONST_FLOAT fCHANCE_F_BAR_a_01						1.5
		CONST_FLOAT fCHANCE_F_BAR_b_01						1.5
		CONST_FLOAT fCHANCE_F_BAR_c_02						1.5
		CONST_FLOAT fCHANCE_F_BAR_d_02						1.5
		CONST_FLOAT fCHANCE_F_BAR_e_01						1.5
		
		CONST_FLOAT fCHANCE_F_CS_CHECKSHOE					6.0	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5-1]
		CONST_FLOAT fCHANCE_F_CS_WIPEHANDS					5.0	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5-1]
		CONST_FLOAT fCHANCE_F_CS_WIPERIGHT					6.0	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5-1]
		
		CONST_FLOAT fCHANCE_F0_WALKCHOP						6.0	//(only if chop app enabled)
		CONST_FLOAT fCHANCE_F0_PLAYCHOP						6.0	//(only if chop app enabled)
		
//		eRequestArray[0-1] = PR_SCENE_F_TAUNT					fPercentArray[0-1] = fCHANCE_F_TAUNT
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LAMAR)
			eRequestArray[1-1] = PR_SCENE_F_LAMTAUNT_P1		fPercentArray[1-1] = fCHANCE_F_LAMTAUNT_P1
//			eRequestArray[2-1] = PR_SCENE_F_LAMTAUNT_P2		fPercentArray[2-1] = fCHANCE_F_LAMTAUNT_P2
			eRequestArray[3-1-1] = PR_SCENE_F_LAMTAUNT_P3	fPercentArray[3-1-1] = fCHANCE_F_LAMTAUNT_P3
			eRequestArray[4-1-1] = PR_SCENE_F_LAMTAUNT_P5	fPercentArray[4-1-1] = fCHANCE_F_LAMTAUNT_P5
//			eRequestArray[5-1-1] = PR_SCENE_F_LAMGRAFF		fPercentArray[5-1-1] = fCHANCE_F_LAMGRAFF
			
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
					PR_SCENE_F_LAMTAUNT_P1, //PR_SCENE_F_LAMTAUNT_P2,
					PR_SCENE_F_LAMTAUNT_P3, PR_SCENE_F_LAMTAUNT_P5,
//					PR_SCENE_F_LAMGRAFF,
					PR_SCENE_F0_TANISHAFIGHT)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
			ENDIF
		ELSE
			eRequestArray[1-1] = PR_SCENE_INVALID			fPercentArray[1-1] = 0.0						
//			eRequestArray[2-1] = PR_SCENE_INVALID			fPercentArray[2-1] = 0.0						
			eRequestArray[3-1-1] = PR_SCENE_INVALID			fPercentArray[3-1-1] = 0.0
			eRequestArray[4-1-1] = PR_SCENE_INVALID			fPercentArray[4-1-1] = 0.0						
			eRequestArray[5-1-1] = PR_SCENE_INVALID			fPercentArray[5-1-1] = 0.0
			
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
					PR_SCENE_F0_TANISHAFIGHT)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
			ENDIF
		ENDIF
		
		eRequestArray[6-1-1] = PR_SCENE_F_TRAFFIC_a			fPercentArray[6-1-1] = fCHANCE_F_TRAFFIC_a						
		eRequestArray[7-1-1] = PR_SCENE_F_TRAFFIC_b			fPercentArray[7-1-1] = fCHANCE_F_TRAFFIC_b						
		eRequestArray[8-1-1] = PR_SCENE_F_TRAFFIC_c			fPercentArray[8-1-1] = fCHANCE_F_TRAFFIC_c						
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE)
			eRequestArray[9-1-1] = PR_SCENE_F0_BIKE			fPercentArray[9-1-1] = fCHANCE_F0_BIKE						
			eRequestArray[10-1-1] = PR_SCENE_F_BIKE_c			fPercentArray[10-1-1] = fCHANCE_F_BIKE_c
			eRequestArray[11-1-1] = PR_SCENE_F_BIKE_d			fPercentArray[11-1-1] = fCHANCE_F_BIKE_d
		ELSE
			eRequestArray[9-1-1] = PR_SCENE_INVALID			fPercentArray[9-1-1] = 0.0
			eRequestArray[10-1-1] = PR_SCENE_INVALID			fPercentArray[10-1-1] = 0.0
			eRequestArray[11-1-1] = PR_SCENE_INVALID			fPercentArray[11-1-1] = 0.0
		ENDIF
		
		eRequestArray[12-1-1] = PR_SCENE_F_MD_KUSH_DOC			fPercentArray[12-1-1] = fCHANCE_F_MD_KUSH_DOC
		eRequestArray[12-1] = PR_SCENE_F_KUSH_DOC_a			fPercentArray[12-1] = fCHANCE_F_KUSH_DOC_a
		eRequestArray[13-1] = PR_SCENE_F_KUSH_DOC_b			fPercentArray[13-1] = fCHANCE_F_KUSH_DOC_b
		eRequestArray[14-1] = PR_SCENE_F_KUSH_DOC_c			fPercentArray[14-1] = fCHANCE_F_KUSH_DOC_c
		
		eRequestArray[15-1] = PR_SCENE_F0_GARBAGE				fPercentArray[15-1] = fCHANCE_F0_GARBAGE
		eRequestArray[16-1] = PR_SCENE_F_THROW_CUP			fPercentArray[16-1] = fCHANCE_F_THROW_CUP
		eRequestArray[17-1] = PR_SCENE_F_HIT_CUP_HAND		fPercentArray[17-1] = fCHANCE_F_HIT_CUP_HAND
		
//		eRequestArray[18-1] = PR_SCENE_F0_WATCHINGTV		fPercentArray[18-1] = fCHANCE_F0_WATCHINGTV
		eRequestArray[19-1-1] = PR_SCENE_F0_CLEANCAR			fPercentArray[19-1-1] = fCHANCE_F0_CLEANCAR
		
		eRequestArray[20-1-1] = PR_SCENE_F_BAR_a_01			fPercentArray[20-1-1] = fCHANCE_F_BAR_a_01
		eRequestArray[22+1-3-1] = PR_SCENE_F_BAR_b_01			fPercentArray[22+1-3-1] = fCHANCE_F_BAR_b_01
		eRequestArray[23+1-3-1] = PR_SCENE_F_BAR_c_02			fPercentArray[23+1-3-1] = fCHANCE_F_BAR_c_02
		eRequestArray[24+1-3-1] = PR_SCENE_F_BAR_d_02			fPercentArray[24+1-3-1] = fCHANCE_F_BAR_d_02
		eRequestArray[25+1-3-1] = PR_SCENE_F_BAR_e_01			fPercentArray[25+1-3-1] = fCHANCE_F_BAR_e_01
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4)
			eRequestArray[23+3+1-3-1] = PR_SCENE_F_CS_CHECKSHOE		fPercentArray[23+3+1-3-1] = fCHANCE_F_CS_CHECKSHOE
			eRequestArray[24+3+1-3-1] = PR_SCENE_F_CS_WIPEHANDS		fPercentArray[24+3+1-3-1] = fCHANCE_F_CS_WIPEHANDS
			eRequestArray[25+3+1-3-1] = PR_SCENE_F_CS_WIPERIGHT		fPercentArray[25+3+1-3-1] = fCHANCE_F_CS_WIPERIGHT
		ELSE
			eRequestArray[23+3+1-3-1] = PR_SCENE_INVALID			fPercentArray[23+3+1-3-1] = 0.0
			eRequestArray[24+3+1-3-1] = PR_SCENE_INVALID			fPercentArray[24+3+1-3-1] = 0.0
			eRequestArray[25+3+1-3-1] = PR_SCENE_INVALID			fPercentArray[25+3+1-3-1] = 0.0
		ENDIF
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED)
			eRequestArray[26+3+1-3-1] = PR_SCENE_F0_WALKCHOP		fPercentArray[26+3+1-3-1] = fCHANCE_F0_WALKCHOP
			eRequestArray[27+3+1-3-1] = PR_SCENE_F0_PLAYCHOP		fPercentArray[27+3+1-3-1] = fCHANCE_F0_PLAYCHOP
		ELSE
			eRequestArray[26+3+1-3-1] = PR_SCENE_INVALID			fPercentArray[26+3+1-3-1] = 0.0
			eRequestArray[27+3+1-3-1] = PR_SCENE_INVALID			fPercentArray[27+3+1-3-1] = 0.0
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ELSE
		PED_REQUEST_SCENE_ENUM	eRequestArray[33-2-1]
		FLOAT 					fPercentArray[33-2-1]
		
		CONST_FLOAT fCHANCE_F_GYM							4.3
		CONST_FLOAT fCHANCE_F_TRAFFIC_a						1.4
		CONST_FLOAT fCHANCE_F_TRAFFIC_b						1.4
		CONST_FLOAT fCHANCE_F_TRAFFIC_c						1.4
		
		CONST_FLOAT fCHANCE_F1_BIKE							1.4
		CONST_FLOAT fCHANCE_F_BIKE_c						1.4
		CONST_FLOAT fCHANCE_F_BIKE_d						1.4
		
		CONST_FLOAT fCHANCE_F_MD_KUSH_DOC					4.3
		CONST_FLOAT fCHANCE_F_KUSH_DOC_a					4.3
		CONST_FLOAT fCHANCE_F_KUSH_DOC_b					4.3
		CONST_FLOAT fCHANCE_F_KUSH_DOC_c					4.3
		CONST_FLOAT fCHANCE_F1_GARBAGE						4.3
		CONST_FLOAT fCHANCE_F_THROW_CUP						2.15
		CONST_FLOAT fCHANCE_F_HIT_CUP_HAND					2.15
		
		CONST_FLOAT fCHANCE_F_CS_CHECKSHOE					1.4	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPEHANDS					1.4	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPERIGHT					1.4	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		
		CONST_FLOAT fCHANCE_F_BAR_a_01						7.0
		CONST_FLOAT fCHANCE_F_BAR_b_01						7.0
		CONST_FLOAT fCHANCE_F_BAR_c_02						7.0
		CONST_FLOAT fCHANCE_F_BAR_d_02						7.0
		CONST_FLOAT fCHANCE_F_BAR_e_01						7.0
		
		CONST_FLOAT fCHANCE_F1_WALKCHOP						4.3	//(only if chop app enabled)
		CONST_FLOAT fCHANCE_F1_PLAYCHOP						4.3	//(only if chop app enabled)
		
		CONST_FLOAT fCHANCE_F1_WATCHINGTV					8.3
		CONST_FLOAT fCHANCE_F1_SNACKING						8.3
		CONST_FLOAT fCHANCE_F1_ONLAPTOP						8.3
		CONST_FLOAT fCHANCE_F1_CLEANCAR						8.3
		CONST_FLOAT fCHANCE_F1_IRONING						8.3
		CONST_FLOAT fCHANCE_F1_CLEANINGAPT					8.3
		CONST_FLOAT fCHANCE_F1_ONCELL						8.2
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
				PR_SCENE_F1_NEWHOUSE)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
		ENDIF
		
		eRequestArray[0] = PR_SCENE_F_GYM					fPercentArray[0] = fCHANCE_F_GYM			
		eRequestArray[1] = PR_SCENE_F_TRAFFIC_a				fPercentArray[1] = fCHANCE_F_TRAFFIC_a		
		eRequestArray[2] = PR_SCENE_F_TRAFFIC_b				fPercentArray[2] = fCHANCE_F_TRAFFIC_b		
		eRequestArray[3] = PR_SCENE_F_TRAFFIC_c				fPercentArray[3] = fCHANCE_F_TRAFFIC_c		
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE)
			eRequestArray[4] = PR_SCENE_F1_BIKE				fPercentArray[4] = fCHANCE_F1_BIKE		
			eRequestArray[5] = PR_SCENE_F_BIKE_c			fPercentArray[5] = fCHANCE_F_BIKE_c		
			eRequestArray[6] = PR_SCENE_F_BIKE_d			fPercentArray[6] = fCHANCE_F_BIKE_d		
		ELSE
			eRequestArray[4] = PR_SCENE_INVALID				fPercentArray[4] = 0.0
			eRequestArray[5] = PR_SCENE_INVALID				fPercentArray[5] = 0.0
			eRequestArray[6] = PR_SCENE_INVALID				fPercentArray[6] = 0.0
		ENDIF
		
		eRequestArray[7] = PR_SCENE_F_MD_KUSH_DOC			fPercentArray[7] = fCHANCE_F_MD_KUSH_DOC
		eRequestArray[7+1] = PR_SCENE_F_KUSH_DOC_a			fPercentArray[7+1] = fCHANCE_F_KUSH_DOC_a	
		eRequestArray[8+1] = PR_SCENE_F_KUSH_DOC_b			fPercentArray[8+1] = fCHANCE_F_KUSH_DOC_b	
		eRequestArray[9+1] = PR_SCENE_F_KUSH_DOC_c			fPercentArray[9+1] = fCHANCE_F_KUSH_DOC_c	
		eRequestArray[10+1] = PR_SCENE_F1_GARBAGE			fPercentArray[10+1] = fCHANCE_F1_GARBAGE	
		eRequestArray[11+1] = PR_SCENE_F_THROW_CUP			fPercentArray[11+1] = fCHANCE_F_THROW_CUP	
		eRequestArray[12+1] = PR_SCENE_F_HIT_CUP_HAND		fPercentArray[12+1] = fCHANCE_F_HIT_CUP_HAND	
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4)
			eRequestArray[13+1] = PR_SCENE_F_CS_CHECKSHOE		fPercentArray[13+1] = fCHANCE_F_CS_CHECKSHOE	
			eRequestArray[14+1] = PR_SCENE_F_CS_WIPEHANDS		fPercentArray[14+1] = fCHANCE_F_CS_WIPEHANDS	
			eRequestArray[15+1] = PR_SCENE_F_CS_WIPERIGHT		fPercentArray[15+1] = fCHANCE_F_CS_WIPERIGHT		
		ELSE
			eRequestArray[13+1] = PR_SCENE_INVALID			fPercentArray[13+1] = 0.0
			eRequestArray[14+1] = PR_SCENE_INVALID			fPercentArray[14+1] = 0.0
			eRequestArray[15+1] = PR_SCENE_INVALID			fPercentArray[15+1] = 0.0
		ENDIF
		
		eRequestArray[16+1] = PR_SCENE_F_BAR_a_01				fPercentArray[16+1] = fCHANCE_F_BAR_a_01		
		eRequestArray[18] = PR_SCENE_F_BAR_b_01				fPercentArray[18] = fCHANCE_F_BAR_b_01		
		eRequestArray[19] = PR_SCENE_F_BAR_c_02				fPercentArray[19] = fCHANCE_F_BAR_c_02		
		eRequestArray[20] = PR_SCENE_F_BAR_d_02				fPercentArray[20] = fCHANCE_F_BAR_d_02
		eRequestArray[21] = PR_SCENE_F_BAR_e_01				fPercentArray[21] = fCHANCE_F_BAR_e_01
		
		IF Get_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED)
//			eRequestArray[24-2] = PR_SCENE_F1_WALKCHOP		fPercentArray[24-2] = fCHANCE_F1_WALKCHOP		
			eRequestArray[25-2-1] = PR_SCENE_F1_PLAYCHOP		fPercentArray[25-2-1] = fCHANCE_F1_PLAYCHOP		
		ELSE
//			eRequestArray[24-2-1] = PR_SCENE_INVALID			fPercentArray[24-2-1] = 0.0
			eRequestArray[25-2-1] = PR_SCENE_INVALID			fPercentArray[25-2-1] = 0.0
		ENDIF
		
		eRequestArray[26-2-1] = PR_SCENE_F1_WATCHINGTV		fPercentArray[26-2-1] = fCHANCE_F1_WATCHINGTV	
		eRequestArray[27-2-1] = PR_SCENE_F1_SNACKING			fPercentArray[27-2-1] = fCHANCE_F1_SNACKING	
		eRequestArray[28-2-1] = PR_SCENE_F1_ONLAPTOP			fPercentArray[28-2-1] = fCHANCE_F1_ONLAPTOP		
		eRequestArray[29-2-1] = PR_SCENE_F1_CLEANCAR			fPercentArray[29-2-1] = fCHANCE_F1_CLEANCAR		
		eRequestArray[28+2-2-1] = PR_SCENE_F1_IRONING			fPercentArray[28+2-2-1] = fCHANCE_F1_IRONING		
		eRequestArray[29+2-2-1] = PR_SCENE_F1_CLEANINGAPT		fPercentArray[29+2-2-1] = fCHANCE_F1_CLEANINGAPT	
		eRequestArray[32-2-1] = PR_SCENE_F1_ONCELL			fPercentArray[32-2-1] = fCHANCE_F1_ONCELL		
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ENDIF
ENDFUNC
FUNC BOOL GetFranklinSceneForAfternoonParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetFranklinSceneForAfternoon(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

FUNC BOOL GetFranklinSceneForEvening(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		SceneScheduleFunc invalidSceneScheduleFunc, BOOL bDescentOnlyScene)
	enumCharacterList ePed = CHAR_FRANKLIN
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		PED_REQUEST_SCENE_ENUM	eRequestArray[17]
		FLOAT 					fPercentArray[17]
		
		CONST_FLOAT fCHANCE_F_CLUB						10.0
		
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P1				1.0		//ONLY UP TO JEWEL HEIST)
//		CONST_FLOAT fCHANCE_F_LAMTAUNT_P2				1.0		//ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P3				1.0		//ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMTAUNT_P5				1.0		//ONLY UP TO JEWEL HEIST)
		CONST_FLOAT fCHANCE_F_LAMTAUNT_NIGHT			5.0		//ONLY UP TO JEWEL HEIST)
		
		CONST_FLOAT fCHANCE_F_BAR_a_01					1.0
		CONST_FLOAT fCHANCE_F_BAR_b_01					1.0
		CONST_FLOAT fCHANCE_F_BAR_c_02					1.0
		CONST_FLOAT fCHANCE_F_BAR_d_02					1.0
		CONST_FLOAT fCHANCE_F_BAR_e_01					0.98
		
		CONST_FLOAT fCHANCE_F0_WATCHINGTV				20.0
		
		CONST_FLOAT fCHANCE_F_CS_CHECKSHOE				5.0		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPEHANDS				5.0		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPERIGHT				5.0		//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		
		//B*-2351523
		CONST_FLOAT fCHANCE_F_HIT_CUP_HAND				0.01
		CONST_FLOAT fCHANCE_F_TRAFFIC					0.01
		//B*-2351523
		
		eRequestArray[0] = PR_SCENE_F_CLUB				fPercentArray[0] = fCHANCE_F_CLUB			
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LAMAR)
			eRequestArray[1] = PR_SCENE_F_LAMTAUNT_P1		fPercentArray[1] = fCHANCE_F_LAMTAUNT_P1	
//			eRequestArray[2] = PR_SCENE_F_LAMTAUNT_P2		fPercentArray[2] = fCHANCE_F_LAMTAUNT_P2	
			eRequestArray[2] = PR_SCENE_F_LAMTAUNT_P3		fPercentArray[2] = fCHANCE_F_LAMTAUNT_P3	
			eRequestArray[3] = PR_SCENE_F_LAMTAUNT_P5		fPercentArray[3] = fCHANCE_F_LAMTAUNT_P5
			eRequestArray[4] = PR_SCENE_F_LAMTAUNT_NIGHT	fPercentArray[4] = fCHANCE_F_LAMTAUNT_NIGHT
			
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
					PR_SCENE_F_LAMTAUNT_P1, //PR_SCENE_F_LAMTAUNT_P2,
					PR_SCENE_F_LAMTAUNT_P3, PR_SCENE_F_LAMTAUNT_P5,
					PR_SCENE_F_LAMTAUNT_NIGHT,
					PR_SCENE_F0_TANISHAFIGHT)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
			ENDIF
		ELSE
			eRequestArray[1] = PR_SCENE_INVALID			fPercentArray[1] = 0.0
//			eRequestArray[2] = PR_SCENE_INVALID			fPercentArray[2] = 0.0
			eRequestArray[2] = PR_SCENE_INVALID			fPercentArray[2] = 0.0
			eRequestArray[3] = PR_SCENE_INVALID			fPercentArray[3] = 0.0
			eRequestArray[4] = PR_SCENE_INVALID			fPercentArray[4] = 0.0
			
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
					PR_SCENE_F0_TANISHAFIGHT)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
			ENDIF

		ENDIF
		
		eRequestArray[5] = PR_SCENE_F_BAR_a_01			fPercentArray[5] = fCHANCE_F_BAR_a_01		
		eRequestArray[6] = PR_SCENE_F_BAR_b_01			fPercentArray[6] = fCHANCE_F_BAR_b_01		
		eRequestArray[7] = PR_SCENE_F_BAR_c_02			fPercentArray[7] = fCHANCE_F_BAR_c_02		
		eRequestArray[8] = PR_SCENE_F_BAR_d_02			fPercentArray[8] = fCHANCE_F_BAR_d_02		
		eRequestArray[9] = PR_SCENE_F_BAR_e_01			fPercentArray[9] = fCHANCE_F_BAR_e_01
		
//		eRequestArray[12-1-1] = PR_SCENE_F0_WATCHINGTV		fPercentArray[12-1-1] = fCHANCE_F0_WATCHINGTV	
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4)
			eRequestArray[10] = PR_SCENE_F_CS_CHECKSHOE	fPercentArray[10] = fCHANCE_F_CS_CHECKSHOE						
			eRequestArray[11] = PR_SCENE_F_CS_WIPEHANDS	fPercentArray[11] = fCHANCE_F_CS_WIPEHANDS						
			eRequestArray[12] = PR_SCENE_F_CS_WIPERIGHT	fPercentArray[12] = fCHANCE_F_CS_WIPERIGHT
		ELSE
			eRequestArray[10] = PR_SCENE_INVALID		fPercentArray[10] = 0.0						
			eRequestArray[11] = PR_SCENE_INVALID		fPercentArray[11] = 0.0						
			eRequestArray[12] = PR_SCENE_INVALID		fPercentArray[12] = 0.0
		ENDIF
		
		//B*-2351523
		eRequestArray[13] = PR_SCENE_F_HIT_CUP_HAND		fPercentArray[13] = fCHANCE_F_HIT_CUP_HAND	
		eRequestArray[14] = PR_SCENE_F_TRAFFIC_c		fPercentArray[14] = fCHANCE_F_TRAFFIC
		eRequestArray[15] = PR_SCENE_F_TRAFFIC_a		fPercentArray[15] = fCHANCE_F_TRAFFIC	
		eRequestArray[16] = PR_SCENE_F_TRAFFIC_b		fPercentArray[16] = fCHANCE_F_TRAFFIC
		//B*-2351523
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ELSE
		PED_REQUEST_SCENE_ENUM	eRequestArray[13]
		FLOAT 					fPercentArray[13]
		
		CONST_FLOAT fCHANCE_F_CLUB						14.2
		CONST_FLOAT fCHANCE_F_CS_CHECKSHOE				4.7	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPEHANDS				4.7	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		CONST_FLOAT fCHANCE_F_CS_WIPERIGHT				4.7	//[ONLY AFTER PASSED CarSteal1 UNTIL PASSED CS5]
		
		CONST_FLOAT fCHANCE_F_BAR_a_01					13.5
		CONST_FLOAT fCHANCE_F_BAR_b_01					13.5
		CONST_FLOAT fCHANCE_F_BAR_c_02					13.5
		CONST_FLOAT fCHANCE_F_BAR_d_02					13.5
		CONST_FLOAT fCHANCE_F_BAR_e_01					13.3
		
		//B*-2351523
		CONST_FLOAT fCHANCE_F_HIT_CUP_HAND				0.01
		CONST_FLOAT fCHANCE_F_TRAFFIC					0.01
		//B*-2351523
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
				PR_SCENE_F1_NEWHOUSE)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
		ENDIF
		
		eRequestArray[0] = PR_SCENE_F_CLUB				fPercentArray[0] = fCHANCE_F_CLUB			
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4)
			eRequestArray[1] = PR_SCENE_F_CS_CHECKSHOE	fPercentArray[1] = fCHANCE_F_CS_CHECKSHOE	
			eRequestArray[2] = PR_SCENE_F_CS_WIPEHANDS	fPercentArray[2] = fCHANCE_F_CS_WIPEHANDS	
			eRequestArray[3] = PR_SCENE_F_CS_WIPERIGHT	fPercentArray[3] = fCHANCE_F_CS_WIPERIGHT	
		ELSE
			eRequestArray[1] = PR_SCENE_INVALID			fPercentArray[1] = 0.0
			eRequestArray[2] = PR_SCENE_INVALID			fPercentArray[2] = 0.0
			eRequestArray[3] = PR_SCENE_INVALID			fPercentArray[3] = 0.0
		ENDIF
		
		eRequestArray[4] = PR_SCENE_F_BAR_a_01			fPercentArray[4] = fCHANCE_F_BAR_a_01		
		eRequestArray[5] = PR_SCENE_F_BAR_b_01			fPercentArray[5] = fCHANCE_F_BAR_b_01		
		eRequestArray[6] = PR_SCENE_F_BAR_c_02			fPercentArray[6] = fCHANCE_F_BAR_c_02		
		eRequestArray[7] = PR_SCENE_F_BAR_d_02			fPercentArray[7] = fCHANCE_F_BAR_d_02		
		eRequestArray[8] = PR_SCENE_F_BAR_e_01			fPercentArray[8] = fCHANCE_F_BAR_e_01		
		
		//B*-2351523
		eRequestArray[9] = PR_SCENE_F_HIT_CUP_HAND		fPercentArray[9] = fCHANCE_F_HIT_CUP_HAND	
		eRequestArray[10] = PR_SCENE_F_TRAFFIC_c		fPercentArray[10] = fCHANCE_F_TRAFFIC
		eRequestArray[11] = PR_SCENE_F_TRAFFIC_a		fPercentArray[11] = fCHANCE_F_TRAFFIC	
		eRequestArray[12] = PR_SCENE_F_TRAFFIC_b		fPercentArray[12] = fCHANCE_F_TRAFFIC
		//B*-2351523
		
		IF GetDesiredPedRequestSceneFromArray(ePed,
				eRequestArray, fPercentArray,
				eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ELSE
			RETURN CALL invalidSceneScheduleFunc(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		ENDIF
	ENDIF
ENDFUNC
FUNC BOOL GetFranklinSceneForEveningParam(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	RETURN GetFranklinSceneForEvening(eReqScene, fScenePercent, sPedScene, sPassedScene, &BlankSceneScheduleFunc, bDescentOnlyScene)
ENDFUNC

/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL GetFranklinSceneForCurrentTime(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
//	enumCharacterList ePed = CHAR_FRANKLIN
	eReqScene = PR_SCENE_INVALID		fScenePercent = -1
	
	IF NOT bDescentOnlyScene
		IF GetPedRequestSceneForAvailableMission(CHAR_FRANKLIN, eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	
	SWITCH PRIVATE_Get_Switch_Clock_Hours()
		/*	03:00 - 08:59	*/
		CASE 3 CASE 4 CASE 5
		CASE 6 CASE 7 CASE 8
			/* 100% */
			GetFranklinSceneForNight(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetFranklinSceneForMorningParam, bDescentOnlyScene)
		BREAK
		
		/*	09:00 - 14:59	*/
		CASE 9 CASE 10 CASE 11
		CASE 12 CASE 13 CASE 14
			GetFranklinSceneForMorning(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetFranklinSceneForAfternoonParam, bDescentOnlyScene)
		BREAK
		
		/*	15:00 - 20:59	*/
		CASE 15 CASE 16 CASE 17
		CASE 18 CASE 19 CASE 20
			GetFranklinSceneForAfternoon(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetFranklinSceneForEveningParam, bDescentOnlyScene)
		BREAK
		
		/*	21:00 - 02:59	*/
		CASE 21 CASE 22 CASE 23
		CASE 0 CASE 1 CASE 2
			GetFranklinSceneForEvening(eReqScene, fScenePercent, sPedScene, sPassedScene,
					&GetFranklinSceneForNightParam, bDescentOnlyScene)
		BREAK
	ENDSWITCH
	
	RETURN (eReqScene <> PR_SCENE_INVALID)
ENDFUNC


// *******************************************************************************************
//	FUNCTIONS TO RETURN NEXT SCENE FOR TREVOR
// *******************************************************************************************

CONST_INT iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES 57		//58	//60	//52

/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL FillSlotInTrevorSceneArray(PED_REQUEST_SCENE_ENUM eReqScene, FLOAT fScenePercent,
		PED_REQUEST_SCENE_ENUM &eRequestArray[], FLOAT &fPercentArray[],
		INT &iSceneIndex)
	
	IF (iSceneIndex) >= COUNT_OF(eRequestArray)
		CASSERTLN(DEBUG_SWITCH, "Bug for Alwyn - non-fatal array overrun in FillSlotInTrevorSceneArray()")
		RETURN FALSE
	ENDIF
	
	eRequestArray[iSceneIndex]	= eReqScene
	fPercentArray[iSceneIndex]	= fScenePercent
	iSceneIndex++
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL FillSlotInTrevorSceneCrazyArray(INT &iScene,
		PED_REQUEST_SCENE_ENUM eReqScene, PED_REQUEST_SCENE_ENUM &eRequestArray[])
	
	IF (iScene) >= COUNT_OF(eRequestArray)
		CASSERTLN(DEBUG_SWITCH, "Bug for Alwyn - non-fatal array overrun in FillSlotInTrevorSceneCrazyArray()")
		RETURN FALSE
	ENDIF
	
	eRequestArray[iScene]	= eReqScene
	iScene++
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL FillTrevorCrazyCountrySceneArray(INT &iSceneIndex, FLOAT fTOTAL_PERCENT,
		PED_REQUEST_SCENE_ENUM &eRequestArray[], FLOAT &fPercentArray[])
	INT iStart = iSceneIndex
	
	SWITCH PRIVATE_Get_Switch_Clock_Hours()
		/*	00:00 - 5:59	*/
		CASE 0 CASE 1 CASE 2
		CASE 3 CASE 4 CASE 5
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_CHATEAU_b, eRequestArray)
			
		BREAK
		
		/*	06:00 - 14:59	*/
		CASE 6 CASE 7 CASE 8
		CASE 9 CASE 10 CASE 11
		CASE 12 CASE 13 CASE 14
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_CHATEAU_c, eRequestArray)
			
		BREAK
		
		/*	15:00 - 23:59	*/
		CASE 15 CASE 16 CASE 17
		CASE 18 CASE 19 CASE 20
		CASE 21 CASE 22 CASE 23
			//
			
		BREAK
	ENDSWITCH
	
	IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_BAR_HEN_HOUSE) <> CHAR_TREVOR
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FIGHTBAR_c, eRequestArray)
	ENDIF
	
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_PARK_b, eRequestArray)
//	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_PARK_c, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_WAKETRASH_b, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_WAKEBARN, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_WAKETRAIN, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_WAKEMOUNTAIN, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_POLICE_b, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_POLICE_c, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_CHASECAR_b, eRequestArray)
	
	IF DoesPlayerPedHaveWeaponStored(CHAR_TREVOR, WEAPONTYPE_PISTOL)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CN_PIER, eRequestArray)
	ENDIF
	
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_2)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FLYING_PLANE, eRequestArray)
	ENDIF
	
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T6_LAKE, eRequestArray)
//	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T6_DISPOSEBODY_A, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T6_DIGGING, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T6_FLUSHESFOOT, eRequestArray)
	
	INT iSceneCount
	FOR iSceneCount = iStart TO (COUNT_OF(fPercentArray) - 1)
		IF (eRequestArray[iSceneCount] = INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, 0))
			eRequestArray[iSceneCount] = PR_SCENE_INVALID
		ENDIF
		
		IF (eRequestArray[iSceneCount] <> PR_SCENE_INVALID)
			fPercentArray[iSceneCount] = fTOTAL_PERCENT / TO_FLOAT(iSceneIndex)
		ELSE
			fPercentArray[iSceneCount] = 0.0
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL FillTrevorCrazyCitySceneArray(INT &iSceneIndex, FLOAT fTOTAL_PERCENT,
		PED_REQUEST_SCENE_ENUM &eRequestArray[], FLOAT &fPercentArray[])
	INT iStart = iSceneIndex
	
	INT iPRIVATE_Get_Switch_Clock_Hours = PRIVATE_Get_Switch_Clock_Hours()
	
	SWITCH iPRIVATE_Get_Switch_Clock_Hours
		/*	00:00 - 5:59	*/
		CASE 0 CASE 1 CASE 2
		CASE 3 CASE 4 CASE 5
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_ESCORTED_OUT, eRequestArray)
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_CHATEAU_d, eRequestArray)
			
		BREAK
		
		/*	06:00 - 14:59	*/
		CASE 6 CASE 7 CASE 8
		CASE 9 CASE 10 CASE 11
		CASE 12 CASE 13 CASE 14
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FIGHTBBUILD, eRequestArray)
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_ANNOYSUNBATHERS, eRequestArray)
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_BLOCK_CAMERA, eRequestArray)
			FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_GUITARBEATDOWN, eRequestArray)
			
		BREAK
		
		/*	15:00 - 23:59	*/
		CASE 15 CASE 16 CASE 17
		CASE 18 CASE 19 CASE 20
		CASE 21 CASE 22 CASE 23
			
			IF (iPRIVATE_Get_Switch_Clock_Hours = 18)		//PR_SCENE_T_CR_RAND_TEMPLE in the sunset (1232107)
			OR (iPRIVATE_Get_Switch_Clock_Hours = 19)		//
				FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_RAND_TEMPLE, eRequestArray)
				FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_RAND_TEMPLE, eRequestArray)
			ELSE
				FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_LINGERIE, eRequestArray)
				FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_FUNERAL, eRequestArray)
			ENDIF
			
		BREAK
	ENDSWITCH
	
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_GARBAGE_FOOD, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_THROW_FOOD, eRequestArray)
	
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FIGHTBAR_a, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FIGHTBAR_b, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_YELLATDOORMAN, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FIGHTYAUCLUB_b, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FIGHTCASINO, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_SCARETRAMP, eRequestArray)
	
	IF NOT IS_MISSION_AVAILABLE(SP_MISSION_CARSTEAL_1)			//1203682
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_DUMPSTER, eRequestArray)
	ENDIF
	
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_WAKEBEACH, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_WAKEROOFTOP, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_ALLEYDRUNK, eRequestArray)
	
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_4)			//1403370
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_SC)	//1450306
				FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_SC_ALLEYDRUNK, eRequestArray)
				FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_SC_DRUNKHOWLING, eRequestArray)
			ENDIF
		#endif
		#endif
	ENDIF
	
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_PUKEINTOFOUNT, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_POLICE_a, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_NAKED_BRIDGE, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_NAKED_GARDEN, eRequestArray)
//	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_NAKED_ISLAND, eRequestArray)		//only post-Fam6
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_CHASECAR_a, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_CHASEBIKE, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_CHASESCOOTER, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_BRIDGEDROP, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_CR_RUDEATCAFE, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_UNDERPIER, eRequestArray)
	FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_DRUNKHOWLING, eRequestArray)
	
	IF NOT GET_MISSION_COMPLETE_STATE(eTREV_CITY_SAFEHOUSE_REMOVED)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_KONEIGHBOUR, eRequestArray)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FLOYDSPOON_A, eRequestArray)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FLOYDSPOON_B, eRequestArray)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FLOYDSPOON_B2, eRequestArray)
		FillSlotInTrevorSceneCrazyArray(iSceneIndex, PR_SCENE_T_FLOYDSPOON_A2, eRequestArray)
	ENDIF
	
	INT iSceneCount
	FOR iSceneCount = iStart TO (COUNT_OF(fPercentArray) - 1)
		IF (eRequestArray[iSceneCount] = INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, 0))
			eRequestArray[iSceneCount] = PR_SCENE_INVALID
		ENDIF
		
		IF (eRequestArray[iSceneCount] <> PR_SCENE_INVALID)
			fPercentArray[iSceneCount] = fTOTAL_PERCENT / TO_FLOAT(iSceneIndex)
		ELSE
			fPercentArray[iSceneCount] = 0.0
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    /*	00:00 - 5:59	*/
/// PARAMS:
///    eReqScene - 
///    ePed - 
/// RETURNS:
///    
FUNC BOOL GetTrevorSceneForMorning(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	INT iSceneIndex = 0
	CONST_INT iCOUNT_T_CITY_SCENES							8
	CONST_INT iCOUNT_T_COUNTRY_SCENES						7	//5
	
	enumCharacterList ePed = CHAR_TREVOR
	PED_REQUEST_SCENE_ENUM	eRequestArray[iCOUNT_T_CITY_SCENES+iCOUNT_T_COUNTRY_SCENES+iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES]
	FLOAT 					fPercentArray[iCOUNT_T_CITY_SCENES+iCOUNT_T_COUNTRY_SCENES+iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES]

	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY)
		IF NOT GET_MISSION_COMPLETE_STATE(eTREV_CITY_SAFEHOUSE_REMOVED)
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING, bDescentOnlyScene,
					PR_SCENE_T_FLOYD_BEAR, PR_SCENE_T_FLOYD_DOLL, PR_SCENE_T_FLOYDPINEAPPLE)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
			ENDIF
		ELSE
//			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING,
//					PR_SCENE_T_CR_POLICE_a, PR_SCENE_T_CR_BRIDGEDROP)
//				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
//			ENDIF
		ENDIF
		
		CONST_FLOAT fCHANCE_T_FLOYDSPOON_A					7.5
		CONST_FLOAT fCHANCE_T_FLOYDSPOON_B					7.5
		CONST_FLOAT fCHANCE_T_FLOYDSPOON_C					7.5
		CONST_FLOAT fCHANCE_T_FLOYDSPOON_D					7.5
//		CONST_FLOAT fCHANCE_T_STRIPCLUB_a					5.0
		CONST_FLOAT fCHANCE_T_SC_MOCKLAPDANCE				5.0
		CONST_FLOAT fCHANCE_T_SC_BAR						5.0
		CONST_FLOAT fCHANCE_T_STRIPCLUB_out					5.0
		CONST_FLOAT fCHANCE_T_CITY_RANDOM					50.0
		
		IF NOT GET_MISSION_COMPLETE_STATE(eTREV_CITY_SAFEHOUSE_REMOVED)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYDSPOON_A,		fCHANCE_T_FLOYDSPOON_A, eRequestArray, fPercentArray, iSceneIndex)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYDSPOON_B,		fCHANCE_T_FLOYDSPOON_B, eRequestArray, fPercentArray, iSceneIndex)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYDSPOON_B2,	fCHANCE_T_FLOYDSPOON_C, eRequestArray, fPercentArray, iSceneIndex)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYDSPOON_A2,	fCHANCE_T_FLOYDSPOON_D, eRequestArray, fPercentArray, iSceneIndex)
			
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYD_BEAR, 1.0, eRequestArray, fPercentArray, iSceneIndex)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYD_DOLL, 1.0, eRequestArray, fPercentArray, iSceneIndex)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYDPINEAPPLE, 1.0, eRequestArray, fPercentArray, iSceneIndex)

		ENDIF
		
//		FillSlotInTrevorSceneArray(PR_SCENE_T_STRIPCLUB_a,		fCHANCE_T_STRIPCLUB_a, eRequestArray, fPercentArray, iSceneIndex)	
		FillSlotInTrevorSceneArray(PR_SCENE_T_SC_MOCKLAPDANCE,	fCHANCE_T_SC_MOCKLAPDANCE, eRequestArray, fPercentArray, iSceneIndex)	
		
		IF NOT IS_MISSION_AVAILABLE(SP_HEIST_FINALE_2B)	//1072399
			FillSlotInTrevorSceneArray(PR_SCENE_T_SC_BAR,			fCHANCE_T_SC_BAR / 2.0, eRequestArray, fPercentArray, iSceneIndex)	
			FillSlotInTrevorSceneArray(PR_SCENE_T_SC_CHASE,			fCHANCE_T_SC_BAR / 2.0, eRequestArray, fPercentArray, iSceneIndex)	
		ENDIF
		
		FillSlotInTrevorSceneArray(PR_SCENE_T_STRIPCLUB_out,	fCHANCE_T_STRIPCLUB_out, eRequestArray, fPercentArray, iSceneIndex)
		
		FillTrevorCrazyCitySceneArray(iSceneIndex, fCHANCE_T_CITY_RANDOM, eRequestArray, fPercentArray)
	ENDIF
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_CITY)
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_MORNING, bDescentOnlyScene,
				PR_SCENE_T_CN_WAKEBARN, PR_SCENE_T_CN_WAKEMOUNTAIN)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_MORNING)
		ENDIF
		
		CONST_FLOAT fCHANCE_T6_SMOKECRYSTAL					15.0
		CONST_FLOAT fCHANCE_T_SMOKEMETH						15.0
		CONST_FLOAT fCHANCE_T6_METHLAB						5.0
		CONST_FLOAT fCHANCE_T6_TRAF_AIR						10.0
		CONST_FLOAT fCHANCE_T_HEADINSINK					5.0
		CONST_FLOAT fCHANCE_T_JERKOFF						5.0
		CONST_FLOAT fCHANCE_T_COUNTRY_RANDOM				30.0
		
		FillSlotInTrevorSceneArray(PR_SCENE_T6_SMOKECRYSTAL,	fCHANCE_T6_SMOKECRYSTAL, eRequestArray, fPercentArray, iSceneIndex)	
		FillSlotInTrevorSceneArray(PR_SCENE_T_SMOKEMETH,		fCHANCE_T_SMOKEMETH, eRequestArray, fPercentArray, iSceneIndex)	

		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CHINESE_1) AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2)
			IF NOT IS_MISSION_AVAILABLE(SP_HEIST_RURAL_2)	//1339576
			AND NOT IS_MISSION_AVAILABLE(SP_HEIST_RURAL_1)	//1541148
				FillSlotInTrevorSceneArray(PR_SCENE_T6_METHLAB,		fCHANCE_T6_METHLAB, eRequestArray, fPercentArray, iSceneIndex)	
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_ARMS_TRAFFICKING) = CHAR_TREVOR
			FillSlotInTrevorSceneArray(PR_SCENE_T6_TRAF_AIR,	fCHANCE_T6_TRAF_AIR, eRequestArray, fPercentArray, iSceneIndex)
		ENDIF
		
		FillSlotInTrevorSceneArray(PR_SCENE_T_HEADINSINK,	fCHANCE_T_HEADINSINK, eRequestArray, fPercentArray, iSceneIndex)	
		#IF NOT IS_JAPANESE_BUILD
		FillSlotInTrevorSceneArray(PR_SCENE_T_JERKOFF,		fCHANCE_T_JERKOFF, eRequestArray, fPercentArray, iSceneIndex)
		#ENDIF
		
		FillTrevorCrazyCountrySceneArray(iSceneIndex, fCHANCE_T_COUNTRY_RANDOM, eRequestArray, fPercentArray)
	ENDIF
	
	RETURN GetDesiredPedRequestSceneFromArray(ePed,
			eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
ENDFUNC

/// PURPOSE:
///    /* 06:00 - 14:59 */
/// PARAMS: 
///    eReqScene - 
///    ePed - 
/// RETURNS: 
///
FUNC BOOL GetTrevorSceneForAfternoon(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	INT iSceneIndex = 0
	CONST_INT iCOUNT_T_CITY_SCENES							1
	CONST_INT iCOUNT_T_COUNTRY_SCENES						6	//4
	
	enumCharacterList ePed = CHAR_TREVOR
	PED_REQUEST_SCENE_ENUM	eRequestArray[iCOUNT_T_CITY_SCENES+iCOUNT_T_COUNTRY_SCENES+iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES]
	FLOAT					fPercentArray[iCOUNT_T_CITY_SCENES+iCOUNT_T_COUNTRY_SCENES+iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES]
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY)
		
		IF NOT GET_MISSION_COMPLETE_STATE(eTREV_CITY_SAFEHOUSE_REMOVED)
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
					PR_SCENE_T_FLOYD_BEAR, PR_SCENE_T_FLOYD_DOLL, PR_SCENE_T_FLOYDPINEAPPLE)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
			ENDIF
		ELSE
//			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON,
//					PR_SCENE_T_CR_POLICE_a, PR_SCENE_T_CR_BRIDGEDROP)
//				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
//			ENDIF
		ENDIF
		
		CONST_FLOAT fCHANCE_T_FLOYDSAVEHOUSE				20.0
		CONST_FLOAT fCHANCE_T_CITY_RANDOM					80.0
		
		IF NOT GET_MISSION_COMPLETE_STATE(eTREV_CITY_SAFEHOUSE_REMOVED)
			FillSlotInTrevorSceneArray(PR_SCENE_T_FLOYDSAVEHOUSE , fCHANCE_T_FLOYDSAVEHOUSE, eRequestArray, fPercentArray, iSceneIndex)	
		ENDIF
		
		FillTrevorCrazyCitySceneArray(iSceneIndex, fCHANCE_T_CITY_RANDOM, eRequestArray, fPercentArray)
	ENDIF
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_CITY)
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_AFTERNOON, bDescentOnlyScene,
				PR_SCENE_T_CN_WAKEBARN, PR_SCENE_T_CN_WAKEMOUNTAIN)
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_AFTERNOON)
		ENDIF
		
//		CONST_FLOAT fCHANCE_T6_BLOWSHITUP					20.0
		CONST_FLOAT fCHANCE_T6_METHLAB						5.0
		CONST_FLOAT fCHANCE_T6_HUNTING						5.0
		CONST_FLOAT fCHANCE_T6_TRAF_AIR						10.0
		CONST_FLOAT fCHANCE_T_HEADINSINK					5.0
		CONST_FLOAT fCHANCE_T_JERKOFF						5.0
		CONST_FLOAT fCHANCE_T_COUNTRY_RANDOM				40.0
		
//		FillSlotInTrevorSceneArray(PR_SCENE_T6_BLOWSHITUP, fCHANCE_T6_BLOWSHITUP, eRequestArray, fPercentArray, iSceneIndex)	
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CHINESE_1) AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2)
			IF NOT IS_MISSION_AVAILABLE(SP_HEIST_RURAL_2)	//1339576
			AND NOT IS_MISSION_AVAILABLE(SP_HEIST_RURAL_1)	//1541148
				FillSlotInTrevorSceneArray(PR_SCENE_T6_METHLAB,		fCHANCE_T6_METHLAB, eRequestArray, fPercentArray, iSceneIndex)	
			ENDIF
		ENDIF
		
		IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_HUNTING))
			IF NOT PRIVATE_IsRandomCharFlagSet(RC_HUNTING_1, RC_FLAG_READY_TO_PLAY)
			AND NOT PRIVATE_IsRandomCharFlagSet(RC_HUNTING_2, RC_FLAG_READY_TO_PLAY)
				IF DoesPlayerPedHaveWeaponStored(CHAR_TREVOR, WEAPONTYPE_SNIPERRIFLE)
				OR DoesPlayerPedHaveWeaponStored(CHAR_TREVOR, WEAPONTYPE_HEAVYSNIPER)
					FillSlotInTrevorSceneArray(PR_SCENE_T6_HUNTING1, fCHANCE_T6_HUNTING, eRequestArray, fPercentArray, iSceneIndex)	
					FillSlotInTrevorSceneArray(PR_SCENE_T6_HUNTING2, fCHANCE_T6_HUNTING, eRequestArray, fPercentArray, iSceneIndex)	
					FillSlotInTrevorSceneArray(PR_SCENE_T6_HUNTING3, fCHANCE_T6_HUNTING, eRequestArray, fPercentArray, iSceneIndex)	
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_ARMS_TRAFFICKING) = CHAR_TREVOR
			FillSlotInTrevorSceneArray(PR_SCENE_T6_TRAF_AIR, fCHANCE_T6_TRAF_AIR, eRequestArray, fPercentArray, iSceneIndex)
		ENDIF
		
		FillSlotInTrevorSceneArray(PR_SCENE_T_HEADINSINK, fCHANCE_T_HEADINSINK, eRequestArray, fPercentArray, iSceneIndex)	
		#IF NOT IS_JAPANESE_BUILD
		FillSlotInTrevorSceneArray(PR_SCENE_T_JERKOFF, fCHANCE_T_JERKOFF, eRequestArray, fPercentArray, iSceneIndex)
		#ENDIF
		
		FillTrevorCrazyCountrySceneArray(iSceneIndex, fCHANCE_T_COUNTRY_RANDOM, eRequestArray, fPercentArray)
	ENDIF
	
	RETURN GetDesiredPedRequestSceneFromArray(ePed,
			eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
ENDFUNC


/// PURPOSE:
///    /* 15:00 - 23:59 */
/// PARAMS: 
///    eReqScene - 
///    ePed - 
/// RETURNS: 
///
FUNC BOOL GetTrevorSceneForEvening(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
	INT iSceneIndex = 0
	CONST_INT iCOUNT_T_CITY_SCENES							8
	CONST_INT iCOUNT_T_COUNTRY_SCENES						6	//4
	
	enumCharacterList ePed = CHAR_TREVOR
	PED_REQUEST_SCENE_ENUM eRequestArray[iCOUNT_T_CITY_SCENES+iCOUNT_T_COUNTRY_SCENES+iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES]							
	FLOAT            fPercentArray[iCOUNT_T_CITY_SCENES+iCOUNT_T_COUNTRY_SCENES+iNUM_OF_TREVOR_CRAZY_RANDOM_SCENES]							

	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY)
		
		IF NOT GET_MISSION_COMPLETE_STATE(eTREV_CITY_SAFEHOUSE_REMOVED)
			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
					PR_SCENE_T_FLOYD_BEAR, PR_SCENE_T_FLOYD_DOLL, PR_SCENE_T_FLOYDPINEAPPLE)
				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
			ENDIF
		ELSE
//			IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING,
//					PR_SCENE_T_CR_POLICE_a, PR_SCENE_T_CR_BRIDGEDROP)
//				RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
//			ENDIF
		ENDIF
		
		CONST_FLOAT fCHANCE_T_DOCKS_a						5.0
		CONST_FLOAT fCHANCE_T_DOCKS_b						5.0
		CONST_FLOAT fCHANCE_T_DOCKS_c						5.0
		CONST_FLOAT fCHANCE_T_DOCKS_d						5.0
//		CONST_FLOAT fCHANCE_T_STRIPCLUB_a					2.5
		CONST_FLOAT fCHANCE_T_SC_MOCKLAPDANCE				2.5
		CONST_FLOAT fCHANCE_T_SC_BAR						2.5
		CONST_FLOAT fCHANCE_T_STRIPCLUB_out					2.5
		CONST_FLOAT fCHANCE_T_CITY_RANDOM					70.0

		IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_HEIST_FINISHED_DOCKS)
			FillSlotInTrevorSceneArray(PR_SCENE_T_DOCKS_a,		fCHANCE_T_DOCKS_a, eRequestArray, fPercentArray, iSceneIndex)	
			FillSlotInTrevorSceneArray(PR_SCENE_T_DOCKS_b,		fCHANCE_T_DOCKS_b, eRequestArray, fPercentArray, iSceneIndex)	
			
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_2)
				FillSlotInTrevorSceneArray(PR_SCENE_T_DOCKS_c,	fCHANCE_T_DOCKS_c, eRequestArray, fPercentArray, iSceneIndex)	
			ENDIF
			
			FillSlotInTrevorSceneArray(PR_SCENE_T_DOCKS_d,		fCHANCE_T_DOCKS_d, eRequestArray, fPercentArray, iSceneIndex)	
		ENDIF
		
//		FillSlotInTrevorSceneArray(PR_SCENE_T_STRIPCLUB_a,		fCHANCE_T_STRIPCLUB_a, eRequestArray, fPercentArray, iSceneIndex)	
		FillSlotInTrevorSceneArray(PR_SCENE_T_SC_MOCKLAPDANCE,	fCHANCE_T_SC_MOCKLAPDANCE, eRequestArray, fPercentArray, iSceneIndex)	
			
		IF NOT IS_MISSION_AVAILABLE(SP_HEIST_FINALE_2B)	//1072399
			FillSlotInTrevorSceneArray(PR_SCENE_T_SC_BAR,			fCHANCE_T_SC_BAR / 2.0, eRequestArray, fPercentArray, iSceneIndex)	
			FillSlotInTrevorSceneArray(PR_SCENE_T_SC_CHASE,			fCHANCE_T_SC_BAR / 2.0, eRequestArray, fPercentArray, iSceneIndex)	
		ENDIF
		
		FillSlotInTrevorSceneArray(PR_SCENE_T_STRIPCLUB_out,	fCHANCE_T_STRIPCLUB_out, eRequestArray, fPercentArray, iSceneIndex)
		
		FillTrevorCrazyCitySceneArray(iSceneIndex, fCHANCE_T_CITY_RANDOM, eRequestArray, fPercentArray)
	ENDIF							
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_TREVOR_RESTRICTED_TO_CITY)
		
		IF PRIVATE_ShouldPlayerTriggerPrioritySwitch(ePed, eReqScene, fScenePercent, sPedScene, sPassedScene, TS_EVENING, bDescentOnlyScene,
				PR_SCENE_T_CN_WAKEBARN, PR_SCENE_T_CN_WAKEMOUNTAIN)
			//
			RETURN PRIVATE_TriggerThisPrioritySwitch(ePed, eReqScene, TS_EVENING)
		ENDIF
		
//		CONST_FLOAT fCHANCE_T6_EVENING						30.0
		CONST_FLOAT fCHANCE_T6_METHLAB						5.0
		CONST_FLOAT fCHANCE_T6_HUNTING						5.0
		CONST_FLOAT fCHANCE_T6_TRAF_AIR						10.0
		CONST_FLOAT fCHANCE_T_HEADINSINK					5.0
		CONST_FLOAT fCHANCE_T_JERKOFF						5.0
		CONST_FLOAT fCHANCE_T_COUNTRY_RANDOM				30.0

//		FillSlotInTrevorSceneArray(PR_SCENE_T6_EVENING,		fCHANCE_T6_EVENING, eRequestArray, fPercentArray, iSceneIndex)	
		
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CHINESE_1) AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2)
			IF NOT IS_MISSION_AVAILABLE(SP_HEIST_RURAL_2)	//1339576
			AND NOT IS_MISSION_AVAILABLE(SP_HEIST_RURAL_1)	//1541148
				FillSlotInTrevorSceneArray(PR_SCENE_T6_METHLAB,		fCHANCE_T6_METHLAB, eRequestArray, fPercentArray, iSceneIndex)	
			ENDIF
		ENDIF
		
		IF NOT PRIVATE_IsRandomCharFlagSet(RC_HUNTING_1, RC_FLAG_READY_TO_PLAY)
		AND NOT PRIVATE_IsRandomCharFlagSet(RC_HUNTING_2, RC_FLAG_READY_TO_PLAY)
			IF DoesPlayerPedHaveWeaponStored(CHAR_TREVOR, WEAPONTYPE_SNIPERRIFLE)
			OR DoesPlayerPedHaveWeaponStored(CHAR_TREVOR, WEAPONTYPE_HEAVYSNIPER)
				FillSlotInTrevorSceneArray(PR_SCENE_T6_HUNTING1,	fCHANCE_T6_HUNTING, eRequestArray, fPercentArray, iSceneIndex)	
				FillSlotInTrevorSceneArray(PR_SCENE_T6_HUNTING2,	fCHANCE_T6_HUNTING, eRequestArray, fPercentArray, iSceneIndex)	
				FillSlotInTrevorSceneArray(PR_SCENE_T6_HUNTING3,	fCHANCE_T6_HUNTING, eRequestArray, fPercentArray, iSceneIndex)	
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_ARMS_TRAFFICKING) = CHAR_TREVOR
			FillSlotInTrevorSceneArray(PR_SCENE_T6_TRAF_AIR,	fCHANCE_T6_TRAF_AIR, eRequestArray, fPercentArray, iSceneIndex)
		ENDIF
		FillSlotInTrevorSceneArray(PR_SCENE_T_HEADINSINK,	fCHANCE_T_HEADINSINK, eRequestArray, fPercentArray, iSceneIndex)	
		#IF NOT IS_JAPANESE_BUILD
		FillSlotInTrevorSceneArray(PR_SCENE_T_JERKOFF,		fCHANCE_T_JERKOFF, eRequestArray, fPercentArray, iSceneIndex)
		#ENDIF
		FillTrevorCrazyCountrySceneArray(iSceneIndex, 		fCHANCE_T_COUNTRY_RANDOM, eRequestArray, fPercentArray)
	ENDIF
	
	RETURN GetDesiredPedRequestSceneFromArray(ePed,
			eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
ENDFUNC



/// PURPOSE: Returns the scene ID from the timetable
FUNC BOOL GetTrevorSceneForCurrentTime(PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bDescentOnlyScene)
//	enumCharacterList ePed = CHAR_TREVOR
	eReqScene = PR_SCENE_INVALID
	fScenePercent = -1
	
	IF NOT bDescentOnlyScene
		IF GetPedRequestSceneForAvailableMission(CHAR_TREVOR, eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	
	SWITCH PRIVATE_Get_Switch_Clock_Hours()
		/*	00:00 - 5:59	*/
        CASE 0 CASE 1 CASE 2
        CASE 3 CASE 4 CASE 5
			GetTrevorSceneForMorning(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		BREAK
		
		/*	06:00 - 14:59	*/
        CASE 6 CASE 7 CASE 8
        CASE 9 CASE 10 CASE 11
        CASE 12 CASE 13 CASE 14
			GetTrevorSceneForAfternoon(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		BREAK
		
		/*	15:00 - 23:59	*/
        CASE 15 CASE 16 CASE 17
        CASE 18 CASE 19 CASE 20
        CASE 21 CASE 22 CASE 23
			GetTrevorSceneForEvening(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		BREAK
	ENDSWITCH
	
	RETURN (eReqScene <> PR_SCENE_INVALID)
ENDFUNC

// *******************************************************************************************
//	MAIN FUNCTION TO RETURN NEXT SCENE FOR PLAYER CHAR
// *******************************************************************************************

FUNC BOOL PassedBlimpCheckOnLastAndNext(enumCharacterList ePed)
	
	IF g_sPlayerLastVeh[ePed].model = BLIMP
	OR g_sPlayerLastVeh[ePed].model = BLIMP2
		TIMEOFDAY sLastTimeActive
		SWITCH ePed
			CASE CHAR_MICHAEL
				//check franklins blimp state
				IF g_sPlayerLastVeh[CHAR_FRANKLIN].model = BLIMP
				OR g_sPlayerLastVeh[CHAR_FRANKLIN].model = BLIMP2
					sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[CHAR_FRANKLIN]
					IF Is_TIMEOFDAY_Valid(sLastTimeActive)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_FRANKLIN], <<0,0,0>>, 1.5)
							IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
								CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Franklin was in a blimp")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//check trevors blimp state
				IF g_sPlayerLastVeh[CHAR_TREVOR].model = BLIMP
				OR g_sPlayerLastVeh[CHAR_TREVOR].model = BLIMP2
					sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[CHAR_TREVOR]
					IF Is_TIMEOFDAY_Valid(sLastTimeActive)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_TREVOR], <<0,0,0>>, 1.5)
							IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
								CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Trevor was in a blimp")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE CHAR_FRANKLIN
				//check michaels blimp state
				IF g_sPlayerLastVeh[CHAR_MICHAEL].model = BLIMP
				OR g_sPlayerLastVeh[CHAR_MICHAEL].model = BLIMP2
					sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[CHAR_MICHAEL]
					IF Is_TIMEOFDAY_Valid(sLastTimeActive)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_MICHAEL], <<0,0,0>>, 1.5)
							IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
								CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Michael was in a blimp")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//check trevors blimp state
				IF g_sPlayerLastVeh[CHAR_TREVOR].model = BLIMP
				OR g_sPlayerLastVeh[CHAR_TREVOR].model = BLIMP2
					sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[CHAR_TREVOR]
					IF Is_TIMEOFDAY_Valid(sLastTimeActive)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_TREVOR], <<0,0,0>>, 1.5)
							IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
								CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Trevor was in a blimp")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				//check michaels blimp state
				IF g_sPlayerLastVeh[CHAR_MICHAEL].model = BLIMP
				OR g_sPlayerLastVeh[CHAR_MICHAEL].model = BLIMP2
					sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[CHAR_MICHAEL]
					IF Is_TIMEOFDAY_Valid(sLastTimeActive)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_MICHAEL], <<0,0,0>>, 1.5)
							IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
								CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Michael was in a blimp")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//check franklins blimp state
				IF g_sPlayerLastVeh[CHAR_FRANKLIN].model = BLIMP
				OR g_sPlayerLastVeh[CHAR_FRANKLIN].model = BLIMP2
					sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[CHAR_FRANKLIN]
					IF Is_TIMEOFDAY_Valid(sLastTimeActive)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_FRANKLIN], <<0,0,0>>, 1.5)
							IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
								CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Franklin was in a blimp")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		VEHICLE_INDEX vehID
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		ELSE
			vehID = GET_PLAYERS_LAST_VEHICLE()
		ENDIF
		IF DOES_ENTITY_EXIST(vehID)
			MODEL_NAMES modelID = GET_ENTITY_MODEL(vehID)
			IF modelID = BLIMP
			OR modelID = BLIMP2
				CASSERTLN(DEBUG_SWITCH, "PassedBlimpCheckOnLastAndNext(", GET_PLAYER_PED_STRING(ePed), ") - Player is/was in a blimp")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL FallenThroughPostMissionScene(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM ePostReqScene,
		PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent,
		PED_SCENE_STRUCT &sPedScene,
		PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "FallenThroughPostMissionScene ", Get_String_From_Ped_Request_Scene_Enum(ePostReqScene))
	CERRORLN(DEBUG_SWITCH, "FallenThroughPostMissionScene ", Get_String_From_Ped_Request_Scene_Enum(ePostReqScene))
	#ENDIF
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			GetMichaelSceneForCurrentTime(eReqScene, fScenePercent, sPedScene, sPassedScene, FALSE)
		BREAK
		CASE CHAR_FRANKLIN
			GetFranklinSceneForCurrentTime(eReqScene, fScenePercent, sPedScene, sPassedScene, FALSE)
		BREAK
		CASE CHAR_TREVOR
			GetTrevorSceneForCurrentTime(eReqScene, fScenePercent, sPedScene, sPassedScene, FALSE)
		BREAK
		
		DEFAULT
			ePostReqScene = ePostReqScene
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(ePostReqScene), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
	#ENDIF
	
	RETURN (eReqScene <> PR_SCENE_INVALID)
ENDFUNC

FUNC BOOL GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM &eReqScene, FLOAT &fScenePercent, PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, BOOL bBypassDefaultSwitch = FALSE)
	
	eReqScene = PR_SCENE_INVALID
	fScenePercent = -1
	
	#IF IS_DEBUG_BUILD		//#1508299
	IF GET_COMMANDLINE_PARAM_EXISTS("ForceDefaultSwitch")
		IF NOT Is_TIMEOFDAY_Valid(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
			g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed] = GET_CURRENT_TIMEOFDAY()
		ENDIF
		
		IF ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed], <<0,0,0>>, 1.5)
			g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF
		
		SWITCH ePed
			CASE CHAR_MICHAEL
				CASSERTLN(DEBUG_SWITCH, "Michael - \"ForceDefaultSwitch\"")
				
				fScenePercent = 100
				eReqScene = PR_SCENE_M_DEFAULT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_FRANKLIN
				CASSERTLN(DEBUG_SWITCH, "Franklin - \"ForceDefaultSwitch\"")
				
				fScenePercent = 100
				eReqScene = PR_SCENE_F_DEFAULT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_TREVOR
				CASSERTLN(DEBUG_SWITCH, "Trevor - \"ForceDefaultSwitch\"")
				
				fScenePercent = 100
				eReqScene = PR_SCENE_T_DEFAULT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
		ENDSWITCH
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("ForceOverrideSwitch")
		SWITCH ePed
			CASE CHAR_MICHAEL
				CASSERTLN(DEBUG_SWITCH, "Michael - \"ForceOverrideSwitch\"")
				
				g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_M_OVERRIDE
				
				g_sOverrideScene[ePed].ePed = ePed
				g_sOverrideScene[ePed].eScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
				g_sOverrideScene[ePed].vCreateCoords = g_vDebugStartPositions[0]
				g_sOverrideScene[ePed].fCreateHead = g_vDebugStartOrientations[0]
				
				fScenePercent = 100
				eReqScene = PR_SCENE_M_OVERRIDE
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_FRANKLIN
				CASSERTLN(DEBUG_SWITCH, "Franklin - \"ForceOverrideSwitch\"")
				
				g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_F_OVERRIDE
				
				g_sOverrideScene[ePed].ePed = ePed
				g_sOverrideScene[ePed].eScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
				g_sOverrideScene[ePed].vCreateCoords = g_vDebugStartPositions[1]
				g_sOverrideScene[ePed].fCreateHead = g_vDebugStartOrientations[1]
				
				fScenePercent = 100
				eReqScene = PR_SCENE_F_OVERRIDE
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_TREVOR
				CASSERTLN(DEBUG_SWITCH, "Trevor - \"ForceOverrideSwitch\"")
				
				g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_T_OVERRIDE
				
				g_sOverrideScene[ePed].ePed = ePed
				g_sOverrideScene[ePed].eScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
				g_sOverrideScene[ePed].vCreateCoords = g_vDebugStartPositions[2]
				g_sOverrideScene[ePed].fCreateHead = g_vDebugStartOrientations[2]
				
				fScenePercent = 100
				eReqScene = PR_SCENE_T_OVERRIDE
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
		ENDSWITCH
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceNamedSwitch")
		STRING sNamedSwitch = GET_COMMANDLINE_PARAM("sc_ForceNamedSwitch")
		SWITCH GET_HASH_KEY(sNamedSwitch)			/* michael */
			CASE HASH("PR_SCENE_M2_BEDROOM")		fScenePercent = 100 eReqScene = PR_SCENE_M2_BEDROOM	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_SAVEHOUSE0_b")	fScenePercent = 100 eReqScene = PR_SCENE_M2_SAVEHOUSE0_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_SAVEHOUSE1_a")	fScenePercent = 100 eReqScene = PR_SCENE_M2_SAVEHOUSE1_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_SAVEHOUSE1_b")	fScenePercent = 100 eReqScene = PR_SCENE_M2_SAVEHOUSE1_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_SMOKINGGOLF")	fScenePercent = 100 eReqScene = PR_SCENE_M2_SMOKINGGOLF	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_WAKEUPSCREAM")	fScenePercent = 100 eReqScene = PR_SCENE_M4_WAKEUPSCREAM	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_WAKESUPSCARED")	fScenePercent = 100 eReqScene = PR_SCENE_M4_WAKESUPSCARED	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_HOUSEBED")		fScenePercent = 100 eReqScene = PR_SCENE_M6_HOUSEBED	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_WATCHINGTV")		fScenePercent = 100 eReqScene = PR_SCENE_M4_WATCHINGTV	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_KIDS_TV")		fScenePercent = 100 eReqScene = PR_SCENE_M2_KIDS_TV	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_POOLSIDE_a")		fScenePercent = 100 eReqScene = PR_SCENE_M_POOLSIDE_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_POOLSIDE_b")		fScenePercent = 100 eReqScene = PR_SCENE_M_POOLSIDE_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_CARSLEEP_a")		fScenePercent = 100 eReqScene = PR_SCENE_M2_CARSLEEP_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_CARSLEEP_b")		fScenePercent = 100 eReqScene = PR_SCENE_M2_CARSLEEP_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_CANAL_a")			fScenePercent = 100 eReqScene = PR_SCENE_M_CANAL_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_CANAL_b")			fScenePercent = 100 eReqScene = PR_SCENE_M_CANAL_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_CANAL_c")			fScenePercent = 100 eReqScene = PR_SCENE_M_CANAL_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_LUNCH_a")		fScenePercent = 100 eReqScene = PR_SCENE_M2_LUNCH_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_EXITRESTAURANT")	fScenePercent = 100 eReqScene = PR_SCENE_M4_EXITRESTAURANT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_LUNCH_b")		fScenePercent = 100 eReqScene = PR_SCENE_M4_LUNCH_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_CINEMA")			fScenePercent = 100 eReqScene = PR_SCENE_M4_CINEMA	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_WIFEEXITSCAR")	fScenePercent = 100 eReqScene = PR_SCENE_M2_WIFEEXITSCAR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_DROPOFFDAU_a")	fScenePercent = 100 eReqScene = PR_SCENE_M2_DROPOFFDAU_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_DROPOFFDAU_b")	fScenePercent = 100 eReqScene = PR_SCENE_M2_DROPOFFDAU_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_DROPOFFSON_a")	fScenePercent = 100 eReqScene = PR_SCENE_M2_DROPOFFSON_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_DROPOFFSON_b")	fScenePercent = 100 eReqScene = PR_SCENE_M2_DROPOFFSON_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_PIER_a")			fScenePercent = 100 eReqScene = PR_SCENE_M_PIER_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_PIER_b")			fScenePercent = 100 eReqScene = PR_SCENE_M_PIER_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_TRAFFIC_a")		fScenePercent = 100 eReqScene = PR_SCENE_M_TRAFFIC_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_TRAFFIC_b")		fScenePercent = 100 eReqScene = PR_SCENE_M_TRAFFIC_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_TRAFFIC_c")		fScenePercent = 100 eReqScene = PR_SCENE_M_TRAFFIC_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_VWOODPARK_a")		fScenePercent = 100 eReqScene = PR_SCENE_M_VWOODPARK_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_VWOODPARK_b")		fScenePercent = 100 eReqScene = PR_SCENE_M_VWOODPARK_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_BENCHCALL_a")		fScenePercent = 100 eReqScene = PR_SCENE_M_BENCHCALL_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_BENCHCALL_b")		fScenePercent = 100 eReqScene = PR_SCENE_M_BENCHCALL_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_PARKEDHILLS_a")	fScenePercent = 100 eReqScene = PR_SCENE_M_PARKEDHILLS_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_PARKEDHILLS_b")	fScenePercent = 100 eReqScene = PR_SCENE_M_PARKEDHILLS_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_PHARMACY")		fScenePercent = 100 eReqScene = PR_SCENE_M2_PHARMACY	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_COFFEE_a")		fScenePercent = 100 eReqScene = PR_SCENE_M_COFFEE_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_COFFEE_b")		fScenePercent = 100 eReqScene = PR_SCENE_M_COFFEE_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_COFFEE_c")		fScenePercent = 100 eReqScene = PR_SCENE_M_COFFEE_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_CYCLING_a")		fScenePercent = 100 eReqScene = PR_SCENE_M2_CYCLING_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_CYCLING_b")		fScenePercent = 100 eReqScene = PR_SCENE_M2_CYCLING_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_CYCLING_c")		fScenePercent = 100 eReqScene = PR_SCENE_M2_CYCLING_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_MARINA")			fScenePercent = 100 eReqScene = PR_SCENE_M2_MARINA	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_ARGUEWITHWIFE")	fScenePercent = 100 eReqScene = PR_SCENE_M2_ARGUEWITHWIFE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_PARKEDBEACH")	fScenePercent = 100 eReqScene = PR_SCENE_M4_PARKEDBEACH	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M4_WASHFACE")		fScenePercent = 100 eReqScene = PR_SCENE_M4_WASHFACE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M_HOOKERMOTEL")		fScenePercent = 100 eReqScene = PR_SCENE_M_HOOKERMOTEL	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_MORNING_a")		fScenePercent = 100 eReqScene = PR_SCENE_M6_MORNING_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_CARSLEEP")		fScenePercent = 100 eReqScene = PR_SCENE_M6_CARSLEEP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_HOUSETV_a")		fScenePercent = 100 eReqScene = PR_SCENE_M6_HOUSETV_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_SUNBATHING")		fScenePercent = 100 eReqScene = PR_SCENE_M6_SUNBATHING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRINKINGBEER")	fScenePercent = 100 eReqScene = PR_SCENE_M6_DRINKINGBEER	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_ONPHONE")		fScenePercent = 100 eReqScene = PR_SCENE_M6_ONPHONE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DEPRESSED")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DEPRESSED	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_BOATING")		fScenePercent = 100 eReqScene = PR_SCENE_M6_BOATING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_LIQUORSTORE")	fScenePercent = 100 eReqScene = PR_SCENE_M6_LIQUORSTORE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_PARKEDHILLS_a")	fScenePercent = 100 eReqScene = PR_SCENE_M6_PARKEDHILLS_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_PARKEDHILLS_b")	fScenePercent = 100 eReqScene = PR_SCENE_M6_PARKEDHILLS_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_PARKEDHILLS_c")	fScenePercent = 100 eReqScene = PR_SCENE_M6_PARKEDHILLS_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_PARKEDHILLS_d")	fScenePercent = 100 eReqScene = PR_SCENE_M6_PARKEDHILLS_d	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_PARKEDHILLS_e")	fScenePercent = 100 eReqScene = PR_SCENE_M6_PARKEDHILLS_e	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_DRIVING_a")		fScenePercent = 100 eReqScene = PR_SCENE_M2_DRIVING_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M2_DRIVING_b")		fScenePercent = 100 eReqScene = PR_SCENE_M2_DRIVING_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_a")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_b")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_c")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_d")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_d	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_e")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_e	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_f")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_f	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_g")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_g	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_DRIVING_h")		fScenePercent = 100 eReqScene = PR_SCENE_M6_DRIVING_h	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M6_RONBORING")		fScenePercent = 100 eReqScene = PR_SCENE_M6_RONBORING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_RESTAURANT")		fScenePercent = 100 eReqScene = PR_SCENE_M7_RESTAURANT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_LOUNGECHAIRS")	fScenePercent = 100 eReqScene = PR_SCENE_M7_LOUNGECHAIRS	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_BYESOLOMON_a")	fScenePercent = 100 eReqScene = PR_SCENE_M7_BYESOLOMON_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_BYESOLOMON_b")	fScenePercent = 100 eReqScene = PR_SCENE_M7_BYESOLOMON_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_WIFETENNIS")		fScenePercent = 100 eReqScene = PR_SCENE_M7_WIFETENNIS	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_ROUNDTABLE")		fScenePercent = 100 eReqScene = PR_SCENE_M7_ROUNDTABLE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_REJECTENTRY")	fScenePercent = 100 eReqScene = PR_SCENE_M7_REJECTENTRY	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_HOOKERS")		fScenePercent = 100 eReqScene = PR_SCENE_M7_HOOKERS	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_EXITBARBER")		fScenePercent = 100 eReqScene = PR_SCENE_M7_EXITBARBER	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_EXITFANCYSHOP")	fScenePercent = 100 eReqScene = PR_SCENE_M7_EXITFANCYSHOP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_FAKEYOGA")		fScenePercent = 100 eReqScene = PR_SCENE_M7_FAKEYOGA	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_COFFEE")			fScenePercent = 100 eReqScene = PR_SCENE_M7_COFFEE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_GETSREADY")		fScenePercent = 100 eReqScene = PR_SCENE_M7_GETSREADY	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_READSCRIPT")		fScenePercent = 100 eReqScene = PR_SCENE_M7_READSCRIPT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_EMPLOYEECONVO")	fScenePercent = 100 eReqScene = PR_SCENE_M7_EMPLOYEECONVO	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_TALKTOGUARD")	fScenePercent = 100 eReqScene = PR_SCENE_M7_TALKTOGUARD	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_LOT_JIMMY")		fScenePercent = 100 eReqScene = PR_SCENE_M7_LOT_JIMMY	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_KIDS_TV")		fScenePercent = 100 eReqScene = PR_SCENE_M7_KIDS_TV	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_KIDS_GAMING")	fScenePercent = 100 eReqScene = PR_SCENE_M7_KIDS_GAMING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_OPENDOORFORAMA")	fScenePercent = 100 eReqScene = PR_SCENE_M7_OPENDOORFORAMA	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_DROPPINGOFFJMY")	fScenePercent = 100 eReqScene = PR_SCENE_M7_DROPPINGOFFJMY	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_TRACEYEXITSCAR")	fScenePercent = 100 eReqScene = PR_SCENE_M7_TRACEYEXITSCAR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_M7_BIKINGJIMMY")	fScenePercent = 100 eReqScene = PR_SCENE_M7_BIKINGJIMMY	 RETURN TRUE BREAK
			
			CASE HASH("PR_SCENE_M_S_FAMILY4")		fScenePercent = 100 eReqScene = PR_SCENE_M_S_FAMILY4	 RETURN TRUE BREAK
		ENDSWITCH
		SWITCH GET_HASH_KEY(sNamedSwitch)			/* franklin */
			CASE HASH("PR_SCENE_F0_SH_ASLEEP")		fScenePercent = 100 eReqScene = PR_SCENE_F0_SH_ASLEEP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_SH_ASLEEP")		fScenePercent = 100 eReqScene = PR_SCENE_F1_SH_ASLEEP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_NAPPING")		fScenePercent = 100 eReqScene = PR_SCENE_F1_NAPPING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_GETTINGREADY")	fScenePercent = 100 eReqScene = PR_SCENE_F1_GETTINGREADY	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_SH_READING")		fScenePercent = 100 eReqScene = PR_SCENE_F0_SH_READING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_SH_READING")		fScenePercent = 100 eReqScene = PR_SCENE_F1_SH_READING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_SH_PUSHUP_a")	fScenePercent = 100 eReqScene = PR_SCENE_F0_SH_PUSHUP_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_SH_PUSHUP_b")	fScenePercent = 100 eReqScene = PR_SCENE_F0_SH_PUSHUP_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_SH_PUSHUP")		fScenePercent = 100 eReqScene = PR_SCENE_F1_SH_PUSHUP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_POOLSIDE_a")		fScenePercent = 100 eReqScene = PR_SCENE_F1_POOLSIDE_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_POOLSIDE_b")		fScenePercent = 100 eReqScene = PR_SCENE_F1_POOLSIDE_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_CLEANINGAPT")	fScenePercent = 100 eReqScene = PR_SCENE_F1_CLEANINGAPT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_ONCELL")			fScenePercent = 100 eReqScene = PR_SCENE_F1_ONCELL	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_SNACKING")		fScenePercent = 100 eReqScene = PR_SCENE_F1_SNACKING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_ONLAPTOP")		fScenePercent = 100 eReqScene = PR_SCENE_F1_ONLAPTOP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_IRONING")		fScenePercent = 100 eReqScene = PR_SCENE_F1_IRONING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_WATCHINGTV")		fScenePercent = 100 eReqScene = PR_SCENE_F1_WATCHINGTV	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_MD_KUSH_DOC")		fScenePercent = 100 eReqScene = PR_SCENE_F_MD_KUSH_DOC	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_KUSH_DOC_a")		fScenePercent = 100 eReqScene = PR_SCENE_F_KUSH_DOC_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_KUSH_DOC_b")		fScenePercent = 100 eReqScene = PR_SCENE_F_KUSH_DOC_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_KUSH_DOC_c")		fScenePercent = 100 eReqScene = PR_SCENE_F_KUSH_DOC_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_TANISHAFIGHT")	fScenePercent = 100 eReqScene = PR_SCENE_F0_TANISHAFIGHT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_NEWHOUSE")		fScenePercent = 100 eReqScene = PR_SCENE_F1_NEWHOUSE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_GARBAGE")		fScenePercent = 100 eReqScene = PR_SCENE_F0_GARBAGE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_GARBAGE")		fScenePercent = 100 eReqScene = PR_SCENE_F1_GARBAGE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_THROW_CUP")		fScenePercent = 100 eReqScene = PR_SCENE_F_THROW_CUP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_HIT_CUP_HAND")	fScenePercent = 100 eReqScene = PR_SCENE_F_HIT_CUP_HAND	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_GYM")				fScenePercent = 100 eReqScene = PR_SCENE_F_GYM	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_WALKCHOP")		fScenePercent = 100 eReqScene = PR_SCENE_F0_WALKCHOP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_PLAYCHOP")		fScenePercent = 100 eReqScene = PR_SCENE_F0_PLAYCHOP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_PLAYCHOP")		fScenePercent = 100 eReqScene = PR_SCENE_F1_PLAYCHOP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_WALKCHOP_a")		fScenePercent = 100 eReqScene = PR_SCENE_F_WALKCHOP_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_WALKCHOP_b")		fScenePercent = 100 eReqScene = PR_SCENE_F_WALKCHOP_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_TRAFFIC_a")		fScenePercent = 100 eReqScene = PR_SCENE_F_TRAFFIC_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_TRAFFIC_b")		fScenePercent = 100 eReqScene = PR_SCENE_F_TRAFFIC_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_TRAFFIC_c")		fScenePercent = 100 eReqScene = PR_SCENE_F_TRAFFIC_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_BIKE")			fScenePercent = 100 eReqScene = PR_SCENE_F0_BIKE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F0_CLEANCAR")		fScenePercent = 100 eReqScene = PR_SCENE_F0_CLEANCAR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_BIKE")			fScenePercent = 100 eReqScene = PR_SCENE_F1_BIKE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_CLEANCAR")		fScenePercent = 100 eReqScene = PR_SCENE_F1_CLEANCAR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F1_BYETAXI")		fScenePercent = 100 eReqScene = PR_SCENE_F1_BYETAXI	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BIKE_c")			fScenePercent = 100 eReqScene = PR_SCENE_F_BIKE_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BIKE_d")			fScenePercent = 100 eReqScene = PR_SCENE_F_BIKE_d	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_LAMTAUNT_P1")		fScenePercent = 100 eReqScene = PR_SCENE_F_LAMTAUNT_P1	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_LAMTAUNT_P3")		fScenePercent = 100 eReqScene = PR_SCENE_F_LAMTAUNT_P3	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_LAMTAUNT_P5")		fScenePercent = 100 eReqScene = PR_SCENE_F_LAMTAUNT_P5	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_CLUB")			fScenePercent = 100 eReqScene = PR_SCENE_F_CLUB	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_CS_CHECKSHOE")	fScenePercent = 100 eReqScene = PR_SCENE_F_CS_CHECKSHOE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_CS_WIPEHANDS")	fScenePercent = 100 eReqScene = PR_SCENE_F_CS_WIPEHANDS	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_CS_WIPERIGHT")	fScenePercent = 100 eReqScene = PR_SCENE_F_CS_WIPERIGHT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_LAMTAUNT_NIGHT")	fScenePercent = 100 eReqScene = PR_SCENE_F_LAMTAUNT_NIGHT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BAR_a_01")		fScenePercent = 100 eReqScene = PR_SCENE_F_BAR_a_01	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BAR_b_01")		fScenePercent = 100 eReqScene = PR_SCENE_F_BAR_b_01	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BAR_c_02")		fScenePercent = 100 eReqScene = PR_SCENE_F_BAR_c_02	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BAR_d_02")		fScenePercent = 100 eReqScene = PR_SCENE_F_BAR_d_02	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_BAR_e_01")		fScenePercent = 100 eReqScene = PR_SCENE_F_BAR_e_01	 RETURN TRUE BREAK
			
			CASE HASH("PR_SCENE_F_S_EXILE2")		fScenePercent = 100 eReqScene = PR_SCENE_F_S_EXILE2	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_S_AGENCY_2A_a")	fScenePercent = 100 eReqScene = PR_SCENE_F_S_AGENCY_2A_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_F_S_AGENCY_2A_b")	fScenePercent = 100 eReqScene = PR_SCENE_F_S_AGENCY_2A_b	 RETURN TRUE BREAK
		ENDSWITCH
		SWITCH GET_HASH_KEY(sNamedSwitch)			/* trevor */			
			#IF NOT IS_JAPANESE_BUILD			
			CASE HASH("PR_SCENE_T_SHIT")			fScenePercent = 100 eReqScene = PR_SCENE_T_SHIT	 RETURN TRUE BREAK
			#ENDIF			
			CASE HASH("PR_SCENE_T_SC_MOCKLAPDANCE")	fScenePercent = 100 eReqScene = PR_SCENE_T_SC_MOCKLAPDANCE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_SC_BAR")			fScenePercent = 100 eReqScene = PR_SCENE_T_SC_BAR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_SC_CHASE")		fScenePercent = 100 eReqScene = PR_SCENE_T_SC_CHASE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_STRIPCLUB_out")	fScenePercent = 100 eReqScene = PR_SCENE_T_STRIPCLUB_out	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_ESCORTED_OUT")	fScenePercent = 100 eReqScene = PR_SCENE_T_ESCORTED_OUT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_CHATEAU_b")	fScenePercent = 100 eReqScene = PR_SCENE_T_CN_CHATEAU_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_CHATEAU_c")	fScenePercent = 100 eReqScene = PR_SCENE_T_CN_CHATEAU_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_CHATEAU_d")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_CHATEAU_d	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_GARBAGE_FOOD")	fScenePercent = 100 eReqScene = PR_SCENE_T_GARBAGE_FOOD	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_THROW_FOOD")		fScenePercent = 100 eReqScene = PR_SCENE_T_THROW_FOOD	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_SMOKEMETH")		fScenePercent = 100 eReqScene = PR_SCENE_T_SMOKEMETH	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FIGHTBBUILD")		fScenePercent = 100 eReqScene = PR_SCENE_T_FIGHTBBUILD	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_ANNOYSUNBATHERS")	fScenePercent = 100 eReqScene = PR_SCENE_T_ANNOYSUNBATHERS	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_BLOCK_CAMERA")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_BLOCK_CAMERA	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_GUITARBEATDOWN")	fScenePercent = 100 eReqScene = PR_SCENE_T_GUITARBEATDOWN	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_DOCKS_a")			fScenePercent = 100 eReqScene = PR_SCENE_T_DOCKS_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_DOCKS_b")			fScenePercent = 100 eReqScene = PR_SCENE_T_DOCKS_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_DOCKS_c")			fScenePercent = 100 eReqScene = PR_SCENE_T_DOCKS_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_DOCKS_d")			fScenePercent = 100 eReqScene = PR_SCENE_T_DOCKS_d	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_LINGERIE")		fScenePercent = 100 eReqScene = PR_SCENE_T_CR_LINGERIE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_FUNERAL")		fScenePercent = 100 eReqScene = PR_SCENE_T_CR_FUNERAL	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FIGHTBAR_a")		fScenePercent = 100 eReqScene = PR_SCENE_T_FIGHTBAR_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FIGHTBAR_b")		fScenePercent = 100 eReqScene = PR_SCENE_T_FIGHTBAR_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FIGHTBAR_c")		fScenePercent = 100 eReqScene = PR_SCENE_T_FIGHTBAR_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_YELLATDOORMAN")	fScenePercent = 100 eReqScene = PR_SCENE_T_YELLATDOORMAN	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FIGHTYAUCLUB_b")	fScenePercent = 100 eReqScene = PR_SCENE_T_FIGHTYAUCLUB_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FIGHTCASINO")		fScenePercent = 100 eReqScene = PR_SCENE_T_FIGHTCASINO	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_KONEIGHBOUR")		fScenePercent = 100 eReqScene = PR_SCENE_T_KONEIGHBOUR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_SCARETRAMP")		fScenePercent = 100 eReqScene = PR_SCENE_T_SCARETRAMP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_DUMPSTER")		fScenePercent = 100 eReqScene = PR_SCENE_T_CR_DUMPSTER	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_WAKETRASH_b")	fScenePercent = 100 eReqScene = PR_SCENE_T_CN_WAKETRASH_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_WAKEBEACH")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_WAKEBEACH	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_WAKEBARN")		fScenePercent = 100 eReqScene = PR_SCENE_T_CN_WAKEBARN	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_WAKETRAIN")	fScenePercent = 100 eReqScene = PR_SCENE_T_CN_WAKETRAIN	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_WAKEROOFTOP")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_WAKEROOFTOP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_WAKEMOUNTAIN")	fScenePercent = 100 eReqScene = PR_SCENE_T_CN_WAKEMOUNTAIN	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_ALLEYDRUNK")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_ALLEYDRUNK	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_SC_ALLEYDRUNK")	fScenePercent = 100 eReqScene = PR_SCENE_T_SC_ALLEYDRUNK	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_PUKEINTOFOUNT")	fScenePercent = 100 eReqScene = PR_SCENE_T_PUKEINTOFOUNT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_PARK_b")		fScenePercent = 100 eReqScene = PR_SCENE_T_CN_PARK_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_POLICE_a")		fScenePercent = 100 eReqScene = PR_SCENE_T_CR_POLICE_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_POLICE_b")		fScenePercent = 100 eReqScene = PR_SCENE_T_CN_POLICE_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_POLICE_c")		fScenePercent = 100 eReqScene = PR_SCENE_T_CN_POLICE_c	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_NAKED_BRIDGE")	fScenePercent = 100 eReqScene = PR_SCENE_T_NAKED_BRIDGE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_NAKED_GARDEN")	fScenePercent = 100 eReqScene = PR_SCENE_T_NAKED_GARDEN	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_NAKED_ISLAND")	fScenePercent = 100 eReqScene = PR_SCENE_T_NAKED_ISLAND	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_CHASECAR_a")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_CHASECAR_a	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_CHASECAR_b")	fScenePercent = 100 eReqScene = PR_SCENE_T_CN_CHASECAR_b	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_CHASEBIKE")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_CHASEBIKE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_CHASESCOOTER")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_CHASESCOOTER	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_BRIDGEDROP")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_BRIDGEDROP	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_RUDEATCAFE")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_RUDEATCAFE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CR_RAND_TEMPLE")	fScenePercent = 100 eReqScene = PR_SCENE_T_CR_RAND_TEMPLE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_UNDERPIER")		fScenePercent = 100 eReqScene = PR_SCENE_T_UNDERPIER	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_DRUNKHOWLING")	fScenePercent = 100 eReqScene = PR_SCENE_T_DRUNKHOWLING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_SC_DRUNKHOWLING")	fScenePercent = 100 eReqScene = PR_SCENE_T_SC_DRUNKHOWLING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDSAVEHOUSE")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDSAVEHOUSE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDSPOON_A")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDSPOON_A	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDSPOON_B")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDSPOON_B	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDSPOON_B2")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDSPOON_B2	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDSPOON_A2")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDSPOON_A2	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYD_BEAR")		fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYD_BEAR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYD_DOLL")		fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYD_DOLL	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDPINEAPPLE")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDPINEAPPLE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDCRYING_A")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDCRYING_A	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDCRYING_E0")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDCRYING_E0	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDCRYING_E1")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDCRYING_E1	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDCRYING_E2")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDCRYING_E2	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLOYDCRYING_E3")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLOYDCRYING_E3	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_SMOKECRYSTAL")	fScenePercent = 100 eReqScene = PR_SCENE_T6_SMOKECRYSTAL	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_METHLAB")		fScenePercent = 100 eReqScene = PR_SCENE_T6_METHLAB	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_HUNTING1")		fScenePercent = 100 eReqScene = PR_SCENE_T6_HUNTING1	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_HUNTING2")		fScenePercent = 100 eReqScene = PR_SCENE_T6_HUNTING2	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_HUNTING3")		fScenePercent = 100 eReqScene = PR_SCENE_T6_HUNTING3	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_TRAF_AIR")		fScenePercent = 100 eReqScene = PR_SCENE_T6_TRAF_AIR	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_DIGGING")		fScenePercent = 100 eReqScene = PR_SCENE_T6_DIGGING	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_FLUSHESFOOT")	fScenePercent = 100 eReqScene = PR_SCENE_T6_FLUSHESFOOT	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_CN_PIER")			fScenePercent = 100 eReqScene = PR_SCENE_T_CN_PIER	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T6_LAKE")			fScenePercent = 100 eReqScene = PR_SCENE_T6_LAKE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_FLYING_PLANE")	fScenePercent = 100 eReqScene = PR_SCENE_T_FLYING_PLANE	 RETURN TRUE BREAK
			CASE HASH("PR_SCENE_T_HEADINSINK")		fScenePercent = 100 eReqScene = PR_SCENE_T_HEADINSINK	 RETURN TRUE BREAK
			#IF NOT IS_JAPANESE_BUILD			
			CASE HASH("PR_SCENE_T_JERKOFF")			fScenePercent = 100 eReqScene = PR_SCENE_T_JERKOFF	 RETURN TRUE BREAK
			#ENDIF
		ENDSWITCH
		
		CASSERTLN(DEBUG_SWITCH, "command like arg \"sc_ForceNamedSwitch\" called with unknown param \"", sNamedSwitch, "\"")
	ENDIF
	#ENDIF					//#1508299
	
	
	BOOL bDescentOnlyScene = FALSE
	
	IF (ePed >= CHAR_MULTIPLAYER)
		
		CPRINTLN(DEBUG_SWITCH, "PR_SCENE_INVALID for (ePed >= CHAR_MULTIPLAYER)")
		
		eReqScene = PR_SCENE_INVALID
		fScenePercent = -1
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
	IF g_bMagDemoActive
//		SWITCH ePed
//			CASE CHAR_MICHAEL
//				fScenePercent = 100
//				eReqScene = PR_SCENE_M_MD_FBI2
//				RETURN (eReqScene <> PR_SCENE_INVALID)
//			BREAK
//			CASE CHAR_FRANKLIN
//				fScenePercent = 100
//				eReqScene = PR_SCENE_F_MD_KUSH_DOC		//PR_SCENE_F_KUSH_DOC_a
//				RETURN (eReqScene <> PR_SCENE_INVALID)
//			BREAK
//			CASE CHAR_TREVOR
//				fScenePercent = 100
//				eReqScene = PR_SCENE_T_SHIT
//				RETURN (eReqScene <> PR_SCENE_INVALID)
//			BREAK
//		ENDSWITCH
		SWITCH ePed
			CASE CHAR_MICHAEL
				fScenePercent = 100
				eReqScene = PR_SCENE_M4_EXITRESTAURANT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_FRANKLIN
				fScenePercent = 100
				eReqScene = PR_SCENE_F_MD_FRANKLIN2
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_TREVOR
				fScenePercent = 100
				eReqScene = PR_SCENE_T_NAKED_ISLAND
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
		ENDSWITCH
	ENDIF
	
	IF NOT bDescentOnlyScene
		//if is player char dead
		CONST_INT iNUMBER_OF_HOURS_DEAD	6
		IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
			IF NOT HasNumOfHoursPassedSincePedTimeStruct(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed], iNUMBER_OF_HOURS_DEAD)
				fScenePercent = 100
				eReqScene = PR_SCENE_HOSPITAL
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
			
			g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
		ENDIF
	ENDIF
	
	IF NOT bBypassDefaultSwitch
		IF NOT IS_PED_THE_CURRENT_PLAYER_PED(ePed)
			IF IsCharPresentInWorld(ePed)
				SWITCH ePed
					CASE CHAR_MICHAEL
						fScenePercent = 100
						eReqScene = PR_SCENE_M_DEFAULT
						RETURN (eReqScene <> PR_SCENE_INVALID)
					BREAK
					CASE CHAR_FRANKLIN
						fScenePercent = 100
						eReqScene = PR_SCENE_F_DEFAULT
						RETURN (eReqScene <> PR_SCENE_INVALID)
					BREAK
					CASE CHAR_TREVOR
						fScenePercent = 100
						eReqScene = PR_SCENE_T_DEFAULT
						RETURN (eReqScene <> PR_SCENE_INVALID)
					BREAK
					
				ENDSWITCH
			
			ENDIF
			
			IF NOT IS_PLAYER_PED_AVAILABLE(ePed)
				SWITCH ePed
					CASE CHAR_MICHAEL
					BREAK
					CASE CHAR_FRANKLIN
					BREAK
					CASE CHAR_TREVOR
					BREAK
				ENDSWITCH
				
				CASSERTLN(DEBUG_SWITCH, "PR_SCENE_INVALID for IS_PLAYER_PED_AVAILABLE ", GET_PLAYER_PED_STRING(ePed), ")???")
				
				eReqScene = PR_SCENE_INVALID
				fScenePercent = -1
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
		ELSE
			IF NOT ARE_STRINGS_EQUAL(GET_THIS_SCRIPT_NAME(), "debug")
			CPRINTLN(GET_THIS_SCRIPT_NAME(), ": PR_SCENE_INVALID for IS_PED_THE_CURRENT_PLAYER_PED")
			ENDIF
			
//			eReqScene = PR_SCENE_INVALID
//			fScenePercent = -1
//			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SWITCH, "bBypassDefaultSwitch is true...")
	ENDIF

	//pre-mission switches forced if mission is available
	SWITCH ePed
		CASE CHAR_MICHAEL
			
			// #1293475  temp comment out for Les [#784198]
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)
				fScenePercent = 100
				eReqScene = PR_SCENE_M_S_FAMILY4
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
			// 
			IF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_2)	//574569
			#IF USE_TU_CHANGES
			//Fix for 1654538. Don't attempt to switch into Exile2 if the mission's trigger has a leave area flag set.
			AND NOT g_sMissionActiveData[SP_MISSION_EXILE_2].leaveArea    
			#ENDIF
			
				fScenePercent = 100
				eReqScene = PR_SCENE_F_S_EXILE2
				RETURN (eReqScene <> PR_SCENE_INVALID)
			
			#IF USE_TU_CHANGES   
			ELIF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_2)
				//Fix for 1654538. If we have denied this mission switch due to a leave area flag, 
				//also clear Franklin's last known location to force a switch scene.
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = <<0,0,0>>
			#ENDIF
			
			ENDIF
			
			//AGENCY_HEIST_2A,2B,2C have been reduced down to AGENCY_HEIST_2. Commented out for now.
			//PLEASE REVIEW THIS CHANGE. Ben R
			
			IF IS_MISSION_AVAILABLE(SP_HEIST_AGENCY_2)
				fScenePercent = 100
				IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_2_AUTOTRIGGERED)
					eReqScene = PR_SCENE_F_S_AGENCY_2A_a
				ELSE
					eReqScene = PR_SCENE_F_S_AGENCY_2A_b
				ENDIF
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
//			IF IS_MISSION_AVAILABLE(SP_HEIST_AGENCY_2B)
//				fScenePercent = 100
//				eReqScene = PR_SCENE_F_S_AGENCY_2B
//				RETURN (eReqScene <> PR_SCENE_INVALID)
//			ENDIF
//			IF IS_MISSION_AVAILABLE(SP_HEIST_AGENCY_2C)
//				fScenePercent = 100
//				eReqScene = PR_SCENE_F_S_AGENCY_2C
//				RETURN (eReqScene <> PR_SCENE_INVALID)
//			ENDIF

		BREAK
		CASE CHAR_TREVOR
			//
		BREAK
	ENDSWITCH
	
	//should the override switch scene be used?
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_M_OVERRIDE)
		fScenePercent = 100
		eReqScene = PR_SCENE_M_OVERRIDE
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_F_OVERRIDE)
		fScenePercent = 100
		eReqScene = PR_SCENE_F_OVERRIDE
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_T_OVERRIDE)
		fScenePercent = 100
		eReqScene = PR_SCENE_T_OVERRIDE
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
	//should the next scene be a mission scene??
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_ARM3, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FAMILY1, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FAMILY3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
//		#IF IS_DEBUG_BUILD
//		SAVE_STRING_TO_DEBUG_FILE("SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FAMILY3, eReqScene, fScenePercent, sPedScene, sPassedScene)")
//		SAVE_NEWLINE_TO_DEBUG_FILE()
//		#ENDIF
		
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI2, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FBI2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FAMILY1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI4intro, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FBI4intro, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI4, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FBI4, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FBI5, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[2]
		FLOAT 					fPercentArray[2]
		
		eRequestArray[0] = PR_SCENE_M4_WASHFACE				fPercentArray[0] = 90.0
		eRequestArray[1] = PR_SCENE_M4_WATCHINGTV			fPercentArray[1] = 10.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_MICHAEL,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ma_FBI5), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_MICHAEL, PR_SCENE_Ma_FBI5,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FBI3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI4, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI5, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FBI5, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FAMILY4_a, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		
//		CONST_INT iNumOfMinutes	10
//		IF HasNumOfMinutesPassedSincePedTimeStruct(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed], iNumOfMinutes)
//			IF UpdatePostMissionInfo(g_savedGlobals.sPlayerData.sInfo, ePed, PR_SCENE_Ma_FAMILY4_b)
//			
//				fScenePercent = 100
//				eReqScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
//				RETURN (eReqScene <> PR_SCENE_INVALID)
//			ENDIF
//		ENDIF
//		
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FAMILY4_b, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FAMILY4, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_NICE1B, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_AGENCY1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_AGENCYprep1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_AGENCY3B, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_AGENCY2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_AGENCY3A, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_CARSTEAL1, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_CARSTEAL1, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_CARSTEAL1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_CARSTEAL1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_AGENCY2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_CARSTEAL2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FBI2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[3]
		FLOAT 					fPercentArray[3]
		
		IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
			IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
				eRequestArray[0] = PR_SCENE_F0_WALKCHOP		fPercentArray[0] = 80.0
				eRequestArray[1] = PR_SCENE_F0_SH_READING	fPercentArray[1] = 10.0
				eRequestArray[2] = PR_SCENE_F0_BIKE			fPercentArray[2] = 10.0
			ELSE
				eRequestArray[0] = PR_SCENE_F_BAR_c_02		fPercentArray[0] = 30.0
				eRequestArray[1] = PR_SCENE_F_LAMTAUNT_P3	fPercentArray[1] = 40.0
				eRequestArray[2] = PR_SCENE_F_BAR_e_01		fPercentArray[2] = 30.0
			ENDIF
		ELSE
			eRequestArray[0] = PR_SCENE_F1_PLAYCHOP 		fPercentArray[0] = 80.0
			eRequestArray[1] = PR_SCENE_F1_SH_READING		fPercentArray[1] = 10.0
			eRequestArray[2] = PR_SCENE_F1_BIKE				fPercentArray[2] = 10.0
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene, TRUE)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_FBI2), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_FBI2,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FBI4, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[4]
		FLOAT 					fPercentArray[4]
		
		eRequestArray[0] = PR_SCENE_T_KONEIGHBOUR		fPercentArray[0] = 85.0
		eRequestArray[1] = PR_SCENE_T_THROW_FOOD		fPercentArray[1] = 5.0
		eRequestArray[2] = PR_SCENE_T_GUITARBEATDOWN	fPercentArray[2] = 5.0
		eRequestArray[3] = PR_SCENE_T_CR_BLOCK_CAMERA	fPercentArray[3] = 5.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_TREVOR,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ta_FBI4), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_TREVOR, PR_SCENE_Ta_FBI4,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_DOCKS2B, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[6]
		FLOAT 					fPercentArray[6]
		
		IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
			eRequestArray[0] = PR_SCENE_F_THROW_CUP			fPercentArray[0] = 80.0
			eRequestArray[1] = PR_SCENE_F_BAR_c_02			fPercentArray[1] = 10.0
			eRequestArray[2] = PR_SCENE_F_LAMTAUNT_P1		fPercentArray[2] = 2.5
			eRequestArray[3] = PR_SCENE_F_LAMTAUNT_P3		fPercentArray[3] = 2.5
			eRequestArray[4] = PR_SCENE_F_LAMTAUNT_P5		fPercentArray[4] = 2.5
			eRequestArray[3] = PR_SCENE_F_GYM				fPercentArray[3] = 2.5
		ELSE
			eRequestArray[0] = PR_SCENE_F1_CLEANCAR			fPercentArray[0] = 80.0
			eRequestArray[1] = PR_SCENE_F1_SH_READING		fPercentArray[1] = 10.0
			eRequestArray[2] = PR_SCENE_F1_BIKE				fPercentArray[2] = 2.5
			eRequestArray[3] = PR_SCENE_F1_PLAYCHOP			fPercentArray[3] = 2.5
			eRequestArray[4] = PR_SCENE_F1_NEWHOUSE			fPercentArray[4] = 2.5
			eRequestArray[5] = PR_SCENE_F_GYM				fPercentArray[5] = 2.5
		ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_DOCKS2B), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_DOCKS2B,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FAMILY6, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#944265 & 1031378
		PED_REQUEST_SCENE_ENUM	eRequestArray[1]
		FLOAT 					fPercentArray[1]
		
		eRequestArray[0] = PR_SCENE_T_NAKED_ISLAND		fPercentArray[0] = 100.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_TREVOR,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ta_FAMILY6), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_TREVOR, PR_SCENE_Ta_FAMILY6,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FINALEprepD, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#1582249
		PED_REQUEST_SCENE_ENUM	eRequestArray[4]
		FLOAT 					fPercentArray[4]
		
		eRequestArray[0] = PR_SCENE_T_HEADINSINK		fPercentArray[0] = 80.0
		eRequestArray[1] = PR_SCENE_T6_FLUSHESFOOT		fPercentArray[1] = 10.0
		eRequestArray[2] = PR_SCENE_T6_SMOKECRYSTAL		fPercentArray[2] = 10.0
		#IF NOT IS_JAPANESE_BUILD
		eRequestArray[3] = PR_SCENE_T_SHIT				fPercentArray[3] = 10.0
		#ENDIF
		#IF IS_JAPANESE_BUILD
		eRequestArray[3] = PR_SCENE_T_SMOKEMETH			fPercentArray[3] = 10.0
		#ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_TREVOR,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ta_FINALEprepD), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_TREVOR, PR_SCENE_Ta_FINALEprepD,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FAMILY6, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#1286223
		PED_REQUEST_SCENE_ENUM	eRequestArray[2]
		FLOAT 					fPercentArray[2]
		
		eRequestArray[0] = PR_SCENE_M7_ROUNDTABLE		fPercentArray[0] = 50.0
		eRequestArray[1] = PR_SCENE_M7_KIDS_TV			fPercentArray[1] = 50.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_MICHAEL,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ma_FAMILY6), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_MICHAEL, PR_SCENE_Ma_FAMILY6,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_MARTIN1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#944265
		PED_REQUEST_SCENE_ENUM	eRequestArray[3]
		FLOAT 					fPercentArray[3]
		
		eRequestArray[0] = PR_SCENE_F1_NEWHOUSE				fPercentArray[0] = 50.0
		eRequestArray[1] = PR_SCENE_F_WALKCHOP_a			fPercentArray[1] = 25.0
		eRequestArray[2] = PR_SCENE_F_WALKCHOP_b			fPercentArray[2] = 25.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_MARTIN1), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_MARTIN1,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_TREVOR3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#944265
		PED_REQUEST_SCENE_ENUM	eRequestArray[3]
		FLOAT 					fPercentArray[3]
		
		eRequestArray[0] = PR_SCENE_M_POOLSIDE_a			fPercentArray[0] = 50.0
		eRequestArray[1] = PR_SCENE_M4_WATCHINGTV			fPercentArray[1] = 50.0
		eRequestArray[2] = PR_SCENE_M4_WASHFACE				fPercentArray[1] = 50.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_MICHAEL,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ma_TREVOR3), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_MICHAEL, PR_SCENE_Ma_TREVOR3,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_TREVOR3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#944265
		PED_REQUEST_SCENE_ENUM	eRequestArray[2]
		FLOAT 					fPercentArray[2]
		
		IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
			eRequestArray[0] = PR_SCENE_F0_CLEANCAR			fPercentArray[0] = 50.0
			eRequestArray[1] = PR_SCENE_F0_SH_READING		fPercentArray[1] = 50.0
		ELSE
			eRequestArray[0] = PR_SCENE_F1_CLEANCAR			fPercentArray[0] = 50.0
			eRequestArray[1] = PR_SCENE_F1_SH_READING		fPercentArray[1] = 50.0
		ENDIF
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_TREVOR3), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_TREVOR3,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FRANKLIN2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FRANKLIN2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FBI1end, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_MARTIN1, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_MARTIN1, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_RURAL2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		//#944265
		PED_REQUEST_SCENE_ENUM	eRequestArray[4]
		FLOAT 					fPercentArray[4]
		
		eRequestArray[0] = PR_SCENE_T_HEADINSINK		fPercentArray[0] = 80.0
		eRequestArray[1] = PR_SCENE_T_SMOKEMETH			fPercentArray[1] = 10.0
		eRequestArray[2] = PR_SCENE_T6_SMOKECRYSTAL		fPercentArray[2] = 10.0
		#IF NOT IS_JAPANESE_BUILD
		eRequestArray[3] = PR_SCENE_T_SHIT				fPercentArray[3] = 10.0
		#ENDIF
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_TREVOR,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene, TRUE)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ta_RURAL2A), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_TREVOR, PR_SCENE_Ta_RURAL2A,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_RURAL2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_RC_MRSP2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_RURAL1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_NICE2B, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_NICE2B, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_FTa_FRANKLIN1a, eReqScene, fScenePercent, sPedScene, sPassedScene)
//	OR SchedulePostMissionSpecificScene(ePed, PR_SCENE_FTa_FRANKLIN1b, eReqScene, fScenePercent, sPedScene, sPassedScene)
	OR SchedulePostMissionSpecificScene(ePed, PR_SCENE_FTa_FRANKLIN1c, eReqScene, fScenePercent, sPedScene, sPassedScene)
	OR SchedulePostMissionSpecificScene(ePed, PR_SCENE_FTa_FRANKLIN1d, eReqScene, fScenePercent, sPedScene, sPassedScene)
	OR SchedulePostMissionSpecificScene(ePed, PR_SCENE_FTa_FRANKLIN1e, eReqScene, fScenePercent, sPedScene, sPassedScene)
		
		PED_REQUEST_SCENE_ENUM	eRequestArray[5-1]
		FLOAT 					fPercentArray[5-1]
		
		SWITCH g_ePostFranklin1Switch
			CASE POST_FRANKLIN1_0_escapePoliceAlone
				//#1015706
				
				eRequestArray[0] = PR_SCENE_FTa_FRANKLIN1a			fPercentArray[0] = 25.0
				eRequestArray[1] = PR_SCENE_FTa_FRANKLIN1c			fPercentArray[1] = 25.0
				eRequestArray[2] = PR_SCENE_FTa_FRANKLIN1d			fPercentArray[3-1] = 25.0
				eRequestArray[3] = PR_SCENE_FTa_FRANKLIN1e			fPercentArray[4-1] = 25.0
				
				IF GetDesiredPedRequestSceneFromArray(ePed,
						eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//0_escapePoliceAlone")
					#ENDIF
					
					RETURN (eReqScene <> PR_SCENE_INVALID)
				ENDIF
			BREAK
			CASE POST_FRANKLIN1_1_TrevorStripclub
				
				IF GET_RANDOM_BOOL()
					eReqScene = PR_SCENE_T_SC_BAR
					fScenePercent = 50.0
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//1_TrevorStripclub")
					#ENDIF
					
					RETURN (eReqScene <> PR_SCENE_INVALID)
				ELSE
					eReqScene = PR_SCENE_T_SC_DRUNKHOWLING		
					fScenePercent= 50.0
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//1_TrevorStripclub")
					#ENDIF
					
					RETURN (eReqScene <> PR_SCENE_INVALID)
				ENDIF
			BREAK
			CASE POST_FRANKLIN1_2_Frank
				IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
					IF GET_RANDOM_BOOL()
						eReqScene = PR_SCENE_F0_SH_READING
						fScenePercent = 50.0
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//2_Frank")
						#ENDIF
						
						RETURN (eReqScene <> PR_SCENE_INVALID)
					ELSE
						eReqScene = PR_SCENE_F0_SH_PUSHUP_a		
						fScenePercent= 50.0
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//2_Frank")
						#ENDIF
						
						RETURN (eReqScene <> PR_SCENE_INVALID)
					ENDIF
				ELSE
					IF GET_RANDOM_BOOL()
						eReqScene = PR_SCENE_F_BAR_a_01
						fScenePercent = 50.0
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//2_Frank (allow safehouse)")
						#ENDIF
						
						RETURN (eReqScene <> PR_SCENE_INVALID)
					ELSE
						eReqScene = PR_SCENE_F_BAR_b_01		
						fScenePercent= 50.0
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_FTa_FRANKLIN1a), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene), "	//2_Frank (ignore safehouse)")
						#ENDIF
						
						RETURN (eReqScene <> PR_SCENE_INVALID)
					ENDIF
				ENDIF
			BREAK
			
			DEFAULT
				
			BREAK
		ENDSWITCH
				
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FRANKLIN2, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FRANKLIN2, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_EXILE2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[2]
		FLOAT 					fPercentArray[2]
		
		eRequestArray[0] = PR_SCENE_M6_MORNING_a		fPercentArray[0] = 95.0
		eRequestArray[1] = PR_SCENE_M6_DRINKINGBEER		fPercentArray[1] = 5.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_MICHAEL,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ma_EXILE2), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_MICHAEL, PR_SCENE_Ma_EXILE2,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_EXILE2, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_EXILE3, eReqScene, fScenePercent, sPedScene, sPassedScene, 3.0)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_EXILE3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_MICHAEL2, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_MICHAEL2, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_MICHAEL3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_MICHAEL3, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_DOCKS2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_DOCKS2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FINALE1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[5]
		FLOAT 					fPercentArray[5]
		
		eRequestArray[0] = PR_SCENE_F1_ONLAPTOP			fPercentArray[0] = 10.0
		eRequestArray[1] = PR_SCENE_F1_IRONING			fPercentArray[1] = 5.0
		eRequestArray[2] = PR_SCENE_F1_SNACKING			fPercentArray[2] = 75.0
		eRequestArray[3] = PR_SCENE_F1_SH_READING		fPercentArray[3] = 5.0
		eRequestArray[4] = PR_SCENE_F1_NEWHOUSE			fPercentArray[4] = 5.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene, TRUE)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_FINALE1), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_FINALE1,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FINALE1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_CARSTEAL4, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FINALE2intro, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FINALE2intro, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_DOCKS2B, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FINALE1, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_AGENCY3A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FINALE2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[3]
		FLOAT 					fPercentArray[3]
		
		eRequestArray[0] = PR_SCENE_M4_WASHFACE			fPercentArray[0] = 40.0
		eRequestArray[1] = PR_SCENE_M4_WATCHINGTV		fPercentArray[1] = 60.0
		eRequestArray[2] = PR_SCENE_INVALID				fPercentArray[2] = 0.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_MICHAEL,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene, TRUE)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ma_FINALE2A), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_MICHAEL, PR_SCENE_Ma_FINALE2A,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FINALE2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FINALE2B, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FINALE2B, eReqScene, fScenePercent, sPedScene, sPassedScene)
		RETURN (eReqScene <> PR_SCENE_INVALID)
	ENDIF
	
	
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FINALEA, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[2]
		FLOAT 					fPercentArray[2]
		
		eRequestArray[0] = PR_SCENE_F1_POOLSIDE_b			fPercentArray[0] = 95.0
		eRequestArray[1] = PR_SCENE_F1_BYETAXI				fPercentArray[1] = 5.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_FINALEA), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_FINALEA,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FINALEB, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[2]
		FLOAT 					fPercentArray[2]
		
		eRequestArray[0] = PR_SCENE_F1_POOLSIDE_b			fPercentArray[0] = 95.0
		eRequestArray[1] = PR_SCENE_F1_BYETAXI				fPercentArray[1] = 5.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_FINALEB), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_FINALEB,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_FINALEC, eReqScene, fScenePercent, sPedScene, sPassedScene)
	AND (GetMichaelScheduleStage() = MSS_M7_ReunitedWithFamily)	
		PED_REQUEST_SCENE_ENUM	eRequestArray[4]
		FLOAT 					fPercentArray[4]
		
		eRequestArray[0] = PR_SCENE_M7_WIFETENNIS		fPercentArray[0] = 85.0
		eRequestArray[1] = PR_SCENE_M7_ROUNDTABLE		fPercentArray[1] = 5.0
		eRequestArray[2] = PR_SCENE_M7_KIDS_TV			fPercentArray[2] = 5.0
		eRequestArray[3] = PR_SCENE_M7_GETSREADY		fPercentArray[3] = 5.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_MICHAEL,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Ma_FINALEC), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_MICHAEL, PR_SCENE_Ma_FINALEC,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_FINALEC, eReqScene, fScenePercent, sPedScene, sPassedScene)
		PED_REQUEST_SCENE_ENUM	eRequestArray[5]
		FLOAT 					fPercentArray[5]
		
		eRequestArray[0] = PR_SCENE_F1_BYETAXI			fPercentArray[0] = 95.0
		eRequestArray[1] = PR_SCENE_F1_POOLSIDE_b		fPercentArray[1] = 2.0
		eRequestArray[2] = PR_SCENE_F1_POOLSIDE_a		fPercentArray[2] = 1.0
		eRequestArray[3] = PR_SCENE_F1_WATCHINGTV		fPercentArray[3] = 1.0
		eRequestArray[4] = PR_SCENE_F_BAR_c_02			fPercentArray[4] = 1.0
		
		IF GetDesiredPedRequestSceneFromArray(CHAR_FRANKLIN,
				eRequestArray, fPercentArray, eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SchedulePostMissionSpecificScene(", Get_String_From_Ped_Request_Scene_Enum(PR_SCENE_Fa_FINALEC), ") chose ", Get_String_From_Ped_Request_Scene_Enum(eReqScene))
			#ENDIF
			
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
		
		IF FallenThroughPostMissionScene(CHAR_FRANKLIN, PR_SCENE_Fa_FINALEC,
				eReqScene, fScenePercent, sPedScene, sPassedScene)
			RETURN (eReqScene <> PR_SCENE_INVALID)
		ENDIF
	ENDIF
	
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ta_FINALE2A, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Ma_SOLOMON4, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
//	IF SchedulePostMissionSpecificScene(ePed, PR_SCENE_Fa_SOLOMON4, eReqScene, fScenePercent, sPedScene, sPassedScene)
//		RETURN (eReqScene <> PR_SCENE_INVALID)
//	ENDIF
	
	//was the friend active in the last game-hour
	TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed]
	IF Is_TIMEOFDAY_Valid(sLastTimeActive)
		IF NOT bBypassDefaultSwitch
			IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed], <<0,0,0>>, 1.5)
				IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, 3)
					IF PassedBlimpCheckOnLastAndNext(ePed)
						SWITCH ePed
							CASE CHAR_MICHAEL
								fScenePercent = 100
								eReqScene = PR_SCENE_M_DEFAULT
								RETURN (eReqScene <> PR_SCENE_INVALID)
							BREAK
							CASE CHAR_FRANKLIN
								fScenePercent = 100
								eReqScene = PR_SCENE_F_DEFAULT
								RETURN (eReqScene <> PR_SCENE_INVALID)
							BREAK
							CASE CHAR_TREVOR
								fScenePercent = 100
								eReqScene = PR_SCENE_T_DEFAULT
								RETURN (eReqScene <> PR_SCENE_INVALID)
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//should the next scene be a mission scene??
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_Fa_PHONECALL_ARM3)
		IF NOT PRIVATE_Is_PedRequest_TooCloseToCurrentPlayer(PR_SCENE_Fa_PHONECALL_ARM3, bDescentOnlyScene, 150.0)
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE)
				fScenePercent = 100
				eReqScene = PR_SCENE_Fa_PHONECALL_ARM3
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
			
			CLEAR_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
			g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
		ENDIF
	ENDIF
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_Fa_PHONECALL_FAM1)
		IF NOT PRIVATE_Is_PedRequest_TooCloseToCurrentPlayer(PR_SCENE_Fa_PHONECALL_FAM1, bDescentOnlyScene, 150.0)
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE)
				fScenePercent = 100
				eReqScene = PR_SCENE_Fa_PHONECALL_FAM1
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
			
			CLEAR_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
			g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
		ENDIF
	ENDIF
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_Fa_PHONECALL_FAM3)
		IF NOT PRIVATE_Is_PedRequest_TooCloseToCurrentPlayer(PR_SCENE_Fa_PHONECALL_FAM3, bDescentOnlyScene, 150.0)
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE)
				fScenePercent = 100
				eReqScene = PR_SCENE_Fa_PHONECALL_FAM3
				RETURN (eReqScene <> PR_SCENE_INVALID)
			ENDIF
			
			CLEAR_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
			g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
		ENDIF
	ENDIF
	
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_Fa_EXILE3)
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_5)
			CLEAR_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
			g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
		ENDIF
	ENDIF
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			GetMichaelSceneForCurrentTime(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		BREAK
		
		CASE CHAR_FRANKLIN
			GetFranklinSceneForCurrentTime(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		BREAK
		
		CASE CHAR_TREVOR
			GetTrevorSceneForCurrentTime(eReqScene, fScenePercent, sPedScene, sPassedScene, bDescentOnlyScene)
		BREAK
	ENDSWITCH
	
	IF (eReqScene = PR_SCENE_INVALID)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		
		str =("invalid switch schedule(")
		str +=(GET_PLAYER_PED_STRING(ePed))
		str +=(", ")
		str +=(PRIVATE_Get_Switch_Clock_Hours())
		str +=(":")
		str +=(PRIVATE_Get_Switch_Clock_Minutes())
		str +=(")")
		
		CPRINTLN(DEBUG_SWITCH, "GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME(", str, ")")
		
		g_bDebugPrint_SceneScheduleInfo = TRUE
		CASSERTLN(DEBUG_SWITCH, str)
		#ENDIF
		
		IF NOT Is_TIMEOFDAY_Valid(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed])
			g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed] = GET_CURRENT_TIMEOFDAY()
		ENDIF
		
		IF ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed], <<0,0,0>>, 1.5)
			g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF
		
		SWITCH ePed
			CASE CHAR_MICHAEL
				CASSERTLN(DEBUG_SWITCH, "Michael - \"invalid switch!!!!\"")
				
				fScenePercent = 100
				eReqScene = PR_SCENE_M_DEFAULT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_FRANKLIN
				CASSERTLN(DEBUG_SWITCH, "Franklin - \"invalid switch!!!!\"")
				
				fScenePercent = 100
				eReqScene = PR_SCENE_F_DEFAULT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
			CASE CHAR_TREVOR
				CASSERTLN(DEBUG_SWITCH, "Trevor - \"invalid switch!!!!\"")
				
				fScenePercent = 100
				eReqScene = PR_SCENE_T_DEFAULT
				RETURN (eReqScene <> PR_SCENE_INVALID)
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN (eReqScene <> PR_SCENE_INVALID)
ENDFUNC


FUNC INT PRIVATE_GetRoamingLastKnownSwitchStage(enumCharacterList sceneCharID, TIMEOFDAY sLastTimeActive, FLOAT &fCreateDistMultiplier)
	INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
	GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
	
	fCreateDistMultiplier = TO_FLOAT(iSeconds+(iMinutes*60)+(iHours*60*60)) / 3600.0
	
	IF (fCreateDistMultiplier < 0.0) fCreateDistMultiplier = 0.0 ENDIF
	IF (fCreateDistMultiplier > 1.0) fCreateDistMultiplier = 1.0 ENDIF
	
	IF fCreateDistMultiplier < 0.25
	AND NOT g_sDefaultPlayerSwitchState[sceneCharID].bVehicleCoordForced
		RETURN 0
	ELIF fCreateDistMultiplier < 0.5
		RETURN 1
	ELIF fCreateDistMultiplier < 0.75
		RETURN 2
	ELSE
		RETURN 3
	ENDIF

ENDFUNC
