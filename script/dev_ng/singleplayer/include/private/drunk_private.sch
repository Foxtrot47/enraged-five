USING "rage_builtins.sch"
USING "globals.sch"
USING "drunkNoticeboard_private.sch"

USING "script_player.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	drunk_private.sch
//		AUTHOR			:	Keith / Alwyn
//		DESCRIPTION		:	Private drunk functions that help the public functions.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// PURPOSE:	Find an empty slot in the Drunk Request array
//
// RETURN VALUE:	INT		Empty slot array position, or UNKNOWN_DRUNK_ARRAY_INDEX if none
FUNC INT Get_An_Empty_Drunk_Request_Slot()
	
	INT thisSlot = 0
	REPEAT MAX_NUMBER_OF_DRUNK_REQUESTS thisSlot
		IF (g_drunkRequests[thisSlot].status = DS_NO_STATUS)
			RETURN thisSlot
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX

ENDFUNC


// PURPOSE:	Get the Drunk Request Array Index for this ped
// 
// INPUT PARAMS:	paramPed		Ped Index of drunk ped
// RETURN VALUE:	INT				Drunk Request array index, or UNKNOWN_DRUNK_ARRAY_INDEX if there is no request
FUNC INT Get_Peds_Drunk_Request_Array_Index(PED_INDEX paramPed)

	INT thisSlot = 0
	REPEAT MAX_NUMBER_OF_DRUNK_REQUESTS thisSlot
		IF (g_drunkRequests[thisSlot].ped = paramPed)
			RETURN thisSlot
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX
ENDFUNC


// PURPOSE:	Find an empty slot in the Drunk Ped array
//
// RETURN VALUE:	INT		Empty slot array position, or UNKNOWN_DRUNK_ARRAY_INDEX if none
FUNC INT Get_An_Empty_Drunk_Ped_Slot()
	
	INT thisSlot = 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS thisSlot
		IF (g_drunkPeds[thisSlot].uniqueID = NO_UNIQUE_DRUNK_PED_ID)
			RETURN thisSlot
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX

ENDFUNC


// PURPOSE:	Get the Drunk Ped Array Index for this ped
// 
// INPUT PARAMS:	INT				Unique Drunk Ped ID of drunk ped
// RETURN VALUE:	INT				Drunk Ped array index, or UNKNOWN_DRUNK_ARRAY_INDEX if there is no request
FUNC INT Get_Peds_Drunk_Ped_Array_Index(INT paramUniqueID)

	INT thisSlot = 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS thisSlot
		IF (g_drunkPeds[thisSlot].uniqueID = paramUniqueID)
			RETURN thisSlot
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX

ENDFUNC


// PURPOSE:	Get the next Unique Drunk Ped ID, and advance the ID by one ready for the next drunk ped
//
// RETURN VALUE:	INT		The next unique Drunk Ped ID
FUNC INT Get_Next_Unique_Drunk_Ped_ID()
	
	INT theID = g_nextUniqueDrunkPedID
	g_nextUniqueDrunkPedID++
	
	RETURN theID

ENDFUNC


// PURPOSE:	Reset teh drunk camera variables
PROC Reset_Drunk_Camera_Variables()

	g_drunkCameraActive					= FALSE
	g_drunkCameraIndex					= NULL

	g_drunkCameraTimeout				= DRUNK_LEVEL_NOT_DRUNK
	g_drunkCameraTimeoutDuration		= DRUNK_DEFAULT_TIMEOUT_msec
	g_drunkCameraMotionBlur				= 0
	g_drunkCameraDesiredAmplitudeScalar	= 0
	g_drunkCameraActualAmplitudeScalar	= 0
	g_drunkCameraEffectStrength			= 1.0
	g_drunkCameraTimeCycleModifier		= 0.0
	g_drunkCameraTimeCycleName			= ""
	
ENDPROC


