//Auto generated private lookups for stat system
USING "globals.sch"


PROC MISSION_STAT_RESET_STAT_REGISTER(INT index)
	g_MissionStatTrackingArray[index].ivalue = 0
	g_MissionStatTrackingArray[index].fvalue = 0.0
	g_MissionStatTrackingArray[index].invalidationReason = MSSIR_VALID
	g_MissionStatTrackingArray[index].successshown = FALSE
ENDPROC
PROC MISSION_STAT_ADD_WATCH(ENUM_MISSION_STATS prototype)
	g_bMissionStatSystemPrimed = TRUE
	g_bMissionStatSystemMissionStarted = TRUE

	 IF g_iMissionStatsBeingTracked > (MAX_TRACKED_MISSION_STATS-1)
		SCRIPT_ASSERT("MISSION_STAT_ADD_WATCH: Attempted to register new watch stats when max was already reached")
		EXIT
	ENDIF
	MISSION_STAT_RESET_STAT_REGISTER(g_iMissionStatsBeingTracked)
	g_MissionStatTrackingArray[g_iMissionStatsBeingTracked].target = prototype
	++g_iMissionStatsBeingTracked
	IF g_MissionStatTrackingPrototypes[prototype].type = MISSION_STAT_TYPE_INNOCENTS_KILLED
	g_bTrackingInnocentsLogged = TRUE
	ENDIF
ENDPROC


PROC INTERNAL_GENERATED_ASSOCIATE_STATS_WITH_FLOW()
#if USE_CLF_DLC
	g_sMissionStaticData[SP_MISSION_CLF_TRAIN].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_FIN].statCount = 0						 
	g_sMissionStaticData[SP_MISSION_CLF_CIA_LIE].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_CIA_JET].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_CIA_DRO].statCount = 0						 	
	g_sMissionStaticData[SP_MISSION_CLF_KOR_PRO].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_KOR_RES].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_KOR_SUB].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_KOR_SAT].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_KOR_5].statCount = 0						 
	g_sMissionStaticData[SP_MISSION_CLF_RUS_PLA].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_RUS_CAR].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_RUS_VAS].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_RUS_SAT].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_RUS_JET].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_RUS_CLK].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ARA_1].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ARA_DEF].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ARA_FAKE].statCount = 0						 
	g_sMissionStaticData[SP_MISSION_CLF_ARA_TNK].statCount = 0						 
	g_sMissionStaticData[SP_MISSION_CLF_CAS_SET].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_CAS_PR1].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_CAS_PR2].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_CAS_PR3].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_CAS_HEI].statCount = 0		
	g_sMissionStaticData[SP_MISSION_CLF_ASS_POL].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_RET].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_CAB].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_GEN].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_SUB].statCount = 0						 
	g_sMissionStaticData[SP_MISSION_CLF_ASS_DEA].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_HEL].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_BAR].statCount = 0						 
	g_sMissionStaticData[SP_MISSION_CLF_ASS_STR].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_VIN].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_HNT].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_ASS_SKY].statCount = 0
	g_sMissionStaticData[SP_MISSION_CLF_RC1_1].statCount = 0
	
#endif
#if USE_NRM_DLC
	g_sMissionStaticData[SP_MISSION_NRM_SUR_START].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUR_AMANDA].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_SUR_TRACEY].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_SUR_MICHAEL].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_SUR_HOME].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUR_JIMMY].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUR_PARTY].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUR_CURE].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_RESCUE_ENG].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_RESCUE_MED].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_RESCUE_GUN].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_SUP_FUEL].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUP_AMMO].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUP_MEDS].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_SUP_FOOD].statCount = 0	
	g_sMissionStaticData[SP_MISSION_NRM_RADIO_A].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_RADIO_B].statCount = 0
	g_sMissionStaticData[SP_MISSION_NRM_RADIO_C].statCount = 0
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].statCount = 9
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[0] = ARM1_NOSCRATCH 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[1] = ARM1_CAR_CHOSEN 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[2] = ARM1_LOSE_WANTED_LVL 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[3] = ARM1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[4] = ARM1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[5] = ARM1_WINNER 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[6] = ARM1_HIT_ALIENS 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[7] = ARM1_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_1].stats[8] = ARM1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].statCount = 12
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[0] = ARM2_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[1] = ARM2_PETROL_FIRE 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[2] = ARM2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[3] = ARM2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[4] = ARM2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[5] = ARM2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[6] = ARM2_KILLS 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[7] = ARM2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[8] = ARM2_PLAYER_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[9] = ARM2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[10] = ARM2_CRASH_BIKER 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_2].stats[11] = ARM2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].statCount = 9
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[0] = ARM3_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[1] = ARM3_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[2] = ARM3_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[3] = ARM3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[4] = ARM3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[5] = ARM3_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[6] = ARM3_ACTION_CAM_TIME 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[7] = ARM3_COUNTERS 
	g_sMissionStaticData[SP_MISSION_ARMENIAN_3].stats[8] = ARM3_GARDEN_KO 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].statCount = 13
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[0] = ASS1_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[1] = ASS1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[2] = ASS1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[3] = ASS1_KILLS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[4] = ASS1_NOT_SPOTTED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[5] = ASS1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[6] = ASS1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[7] = ASS1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[8] = ASS1_MIRROR_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[9] = ASS1_MIRROR_SNIPER_USED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[10] = ASS1_MIRROR_CASH 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[11] = ASS1_MIRROR_PERCENT 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_1].stats[12] = ASS1_MIRROR_MEDAL 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].statCount = 14
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[0] = ASS2_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[1] = ASS2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[2] = ASS2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[3] = ASS2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[4] = ASS2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[5] = ASS2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[6] = ASS2_KILLS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[7] = ASS2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[8] = ASS2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[9] = ASS2_MIRROR_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[10] = ASS2_MIRROR_QUICKBOOL 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[11] = ASS2_MIRROR_PERCENT 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[12] = ASS2_MIRROR_MEDAL 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_2].stats[13] = ASS2_MIRROR_CASH 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].statCount = 14
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[0] = ASS3_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[1] = ASS3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[2] = ASS3_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[3] = ASS3_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[4] = ASS3_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[5] = ASS3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[6] = ASS3_KILLS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[7] = ASS3_ACCURACY 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[8] = ASS3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[9] = ASS3_MIRROR_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[10] = ASS3_MIRROR_CLEAN_ESCAPE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[11] = ASS3_MIRROR_CASH 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[12] = ASS3_MIRROR_PERCENTAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_3].stats[13] = ASS3_MIRROR_MEDAL 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].statCount = 15
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[0] = ASS4_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[1] = ASS4_BUS_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[2] = ASS4_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[3] = ASS4_HIT_N_RUN 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[4] = ASS4_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[5] = ASS4_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[6] = ASS4_WANTED_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[7] = ASS4_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[8] = ASS4_ACCURACY 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[9] = ASS4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[10] = ASS4_MIRROR_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[11] = ASS4_MIRROR_HIT_AND_RUN 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[12] = ASS4_MIRROR_CASH 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[13] = ASS4_MIRROR_PERCENTAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_4].stats[14] = ASS4_MIRROR_MEDAL 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].statCount = 16
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[0] = ASS5_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[1] = ASS5_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[2] = ASS5_KILLS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[3] = ASS5_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[4] = ASS5_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[5] = ASS5_ACCURACY 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[6] = ASS5_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[7] = ASS5_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[8] = ASS5_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[9] = ASS5_ESCAPE_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[10] = ASS5_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[11] = ASS5_MIRROR_TIME 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[12] = ASS5_MIRROR_NO_FLY 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[13] = ASS5_MIRROR_CASH 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[14] = ASS5_MIRROR_PERCENT 
	g_sMissionStaticData[SP_MISSION_ASSASSIN_5].stats[15] = ASS5_MIRROR_MEDAL 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].statCount = 9
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[0] = CS2_NO_SCRATCH 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[1] = CS2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[2] = CS2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[3] = CS2_TARGETS_SCANNED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[4] = CS2_CHAD_FOCUS_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[5] = CS2_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[6] = CS2_EAVESDROPPER 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[7] = CS2_SCANMAN 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_2].stats[8] = CS2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].statCount = 9
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[0] = CS1_DROVE_BETWEEN_TRUCKS 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[1] = CS1_DROVE_BETWEEN_BUSES 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[2] = CS1_DROVE_THROUGH_TUNNEL 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[3] = CS1_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[4] = CS1_SPECIAL_USED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[5] = CS1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[6] = CS1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[7] = CS1_SWITCHES 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_1].stats[8] = CS1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].statCount = 9
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[0] = CS3_NO_SCRATCH 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[1] = CS3_FASTEST_SPEED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[2] = CS3_ACTOR_KNOCKOUT 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[3] = CS3_EJECTOR_SEAT_USED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[4] = CS3_RAN_OVER_ACTOR_AGAIN 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[5] = CS3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[6] = CS3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[7] = CS3_DAMAGE 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_3].stats[8] = CS3_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].statCount = 7
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[0] = CS4_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[1] = CS4_NO_SCRATCH 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[2] = CS4_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[3] = CS4_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[4] = CS4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[5] = CS4_COP_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_CARSTEAL_4].stats[6] = CS4_SHREDS 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].statCount = 12
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[0] = CHI1_BODY_COUNT 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[1] = CHI1_UNMARKED 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[2] = CHI1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[3] = CHI1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[4] = CHI1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[5] = CHI1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[6] = CHI1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[7] = CHI1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[8] = CHI1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[9] = CHI1_VEHICLES_DESTROYED 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[10] = CHI1_GRENADE_LAUNCHER_KILLS 
	g_sMissionStaticData[SP_MISSION_CHINESE_1].stats[11] = CHI1_TIME 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].statCount = 13
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[0] = CHI2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[1] = CHI2_UNMARKED 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[2] = CHI2_ONE_SHOT_TWO_KILLS 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[3] = CHI2_KILLS 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[4] = CHI2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[5] = CHI2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[6] = CHI2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[7] = CHI2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[8] = CHI2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[9] = CHI2_GAS_POUR_TIME 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[10] = CHI2_GAS_USED 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[11] = CHI2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_CHINESE_2].stats[12] = CHI2_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_1].statCount = 11
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[0] = EXL1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[1] = EXL1_FLYING_HIGH 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[2] = EXL1_KILLS 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[3] = EXL1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[4] = EXL1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[5] = EXL1_DAMAGE 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[6] = EXL1_FREEFALL_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[7] = EXL1_HARRIER_CAM_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[8] = EXL1_BAIL_IN_CAR 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[9] = EXL1_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_1].stats[10] = EXL1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_2].statCount = 13
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[0] = EXL2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[1] = EXL2_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[2] = EXL2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[3] = EXL2_ANIMAL_KILLED 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[4] = EXL2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[5] = EXL2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[6] = EXL2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[7] = EXL2_TIME_AS_CHOP 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[8] = EXL2_DEATH_FROM_ABOVE 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[9] = EXL2_SWITCHES 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[10] = EXL2_KILLS 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[11] = EXL2_DAMAGE 
	g_sMissionStaticData[SP_MISSION_EXILE_2].stats[12] = EXL2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_3].statCount = 11
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[0] = EXL3_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[1] = EXL3_FASTEST_SPEED 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[2] = EXL3_JUMPED_AT_FIRST_OPPERTUNITY 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[3] = EXL3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[4] = EXL3_KILLS 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[5] = EXL3_DAMAGE 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[6] = EXL3_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[7] = EXL3_ACCURACY 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[8] = EXL3_SWITCHES 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[9] = EXL3_TRAIN_TIME 
	g_sMissionStaticData[SP_MISSION_EXILE_3].stats[10] = EXL3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].statCount = 9
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[0] = FAM1_QUICK_CATCH 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[1] = FAM1_NOSCRATCH 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[2] = FAM1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[3] = FAM1_QUICK_BOARDING 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[4] = FAM1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[5] = FAM1_KILLS 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[6] = FAM1_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[7] = FAM1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_1].stats[8] = FAM1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].statCount = 12
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[0] = FAM2_FASTEST_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[1] = FAM2_FELL_OFF_BIKE 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[2] = FAM2_FAST_SWIM 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[3] = FAM2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[4] = FAM2_MAX_CAR_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[5] = FAM2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[6] = FAM2_CYCLING_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[7] = FAM2_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[8] = FAM2_LOSE_JETSKI 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[9] = FAM2_CHOICES 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[10] = FAM2_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_2].stats[11] = FAM2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].statCount = 12
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[0] = FAM3_NOSCRATCH 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[1] = FAM3_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[2] = FAM3_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[3] = FAM3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[4] = FAM3_GOON_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[5] = FAM3_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[6] = FAM3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[7] = FAM3_KILLS 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[8] = FAM3_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[9] = FAM3_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[10] = FAM3_VEHICLE_KILLS 
	g_sMissionStaticData[SP_MISSION_FAMILY_3].stats[11] = FAM3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].statCount = 10
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[0] = FAM4_LORRY_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[1] = FAM4_GOT_TOO_FAR_FROM_LAZ 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[2] = FAM4_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[3] = FAM4_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[4] = FAM4_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[5] = FAM4_LAZLOW_CAM_USE 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[6] = FAM4_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[7] = FAM4_COORD_KO 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[8] = FAM4_TRUCK_UNHOOKED 
	g_sMissionStaticData[SP_MISSION_FAMILY_4].stats[9] = FAM4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].statCount = 10
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[0] = FAM5_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[1] = FAM5_WARRIOR_POSE 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[2] = FAM5_TRIANGLE_POSE 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[3] = FAM5_SUN_SALUTATION_POSE 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[4] = FAM5_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[5] = FAM5_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[6] = FAM5_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[7] = FAM5_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[8] = FAM5_HOME_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_5].stats[9] = FAM5_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].statCount = 8
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[0] = FAM6_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[1] = FAM6_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[2] = FAM6_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[3] = FAM6_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[4] = FAM6_PIERCING_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[5] = FAM6_TATTOO_TIME 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[6] = FAM6_FRONT_OR_BACK 
	g_sMissionStaticData[SP_MISSION_FAMILY_6].stats[7] = FAM6_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_INTRO].statCount = 0
	g_sMissionStaticData[SP_MISSION_FINALE_C1].statCount = 16
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[0] = FIN_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[1] = FIN_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[2] = FIN_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[3] = FIN_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[4] = FIN_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[5] = FIN_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[6] = FIN_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[7] = FIN_KILLS 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[8] = FIN_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[9] = FIN_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[10] = FIN_STICKY_BOMBS_USED 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[11] = FIN_SNIPER_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[12] = FIN_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[13] = FIN_CHENG_BOMBER 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[14] = FIN_HAINES_HEADSHOT 
	g_sMissionStaticData[SP_MISSION_FINALE_C1].stats[15] = FIN_STRETCH_STRONGARM 
	g_sMissionStaticData[SP_MISSION_FINALE_CREDITS].statCount = 0
	g_sMissionStaticData[SP_MISSION_FBI_1].statCount = 10
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[0] = FBI1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[1] = FBI1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[2] = FBI1_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[3] = FBI1_SPECKILLS 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[4] = FBI1_UNMARKED 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[5] = FBI1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[6] = FBI1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[7] = FBI1_KILLS 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[8] = FBI1_COP_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_1].stats[9] = FBI1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_2].statCount = 11
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[0] = FBI2_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[1] = FBI2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[2] = FBI2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[3] = FBI2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[4] = FBI2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[5] = FBI2_KILLS 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[6] = FBI2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[7] = FBI2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[8] = FBI2_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[9] = FBI2_RAPPEL_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_2].stats[10] = FBI2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_3].statCount = 12
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[0] = FBI3_HEART_STOPPED 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[1] = FBI3_ELECTROCUTION 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[2] = FBI3_TOOTH 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[3] = FBI3_WRENCH 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[4] = FBI3_WATERBOARD 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[5] = FBI3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[6] = FBI3_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[7] = FBI3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[8] = FBI3_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[9] = FBI3_ASSASSIN_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[10] = FBI3_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_3].stats[11] = FBI3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4].statCount = 11
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[0] = FBI4_UNMARKED 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[1] = FBI4_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[2] = FBI4_HELISHOT 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[3] = FBI4_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[4] = FBI4_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[5] = FBI4_KILLS 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[6] = FBI4_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[7] = FBI4_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[8] = FBI4_STICKY_BOMB_KILLS 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[9] = FBI4_VEHICLES_DESTROYED 
	g_sMissionStaticData[SP_MISSION_FBI_4].stats[10] = FBI4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_1].statCount = 5
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_1].stats[0] = FBI4P1_TIME_TAKEN 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_1].stats[1] = FBI4P1_VEHICLE_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_1].stats[2] = FBI4P1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_1].stats[3] = FBI4P1_LEVEL_LOSS 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_1].stats[4] = FBI4P1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_2].statCount = 4
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_2].stats[0] = FBI4P2_TIME_TAKEN 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_2].stats[1] = FBI4P2_VEHICLE_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_2].stats[2] = FBI4P2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_2].stats[3] = FBI4P2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_5].statCount = 12
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[0] = FBI5_STUNS 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[1] = FBI5_KILLS 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[2] = FBI5_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[3] = FBI5_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[4] = FBI5_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[5] = FBI5_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[6] = FBI5_BAR_CUT_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[7] = FBI5_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[8] = FBI5_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[9] = FBI5_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[10] = FBI5_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_5].stats[11] = FBI5_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].statCount = 8
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[0] = FRA0_NOSCRATCH 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[1] = FRA0_DOGCAM 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[2] = FRA0_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[3] = FRA0_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[4] = FRA0_DOGGY_STYLE 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[5] = FRA0_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[6] = FRA0_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_0].stats[7] = FRA0_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].statCount = 14
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[0] = FRA1_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[1] = FRA1_FRANKLIN_KILLS 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[2] = FRA1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[3] = FRA1_TREVOR_KILLS 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[4] = FRA1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[5] = FRA1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[6] = FRA1_KILLS 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[7] = FRA1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[8] = FRA1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[9] = FRA1_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[10] = FRA1_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[11] = FRA1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[12] = FRA1_COP_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_1].stats[13] = FRA1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].statCount = 12
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[0] = FRA2_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[1] = FRA2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[2] = FRA2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[3] = FRA2_KILLS 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[4] = FRA2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[5] = FRA2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[6] = FRA2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[7] = FRA2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[8] = FRA2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[9] = FRA2_SWITCHES 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[10] = FRA2_VEHICLE_CHOSEN 
	g_sMissionStaticData[SP_MISSION_FRANKLIN_2].stats[11] = FRA2_TIME 
	g_sMissionStaticData[SP_MISSION_LAMAR].statCount = 12
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[0] = LAM1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[1] = LAM1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[2] = LAM1_UNMARKED 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[3] = LAM1_TIME 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[4] = LAM1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[5] = LAM1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[6] = LAM1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[7] = LAM1_KILLS 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[8] = LAM1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[9] = LAM1_COP_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[10] = LAM1_BODY_ARMOR 
	g_sMissionStaticData[SP_MISSION_LAMAR].stats[11] = LAM1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_LESTER_1].statCount = 9
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[0] = LES1A_TIME 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[1] = LES1A_CLEAR_POPUPS 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[2] = LES1A_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[3] = LES1A_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[4] = LES1A_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[5] = LES1A_MOUSE_CLICKS 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[6] = LES1A_POPUPS_CLOSED 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[7] = LES1B_TIME_WATCHING 
	g_sMissionStaticData[SP_MISSION_LESTER_1].stats[8] = LES1A_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].statCount = 7
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[0] = MAR1_FASTEST_SPEED 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[1] = MAR1_FELL_OFF_THE_BIKE 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[2] = MAR1_ONE_SHOT 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[3] = MAR1_TIME 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[4] = MAR1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[5] = MAR1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_MARTIN_1].stats[6] = MAR1_PLANE_HIT_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].statCount = 13
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[0] = MIC1_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[1] = MIC1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[2] = MIC1_KILLS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[3] = MIC1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[4] = MIC1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[5] = MIC1_LANDING_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[6] = MIC1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[7] = MIC1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[8] = MIC1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[9] = MIC1_PLANE_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[10] = MIC1_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[11] = MIC1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_1].stats[12] = MIC1_STARTING_CHAR 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].statCount = 16
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[0] = MIC2_TIMES_SWITCHED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[1] = MIC2_ACCURACY 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[2] = MIC2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[3] = MIC2_MIKE_RESCUE_TIMER 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[4] = MIC2_WAYPOINT_USED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[5] = MIC2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[6] = MIC2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[7] = MIC2_KILLS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[8] = MIC2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[9] = MIC2_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[10] = MIC2_TIME_TO_LOSE_TRIADS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[11] = MIC2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[12] = MIC2_TIME_TO_SAVE_FRANKLIN 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[13] = MIC2_UNIQUE_TRIAD_DEATHS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[14] = MIC2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_2].stats[15] = MIC2_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].statCount = 12
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[0] = MIC3_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[1] = MIC3_KILLS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[2] = MIC3_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[3] = MIC3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[4] = MIC3_ACCURACY 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[5] = MIC3_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[6] = MIC3_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[7] = MIC3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[8] = MIC3_SWITCHES 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[9] = MIC3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[10] = MIC3_VEHICLES_DESTROYED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_3].stats[11] = MIC3_HELIKILL 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].statCount = 13
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[0] = MIC4_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[1] = MIC4_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[2] = MIC4_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[3] = MIC4_DAMAGE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[4] = MIC4_KILLS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[5] = MIC4_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[6] = MIC4_ACCURACY 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[7] = MIC4_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[8] = MIC4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[9] = MIC4_HOME_TIME 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[10] = MIC4_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[11] = MIC4_HEADSHOT_RESCUE 
	g_sMissionStaticData[SP_MISSION_MICHAEL_4].stats[12] = MIC4_VEHICLE_CHOSEN 
	g_sMissionStaticData[SP_MISSION_ME_AMANDA].statCount = 0
	g_sMissionStaticData[SP_MISSION_ME_JIMMY].statCount = 0
	g_sMissionStaticData[SP_MISSION_ME_TRACEY].statCount = 0
	g_sMissionStaticData[SP_MISSION_PROLOGUE].statCount = 12
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[0] = PRO_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[1] = PRO_MICHAEL_SAVE_TIME 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[2] = PRO_GETAWAY_TIME 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[3] = PRO_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[4] = PRO_KILLS 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[5] = PRO_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[6] = PRO_ACCURACY 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[7] = PRO_DAMAGE 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[8] = PRO_STAY_ON_ROAD 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[9] = PRO_SWITCHES 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[10] = PRO_TIME 
	g_sMissionStaticData[SP_MISSION_PROLOGUE].stats[11] = PRO_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SHRINK_1].statCount = 0
	g_sMissionStaticData[SP_MISSION_SHRINK_2].statCount = 0
	g_sMissionStaticData[SP_MISSION_SHRINK_3].statCount = 0
	g_sMissionStaticData[SP_MISSION_SHRINK_4].statCount = 0
	g_sMissionStaticData[SP_MISSION_SHRINK_5].statCount = 0
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].statCount = 8
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[0] = SOL2_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[1] = SOL2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[2] = SOL2_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[3] = SOL2_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[4] = SOL2_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[5] = SOL2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[6] = SOL2_INNOCENT_KILLS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_2].stats[7] = SOL2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].statCount = 12
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[0] = SOL3_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[1] = SOL3_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[2] = SOL3_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[3] = SOL3_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[4] = SOL3_ACCURACY 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[5] = SOL3_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[6] = SOL3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[7] = SOL3_KILLS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[8] = SOL3_COP_LOSS_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[9] = SOL3_INNOCENT_KILLS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[10] = SOL3_NEWS_HELI_CAM_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_3].stats[11] = SOL3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].statCount = 13
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[0] = TRV1_NO_SURVIVORS 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[1] = TRV1_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[2] = TRV1_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[3] = TRV1_TRAILER_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[4] = TRV1_BIKERS_KILLED_BEFORE_LOCATION 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[5] = TRV1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[6] = TRV1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[7] = TRV1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[8] = TRV1_KILLS 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[9] = TRV1_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[10] = TRV1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[11] = TRV1_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_TREVOR_1].stats[12] = TRV1_ACCURACY 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].statCount = 16
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[0] = TRV2_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[1] = TRV2_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[2] = TRV2_RACEBACK_WON 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[3] = TRV2_UNDERBRIDGES 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[4] = TRV2_RACE_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[5] = TRV2_ATV_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[6] = TRV2_ATV_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[7] = TRV2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[8] = TRV2_KILLS 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[9] = TRV2_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[10] = TRV2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[11] = TRV2_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[12] = TRV2_PLANE_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[13] = TRV2_PLANE_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[14] = TRV2_SHOTS_TO_KILL_HELI 
	g_sMissionStaticData[SP_MISSION_TREVOR_2].stats[15] = TRV2_ALL_BIKERS_KILLED_ON_WING 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].statCount = 12
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[0] = TRV3_HEADSHOTS 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[1] = TRV3_NOT_DETECTED 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[2] = TRV3_UNMARKED 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[3] = TRV3_ALL_TRAILERS_AT_ONCE 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[4] = TRV3_STEALTH_KILLS 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[5] = TRV3_KILLS 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[6] = TRV3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[7] = TRV3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[8] = TRV3_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[9] = TRV3_ACCURACY 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[10] = TRV3_STICKY_BOMBS_USED 
	g_sMissionStaticData[SP_MISSION_TREVOR_3].stats[11] = TRV3_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].statCount = 7
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[0] = AH1_EAGLE_EYE 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[1] = AH1_MISSED_A_SPOT 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[2] = AH1_CLEANED_OUT 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[3] = AH1_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[4] = AH1_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[5] = AH1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_AGENCY_1].stats[6] = AH1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_2].statCount = 6
	g_sMissionStaticData[SP_HEIST_AGENCY_2].stats[0] = AH2_QUICK_GETAWAY 
	g_sMissionStaticData[SP_HEIST_AGENCY_2].stats[1] = AH2_DAMAGE 
	g_sMissionStaticData[SP_HEIST_AGENCY_2].stats[2] = AH2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_AGENCY_2].stats[3] = AH2_METHOD 
	g_sMissionStaticData[SP_HEIST_AGENCY_2].stats[4] = AH2_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_2].stats[5] = AH2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].statCount = 7
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[0] = AH1P_FIRETRUCK_SPEED 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[1] = AH1P_FACTORY_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[2] = AH1P_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[3] = AHP1_TRUCKCALLED 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[4] = AHP1_NOSCRATCH 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[5] = AH1P_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_PREP_1].stats[6] = AH1P_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].statCount = 14
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[0] = AH3A_KILLS 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[1] = AH3A_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[2] = AH3A_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[3] = AH3A_ACCURACY 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[4] = AH3A_DAMAGE 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[5] = AH3A_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[6] = AH3A_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[7] = AH3A_SWITCHES 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[8] = AH3A_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[9] = AH3A_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[10] = AH3A_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[11] = AH3A_OXYGEN_REMAINING 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[12] = AH3A_FLOOR_MOP_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3A].stats[13] = AH3A_ABSEIL_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].statCount = 15
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[0] = AH3B_SWITCHES 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[1] = AH3B_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[2] = AH3B_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[3] = AH3B_KILLS 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[4] = AH3B_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[5] = AH3B_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[6] = AH3B_DAMAGE 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[7] = AH3B_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[8] = AH3B_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[9] = AH3B_ACCURACY 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[10] = AH3B_FREEFALL_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[11] = AH3B_LANDING_ACCURACY 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[12] = AH3B_HACKING_ERRORS 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[13] = AH3B_HACKING_TIME 
	g_sMissionStaticData[SP_HEIST_AGENCY_3B].stats[14] = AH3B_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].statCount = 11
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[0] = DH1_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[1] = DH1_EMPLOYEE_OF_THE_MONTH 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[2] = DH1_PERFECT_SURVEILLANCE 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[3] = DH1_HONEST_DAYS_WORK 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[4] = DH1_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[5] = DH1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[6] = DH1_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[7] = DH1_FORKLIFT_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[8] = DH1_CRANE_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[9] = DH1_PHOTOS_TAKEN 
	g_sMissionStaticData[SP_HEIST_DOCKS_1].stats[10] = DH1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].statCount = 8
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[0] = DH1P_SUB_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[1] = DH1P_SUB_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[2] = DH1P_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[3] = DH1P_TRUCK_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[4] = DH1P_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[5] = DHP1_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[6] = DHP1_NO_BOARDING 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_1].stats[7] = DH1P_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].statCount = 10
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[0] = DH2BP_KILLS 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[1] = DH2BP_ACCURACY 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[2] = DH2BP_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[3] = DH2BP_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[4] = DH2BP_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[5] = DH2BP_HELI_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[6] = DH2BP_MAX_HELI_SPEED 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[7] = DH2BP_HELI_TAKEDOWN_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[8] = DHP2_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_PREP_2B].stats[9] = DH2BP_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].statCount = 16
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[0] = DH2A_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[1] = DH2A_ACCURACY 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[2] = DH2A_KILLS 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[3] = DH2A_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[4] = DH2A_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[5] = DH2A_ACTUAL_TAKE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[6] = DH2A_PERSONAL_TAKE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[7] = DH2A_SWITCHES 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[8] = DH2A_STEALTH_KILLS 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[9] = DH2A_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[10] = DH2A_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[11] = DH2A_TIME_TO_FIND_CONTAINER 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[12] = DH2A_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[13] = DH2A_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[14] = DH2A_NO_ALARMS 
	g_sMissionStaticData[SP_HEIST_DOCKS_2A].stats[15] = DH2A_TIME_TO_CLEAR 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].statCount = 16
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[0] = DH2B_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[1] = DH2B_ACTUAL_TAKE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[2] = DH2B_PERSONAL_TAKE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[3] = DH2B_SWITCHES 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[4] = DH2B_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[5] = DH2B_SUB_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[6] = DH2B_HELI_DAMAGE 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[7] = DH2B_TIME_TO_FIND_CONTAINER 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[8] = DH2B_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[9] = DH2B_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[10] = DH2B_ACCURACY 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[11] = DH2B_KILLED_ALL_MERRYWEATHER 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[12] = DH2B_ESCAPE_TIME 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[13] = DH2B_KILLS 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[14] = DH2B_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_DOCKS_2B].stats[15] = DH2B_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_FINALE_1].statCount = 12
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[0] = FH1_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[1] = FH1_HELI_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[2] = FH1_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[3] = FH1_MAX_HELI_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[4] = FH1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[5] = FH1_SWITCHES 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[6] = FH1_PERFECT_DISTANCE 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[7] = FH1_FIND_HOLE_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[8] = FH1_UNDER_BRIDGE 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[9] = FH1_THROUGH_TUNNEL 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[10] = FH1_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_1].stats[11] = FH1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2_INTRO].statCount = 0
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_A].statCount = 4
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_A].stats[0] = FHPA_ESCAPE_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_A].stats[1] = FHPA_DAMAGE_TO_WAGON 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_A].stats[2] = FHPA_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_A].stats[3] = FHPA_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].statCount = 7
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[0] = FHPB_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[1] = FHPB_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[2] = FHPB_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[3] = FHPB_TRUCK_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[4] = FHPB_SNEAK_THIEF 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[5] = FHPB_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_B].stats[6] = FHPB_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].statCount = 14
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[0] = FH2A_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[1] = FH2A_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[2] = FH2A_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[3] = FH2A_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[4] = FH2A_ACTUAL_TAKE 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[5] = FH2A_CREW_TAKE 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[6] = FH2A_KILLS 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[7] = FH2A_SWITCHES 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[8] = FH2A_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[9] = FH2A_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[10] = FH2A_TRAFFIC_LIGHT_CHANGES 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[11] = FH2A_PERSONAL_TAKE 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[12] = FH2A_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2A].stats[13] = FH2A_ACCURACY 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].statCount = 15
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[0] = FH2B_ACTUAL_TAKE 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[1] = FH2B_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[2] = FH2B_CREW_TAKE 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[3] = FH2B_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[4] = FH2B_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[5] = FH2B_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[6] = FH2B_KILLS 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[7] = FH2B_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[8] = FH2B_PERSONAL_TAKE 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[9] = FH2B_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[10] = FH2B_SWITCHES 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[11] = FH2B_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[12] = FH2B_CHASE_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[13] = FH2B_GOLD_DROP_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_2B].stats[14] = FH2B_ACCURACY 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].statCount = 7
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[0] = JH1_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[1] = JH1_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[2] = JH1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[3] = JH1_PHOTOS_TAKEN 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[4] = JH1_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[5] = JH1_PERFECT_PIC 
	g_sMissionStaticData[SP_HEIST_JEWELRY_1].stats[6] = JH1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].statCount = 6
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].stats[0] = JH1P_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].stats[1] = JH1P_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].stats[2] = JH1P_VAN_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].stats[3] = JHP1A_SNEAKY_PEST 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].stats[4] = JHP1A_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1A].stats[5] = JH1P_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].statCount = 7
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[0] = JH2AP_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[1] = JH2AP_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[2] = JH2AP_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[3] = JH2AP_FACTORY_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[4] = JHP2A_LOOSE_CARGO 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[5] = JHP2A_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].stats[6] = JHP2A_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_1].statCount = 9
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[0] = RH1_LEISURELY_DRIVE 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[1] = RH1_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[2] = RH1_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[3] = RH1_ACCURACY 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[4] = RH1_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[5] = RH1_SWITCHES 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[6] = RH1_WINNER 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[7] = RH1_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_1].stats[8] = RH1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].statCount = 11
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[0] = RH1P_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[1] = RH1P_KILLS 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[2] = RH1P_ACCURACY 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[3] = RH1P_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[4] = RH1P_DAMAGE 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[5] = RH1P_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[6] = RH1P_VAN_TO_LAB_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[7] = RH1P_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[8] = RH1P_CONVOY_STOPPED 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[9] = RH1P_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].stats[10] = RH1P_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_2].statCount = 12
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[0] = RH2_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[1] = RH2_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[2] = RH2_BULLETS_FIRED 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[3] = RH2_KILLS 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[4] = RH2_DAMAGE 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[5] = RH2_ACTUAL_TAKE 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[6] = RH2_PERSONAL_TAKE 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[7] = RH2_HEADSHOTS 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[8] = RH2_ACCURACY 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[9] = RH2_TIME 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[10] = RH2_COLLATERAL_DAMAGE 
	g_sMissionStaticData[SP_HEIST_RURAL_2].stats[11] = RH2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].statCount = 10
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[0] = JH2A_ACTUAL_TAKE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[1] = JH2A_PERSONAL_TAKE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[2] = JH2A_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[3] = JH2A_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[4] = JH2A_CASE_GRAB_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[5] = JH2A_FRANKLIN_BIKE_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[6] = JH2A_CASES_SMASHED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[7] = JH2A_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[8] = JH2A_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_2].stats[9] = JH2A_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_4].statCount = 4
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_4].stats[0] = FBI4P4_FACE_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_4].stats[1] = FBI4P4_CLICHE 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_4].stats[2] = FBI4P4_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_4].stats[3] = FBI4P4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_5].statCount = 4
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_5].stats[0] = FBI4P5_QUICK_SHOPPER 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_5].stats[1] = FBI4P5_UNITED_COLOURS 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_5].stats[2] = FBI4P5_TIME 
	g_sMissionStaticData[SP_MISSION_FBI_4_PREP_5].stats[3] = FBI4P5_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_A].statCount = 6
	g_sMissionStaticData[SP_MISSION_FINALE_A].stats[0] = FINA_KILLTREV 
	g_sMissionStaticData[SP_MISSION_FINALE_A].stats[1] = FINA_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_A].stats[2] = FINA_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FINALE_A].stats[3] = FINA_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FINALE_A].stats[4] = FINA_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FINALE_A].stats[5] = FINA_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_B].statCount = 9
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[0] = FINB_KILLMIC 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[1] = FINB_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[2] = FINB_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[3] = FINB_BULLETS_FIRED 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[4] = FINB_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[5] = FINB_CHOICE 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[6] = FINB_FOOTCHASE_TIME 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[7] = FINB_DAMAGE 
	g_sMissionStaticData[SP_MISSION_FINALE_B].stats[8] = FINB_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].statCount = 9
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[0] = SOL1_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[1] = SOL1_SILENT_TAKEDOWNS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[2] = SOL1_BRAWL_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[3] = SOL1_PERFECT_LANDING 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[4] = SOL1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[5] = SOL1_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[6] = SOL1_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[7] = SOL1_INNOCENT_KILLS 
	g_sMissionStaticData[SP_MISSION_SOLOMON_1].stats[8] = SOL1_HELI_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_4].statCount = 5
	g_sMissionStaticData[SP_MISSION_TREVOR_4].stats[0] = TRV4_TIME 
	g_sMissionStaticData[SP_MISSION_TREVOR_4].stats[1] = TRV4_MAX_SPEED 
	g_sMissionStaticData[SP_MISSION_TREVOR_4].stats[2] = TRV4_CAR_DAMAGE 
	g_sMissionStaticData[SP_MISSION_TREVOR_4].stats[3] = TRV4_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_MISSION_TREVOR_4].stats[4] = TRV4_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].statCount = 8
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[0] = FINPC1_MAPPED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[1] = FINPC1_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[2] = FINPC1_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[3] = FINPC1_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[4] = FINPC1_TIME_TO_GARAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[5] = FINPC1_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[6] = FINPC1_MOD_GAUNTLET 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C1].stats[7] = FINPC1_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].statCount = 8
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[0] = FINPC2_MAPPED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[1] = FINPC2_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[2] = FINPC2_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[3] = FINPC2_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[4] = FINPC2_TIME_TO_GARAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[5] = FINPC2_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[6] = FINPC2_MOD_GAUNTLET 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C2].stats[7] = FINPC2_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].statCount = 8
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[0] = FINPC3_MAPPED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[1] = FINPC3_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[2] = FINPC3_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[3] = FINPC3_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[4] = FINPC3_TIME_TO_GARAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[5] = FINPC3_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[6] = FINPC3_MOD_GAUNTLET 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_C3].stats[7] = FINPC3_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].statCount = 7
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[0] = FINPD_TIME 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[1] = FINPD_UNDETECTED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[2] = FINPD_KILLS 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[3] = FINPD_STEALTH_KILLS 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[4] = FINPD_MAX_HELI_SPEED 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[5] = FINPD_HELI_DAMAGE 
	g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].stats[6] = FINPD_SPECIAL_ABILITY_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].statCount = 7
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[0] = JHP1B_SWIFT_GETAWAY 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[1] = JHP1B_TIME 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[2] = JHP1B_CAR_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[3] = JHP1B_DAMAGE 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[4] = JHP1B_MAX_SPEED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[5] = JHP1B_INNOCENTS_KILLED 
	g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].stats[6] = JHP1B_SPECIAL_ABILITY_TIME 
#endif
#endif


ENDPROC



FUNC STRING GET_MISSION_STAT_NAME(ENUM_MISSION_STATS e)
	SWITCH(e)
		CASE ARM1_NOSCRATCH 
			RETURN "MISHSTA_0" 
		CASE ARM2_TIME 
			RETURN "MISHSTA_1" 
		CASE ARM2_PETROL_FIRE 
			RETURN "MISHSTA_2" 
		CASE ARM3_TIME 
			RETURN "MISHSTA_1" 
		CASE ARM3_DAMAGE 
			RETURN "MISHSTA_3" 
		CASE FRA0_NOSCRATCH 
			RETURN "MISHSTA_0" 
		CASE FRA0_DOGCAM 
			RETURN "MISHSTA_4" 
		CASE FAM1_QUICK_CATCH 
			RETURN "MISHSTA_5" 
		CASE FAM1_NOSCRATCH 
			RETURN "MISHSTA_0" 
		CASE FAM2_FASTEST_SPEED 
			RETURN "MISHSTA_6" 
		CASE FAM2_FELL_OFF_BIKE 
			RETURN "MISHSTA_7" 
		CASE FAM2_FAST_SWIM 
			RETURN "MISHSTA_8" 
		CASE FAM3_TIME 
			RETURN "MISHSTA_1" 
		CASE FAM3_NOSCRATCH 
			RETURN "MISHSTA_0" 
		CASE LES1A_TIME 
			RETURN "MISHSTA_1" 
		CASE LES1A_CLEAR_POPUPS 
			RETURN "MISHSTA_9" 
		CASE LAM1_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE LAM1_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE LAM1_TIME 
			RETURN "MISHSTA_1" 
		CASE LAM1_UNMARKED 
			RETURN "MISHSTA_12" 
		CASE TRV1_NO_SURVIVORS 
			RETURN "MISHSTA_13" 
		CASE TRV1_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE TRV1_TIME 
			RETURN "MISHSTA_1" 
		CASE TRV1_TRAILER_DAMAGE 
			RETURN "MISHSTA_14" 
		CASE TRV1_BIKERS_KILLED_BEFORE_LOCATION 
			RETURN "MISHSTA_15" 
		CASE TRV2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE TRV2_TIME 
			RETURN "MISHSTA_1" 
		CASE TRV2_RACEBACK_WON 
			RETURN "MISHSTA_16" 
		CASE TRV2_UNDERBRIDGES 
			RETURN "MISHSTA_17" 
		CASE TRV3_HEADSHOTS 
			RETURN "MISHSTA_11" 
	ENDSWITCH
	SWITCH(e)
		CASE TRV3_NOT_DETECTED 
			RETURN "MISHSTA_18" 
		CASE TRV3_UNMARKED 
			RETURN "MISHSTA_12" 
		CASE TRV3_ALL_TRAILERS_AT_ONCE 
			RETURN "MISHSTA_19" 
		CASE CHI1_UNMARKED 
			RETURN "MISHSTA_12" 
		CASE CHI1_BODY_COUNT 
			RETURN "MISHSTA_20" 
		CASE CHI2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE CHI2_UNMARKED 
			RETURN "MISHSTA_12" 
		CASE CHI2_ONE_SHOT_TWO_KILLS 
			RETURN "MISHSTA_21" 
		CASE FAM4_LORRY_SPEED 
			RETURN "MISHSTA_6" 
		CASE FAM4_GOT_TOO_FAR_FROM_LAZ 
			RETURN "MISHSTA_22" 
		CASE FBI1_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FBI1_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FBI1_TIME 
			RETURN "MISHSTA_1" 
		CASE FBI1_SPECKILLS 
			RETURN "MISHSTA_23" 
		CASE FBI1_UNMARKED 
			RETURN "MISHSTA_12" 
		CASE FBI2_TIME 
			RETURN "MISHSTA_1" 
		CASE FBI2_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FBI2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FRA1_TIME 
			RETURN "MISHSTA_1" 
		CASE FAM5_TIME 
			RETURN "MISHSTA_1" 
		CASE FAM5_WARRIOR_POSE 
			RETURN "MISHSTA_24" 
		CASE FAM5_TRIANGLE_POSE 
			RETURN "MISHSTA_25" 
		CASE FAM5_SUN_SALUTATION_POSE 
			RETURN "MISHSTA_26" 
		CASE FBI3_HEART_STOPPED 
			RETURN "MISHSTA_27" 
		CASE FBI3_ELECTROCUTION 
			RETURN "MISHSTA_28" 
		CASE FBI3_TOOTH 
			RETURN "MISHSTA_29" 
		CASE FBI3_WRENCH 
			RETURN "MISHSTA_30" 
		CASE FBI3_WATERBOARD 
			RETURN "MISHSTA_31" 
		CASE FBI4_HELISHOT 
			RETURN "MISHSTA_32" 
		CASE CS2_NO_SCRATCH 
			RETURN "MISHSTA_0" 
		CASE CS1_SPECIAL_USED 
			RETURN "MISHSTA_33" 
	ENDSWITCH
	SWITCH(e)
		CASE CS1_TIME 
			RETURN "MISHSTA_1" 
		CASE CS1_DROVE_BETWEEN_TRUCKS 
			RETURN "MISHSTA_34" 
		CASE CS1_DROVE_BETWEEN_BUSES 
			RETURN "MISHSTA_35" 
		CASE CS1_DROVE_THROUGH_TUNNEL 
			RETURN "MISHSTA_36" 
		CASE MAR1_TIME 
			RETURN "MISHSTA_1" 
		CASE MAR1_FASTEST_SPEED 
			RETURN "MISHSTA_37" 
		CASE MAR1_FELL_OFF_THE_BIKE 
			RETURN "MISHSTA_38" 
		CASE MAR1_ONE_SHOT 
			RETURN "MISHSTA_39" 
		CASE CS3_NO_SCRATCH 
			RETURN "MISHSTA_0" 
		CASE CS3_FASTEST_SPEED 
			RETURN "MISHSTA_6" 
		CASE CS3_ACTOR_KNOCKOUT 
			RETURN "MISHSTA_40" 
		CASE CS3_EJECTOR_SEAT_USED 
			RETURN "MISHSTA_41" 
		CASE CS3_RAN_OVER_ACTOR_AGAIN 
			RETURN "MISHSTA_42" 
		CASE EXL1_ACCURACY 
			RETURN "MISHSTA_43" 
		CASE EXL2_ACCURACY 
			RETURN "MISHSTA_43" 
		CASE EXL2_TIME 
			RETURN "MISHSTA_1" 
		CASE EXL2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE EXL2_ANIMAL_KILLED 
			RETURN "MISHSTA_44" 
		CASE EXL3_TIME 
			RETURN "MISHSTA_1" 
		CASE EXL3_FASTEST_SPEED 
			RETURN "MISHSTA_6" 
		CASE EXL3_JUMPED_AT_FIRST_OPPERTUNITY 
			RETURN "MISHSTA_45" 
		CASE FBI5_STUNS 
			RETURN "MISHSTA_46" 
		CASE CS4_TIME 
			RETURN "MISHSTA_1" 
		CASE CS4_NO_SCRATCH 
			RETURN "MISHSTA_0" 
		CASE SOL2_TIME 
			RETURN "MISHSTA_1" 
		CASE MIC1_TIME 
			RETURN "MISHSTA_1" 
		CASE MIC1_HEADSHOTS 
			RETURN "MISHSTA_47" 
		CASE MIC2_TIMES_SWITCHED 
			RETURN "MISHSTA_48" 
		CASE MIC2_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE MIC2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE MIC2_MIKE_RESCUE_TIMER 
			RETURN "MISHSTA_49" 
	ENDSWITCH
	SWITCH(e)
		CASE MIC2_WAYPOINT_USED 
			RETURN "MISHSTA_50" 
		CASE FAM6_TIME 
			RETURN "MISHSTA_1" 
		CASE SOL3_TIME 
			RETURN "MISHSTA_1" 
		CASE FIN_TIME 
			RETURN "MISHSTA_1" 
		CASE AH1_EAGLE_EYE 
			RETURN "MISHSTA_51" 
		CASE AH1_MISSED_A_SPOT 
			RETURN "MISHSTA_52" 
		CASE AH1_CLEANED_OUT 
			RETURN "MISHSTA_53" 
		CASE AH2_QUICK_GETAWAY 
			RETURN "MISHSTA_54" 
		CASE RH1_LEISURELY_DRIVE 
			RETURN "MISHSTA_55" 
		CASE DH1_TIME 
			RETURN "MISHSTA_1" 
		CASE DH1_EMPLOYEE_OF_THE_MONTH 
			RETURN "MISHSTA_56" 
		CASE DH1_PERFECT_SURVEILLANCE 
			RETURN "MISHSTA_57" 
		CASE DH1_HONEST_DAYS_WORK 
			RETURN "MISHSTA_58" 
		CASE BA1_KILLCHAIN 
			RETURN "MISHSTA_59" 
		CASE BA1_DAMAGE_TAKEN 
			RETURN "MISHSTA_60" 
		CASE BA2_VANS_DESTROYED 
			RETURN "MISHSTA_61" 
		CASE BA3A_DELIVERY_TIME 
			RETURN "MISHSTA_62" 
		CASE BA3C_DELIVERY_TIME 
			RETURN "MISHSTA_62" 
		CASE BA3C_STASH_UNHOOKED 
			RETURN "MISHSTA_63" 
		CASE DR1_DREYFUSS_KILLED 
			RETURN "MISHSTA_64" 
		CASE EP4_ARTIFACT_DETECTOR_USED 
			RETURN "MISHSTA_65" 
		CASE EP6_PERFECT_LANDING 
			RETURN "MISHSTA_66" 
		CASE EP8_MONEY_STOLEN 
			RETURN "MISHSTA_67" 
		CASE EP8_SECURITY_WIPED_OUT 
			RETURN "MISHSTA_68" 
		CASE EXT1_RACE_WON 
			RETURN "MISHSTA_69" 
		CASE EXT1_BIG_AIR 
			RETURN "MISHSTA_70" 
		CASE EXT1_FALL_TIME 
			RETURN "MISHSTA_71" 
		CASE EXT2_NUMBER_OF_SPINS 
			RETURN "MISHSTA_72" 
		CASE EXT2_LEAP_FROM_ATV_TO_WATER 
			RETURN "MISHSTA_73" 
		CASE EXT3_PERFECT_LANDING 
			RETURN "MISHSTA_74" 
		CASE EXT3_DARE_DEVIL 
			RETURN "MISHSTA_75" 
	ENDSWITCH
	SWITCH(e)
		CASE EXT4_FALL_SURVIVED 
			RETURN "MISHSTA_76" 
		CASE FA1_SHORTCUT_USED 
			RETURN "MISHSTA_77" 
		CASE FA3_SHORTCUT_USED 
			RETURN "MISHSTA_78" 
		CASE HU1_HIT_EACH_SATELLITE_FIRST_GO 
			RETURN "MISHSTA_79" 
		CASE HU1_TYRE_SHOOTING_ACCURACY 
			RETURN "MISHSTA_80" 
		CASE HU1_TWO_COYOTES_KILLED_IN_ONE 
			RETURN "MISHSTA_81" 
		CASE HU2_ELK_HEADSHOTS 
			RETURN "MISHSTA_82" 
		CASE HU2_DETECTED_BY_ELK 
			RETURN "MISHSTA_83" 
		CASE JO2_STOPPED_IN_TIME 
			RETURN "MISHSTA_84" 
		CASE JO2_JOSH_MELEED 
			RETURN "MISHSTA_85" 
		CASE JO3_POUR_FUEL_IN_ONE_GO 
			RETURN "MISHSTA_86" 
		CASE JO3_ESCAPE_WITHOUT_ALERTING_COPS 
			RETURN "MISHSTA_87" 
		CASE JO4_ESCAPE_IN_PARKED_COP_CAR 
			RETURN "MISHSTA_88" 
		CASE JO4_JOSH_KILLED_BEFORE_ESCAPE 
			RETURN "MISHSTA_89" 
		CASE MIN1_FAST_STOP 
			RETURN "MISHSTA_90" 
		CASE MIN1_VEHICLE_TAKEN_AFTER_STUNS 
			RETURN "MISHSTA_91" 
		CASE MIN2_STUNNED_ALL 
			RETURN "MISHSTA_92" 
		CASE MIN2_FIRST_CAPTURE 
			RETURN "MISHSTA_93" 
		CASE MIN2_SECOND_CAPTURE 
			RETURN "MISHSTA_94" 
		CASE MIN3_KILL_BEFORE_ESCAPE 
			RETURN "MISHSTA_95" 
		CASE MIN3_STUNNED_AND_KILLED 
			RETURN "MISHSTA_96" 
		CASE NI1A_ENTOURAGE 
			RETURN "MISHSTA_97" 
		CASE NI1A_PLAYER_DAMAGE_DURING_BRAWL 
			RETURN "MISHSTA_98" 
		CASE NI1B_CLOTHES_TAKEN_NO_ALERTS 
			RETURN "MISHSTA_99" 
		CASE NI1B_GARDENER_TAKEN_OUT 
			RETURN "MISHSTA_100" 
		CASE NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY 
			RETURN "MISHSTA_101" 
		CASE NI1D_KILLED_STANKY_AND_GUARDS 
			RETURN "MISHSTA_102" 
		CASE NI1D_HEADSHOTTED_STANKY 
			RETURN "MISHSTA_103" 
		CASE NI1D_GOLF_CLUB_STOLEN_IN_TIME 
			RETURN "MISHSTA_104" 
		CASE NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI 
			RETURN "MISHSTA_105" 
		CASE NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH 
			RETURN "MISHSTA_106" 
	ENDSWITCH
	SWITCH(e)
		CASE NI2_VEHICLE_DAMAGE 
			RETURN "MISHSTA_0" 
		CASE NI3_BAILED_AT_LAST_MOMENT 
			RETURN "MISHSTA_107" 
		CASE NI3_REVERSED_TO_KILL 
			RETURN "MISHSTA_108" 
		CASE PAP1_TAKEN_OUT_IN_ONE_SWING 
			RETURN "MISHSTA_109" 
		CASE PAP1_PICTURES_SNAPPED 
			RETURN "MISHSTA_110" 
		CASE PAP2_POOL_JUMP 
			RETURN "MISHSTA_111" 
		CASE PAP2_FACE_RECOG_PERCENT 
			RETURN "MISHSTA_112" 
		CASE PAP3A_FAR_FROM_POPPY 
			RETURN "MISHSTA_113" 
		CASE PAP3A_PHOTO_IN_CUFFS 
			RETURN "MISHSTA_114" 
		CASE PAP3B_PHOTO_SPOTTED 
			RETURN "MISHSTA_115" 
		CASE PAP3B_PHOTO_OF_USE 
			RETURN "MISHSTA_116" 
		CASE PAP4_ENTIRE_CREW_KILLED_IN_ONE 
			RETURN "MISHSTA_117" 
		CASE TLO_WOUNDS 
			RETURN "MISHSTA_118" 
		CASE TLO_AMBIENT_ANIMAL_KILLS 
			RETURN "MISHSTA_119" 
		CASE TLO_TO_SITE_ON_FOOT 
			RETURN "MISHSTA_120" 
		CASE MIC3_TIME 
			RETURN "MISHSTA_1" 
		CASE MIC4_TIME 
			RETURN "MISHSTA_1" 
		CASE ARM1_SPECIAL_ABILITY_TIME 
			RETURN "MISHSTA_121" 
		CASE ARM2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FRA0_SPECIAL_ABILITY_TIME 
			RETURN "MISHSTA_122" 
		CASE ARM1_WINNER 
			RETURN "MISHSTA_123" 
		CASE ARM1_HIT_ALIENS 
			RETURN "MISHSTA_124" 
		CASE ARM2_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE EXL1_BAIL_IN_CAR 
			RETURN "MISHSTA_125" 
		CASE RH2_BULLETS_FIRED 
			RETURN "MISHSTA_126" 
		CASE RH2_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE CHI1_VEHICLES_DESTROYED 
			RETURN "MISHSTA_127" 
		CASE CHI2_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FRA1_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FRA1_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE DH2A_HEADSHOTS 
			RETURN "MISHSTA_11" 
	ENDSWITCH
	SWITCH(e)
		CASE DH2A_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE DH2A_STEALTH_KILLS 
			RETURN "MISHSTA_128" 
		CASE DH2A_TIME_TO_FIND_CONTAINER 
			RETURN "MISHSTA_129" 
		CASE DH2B_TIME_TO_FIND_CONTAINER 
			RETURN "MISHSTA_130" 
		CASE FBI4_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FBI4_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FBI4_SWITCHES 
			RETURN "MISHSTA_131" 
		CASE SOL3_MAX_SPEED 
			RETURN "MISHSTA_37" 
		CASE SOL3_COP_LOSS_TIME 
			RETURN "MISHSTA_132" 
		CASE SOL3_NEWS_HELI_CAM_TIME 
			RETURN "MISHSTA_133" 
		CASE RH1_WINNER 
			RETURN "MISHSTA_123" 
		CASE AH3B_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE AH3B_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE AH3B_INNOCENTS_KILLED 
			RETURN "MISHSTA_134" 
		CASE AH3B_LANDING_ACCURACY 
			RETURN "MISHSTA_135" 
		CASE AH3B_HACKING_TIME 
			RETURN "MISHSTA_136" 
		CASE MIC3_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE MIC4_MAX_SPEED 
			RETURN "MISHSTA_137" 
		CASE MIC4_HEADSHOTS 
			RETURN "MISHSTA_138" 
		CASE MIC4_HEADSHOT_RESCUE 
			RETURN "MISHSTA_139" 
		CASE FRA2_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FRA2_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FRA2_SWITCHES 
			RETURN "MISHSTA_140" 
		CASE FH2A_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FH2A_TRAFFIC_LIGHT_CHANGES 
			RETURN "MISHSTA_141" 
		CASE FH2B_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FIN_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FIN_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FIN_CHENG_BOMBER 
			RETURN "MISHSTA_142" 
		CASE FIN_HAINES_HEADSHOT 
			RETURN "MISHSTA_143" 
		CASE FIN_STRETCH_STRONGARM 
			RETURN "MISHSTA_144" 
	ENDSWITCH
	SWITCH(e)
		CASE FA2_QUICK_WIN 
			RETURN "MISHSTA_145" 
		CASE FA2_BUMPED_INTO_MARY_ANN 
			RETURN "MISHSTA_146" 
		CASE FBI5_HEADSHOTS 
			RETURN "MISHSTA_11" 
		CASE FBI5_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FH1_PERFECT_DISTANCE 
			RETURN "MISHSTA_147" 
		CASE FH1_FIND_HOLE_TIME 
			RETURN "MISHSTA_148" 
		CASE FH1_UNDER_BRIDGE 
			RETURN "MISHSTA_149" 
		CASE FH1_THROUGH_TUNNEL 
			RETURN "MISHSTA_150" 
		CASE MIC1_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE RH1P_HEADSHOTS 
			RETURN "MISHSTA_151" 
		CASE EP6_UNDER_BRIDGE 
			RETURN "MISHSTA_152" 
		CASE CHI1_TIME 
			RETURN "MISHSTA_1" 
		CASE FBI5_TIME 
			RETURN "MISHSTA_1" 
		CASE FRA2_TIME 
			RETURN "MISHSTA_1" 
		CASE AH3A_TIME 
			RETURN "MISHSTA_1" 
		CASE AH3B_TIME 
			RETURN "MISHSTA_1" 
		CASE DH2B_TIME 
			RETURN "MISHSTA_1" 
		CASE FH1_TIME 
			RETURN "MISHSTA_1" 
		CASE FH2B_TIME 
			RETURN "MISHSTA_1" 
		CASE JH1_TIME 
			RETURN "MISHSTA_1" 
		CASE RH2_TIME 
			RETURN "MISHSTA_1" 
		CASE FHPA_DAMAGE_TO_WAGON 
			RETURN "MISHSTA_0" 
		CASE FHPA_ESCAPE_TIME 
			RETURN "MISHSTA_153" 
		CASE FBI4P5_UNITED_COLOURS 
			RETURN "MISHSTA_154" 
		CASE FBI4P5_QUICK_SHOPPER 
			RETURN "MISHSTA_155" 
		CASE FBI4P4_CLICHE 
			RETURN "MISHSTA_156" 
		CASE FBI4P4_FACE_TIME 
			RETURN "MISHSTA_157" 
		CASE FBI4P1_MAX_SPEED 
			RETURN "MISHSTA_158" 
		CASE FBI4P1_VEHICLE_DAMAGE 
			RETURN "MISHSTA_159" 
		CASE FBI4P1_TIME_TAKEN 
			RETURN "MISHSTA_1" 
		CASE FBI4P2_TIME_TAKEN 
			RETURN "MISHSTA_1" 
	ENDSWITCH
	SWITCH(e)
		CASE FBI4P2_VEHICLE_DAMAGE 
			RETURN "MISHSTA_0" 
		CASE FBI4P2_MAX_SPEED 
			RETURN "MISHSTA_160" 
		CASE JH2A_CASES_SMASHED 
			RETURN "MISHSTA_161" 
		CASE JH2A_FRANKLIN_BIKE_DAMAGE 
			RETURN "MISHSTA_162" 
		CASE JH2A_CASE_GRAB_TIME 
			RETURN "MISHSTA_163" 
		CASE BA3A_AVOID_STAKEOUT 
			RETURN "MISHSTA_164" 
		CASE TRV2_ALL_BIKERS_KILLED_ON_WING 
			RETURN "MISHSTA_165" 
		CASE BA2_DANCING_CLOWNS_KILLED 
			RETURN "MISHSTA_166" 
		CASE TON5_UNHOOK 
			RETURN "MISHSTA_167" 
		CASE TON5_TIME 
			RETURN "MISHSTA_1" 
		CASE TON4_UNHOOK 
			RETURN "MISHSTA_167" 
		CASE TON1_TIME 
			RETURN "MISHSTA_1" 
		CASE TON1_UNHOOK 
			RETURN "MISHSTA_167" 
		CASE TON2_TIME 
			RETURN "MISHSTA_1" 
		CASE TON2_UNHOOK 
			RETURN "MISHSTA_167" 
		CASE TON3_TIME 
			RETURN "MISHSTA_1" 
		CASE TON3_UNHOOK 
			RETURN "MISHSTA_167" 
		CASE TON4_TIME 
			RETURN "MISHSTA_1" 
		CASE RH2_COLLATERAL_DAMAGE 
			RETURN "MISHSTA_168" 
		CASE DH2A_NO_ALARMS 
			RETURN "MISHSTA_169" 
		CASE AH3A_ABSEIL_TIME 
			RETURN "MISHSTA_170" 
		CASE AH3A_FLOOR_MOP_TIME 
			RETURN "MISHSTA_171" 
		CASE AH3A_OXYGEN_REMAINING 
			RETURN "MISHSTA_172" 
		CASE FH2A_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FH2B_ACCURACY 
			RETURN "MISHSTA_10" 
		CASE FH2B_GOLD_DROP_TIME 
			RETURN "MISHSTA_173" 
		CASE HAO1_RACE_TIME 
			RETURN "MISHSTA_174" 
		CASE HAO1_COLLISIONS 
			RETURN "MISHSTA_175" 
		CASE HAO1_FASTEST_LAP 
			RETURN "MISHSTA_176" 
		CASE FAM4_COORD_KO 
			RETURN "MISHSTA_177" 
		CASE ARM3_GARDEN_KO 
			RETURN "MISHSTA_178" 
	ENDSWITCH
	SWITCH(e)
		CASE CS2_SCANMAN 
			RETURN "MISHSTA_179" 
		CASE CS2_EAVESDROPPER 
			RETURN "MISHSTA_180" 
		CASE CS4_SHREDS 
			RETURN "MISHSTA_181" 
		CASE FAM3_VEHICLE_KILLS 
			RETURN "MISHSTA_182" 
		CASE FINB_KILLMIC 
			RETURN "MISHSTA_183" 
		CASE FINA_KILLTREV 
			RETURN "MISHSTA_184" 
		CASE MIC3_HELIKILL 
			RETURN "MISHSTA_185" 
		CASE SOL1_PERFECT_LANDING 
			RETURN "MISHSTA_186" 
		CASE SOL1_BRAWL_DAMAGE 
			RETURN "MISHSTA_187" 
		CASE SOL1_TIME 
			RETURN "MISHSTA_1" 
		CASE SOL1_SILENT_TAKEDOWNS 
			RETURN "MISHSTA_188" 
		CASE TRV4_TIME 
			RETURN "MISHSTA_1" 
		CASE AHP1_NOSCRATCH 
			RETURN "MISHSTA_0" 
		CASE AHP1_TRUCKCALLED 
			RETURN "MISHSTA_189" 
		CASE DHP1_NO_BOARDING 
			RETURN "MISHSTA_190" 
		CASE DHP1_TIME 
			RETURN "MISHSTA_1" 
		CASE DHP2_TIME 
			RETURN "MISHSTA_1" 
		CASE FINPD_TIME 
			RETURN "MISHSTA_1" 
		CASE FINPD_UNDETECTED 
			RETURN "MISHSTA_191" 
		CASE FINPC2_MAPPED 
			RETURN "MISHSTA_192" 
		CASE FINPC3_MAPPED 
			RETURN "MISHSTA_192" 
		CASE FHPB_SNEAK_THIEF 
			RETURN "MISHSTA_193" 
		CASE FINPC1_MAPPED 
			RETURN "MISHSTA_192" 
		CASE JHP1B_SWIFT_GETAWAY 
			RETURN "MISHSTA_194" 
		CASE JHP2A_LOOSE_CARGO 
			RETURN "MISHSTA_195" 
		CASE JHP1A_SNEAKY_PEST 
			RETURN "MISHSTA_196" 
		CASE RH1P_CONVOY_STOPPED 
			RETURN "MISHSTA_197" 
		CASE FAM4_TRUCK_UNHOOKED 
			RETURN "MISHSTA_198" 
		CASE FHPB_TIME 
			RETURN "MISHSTA_1" 
		CASE FINPC1_CAR_DAMAGE 
			RETURN "MISHSTA_0" 
		CASE FINPC2_CAR_DAMAGE 
			RETURN "MISHSTA_0" 
	ENDSWITCH
	SWITCH(e)
		CASE FINPC3_CAR_DAMAGE 
			RETURN "MISHSTA_0" 
		CASE JH1_PERFECT_PIC 
			RETURN "MISHSTA_110" 
		CASE FINPC1_MOD_GAUNTLET 
			RETURN "MISHSTA_199" 
		CASE FINPC2_MOD_GAUNTLET 
			RETURN "MISHSTA_199" 
		CASE FINPC3_MOD_GAUNTLET 
			RETURN "MISHSTA_199" 
		CASE DH2B_ESCAPE_TIME 
			RETURN "MISHSTA_200" 
		CASE DH2B_KILLED_ALL_MERRYWEATHER 
			RETURN "MISHSTA_201" 
		CASE ASS1_MIRROR_TIME 
			RETURN "MISHSTA_202" 
		CASE ASS1_MIRROR_PERCENT 
			RETURN "MISHSTA_203" 
		CASE ASS1_MIRROR_SNIPER_USED 
			RETURN "MISHSTA_204" 
		CASE ASS1_MIRROR_CASH 
			RETURN "MISHSTA_205" 
		CASE ASS1_MIRROR_MEDAL 
			RETURN "MISHSTA_206" 
		CASE ASS2_MIRROR_CASH 
			RETURN "MISHSTA_205" 
		CASE ASS2_MIRROR_TIME 
			RETURN "MISHSTA_202" 
		CASE ASS2_MIRROR_QUICKBOOL 
			RETURN "MISHSTA_207" 
		CASE ASS2_MIRROR_PERCENT 
			RETURN "MISHSTA_203" 
		CASE ASS2_MIRROR_MEDAL 
			RETURN "MISHSTA_206" 
		CASE ASS3_MIRROR_MEDAL 
			RETURN "MISHSTA_206" 
		CASE ASS3_MIRROR_PERCENTAGE 
			RETURN "MISHSTA_203" 
		CASE ASS3_MIRROR_CLEAN_ESCAPE 
			RETURN "MISHSTA_132" 
		CASE ASS3_MIRROR_CASH 
			RETURN "MISHSTA_205" 
		CASE ASS3_MIRROR_TIME 
			RETURN "MISHSTA_202" 
		CASE ASS4_MIRROR_TIME 
			RETURN "MISHSTA_202" 
		CASE ASS4_MIRROR_HIT_AND_RUN 
			RETURN "MISHSTA_208" 
		CASE ASS4_MIRROR_PERCENTAGE 
			RETURN "MISHSTA_203" 
		CASE ASS4_MIRROR_CASH 
			RETURN "MISHSTA_205" 
		CASE ASS4_MIRROR_MEDAL 
			RETURN "MISHSTA_206" 
		CASE ASS5_MIRROR_TIME 
			RETURN "MISHSTA_202" 
		CASE ASS5_MIRROR_NO_FLY 
			RETURN "MISHSTA_209" 
		CASE ASS5_MIRROR_PERCENT 
			RETURN "MISHSTA_203" 
		CASE ASS5_MIRROR_CASH 
			RETURN "MISHSTA_205" 
	ENDSWITCH
	SWITCH(e)
		CASE ASS5_MIRROR_MEDAL 
			RETURN "MISHSTA_206" 
	ENDSWITCH
	SCRIPT_ASSERT("GET_MISSION_STAT_NAME : Mission stat with no string found!")
RETURN "MISSING_MISSION_STAT_STRING"
ENDFUNC




FUNC STRING GET_MISSION_STAT_DESCRIPTION(ENUM_MISSION_STATS e)
	SWITCH(e)
	CASE ARM1_NOSCRATCH
		RETURN "MISHSTD0"
	CASE ARM2_TIME
		RETURN "MISHSTD1"
	CASE ARM2_PETROL_FIRE
		RETURN "MISHSTD2"
	CASE ARM3_TIME
		RETURN "MISHSTD3"
	CASE ARM3_DAMAGE
		RETURN "MISHSTD4"
	CASE FRA0_NOSCRATCH
		RETURN "MISHSTD5"
	CASE FRA0_DOGCAM
		RETURN "MISHSTD6"
	CASE FAM1_QUICK_CATCH
		RETURN "MISHSTD7"
	CASE FAM1_NOSCRATCH
		RETURN "MISHSTD8"
	CASE FAM2_FASTEST_SPEED
		RETURN "MISHSTD9"
	CASE FAM2_FELL_OFF_BIKE
		RETURN "MISHSTD10"
	CASE FAM2_FAST_SWIM
		RETURN "MISHSTD11"
	CASE FAM3_TIME
		RETURN "MISHSTD12"
	CASE FAM3_NOSCRATCH
		RETURN "MISHSTD13"
	CASE LES1A_TIME
		RETURN "MISHSTD14"
	CASE LES1A_CLEAR_POPUPS
		RETURN "MISHSTD15"
	CASE LAM1_ACCURACY
		RETURN "MISHSTD16"
	CASE LAM1_HEADSHOTS
		RETURN "MISHSTD17"
	CASE LAM1_TIME
		RETURN "MISHSTD18"
	CASE LAM1_UNMARKED
		RETURN "MISHSTD19"
	ENDSWITCH
	SWITCH(e)
	CASE TRV1_NO_SURVIVORS
		RETURN "MISHSTD20"
	CASE TRV1_HEADSHOTS
		RETURN "MISHSTD21"
	CASE TRV1_TIME
		RETURN "MISHSTD22"
	CASE TRV1_TRAILER_DAMAGE
		RETURN "MISHSTD23"
	CASE TRV1_BIKERS_KILLED_BEFORE_LOCATION
		RETURN "MISHSTD24"
	CASE TRV2_HEADSHOTS
		RETURN "MISHSTD25"
	CASE TRV2_TIME
		RETURN "MISHSTD26"
	CASE TRV2_RACEBACK_WON
		RETURN "MISHSTD27"
	CASE TRV2_UNDERBRIDGES
		RETURN "MISHSTD28"
	CASE TRV3_HEADSHOTS
		RETURN "MISHSTD25"
	CASE TRV3_NOT_DETECTED
		RETURN "MISHSTD30"
	CASE TRV3_UNMARKED
		RETURN "MISHSTD19"
	CASE TRV3_ALL_TRAILERS_AT_ONCE
		RETURN "MISHSTD32"
	CASE CHI1_UNMARKED
		RETURN "MISHSTD19"
	CASE CHI1_BODY_COUNT
		RETURN "MISHSTD34"
	CASE CHI2_HEADSHOTS
		RETURN "MISHSTD17"
	CASE CHI2_UNMARKED
		RETURN "MISHSTD19"
	CASE CHI2_ONE_SHOT_TWO_KILLS
		RETURN "MISHSTD37"
	CASE FAM4_LORRY_SPEED
		RETURN "MISHSTD38"
	ENDSWITCH
	SWITCH(e)
	CASE FAM4_GOT_TOO_FAR_FROM_LAZ
		RETURN "MISHSTD39"
	CASE FBI1_ACCURACY
		RETURN "MISHSTD40"
	CASE FBI1_HEADSHOTS
		RETURN "MISHSTD41"
	CASE FBI1_TIME
		RETURN "MISHSTD42"
	CASE FBI1_SPECKILLS
		RETURN "MISHSTD43"
	CASE FBI1_UNMARKED
		RETURN "MISHSTD19"
	CASE FBI2_TIME
		RETURN "MISHSTD45"
	CASE FBI2_ACCURACY
		RETURN "MISHSTD16"
	CASE FBI2_HEADSHOTS
		RETURN "MISHSTD17"
	CASE FRA1_TIME
		RETURN "MISHSTD48"
	CASE FAM5_TIME
		RETURN "MISHSTD49"
	CASE FAM5_WARRIOR_POSE
		RETURN "MISHSTD50"
	CASE FAM5_TRIANGLE_POSE
		RETURN "MISHSTD51"
	CASE FAM5_SUN_SALUTATION_POSE
		RETURN "MISHSTD52"
	CASE FBI3_HEART_STOPPED
		RETURN "MISHSTD53"
	CASE FBI3_ELECTROCUTION
		RETURN "MISHSTD54"
	CASE FBI3_TOOTH
		RETURN "MISHSTD55"
	CASE FBI3_WRENCH
		RETURN "MISHSTD56"
	CASE FBI3_WATERBOARD
		RETURN "MISHSTD57"
	ENDSWITCH
	SWITCH(e)
	CASE FBI4_HELISHOT
		RETURN "MISHSTD58"
	CASE CS2_NO_SCRATCH
		RETURN "MISHSTD59"
	CASE CS1_SPECIAL_USED
		RETURN "MISHSTD60"
	CASE CS1_TIME
		RETURN "MISHSTD22"
	CASE CS1_DROVE_BETWEEN_TRUCKS
		RETURN "MISHSTD62"
	CASE CS1_DROVE_BETWEEN_BUSES
		RETURN "MISHSTD63"
	CASE CS1_DROVE_THROUGH_TUNNEL
		RETURN "MISHSTD64"
	CASE MAR1_TIME
		RETURN "MISHSTD65"
	CASE MAR1_FASTEST_SPEED
		RETURN "MISHSTD66"
	CASE MAR1_FELL_OFF_THE_BIKE
		RETURN "MISHSTD67"
	CASE MAR1_ONE_SHOT
		RETURN "MISHSTD68"
	CASE CS3_NO_SCRATCH
		RETURN "MISHSTD69"
	CASE CS3_FASTEST_SPEED
		RETURN "MISHSTD70"
	CASE CS3_ACTOR_KNOCKOUT
		RETURN "MISHSTD71"
	CASE CS3_EJECTOR_SEAT_USED
		RETURN "MISHSTD72"
	CASE CS3_RAN_OVER_ACTOR_AGAIN
		RETURN "MISHSTD73"
	CASE EXL1_ACCURACY
		RETURN "MISHSTD74"
	CASE EXL2_ACCURACY
		RETURN "MISHSTD40"
	CASE EXL2_TIME
		RETURN "MISHSTD76"
	ENDSWITCH
	SWITCH(e)
	CASE EXL2_HEADSHOTS
		RETURN "MISHSTD77"
	CASE EXL2_ANIMAL_KILLED
		RETURN "MISHSTD78"
	CASE EXL3_TIME
		RETURN "MISHSTD79"
	CASE EXL3_FASTEST_SPEED
		RETURN "MISHSTD66"
	CASE EXL3_JUMPED_AT_FIRST_OPPERTUNITY
		RETURN "MISHSTD81"
	CASE FBI5_STUNS
		RETURN "MISHSTD82"
	CASE CS4_TIME
		RETURN "MISHSTD22"
	CASE CS4_NO_SCRATCH
		RETURN "MISHSTD84"
	CASE SOL2_TIME
		RETURN "MISHSTD85"
	CASE MIC1_TIME
		RETURN "MISHSTD86"
	CASE MIC1_HEADSHOTS
		RETURN "MISHSTD87"
	CASE MIC2_TIMES_SWITCHED
		RETURN "MISHSTD88"
	CASE MIC2_ACCURACY
		RETURN "MISHSTD40"
	CASE MIC2_HEADSHOTS
		RETURN "MISHSTD17"
	CASE MIC2_MIKE_RESCUE_TIMER
		RETURN "MISHSTD91"
	CASE MIC2_WAYPOINT_USED
		RETURN "MISHSTD92"
	CASE FAM6_TIME
		RETURN "MISHSTD18"
	CASE SOL3_TIME
		RETURN "MISHSTD12"
	CASE FIN_TIME
		RETURN "MISHSTD95"
	ENDSWITCH
	SWITCH(e)
	CASE AH1_EAGLE_EYE
		RETURN "MISHSTD96"
	CASE AH1_MISSED_A_SPOT
		RETURN "MISHSTD97"
	CASE AH1_CLEANED_OUT
		RETURN "MISHSTD76"
	CASE AH2_QUICK_GETAWAY
		RETURN "MISHSTD99"
	CASE RH1_LEISURELY_DRIVE
		RETURN "MISHSTD100"
	CASE DH1_TIME
		RETURN "MISHSTD101"
	CASE DH1_EMPLOYEE_OF_THE_MONTH
		RETURN "MISHSTD102"
	CASE DH1_PERFECT_SURVEILLANCE
		RETURN "MISHSTD103"
	CASE DH1_HONEST_DAYS_WORK
		RETURN "MISHSTD104"
	CASE BA1_KILLCHAIN
		RETURN "MISHSTD105"
	CASE BA1_DAMAGE_TAKEN
		RETURN "MISHSTD19"
	CASE BA2_VANS_DESTROYED
		RETURN "MISHSTD107"
	CASE BA3A_DELIVERY_TIME
		RETURN "MISHSTD108"
	CASE BA3C_DELIVERY_TIME
		RETURN "MISHSTD109"
	CASE BA3C_STASH_UNHOOKED
		RETURN "MISHSTD110"
	CASE DR1_DREYFUSS_KILLED
		RETURN "MISHSTD111"
	CASE EP4_ARTIFACT_DETECTOR_USED
		RETURN "MISHSTD112"
	CASE EP6_PERFECT_LANDING
		RETURN "MISHSTD113"
	CASE EP8_MONEY_STOLEN
		RETURN "MISHSTD114"
	ENDSWITCH
	SWITCH(e)
	CASE EP8_SECURITY_WIPED_OUT
		RETURN "MISHSTD115"
	CASE EXT1_RACE_WON
		RETURN "MISHSTD116"
	CASE EXT1_BIG_AIR
		RETURN "MISHSTD117"
	CASE EXT1_FALL_TIME
		RETURN "MISHSTD118"
	CASE EXT2_NUMBER_OF_SPINS
		RETURN "MISHSTD119"
	CASE EXT2_LEAP_FROM_ATV_TO_WATER
		RETURN "MISHSTD120"
	CASE EXT3_PERFECT_LANDING
		RETURN "MISHSTD121"
	CASE EXT3_DARE_DEVIL
		RETURN "MISHSTD122"
	CASE EXT4_FALL_SURVIVED
		RETURN "MISHSTD123"
	CASE FA1_SHORTCUT_USED
		RETURN "MISHSTD124"
	CASE FA3_SHORTCUT_USED
		RETURN "MISHSTD124"
	CASE HU1_HIT_EACH_SATELLITE_FIRST_GO
		RETURN "MISHSTD126"
	CASE HU1_TYRE_SHOOTING_ACCURACY
		RETURN "MISHSTD127"
	CASE HU1_TWO_COYOTES_KILLED_IN_ONE
		RETURN "MISHSTD128"
	CASE HU2_ELK_HEADSHOTS
		RETURN "MISHSTD129"
	CASE HU2_DETECTED_BY_ELK
		RETURN "MISHSTD130"
	CASE JO2_STOPPED_IN_TIME
		RETURN "MISHSTD131"
	CASE JO2_JOSH_MELEED
		RETURN "MISHSTD132"
	CASE JO3_POUR_FUEL_IN_ONE_GO
		RETURN "MISHSTD133"
	ENDSWITCH
	SWITCH(e)
	CASE JO3_ESCAPE_WITHOUT_ALERTING_COPS
		RETURN "MISHSTD134"
	CASE JO4_ESCAPE_IN_PARKED_COP_CAR
		RETURN "MISHSTD135"
	CASE JO4_JOSH_KILLED_BEFORE_ESCAPE
		RETURN "MISHSTD136"
	CASE MIN1_FAST_STOP
		RETURN "MISHSTD137"
	CASE MIN1_VEHICLE_TAKEN_AFTER_STUNS
		RETURN "MISHSTD138"
	CASE MIN2_STUNNED_ALL
		RETURN "MISHSTD139"
	CASE MIN2_FIRST_CAPTURE
		RETURN "MISHSTD140"
	CASE MIN2_SECOND_CAPTURE
		RETURN "MISHSTD141"
	CASE MIN3_KILL_BEFORE_ESCAPE
		RETURN "MISHSTD142"
	CASE MIN3_STUNNED_AND_KILLED
		RETURN "MISHSTD143"
	CASE NI1A_ENTOURAGE
		RETURN "MISHSTD144"
	CASE NI1A_PLAYER_DAMAGE_DURING_BRAWL
		RETURN "MISHSTD145"
	CASE NI1B_CLOTHES_TAKEN_NO_ALERTS
		RETURN "MISHSTD146"
	CASE NI1B_GARDENER_TAKEN_OUT
		RETURN "MISHSTD147"
	CASE NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY
		RETURN "MISHSTD148"
	CASE NI1D_KILLED_STANKY_AND_GUARDS
		RETURN "MISHSTD149"
	CASE NI1D_HEADSHOTTED_STANKY
		RETURN "MISHSTD150"
	CASE NI1D_GOLF_CLUB_STOLEN_IN_TIME
		RETURN "MISHSTD151"
	CASE NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI
		RETURN "MISHSTD152"
	ENDSWITCH
	SWITCH(e)
	CASE NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH
		RETURN "MISHSTD153"
	CASE NI2_VEHICLE_DAMAGE
		RETURN "MISHSTD154"
	CASE NI3_BAILED_AT_LAST_MOMENT
		RETURN "MISHSTD155"
	CASE NI3_REVERSED_TO_KILL
		RETURN "MISHSTD156"
	CASE PAP1_TAKEN_OUT_IN_ONE_SWING
		RETURN "MISHSTD157"
	CASE PAP1_PICTURES_SNAPPED
		RETURN "MISHSTD158"
	CASE PAP2_POOL_JUMP
		RETURN "MISHSTD159"
	CASE PAP2_FACE_RECOG_PERCENT
		RETURN "MISHSTD160"
	CASE PAP3A_FAR_FROM_POPPY
		RETURN "MISHSTD161"
	CASE PAP3A_PHOTO_IN_CUFFS
		RETURN "MISHSTD162"
	CASE PAP3B_PHOTO_SPOTTED
		RETURN "MISHSTD163"
	CASE PAP3B_PHOTO_OF_USE
		RETURN "MISHSTD164"
	CASE PAP4_ENTIRE_CREW_KILLED_IN_ONE
		RETURN "MISHSTD165"
	CASE TLO_WOUNDS
		RETURN "MISHSTD166"
	CASE TLO_AMBIENT_ANIMAL_KILLS
		RETURN "MISHSTD167"
	CASE TLO_TO_SITE_ON_FOOT
		RETURN "MISHSTD168"
	CASE MIC3_TIME
		RETURN "MISHSTD48"
	CASE MIC4_TIME
		RETURN "MISHSTD1"
	CASE ARM1_SPECIAL_ABILITY_TIME
		RETURN "MISHSTD171"
	ENDSWITCH
	SWITCH(e)
	CASE ARM2_HEADSHOTS
		RETURN "MISHSTD172"
	CASE FRA0_SPECIAL_ABILITY_TIME
		RETURN "MISHSTD173"
	CASE ARM1_WINNER
		RETURN "MISHSTD174"
	CASE ARM1_HIT_ALIENS
		RETURN "MISHSTD175"
	CASE ARM2_ACCURACY
		RETURN "MISHSTD40"
	CASE EXL1_BAIL_IN_CAR
		RETURN "MISHSTD177"
	CASE RH2_BULLETS_FIRED
		RETURN "MISHSTD178"
	CASE RH2_ACCURACY
		RETURN "MISHSTD179"
	CASE CHI1_VEHICLES_DESTROYED
		RETURN "MISHSTD180"
	CASE CHI2_ACCURACY
		RETURN "MISHSTD74"
	CASE FRA1_ACCURACY
		RETURN "MISHSTD40"
	CASE FRA1_HEADSHOTS
		RETURN "MISHSTD21"
	CASE DH2A_HEADSHOTS
		RETURN "MISHSTD21"
	CASE DH2A_ACCURACY
		RETURN "MISHSTD74"
	CASE DH2A_STEALTH_KILLS
		RETURN "MISHSTD186"
	CASE DH2A_TIME_TO_FIND_CONTAINER
		RETURN "MISHSTD187"
	CASE DH2B_TIME_TO_FIND_CONTAINER
		RETURN "MISHSTD188"
	CASE FBI4_HEADSHOTS
		RETURN "MISHSTD21"
	CASE FBI4_ACCURACY
		RETURN "MISHSTD16"
	ENDSWITCH
	SWITCH(e)
	CASE FBI4_SWITCHES
		RETURN "MISHSTD191"
	CASE SOL3_MAX_SPEED
		RETURN "MISHSTD192"
	CASE SOL3_COP_LOSS_TIME
		RETURN "MISHSTD193"
	CASE SOL3_NEWS_HELI_CAM_TIME
		RETURN "MISHSTD194"
	CASE RH1_WINNER
		RETURN "MISHSTD195"
	CASE AH3B_ACCURACY
		RETURN "MISHSTD40"
	CASE AH3B_HEADSHOTS
		RETURN "MISHSTD87"
	CASE AH3B_INNOCENTS_KILLED
		RETURN "MISHSTD198"
	CASE AH3B_LANDING_ACCURACY
		RETURN "MISHSTD199"
	CASE AH3B_HACKING_TIME
		RETURN "MISHSTD200"
	CASE MIC3_HEADSHOTS
		RETURN "MISHSTD201"
	CASE MIC4_MAX_SPEED
		RETURN "MISHSTD202"
	CASE MIC4_HEADSHOTS
		RETURN "MISHSTD21"
	CASE MIC4_HEADSHOT_RESCUE
		RETURN "MISHSTD204"
	CASE FRA2_HEADSHOTS
		RETURN "MISHSTD201"
	CASE FRA2_ACCURACY
		RETURN "MISHSTD40"
	CASE FRA2_SWITCHES
		RETURN "MISHSTD207"
	CASE FH2A_HEADSHOTS
		RETURN "MISHSTD87"
	CASE FH2A_TRAFFIC_LIGHT_CHANGES
		RETURN "MISHSTD209"
	ENDSWITCH
	SWITCH(e)
	CASE FH2B_HEADSHOTS
		RETURN "MISHSTD87"
	CASE FIN_HEADSHOTS
		RETURN "MISHSTD87"
	CASE FIN_ACCURACY
		RETURN "MISHSTD40"
	CASE FIN_CHENG_BOMBER
		RETURN "MISHSTD213"
	CASE FIN_HAINES_HEADSHOT
		RETURN "MISHSTD214"
	CASE FIN_STRETCH_STRONGARM
		RETURN "MISHSTD215"
	CASE FA2_QUICK_WIN
		RETURN "MISHSTD216"
	CASE FA2_BUMPED_INTO_MARY_ANN
		RETURN "MISHSTD217"
	CASE FBI5_HEADSHOTS
		RETURN "MISHSTD218"
	CASE FBI5_ACCURACY
		RETURN "MISHSTD40"
	CASE FH1_PERFECT_DISTANCE
		RETURN "MISHSTD220"
	CASE FH1_FIND_HOLE_TIME
		RETURN "MISHSTD221"
	CASE FH1_UNDER_BRIDGE
		RETURN "MISHSTD222"
	CASE FH1_THROUGH_TUNNEL
		RETURN "MISHSTD223"
	CASE MIC1_ACCURACY
		RETURN "MISHSTD74"
	CASE RH1P_HEADSHOTS
		RETURN "MISHSTD25"
	CASE EP6_UNDER_BRIDGE
		RETURN "MISHSTD226"
	CASE CHI1_TIME
		RETURN "MISHSTD227"
	CASE FBI5_TIME
		RETURN "MISHSTD228"
	ENDSWITCH
	SWITCH(e)
	CASE FRA2_TIME
		RETURN "MISHSTD228"
	CASE AH3A_TIME
		RETURN "MISHSTD230"
	CASE AH3B_TIME
		RETURN "MISHSTD231"
	CASE DH2B_TIME
		RETURN "MISHSTD232"
	CASE FH1_TIME
		RETURN "MISHSTD86"
	CASE FH2B_TIME
		RETURN "MISHSTD234"
	CASE JH1_TIME
		RETURN "MISHSTD235"
	CASE RH2_TIME
		RETURN "MISHSTD234"
	CASE FHPA_DAMAGE_TO_WAGON
		RETURN "MISHSTD237"
	CASE FHPA_ESCAPE_TIME
		RETURN "MISHSTD238"
	CASE FBI4P5_UNITED_COLOURS
		RETURN "MISHSTD239"
	CASE FBI4P5_QUICK_SHOPPER
		RETURN "MISHSTD240"
	CASE FBI4P4_CLICHE
		RETURN "MISHSTD241"
	CASE FBI4P4_FACE_TIME
		RETURN "MISHSTD242"
	CASE FBI4P1_MAX_SPEED
		RETURN "MISHSTD243"
	CASE FBI4P1_VEHICLE_DAMAGE
		RETURN "MISHSTD244"
	CASE FBI4P1_TIME_TAKEN
		RETURN "MISHSTD3"
	CASE FBI4P2_TIME_TAKEN
		RETURN "MISHSTD85"
	CASE FBI4P2_VEHICLE_DAMAGE
		RETURN "MISHSTD247"
	ENDSWITCH
	SWITCH(e)
	CASE FBI4P2_MAX_SPEED
		RETURN "MISHSTD248"
	CASE JH2A_CASES_SMASHED
		RETURN "MISHSTD249"
	CASE JH2A_FRANKLIN_BIKE_DAMAGE
		RETURN "MISHSTD250"
	CASE JH2A_CASE_GRAB_TIME
		RETURN "MISHSTD251"
	CASE BA3A_AVOID_STAKEOUT
		RETURN "MISHSTD252"
	CASE TRV2_ALL_BIKERS_KILLED_ON_WING
		RETURN "MISHSTD253"
	CASE BA2_DANCING_CLOWNS_KILLED
		RETURN "MISHSTD254"
	CASE TON5_UNHOOK
		RETURN "MISHSTD255"
	CASE TON5_TIME
		RETURN "MISHSTD3"
	CASE TON4_UNHOOK
		RETURN "MISHSTD255"
	CASE TON1_TIME
		RETURN "MISHSTD3"
	CASE TON1_UNHOOK
		RETURN "MISHSTD255"
	CASE TON2_TIME
		RETURN "MISHSTD12"
	CASE TON2_UNHOOK
		RETURN "MISHSTD255"
	CASE TON3_TIME
		RETURN "MISHSTD48"
	CASE TON3_UNHOOK
		RETURN "MISHSTD255"
	CASE TON4_TIME
		RETURN "MISHSTD264"
	CASE RH2_COLLATERAL_DAMAGE
		RETURN "MISHSTD265"
	CASE DH2A_NO_ALARMS
		RETURN "MISHSTD266"
	ENDSWITCH
	SWITCH(e)
	CASE AH3A_ABSEIL_TIME
		RETURN "MISHSTD267"
	CASE AH3A_FLOOR_MOP_TIME
		RETURN "MISHSTD268"
	CASE AH3A_OXYGEN_REMAINING
		RETURN "MISHSTD269"
	CASE FH2A_ACCURACY
		RETURN "MISHSTD16"
	CASE FH2B_ACCURACY
		RETURN "MISHSTD16"
	CASE FH2B_GOLD_DROP_TIME
		RETURN "MISHSTD272"
	CASE HAO1_RACE_TIME
		RETURN "MISHSTD273"
	CASE HAO1_COLLISIONS
		RETURN "MISHSTD274"
	CASE HAO1_FASTEST_LAP
		RETURN "MISHSTD275"
	CASE FAM4_COORD_KO
		RETURN "MISHSTD276"
	CASE ARM3_GARDEN_KO
		RETURN "MISHSTD277"
	CASE CS2_SCANMAN
		RETURN "MISHSTD278"
	CASE CS2_EAVESDROPPER
		RETURN "MISHSTD279"
	CASE CS4_SHREDS
		RETURN "MISHSTD280"
	CASE FAM3_VEHICLE_KILLS
		RETURN "MISHSTD281"
	CASE FINB_KILLMIC
		RETURN "MISHSTD282"
	CASE FINA_KILLTREV
		RETURN "MISHSTD283"
	CASE MIC3_HELIKILL
		RETURN "MISHSTD284"
	CASE SOL1_PERFECT_LANDING
		RETURN "MISHSTD285"
	ENDSWITCH
	SWITCH(e)
	CASE SOL1_BRAWL_DAMAGE
		RETURN "MISHSTD286"
	CASE SOL1_TIME
		RETURN "MISHSTD287"
	CASE SOL1_SILENT_TAKEDOWNS
		RETURN "MISHSTD288"
	CASE TRV4_TIME
		RETURN "MISHSTD289"
	CASE AHP1_NOSCRATCH
		RETURN "MISHSTD290"
	CASE AHP1_TRUCKCALLED
		RETURN "MISHSTD291"
	CASE DHP1_NO_BOARDING
		RETURN "MISHSTD292"
	CASE DHP1_TIME
		RETURN "MISHSTD14"
	CASE DHP2_TIME
		RETURN "MISHSTD12"
	CASE FINPD_TIME
		RETURN "MISHSTD295"
	CASE FINPD_UNDETECTED
		RETURN "MISHSTD296"
	CASE FINPC2_MAPPED
		RETURN "MISHSTD297"
	CASE FINPC3_MAPPED
		RETURN "MISHSTD297"
	CASE FHPB_SNEAK_THIEF
		RETURN "MISHSTD299"
	CASE FINPC1_MAPPED
		RETURN "MISHSTD297"
	CASE JHP1B_SWIFT_GETAWAY
		RETURN "MISHSTD193"
	CASE JHP2A_LOOSE_CARGO
		RETURN "MISHSTD302"
	CASE JHP1A_SNEAKY_PEST
		RETURN "MISHSTD303"
	CASE RH1P_CONVOY_STOPPED
		RETURN "MISHSTD304"
	ENDSWITCH
	SWITCH(e)
	CASE FAM4_TRUCK_UNHOOKED
		RETURN "MISHSTD305"
	CASE FHPB_TIME
		RETURN "MISHSTD3"
	CASE FINPC1_CAR_DAMAGE
		RETURN "MISHSTD307"
	CASE FINPC2_CAR_DAMAGE
		RETURN "MISHSTD307"
	CASE FINPC3_CAR_DAMAGE
		RETURN "MISHSTD307"
	CASE JH1_PERFECT_PIC
		RETURN "MISHSTD310"
	CASE FINPC1_MOD_GAUNTLET
		RETURN "MISHSTD311"
	CASE FINPC2_MOD_GAUNTLET
		RETURN "MISHSTD312"
	CASE FINPC3_MOD_GAUNTLET
		RETURN "MISHSTD311"
	CASE DH2B_ESCAPE_TIME
		RETURN "MISHSTD314"
	CASE DH2B_KILLED_ALL_MERRYWEATHER
		RETURN "MISHSTD315"
	CASE ASS1_MIRROR_TIME
		RETURN "MISHSTD316"
	CASE ASS1_MIRROR_PERCENT
		RETURN "MISHSTD317"
	CASE ASS1_MIRROR_SNIPER_USED
		RETURN "MISHSTD318"
	CASE ASS1_MIRROR_CASH
		RETURN "MISHSTD319"
	CASE ASS1_MIRROR_MEDAL
		RETURN "MISHSTD320"
	CASE ASS2_MIRROR_CASH
		RETURN "MISHSTD319"
	CASE ASS2_MIRROR_TIME
		RETURN "MISHSTD316"
	CASE ASS2_MIRROR_QUICKBOOL
		RETURN "MISHSTD323"
	ENDSWITCH
	SWITCH(e)
	CASE ASS2_MIRROR_PERCENT
		RETURN "MISHSTD317"
	CASE ASS2_MIRROR_MEDAL
		RETURN "MISHSTD320"
	CASE ASS3_MIRROR_MEDAL
		RETURN "MISHSTD320"
	CASE ASS3_MIRROR_PERCENTAGE
		RETURN "MISHSTD317"
	CASE ASS3_MIRROR_CLEAN_ESCAPE
		RETURN "MISHSTD328"
	CASE ASS3_MIRROR_CASH
		RETURN "MISHSTD319"
	CASE ASS3_MIRROR_TIME
		RETURN "MISHSTD316"
	CASE ASS4_MIRROR_TIME
		RETURN "MISHSTD316"
	CASE ASS4_MIRROR_HIT_AND_RUN
		RETURN "MISHSTD332"
	CASE ASS4_MIRROR_PERCENTAGE
		RETURN "MISHSTD317"
	CASE ASS4_MIRROR_CASH
		RETURN "MISHSTD319"
	CASE ASS4_MIRROR_MEDAL
		RETURN "MISHSTD320"
	CASE ASS5_MIRROR_TIME
		RETURN "MISHSTD316"
	CASE ASS5_MIRROR_NO_FLY
		RETURN "MISHSTD337"
	CASE ASS5_MIRROR_PERCENT
		RETURN "MISHSTD317"
	CASE ASS5_MIRROR_CASH
		RETURN "MISHSTD319"
	CASE ASS5_MIRROR_MEDAL
		RETURN "MISHSTD320"
	ENDSWITCH
	SCRIPT_ASSERT("GET_MISSION_STAT_NAME : Mission stat with no description found!")
RETURN "MISSING_MISSION_STAT_STRING"
ENDFUNC
#if USE_CLF_DLC
Proc INTERNAL_FLOW_PRIME_STATS_FOR_MISSIONCLF(SP_missions m)
	m = m
ENDPROC
#ENDIF
#if USE_NRM_DLC
Proc INTERNAL_FLOW_PRIME_STATS_FOR_MISSIONNRM(SP_missions m)
	m = m
ENDPROC
#ENDIF
PROC INTERNAL_FLOW_PRIME_STATS_FOR_MISSION(SP_MISSIONS m)
#if USE_CLF_DLC
	INTERNAL_FLOW_PRIME_STATS_FOR_MISSIONCLF(m)
#ENDIF
#if USE_NRM_DLC
	INTERNAL_FLOW_PRIME_STATS_FOR_MISSIONNRM(m)
#ENDIF
#if not USE_CLF_DLC
#if not USE_NRM_DLC
SWITCH(m)
	CASE SP_MISSION_ARMENIAN_1  
		MISSION_STAT_ADD_WATCH(ARM1_NOSCRATCH)  
		MISSION_STAT_ADD_WATCH(ARM1_CAR_CHOSEN)  
		MISSION_STAT_ADD_WATCH(ARM1_LOSE_WANTED_LVL)  
		MISSION_STAT_ADD_WATCH(ARM1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ARM1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ARM1_WINNER)  
		MISSION_STAT_ADD_WATCH(ARM1_HIT_ALIENS)  
		MISSION_STAT_ADD_WATCH(ARM1_TIME)  
		MISSION_STAT_ADD_WATCH(ARM1_MAX_SPEED)  
		EXIT
	CASE SP_MISSION_ARMENIAN_2  
		MISSION_STAT_ADD_WATCH(ARM2_TIME)  
		MISSION_STAT_ADD_WATCH(ARM2_PETROL_FIRE)  
		MISSION_STAT_ADD_WATCH(ARM2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(ARM2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ARM2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(ARM2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ARM2_KILLS)  
		MISSION_STAT_ADD_WATCH(ARM2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(ARM2_PLAYER_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ARM2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ARM2_CRASH_BIKER)  
		MISSION_STAT_ADD_WATCH(ARM2_ACCURACY)  
		EXIT
	CASE SP_MISSION_ARMENIAN_3  
		MISSION_STAT_ADD_WATCH(ARM3_TIME)  
		MISSION_STAT_ADD_WATCH(ARM3_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ARM3_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ARM3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ARM3_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ARM3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(ARM3_ACTION_CAM_TIME)  
		MISSION_STAT_ADD_WATCH(ARM3_COUNTERS)  
		MISSION_STAT_ADD_WATCH(ARM3_GARDEN_KO)  
		EXIT
	CASE SP_MISSION_ASSASSIN_1  
		MISSION_STAT_ADD_WATCH(ASS1_TIME)  
		MISSION_STAT_ADD_WATCH(ASS1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(ASS1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(ASS1_KILLS)  
		MISSION_STAT_ADD_WATCH(ASS1_NOT_SPOTTED)  
		MISSION_STAT_ADD_WATCH(ASS1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ASS1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(ASS1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ASS1_MIRROR_TIME)  
		MISSION_STAT_ADD_WATCH(ASS1_MIRROR_SNIPER_USED)  
		MISSION_STAT_ADD_WATCH(ASS1_MIRROR_CASH)  
		MISSION_STAT_ADD_WATCH(ASS1_MIRROR_PERCENT)  
		MISSION_STAT_ADD_WATCH(ASS1_MIRROR_MEDAL)  
		EXIT
	CASE SP_MISSION_ASSASSIN_2  
		MISSION_STAT_ADD_WATCH(ASS2_TIME)  
		MISSION_STAT_ADD_WATCH(ASS2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(ASS2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(ASS2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ASS2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(ASS2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ASS2_KILLS)  
		MISSION_STAT_ADD_WATCH(ASS2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(ASS2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ASS2_MIRROR_TIME)  
		MISSION_STAT_ADD_WATCH(ASS2_MIRROR_QUICKBOOL)  
		MISSION_STAT_ADD_WATCH(ASS2_MIRROR_PERCENT)  
		MISSION_STAT_ADD_WATCH(ASS2_MIRROR_MEDAL)  
		MISSION_STAT_ADD_WATCH(ASS2_MIRROR_CASH)  
		EXIT
	CASE SP_MISSION_ASSASSIN_3  
		MISSION_STAT_ADD_WATCH(ASS3_TIME)  
		MISSION_STAT_ADD_WATCH(ASS3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(ASS3_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(ASS3_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ASS3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(ASS3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ASS3_KILLS)  
		MISSION_STAT_ADD_WATCH(ASS3_ACCURACY)  
		MISSION_STAT_ADD_WATCH(ASS3_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ASS3_MIRROR_TIME)  
		MISSION_STAT_ADD_WATCH(ASS3_MIRROR_CLEAN_ESCAPE)  
		MISSION_STAT_ADD_WATCH(ASS3_MIRROR_CASH)  
		MISSION_STAT_ADD_WATCH(ASS3_MIRROR_PERCENTAGE)  
		MISSION_STAT_ADD_WATCH(ASS3_MIRROR_MEDAL)  
		EXIT
	CASE SP_MISSION_ASSASSIN_4  
		MISSION_STAT_ADD_WATCH(ASS4_TIME)  
		MISSION_STAT_ADD_WATCH(ASS4_BUS_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ASS4_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(ASS4_HIT_N_RUN)  
		MISSION_STAT_ADD_WATCH(ASS4_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(ASS4_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(ASS4_WANTED_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(ASS4_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ASS4_ACCURACY)  
		MISSION_STAT_ADD_WATCH(ASS4_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ASS4_MIRROR_TIME)  
		MISSION_STAT_ADD_WATCH(ASS4_MIRROR_HIT_AND_RUN)  
		MISSION_STAT_ADD_WATCH(ASS4_MIRROR_CASH)  
		MISSION_STAT_ADD_WATCH(ASS4_MIRROR_PERCENTAGE)  
		MISSION_STAT_ADD_WATCH(ASS4_MIRROR_MEDAL)  
		EXIT
	CASE SP_MISSION_ASSASSIN_5  
		MISSION_STAT_ADD_WATCH(ASS5_TIME)  
		MISSION_STAT_ADD_WATCH(ASS5_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ASS5_KILLS)  
		MISSION_STAT_ADD_WATCH(ASS5_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(ASS5_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(ASS5_ACCURACY)  
		MISSION_STAT_ADD_WATCH(ASS5_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(ASS5_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(ASS5_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(ASS5_ESCAPE_TIME)  
		MISSION_STAT_ADD_WATCH(ASS5_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(ASS5_MIRROR_TIME)  
		MISSION_STAT_ADD_WATCH(ASS5_MIRROR_NO_FLY)  
		MISSION_STAT_ADD_WATCH(ASS5_MIRROR_CASH)  
		MISSION_STAT_ADD_WATCH(ASS5_MIRROR_PERCENT)  
		MISSION_STAT_ADD_WATCH(ASS5_MIRROR_MEDAL)  
		EXIT
	CASE SP_MISSION_CARSTEAL_2  
		MISSION_STAT_ADD_WATCH(CS2_NO_SCRATCH)  
		MISSION_STAT_ADD_WATCH(CS2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(CS2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(CS2_TARGETS_SCANNED)  
		MISSION_STAT_ADD_WATCH(CS2_CHAD_FOCUS_TIME)  
		MISSION_STAT_ADD_WATCH(CS2_TIME)  
		MISSION_STAT_ADD_WATCH(CS2_EAVESDROPPER)  
		MISSION_STAT_ADD_WATCH(CS2_SCANMAN)  
		MISSION_STAT_ADD_WATCH(CS2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_CARSTEAL_1  
		MISSION_STAT_ADD_WATCH(CS1_DROVE_BETWEEN_TRUCKS)  
		MISSION_STAT_ADD_WATCH(CS1_DROVE_BETWEEN_BUSES)  
		MISSION_STAT_ADD_WATCH(CS1_DROVE_THROUGH_TUNNEL)  
		MISSION_STAT_ADD_WATCH(CS1_TIME)  
		MISSION_STAT_ADD_WATCH(CS1_SPECIAL_USED)  
		MISSION_STAT_ADD_WATCH(CS1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(CS1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(CS1_SWITCHES)  
		MISSION_STAT_ADD_WATCH(CS1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_CARSTEAL_3  
		MISSION_STAT_ADD_WATCH(CS3_NO_SCRATCH)  
		MISSION_STAT_ADD_WATCH(CS3_FASTEST_SPEED)  
		MISSION_STAT_ADD_WATCH(CS3_ACTOR_KNOCKOUT)  
		MISSION_STAT_ADD_WATCH(CS3_EJECTOR_SEAT_USED)  
		MISSION_STAT_ADD_WATCH(CS3_RAN_OVER_ACTOR_AGAIN)  
		MISSION_STAT_ADD_WATCH(CS3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(CS3_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(CS3_DAMAGE)  
		MISSION_STAT_ADD_WATCH(CS3_TIME)  
		EXIT
	CASE SP_MISSION_CARSTEAL_4  
		MISSION_STAT_ADD_WATCH(CS4_TIME)  
		MISSION_STAT_ADD_WATCH(CS4_NO_SCRATCH)  
		MISSION_STAT_ADD_WATCH(CS4_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(CS4_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(CS4_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(CS4_COP_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(CS4_SHREDS)  
		EXIT
	CASE SP_MISSION_CHINESE_1  
		MISSION_STAT_ADD_WATCH(CHI1_BODY_COUNT)  
		MISSION_STAT_ADD_WATCH(CHI1_UNMARKED)  
		MISSION_STAT_ADD_WATCH(CHI1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(CHI1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(CHI1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(CHI1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(CHI1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(CHI1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(CHI1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(CHI1_VEHICLES_DESTROYED)  
		MISSION_STAT_ADD_WATCH(CHI1_GRENADE_LAUNCHER_KILLS)  
		MISSION_STAT_ADD_WATCH(CHI1_TIME)  
		EXIT
	CASE SP_MISSION_CHINESE_2  
		MISSION_STAT_ADD_WATCH(CHI2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(CHI2_UNMARKED)  
		MISSION_STAT_ADD_WATCH(CHI2_ONE_SHOT_TWO_KILLS)  
		MISSION_STAT_ADD_WATCH(CHI2_KILLS)  
		MISSION_STAT_ADD_WATCH(CHI2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(CHI2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(CHI2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(CHI2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(CHI2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(CHI2_GAS_POUR_TIME)  
		MISSION_STAT_ADD_WATCH(CHI2_GAS_USED)  
		MISSION_STAT_ADD_WATCH(CHI2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(CHI2_TIME)  
		EXIT
	CASE SP_MISSION_EXILE_1  
		MISSION_STAT_ADD_WATCH(EXL1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(EXL1_FLYING_HIGH)  
		MISSION_STAT_ADD_WATCH(EXL1_KILLS)  
		MISSION_STAT_ADD_WATCH(EXL1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(EXL1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(EXL1_DAMAGE)  
		MISSION_STAT_ADD_WATCH(EXL1_FREEFALL_TIME)  
		MISSION_STAT_ADD_WATCH(EXL1_HARRIER_CAM_TIME)  
		MISSION_STAT_ADD_WATCH(EXL1_BAIL_IN_CAR)  
		MISSION_STAT_ADD_WATCH(EXL1_TIME)  
		MISSION_STAT_ADD_WATCH(EXL1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_EXILE_2  
		MISSION_STAT_ADD_WATCH(EXL2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(EXL2_TIME)  
		MISSION_STAT_ADD_WATCH(EXL2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(EXL2_ANIMAL_KILLED)  
		MISSION_STAT_ADD_WATCH(EXL2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(EXL2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(EXL2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(EXL2_TIME_AS_CHOP)  
		MISSION_STAT_ADD_WATCH(EXL2_DEATH_FROM_ABOVE)  
		MISSION_STAT_ADD_WATCH(EXL2_SWITCHES)  
		MISSION_STAT_ADD_WATCH(EXL2_KILLS)  
		MISSION_STAT_ADD_WATCH(EXL2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(EXL2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_EXILE_3  
		MISSION_STAT_ADD_WATCH(EXL3_TIME)  
		MISSION_STAT_ADD_WATCH(EXL3_FASTEST_SPEED)  
		MISSION_STAT_ADD_WATCH(EXL3_JUMPED_AT_FIRST_OPPERTUNITY)  
		MISSION_STAT_ADD_WATCH(EXL3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(EXL3_KILLS)  
		MISSION_STAT_ADD_WATCH(EXL3_DAMAGE)  
		MISSION_STAT_ADD_WATCH(EXL3_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(EXL3_ACCURACY)  
		MISSION_STAT_ADD_WATCH(EXL3_SWITCHES)  
		MISSION_STAT_ADD_WATCH(EXL3_TRAIN_TIME)  
		MISSION_STAT_ADD_WATCH(EXL3_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FAMILY_1  
		MISSION_STAT_ADD_WATCH(FAM1_QUICK_CATCH)  
		MISSION_STAT_ADD_WATCH(FAM1_NOSCRATCH)  
		MISSION_STAT_ADD_WATCH(FAM1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FAM1_QUICK_BOARDING)  
		MISSION_STAT_ADD_WATCH(FAM1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FAM1_KILLS)  
		MISSION_STAT_ADD_WATCH(FAM1_TIME)  
		MISSION_STAT_ADD_WATCH(FAM1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FAMILY_2  
		MISSION_STAT_ADD_WATCH(FAM2_FASTEST_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM2_FELL_OFF_BIKE)  
		MISSION_STAT_ADD_WATCH(FAM2_FAST_SWIM)  
		MISSION_STAT_ADD_WATCH(FAM2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FAM2_MAX_CAR_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FAM2_CYCLING_TIME)  
		MISSION_STAT_ADD_WATCH(FAM2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FAM2_LOSE_JETSKI)  
		MISSION_STAT_ADD_WATCH(FAM2_CHOICES)  
		MISSION_STAT_ADD_WATCH(FAM2_TIME)  
		MISSION_STAT_ADD_WATCH(FAM2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FAMILY_3  
		MISSION_STAT_ADD_WATCH(FAM3_NOSCRATCH)  
		MISSION_STAT_ADD_WATCH(FAM3_TIME)  
		MISSION_STAT_ADD_WATCH(FAM3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FAM3_GOON_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(FAM3_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FAM3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FAM3_KILLS)  
		MISSION_STAT_ADD_WATCH(FAM3_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FAM3_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FAM3_VEHICLE_KILLS)  
		MISSION_STAT_ADD_WATCH(FAM3_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FAMILY_4  
		MISSION_STAT_ADD_WATCH(FAM4_LORRY_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM4_GOT_TOO_FAR_FROM_LAZ)  
		MISSION_STAT_ADD_WATCH(FAM4_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FAM4_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM4_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FAM4_LAZLOW_CAM_USE)  
		MISSION_STAT_ADD_WATCH(FAM4_TIME)  
		MISSION_STAT_ADD_WATCH(FAM4_COORD_KO)  
		MISSION_STAT_ADD_WATCH(FAM4_TRUCK_UNHOOKED)  
		MISSION_STAT_ADD_WATCH(FAM4_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FAMILY_5  
		MISSION_STAT_ADD_WATCH(FAM5_TIME)  
		MISSION_STAT_ADD_WATCH(FAM5_WARRIOR_POSE)  
		MISSION_STAT_ADD_WATCH(FAM5_TRIANGLE_POSE)  
		MISSION_STAT_ADD_WATCH(FAM5_SUN_SALUTATION_POSE)  
		MISSION_STAT_ADD_WATCH(FAM5_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FAM5_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FAM5_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FAM5_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM5_HOME_TIME)  
		MISSION_STAT_ADD_WATCH(FAM5_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FAMILY_6  
		MISSION_STAT_ADD_WATCH(FAM6_TIME)  
		MISSION_STAT_ADD_WATCH(FAM6_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FAM6_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FAM6_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FAM6_PIERCING_TIME)  
		MISSION_STAT_ADD_WATCH(FAM6_TATTOO_TIME)  
		MISSION_STAT_ADD_WATCH(FAM6_FRONT_OR_BACK)  
		MISSION_STAT_ADD_WATCH(FAM6_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FINALE_C1  
		MISSION_STAT_ADD_WATCH(FIN_TIME)  
		MISSION_STAT_ADD_WATCH(FIN_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FIN_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FIN_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(FIN_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FIN_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FIN_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FIN_KILLS)  
		MISSION_STAT_ADD_WATCH(FIN_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FIN_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FIN_STICKY_BOMBS_USED)  
		MISSION_STAT_ADD_WATCH(FIN_SNIPER_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FIN_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FIN_CHENG_BOMBER)  
		MISSION_STAT_ADD_WATCH(FIN_HAINES_HEADSHOT)  
		MISSION_STAT_ADD_WATCH(FIN_STRETCH_STRONGARM)  
		EXIT
	CASE SP_MISSION_FBI_1  
		MISSION_STAT_ADD_WATCH(FBI1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FBI1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FBI1_TIME)  
		MISSION_STAT_ADD_WATCH(FBI1_SPECKILLS)  
		MISSION_STAT_ADD_WATCH(FBI1_UNMARKED)  
		MISSION_STAT_ADD_WATCH(FBI1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FBI1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FBI1_KILLS)  
		MISSION_STAT_ADD_WATCH(FBI1_COP_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(FBI1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FBI_2  
		MISSION_STAT_ADD_WATCH(FBI2_TIME)  
		MISSION_STAT_ADD_WATCH(FBI2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FBI2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FBI2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FBI2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FBI2_KILLS)  
		MISSION_STAT_ADD_WATCH(FBI2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FBI2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FBI2_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FBI2_RAPPEL_TIME)  
		MISSION_STAT_ADD_WATCH(FBI2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FBI_3  
		MISSION_STAT_ADD_WATCH(FBI3_HEART_STOPPED)  
		MISSION_STAT_ADD_WATCH(FBI3_ELECTROCUTION)  
		MISSION_STAT_ADD_WATCH(FBI3_TOOTH)  
		MISSION_STAT_ADD_WATCH(FBI3_WRENCH)  
		MISSION_STAT_ADD_WATCH(FBI3_WATERBOARD)  
		MISSION_STAT_ADD_WATCH(FBI3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FBI3_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FBI3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FBI3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FBI3_ASSASSIN_TIME)  
		MISSION_STAT_ADD_WATCH(FBI3_TIME)  
		MISSION_STAT_ADD_WATCH(FBI3_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FBI_4  
		MISSION_STAT_ADD_WATCH(FBI4_UNMARKED)  
		MISSION_STAT_ADD_WATCH(FBI4_TIME)  
		MISSION_STAT_ADD_WATCH(FBI4_HELISHOT)  
		MISSION_STAT_ADD_WATCH(FBI4_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FBI4_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FBI4_KILLS)  
		MISSION_STAT_ADD_WATCH(FBI4_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FBI4_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FBI4_STICKY_BOMB_KILLS)  
		MISSION_STAT_ADD_WATCH(FBI4_VEHICLES_DESTROYED)  
		MISSION_STAT_ADD_WATCH(FBI4_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FBI_4_PREP_1  
		MISSION_STAT_ADD_WATCH(FBI4P1_TIME_TAKEN)  
		MISSION_STAT_ADD_WATCH(FBI4P1_VEHICLE_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FBI4P1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FBI4P1_LEVEL_LOSS)  
		MISSION_STAT_ADD_WATCH(FBI4P1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FBI_4_PREP_2  
		MISSION_STAT_ADD_WATCH(FBI4P2_TIME_TAKEN)  
		MISSION_STAT_ADD_WATCH(FBI4P2_VEHICLE_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FBI4P2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FBI4P2_SPECIAL_ABILITY_TIME)  
		EXIT
ENDSWITCH
SWITCH(m)
	CASE SP_MISSION_FBI_5  
		MISSION_STAT_ADD_WATCH(FBI5_STUNS)  
		MISSION_STAT_ADD_WATCH(FBI5_KILLS)  
		MISSION_STAT_ADD_WATCH(FBI5_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FBI5_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FBI5_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FBI5_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FBI5_BAR_CUT_TIME)  
		MISSION_STAT_ADD_WATCH(FBI5_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FBI5_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FBI5_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FBI5_TIME)  
		MISSION_STAT_ADD_WATCH(FBI5_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FRANKLIN_0  
		MISSION_STAT_ADD_WATCH(FRA0_NOSCRATCH)  
		MISSION_STAT_ADD_WATCH(FRA0_DOGCAM)  
		MISSION_STAT_ADD_WATCH(FRA0_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FRA0_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(FRA0_DOGGY_STYLE)  
		MISSION_STAT_ADD_WATCH(FRA0_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FRA0_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FRA0_TIME)  
		EXIT
	CASE SP_MISSION_FRANKLIN_1  
		MISSION_STAT_ADD_WATCH(FRA1_TIME)  
		MISSION_STAT_ADD_WATCH(FRA1_FRANKLIN_KILLS)  
		MISSION_STAT_ADD_WATCH(FRA1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FRA1_TREVOR_KILLS)  
		MISSION_STAT_ADD_WATCH(FRA1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FRA1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FRA1_KILLS)  
		MISSION_STAT_ADD_WATCH(FRA1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FRA1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FRA1_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FRA1_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FRA1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FRA1_COP_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(FRA1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FRANKLIN_2  
		MISSION_STAT_ADD_WATCH(FRA2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FRA2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FRA2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FRA2_KILLS)  
		MISSION_STAT_ADD_WATCH(FRA2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FRA2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(FRA2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FRA2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(FRA2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FRA2_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FRA2_VEHICLE_CHOSEN)  
		MISSION_STAT_ADD_WATCH(FRA2_TIME)  
		EXIT
	CASE SP_MISSION_LAMAR  
		MISSION_STAT_ADD_WATCH(LAM1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(LAM1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(LAM1_UNMARKED)  
		MISSION_STAT_ADD_WATCH(LAM1_TIME)  
		MISSION_STAT_ADD_WATCH(LAM1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(LAM1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(LAM1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(LAM1_KILLS)  
		MISSION_STAT_ADD_WATCH(LAM1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(LAM1_COP_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(LAM1_BODY_ARMOR)  
		MISSION_STAT_ADD_WATCH(LAM1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_LESTER_1  
		MISSION_STAT_ADD_WATCH(LES1A_TIME)  
		MISSION_STAT_ADD_WATCH(LES1A_CLEAR_POPUPS)  
		MISSION_STAT_ADD_WATCH(LES1A_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(LES1A_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(LES1A_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(LES1A_MOUSE_CLICKS)  
		MISSION_STAT_ADD_WATCH(LES1A_POPUPS_CLOSED)  
		MISSION_STAT_ADD_WATCH(LES1B_TIME_WATCHING)  
		MISSION_STAT_ADD_WATCH(LES1A_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_MARTIN_1  
		MISSION_STAT_ADD_WATCH(MAR1_FASTEST_SPEED)  
		MISSION_STAT_ADD_WATCH(MAR1_FELL_OFF_THE_BIKE)  
		MISSION_STAT_ADD_WATCH(MAR1_ONE_SHOT)  
		MISSION_STAT_ADD_WATCH(MAR1_TIME)  
		MISSION_STAT_ADD_WATCH(MAR1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MAR1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(MAR1_PLANE_HIT_TIME)  
		EXIT
	CASE SP_MISSION_MICHAEL_1  
		MISSION_STAT_ADD_WATCH(MIC1_TIME)  
		MISSION_STAT_ADD_WATCH(MIC1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(MIC1_KILLS)  
		MISSION_STAT_ADD_WATCH(MIC1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(MIC1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(MIC1_LANDING_TIME)  
		MISSION_STAT_ADD_WATCH(MIC1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(MIC1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(MIC1_PLANE_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC1_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(MIC1_STARTING_CHAR)  
		EXIT
	CASE SP_MISSION_MICHAEL_2  
		MISSION_STAT_ADD_WATCH(MIC2_TIMES_SWITCHED)  
		MISSION_STAT_ADD_WATCH(MIC2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(MIC2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(MIC2_MIKE_RESCUE_TIMER)  
		MISSION_STAT_ADD_WATCH(MIC2_WAYPOINT_USED)  
		MISSION_STAT_ADD_WATCH(MIC2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(MIC2_KILLS)  
		MISSION_STAT_ADD_WATCH(MIC2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(MIC2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC2_TIME_TO_LOSE_TRIADS)  
		MISSION_STAT_ADD_WATCH(MIC2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(MIC2_TIME_TO_SAVE_FRANKLIN)  
		MISSION_STAT_ADD_WATCH(MIC2_UNIQUE_TRIAD_DEATHS)  
		MISSION_STAT_ADD_WATCH(MIC2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(MIC2_TIME)  
		EXIT
	CASE SP_MISSION_MICHAEL_3  
		MISSION_STAT_ADD_WATCH(MIC3_TIME)  
		MISSION_STAT_ADD_WATCH(MIC3_KILLS)  
		MISSION_STAT_ADD_WATCH(MIC3_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(MIC3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(MIC3_ACCURACY)  
		MISSION_STAT_ADD_WATCH(MIC3_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(MIC3_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(MIC3_SWITCHES)  
		MISSION_STAT_ADD_WATCH(MIC3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(MIC3_VEHICLES_DESTROYED)  
		MISSION_STAT_ADD_WATCH(MIC3_HELIKILL)  
		EXIT
	CASE SP_MISSION_MICHAEL_4  
		MISSION_STAT_ADD_WATCH(MIC4_TIME)  
		MISSION_STAT_ADD_WATCH(MIC4_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC4_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(MIC4_DAMAGE)  
		MISSION_STAT_ADD_WATCH(MIC4_KILLS)  
		MISSION_STAT_ADD_WATCH(MIC4_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(MIC4_ACCURACY)  
		MISSION_STAT_ADD_WATCH(MIC4_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(MIC4_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(MIC4_HOME_TIME)  
		MISSION_STAT_ADD_WATCH(MIC4_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(MIC4_HEADSHOT_RESCUE)  
		MISSION_STAT_ADD_WATCH(MIC4_VEHICLE_CHOSEN)  
		EXIT
	CASE SP_MISSION_PROLOGUE  
		MISSION_STAT_ADD_WATCH(PRO_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(PRO_MICHAEL_SAVE_TIME)  
		MISSION_STAT_ADD_WATCH(PRO_GETAWAY_TIME)  
		MISSION_STAT_ADD_WATCH(PRO_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(PRO_KILLS)  
		MISSION_STAT_ADD_WATCH(PRO_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(PRO_ACCURACY)  
		MISSION_STAT_ADD_WATCH(PRO_DAMAGE)  
		MISSION_STAT_ADD_WATCH(PRO_STAY_ON_ROAD)  
		MISSION_STAT_ADD_WATCH(PRO_SWITCHES)  
		MISSION_STAT_ADD_WATCH(PRO_TIME)  
		MISSION_STAT_ADD_WATCH(PRO_CAR_DAMAGE)  
		EXIT
	CASE SP_MISSION_SOLOMON_2  
		MISSION_STAT_ADD_WATCH(SOL2_TIME)  
		MISSION_STAT_ADD_WATCH(SOL2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(SOL2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(SOL2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(SOL2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(SOL2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(SOL2_INNOCENT_KILLS)  
		MISSION_STAT_ADD_WATCH(SOL2_HEADSHOTS)  
		EXIT
	CASE SP_MISSION_SOLOMON_3  
		MISSION_STAT_ADD_WATCH(SOL3_TIME)  
		MISSION_STAT_ADD_WATCH(SOL3_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(SOL3_DAMAGE)  
		MISSION_STAT_ADD_WATCH(SOL3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(SOL3_ACCURACY)  
		MISSION_STAT_ADD_WATCH(SOL3_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(SOL3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(SOL3_KILLS)  
		MISSION_STAT_ADD_WATCH(SOL3_COP_LOSS_TIME)  
		MISSION_STAT_ADD_WATCH(SOL3_INNOCENT_KILLS)  
		MISSION_STAT_ADD_WATCH(SOL3_NEWS_HELI_CAM_TIME)  
		MISSION_STAT_ADD_WATCH(SOL3_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_TREVOR_1  
		MISSION_STAT_ADD_WATCH(TRV1_NO_SURVIVORS)  
		MISSION_STAT_ADD_WATCH(TRV1_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(TRV1_TIME)  
		MISSION_STAT_ADD_WATCH(TRV1_TRAILER_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV1_BIKERS_KILLED_BEFORE_LOCATION)  
		MISSION_STAT_ADD_WATCH(TRV1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(TRV1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(TRV1_KILLS)  
		MISSION_STAT_ADD_WATCH(TRV1_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(TRV1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(TRV1_ACCURACY)  
		EXIT
	CASE SP_MISSION_TREVOR_2  
		MISSION_STAT_ADD_WATCH(TRV2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(TRV2_TIME)  
		MISSION_STAT_ADD_WATCH(TRV2_RACEBACK_WON)  
		MISSION_STAT_ADD_WATCH(TRV2_UNDERBRIDGES)  
		MISSION_STAT_ADD_WATCH(TRV2_RACE_TIME)  
		MISSION_STAT_ADD_WATCH(TRV2_ATV_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV2_ATV_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(TRV2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(TRV2_KILLS)  
		MISSION_STAT_ADD_WATCH(TRV2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV2_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(TRV2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(TRV2_PLANE_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(TRV2_PLANE_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV2_SHOTS_TO_KILL_HELI)  
		MISSION_STAT_ADD_WATCH(TRV2_ALL_BIKERS_KILLED_ON_WING)  
		EXIT
	CASE SP_MISSION_TREVOR_3  
		MISSION_STAT_ADD_WATCH(TRV3_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(TRV3_NOT_DETECTED)  
		MISSION_STAT_ADD_WATCH(TRV3_UNMARKED)  
		MISSION_STAT_ADD_WATCH(TRV3_ALL_TRAILERS_AT_ONCE)  
		MISSION_STAT_ADD_WATCH(TRV3_STEALTH_KILLS)  
		MISSION_STAT_ADD_WATCH(TRV3_KILLS)  
		MISSION_STAT_ADD_WATCH(TRV3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(TRV3_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(TRV3_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(TRV3_ACCURACY)  
		MISSION_STAT_ADD_WATCH(TRV3_STICKY_BOMBS_USED)  
		MISSION_STAT_ADD_WATCH(TRV3_TIME)  
		EXIT
	CASE SP_HEIST_AGENCY_1  
		MISSION_STAT_ADD_WATCH(AH1_EAGLE_EYE)  
		MISSION_STAT_ADD_WATCH(AH1_MISSED_A_SPOT)  
		MISSION_STAT_ADD_WATCH(AH1_CLEANED_OUT)  
		MISSION_STAT_ADD_WATCH(AH1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(AH1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(AH1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(AH1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_AGENCY_2  
		MISSION_STAT_ADD_WATCH(AH2_QUICK_GETAWAY)  
		MISSION_STAT_ADD_WATCH(AH2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(AH2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(AH2_METHOD)  
		MISSION_STAT_ADD_WATCH(AH2_TIME)  
		MISSION_STAT_ADD_WATCH(AH2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_AGENCY_PREP_1  
		MISSION_STAT_ADD_WATCH(AH1P_FIRETRUCK_SPEED)  
		MISSION_STAT_ADD_WATCH(AH1P_FACTORY_TIME)  
		MISSION_STAT_ADD_WATCH(AH1P_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(AHP1_TRUCKCALLED)  
		MISSION_STAT_ADD_WATCH(AHP1_NOSCRATCH)  
		MISSION_STAT_ADD_WATCH(AH1P_TIME)  
		MISSION_STAT_ADD_WATCH(AH1P_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_AGENCY_3A  
		MISSION_STAT_ADD_WATCH(AH3A_KILLS)  
		MISSION_STAT_ADD_WATCH(AH3A_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(AH3A_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(AH3A_ACCURACY)  
		MISSION_STAT_ADD_WATCH(AH3A_DAMAGE)  
		MISSION_STAT_ADD_WATCH(AH3A_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(AH3A_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(AH3A_SWITCHES)  
		MISSION_STAT_ADD_WATCH(AH3A_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(AH3A_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(AH3A_TIME)  
		MISSION_STAT_ADD_WATCH(AH3A_OXYGEN_REMAINING)  
		MISSION_STAT_ADD_WATCH(AH3A_FLOOR_MOP_TIME)  
		MISSION_STAT_ADD_WATCH(AH3A_ABSEIL_TIME)  
		EXIT
	CASE SP_HEIST_AGENCY_3B  
		MISSION_STAT_ADD_WATCH(AH3B_SWITCHES)  
		MISSION_STAT_ADD_WATCH(AH3B_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(AH3B_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(AH3B_KILLS)  
		MISSION_STAT_ADD_WATCH(AH3B_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(AH3B_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(AH3B_DAMAGE)  
		MISSION_STAT_ADD_WATCH(AH3B_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(AH3B_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(AH3B_ACCURACY)  
		MISSION_STAT_ADD_WATCH(AH3B_FREEFALL_TIME)  
		MISSION_STAT_ADD_WATCH(AH3B_LANDING_ACCURACY)  
		MISSION_STAT_ADD_WATCH(AH3B_HACKING_ERRORS)  
		MISSION_STAT_ADD_WATCH(AH3B_HACKING_TIME)  
		MISSION_STAT_ADD_WATCH(AH3B_TIME)  
		EXIT
	CASE SP_HEIST_DOCKS_1  
		MISSION_STAT_ADD_WATCH(DH1_TIME)  
		MISSION_STAT_ADD_WATCH(DH1_EMPLOYEE_OF_THE_MONTH)  
		MISSION_STAT_ADD_WATCH(DH1_PERFECT_SURVEILLANCE)  
		MISSION_STAT_ADD_WATCH(DH1_HONEST_DAYS_WORK)  
		MISSION_STAT_ADD_WATCH(DH1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(DH1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(DH1_FORKLIFT_TIME)  
		MISSION_STAT_ADD_WATCH(DH1_CRANE_TIME)  
		MISSION_STAT_ADD_WATCH(DH1_PHOTOS_TAKEN)  
		MISSION_STAT_ADD_WATCH(DH1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_DOCKS_PREP_1  
		MISSION_STAT_ADD_WATCH(DH1P_SUB_TIME)  
		MISSION_STAT_ADD_WATCH(DH1P_SUB_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH1P_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(DH1P_TRUCK_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH1P_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(DHP1_TIME)  
		MISSION_STAT_ADD_WATCH(DHP1_NO_BOARDING)  
		MISSION_STAT_ADD_WATCH(DH1P_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_DOCKS_PREP_2B  
		MISSION_STAT_ADD_WATCH(DH2BP_KILLS)  
		MISSION_STAT_ADD_WATCH(DH2BP_ACCURACY)  
		MISSION_STAT_ADD_WATCH(DH2BP_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(DH2BP_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(DH2BP_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH2BP_HELI_TIME)  
		MISSION_STAT_ADD_WATCH(DH2BP_MAX_HELI_SPEED)  
		MISSION_STAT_ADD_WATCH(DH2BP_HELI_TAKEDOWN_TIME)  
		MISSION_STAT_ADD_WATCH(DHP2_TIME)  
		MISSION_STAT_ADD_WATCH(DH2BP_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_DOCKS_2A  
		MISSION_STAT_ADD_WATCH(DH2A_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(DH2A_ACCURACY)  
		MISSION_STAT_ADD_WATCH(DH2A_KILLS)  
		MISSION_STAT_ADD_WATCH(DH2A_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH2A_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(DH2A_ACTUAL_TAKE)  
		MISSION_STAT_ADD_WATCH(DH2A_PERSONAL_TAKE)  
		MISSION_STAT_ADD_WATCH(DH2A_SWITCHES)  
		MISSION_STAT_ADD_WATCH(DH2A_STEALTH_KILLS)  
		MISSION_STAT_ADD_WATCH(DH2A_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH2A_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(DH2A_TIME_TO_FIND_CONTAINER)  
		MISSION_STAT_ADD_WATCH(DH2A_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(DH2A_TIME)  
		MISSION_STAT_ADD_WATCH(DH2A_NO_ALARMS)  
		MISSION_STAT_ADD_WATCH(DH2A_TIME_TO_CLEAR)  
		EXIT
	CASE SP_HEIST_DOCKS_2B  
		MISSION_STAT_ADD_WATCH(DH2B_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH2B_ACTUAL_TAKE)  
		MISSION_STAT_ADD_WATCH(DH2B_PERSONAL_TAKE)  
		MISSION_STAT_ADD_WATCH(DH2B_SWITCHES)  
		MISSION_STAT_ADD_WATCH(DH2B_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(DH2B_SUB_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH2B_HELI_DAMAGE)  
		MISSION_STAT_ADD_WATCH(DH2B_TIME_TO_FIND_CONTAINER)  
		MISSION_STAT_ADD_WATCH(DH2B_TIME)  
		MISSION_STAT_ADD_WATCH(DH2B_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(DH2B_ACCURACY)  
		MISSION_STAT_ADD_WATCH(DH2B_KILLED_ALL_MERRYWEATHER)  
		MISSION_STAT_ADD_WATCH(DH2B_ESCAPE_TIME)  
		MISSION_STAT_ADD_WATCH(DH2B_KILLS)  
		MISSION_STAT_ADD_WATCH(DH2B_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(DH2B_BULLETS_FIRED)  
		EXIT
	CASE SP_HEIST_FINALE_1  
		MISSION_STAT_ADD_WATCH(FH1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FH1_HELI_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FH1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FH1_MAX_HELI_SPEED)  
		MISSION_STAT_ADD_WATCH(FH1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FH1_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FH1_PERFECT_DISTANCE)  
		MISSION_STAT_ADD_WATCH(FH1_FIND_HOLE_TIME)  
		MISSION_STAT_ADD_WATCH(FH1_UNDER_BRIDGE)  
		MISSION_STAT_ADD_WATCH(FH1_THROUGH_TUNNEL)  
		MISSION_STAT_ADD_WATCH(FH1_TIME)  
		MISSION_STAT_ADD_WATCH(FH1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_PREP_A  
		MISSION_STAT_ADD_WATCH(FHPA_ESCAPE_TIME)  
		MISSION_STAT_ADD_WATCH(FHPA_DAMAGE_TO_WAGON)  
		MISSION_STAT_ADD_WATCH(FHPA_TIME)  
		MISSION_STAT_ADD_WATCH(FHPA_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_PREP_B  
		MISSION_STAT_ADD_WATCH(FHPB_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FHPB_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FHPB_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FHPB_TRUCK_TIME)  
		MISSION_STAT_ADD_WATCH(FHPB_SNEAK_THIEF)  
		MISSION_STAT_ADD_WATCH(FHPB_TIME)  
		MISSION_STAT_ADD_WATCH(FHPB_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_2A  
		MISSION_STAT_ADD_WATCH(FH2A_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FH2A_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FH2A_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(FH2A_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FH2A_ACTUAL_TAKE)  
		MISSION_STAT_ADD_WATCH(FH2A_CREW_TAKE)  
		MISSION_STAT_ADD_WATCH(FH2A_KILLS)  
		MISSION_STAT_ADD_WATCH(FH2A_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FH2A_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FH2A_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FH2A_TRAFFIC_LIGHT_CHANGES)  
		MISSION_STAT_ADD_WATCH(FH2A_PERSONAL_TAKE)  
		MISSION_STAT_ADD_WATCH(FH2A_TIME)  
		MISSION_STAT_ADD_WATCH(FH2A_ACCURACY)  
		EXIT
ENDSWITCH
SWITCH(m)
	CASE SP_HEIST_FINALE_2B  
		MISSION_STAT_ADD_WATCH(FH2B_ACTUAL_TAKE)  
		MISSION_STAT_ADD_WATCH(FH2B_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FH2B_CREW_TAKE)  
		MISSION_STAT_ADD_WATCH(FH2B_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FH2B_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(FH2B_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FH2B_KILLS)  
		MISSION_STAT_ADD_WATCH(FH2B_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FH2B_PERSONAL_TAKE)  
		MISSION_STAT_ADD_WATCH(FH2B_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(FH2B_SWITCHES)  
		MISSION_STAT_ADD_WATCH(FH2B_TIME)  
		MISSION_STAT_ADD_WATCH(FH2B_CHASE_TIME)  
		MISSION_STAT_ADD_WATCH(FH2B_GOLD_DROP_TIME)  
		MISSION_STAT_ADD_WATCH(FH2B_ACCURACY)  
		EXIT
	CASE SP_HEIST_JEWELRY_1  
		MISSION_STAT_ADD_WATCH(JH1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JH1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(JH1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(JH1_PHOTOS_TAKEN)  
		MISSION_STAT_ADD_WATCH(JH1_TIME)  
		MISSION_STAT_ADD_WATCH(JH1_PERFECT_PIC)  
		MISSION_STAT_ADD_WATCH(JH1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_JEWELRY_PREP_1A  
		MISSION_STAT_ADD_WATCH(JH1P_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(JH1P_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(JH1P_VAN_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JHP1A_SNEAKY_PEST)  
		MISSION_STAT_ADD_WATCH(JHP1A_TIME)  
		MISSION_STAT_ADD_WATCH(JH1P_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_JEWELRY_PREP_2A  
		MISSION_STAT_ADD_WATCH(JH2AP_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(JH2AP_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JH2AP_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(JH2AP_FACTORY_TIME)  
		MISSION_STAT_ADD_WATCH(JHP2A_LOOSE_CARGO)  
		MISSION_STAT_ADD_WATCH(JHP2A_TIME)  
		MISSION_STAT_ADD_WATCH(JHP2A_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_RURAL_1  
		MISSION_STAT_ADD_WATCH(RH1_LEISURELY_DRIVE)  
		MISSION_STAT_ADD_WATCH(RH1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(RH1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(RH1_ACCURACY)  
		MISSION_STAT_ADD_WATCH(RH1_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(RH1_SWITCHES)  
		MISSION_STAT_ADD_WATCH(RH1_WINNER)  
		MISSION_STAT_ADD_WATCH(RH1_TIME)  
		MISSION_STAT_ADD_WATCH(RH1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_RURAL_PREP_1  
		MISSION_STAT_ADD_WATCH(RH1P_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(RH1P_KILLS)  
		MISSION_STAT_ADD_WATCH(RH1P_ACCURACY)  
		MISSION_STAT_ADD_WATCH(RH1P_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(RH1P_DAMAGE)  
		MISSION_STAT_ADD_WATCH(RH1P_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(RH1P_VAN_TO_LAB_TIME)  
		MISSION_STAT_ADD_WATCH(RH1P_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(RH1P_CONVOY_STOPPED)  
		MISSION_STAT_ADD_WATCH(RH1P_TIME)  
		MISSION_STAT_ADD_WATCH(RH1P_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_RURAL_2  
		MISSION_STAT_ADD_WATCH(RH2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(RH2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(RH2_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(RH2_KILLS)  
		MISSION_STAT_ADD_WATCH(RH2_DAMAGE)  
		MISSION_STAT_ADD_WATCH(RH2_ACTUAL_TAKE)  
		MISSION_STAT_ADD_WATCH(RH2_PERSONAL_TAKE)  
		MISSION_STAT_ADD_WATCH(RH2_HEADSHOTS)  
		MISSION_STAT_ADD_WATCH(RH2_ACCURACY)  
		MISSION_STAT_ADD_WATCH(RH2_TIME)  
		MISSION_STAT_ADD_WATCH(RH2_COLLATERAL_DAMAGE)  
		MISSION_STAT_ADD_WATCH(RH2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_JEWELRY_2  
		MISSION_STAT_ADD_WATCH(JH2A_ACTUAL_TAKE)  
		MISSION_STAT_ADD_WATCH(JH2A_PERSONAL_TAKE)  
		MISSION_STAT_ADD_WATCH(JH2A_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(JH2A_TIME)  
		MISSION_STAT_ADD_WATCH(JH2A_CASE_GRAB_TIME)  
		MISSION_STAT_ADD_WATCH(JH2A_FRANKLIN_BIKE_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JH2A_CASES_SMASHED)  
		MISSION_STAT_ADD_WATCH(JH2A_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JH2A_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(JH2A_INNOCENTS_KILLED)  
		EXIT
	CASE SP_MISSION_FBI_4_PREP_4  
		MISSION_STAT_ADD_WATCH(FBI4P4_FACE_TIME)  
		MISSION_STAT_ADD_WATCH(FBI4P4_CLICHE)  
		MISSION_STAT_ADD_WATCH(FBI4P4_TIME)  
		MISSION_STAT_ADD_WATCH(FBI4P4_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FBI_4_PREP_5  
		MISSION_STAT_ADD_WATCH(FBI4P5_QUICK_SHOPPER)  
		MISSION_STAT_ADD_WATCH(FBI4P5_UNITED_COLOURS)  
		MISSION_STAT_ADD_WATCH(FBI4P5_TIME)  
		MISSION_STAT_ADD_WATCH(FBI4P5_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FINALE_A  
		MISSION_STAT_ADD_WATCH(FINA_KILLTREV)  
		MISSION_STAT_ADD_WATCH(FINA_TIME)  
		MISSION_STAT_ADD_WATCH(FINA_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FINA_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINA_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FINA_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_FINALE_B  
		MISSION_STAT_ADD_WATCH(FINB_KILLMIC)  
		MISSION_STAT_ADD_WATCH(FINB_TIME)  
		MISSION_STAT_ADD_WATCH(FINB_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FINB_BULLETS_FIRED)  
		MISSION_STAT_ADD_WATCH(FINB_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINB_CHOICE)  
		MISSION_STAT_ADD_WATCH(FINB_FOOTCHASE_TIME)  
		MISSION_STAT_ADD_WATCH(FINB_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINB_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_MISSION_SOLOMON_1  
		MISSION_STAT_ADD_WATCH(SOL1_TIME)  
		MISSION_STAT_ADD_WATCH(SOL1_SILENT_TAKEDOWNS)  
		MISSION_STAT_ADD_WATCH(SOL1_BRAWL_DAMAGE)  
		MISSION_STAT_ADD_WATCH(SOL1_PERFECT_LANDING)  
		MISSION_STAT_ADD_WATCH(SOL1_SPECIAL_ABILITY_TIME)  
		MISSION_STAT_ADD_WATCH(SOL1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(SOL1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(SOL1_INNOCENT_KILLS)  
		MISSION_STAT_ADD_WATCH(SOL1_HELI_DAMAGE)  
		EXIT
	CASE SP_MISSION_TREVOR_4  
		MISSION_STAT_ADD_WATCH(TRV4_TIME)  
		MISSION_STAT_ADD_WATCH(TRV4_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(TRV4_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(TRV4_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(TRV4_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_PREP_C1  
		MISSION_STAT_ADD_WATCH(FINPC1_MAPPED)  
		MISSION_STAT_ADD_WATCH(FINPC1_TIME)  
		MISSION_STAT_ADD_WATCH(FINPC1_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINPC1_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FINPC1_TIME_TO_GARAGE)  
		MISSION_STAT_ADD_WATCH(FINPC1_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FINPC1_MOD_GAUNTLET)  
		MISSION_STAT_ADD_WATCH(FINPC1_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_PREP_C2  
		MISSION_STAT_ADD_WATCH(FINPC2_MAPPED)  
		MISSION_STAT_ADD_WATCH(FINPC2_TIME)  
		MISSION_STAT_ADD_WATCH(FINPC2_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINPC2_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FINPC2_TIME_TO_GARAGE)  
		MISSION_STAT_ADD_WATCH(FINPC2_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FINPC2_MOD_GAUNTLET)  
		MISSION_STAT_ADD_WATCH(FINPC2_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_PREP_C3  
		MISSION_STAT_ADD_WATCH(FINPC3_MAPPED)  
		MISSION_STAT_ADD_WATCH(FINPC3_TIME)  
		MISSION_STAT_ADD_WATCH(FINPC3_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINPC3_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(FINPC3_TIME_TO_GARAGE)  
		MISSION_STAT_ADD_WATCH(FINPC3_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(FINPC3_MOD_GAUNTLET)  
		MISSION_STAT_ADD_WATCH(FINPC3_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_FINALE_PREP_D  
		MISSION_STAT_ADD_WATCH(FINPD_TIME)  
		MISSION_STAT_ADD_WATCH(FINPD_UNDETECTED)  
		MISSION_STAT_ADD_WATCH(FINPD_KILLS)  
		MISSION_STAT_ADD_WATCH(FINPD_STEALTH_KILLS)  
		MISSION_STAT_ADD_WATCH(FINPD_MAX_HELI_SPEED)  
		MISSION_STAT_ADD_WATCH(FINPD_HELI_DAMAGE)  
		MISSION_STAT_ADD_WATCH(FINPD_SPECIAL_ABILITY_TIME)  
		EXIT
	CASE SP_HEIST_JEWELRY_PREP_1B  
		MISSION_STAT_ADD_WATCH(JHP1B_SWIFT_GETAWAY)  
		MISSION_STAT_ADD_WATCH(JHP1B_TIME)  
		MISSION_STAT_ADD_WATCH(JHP1B_CAR_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JHP1B_DAMAGE)  
		MISSION_STAT_ADD_WATCH(JHP1B_MAX_SPEED)  
		MISSION_STAT_ADD_WATCH(JHP1B_INNOCENTS_KILLED)  
		MISSION_STAT_ADD_WATCH(JHP1B_SPECIAL_ABILITY_TIME)  
		EXIT
ENDSWITCH
	SCRIPT_ASSERT("INTERNAL_FLOW_PRIME_STATS_FOR_MISSION : Mission has no stats registered, if this is intentional please inform Default Levels. so he can add this mission to the stat exclusion list. Thanks!")
#ENDIF
#ENDIF
ENDPROC

PROC INTERNAL_FLOW_PRIME_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)
SWITCH(m)
	CASE RC_BARRY_1  
		MISSION_STAT_ADD_WATCH(BA1_DAMAGE_TAKEN)  
		MISSION_STAT_ADD_WATCH(BA1_KILLCHAIN)  
		EXIT
	CASE RC_BARRY_2  
		MISSION_STAT_ADD_WATCH(BA2_VANS_DESTROYED)  
		MISSION_STAT_ADD_WATCH(BA2_DANCING_CLOWNS_KILLED)  
		EXIT
	CASE RC_BARRY_3A  
		MISSION_STAT_ADD_WATCH(BA3A_DELIVERY_TIME)  
		MISSION_STAT_ADD_WATCH(BA3A_AVOID_STAKEOUT)  
		EXIT
	CASE RC_BARRY_3C  
		MISSION_STAT_ADD_WATCH(BA3C_STASH_UNHOOKED)  
		MISSION_STAT_ADD_WATCH(BA3C_DELIVERY_TIME)  
		EXIT
	CASE RC_DREYFUSS_1  
		MISSION_STAT_ADD_WATCH(DR1_DREYFUSS_KILLED)  
		EXIT
	CASE RC_EPSILON_4  
		MISSION_STAT_ADD_WATCH(EP4_ARTIFACT_DETECTOR_USED)  
		EXIT
	CASE RC_EPSILON_6  
		MISSION_STAT_ADD_WATCH(EP6_PERFECT_LANDING)  
		MISSION_STAT_ADD_WATCH(EP6_UNDER_BRIDGE)  
		EXIT
	CASE RC_EPSILON_8  
		MISSION_STAT_ADD_WATCH(EP8_SECURITY_WIPED_OUT)  
		MISSION_STAT_ADD_WATCH(EP8_MONEY_STOLEN)  
		EXIT
	CASE RC_EXTREME_1  
		MISSION_STAT_ADD_WATCH(EXT1_FALL_TIME)  
		MISSION_STAT_ADD_WATCH(EXT1_BIG_AIR)  
		MISSION_STAT_ADD_WATCH(EXT1_RACE_WON)  
		EXIT
	CASE RC_EXTREME_2  
		MISSION_STAT_ADD_WATCH(EXT2_LEAP_FROM_ATV_TO_WATER)  
		MISSION_STAT_ADD_WATCH(EXT2_NUMBER_OF_SPINS)  
		EXIT
	CASE RC_EXTREME_3  
		MISSION_STAT_ADD_WATCH(EXT3_DARE_DEVIL)  
		MISSION_STAT_ADD_WATCH(EXT3_PERFECT_LANDING)  
		EXIT
	CASE RC_EXTREME_4  
		MISSION_STAT_ADD_WATCH(EXT4_FALL_SURVIVED)  
		EXIT
	CASE RC_FANATIC_1  
		MISSION_STAT_ADD_WATCH(FA1_SHORTCUT_USED)  
		EXIT
	CASE RC_FANATIC_2  
		MISSION_STAT_ADD_WATCH(FA2_BUMPED_INTO_MARY_ANN)  
		MISSION_STAT_ADD_WATCH(FA2_QUICK_WIN)  
		EXIT
	CASE RC_FANATIC_3  
		MISSION_STAT_ADD_WATCH(FA3_SHORTCUT_USED)  
		EXIT
	CASE RC_HUNTING_1  
		MISSION_STAT_ADD_WATCH(HU1_TWO_COYOTES_KILLED_IN_ONE)  
		MISSION_STAT_ADD_WATCH(HU1_TYRE_SHOOTING_ACCURACY)  
		MISSION_STAT_ADD_WATCH(HU1_HIT_EACH_SATELLITE_FIRST_GO)  
		EXIT
	CASE RC_HUNTING_2  
		MISSION_STAT_ADD_WATCH(HU2_DETECTED_BY_ELK)  
		MISSION_STAT_ADD_WATCH(HU2_ELK_HEADSHOTS)  
		EXIT
	CASE RC_JOSH_2  
		MISSION_STAT_ADD_WATCH(JO2_JOSH_MELEED)  
		MISSION_STAT_ADD_WATCH(JO2_STOPPED_IN_TIME)  
		EXIT
	CASE RC_JOSH_3  
		MISSION_STAT_ADD_WATCH(JO3_ESCAPE_WITHOUT_ALERTING_COPS)  
		MISSION_STAT_ADD_WATCH(JO3_POUR_FUEL_IN_ONE_GO)  
		EXIT
	CASE RC_JOSH_4  
		MISSION_STAT_ADD_WATCH(JO4_JOSH_KILLED_BEFORE_ESCAPE)  
		MISSION_STAT_ADD_WATCH(JO4_ESCAPE_IN_PARKED_COP_CAR)  
		EXIT
	CASE RC_MINUTE_1  
		MISSION_STAT_ADD_WATCH(MIN1_VEHICLE_TAKEN_AFTER_STUNS)  
		MISSION_STAT_ADD_WATCH(MIN1_FAST_STOP)  
		EXIT
	CASE RC_MINUTE_2  
		MISSION_STAT_ADD_WATCH(MIN2_FIRST_CAPTURE)  
		MISSION_STAT_ADD_WATCH(MIN2_SECOND_CAPTURE)  
		MISSION_STAT_ADD_WATCH(MIN2_STUNNED_ALL)  
		EXIT
	CASE RC_MINUTE_3  
		MISSION_STAT_ADD_WATCH(MIN3_STUNNED_AND_KILLED)  
		MISSION_STAT_ADD_WATCH(MIN3_KILL_BEFORE_ESCAPE)  
		EXIT
	CASE RC_NIGEL_1A  
		MISSION_STAT_ADD_WATCH(NI1A_PLAYER_DAMAGE_DURING_BRAWL)  
		MISSION_STAT_ADD_WATCH(NI1A_ENTOURAGE)  
		EXIT
	CASE RC_NIGEL_1B  
		MISSION_STAT_ADD_WATCH(NI1B_GARDENER_TAKEN_OUT)  
		MISSION_STAT_ADD_WATCH(NI1B_CLOTHES_TAKEN_NO_ALERTS)  
		EXIT
	CASE RC_NIGEL_1C  
		MISSION_STAT_ADD_WATCH(NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY)  
		EXIT
	CASE RC_NIGEL_1D  
		MISSION_STAT_ADD_WATCH(NI1D_GOLF_CLUB_STOLEN_IN_TIME)  
		MISSION_STAT_ADD_WATCH(NI1D_HEADSHOTTED_STANKY)  
		MISSION_STAT_ADD_WATCH(NI1D_KILLED_STANKY_AND_GUARDS)  
		EXIT
	CASE RC_NIGEL_2  
		MISSION_STAT_ADD_WATCH(NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI)  
		MISSION_STAT_ADD_WATCH(NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH)  
		MISSION_STAT_ADD_WATCH(NI2_VEHICLE_DAMAGE)  
		EXIT
	CASE RC_NIGEL_3  
		MISSION_STAT_ADD_WATCH(NI3_BAILED_AT_LAST_MOMENT)  
		MISSION_STAT_ADD_WATCH(NI3_REVERSED_TO_KILL)  
		EXIT
	CASE RC_PAPARAZZO_1  
		MISSION_STAT_ADD_WATCH(PAP1_TAKEN_OUT_IN_ONE_SWING)  
		MISSION_STAT_ADD_WATCH(PAP1_PICTURES_SNAPPED)  
		EXIT
ENDSWITCH
SWITCH(m)
	CASE RC_PAPARAZZO_2  
		MISSION_STAT_ADD_WATCH(PAP2_POOL_JUMP)  
		MISSION_STAT_ADD_WATCH(PAP2_FACE_RECOG_PERCENT)  
		EXIT
	CASE RC_PAPARAZZO_3A  
		MISSION_STAT_ADD_WATCH(PAP3A_FAR_FROM_POPPY)  
		MISSION_STAT_ADD_WATCH(PAP3A_PHOTO_IN_CUFFS)  
		EXIT
	CASE RC_PAPARAZZO_3B  
		MISSION_STAT_ADD_WATCH(PAP3B_PHOTO_SPOTTED)  
		MISSION_STAT_ADD_WATCH(PAP3B_PHOTO_OF_USE)  
		EXIT
	CASE RC_PAPARAZZO_4  
		MISSION_STAT_ADD_WATCH(PAP4_ENTIRE_CREW_KILLED_IN_ONE)  
		EXIT
	CASE RC_THELASTONE  
		MISSION_STAT_ADD_WATCH(TLO_WOUNDS)  
		MISSION_STAT_ADD_WATCH(TLO_AMBIENT_ANIMAL_KILLS)  
		MISSION_STAT_ADD_WATCH(TLO_TO_SITE_ON_FOOT)  
		EXIT
	CASE RC_TONYA_1  
		MISSION_STAT_ADD_WATCH(TON1_TIME)  
		MISSION_STAT_ADD_WATCH(TON1_UNHOOK)  
		MISSION_STAT_ADD_WATCH(TON1_QUICK_UNHOOK)  
		EXIT
	CASE RC_TONYA_2  
		MISSION_STAT_ADD_WATCH(TON2_TIME)  
		MISSION_STAT_ADD_WATCH(TON2_UNHOOK)  
		MISSION_STAT_ADD_WATCH(TON2_QUICK_UNHOOK)  
		EXIT
	CASE RC_TONYA_3  
		MISSION_STAT_ADD_WATCH(TON3_TIME)  
		MISSION_STAT_ADD_WATCH(TON3_UNHOOK)  
		MISSION_STAT_ADD_WATCH(TON3_QUICK_UNHOOK)  
		EXIT
	CASE RC_TONYA_4  
		MISSION_STAT_ADD_WATCH(TON4_TIME)  
		MISSION_STAT_ADD_WATCH(TON4_UNHOOK)  
		MISSION_STAT_ADD_WATCH(TON4_QUICK_UNHOOK)  
		EXIT
	CASE RC_TONYA_5  
		MISSION_STAT_ADD_WATCH(TON5_TIME)  
		MISSION_STAT_ADD_WATCH(TON5_UNHOOK)  
		MISSION_STAT_ADD_WATCH(TON5_QUICK_UNHOOK)  
		EXIT
	CASE RC_HAO_1  
		MISSION_STAT_ADD_WATCH(HAO1_FASTEST_LAP)  
		MISSION_STAT_ADD_WATCH(HAO1_RACE_TIME)  
		MISSION_STAT_ADD_WATCH(HAO1_COLLISIONS)  
		EXIT
ENDSWITCH
	SCRIPT_ASSERT("INTERNAL_FLOW_PRIME_STATS_FOR_RC_MISSION : RC Mission has no stats registered, please enter a bug to Default Levels.")
ENDPROC
PROC INTERNAL_GENERATED_MISSION_STAT_CONFIGURE_TYPES_AND_VALUES_FOR_INDEX(ENUM_MISSION_STATS index, MissionStatInfo &tar)
	SWITCH(index)
	CASE ARM1_NOSCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 100
		tar.less_than_threshold = True
		tar.statname = MS_ARM1_NOSCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 390000
		tar.less_than_threshold = True
		tar.statname = MS_ARM2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_PETROL_FIRE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ARM2_PETROL_FIRE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 2
	EXIT
	CASE ARM3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 300000
		tar.less_than_threshold = True
		tar.statname = MS_ARM3_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_ARM3_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA0_NOSCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 100
		tar.less_than_threshold = True
		tar.statname = MS_FRA0_NOSCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA0_DOGCAM
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 10000
		tar.less_than_threshold = False
		tar.statname = MS_FRA0_DOGCAM
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_QUICK_CATCH
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 10000
		tar.less_than_threshold = True
		tar.statname = MS_FAM1_QUICK_CATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_NOSCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FAM1_NOSCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_FASTEST_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 27
		tar.less_than_threshold = False
		tar.statname = MS_FAM2_FASTEST_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_FELL_OFF_BIKE
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FAM2_FELL_OFF_BIKE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_FAST_SWIM
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 60000
		tar.less_than_threshold = True
		tar.statname = MS_FAM2_FAST_SWIM
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 330000
		tar.less_than_threshold = True
		tar.statname = MS_FAM3_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_NOSCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 100
		tar.less_than_threshold = True
		tar.statname = MS_FAM3_NOSCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 510000
		tar.less_than_threshold = True
		tar.statname = MS_LES1A_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_CLEAR_POPUPS
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 32000
		tar.less_than_threshold = True
		tar.statname = MS_LES1A_CLEAR_POPUPS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = False
		tar.statname = MS_LAM1_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 10
		tar.less_than_threshold = False
		tar.statname = MS_LAM1_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 630000
		tar.less_than_threshold = True
		tar.statname = MS_LAM1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_UNMARKED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = True
		tar.statname = MS_LAM1_UNMARKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_NO_SURVIVORS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TRV1_NO_SURVIVORS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 12
		tar.less_than_threshold = False
		tar.statname = MS_TRV1_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 720000
		tar.less_than_threshold = True
		tar.statname = MS_TRV1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_TRAILER_DAMAGE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 5000
		tar.less_than_threshold = False
		tar.statname = MS_TRV1_TRAILER_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_BIKERS_KILLED_BEFORE_LOCATION
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TRV1_BIKERS_KILLED_BEFORE_LO
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 5
		tar.less_than_threshold = False
		tar.statname = MS_TRV2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 750000
		tar.less_than_threshold = True
		tar.statname = MS_TRV2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_RACEBACK_WON
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TRV2_RACEBACK_WON
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_UNDERBRIDGES
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 6
		tar.less_than_threshold = False
		tar.statname = MS_TRV2_UNDERBRIDGES
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 5
		tar.less_than_threshold = False
		tar.statname = MS_TRV3_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE TRV3_NOT_DETECTED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TRV3_NOT_DETECTED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_UNMARKED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = True
		tar.statname = MS_TRV3_UNMARKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_ALL_TRAILERS_AT_ONCE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TRV3_ALL_TRAILERS_AT_ONCE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_UNMARKED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = True
		tar.statname = MS_CHI1_UNMARKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_BODY_COUNT
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 32
		tar.less_than_threshold = False
		tar.statname = MS_CHI1_BODY_COUNT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 10
		tar.less_than_threshold = False
		tar.statname = MS_CHI2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_UNMARKED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = True
		tar.statname = MS_CHI2_UNMARKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_ONE_SHOT_TWO_KILLS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CHI2_ONE_SHOT_TWO_KILLS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_LORRY_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 34
		tar.less_than_threshold = False
		tar.statname = MS_FAM4_LORRY_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_GOT_TOO_FAR_FROM_LAZ
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FAM4_GOT_TOO_FAR_FROM_LAZ
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_FBI1_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 14
		tar.less_than_threshold = False
		tar.statname = MS_FBI1_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 570000
		tar.less_than_threshold = True
		tar.statname = MS_FBI1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_SPECKILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.statname = MS_FBI1_SPECKILLS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_UNMARKED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = True
		tar.statname = MS_FBI1_UNMARKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 450000
		tar.less_than_threshold = True
		tar.statname = MS_FBI2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = False
		tar.statname = MS_FBI2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 10
		tar.less_than_threshold = False
		tar.statname = MS_FBI2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 420000
		tar.less_than_threshold = True
		tar.statname = MS_FRA1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 900000
		tar.less_than_threshold = True
		tar.statname = MS_FAM5_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_WARRIOR_POSE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FAM5_WARRIOR_POSE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_TRIANGLE_POSE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FAM5_TRIANGLE_POSE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_SUN_SALUTATION_POSE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FAM5_SUN_SALUTATION_POSE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_HEART_STOPPED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FBI3_HEART_STOPPED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_ELECTROCUTION
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI3_ELECTROCUTION
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_TOOTH
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI3_TOOTH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_WRENCH
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI3_WRENCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_WATERBOARD
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI3_WATERBOARD
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_UNMARKED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 660000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_HELISHOT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI4_HELISHOT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE CS2_NO_SCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_CS2_NO_SCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_SPECIAL_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS1_SPECIAL_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 720000
		tar.less_than_threshold = True
		tar.statname = MS_CS1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_DROVE_BETWEEN_TRUCKS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS1_DROVE_BETWEEN_TRUCKS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_DROVE_BETWEEN_BUSES
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS1_DROVE_BETWEEN_BUSES
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_DROVE_THROUGH_TUNNEL
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS1_DROVE_THROUGH_TUNNEL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 585000
		tar.less_than_threshold = True
		tar.statname = MS_MAR1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_FASTEST_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 42
		tar.less_than_threshold = False
		tar.statname = MS_MAR1_FASTEST_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_FELL_OFF_THE_BIKE
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_MAR1_FELL_OFF_THE_BIKE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_ONE_SHOT
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = True
		tar.statname = MS_MAR1_ONE_SHOT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_NO_SCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 200
		tar.less_than_threshold = True
		tar.statname = MS_CS3_NO_SCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_FASTEST_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 52
		tar.less_than_threshold = False
		tar.statname = MS_CS3_FASTEST_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_ACTOR_KNOCKOUT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS3_ACTOR_KNOCKOUT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_EJECTOR_SEAT_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS3_EJECTOR_SEAT_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_RAN_OVER_ACTOR_AGAIN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS3_RAN_OVER_ACTOR_AGAIN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 80
		tar.less_than_threshold = False
		tar.statname = MS_EXL1_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_EXL2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 540000
		tar.less_than_threshold = True
		tar.statname = MS_EXL2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_EXL2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_ANIMAL_KILLED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_EXL2_ANIMAL_KILLED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 690000
		tar.less_than_threshold = True
		tar.statname = MS_EXL3_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_FASTEST_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 40
		tar.less_than_threshold = False
		tar.statname = MS_EXL3_FASTEST_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_JUMPED_AT_FIRST_OPPERTUNITY
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXL3_JUMPED_AT_FIRST_OPPERTU
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_STUNS
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 8
		tar.less_than_threshold = False
		tar.statname = MS_FBI5_STUNS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 720000
		tar.less_than_threshold = True
		tar.statname = MS_CS4_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_NO_SCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 100
		tar.less_than_threshold = True
		tar.statname = MS_CS4_NO_SCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 180000
		tar.less_than_threshold = True
		tar.statname = MS_SOL2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 660000
		tar.less_than_threshold = True
		tar.statname = MS_MIC1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 20
		tar.less_than_threshold = False
		tar.statname = MS_MIC1_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_TIMES_SWITCHED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = True
		tar.statname = MS_MIC2_TIMES_SWITCHED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_MIC2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE MIC2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 10
		tar.less_than_threshold = False
		tar.statname = MS_MIC2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_MIKE_RESCUE_TIMER
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 210000
		tar.less_than_threshold = True
		tar.statname = MS_MIC2_MIKE_RESCUE_TIMER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_WAYPOINT_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_MIC2_WAYPOINT_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 630000
		tar.less_than_threshold = True
		tar.statname = MS_FAM6_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 330000
		tar.less_than_threshold = True
		tar.statname = MS_SOL3_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1290000
		tar.less_than_threshold = True
		tar.statname = MS_FIN_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_EAGLE_EYE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_AH1_EAGLE_EYE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_MISSED_A_SPOT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_AH1_MISSED_A_SPOT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_CLEANED_OUT
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 540000
		tar.less_than_threshold = True
		tar.statname = MS_AH1_CLEANED_OUT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH2_QUICK_GETAWAY
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 45000
		tar.less_than_threshold = True
		tar.statname = MS_AH2_QUICK_GETAWAY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_LEISURELY_DRIVE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 210000
		tar.less_than_threshold = True
		tar.statname = MS_RH1_LEISURELY_DRIVE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1200000
		tar.less_than_threshold = True
		tar.statname = MS_DH1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_EMPLOYEE_OF_THE_MONTH
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_DH1_EMPLOYEE_OF_THE_MONTH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_PERFECT_SURVEILLANCE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_DH1_PERFECT_SURVEILLANCE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_HONEST_DAYS_WORK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_DH1_HONEST_DAYS_WORK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA1_KILLCHAIN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_BA1_KILLCHAIN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA1_DAMAGE_TAKEN
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = True
		tar.statname = MS_BA1_DAMAGE_TAKEN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA2_VANS_DESTROYED
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.statname = MS_BA2_VANS_DESTROYED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA3A_DELIVERY_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 165000
		tar.less_than_threshold = True
		tar.statname = MS_BA3A_DELIVERY_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA3C_DELIVERY_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 90000
		tar.less_than_threshold = True
		tar.statname = MS_BA3C_DELIVERY_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA3C_STASH_UNHOOKED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_BA3C_STASH_UNHOOKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DR1_DREYFUSS_KILLED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_DR1_DREYFUSS_KILLED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EP4_ARTIFACT_DETECTOR_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EP4_ARTIFACT_DETECTOR_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EP6_PERFECT_LANDING
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EP6_PERFECT_LANDING
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EP8_MONEY_STOLEN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EP8_MONEY_STOLEN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EP8_SECURITY_WIPED_OUT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EP8_SECURITY_WIPED_OUT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT1_RACE_WON
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXT1_RACE_WON
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT1_BIG_AIR
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXT1_BIG_AIR
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT1_FALL_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 7000
		tar.less_than_threshold = False
		tar.statname = MS_EXT1_FALL_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT2_NUMBER_OF_SPINS
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 8
		tar.less_than_threshold = False
		tar.statname = MS_EXT2_NUMBER_OF_SPINS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE EXT2_LEAP_FROM_ATV_TO_WATER
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXT2_LEAP_FROM_ATV_TO_WATER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT3_PERFECT_LANDING
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXT3_PERFECT_LANDING
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT3_DARE_DEVIL
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 8000
		tar.less_than_threshold = False
		tar.statname = MS_EXT3_DARE_DEVIL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXT4_FALL_SURVIVED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXT4_FALL_SURVIVED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FA1_SHORTCUT_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FA1_SHORTCUT_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FA3_SHORTCUT_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FA3_SHORTCUT_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HU1_HIT_EACH_SATELLITE_FIRST_GO
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_HU1_HIT_EACH_SATELLITE_FIRST
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HU1_TYRE_SHOOTING_ACCURACY
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 75
		tar.less_than_threshold = False
		tar.statname = MS_HU1_TYRE_SHOOTING_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HU1_TWO_COYOTES_KILLED_IN_ONE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_HU1_TWO_COYOTES_KILLED_IN_ON
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HU2_ELK_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_HU2_ELK_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HU2_DETECTED_BY_ELK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_HU2_DETECTED_BY_ELK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JO2_STOPPED_IN_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 40000
		tar.less_than_threshold = True
		tar.statname = MS_JO2_STOPPED_IN_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JO2_JOSH_MELEED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JO2_JOSH_MELEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JO3_POUR_FUEL_IN_ONE_GO
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JO3_POUR_FUEL_IN_ONE_GO
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JO3_ESCAPE_WITHOUT_ALERTING_COPS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JO3_ESCAPE_WITHOUT_ALERTING_
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JO4_ESCAPE_IN_PARKED_COP_CAR
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JO4_ESCAPE_IN_PARKED_COP_CAR
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JO4_JOSH_KILLED_BEFORE_ESCAPE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JO4_JOSH_KILLED_BEFORE_ESCAP
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN1_FAST_STOP
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 40000
		tar.less_than_threshold = True
		tar.statname = MS_MIN1_FAST_STOP
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN1_VEHICLE_TAKEN_AFTER_STUNS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_MIN1_VEHICLE_TAKEN_AFTER_STU
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN2_STUNNED_ALL
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_MIN2_STUNNED_ALL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN2_FIRST_CAPTURE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 30000
		tar.less_than_threshold = True
		tar.statname = MS_MIN2_FIRST_CAPTURE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN2_SECOND_CAPTURE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 55000
		tar.less_than_threshold = True
		tar.statname = MS_MIN2_SECOND_CAPTURE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN3_KILL_BEFORE_ESCAPE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_MIN3_KILL_BEFORE_ESCAPE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIN3_STUNNED_AND_KILLED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_MIN3_STUNNED_AND_KILLED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1A_ENTOURAGE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI1A_ENTOURAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1A_PLAYER_DAMAGE_DURING_BRAWL
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_NI1A_PLAYER_DAMAGE_DURING_BR
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1B_CLOTHES_TAKEN_NO_ALERTS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI1B_CLOTHES_TAKEN_NO_ALERTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1B_GARDENER_TAKEN_OUT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI1B_GARDENER_TAKEN_OUT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_NI1C_PLAYER_GOT_TOO_FAR_FROM
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1D_KILLED_STANKY_AND_GUARDS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI1D_KILLED_STANKY_AND_GUARD
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI1D_HEADSHOTTED_STANKY
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI1D_HEADSHOTTED_STANKY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE NI1D_GOLF_CLUB_STOLEN_IN_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 30000
		tar.less_than_threshold = True
		tar.statname = MS_NI1D_GOLF_CLUB_STOLEN_IN_TIM
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_NI2_GOT_TOO_FAR_AWAY_FROM_NA
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_NI2_HARM_IN_HOSPITAL_DRIVE_T
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI2_VEHICLE_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 40
		tar.less_than_threshold = True
		tar.statname = MS_NI2_VEHICLE_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI3_BAILED_AT_LAST_MOMENT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI3_BAILED_AT_LAST_MOMENT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE NI3_REVERSED_TO_KILL
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_NI3_REVERSED_TO_KILL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP1_TAKEN_OUT_IN_ONE_SWING
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_PAP1_TAKEN_OUT_IN_ONE_SWING
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP1_PICTURES_SNAPPED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_PAP1_PICTURES_SNAPPED
		tar.MinRange = 0
		tar.MaxRange = 3
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP2_POOL_JUMP
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_PAP2_POOL_JUMP
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP2_FACE_RECOG_PERCENT
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 90
		tar.less_than_threshold = False
		tar.statname = MS_PAP2_FACE_RECOG_PERCENT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP3A_FAR_FROM_POPPY
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_PAP3A_FAR_FROM_POPPY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP3A_PHOTO_IN_CUFFS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_PAP3A_PHOTO_IN_CUFFS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP3B_PHOTO_SPOTTED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_PAP3B_PHOTO_SPOTTED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP3B_PHOTO_OF_USE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_PAP3B_PHOTO_OF_USE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PAP4_ENTIRE_CREW_KILLED_IN_ONE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_PAP4_ENTIRE_CREW_KILLED_IN_O
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TLO_WOUNDS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_TLO_WOUNDS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TLO_AMBIENT_ANIMAL_KILLS
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.statname = MS_TLO_AMBIENT_ANIMAL_KILLS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TLO_TO_SITE_ON_FOOT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TLO_TO_SITE_ON_FOOT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 420000
		tar.less_than_threshold = True
		tar.statname = MS_MIC3_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 390000
		tar.less_than_threshold = True
		tar.statname = MS_MIC4_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM1_CAR_CHOSEN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 0
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE ARM1_LOSE_WANTED_LVL
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 3600000
		tar.lb_precedence = 0
	EXIT
	CASE ARM1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE ARM1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 7000
		tar.less_than_threshold = False
		tar.statname = MS_ARM1_SPECIAL_ABILITY_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE ARM2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_PLAYER_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 6
		tar.less_than_threshold = False
		tar.statname = MS_ARM2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE ARM2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_QUICK_BOARDING
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA0_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA0_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 7000
		tar.less_than_threshold = False
		tar.statname = MS_FRA0_SPECIAL_ABILITY_TIME
		tar.MinRange = 0
		tar.MaxRange = 30000
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 3600000
		tar.lb_precedence = 0
	EXIT
	CASE FRA0_DOGGY_STYLE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 3600000
		tar.lb_precedence = 0
	EXIT
	CASE FRA0_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA0_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_MAX_CAR_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 5
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 999
		tar.lb_precedence = 0
	EXIT
	CASE FAM2_CYCLING_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 3600000
		tar.lb_precedence = 0
	EXIT
	CASE FAM2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE ARM1_WINNER
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ARM1_WINNER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 0
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE ARM1_HIT_ALIENS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_ARM1_HIT_ALIENS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 0
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE ARM2_CRASH_BIKER
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 2
	EXIT
	CASE ARM2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_ARM2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 2
	EXIT
	CASE ARM3_ACTION_CAM_TIME
		tar.type = MISSION_STAT_TYPE_ACTION_CAM_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_COUNTERS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_LOSE_JETSKI
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 40000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_CHOICES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_GOON_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FAM3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_FLYING_HIGH
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 5
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_FREEFALL_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_HARRIER_CAM_TIME
		tar.type = MISSION_STAT_TYPE_ACTION_CAM_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_BAIL_IN_CAR
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EXL1_BAIL_IN_CAR
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_TIME_AS_CHOP
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_DEATH_FROM_ABOVE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 4000
		tar.less_than_threshold = False
		tar.statname = MS_RH2_BULLETS_FIRED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_ACTUAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_PERSONAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 50
		tar.less_than_threshold = False
		tar.statname = MS_RH2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE EXL3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_TRAIN_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_MICHAEL_SAVE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_GETAWAY_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_STAY_ON_ROAD
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_MOUSE_CLICKS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_POPUPS_CLOSED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1B_TIME_WATCHING
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE LAM1_COP_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LAM1_BODY_ARMOR
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_PHOTOS_TAKEN
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_ACTUAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_PERSONAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_RACE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_ATV_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_ATV_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_PLANE_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_PLANE_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_SHOTS_TO_KILL_HELI
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_STEALTH_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE TRV3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_STICKY_BOMBS_USED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 34
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_VEHICLES_DESTROYED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 6
		tar.less_than_threshold = False
		tar.statname = MS_CHI1_VEHICLES_DESTROYED
		tar.MinRange = 0
		tar.MaxRange = 6
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_GRENADE_LAUNCHER_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_GAS_POUR_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_GAS_USED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 80
		tar.less_than_threshold = False
		tar.statname = MS_CHI2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_LAZLOW_CAM_USE
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_COP_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FBI2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_RAPPEL_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_FORKLIFT_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_CRANE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_PHOTOS_TAKEN
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_FRANKLIN_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_TREVOR_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_FRA1_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 12
		tar.less_than_threshold = False
		tar.statname = MS_FRA1_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_COP_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_HOME_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FBI3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_ASSASSIN_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_TIME_TO_LOSE_TRIADS
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_TIME_TO_SAVE_FRANKLIN
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_UNIQUE_TRIAD_DEATHS
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_PIERCING_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_TATTOO_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_FRONT_OR_BACK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 12
		tar.less_than_threshold = False
		tar.statname = MS_DH2A_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 80
		tar.less_than_threshold = False
		tar.statname = MS_DH2A_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_KILLS
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_ACTUAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_PERSONAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_STEALTH_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 12
		tar.less_than_threshold = False
		tar.statname = MS_DH2A_STEALTH_KILLS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE DH2A_TIME_TO_FIND_CONTAINER
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 60000
		tar.less_than_threshold = True
		tar.statname = MS_DH2A_TIME_TO_FIND_CONTAINER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_ACTUAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_PERSONAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_SUB_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_HELI_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_TIME_TO_FIND_CONTAINER
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 120000
		tar.less_than_threshold = True
		tar.statname = MS_DH2B_TIME_TO_FIND_CONTAINER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 12
		tar.less_than_threshold = False
		tar.statname = MS_FBI4_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = False
		tar.statname = MS_FBI4_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 10
		tar.less_than_threshold = False
		tar.statname = MS_FBI4_SWITCHES
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_STICKY_BOMB_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_VEHICLES_DESTROYED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MAR1_PLANE_HIT_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 52
		tar.less_than_threshold = False
		tar.statname = MS_SOL3_MAX_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_COP_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 120000
		tar.less_than_threshold = True
		tar.statname = MS_SOL3_COP_LOSS_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_INNOCENT_KILLS
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_NEWS_HELI_CAM_TIME
		tar.type = MISSION_STAT_TYPE_ACTION_CAM_USE
		tar.currentvalue = 0
		tar.success_threshold = 15000
		tar.less_than_threshold = False
		tar.statname = MS_SOL3_NEWS_HELI_CAM_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE CS2_TARGETS_SCANNED
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS2_CHAD_FOCUS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_WINNER
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_RH1_WINNER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH2_METHOD
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_AH3B_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 20
		tar.less_than_threshold = False
		tar.statname = MS_AH3B_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE AH3B_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_AH3B_INNOCENTS_KILLED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_FREEFALL_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_LANDING_ACCURACY
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_AH3B_LANDING_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_HACKING_ERRORS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_HACKING_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 45000
		tar.less_than_threshold = True
		tar.statname = MS_AH3B_HACKING_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 18
		tar.less_than_threshold = False
		tar.statname = MS_MIC3_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_VEHICLES_DESTROYED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 48
		tar.less_than_threshold = False
		tar.statname = MS_MIC4_MAX_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 12
		tar.less_than_threshold = False
		tar.statname = MS_MIC4_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 12
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_HOME_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_HEADSHOT_RESCUE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_MIC4_HEADSHOT_RESCUE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC4_VEHICLE_CHOSEN
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FRA2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 18
		tar.less_than_threshold = False
		tar.statname = MS_FRA2_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_FRA2_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_FRA2_SWITCHES
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_VEHICLE_CHOSEN
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_ACTUAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_CREW_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 20
		tar.less_than_threshold = False
		tar.statname = MS_FH2A_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_TRAFFIC_LIGHT_CHANGES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 10
		tar.less_than_threshold = True
		tar.statname = MS_FH2A_TRAFFIC_LIGHT_CHANGES
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_PERSONAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_ACTUAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_CREW_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 20
		tar.less_than_threshold = False
		tar.statname = MS_FH2B_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 65
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_PERSONAL_TAKE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FH2B_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 20
		tar.less_than_threshold = False
		tar.statname = MS_FIN_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_FIN_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_STICKY_BOMBS_USED
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_SNIPER_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_CHENG_BOMBER
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FIN_CHENG_BOMBER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_HAINES_HEADSHOT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FIN_HAINES_HEADSHOT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FIN_STRETCH_STRONGARM
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FIN_STRETCH_STRONGARM
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FA2_QUICK_WIN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FA2_QUICK_WIN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FA2_BUMPED_INTO_MARY_ANN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FA2_BUMPED_INTO_MARY_ANN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 15
		tar.less_than_threshold = False
		tar.statname = MS_FBI5_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 70
		tar.less_than_threshold = False
		tar.statname = MS_FBI5_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_BAR_CUT_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FH1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_HELI_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_MAX_HELI_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_SWITCHES
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_PERFECT_DISTANCE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FH1_PERFECT_DISTANCE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_FIND_HOLE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 20000
		tar.less_than_threshold = True
		tar.statname = MS_FH1_FIND_HOLE_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_UNDER_BRIDGE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FH1_UNDER_BRIDGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_THROUGH_TUNNEL
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FH1_THROUGH_TUNNEL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 80
		tar.less_than_threshold = False
		tar.statname = MS_MIC1_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_LANDING_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_PLANE_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC1_STARTING_CHAR
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1P_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1P_VAN_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1P_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1P_FIRETRUCK_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1P_FACTORY_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1P_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1P_SUB_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1P_SUB_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1P_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1P_TRUCK_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE DH1P_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_HELI_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_MAX_HELI_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_HELI_TAKEDOWN_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2AP_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2AP_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2AP_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2AP_FACTORY_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 5
		tar.less_than_threshold = False
		tar.statname = MS_RH1P_HEADSHOTS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_VAN_TO_LAB_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_TRUCK_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EP6_UNDER_BRIDGE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_EP6_UNDER_BRIDGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE CS2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 240000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CHI1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 270000
		tar.less_than_threshold = True
		tar.statname = MS_CHI1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE CHI2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 900000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 810000
		tar.less_than_threshold = True
		tar.statname = MS_FBI5_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA0_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 810000
		tar.less_than_threshold = True
		tar.statname = MS_FRA2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1080000
		tar.less_than_threshold = True
		tar.statname = MS_AH3A_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3B_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1140000
		tar.less_than_threshold = True
		tar.statname = MS_AH3B_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 870000
		tar.less_than_threshold = True
		tar.statname = MS_DH2B_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 660000
		tar.less_than_threshold = True
		tar.statname = MS_FH1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 600000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 960000
		tar.less_than_threshold = True
		tar.statname = MS_FH2B_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 480000
		tar.less_than_threshold = True
		tar.statname = MS_JH1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 660000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 960000
		tar.less_than_threshold = True
		tar.statname = MS_RH2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 960000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPA_DAMAGE_TO_WAGON
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FHPA_DAMAGE_TO_WAGON
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPA_ESCAPE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 120000
		tar.less_than_threshold = True
		tar.statname = MS_FHPA_ESCAPE_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P5_UNITED_COLOURS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI4P5_UNITED_COLOURS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P5_QUICK_SHOPPER
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 30000
		tar.less_than_threshold = True
		tar.statname = MS_FBI4P5_QUICK_SHOPPER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P4_CLICHE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FBI4P4_CLICHE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P4_FACE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 20000
		tar.less_than_threshold = True
		tar.statname = MS_FBI4P4_FACE_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P1_LEVEL_LOSS
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 33
		tar.less_than_threshold = False
		tar.statname = MS_FBI4P1_MAX_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FBI4P1_VEHICLE_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FBI4P1_VEHICLE_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P1_TIME_TAKEN
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 300000
		tar.less_than_threshold = True
		tar.statname = MS_FBI4P1_TIME_TAKEN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P2_TIME_TAKEN
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 180000
		tar.less_than_threshold = True
		tar.statname = MS_FBI4P2_TIME_TAKEN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P2_VEHICLE_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FBI4P2_VEHICLE_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 33
		tar.less_than_threshold = False
		tar.statname = MS_FBI4P2_MAX_SPEED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE PRO_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_COP_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 3600000
		tar.lb_precedence = 0
	EXIT
	CASE JH2A_CASES_SMASHED
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 20
		tar.less_than_threshold = False
		tar.statname = MS_JH2A_CASES_SMASHED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_FRANKLIN_BIKE_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 500
		tar.less_than_threshold = True
		tar.statname = MS_JH2A_FRANKLIN_BIKE_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_CASE_GRAB_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 50000
		tar.less_than_threshold = True
		tar.statname = MS_JH2A_CASE_GRAB_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA3A_AVOID_STAKEOUT
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_BA3A_AVOID_STAKEOUT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV2_ALL_BIKERS_KILLED_ON_WING
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_TRV2_ALL_BIKERS_KILLED_ON_WI
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE BA2_DANCING_CLOWNS_KILLED
		tar.type = MISSION_STAT_TYPE_FRACTION
		tar.currentvalue = 0
		tar.success_threshold = 6
		tar.less_than_threshold = False
		tar.statname = MS_BA2_DANCING_CLOWNS_KILLED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON5_QUICK_UNHOOK
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON5_UNHOOK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_TON5_UNHOOK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON4_QUICK_UNHOOK
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON5_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 300000
		tar.less_than_threshold = True
		tar.statname = MS_TON5_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON4_UNHOOK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_TON4_UNHOOK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 300000
		tar.less_than_threshold = True
		tar.statname = MS_TON1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON1_UNHOOK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_TON1_UNHOOK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON1_QUICK_UNHOOK
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 330000
		tar.less_than_threshold = True
		tar.statname = MS_TON2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON2_UNHOOK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_TON2_UNHOOK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON2_QUICK_UNHOOK
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 420000
		tar.less_than_threshold = True
		tar.statname = MS_TON3_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON3_UNHOOK
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_TON3_UNHOOK
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON3_QUICK_UNHOOK
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TON4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 360000
		tar.less_than_threshold = True
		tar.statname = MS_TON4_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_COLLATERAL_DAMAGE
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1000000
		tar.less_than_threshold = False
		tar.statname = MS_RH2_COLLATERAL_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2A_NO_ALARMS
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_DH2A_NO_ALARMS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE DH2A_TIME_TO_CLEAR
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_ABSEIL_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 30000
		tar.less_than_threshold = True
		tar.statname = MS_AH3A_ABSEIL_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_FLOOR_MOP_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 180000
		tar.less_than_threshold = True
		tar.statname = MS_AH3A_FLOOR_MOP_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH3A_OXYGEN_REMAINING
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 40
		tar.less_than_threshold = False
		tar.statname = MS_AH3A_OXYGEN_REMAINING
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2A_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = False
		tar.statname = MS_FH2A_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 60
		tar.less_than_threshold = False
		tar.statname = MS_FH2B_ACCURACY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_CHASE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 70000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH2B_GOLD_DROP_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 30000
		tar.less_than_threshold = True
		tar.statname = MS_FH2B_GOLD_DROP_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HAO1_RACE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 170000
		tar.less_than_threshold = True
		tar.statname = MS_HAO1_RACE_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HAO1_COLLISIONS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 5
		tar.less_than_threshold = True
		tar.statname = MS_HAO1_COLLISIONS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE HAO1_FASTEST_LAP
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 80000
		tar.less_than_threshold = True
		tar.statname = MS_HAO1_FASTEST_LAP
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_COORD_KO
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FAM4_COORD_KO
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM3_GARDEN_KO
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ARM3_GARDEN_KO
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS2_SCANMAN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_CS2_SCANMAN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS2_EAVESDROPPER
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_CS2_EAVESDROPPER
		tar.MinRange = 0
		tar.MaxRange = 3
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS4_SHREDS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_CS4_SHREDS
		tar.MinRange = 0
		tar.MaxRange = 3
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_VEHICLE_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_FAM3_VEHICLE_KILLS
		tar.MinRange = 0
		tar.MaxRange = 3
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_KILLMIC
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FINB_KILLMIC
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINA_KILLTREV
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FINA_KILLTREV
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE MIC3_HELIKILL
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_MIC3_HELIKILL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_PERFECT_LANDING
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_SOL1_PERFECT_LANDING
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_BRAWL_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_SOL1_BRAWL_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 600000
		tar.less_than_threshold = True
		tar.statname = MS_SOL1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_SILENT_TAKEDOWNS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 3
		tar.less_than_threshold = False
		tar.statname = MS_SOL1_SILENT_TAKEDOWNS
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 240000
		tar.less_than_threshold = True
		tar.statname = MS_TRV4_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AHP1_NOSCRATCH
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_AHP1_NOSCRATCH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AHP1_TRUCKCALLED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_AHP1_TRUCKCALLED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DHP1_NO_BOARDING
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_DHP1_NO_BOARDING
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DHP1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 510000
		tar.less_than_threshold = True
		tar.statname = MS_DHP1_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DHP2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 330000
		tar.less_than_threshold = True
		tar.statname = MS_DHP2_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPD_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 270000
		tar.less_than_threshold = True
		tar.statname = MS_FINPD_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FINPD_UNDETECTED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FINPD_UNDETECTED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_MAPPED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FINPC2_MAPPED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_MAPPED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FINPC3_MAPPED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_SNEAK_THIEF
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FHPB_SNEAK_THIEF
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC1_MAPPED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_FINPC1_MAPPED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1B_SWIFT_GETAWAY
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 120000
		tar.less_than_threshold = True
		tar.statname = MS_JHP1B_SWIFT_GETAWAY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP2A_LOOSE_CARGO
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JHP2A_LOOSE_CARGO
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1A_SNEAKY_PEST
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JHP1A_SNEAKY_PEST
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_CONVOY_STOPPED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_RH1P_CONVOY_STOPPED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_TRUCK_UNHOOKED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FAM4_TRUCK_UNHOOKED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 300000
		tar.less_than_threshold = True
		tar.statname = MS_FHPB_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ARM1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 0
	EXIT
	CASE JHP1B_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1B_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1B_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1B_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1B_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINA_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINA_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINA_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINA_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_FOOTCHASE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_CHOICE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 1
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV4_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV4_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV4_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FINPC1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FINPC1_CAR_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC1_TIME_TO_GARAGE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FINPC2_CAR_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_TIME_TO_GARAGE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.statname = MS_FINPC3_CAR_DAMAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_TIME_TO_GARAGE
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_NOT_SPOTTED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE ASS3_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_BUS_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_HIT_N_RUN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_WANTED_LOSS_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_ESCAPE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_PERFECT_PIC
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_JH1_PERFECT_PIC
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPD_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPD_STEALTH_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPD_MAX_HELI_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPD_HELI_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE JH2A_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH2A_INNOCENTS_KILLED
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP2A_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 660000
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC1_MOD_GAUNTLET
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 17000
		tar.less_than_threshold = False
		tar.statname = MS_FINPC1_MOD_GAUNTLET
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 0
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_MOD_GAUNTLET
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 17000
		tar.less_than_threshold = False
		tar.statname = MS_FINPC2_MOD_GAUNTLET
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 0
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_MOD_GAUNTLET
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 17000
		tar.less_than_threshold = False
		tar.statname = MS_FINPC3_MOD_GAUNTLET
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 0
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPA_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P5_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P4_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1P_TIME
		tar.type = MISSION_STAT_TYPE_TOTALTIME
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE CS1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE EXL3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM5_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FAM6_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = False
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FBI3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI5_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FRA1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 3600000
		tar.lb_precedence = 0
	EXIT
	CASE LAM1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE LES1A_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 4
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE AH1P_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH1P_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2BP_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FH1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPA_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FHPB_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JH1P_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP2A_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH1P_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE RH2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FBI4P5_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINA_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINB_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE TRV4_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE FINPC1_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC2_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPC3_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE FINPD_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE JHP1B_SPECIAL_ABILITY_TIME
		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -3
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_ACCURACY
		tar.type = MISSION_STAT_TYPE_ACCURACY
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 250
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_ESCAPE_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 240000
		tar.less_than_threshold = True
		tar.statname = MS_DH2B_ESCAPE_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_KILLED_ALL_MERRYWEATHER
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_DH2B_KILLED_ALL_MERRYWEATHER
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_KILLS
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 2500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE DH2B_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = True
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_MIRROR_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS1_MIRROR_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_MIRROR_PERCENT
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS1_MIRROR_PERCENT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_MIRROR_SNIPER_USED
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS1_MIRROR_SNIPER_USED
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_MIRROR_CASH
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS1_MIRROR_CASH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS1_MIRROR_MEDAL
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS1_MIRROR_MEDAL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_MIRROR_CASH
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS2_MIRROR_CASH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_MIRROR_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS2_MIRROR_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_MIRROR_QUICKBOOL
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS2_MIRROR_QUICKBOOL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_MIRROR_PERCENT
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS2_MIRROR_PERCENT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS2_MIRROR_MEDAL
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS2_MIRROR_MEDAL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_MIRROR_MEDAL
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS3_MIRROR_MEDAL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_MIRROR_PERCENTAGE
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS3_MIRROR_PERCENTAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_MIRROR_CLEAN_ESCAPE
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS3_MIRROR_CLEAN_ESCAPE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_MIRROR_CASH
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS3_MIRROR_CASH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS3_MIRROR_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS3_MIRROR_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_MIRROR_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS4_MIRROR_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_MIRROR_HIT_AND_RUN
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS4_MIRROR_HIT_AND_RUN
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_MIRROR_PERCENTAGE
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS4_MIRROR_PERCENTAGE
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_MIRROR_CASH
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS4_MIRROR_CASH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS4_MIRROR_MEDAL
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS4_MIRROR_MEDAL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
	SWITCH(index)
	CASE ASS5_MIRROR_TIME
		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS5_MIRROR_TIME
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_MIRROR_NO_FLY
		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS5_MIRROR_NO_FLY
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_MIRROR_PERCENT
		tar.type = MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS5_MIRROR_PERCENT
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_MIRROR_CASH
		tar.type = MISSION_STAT_TYPE_FINANCE_DIRECT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS5_MIRROR_CASH
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE ASS5_MIRROR_MEDAL
		tar.type = MISSION_STAT_TYPE_PURE_COUNT
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.statname = MS_ASS5_MIRROR_MEDAL
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = False
		tar.lb_differentiator = False
		tar.lb_weight_PPC = 1
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_INNOCENT_KILLS
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL1_HELI_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_MAX_SPEED
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 500
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_CAR_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_DAMAGE
		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -1000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_BULLETS_FIRED
		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -100
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_INNOCENT_KILLS
		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = -10000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	CASE SOL2_HEADSHOTS
		tar.type = MISSION_STAT_TYPE_HEADSHOTS
		tar.currentvalue = 0
		tar.success_threshold = 1
		tar.less_than_threshold = False
		tar.MinRange = 0
		tar.MaxRange = 1
		tar.bHidden = True
		tar.lb_differentiator = True
		tar.lb_weight_PPC = 5000
		tar.lb_min_legal = 0
		tar.lb_max_legal = 1
		tar.lb_precedence = 1
	EXIT
	ENDSWITCH
ENDPROC

FUNC INT GET_NUMBER_OF_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)
	
	SWITCH m
		CASE RC_BARRY_1
			RETURN 2
		CASE RC_BARRY_2
			RETURN 2
		CASE RC_BARRY_3A
			RETURN 2
		CASE RC_BARRY_3C
			RETURN 2
		CASE RC_DREYFUSS_1
			RETURN 1
		CASE RC_EPSILON_4
			RETURN 1
		CASE RC_EPSILON_6
			RETURN 2
		CASE RC_EPSILON_8
			RETURN 2
		CASE RC_EXTREME_1
			RETURN 3
		CASE RC_EXTREME_2
			RETURN 2
		CASE RC_EXTREME_3
			RETURN 2
		CASE RC_EXTREME_4
			RETURN 1
		CASE RC_FANATIC_1
			RETURN 1
		CASE RC_FANATIC_2
			RETURN 2
		CASE RC_FANATIC_3
			RETURN 1
		CASE RC_HUNTING_1
			RETURN 3
		CASE RC_HUNTING_2
			RETURN 2
		CASE RC_JOSH_2
			RETURN 2
		CASE RC_JOSH_3
			RETURN 2
		CASE RC_JOSH_4
			RETURN 2
		CASE RC_MINUTE_1
			RETURN 2
	ENDSWITCH
	SWITCH m
		CASE RC_MINUTE_2
			RETURN 3
		CASE RC_MINUTE_3
			RETURN 2
		CASE RC_NIGEL_1A
			RETURN 2
		CASE RC_NIGEL_1B
			RETURN 2
		CASE RC_NIGEL_1C
			RETURN 1
		CASE RC_NIGEL_1D
			RETURN 3
		CASE RC_NIGEL_2
			RETURN 3
		CASE RC_NIGEL_3
			RETURN 2
		CASE RC_PAPARAZZO_1
			RETURN 2
		CASE RC_PAPARAZZO_2
			RETURN 2
		CASE RC_PAPARAZZO_3A
			RETURN 2
		CASE RC_PAPARAZZO_3B
			RETURN 2
		CASE RC_PAPARAZZO_4
			RETURN 1
		CASE RC_THELASTONE
			RETURN 3
		CASE RC_TONYA_1
			RETURN 3
		CASE RC_TONYA_2
			RETURN 3
		CASE RC_TONYA_3
			RETURN 3
		CASE RC_TONYA_4
			RETURN 3
		CASE RC_TONYA_5
			RETURN 3
		CASE RC_HAO_1
			RETURN 3
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC ENUM_MISSION_STATS GET_NTH_STAT_FOR_RC_MISSION(g_eRC_MissionIDs m, INT i )
	
	SWITCH m
		CASE RC_BARRY_1
			SWITCH i
				CASE 0
				RETURN BA1_DAMAGE_TAKEN
				CASE 1
				RETURN BA1_KILLCHAIN
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_BARRY_2
			SWITCH i
				CASE 0
				RETURN BA2_VANS_DESTROYED
				CASE 1
				RETURN BA2_DANCING_CLOWNS_KILLED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_BARRY_3A
			SWITCH i
				CASE 0
				RETURN BA3A_DELIVERY_TIME
				CASE 1
				RETURN BA3A_AVOID_STAKEOUT
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_BARRY_3C
			SWITCH i
				CASE 0
				RETURN BA3C_STASH_UNHOOKED
				CASE 1
				RETURN BA3C_DELIVERY_TIME
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_DREYFUSS_1
			SWITCH i
				CASE 0
				RETURN DR1_DREYFUSS_KILLED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EPSILON_4
			SWITCH i
				CASE 0
				RETURN EP4_ARTIFACT_DETECTOR_USED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EPSILON_6
			SWITCH i
				CASE 0
				RETURN EP6_PERFECT_LANDING
				CASE 1
				RETURN EP6_UNDER_BRIDGE
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EPSILON_8
			SWITCH i
				CASE 0
				RETURN EP8_SECURITY_WIPED_OUT
				CASE 1
				RETURN EP8_MONEY_STOLEN
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EXTREME_1
			SWITCH i
				CASE 0
				RETURN EXT1_FALL_TIME
				CASE 1
				RETURN EXT1_BIG_AIR
				CASE 2
				RETURN EXT1_RACE_WON
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EXTREME_2
			SWITCH i
				CASE 0
				RETURN EXT2_LEAP_FROM_ATV_TO_WATER
				CASE 1
				RETURN EXT2_NUMBER_OF_SPINS
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EXTREME_3
			SWITCH i
				CASE 0
				RETURN EXT3_DARE_DEVIL
				CASE 1
				RETURN EXT3_PERFECT_LANDING
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_EXTREME_4
			SWITCH i
				CASE 0
				RETURN EXT4_FALL_SURVIVED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_FANATIC_1
			SWITCH i
				CASE 0
				RETURN FA1_SHORTCUT_USED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_FANATIC_2
			SWITCH i
				CASE 0
				RETURN FA2_BUMPED_INTO_MARY_ANN
				CASE 1
				RETURN FA2_QUICK_WIN
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_FANATIC_3
			SWITCH i
				CASE 0
				RETURN FA3_SHORTCUT_USED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_HUNTING_1
			SWITCH i
				CASE 0
				RETURN HU1_TWO_COYOTES_KILLED_IN_ONE
				CASE 1
				RETURN HU1_TYRE_SHOOTING_ACCURACY
				CASE 2
				RETURN HU1_HIT_EACH_SATELLITE_FIRST_GO
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_HUNTING_2
			SWITCH i
				CASE 0
				RETURN HU2_DETECTED_BY_ELK
				CASE 1
				RETURN HU2_ELK_HEADSHOTS
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_JOSH_2
			SWITCH i
				CASE 0
				RETURN JO2_JOSH_MELEED
				CASE 1
				RETURN JO2_STOPPED_IN_TIME
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_JOSH_3
			SWITCH i
				CASE 0
				RETURN JO3_ESCAPE_WITHOUT_ALERTING_COPS
				CASE 1
				RETURN JO3_POUR_FUEL_IN_ONE_GO
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_JOSH_4
			SWITCH i
				CASE 0
				RETURN JO4_JOSH_KILLED_BEFORE_ESCAPE
				CASE 1
				RETURN JO4_ESCAPE_IN_PARKED_COP_CAR
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_MINUTE_1
			SWITCH i
				CASE 0
				RETURN MIN1_VEHICLE_TAKEN_AFTER_STUNS
				CASE 1
				RETURN MIN1_FAST_STOP
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
	ENDSWITCH
	SWITCH m
		CASE RC_MINUTE_2
			SWITCH i
				CASE 0
				RETURN MIN2_FIRST_CAPTURE
				CASE 1
				RETURN MIN2_SECOND_CAPTURE
				CASE 2
				RETURN MIN2_STUNNED_ALL
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_MINUTE_3
			SWITCH i
				CASE 0
				RETURN MIN3_STUNNED_AND_KILLED
				CASE 1
				RETURN MIN3_KILL_BEFORE_ESCAPE
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_NIGEL_1A
			SWITCH i
				CASE 0
				RETURN NI1A_PLAYER_DAMAGE_DURING_BRAWL
				CASE 1
				RETURN NI1A_ENTOURAGE
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_NIGEL_1B
			SWITCH i
				CASE 0
				RETURN NI1B_GARDENER_TAKEN_OUT
				CASE 1
				RETURN NI1B_CLOTHES_TAKEN_NO_ALERTS
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_NIGEL_1C
			SWITCH i
				CASE 0
				RETURN NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_NIGEL_1D
			SWITCH i
				CASE 0
				RETURN NI1D_GOLF_CLUB_STOLEN_IN_TIME
				CASE 1
				RETURN NI1D_HEADSHOTTED_STANKY
				CASE 2
				RETURN NI1D_KILLED_STANKY_AND_GUARDS
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_NIGEL_2
			SWITCH i
				CASE 0
				RETURN NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI
				CASE 1
				RETURN NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH
				CASE 2
				RETURN NI2_VEHICLE_DAMAGE
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_NIGEL_3
			SWITCH i
				CASE 0
				RETURN NI3_BAILED_AT_LAST_MOMENT
				CASE 1
				RETURN NI3_REVERSED_TO_KILL
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_PAPARAZZO_1
			SWITCH i
				CASE 0
				RETURN PAP1_TAKEN_OUT_IN_ONE_SWING
				CASE 1
				RETURN PAP1_PICTURES_SNAPPED
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_PAPARAZZO_2
			SWITCH i
				CASE 0
				RETURN PAP2_POOL_JUMP
				CASE 1
				RETURN PAP2_FACE_RECOG_PERCENT
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_PAPARAZZO_3A
			SWITCH i
				CASE 0
				RETURN PAP3A_FAR_FROM_POPPY
				CASE 1
				RETURN PAP3A_PHOTO_IN_CUFFS
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_PAPARAZZO_3B
			SWITCH i
				CASE 0
				RETURN PAP3B_PHOTO_SPOTTED
				CASE 1
				RETURN PAP3B_PHOTO_OF_USE
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_PAPARAZZO_4
			SWITCH i
				CASE 0
				RETURN PAP4_ENTIRE_CREW_KILLED_IN_ONE
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_THELASTONE
			SWITCH i
				CASE 0
				RETURN TLO_WOUNDS
				CASE 1
				RETURN TLO_AMBIENT_ANIMAL_KILLS
				CASE 2
				RETURN TLO_TO_SITE_ON_FOOT
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_TONYA_1
			SWITCH i
				CASE 0
				RETURN TON1_TIME
				CASE 1
				RETURN TON1_UNHOOK
				CASE 2
				RETURN TON1_QUICK_UNHOOK
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_TONYA_2
			SWITCH i
				CASE 0
				RETURN TON2_TIME
				CASE 1
				RETURN TON2_UNHOOK
				CASE 2
				RETURN TON2_QUICK_UNHOOK
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_TONYA_3
			SWITCH i
				CASE 0
				RETURN TON3_TIME
				CASE 1
				RETURN TON3_UNHOOK
				CASE 2
				RETURN TON3_QUICK_UNHOOK
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_TONYA_4
			SWITCH i
				CASE 0
				RETURN TON4_TIME
				CASE 1
				RETURN TON4_UNHOOK
				CASE 2
				RETURN TON4_QUICK_UNHOOK
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_TONYA_5
			SWITCH i
				CASE 0
				RETURN TON5_TIME
				CASE 1
				RETURN TON5_UNHOOK
				CASE 2
				RETURN TON5_QUICK_UNHOOK
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
		CASE RC_HAO_1
			SWITCH i
				CASE 0
				RETURN HAO1_FASTEST_LAP
				CASE 1
				RETURN HAO1_RACE_TIME
				CASE 2
				RETURN HAO1_COLLISIONS
			ENDSWITCH
			RETURN UNSET_MISSION_STAT_ENUM
	ENDSWITCH
	RETURN UNSET_MISSION_STAT_ENUM
ENDFUNC

