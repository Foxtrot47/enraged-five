//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	script_launch_control.sch									//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Checks if any controller scripts should be launched when	//
//							required. This was set up to reduce the number of 			//
//							controller scripts that constantly run.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "script_player.sch"

USING "wardrobe_private.sch"
USING "random_events_public.sch"
USING "director_mode_public.sch"

#IF NOT USE_SP_DLC
	USING "photographyWildlife.sch"
	USING "randomChar_Public.sch"
#ENDIF


STRUCT ScriptLaunchVars
	INT iLaunchCheckStage
	INT iCurrentWardrobeFrame
	INT iWardrobeScriptRequestedBitset
	INT iDirectorModeLaunchState
	FEATURE_BLOCK_REASON eDirectorModeBlockLastFrame
	BOOL bWardrobeScriptRequested
	BOOL bFirstSpawnDone
	int iDelayLaunchTime
ENDSTRUCT

CONST_INT 	BIT_DM_LAUNCH_GAME_PAUSED		0
CONST_INT 	BIT_DM_LAUNCH_PRE_CLEANUP		1
CONST_INT	BIT_DM_LAUNCH_CONTROL_WAS_OFF	2


////////////////////////////////////////////////////
///    WARDROBE
CONST_INT MAX_FRAMES_TO_PROCESS_WARDROBES 30

CONST_FLOAT CABLE_CAR_RESTART_MIN_X       -800.0            // if player x is bigger than this - restart
CONST_FLOAT CABLE_CAR_RESTART_MAX_X       640.0      		// if player x is smaller than this - restart
CONST_FLOAT CABLE_CAR_RESTART_MIN_Y       5300.0            // if player y is bigger than this - restart
CONST_FLOAT CABLE_CAR_RESTART_MAX_Y       5800.0            // if player y is smaller than this - restart

#IF NOT USE_SP_DLC
VECTOR vAmbSolLaunchCenter = <<-1124.392212,-514.700134,33.214931>>
FLOAT fAmbSolLaunchRadius = 200.0
#endif

VECTOR vAmbUFOPosition0 = <<2490.0, 3777.0, 2402.879>>
VECTOR vAmbUFOPosition1 = <<-2052.0, 3237.0, 1450.078>>  // OLD <<-2366.379, 2962.758, 1448.873>>

CONST_FLOAT AMBIENT_UFO_SCRIPT_TRIGGER_RANGE	 290.0	// increased range for B*1686895



FUNC BOOL REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(INT paramScriptHash, #IF IS_DEBUG_BUILD STRING paramScriptName, #ENDIF INT paramStackSize = DEFAULT_STACK_SIZE)
	REQUEST_SCRIPT_WITH_NAME_HASH(paramScriptHash)
	IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(paramScriptHash)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Starting new \"", paramScriptName, "\" thread.")
		#ENDIF
		
		START_NEW_SCRIPT_WITH_NAME_HASH(paramScriptHash, paramStackSize)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(paramScriptHash)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



PROC DO_PLAYER_CONTROLLER_B_CHECKS()
	
	BOOL bPreventPlayerControllerBForMPStart = FALSE
	IF g_bInMultiplayer
		CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Prevent player_controller_b for g_bInMultiplayer")
		bPreventPlayerControllerBForMPStart = TRUE
	ENDIF
	IF MPGlobals.g_bEndSingleplayerNow
		CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Prevent player_controller_b for g_bEndSingleplayerNow")
		bPreventPlayerControllerBForMPStart = TRUE
	ENDIF
	IF g_TransitionData.SwoopStage = SKYSWOOP_GOINGUP
		CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Prevent player_controller_b for g_TransitionData.SwoopStage")
		bPreventPlayerControllerBForMPStart = TRUE
	ENDIF
	IF g_bRunMultiplayerOnStartup
		CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Prevent player_controller_b for g_bRunMultiplayerOnStartup")
		bPreventPlayerControllerBForMPStart = TRUE
	ENDIF
	
	IF (CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY) 
	AND NOT bPreventPlayerControllerBForMPStart)
#IF IS_DEBUG_BUILD
	OR g_bDebug_KeepPlayerControllerBRunning
#ENDIF

#IF USE_CLF_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("player_controller_bCLF")) = 0
			//#1356702
			IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			OR IS_PLAYER_SWITCH_IN_PROGRESS()
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				DISABLE_FRONTEND_THIS_FRAME()
			ENDIF
			
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( 	HASH("player_controller_bCLF"),
														#IF IS_DEBUG_BUILD "player_controller_bCLF", #ENDIF
														MULTIPLAYER_MISSION_STACK_SIZE)
		ENDIF
		
	ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("player_controller_bCLF"))
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("player_controller_bCLF"))
#ENDIF

#IF USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("player_controller_bNRM")) = 0
			//#1356702
			IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			OR IS_PLAYER_SWITCH_IN_PROGRESS()
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				DISABLE_FRONTEND_THIS_FRAME()
			ENDIF
			
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( 	HASH("player_controller_bNRM"),
														#IF IS_DEBUG_BUILD "player_controller_bNRM", #ENDIF
														MULTIPLAYER_MISSION_STACK_SIZE)
		ENDIF
			
	ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("player_controller_bNRM"))
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("player_controller_bNRM"))
#ENDIF

#IF NOT USE_SP_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("player_controller_b")) = 0
			//#1356702
			IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			OR IS_PLAYER_SWITCH_IN_PROGRESS()
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				DISABLE_FRONTEND_THIS_FRAME()
			ENDIF
			
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( 	HASH("player_controller_b"),
														#IF IS_DEBUG_BUILD "player_controller_b", #ENDIF
														MULTIPLAYER_MISSION_STACK_SIZE)
		ENDIF
		
	ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("player_controller_b"))
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("player_controller_b"))
#ENDIF
	ENDIF

ENDPROC



PROC DO_FRIENDS_CONTROLLER_CHECKS()	
	IF Should_Friends_Controller_Be_Running()
#IF IS_DEBUG_BUILD
	OR g_bDebug_KeepFriendsControllerRunning
#ENDIF
	
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("friends_controller")) = 0
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( 	HASH("friends_controller"),
														#IF IS_DEBUG_BUILD "friends_controller", #ENDIF
														FRIEND_STACK_SIZE)
		ENDIF
	ENDIF
ENDPROC



PROC DO_AUTOSAVE_CONTROLLER_CHECKS()

	// Check if code are wanting us to make an autosave.
	IF HAS_CODE_REQUESTED_AUTOSAVE()
		MAKE_AUTOSAVE_REQUEST()
		CLEAR_CODE_REQUESTED_AUTOSAVE()
	ENDIF
	
	IF g_sAutosaveData.iQueuedRequests > 0
#IF IS_DEBUG_BUILD
	OR g_bDebug_KeepAutoSaveControllerRunning
#ENDIF
		#if USE_CLF_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("autosave_controllerCLF")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("autosave_controllerCLF") #IF IS_DEBUG_BUILD ,"autosave_controllerCLF" #ENDIF )
			ENDIF
		#endif
		#if USE_NRM_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("autosave_controllerNRM")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("autosave_controllerNRM") #IF IS_DEBUG_BUILD ,"autosave_controllerNRM" #ENDIF )
			ENDIF
		#endif
		#IF NOT USE_SP_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("autosave_controller")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("autosave_controller") #IF IS_DEBUG_BUILD ,"autosave_controller" #ENDIF )
			ENDIF
		#endif
	ENDIF
ENDPROC



PROC DO_RESPAWN_CONTROLLER_CHECKS()
#if FEATURE_SP_DLC_DIRECTOR_MODE
	if !g_bRockstarEditorActive
#endif
		#IF USE_CLF_DLC
		CONST_INT paramScriptHash HASH("respawn_controllerCLF")
		#ENDIF
		#IF USE_NRM_DLC
		CONST_INT paramScriptHash HASH("respawn_controllerNRM")
		#ENDIF
		#IF NOT USE_SP_DLC
		CONST_INT paramScriptHash HASH("respawn_controller")
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		g_bDebug_IsRespawnControllerRunning = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(paramScriptHash) > 0
		#ENDIF

		IF NOT IS_CURRENT_MINIGAME_SET_TO_BYPASS_RESPAWN_CUTSCENE()	
		AND NOT IS_REPEAT_PLAY_ACTIVE()	//Disable the respawn controller during repeat plays
	#IF IS_DEBUG_BUILD
		OR g_bDebug_KeepRespawnControllerRunning
	#ENDIF
			
			
			REQUEST_SCRIPT_WITH_NAME_HASH(paramScriptHash)
			IF IS_PED_INJURED(PLAYER_PED_ID())
			OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
	#IF IS_DEBUG_BUILD
			OR g_bDebug_KeepRespawnControllerRunning
	#ENDIF
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					#IF USE_CLF_DLC
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(paramScriptHash) = 0
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_INJURED(PLAYER_PED_ID())
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PED_INJURED(PLAYER_PED_ID()).")
							ENDIF
							IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE).")
							ENDIF
							IF g_bDebug_KeepRespawnControllerRunning
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "g_bDebug_KeepRespawnControllerRunning.")
							ENDIF
							#ENDIF
							
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( paramScriptHash #IF IS_DEBUG_BUILD ,"respawn_controllerCLF" #ENDIF )
						ENDIF
					#ENDIF
					
					#IF USE_NRM_DLC
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(paramScriptHash) = 0
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_INJURED(PLAYER_PED_ID())
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PED_INJURED(PLAYER_PED_ID()).")
							ENDIF
							IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE).")
							ENDIF
							IF g_bDebug_KeepRespawnControllerRunning
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "g_bDebug_KeepRespawnControllerRunning.")
							ENDIF
							#ENDIF
							
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( paramScriptHash #IF IS_DEBUG_BUILD ,"respawn_controllerNRM" #ENDIF )
						ENDIF
					#ENDIF
					
					#IF NOT USE_SP_DLC
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(paramScriptHash) = 0
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_INJURED(PLAYER_PED_ID())
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PED_INJURED(PLAYER_PED_ID()).")
							ENDIF
							IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE).")
							ENDIF
							IF g_bDebug_KeepRespawnControllerRunning
								CPRINTLN(DEBUG_SCRIPT_LAUNCH, "g_bDebug_KeepRespawnControllerRunning.")
							ENDIF
							#ENDIF
							
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( paramScriptHash #IF IS_DEBUG_BUILD ,"respawn_controller" #ENDIF )
						ENDIF
					#ENDIF	
	#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "IS_PLAYER_SWITCH_IN_PROGRESS().")
	#ENDIF
				ENDIF
			ENDIF
		ELSE
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(paramScriptHash)
		ENDIF
#if FEATURE_SP_DLC_DIRECTOR_MODE
	endif
#endif
ENDPROC



PROC DO_WARDROBE_SCRIPT_CHECKS(ScriptLaunchVars &sVars)
	WARDROBE_LAUNCHER_STRUCT sWardrobeLauncherData
	
	// Update the frame that we are processing if not loading for the current
	IF NOT sVars.bWardrobeScriptRequested
		sVars.iCurrentWardrobeFrame = (sVars.iCurrentWardrobeFrame+1) % MAX_FRAMES_TO_PROCESS_WARDROBES
	ENDIF
							
	INT i, j
	REPEAT (NUM_PLAYER_PED_WARDROBES / MAX_FRAMES_TO_PROCESS_WARDROBES)+1 j
	
		i = sVars.iCurrentWardrobeFrame+(j*MAX_FRAMES_TO_PROCESS_WARDROBES)
		
		// Only perform an update if this wardrobe is within the range
		IF i < NUM_PLAYER_PED_WARDROBES
			sWardrobeLauncherData.eWardrobe = INT_TO_ENUM(PLAYER_WARDROBE_ENUM, i)
			
			// Cleanup
			IF g_bWardrobeScriptLaunched[i]
				IF NOT g_bWardrobeScriptRunning[i]
					g_bWardrobeScriptLaunched[i] = FALSE
				ENDIF
				
			// Request
			ELIF NOT g_bWardrobeScriptRunning[i]
			AND NOT IS_BIT_SET(sVars.iWardrobeScriptRequestedBitset, i)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_PED_WARDROBE_COORDS(sWardrobeLauncherData.eWardrobe), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 20.0
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Requesting wardrobe_sp script ", i)
					REQUEST_SCRIPT_WITH_NAME_HASH(HASH("wardrobe_sp"))
					SET_BIT(sVars.iWardrobeScriptRequestedBitset, i)
					sVars.bWardrobeScriptRequested = TRUE
				ENDIF
			
			// Launch
			ELSE
				IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("wardrobe_sp"))
					IF GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(SHOP_STACK_SIZE) > 1
						CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Launching wardrobe script ", i)
						g_bWardrobeScriptRunning[i] = TRUE
						g_bWardrobeScriptLaunched[i] = TRUE
						CLEAR_BIT(sVars.iWardrobeScriptRequestedBitset, i)
						
						// Set the launcher data and start script
						START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("wardrobe_sp"), sWardrobeLauncherData, SIZE_OF(sWardrobeLauncherData), SHOP_STACK_SIZE)
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("wardrobe_sp"))
#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Unable to launch wardrobe_sp script ", i, " - no free stacks")
#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF sVars.bWardrobeScriptRequested
	AND sVars.iWardrobeScriptRequestedBitset = 0
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("wardrobe_sp"))
		sVars.bWardrobeScriptRequested = FALSE
	ENDIF
ENDPROC



PROC DO_MODEL_SUPPRESS_CHECKS()
	IF g_bAmbientModelSuppressActive
		BOOL bModelSuppressed = FALSE
		INT i
		REPEAT COUNT_OF(g_eAmbientModelSuppress) i
			IF g_eAmbientModelSuppress[i] != DUMMY_MODEL_FOR_SCRIPT
				IF (GET_GAME_TIMER() - g_iAmbientModelSuppressTimer[i]) > 0
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SCRIPT_LAUNCH, "SET_MODEL_IS_SUPPRESSED_FOR_DURATION - No longer suppressing model ", GET_MODEL_NAME_FOR_DEBUG(g_eAmbientModelSuppress[i]))
					#ENDIF
					
					IF g_bAmbientModelSuppressVehicle[i]
						SET_VEHICLE_MODEL_IS_SUPPRESSED(g_eAmbientModelSuppress[i], FALSE)
					ELSE
						SET_PED_MODEL_IS_SUPPRESSED(g_eAmbientModelSuppress[i], FALSE)
					ENDIF
					g_eAmbientModelSuppress[i] = DUMMY_MODEL_FOR_SCRIPT
				ELSE
					IF g_bAmbientModelSuppressVehicle[i]
						SET_VEHICLE_MODEL_IS_SUPPRESSED(g_eAmbientModelSuppress[i], TRUE)
					ELSE
						SET_PED_MODEL_IS_SUPPRESSED(g_eAmbientModelSuppress[i], TRUE)
					ENDIF
					bModelSuppressed = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		IF NOT bModelSuppressed
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "SET_MODEL_IS_SUPPRESSED_FOR_DURATION - No more models to suppress")
			g_bAmbientModelSuppressActive = FALSE
		ENDIF
	ENDIF
ENDPROC



PROC DO_MISSION_TRIGGERER_CHECKS()

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)

#IF USE_CLF_DLC
		//If the game has been completed there is never a need to launch the triggerer.
		IF NOT g_savedGlobalsClifford.sFlow.flowCompleted
		
			//Don't launch anything until the flow has settled.
			IF NOT g_flowUnsaved.bFlowControllerBusy
			
				//Only start the mission trigger if the game is in a state where story missions can trigger.
				IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY) AND NOT IS_REPEAT_PLAY_ACTIVE()
				
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_trigger_CLF")) = 0
						REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("mission_trigger_CLF") #IF IS_DEBUG_BUILD ,"mission_trigger_CLF" #ENDIF )
					ENDIF		
				ELSE
					//We're on mission. Make sure to unload any loaded mission triggers.
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_trigger_CLF"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_trigger_CLF"))
					ENDIF
				ENDIF
				
				IF g_savedGlobalsClifford.sFlow.flowCompleted
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_trigger_CLF"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_trigger_CLF"))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
#ENDIF

#IF USE_NRM_DLC
	//If the game has been completed there is never a need to launch the triggerer.
	IF NOT g_savedGlobalsnorman.sFlow.flowCompleted
	
		//Don't launch anything until the flow has settled.
		IF NOT g_flowUnsaved.bFlowControllerBusy
		
			//Only start the mission trigger if the game is in a state where story missions can trigger.
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY) AND NOT IS_REPEAT_PLAY_ACTIVE()
			
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_triggerNRM")) = 0
					REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("mission_triggerNRM") #IF IS_DEBUG_BUILD ,"mission_triggerNRM" #ENDIF )
				ENDIF		
			ELSE
				//We're on mission. Make sure to unload any loaded mission triggers.
				IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerNRM"))
					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerNRM"))
				ENDIF
			ENDIF
			
			IF g_savedGlobalsnorman.sFlow.flowCompleted
				IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerNRM"))
					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerNRM"))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
#ENDIF

#IF NOT USE_SP_DLC
		//If the game has been completed there is never a need to launch the triggerer.
		IF NOT g_savedGlobals.sFlow.flowCompleted
		
			//Don't launch anything until the flow has settled.
			IF NOT g_flowUnsaved.bFlowControllerBusy
			
				//Only start the mission trigger if the game is in a state where story missions can trigger.
				IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY) AND NOT IS_REPEAT_PLAY_ACTIVE()
			
					//Load the mission triggerer with mission data up to the end of Trevor1.
					IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_triggerer_A")) = 0
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(	HASH("mission_triggerer_A") 
																		#IF IS_DEBUG_BUILD ,"mission_triggerer_A" #ENDIF , 
																		MULTIPLAYER_MISSION_STACK_SIZE)
						ENDIF
					
					//Load the mission triggerer with mission data from Trevor2 to the end of FIB4.
					ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_triggerer_B")) = 0
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(	HASH("mission_triggerer_B") 
																		#IF IS_DEBUG_BUILD ,"mission_triggerer_B" #ENDIF , 
																		MULTIPLAYER_MISSION_STACK_SIZE)
						ENDIF
						
					//Load the mission triggerer with mission data from Carsteal1 to the end of The Big Score 1.
					ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_1)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_triggerer_C")) = 0
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(	HASH("mission_triggerer_C") 
																		#IF IS_DEBUG_BUILD ,"mission_triggerer_C" #ENDIF , 
																		MULTIPLAYER_MISSION_STACK_SIZE)
						ENDIF
					
					//Load the mission triggerer with mission data for the rest of the game.
					ELSE
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_triggerer_D")) = 0
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(	HASH("mission_triggerer_D") 
																		#IF IS_DEBUG_BUILD ,"mission_triggerer_D" #ENDIF , 
																		MULTIPLAYER_MISSION_STACK_SIZE)
						ENDIF
					ENDIF
				ELSE
					//We're on mission. Make sure to unload any loaded mission triggers.
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_A"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_A"))
					ENDIF
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_B"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_B"))
					ENDIF
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_C"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_C"))
					ENDIF
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_D"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_D"))
					ENDIF
				ENDIF
				
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_A"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_A"))
					ENDIF
				ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4)
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_B"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_B"))
					ENDIF
				ELIF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_1)
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_C"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_C"))
					ENDIF
				ELIF g_savedGlobals.sFlow.flowCompleted
					IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("mission_triggerer_D"))
						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("mission_triggerer_D"))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
#ENDIF

	ENDIF

ENDPROC



PROC DO_MISSION_REPEAT_CHECKS()
	IF IS_REPEAT_PLAY_ACTIVE(TRUE)	//Ignore the benchmark script, not actually a mission
	
		#IF USE_CLF_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_repeat_controllerCLF")) = 0
				IF NOT IS_BIT_SET(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PAUSED_GAME))
					CPRINTLN(DEBUG_REPEAT, "Pausing game to launch repeat play.")
					SET_GAME_PAUSED(TRUE)
					SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PAUSED_GAME))
				ENDIF
				IF REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("mission_repeat_controllerCLF") #IF IS_DEBUG_BUILD ,"mission_repeat_controllerCLF" #ENDIF , FRIEND_STACK_SIZE)
					CPRINTLN(DEBUG_REPEAT, "Unpausing game as repeat play script launched.")
					SET_GAME_PAUSED(FALSE)
				ENDIF
			ENDIF
		#ENDIF
	
		#IF USE_NRM_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_repeat_controllerNRM")) = 0
				IF NOT IS_BIT_SET(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PAUSED_GAME))
					CPRINTLN(DEBUG_REPEAT, "Pausing game to launch repeat play.")
					SET_GAME_PAUSED(TRUE)
					SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PAUSED_GAME))
				ENDIF
				IF REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("mission_repeat_controllerNRM") #IF IS_DEBUG_BUILD ,"mission_repeat_controllerNRM" #ENDIF , FRIEND_STACK_SIZE)
					CPRINTLN(DEBUG_REPEAT, "Unpausing game as repeat play script launched.")
					SET_GAME_PAUSED(FALSE)
				ENDIF
			ENDIF
		#ENDIF
		
		#IF NOT USE_SP_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_repeat_controller")) = 0
				IF NOT IS_BIT_SET(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PAUSED_GAME))
					CPRINTLN(DEBUG_REPEAT, "Pausing game to launch repeat play.")
					SET_GAME_PAUSED(TRUE)
					SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PAUSED_GAME))
				ENDIF
				IF REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("mission_repeat_controller") #IF IS_DEBUG_BUILD ,"mission_repeat_controller" #ENDIF , FRIEND_STACK_SIZE)
					CPRINTLN(DEBUG_REPEAT, "Unpausing game as repeat play script launched.")
					SET_GAME_PAUSED(FALSE)
				ENDIF
			ENDIF
		#ENDIF
		
	ENDIF
ENDPROC



PROC DO_CODE_CONTROLLER_CHECKS()

	#IF USE_CLF_DLC
		IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("code_controllerCLF")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("code_controllerCLF") #IF IS_DEBUG_BUILD ,"code_controllerCLF" #ENDIF )
			ENDIF
		ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("code_controllerCLF"))
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("code_controllerCLF"))
		ENDIF
	#ENDIF	
	
	#IF USE_NRM_DLC
		IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("code_controllerNRM")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("code_controllerNRM") #IF IS_DEBUG_BUILD ,"code_controllerNRM" #ENDIF )
			ENDIF
		ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("code_controllerNRM"))
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("code_controllerNRM"))
		ENDIF
	#ENDIF	
	
	#IF NOT USE_SP_DLC
		IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("code_controller")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("code_controller") #IF IS_DEBUG_BUILD ,"code_controller" #ENDIF )
			ENDIF
		ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("code_controller"))
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("code_controller"))
		ENDIF
	#ENDIF
	
ENDPROC



PROC DO_RC_CONTROLLER_CHECKS()
	IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_RANDOM_CHAR)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("RandomChar_Controller")) = 0
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("RandomChar_Controller") #IF IS_DEBUG_BUILD ,"RandomChar_Controller" #ENDIF )
		ENDIF
	ENDIF
ENDPROC



PROC DO_RACE_CONTROLLER_CHECKS()
#IF NOT USE_SP_DLC
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STREET_RACES))
	OR GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_SEA_RACES))
		IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("controller_Races")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("controller_Races") #IF IS_DEBUG_BUILD ,"controller_Races" #ENDIF )
			ENDIF
		ENDIF
	ENDIF
#endif
ENDPROC



PROC DO_FLOW_HELP_CHECKS()

	IF g_OnMissionState = MISSION_TYPE_FRIEND_ACTIVITY
	OR g_OnMissionState = MISSION_TYPE_OFF_MISSION	
	OR g_bHeistBoardViewActive
	
		#IF USE_CLF_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("flow_helpCLF")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("flow_helpCLF") #IF IS_DEBUG_BUILD ,"flow_helpCLF" #ENDIF , MICRO_STACK_SIZE )
			ENDIF
		#ENDIF	
		
		#IF USE_NRM_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("flow_helpNRM")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("flow_helpNRM") #IF IS_DEBUG_BUILD ,"flow_helpNRM" #ENDIF , MICRO_STACK_SIZE )
			ENDIF
		#ENDIF	
		
		#IF NOT USE_SP_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("flow_help")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("flow_help") #IF IS_DEBUG_BUILD ,"flow_help" #ENDIF , MICRO_STACK_SIZE )
			ENDIF
		#ENDIF
	
	ENDIF
	
ENDPROC



PROC DO_BOOTY_CALL_HANDLER_CHECKS()
	IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bootycallhandler")) = 0
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("bootycallhandler") #IF IS_DEBUG_BUILD ,"bootycallhandler" #ENDIF)
		ENDIF
	ENDIF
ENDPROC



PROC DO_COMMS_CONTROLLER_CHECKS()
	IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		
		#IF USE_CLF_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("comms_controllerCLF")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("comms_controllerCLF") #IF IS_DEBUG_BUILD ,"comms_controllerCLF" #ENDIF )
			ENDIF
		#ENDIF
		
		#IF USE_NRM_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("comms_controllerNRM")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("comms_controllerNRM") #IF IS_DEBUG_BUILD ,"comms_controllerNRM" #ENDIF )
			ENDIF
		#ENDIF
		
		#IF NOT USE_SP_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("comms_controller")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("comms_controller") #IF IS_DEBUG_BUILD ,"comms_controller" #ENDIF )
			ENDIF
		#ENDIF	
		
	ENDIF
ENDPROC


PROC DO_REPLAY_CONTROLLER_CHECKS()

	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
	
		#IF USE_CLF_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("replay_controllerCLF")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("replay_controllerCLF") #IF IS_DEBUG_BUILD ,"replay_controllerCLF" #ENDIF )
			ENDIF
		#ENDIF
		
		#IF USE_NRM_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("replay_controllerNRM")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("replay_controllerNRM") #IF IS_DEBUG_BUILD ,"replay_controllerNRM" #ENDIF )
			ENDIF
		#ENDIF
		
		#IF NOT USE_SP_DLC
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("replay_controller")) = 0
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("replay_controller") #IF IS_DEBUG_BUILD ,"replay_controller" #ENDIF )
			ENDIF 
		#ENDIF
	
	ENDIF
ENDPROC


PROC DO_SAVE_ANYWHERE_CHECKS()
	IF g_HasCellphoneRequestedSave
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("save_anywhere")) = 0
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("save_anywhere") #IF IS_DEBUG_BUILD ,"save_anywhere" #ENDIF )
		ENDIF
	ENDIF
ENDPROC


#IF FEATURE_SP_DLC_DIRECTOR_MODE
	PROC DO_DIRECTOR_MODE_CHECKS(ScriptLaunchVars &sVars)
	
		IF HAS_DIRECTOR_MODE_BEEN_LAUNCHED_BY_CODE()
			CPRINTLN(DEBUG_DIRECTOR, "Detected director mode launch coming from code.")
			g_bLaunchDirectorMode = TRUE
			SET_DIRECTOR_MODE_LAUNCHED_BY_SCRIPT()
		ENDIF
		
		FEATURE_BLOCK_REASON eBlockReasonThisFrame = GET_FEATURE_BLOCKED_REASON(TRUE,default,DEBUG_DIRECTOR)
//		#IF IS_DEBUG_BUILD
//			CDEBUG3LN(DEBUG_DIRECTOR, " - script_launch_control - DO_DIRECTOR_MODE_CHECKS - eBlockReasonThisFrame:             ", eBlockReasonThisFrame)
//			CDEBUG3LN(DEBUG_DIRECTOR, " - script_launch_control - DO_DIRECTOR_MODE_CHECKS - sVars.eDirectorModeBlockLastFrame: ", sVars.eDirectorModeBlockLastFrame)
//		#ENDIF
		
		IF eBlockReasonThisFrame != sVars.eDirectorModeBlockLastFrame
			IF eBlockReasonThisFrame = FBR_NONE
				CPRINTLN(DEBUG_DIRECTOR, "Detected Director Mode is no longer blocked. Setting available.")
				SET_DIRECTOR_MODE_AVAILABLE(TRUE)
			ELSE
				CPRINTLN(DEBUG_DIRECTOR, "Detected Director Mode is now blocked (Reason:", DEBUG_GET_FEATURE_BLOCK_REASON_STRING(eBlockReasonThisFrame,DEBUG_DIRECTOR), "). Setting unavailable.")
				SET_DIRECTOR_MODE_AVAILABLE(FALSE)
			ENDIF
			sVars.eDirectorModeBlockLastFrame = eBlockReasonThisFrame
		ENDIF
		
		IF g_bLaunchDirectorMode
			IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				CPRINTLN(DEBUG_DIRECTOR, "Main thread detected request to launch the Director Mode script...")

				IF eBlockReasonThisFrame = FBR_NONE
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),TRUE)
				
					//Director Mode is allowed to launch. Fire the script now.
					IF NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
						CPRINTLN(DEBUG_DIRECTOR, "Fading screen out in peraparation for launching Director Mode.")
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					ELSE
						IF NOT IS_BIT_SET(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_PRE_CLEANUP)
							CPRINTLN(DEBUG_DIRECTOR, "Running Director Mode pre-launch cleanup..")
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							//Turn player control off, remember if it used to be on
							IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
								SET_BIT(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_CONTROL_WAS_OFF)
							ENDIF
							CPRINTLN(DEBUG_DIRECTOR,"Setting player control OFF as we launch DM, it used to be ",IS_PLAYER_CONTROL_ON(PLAYER_ID()))
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						
							SET_BIT(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_PRE_CLEANUP)
						ENDIF
					
						IF REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("director_mode") #IF IS_DEBUG_BUILD ,"director_mode" #ENDIF, MISSION_STACK_SIZE)
							sVars.iDirectorModeLaunchState = 0
							g_bLaunchDirectorMode = FALSE
						ENDIF
					ENDIF
					
				ELSE
					if GET_GAME_TIMER() - sVars.iDelayLaunchTime >= 250	
					
						//Draw the warning screen.
						SET_GAME_PAUSED(TRUE)
						HIDE_LOADING_ON_FADE_THIS_FRAME()
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)		
						SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						
						STRING strFBLabel = GET_FEATURE_BLOCK_MESSAGE_TEXT(eBlockReasonThisFrame,DEBUG_DIRECTOR)
						SET_WARNING_MESSAGE("FBR_GENERIC", FE_WARNING_CONTINUE, DEFAULT, DEFAULT, DEFAULT, "FBR_DIR_MODE", strFBLabel)
						
						CDEBUG3LN(DEBUG_DIRECTOR, "Displaying ", DEBUG_GET_FEATURE_BLOCK_REASON_STRING(eBlockReasonThisFrame,DEBUG_DIRECTOR), " block screen.")
															
						
						CDEBUG3LN(DEBUG_DIRECTOR, "now able to clear alert screen")
						if !IS_BIT_SET(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_GAME_PAUSED)
							SET_BIT(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_GAME_PAUSED)
						ELSE
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
								IF IS_BIT_SET(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_GAME_PAUSED)							
									CLEAR_BIT(sVars.iDirectorModeLaunchState, BIT_DM_LAUNCH_GAME_PAUSED)
								ENDIF						
								
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),FALSE)	//Disable temporary invulnerability in case of exiting
								SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								SET_GAME_PAUSED(FALSE)
								CPRINTLN(DEBUG_DIRECTOR, "Cleaning up Director Mode launch due to block reason.")
								sVars.iDirectorModeLaunchState = 0
								g_bLaunchDirectorMode = FALSE
								IF NOT IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
									DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
								ENDIF
							
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				CPRINTLN(DEBUG_DIRECTOR, "Director Mode is already launched. Flagging to return to trailer instead of launching.")
				CDEBUG1LN(DEBUG_DIRECTOR,"DO_DIRECTOR_MODE_CHECKS: setting g_bDirectorSwitchToCastingTrailer to TRUE")
				g_bDirectorSwitchToCastingTrailer = TRUE
				g_bDirectorSwitchToCastingTrailerFromCode = TRUE
				g_bLaunchDirectorMode = FALSE
			ENDIF
		ELSE
			sVars.iDelayLaunchTime = GET_GAME_TIMER()
		ENDIF
	ENDPROC
	
#ENDIF


PROC DO_CABLECAR_LAUNCH_CHECKS()
	IF (g_bInMultiplayer = FALSE) AND (g_bForceNoCableCar = FALSE) AND NOT GET_MISSION_FLAG()
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	
			VECTOR plyrPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF (plyrPos.x >= CABLE_CAR_RESTART_MIN_X) AND (plyrPos.y >= CABLE_CAR_RESTART_MIN_Y) AND (plyrPos.x <= CABLE_CAR_RESTART_MAX_X) AND (plyrPos.y <= CABLE_CAR_RESTART_MAX_Y)
			
				#IF USE_CLF_DLC
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cablecarCLF")) = 0
						REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("cablecarCLF") #IF IS_DEBUG_BUILD ,"cablecarCLF" #ENDIF )
					ENDIF
					
					EXIT 
				#ENDIF
				
				#IF NOT USE_SP_DLC
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) = TRUE		
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cablecar")) = 0
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("cablecar") #IF IS_DEBUG_BUILD ,"cablecar" #ENDIF )
						ENDIF
					ENDIF
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
		/*
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cablecar")) = 0)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				#IF NOT USE_SP_DLC
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) = TRUE
				#endif
					VECTOR plyrPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					IF (plyrPos.x >= CABLE_CAR_RESTART_MIN_X) AND (plyrPos.y >= CABLE_CAR_RESTART_MIN_Y) AND (plyrPos.x <= CABLE_CAR_RESTART_MAX_X) AND (plyrPos.y <= CABLE_CAR_RESTART_MAX_Y)
						REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("cablecar") #IF IS_DEBUG_BUILD ,"cablecar" #ENDIF )
					ENDIF
				#IF NOT USE_SP_DLC
				ENDIF
				#endif
			ENDIF
		ENDIF
		*/
		
ENDPROC


PROC DO_AMBIENT_UFOS_LAUNCH_CHECKS()

	IF (g_bInMultiplayer = FALSE) AND (g_bForceNoAmbientUFO = FALSE)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ambient_ufos")) = 0)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF (g_savedGlobals.sCompletionPercentageData.b_g_OneHundredPercentReached = TRUE)
					IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAmbUFOPosition0) < AMBIENT_UFO_SCRIPT_TRIGGER_RANGE)
						OR (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAmbUFOPosition1) < AMBIENT_UFO_SCRIPT_TRIGGER_RANGE)
							REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("ambient_ufos") #IF IS_DEBUG_BUILD ,"ambient_ufos" #ENDIF )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC DO_AMBIENT_SOLOMON_LAUNCH_CHECKS()

	#IF NOT USE_SP_DLC
		IF (g_bInMultiplayer = FALSE) AND NOT GET_MISSION_FLAG()
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN) = FALSE
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_SOLOMON_1) = FALSE
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ambient_solomon")) = 0)
						IF IS_MISSION_AVAILABLE(SP_MISSION_SOLOMON_1)
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAmbSolLaunchCenter) <= (fAmbSolLaunchRadius * fAmbSolLaunchRadius)
									IF (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL)
										REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("ambient_solomon") #IF IS_DEBUG_BUILD ,"ambient_solomon" #ENDIF )
									ENDIF

								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
ENDPROC


#IF NOT USE_SP_DLC
	PROC DO_PHOTOGRAPHY_WILDLIFE_LAUNCH_CHECKS()
		IF IS_LAST_GEN_PLAYER()
		AND NOT IS_REPEAT_PLAY_ACTIVE()
		AND NOT IS_DIRECTOR_MODE_RUNNING()
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_WILDLIFE_PHOTOGRAPHY_UNLOCKED)
#IF IS_DEBUG_BUILD
			AND !g_bBlockPhotographyWildlifeRelaunch
#ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("photographyWildlife")) = 0
					AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_COLLECT_WILDLIFE_PHOTOGRAPHS_FINISHED)
					AND NOT g_flowUnsaved.bUpdatingGameflow
						REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(HASH("photographyWildlife") #IF IS_DEBUG_BUILD , "photographyWildlife" #ENDIF)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF


PROC DO_MINIGAME_LAUNCH_CHECKS()
	
	IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
		IF NOT g_HasPerformedMiniGameRangeChecks
			VECTOR vMyCurrentPlayerCoords
			STRING launcherScriptName
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vMyCurrentPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ELSE
				EXIT // Leave if the ped is injured.
			ENDIF
			

			//BASE Jumping Helicopter
			launcherScriptName = "launcher_BasejumpHeli"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_BasejumpHeli")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_BJ_HELI_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_LARGE)	
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//BASE Jumping Pack
			launcherScriptName = "launcher_BasejumpPack"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_BasejumpPack")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_BJ_PACK_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_MEDIUM)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Golf
			launcherScriptName = "launcher_golf"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_golf")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_GOLF_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_LARGE)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Hunting
			launcherScriptName = "launcher_Hunting_Ambient"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_Hunting_Ambient")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_HUNTING_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_LARGE)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Off Road Racing
			launcherScriptName = "launcher_OffroadRacing"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_OffroadRacing")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_OFFROAD_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_LARGE)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Racing
			launcherScriptName = "launcher_Racing"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_Racing")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_RACING_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_LARGE)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			//Rampage
			launcherScriptName = "launcher_rampage"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_rampage")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_RAMPAGE_LAUNCHER(vMyCurrentPlayerCoords, FLOOR(RC_BRAIN_ACTIVATION_RANGE_EXTRA))	
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Flight school
			launcherScriptName = "launcher_pilotschool"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_pilotschool")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_PILOT_SCHOOL_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_MEDIUM)	
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Shooting Range
			launcherScriptName = "launcher_range"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_range")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_RANGE_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_SMALL)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Stunt plane time trials
			launcherScriptName = "launcher_stunts"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_stunts")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_STUNTS_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_LARGE)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Tennis
			launcherScriptName = "launcher_tennis"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_tennis")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_TENNIS_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_MEDIUM)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Triathlon
			launcherScriptName = "launcher_Triathlon"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_Triathlon")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_TRIATHLON_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_MEDIUM)	
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Yoga
			launcherScriptName = "launcher_Yoga"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_Yoga")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_YOGA_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_SMALL)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			//Darts
			launcherScriptName = "launcher_Darts"
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_Darts")) = 0
				//check if im in range of one of the points
				IF IS_PLAYER_WITHIN_RANGE_OF_DARTS_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_MEDIUM)		
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
					//object attached, so using object_brains reactivation instead of world_brains
					REACTIVATE_NAMED_OBJECT_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
				ELSE
					CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
			ENDIF
			
			
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Finished performing mini game launch script checks ")
			g_HasPerformedMiniGameRangeChecks = TRUE //set a flag that we've completed the checks
		ENDIF
	ELIF g_HasPerformedMiniGameRangeChecks
		g_HasPerformedMiniGameRangeChecks = FALSE //clear the flag that we've completed the checks
	ENDIF

ENDPROC 

#IF NOT USE_SP_DLC
PROC DO_PHOTOGRAPHY_MONKEY_LAUNCH_CHECKS()
	IF( g_SavedGlobals.sAmbient.iPhotographyMonkeyFlags[ 1 ] <> -1 )
		IF NOT g_bInMultiplayer
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("photographyMonkey")) = 0
			AND NOT g_flowUnsaved.bUpdatingGameflow
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("photographyMonkey") #IF IS_DEBUG_BUILD ,"photographyMonkey" #ENDIF , FRIEND_STACK_SIZE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC DO_BLIMP_LAUNCH_CHECKS()

	#IF NOT USE_SP_DLC
	
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
			IF IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_BLIMP)
				IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_BLIMP_UNLOCK)
				OR GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_BLIMP, 0) != LISTED
				OR GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_BLIMP, 1) != LISTED
				OR GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_BLIMP, 2) != LISTED 
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ambientblimp")) = 0
						REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("ambientblimp") #IF IS_DEBUG_BUILD ,"ambientblimp" #ENDIF )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	#ENDIF
	
ENDPROC


PROC DO_ENVIRO_LAUNCH_CHECK()

	#IF USE_NRM_DLC
	
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("enviro_controllerNRM")) = 0)	
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("enviro_controllerNRM") #IF IS_DEBUG_BUILD ,"enviro_controllerNRM" #ENDIF )
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("norman_controller")) = 0)	
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("norman_controller") #IF IS_DEBUG_BUILD ,"norman_controller" #ENDIF )
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("hostile_controller")) = 0)	
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("hostile_controller") #IF IS_DEBUG_BUILD ,"hostile_controller" #ENDIF )
		ENDIF
		
	#ENDIF
	
ENDPROC


PROC DO_ANIMAL_CONTROLLER_LAUNCH_CHECKS()
	#IF NOT USE_SP_DLC
	
		#IF IS_DEBUG_BUILD
		IF NOT g_flowUnsaved.bUpdatingGameflow
		AND GET_GAME_TIMER() > g_iDebugPeyoteBlockControllerUntil
		#ENDIF
		
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_ANIMAL)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("animal_controller")) = 0
						REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("animal_controller") #IF IS_DEBUG_BUILD ,"animal_controller" #ENDIF, MULTIPLAYER_MISSION_STACK_SIZE)
					ENDIF
				ENDIF
			ENDIF
			
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF

	#ENDIF
ENDPROC

#IF USE_CLF_DLC
PROC DO_JETPACK_LAUNCH_CHECK
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jetpack_controller")) = 0)
	AND (GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_JP_UNLOCKED))
	AND ((NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()) OR g_sjpData.bJPEnabledOnMission)
	AND (NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID()))
		REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(HASH("jetpack_controller") #IF IS_DEBUG_BUILD ,"jetpack_controller" #ENDIF)
	ENDIF
ENDPROC
#ENDIF

PROC DO_PI_MENU_LAUNCH_CHECKS()

	//add terminate conditions in here
	#IF USE_CLF_DLC
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CLF_TRAIN)
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENUCLF")) = 0)	
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("PI_MENUCLF") #IF IS_DEBUG_BUILD ,"PI_MENUCLF" #ENDIF )
			ENDIF
		ENDIF	
	#ENDIF
	
	#IF USE_NRM_DLC
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_NRM_SUR_START)
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENUNRM")) = 0)	
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("PI_MENUNRM") #IF IS_DEBUG_BUILD ,"PI_MENUNRM" #ENDIF )
			ENDIF
		ENDIF	
	#ENDIF
	
	#IF NOT USE_SP_DLC
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)			
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_PIMENU_ACTIVATE)
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("PIMENU_ACTIVATE")
					CASE FHS_EXPIRED
						ADD_HELP_TO_FLOW_QUEUE("PIMENU_ACTIVATE", FHP_LOW,60000)
					BREAK
					CASE FHS_DISPLAYED
						// Autosave and terminate script
						SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_PIMENU_ACTIVATE)						
					BREAK
				ENDSWITCH	
			ENDIF			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENU")) = 0				
				REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("PI_MENU") #IF IS_DEBUG_BUILD ,"PI_MENU" #ENDIF,MULTIPLAYER_MISSION_STACK_SIZE)	
			ENDIF
		ENDIF
	#ENDIF
	
ENDPROC


PROC DO_COUNTRY_RACE_CONTROLLER_LAUNCH_CHECKS()
	#IF NOT USE_SP_DLC
	
		//CGtoNG only content.
		IF IS_LAST_GEN_PLAYER()
			IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_COUNTRY_RACE))
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
					IF NOT g_savedGlobals.sCountryRaceData.bTextRegistered
						Execute_Code_ID(CID_ACTIVATE_MINIGAME_COUNTRY_RACE, 0)
					ENDIF
					
					//Fix for 2820744: It is possible we register the text message from Trevor to unlock the MG but
					//the player chooses to kill Trevor before it sends. At this point the MG would never unlock.
					//Detect this situation and auto unlock it.
					IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_KILLED)
						CPRINTLN(DEBUG_FLOW, "Detected player killed Trevor without Country Races being unlocked. Unlocking.")
						Execute_Code_ID(CID_COUNTRY_RACE_TEXT_RECEIVED, 0)
					ENDIF
					
				ENDIF
			ELSE
				IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
					IF NOT IS_MISSION_AVAILABLE(SP_MISSION_CHINESE_2)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_GameBlips[STATIC_BLIP_MINIGAME_COUNTRY_RACE].vCoords[0]) < 90000 // 300^2
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("country_race_controller")) = 0
									REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("country_race_controller") #IF IS_DEBUG_BUILD ,"country_race_controller" #ENDIF)	
								ENDIF
							ELSE
								//If the player is away from the trigger and the controller isn't running, set blip active.
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("country_race_controller")) = 0
									IF g_savedGlobals.sCountryRaceData.bDisrupted
										g_savedGlobals.sCountryRaceData.bDisrupted = FALSE 
									ENDIF
									IF g_savedGlobals.sCountryRaceData.bRaceJustCompleted
										g_savedGlobals.sCountryRaceData.bRaceJustCompleted = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_COUNTRY_RACE))
				CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Disabling country races minigame as the player is no longer a Last-Gen player.")
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_COUNTRY_RACE), FALSE)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_COUNTRY_RACE, FALSE)
			ENDIF
		ENDIF

	#ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
PROC DO_FRIENDS_DEBUG_CONTROLLER_CHECKS()
	IF g_bDebug_KeepFriendsDebugControllerRunning
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("friends_debug_controller")) = 0
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH(	HASH("friends_debug_controller"),
														#IF IS_DEBUG_BUILD "friends_debug_controller", #ENDIF  
														CELLPHONE_STACK_SIZE)	
		ENDIF
	ENDIF
ENDPROC
#ENDIF


PROC INITIALISE_SCRIPT_LAUNCH_CHECKS(	#IF IS_DEBUG_BUILD WIDGET_GROUP_ID main_widget #ENDIF	)
	CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Script launch checks initialising.")
	
	#IF IS_DEBUG_BUILD
		SET_CURRENT_WIDGET_GROUP(main_widget)
		START_WIDGET_GROUP("Script Launch Controller")
			ADD_WIDGET_BOOL("Keep player_controller_b running", g_bDebug_KeepPlayerControllerBRunning)
			ADD_WIDGET_BOOL("Keep autosave_controller running", g_bDebug_KeepAutoSaveControllerRunning)
			
			ADD_WIDGET_BOOL("Keep respawn_controller running", g_bDebug_KeepRespawnControllerRunning)
			ADD_WIDGET_BOOL("Is respawn_controller running", g_bDebug_IsRespawnControllerRunning)
			
			ADD_WIDGET_BOOL("Keep drunk_controller running", g_bDebug_KeepDrunkControllerRunning)
			ADD_WIDGET_BOOL("Keep friends_controller running", g_bDebug_KeepFriendsControllerRunning)
			ADD_WIDGET_BOOL("Keep friends_debug_controller running", g_bDebug_KeepFriendsDebugControllerRunning)

		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(main_widget)
	#ENDIF
	
	// Cleanup previous autosave flags
	g_sAutosaveData.bRequest = FALSE
	g_sAutosaveData.iQueuedRequests = 0
ENDPROC


PROC DO_SCRIPT_LAUNCH_CHECKS(ScriptLaunchVars &sVars)
	
	IF sVars.bFirstSpawnDone

		//Don't check during a debug launch.
		//Schedule a set of checks per frame.
		SWITCH sVars.iLaunchCheckStage
		
			CASE 0
				DO_MISSION_TRIGGERER_CHECKS()
				DO_PLAYER_CONTROLLER_B_CHECKS()
				DO_PI_MENU_LAUNCH_CHECKS()
				DO_COUNTRY_RACE_CONTROLLER_LAUNCH_CHECKS()

				#IF NOT USE_SP_DLC
					DO_FRIENDS_CONTROLLER_CHECKS()
					DO_BLIMP_LAUNCH_CHECKS()				
				#ENDIF
				
			BREAK
			
			CASE 1
				DO_AUTOSAVE_CONTROLLER_CHECKS()
				DO_WARDROBE_SCRIPT_CHECKS(sVars)
				DO_MODEL_SUPPRESS_CHECKS()
				DO_MISSION_REPEAT_CHECKS()
				DO_ANIMAL_CONTROLLER_LAUNCH_CHECKS()
			BREAK
			
			CASE 2
				DO_FLOW_HELP_CHECKS()
				DO_COMMS_CONTROLLER_CHECKS()
				DO_REPLAY_CONTROLLER_CHECKS()
				DO_SAVE_ANYWHERE_CHECKS()
				
				
				#IF USE_NRM_DLC
					DO_ENVIRO_LAUNCH_CHECK()
				#ENDIF

				#IF USE_CLF_DLC
					DO_JETPACK_LAUNCH_CHECK()
				#ENDIF
				
				#IF NOT USE_SP_DLC
					DO_BOOTY_CALL_HANDLER_CHECKS()
				#ENDIF	
			BREAK
			
			CASE 3
				#IF NOT USE_SP_DLC
					DO_AMBIENT_UFOS_LAUNCH_CHECKS()
					DO_RC_CONTROLLER_CHECKS()
					DO_RACE_CONTROLLER_CHECKS()
					DO_AMBIENT_SOLOMON_LAUNCH_CHECKS()
					DO_PHOTOGRAPHY_WILDLIFE_LAUNCH_CHECKS()
					DO_PHOTOGRAPHY_MONKEY_LAUNCH_CHECKS()
				#ENDIF
				
				DO_CABLECAR_LAUNCH_CHECKS()				
				DO_MINIGAME_LAUNCH_CHECKS()
			BREAK
		ENDSWITCH
		
		sVars.iLaunchCheckStage++
		IF sVars.iLaunchCheckStage > 3
			sVars.iLaunchCheckStage = 0
		ENDIF

		#IF IS_DEBUG_BUILD
			#IF NOT USE_SP_DLC
				DO_FRIENDS_DEBUG_CONTROLLER_CHECKS()
			#ENDIF	
		#ENDIF
		
		//Check during a debug launch.
		DO_CODE_CONTROLLER_CHECKS()
		DO_RESPAWN_CONTROLLER_CHECKS()
		
		#IF FEATURE_SP_DLC_DIRECTOR_MODE
			DO_DIRECTOR_MODE_CHECKS(sVars)
		#ENDIF
		
	//Wait until the player has spawned the first time before
	//running checks. Might be returning from dead MP player.
	ELIF NOT IS_PED_INJURED(PLAYER_PED_ID())
		sVars.bFirstSpawnDone = TRUE
	ENDIF
ENDPROC
