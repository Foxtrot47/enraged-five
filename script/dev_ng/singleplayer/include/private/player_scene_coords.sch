USING "familyCoords_private.sch"

#IF IS_DEBUG_BUILD
USING "player_ped_debug.sch"
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_coords.sch										//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC STRING GET_ROOM_NAME_FOR_KEY(INT iLastKnownRoomKey)
	
	SWITCH iLastKnownRoomKey
		CASE HASH("v_chopshop")			RETURN "v_chopshop" BREAK
		CASE HASH("v_franklins")		RETURN "v_franklins" BREAK
		CASE HASH("v_franklinshouse")	RETURN "v_franklinshouse" BREAK
		CASE HASH("v_methlab")			RETURN "v_methlab" BREAK
		CASE HASH("v_michael")			RETURN "v_michael" BREAK
		CASE HASH("v_strip3")			RETURN "v_strip3" BREAK
		CASE HASH("v_trailer")			RETURN "v_trailer" BREAK
		CASE HASH("v_Trevors")			RETURN "v_Trevors" BREAK
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL GET_PLAYER_PED_HEADING_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
	//	VECTOR &vCreateCoords, 
		FLOAT &fCreateHead, TEXT_LABEL_31 &tRoom)
	
	VECTOR vTempCoords
	
	SWITCH eScene
		CASE PR_SCENE_DEAD
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDead
			sDead = "dead eScene for player scene position: "
			sDead += Get_String_From_Ped_Request_Scene_Enum(eScene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sDead)PRINTNL()
			SCRIPT_ASSERT(sDead)
			#ENDIF
			
			RETURN FALSE
		BREAK
		CASE PR_SCENE_HOSPITAL
			
			HOSPITAL_NAME_ENUM eSelectedHospital
			GetSelectedHospital(eSelectedHospital)
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> GET_PLAYER_PED_HEADING_FOR_SCENE event ")
			PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eScene))
			PRINTSTRING(" has hospital \"")
			PRINTSTRING(Get_Hospital_Respawn_Name(eSelectedHospital))
			PRINTSTRING("\"")
			PRINTNL()
			#ENDIF
			
			IF eSelectedHospital < NUMBER_OF_HOSPITAL_LOCATIONS
//				vCreateCoords	= g_sHospitals[eSelectedHospital].vSpawnCoords
				fCreateHead	= g_sHospitals[eSelectedHospital].fSpawnHeading
				tRoom	= ""
				RETURN TRUE
			ELSE
//				vCreateCoords	= g_sHospitals[0].vSpawnCoords
				fCreateHead	= g_sHospitals[0].fSpawnHeading
				tRoom	= ""
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_M_OVERRIDE
			/*vCreateCoords	= g_sOverrideScene[CHAR_MICHAEL].vCreateCoords*/
			fCreateHead	= g_sOverrideScene[CHAR_MICHAEL].fCreateHead
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_OVERRIDE
			/*vCreateCoords	= g_sOverrideScene[CHAR_FRANKLIN].vCreateCoords*/
			fCreateHead	= g_sOverrideScene[CHAR_FRANKLIN].fCreateHead
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_OVERRIDE
			/*vCreateCoords	= g_sOverrideScene[CHAR_TREVOR].vCreateCoords*/
			fCreateHead	= g_sOverrideScene[CHAR_TREVOR].fCreateHead
			tRoom = ""
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_DEFAULT
			#if USE_CLF_DLC
				fCreateHead	= g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[CHAR_MICHAEL]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_MICHAEL])
			#endif
			#if USE_NRM_DLC	
				fCreateHead	= g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[CHAR_MICHAEL]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_MICHAEL])
			#endif	
			#if not USE_NRM_DLC
			#if not USE_CLF_DLC
				fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_MICHAEL]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_MICHAEL])
			#endif
			#endif
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_DEFAULT
			#if USE_CLF_DLC
				fCreateHead	= g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[CHAR_FRANKLIN]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_FRANKLIN])
			#endif
			#if USE_NRM_DLC	
				fCreateHead	= g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[CHAR_NRM_JIMMY]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_NRM_JIMMY])
			#endif	
			#if not USE_NRM_DLC
			#if not USE_CLF_DLC
				fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_FRANKLIN]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_FRANKLIN])
			#endif
			#endif
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DEFAULT
			#if USE_CLF_DLC
				fCreateHead	= g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[CHAR_TREVOR]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_TREVOR])
			#endif
			#if USE_NRM_DLC	
				fCreateHead	= g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[CHAR_NRM_TRACEY]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_NRM_TRACEY])
			#endif	
			#if not USE_NRM_DLC
			#if not USE_CLF_DLC
				fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_TREVOR]
				tRoom = GET_ROOM_NAME_FOR_KEY(g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_TREVOR])
			#endif
			#endif
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = "v_strip3"
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_PHONECALL_ARM3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ma_ARM3
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1		RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM1		RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_Fa_PHONECALL_ARM3, fCreateHead, tRoom) BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3		RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_Fa_STRIPCLUB_ARM3, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM3		RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_Fa_PHONECALL_ARM3, fCreateHead, tRoom) BREAK
//		CASE PR_SCENE_Fa_FAMILY1
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Fa_FAMILY3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FBI2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FAMILY1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI4intro
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI4intro
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI4
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI5
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI5
			/*0,0,0*/ 	fCreateHead = 0	tRoom = ""
			RETURN FALSE	BREAK
//		CASE PR_SCENE_Ma_FAMILY4_a
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ma_FAMILY4_b
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Ta_FAMILY4
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALEC
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY1
			RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_F_TRAFFIC_a, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_Fa_AGENCYprep1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY3B
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ta_CARSTEAL1
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Fa_CARSTEAL1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI2
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FBI4
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_DOCKS2B
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FAMILY6
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FINALEprepD
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ma_FAMILY6
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_MARTIN1
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ma_TREVOR3
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_TREVOR3
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		
		
		CASE PR_SCENE_Ma_FRANKLIN2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FRANKLIN2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI1end
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		
//		CASE PR_SCENE_Ma_MARTIN1
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ta_MARTIN1
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Fa_RURAL2A
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_RURAL2A
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_RC_MRSP2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = "v_trailer"	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_RURAL1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1a
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_FTa_FRANKLIN1b
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1c
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1d
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1e
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ma_FRANKLIN2
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ta_FRANKLIN2
//			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
//				tRoom = ""	RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Ma_EXILE2
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_EXILE2
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_EXILE3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_EXILE3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M4_WASHFACE
			vTempCoords = <<-803.7340, 168.1480, 76.3542>>			
			fCreateHead= 105.0000
			tRoom = "v_michael"	RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_MICHAEL3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_MICHAEL3
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_DOCKS2A 
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_DOCKS2A
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALE1
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FINALE1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL4
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALE2intro
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALE2intro
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_DOCKS2B
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALE1
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY3A
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALE2A
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FINALE2A
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALE2B
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALE2B
			IF GetLastKnownPedInfoPostMission(eScene, vTempCoords, fCreateHead)
				tRoom = ""	RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALEA
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_FINALEB
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Ma_FINALEC
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_FINALEC
			/*0,0,0*/ 	fCreateHead = 0
			tRoom = ""	RETURN FALSE	BREAK

		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
			//fCreateHead = -150.0		//#1295948
			fCreateHead = 122.6900
			tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_T_JERKOFF			/*1971.1860, 3818.9680, 33.4287*/ 		fCreateHead = -60.3100	tRoom = "v_trailer"	RETURN TRUE BREAK
		#ENDIF
		CASE PR_SCENE_T_HEADINSINK		
			fCreateHead = 41.6540-180
			tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M_MD_FBI2			/*1424.2590, -1813.7014, 67.8935*/ 		fCreateHead = -172.697	tRoom = ""	RETURN TRUE BREAK			
		CASE PR_SCENE_F_MD_FRANKLIN2
			/*1377.4924, -2327.0713, 60.1910*/ 
			fCreateHead = 181.8927-180
			
			tRoom = ""	RETURN TRUE BREAK

		CASE PR_SCENE_M2_BEDROOM		/*-814.2460, 181.2640, 75.7407*/ 		fCreateHead = -158.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b	/*-813.7660, 181.0540, 76.7504*/ 		fCreateHead = -152.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a
			fCreateHead = 20.3530
			tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_b	fCreateHead = -48.5300+180.0			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF	/*-1313.7480, 121.4050, 56.6578*/ 		fCreateHead = -45.0000	tRoom = ""	RETURN TRUE BREAK
		
		CASE PR_SCENE_M4_WAKEUPSCREAM	/*-812.9260, 181.6140, 76.7408*/ 		fCreateHead = -113.7480	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED	/*-813.1960, 181.7640, 76.7407*/ 		fCreateHead = -173.7480	tRoom = "v_michael"	RETURN TRUE BREAK
//		CASE PR_SCENE_M4_HOUSEBED_b		/*-803.0300, 172.7300, 72.8400*/ 		fCreateHead = -102.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSEBED		fCreateHead = 32.7938			tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M4_WATCHINGTV		/*-802.3999, 172.4400, 72.8447*/ 		fCreateHead = -56.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_KIDS_TV		fCreateHead = 13.0000			tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_a		/*-780.694, 187.325, 72.812-1+1*/ 		fCreateHead = 166.320	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_b
			/*-1322.6899, 343.8200, 63.0790+1*/ 	fCreateHead = 21.0000
			/*-1353.3110, 355.9345, 64.0496*/ 		fCreateHead = -132.0000
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_a		/*-826.5596, 155.8342, 68.3283*/ 			fCreateHead = -84.8108		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b		fCreateHead = -90.5046	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_a			/*-1210.3170, -955.7397, 1.6553*/ 		fCreateHead = 105.0795	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_b			/*-848.0614, 855.9160, 202.5614*/ 		fCreateHead = -54.3470	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_c			/*-1268.6400, -711.4000, 22.4619*/ 		fCreateHead = 117.0000	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_LUNCH_a		fCreateHead = -36.000		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_M7_LUNCH_b		fCreateHead = -36.000		tRoom = ""	RETURN TRUE BREAK	//fCreateHead = -94.9990
		CASE PR_SCENE_M4_EXITRESTAURANT	/*394.6800, 176.8100, 103.8401*/ 		fCreateHead = 70.0000	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M4_LUNCH_b
			/*-1368.0250, 54.7852, 52.7046*/ 
			fCreateHead = 34.6210
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M4_CINEMA		fCreateHead = -45.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR
			/*-613.7560, -211.7579, 36.1054+1>> 		+ <<0.0203, 0.0260, -0.0069*/ 
			fCreateHead = -150.6148									+ 0.0095
			

			/*vCreateCoords += <<0.0164, 0.0261, -0.0009*/ 
			fCreateHead += 0.0004

			/*vCreateCoords += <<0.0168, 0.0264, 0.0011*/ 
			fCreateHead += 0.0015
			
			/*vCreateCoords += <<0.0173, 0.0268, 0.0011*/ 
			fCreateHead += 0.0002
			
			/*vCreateCoords += <<0.0179, 0.0271, 0.0011*/ 
			fCreateHead += -0.0009			
			
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a	/*-1365.2017, -563.9809, 29.6000*/ 		fCreateHead = -57.0000	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b	fCreateHead = 84.6073	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a
			/*166.4449, -211.2980, 53.0941*/ 
			fCreateHead = 249.0753
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b	/*401.7279, 308.2417, 102.5000*/ 		fCreateHead = 69.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_PIER_a			/*-1731.9399, -1125.1300, 12.0176+1*/ 	fCreateHead = 143.4931		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_PIER_b			/*-1927.7800, -579.0700, 11.1705*/ 		fCreateHead = 123.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_a		/*-464.22, -1592.98, 38.73*/ 			fCreateHead = 168.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_b		/*-1744.1995, -625.3162, 9.8308*/ 		fCreateHead = 63.4995		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_c		/*-1426.9100, -39.0000, 51.8742*/ 		fCreateHead = -159.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_a		/*260.9800, 1117.8101, 220.1383*/ 		fCreateHead = -6.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b		/*-1819.5800, -677.5900, 10.4119*/ 		fCreateHead = 99.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_a		/*-95.550, -415.100, 35.6750*/ 		fCreateHead = 133.000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_b		/*-1292.6710, -697.2887, 24.2705*/ 		fCreateHead = 33.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_a	/*814.9800, 1270.0100, 360.4754*/ 		fCreateHead = -162.3110		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b	/*-1668.2600, 488.3000, 128.8760*/ 		fCreateHead = 172.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a	/*667.7000, 3503.7000, 33.9937*/ 		fCreateHead = -59.2500		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b	fCreateHead = 82.2540		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c	/*100.7771, 3365.1331, 34.5265*/ 		fCreateHead = -152.9650		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d	/*2445.2058, 3800.6694, 40.0793*/ 		fCreateHead = -10.0990		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e	/*1775.4447, 4584.7432, 37.6512*/ 		fCreateHead = 158.5974		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_DRIVING_a
			/*
			VECTOR vDriveM2AJumpOffset
			Get_Vector_From_DebugJumpAngle(GET_RANDOM_FLOAT_IN_RANGE(0, 360), 125, vDriveM2AJumpOffset)
			
			/*GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY)+vDriveM2AJumpOffset
			fCreateHead = GET_HEADING_FROM_VECTOR_2D(-vDriveM2AJumpOffset.x, -vDriveM2AJumpOffset.y)
			*/
			
			/*-760.5784, 229.5830, 74.6747*/ 
			fCreateHead = 99.180	//72.1803
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DRIVING_b
			/*
			VECTOR vDriveM2BJumpOffset
			Get_Vector_From_DebugJumpAngle(GET_RANDOM_FLOAT_IN_RANGE(0, 360), 125, vDriveM2BJumpOffset)
			
			/*GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_LESTER)+vDriveM2BJumpOffset
			fCreateHead = GET_HEADING_FROM_VECTOR_2D(-vDriveM2BJumpOffset.x, -vDriveM2BJumpOffset.y)
			*/
			
			/*1316.4146, -1599.3450, 51.3924*/ 
			fCreateHead = 218.4774
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_DRIVING_a		fCreateHead = 125.6193		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_b		/*-483.2213, 5876.3921, 33.0000*/ 		fCreateHead = 142.3730		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_c		/*-180.0459, 6464.7988, 30.2000*/ 		fCreateHead = -34.8780		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_d		/*1663.8590, 4876.2842, 41.6000*/ 		fCreateHead = -172.4190		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_e		/*2156.0642, 3253.6323, 46.9000*/ 		fCreateHead = -107.4390		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_f		fCreateHead = 157.311		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_g		/*2543.8311, 2618.3198, 37.5000*/ 		fCreateHead = -68.8120		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_h		fCreateHead = 4.6930		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_RONBORING		/*1978.0699, 3819.5640, 32.4290*/ 		fCreateHead = 78.6500		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_PHARMACY 		/*68.7900, -1561.2699, 29.4564*/ 		fCreateHead = 8.0000		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_M4_DOORSTUMBLE	/*554.0200, 141.8630, 98.8955*/ 		fCreateHead = 67.5000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_a		/*-511.7300, -21.8700, 45.6100*/ 		fCreateHead = 69.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_b		/*	-628.800, 242.463, 81.9*/ 			fCreateHead = 0.000			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_c		/*-834.5300, -350.7100, 38.6537*/ 		fCreateHead = -74.7818		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_a		/*-1073.1270, -1538.8320, 4.1100*/ 		fCreateHead = -48.3600		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_b		/*123.0931, 649.6752, 207.7751*/ 		fCreateHead = 144.1780		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_c		/*-820.9000, 85.3000, 51.9813*/ 		fCreateHead = 288.0000		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_a			/*-1209.1970,-824.5692, 15.28*/ 		fCreateHead = -46.291		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_b			/*-429.8782, -24.0212, 46.2039*/ 		fCreateHead = -88.1930		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_MARINA			/*-831.3530, -1358.7480, 4.9732*/ 		fCreateHead = 101.5000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE	/*-812.3460, 179.8700, 72.1592*/ 		fCreateHead = 99.7200		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M4_PARKEDBEACH	/*-2015.6801, -495.4000, 11.7326*/ 		fCreateHead = 120.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_HOOKERMOTEL		fCreateHead =  -3.0000		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_M_HOOKERCAR		/*851.6744, -1198.7061, 25.0325*/ 		fCreateHead = -88.6774		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_MORNING_a		fCreateHead = -158.0894		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_M6_MORNING_b		/*1974.1980, 3820.1880, 32.4266*/ 		fCreateHead = -142.8420		tRoom = "v_Trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_CARSLEEP		/*669.0789, 3502.1499, 34.1100*/ 		fCreateHead = -76.3681		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSETV_a		fCreateHead = 30.0000+180.0		tRoom = "v_trailer"	RETURN TRUE BREAK
//		CASE PR_SCENE_M6_HOUSETV_b		/*1969.7531, 3815.7180, 32.4000*/ 		fCreateHead = -138.8460		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_SUNBATHING		fCreateHead = -80.6000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRINKINGBEER	fCreateHead = -9.1673		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_ONPHONE		fCreateHead = -86.0894		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_DEPRESSED		/*1974.3120, 3821.1001, 32.8864*/ 		fCreateHead = -161.0894		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_BOATING		/*1940.0519, 4018.8535, 28.9009*/ 		fCreateHead = 226.5579-270 	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M6_LIQUORSTORE	fCreateHead = -33.1280		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_RESTAURANT		/*-115.9200, 363.5000, 112.8857*/ 		fCreateHead = -6.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS	fCreateHead = 72.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a	fCreateHead = -176.2500		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b	/*-718.8735, 256.4936, 79.8259*/ 		fCreateHead = -147.1920		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_WIFETENNIS		/*-770.6851, 157.8133, 67.5042*/ 		fCreateHead = 59.0820		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_ROUNDTABLE		/*-796.7593, 180.4725, 71.8266*/ 		fCreateHead = 26.0870		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M7_REJECTENTRY	fCreateHead = 37.2700		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_HOOKERS		/*533.1877, 109.0133, 96.4624*/ 		fCreateHead = -13.8153		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITBARBER		/*-823.2000, -187.0830, 37.7753*/ 		fCreateHead = -62.5000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP	/*-715.6204, -155.5691, 37.4023*/ 		fCreateHead = 119.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_FAKEYOGA		fCreateHead = 86.3776		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_COFFEE			/*-1367.3500, -208.8400, 44.4420*/ 		fCreateHead = 117.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_GETSREADY		/*-812.8960, 181.1140, 76.7233*/ 		fCreateHead = -164.0000		tRoom = "v_michael"	RETURN TRUE BREAK
//		CASE PR_SCENE_M7_PARKEDHILLS	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_M_PARKEDHILLS_a, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_M7_READSCRIPT		/*-781.2640, 187.1150, 72.8425*/ 		fCreateHead = 88.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO	/*-1135.5466, -450.7346, 35.5199*/ 		fCreateHead = -144.6220		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_TALKTOGUARD	fCreateHead = -61.2262		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOT_JIMMY		/*-1180.0620, -498.2454, 35.5670*/ 		fCreateHead = -22.3200		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_TV
			VECTOR vInitOffset
			FLOAT fInitHead
			
			IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, FE_M7_SON_watching_TV_with_tracey,
					vInitOffset, fInitHead)
				/*vInitOffset + << -812.0607, 179.5117, 71.1531*/  //+ <<0,0,1*/ 
				fCreateHead = fInitHead + 222.8314
				tRoom = "v_michael"
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING	fCreateHead = 112.8410		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA	fCreateHead = -103.8158+180		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			fCreateHead = -28.0926
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY	/*-817.7500, 170.0200, 70.4872*/ 		fCreateHead = -0.0301		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
			/*-248.9653, -81.0186, 48.6174*/ 
			fCreateHead = -30.1850
			
			/*vCreateCoords += <<0.0091, 0.0273, 0.0134*/ 
			fCreateHead += 0.0030
			
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_M_S_FAMILY4		fCreateHead	= 14.98			tRoom	= "v_michael"			RETURN TRUE		BREAK
	
	ENDSWITCH
	SWITCH eScene
		CASE PR_SCENE_F0_SH_ASLEEP		/*-17.2168, -1441.2240, 30.1015*/ 		fCreateHead = -179.6530		tRoom = "v_franklins"		RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_ASLEEP		fCreateHead = -147.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_NAPPING		/*-0.1090, 524.3119, 170.3068*/ 		fCreateHead = -81.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_GETTINGREADY	fCreateHead = -95.4016		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_READING		/*-17.2672, -1441.1541, 30.1015*/ 		fCreateHead =-16.0627+180	tRoom = "v_franklins"		RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_READING		/*1.7600, 525.9200, 173.63*/ 			fCreateHead = 129.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a	/*-17.4073, -1439.4010, 31.1023*/ 		fCreateHead = -86.6130		tRoom = "v_franklins"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_b	fCreateHead = -63.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_PUSHUP		/*15.3608, 523.6475, 169.2282+1*/ 		fCreateHead = 111.6880		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_POOLSIDE_a		/*-11.5281, 512.3040, 174.5978*/ 		fCreateHead = 143.7974		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_POOLSIDE_b		/*20.9569, 521.8147, 170.1977*/ 		fCreateHead = 143.7920		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANINGAPT	/*-1.5790, 535.2489, 175.3424*/ 		fCreateHead = 4.6834		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONCELL			/*3.8524, 525.7295, 174.6283*/ 			fCreateHead = -108.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_SNACKING		/*-8.8600, 515.8400, 174.6280*/ 		fCreateHead = 69.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONLAPTOP		/*-6.9799, 524.9367, 174.9997*/ 		fCreateHead = -172.2207		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_IRONING		/*1.4483, 527.5843, 170.0730*/ 			fCreateHead = 0.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
//		CASE PR_SCENE_F0_WATCHINGTV		/*-9.6372, -1439.4460, 31.1015*/ 		fCreateHead = 179.0318		tRoom = "v_franklins"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_WATCHINGTV		/*1.8291, 526.7450, 174.6267*/ 			fCreateHead = -12.5158		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT	fCreateHead = -1.5000		tRoom = "v_franklins"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_NEWHOUSE		fCreateHead = 27.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC		fCreateHead = 107.9810		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_a		/*-1175.2980, -1573.6920, 4.3599*/ 		fCreateHead = 172.9187		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_b		/*-1153.5110, -1371.6520, 4.0523*/ 		fCreateHead = -67.6080		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_c		/*-1162.9871, -1427.2640, 3.6058*/ 		fCreateHead = 74.1158		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F0_GARBAGE		fCreateHead = 1.0411		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_GARBAGE		/*14.3790, 544.1280, 175.0021*/ 		fCreateHead = -152.2030		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_THROW_CUP		/*2.8895, -1607.2864, 28.2858-0.5+1.5*/ fCreateHead = 310.879-180	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND	/*2.8895, -1607.2864, 29.2903*/ 		fCreateHead = 130.8790		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_GYM				/*-1244.8879, -1613.6560, 4.1295*/ 		fCreateHead = 35.6040		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F0_WALKCHOP		fCreateHead = -93.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F0_PLAYCHOP		/*-15.0259, -1422.9363, 30.6917*/ 		fCreateHead = -119.3944		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_F1_WALKCHOP		/*15.3388, 523.7823, 170.1958*/ 		fCreateHead = 120.7268		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_PLAYCHOP		/*15.3678, 523.7120, 170.2095*/ 		fCreateHead = 121.9322		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_a		/*154.0731, 765.5721, 209.6901*/ 		fCreateHead = -36.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_b		/*-268.1390, 415.2881, 109.7258*/ 		fCreateHead = -95.5880		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_a		/*-464.2200, -1592.9800, 38.7300*/ 		fCreateHead = 168.0000 		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_b		/*31.90, -1483.30, 29.26*/ 				fCreateHead = 230.78		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_c		/*208.9683, 222.0408, 104.6000*/ 		fCreateHead = 165.7751		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F0_BIKE			/*-24.5203, -1436.2000, 30.1544*/ 		fCreateHead = -179.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F0_CLEANCAR
			/*-23.8762, -1444.5953, 30.6542*/ 
			fCreateHead = 1.2709
			tRoom = ""	RETURN TRUE BREAK
			
		CASE PR_SCENE_F1_BIKE			fCreateHead = 84.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR		fCreateHead = -117.0300		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F1_BYETAXI		fCreateHead = -49.0324		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_c			fCreateHead = -45.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_d			fCreateHead = 153.0000		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMGRAFF 		/*-86.0010, -1456.8710, 33.0587*/ 		fCreateHead = 90.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_CLUB
			fCreateHead = 84.9600		//fCreateHead = 3.9600
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE	/*480.9113, -1316.3550, 29.2018*/ 		fCreateHead = -59.3848		tRoom = "v_chopshop"	RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPEHANDS	/*473.3613, -1309.9950, 29.2326*/ 		fCreateHead = 43.8200		tRoom = "v_chopshop"	RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_F_CS_CHECKSHOE, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_F_BAR_a_01		/*28.9860, -1351.4120, 29.3437*/ 		fCreateHead = 160.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_b_01		/*-379.1773, 220.9259, 84.1440*/ 		fCreateHead = -14.7490		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_c_02		/*131.5816, -1303.5580, 29.1592*/ 		fCreateHead = -150.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_d_02		/*792.1553, -735.5871, 27.5905*/ 		fCreateHead = 96.0116		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_e_01		/*-297.4081, -1332.3430, 31.3057*/ 		fCreateHead = -43.6661		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_F_TAUNT			/*-44.2374, -1512.5023, 29.8432*/ 		fCreateHead = -115.4124		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1								//alley behind franklin's house
			/*vCreateCoords =	<<-9.4, -1415.3, 28.32*/ 
			fCreateHead = -70.4124
			tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2								//outside courthouse
//			/*298.0812, -1617.1174, 29.5333*/ 
//			fCreateHead = 130.4238
//			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3								//rec center next to basketball courts
			/*-242.0927, -1538.1809, 30.5334*/ 
			fCreateHead = -12.0000
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5								//street corner gang area
			/*-18.8892, -1823.9120, 25.8485*/ 
			fCreateHead = -117.3560
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT							//alley in gang area
			/*192.751, -1672.653, 28.8033*/ 
			fCreateHead = -83.8
			tRoom = ""	RETURN TRUE BREAK
			
		CASE PR_SCENE_F_S_EXILE2
			/*-2689.2244, 2368.0752, 15.7681*/ 
			fCreateHead = 350.3382
			tRoom = ""	RETURN TRUE BREAK
			
		CASE PR_SCENE_F_S_AGENCY_2A_a			fCreateHead = 109.0206			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_b			fCreateHead = 109.0206			tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_F_S_FBI1end
//			/*1601.751,-1944.011,100.732*/ 
//			fCreateHead = RAD_TO_DEG(1.25 * CONST_PI)-180
//			tRoom = ""	RETURN TRUE BREAK
		
//		CASE PR_SCENE_F_S_AGENCY_2B
//			/*144.4447, -933.0371, 28.7486*/ 
//			fCreateHead = -17.2431
//			tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_F_S_AGENCY_2C
//			/*217.4798, 363.9874, 105.1759*/ 
//			fCreateHead = 198.0893
//			tRoom = ""	RETURN TRUE BREAK
		
//		CASE PR_SCENE_T_STRIPCLUB_a		/*110.1095, -1291.8535, 27.2609*/ 		fCreateHead = 30.0			tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE	/*116.9369, -1287.7040, 28.2979*/ 		fCreateHead = -112.0000		tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_BAR			/*126.8211, -1283.7660, 29.2740*/ 		fCreateHead = 114.0000		tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_CHASE		/*127.9570, -1298.5129, 29.4270*/ 		fCreateHead = 30.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_STRIPCLUB_out	/*130.2769, -1300.8740, 29.1559*/ 		fCreateHead = -164.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_ESCORTED_OUT	/*-55.8087, 358.255, 113.0610*/ 		fCreateHead = -122.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_b	fCreateHead =  -4.1240		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_c	/*-175.4296, 6428.7500, 28.9697*/ 		fCreateHead = 108.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHATEAU_d	/*-1654.9370, -147.5126, 57.5578*/ 		fCreateHead = 13.7207		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_SMOKEMETH		/*1972.8101, 3818.2729, 32.0050*/ 		fCreateHead = 27.7460		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD	/*433.8850, -1462.4780, 28.2735*/ 		fCreateHead = 18.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_THROW_FOOD		/*433.8850, -1462.4780, 28.2804*/ 		fCreateHead = -51.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		/*-1199.5500, -1569.6880, 4.6120*/ 		fCreateHead = -165.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS	fCreateHead = 133.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	/*285.9300, 182.1800, 103.3496*/ 		fCreateHead = 10.7700		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN	/*292.1700, 191.0900, 103.3496+1*/ 		fCreateHead = 138.0000-180	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_a			/*288.0774, -3201.8811, 5.8080*/ 		fCreateHead = 87.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_b			fCreateHead = -42.8529		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_c			/*-46.1798, -1474.1639, 31.0453*/ 		fCreateHead = 2.6497		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_d			fCreateHead = 135.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_LINGERIE		/*154.7300, -219.2100, 54.3030*/ 		fCreateHead = -40.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_FUNERAL		/*411.6250, -1488.9890, 30.1244*/ 		fCreateHead = 30.2400		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_DUMPSTER		/*488.0162, -1342.3940, 29.4108*/ 		fCreateHead = -90.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRASH_b	fCreateHead = -144.2740		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEBEACH	fCreateHead = 68.8227		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEBARN		fCreateHead = 56.2037		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRAIN	fCreateHead = 33.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEROOFTOP	/*418.6078, -788.4689, 43.5311*/ 		fCreateHead = -106.6605		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN	fCreateHead = -102.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK	fCreateHead = 26.3597		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_ALLEYDRUNK	/*107.0137, -1316.0350, 28.2084*/ 		fCreateHead = -83.3175		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT	/*-118.1968, -442.9148, 35.2820*/ 		fCreateHead = -153.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PARK_b		/*-1858.9570, 2071.2300, 140.3656*/ 	fCreateHead = 9.0000		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_T_CN_PARK_c		/*-1874.2850, 2033.0222, 138.6292*/ 	fCreateHead = -98.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_POLICE_a		/*895.3365, -1199.5375, 45.9852*/ 		fCreateHead = 277.613-360	tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_POLICE_b		/*1881.9980, 2225.0313, 54.0000*/ 		fCreateHead = -4.7459		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_POLICE_c		/*1039.9524, 2687.9521, 39.0000*/ 		fCreateHead = -98.5600		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE	/*642.6800, -1001.2700, 36.8997*/ 		fCreateHead = -33.7700		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_GARDEN	/*-145.8739, 868.3813, 231.6100*/ 		fCreateHead = 155.6800		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_ISLAND	/*2789.8450, -1453.7310, 0.5519*/ 		fCreateHead = -49.5600		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASECAR_a	/*609.8130, -977.7325, 9.5456*/ 		fCreateHead = -5.8739		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b	/*-930.9908, 2873.0781, 22.1187*/ 		fCreateHead = 70.1627		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE	/*-3015.0432, 318.9735, 13.7263*/ 		fCreateHead = 158.9790		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER	/*1307.9390, -1153.0182, 50.5614*/ 		fCreateHead = -67.1851		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	/*48.1743, -2057.1294, 18.3524*/ 		fCreateHead = 47.0540		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_a		/*-1242.68, -1105.15, 7.10*/ 			fCreateHead = 120.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_b		/*-1667.1479, -974.7168, 6.4790*/ 		fCreateHead = 171.2530		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_c		/*-301.4778, 6250.8999, 30.5054*/ 		fCreateHead = 10.2470		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_YELLATDOORMAN	/*-724.2600, -1307.0500, 5.0602*/ 		fCreateHead = -32.4880		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b	/*-1280.0540, 303.9235, 63.9553*/ 		fCreateHead = -29.0930		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTCASINO		/*924.1288, 48.0048, 79.7644*/ 			fCreateHead = 229.6085		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE	/*-1273.6899, -1195.0100, 5.0369*/ 		fCreateHead = -150.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE	/*-888.4500, -853.1100, 19.5602*/ 		fCreateHead = -81.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_UNDERPIER		/*-1696.1400, -1073.2000, 0.6898*/ 		fCreateHead = 12.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_KONEIGHBOUR		/*-1155.9570, -1521.6860, 4.3519*/ 		fCreateHead = -90.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_SCARETRAMP		/*-565.3700, -1258.0200, 13.8618*/ 		fCreateHead = -171.0000		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_DRUNKHOWLING	/*439.8337, -228.2973, 55.9727*/ 		fCreateHead = -11.5018		tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING	/*118.4869, -1286.4139, 28.2610*/ 		fCreateHead = -129.0000		tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	fCreateHead = -147.0000		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A
			fCreateHead = 28.7271
			tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B
			fCreateHead = 28.7271
			tRoom = "v_Trevors"	RETURN TRUE BREAK
			
		CASE PR_SCENE_T_FLOYDSPOON_B2	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_T_FLOYDSPOON_B, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A2	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_T_FLOYDSPOON_A, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A	/*-1158.1331, -1521.3940, 9.6327*/ 		fCreateHead = 34.6610		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_T_FLOYDCRYING_E3, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E1	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_T_FLOYDCRYING_E3, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E2	RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_T_FLOYDCRYING_E3, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E3	fCreateHead = 32.0000		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_BEAR		/*-1146.1270, -1515.5250, 9.6346*/ 		fCreateHead = 116.7420		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_DOLL		/*-1153.5150, -1518.4351, 9.6346*/ 		fCreateHead = 100.4600		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE	/*-1156.4220, -1519.5610, 10.6327*/ 	fCreateHead = 102.0000		tRoom = "v_Trevors"	RETURN TRUE BREAK
		
		CASE PR_SCENE_T6_SMOKECRYSTAL	fCreateHead = -152.0894		tRoom = ""	RETURN TRUE BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP
//			
//			
//			VECTOR vTBlowShitUpOffset
//			FLOAT fTBlowShitUpHead
//			
//			IF PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_blowing_shit_up,
//					vTBlowShitUpOffset, fTBlowShitUpHead)
//				/*vTBlowShitUpOffset + << 1974.6129, 3819.1438, 32.4374*/ //+ <<0,0,1*/ 
//				fCreateHead = fTBlowShitUpHead + 92.6017
//				tRoom = ""
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_T6_EVENING
//			RETURN GET_PLAYER_PED_HEADING_FOR_SCENE(PR_SCENE_M6_EVENING, fCreateHead, tRoom) BREAK
		CASE PR_SCENE_T6_METHLAB		/*1394.2081, 3602.2839, 37.9419*/ 		fCreateHead = 122.5269	tRoom = "v_methlab"	RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING1
			VECTOR vHunt1Offset
			vHunt1Offset = <<-7.4998, 7.4995, -0.5258>>
		
			/*GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING1) + vHunt1Offset*/
			fCreateHead = GET_HEADING_FROM_VECTOR_2D(-vHunt1Offset.x, -vHunt1Offset.y)
			
			
			/*
			#IF IS_DEBUG_BUILD
			/*-572.2186, 5649.2632, 37.4335>>
			VECTOR vHunt1Coord
			vHunt1Coord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING1)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_HUNTING1 vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vHunt1Coord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vHunt1Coord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING2
			VECTOR vHunt2Offset 
			vHunt2Offset = <<10.6345, 0.7246, 1.2508>>
		
			/*GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING2) + vHunt2Offset*/
			fCreateHead = GET_HEADING_FROM_VECTOR_2D(-vHunt2Offset.x, -vHunt2Offset.y)
			
			
			/*
			#IF IS_DEBUG_BUILD
			/*-1574.4655, 4693.4248, 47.6508>>
			VECTOR vHunt2Coord
			vHunt2Coord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING2)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_HUNTING2 vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vHunt2Coord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vHunt2Coord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING3
			VECTOR vHunt3Offset 
			vHunt3Offset = <<-3.4271, -13.6787, -1.4107>>
		
			/*GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING3) + vHunt3Offset*/
			fCreateHead = GET_HEADING_FROM_VECTOR_2D(-vHunt3Offset.x, -vHunt3Offset.y)
			
			
			/*
			#IF IS_DEBUG_BUILD
			/*-1557.3943, 4590.1157, 18.6265>>
			VECTOR vHunt3Coord
			vHunt3Coord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING3)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_HUNTING3 vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vHunt3Coord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vHunt3Coord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T6_TRAF_AIR
			VECTOR vTrafAirOffset
			vTrafAirOffset = <<-19.6582, 7.8960, 0.1334>>
		
			/*GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TRAF_AIR) + vTrafAirOffset*/
			fCreateHead = GET_HEADING_FROM_VECTOR_2D(-vTrafAirOffset.x, -vTrafAirOffset.y)
			
			
			/*
			#IF IS_DEBUG_BUILD
			/*2113.6943, 4792.3535, 40.1971>>
			VECTOR vTrafAirCoord
			vTrafAirCoord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TRAF_AIR)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_TRAF_AIR vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vTrafAirCoord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vTrafAirCoord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			tRoom = ""	RETURN TRUE BREAK
		
//		CASE PR_SCENE_T6_DISPOSEBODY_A
//			/*787.5558, -2614.8931, 51.8703*/ 
//			fCreateHead = 11.6025
//			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T6_DIGGING
			/*2020.2517, 3401.1542, 42.7215*/ 
			fCreateHead = -87.7215
			tRoom = ""	RETURN TRUE BREAK
		
		CASE PR_SCENE_T6_FLUSHESFOOT
			fCreateHead = -145.0000	tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PIER
			fCreateHead = (103.2841+88.7571)/2.0	//110.5931
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T6_LAKE
			fCreateHead = -177.000+180
			tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLYING_PLANE
			fCreateHead = 327.7746
			tRoom = ""	RETURN TRUE BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for player scene heading: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	/*vCreateCoords	= <<0,0,0>>*/ 
	fCreateHead	= 0
	tRoom 	= ""
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL GET_PLAYER_PED_POSITION_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		VECTOR &vCreateCoords, FLOAT &fCreateHead, TEXT_LABEL_31 &tRoom)
	
	IF GET_PLAYER_PED_HEADING_FOR_SCENE(eScene, fCreateHead, tRoom)
	
		SWITCH eScene
			CASE PR_SCENE_DEAD
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 sDead
				sDead = "dead eScene for player scene position: "
				sDead += Get_String_From_Ped_Request_Scene_Enum(eScene)
				
				PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sDead)PRINTNL()
				SCRIPT_ASSERT(sDead)
				#ENDIF
				
				RETURN FALSE
			BREAK
			CASE PR_SCENE_HOSPITAL
			
				HOSPITAL_NAME_ENUM eSelectedHospital
				GetSelectedHospital(eSelectedHospital)
				
				#IF IS_DEBUG_BUILD
				PRINTSTRING("<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTSTRING("> GET_PLAYER_PED_POSITION_FOR_SCENE event ")
				PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eScene))
				PRINTSTRING(" has hospital \"")
				PRINTSTRING(Get_Hospital_Respawn_Name(eSelectedHospital))
				PRINTSTRING("\"")
				PRINTNL()
				#ENDIF
				
				IF eSelectedHospital < NUMBER_OF_HOSPITAL_LOCATIONS
					vCreateCoords	= g_sHospitals[eSelectedHospital].vSpawnCoords
					fCreateHead	= g_sHospitals[eSelectedHospital].fSpawnHeading
					tRoom	= ""
					RETURN TRUE
				ELSE
					vCreateCoords	= g_sHospitals[0].vSpawnCoords
					fCreateHead	= g_sHospitals[0].fSpawnHeading
					tRoom	= ""
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE PR_SCENE_M_OVERRIDE
				vCreateCoords	= g_sOverrideScene[CHAR_MICHAEL].vCreateCoords
				fCreateHead	= g_sOverrideScene[CHAR_MICHAEL].fCreateHead
				tRoom = ""
				RETURN TRUE
			BREAK
			CASE PR_SCENE_F_OVERRIDE
				vCreateCoords	= g_sOverrideScene[CHAR_FRANKLIN].vCreateCoords
				fCreateHead	= g_sOverrideScene[CHAR_FRANKLIN].fCreateHead
				tRoom = ""
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_OVERRIDE
				vCreateCoords	= g_sOverrideScene[CHAR_TREVOR].vCreateCoords
				fCreateHead	= g_sOverrideScene[CHAR_TREVOR].fCreateHead
				tRoom = ""
				RETURN TRUE
			BREAK
			
			CASE PR_SCENE_M_DEFAULT
				vCreateCoords	= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_MICHAEL] + << 0,0,-1 >>
				fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_MICHAEL]
				tRoom = ""
				RETURN TRUE
			BREAK
			CASE PR_SCENE_F_DEFAULT
				vCreateCoords	= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_FRANKLIN] + << 0,0,-1 >>
				fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_FRANKLIN]
				tRoom	= ""
				RETURN TRUE
			BREAK
			CASE PR_SCENE_T_DEFAULT
				vCreateCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_TREVOR] + << 0,0,-1 >>
				fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_TREVOR]
				tRoom	= ""
				RETURN TRUE
			BREAK
		ENDSWITCH
		
		vCreateCoords = g_sPedSceneData[eScene].vCreateCoords
//		fCreateHead = g_sPedSceneData[eScene].fCreateHead
//		tRoom = g_sPedSceneData[eScene].tRoom
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL GET_PLAYER_PED_ROTATION_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		FLOAT fCreateHead, VECTOR &vCreateRot)

	SWITCH eScene
		CASE PR_SCENE_M2_CARSLEEP_a
			vCreateRot = <<9.9861, 0.4192, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b
			vCreateRot = <<1.0500, 1.8900, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M6_CARSLEEP
			vCreateRot = <<-0.2500, -0.5100, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR
			vCreateRot = <<-1.1353, 2.7237, fCreateHead>> + <<-0.1821, -0.3079,0>>
			vCreateRot += <<-0.0028, 0.0461, 0.0>>
			vCreateRot += <<-0.0029, 0.0519, 0.0>>
			vCreateRot += <<-0.0030, 0.0527, 0.0>>
			vCreateRot += <<-0.0024, 0.0551, 0.0>>
			
			RETURN TRUE BREAK
//		CASE PR_SCENE_F0_CLEANCAR
//			vCreateRot = <<0.3266, 4.6872, fCreateHead>>
//			vCreateRot += <<0.1470, 4.6962, 0.0>>
//	
//			RETURN TRUE BREAK
		CASE PR_SCENE_F1_BIKE
			vCreateRot = <<-2.5200, 0.0000, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR
			vCreateRot = <<2.3200, 2.7200, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_F1_BYETAXI
			vCreateRot = <<8.7000, 0.4800, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_c
			vCreateRot = <<2.0100, 0.0000, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_d
			vCreateRot = <<0.0, 0.0000, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_TALKTOGUARD
			vCreateRot = <<0.6300, 1.8700, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA
			vCreateRot = <<-1.1011, 0.0000, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			vCreateRot = <<3.1200, 0.9000, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
			vCreateRot = <<2.4600, 0.5100, fCreateHead>>
			
			vCreateRot += <<-0.3169, 0.0958, 0.0>>
			
			
			RETURN TRUE BREAK
		
		CASE PR_SCENE_M4_PARKEDBEACH
			vCreateRot = <<2.430, -0.0, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_Ma_RURAL1
			vCreateRot = <<-0.7800, -0.4800, fCreateHead>>
			RETURN TRUE BREAK
			
		CASE PR_SCENE_M6_PARKEDHILLS_b
			vCreateRot = <<0.0000, -0.9300, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c
			vCreateRot = <<-1.4700*0.5, 1.8600*0.5, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d
			vCreateRot = <<0.9854, -0.8908, fCreateHead>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e
			vCreateRot = <<-0.4615, -0.3157, fCreateHead>>
			RETURN TRUE BREAK
			
	ENDSWITCH
	
	vCreateRot	= <<0,0,fCreateHead>>
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC BOOL TestSelectedMissionSceneCoords(PED_REQUEST_SCENE_ENUM eReqScene)
	VECTOR vLastKnownCoords
	FLOAT fLastKnownHead
	
	IF GetLastKnownPedInfoPostMission(eReqScene, vLastKnownCoords, fLastKnownHead)
	
		VECTOR vRequestCoord
		FLOAT fCreateHead
		TEXT_LABEL_31 tRoom
		IF GET_PLAYER_PED_POSITION_FOR_SCENE(eReqScene, vRequestCoord, fCreateHead, tRoom)
			IF NOT (fLastKnownHead = fCreateHead)
				PRINTSTRING("differing headings with post mission switch ")
				PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eReqScene))
				PRINTSTRING(": ")
				PRINTFLOAT(fLastKnownHead)
				PRINTSTRING(" <> ")
				PRINTFLOAT(fCreateHead)
				PRINTNL()
				
				SCRIPT_ASSERT("differing headings with post mission switch")
				eReqScene = PR_SCENE_INVALID
				RETURN FALSE
			ENDIF
			IF NOT ARE_VECTORS_EQUAL(vLastKnownCoords, vRequestCoord)
				PRINTSTRING("differing coords with post mission switch ")
				PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eReqScene))
				PRINTSTRING(": ")
				PRINTVECTOR(vLastKnownCoords)
				PRINTSTRING(" <> ")
				PRINTVECTOR(vRequestCoord)
				PRINTNL()
				
				SCRIPT_ASSERT("differing coords with post mission switch")
				eReqScene = PR_SCENE_INVALID
				RETURN FALSE
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF
