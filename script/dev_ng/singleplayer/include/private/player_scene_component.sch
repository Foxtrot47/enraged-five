USING "ped_component_public.sch"
USING "wardrobe_private.sch"
USING "family_private.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_component.sch									//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Checks if the ped component needs to be removed for default switch scene.
/// RETURNS:
///    A suitable replacement if it needs to be removed or returns DUMMY_PED_COMP if it is fine
FUNC PED_COMP_NAME_ENUM IS_Ped_Comp_Name_Enum_Safe_For_Default_Switch(MODEL_NAMES ePedModel, PED_COMP_TYPE_ENUM eType, PED_COMP_NAME_ENUM eItem)
	
	//CPRINTLN(DEBUG_PED_COMP & DEBUG_SWITCH, "IS_Ped_Comp_Name_Enum_Safe_For_Default_Switch: eType= ", eType, "  eItem= ", eItem)
	
	SWITCH ePedModel
		CASE PLAYER_ZERO	//CHAR_MICHAEL
			SWITCH eType
				CASE COMP_TYPE_TORSO
					SWITCH eItem
						CASE TORSO_P0_BED		RETURN TORSO_P0_GREY_SUIT	BREAK
					ENDSWITCH
				BREAK
				CASE COMP_TYPE_LEGS
					SWITCH eItem
						CASE LEGS_P0_BED		RETURN LEGS_P0_GREY_SUIT	BREAK
					ENDSWITCH
				BREAK
				CASE COMP_TYPE_FEET
					SWITCH eItem
						CASE FEET_P0_BED		RETURN FEET_P0_BLACK_SHOES	BREAK
					ENDSWITCH
				BREAK
				CASE COMP_TYPE_OUTFIT
					SWITCH eItem
						CASE OUTFIT_P0_BED		RETURN OUTFIT_P0_DEFAULT	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		BREAK
		CASE PLAYER_ONE		//CHAR_FRANKLIN
			//
			
		BREAK
		CASE PLAYER_TWO		//CHAR_TREVOR
			SWITCH eType
				CASE COMP_TYPE_PROPS
					SWITCH eItem
						CASE PROPS_P2_BEANIE_HAT	RETURN PROPS_HEAD_NONE	BREAK
						CASE PROPS_P2_GLASSES		RETURN PROPS_EYES_NONE	BREAK
					ENDSWITCH
				BREAK
				CASE COMP_TYPE_OUTFIT
					SWITCH eItem
						CASE OUTFIT_P2_UNDERWEAR	RETURN OUTFIT_P2_DEFAULT	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
					
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_SWITCH, "Item is fine") #ENDIF						
	// item is fine
	RETURN DUMMY_PED_COMP
ENDFUNC

#if USE_CLF_DLC
FUNC BOOL SET_PED_COMPONENT_FOR_MISSIONCLF(PED_INDEX pedIndex, enumCharacterList ePedChar = NO_CHARACTER)
	IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive
		CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: NOT isGameflowActive")
		RETURN FALSE
	ENDIF
	
	IF (ePedChar = NO_CHARACTER)
		ePedChar = GET_PLAYER_PED_ENUM(pedIndex)
	ENDIF
	
	IF NOT IS_PLAYER_PED_PLAYABLE(ePedChar)
		RETURN FALSE
	ELSE
		TIMEOFDAY sLastChangedOutfits = GET_TIME_PLAYER_PED_LAST_CHANGED_CLOTHES(ePedChar)
		IF Is_TIMEOFDAY_Valid(sLastChangedOutfits)
			CONST_INT	iCONST_HoursToWaitToChangeClothes	2
			TEXT_LABEL_63 tLastChangedOutfits
			#IF IS_DEBUG_BUILD
			tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(sLastChangedOutfits)
			#ENDIF
			#IF IS_FINAL_BUILD
			tLastChangedOutfits = tLastChangedOutfits
			#ENDIF
			
			IF HasNumOfHoursPassedSincePedTimeStruct(sLastChangedOutfits, iCONST_HoursToWaitToChangeClothes)				
				CPRINTLN(DEBUG_PED_COMP,"SET_PED_COMPONENT_FOR_MISSION: dont change outfits - not enough time has passed since [", GET_PLAYER_PED_STRING(ePedChar), "] clothes changed [", tLastChangedOutfits, "]")
				CPRINTLN(DEBUG_SWITCH,"SET_PED_COMPONENT_FOR_MISSION: dont change outfits - not enough time has passed since [", GET_PLAYER_PED_STRING(ePedChar), "] clothes changed [", tLastChangedOutfits, "]")
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is less than 2hrs [", tLastChangedOutfits, "]")
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is less than 2hrs [", tLastChangedOutfits, "]")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is null")
			CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is null")
		ENDIF
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_CLF_TRAIN)
		IF (ePedChar = CHAR_TREVOR	)
			BOOL bSetPedComponentForMission = FALSE
			IF bSetPedComponentForMission
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_TSHIRT_1)
				AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_JEANS_1)
				AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_FEET, FEET_P2_BARE_FEET)
					CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: TORSO_P1_WHITE_SHIRT, LEGS_P1_BEIGE_SHORTS, COMP_TYPE_FEET - FRANKLIN_0")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: none")
	RETURN FALSE
ENDFUNC
#ENDIF
#if USE_NRM_DLC
FUNC BOOL SET_PED_COMPONENT_FOR_MISSIONNRM(PED_INDEX pedIndex, enumCharacterList ePedChar = NO_CHARACTER)

	IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive
		CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: NOT isGameflowActive")
		RETURN FALSE
	ENDIF
	
	IF (ePedChar = NO_CHARACTER)
		ePedChar = GET_PLAYER_PED_ENUM(pedIndex)
	ENDIF
	
	IF NOT IS_PLAYER_PED_PLAYABLE(ePedChar)
		RETURN FALSE
	ELSE
		TIMEOFDAY sLastChangedOutfits = GET_TIME_PLAYER_PED_LAST_CHANGED_CLOTHES(ePedChar)
		IF Is_TIMEOFDAY_Valid(sLastChangedOutfits)
			CONST_INT	iCONST_HoursToWaitToChangeClothes	2
			TEXT_LABEL_63 tLastChangedOutfits
			#IF IS_DEBUG_BUILD
			tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(sLastChangedOutfits)
			#ENDIF
			#IF IS_FINAL_BUILD
			tLastChangedOutfits = tLastChangedOutfits
			#ENDIF
			
			IF HasNumOfHoursPassedSincePedTimeStruct(sLastChangedOutfits, iCONST_HoursToWaitToChangeClothes)				
				CPRINTLN(DEBUG_PED_COMP,"SET_PED_COMPONENT_FOR_MISSION: dont change outfits - not enough time has passed since [", GET_PLAYER_PED_STRING(ePedChar), "] clothes changed [", tLastChangedOutfits, "]")
				CPRINTLN(DEBUG_SWITCH,"SET_PED_COMPONENT_FOR_MISSION: dont change outfits - not enough time has passed since [", GET_PLAYER_PED_STRING(ePedChar), "] clothes changed [", tLastChangedOutfits, "]")
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is less than 2hrs [", tLastChangedOutfits, "]")
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is less than 2hrs [", tLastChangedOutfits, "]")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is null")
			CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is null")
		ENDIF
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_NRM_SUR_START)	
		IF (ePedChar = CHAR_MICHAEL)
			BOOL bSetPedComponentForMission = FALSE
			IF bSetPedComponentForMission
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P0_HEAVY_JACKET)
				AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P0_JEANS)
				AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_FEET, FEET_P0_BLACK_SHOES)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: none")
	RETURN FALSE
ENDFUNC
#ENDIF
FUNC BOOL SET_PED_COMPONENT_FOR_MISSION(PED_INDEX pedIndex, enumCharacterList ePedChar = NO_CHARACTER)
#if USE_CLF_DLC
 return SET_PED_COMPONENT_FOR_MISSIONCLF(pedIndex, ePedChar)
#ENDIF
#if USE_NRM_DLC
 return SET_PED_COMPONENT_FOR_MISSIONNRM(pedIndex, ePedChar)
#ENDIF

#if not USE_CLF_DLC
#if not USE_NRM_DLC	
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: NOT isGameflowActive")
		RETURN FALSE
	ENDIF
	
	IF (ePedChar = NO_CHARACTER)
		ePedChar = GET_PLAYER_PED_ENUM(pedIndex)
	ENDIF
	
	IF NOT IS_PLAYER_PED_PLAYABLE(ePedChar)
		RETURN FALSE
	ELSE
		TIMEOFDAY sLastChangedOutfits = GET_TIME_PLAYER_PED_LAST_CHANGED_CLOTHES(ePedChar)
		IF Is_TIMEOFDAY_Valid(sLastChangedOutfits)
			CONST_INT	iCONST_HoursToWaitToChangeClothes	2
			TEXT_LABEL_63 tLastChangedOutfits
			#IF IS_DEBUG_BUILD
			tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(sLastChangedOutfits)
			#ENDIF
			#IF IS_FINAL_BUILD
			tLastChangedOutfits = tLastChangedOutfits
			#ENDIF
			
			IF HasNumOfHoursPassedSincePedTimeStruct(sLastChangedOutfits, iCONST_HoursToWaitToChangeClothes)				
				CPRINTLN(DEBUG_PED_COMP,"SET_PED_COMPONENT_FOR_MISSION: dont change outfits - not enough time has passed since [", GET_PLAYER_PED_STRING(ePedChar), "] clothes changed [", tLastChangedOutfits, "]")
				CPRINTLN(DEBUG_SWITCH,"SET_PED_COMPONENT_FOR_MISSION: dont change outfits - not enough time has passed since [", GET_PLAYER_PED_STRING(ePedChar), "] clothes changed [", tLastChangedOutfits, "]")
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is less than 2hrs [", tLastChangedOutfits, "]")
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is less than 2hrs [", tLastChangedOutfits, "]")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is null")
			CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePedChar), "] is null")
		ENDIF
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_0)	 //1029457: Switch clothing change for Franklin 0 - switch Franklin into outfit attached. See character artists for exact clothing variations. 
		IF (ePedChar = CHAR_FRANKLIN)
			BOOL bSetPedComponentForMission = FALSE
			
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_3)
				IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)
					bSetPedComponentForMission = TRUE
				ENDIF
			ENDIF
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2)
				bSetPedComponentForMission = TRUE
			ENDIF
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
				bSetPedComponentForMission = TRUE
			ENDIF
			
			IF bSetPedComponentForMission
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_WHITE_SHIRT)
				AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_BEIGE_SHORTS)
				AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_FEET, FEET_P1_TRAINERS)
					CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: TORSO_P1_WHITE_SHIRT, LEGS_P1_BEIGE_SHORTS, COMP_TYPE_FEET - FRANKLIN_0")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_LAMAR)		 	//1029472: Switch clothing change for lamar 1 - switch Franklin into outfit attached. See character artists for exact clothing variations. 
		IF (ePedChar = CHAR_FRANKLIN)
			IF (g_eSelectedPlayerCharScene <> PR_SCENE_Fa_STRIPCLUB_ARM3)	//#1220376
			AND (g_eSelectedPlayerCharScene <> PR_SCENE_Fa_PHONECALL_ARM3)	//#1220376
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P1_TRACKSUIT_JEANS - LAMAR")
				RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P1_TRACKSUIT_JEANS)
			ELSE
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: blocked for Fa_STRIPCLUB_ARM3 - LAMAR")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1) 			//1029491: Switch clothing change for Family 1 
		IF (ePedChar = CHAR_FRANKLIN)
			IF (g_eSelectedPlayerCharScene <> PR_SCENE_Fa_STRIPCLUB_ARM3)	//#1220376
			AND (g_eSelectedPlayerCharScene <> PR_SCENE_Fa_PHONECALL_ARM3)	//#1220376
				BOOL bSetPedComponentForMission = FALSE
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FRANKLIN_0)
					bSetPedComponentForMission = TRUE
				ENDIF
				
				IF bSetPedComponentForMission
					IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_SHIRT_JEANS)
						CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P1_WHITE_SHIRT_JEANS - FAMILY_1")
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: blocked for Fa_STRIPCLUB_ARM3 - FAMILY_1")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_3)
	
		BOOL bSetPedComponentForMission = FALSE
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
			bSetPedComponentForMission = TRUE
		ENDIF
		
		IF bSetPedComponentForMission
			IF (ePedChar = CHAR_MICHAEL) 			//1238856: can we put Michael in this outfit if player switched from Franklin to Michael after family 1, but before triggering family 3 (now 2)
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_POLOSHIRT_JEANS_1)
					CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P0_POLOSHIRT_JEANS_1 - FAMILY_3")
					RETURN TRUE
				ENDIF
			ENDIF
			IF (ePedChar = CHAR_FRANKLIN) 			//1029521: Switch clothing change for Family 3 
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P1_GREEN_SHIRT_JEANS)
					CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P1_GREEN_SHIRT_JEANS - FAMILY_3")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4) 			//1212733: Can we set Michael to be wearing his Black Suit if player switches to him After Trevor 3 and before Family 4
		IF (ePedChar = CHAR_MICHAEL)
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_3)
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P0_NAVY_SUIT - FAMILY_4")
				RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_NAVY_SUIT)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_FBI_1) 			//1294152: Clothing change - could we put Franklin in this outfit if player switches to him after passing Trevor 3 but before FIB 1
		IF (ePedChar = CHAR_FRANKLIN)
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_3)
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P1_HOODIE_AND_JEANS_3  - FAMILY_4")
				RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P1_HOODIE_AND_JEANS_3)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_2)
			IF (ePedChar = CHAR_TREVOR)					 	//1070023: can we change Trevor's outfit if player switches after FIB 2 and before triggering Franklin 1
				IF (g_eSelectedPlayerCharScene <> PR_SCENE_Ta_FBI2)
					IF IS_SPECIAL_EDITION_GAME()
						CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P2_DENIM - FRANKLIN_1")
						RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P2_DENIM)
					ELSE
						CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P2_TSHIRT_CARGOPANTS_3 - FRANKLIN_1")
						RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P2_TSHIRT_CARGOPANTS_3)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: blocked for Ta_FBI2 - FRANKLIN_1")
					RETURN FALSE
				ENDIF
			ENDIF
			IF (ePedChar = CHAR_FRANKLIN)					//1070077: can we change Franklins outfit if player switches to him after FIB2 but before Franklin 1
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P1_HOODIE_AND_JEANS_1 - FRANKLIN_1")
				RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P1_HOODIE_AND_JEANS_1)
			ENDIF
		ENDIF
	ENDIF
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_2) 	//1070093: If player switches away and back to Michael after Michael 2, change outfit to following
		IF IS_MISSION_AVAILABLE(SP_MISSION_SOLOMON_1)		//1575745
			IF (ePedChar = CHAR_MICHAEL)
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P0_LEATHER_AND_JEANS - MICHAEL_2")
				RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_LEATHER_AND_JEANS)
			ENDIF
		ENDIF
	ENDIF
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2) 		//1070114: clothing change for Trevor 4 - Change Trevor on players next switch to him, after completing Paleto Score 2.
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_5)
			IF (ePedChar = CHAR_TREVOR)
				CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: OUTFIT_P2_TSHIRT_CARGOPANTS_1 - RURAL_2")
				RETURN SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P2_TSHIRT_CARGOPANTS_1)
			ENDIF
		ENDIF
	ENDIF

	CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION: none")
	RETURN FALSE
#ENDIF
#ENDIF
ENDFUNC
#IF NOT IS_JAPANESE_BUILD
//1428410
FUNC BOOL Set_Pretty_Trevor_Dress(PED_REQUEST_SCENE_ENUM eScene,
		PED_COMP_NAME_ENUM &eTorso, PED_COMP_NAME_ENUM &eLegs, PED_COMP_NAME_ENUM &eFeet,
		PED_COMP_NAME_ENUM &eHand, PED_COMP_NAME_ENUM &eJbib,
		PED_COMP_NAME_ENUM &eHeadProp, PED_COMP_NAME_ENUM &eEyesProp,
		INT &iCheckAcquired, INT &iLimitedItems, BOOL &bDoRandom, BOOL &bMatchingTopAndPants)
	
	IF g_SavedGlobals.sPlayerSceneData.g_bSeenTrevorsPrettyDress
		RETURN FALSE
	ENDIF
	
	IF NOT g_savedGlobals.sCompletionPercentageData.b_g_OneHundredPercentReached
		RETURN FALSE
	ENDIF
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_CREDITS)
		RETURN FALSE
	ENDIF
	
	SWITCH eScene
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
		CASE PR_SCENE_T_CN_WAKETRAIN
		CASE PR_SCENE_T_CR_LINGERIE
		CASE PR_SCENE_T_NAKED_ISLAND
			//
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	eTorso = TORSO_P2_DRESS
	eLegs = LEGS_P2_TOILET
	eFeet = FEET_P2_TOILET
	
	eHand = HAND_P2_NONE
	eJbib = JBIB_P2_NONE
	eHeadProp = PROPS_EYES_NONE
	eEyesProp = PROPS_EYES_NONE
	
	iCheckAcquired = 0
	iLimitedItems = 0
	bDoRandom = TRUE
	bMatchingTopAndPants = FALSE
	

	CPRINTLN(DEBUG_SWITCH, "Set_Pretty_Trevor_Dress!!!")
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL RESET_PED_VARIATIONS_AFTER_MP_SWITCH_OUT_OF_MISSION(enumCharacterList ePed, SP_MISSIONS &eResetOutfitMPSwitchReturn)
	
	eResetOutfitMPSwitchReturn = SP_MISSION_NONE
	SWITCH g_eMissionRunningOnMPSwitchStart
		CASE SP_MISSION_MICHAEL_1
			IF (ePed = CHAR_MICHAEL)
			OR (ePed = CHAR_TREVOR)
				eResetOutfitMPSwitchReturn = g_eMissionRunningOnMPSwitchStart
			ENDIF
		BREAK
		CASE SP_MISSION_TREVOR_1
		CASE SP_HEIST_DOCKS_1
		CASE SP_MISSION_FBI_3
			IF (ePed = CHAR_TREVOR)
				eResetOutfitMPSwitchReturn = g_eMissionRunningOnMPSwitchStart
			ENDIF
		BREAK
		CASE SP_HEIST_AGENCY_3A
		CASE SP_HEIST_AGENCY_3B
		CASE SP_HEIST_JEWELRY_2
		CASE SP_HEIST_RURAL_2
		CASE SP_HEIST_FINALE_2A
		CASE SP_HEIST_FINALE_2B
			eResetOutfitMPSwitchReturn = g_eMissionRunningOnMPSwitchStart
		BREAK
		CASE SP_HEIST_DOCKS_2A
		CASE SP_HEIST_DOCKS_2B
		CASE SP_MISSION_FBI_5
			IF (ePed = CHAR_MICHAEL)
				eResetOutfitMPSwitchReturn = g_eMissionRunningOnMPSwitchStart
			ENDIF
		BREAK
		CASE SP_MISSION_CARSTEAL_1
			IF (ePed = CHAR_MICHAEL)
			OR (ePed = CHAR_TREVOR)
				eResetOutfitMPSwitchReturn = g_eMissionRunningOnMPSwitchStart
			ENDIF
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_SWITCH, "ignore reset stored outfit - switched out of mission")
		BREAK
	ENDSWITCH
	IF (eResetOutfitMPSwitchReturn != SP_MISSION_NONE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_PED_PRESET_OUTFIT_FOR_SCENE(enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eScene, PED_INDEX pedIndex, INT &iOutfitPreload)
	IF IS_ENTITY_DEAD(pedIndex)
		RETURN FALSE
	ENDIF
	
	IF (iOutfitPreload < 0)
		RETURN FALSE
	ENDIF
	
	IF ePed <> GET_CURRENT_PLAYER_PED_ENUM()	//
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "SET_PED_PRESET_OUTFIT_FOR_SCENE(", GET_PLAYER_PED_STRING(ePed), ", ", Get_String_From_Ped_Request_Scene_Enum(eScene), ")")
	#ENDIF
	
	PED_COMP_NAME_ENUM eTorso = DUMMY_PED_COMP, eLegs = DUMMY_PED_COMP, eFeet = DUMMY_PED_COMP, eHand, eSpecial, eJbib
	PED_COMP_NAME_ENUM eHeadProp = PROPS_HEAD_NONE, eEyesProp = PROPS_EYES_NONE
	INT iCheckAcquired = 0
	INT iLimitedItems = 0
	BOOL bDoRandom = TRUE
	BOOL bMatchingTopAndPants = FALSE
	
	// set default  hand / jbib as none
	SWITCH GET_ENTITY_MODEL(pedIndex)
		CASE PLAYER_ZERO
			eHand = HAND_P0_NONE
			eJbib = JBIB_P0_NONE
			eSpecial = SPECIAL_P0_NONE
		BREAK
		CASE PLAYER_ONE
			eHand = HAND_P1_NONE
			eJbib = JBIB_P1_NONE
			eSpecial = SPECIAL_P1_DUMMY
		BREAK
		CASE PLAYER_TWO
			eHand = HAND_P2_NONE
			eJbib = JBIB_P2_NONE
			eSpecial = SPECIAL_P2_DUMMY
		BREAK
	ENDSWITCH
	
	SWITCH eScene
		
		CASE PR_SCENE_M2_BEDROOM		//#1101731
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_BED
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE	//#1101910
			eTorso = TORSO_P0_LEATHER_JACKET_0
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M2_PHARMACY		//#1101631
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M_VWOODPARK_a		//#1101435
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eJbib = PROPS_P0_GLASSES_DARK_9
		BREAK
//		CASE PR_SCENE_M_VWOODPARK_b		//#1101435 & 1542597
//			eTorso = TORSO_P0_OPEN_SHIRT
//			eLegs = LEGS_P0_CASUAL_JEANS
//			eFeet = FEET_P0_1
//			eJbib = JBIB_P0_VEST
//		BREAK
		CASE PR_SCENE_M_PIER_a			//#1101163
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_BARE_CHEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_a	//#1098986
			eTorso = TORSO_P0_GREY_SUIT_04
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b	//#1098986
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a	//#1098986
			eTorso = TORSO_P0_GREY_SUIT_01
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_B	//#1098986
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_C	//#1098986
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d	//#1098986 & #1415492
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_E	//#1098986
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M_CANAL_a			//#1101910
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_VEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9	
		BREAK
		CASE PR_SCENE_M_CANAL_b			//#1101910
			eTorso = TORSO_P0_YOGA_1
			eLegs = LEGS_P0_GREY_SUIT
			eFeet = FEET_P0_SKATE_SHOES
		BREAK
		CASE PR_SCENE_M_CANAL_c			//#1101910
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
//		CASE PR_SCENE_M_PIER_b			//#1101910
//			eTorso = TORSO_P0_BARE_CHEST
//			eLegs = LEGS_P0_YOGA_0
//			eFeet = FEET_P0_1
//		BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF	//#1101910
			IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_ENTITY_MODEL(pedIndex), COMP_TYPE_OUTFIT, OUTFIT_P0_GOLF)
				eTorso = TORSO_P0_YOGA_1
				eLegs = LEGS_P0_GREY_SUIT
				eFeet = FEET_P0_SKATE_SHOES
			ELSE
				eTorso = TORSO_P0_GOLF
				eLegs = LEGS_P0_GOLF
				eFeet = FEET_P0_1
				eHand = HAND_P0_GOLF
				eHeadProp = PROPS_P0_GOLF_VISOR
			ENDIF
		BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR	//#1101948
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT	//#1102356
			eTorso = TORSO_P0_GREY_SUIT_04
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M2_MARINA			//#1231575
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_NONE
			eEyesProp = PROPS_P0_GLASSES_DARK_9	
		BREAK
		CASE PR_SCENE_M2_LUNCH_a		//#1231568
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eJbib = JBIB_P0_NONE
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M4_WASHFACE	//#
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M_POOLSIDE_a		//#1240171
			IF GET_RANDOM_BOOL()
				eTorso = TORSO_P0_YOGA_0 
			ELSE
				eTorso = TORSO_P0_BARE_CHEST
			ENDIF
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M_POOLSIDE_b		//#1240171
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_BARE_CHEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9		
		BREAK
//		CASE PR_SCENE_M2_SAVEHOUSE0_b	//#1206223 & 1556272
//			IF GET_RANDOM_BOOL()
//				eTorso = TORSO_P0_BARE_CHEST 
//			ELSE
//				eTorso = TORSO_P0_BED
//			ENDIF
//			eLegs = LEGS_P0_BED
//			eFeet = FEET_P0_1
//		BREAK
		CASE PR_SCENE_M_HOOKERMOTEL		//#1323393
			eTorso = TORSO_P0_LEATHER_JACKET_0
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M_S_FAMILY4		//#1327230
			eTorso = TORSO_P0_YOGA_1
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a	//#1327230
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_SKATE_SHOES
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_b	//#1327351
			IF GET_RANDOM_BOOL()
				eTorso = TORSO_P0_BARE_CHEST 
			ELSE
				eTorso = TORSO_P0_YOGA_0
			ENDIF
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a	//#1327383
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_SKATE_SHOES
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b	//#1327386
			eTorso = TORSO_P0_YOGA_2
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a	//#1327395
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_VEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b	//#1327398
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M_TRAFFIC_a		//#1384628
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M_TRAFFIC_b		//#1384631
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M_TRAFFIC_c		//#1384638
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_VEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED	//#1102230
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_BED
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M4_WATCHINGTV		//#1231590
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_SKATE_SHOES
		BREAK
		CASE PR_SCENE_M4_LUNCH_b		//#1327420
			IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_ENTITY_MODEL(pedIndex), COMP_TYPE_OUTFIT, OUTFIT_P0_GOLF)
				eTorso = TORSO_P0_YOGA_1
				eLegs = LEGS_P0_LONG_SHORTS_1
				eFeet = FEET_P0_1
			ELSE
				eTorso = TORSO_P0_GOLF
				eLegs = LEGS_P0_GOLF
				eFeet = FEET_P0_1
				eHand = HAND_P0_GOLF
				eHeadProp = PROPS_P0_GOLF_VISOR
			ENDIF
		BREAK
		CASE PR_SCENE_M4_WAKEUPSCREAM		//#1102215
			eTorso = TORSO_P0_BARE_CHEST
			eLegs = LEGS_P0_BED
			eFeet = FEET_P0_1
		BREAK
//		CASE PR_SCENE_M4_HOUSEBED_b		//#1102215
//			eTorso = TORSO_P0_BED
//			eLegs = LEGS_P0_JEANS_BAREFEET
//			eFeet = FEET_P0_1
//		BREAK
		CASE PR_SCENE_M2_CYCLING_a		//#1384654
			eTorso = TORSO_P0_YOGA_0
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M2_CYCLING_b		//#1384672
			IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_ENTITY_MODEL(pedIndex), COMP_TYPE_OUTFIT, OUTFIT_P0_TRIATHLON)
				eTorso = TORSO_P0_YOGA_1
				eLegs = LEGS_P0_LONG_SHORTS_1
				eFeet = FEET_P0_1
				eEyesProp = PROPS_P0_GLASSES_DARK_9
			ELSE
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_TRIATHLON, TRUE)
					iOutfitPreload = -1
					RETURN FALSE
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_CYCLING_c		//#1384676
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M_BENCHCALL_a		//#1384571
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M_BENCHCALL_b		//#1384571
			eTorso = TORSO_P0_YOGA_2
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M2_DRIVING_a		//#1384645
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M2_DRIVING_b		//#1384650
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M6_SUNBATHING		//#1206320
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRINKINGBEER		//#1327452
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
//		CASE PR_SCENE_M6_HOUSETV_a		//#1327458
//			eTorso = TORSO_P0_BED
//			eLegs = LEGS_P0_BED
//			eFeet = FEET_P0_1
//			bDoRandom = FALSE
//		BREAK
		CASE PR_SCENE_M6_MORNING_a		//#1327478
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_RONBORING		//#1206353
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_b		//#1384725
			eTorso = TORSO_P0_GREY_SUIT_01
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_c		//#1384734
			eTorso = TORSO_P0_GREY_SUIT_01
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_d		//#1384737
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom=FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_e		//#1384741
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_f		//#1384746
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_g		//#1384749
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DRIVING_h		//#1384752
			eTorso = TORSO_P0_GREY_SUIT_01
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M_COFFEE_a		//#1098961
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_VEST
		BREAK
		CASE PR_SCENE_M_COFFEE_b		//#1098961
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M_COFFEE_c		//#1098961
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
		BREAK
		CASE PR_SCENE_M7_COFFEE		//#1098961
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M2_CARSLEEP_a			//#1327296
			eTorso = TORSO_P0_LEATHER_JACKET_0
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M2_CARSLEEP_b			//#1327296
			eTorso = TORSO_P0_LEATHER_JACKET_0
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M6_CARSLEEP			//#1327296
			eTorso = TORSO_P0_GREY_SUIT_01
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M2_KIDS_TV			//#1327302
			eTorso = TORSO_P0_YOGA_2
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_ONPHONE			//#1327488
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_DEPRESSED			//#1327507
			eTorso = TORSO_P0_BED
			eLegs = LEGS_P0_GREY_SUIT_1
			eFeet = FEET_P0_BLACK_SHOES
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M6_BOATING			//#1384755 & #1521647 & #1570424
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_YOGA_0			//LEGS_P0_BED
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_BARE_CHEST
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_M7_TALKTOGUARD			//#1396923
			eTorso = TORSO_P0_LEATHER_JACKET_0
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR			//#1396970
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_SKATE_SHOES
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY			//#1102451
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_VEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a			//#1102477
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b			//#1117782
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY			//#1102495
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO			//#1206363
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_EXITBARBER				//#1102506
			eTorso = TORSO_P0_LEATHER_JACKET_0
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP			//#1102532
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_GETSREADY				//#1102556
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_HOOKERS				//#1327554
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING			//#1231626
			eTorso = TORSO_P0_YOGA_1
			eLegs = LEGS_P0_GREY_SUIT
			eFeet = FEET_P0_SKATE_SHOES
		BREAK
		CASE PR_SCENE_M7_KIDS_TV				//#1206386
			SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
			iOutfitPreload = -1
			RETURN FALSE
		BREAK
		CASE PR_SCENE_M7_LOT_JIMMY				//#1102575
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS			//#1206384
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_BARE_CHEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_READSCRIPT				//#1104510
			eTorso = TORSO_P0_YOGA_0
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M7_REJECTENTRY				//#1104512
			eTorso = TORSO_P0_OPEN_SHIRT
			eLegs = LEGS_P0_CASUAL_JEANS
			eFeet = FEET_P0_1
			eJbib = JBIB_P0_VEST
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_RESTAURANT				//#1104518
			IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_ENTITY_MODEL(pedIndex), COMP_TYPE_OUTFIT, OUTFIT_P0_TENNIS)
				eTorso = TORSO_P0_YOGA_1
				eLegs = LEGS_P0_LONG_SHORTS_1
				eFeet = FEET_P0_1
				eEyesProp = PROPS_P0_GLASSES_DARK_9
			ELSE
				IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_TENNIS, TRUE)
					iOutfitPreload = -1
					RETURN FALSE
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA				//#1104518
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P0_V_NECK_0, iCheckAcquired)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P0_V_NECK_0
			ELSE
				eTorso = TORSO_P0_BARE_CHEST
			ENDIF
			eLegs = LEGS_P0_YOGA_0
			eFeet = FEET_P0_1
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE				//#1231650
			eTorso = TORSO_P0_DENIM_SHIRT_0
			eLegs = LEGS_P0_LONG_SHORTS_1
			eFeet = FEET_P0_1
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA				//#1396899
			eTorso = TORSO_P0_GREY_SUIT
			eLegs = LEGS_P0_GREY_SUIT_4
			eFeet = FEET_P0_BLACK_SHOES
			eEyesProp = PROPS_P0_GLASSES_DARK_9
		BREAK
		
		// // // // // // // // // // // // // // // // // //
		CASE PR_SCENE_F_CLUB					//#1404292
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SUIT, iCheckAcquired)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SUIT
				eLegs = LEGS_P1_SUIT
				eFeet = FEET_P1_SUIT  
				eJbib = JBIB_P1_SHIRT_A
				bMatchingTopAndPants = TRUE
			ELSE
				eTorso = TORSO_P1_BLACK_LNGSLEEVE
				eLegs = LEGS_P1_BLACK_JEANS
				eFeet = FEET_P1_SNEAKERS_A_0
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3				//#1404292
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SUIT, iCheckAcquired)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SUIT
				eLegs = LEGS_P1_SUIT
				eFeet = FEET_P1_SUIT  
				eJbib = JBIB_P1_SHIRT_A
				bMatchingTopAndPants = TRUE
			ELSE
				eTorso = TORSO_P1_TRACKSUIT_2
				eLegs = LEGS_P1_BLACK_JEANS
				eFeet = FEET_P1_SNEAKERS_A_0
			ENDIF
		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1				//#1404292
//			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP (pedIndex, COMP_TYPE_TORSO, TORSO_P1_DRESS_SHIRT, iLimitedItems)
//			AND IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
//				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
//				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
//				
//				eTorso = TORSO_P1_DRESS_SHIRT
//				eLegs = LEGS_P1_SUIT
//				eFeet = FEET_P1_SUIT
//			ELSE
//				eTorso = TORSO_P1_TRACKSUIT_2
//				eLegs = LEGS_P1_BLACK_JEANS
//				eFeet = FEET_P1_SNEAKERS_A_0
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3				//#1404292
//			eTorso = TORSO_P1_BLUE_SHIRT
//			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
//				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
//				eLegs = LEGS_P1_JEANS_0
//			ELSE
//				eLegs = LEGS_P1_BLACK_JEANS
//			ENDIF
//			eFeet = FEET_P1_NUBUCK_BOOTS
//		BREAK
		CASE PR_SCENE_F0_GARBAGE				//#1396999
			eTorso = TORSO_P1_BLUE_SHIRT
			SET_BIT(iLimitedItems, 0)
			SET_BIT(iLimitedItems, 2)
			SET_BIT(iLimitedItems, 5)
			SET_BIT(iLimitedItems, 10)
			SET_BIT(iLimitedItems, 13)
			SET_BIT(iLimitedItems, 14)
			
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS
			ENDIF
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		CASE PR_SCENE_F1_GARBAGE				//#1401475
			eTorso = TORSO_P1_YELLOW_SHIRT
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_F0_SH_READING 				//#1401552
			eTorso = TORSO_P1_HOODIE
			SET_BIT(iLimitedItems, 0)
			SET_BIT(iLimitedItems, 2)
			SET_BIT(iLimitedItems, 5)
			SET_BIT(iLimitedItems, 9)
			SET_BIT(iLimitedItems, 12)
			
			IF GET_RANDOM_BOOL()
				IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_0
				ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_B_0
				ELSE
					elegs = LEGS_P1_BLACK_JEANS 
				ENDIF
			ELSE
				IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_B_0
				ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_0
				ELSE
					elegs = LEGS_P1_BLACK_JEANS 
				ENDIF
			ENDIF
		BREAK
		CASE PR_SCENE_F1_SH_READING				//#1401552
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_JACKET_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_JACKET_0
				
				IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, JBIB_P1_TSHIRT_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
					eJbib = JBIB_P1_TSHIRT_0
				ENDIF
			ELSE
				eTorso = TORSO_P1_BLUE_SHIRT
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_0
			ELSE
				elegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		CASE PR_SCENE_F0_CLEANCAR 				//#1401484
			eTorso = TORSO_P1_WHITE_VEST
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F1_CLEANCAR				//#1401484
			eTorso = TORSO_P1_BLACK_VEST
			SET_BIT(iLimitedItems, 1)
			
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_B_0
		BREAK
		CASE PR_SCENE_F0_SH_ASLEEP 				//#1401487
			eTorso = TORSO_P1_BLACK_VEST
			SET_BIT(iLimitedItems, 1)
			
			eLegs = LEGS_P1_BEIGE_SHORTS		//LEGS_P1_BOXERS
			eFeet = FEET_P1_SNEAKERS_A_0		//FEET_P1_BARE_FEET
		BREAK
		CASE PR_SCENE_F1_SH_ASLEEP				//#1401487
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST
			eLegs = LEGS_P1_BEIGE_SHORTS		//LEGS_P1_BOXERS
			eFeet = FEET_P1_SNEAKERS_A_0		//FEET_P1_BARE_FEET
		BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a 				//#1401537
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_b				//#1401537
			eTorso = TORSO_P1_BLACK_VEST
			eLegs = LEGS_P1_SWEATPANTS
			eFeet = FEET_P1_SNEAKERS_B_0
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_F1_SH_PUSHUP					//#1401537
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_BASKETBALL_SHORTS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_BASKETBALL_SHORTS_0
			ELSE
				eLegs = LEGS_P1_BEIGE_SHORTS
			ENDIF
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F1_SNACKING				//#1401557
			eTorso = TORSO_P1_WHITE_VEST
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_BASKETBALL_SHORTS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_BASKETBALL_SHORTS_0
			ELSE
				eLegs = LEGS_P1_BEIGE_SHORTS
			ENDIF
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F1_CLEANINGAPT				//#1401564
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_DRESS_SHIRT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_DRESS_SHIRT
			ELSE
				eTorso = TORSO_P1_HOODIE_2
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_SUIT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_SUIT
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_FEET, FEET_P1_SUIT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_FEET))
				eFeet = FEET_P1_SUIT
			ELSE
				eFeet = FEET_P1_NUBUCK_BOOTS
			ENDIF
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_F1_IRONING				//#1401594
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST
			eLegs = LEGS_P1_BEIGE_SHORTS		//LEGS_P1_BOXERS
			eFeet = FEET_P1_SNEAKERS_A_0		//FEET_P1_DUMMY
		BREAK
		CASE PR_SCENE_F1_BYETAXI				//#1401597
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHORT_SLEEVE, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SHORT_SLEEVE
			ELSE
				eTorso = TORSO_P1_WHITE_VEST
				SET_BIT(iLimitedItems, 0)
				SET_BIT(iLimitedItems, 1)
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_FEET, FEET_P1_SKATE_SHOES_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_FEET))
				eFeet = FEET_P1_SKATE_SHOES_0
			ELSE
				eFeet = FEET_P1_SNEAKERS_A_0
			ENDIF
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP				//#1401601
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_ARMY_JACKET, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_ARMY_JACKET
			ELSE
				eTorso = TORSO_P1_OFF_WHITE_SHIRT
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS
			ENDIF
			eFeet = FEET_P1_SNEAKERS_A_0
			eHeadProp = PROPS_P1_GREEN_CAP
		BREAK
		CASE PR_SCENE_F1_ONCELL				//#1401623
			eTorso = TORSO_P1_WHITE_VEST
			SET_BIT(iLimitedItems, 0)
			SET_BIT(iLimitedItems, 1)
			
			eLegs = LEGS_P1_SWEATPANTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F0_BIKE				//#1401481
			eTorso = TORSO_P1_WHITE_LNGSLEEVE
			SET_BIT(iLimitedItems, 0)
			SET_BIT(iLimitedItems, 2)
			SET_BIT(iLimitedItems, 10)
			SET_BIT(iLimitedItems, 12)
			SET_BIT(iLimitedItems, 14)
			SET_BIT(iLimitedItems, 15)
			
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_B_0
		BREAK
		CASE PR_SCENE_F1_BIKE				//#1401481
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHIRT_0, iLimitedItems)
				IF GET_RANDOM_BOOL()
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
					eTorso = TORSO_P1_SHIRT_0
				ELSE
					eTorso = TORSO_P1_HOODIE_2
				ENDIF
			ELSE
				eTorso = TORSO_P1_HOODIE_2
			ENDIF

			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_B_0
			ELSE
				elegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		CASE PR_SCENE_F1_PLAYCHOP				//#1403880
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_BASKETBALL_SHORTS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_BASKETBALL_SHORTS_0
			ELSE
				eLegs = LEGS_P1_BEIGE_SHORTS
			ENDIF
			eFeet = FEET_P1_SNEAKERS_B_0
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_E_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_E_0
			ENDIF
		BREAK
		CASE PR_SCENE_F0_PLAYCHOP				//#1403880
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHORT_SLEEVE, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SHORT_SLEEVE
				SET_BIT(iLimitedItems, 0)
			ELSE
				eTorso = TORSO_P1_WHITE_VEST
				SET_BIT(iLimitedItems, 0)
			ENDIF
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F0_WALKCHOP				//#1403880
			eTorso = TORSO_P1_HOODIE
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_TRAINERS
		BREAK
		CASE PR_SCENE_F_WALKCHOP_A				//#1403880
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_GRAY_HOODIE, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_GRAY_HOODIE
			ELSE
				IF GET_RANDOM_BOOL()
					eTorso = TORSO_P1_HOODIE_1 
				ELSE
					eTorso = TORSO_P1_TRACKSUIT_2
				ENDIF
			ENDIF
			eLegs = LEGS_P1_SWEATPANTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F_WALKCHOP_b				//#1403880
			IF GET_RANDOM_BOOL()
				eTorso = TORSO_P1_TRACKSUIT_2 
			ELSE
				eTorso = TORSO_P1_HOODIE_1
			ENDIF
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F1_POOLSIDE_a				//#1403979
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST 
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_BASKETBALL_SHORTS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_BASKETBALL_SHORTS_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_TENNIS, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_TENNIS
			ELSE
				eLegs = LEGS_P1_BEIGE_SHORTS 
			ENDIF
			IF (eLegs = LEGS_P1_TENNIS)
				eFeet = FEET_P1_BARE_FEET
			ELSE
				eFeet = FEET_P1_TRAINERS
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_A_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_A_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_B_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_B_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_C_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_C_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_D_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_D_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_E_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_E_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_F_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_F_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_G_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_G_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_H_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_H_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_I_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_I_0
			ENDIF
		BREAK
		CASE PR_SCENE_F1_POOLSIDE_b				//#1403979
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST 
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_TENNIS, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_TENNIS
			ELSE
				eLegs = LEGS_P1_BEIGE_SHORTS		//LEGS_P1_BOXERS 
			ENDIF
			eFeet = FEET_P1_BARE_FEET
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_D_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_D_0
			ENDIF
		BREAK
		CASE PR_SCENE_F1_GETTINGREADY				//#1404014
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHIRT_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SHIRT_0
			ELSE
				eTorso = TORSO_P1_BLUE_SHIRT 
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		CASE PR_SCENE_F1_WATCHINGTV				//#1404021
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHORT_SLEEVE, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SHORT_SLEEVE
			ELSE
				eTorso = TORSO_P1_WHITE_VEST
				SET_BIT(iLimitedItems, 0)
			ENDIF
			eLegs = LEGS_P1_SWEATPANTS 
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_FEET, FEET_P1_SKATE_SHOES_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_FEET))
				eFeet = FEET_P1_SKATE_SHOES_0
			ELSE
				eFeet = FEET_P1_SNEAKERS_A_0 
			ENDIF
		BREAK
		CASE PR_SCENE_F1_NAPPING				//#1404180
			eTorso = TORSO_P1_WHITE_VEST		//TORSO_P1_BARE_CHEST
			eLegs = LEGS_P1_SWEATPANTS
			eFeet = FEET_P1_SNEAKERS_B_0		//FEET_P1_BARE_FEET
		BREAK
		CASE PR_SCENE_F_GYM						//#1404377
			eTorso = TORSO_P1_BLACK_VEST
			eLegs = LEGS_P1_SWEATPANTS
			eFeet = FEET_P1_SNEAKERS_B_0
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND			//#1404415
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_ARMY_JACKET, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_ARMY_JACKET
			ELSE
				eTorso = TORSO_P1_OFF_WHITE_SHIRT
				SET_BIT(iLimitedItems, 2)
			ENDIF
			IF GET_RANDOM_BOOL()
				IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_0
				ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_B_0
				ELSE
					elegs = LEGS_P1_BLACK_JEANS 
				ENDIF
			ELSE
				IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_B_0
				ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					elegs = LEGS_P1_JEANS_0
				ELSE
					elegs = LEGS_P1_BLACK_JEANS 
				ENDIF
			ENDIF
			
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC				//#1404465
			eTorso = TORSO_P1_BLACK_LNGSLEEVE
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_B_0
			ELSE
				elegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_SNEAKERS_A_0
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_A_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_A_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_B_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_B_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_C_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_C_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_D_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_D_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_E_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_E_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_F_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_F_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_G_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_G_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_H_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_H_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_PROPS, PROPS_P1_SUNGLASSES_I_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_PROPS))
				eEyesProp = PROPS_P1_SUNGLASSES_I_0
			ENDIF
		BREAK
		CASE PR_SCENE_F1_NEWHOUSE				//#1404536
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SWEATER_5, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SWEATER_5
			ELSE
				eTorso = TORSO_P1_TRACKSUIT_2
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_0
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_B_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				elegs = LEGS_P1_JEANS_B_0
			ELSE
				elegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_FEET, FEET_P1_SKATE_SHOES_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_FEET))
				eFeet = FEET_P1_SKATE_SHOES_0
			ELSE
				eFeet = FEET_P1_SNEAKERS_A_0 
			ENDIF
		BREAK
		CASE PR_SCENE_F_S_EXILE2				//#1404565
			eTorso = TORSO_P1_WHITE_VEST
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F_BAR_a_01				//#1404257
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_JACKET_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_JACKET_0
				eJbib = JBIB_P1_TSHIRT_0
			ELSE
				eTorso = TORSO_P1_BLACK_LNGSLEEVE
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		CASE PR_SCENE_F_BAR_b_01				//#1404257
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHIRT_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_SHIRT_0
			ELSE
				eTorso = TORSO_P1_BLACK_LNGSLEEVE
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		CASE PR_SCENE_F_BAR_c_02				//#1404257
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_DRESS_SHIRT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_DRESS_SHIRT
			ELSE
				eTorso = TORSO_P1_YELLOW_SHIRT
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_SUIT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_SUIT
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_FEET, FEET_P1_SUIT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_FEET))
				eFeet = FEET_P1_SUIT
			ELSE
				eFeet = FEET_P1_NUBUCK_BOOTS 
			ENDIF
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_F_BAR_d_02				//#1404257
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_VARSITY_14, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_VARSITY_14
			ELSE
				eTorso = TORSO_P1_OFF_WHITE_SHIRT
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_SNEAKERS_A_0
			eHeadProp = PROPS_P1_GREEN_CAP
		BREAK
		CASE PR_SCENE_F_BAR_e_01				//#1404257
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHORT_SLEEVE, iLimitedItems)
				IF GET_RANDOM_BOOL()
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
					eTorso = TORSO_P1_SHORT_SLEEVE
				ELSE
					eTorso = TORSO_P1_WHITE_VEST
				ENDIF
			ELSE
				eTorso = TORSO_P1_WHITE_VEST
			ENDIF
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a				//#1404400
			eTorso = TORSO_P1_TRACKSUIT_2
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_b				//#1404400
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_SHORT_SLEEVE, iLimitedItems)
				IF GET_RANDOM_BOOL()
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
					eTorso = TORSO_P1_SHORT_SLEEVE
				ELSE
					eTorso = TORSO_P1_TRACKSUIT_2
				ENDIF
			ELSE
				eTorso = TORSO_P1_TRACKSUIT_2
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P1_JEANS_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P1_JEANS_0
			ELSE
				eLegs = LEGS_P1_BLACK_JEANS 
			ENDIF
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_c				//#1404400
			eTorso = TORSO_P1_HOODIE
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_TRAINERS
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT			//#1404513
			eTorso = TORSO_P1_BLACK_VEST
			eLegs = LEGS_P1_SWEATPANTS
			eFeet = FEET_P1_SNEAKERS_B_0
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1				//#1404513
			eTorso = TORSO_P1_HOODIE
			eLegs = LEGS_P1_BEIGE_SHORTS
			eFeet = FEET_P1_SNEAKERS_A_0
			bDoRandom = FALSE
		BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P3				//#1404513
//			eTorso = TORSO_P1_HOODIE
//			eLegs = LEGS_P1_BEIGE_SHORTS
//			eFeet = FEET_P1_TRAINERS
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5				//#1404513
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P1_VARSITY_11, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P1_VARSITY_11
			ELSE
				eTorso = TORSO_P1_HOODIE
			ENDIF
			eLegs = LEGS_P1_BLACK_JEANS
			eFeet = FEET_P1_NUBUCK_BOOTS
		BREAK
		
		// // // // // // // // // // // // // // // // // //
		CASE PR_SCENE_T_DRUNKHOWLING				//#1407694
			eTorso = TORSO_P2_NONE
			eSpecial = SPECIAL_P2_WATCH
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_BEACH, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P2_BEACH
			ELSE
				eLegs = LEGS_P2_CARGOPANTS 
			ENDIF
			eFeet = FEET_P2_DUMMY
			eEyesProp = PROPS_P2_SUNGLASSES_B_0
		BREAK
		CASE PR_SCENE_T_SC_CHASE				//#1407606
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_LONG_SLEEVE, iLimitedItems)
				IF GET_RANDOM_BOOL()
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
					eTorso = TORSO_P2_LONG_SLEEVE
				ELSE
					eTorso = TORSO_P2_NONE
					eSpecial = SPECIAL_P2_WATCH
				ENDIF
			ELSE
				eTorso = TORSO_P2_NONE
				eSpecial = SPECIAL_P2_WATCH
			ENDIF
			eLegs = LEGS_P2_UNDERWEAR
			eFeet = FEET_P2_DUMMY
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A				//#1407546
			eTorso = TORSO_P2_TANK_TOP_1
			eLegs = LEGS_P2_SWEAT_PANTS
			eFeet = FEET_P2_REDWINGS
			eEyesProp = PROPS_P2_SUNGLASSES_B_0
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0				//#1407546
			eTorso = TORSO_P2_WHITE_TSHIRT
			eLegs = LEGS_P2_SWEAT_PANTS
			eFeet = FEET_P2_REDWINGS
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E1				//#1407546
			eTorso = TORSO_P2_WHITE_TSHIRT
			eLegs = LEGS_P2_SWEAT_PANTS
			eFeet = FEET_P2_REDWINGS
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E2				//#1407546
			eTorso = TORSO_P2_NONE
			eSpecial = SPECIAL_P2_WATCH
			eLegs = LEGS_P2_CARGOPANTS_9
			eFeet = FEET_P2_DUMMY
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E3				//#1407546
			eTorso = TORSO_P2_TANK_TOP_1
			eLegs = LEGS_P2_UNDERWEAR
			eFeet = FEET_P2_DUMMY
		BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE				//#1407552
			eTorso = TORSO_P2_NONE
			eSpecial = SPECIAL_P2_WATCH
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_BEACH, iLimitedItems)
				IF GET_RANDOM_BOOL()
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
					eLegs = LEGS_P2_BEACH
					eFeet = FEET_P2_DUMMY
				ELSE
					eLegs = LEGS_P2_BLUE_JEANS
					eFeet = FEET_P2_REDWINGS
				ENDIF
			ELSE
				eLegs = LEGS_P2_BLUE_JEANS
				eFeet = FEET_P2_REDWINGS
			ENDIF
		BREAK
		CASE PR_SCENE_T_DOCKS_a						//#1407269
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_DENIM_SHIRT, iLimitedItems)
				IF GET_RANDOM_BOOL()
					SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
					eTorso = TORSO_P2_DENIM_SHIRT
				ELSE
					eTorso = TORSO_P2_WHITE_TSHIRT
				ENDIF
			ELSE
				eTorso = TORSO_P2_WHITE_TSHIRT
			ENDIF
			IF GET_RANDOM_BOOL()
				eLegs = LEGS_P2_BLUE_JEANS
			ELSE
				eLegs = LEGS_P2_SWEAT_PANTS
			ENDIF
			eFeet = FEET_P2_REDWINGS
		BREAK
		CASE PR_SCENE_T_DOCKS_b						//#1407269
			eTorso = TORSO_P2_TANK_TOP_1
			eLegs = LEGS_P2_SWEAT_PANTS
			eFeet = FEET_P2_REDWINGS
			eEyesProp = PROPS_P2_SUNGLASSES_B_0
		BREAK
		CASE PR_SCENE_T_DOCKS_c						//#1407269
			eTorso = TORSO_P2_VNECK_2
			eLegs = LEGS_P2_CARGOPANTS
			eFeet = FEET_P2_DUMMY
		BREAK
		CASE PR_SCENE_T_DOCKS_d						//#1407269
			eTorso = TORSO_P2_TSHIRT_1
			eLegs = LEGS_P2_BLUE_JEANS
			eFeet = FEET_P2_REDWINGS
		BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER				//#1449949
			eTorso = TORSO_P2_TSHIRT_1
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_BEACH_5, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P2_BEACH_5
				eFeet = FEET_P2_DUMMY
			ELSE
				eLegs = LEGS_P2_SWEAT_PANTS
				eFeet = FEET_P2_BLACK_BOOTS
			ENDIF
			eEyesProp = PROPS_P2_SUNGLASSES_B_0
		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT				//#1449955
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_CHEAPSUIT_0, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P2_CHEAPSUIT_0
			ELSE
				eTorso = TORSO_P2_VNECK_2
			ENDIF
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_CHEAP_SUIT_PANTS, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P2_CHEAP_SUIT_PANTS
				eFeet = FEET_P2_DUMMY
			ELSE
				eLegs = LEGS_P2_BLUE_JEANS
				eFeet = FEET_P2_BLACK_BOOTS
			ENDIF
		BREAK
		CASE PR_SCENE_T_CR_FUNERAL						//#1450036
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_LONG_SLEEVE, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P2_LONG_SLEEVE
			ELIF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_DENIM_SHIRT, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P2_DENIM_SHIRT
			ELSE
				eTorso = TORSO_P2_WHITE_TSHIRT
			ENDIF
			eLegs = LEGS_P2_BLUE_JEANS
			eFeet = FEET_P2_BLACK_BOOTS
		BREAK
		CASE PR_SCENE_T_CN_CHATEAU_c						//#1450043
			eTorso = TORSO_P2_TSHIRT_1
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_BEACH, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P2_BEACH
			ELSE
				eLegs = LEGS_P2_CARGOPANTS
			ENDIF
			eFeet = FEET_P2_DUMMY
			eEyesProp = PROPS_P2_SUNGLASSES_B_0
		BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT						//#1454012
			eTorso = TORSO_P2_NONE
			eSpecial = SPECIAL_P2_WATCH
			eLegs = LEGS_P2_SWEAT_PANTS
			eFeet = FEET_P2_BLACK_BOOTS
		BREAK
		CASE PR_SCENE_T_CR_CHATEAU_d						//#1453677
			
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_CHEAPSUIT_0, 0)
			OR IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_STYLESUIT_JACKET, 0)
				SET_RANDOM_CLOTHES_COMBO(pedIndex, TRUE, TRUE, TRUE)
				iOutfitPreload = -1
				RETURN FALSE
			ELSE
				eTorso = TORSO_P2_WHITE_TSHIRT
				eLegs = LEGS_P2_CARGOPANTS
				eFeet = FEET_P2_DUMMY
			ENDIF
		BREAK
		CASE PR_SCENE_T_CR_LINGERIE						//#1454013
			
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_TORSO, TORSO_P2_DRESS_3, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_TORSO))
				eTorso = TORSO_P2_DRESS_3
				eLegs = LEGS_P2_UNDERWEAR
				eFeet = FEET_P2_DUMMY
			ELSE
				eTorso = TORSO_P2_WHITE_TSHIRT
				eLegs = LEGS_P2_CARGOPANTS
				eFeet = FEET_P2_DUMMY
			ENDIF
		BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS						//#1407753
			eTorso = TORSO_P2_TANK_TOP_1
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_BEACH, iLimitedItems)
				SET_BIT(iCheckAcquired, ENUM_TO_INT(COMP_TYPE_LEGS))
				eLegs = LEGS_P2_BEACH
			ELSE
				eLegs = LEGS_P2_UNDERWEAR_BF_0
			ENDIF
			eFeet = FEET_P2_DUMMY
		BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_JERKOFF						//#1407609
			eTorso = TORSO_P2_NONE
			eSpecial = SPECIAL_P2_WATCH
			eLegs = LEGS_P2_SWEAT_PANTS
			eFeet = FEET_P2_BLACK_BOOTS
			bDoRandom = FALSE
		BREAK
		CASE PR_SCENE_T_SHIT
			IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT,
					OUTFIT_P2_TOILET, FALSE)
				iOutfitPreload = -1
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		#ENDIF
		CASE PR_SCENE_T_NAKED_BRIDGE
		CASE PR_SCENE_T_NAKED_GARDEN
		CASE PR_SCENE_T_NAKED_ISLAND	
			
			eTorso = TORSO_P2_NONE
			eSpecial = SPECIAL_P2_WATCH
			eLegs = LEGS_P2_UNDERWEAR
			eFeet = FEET_P2_DUMMY
			
			IF SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_TORSO, eTorso, FALSE)
			AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_LEGS, eLegs, FALSE)
			AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_FEET, eFeet, FALSE)
			AND SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_SPECIAL, eSpecial, FALSE)
				iOutfitPreload = -1
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
//			SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_TORSO,
//					TORSO_P2_DRESS, FALSE)
//			SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_DRESS, TRUE)
//			
//			iOutfitPreload = -1
//			RETURN FALSE
			
			eTorso = TORSO_P2_DRESS
			eLegs = LEGS_P2_UNDERWEAR
			eFeet = FEET_P2_DUMMY
		BREAK
		
		DEFAULT
//			iOutfitPreload = -1
//			RETURN FALSE
			
			eTorso = DUMMY_PED_COMP
			eLegs = DUMMY_PED_COMP
			eFeet = DUMMY_PED_COMP
		BREAK
	ENDSWITCH
	
	INT	iRealtimeHoursToWaitToChangeClothes = 2
	SWITCH eScene
		//Force
		CASE PR_SCENE_M_POOLSIDE_a
		CASE PR_SCENE_M_POOLSIDE_b
		CASE PR_SCENE_M2_BEDROOM
		CASE PR_SCENE_M4_WAKEUPSCREAM 
		CASE PR_SCENE_M4_WAKESUPSCARED
		CASE PR_SCENE_M6_HOUSEBED
		CASE PR_SCENE_M7_EXITFANCYSHOP
		CASE PR_SCENE_M7_LOUNGECHAIRS
		CASE PR_SCENE_M7_READSCRIPT
		CASE PR_SCENE_M7_WIFETENNIS
		CASE PR_SCENE_F0_SH_ASLEEP
		CASE PR_SCENE_F1_SH_ASLEEP
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
		#ENDIF
		CASE PR_SCENE_T_NAKED_BRIDGE
		CASE PR_SCENE_T_NAKED_ISLAND
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
		CASE PR_SCENE_M4_WASHFACE
			iRealtimeHoursToWaitToChangeClothes = -1
		BREAK
		
		// 1 hour
		CASE PR_SCENE_M_HOOKERMOTEL
		CASE PR_SCENE_M4_EXITRESTAURANT
		CASE PR_SCENE_M2_CYCLING_a
		CASE PR_SCENE_M2_CYCLING_b
		CASE PR_SCENE_M2_CYCLING_c
		CASE PR_SCENE_M6_SUNBATHING
		CASE PR_SCENE_M6_RONBORING
		CASE PR_SCENE_M6_BOATING
		CASE PR_SCENE_M6_MORNING_a
		CASE PR_SCENE_M6_ONPHONE
		CASE PR_SCENE_M6_DEPRESSED
		CASE PR_SCENE_M7_FAKEYOGA
		CASE PR_SCENE_F0_SH_PUSHUP_a
		CASE PR_SCENE_F0_SH_PUSHUP_b
		CASE PR_SCENE_F1_SH_PUSHUP
		CASE PR_SCENE_F_GYM
		CASE PR_SCENE_F1_GETTINGREADY		//#1561557
		CASE PR_SCENE_T_SC_CHASE
		CASE PR_SCENE_T_DRUNKHOWLING
		CASE PR_SCENE_T_ANNOYSUNBATHERS
			iRealtimeHoursToWaitToChangeClothes = 1
		BREAK
		
		// Never overide a selected outfit
		CASE PR_SCENE_M_TRAFFIC_a
		CASE PR_SCENE_M_TRAFFIC_b
		CASE PR_SCENE_M_TRAFFIC_c
		CASE PR_SCENE_M2_CARSLEEP_a
		CASE PR_SCENE_M2_CARSLEEP_B
		CASE PR_SCENE_M2_DRIVING_a
		CASE PR_SCENE_M2_DRIVING_b
		CASE PR_SCENE_M2_DROPOFFDAU_a
		CASE PR_SCENE_M2_DROPOFFDAU_b
		CASE PR_SCENE_M2_DROPOFFSON_a
		CASE PR_SCENE_M2_DROPOFFSON_b
		CASE PR_SCENE_M_VWOODPARK_b
		CASE PR_SCENE_M6_DRIVING_a
		CASE PR_SCENE_M6_DRIVING_b
		CASE PR_SCENE_M6_DRIVING_c
		CASE PR_SCENE_M6_DRIVING_d
		CASE PR_SCENE_M6_DRIVING_e
		CASE PR_SCENE_M6_DRIVING_f
		CASE PR_SCENE_M6_DRIVING_g
		CASE PR_SCENE_M6_DRIVING_h
		CASE PR_SCENE_F_TRAFFIC_a
		CASE PR_SCENE_F_TRAFFIC_b
		CASE PR_SCENE_F_TRAFFIC_c
		CASE PR_SCENE_F_MD_FRANKLIN2
		CASE PR_SCENE_F_S_EXILE2
		CASE PR_SCENE_T_FLYING_PLANE
		CASE PR_SCENE_T_CR_CHASECAR_a
		CASE PR_SCENE_T_CN_CHASECAR_b
		CASE PR_SCENE_T_CR_CHASEBIKE
		CASE PR_SCENE_T_CR_CHASESCOOTER
		CASE PR_SCENE_T_CR_POLICE_a
		CASE PR_SCENE_T_CN_POLICE_b
		CASE PR_SCENE_T_CN_POLICE_c
			iRealtimeHoursToWaitToChangeClothes = 24
		BREAK
		
		DEFAULT
			SWITCH eScene
				// 10 hours
				CASE PR_SCENE_M_BENCHCALL_a
				CASE PR_SCENE_M_BENCHCALL_b
				CASE PR_SCENE_M_CANAL_a
				CASE PR_SCENE_M_CANAL_b
				CASE PR_SCENE_M_CANAL_c
				CASE PR_SCENE_M_PIER_b
				CASE PR_SCENE_M2_SMOKINGGOLF
				CASE PR_SCENE_M_COFFEE_a
				CASE PR_SCENE_M_COFFEE_b
				CASE PR_SCENE_M_COFFEE_c
				CASE PR_SCENE_M4_CINEMA
				CASE PR_SCENE_M_PARKEDHILLS_a
				CASE PR_SCENE_M_PARKEDHILLS_b
				CASE PR_SCENE_M6_PARKEDHILLS_a
				CASE PR_SCENE_M6_PARKEDHILLS_b
				CASE PR_SCENE_M6_PARKEDHILLS_c
				CASE PR_SCENE_M6_PARKEDHILLS_d
				CASE PR_SCENE_M6_PARKEDHILLS_e
				CASE PR_SCENE_M4_PARKEDBEACH
				CASE PR_SCENE_M_PIER_a
				CASE PR_SCENE_M_VWOODPARK_a
				CASE PR_SCENE_M2_KIDS_TV
				CASE PR_SCENE_M2_LUNCH_a
				CASE PR_SCENE_M2_MARINA
				CASE PR_SCENE_M2_PHARMACY
				CASE PR_SCENE_M2_ARGUEWITHWIFE
				CASE PR_SCENE_M2_WIFEEXITSCAR
				CASE PR_SCENE_M4_WATCHINGTV
				CASE PR_SCENE_M2_SAVEHOUSE1_a
				CASE PR_SCENE_M2_SAVEHOUSE1_b
				CASE PR_SCENE_M4_LUNCH_b
				CASE PR_SCENE_M6_DRINKINGBEER
				CASE PR_SCENE_M6_HOUSETV_a
				CASE PR_SCENE_M6_LIQUORSTORE
				CASE PR_SCENE_M7_BYESOLOMON_a
				CASE PR_SCENE_M7_BYESOLOMON_b
				CASE PR_SCENE_M7_DROPPINGOFFJMY
				CASE PR_SCENE_M7_EMPLOYEECONVO
				CASE PR_SCENE_M7_EXITBARBER
				CASE PR_SCENE_M7_ROUNDTABLE
				CASE PR_SCENE_M7_GETSREADY
				CASE PR_SCENE_M7_HOOKERS
				CASE PR_SCENE_M7_KIDS_GAMING
				CASE PR_SCENE_M7_KIDS_TV
				CASE PR_SCENE_M7_LOT_JIMMY
				CASE PR_SCENE_M7_OPENDOORFORAMA
				CASE PR_SCENE_M7_REJECTENTRY
				CASE PR_SCENE_M7_RESTAURANT
				CASE PR_SCENE_M7_TALKTOGUARD
				CASE PR_SCENE_M7_TRACEYEXITSCAR
				CASE PR_SCENE_F0_GARBAGE
				CASE PR_SCENE_F1_GARBAGE
				CASE PR_SCENE_F0_BIKE
				CASE PR_SCENE_F1_BIKE
				CASE PR_SCENE_F0_CLEANCAR
				CASE PR_SCENE_F1_CLEANCAR
				CASE PR_SCENE_F0_SH_READING
				CASE PR_SCENE_F1_SH_READING
				CASE PR_SCENE_F1_SNACKING
				CASE PR_SCENE_F1_CLEANINGAPT
				CASE PR_SCENE_F1_IRONING
				CASE PR_SCENE_F1_BYETAXI
				CASE PR_SCENE_F1_ONLAPTOP
				CASE PR_SCENE_F1_ONCELL
				CASE PR_SCENE_F0_PLAYCHOP
				CASE PR_SCENE_F0_WALKCHOP
				CASE PR_SCENE_F1_PLAYCHOP
				CASE PR_SCENE_F_WALKCHOP_a
				CASE PR_SCENE_F_WALKCHOP_b
				CASE PR_SCENE_F1_POOLSIDE_a
				CASE PR_SCENE_F1_POOLSIDE_b
				CASE PR_SCENE_F1_NAPPING
				CASE PR_SCENE_F1_WATCHINGTV
				CASE PR_SCENE_F_MD_KUSH_DOC
				CASE PR_SCENE_F_BAR_a_01
				CASE PR_SCENE_F_BAR_b_01
				CASE PR_SCENE_F_BAR_c_02
				CASE PR_SCENE_F_BAR_d_02
				CASE PR_SCENE_F_BAR_e_01
				CASE PR_SCENE_F_CLUB
				CASE PR_SCENE_F_CS_CHECKSHOE
				CASE PR_SCENE_F_CS_WIPEHANDS
				CASE PR_SCENE_F_CS_WIPERIGHT
				CASE PR_SCENE_F_KUSH_DOC_a
				CASE PR_SCENE_F_LAMTAUNT_P1
				CASE PR_SCENE_F_LAMTAUNT_NIGHT
				CASE PR_SCENE_F_LAMTAUNT_P3
				CASE PR_SCENE_F_LAMTAUNT_P5
				CASE PR_SCENE_F_THROW_CUP
				CASE PR_SCENE_F_HIT_CUP_HAND
				CASE PR_SCENE_F0_TANISHAFIGHT
				CASE PR_SCENE_F1_NEWHOUSE
				CASE PR_SCENE_F_KUSH_DOC_b
				CASE PR_SCENE_F_KUSH_DOC_c
				CASE PR_SCENE_T_DOCKS_a
				CASE PR_SCENE_T_DOCKS_b
				CASE PR_SCENE_T_DOCKS_c
				CASE PR_SCENE_T_DOCKS_d
				CASE PR_SCENE_T_FLOYDCRYING_A
				CASE PR_SCENE_T_FLOYDCRYING_E0
				CASE PR_SCENE_T_FLOYDCRYING_E1
				CASE PR_SCENE_T_FLOYDCRYING_E2
				CASE PR_SCENE_T_FLOYDCRYING_E3
				CASE PR_SCENE_T_FLOYDSPOON_A
				CASE PR_SCENE_T_FLOYDSPOON_A2
				CASE PR_SCENE_T_FLOYDSPOON_B
				CASE PR_SCENE_T_FLOYDSPOON_B2
				CASE PR_SCENE_T_FLOYD_BEAR
				CASE PR_SCENE_T_FLOYD_DOLL
				CASE PR_SCENE_T_FLOYDPINEAPPLE
				CASE PR_SCENE_T_HEADINSINK
				#IF NOT IS_JAPANESE_BUILD
				CASE PR_SCENE_T_JERKOFF
				#ENDIF
				CASE PR_SCENE_T_SMOKEMETH
				CASE PR_SCENE_T_SC_MOCKLAPDANCE
				CASE PR_SCENE_T_SC_BAR
				CASE PR_SCENE_T_SC_DRUNKHOWLING
				CASE PR_SCENE_T_STRIPCLUB_out
				CASE PR_SCENE_T6_DIGGING
				CASE PR_SCENE_T6_FLUSHESFOOT
				CASE PR_SCENE_T_FLOYDSAVEHOUSE
				CASE PR_SCENE_T6_SMOKECRYSTAL
				CASE PR_SCENE_T_CR_ALLEYDRUNK
				CASE PR_SCENE_T6_HUNTING1
				CASE PR_SCENE_T6_HUNTING2
				CASE PR_SCENE_T6_HUNTING3
				CASE PR_SCENE_T6_METHLAB
				CASE PR_SCENE_T_CN_PIER
				CASE PR_SCENE_T6_LAKE
				CASE PR_SCENE_T6_TRAF_AIR
				CASE PR_SCENE_T_CR_BRIDGEDROP
				CASE PR_SCENE_T_ESCORTED_OUT
				CASE PR_SCENE_T_CR_FUNERAL
				CASE PR_SCENE_T_CR_LINGERIE
				CASE PR_SCENE_T_PUKEINTOFOUNT
				CASE PR_SCENE_T_CN_PARK_b
				CASE PR_SCENE_T_CR_RAND_TEMPLE
				CASE PR_SCENE_T_THROW_FOOD
				CASE PR_SCENE_T_GARBAGE_FOOD
				CASE PR_SCENE_T_CR_BLOCK_CAMERA
				CASE PR_SCENE_T_GUITARBEATDOWN
				CASE PR_SCENE_T_UNDERPIER
				CASE PR_SCENE_T_CR_RUDEATCAFE
				CASE PR_SCENE_T_FIGHTBBUILD
				CASE PR_SCENE_T_KONEIGHBOUR
				CASE PR_SCENE_T_SCARETRAMP
				CASE PR_SCENE_T_YELLATDOORMAN
				CASE PR_SCENE_T_CN_WAKETRAIN
				CASE PR_SCENE_T_FIGHTBAR_a
				CASE PR_SCENE_T_FIGHTBAR_b
				CASE PR_SCENE_T_FIGHTBAR_c
				CASE PR_SCENE_T_FIGHTYAUCLUB_b
				CASE PR_SCENE_T_FIGHTCASINO
				CASE PR_SCENE_T_NAKED_GARDEN
				CASE PR_SCENE_T_CN_CHATEAU_b
				CASE PR_SCENE_T_CN_CHATEAU_c
				CASE PR_SCENE_T_CR_CHATEAU_d
				CASE PR_SCENE_T_CN_WAKETRASH_b
				CASE PR_SCENE_T_CR_WAKEBEACH
				CASE PR_SCENE_T_CN_WAKEBARN
				CASE PR_SCENE_T_CR_WAKEROOFTOP
					iRealtimeHoursToWaitToChangeClothes = 10
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	#IF NOT IS_JAPANESE_BUILD
	BOOL bSet_Pretty_Trevor_Dress = FALSE
	IF Set_Pretty_Trevor_Dress(eScene, eTorso, eLegs, eFeet, eHand, eJbib, eHeadProp, eEyesProp,
			iCheckAcquired, iLimitedItems, bDoRandom, bMatchingTopAndPants)
		bSet_Pretty_Trevor_Dress = TRUE
		iRealtimeHoursToWaitToChangeClothes = -1
	ENDIF
	#ENDIF
	
	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN FALSE
	ELSE
//		TIMEOFDAY sLastChangedOutfits = GET_TIME_PLAYER_PED_LAST_CHANGED_CLOTHES(ePed)
//		IF Is_TIMEOFDAY_Valid(sLastChangedOutfits)
//			IF iRealtimeHoursToWaitToChangeClothes > 0
//				
//				TEXT_LABEL_63 tLastChangedOutfits
//				#IF IS_DEBUG_BUILD
//				tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(sLastChangedOutfits)
//				#ENDIF
//				#IF IS_FINAL_BUILD
//				tLastChangedOutfits = tLastChangedOutfits
//				#ENDIF
//				
//				INT iGameHoursToWaitToChangeClothes = iRealtimeHoursToWaitToChangeClothes * 30
//				IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastChangedOutfits, iGameHoursToWaitToChangeClothes)
//					
//					#IF IS_DEBUG_BUILD
//					INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
//					GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastChangedOutfits, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
//					
//					CPRINTLN(DEBUG_PED_COMP & DEBUG_SWITCH,"dont change outfits - not enough time has passed since [",
//							GET_PLAYER_PED_STRING(ePed),
//							"] clothes changed [", tLastChangedOutfits, ", ",
//							iMinutes, "m ", iHours, "h ",
//							iDays, "d ", iMonths+(iYears*12), "m / ", iGameHoursToWaitToChangeClothes, "gamehrs]")
//					#ENDIF
//					
//					iOutfitPreload = -1
//					RETURN FALSE
//				ELSE
//					
//					#IF IS_DEBUG_BUILD
//					INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
//					GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastChangedOutfits, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
//					
//					CPRINTLN(DEBUG_PED_COMP & DEBUG_SWITCH, "allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePed), "] is less than 2hrs [", tLastChangedOutfits, ", ",
//							iMinutes, "m ", iHours, "h ",
//							iDays, "d ", iMonths+(iYears*12), "m / ", iGameHoursToWaitToChangeClothes, "gamehrs]")
//					#ENDIF
//				ENDIF
//			ELSE
//				CPRINTLN(DEBUG_PED_COMP & DEBUG_SWITCH, "allow change outfits - force for scene")
//			ENDIF
//		ELSE
//			CPRINTLN(DEBUG_PED_COMP & DEBUG_SWITCH, "allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePed), "] is null")
//		ENDIF
	
		IF NOT HAVE_REALTIME_HOURS_PASSED_SINCE_PED_LAST_CHANGED_CLOTHES(ePed, iRealtimeHoursToWaitToChangeClothes)
//			iOutfitPreload = -1
//			RETURN FALSE
			
			eTorso = DUMMY_PED_COMP
			eLegs = DUMMY_PED_COMP
			eFeet = DUMMY_PED_COMP
		
		ENDIF
	ENDIF
	
	IF SET_PED_COMPONENT_FOR_MISSION(pedIndex)
		CPRINTLN(DEBUG_SWITCH, "SET_PED_COMPONENT_FOR_MISSION!!!")
		iOutfitPreload = -1
		RETURN FALSE
	ENDIF
	
	SWITCH iOutfitPreload
		CASE 0
	
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "SET_PED_PRESET_OUTFIT_FOR_SCENE(", Get_String_From_Ped_Request_Scene_Enum(eScene), ", ", GET_PLAYER_PED_STRING(ePed), ")")
			#ENDIF
			
			IF eTorso = DUMMY_PED_COMP
			AND eLegs = DUMMY_PED_COMP
			AND eFeet = DUMMY_PED_COMP
				IF IS_PLAYER_SWITCH_IN_PROGRESS()
				AND GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT
					
					
					SP_MISSIONS eResetOutfitMPSwitchReturn
					IF RESET_PED_VARIATIONS_AFTER_MP_SWITCH_OUT_OF_MISSION(ePed, eResetOutfitMPSwitchReturn)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "reset ", GET_PLAYER_PED_STRING(ePed), " stored outfit - switched out of ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eResetOutfitMPSwitchReturn))
						#ENDIF
						#IF NOT IS_DEBUG_BUILD
						eResetOutfitMPSwitchReturn = SP_MISSION_NONE
						#ENDIF
						
						SETUP_DEFAULT_PLAYER_VARIATIONS(ePed)
					ENDIF
					
					PRELOAD_STORED_PLAYER_PED_VARIATION(pedIndex)

					CPRINTLN(DEBUG_SWITCH, "iOutfitPreload 0 = 1 (PRELOAD_STORED_PLAYER_PED_VARIATION)")
					iOutfitPreload = 1
					RETURN FALSE
				ENDIF
				
				CPRINTLN(DEBUG_SWITCH, "iOutfitPreload 0 = -1 (no preload, not dummy)")
				
				iOutfitPreload = -1
				RETURN FALSE
			ELSE
				// For each bug I'll let you know what torso, leg and feet to use.
				// Pass them in to this function:
				FILL_OUTFIT_FOR_SWITCH_SCENE(pedIndex, eTorso, eLegs, eFeet, eHand, eSpecial, eJbib, eHeadProp, eEyesProp, iLimitedItems, bDoRandom, iCheckAcquired, bMatchingTopAndPants)
				
				// Then when you need to preload it call this:
				PRELOAD_OUTFIT_FROM_STRUCT(pedIndex, GET_ENTITY_MODEL(pedIndex), g_sTempOutfitData)
				
				CPRINTLN(DEBUG_SWITCH, "iOutfitPreload 0 = 1 (PRELOAD_OUTFIT_FROM_STRUCT)")
				iOutfitPreload = 1
			ENDIF
		BREAK
		CASE 1
			// Then if you're waiting for the preload to finish you need these commands:
			// (might not need the prop one yet)
			IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedIndex)
			AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(pedIndex)
				
				IF eTorso = DUMMY_PED_COMP
				AND eLegs = DUMMY_PED_COMP
				AND eFeet = DUMMY_PED_COMP
					RESTORE_PLAYER_PED_VARIATIONS(pedIndex)
				ELSE
					//Then to equip the outfit: (The true on the end here tells it to ignore the OUTFIT_DEFAULT and instead use the outfit we've just built: g_sTempOutfitData)
					SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE, -1, TRUE)
				ENDIF
				
				//And finally release the preload data.
				RELEASE_PED_PRELOAD_VARIATION_DATA(pedIndex)
				RELEASE_PED_PRELOAD_PROP_DATA(pedIndex)
				
				STORE_PLAYER_PED_VARIATIONS(pedIndex)
				
				#IF NOT IS_JAPANESE_BUILD
				IF bSet_Pretty_Trevor_Dress
					g_SavedGlobals.sPlayerSceneData.g_bSeenTrevorsPrettyDress = TRUE
				ENDIF
				#ENDIF
				
				CPRINTLN(DEBUG_SWITCH, "iOutfitPreload 1 = -1 ")
				iOutfitPreload = -1
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_SWITCH, "SET_PED_PRESET_OUTFIT_FOR_SCENE failing to preload... ")
			ENDIF
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SET_PED_DAMAGE_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, PED_INDEX pedIndex)
	
	IF IS_ENTITY_DEAD(pedIndex)
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "SET_PED_DAMAGE_FOR_SCENE(", Get_String_From_Ped_Request_Scene_Enum(eScene), ") - entity is dead!")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "SET_PED_DAMAGE_FOR_SCENE(", Get_String_From_Ped_Request_Scene_Enum(eScene), ")")
	#ENDIF
	
	SWITCH eScene
		
		//ignore ped variation for default switch
		CASE PR_SCENE_M_DEFAULT
		CASE PR_SCENE_F_DEFAULT
		CASE PR_SCENE_T_DEFAULT
			RETURN FALSE
		BREAK
		
		CASE PR_SCENE_T_NAKED_ISLAND	//1007927
			/* 1224263
			APPLY_PED_BLOOD_BY_ZONE(pedIndex, ENUM_TO_INT(PDZ_HEAD),		0.360, 0.710, "ShotgunSmall")
			APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedIndex, PDZ_HEAD, 				0.810, 0.733, BDT_STAB)
			
			APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedIndex, PDZ_TORSO, 			0.940, 0.590, BDT_SHOTGUN_LARGE)
			
			APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedIndex, PDZ_RIGHT_ARM,			0.240, 0.620, BDT_STAB)
			APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedIndex, PDZ_LEFT_ARM,			0.000, 0.150, BDT_SHOTGUN_SMALL)
			
			APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedIndex, PDZ_RIGHT_LEG,			0.460, 0.853, BDT_SHOTGUN_LARGE)
			APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedIndex, PDZ_LEFT_LEG,			0.308, 0.786, BDT_SHOTGUN_LARGE)
			
			APPLY_PED_BLOOD_SPECIFIC(pedIndex, ENUM_TO_INT(PDZ_RIGHT_ARM),	0.375, 0.398, 000, 1.0, -1, 0.0, "BasicSlash")
			APPLY_PED_BLOOD_SPECIFIC(pedIndex, ENUM_TO_INT(PDZ_RIGHT_ARM),	0.500, 0.600, 000, 1.0, -1, 0.0, "BasicSlash")
			
			APPLY_PED_BLOOD_SPECIFIC(pedIndex, ENUM_TO_INT(PDZ_RIGHT_ARM),	0.600, 0.250, 050, 0.5, -1, 0.0, "BasicSlash")
			APPLY_PED_BLOOD_SPECIFIC(pedIndex, ENUM_TO_INT(PDZ_RIGHT_ARM),	0.650, 0.325, 050, 0.5, -1, 0.0, "BasicSlash")
			
			APPLY_PED_BLOOD_SPECIFIC(pedIndex, ENUM_TO_INT(PDZ_TORSO),		0.580, 0.704, 000, 1.0, -1, 0.0, "ShotgunLarge")
			*/
			
			APPLY_PED_DAMAGE_PACK(pedIndex, "Skin_Melee_0", 0.0, 1.0)
			
			
			RETURN TRUE
		BREAK
		
		
		
		DEFAULT
			//
		BREAK
	ENDSWITCH
	
	CLEAR_PED_WETNESS(pedIndex)
	CLEAR_PED_BLOOD_DAMAGE(pedIndex)
	RESET_PED_VISIBLE_DAMAGE(pedIndex)
	
	RETURN FALSE
ENDFUNC

PROC SET_PED_SCUBA_FOR_SCENE(PED_INDEX ped, enumCharacterList ePed)

	CDEBUG3LN(DEBUG_SWITCH, " - player_scene_component - SET_PED_SCUBA_FOR_SCENE - Checking ped (enumCharList): ", ePed, " for g_sDefaultPlayerSwitchState.bPlayerInScuba flag (", PICK_STRING(g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba, "TRUE", "FALSE"), ").")
	
	IF g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba
		
		FLOAT fUnneededWaterHeight
		CDEBUG3LN(DEBUG_SWITCH, " - player_scene_component - SET_PED_SCUBA_FOR_SCENE - Ped in water (", PICK_STRING(IS_ENTITY_IN_WATER(ped), "TRUE", "FALSE"), ") and if TEST_VERTICAL_PROBE_AGAINST_ALL_WATER = ", TEST_VERTICAL_PROBE_AGAINST_ALL_WATER(GET_ENTITY_COORDS(ped), -1, fUnneededWaterHeight), ". ( 1 = SCRIPT_WATER_TEST_RESULT_WATER ).")
		
		IF IS_ENTITY_IN_WATER(ped)
		OR TEST_VERTICAL_PROBE_AGAINST_ALL_WATER(GET_ENTITY_COORDS(ped), -1, fUnneededWaterHeight) = SCRIPT_WATER_TEST_RESULT_WATER
			SET_PED_SCUBA_GEAR_VARIATION(ped)
			SET_ENABLE_SCUBA(ped, TRUE)
			CDEBUG1LN(DEBUG_SWITCH, " - player_scene_component - SET_PED_SCUBA_FOR_SCENE - Setting scuba gear onto ped.")
		ELSE
			CLEAR_PED_SCUBA_GEAR_VARIATION(ped)
			SET_ENABLE_SCUBA(ped, FALSE)
			CDEBUG1LN(DEBUG_SWITCH, " - player_scene_component - SET_PED_SCUBA_FOR_SCENE - Player was set with scuba, but now not in water so not setting scuba gear on.")
		ENDIF
		
		CDEBUG3LN(DEBUG_SWITCH, " - player_scene_component - SET_PED_SCUBA_FOR_SCENE - Resetting g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba to FALSE.")
		g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba = FALSE
	ENDIF	
	
ENDPROC

FUNC BOOL PRIVATE_SetDefaultSceneBuddyCompVar(PED_INDEX &pedIndex, enumCharacterList eSceneBuddy, PED_REQUEST_SCENE_ENUM eScene)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "scene buddy[", Get_String_From_Ped_Request_Scene_Enum(eScene), ":", GET_PLAYER_PED_STRING(eSceneBuddy), "] component variation")
	#ENDIF
	
	SWITCH eScene
		CASE PR_SCENE_M2_BEDROOM
		CASE PR_SCENE_M2_SAVEHOUSE0_b
			SetOutfitForFamilyMember(pedIndex, FC_AMANDA_OUTFIT_sleeping2)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_GETSREADY
			SetOutfitForFamilyMember(pedIndex, FC_AMANDA_OUTFIT_sleeping7)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE
			SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_WIFE, FE_M_WIFE_gets_drink_in_kitchen)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
//		CASE PR_SCENE_M7_LUNCH_b
		CASE PR_SCENE_M2_WIFEEXITSCAR
		CASE PR_SCENE_M7_OPENDOORFORAMA
			SetOutfitForFamilyMember(pedIndex, FC_AMANDA_OUTFIT_leavingGlasses)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			SetOutfitForFamilyMember(pedIndex, FC_AMANDA_OUTFIT_tennis_a)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			SetOutfitForFamilyMember(pedIndex, FC_AMANDA_OUTFIT_sunbathing)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
		CASE PR_SCENE_M7_FAKEYOGA
			SetOutfitForFamilyMember(pedIndex, FC_AMANDA_OUTFIT_tennis_a)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a
			SetOutfitForFamilyMember(pedIndex, FC_TRACEY_OUTFIT_goingOut)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
			SetOutfitForFamilyMember(pedIndex, FC_TRACEY_OUTFIT_goingOut_b)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b
			SetOutfitForFamilyMember(pedIndex, FC_TRACEY_OUTFIT_audition)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_SON, FE_M7_SON_gaming)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOT_JIMMY
			SetOutfitForFamilyMember(pedIndex, FC_JIMMY_OUTFIT_movieLot)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a
		CASE PR_SCENE_M2_DROPOFFSON_b
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_SON, FE_M7_SON_going_for_a_bike_ride)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_SON, FE_M7_SON_coming_back_from_a_bike_ride)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_KIDS_TV
			SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_SON, FE_M2_SON_watching_TV)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_TV
			IF (eSceneBuddy = CHAR_JIMMY)		//1546639
				SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_SON, FE_M7_SON_watching_TV_with_tracey)
				RETURN TRUE
			ENDIF
			IF (eSceneBuddy = CHAR_TRACEY)
				SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_DAUGHTER, FE_M7_SON_watching_TV_with_tracey)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			IF (eSceneBuddy = CHAR_JIMMY)
				SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_SON, FE_M7_FAMILY_finished_breakfast)
				RETURN TRUE
			ENDIF
			IF (eSceneBuddy = CHAR_TRACEY)
				SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_DAUGHTER, FE_M7_FAMILY_finished_breakfast)
				RETURN TRUE
			ENDIF
			IF (eSceneBuddy = CHAR_AMANDA)
				SetFamilyMemberComponentVariation(pedIndex, FM_MICHAEL_WIFE, FE_M7_FAMILY_finished_breakfast)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_F_LAMTAUNT_P1
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HEAD,	0, 1)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_BERD,	2, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAND,	0, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAIR,	4, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_TORSO,	2, 2)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_LEG,		5, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_FEET,	1, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_DECL,	1, 0)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAND, 2, 0)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HEAD,	0, 1)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_BERD,	2, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAND,	0, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAIR,	2, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_TORSO,	2, 1)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_LEG,		4, 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_FEET,	0, 0)
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_FLOYDSPOON_A		FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B		FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B2		FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_A2
			SetFamilyMemberComponentVariation(pedIndex, FM_TREVOR_1_FLOYD, FE_T1_FLOYD_cries_in_foetal_position)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR			FALLTHRU
		CASE PR_SCENE_T_FLOYD_DOLL
			SetOutfitForFamilyMember(pedIndex, FC_FLOYD_OUTFIT_work)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			SetFamilyMemberComponentVariation(pedIndex, FM_TREVOR_1_FLOYD, FE_T1_FLOYD_pineapple)
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_FLOYDCRYING_A			//#1547723
		CASE PR_SCENE_T_FLOYDCRYING_E2			//#1547723
			SetOutfitForFamilyMember(pedIndex, FC_FLOYD_OUTFIT_sleeping)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E1			//#1547723
			SetOutfitForFamilyMember(pedIndex, FC_FLOYD_OUTFIT_casual)
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0		FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E3		
			SetFamilyMemberComponentVariation(pedIndex, FM_TREVOR_1_FLOYD, FE_T1_FLOYD_cries_on_sofa)
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF (eSceneBuddy = CHAR_AMANDA)
	OR (eSceneBuddy = CHAR_TRACEY)
	OR (eSceneBuddy = CHAR_JIMMY)
	OR (eSceneBuddy = CHAR_FLOYD)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "scene buddy[", Get_String_From_Ped_Request_Scene_Enum(eScene), ":", GET_PLAYER_PED_STRING(eSceneBuddy), "] random component variation")
		#ENDIF
		
		SET_PED_RANDOM_COMPONENT_VARIATION(pedIndex)
		
		RETURN TRUE
	ENDIF
	IF (eSceneBuddy = CHAR_LAMAR)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "scene buddy[", Get_String_From_Ped_Request_Scene_Enum(eScene), ":", GET_PLAYER_PED_STRING(eSceneBuddy), "] random component variation")
		#ENDIF
		
		IF (GET_PED_DRAWABLE_VARIATION(pedIndex, PED_COMP_TORSO) = 0)
			SET_PED_COMPONENT_VARIATION(pedIndex, PED_COMP_HAND, 2, 0)
		ENDIF
		
		RETURN TRUE
	ENDIF
	IF (eSceneBuddy = CHAR_RON)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "scene buddy[", Get_String_From_Ped_Request_Scene_Enum(eScene), ":", GET_PLAYER_PED_STRING(eSceneBuddy), "] random component variation")
		#ENDIF
		
		SetFamilyMemberComponentVariation(pedIndex, FM_TREVOR_0_RON, NO_FAMILY_EVENTS)
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "scene buddy[", Get_String_From_Ped_Request_Scene_Enum(eScene), "] DEFAULT component variation")
	#ENDIF
	
	SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
	RETURN FALSE
ENDFUNC

