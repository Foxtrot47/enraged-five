//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 		 Load Queue Private Header							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			23/06/14												│
//│		DESCRIPTION: 	The private commands for queuing up load				│
//│						requests to be scheduled over multiple frames.			│								
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_audio.sch"
USING "commands_script.sch"
USING "load_queue_private.sch"


STRUCT LoadQueueDynamic
	LoadQueueData sQueue[LOAD_QUEUE_SIZE_DYNAMIC]
	BOOL bLoading
	INT iLastFrame
	INT iFrameDelay = 1
ENDSTRUCT

PROC PRIVATE_Load_Queue_Dynamic_Add_Data(LoadQueueDynamic &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, INT paramBitSettings = 0)
	CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to queue load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in dynamic queue.")

	//First check this item isn't already queued.
	INT i
	REPEAT LOAD_QUEUE_SIZE_DYNAMIC i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[i].iData = paramIntData
						CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
						EXIT
					ENDIF
				ENDIF
				IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
					IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
						IF ARE_STRINGS_EQUAL(paramQueue.sQueue[i].txtData, paramStringData)
							CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
							EXIT
						ENDIF
					ENDIF
				ENDIF
				IF paramType = LQT_PTFX_ASSET
					CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Already in queue.")
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Flag the queue as loading if it isn't already.
	IF NOT paramQueue.bLoading
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Flagged queue as loading.")
		paramQueue.bLoading = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queuing load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] in dynamic queue.")

	//Find a free slot in the queue and add the load request.
	REPEAT LOAD_QUEUE_SIZE_DYNAMIC i
		IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found slot ", i, " as free.")
			paramQueue.sQueue[i].txtData = paramStringData
			paramQueue.sQueue[i].iData = paramIntData
			paramQueue.sQueue[i].iState = paramBitSettings
			SET_BIT(paramQueue.sQueue[i].iState, ENUM_TO_INT(paramType))
			SET_BIT(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			EXIT
		ENDIF
	ENDREPEAT
	
	//No free slots.
	SCRIPT_ASSERT("PRIVATE_Request_Queue_Dynamic_Add_Data: The queue was full. Do we need to create a dynamicr queue size?")
ENDPROC

PROC PRIVATE_Load_Queue_Dynamic_Remove_Data(LoadQueueDynamic &paramQueue, LoadQueueType paramType, INT paramIntData, STRING paramStringData, BOOL paramSetAsNoLongerNeeded = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Attempting to remove load request [", PRIVATE_Get_Load_Queue_Type_Debug_String(paramType), "|", paramIntData, "|", paramStringData, "] from dynamic queue.")

	//Find the array index this item is at.
	INT iSearchIndex = 0
	BOOL bFoundItem = FALSE
	WHILE NOT bFoundItem AND iSearchIndex < LOAD_QUEUE_SIZE_DYNAMIC
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, ENUM_TO_INT(paramType))
				IF paramIntData != -1
					IF paramQueue.sQueue[iSearchIndex].iData = paramIntData
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType <> LQT_VEH_REC // vehicle recs for the same mission will often have the same stringdata
						IF NOT ARE_STRINGS_EQUAL(paramStringData, "NULL")
							IF ARE_STRINGS_EQUAL(paramQueue.sQueue[iSearchIndex].txtData, paramStringData)
								CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
								bFoundItem = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT bFoundItem
					IF paramType = LQT_PTFX_ASSET
						CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Found item at queue index ", iSearchIndex, ".")
						bFoundItem = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bFoundItem
			iSearchIndex++
		ENDIF
	ENDWHILE
	
	IF NOT bFoundItem
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> The item was not found in the queue. Nothing to remove.")
		EXIT
	ENDIF
	
	//Remove the item from memory if requested.
	IF paramSetAsNoLongerNeeded
		CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Unloading item from memory as we remove it.")
		PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[iSearchIndex])
	ENDIF
	
	//Iterate through items at higher indexes in the queue and move them up one space.
	BOOL bFoundQueueEnd = FALSE
	WHILE NOT bFoundQueueEnd AND iSearchIndex < (LOAD_QUEUE_SIZE_DYNAMIC - 1)
		iSearchIndex++
		
		//Is the next queue item in use?
		IF IS_BIT_SET(paramQueue.sQueue[iSearchIndex].iState, BIT_LQD_USED)
			//Yes, copy it down one space.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Copying queue index ", iSearchIndex, " over index ", iSearchIndex - 1, ".")
			PRIVATE_Copy_Load_Queue_Data(paramQueue.sQueue[iSearchIndex], paramQueue.sQueue[iSearchIndex-1])
		ELSE
			//No, we've found the end of the used queue.
			CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was not in use. Clearing index ", iSearchIndex -1, ".")
			PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex-1])
			bFoundQueueEnd = TRUE
		ENDIF
	ENDWHILE
	
	IF NOT bFoundQueueEnd
		// clear the index we just removed at the queue end
		CDEBUG3LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Queue index ", iSearchIndex, " was the last index in queue. Clearing index ", iSearchIndex , ".")
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[iSearchIndex])
	ENDIF
ENDPROC
