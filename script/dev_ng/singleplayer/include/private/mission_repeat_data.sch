//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 14/09/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					Mission Repeat System - Public Script Interface				│
//│																				│
//│			This header contains all public script commands used by				│
//│			the mission repeat system. This system allows replaying of 			│
//│			missions that have already been completed by the player.			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_stats.sch"
USING "commands_dlc.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "randomchar_private_gta5.sch"
#endif
#endif
USING "commands_decorator.sch"
USING "mission_repeat_public.sch"

#IF USE_CLF_DLC
USING "mission_stat_generated_private_CLF.sch"
#ENDIF
#IF NOT USE_CLF_DLC
USING "mission_stat_generated_private.sch"
#ENDIF

/// PURPOSE:
///    Finds the story mission that has the lowest completion order (that is at least the minimum passed in)
/// PARAMS:
///    iMinCompOrder - the minimum value the missions' completion order can be
///    bRepeatableOnly - only include missions that are repeatable
///    eMissionType - Story missions or RC missions
/// RETURNS:
///    the mission index of the mission with the lowest completion order (that is at least the minimum passed in)
///    Or -1 if no suitable mission found
FUNC INT FindLowestCompletionOrderMission(INT iMinCompOrder, BOOL bRepeatableOnly, enumGrouping eMissionType)
	INT iMissionDataIndex = 0
	INT iLowestCompOrder = -1
	INT iLowestMission = -1
	INT iCompletionOrder
	INT iMaxIndex
	
	iMaxIndex = GetMaxMissionsOfType(eMissionType)
	
	REPEAT iMaxIndex  iMissionDataIndex

		IF HasMissionBeenCompleted(iMissionDataIndex, eMissionType) = TRUE
			IF bRepeatableOnly = FALSE 
			OR IsMissionRepeatable(iMissionDataIndex, eMissionType) = TRUE
		
				// Get this missions completion order
				iCompletionOrder = GetMissionCompletionOrder(iMissionDataIndex, eMissionType)

				IF iCompletionOrder >= iMinCompOrder
					// Comp order is at least the min required
					
					IF iCompletionOrder < iLowestCompOrder
					OR iLowestCompOrder = -1
						// Comp order is new lowest (or we don't currently have a lowest set)
						iLowestCompOrder = iCompletionOrder
						iLowestMission = iMissionDataIndex
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDREPEAT
	
	CPRINTLN(DEBUG_REPEAT, "Found next lowest mission as ", iLowestMission, ".")
	RETURN iLowestMission 
ENDFUNC


/// PURPOSE:
///    Adds all of the completed missions of specified type to the repeat play array
/// PARAMS:
///    mRepeatPlayArray - array to populate
///    iRepeatableMissions - counts number of repeatable missions
///    eMissionType - type of mission to add (story / RC)
///    bRepeatableOnly - only add repeatable missions?
///    bFillGaps - only set this to true if you are only using one mission type in this array- 
///    this fills gaps so all acceptable missions are stored at start of the array
PROC AddMissionsOfTypeToRepeatPlayArray(RepeatPlayData &mRepeatPlayArray[], INT &iRepeatableMissions, enumGrouping eMissionType, BOOL bRepeatableOnly, BOOL bFillGaps)

	INT	iMaxIndex = GetMaxMissionsOfType(eMissionType)
	INT iMissionDataIndex
	INT iCompletionORder
	#IF IS_DEBUG_BUILD TEXT_LABEL_7 tMissionLabel #ENDIF
	
	CPRINTLN(DEBUG_REPEAT, "----------------------------------")
	CPRINTLN(DEBUG_REPEAT, "STARTING REPEAT MISSION LIST BUILD")
	CPRINTLN(DEBUG_REPEAT, "----------------------------------")
	
	IF bFillGaps = FALSE
		CPRINTLN(DEBUG_REPEAT, "Not filling gaps.")

		// Find all completed story missions and put them in the array
		REPEAT iMaxIndex iMissionDataIndex
		
			CPRINTLN(DEBUG_REPEAT, "Processing data index ", iMissionDataIndex, ".")
			// --------------- Standard mission check -----------------
			//Find completed missions.
			IF HasMissionBeenCompleted(iMissionDataIndex, eMissionType)
		
				iCompletionOrder = GetMissionCompletionOrder(iMissionDataIndex, eMissionType)
				
				CPRINTLN(DEBUG_REPEAT, "Found a standard mission with completion order ", iCompletionOrder, ".")
					
				IF iCompletionOrder = -1
					#IF IS_DEBUG_BUILD
						tMissionLabel = GetRepeatPlayMissionName(iMissionDataIndex, eMissionType)
						CPRINTLN(DEBUG_REPEAT, "AddMissionsOfTypeToRepeatPlayArray: Invalid completion order for: ", tMissionLabel)
						SCRIPT_ASSERT("AddMissionsOfTypeToRepeatPlayArray: Mission set as completed but has completion order of -1")
					#ENDIF
				ELSE
					// Store the mission if its repeatable, or if we are including non-repeatable missions too
					IF bRepeatableOnly = FALSE 
					OR IsMissionRepeatable(iMissionDataIndex, eMissionType) = TRUE
						mRepeatPlayArray[iCompletionOrder].iMissionIndex = iMissionDataIndex
						mRepeatPlayArray[iCompletionOrder].eMissionType = eMissionType
						CPRINTLN(DEBUG_REPEAT, "Stored mission in list.")
#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_REPEAT, "Mission not repeatable.")
#ENDIF
					ENDIF

					IF IsMissionRepeatable(iMissionDataIndex, eMissionType) = TRUE
						iRepeatableMissions++ // count number of repeatable missions the player has completed- used to display in the phone
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		CPRINTLN(DEBUG_REPEAT, "Filling gaps.")
	
		// Fill gaps-so all acceptable missions are stored at start of the array
		INT iSlot
		INT iLowestCompletionOrder
		INT iLowestMission
	
		iLowestCompletionOrder = 0
		
		FOR iSlot = 0 TO iMaxIndex -1
			CPRINTLN(DEBUG_REPEAT, "Processing slot ", iSlot, ".")
			
			// find the mission with the next lowest completion order
			iLowestMission = FindLowestCompletionOrderMission(iLowestCompletionOrder, bRepeatableOnly, eMissionType)
			IF iLowestMission <> -1
				// found a valid mission, store its info
				iLowestCompletionOrder = GetMissionCompletionOrder(iLowestMission, eMissionType) + 1

				mRepeatPlayArray[iSlot].iMissionIndex = iLowestMission
				mRepeatPlayArray[iSlot].eMissionType = eMissionType

				IF IsMissionRepeatable(iLowestMission, eMissionType)
					iRepeatableMissions++ // count number of repeatable missions the player has completed- used to display in the phone
				ELSE
					CPRINTLN(DEBUG_REPEAT, "Mission not repeatable.")
				ENDIF
			ELSE
				// didn't find a next lowest mission...early out
				CPRINTLN(DEBUG_REPEAT,"AddMissionsOfTypeToRepeatPlayArray didn't find a next lowest mission...early out")
				iSlot = iMaxIndex + 1
			ENDIF
		ENDFOR
	ENDIF
ENDPROC


/// PURPOSE:
///    Populates an array with repeat play data (mission index + type). Sorted in order the missions were completed.
/// PARAMS:
///    mRepeatPlayArray - the array you want to populate
///    bIncludeStoryMissions - are we including completed story missions?
///    bIncludeRCMissions - are we including completed RC missions?
///    bRepeatableOnly - only include missions that are set to be repeatable?
/// RETURNS:
///    Number of repeatable missions that are now stored in the array.
FUNC INT PopulateRepeatPlayArray(RepeatPlayData &mRepeatPlayArray[], BOOL bIncludeStoryMissions, BOOL bIncludeRCMissions, BOOL bRepeatableOnly)
	INT iRepeatableMissions = 0

	IF bIncludeStoryMissions = TRUE
		IF bIncludeRCMissions = TRUE
			CPRINTLN(DEBUG_REPEAT,"PopulateRepeatPlayArray: Story Missions + RC Missions")
		
			// Find all completed story missions and put them in the array
			AddMissionsOfTypeToRepeatPlayArray(mRepeatPlayArray, iRepeatableMissions, CP_GROUP_MISSIONS, bRepeatableOnly, FALSE)
			
			// Find all completed RC missions and put them in the array
			AddMissionsOfTypeToRepeatPlayArray(mRepeatPlayArray, iRepeatableMissions, CP_GROUP_RANDOMCHARS, bRepeatableOnly, FALSE)
		ELSE
			CPRINTLN(DEBUG_REPEAT,"PopulateRepeatPlayArray: Story Missions Only")
			
			// Find all completed story missions and put them in the array- filling gaps as we go
			AddMissionsOfTypeToRepeatPlayArray(mRepeatPlayArray, iRepeatableMissions, CP_GROUP_MISSIONS, bRepeatableOnly, TRUE)
		ENDIF
	ELSE
		IF bIncludeRCMissions = TRUE
			CPRINTLN(DEBUG_REPEAT,"PopulateRepeatPlayArray: RC Missions only")
		
			// Find all completed RC missions and put them in the array- filling gaps as we go
			AddMissionsOfTypeToRepeatPlayArray(mRepeatPlayArray, iRepeatableMissions, CP_GROUP_RANDOMCHARS, bRepeatableOnly, TRUE)
		ELSE
			CPRINTLN(DEBUG_REPEAT,"PopulateRepeatPlayArray: No mission types selected!")
		ENDIF
	ENDIF

	RETURN iRepeatableMissions
ENDFUNC
