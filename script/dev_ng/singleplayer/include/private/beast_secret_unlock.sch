USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_camera.sch"
USING "candidate_public.sch"
USING "beast_secret_shared.sch"
USING "flow_help_public.sch"

#IF FEATURE_SP_DLC_BEAST_SECRET

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════    The Beast Unlock     ═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT	BU_BIT_SWAP_MODE_ACTIVE				3	//Is swap mode active?
CONST_INT	BU_BIT_SWAPPED_TO_BEAST				6	//Have we swapped the player model?
CONST_INT	BU_BIT_STARTED_FADE					8	//Did this script instigate a fade out?
CONST_INT	BU_BIT_ASSETS_REQUESTED				12	//Have we started loading the beast model and other assets?
CONST_INT	BU_BIT_SWAPPING_MODEL				16	//Are we in the process of swapping the player model?
CONST_INT	BU_BIT_WAITING_FOR_FADE				17	//Are we waiting for the screen to fade before starting a swap?
CONST_INT	BU_BIT_JUMPING_SOUND_PLAYING		18	//Are we playing the beast super jump sound?
CONST_INT	BU_BIT_LANDING_SOUND_PLAYING		19	//Are we playing the beast super jump landing sound?
CONST_INT	BU_BIT_SASQUATCH_SELECTED			29	//Has the player chosen to be the Sasquatch instead of the beast?
CONST_INT	BU_BIT_IN_CONTROL_OF_MODEL			30	//Is this system in control of the model right now?


STRUCT BeastUnlockVars
	INT iState
	INT iTimeNextInput
	INT iTimeLastInTrailer
	BOOL bTransitionHelpShown
	PED_INDEX pedsNearby[16]
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugTerminateThread
		BOOL bDebugSecretIsUnlocked
		BOOL bDebugToggleSecretUnlocked
	#ENDIF
ENDSTRUCT


PROC Reset_Beast_Unlock_Variables(BeastUnlockVars &sBeastUnlockVars)
	sBeastUnlockVars.iState = 0
	sBeastUnlockVars.iTimeNextInput = 0
	sBeastUnlockVars.bTransitionHelpShown = FALSE
ENDPROC


PROC Request_Beast_Unlock_Assets(BeastUnlockVars &sBeastUnlockVars)
	CPRINTLN(DEBUG_HUNTING, "Requesting ***** unlock assets.")
	REQUEST_MODEL(Get_Beast_Model())
	REQUEST_MODEL(Get_Sasquatch_Model())
	SET_BIT(sBeastUnlockVars.iState, BU_BIT_ASSETS_REQUESTED)
ENDPROC


FUNC BOOL Have_Beast_Unlock_Assets_Loaded()
	IF HAS_MODEL_LOADED(Get_Beast_Model())
	AND HAS_MODEL_LOADED(Get_Sasquatch_Model())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC Release_Beast_Unlock_Assets(BeastUnlockVars &sBeastUnlockVars)
	CPRINTLN(DEBUG_HUNTING, "Releasing ***** unlock assets.")
	SET_MODEL_AS_NO_LONGER_NEEDED(Get_Beast_Model())
	SET_MODEL_AS_NO_LONGER_NEEDED(Get_Sasquatch_Model())
	CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_ASSETS_REQUESTED)
ENDPROC


FUNC MODEL_NAMES Get_Player_Model_Beast_Unlock()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN GET_ENTITY_MODEL(PLAYER_PED_ID())
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


PROC EnableBeastAbilities()
	CPRINTLN(DEBUG_HUNTING, "Enabling **** special abilities.")
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, TRUE)
//    SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 0.1)
    SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 15000)
    SET_ENTITY_HEALTH(PLAYER_PED_ID(), 15000)
ENDPROC


PROC Maintain_Beast_Superjump(BeastUnlockVars &sBeastUnlockVars)
	SET_BEAST_JUMP_THIS_FRAME(PLAYER_ID())
	SET_SUPER_JUMP_THIS_FRAME(PLAYER_ID())
	IF IS_PED_DOING_A_BEAST_JUMP(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_DisableParachuting, TRUE)
		TEXT_LABEL_63 temp1, temp2
		//Play super jump audio
		IF NOT (IS_PED_CLIMBING(PLAYER_PED_ID()) OR IS_PED_LANDING(PLAYER_PED_ID()))
		AND NOT IS_BIT_SET(sBeastUnlockVars.iState,BU_BIT_JUMPING_SOUND_PLAYING)
			CPRINTLN(debug_hunting, "Started playing the super jump sound")
			temp1 = Build_Beast_String_63("as","Be","t_j","ump") 		// "Beast_Jump"
			temp2 = Build_Beast_String_63("T_Bv","AP","dset","S_Soun")	// "APT_BvS_Soundset")
			PLAY_SOUND_FROM_ENTITY(-1, temp1, PLAYER_PED_ID(), temp2)	
			
			SET_BIT(sBeastUnlockVars.iState,BU_BIT_JUMPING_SOUND_PLAYING)
		ENDIF
		//Play landing audio
		IF IS_PED_LANDING(PLAYER_PED_ID())
			IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID()) AND NOT IS_BIT_SET(sBeastUnlockVars.iState,BU_BIT_LANDING_SOUND_PLAYING)
				CPRINTLN(debug_hunting,"Playing super jump landing sound")
				temp1 = Build_Beast_String_63("st_J","Bea","and","ump_L") 	//"Beast_Jump_Land"
				temp2 = Build_Beast_String_63("T_Bv","AP","dset","S_Soun")	// "APT_BvS_Soundset")
				PLAY_SOUND_FROM_ENTITY(-1, temp1, PLAYER_PED_ID(), temp2)
				
				SET_BIT(sBeastUnlockVars.iState,BU_BIT_LANDING_SOUND_PLAYING)
			ENDIF
		ENDIF
	ELSE
		//Reset audio bits
		CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_JUMPING_SOUND_PLAYING)
		CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_LANDING_SOUND_PLAYING)
	ENDIF
ENDPROC


PROC UpdateBeastAbilities(BeastUnlockVars &sBeastUnlockVars)
	Maintain_Beast_Superjump(sBeastUnlockVars)
	
	//Disable first person mode
	DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
	
	RESET_PLAYER_STAMINA(PLAYER_ID())
	SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(), 1.75)
	SET_PED_MOVE_RATE_IN_WATER_OVERRIDE(PLAYER_PED_ID(), 1.75)
	
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_InfiniteStamina, TRUE)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressTakedownMeleeActions, TRUE)
	
	
	//Find peds near the Beast. Apply force punches to them.
	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), sBeastUnlockVars.pedsNearby)
	INT index
	REPEAT COUNT_OF(sBeastUnlockVars.pedsNearby) index
		IF DOES_ENTITY_EXIST(sBeastUnlockVars.pedsNearby[index])
			Maintain_Apply_Beast_Punches_To_Ped(sBeastUnlockVars.pedsNearby[index], PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDREPEAT
ENDPROC


PROC DisableBeastAbilities()
	CPRINTLN(DEBUG_HUNTING, "Disabling beast special abilities.")
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, TRUE)
    SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 100.0)
    SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 5000)
    SET_ENTITY_HEALTH(PLAYER_PED_ID(), 5000)
ENDPROC


PROC Set_Player_To_Beast(INT &iState)
	IF NOT (IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT())
		CPRINTLN(DEBUG_HUNTING, "Fading screen out for swap to Beast.")
		SET_BIT(iState, BU_BIT_STARTED_FADE)
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
	ENDIF

	IF IS_SCREEN_FADED_OUT()
		IF IS_PLAYER_PLAYING(PLAYER_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CPRINTLN(DEBUG_HUNTING, "Swapping player to the Beast model.")
			SET_PLAYER_MODEL(PLAYER_ID(), Get_Beast_Model())
			Set_Male_Beast_Outfit_On_Ped(PLAYER_PED_ID())
			EnableBeastAbilities()
			SET_BIT(iState, BU_BIT_SWAPPING_MODEL)
			SET_BIT(iState, BU_BIT_IN_CONTROL_OF_MODEL)
			SET_BIT(iState, BU_BIT_SWAPPED_TO_BEAST)
		ENDIF
	ENDIF
ENDPROC


PROC Set_Player_To_Sasquatch(INT &iState)
	IF NOT (IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT())
		CPRINTLN(DEBUG_HUNTING, "Fading screen out for swap to Sasquatch.")
		SET_BIT(iState, BU_BIT_STARTED_FADE)
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
	ENDIF

	IF IS_SCREEN_FADED_OUT()
		IF IS_PLAYER_PLAYING(PLAYER_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CPRINTLN(DEBUG_HUNTING, "Swapping player to the Sasquatch model.")
			SET_PLAYER_MODEL(PLAYER_ID(), Get_Sasquatch_Model())
			SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			DisableBeastAbilities()
			SET_BIT(iState, BU_BIT_SWAPPING_MODEL)
			SET_BIT(iState, BU_BIT_IN_CONTROL_OF_MODEL)
			CLEAR_BIT(iState, BU_BIT_SWAPPED_TO_BEAST)
		ENDIF
	ENDIF
ENDPROC


PROC Set_Beast_Swap_Active(BOOL bActive, BeastUnlockVars &sBeastUnlockVars)
	IF bActive
		CPRINTLN(DEBUG_HUNTING, "Enabling beast swap mode.")
		SET_BIT(sBeastUnlockVars.iState, BU_BIT_SWAP_MODE_ACTIVE)
	ELSE
		CPRINTLN(DEBUG_HUNTING, "Disabling beast swap mode.")
		CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SWAP_MODE_ACTIVE)
	ENDIF
	
	CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SASQUATCH_SELECTED)
	CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SWAPPED_TO_BEAST)
	CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
	CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SWAPPING_MODEL)
	sBeastUnlockVars.iTimeNextInput = GET_GAME_TIMER() + 1000
ENDPROC


FUNC TEXT_LABEL_63 Get_Transition_Help_Label()
	RETURN Build_Beast_String_63("N_", "GE", "AP", "SW") //GEN_SWAP.
ENDFUNC


FUNC TEXT_LABEL_63 Get_No_Transition_Help_Label()
	RETURN Build_Beast_String_63("_S", "NO", "P", "WA") //NO_SWAP.
ENDFUNC


PROC Cleanup_Beast_Unlock(BeastUnlockVars &sBeastUnlockVars)
	IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAP_MODE_ACTIVE)
		Set_Beast_Swap_Active(FALSE, sBeastUnlockVars)
	ENDIF
	
	IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_ASSETS_REQUESTED)
		Release_Beast_Unlock_Assets(sBeastUnlockVars)
	ENDIF
	
	IF sBeastUnlockVars.bTransitionHelpShown
		TEXT_LABEL_63 tlHelpLabel = Get_Transition_Help_Label()
		REMOVE_HELP_FROM_FLOW_QUEUE(tlHelpLabel)
		sBeastUnlockVars.bTransitionHelpShown = FALSE
	ENDIF
ENDPROC


FUNC BOOL Is_Beast_Swap_Allowed(BeastUnlockVars &sBUV)
	sBUV.iState = sBUV.istate

	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		RETURN FALSE
	ELIF IS_ENTITY_IN_WATER(PLAYER_PED_ID())      
		RETURN FALSE
    ELIF IS_ENTITY_IN_AIR(PLAYER_PED_ID()) 
		RETURN FALSE
    ELIF IS_PLAYER_CLIMBING(PLAYER_ID())
		RETURN FALSE
    ELIF IS_PED_ON_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
    ELIF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
		RETURN FALSE
    ELIF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
		RETURN FALSE
	ELIF (NOT IS_SCREEN_FADED_IN())
		RETURN FALSE
   	ENDIF
	
	RETURN TRUE
ENDFUNC



PROC Maintain_Swap_Beast(BeastUnlockVars &sBeastUnlockVars)

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	 	IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-11.9823, -14.7217, 499.0583>>) > 100.0
	
			IF NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAP_MODE_ACTIVE)
				IF Get_Player_Model_Beast_Unlock() = Get_Sasquatch_Model()
					Set_Beast_Swap_Active(TRUE, sBeastUnlockVars)
				ENDIF
			ELSE
				//Check to see if we still need to display transition help.
				IF NOT sBeastUnlockVars.bTransitionHelpShown
					IF NOT IS_HELP_MESSAGE_ON_SCREEN()
					AND IS_SCREEN_FADED_IN()
						TEXT_LABEL_63 tlTransitionHelp = Get_Transition_Help_Label()
						PRINT_HELP(tlTransitionHelp)
						sBeastUnlockVars.bTransitionHelpShown = TRUE
					ENDIF
				ENDIF

				//Check for player input to swap between models.
				IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_LS)
				AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS))
					IF Is_Beast_Swap_Allowed(sBeastUnlockVars)
						IF GET_GAME_TIMER() > sBeastUnlockVars.iTimeNextInput
						AND GET_GAME_TIMER() > (sBeastUnlockVars.iTimeLastInTrailer + 5000)
							IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SASQUATCH_SELECTED)
								CPRINTLN(DEBUG_HUNTING, "Player chose to swap to the Beast.")
								CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SASQUATCH_SELECTED)
							ELSE
								CPRINTLN(DEBUG_HUNTING, "Player chose to swap to the Sasquatch.")
								SET_BIT(sBeastUnlockVars.iState, BU_BIT_SASQUATCH_SELECTED)
							ENDIF
							sBeastUnlockVars.iTimeNextInput = GET_GAME_TIMER() + 1000
						ENDIF
					ELSE
						//Display a help message if it's unsafe to transition when
						//inputs are pressed.
						IF NOT IS_HELP_MESSAGE_ON_SCREEN()
						AND IS_SCREEN_FADED_IN()
							TEXT_LABEL_63 tlNoTransitionHelp = Get_No_Transition_Help_Label()
							PRINT_HELP(tlNoTransitionHelp)
						ENDIF
					ENDIF
				ENDIF
				
				//Swap models if we aren't in the correct state.
				IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SASQUATCH_SELECTED)
				
					//Swap to Sasquatch.
					IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAPPED_TO_BEAST)
//					OR NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
						IF Have_Beast_Unlock_Assets_Loaded()
							Set_Player_To_Sasquatch(sBeastUnlockVars.iState)
						ENDIF
					ENDIF
				
				ELSE
					//Swap to beast.
					IF NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAPPED_TO_BEAST)
//					OR NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
						IF Have_Beast_Unlock_Assets_Loaded()
							Set_Player_To_Beast(sBeastUnlockVars.iState)
						ENDIF
					ENDIF
					
				ENDIF
				
				//Check if we are faded out but have just finished swapping models.
				IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAPPING_MODEL)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
							CPRINTLN(DEBUG_HUNTING, "Model swap complete.")
							IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_STARTED_FADE)
							AND GET_GAME_TIMER() > (sBeastUnlockVars.iTimeLastInTrailer + 5000)
								CPRINTLN(DEBUG_HUNTING, "Fading screen in after model swap.")
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
								CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_STARTED_FADE)
							ENDIF
							CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SWAPPING_MODEL)
						ENDIF
					ENDIF
				ENDIF
			
				//Manage beast special powers if we are definitely playing as him right now.
				IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
				AND IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAPPED_TO_BEAST)
				AND NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAPPING_MODEL)
					UpdateBeastAbilities(sBeastUnlockVars)
				ENDIF
				
			ENDIF

		ELSE
			sBeastUnlockVars.iTimeLastInTrailer = GET_GAME_TIMER()
		
			IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAPPED_TO_BEAST)
				Set_Player_To_Sasquatch(sBeastUnlockVars.iState)
				CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_STARTED_FADE)
			ENDIF
			
			IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_SWAP_MODE_ACTIVE)
				CPRINTLN(DEBUG_HUNTING, "Disabling beast swap mode as we are near the casting trailer.")
				Set_Beast_Swap_Active(FALSE, sBeastUnlockVars)
			ENDIF
		ENDIF
	ENDIF
	
	MODEL_NAMES playerModel = Get_Player_Model_Beast_Unlock()
	//Check if another system has swapped us away to something completely different.
	IF playerModel != Get_Beast_Model() AND playerModel != Get_Sasquatch_Model()
		IF IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
			CPRINTLN(DEBUG_HUNTING, "Player was switched away from the beast by another system.")
			CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
			
		ENDIF
	ELIF NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
		BOOL isSas = (playerModel = Get_Sasquatch_Model())
		CPRINTLN(DEBUG_HUNTING, "Player was switched back to ",PICK_STRING(isSas,"Sasquatch","Beast"), " by another system.")
		SET_BIT(sBeastUnlockVars.iState, BU_BIT_IN_CONTROL_OF_MODEL)
		IF isSas
			CLEAR_BIT(sBeastUnlockVars.iState, BU_BIT_SWAPPED_TO_BEAST)
		ENDIF
		
	ENDIF

ENDPROC


PROC Maintain_Beast_Unlock(BeastUnlockVars &sBeastUnlockVars)

	IF IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_KILLED_AND_UNLOCKED)
	
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			IF NOT IS_BIT_SET(sBeastUnlockVars.iState, BU_BIT_ASSETS_REQUESTED)
				Request_Beast_Unlock_Assets(sBeastUnlockVars)
			ENDIF
			
			Maintain_Swap_Beast(sBeastUnlockVars)
		ELSE
			Cleanup_Beast_Unlock(sBeastUnlockVars)
		ENDIF
		
	ELSE
		Cleanup_Beast_Unlock(sBeastUnlockVars)
	ENDIF
	
ENDPROC


#IF IS_DEBUG_BUILD

PROC Create_Beast_Unlock_Widgets(BeastUnlockVars &sBeastUnlockVars, WIDGET_GROUP_ID widgetID)
	CPRINTLN(DEBUG_HUNTING, "Creating beast unlock widgets.")
	
	IF widgetID != NULL
		SET_CURRENT_WIDGET_GROUP(widgetID)
	ENDIF
	
	START_WIDGET_GROUP("Beast Unlock")
		ADD_WIDGET_BOOL("Terminate thread", sBeastUnlockVars.bDebugTerminateThread)
		ADD_WIDGET_BOOL("Beast Unlocked?", sBeastUnlockVars.bDebugSecretIsUnlocked)
		ADD_WIDGET_BOOL("Toggle Beast Unlock", sBeastUnlockVars.bDebugToggleSecretUnlocked)
	STOP_WIDGET_GROUP()
	
	IF widgetID != NULL
		CLEAR_CURRENT_WIDGET_GROUP(widgetID)
	ENDIF
ENDPROC


PROC Update_Beast_Unlock_Widgets(BeastUnlockVars &sBeastUnlockVars)

	IF sBeastUnlockVars.bDebugTerminateThread
		CPRINTLN(DEBUG_HUNTING, "Beast unlock thread terminating from widget.")
		Cleanup_Beast_Unlock(sBeastUnlockVars)
		TERMINATE_THIS_THREAD()
	ENDIF
	
	//Read only bool. Set it every frame based on state saved in global bitset.
	sBeastUnlockVars.bDebugSecretIsUnlocked = IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_KILLED_AND_UNLOCKED)
	
	//Allow toggling of the saved bit.
	IF sBeastUnlockVars.bDebugToggleSecretUnlocked
		IF IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_KILLED_AND_UNLOCKED)
			CLEAR_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_KILLED_AND_UNLOCKED)
		ELSE
			SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_BEAST_KILLED_AND_UNLOCKED)
		ENDIF
		sBeastUnlockVars.bDebugToggleSecretUnlocked = FALSE
	ENDIF
	
ENDPROC

#ENDIF //DEBUG
#ENDIF //FEATURE_SP_DLC_BEAST_SECRET
