USING "globals.sch"
USING "properties_locations.sch"
USING "commands_audio.sch"

// DELIVERY

// request the assets for a delivery event
PROC REQUEST_DELIVERY_EVENT_ASSETS(PROPERTY_ENUM thisProperty, DELIVERY_MISSION_VARIATION_ENUM thisDelivery)
	thisDelivery=thisDelivery

	SWITCH thisProperty
		CASE PROPERTY_WEED_SHOP
			REQUEST_MODEL(PONY2)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(PONY2, TRUE)
		BREAK
		CASE PROPERTY_BAR_TEQUILALA
		CASE PROPERTY_BAR_PITCHERS
		CASE PROPERTY_BAR_HEN_HOUSE
		CASE PROPERTY_BAR_HOOKIES
			REQUEST_MODEL(BENSON)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(BENSON, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

// have the assets loaded for a delivery event
FUNC BOOL HAVE_DELIVERY_EVENT_ASSETS_LOADED(PROPERTY_ENUM thisProperty, DELIVERY_MISSION_VARIATION_ENUM thisDelivery)
	thisDelivery=thisDelivery

	SWITCH thisProperty
		CASE PROPERTY_WEED_SHOP
			IF NOT HAS_MODEL_LOADED(PONY2)
				RETURN FALSE
			ENDIF
		BREAK
		CASE PROPERTY_BAR_TEQUILALA
		CASE PROPERTY_BAR_PITCHERS
		CASE PROPERTY_BAR_HEN_HOUSE
		CASE PROPERTY_BAR_HOOKIES
			IF NOT HAS_MODEL_LOADED(BENSON)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

// release a delivery event
PROC RELEASE_DELIVERY_EVENT_ASSETS(PROPERTY_ENUM thisProperty, DELIVERY_MISSION_VARIATION_ENUM thisDelivery)
	thisDelivery=thisDelivery

	SWITCH thisProperty
		CASE PROPERTY_WEED_SHOP
			SET_MODEL_AS_NO_LONGER_NEEDED(PONY2)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(PONY2, FALSE)
		BREAK
		CASE PROPERTY_BAR_TEQUILALA
		CASE PROPERTY_BAR_PITCHERS
		CASE PROPERTY_BAR_HEN_HOUSE
		CASE PROPERTY_BAR_HOOKIES
			SET_MODEL_AS_NO_LONGER_NEEDED(BENSON)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(BENSON, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

// create delivery event entities
PROC CREATE_DELIVERY_EVENT_ENTITIES(PROPERTY_ENUM thisProperty, DELIVERY_MISSION_VARIATION_ENUM thisDelivery)
	SWITCH thisProperty
		CASE PROPERTY_WEED_SHOP
			g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE] = CREATE_VEHICLE(PONY2, GET_DELIVERY_EVENT_INIT_COORDS(thisProperty, thisDelivery), GET_DELIVERY_EVENT_INIT_HEADING(thisProperty, thisDelivery))
			IF IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE])
                SET_VEHICLE_CAN_BREAK(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE], FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE])
			ENDIF		
		BREAK
		CASE PROPERTY_BAR_TEQUILALA
		CASE PROPERTY_BAR_PITCHERS
		CASE PROPERTY_BAR_HEN_HOUSE
		CASE PROPERTY_BAR_HOOKIES
			g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE] = CREATE_VEHICLE(BENSON, GET_DELIVERY_EVENT_INIT_COORDS(thisProperty, thisDelivery), GET_DELIVERY_EVENT_INIT_HEADING(thisProperty, thisDelivery))
			IF IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE])
                SET_VEHICLE_CAN_BREAK(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE], FALSE)
				INT i
				FOR i=1 TO 8
					SET_VEHICLE_EXTRA(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE], i, TRUE)
				ENDFOR
				SET_VEHICLE_EXTRA(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE], 2, FALSE)
				//SET_VEHICLE_EXTRA(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE], 7, FALSE)		
				SET_VEHICLE_ON_GROUND_PROPERLY(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE])
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// update the delivery event in the world - return FALSE if disrupted
FUNC BOOL UPDATE_DELIVERY_EVENT_IN_WORLD(PROPERTY_ENUM thisProperty, DELIVERY_MISSION_VARIATION_ENUM thisDelivery)
	thisProperty=thisProperty
	thisDelivery=thisDelivery
	
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE])
		IF NOT IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE])
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC


// PLANE PROMOTION

// request the assets for a promotion event
PROC REQUEST_PLANE_PROMOTION_EVENT_ASSETS()
	REQUEST_MODEL(STUNT)
ENDPROC 

// have the assets loaded for a promotion event
FUNC BOOL HAVE_PLANE_PROMOTION_EVENT_ASSETS_LOADED()
	IF NOT HAS_MODEL_LOADED(STUNT)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// release a promotion event
PROC RELEASE_PLANE_PROMOTION_EVENT_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(STUNT)
ENDPROC

// create promotion entities
PROC CREATE_PLANE_PROMOTION_EVENT_ENTITIES()
	CLEAR_AREA(<<-1085.2794, -2970.5725, 12.9457>>, 30.0, TRUE)
	g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_PLANE_PROMOTION_VEHICLE] = CREATE_VEHICLE(STUNT, <<-1085.2794, -2970.5725, 12.9457>>, 121.2244)
ENDPROC

// update the promotion event in the world - return FALSE if disrupted
FUNC BOOL UPDATE_PLANE_PROMOTION_EVENT_IN_WORLD()
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_PLANE_PROMOTION_VEHICLE])
		IF NOT IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_PLANE_PROMOTION_VEHICLE])
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC


//  RECOVER STOLEN

// request the assets for a recover stolen event
PROC REQUEST_RECOVER_STOLEN_EVENT_ASSETS(PROPERTY_ENUM thisProperty, RECOVER_STOLEN_MISSION_VARIATION_ENUM thisRecoverStolen)

	IF thisProperty = thisProperty	ENDIF

	SWITCH thisRecoverStolen
		CASE RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
		
			IF g_PropertySystemData.managementEventModel = DUMMY_MODEL_FOR_SCRIPT
				SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
					CASE 0			g_PropertySystemData.managementEventModel 	= FUTO					BREAK
					CASE 1			g_PropertySystemData.managementEventModel 	= PHOENIX				BREAK
					CASE 2			g_PropertySystemData.managementEventModel 	= SULTAN				BREAK
				ENDSWITCH
			ENDIF

			REQUEST_MODEL(g_PropertySystemData.managementEventModel)
			REQUEST_MODEL(A_M_M_EASTSA_02)
			REQUEST_SCRIPT_AUDIO_BANK("TAKINGS")
		BREAK
		CASE RECOVER_STOLEN_MISSION_VARIATION_PAP
			REQUEST_MODEL(A_M_M_PAPARAZZI_01)
			REQUEST_MODEL(VADER)
		BREAK
	ENDSWITCH
	
ENDPROC 

// have the assets loaded for a recover stolen event
FUNC BOOL HAVE_RECOVER_STOLEN_EVENT_ASSETS_LOADED(PROPERTY_ENUM thisProperty, RECOVER_STOLEN_MISSION_VARIATION_ENUM thisRecoverStolen)
	
	IF thisProperty = thisProperty	ENDIF
	
	SWITCH thisRecoverStolen
		CASE RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
			IF HAS_MODEL_LOADED(A_M_M_EASTSA_02)
			AND HAS_MODEL_LOADED(g_PropertySystemData.managementEventModel)
			AND REQUEST_SCRIPT_AUDIO_BANK("TAKINGS")
				RETURN TRUE
			ENDIF
		BREAK
		CASE RECOVER_STOLEN_MISSION_VARIATION_PAP
			IF HAS_MODEL_LOADED(A_M_M_PAPARAZZI_01)
			AND HAS_MODEL_LOADED(VADER)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// release a recover stolen event
PROC RELEASE_RECOVER_STOLEN_EVENT_ASSETS(PROPERTY_ENUM thisProperty, RECOVER_STOLEN_MISSION_VARIATION_ENUM thisRecoverStolen)

	IF thisProperty = thisProperty	ENDIF

	SWITCH thisRecoverStolen
		CASE RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_EASTSA_02)
			SET_MODEL_AS_NO_LONGER_NEEDED(g_PropertySystemData.managementEventModel)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("TAKINGS")
			g_PropertySystemData.managementEventModel = DUMMY_MODEL_FOR_SCRIPT
		BREAK
		CASE RECOVER_STOLEN_MISSION_VARIATION_PAP
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_PAPARAZZI_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(VADER)
		BREAK
	ENDSWITCH
	
ENDPROC 

// create recover stolen entities
PROC CREATE_RECOVER_STOLEN_EVENT_ENTITIES(PROPERTY_ENUM thisProperty, RECOVER_STOLEN_MISSION_VARIATION_ENUM thisRecoverStolen)
	
	VECTOR 		vPlayerCoord
	VECTOR		vVehSpawnCoord
	FLOAT		fVehSpawnHeading
	
	vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	SWITCH thisProperty
	
	// CINEMA'S
		CASE PROPERTY_CINEMA_VINEWOOD			
			vVehSpawnCoord 				= <<327.6613, 163.5656, 102.4007>>
			
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)
				fVehSpawnHeading		= 70.3316
			ELSE
				fVehSpawnHeading		= 246.2252
			ENDIF
		BREAK
		CASE PROPERTY_CINEMA_DOWNTOWN
			vVehSpawnCoord 				= <<400.8661, -713.4232, 28.1488>>
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)
				fVehSpawnHeading		= 198.4031
			ELSE
				fVehSpawnHeading		= 340.8739
			ENDIF
		BREAK
		CASE PROPERTY_CINEMA_MORNINGWOOD
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)
				vVehSpawnCoord 			= <<-1409.2603, -198.7362, 46.0458>>
				fVehSpawnHeading		= 220.3334
			ELSE
				vVehSpawnCoord 			= <<-1436.8124, -199.6595, 46.4139>>
				fVehSpawnHeading		= 127.0303
			ENDIF
		BREAK
	// BARS
		CASE PROPERTY_BAR_TEQUILALA
			vVehSpawnCoord 				= <<-565.1810, 268.2033, 81.8987>>
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)
				fVehSpawnHeading 		= 95.9266
			ELSE
				fVehSpawnHeading 		= 257.7337
			ENDIF
		BREAK
		CASE PROPERTY_BAR_PITCHERS
			vVehSpawnCoord 		= <<227.1843, 347.0700, 104.4053>>
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)
				fVehSpawnHeading 		= 254.2722
			ELSE
				fVehSpawnHeading 		= 53.2505
			ENDIF
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
			vVehSpawnCoord 				= <<-293.9433, 6249.7012, 30.2897>>
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)
				fVehSpawnHeading 		= 148.9658
			ELSE
				fVehSpawnHeading 		= 298.0306
			ENDIF
		BREAK
		CASE PROPERTY_BAR_HOOKIES
			vVehSpawnCoord 				= <<-2208.3889, 4288.6763, 47.1738>>
			IF IS_COORD_LEFT_OF_PROPERTY(thisProperty, vPlayerCoord)			
				fVehSpawnHeading 		= 342.7601
			ELSE
				fVehSpawnHeading 		= 133.1666
			ENDIF
		BREAK
	ENDSWITCH
	
		
	MODEL_NAMES		modVehicle				= DUMMY_MODEL_FOR_SCRIPT
	INT				iVehVariation			= -1
	
	MODEL_NAMES		modPed1					= DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES		modPed2					= DUMMY_MODEL_FOR_SCRIPT
	
	BOOL			bGivePedHelet			= FALSE
	
	SWITCH thisRecoverStolen
		CASE RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
			modVehicle									= g_PropertySystemData.managementEventModel
			modPed1										= A_M_M_EASTSA_02
		BREAK
	
		CASE RECOVER_STOLEN_MISSION_VARIATION_PAP
			modVehicle		= VADER
			modPed1			= A_M_M_PAPARAZZI_01
			modPed2			= A_M_M_PAPARAZZI_01
			iVehVariation 	= 3
		BREAK
	ENDSWITCH
	
// Vehicle
	CLEAR_AREA_OF_VEHICLES(vVehSpawnCoord, 30.0)
	g_PropertySystemData.managementEventVehicle[0] = CREATE_VEHICLE(modVehicle, vVehSpawnCoord, fVehSpawnHeading)
	IF iVehVariation != -1
		SET_VEHICLE_COLOUR_COMBINATION(g_PropertySystemData.managementEventVehicle[0], iVehVariation)
	ENDIF
	SET_VEHICLE_ENGINE_ON(g_PropertySystemData.managementEventVehicle[0], TRUE, TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modVehicle, TRUE)
	
// Ped 1
	g_PropertySystemData.managementEventPed[0] = CREATE_PED_INSIDE_VEHICLE(g_PropertySystemData.managementEventVehicle[0], PEDTYPE_MISSION, modPed1)
	IF modPed1 = A_M_M_PAPARAZZI_01
		SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[0],PED_COMP_SPECIAL2,0,0)
		SET_ENTITY_HEALTH(g_PropertySystemData.managementEventPed[0], 400)
	ENDIF
	SET_ENTITY_LOAD_COLLISION_FLAG(g_PropertySystemData.managementEventPed[0], TRUE)
	SET_PED_CONFIG_FLAG(g_PropertySystemData.managementEventPed[0], PCF_DontInfluenceWantedLevel, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_PropertySystemData.managementEventPed[0], TRUE)
	
	IF IS_THIS_MODEL_A_BIKE(modVehicle)
		IF bGivePedHelet
			SET_PED_HELMET(g_PropertySystemData.managementEventPed[0], TRUE)
			GIVE_PED_HELMET(g_PropertySystemData.managementEventPed[0], TRUE)
		ELSE
			SET_PED_HELMET(g_PropertySystemData.managementEventPed[0], FALSE)
		ENDIF
	ENDIF
	
// Ped 2
	IF modPed2 != DUMMY_MODEL_FOR_SCRIPT
		g_PropertySystemData.managementEventPed[1] = CREATE_PED_INSIDE_VEHICLE(g_PropertySystemData.managementEventVehicle[0], PEDTYPE_MISSION, modPed2, VS_FRONT_RIGHT)
		SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[1],PED_COMP_SPECIAL2,0,0)
		SET_ENTITY_HEALTH(g_PropertySystemData.managementEventPed[1], 400)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_PropertySystemData.managementEventPed[1], TRUE)
		IF modPed2 = A_M_M_PAPARAZZI_01
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[1],PED_COMP_SPECIAL2,0,0)
		ENDIF
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_PropertySystemData.managementEventPed[1], TRUE)
		
		IF IS_THIS_MODEL_A_BIKE(modVehicle)
			IF bGivePedHelet
				SET_PED_HELMET(g_PropertySystemData.managementEventPed[1], TRUE)
				GIVE_PED_HELMET(g_PropertySystemData.managementEventPed[1], TRUE)
			ELSE
				SET_PED_HELMET(g_PropertySystemData.managementEventPed[1], FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// update the recover stolen event in the world - return FALSE if disrupted
FUNC BOOL UPDATE_RECOVER_STOLEN_EVENT_IN_WORLD(PROPERTY_ENUM thisProperty, RECOVER_STOLEN_MISSION_VARIATION_ENUM thisRecoverStolen)
	// temp for compile
	thisProperty=thisProperty
	thisRecoverStolen=thisRecoverStolen
	
	// killed either of the paps, disrupt the mission
	IF thisRecoverStolen = RECOVER_STOLEN_MISSION_VARIATION_PAP
		IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[0])
			IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[0])
				RETURN FALSE
			ENDIF
		ELIF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[1])
			IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[1])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

// should a recover stolen event force start before we hit the trigger
FUNC BOOL SHOULD_RECOVER_STOLEN_EVENT_FORCE_START(PROPERTY_ENUM thisProperty, RECOVER_STOLEN_MISSION_VARIATION_ENUM thisRecoverStolen)
	thisProperty=thisProperty
	thisRecoverStolen=thisRecoverStolen
	
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[0])
		
		IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[0])
			IF thisRecoverStolen != RECOVER_STOLEN_MISSION_VARIATION_PAP	// do not trigger early for a kill when its the pap mission
				RETURN TRUE
			ENDIF
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_PropertySystemData.managementEventPed[0], PLAYER_PED_ID())
		OR HAS_PED_RECEIVED_EVENT(g_PropertySystemData.managementEventPed[0], EVENT_SHOT_FIRED)
		OR HAS_PED_RECEIVED_EVENT(g_PropertySystemData.managementEventPed[0], EVENT_SHOT_FIRED_BULLET_IMPACT)
		OR HAS_PED_RECEIVED_EVENT(g_PropertySystemData.managementEventPed[0], EVENT_SHOT_FIRED_WHIZZED_BY)
			RETURN TRUE
		ENDIF
		
	ELIF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[1])
		
		IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[1])
			IF thisRecoverStolen != RECOVER_STOLEN_MISSION_VARIATION_PAP	// do not trigger early for a kill when its the pap mission
				RETURN TRUE
			ENDIF
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_PropertySystemData.managementEventPed[1], PLAYER_PED_ID())
		OR HAS_PED_RECEIVED_EVENT(g_PropertySystemData.managementEventPed[1], EVENT_SHOT_FIRED)
		OR HAS_PED_RECEIVED_EVENT(g_PropertySystemData.managementEventPed[1], EVENT_SHOT_FIRED_BULLET_IMPACT)
		OR HAS_PED_RECEIVED_EVENT(g_PropertySystemData.managementEventPed[1], EVENT_SHOT_FIRED_WHIZZED_BY)
			RETURN TRUE
		ENDIF
		
	ELIF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[0])
		IF NOT IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[0])
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_PropertySystemData.managementEventVehicle[0], PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//  GANG ATTACK
FUNC MODEL_NAMES GET_PROPERTY_GANG_PED_MODEL(PROPERTY_GANG_ENUM eGang)
	
	SWITCH eGang
		CASE PROPERTYGANG_BALLAS	RETURN		G_M_Y_BALLAORIG_01		BREAK
		CASE PROPERTYGANG_TRIAD		RETURN	 	G_M_Y_KOREAN_01			BREAK
		CASE PROPERTYGANG_VAGOS		RETURN	 	G_M_Y_MEXGOON_02		BREAK
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTY_GANG_ENUM eGang)
	
	SWITCH eGang
		CASE PROPERTYGANG_BALLAS 	RETURN 		FELON			BREAK
		CASE PROPERTYGANG_TRIAD		RETURN 		FELTZER2		BREAK
		CASE PROPERTYGANG_VAGOS		RETURN		CAVALCADE2		BREAK
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL GET_PROPERTY_GANG_VEHICLE_COLOURS(PROPERTY_GANG_ENUM eGang, INT &iColour1, INT &iColour2, INT &iExtraColour1, INT &iExtraColour2)

	SWITCH eGang
		CASE PROPERTYGANG_BALLAS			
			iColour1			= 142
			iColour2			= 0 
			iExtraColour1		= 148
			iExtraColour2		= 156
			RETURN TRUE
		BREAK
		CASE PROPERTYGANG_TRIAD
			iColour1			= 0
			iColour2			= 15
			iExtraColour1		= 0
			iExtraColour2		= 156	
			RETURN TRUE
		BREAK
		CASE PROPERTYGANG_VAGOS			
			iColour1			= 6
			iColour2			= 6
			iExtraColour1		= 111
			iExtraColour2		= 156		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


// request the assets for a gang attack event
PROC REQUEST_GANG_ATTACK_EVENT_ASSETS(PROPERTY_ENUM thisProperty, GANG_ATTACK_MISSION_VARIATION_ENUM thisGangAttack)

	thisGangAttack=thisGangAttack
	
	SWITCH thisProperty
		CASE PROPERTY_BAR_TEQUILALA
			REQUEST_MODEL(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS))
			REQUEST_MODEL(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS))
			SET_PED_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS), TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS), TRUE)
		BREAK
		CASE PROPERTY_BAR_PITCHERS
			REQUEST_MODEL(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_BALLAS))
			REQUEST_MODEL(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_BALLAS))
			SET_PED_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_BALLAS), TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_BALLAS), TRUE)
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
			REQUEST_MODEL(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_TRIAD))
			REQUEST_MODEL(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_TRIAD))
			SET_PED_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_TRIAD), TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_TRIAD), TRUE)
		BREAK
		CASE PROPERTY_BAR_HOOKIES
			REQUEST_MODEL(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS))
			REQUEST_MODEL(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS))
			SET_PED_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS), TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS), TRUE)
		BREAK
	ENDSWITCH
	
ENDPROC 

// have the assets loaded for a gang attack event
FUNC BOOL HAVE_GANG_ATTACK_EVENT_ASSETS_LOADED(PROPERTY_ENUM thisProperty, GANG_ATTACK_MISSION_VARIATION_ENUM thisGangAttack)
	
	thisGangAttack=thisGangAttack
	
	SWITCH thisProperty
		CASE PROPERTY_BAR_TEQUILALA
			IF HAS_MODEL_LOADED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS))
			AND HAS_MODEL_LOADED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS))
				RETURN TRUE
			ENDIF
		BREAK
		CASE PROPERTY_BAR_PITCHERS
			IF HAS_MODEL_LOADED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_BALLAS))
			AND HAS_MODEL_LOADED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_BALLAS))
				RETURN TRUE
			ENDIF
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
			IF HAS_MODEL_LOADED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_TRIAD))
			AND HAS_MODEL_LOADED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_TRIAD))
				RETURN TRUE
			ENDIF
		BREAK
		CASE PROPERTY_BAR_HOOKIES
			IF HAS_MODEL_LOADED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS))
			AND HAS_MODEL_LOADED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS))
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// release a gang attack event
PROC RELEASE_GANG_ATTACK_EVENT_ASSETS(PROPERTY_ENUM thisProperty, GANG_ATTACK_MISSION_VARIATION_ENUM thisGangAttack)

	thisGangAttack=thisGangAttack
	
	SWITCH thisProperty
		CASE PROPERTY_BAR_TEQUILALA
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS))
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS))
		BREAK
		CASE PROPERTY_BAR_PITCHERS
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_BALLAS))
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_BALLAS))
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_TRIAD))
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_TRIAD))
		BREAK
		CASE PROPERTY_BAR_HOOKIES
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_PED_MODEL(PROPERTYGANG_VAGOS))
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PROPERTY_GANG_VEHICLE_MODEL(PROPERTYGANG_VAGOS))
		BREAK
	ENDSWITCH
	
ENDPROC

STRUCT PROPERTY_GANG_ATTACK_PED
	VECTOR 			vSpawnLocation
	FLOAT			fSpawnHeading
	VECTOR			vAimCoord
	WEAPON_TYPE		eWeapon
	INT				iVehToGetTo
	VEHICLE_SEAT	eVehSeatToGetTo
ENDSTRUCT

PROC GET_GANG_ATTACK_PED_DATA(PROPERTY_GANG_ATTACK_PED &sPed, PROPERTY_ENUM thisProperty, INT iPedIndex)

	SWITCH thisProperty

		CASE PROPERTY_BAR_TEQUILALA
		
			SWITCH iPedIndex
				CASE 0
					sPed.vSpawnLocation				= <<-543.6415, 303.5388, 82.0202>>
					sPed.fSpawnHeading				= 231.5559
					sPed.vAimCoord					= <<0,0,0>>
					sPed.eWeapon					= WEAPONTYPE_PUMPSHOTGUN
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 1
					sPed.vSpawnLocation				= <<-563.5045, 303.3635, 82.1920>>
					sPed.fSpawnHeading				= 191.6703
					sPed.vAimCoord					= <<-561.9358, 293.7914, 84.1827>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 2
					sPed.vSpawnLocation				= <<-547.9926, 299.5310, 82.0218>>
					sPed.fSpawnHeading				= 170.2791
					sPed.vAimCoord					= <<-550.0999, 292.7203, 87.8903>>
					sPed.eWeapon					= WEAPONTYPE_ASSAULTRIFLE
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 3
					sPed.vSpawnLocation				= <<-551.7525, 301.2135, 82.1226>>
					sPed.fSpawnHeading				= 132.7229
					sPed.vAimCoord					= <<-561.2323, 294.0457, 87.6588>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 4
					sPed.vSpawnLocation				= <<-549.7384, 303.0894, 82.0981>>
					sPed.fSpawnHeading				= 167.5811
					sPed.vAimCoord					= <<-552.8337, 292.7388, 88.2295>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
				CASE 5
					sPed.vSpawnLocation				= <<-558.4697, 303.0401, 82.2333>>
					sPed.fSpawnHeading				= 161.0839
					sPed.vAimCoord					= <<-561.9985, 293.9765, 87.8096>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
			ENDSWITCH

		BREAK
		CASE PROPERTY_BAR_PITCHERS
		
			SWITCH iPedIndex
				CASE 0
					sPed.vSpawnLocation				= <<231.2834, 345.2137, 104.3893>>
					sPed.fSpawnHeading				= 324.6404
					sPed.vAimCoord					= <<0,0,0>>
					sPed.eWeapon					= WEAPONTYPE_SAWNOFFSHOTGUN
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 1
					sPed.vSpawnLocation				= <<236.5094, 339.3164, 104.5298>>
					sPed.fSpawnHeading				= 112.7911
					sPed.vAimCoord					= <<230.1282, 337.0690, 106.8048>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 2
					sPed.vSpawnLocation				= <<224.3295, 348.2992, 104.4469>>
					sPed.fSpawnHeading				= 165.7146
					sPed.vAimCoord					= <<221.6486, 340.3152, 106.6212>>
					sPed.eWeapon					= WEAPONTYPE_ASSAULTRIFLE
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 3
					sPed.vSpawnLocation				= <<234.1038, 341.6698, 104.5309>>
					sPed.fSpawnHeading				= 110.0142
					sPed.vAimCoord					= <<223.9347, 338.3914, 106.8137>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 4
					sPed.vSpawnLocation				= <<228.9968, 344.3681, 104.5384>>
					sPed.fSpawnHeading				= 153.6073
					sPed.vAimCoord					= <<224.4416, 336.0886, 106.1191>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
				CASE 5
					sPed.vSpawnLocation				= <<221.3820, 346.3744, 104.5675>>
					sPed.fSpawnHeading				= 165.8422
					sPed.vAimCoord					= <<219.6116, 340.9911, 106.4532>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
			ENDSWITCH
			
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
		
			SWITCH iPedIndex
				CASE 0
					sPed.vSpawnLocation				= <<-325.1079, 6263.8325, 30.4151>>
					sPed.fSpawnHeading				= 195.9702
					sPed.vAimCoord					= <<0,0,0>>
					sPed.eWeapon					= WEAPONTYPE_SAWNOFFSHOTGUN
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 1
					sPed.vSpawnLocation				= <<-312.9843, 6250.1147, 30.4899>>
					sPed.fSpawnHeading				= 319.1074
					sPed.vAimCoord					= <<-308.3476, 6254.8569, 32.6941>>
					sPed.eWeapon					= WEAPONTYPE_ASSAULTRIFLE
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
				CASE 2
					sPed.vSpawnLocation				= <<-326.1756, 6268.3384, 30.4714>>
					sPed.fSpawnHeading				= 261.9836
					sPed.vAimCoord					= <<-312.8552, 6265.8569, 43.5835>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 3
					sPed.vSpawnLocation				= <<-323.9344, 6259.6587, 30.3539>>
					sPed.fSpawnHeading				= 292.4941
					sPed.vAimCoord					= <<-313.3472, 6263.3945, 42.7414>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
				CASE 4
					sPed.vSpawnLocation				= <<-318.7812, 6259.5762, 30.5161>>
					sPed.fSpawnHeading				= 295.0701
					sPed.vAimCoord					= <<-313.7660, 6261.4790, 32.6516>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 5
					sPed.vSpawnLocation				= <<-314.9250, 6255.5420, 30.5164>>
					sPed.fSpawnHeading				= 309.7791
					sPed.vAimCoord					= <<-310.5720, 6258.4502, 32.6648>>
					sPed.eWeapon					= WEAPONTYPE_PUMPSHOTGUN
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
			ENDSWITCH
		
		BREAK
		CASE PROPERTY_BAR_HOOKIES
		
			SWITCH iPedIndex
				CASE 0
					sPed.vSpawnLocation				= <<-2215.3203, 4275.0464, 46.4750>>
					sPed.fSpawnHeading				= 16.9550
					sPed.vAimCoord					= <<0,0,0>>
					sPed.eWeapon					= WEAPONTYPE_PUMPSHOTGUN
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 1
					sPed.vSpawnLocation				= <<-2195.4250, 4262.3101, 47.2741>>
					sPed.fSpawnHeading				= 241.4366
					sPed.vAimCoord					= <<-2185.3621, 4256.3765, 48.4082>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
				CASE 2
					sPed.vSpawnLocation				= <<-2200.8479, 4268.1904, 47.3093>>
					sPed.fSpawnHeading				= 331.1949
					sPed.vAimCoord					= <<-2196.5752, 4275.1294, 49.0669>>
					sPed.eWeapon					= WEAPONTYPE_ASSAULTRIFLE
					sPed.iVehToGetTo				= -1
					sPed.eVehSeatToGetTo			= VS_ANY_PASSENGER
				BREAK
				CASE 3
					sPed.vSpawnLocation				= <<-2202.8179, 4294.1494, 47.4518>>
					sPed.fSpawnHeading				= 263.7257
					sPed.vAimCoord					= <<-2192.7834, 4289.5718, 50.3251>>
					sPed.eWeapon					= WEAPONTYPE_MOLOTOV
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_FRONT_RIGHT
				BREAK
				CASE 4
					sPed.vSpawnLocation				= <<-2196.2708, 4265.0645, 47.3446>>
					sPed.fSpawnHeading				= 325.4524
					sPed.vAimCoord					= <<-2191.1833, 4271.7471, 49.8670>>
					sPed.eWeapon					= WEAPONTYPE_PUMPSHOTGUN
					sPed.iVehToGetTo				= 1
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
				CASE 5
					sPed.vSpawnLocation				= <<-2204.4917, 4290.0586, 47.3773>>
					sPed.fSpawnHeading				= 269.5739
					sPed.vAimCoord					= <<-2193.0168, 4292.6694, 54.8730>>
					sPed.eWeapon					= WEAPONTYPE_MICROSMG
					sPed.iVehToGetTo				= 0
					sPed.eVehSeatToGetTo			= VS_DRIVER
				BREAK
			ENDSWITCH

		BREAK

	ENDSWITCH

ENDPROC

// create gang attack entities
PROC CREATE_GANG_ATTACK_EVENT_ENTITIES(PROPERTY_ENUM thisProperty, GANG_ATTACK_MISSION_VARIATION_ENUM thisGangAttack)

	thisGangAttack = thisGangAttack
	
	TEXT_LABEL_15 					strReuse
	PROPERTY_GANG_ENUM 				eGang
	VECTOR							vVehSpawnCoord[NUM_PROPERTY_GANG_VEHS]
	FLOAT							fVehSpawnHeading[NUM_PROPERTY_GANG_VEHS]
	PROPERTY_GANG_ATTACK_PED		sPed[6]

	SWITCH thisProperty

		CASE PROPERTY_BAR_TEQUILALA
		
			eGang 							= PROPERTYGANG_VAGOS
			
			vVehSpawnCoord[0] 				= <<-546.2500, 307.1572, 82.0203>>
			fVehSpawnHeading[0]				= 292.7172

			vVehSpawnCoord[1] 				= <<-553.0233, 305.0579, 82.2847>>
			fVehSpawnHeading[1]				= 215.8545

		BREAK
		CASE PROPERTY_BAR_PITCHERS
		
			eGang 							= PROPERTYGANG_BALLAS
						
			vVehSpawnCoord[0] 				= <<239.7992, 339.1508, 104.4417>>
			fVehSpawnHeading[0]				= 207.3874
		
			vVehSpawnCoord[1] 				= <<228.0525, 346.2598, 104.3948>>
			fVehSpawnHeading[1]				= 246.0324
			
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
		
			eGang 							= PROPERTYGANG_TRIAD
						
			vVehSpawnCoord[0] 				= <<-318.1871, 6254.5132, 30.3558>>
			fVehSpawnHeading[0]				= 213.6937
		
			vVehSpawnCoord[1] 				= <<-327.4945, 6264.9541, 30.3986>>
			fVehSpawnHeading[1]				= 260.5945
						
		BREAK
		CASE PROPERTY_BAR_HOOKIES

			eGang 							= PROPERTYGANG_VAGOS
			
			vVehSpawnCoord[0] 				= <<-2208.0652, 4268.6445, 46.8024>>
			fVehSpawnHeading[0]				= 31.6846
		
			vVehSpawnCoord[1] 				= <<-2201.4014, 4261.4736, 46.9114>>
			fVehSpawnHeading[1]				= 6.3115
			
		BREAK

	ENDSWITCH
	
	// Get ped spawn data
	GET_GANG_ATTACK_PED_DATA(sPed[0], thisProperty, 0)
	GET_GANG_ATTACK_PED_DATA(sPed[1], thisProperty, 1)
	GET_GANG_ATTACK_PED_DATA(sPed[2], thisProperty, 2)
	GET_GANG_ATTACK_PED_DATA(sPed[3], thisProperty, 3)
	GET_GANG_ATTACK_PED_DATA(sPed[4], thisProperty, 4)
	GET_GANG_ATTACK_PED_DATA(sPed[5], thisProperty, 5)
	
	INT i
	
// Spawn ped
	FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
		g_PropertySystemData.managementEventPed[i] = CREATE_PED(PEDTYPE_MISSION, GET_PROPERTY_GANG_PED_MODEL(eGang), sPed[i].vSpawnLocation, sPed[i].fSpawnHeading)
		strReuse = "GANG_"
		strReuse += i
		SET_PED_NAME_DEBUG(g_PropertySystemData.managementEventPed[i], strReuse)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_PropertySystemData.managementEventPed[i], TRUE)
		GIVE_WEAPON_TO_PED(g_PropertySystemData.managementEventPed[i], sPed[i].eWeapon, INFINITE_AMMO, TRUE, 	TRUE)
		SET_PED_USING_ACTION_MODE(g_PropertySystemData.managementEventPed[i], TRUE, -1, "DEFAULT_ACTION")
	ENDFOR
	
// Spawn vehicles
	INT iColour1, iColour2, iExtraColour1, iExTraColour2	
	GET_PROPERTY_GANG_VEHICLE_COLOURS(eGang, iColour1, iColour2, iExtraColour1, iExTraColour2)
	FOR i = 0 TO NUM_PROPERTY_GANG_VEHS-1
		g_PropertySystemData.managementEventVehicle[i] = CREATE_VEHICLE(GET_PROPERTY_GANG_VEHICLE_MODEL(eGang), vVehSpawnCoord[i], fVehSpawnHeading[i])
		SET_VEHICLE_COLOURS(g_PropertySystemData.managementEventVehicle[i], iColour1, iColour2)
		SET_VEHICLE_EXTRA_COLOURS(g_PropertySystemData.managementEventVehicle[i], iExtraColour1, iExTraColour2)
		SET_VEHICLE_ENGINE_ON(g_PropertySystemData.managementEventVehicle[i], TRUE, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_PropertySystemData.managementEventVehicle[i], TRUE)
	ENDFOR

ENDPROC

PROC PROPERTY_GANG_ATTACK_COMMON_AI_START(PED_INDEX ped, VECTOR vAttackCoord)

	WEAPON_TYPE eWeapon, eWeaponCurrent
	SEQUENCE_INDEX seq
	GET_CURRENT_PED_WEAPON(ped, eWeapon)
					
	// Ped with a shoot target to aim at
	IF NOT IS_VECTOR_ZERO(vAttackCoord)
	
		GET_CURRENT_PED_WEAPON(ped, eWeaponCurrent)
	
		// Molotov thrower ped
		IF eWeapon = WEAPONTYPE_MOLOTOV
		
			IF eWeaponCurrent != WEAPONTYPE_MOLOTOV
				SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_MOLOTOV, TRUE)
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
				OPEN_SEQUENCE_TASK(seq)
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 6000))
					TASK_THROW_PROJECTILE(NULL, GET_RANDOM_POINT_IN_SPHERE(vAttackCoord, 1.0))
					SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(ped, seq)
				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
		
		// Normal gun
		ELSE
			IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SHOOT_AT_COORD) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SHOOT_AT_COORD) != WAITING_TO_START_TASK
				TASK_SHOOT_AT_COORD(ped, vAttackCoord, -1, FIRING_TYPE_RANDOM_BURSTS)
			ENDIF
			
		ENDIF
	
	// all other peds
	ELSE
	
		IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_STAND_STILL) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_STAND_STILL) != WAITING_TO_START_TASK
			TASK_STAND_STILL(ped, -1)
		ENDIF
	
	ENDIF

ENDPROC

// update the gang attack event in the world - return FALSE if disrupted
FUNC BOOL UPDATE_GANG_ATTACK_EVENT_IN_WORLD(PROPERTY_ENUM thisProperty, GANG_ATTACK_MISSION_VARIATION_ENUM thisGangAttack)
	// temp for compile
	thisProperty=thisProperty
	thisGangAttack=thisGangAttack
	
	PROPERTY_GANG_ATTACK_PED sPedData
	INT i
	REPEAT 6 i
		IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[i])
		AND NOT IS_PED_INJURED(g_PropertySystemData.managementEventPed[i])
			GET_GANG_ATTACK_PED_DATA(sPedData, thisProperty, i)
			PROPERTY_GANG_ATTACK_COMMON_AI_START(g_PropertySystemData.managementEventPed[i], sPedData.vAimCoord)
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

// should a recover stolen event force start before we hit the trigger
FUNC BOOL SHOULD_GANG_ATTACK_EVENT_FORCE_START(PROPERTY_ENUM thisProperty, GANG_ATTACK_MISSION_VARIATION_ENUM thisGangAttack)
	thisProperty=thisProperty
	thisGangAttack=thisGangAttack
	
	INT i 
	REPEAT COUNT_OF(g_PropertySystemData.managementEventPed) i
		IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[i])
			IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[i])
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_PropertySystemData.managementEventPed[i], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(g_PropertySystemData.managementEventVehicle) i
		IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[i])
			IF NOT IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[i])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC



// TAXI

// request the assets for a taxi event
PROC REQUEST_TAXI_EVENT_ASSETS(TAXI_MISSION_VARIATION_ENUM thisTaxi)
	SWITCH thisTaxi
		CASE TAXI_MISSION_VARIATION_CUT_YOU_IN			REQUEST_MODEL(U_M_M_JEWELTHIEF)		BREAK
		CASE TAXI_MISSION_VARIATION_DEADLINE 			REQUEST_MODEL(G_M_M_CHIGOON_02)		BREAK
		CASE TAXI_MISSION_VARIATION_FOLLOW_CAR 			REQUEST_MODEL(A_F_Y_GENHOT_01)		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOU_NOW 		REQUEST_MODEL(G_M_Y_MexGoon_03)		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOUR_BACK 		REQUEST_MODEL(A_M_M_FARMER_01)		BREAK
		CASE TAXI_MISSION_VARIATION_NEEDS_EXCITEMENT 	REQUEST_MODEL(A_M_Y_SKATER_02)		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_IT_EASY 		REQUEST_MODEL(A_M_Y_SouCent_04)		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_TO_BEST 		REQUEST_MODEL(A_F_Y_BEVHILLS_03)	BREAK
		CASE TAXI_MISSION_VARIATION_CLOWN_CAR 			
			REQUEST_MODEL(A_M_Y_BUSINESS_02)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_MODEL(A_F_Y_Beach_01)
			REQUEST_MODEL(STRETCH)
			REQUEST_MODEL(SUPERD)
		BREAK
	ENDSWITCH
ENDPROC 

// have the assets loaded for a taxi event
FUNC BOOL HAVE_TAXI_EVENT_ASSETS_LOADED(TAXI_MISSION_VARIATION_ENUM thisTaxi)
	SWITCH thisTaxi
		CASE TAXI_MISSION_VARIATION_CUT_YOU_IN
			IF HAS_MODEL_LOADED(U_M_M_JEWELTHIEF)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_DEADLINE
			IF HAS_MODEL_LOADED(G_M_M_CHIGOON_02)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_FOLLOW_CAR
			IF HAS_MODEL_LOADED(A_F_Y_GENHOT_01)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOU_NOW
			IF HAS_MODEL_LOADED(G_M_Y_MexGoon_03)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOUR_BACK
			IF HAS_MODEL_LOADED(A_M_M_FARMER_01)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_NEEDS_EXCITEMENT
			IF HAS_MODEL_LOADED(A_M_Y_SKATER_02)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_IT_EASY
			IF HAS_MODEL_LOADED(A_M_Y_SouCent_04)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_TO_BEST
			IF HAS_MODEL_LOADED(A_F_Y_BEVHILLS_03)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_CLOWN_CAR
			IF HAS_MODEL_LOADED(A_M_Y_BUSINESS_02)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_MODEL_LOADED(A_F_Y_Beach_01)
			AND HAS_MODEL_LOADED(STRETCH)
			AND HAS_MODEL_LOADED(SUPERD)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// release a taxi event
PROC RELEASE_TAXI_EVENT_ASSETS(TAXI_MISSION_VARIATION_ENUM thisTaxi)
	SWITCH thisTaxi
		CASE TAXI_MISSION_VARIATION_CUT_YOU_IN
			SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_JEWELTHIEF)
		BREAK
		CASE TAXI_MISSION_VARIATION_DEADLINE
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_M_CHIGOON_02)
		BREAK
		CASE TAXI_MISSION_VARIATION_FOLLOW_CAR
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_GENHOT_01)
		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOU_NOW
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MexGoon_03)
		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOUR_BACK
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_FARMER_01)
		BREAK
		CASE TAXI_MISSION_VARIATION_NEEDS_EXCITEMENT
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_SKATER_02)
		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_IT_EASY
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_SouCent_04)
		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_TO_BEST
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BEVHILLS_03)
		BREAK
		CASE TAXI_MISSION_VARIATION_CLOWN_CAR
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BUSINESS_02)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BUSINESS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Beach_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(STRETCH)
			SET_MODEL_AS_NO_LONGER_NEEDED(SUPERD)
		BREAK
	ENDSWITCH
ENDPROC

// create taxi entities
PROC CREATE_TAXI_EVENT_ENTITIES(TAXI_MISSION_VARIATION_ENUM thisTaxi)
	SWITCH thisTaxi
		CASE TAXI_MISSION_VARIATION_CUT_YOU_IN
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, U_M_M_JEWELTHIEF, << -1042.9464, -2689.5498, 12.7572 >>, 116.1366)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_SPECIAL, 0, 0)
		BREAK
		CASE TAXI_MISSION_VARIATION_DEADLINE
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, G_M_M_CHIGOON_02, <<1971.2462, 3741.5171, 31.3268>>, 180.6)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,8), 1, 1, 0) //(accs)
		BREAK
		CASE TAXI_MISSION_VARIATION_FOLLOW_CAR
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_GENHOT_01, << 1358.8215, -1547.3961, 53.7793 >>, 41.1334)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,4), 1, 3, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOU_NOW
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MexGoon_03, << -1612.2349, 189.1934, 58.9435 >>, 196.3547)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
		BREAK
		CASE TAXI_MISSION_VARIATION_GOT_YOUR_BACK
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_FARMER_01, << 11.8607, -1123.4800, 27.6801 >>, 202.0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_LEG, 0,2)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_HEAD, 0,2)
		BREAK
		CASE TAXI_MISSION_VARIATION_NEEDS_EXCITEMENT
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SKATER_02, << -496.0739, -336.6628, 33.5017 >>, 256)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_TORSO, 1, 1)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_LEG, 1, 2)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_HEAD, 0,1)
		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_IT_EASY
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_04, << 58.8213, 293.8480, 109.6124 >>, 135.3)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_LEG, 1, 0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_HEAD, 1, 0)
		BREAK
		CASE TAXI_MISSION_VARIATION_TAKE_TO_BEST
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEVHILLS_03, << -412.0875, 1171.3588, 324.8176 >>, 0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_HAIR, 1, 0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_LEG, 1,0)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], PED_COMP_HEAD, 0,1)	
		BREAK
		CASE TAXI_MISSION_VARIATION_CLOWN_CAR
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_BUSINESS_02, <<-1284.36658, 295.74365, 63.83044>>, -178.76)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_BUSINESS_02, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1],<<0.5,0.5,0>>), 21.77)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2], INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_BUSINESS_02, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1],<<0.5,-0.5,0>>), -147.25)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3], INT_TO_ENUM(PED_COMPONENT,8), 0, 3, 0) //(accs)
			g_PropertySystemData.managementEventPed[3] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Beach_01, << -1283.4502, 299.3781, 63.9305 >>, 151.9095)
			g_PropertySystemData.managementEventPed[4] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Beach_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_PropertySystemData.managementEventPed[3],<<1.0,1.0,0>>),90.5877)
			g_PropertySystemData.managementEventPed[5] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Beach_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_PropertySystemData.managementEventPed[3],<<-1.0,1.0,0>>),163.5411)
			g_PropertySystemData.managementEventPed[6] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Beach_01, << -1282.8298, 306.2308, 63.9354 >>, 163.5411)
			g_PropertySystemData.managementEventPed[7] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_BUSINESS_01, << -1280.9871, 305.4652, 63.9499 >>, 90.5877)
			g_PropertySystemData.managementEventVehicle[0] = CREATE_VEHICLE(STRETCH, << -1290.7235, 284.8930, 63.7823 >>, 55.6864)
			g_PropertySystemData.managementEventVehicle[1] = CREATE_VEHICLE(SUPERD, << -1288.9644, 296.9287, 63.8537 >>, 67.6169)
		BREAK
	ENDSWITCH
ENDPROC

// update the taxi event in the world - return FALSE if disrupted
FUNC BOOL UPDATE_TAXI_EVENT_IN_WORLD(TAXI_MISSION_VARIATION_ENUM thisTaxi)
	INT idx
	
	SWITCH thisTaxi
		CASE TAXI_MISSION_VARIATION_CUT_YOU_IN
		CASE TAXI_MISSION_VARIATION_DEADLINE
		CASE TAXI_MISSION_VARIATION_FOLLOW_CAR
		CASE TAXI_MISSION_VARIATION_GOT_YOU_NOW
		CASE TAXI_MISSION_VARIATION_GOT_YOUR_BACK
		CASE TAXI_MISSION_VARIATION_NEEDS_EXCITEMENT
		CASE TAXI_MISSION_VARIATION_TAKE_IT_EASY
		CASE TAXI_MISSION_VARIATION_TAKE_TO_BEST
			IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1])
				IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1])
					RETURN FALSE
				ELSE
					IF IS_PED_FLEEING(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1])
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE TAXI_MISSION_VARIATION_CLOWN_CAR
			REPEAT 7 idx
				IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[idx])
					IF IS_PED_INJURED(g_PropertySystemData.managementEventPed[idx])
						RETURN FALSE
					ELSE
						IF IS_PED_FLEEING(g_PropertySystemData.managementEventPed[idx])
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC




