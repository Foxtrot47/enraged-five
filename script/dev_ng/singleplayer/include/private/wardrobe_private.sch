//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	wardrobe_private.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains all the data for each wardrobe 	//
//							location.													//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "LineActivation.sch"
USING "player_ped_public.sch"
USING "net_ambience.sch"
USING "tattoo_shop_private.sch"
USING "clothes_shop_private.sch"

TYPEDEF PROC BuildBrowseListForWardrobe(SHOP_COMPONENT_ITEMS_STRUCT &sClothesInfo, CLOTHES_MENU_ENUM eClothesMenu, INT iMainMenuGroup, SetupClothingItemForShop &fpSetupClothingItemForShop, GetPedComponentItemRequisite &fpGetPedComponentItemRequisite)
TYPEDEF PROC UnlockPlayerPedClothesForSavehouse(SAVEHOUSE_NAME_ENUM eSavehouse)

ENUM WARDROBE_STAGE_ENUM
	INITIALISE = 0,
	WAIT_FOR_TRIGGER,
	BROWSE_WARDROBE,
	EXIT_WARDROBE
ENDENUM

ENUM WARDROBE_BROWSE_STAGE_ENUM
	BROWSE_INIT = 0,
	BROWSE_INTRO,
	BROWSE_BROWSING,
	BROWSE_OUTRO,
	BROWSE_CLEANUP
ENDENUM

ENUM WARDROBE_CAMERA_TYPE_ENUM
	WARDROBE_CAMERA_TYPE_HEAD = 0,
	WARDROBE_CAMERA_TYPE_TORSO,
	WARDROBE_CAMERA_TYPE_LEGS,
	WARDROBE_CAMERA_TYPE_FEET,
	WARDROBE_CAMERA_TYPE_HAND,
	WARDROBE_CAMERA_TYPE_OUTFIT
ENDENUM

STRUCT WARDROBE_BROWSE_STRUCT
	INT iCurrentItem			// Component item in group
	INT iCurrentGroup
	INT iCurrentSubItem

	// used to block some inputs as player exits the wardrobe
	INT iInputBlockTimer
	TIME_DATATYPE tdInputBlockTimer
	BOOL bInputBlockTimerSet
	
	BOOL bMustLeaveLocate		// Flags if the player should leave the locate area before they can re-trigger
	BOOL bReset					// Flags if the wardrobe script should reset
	BOOL bPhoneDisabled			// Flags if the phone was disabled before we entered
	WARDROBE_BROWSE_STAGE_ENUM 	eStage 	// The current browse stage
	CLOTHES_MENU_ENUM 			eMenu 	// The current menu
	
	INT iMainMenuGroup
ENDSTRUCT

STRUCT WARDROBE_INFO_STRUCT

	// Wardrobe stuff
	VECTOR vWardrobeCoords
	VECTOR vBuddyCoords
	BOOL     bActive
	WARDROBE_STAGE_ENUM eStage
	enumCharacterList eCharacter
	SAVEHOUSE_NAME_ENUM eSavehouse
	INT iProperties
	PLAYER_WARDROBE_ENUM eWardrobe
	
	// Locate
	VECTOR vAngledAreaCoords[2]
	FLOAT fAngledAreaWidth
	
	// Camera
	VECTOR vCameraCoords
	VECTOR vCameraRot
	FLOAT fCameraRotMax
	VECTOR vCameraOffset
	FLOAT fCameraFOV
	FLOAT fCameraHeightOffset
	CAMERA_INDEX camID
	WARDROBE_CAMERA_TYPE_ENUM eCameraType
	
	// Player stuff
	VECTOR 	 vPlayerCoords
	FLOAT    fPlayerHead
	
	// Lights stuff
	MODEL_NAMES eWardrobeLight 
	MODEL_NAMES eWardrobeHeadLight
	
	BOOL bDataSet
ENDSTRUCT

/// PURPOSE: The main struct that we use to hold everything about a specific wardrobe
STRUCT WARDROBE_DATA_STRUCT

	WARDROBE_LAUNCHER_STRUCT sWardrobeLauncherData
	
	WARDROBE_INFO_STRUCT	sWardrobeInfo
	WARDROBE_BROWSE_STRUCT 	sBrowseInfo
	//SHOP_COMPONENT_ITEMS_STRUCT	sClothesInfo
	SHOP_INPUT_DATA_STRUCT sInputData
	
	GetPedCompDataForItem fpGetPedCompDataForItem
	GetPedCompItemCurrent fpGetPedCompItemCurrent
	SetupClothingItemForShop fpSetupClothingItemForShop
	SetPedCompItemCurrent fpSetPedCompItemCurrent
	IsPedCompItemCurrent fpIsPedCompItemCurrent
	IsPedCompItemNew fpIsPedCompItemNew
	IsPedCompItemNew fpSetPedCompItemViewed
	ForceValidPedCompComboForItem fpForceValidPedCompComboForItem
	BuildBrowseListForWardrobe fpBuildBrowseList
	UnlockPlayerPedClothesForSavehouse fpUnlockPlayerPedClothesForSavehouse
	GetPedComponentItemRequisite fpGetPedComponentItemRequisite
	SetPedCompItemIsNew fpSetPedCompItemIsNew
	IsAnyVariationOfItemAcquired fpIsAnyVariationOfItemAcquired
	DressFreemodePlayerAtStartTorso fpDressFreemodePlayerAtStartTorso
	CanPedComponentItemMixWithItem fpCanPedComponentItemMixWithItem
	
	INT iWardrobeBitset
	
	INT iBlockInputFrameCount
	
	FLOAT fDistToPlayer
	BOOL bUpdateSavehouseBlip

	MODEL_NAMES ePedModel

	BOOL bRebuildMenu, bTryOnClothesAfterRebuild, bCurrentItemIsNew
	TEXT_LABEL_15 sItemDesc
	INT iTopItem_Main, iTopItem_Sub, iTopItem_Sub2
	INt iDepth
	TEXT_LABEL_15 sHelpTrig
	PED_VARIATION_STRUCT sCurrentClothes
	
	// Camera
	VECTOR vCurrentBrowseCamLookAt//, vStoredBrowseCamLookAt
	FLOAT fLowerCamLimit
	FLOAT fUpperCamLimit
	
	FLOAT fCamZoomAlpha

	BOOL bUpdateWardrobeCamera
	BOOL bDoEndOfArm1Checks
	BOOL bArm1ResultsUp
	VECTOR vArm1Coords

	BUDDY_HIDE_STRUCT sBuddyHideData

	BLIP_INDEX wardrobeBlipID

	INT iContextID = NEW_CONTEXT_INTENTION

	BOOL bDoFinaliseHeadBlend

	BOOL bSelectedItemIsCurrent

	INT iInitStage

	// wardrobe lights
	OBJECT_INDEX mWardrobeLight // Object for the wardrobe light
	OBJECT_INDEX mWardrobeHeadLight // Object for the wardrobe light used for head close ups

	// wardrobe audio
	BOOL bSoundBankRequested = FALSE
	
	BOOL bClothesChanges = FALSE
	
	// debug variables
	#IF IS_DEBUG_BUILD

		BOOL bToggleWardrobeLight
		BOOL bWardrobeLightOn = FALSE

		BOOL bEnableDebugWardrobeInFM
		FLOAT fMenuX = 0.175 
		FLOAT fMenuY = 0.300
		FLOAT fPlayerHead
		FLOAT fLookHeightOffset
		VECTOR vPlayerCoords
		BOOL bOutputCamOffsets, bUpdatePlayerPosition, bInitPlayerPosition, bOutputPlayerPosition, bKillScript, bUpdateLookCamHeight
	#ENDIF

	BOOL bPreviewing // If true we are just previewing the item. (if false we are equipping)
	BOOL bWearingJacket // true if the player is wearing a jacket in MP
	INT iCurrentItemBeforeJacketChange // used to keep correct item highlighted when equipping / removing jacket
	BOOL bWaitingForPreload // used to preload hats and masks in MP
	TATTOO_NAME_ENUM eCrewLogoTattoo
	
	INT iSaveOutfitFlags
	INT iSavedOutfitsItem
	
	INT iMaskMenuTopItem
	
	ARMORY_TRUCK_SECTIONS_ENUM eArmoryTruckSection = ATS_INVALID_SECTION
ENDSTRUCT

// ---------------Functions ----------------------------------------------------

/// PURPOSE:
///    Blocks any inputs that conflict with the wardrobe.
///    Currently just blocks melee attack when exiting, as B is also used to exit the wardrobe.
/// PARAMS:
///    sBrowseInfo - the wardrobe browse struct
PROC UPDATE_WARDROBE_INPUT_BLOCKS(WARDROBE_BROWSE_STRUCT &sBrowseInfo)
	IF sBrowseInfo.bInputBlockTimerSet
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(sBrowseInfo.tdInputBlockTimer, 1000))
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			ELSE
				sBrowseInfo.bInputBlockTimerSet = FALSE
				CPRINTLN(DEBUG_PED_COMP, "Wardrobe clearing input block timer now.")
			ENDIF
		ELSE
			IF (GET_GAME_TIMER() - sBrowseInfo.iInputBlockTimer) < 1000
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			ELSE
				sBrowseInfo.bInputBlockTimerSet = FALSE
				CPRINTLN(DEBUG_PED_COMP, "Wardrobe clearing input block timer now.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_MP(SAVEHOUSE_NAME_ENUM eSavehouse)
	IF eSavehouse = eSavehouse
	ENDIF
ENDPROC
PROC UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_SPCLF(SAVEHOUSE_NAME_ENUM eSavehouse)
	IF eSavehouse = eSavehouse
	ENDIF
ENDPROC
PROC UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_SPNRM(SAVEHOUSE_NAME_ENUM eSavehouse)
	IF eSavehouse = eSavehouse
	ENDIF
ENDPROC
PROC UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_SP(SAVEHOUSE_NAME_ENUM eSavehouse)

#if USE_CLF_DLC
	UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_SPCLF(eSavehouse)
#endif
#if USE_NRM_DLC
	UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_SPNRM(eSavehouse)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC

	
	// Items we give the player
	SWITCH eSavehouse
		CASE SAVEHOUSE_TREVOR_VB
			SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_DRESS, TRUE, TRUE)
		BREAK
	ENDSWITCH
	
	
	// Items that can be used in limited wardrobes
	// - Default
	SWITCH eSavehouse
		CASE SAVEHOUSE_TREVOR_VB
		CASE SAVEHOUSE_TREVOR_SC
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_TANKTOP_SWEATPANTS_1, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_DRESS, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_VNECK_2, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_WHITE_TSHIRT, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_TANK_TOP_1, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_LEGS, LEGS_P2_SWEAT_PANTS, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_LEGS, LEGS_P2_CARGOPANTS_7, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_LEGS, LEGS_P2_CARGOPANTS_8, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_LEGS, LEGS_P2_CARGOPANTS_9, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_FEET, FEET_P2_REDWINGS, TRUE) 
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_PROPS, PROPS_EYES_NONE, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_PROPS, PROPS_P2_SUNGLASSES_B_0, TRUE)
		BREAK
		CASE SAVEHOUSE_MICHAEL_CS
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_01, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_BED, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_1, TRUE)
			ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_FEET, FEET_P0_BLACK_SHOES, TRUE)
		BREAK
	ENDSWITCH
	// - Current
	MODEL_NAMES ePedModel = GET_PLAYER_PED_MODEL(GET_CURRENT_PLAYER_PED_ENUM())
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_HEAD, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_HEAD), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_HAIR, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_HAIR), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_TORSO, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_LEGS, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_FEET, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_FEET), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_HAND, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_HAND), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_SPECIAL, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_SPECIAL2, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_DECL, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_DECL), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_BERD, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_BERD), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_TEETH, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TEETH), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_JBIB, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_JBIB), TRUE)
	ADD_PED_COMPONENT_ITEM_TO_LIMITED_WARDROBES(ePedModel, COMP_TYPE_OUTFIT, GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT), TRUE)



#endif
#endif	
ENDPROC


PROC SETUP_SP_CLOTHES_DLC(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	
	PED_COMP_NAME_ENUM eDLCUpper, eDLCLower, eDLCFeet, eDLCProps, eDLCOutfits, eDLCItem
	
	SWITCH iCurrentPed
		CASE 0 // Michael
			eDLCUpper = TORSO_P0_DLC
			eDLCLower = LEGS_P0_DLC
			eDLCFeet = FEET_P0_DLC
			eDLCProps = PROPS_P0_DLC
			eDLCOutfits = OUTFIT_P0_DLC
		BREAK
		CASE 1 // Franklin
			eDLCUpper = TORSO_P1_DLC
			eDLCLower = LEGS_P1_DLC
			eDLCFeet = FEET_P1_DLC
			eDLCProps = PROPS_P1_DLC
			eDLCOutfits = OUTFIT_P1_DLC
		BREAK
		CASE 2 // Trevor
			eDLCUpper = TORSO_P2_DLC
			eDLCLower = LEGS_P2_DLC
			eDLCFeet = FEET_P2_DLC
			eDLCProps = PROPS_P2_DLC
			eDLCOutfits = OUTFIT_P2_DLC
		BREAK
	ENDSWITCH
	
	scrShopPedComponent componentItem
	INIT_SHOP_PED_COMPONENT(componentItem)
	
	scrShopPedProp propItem
	INIT_SHOP_PED_PROP(propItem)
	
	scrShopPedOutfit outfitItem
	
	INT iDLCItem
	INT iDLCItemCount
	INT iDLCTorsoCount
	INT iDLCLegsCount
	INT iDLCFeetCount
	
	// DLC COMPONENTS
	iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY(iCurrentPed, ENUM_TO_INT(CLO_SHOP_LOW), 11, ENUM_TO_INT(SHOP_PED_COMPONENT))
	REPEAT iDLCItemCount iDLCItem
		GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
		
		PRINTLN("DLC COMPONENT ITEM")
		PRINTLN("...label = ", componentItem.m_textLabel)
		PRINTLN("...comp type = ", componentItem.m_eCompType)
		PRINTLN("...drawable = ", componentItem.m_drawableIndex)
		PRINTLN("...texture = ", componentItem.m_textureIndex)
		PRINTLN("...locked = ", IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash))
		
		IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
			IF componentItem.m_eCompType = ENUM_TO_INT(PED_COMP_TORSO)
				
				IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(componentItem.m_nameHash, DLC_RESTRICTION_OUTFIT_ONLY, ENUM_TO_INT(SHOP_PED_COMPONENT))
					eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCUpper)+iDLCTorsoCount)
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_JACKETS_W, eDLCItem, COMP_TYPE_TORSO, FALSE, CLO_LBL_NONE)
				ENDIF
				iDLCTorsoCount++
				
			ELIF componentItem.m_eCompType = ENUM_TO_INT(PED_COMP_LEG)
				IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(componentItem.m_nameHash, DLC_RESTRICTION_OUTFIT_ONLY, ENUM_TO_INT(SHOP_PED_COMPONENT))
					eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCLower)+iDLCLegsCount)
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_LEGS_W, eDLCItem, COMP_TYPE_LEGS, FALSE, CLO_LBL_NONE)
				ENDIF
				iDLCLegsCount++
				
			ELIF componentItem.m_eCompType = ENUM_TO_INT(PED_COMP_FEET)
				IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(componentItem.m_nameHash, DLC_RESTRICTION_OUTFIT_ONLY, ENUM_TO_INT(SHOP_PED_COMPONENT))
					eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCFeet)+iDLCFeetCount)
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_FEET_W, eDLCItem, COMP_TYPE_FEET, FALSE, CLO_LBL_NONE)
				ENDIF
				iDLCFeetCount++
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	// DLC PROPS
	iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY(iCurrentPed, ENUM_TO_INT(CLO_SHOP_LOW), 11, ENUM_TO_INT(SHOP_PED_PROP))
	REPEAT iDLCItemCount iDLCItem
		GET_SHOP_PED_QUERY_PROP(iDLCItem, propItem)
		
		PRINTLN("DLC PROP ITEM")
		PRINTLN("...label = ", propItem.m_textLabel)
		PRINTLN("...anchor = ", propItem.m_eAnchorPoint)
		PRINTLN("...drawable = ", propItem.m_propIndex)
		PRINTLN("...texture = ", propItem.m_textureIndex)
		
		IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(propItem.m_nameHash, DLC_RESTRICTION_OUTFIT_ONLY, ENUM_TO_INT(SHOP_PED_PROP))
			IF propItem.m_eAnchorPoint = ENUM_TO_INT(ANCHOR_HEAD)
				eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCProps)+iDLCItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_HATS_W, eDLCItem, COMP_TYPE_PROPS, FALSE, CLO_LBL_NONE)
				
			ELIF propItem.m_eAnchorPoint = ENUM_TO_INT(ANCHOR_EYES)
				eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCProps)+iDLCItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_GLASSES_W, eDLCItem, COMP_TYPE_PROPS, FALSE, CLO_LBL_NONE)
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	// DLC OUTFITS
	iDLCItemCount = SETUP_SHOP_PED_OUTFIT_QUERY(iCurrentPed, ENUM_TO_INT(CLO_SHOP_LOW))
	REPEAT iDLCItemCount iDLCItem
		GET_SHOP_PED_QUERY_OUTFIT(iDLCItem, outfitItem)
		PRINTLN("DLC OUTFIT ITEM - ", outfitItem.m_textLabel, " : comps = ", outfitItem.m_numberOfComponents, ", props = ", outfitItem.m_numberOfProps)
		eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCOutfits)+iDLCItem)
		CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_OUTFITS_W, eDLCItem, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_NONE)
		
	ENDREPEAT
ENDPROC
#if USE_CLF_DLC
PROC SETUP_SP_CLOTHES_OUTFITSCLF(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_DENIM				, COMP_TYPE_OUTFIT)	// Denim Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_JEWEL_HEIST 	, COMP_TYPE_OUTFIT)	// Jewel Heist Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_LEATHER_AND_JEANS	, COMP_TYPE_OUTFIT)	// Leather Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_LUDENDORFF			, COMP_TYPE_OUTFIT)	// Ludendorff
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_YOGA_FLIP_FLOPS 	, COMP_TYPE_OUTFIT)	// Polo Shirt, Beach Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_POLOSHIRT_JEANS_1	, COMP_TYPE_OUTFIT)	// Polo Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_POLOSHIRT_PANTS	, COMP_TYPE_OUTFIT)	// Polo Shirt, Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PROLOGUE			, COMP_TYPE_OUTFIT)	// Prologue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SHIRT_SHORTS_1		, COMP_TYPE_OUTFIT)	// Shirt, Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_MOVIE_TUXEDO			, COMP_TYPE_OUTFIT)	// Tuxedo
			
			// Sports
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_GOLF 				, COMP_TYPE_OUTFIT)	// Golf
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_MOTO_X			, COMP_TYPE_OUTFIT)	// Moto X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_TENNIS			, COMP_TYPE_OUTFIT)	// Tennis
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_TRIATHLON			, COMP_TYPE_OUTFIT)	// Triathlon
			
			// Services, Missions
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_COMMANDO			, COMP_TYPE_OUTFIT)	// Commando
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_HIGHWAY_PATROL 	, COMP_TYPE_OUTFIT)	// Highway Patrol
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SECURITY			, COMP_TYPE_OUTFIT)	// Security
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_STEALTH			, COMP_TYPE_OUTFIT)	// Stealth
			
			// Workers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_BLUE_BOILER_SUIT	, COMP_TYPE_OUTFIT)	// Blue Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EXTERMINATOR		, COMP_TYPE_OUTFIT)	// Exterminator
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_NAVY_JANITOR 		, COMP_TYPE_OUTFIT)	// Janitor
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler Suit
			
			// Gimmicks
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_BED 				, COMP_TYPE_OUTFIT)	// Bed
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EPSILON			, COMP_TYPE_OUTFIT)	// Epsilon Robes (You can only have one of these outfits at once!)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EPSILON_WITH_MEDAL	, COMP_TYPE_OUTFIT)	// Epsilon Robes (You can only have one of these outfits at once!)
		BREAK
		CASE CHAR_FRANKLIN
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_JEANS_1	, COMP_TYPE_OUTFIT) // Black Hoodie, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TUXEDO				, COMP_TYPE_OUTFIT)	// Black Tuxedo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_DEFAULT 				, COMP_TYPE_OUTFIT)	// Blue Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_JEANS_3 	, COMP_TYPE_OUTFIT)	// Gray Hoodie, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_SWEATS		, COMP_TYPE_OUTFIT)	// Gray Hoodie, Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SUIT_1				, COMP_TYPE_OUTFIT) // Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_GREEN_SHIRT_JEANS		, COMP_TYPE_OUTFIT)	// Light Yellow Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_WHITE_SHIRT_JEANS		, COMP_TYPE_OUTFIT) // Off-White Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TRACKSUIT_JEANS		, COMP_TYPE_OUTFIT) // Track Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_WHITE_TUXEDO 			, COMP_TYPE_OUTFIT)	// White Tuxedo
			
			// Uptown Ryders
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_1 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_2 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_3 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_4 			, COMP_TYPE_OUTFIT)	
			
			// Sports
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_GOLF					, COMP_TYPE_OUTFIT)	// Golf
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_MOTO_X					, COMP_TYPE_OUTFIT)	// Moto X
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TRIATHLON				, COMP_TYPE_OUTFIT)	// Triathlon
			
			
			// Services, Missions
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SKYDIVING				, COMP_TYPE_OUTFIT)	// Skydiving
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_STEALTH				, COMP_TYPE_OUTFIT)	// Stealth
			
			
			// Workers
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_BLACK_BOILER			, COMP_TYPE_OUTFIT)	// Black Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_BLUE_BOILER_SUIT		, COMP_TYPE_OUTFIT)	// Blue Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_EXTERMINATOR			, COMP_TYPE_OUTFIT)	// Exterminator
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler
			
			// Gimmicks
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UNDERWEAR				, COMP_TYPE_OUTFIT)	// Underwear
			
		BREAK
		CASE CHAR_TREVOR
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DENIM					, COMP_TYPE_OUTFIT) // Denim Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_JEANS_1			, COMP_TYPE_OUTFIT) // Lavender Polo Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_LUDENDORFF				, COMP_TYPE_OUTFIT) // Ludendorff
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PROLOGUE				, COMP_TYPE_OUTFIT)	// Prologue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_2		, COMP_TYPE_OUTFIT)	// Red T-Shirt, Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TANKTOP_SWEATPANTS_1	, COMP_TYPE_OUTFIT)	// White Tank Top, Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_3		, COMP_TYPE_OUTFIT)	// White T-Shirt, Camo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_1		, COMP_TYPE_OUTFIT) // White T-Shirt, Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_JEANS_2		, COMP_TYPE_OUTFIT) // White T-Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DEFAULT 				, COMP_TYPE_OUTFIT)	// White T-Shirt, Sweat Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TUXEDO					, COMP_TYPE_OUTFIT)	// Tuxedo

			// Sports
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_GOLF				, COMP_TYPE_OUTFIT)	// Golf
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_HUNTING			, COMP_TYPE_OUTFIT)	// Hunting
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_MOTO_X				, COMP_TYPE_OUTFIT)	// Moto-X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TENNIS				, COMP_TYPE_OUTFIT)	// Tennis
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TRIATHLON			, COMP_TYPE_OUTFIT)	// Triathlon
			
			// Services, Missions
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_HIGHWAY_PATROL 	, COMP_TYPE_OUTFIT)	// Highway Patrol
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_SECURITY			, COMP_TYPE_OUTFIT)	// Security
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_STEALTH			, COMP_TYPE_OUTFIT)	// Stealth
			
			// Workers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_BLUE_BOILER_SUIT	, COMP_TYPE_OUTFIT)	// Blue Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DOCK_WORKER 		, COMP_TYPE_OUTFIT)	// Dock Worker
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler Suit
			
			// Gimmicks
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_LADIES				, COMP_TYPE_OUTFIT)	// Pink Ladies Sweats
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_UNDERWEAR			, COMP_TYPE_OUTFIT)	// Underwear
		BREAK
	ENDSWITCH
ENDPROC

#endif
#if USE_NRM_DLC
PROC SETUP_SP_CLOTHES_OUTFITSNRM(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_DENIM				, COMP_TYPE_OUTFIT)	// Denim Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_JEWEL_HEIST 	, COMP_TYPE_OUTFIT)	// Jewel Heist Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_LEATHER_AND_JEANS	, COMP_TYPE_OUTFIT)	// Leather Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_LUDENDORFF			, COMP_TYPE_OUTFIT)	// Ludendorff
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_YOGA_FLIP_FLOPS 	, COMP_TYPE_OUTFIT)	// Polo Shirt, Beach Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_POLOSHIRT_JEANS_1	, COMP_TYPE_OUTFIT)	// Polo Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_POLOSHIRT_PANTS	, COMP_TYPE_OUTFIT)	// Polo Shirt, Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PROLOGUE			, COMP_TYPE_OUTFIT)	// Prologue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SHIRT_SHORTS_1		, COMP_TYPE_OUTFIT)	// Shirt, Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_MOVIE_TUXEDO			, COMP_TYPE_OUTFIT)	// Tuxedo
			
			// Sports
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_GOLF 				, COMP_TYPE_OUTFIT)	// Golf
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_MOTO_X			, COMP_TYPE_OUTFIT)	// Moto X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_TENNIS			, COMP_TYPE_OUTFIT)	// Tennis
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_TRIATHLON			, COMP_TYPE_OUTFIT)	// Triathlon
			
			// Services, Missions
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_COMMANDO			, COMP_TYPE_OUTFIT)	// Commando
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_HIGHWAY_PATROL 	, COMP_TYPE_OUTFIT)	// Highway Patrol
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SECURITY			, COMP_TYPE_OUTFIT)	// Security
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_STEALTH			, COMP_TYPE_OUTFIT)	// Stealth
			
			// Workers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_BLUE_BOILER_SUIT	, COMP_TYPE_OUTFIT)	// Blue Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EXTERMINATOR		, COMP_TYPE_OUTFIT)	// Exterminator
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_NAVY_JANITOR 		, COMP_TYPE_OUTFIT)	// Janitor
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler Suit
			
			// Gimmicks
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_BED 				, COMP_TYPE_OUTFIT)	// Bed
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EPSILON			, COMP_TYPE_OUTFIT)	// Epsilon Robes (You can only have one of these outfits at once!)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EPSILON_WITH_MEDAL	, COMP_TYPE_OUTFIT)	// Epsilon Robes (You can only have one of these outfits at once!)
		BREAK
		CASE CHAR_FRANKLIN
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_JEANS_1	, COMP_TYPE_OUTFIT) // Black Hoodie, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TUXEDO				, COMP_TYPE_OUTFIT)	// Black Tuxedo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_DEFAULT 				, COMP_TYPE_OUTFIT)	// Blue Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_JEANS_3 	, COMP_TYPE_OUTFIT)	// Gray Hoodie, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_SWEATS		, COMP_TYPE_OUTFIT)	// Gray Hoodie, Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SUIT_1				, COMP_TYPE_OUTFIT) // Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_GREEN_SHIRT_JEANS		, COMP_TYPE_OUTFIT)	// Light Yellow Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_WHITE_SHIRT_JEANS		, COMP_TYPE_OUTFIT) // Off-White Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TRACKSUIT_JEANS		, COMP_TYPE_OUTFIT) // Track Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_WHITE_TUXEDO 			, COMP_TYPE_OUTFIT)	// White Tuxedo
			
			// Uptown Ryders
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_1 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_2 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_3 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_4 			, COMP_TYPE_OUTFIT)	
			
			// Sports
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_GOLF					, COMP_TYPE_OUTFIT)	// Golf
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_MOTO_X					, COMP_TYPE_OUTFIT)	// Moto X
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TRIATHLON				, COMP_TYPE_OUTFIT)	// Triathlon
			
			
			// Services, Missions
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SKYDIVING				, COMP_TYPE_OUTFIT)	// Skydiving
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_STEALTH				, COMP_TYPE_OUTFIT)	// Stealth
			
			
			// Workers
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_BLACK_BOILER			, COMP_TYPE_OUTFIT)	// Black Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_BLUE_BOILER_SUIT		, COMP_TYPE_OUTFIT)	// Blue Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_EXTERMINATOR			, COMP_TYPE_OUTFIT)	// Exterminator
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler
			
			// Gimmicks
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UNDERWEAR				, COMP_TYPE_OUTFIT)	// Underwear
			
		BREAK
		CASE CHAR_TREVOR
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DENIM					, COMP_TYPE_OUTFIT) // Denim Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_JEANS_1			, COMP_TYPE_OUTFIT) // Lavender Polo Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_LUDENDORFF				, COMP_TYPE_OUTFIT) // Ludendorff
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PROLOGUE				, COMP_TYPE_OUTFIT)	// Prologue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_2		, COMP_TYPE_OUTFIT)	// Red T-Shirt, Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TANKTOP_SWEATPANTS_1	, COMP_TYPE_OUTFIT)	// White Tank Top, Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_3		, COMP_TYPE_OUTFIT)	// White T-Shirt, Camo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_1		, COMP_TYPE_OUTFIT) // White T-Shirt, Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_JEANS_2		, COMP_TYPE_OUTFIT) // White T-Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DEFAULT 				, COMP_TYPE_OUTFIT)	// White T-Shirt, Sweat Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TUXEDO					, COMP_TYPE_OUTFIT)	// Tuxedo

			// Sports
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_GOLF				, COMP_TYPE_OUTFIT)	// Golf
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_HUNTING			, COMP_TYPE_OUTFIT)	// Hunting
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_MOTO_X				, COMP_TYPE_OUTFIT)	// Moto-X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TENNIS				, COMP_TYPE_OUTFIT)	// Tennis
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TRIATHLON			, COMP_TYPE_OUTFIT)	// Triathlon
			
			// Services, Missions
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_HIGHWAY_PATROL 	, COMP_TYPE_OUTFIT)	// Highway Patrol
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_SECURITY			, COMP_TYPE_OUTFIT)	// Security
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_STEALTH			, COMP_TYPE_OUTFIT)	// Stealth
			
			// Workers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_BLUE_BOILER_SUIT	, COMP_TYPE_OUTFIT)	// Blue Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DOCK_WORKER 		, COMP_TYPE_OUTFIT)	// Dock Worker
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler Suit
			
			// Gimmicks
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_LADIES				, COMP_TYPE_OUTFIT)	// Pink Ladies Sweats
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_UNDERWEAR			, COMP_TYPE_OUTFIT)	// Underwear
		BREAK
	ENDSWITCH
ENDPROC
#endif
PROC SETUP_SP_CLOTHES_OUTFITS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	#if USE_CLF_DLC
		SETUP_SP_CLOTHES_OUTFITSCLF(fpSetupClothingItemForShop,sTempClothesInfo,iCurrentPed,eCurrentSubMenu)
	#endif
	#if USE_NRM_DLC
		SETUP_SP_CLOTHES_OUTFITSNRM(fpSetupClothingItemForShop,sTempClothesInfo,iCurrentPed,eCurrentSubMenu)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_DENIM				, COMP_TYPE_OUTFIT)	// Denim Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_JEWEL_HEIST 	, COMP_TYPE_OUTFIT)	// Jewel Heist Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_LEATHER_AND_JEANS	, COMP_TYPE_OUTFIT)	// Leather Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_LUDENDORFF			, COMP_TYPE_OUTFIT)	// Ludendorff
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_YOGA_FLIP_FLOPS 	, COMP_TYPE_OUTFIT)	// Polo Shirt, Beach Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_POLOSHIRT_JEANS_1	, COMP_TYPE_OUTFIT)	// Polo Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_POLOSHIRT_PANTS	, COMP_TYPE_OUTFIT)	// Polo Shirt, Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PROLOGUE			, COMP_TYPE_OUTFIT)	// Prologue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SHIRT_SHORTS_1		, COMP_TYPE_OUTFIT)	// Shirt, Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_MOVIE_TUXEDO			, COMP_TYPE_OUTFIT)	// Tuxedo
			
			// Sports
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_GOLF 				, COMP_TYPE_OUTFIT)	// Golf
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_MOTO_X			, COMP_TYPE_OUTFIT)	// Moto X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_TENNIS			, COMP_TYPE_OUTFIT)	// Tennis
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_TRIATHLON			, COMP_TYPE_OUTFIT)	// Triathlon
			ENDIF
			
			// Services, Missions
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_COMMANDO			, COMP_TYPE_OUTFIT)	// Commando
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_HIGHWAY_PATROL 	, COMP_TYPE_OUTFIT)	// Highway Patrol
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_SECURITY			, COMP_TYPE_OUTFIT)	// Security
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_STEALTH			, COMP_TYPE_OUTFIT)	// Stealth
			ENDIF
			
			// Workers
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_BLUE_BOILER_SUIT	, COMP_TYPE_OUTFIT)	// Blue Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EXTERMINATOR		, COMP_TYPE_OUTFIT)	// Exterminator
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_NAVY_JANITOR 		, COMP_TYPE_OUTFIT)	// Janitor
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler Suit
			ENDIF
			
			// Gimmicks
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_BED 				, COMP_TYPE_OUTFIT)	// Bed
			ENDIF
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EPSILON			, COMP_TYPE_OUTFIT)	// Epsilon Robes (You can only have one of these outfits at once!)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P0_EPSILON_WITH_MEDAL	, COMP_TYPE_OUTFIT)	// Epsilon Robes (You can only have one of these outfits at once!)
		BREAK
		CASE CHAR_FRANKLIN
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_JEANS_1	, COMP_TYPE_OUTFIT) // Black Hoodie, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TUXEDO				, COMP_TYPE_OUTFIT)	// Black Tuxedo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_DEFAULT 				, COMP_TYPE_OUTFIT)	// Blue Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_JEANS_3 	, COMP_TYPE_OUTFIT)	// Gray Hoodie, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_HOODIE_AND_SWEATS		, COMP_TYPE_OUTFIT)	// Gray Hoodie, Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SUIT_1				, COMP_TYPE_OUTFIT) // Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_GREEN_SHIRT_JEANS		, COMP_TYPE_OUTFIT)	// Light Yellow Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_WHITE_SHIRT_JEANS		, COMP_TYPE_OUTFIT) // Off-White Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TRACKSUIT_JEANS		, COMP_TYPE_OUTFIT) // Track Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_WHITE_TUXEDO 			, COMP_TYPE_OUTFIT)	// White Tuxedo
			
			// Uptown Ryders
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_1 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_2 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_3 			, COMP_TYPE_OUTFIT)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UPTOWN_4 			, COMP_TYPE_OUTFIT)	
			
			// Sports
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_GOLF					, COMP_TYPE_OUTFIT)	// Golf
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_MOTO_X					, COMP_TYPE_OUTFIT)	// Moto X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_TRIATHLON				, COMP_TYPE_OUTFIT)	// Triathlon
			ENDIF
			
			// Services, Missions
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_SKYDIVING				, COMP_TYPE_OUTFIT)	// Skydiving
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_STEALTH				, COMP_TYPE_OUTFIT)	// Stealth
			ENDIF
			
			// Workers
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_BLACK_BOILER			, COMP_TYPE_OUTFIT)	// Black Boiler
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_BLUE_BOILER_SUIT		, COMP_TYPE_OUTFIT)	// Blue Boiler
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_EXTERMINATOR			, COMP_TYPE_OUTFIT)	// Exterminator
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler
			ENDIF
			
			// Gimmicks
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P1_UNDERWEAR				, COMP_TYPE_OUTFIT)	// Underwear
			ENDIF
		BREAK
		CASE CHAR_TREVOR
			// Regular clothes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DENIM					, COMP_TYPE_OUTFIT) // Denim Jacket, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_JEANS_1			, COMP_TYPE_OUTFIT) // Lavender Polo Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_LUDENDORFF				, COMP_TYPE_OUTFIT) // Ludendorff
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PROLOGUE				, COMP_TYPE_OUTFIT)	// Prologue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_2		, COMP_TYPE_OUTFIT)	// Red T-Shirt, Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TANKTOP_SWEATPANTS_1	, COMP_TYPE_OUTFIT)	// White Tank Top, Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_3		, COMP_TYPE_OUTFIT)	// White T-Shirt, Camo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_CARGOPANTS_1		, COMP_TYPE_OUTFIT) // White T-Shirt, Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TSHIRT_JEANS_2		, COMP_TYPE_OUTFIT) // White T-Shirt, Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DEFAULT 				, COMP_TYPE_OUTFIT)	// White T-Shirt, Sweat Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TUXEDO					, COMP_TYPE_OUTFIT)	// Tuxedo

			// Sports
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_GOLF				, COMP_TYPE_OUTFIT)	// Golf
			ENDIF	
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_HUNTING			, COMP_TYPE_OUTFIT)	// Hunting
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_MOTO_X				, COMP_TYPE_OUTFIT)	// Moto-X
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TENNIS				, COMP_TYPE_OUTFIT)	// Tennis
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_TRIATHLON			, COMP_TYPE_OUTFIT)	// Triathlon
			ENDIF
			
			// Services, Missions
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_HIGHWAY_PATROL 	, COMP_TYPE_OUTFIT)	// Highway Patrol
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_SECURITY			, COMP_TYPE_OUTFIT)	// Security
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_SCUBA_LAND			, COMP_TYPE_OUTFIT)	// Scuba Land
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_STEALTH			, COMP_TYPE_OUTFIT)	// Stealth
			ENDIF
			
			// Workers
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_BLUE_BOILER_SUIT	, COMP_TYPE_OUTFIT)	// Blue Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_DOCK_WORKER 		, COMP_TYPE_OUTFIT)	// Dock Worker
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_2	, COMP_TYPE_OUTFIT)	// Gray Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_3	, COMP_TYPE_OUTFIT)	// Green Boiler Suit
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_PREP_BOILER_SUIT_1	, COMP_TYPE_OUTFIT)	// Navy Boiler Suit
			ENDIF
			
			// Gimmicks
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_LADIES				, COMP_TYPE_OUTFIT)	// Pink Ladies Sweats
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_OUTFITS_W, OUTFIT_P2_UNDERWEAR			, COMP_TYPE_OUTFIT)	// Underwear
			ENDIF
		BREAK
	ENDSWITCH
	#endif
	#endif
ENDPROC

// This proc sets up the SUITS menu which now contains only submenus
PROC SETUP_SP_CLOTHES_SUITS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SUITS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// Suits
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_FULLSUITS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_JACKETS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_PANTS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_VESTS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_SWEATERS)	// SUB MENU
		BREAK
		CASE CHAR_FRANKLIN
			// Suits
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_FULLSUITS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_JACKETS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_JACKETSB)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_PANTS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_VESTS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_TIES)	// SUB MENU
		BREAK
		CASE CHAR_TREVOR
			// Suits
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_FULLSUITS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_JACKETS)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SUIT_PANTS)	// SUB MENU
		BREAK
	ENDSWITCH
ENDPROC

// This proc sets up suit outfits
PROC SETUP_SP_CLOTHES_SUITSFULL(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SUITSFULL_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// Suits
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_BLACK_SUIT		, COMP_TYPE_OUTFIT)	// Black Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_DARK_GRAY_SUIT	, COMP_TYPE_OUTFIT)	// Dark Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_DEFAULT 		, COMP_TYPE_OUTFIT)	// Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_NAVY_SUIT		, COMP_TYPE_OUTFIT)	// Navy Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_5			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_6			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_7			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_8			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_9			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_10			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_11			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_12			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_13			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_14			, COMP_TYPE_OUTFIT)	// Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_P0_SUIT_15			, COMP_TYPE_OUTFIT)	// Suit
		BREAK
		CASE CHAR_FRANKLIN
			// Suits
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_6 		, COMP_TYPE_OUTFIT)	// Beige 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_7 		, COMP_TYPE_OUTFIT)	// Cream 3 Piece Suit 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_4 		, COMP_TYPE_OUTFIT)	// Dark Gray 3 Piece Suit 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_2 		, COMP_TYPE_OUTFIT)	// Dark Gray Plaid 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_0 		, COMP_TYPE_OUTFIT)	// Gray Plaid 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_5 		, COMP_TYPE_OUTFIT)	// Light Gray 3 Piece Suit 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_9 		, COMP_TYPE_OUTFIT)	// Off-White  3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_8 		, COMP_TYPE_OUTFIT)	// Pale Beige 3 Piece Suit 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_1 		, COMP_TYPE_OUTFIT)	// Pale Gray Plaid 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_11 		, COMP_TYPE_OUTFIT)	// Subtle Blue 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_3 		, COMP_TYPE_OUTFIT)	// Tan Plaid 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_10 		, COMP_TYPE_OUTFIT)	// White 3 Piece Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_12 		, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_13		, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_14 		, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_P1_3PC_SUIT_15		, COMP_TYPE_OUTFIT)	// 
		BREAK
		CASE CHAR_TREVOR
			// Suits
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_0			, COMP_TYPE_OUTFIT)	// Beige Suit, White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_1			, COMP_TYPE_OUTFIT)	// Blue Suit, Off-White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_2			, COMP_TYPE_OUTFIT)	// Mustard Suit, Brown Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_3			, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_4			, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_5			, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_6			, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_7			, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_8			, COMP_TYPE_OUTFIT)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_CHEAPSUIT_9			, COMP_TYPE_OUTFIT)	// 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_0		, COMP_TYPE_OUTFIT)	// Gray Suit, White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_1		, COMP_TYPE_OUTFIT)	// Off-White Suit, Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_2		, COMP_TYPE_OUTFIT)	// Gray Suit, Blue Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_3		, COMP_TYPE_OUTFIT)	// Off-White Suit, White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_4		, COMP_TYPE_OUTFIT)	// Cream Suit, Striped Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_5		, COMP_TYPE_OUTFIT)	// Black Suit, Black Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_P2_STYLESUIT_6		, COMP_TYPE_OUTFIT)	// Pale Gray Suit, White Shirt
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_TORSOS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_TORSO_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_BARE_CHEST 				, COMP_TYPE_TORSO, TRUE, CLO_LBL_NO_TOP) // No Top
				ENDIF
			#endif
			#endif
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GILET_0				  	, COMP_TYPE_TORSO)	// Black Gilet, Yellow T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GILET_3					, COMP_TYPE_TORSO)	// Brown Gilet, White T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GILET_1				  	, COMP_TYPE_TORSO)	// Cream Gilet, Gray T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GILET_4					, COMP_TYPE_TORSO)	// Gray Gilet, Blue T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GILET_2					, COMP_TYPE_TORSO)	// Gray Gilet, Mint T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GILET_5					, COMP_TYPE_TORSO)	// Yellow Gilet, Gray T-Shirt
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_TENNIS					, COMP_TYPE_TORSO)	// Tennis Shirt and Sweater Vest
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_TENNIS_1					, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_TENNIS_2					, COMP_TYPE_TORSO)	// 
			ENDIF
			#endif
			#endif
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SHIRT_AND_GILET_0		, COMP_TYPE_TORSO)	// Brown Shooting Vest, Red Check Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SHIRT_AND_GILET_3		, COMP_TYPE_TORSO)	// Brown Shooting Vest, Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SHIRT_AND_GILET_1		, COMP_TYPE_TORSO)	// Camo Shooting Vest, Olive Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SHIRT_AND_GILET_4		, COMP_TYPE_TORSO)	// Camo Shooting Vest, Dark Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SHIRT_AND_GILET_2		, COMP_TYPE_TORSO)	// Taupe Shooting Vest, Gray Check Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SHIRT_AND_GILET_5		, COMP_TYPE_TORSO)	// 
						
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK					, COMP_TYPE_TORSO)	// V Necked Sweater, Pale Blue Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_1					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_2					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_3					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_4					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_5					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_6					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_7					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_8					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VNECK_9					, COMP_TYPE_TORSO)	// 
			
		BREAK
		CASE CHAR_FRANKLIN
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BARE_CHEST				,COMP_TYPE_TORSO, TRUE, CLO_LBL_NO_TOP)	// No top
				ENDIF
			#endif
			#endif
			// Longsleeve
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLACK_LNGSLEEVE		, COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BROKER_LNGSLEEVE		, COMP_TYPE_TORSO)	// Broker
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_UPTOWN_LNGSLEEVE		, COMP_TYPE_TORSO)	// Crevis
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GREEN_LNGSLEEVE		, COMP_TYPE_TORSO)	// Feud Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_FEUD_LNGSLEEVE		 	, COMP_TYPE_TORSO)	// Feud White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_FRUNTALOT_LNGSLEEVE	, COMP_TYPE_TORSO)	// Fruntalot
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GRAY_LNGSLEEVE		 	, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_KING_LNGSLEEVE		 	, COMP_TYPE_TORSO)	// Kingz Of Los Santos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_STRIPED_LNGSLEEVE		, COMP_TYPE_TORSO)	// Stank Striped
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATBOX_LNGSLEEVE	 	, COMP_TYPE_TORSO)	// Sweatbox
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TEN_LNGSLEEVE			, COMP_TYPE_TORSO)	// Ten Off The Chain
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLUE_LNGSLEEVE		 	, COMP_TYPE_TORSO)	// Trey Baker
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CAMO_YETI_LNGSLEEVE	, COMP_TYPE_TORSO)	// Yeti Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLACK_YETI_LNGSLEEVE	, COMP_TYPE_TORSO)	// Yeti Rainbow
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_KHAKI_LNGSLEEVE		, COMP_TYPE_TORSO)	// Yogarishima
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WHITE_LNGSLEEVE		, COMP_TYPE_TORSO)	// White
			// Sweaters
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_11			   	, COMP_TYPE_TORSO)	// Amethyst
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_8			   	, COMP_TYPE_TORSO)	// Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_12			   	, COMP_TYPE_TORSO)	// Brown Two-Tone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_15			   	, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_9			   	, COMP_TYPE_TORSO)	// Cream
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_5			   	, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_2			   	, COMP_TYPE_TORSO)	// Gray Striped
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_13			   	, COMP_TYPE_TORSO)	// Gray Two-Tone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_10			   	, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_1			   	, COMP_TYPE_TORSO)	// Jade Striped
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_6			   	, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GOLF			   		, COMP_TYPE_TORSO)	// Pine Striped
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_7			   	, COMP_TYPE_TORSO)	// Sand
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_3			   	, COMP_TYPE_TORSO)	// Sand Striped
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_14			   	, COMP_TYPE_TORSO)	// Sand Two-Tone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SWEATER_4			   	, COMP_TYPE_TORSO)	// Silver
			// Shooting Vests
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHOOTING_VEST_3		, COMP_TYPE_TORSO)	// Brown Shooting Vest, Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHOOTING_VEST_0		, COMP_TYPE_TORSO)	// Brown Shooting Vest, Red Check Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHOOTING_VEST_4		, COMP_TYPE_TORSO)	// Camo Shooting Vest, Dark Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHOOTING_VEST_1		, COMP_TYPE_TORSO)	// Camo Shooting Vest, Olive Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHOOTING_VEST_2		, COMP_TYPE_TORSO)	// Taupe Shooting Vest, Gray Check Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHOOTING_VEST_5		, COMP_TYPE_TORSO)	// 
			// Cardigans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_9				, COMP_TYPE_TORSO)	// Aqua
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_3				, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_5				, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_1				, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_10			, COMP_TYPE_TORSO)	// Gray Two-Tone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_0				, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_8				, COMP_TYPE_TORSO)	// Pale Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_11			, COMP_TYPE_TORSO)	// Pink 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_4				, COMP_TYPE_TORSO)	// Red
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_7				, COMP_TYPE_TORSO)	// Silver
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_2				, COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_6				, COMP_TYPE_TORSO)	// White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CARDIGAN_12			, COMP_TYPE_TORSO)	// Yellow
			// Basketball tops
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_0		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_1		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_2		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_3		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_4		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_5		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_6		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_7		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BASKETBALL_8		, COMP_TYPE_TORSO)	// 
		BREAK
		CASE CHAR_TREVOR
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_NONE 					, COMP_TYPE_TORSO, TRUE, CLO_LBL_NO_TOP)
			ENDIF
			
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_4					, COMP_TYPE_TORSO)	// Cream Dress
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS					, COMP_TYPE_TORSO)	// Mint Dress with Pink Flowers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_1					, COMP_TYPE_TORSO)	// Pink Dress
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_3					, COMP_TYPE_TORSO)	// Purple and Black Dress
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_2					, COMP_TYPE_TORSO)	// White Dress with Red Flowers
			ENDIF
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_5					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_6					, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DRESS_7					, COMP_TYPE_TORSO)	// 
			
			#endif
			#endif
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GILET_3					, COMP_TYPE_TORSO)	// Brown Shooting Vest, Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GILET					, COMP_TYPE_TORSO)	// Brown Shooting Vest, Red Check Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GILET_4					, COMP_TYPE_TORSO)	// Camo Shooting Vest, Dark Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GILET_1					, COMP_TYPE_TORSO)	// Camo Shooting Vest, Olive Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GILET_2					, COMP_TYPE_TORSO)	// Taupe Shooting Vest, Gray Check Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GILET_5					, COMP_TYPE_TORSO)	// 

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_10				, COMP_TYPE_TORSO)	// Animal Strip Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_11				, COMP_TYPE_TORSO)	// Bill Brown Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_6				, COMP_TYPE_TORSO)	// Dark Chevrons Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_9				, COMP_TYPE_TORSO)	// Dark Field Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER					, COMP_TYPE_TORSO)	// Dark Gray Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_15				, COMP_TYPE_TORSO)	// Geometric Eighties Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_14				, COMP_TYPE_TORSO)	// Grape Eighties Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_13				, COMP_TYPE_TORSO)	// Gray Argyle Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_12				, COMP_TYPE_TORSO)	// Mint Argyle Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_3				, COMP_TYPE_TORSO)	// Pale Blue Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_1				, COMP_TYPE_TORSO)	// Pale Gray Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_5				, COMP_TYPE_TORSO)	// Quarry Brown Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_8				, COMP_TYPE_TORSO)	// Rainbow Field Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_4				, COMP_TYPE_TORSO)	// Tangerine Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_2				, COMP_TYPE_TORSO)	// Taupe Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WOOL_SWEATER_7				, COMP_TYPE_TORSO)	// Zingy Chevrons Sweater
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_JACKETS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_JACKETS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_LEATHER_JACKET_0		, COMP_TYPE_TORSO)	// Brown Leather Jacket, Black Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_LEATHER_JACKET_1		, COMP_TYPE_TORSO)	//  Leather Jacket 1
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_LEATHER_JACKET_2		, COMP_TYPE_TORSO)	//  Leather Jacket 2
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_LEATHER_JACKET_3		, COMP_TYPE_TORSO)	//  Leather Jacket 3
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_LEATHER_JACKET_4		, COMP_TYPE_TORSO)	//  Leather Jacket 4
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_LEATHER_JACKET_5		, COMP_TYPE_TORSO)	//  Leather Jacket 5
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HEAVY_JACKET			, COMP_TYPE_TORSO)	// Gray Jacket, Black Polo Neck
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HEAVY_JACKET_1		, COMP_TYPE_TORSO)	// 

		BREAK
		CASE CHAR_FRANKLIN
			// Army Jackets
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_4		, COMP_TYPE_TORSO)	// Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_1		, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_7		, COMP_TYPE_TORSO)	// Desert Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_6		, COMP_TYPE_TORSO)	// Field Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET			, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_3		, COMP_TYPE_TORSO)	// Olive
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_5		, COMP_TYPE_TORSO)	// Sand
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ARMY_JACKET_2		, COMP_TYPE_TORSO)	// Silver
			// Varsity Jackets
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_10			, COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_4			, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_2			, COMP_TYPE_TORSO)	// Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_6			, COMP_TYPE_TORSO)	// Coffee
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_3			, COMP_TYPE_TORSO)	// Crimson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_12			, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_5			, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_7			, COMP_TYPE_TORSO)	// Olive
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_8			, COMP_TYPE_TORSO)	// Mustard
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_15			, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_9			, COMP_TYPE_TORSO)	// Purple
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_1			, COMP_TYPE_TORSO)	// Red
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_14			, COMP_TYPE_TORSO)	// Silver
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_13			, COMP_TYPE_TORSO)	// Silver Two-Tone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY_11			, COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_VARSITY				, COMP_TYPE_TORSO)	// Yellow
			// Track Jackets
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_14	, COMP_TYPE_TORSO)	// Animal Print Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_0		, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_15	, COMP_TYPE_TORSO)	// Blue Techno Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_12	, COMP_TYPE_TORSO)	// Brown Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_8		, COMP_TYPE_TORSO)	// Coffee Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_3		, COMP_TYPE_TORSO)	// Corkers Green Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_4		, COMP_TYPE_TORSO)	// Deep Grey Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_13	, COMP_TYPE_TORSO)	// LS Snake Weave Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_11	, COMP_TYPE_TORSO)	// Mocha Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_1		, COMP_TYPE_TORSO)	// OG Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_2		, COMP_TYPE_TORSO)	// OG White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_9		, COMP_TYPE_TORSO)	// Plain Coffee Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_6		, COMP_TYPE_TORSO)	// Stank Forest Camo Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_7		, COMP_TYPE_TORSO)	// Trey Baker Desert Camo Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_5		, COMP_TYPE_TORSO)	// Trey Baker Yellow Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_TRACKSUIT_10	, COMP_TYPE_TORSO)	// Fruntalot Green Jacket
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WOOL_PEACOAT		, COMP_TYPE_TORSO)	// Heavy Wool Peacoat
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WOOL_PEACOAT_1		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WOOL_PEACOAT_2		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WOOL_PEACOAT_3		, COMP_TYPE_TORSO)	// 
			
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_JACKET_1				, COMP_TYPE_TORSO)	// Black, Black Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_JACKET_5				, COMP_TYPE_TORSO)	// Black, Brown Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_JACKET_3				, COMP_TYPE_TORSO)	// Brown, Faded Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DENIM_JACKET			, COMP_TYPE_TORSO)	// Denim, White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_JACKET_4				, COMP_TYPE_TORSO)	// Faded Denim, Black Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_JACKET_2				, COMP_TYPE_TORSO)	// Gray, Check Shirt

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_5				, COMP_TYPE_TORSO)	// Champagne Driver Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_4				, COMP_TYPE_TORSO)	// Deep Green Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_7				, COMP_TYPE_TORSO)	// Desert Brown Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_2				, COMP_TYPE_TORSO)	// Natural Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_1				, COMP_TYPE_TORSO)	// Overlooked Red Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON				, COMP_TYPE_TORSO)	// Peyton Blue Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_3				, COMP_TYPE_TORSO)	// Sky Blue Blouson
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BLOUSON_6				, COMP_TYPE_TORSO)	// Snakeskin Blouson
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_10		, COMP_TYPE_TORSO)	// Black and Russet Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET		, COMP_TYPE_TORSO)	// Black and White Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_1		, COMP_TYPE_TORSO)	// Black and Blue Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_7		, COMP_TYPE_TORSO)	// Brown and Cream Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_6		, COMP_TYPE_TORSO)	// Brown Lonewolf Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_11		, COMP_TYPE_TORSO)	// Brown Suede
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_2		, COMP_TYPE_TORSO)	// Chestnut Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_5		, COMP_TYPE_TORSO)	// Gray and Blue Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_8		, COMP_TYPE_TORSO)	// Gray and Red Striped Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_9		, COMP_TYPE_TORSO)	// Gray and White Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_3		, COMP_TYPE_TORSO)	// Red and White Leather
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LEATHER_JACKET_4		, COMP_TYPE_TORSO)	// White and Black Leather
			// Down jackets (puffer jacket)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_10	   	, COMP_TYPE_TORSO)	// Aqua Two-Tone Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_7		   	, COMP_TYPE_TORSO)	// Beige Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_12	   	, COMP_TYPE_TORSO)	// Blue Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_3		   	, COMP_TYPE_TORSO)	// Coffee Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_2		   	, COMP_TYPE_TORSO)	// Fluorescent Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_8		   	, COMP_TYPE_TORSO)	// Fluorescent Two-Tone Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_14	   	, COMP_TYPE_TORSO)	// Gray Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_1		   	, COMP_TYPE_TORSO)	// Green Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_9		   	, COMP_TYPE_TORSO)	// Jade Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_11	   	, COMP_TYPE_TORSO)	// Olive Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_13	   	, COMP_TYPE_TORSO)	// Orange Two-Tone Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_6		   	, COMP_TYPE_TORSO)	// Red Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_5		   	, COMP_TYPE_TORSO)	// Silver Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LUDENDORFF	   		, COMP_TYPE_TORSO)	// Slate Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_15	   	, COMP_TYPE_TORSO)	// Tan Down Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DOWN_JACKET_4		   	, COMP_TYPE_TORSO)	// Yellow Two-Tone Down Jacket
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_CASUAL_JACKETS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_CASUAL_JACKETS_W
	// THIS SHOULD ONLY BE USED FOR FRANKLIN'S JACKETS THAT NEED JBIBS
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			// Jackets
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_JACKET_JACKETS)	// SUB MENU
			// T-shirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_JACKETTOPS)	// SUB MENU
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

// Submenu of Franklin's casual jackets
PROC SETUP_SP_CLOTHES_CASUALJACKETS_JACKETS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_CAS_JACKET_JKTS_W
	// THIS SHOULD ONLY BE USED FOR FRANKLIN'S JACKETS THAT NEED JBIBS
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_JACKET_0, 0)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_0			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_1			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_2			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_3			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_4			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_5			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_6			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_7			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_8			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_9			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_10			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_11			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_12			, COMP_TYPE_TORSO)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_JACKET_13			, COMP_TYPE_TORSO)	// 
			ENDIF
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

// Submenu of Franklin's T-shirts worn under casual jackets
PROC SETUP_SP_CLOTHES_CAS_JACKET_TOPS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_CAS_JACKET_TOPS_W
	// THIS SHOULD ONLY BE USED FOR FRANKLIN'S JBIB TOPS THAT GO WITH HIS CASUAL JACKETS
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_JACKET_0, 0)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_0			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_1			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_2			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_3			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_4			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_5			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_6			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_7			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_8			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_9			, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_10		, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_11		, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_12		, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_13		, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_14		, COMP_TYPE_JBIB)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_TSHIRT_15		, COMP_TYPE_JBIB)	// 
			ENDIF
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SUITJACKETS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SUITJACKETS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_1		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_2		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_3		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_4		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_5		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_6		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_7		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_8		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_9		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_10		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_11		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_12		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_13		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_14		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_SUIT_JACKET_15		, COMP_TYPE_TORSO)	// 
			
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_03 		, COMP_TYPE_TORSO)	// Charcoal Gray Suit Jacket, Blue Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_02			, COMP_TYPE_TORSO)	// Dark Gray Suit Jacket, White Shirt
			
			// pick which of Michael's default grey jackets to put in wardrobe
			// based on Exile wrecked suit flowflag
			#if USE_CLF_DLC
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT 		, COMP_TYPE_TORSO)
			#endif
			#if USE_NRM_DLC
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT 		, COMP_TYPE_TORSO)
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HAS_HAGGARD_SUIT]
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_01 	, COMP_TYPE_TORSO)
			ELSE
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT 		, COMP_TYPE_TORSO)
			ENDIF
			#endif
			#endif
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_04 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_05 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_06 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_07 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_08 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_09 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_10 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_11 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_12 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_13 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_14 		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GREY_SUIT_15 		, COMP_TYPE_TORSO)	// 
		BREAK
		CASE CHAR_FRANKLIN
			// Suit Jackets (open)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_1			    , COMP_TYPE_TORSO)	// Ash Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_15			, COMP_TYPE_TORSO)	// Beige
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_2			    , COMP_TYPE_TORSO)	// Charcoal Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_7			    , COMP_TYPE_TORSO)	// Cream
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT			    , COMP_TYPE_TORSO)	// Gray Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_14			, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_12		 	, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_8			    , COMP_TYPE_TORSO)	// Oatmeal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_9			    , COMP_TYPE_TORSO)	// Off-White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_13		    , COMP_TYPE_TORSO)	// Rust
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_5			    , COMP_TYPE_TORSO)	// Silver
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_4			    , COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_6			    , COMP_TYPE_TORSO)	// Stone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_11			, COMP_TYPE_TORSO)	// Subtle Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_3			    , COMP_TYPE_TORSO)	// Tan Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SUIT_10			, COMP_TYPE_TORSO)	// White
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_0	    	, COMP_TYPE_TORSO)	// Beige Oversized Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_1	    	, COMP_TYPE_TORSO)	// Blue Oversized Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_2	    , COMP_TYPE_TORSO)	// Mustard Oversized Jacket
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_3	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_4	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_5	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_6	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_7	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_8	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_CHEAPSUIT_9	    , COMP_TYPE_TORSO)	// 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET_5	    	, COMP_TYPE_TORSO)	// Black Suit, Black Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET_4	 		, COMP_TYPE_TORSO)	// Cream Suit, Striped Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET_2	   		, COMP_TYPE_TORSO)	// Gray Suit, Blue Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET				, COMP_TYPE_TORSO)	// Gray Suit, White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET_1	  		, COMP_TYPE_TORSO)	// Off-White Suit, Gray Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET_3	  		, COMP_TYPE_TORSO)	// Off-White Suit, White Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_JACKET_6	    	, COMP_TYPE_TORSO)	// Pale Gray Suit, White Shirt
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_6		 		, COMP_TYPE_TORSO)	// Beige Suit, Olive T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_7				, COMP_TYPE_TORSO)	// Beige Suit, White T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_1		 		, COMP_TYPE_TORSO)	// Black Suit, Black T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_0		    	, COMP_TYPE_TORSO)	// Black Suit, White T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_3		    	, COMP_TYPE_TORSO)	// Cream Suit, Beige T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_8				, COMP_TYPE_TORSO)	// Gray Suit, Gray T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_5		    	, COMP_TYPE_TORSO)	// Gray Suit, Pink T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_9		    	, COMP_TYPE_TORSO)	// Pale Gray Suit, White T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_4		    	, COMP_TYPE_TORSO)	// White Suit, Gray T
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_TEE_2		    	, COMP_TYPE_TORSO)	// White Suit, White T
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK		    	, COMP_TYPE_TORSO)	// Gray Suit, V Neck Sweater
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_1		    	, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_2		    	, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_3		    	, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_4		    	, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_5		    	, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_6		    	, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STYLESUIT_VNECK_7		    	, COMP_TYPE_TORSO)	//
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TUXEDO		    			, COMP_TYPE_TORSO)	// Tuxedo
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SUITJACKETS_BUTTONED(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SUITJACKETSB_W

	// THIS SHOULD ONLY BE USED FOR FRANKLIN'S CLOSED SUIT JACKETS
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			// Suit Jackets (closed)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_1			, COMP_TYPE_TORSO)	// Ash Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_15			, COMP_TYPE_TORSO)	// Beige
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_2			, COMP_TYPE_TORSO)	// Charcoal Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_7			, COMP_TYPE_TORSO)	// Cream
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT			, COMP_TYPE_TORSO)	// Gray Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_14			, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_12			, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_8			, COMP_TYPE_TORSO)	// Oatmeal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_9			, COMP_TYPE_TORSO)	// Off-White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_13			, COMP_TYPE_TORSO)	// Rust
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_5			, COMP_TYPE_TORSO)	// Silver
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_4			, COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_6			, COMP_TYPE_TORSO)	// Stone
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_11			, COMP_TYPE_TORSO)	// Subtle Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_3			, COMP_TYPE_TORSO)	// Tan Plaid
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_3PCSUIT_10			, COMP_TYPE_TORSO)	// White
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SWEATERS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SWEATERS_W
	// ONLY TO BE USED FOR MICHAEL'S JBIBS
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET, 0)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_0		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_1		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_2		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_3		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_4		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_5		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_6		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_7		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_8		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_3_9		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_0		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_1		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_2		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_3		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_4		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_5		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_6		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_7		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_8		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_4_9		, COMP_TYPE_JBIB)	//
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_TIES(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	
	CPRINTLN(DEBUG_PED_COMP, "Updating tie options")
	
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_TIES_W
	// ONLY TO BE USED FOR MICHAEL'S JBIBS
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_3PCSUIT, 0)
			OR IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_WAISTCOAT, 0)
			OR IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_SUIT, 0)
				// Ties only available once player has a suit jacket /waistcoat
				
				// get current jbib
				PED_COMP_NAME_ENUM eJbib, eNoTieOption, eTorso
				eJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
				eTorso = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_TORSO)

				// pick tie length based on current jbib
				// short used with collar only, and waistcoat jbibs
				IF (eJbib >= JBIB_P1_COLLAR_A AND eJbib <= JBIB_P1_COLLAR_A_3)
				OR (eJbib >= JBIB_P1_COLLAR_B AND eJbib <= JBIB_P1_COLLAR_B_3)
				OR (eJbib >= JBIB_P1_WAISTCOAT_A AND eJbib <= JBIB_P1_WAISTCOAT_A_15)
				OR (eJbib >= JBIB_P1_WAISTCOAT_B AND eJbib <= JBIB_P1_WAISTCOAT_B_15)
				
					// no tie option:
					eNoTieOption = GET_COLLAR_JBIB(PLAYER_ONE, eTorso, eJbib, TRUE)

					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, eNoTieOption		, COMP_TYPE_JBIB, FALSE, CLO_LBL_NO_TIE)	//
					
					// short ties:
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT		, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_1	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_2	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_3	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_4	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_5	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_6	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_7	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_8	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_9	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_10	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_11	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_12	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_13	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_14	, COMP_TYPE_SPECIAL)	//
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_SHORT_15	, COMP_TYPE_SPECIAL)	//
				ELSE
				
					// medium + long used with full shirt jbibs
					IF (eJbib = JBIB_P1_SHIRT_A)
					OR (eJbib >= JBIB_P1_SHIRT_B AND eJbib <= JBIB_P1_SHIRT_B_1)
					
						// no tie option:
						eNoTieOption = JBIB_P1_SHIRT_A
						
						CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, eNoTieOption		, COMP_TYPE_JBIB, FALSE, CLO_LBL_NO_TIE)	//
								
						// medium used with closed jackets
						IF (eTorso >= TORSO_P1_3PCSUIT AND eTorso <= TORSO_P1_3PCSUIT_15)

							// Medium Ties:
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_1		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_2		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_3		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_4		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_5		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_6		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_7		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_8		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_9		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_10		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_11		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_12		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_13		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_14		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_MEDIUM_15		, COMP_TYPE_SPECIAL)	//

						// long used with open jackets
						ELIF (eTorso >= TORSO_P1_SUIT AND eTorso <= TORSO_P1_SUIT_15)

							// long ties:
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG		, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_1	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_2	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_3	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_4	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_5	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_6	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_7	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_8	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_9	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_10	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_11	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_12	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_13	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_14	, COMP_TYPE_SPECIAL)	//
							CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, SPECIAL_P1_TIE_LONG_15	, COMP_TYPE_SPECIAL)	//
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SUITVESTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SUITVESTS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET, 0)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_0		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_1		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_2		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_3		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_4		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_5		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_6		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_7		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_8		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_9		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_10		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_11		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_12		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_13		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_14		, COMP_TYPE_JBIB)	//
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_5_15		, COMP_TYPE_JBIB)	//
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
		
			// only add suit vest options once player has something to wear them with (suit jacket)
			IF IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_3PCSUIT, 0)
			OR IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P1_SUIT, 0)
				// update options based on whether player is wearing a tie

				// get current special item
				PED_COMP_NAME_ENUM eSpecial
				eSpecial = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
				
				IF (eSpecial >= SPECIAL_P1_TIE_SHORT AND eSpecial <= SPECIAL_P1_TIE_SHORT_15)
				OR (eSpecial >= SPECIAL_P1_TIE_MEDIUM AND eSpecial <= SPECIAL_P1_TIE_MEDIUM_15)
				OR (eSpecial >= SPECIAL_P1_TIE_LONG AND eSpecial <= SPECIAL_P1_TIE_LONG_15)
					// wearing tie, show closed collar versions
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_SHIRT_B				, COMP_TYPE_JBIB, FALSE, CLO_LBL_NO_VEST)	// No Vest 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B			, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_1		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_2		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_3		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_4		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_5		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_6		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_7		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_8		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_9		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_10		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_11		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_12		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_13		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_14		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_B_15		, COMP_TYPE_JBIB)	// 
				ELSE
					// no tie, show open collar versions
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_SHIRT_A				, COMP_TYPE_JBIB, FALSE, CLO_LBL_NO_VEST)	// No Vest 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A			, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_1		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_2		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_3		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_4		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_5		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_6		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_7		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_8		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_9		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_10		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_11		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_12		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_13		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_14		, COMP_TYPE_JBIB)	// 
					CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, JBIB_P1_WAISTCOAT_A_15		, COMP_TYPE_JBIB)	// 
				ENDIF	
			ENDIF
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_VESTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_VESTS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_4		, COMP_TYPE_TORSO)	// Brown Plaid Vest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT		, COMP_TYPE_TORSO)	// Dark Blue Plaid Vest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_1		, COMP_TYPE_TORSO)	// Gray Plaid Vest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_3		, COMP_TYPE_TORSO)	// Gray Vest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_5		, COMP_TYPE_TORSO)	// Pale Gray Vest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_2		, COMP_TYPE_TORSO)	// Pale Gray Plaid Vest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_6		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_7		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_8		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_9		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_10	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_11	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_12	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_13	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_14	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WAISTCOAT_15	, COMP_TYPE_TORSO)	// 
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC


PROC SETUP_SP_CLOTHES_HOODIES(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_HOODIES_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_5			, COMP_TYPE_TORSO)	// Ammunation A
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_1			, COMP_TYPE_TORSO)	// Ammunation Field Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_12			, COMP_TYPE_TORSO)	// Ash
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_6			, COMP_TYPE_TORSO)	// Blauser
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_9			, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_14			, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_8			, COMP_TYPE_TORSO)	// Flourescent
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_0			, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_10			, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_4			, COMP_TYPE_TORSO)	// LSGC Forest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_2			, COMP_TYPE_TORSO)	// LSGC Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_3			, COMP_TYPE_TORSO)	// LSGC Urban
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_11			, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_13			, COMP_TYPE_TORSO)	// Orange
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_15			, COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_HOODIE_7			, COMP_TYPE_TORSO)	// Superstroika
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_12			, COMP_TYPE_TORSO)	// Ash
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_5			, COMP_TYPE_TORSO)	// Crevis
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_6			, COMP_TYPE_TORSO)	// Dust Devils
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_8			, COMP_TYPE_TORSO)	// Eris
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_15			, COMP_TYPE_TORSO)	// Feud Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_10			, COMP_TYPE_TORSO)	// Feud Mint
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_7			, COMP_TYPE_TORSO)	// Feud Olive
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE				, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_9			, COMP_TYPE_TORSO)	// Hinterland
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_1			, COMP_TYPE_TORSO)	// King of Los Santos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_2			, COMP_TYPE_TORSO)	// LS Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_4			, COMP_TYPE_TORSO)	// LS Mustard
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_3			, COMP_TYPE_TORSO)	// LS Yellow
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_11			, COMP_TYPE_TORSO)	// Penetrators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_13			, COMP_TYPE_TORSO)	// Rearwall
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_HOODIE_14			, COMP_TYPE_TORSO)	// Trey Baker
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_5		, COMP_TYPE_TORSO)	// Ammunation A
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_1		, COMP_TYPE_TORSO)	// Ammunation Field Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_12	, COMP_TYPE_TORSO)	// Ash
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_6		, COMP_TYPE_TORSO)	// Blauser
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_9		, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_14	, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_8		, COMP_TYPE_TORSO)	// Flourescent
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_10	, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_4		, COMP_TYPE_TORSO)	// LSGC Forest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_2		, COMP_TYPE_TORSO)	// LSGC Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_3		, COMP_TYPE_TORSO)	// LSGC Urban
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_11	, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_13	, COMP_TYPE_TORSO)	// Orange
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GRAY_HOODIE		, COMP_TYPE_TORSO)	// Silver
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_15	, COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_AMMUN_HOODIE_7		, COMP_TYPE_TORSO)	// Superstroika
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_5			, COMP_TYPE_TORSO)	// Ammunation A
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_2			, COMP_TYPE_TORSO)	// Ammunation Field Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_13			, COMP_TYPE_TORSO)	// Ash
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_STEALTH			, COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_6			, COMP_TYPE_TORSO)	// Blauser
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_10			, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_15			, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_9			, COMP_TYPE_TORSO)	// Fluorescent
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_11			, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_7			, COMP_TYPE_TORSO)	// LSGC Forest
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_4			, COMP_TYPE_TORSO)	// LSGC Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_3			, COMP_TYPE_TORSO)	// LSGC Urban
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_12			, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_14			, COMP_TYPE_TORSO)	// Orange
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_1			, COMP_TYPE_TORSO)	// Silver			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_HOODIE_8			, COMP_TYPE_TORSO)	// Superstroika
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SHIRTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SHIRTS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_0			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_1			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_2			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_3			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_4			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_5			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_6			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_7			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_8			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_9			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_10			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_11			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_12			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_13			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_14			, COMP_TYPE_TORSO)	// Denim Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DENIM_SHIRT_15			, COMP_TYPE_TORSO)	// Denim Shirt

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT				, COMP_TYPE_TORSO)	// Pale Blue Long Sleeved Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_1			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_2			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_3			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_4			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_5			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_6			, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_DRESS_SHIRT_7			, COMP_TYPE_TORSO)	// 

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_CHECK_SHIRT_1			, COMP_TYPE_TORSO)	// Blue Check Winter Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_CHECK_SHIRT_3			, COMP_TYPE_TORSO)	// Brown Winter Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_CHECK_SHIRT_2			, COMP_TYPE_TORSO)	// Charcoal Winter Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_CHECK_SHIRT_0			, COMP_TYPE_TORSO)	// Red Check Winter Shirt
		BREAK
		CASE CHAR_FRANKLIN
			// Short-Sleeve Shirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLUEGRN_CHECK_SHIRT    , COMP_TYPE_TORSO)	// Aqua Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_STEALTH				, COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLUE_CHECK_SHIRT	    , COMP_TYPE_TORSO)	// Blue Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLUE_SHIRT			 	, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CHECK_SHIRT		    , COMP_TYPE_TORSO)	// Brown Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLACK_SHIRT		    , COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WHITE_SHIRT		    , COMP_TYPE_TORSO)	// Cream
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GRAY_CHECK_SHIRT	    , COMP_TYPE_TORSO)	// Gray Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_DARK_GRAY_SHIRT	    , COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GREEN_CHECK_SHIRT		, COMP_TYPE_TORSO)	// Green Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BROWN_CHECK_SHIRT		, COMP_TYPE_TORSO)	// Mint Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_OFF_WHITE_SHIRT	    , COMP_TYPE_TORSO)	// Off-White Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GREEN_SHIRT		    , COMP_TYPE_TORSO)	// Olive Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ORANGE_SHIRT		    , COMP_TYPE_TORSO)	// Orange Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_PURPLE_CHECK_SHIRT		, COMP_TYPE_TORSO)	// Purple Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SALMON_CHECK_SHIRT		, COMP_TYPE_TORSO)	// Red Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_YELLOW_SHIRT			, COMP_TYPE_TORSO)	// Yellow
			// Casual Shirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_0				, COMP_TYPE_TORSO)	// Ash
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_9				, COMP_TYPE_TORSO)	// Beige Gingham
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_2				, COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_12				, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_7				, COMP_TYPE_TORSO)	// Red
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_1				, COMP_TYPE_TORSO)	// Charcoal
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_15				, COMP_TYPE_TORSO)	// Fruity Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_11				, COMP_TYPE_TORSO)	// Gray Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_10				, COMP_TYPE_TORSO)	// Gray Gingham
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_6				, COMP_TYPE_TORSO)	// Jade
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_13				, COMP_TYPE_TORSO)	// Off-White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_5				, COMP_TYPE_TORSO)	// Olive
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_14				, COMP_TYPE_TORSO)	// Pastel Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_3				, COMP_TYPE_TORSO)	// Silver
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_8				, COMP_TYPE_TORSO)	// Tan Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_4				, COMP_TYPE_TORSO)	// White
			// Dress Shirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_5		, COMP_TYPE_TORSO)	// Ash Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_12	, COMP_TYPE_TORSO)	// Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_DRESS_SHIRT		    , COMP_TYPE_TORSO)	// Blue Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_1		, COMP_TYPE_TORSO)	// Beige
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_11	, COMP_TYPE_TORSO)	// Chestnut Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_2		, COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_4		, COMP_TYPE_TORSO)	// Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_13	, COMP_TYPE_TORSO)	// Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_10	, COMP_TYPE_TORSO)	// Off-White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_9		, COMP_TYPE_TORSO)	// Orange
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_7		, COMP_TYPE_TORSO)	// Pale Blue Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_8		, COMP_TYPE_TORSO)	// Pink Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_14	, COMP_TYPE_TORSO)	// Red Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_3		, COMP_TYPE_TORSO)	// Slate
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_15	, COMP_TYPE_TORSO)	// Sand
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHIRT_UP_SLEEVES_6		, COMP_TYPE_TORSO)	// White
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_2				    , COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_13				    , COMP_TYPE_TORSO)	// Blue and Gray Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_6					, COMP_TYPE_TORSO)	// Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_DENIM_SHIRT				, COMP_TYPE_TORSO)	// Denim
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_1				    , COMP_TYPE_TORSO)	// Faded Denim
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_12				    , COMP_TYPE_TORSO)	// Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_4				    , COMP_TYPE_TORSO)	// Gray and Brown Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_14				    , COMP_TYPE_TORSO)	// Gray and Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_3					, COMP_TYPE_TORSO)	// Gray Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_10					, COMP_TYPE_TORSO)	// Gray Large Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_7				    , COMP_TYPE_TORSO)	// Khaki
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_9				    , COMP_TYPE_TORSO)	// Pale Brown Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_11				    , COMP_TYPE_TORSO)	// Maroon Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_15				    , COMP_TYPE_TORSO)	// Navy and Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_5				    , COMP_TYPE_TORSO)	// Red and Black Check
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_SHIRT_8				    , COMP_TYPE_TORSO)	// Red and Gray Check
			// Bowling Shirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_10		    , COMP_TYPE_TORSO)	// Beige and Taupe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_4		    , COMP_TYPE_TORSO)	// Black with Flames
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_15		    , COMP_TYPE_TORSO)	// Black and Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_7		    , COMP_TYPE_TORSO)	// Blue with Scarabs
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_9		    , COMP_TYPE_TORSO)	// Brown and Tan
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT			, COMP_TYPE_TORSO)	// Burgundy and Yellow
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_8		    , COMP_TYPE_TORSO)	// Cream and Orange
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_2		    , COMP_TYPE_TORSO)	// Crimson and Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_5		    , COMP_TYPE_TORSO)	// Mustard with Skulls
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_6		    , COMP_TYPE_TORSO)	// Pale Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_13		    , COMP_TYPE_TORSO)	// Pink and Maroon
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_1		    , COMP_TYPE_TORSO)	// Purple and White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_11		    , COMP_TYPE_TORSO)	// Royal and Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_3		    , COMP_TYPE_TORSO)	// Tan and Cream
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_12		    , COMP_TYPE_TORSO)	// Yellow and Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_BOWLING_SHIRT_14		    , COMP_TYPE_TORSO)	// White and Red
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_6			, COMP_TYPE_TORSO)	// Beige Pattern
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_8			, COMP_TYPE_TORSO)	// Black Silk
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE			    , COMP_TYPE_TORSO)	// Blue and Gray
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_4			, COMP_TYPE_TORSO)	// Blue Chequered
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_10		    , COMP_TYPE_TORSO)	// Burgundy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_11		    , COMP_TYPE_TORSO)	// Camel
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_2			, COMP_TYPE_TORSO)	// Gray and Navy
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_7			, COMP_TYPE_TORSO)	// Lavender Pattern
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_1			, COMP_TYPE_TORSO)	// Maroon
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_9			, COMP_TYPE_TORSO)	// Monogrammed
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_3			, COMP_TYPE_TORSO)	// Mustard and Brown
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_LONG_SLEEVE_5			, COMP_TYPE_TORSO)	// Pale Green Pattern
		BREAK
	ENDSWITCH
ENDPROC

// This contains the submenus for the open shirts and the T-Shirts that can be worn under them
PROC SETUP_SP_CLOTHES_OPENSHIRTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_OPENSHIRTS_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// Open shirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_OPENSHIRTS)	// SUB MENU
			// Undershirts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_UNDERSHIRTS)	// SUB MENU
		BREAK
		CASE CHAR_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

// This is a submenu of open shirts
PROC SETUP_SP_CLOTHES_OPENSHIRTS_M(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_OPENSHIRTSB_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT				, COMP_TYPE_TORSO)	// Open Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_1				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_2				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_3				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_4				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_5				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_6				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_7				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_8				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_9				, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_10			, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_11			, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_12			, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_13			, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_14			, COMP_TYPE_TORSO)	
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_OPEN_SHIRT_15			, COMP_TYPE_TORSO)	
		BREAK
		CASE CHAR_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

// This is a submenu of T-Shirts to wear under open shirts
PROC SETUP_SP_CLOTHES_UNDERSHIRTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_UNDERSHIRTS_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_BARE_CHEST		, COMP_TYPE_JBIB)	// Open Shirt + bare chest
			ENDIF
			#endif
			#endif
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_VEST				, COMP_TYPE_JBIB)	// Open Shirt + tshirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_VEST_1			, COMP_TYPE_JBIB)	// Open Shirt + tshirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_VEST_2			, COMP_TYPE_JBIB)	// Open Shirt + tshirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_VEST_3			, COMP_TYPE_JBIB)	// Open Shirt + tshirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_VEST_4			, COMP_TYPE_JBIB)	// Open Shirt + tshirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, JBIB_P0_VEST_5			, COMP_TYPE_JBIB)	// Open Shirt + tshirt
		BREAK
		CASE CHAR_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_TSHIRTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_TSHIRTS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_8			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_9			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_10			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_11			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_12			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_13			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_14			    	, COMP_TYPE_DECL)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, DECL_P0_TSHIRT_15			    	, COMP_TYPE_DECL)	//
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_4			    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_5			    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_6			    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_7			    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_8			    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_9			    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_10		    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_11		    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_12		    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_13		    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_14		    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_V_NECK_15		    	, COMP_TYPE_TORSO)	// 
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_0	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_1	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_2	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_3	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_4	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_5	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_6	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_7	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_8	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_9	    		, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_10	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_11	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_12	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_13	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_14	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BAGGY_TEE_15	    	, COMP_TYPE_TORSO)	// 
		
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE	    	, COMP_TYPE_TORSO)	// White T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_1	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_2	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_3	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_100_PERCENT_TSHIRT	    , COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_5	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_6	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_7	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_8	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_9	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_10	    	, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_SHORT_SLEEVE_11	    	, COMP_TYPE_TORSO)	// 
			
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_2		   		, COMP_TYPE_TORSO)	// Black V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_5		   		, COMP_TYPE_TORSO)	// Corkers Green V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_4		   		, COMP_TYPE_TORSO)	// Deep Forest V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_6		   		, COMP_TYPE_TORSO)	// Feud Grey V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_12		   		, COMP_TYPE_TORSO)	// Fellowship T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_11		   		, COMP_TYPE_TORSO)	// Fuque V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_9		   		, COMP_TYPE_TORSO)	// Harsh Souls V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_1		   		, COMP_TYPE_TORSO)	// Light Grey V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_7		   		, COMP_TYPE_TORSO)	// Magnetics Black V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_8		   		, COMP_TYPE_TORSO)	// Mint Green V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_3		   		, COMP_TYPE_TORSO)	// Smoky Brown V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_10		   		, COMP_TYPE_TORSO)	// Trey Baker V Neck T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_T_SHIRT_0		   		, COMP_TYPE_TORSO)	// White V Necked T-Shirt
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_4		   		, COMP_TYPE_TORSO)	// Cerveza Barracho T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_2		  		, COMP_TYPE_TORSO)	// Dusche Gold T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_WHITE_TSHIRT				, COMP_TYPE_TORSO)	// White T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_1				, COMP_TYPE_TORSO)	// Yellow T-Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_3				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_5				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_6				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_7				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_8				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_9				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_10				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_11				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_12				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_13				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_14				, COMP_TYPE_TORSO)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_VNECK_15				, COMP_TYPE_TORSO)	// 
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_POLOSHIRTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_POLOSHIRT_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_4				, COMP_TYPE_TORSO)	// Black EE Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_6				, COMP_TYPE_TORSO)	// Brown Rearwall Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_5				, COMP_TYPE_TORSO)	// Light Gray Pro Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_2				, COMP_TYPE_TORSO)	// Maroon Hawaiian Snow Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_1				, COMP_TYPE_TORSO)	// Orange OG Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_GOLF						, COMP_TYPE_TORSO)	// Purple ProLaps Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_7				, COMP_TYPE_TORSO)	// Red LS Golf Club Golf Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_POLO_SHIRT_3				, COMP_TYPE_TORSO)	// Yellow Fruntalot Golf Shirt
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_YOGA_3					, COMP_TYPE_TORSO)	// Black Polo Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_YOGA_5					, COMP_TYPE_TORSO)	// Blue Polo Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_YOGA_0					, COMP_TYPE_TORSO)	// Blue-Gray Polo Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_YOGA_1					, COMP_TYPE_TORSO)	// Green Polo Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_YOGA_2					, COMP_TYPE_TORSO)	// Red Polo Shirt
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_YOGA_4					, COMP_TYPE_TORSO)	// White Polo Shirt
		BREAK
		CASE CHAR_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_8				   	, COMP_TYPE_TORSO)	// Blue and Pink Striped Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_14				    , COMP_TYPE_TORSO)	// Blue and White Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_6					, COMP_TYPE_TORSO)	// Blue Striped Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_11				 	, COMP_TYPE_TORSO)	// Brown Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_15				    , COMP_TYPE_TORSO)	// Brown Striped Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_13					, COMP_TYPE_TORSO)	// Green Checked Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_1					, COMP_TYPE_TORSO)	// Lavender and Cream Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_7				   	, COMP_TYPE_TORSO)	// Mint Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_4				   	, COMP_TYPE_TORSO)	// Mustard Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_5				   	, COMP_TYPE_TORSO)	// Orange Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_12					, COMP_TYPE_TORSO)	// Pale Pink Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_9				   	, COMP_TYPE_TORSO)	// Pink Checked Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_3				   	, COMP_TYPE_TORSO)	// Pink Striped Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_GOLF						, COMP_TYPE_TORSO)	// Red and Orange Patterned Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_10				    , COMP_TYPE_TORSO)	// Turquoise and Purple Polo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TSHIRT_2				   	, COMP_TYPE_TORSO)	// Turquoise Striped Polo
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_TANKTOPS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_TANKTOPS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VEST_4					, COMP_TYPE_TORSO)	// Black Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VEST_3					, COMP_TYPE_TORSO)	// Dark Gray Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VEST_2					, COMP_TYPE_TORSO)	// Light Gray Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_BED						, COMP_TYPE_TORSO)	// Off-White Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, TORSO_P0_VEST_1					, COMP_TYPE_TORSO)	// White Tank Top
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_BLACK_VEST				, COMP_TYPE_TORSO)	// Black
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_YELLOW_VEST	 		    , COMP_TYPE_TORSO)	// Broker
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GRAYSTRIPE_VEST	    	, COMP_TYPE_TORSO)	// Charcoal Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_FEUD3_WHITE_VEST	    , COMP_TYPE_TORSO)	// Feud 3
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_FEUD_GREEN_VEST	   		, COMP_TYPE_TORSO)	// Feud Green
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_FEUD_WHITE_VEST	 		, COMP_TYPE_TORSO)	// Feud White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_FRUNTALOT_BLUE_VEST  	, COMP_TYPE_TORSO)	// Fruntalot Blue
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_YELLOWBROWN_VEST	 	, COMP_TYPE_TORSO)	// Fruntalot Mustard
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GRAY_VEST				, COMP_TYPE_TORSO)	// Gray Tank
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_PURPLE_VEST			    , COMP_TYPE_TORSO)	// Harsh Souls
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_GREEN_WHITE_VEST	    , COMP_TYPE_TORSO)	// Kingz of Los Santos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_ORANGESTRIPE_VEST		, COMP_TYPE_TORSO)	// Orange Stripe
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_CAMO_VEST				, COMP_TYPE_TORSO)	// Rearwall Camo
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_RED_VEST			 	, COMP_TYPE_TORSO)	// Sweatbox
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WHITE_VEST				, COMP_TYPE_TORSO)	// White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, TORSO_P1_WHITESTRIPE_VEST	    , COMP_TYPE_TORSO)	// White Stripe
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_1			, COMP_TYPE_TORSO)	// Del Perro Pier Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_0			, COMP_TYPE_TORSO)	// Los Santos Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_3			, COMP_TYPE_TORSO)	// Love Fist Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_2			, COMP_TYPE_TORSO)	// San Andreas Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_4			, COMP_TYPE_TORSO)	// San Andreas Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_5			, COMP_TYPE_TORSO)	// San Andreas Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_6			, COMP_TYPE_TORSO)	// San Andreas Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_7			, COMP_TYPE_TORSO)	// San Andreas Tank Top
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_TANK_TOP_8			, COMP_TYPE_TORSO)	// San Andreas Tank Top
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST			    , COMP_TYPE_TORSO)	// Yellow and Red Tank
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_1			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_2			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_3			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_4			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_5			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_6			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_7			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_8			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_9			    , COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_10			, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_11			, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_12			, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_13			, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_14			, COMP_TYPE_TORSO)	//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, TORSO_P2_YELLOW_VEST_15			, COMP_TYPE_TORSO)	//
			
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_LEGS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_REGULAR_PANTS	, COMP_TYPE_LEGS) // Regular Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_REGULAR_PANTS_1	, COMP_TYPE_LEGS) //
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_REGULAR_PANTS_2	, COMP_TYPE_LEGS) //
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_REGULAR_PANTS_3	, COMP_TYPE_LEGS) //
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CASUAL_JEANS_1		, COMP_TYPE_LEGS) // Black Casual Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CASUAL_JEANS		, COMP_TYPE_LEGS) // Blue Casual Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CASUAL_JEANS_2		, COMP_TYPE_LEGS) // Faded Casual Jeans
		
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_SMART_JEANS		, COMP_TYPE_LEGS) // Smart Jeans
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF				, COMP_TYPE_LEGS) // Gray Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_1			, COMP_TYPE_LEGS) // Brown Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_2			, COMP_TYPE_LEGS) // Cream Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_3			, COMP_TYPE_LEGS) // Tartan Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_4			, COMP_TYPE_LEGS) // Pastel Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_5			, COMP_TYPE_LEGS) // Beige Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_6			, COMP_TYPE_LEGS) // Brown Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_GOLF_7			, COMP_TYPE_LEGS) // Slate Golf Pants
			ENDIF
			#endif
			#endif
			// Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_0			, COMP_TYPE_LEGS) // Off-White Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_1			, COMP_TYPE_LEGS) // Camel Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_2			, COMP_TYPE_LEGS) // Ash Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_3			, COMP_TYPE_LEGS) // Navy Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_4			, COMP_TYPE_LEGS) // Brown Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_5			, COMP_TYPE_LEGS) // Black Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_6			, COMP_TYPE_LEGS) // Charcoal Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P0_CHINOS_7			, COMP_TYPE_LEGS) // Gray Chinos
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLACK_JEANS		, COMP_TYPE_LEGS) // Black Baggy Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLUE_JEANS		, COMP_TYPE_LEGS) // Blue Baggy Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLUE_JEANS_5		, COMP_TYPE_LEGS) // Blue Green Baggy Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLUE_JEANS_6		, COMP_TYPE_LEGS) // Faded Baggy Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLUE_JEANS_2		, COMP_TYPE_LEGS) // Gray Baggy Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLUE_JEANS_3		, COMP_TYPE_LEGS) // Pale Blue Baggy Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_BLUE_JEANS_4		, COMP_TYPE_LEGS) // Pale Wash Baggy Jeans
			
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_0			, COMP_TYPE_LEGS) // Blue Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_1			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_2			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_3			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_4			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_5			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_6			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_7			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_8			, COMP_TYPE_LEGS) // 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_2		, COMP_TYPE_LEGS) // Aqua Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_1		, COMP_TYPE_LEGS) // Black Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_4		, COMP_TYPE_LEGS) // Blue Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_3		, COMP_TYPE_LEGS) // Brown Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_8		, COMP_TYPE_LEGS) // Coffee Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_0		, COMP_TYPE_LEGS) // Indigo Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_6		, COMP_TYPE_LEGS) // Gray Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_7		, COMP_TYPE_LEGS) // Green Skinny Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_JEANS_B_5		, COMP_TYPE_LEGS) // Red Skinny Jeans

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_CHINOS			, COMP_TYPE_LEGS) // Beige Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_CHINOS_1		, COMP_TYPE_LEGS) // Gray Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_CHINOS_2		, COMP_TYPE_LEGS) // Black Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_CHINOS_3		, COMP_TYPE_LEGS) // Oatmeal Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_CHINOS_4		, COMP_TYPE_LEGS) // Charcoal Chinos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_CHINOS_5		, COMP_TYPE_LEGS) // Navy Chinos
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_0		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_1		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_2		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_3		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_4		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_5		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_6		, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_GOLF_7		, COMP_TYPE_LEGS) //
			ENDIF
			#endif
			#endif
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS		, COMP_TYPE_LEGS) // Gray Sweatpants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_1		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_2		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_3		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_4		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_5		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_6		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_7		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_8		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_9		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_10		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P1_SWEATPANTS_11		, COMP_TYPE_LEGS) // 
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_BLUE_JEANS 		, COMP_TYPE_LEGS) // Blue Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_JEANS_2 			, COMP_TYPE_LEGS) // Dark Blue Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_JEANS_1 			, COMP_TYPE_LEGS) // Dirty Blue Jeans
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_JEANS_3 			, COMP_TYPE_LEGS) // Faded Jeans

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS			, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_1		, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_2		, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_3		, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_4		, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_5		, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_6		, COMP_TYPE_LEGS) // Work Pants, Boots
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_WORKPANTS_7		, COMP_TYPE_LEGS) // Work Pants, Boots
		
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_1		, COMP_TYPE_LEGS) // Cream Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_4		, COMP_TYPE_LEGS) // Dark Gray Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_8		, COMP_TYPE_LEGS) // Desert Camo Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_3		, COMP_TYPE_LEGS) // Gray Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_9		, COMP_TYPE_LEGS) // Green Camo Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_5		, COMP_TYPE_LEGS) // Green Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_2		, COMP_TYPE_LEGS) // Pale Gray Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS		, COMP_TYPE_LEGS) // Tan Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_7		, COMP_TYPE_LEGS) // Urban Camo Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_6		, COMP_TYPE_LEGS) // White Cargos
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_CARGOPANTS_10		, COMP_TYPE_LEGS) // White Cargos
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_11			, COMP_TYPE_LEGS) // Aqua Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_10			, COMP_TYPE_LEGS) // Beige Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF			    , COMP_TYPE_LEGS) // Black Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_8			, COMP_TYPE_LEGS) // Brown Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_9			, COMP_TYPE_LEGS) // Gray Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_4			, COMP_TYPE_LEGS) // Lobster Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_3			, COMP_TYPE_LEGS) // Maroon Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_7			, COMP_TYPE_LEGS) // Mint Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_6			, COMP_TYPE_LEGS) // Pink Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_1			, COMP_TYPE_LEGS) // Russet Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_5			, COMP_TYPE_LEGS) // Stone Plaid Golf Pants
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_GOLF_2			, COMP_TYPE_LEGS) // White Golf Pants
			ENDIF
			#endif
			#endif
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_STEALTH			, COMP_TYPE_LEGS) // Black Cargo Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_LEGS_W, LEGS_P2_SWEAT_PANTS		, COMP_TYPE_LEGS) // Sweat Pants
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SHORTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SHORTS_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_CARGO_SHORTS_0	, COMP_TYPE_LEGS) // Cream Cargo Shorts, White Sneakers
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_CARGO_SHORTS_1	, COMP_TYPE_LEGS) // Khaki Cargo Shorts, Gray Sneakers
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_CARGO_SHORTS_2	, COMP_TYPE_LEGS) // Camo Cargo Shorts, Cream Sneakers
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_CARGO_SHORTS_3	, COMP_TYPE_LEGS) // Gray Cargo Shorts, Dark Gray Sneakers
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_CARGO_SHORTS_4	, COMP_TYPE_LEGS) // 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_BAREFOOT		, COMP_TYPE_LEGS) // Beach Shorts, Bare Feet
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_0			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_1			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 1
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_2			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 2
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_3			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 3
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_4			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 4
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_5			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 5
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_6			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 6
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_7			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 7
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_8			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 8
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_9			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 9
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_10			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 10
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_11			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 11
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_12			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 12
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_13			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 13
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_14			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 14
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_YOGA_15			, COMP_TYPE_LEGS) // Beach Shorts, Flip-Flops 15
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_0		, COMP_TYPE_LEGS) // Long Shorts 0
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_1		, COMP_TYPE_LEGS) // Long Shorts 1
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_2		, COMP_TYPE_LEGS) // Long Shorts 2
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_3		, COMP_TYPE_LEGS) // Long Shorts 3
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_4		, COMP_TYPE_LEGS) // Long Shorts 4
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_5		, COMP_TYPE_LEGS) // Long Shorts 5
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_6		, COMP_TYPE_LEGS) // Long Shorts 6
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_7		, COMP_TYPE_LEGS) // Long Shorts 7
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_8		, COMP_TYPE_LEGS) // Long Shorts 8
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_9		, COMP_TYPE_LEGS) // Long Shorts 9
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_10	, COMP_TYPE_LEGS) // Long Shorts 10
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_11	, COMP_TYPE_LEGS) // Long Shorts 11
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_12	, COMP_TYPE_LEGS) // Long Shorts 12
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_13	, COMP_TYPE_LEGS) // Long Shorts 13
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_14	, COMP_TYPE_LEGS) // Long Shorts 14
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_LONG_SHORTS_15	, COMP_TYPE_LEGS) // Long Shorts 15
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_TENNIS			, COMP_TYPE_LEGS) // Tennis Shorts, White Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_TENNIS_1			, COMP_TYPE_LEGS) //
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_TENNIS_2			, COMP_TYPE_LEGS) // 
			ENDIF
			
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_1			, COMP_TYPE_LEGS) // Blue Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_7			, COMP_TYPE_LEGS) // Blue Heart Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BED				, COMP_TYPE_LEGS) // White Striped Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_3			, COMP_TYPE_LEGS) // Blue Striped Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_5			, COMP_TYPE_LEGS) // Charcoal Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_2			, COMP_TYPE_LEGS) // Ash Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_6			, COMP_TYPE_LEGS) // Red Heart Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_BOXERS_4			, COMP_TYPE_LEGS) // White Boxers
			ENDIF
			#endif
			#endif
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BEIGE_SHORTS 	, COMP_TYPE_LEGS) // Cream Cargo Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_CARGO_SHORTS_2 	, COMP_TYPE_LEGS) // Dark Gray Cargo Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_CARGO_SHORTS_3 	, COMP_TYPE_LEGS) // Gray Cargo Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_CARGO_SHORTS_1 	, COMP_TYPE_LEGS) // Pink Cargo Shorts
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_9		, COMP_TYPE_LEGS) // LOB Blue Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_10		, COMP_TYPE_LEGS) // LOB Green Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_8		, COMP_TYPE_LEGS) // LOB Gray Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_13		, COMP_TYPE_LEGS) // LOB Pale Blue Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_11		, COMP_TYPE_LEGS) // LOB Tan Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_12		, COMP_TYPE_LEGS) // LOB Yellow Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_0		, COMP_TYPE_LEGS) // Panic Purple Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_1		, COMP_TYPE_LEGS) // Panic Yellow Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_3		, COMP_TYPE_LEGS) // Prolaps Blue Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_2		, COMP_TYPE_LEGS) // Prolaps White Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_5		, COMP_TYPE_LEGS) // Salamanders Green Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_4		, COMP_TYPE_LEGS) // Salamanders Red Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_6		, COMP_TYPE_LEGS) // Shrimps Gray Shorts
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BASKETBALL_SHORTS_7		, COMP_TYPE_LEGS) // Shrimps White Shorts

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SHORTS_0			, COMP_TYPE_LEGS) 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SHORTS_1			, COMP_TYPE_LEGS) 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SHORTS_2			, COMP_TYPE_LEGS) 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SHORTS_3			, COMP_TYPE_LEGS) 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SHORTS_4			, COMP_TYPE_LEGS) 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SHORTS_5			, COMP_TYPE_LEGS) 

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_TENNIS			, COMP_TYPE_LEGS) // White Shorts, Sneakers
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BOXERS_2			, COMP_TYPE_LEGS) //Black Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BOXERS_1			, COMP_TYPE_LEGS) //Gray Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BOXERS_4			, COMP_TYPE_LEGS) //Green Feud Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BOXERS_3			, COMP_TYPE_LEGS) //Light Gray Kings Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BOXERS			, COMP_TYPE_LEGS) // White Boxers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_BOXERS_5			, COMP_TYPE_LEGS) //White Feud Boxers
			ENDIF
			#endif
			#endif
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH				, COMP_TYPE_LEGS) // Beach Shorts, Running Shoes
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_1			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_2			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_3			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_4			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_5			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_6			, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_BEACH_7			, COMP_TYPE_LEGS) // 
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_TENNIS			, COMP_TYPE_LEGS) // Tennis Shorts, Sneakers
			ENDIF
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) // blocked until story completed
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_2		, COMP_TYPE_LEGS) // Gray Briefs
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_1		, COMP_TYPE_LEGS) // Green Briefs
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_4		, COMP_TYPE_LEGS) // Impotent Rage Briefs
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_3		, COMP_TYPE_LEGS) // Leopardskin Briefs
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_5		, COMP_TYPE_LEGS) // Pink Leopardskin Briefs
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR			, COMP_TYPE_LEGS) // White Briefs
			ENDIF
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_6		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_7		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_8		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_UNDERWEAR_9		, COMP_TYPE_LEGS) // 
			#endif
			#endif
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SUITPANTS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubmenu = CLO_MENU_SUITPANTS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_3 	, COMP_TYPE_LEGS) // Black Suit Pants
			
			// pick which of Michael's default grey suit pants to put in wardrobe
			// based on Exile wrecked suit flowflag
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HAS_HAGGARD_SUIT]
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_1 	, COMP_TYPE_LEGS) 
			ELSE
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT 	, COMP_TYPE_LEGS) 
			ENDIF
			#endif
			#endif
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_2 	, COMP_TYPE_LEGS) // Charcoal Gray Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_4 	, COMP_TYPE_LEGS) // Dark Gray Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_5 	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_6 	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_7 	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_8 	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_9 	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_10 , COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_11 , COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_12 , COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_13 , COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_14 , COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, LEGS_P0_GREY_SUIT_15 , COMP_TYPE_LEGS) // 
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_6		, COMP_TYPE_LEGS) // Beige Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_7		, COMP_TYPE_LEGS) // Cream Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_4		, COMP_TYPE_LEGS) // Dark Gray Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_2		, COMP_TYPE_LEGS) // Dark Gray Plaid Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT		, COMP_TYPE_LEGS) // Gray Plaid Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_5		, COMP_TYPE_LEGS) // Light Gray Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_8		, COMP_TYPE_LEGS) // Pale Beige Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_1		, COMP_TYPE_LEGS) // Pale Gray Plaid Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_3		, COMP_TYPE_LEGS) // Tan Plaid Suit Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_9		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_10		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_11		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_12		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_13		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_14		, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, LEGS_P1_SUIT_15		, COMP_TYPE_LEGS) // 
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS	, COMP_TYPE_LEGS) // Beige Cheap Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_1	, COMP_TYPE_LEGS) // Blue Cheap Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_2	, COMP_TYPE_LEGS) // Brown Cheap Pants
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_3	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_4	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_5	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_6	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_7	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_8	, COMP_TYPE_LEGS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_SUIT_PANTS_9	, COMP_TYPE_LEGS) // 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_7	, COMP_TYPE_LEGS) // Beige Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_5	, COMP_TYPE_LEGS) // Black Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_8	, COMP_TYPE_LEGS) // Brown Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_3	, COMP_TYPE_LEGS) // Cream Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_2	, COMP_TYPE_LEGS) // Dark Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS		, COMP_TYPE_LEGS) // Gray Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_1	, COMP_TYPE_LEGS) // Khaki Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_6	, COMP_TYPE_LEGS) // White Suit
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_SUIT_PANTS_4	, COMP_TYPE_LEGS) // Yellow Suit
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, LEGS_P2_CHEAP_TUXEDO_PANTS	, COMP_TYPE_LEGS) // Brown Tuxedo Pants
		BREAK
	ENDSWITCH
ENDPROC


PROC SETUP_SP_CLOTHES_FEET(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	
	// get current legs
	PED_COMP_NAME_ENUM eLegs, eReturnItem
	eLegs = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_LEGS)
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// only show shoes options if current legs don't have feet attached
			IF NOT DO_LEGS_CONTAIN_FEET(PLAYER_ZERO, eLegs, eReturnItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_BLACK_SHOES 	, COMP_TYPE_FEET)	// Black Leather Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_BLACK_SHOES_5 	, COMP_TYPE_FEET)	// Blue Suede Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_BLACK_SHOES_1 	, COMP_TYPE_FEET)	// Brown Leather Shoes 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_BLACK_SHOES_2 	, COMP_TYPE_FEET)	// Charcoal Leather Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_BLACK_SHOES_3 	, COMP_TYPE_FEET)	// Gray Leather Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_BLACK_SHOES_4 	, COMP_TYPE_FEET)	// Sand Leather Shoes
		
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_DRESS_LOAFERS 	, COMP_TYPE_FEET)	// Dress Loafers
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES 	, COMP_TYPE_FEET)	// White Skate Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_1 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_2 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_3 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_4 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_5 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_6 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_7 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_8 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SKATE_SHOES_9 	, COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_0 	, COMP_TYPE_FEET)	// Boat Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_1 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_2 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_3 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_4 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_5 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_6 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_COMFY_SHOES_7 	, COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_0 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_1 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_2 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_3 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_4 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_5 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_6 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_7 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_8 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_9 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_10 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_11 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_12 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_13 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_14 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_OXFORDS_15 , COMP_TYPE_FEET)	// 
			
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_0 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_1 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_2 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_3 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_4 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_5 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_6 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_7 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_8 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_9 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_10 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_11 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_12 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_13 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_14 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_SQ_LOAFERS_15 , COMP_TYPE_FEET)	// 
			
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_0 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_1 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_2 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_3 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_4 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_5 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_6 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_7 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_8 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_9 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_10 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_11 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_12 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_13 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_14 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_WINGTIPS_15 , COMP_TYPE_FEET)	// 
			
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_0 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_1 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_2 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_3 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_4 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_5 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_6 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_7 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_8 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_9 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_10 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P0_LOAFERS_11 , COMP_TYPE_FEET)	// 
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
			IF NOT DO_LEGS_CONTAIN_FEET(PLAYER_ONE, eLegs, eReturnItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_NUBUCK_BOOTS_2	, COMP_TYPE_FEET)	// Chocolate Round-Toe Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_NUBUCK_BOOTS_1	, COMP_TYPE_FEET)	// Copper Round-Toe Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_NUBUCK_BOOTS	, COMP_TYPE_FEET)	// Desert Round Toe Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_NUBUCK_BOOTS_3	, COMP_TYPE_FEET)	// Gray Round-Toe Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_NUBUCK_BOOTS_5	, COMP_TYPE_FEET)	// Hawthorn Round-Toe Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_NUBUCK_BOOTS_4	, COMP_TYPE_FEET)	// Slate Round-Toe Boots

				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_BLACK_BOILER	, COMP_TYPE_FEET)	// Work Boots
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS 	, COMP_TYPE_FEET)	// White Athletic Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_1 	, COMP_TYPE_FEET)	// White Athletic Shoes 1
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_2 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_3	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_4 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_5 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_6 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_7 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_8 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_9 	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_10 , COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_TRAINERS_11 , COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_6	, COMP_TYPE_FEET)	// Animal Fashion Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_2	, COMP_TYPE_FEET)	// Black Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_0	, COMP_TYPE_FEET)	// Black and White Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_10	, COMP_TYPE_FEET)	// Blue Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_8	, COMP_TYPE_FEET)	// Cacao Blend Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_3	, COMP_TYPE_FEET)	// Desert Green Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_7	, COMP_TYPE_FEET)	// Feud Classic Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_13	, COMP_TYPE_FEET)	// Forest Camo Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_5	, COMP_TYPE_FEET)	// Freeway Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_15	, COMP_TYPE_FEET)	// Latte Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_9	, COMP_TYPE_FEET)	// Magnetics Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_4	, COMP_TYPE_FEET)	// Mint Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_1	, COMP_TYPE_FEET)	// Orange Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_14	, COMP_TYPE_FEET)	// Soft Seas Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_11	, COMP_TYPE_FEET)	// White Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_A_12	, COMP_TYPE_FEET)	// Winter Camo Sneakers
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_0	, COMP_TYPE_FEET)	// White and Green Skate Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_1	, COMP_TYPE_FEET)	// Green and White Skate Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_2	, COMP_TYPE_FEET)	// White and Gray Skate Sneakers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_3	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_4	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_5	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_6	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_7	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_8	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_9	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_10	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SNEAKERS_B_11	, COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_2	, COMP_TYPE_FEET)	// Chestnut Croc Skate Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_0	, COMP_TYPE_FEET)	// Gray Croc Skate Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_3	, COMP_TYPE_FEET)	// Jade Croc Skate Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_1	, COMP_TYPE_FEET)	// White Skate Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_4	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_5	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_6	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P1_SKATE_SHOES_7	, COMP_TYPE_FEET)	// 
			ENDIF
		BREAK
		CASE CHAR_TREVOR
			IF NOT DO_LEGS_CONTAIN_FEET(PLAYER_TWO, eLegs, eReturnItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_BLACK_BOOTS	 	, COMP_TYPE_FEET)	// Black Chukka Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_2	, COMP_TYPE_FEET)	// Brown Chukka Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_5	, COMP_TYPE_FEET)	// Cowboy Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_4	, COMP_TYPE_FEET)	// Crocodile Skin Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_3	, COMP_TYPE_FEET)	// Gray Chukka Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_7	, COMP_TYPE_FEET)	// Reptile Skin Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_1	, COMP_TYPE_FEET)	// Snakeskin Chukka Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_LEATHER_BOOTS_6	, COMP_TYPE_FEET)	// Yellow Reptile Skin Boots

				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_REDWINGS		, COMP_TYPE_FEET)	// Red/Brown Boots
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_FEET_W, FEET_P2_DOCK_WORKER		, COMP_TYPE_FEET)	// Work Boots
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_SUITSHOES(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_SUITSHOES_W
	
	// get current legs
	PED_COMP_NAME_ENUM eLegs, eReturnItem
	eLegs = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_LEGS)
			
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// not currently needed for Michael as all of his shoes currently work with his suit pants
		BREAK
		CASE CHAR_FRANKLIN
			// only show shoes options if current legs don't have feet attached
			IF NOT DO_LEGS_CONTAIN_FEET(PLAYER_ONE, eLegs, eReturnItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT			, COMP_TYPE_FEET)	// Gray Snake Skin Smart Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_1		, COMP_TYPE_FEET)	// Black Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_2		, COMP_TYPE_FEET)	// White Snake Skin Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_3		, COMP_TYPE_FEET)	// Gray Crocodile Skin Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_4		, COMP_TYPE_FEET)	// Alligator Skin Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_5		, COMP_TYPE_FEET)	// Brown Crocodile Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_6		, COMP_TYPE_FEET)	// Rattlesnake Skin Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_7		, COMP_TYPE_FEET)	// Gray Python Skin Shoes
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SUIT_8		, COMP_TYPE_FEET)	// Two Tone Crocodile Skin Shoes
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_0		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_1		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_2		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_3		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_4		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_5		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_6		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_7		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_8		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_9		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_10		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_LOAFERS_11		, COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_0		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_1		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_2		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_3		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_4		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_5		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_6		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_7		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_8		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_9		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_10		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_11		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_12		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_13		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_14		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_OXFORDS_15		, COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_0		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_1		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_2		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_3		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_4		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_5		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_6		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_7		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_8		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_9		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_10	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_11	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_12	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_13	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_14	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_SQ_LOAFERS_15	, COMP_TYPE_FEET)	// 
				
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_0	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_1	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_2	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_3	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_4	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_5	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_6	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_7	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_8	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_9	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_10	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_11	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_12	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_13	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_14	, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, FEET_P1_WINGTIPS_15	, COMP_TYPE_FEET)	// 
			ENDIF
		BREAK
		CASE CHAR_TREVOR
			// only show shoes options if current legs don't have feet attached
			IF NOT DO_LEGS_CONTAIN_FEET(PLAYER_TWO, eLegs, eReturnItem)
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS			, COMP_TYPE_FEET)	// Black Loafers
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_1			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_2			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_3			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_4			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_5			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_6			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_7			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_8			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_9			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_10			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_LOAFERS_11			, COMP_TYPE_FEET)	// 
			
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_0			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_1			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_2			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_3			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_4			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_5			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_6			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_7			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_8			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_9			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_10			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_11			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_12			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_13			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_14			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_OXFORDS_15			, COMP_TYPE_FEET)	// 
			
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_0			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_1			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_2			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_3			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_4			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_5			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_6			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_7			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_8			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_9			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_10			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_11			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_12			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_13			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_14			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_SQ_LOAFERS_15			, COMP_TYPE_FEET)	// 
			
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_0			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_1			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_2			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_3			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_4			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_5			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_6			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_7			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_8			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_9			, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_10		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_11		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_12		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_13		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_14		, COMP_TYPE_FEET)	// 
				CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, FEET_P2_WINGTIPS_15		, COMP_TYPE_FEET)	// 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_HATS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_HATS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_HAT)	// No hat
		BREAK
		CASE CHAR_FRANKLIN
			// forwards
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_FORWARDSCAPS)
			// backwards
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_BACKWARDSCAPS)
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_HAT)	// No hat
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_0, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_1, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_2, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_3, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_4, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_5, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_6, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_7, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_8, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_9, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_10, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_11, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_12, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_13, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_14, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_CAP_15, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_BEANIE_HAT, COMP_TYPE_PROPS)
		BREAK
	ENDSWITCH
ENDPROC

// Submenu of Franklin's caps worn forwards
PROC SETUP_SP_CLOTHES_HATS_FWD(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_FORWARDSCAPS_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_HAT)	// No hat
			// forwards
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_0, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_1, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_2, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_3, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_4, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_5, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_6, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_7, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_8, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_9, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_10, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_11, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_12, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_13, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_14, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_FRONT_15, COMP_TYPE_PROPS)
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

// Submenu of Franklin's caps worn backwards
PROC SETUP_SP_CLOTHES_HATS_BACK(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_BACKWARDSCAPS_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_HAT)	// No hat
			// backwards
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_GREEN_CAP, COMP_TYPE_PROPS)	// The Feud
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_1, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_2, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_3, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_4, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_5, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_6, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_7, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_8, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_9, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_10, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_11, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_12, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_13, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_14, COMP_TYPE_PROPS)	// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_CAP_BACK_15, COMP_TYPE_PROPS)	// 
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_MASKS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_MASKS_W
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_MASK)	// No Mask

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_HOCKEY_RED, COMP_TYPE_PROPS) 	// Red Hockey Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_HOCKEY_WHITE, COMP_TYPE_PROPS) 	// White Hockey Mask

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_MONSTER_GREEN, COMP_TYPE_PROPS) // Green Monster
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_MONSTER_RED, COMP_TYPE_PROPS) 	// Red Monster 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_PIG_DARK, COMP_TYPE_PROPS) 		// Dark Pig Mask 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_PIG, COMP_TYPE_PROPS) 			// Pig Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_MONKEY, COMP_TYPE_PROPS) 		// Smoking Monkey Mask			

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_APE, COMP_TYPE_PROPS) 			// Ape Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_APE_DARK, COMP_TYPE_PROPS) 		// Dark Ape Mask

			

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_SKULL_YELLOW, COMP_TYPE_PROPS) 	// Bone Skull Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_SKULL_GREY, COMP_TYPE_PROPS) 	// Silver Skull Mask

			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_TRIBAL_1, COMP_TYPE_PROPS) 		// Orange and Green Day of the Dead Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_TRIBAL_2, COMP_TYPE_PROPS) 		// Black and White Day of the Dead Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_MASK_TRIBAL_3, COMP_TYPE_PROPS)		// Teal and Orange Day of the Dead Mask
		
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, SPECIAL2_P0_WRESTLER_MASK_0, COMP_TYPE_SPECIAL2)	// Red Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, SPECIAL2_P0_WRESTLER_MASK_1, COMP_TYPE_SPECIAL2)	// Green Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, SPECIAL2_P0_WRESTLER_MASK_2, COMP_TYPE_SPECIAL2)	// Blue Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, SPECIAL2_P0_WRESTLER_MASK_3, COMP_TYPE_SPECIAL2)	// Black and White Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, SPECIAL2_P0_WRESTLER_MASK_4, COMP_TYPE_SPECIAL2)	// Gray and Black Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, SPECIAL2_P0_WRESTLER_MASK_5, COMP_TYPE_SPECIAL2)	// Red and Green Wrestler Mask
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_MASK)	// No Mask

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL_P1_MASK, COMP_TYPE_SPECIAL) 			// Bandana

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_HOCKEY_RED, COMP_TYPE_PROPS) 	// Red Hockey Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_HOCKEY_WHITE, COMP_TYPE_PROPS) 	// White Hockey Mask

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_MONSTER_GREEN, COMP_TYPE_PROPS) // Green Monster 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_MONSTER_RED, COMP_TYPE_PROPS) 	// Red Monster 
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_PIG_DARK, COMP_TYPE_PROPS) 		// Dark Pig Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_PIG, COMP_TYPE_PROPS) 			// Pig Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_MONKEY, COMP_TYPE_PROPS) 		// Space Monkey
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_APE, COMP_TYPE_PROPS) 			// Ape
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_APE_DARK, COMP_TYPE_PROPS) 		// Dark Ape
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_SKULL_YELLOW, COMP_TYPE_PROPS) 	// Bone Skull
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_SKULL_GREY, COMP_TYPE_PROPS) 	// Silver Skull
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_TRIBAL_1, COMP_TYPE_PROPS) 		// Carnival Orange
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_TRIBAL_2, COMP_TYPE_PROPS) 		// Carnival White
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_MASK_TRIBAL_3, COMP_TYPE_PROPS)		// Carnival Blue
		
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL2_P1_WRESTLER_MASK_0, COMP_TYPE_SPECIAL2)	// Red Wrestler mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL2_P1_WRESTLER_MASK_1, COMP_TYPE_SPECIAL2)	// Green Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL2_P1_WRESTLER_MASK_2, COMP_TYPE_SPECIAL2)	// Blue Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL2_P1_WRESTLER_MASK_3, COMP_TYPE_SPECIAL2)	// Black and White Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL2_P1_WRESTLER_MASK_4, COMP_TYPE_SPECIAL2)	// Gray and Black Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, SPECIAL2_P1_WRESTLER_MASK_5, COMP_TYPE_SPECIAL2)	// Red and Green Wrestler Mask
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_HEAD_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_MASK)	// No Mask 			

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_HOCKEY_RED, COMP_TYPE_PROPS) 		// Red Hockey Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_HOCKEY_WHITE, COMP_TYPE_PROPS) 	// White Hockey Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_MASK_MONSTER_GREEN, COMP_TYPE_SPECIAL2)	// Green Monster
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_MASK_MONSTER_RED, COMP_TYPE_SPECIAL2)	// Red Monster
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_PIG_DARK, COMP_TYPE_PROPS) 		// Dark Pig Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_PIG, COMP_TYPE_PROPS) 			// Pig Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_MONKEY, COMP_TYPE_PROPS) 			// Smoking Monkey Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_APE, COMP_TYPE_PROPS) 			// Ape Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_APE_DARK, COMP_TYPE_PROPS) 		// Dark Ape Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_SKULL_YELLOW, COMP_TYPE_PROPS) 	// Bone Skull Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_SKULL_GREY, COMP_TYPE_PROPS) 		// Silver Skull Mask
			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_TRIBAL_1, COMP_TYPE_PROPS) 		// Orange and Green Day of the Dead Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_TRIBAL_2, COMP_TYPE_PROPS) 		// Black and White Day of the Dead Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_MASK_TRIBAL_3, COMP_TYPE_PROPS)		// Teal and Orange Day of the Dead Mask

			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_WRESTLER_MASK_0, COMP_TYPE_SPECIAL2)	// Red Wrestler mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_WRESTLER_MASK_1, COMP_TYPE_SPECIAL2)	// Green Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_WRESTLER_MASK_2, COMP_TYPE_SPECIAL2)	// Blue Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_WRESTLER_MASK_3, COMP_TYPE_SPECIAL2)	// Black and White Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_WRESTLER_MASK_4, COMP_TYPE_SPECIAL2)	// Gray and Black Wrestler Mask
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, SPECIAL2_P2_WRESTLER_MASK_5, COMP_TYPE_SPECIAL2)	// Red and Green Wrestler Mask
		BREAK
	ENDSWITCH
ENDPROC

// Main glasses menu
PROC SETUP_SP_CLOTHES_GLASSES(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_GLASSES_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)	// No glasses
			// Submenus
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_GLASSES_SUB)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SPORTSHADES_SUB)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_STREETSHADES_SUB)	// SUB MENU
		BREAK
		CASE CHAR_FRANKLIN
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)
			// Submenus
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SPORTSHADES_SUB)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_STREETSHADES_SUB)	// SUB MENU
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES) 	// None
			// Submenus
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_SPORTSHADES_SUB)	// SUB MENU
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubmenu, OUTFIT_DEFAULT, COMP_TYPE_OUTFIT, FALSE, CLO_LBL_SUB_STREETSHADES_SUB)	// SUB MENU		
		BREAK
	ENDSWITCH
ENDPROC

// Submenu
PROC SETUP_SP_CLOTHES_GLASSES_SUB(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_GLASSES_SUB_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)	// No glasses
			//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES, COMP_TYPE_PROPS)				// Black-Rimmed Glasses
			// Designer top-frame glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES, COMP_TYPE_PROPS)		// Enema Brown Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_1, COMP_TYPE_PROPS)	// Enema Gray Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_2, COMP_TYPE_PROPS)	// Enema Black Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_3, COMP_TYPE_PROPS)	// Enema Tortoiseshell Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_4, COMP_TYPE_PROPS)	// Enema Coffee Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_5, COMP_TYPE_PROPS)	// Enema Walnut Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_6, COMP_TYPE_PROPS)	// Enema Silver Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_PROGRAMMER_GLASSES_7, COMP_TYPE_PROPS)	// Enema Smoke Glasses
		BREAK
		CASE CHAR_FRANKLIN
//			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)
		BREAK
		CASE CHAR_TREVOR
//			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES) 	// None
		BREAK
	ENDSWITCH
ENDPROC

// Submenu
PROC SETUP_SP_CLOTHES_SPORTSHADES(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_SPORTSHADES_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)	// No glasses
			//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_REFLECTIVE, COMP_TYPE_PROPS)		// Copper Reflective Sports Shades
			// Coloured plastic frame triathlon style shades			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES, COMP_TYPE_PROPS)			// Tung Charcoal Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_1, COMP_TYPE_PROPS)		// Tung Ash Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_2, COMP_TYPE_PROPS)		// Tung Gray Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_3, COMP_TYPE_PROPS)		// Tung Red Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_4, COMP_TYPE_PROPS)		// Tung Blue Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_5, COMP_TYPE_PROPS)		// Tung Yellow Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_6, COMP_TYPE_PROPS)		// Tung Black Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_7, COMP_TYPE_PROPS)		// Tung Rosy Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_8, COMP_TYPE_PROPS)		// Tung Hornet Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_SHADES_9, COMP_TYPE_PROPS)		// Tung Two-Tone Shades
			// Rectangular thick sided
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_0, COMP_TYPE_PROPS)	// Black Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_1, COMP_TYPE_PROPS)	// Charcoal Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_2, COMP_TYPE_PROPS)	// Ash Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_3, COMP_TYPE_PROPS)	// Gray Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_4, COMP_TYPE_PROPS)	// Tan Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_5, COMP_TYPE_PROPS)	// Chocolate and Walnut Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_6, COMP_TYPE_PROPS)	// Brown and Walnut Rects
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_THICK_RIM_7, COMP_TYPE_PROPS)	// Walnut Rects
		BREAK
		CASE CHAR_FRANKLIN
			// None
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)
			// Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_0, COMP_TYPE_PROPS) // Aviator Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_6, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_7, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_8, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_E_9, COMP_TYPE_PROPS) // 
			//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_0, COMP_TYPE_PROPS) // Contour Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_6, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_7, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_8, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_A_9, COMP_TYPE_PROPS) // 
			//
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_0, COMP_TYPE_PROPS) // Mono Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_6, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_7, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_8, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_H_9, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_B_0, COMP_TYPE_PROPS) // Racer Shades
		BREAK
		CASE CHAR_TREVOR
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES) 	// None
			// Solid-framed sports shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES, COMP_TYPE_PROPS)			// Specs Pest Shades
			// Top-frame triathlon style shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_0, COMP_TYPE_PROPS)			// Tung Charcoal Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_1, COMP_TYPE_PROPS)			// Tung White Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_2, COMP_TYPE_PROPS)			// Tung Ash Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_3, COMP_TYPE_PROPS)			// Tung Red Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_4, COMP_TYPE_PROPS)			// Tung Blue Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_5, COMP_TYPE_PROPS)			// Tung Yellow Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_6, COMP_TYPE_PROPS)			// Tung Black Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_7, COMP_TYPE_PROPS)			// Tung Purple Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_8, COMP_TYPE_PROPS)			// Tung Hornet Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_A_9, COMP_TYPE_PROPS)			// Tung Two-Tone Shades
			// Flat plastic wraparound shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_0, COMP_TYPE_PROPS)			// Vapid Black Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_1, COMP_TYPE_PROPS)			// Vapid Ash Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_2, COMP_TYPE_PROPS)			// Vapid Red Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_3, COMP_TYPE_PROPS)			// Vapid Yellow Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_4, COMP_TYPE_PROPS)			// Vapid Brown Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_5, COMP_TYPE_PROPS)			// Vapid Gray Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_6, COMP_TYPE_PROPS)			// Vapid Blue Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_7, COMP_TYPE_PROPS)			// Vapid Smoke Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_8, COMP_TYPE_PROPS)			// Vapid Orange Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SHADES_B_9, COMP_TYPE_PROPS)			// Vapid Slate Shades
		BREAK
	ENDSWITCH
ENDPROC

// Submenu
PROC SETUP_SP_CLOTHES_STREETSHADES(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_STREETSHADES_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			// None
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)	// No glasses
			// Classic aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK, COMP_TYPE_PROPS)		// Farshtunken Gold Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_1, COMP_TYPE_PROPS)		// Farshtunken Purple Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_2, COMP_TYPE_PROPS)		// Farshtunken Silver Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_3, COMP_TYPE_PROPS)		// Farshtunken Gray Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_4, COMP_TYPE_PROPS)		// Farshtunken Blue Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_5, COMP_TYPE_PROPS)		// Farshtunken Tinted Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_6, COMP_TYPE_PROPS)		// Farshtunken Steel Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_7, COMP_TYPE_PROPS)		// Farshtunken Sepia Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_8, COMP_TYPE_PROPS)		// Farshtunken Black Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_GLASSES_DARK_9, COMP_TYPE_PROPS)		// Farshtunken Smoke Aviators
			// Fat-framed aviators			
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_0, COMP_TYPE_PROPS)			// Silver Mirrored Blue Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_1, COMP_TYPE_PROPS)			// Silver Mirrored Brown Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_2, COMP_TYPE_PROPS)			// Silver Mirrored Sepia Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_3, COMP_TYPE_PROPS)			// Steel Mirrored Blue Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_4, COMP_TYPE_PROPS)			// Steel Mirrored Sepia Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_5, COMP_TYPE_PROPS)			// Steel Mirrored Brown Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_6, COMP_TYPE_PROPS)			// Gunmetal Mirrored Blue Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_7, COMP_TYPE_PROPS)			// Gunmetal Mirrored Brown Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_8, COMP_TYPE_PROPS)			// Gunmetal Mirrored Sepia Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_9, COMP_TYPE_PROPS)			// Gold Mirrored Brown Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_10, COMP_TYPE_PROPS)		// Gold Mirrored Sepia Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, eSubMenu, PROPS_P0_AVIATORS_11, COMP_TYPE_PROPS)		// Gold Mirrored Blue Aviators
		BREAK
		CASE CHAR_FRANKLIN
			// None
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES)
			// Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_0, COMP_TYPE_PROPS) // Aviator Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_6, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_7, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_8, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_D_9, COMP_TYPE_PROPS) // 
			// Stank
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_0, COMP_TYPE_PROPS) // Stank Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_C_6, COMP_TYPE_PROPS) // 
			// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_0, COMP_TYPE_PROPS) // Suburban Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_6, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_7, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_8, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_F_9, COMP_TYPE_PROPS) // 
			// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_0, COMP_TYPE_PROPS) // T-Bone Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_6, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_G_7, COMP_TYPE_PROPS) // 
			// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_0, COMP_TYPE_PROPS) // Triptych Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_1, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_2, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_3, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_4, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_5, COMP_TYPE_PROPS) // 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_SUNGLASSES_I_6, COMP_TYPE_PROPS) // 
		BREAK
		CASE CHAR_TREVOR
			// None
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_EYES_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_GLASSES) 	// None
			// 
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES, COMP_TYPE_PROPS)				// Dix Charcoal Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_1, COMP_TYPE_PROPS)			// Dix Brown Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_2, COMP_TYPE_PROPS)			// Dix Black Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_3, COMP_TYPE_PROPS)			// Dix Checked Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_4, COMP_TYPE_PROPS)			// Dix White Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_5, COMP_TYPE_PROPS)			// Dix Red Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_6, COMP_TYPE_PROPS)			// Dix Maroon Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_7, COMP_TYPE_PROPS)			// Dix Yellow Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_8, COMP_TYPE_PROPS)			// Dix Spring Glasses
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_GLASSES_9, COMP_TYPE_PROPS)			// Dix Fall Glasses
			// Oldschool aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_0, COMP_TYPE_PROPS)		// Steel Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_1, COMP_TYPE_PROPS)		// Slate Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_2, COMP_TYPE_PROPS)		// Gold Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_3, COMP_TYPE_PROPS)		// Silver Two-Tone Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_4, COMP_TYPE_PROPS)		// Aluminium Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_5, COMP_TYPE_PROPS)		// Bronze Two-Tone Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_6, COMP_TYPE_PROPS)		// Brown Two-Tone Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_7, COMP_TYPE_PROPS)		// Black Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_8, COMP_TYPE_PROPS)		// Silver Aviators
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_B_9, COMP_TYPE_PROPS)		// Smoke Aviators
			// Elton style
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_0, COMP_TYPE_PROPS)		// Krepp Gold Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_1, COMP_TYPE_PROPS)		// Krepp Gray Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_2, COMP_TYPE_PROPS)		// Krepp Slate Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_3, COMP_TYPE_PROPS)		// Krepp Black Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_4, COMP_TYPE_PROPS)		// Krepp White Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_5, COMP_TYPE_PROPS)		// Krepp Silver Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_6, COMP_TYPE_PROPS)		// Krepp Ash Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_7, COMP_TYPE_PROPS)		// Krepp Brown Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_8, COMP_TYPE_PROPS)		// Krepp Beige Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SUNGLASSES_C_9, COMP_TYPE_PROPS)		// Krepp Coffee Shades
			// Fat-Framed square shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_0, COMP_TYPE_PROPS)	// Broker Black Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_1, COMP_TYPE_PROPS)	// Broker Purple Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_2, COMP_TYPE_PROPS)	// Broker Brown Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_3, COMP_TYPE_PROPS)	// Broker Orange Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_4, COMP_TYPE_PROPS)	// Broker Gray Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_5, COMP_TYPE_PROPS)	// Broker Striped Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_6, COMP_TYPE_PROPS)	// Broker Beige Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_7, COMP_TYPE_PROPS)	// Broker Ash Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_8, COMP_TYPE_PROPS)	// Broker Charcoal Shades
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, eSubMenu, PROPS_P2_SQUARE_GLASSES_9, COMP_TYPE_PROPS)	// Broker Gradient Shades
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_SP_CLOTHES_EARINGS(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempClothesInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu)
	CLOTHES_MENU_ENUM eSubMenu = CLO_MENU_EARRINGS_W
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			// None
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_EARS_NONE, COMP_TYPE_PROPS, TRUE, CLO_LBL_NO_EARRINGS)
			// Round
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_0, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_1, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_2, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_3, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_4, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_5, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_6, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_ROUND_7, COMP_TYPE_PROPS)
			// Square
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_0, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_1, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_2, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_3, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_4, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_5, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_6, COMP_TYPE_PROPS)
			CALL fpSetupClothingItemForShop(sTempClothesInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, eSubMenu, PROPS_P1_EARRING_SQUARE_7, COMP_TYPE_PROPS)
		BREAK
		CASE CHAR_TREVOR
		BREAK
	ENDSWITCH
ENDPROC
#if USE_CLF_DLC
/// PURPOSE: Fills the specified struct will the wardrobe information and returns TRUE if successful
FUNC BOOL GET_WARDROBE_DATACLF(WARDROBE_DATA_STRUCT &sData, WARDROBE_LAUNCHER_STRUCT &sWardrobeLauncherData)

	// Temp struct so we get clean data
	WARDROBE_INFO_STRUCT sInfo
	
	sInfo.vWardrobeCoords = GET_PLAYER_PED_WARDROBE_COORDS(sWardrobeLauncherData.eWardrobe)
	sInfo.eWardrobe = sWardrobeLauncherData.eWardrobe

	SWITCH sWardrobeLauncherData.eWardrobe
		CASE PW_MICHAEL_MANSION
			sInfo.eCharacter 			= CHAR_MICHAEL
			sInfo.eSavehouse			= SAVEHOUSEclf_MICHAEL_BH
			sInfo.vPlayerCoords 		= <<-811.8961, 175.2218, 76.7453>> 
			sInfo.fPlayerHead 			= 116.4959 
			sInfo.vBuddyCoords 			= <<-811.4418, 179.3454, 75.7407>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-812.456299,176.530106,78.000862>>
			sInfo.vAngledAreaCoords[1] 	= <<-811.343628,173.683456,75.615738>>
			sInfo.fAngledAreaWidth 		= 2.125000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_Michael_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_Michael_ward_face
		BREAK
		CASE PW_MICHAEL_COUNTRYSIDE
			sInfo.eCharacter 			= CHAR_MICHAEL
			sInfo.eSavehouse			= SAVEHOUSEclf_MICHAEL_CS
			sInfo.vPlayerCoords 		= <<1969.1100, 3814.7168, 33.4280>>
			sInfo.fPlayerHead 			= 323.1107
			sInfo.vBuddyCoords 			= <<1974.2910, 3819.0266, 32.4363>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<1969.700806,3814.033447,32.592842>>
			sInfo.vAngledAreaCoords[1] 	= <<1968.748169,3815.676025,34.553722>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevTrail_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevTrail_ward_face
		BREAK
		CASE PW_TREVOR_COUNTRYSIDE
			sInfo.eCharacter 			= CHAR_TREVOR
			sInfo.eSavehouse			= SAVEHOUSEclf_TREVOR_CS
			sInfo.vPlayerCoords 		= <<1969.1100, 3814.7168, 33.4280>>
			sInfo.fPlayerHead 			= 323.1107
			sInfo.vBuddyCoords 			= <<1974.2910, 3819.0266, 32.4363>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<1969.700806,3814.033447,32.592842>>
			sInfo.vAngledAreaCoords[1] 	= <<1968.748169,3815.676025,34.553722>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevTrail_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevTrail_ward_face
		BREAK
		CASE PW_TREVOR_CITY
			sInfo.eCharacter 			= CHAR_TREVOR
			sInfo.eSavehouse			= SAVEHOUSEclf_TREVOR_VB
			sInfo.vPlayerCoords 		= << -1150.4913, -1513.3470, 10.6394 >>
			sInfo.fPlayerHead 			= 245.9826
			sInfo.vBuddyCoords 			= <<-1157.0200, -1518.3557, 9.6327>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-1150.449463,-1514.140015,9.634556>>
			sInfo.vAngledAreaCoords[1] 	= <<-1151.557983,-1512.427368,11.759556>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevApt_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevApt_ward_face
		BREAK
		CASE PW_TREVOR_STRIPCLUB
			sInfo.eCharacter 			= CHAR_TREVOR
			sInfo.eSavehouse			= SAVEHOUSEclf_TREVOR_SC
			sInfo.vPlayerCoords 		= <<105.3011, -1303.3383, 28.7688>>
			sInfo.fPlayerHead 			= 306.5809
			sInfo.vBuddyCoords 			= <<100.6848, -1294.5472, 29.2668>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<105.683533,-1304.552246,27.581297>>
			sInfo.vAngledAreaCoords[1] 	= <<104.314659,-1302.343384,30.143797>> 
			sInfo.fAngledAreaWidth 		= 2.312500
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevStrip_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevStrip_ward_face
		BREAK
		CASE PW_FRANKLIN_AUNTS
			sInfo.eCharacter 			= CHAR_FRANKLIN
			sInfo.eSavehouse			= SAVEHOUSEclf_FRANKLIN_SC
			sInfo.vPlayerCoords 		= << -17.9973, -1438.9110, 31.1018 >>
			sInfo.fPlayerHead 			= 200.2377
			sInfo.vBuddyCoords 			= <<-13.5733, -1445.6842, 29.6473>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-18.429632,-1439.627197,30.101538>>
			sInfo.vAngledAreaCoords[1] 	= <<-18.462055,-1438.029175,32.345192>> 
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_FrankAunt_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_FrankAunt_ward_face
		BREAK
		CASE PW_FRANKLIN_HILLS
			sInfo.eCharacter 			= CHAR_FRANKLIN
			sInfo.eSavehouse			= SAVEHOUSEclf_FRANKLIN_VH
			sInfo.vPlayerCoords 		= << 9.0157, 528.7267, 170.6347 >>
			sInfo.fPlayerHead 			= 120.8883
			sInfo.vBuddyCoords 			= <<6.5471, 536.3779, 169.6173>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<8.094527,530.681396,169.617157>>
			sInfo.vAngledAreaCoords[1] 	= <<9.471528,527.573486,171.617157>> 
			sInfo.fAngledAreaWidth 		= 2.812500
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_FrankHill_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_FrankHill_ward_face
		BREAK
		
		// Freemode wardrobe uses property info passed into the wardrobe script.
		CASE PW_FREEMODE
			sInfo.eCharacter 			= NO_CHARACTER
			sInfo.eSavehouse			= NUMBER_OF_CLF_SAVEHOUSE
			sInfo.vPlayerCoords 		= sWardrobeLauncherData.vCoords
			sInfo.fPlayerHead 			= sWardrobeLauncherData.fHeading
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.bDataSet 				= TRUE
			IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				sInfo.eWardrobeLight 	= SM_PROP_SMUG_HANGAR_WARDROBE_LRIG
			ELSE	
				sInfo.eWardrobeLight	= V_LIRG_MPHIGH_WARD_MAIN
			ENDIF	
			sInfo.eWardrobeHeadLight	= V_LIRG_MPHIGH_WARD_FACE
		BREAK
	ENDSWITCH
	
	// Update our ref copy
	sData.sWardrobeInfo = sInfo
	
	RETURN TRUE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL GET_WARDROBE_DATANRM(WARDROBE_DATA_STRUCT &sData, WARDROBE_LAUNCHER_STRUCT &sWardrobeLauncherData)

	// Temp struct so we get clean data
	WARDROBE_INFO_STRUCT sInfo
	
	sInfo.vWardrobeCoords = GET_PLAYER_PED_WARDROBE_COORDS(sWardrobeLauncherData.eWardrobe)
	sInfo.eWardrobe = sWardrobeLauncherData.eWardrobe

	SWITCH sWardrobeLauncherData.eWardrobe
		CASE PW_MICHAEL_MANSION
			sInfo.eCharacter 			= CHAR_MICHAEL
			sInfo.eSavehouse			= SAVEHOUSENRM_BH
			sInfo.vPlayerCoords 		= <<-811.8961, 175.2218, 76.7453>> 
			sInfo.fPlayerHead 			= 116.4959 
			sInfo.vBuddyCoords 			= <<-811.4418, 179.3454, 75.7407>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-812.456299,176.530106,78.000862>>
			sInfo.vAngledAreaCoords[1] 	= <<-811.343628,173.683456,75.615738>>
			sInfo.fAngledAreaWidth 		= 2.125000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_Michael_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_Michael_ward_face
		BREAK
		CASE PW_MICHAEL_COUNTRYSIDE
			sInfo.eCharacter 			= CHAR_MICHAEL
			sInfo.eSavehouse			= SAVEHOUSENRM_CHATEAU
			sInfo.vPlayerCoords 		= <<1969.1100, 3814.7168, 33.4280>>
			sInfo.fPlayerHead 			= 323.1107
			sInfo.vBuddyCoords 			= <<1974.2910, 3819.0266, 32.4363>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<1969.700806,3814.033447,32.592842>>
			sInfo.vAngledAreaCoords[1] 	= <<1968.748169,3815.676025,34.553722>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevTrail_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevTrail_ward_face
		BREAK
		
		// Freemode wardrobe uses property info passed into the wardrobe script.
		CASE PW_FREEMODE
			sInfo.eCharacter 			= NO_CHARACTER
			sInfo.eSavehouse			= NUMBER_OF_NRM_SAVEHOUSE
			sInfo.vPlayerCoords 		= sWardrobeLauncherData.vCoords
			sInfo.fPlayerHead 			= sWardrobeLauncherData.fHeading
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.bDataSet 				= TRUE
			IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				sInfo.eWardrobeLight 	= SM_PROP_SMUG_HANGAR_WARDROBE_LRIG
			ELSE	
				sInfo.eWardrobeLight	= V_LIRG_MPHIGH_WARD_MAIN
			ENDIF
			sInfo.eWardrobeHeadLight	= V_LIRG_MPHIGH_WARD_FACE
		BREAK
	ENDSWITCH
	
	// Update our ref copy
	sData.sWardrobeInfo = sInfo
	
	RETURN TRUE
ENDFUNC
#endif 
FUNC BOOL GET_WARDROBE_DATA(WARDROBE_DATA_STRUCT &sData, WARDROBE_LAUNCHER_STRUCT &sWardrobeLauncherData)
	#if USE_CLF_DLC
		return GET_WARDROBE_DATACLF(sData,sWardrobeLauncherData)
	#endif
	#if USE_NRM_DLC
		return GET_WARDROBE_DATANRM(sData,sWardrobeLauncherData)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		
	// Temp struct so we get clean data
	WARDROBE_INFO_STRUCT sInfo
	
	sInfo.vWardrobeCoords = GET_PLAYER_PED_WARDROBE_COORDS(sWardrobeLauncherData.eWardrobe)
	sInfo.eWardrobe = sWardrobeLauncherData.eWardrobe

	SWITCH sWardrobeLauncherData.eWardrobe
		CASE PW_MICHAEL_MANSION
			sInfo.eCharacter 			= CHAR_MICHAEL
			sInfo.eSavehouse			= SAVEHOUSE_MICHAEL_BH
			sInfo.vPlayerCoords 		= <<-811.8961, 175.2218, 76.7453>> 
			sInfo.fPlayerHead 			= 116.4959 
			sInfo.vBuddyCoords 			= <<-811.4418, 179.3454, 75.7407>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-812.456299,176.530106,78.000862>>
			sInfo.vAngledAreaCoords[1] 	= <<-811.343628,173.683456,75.615738>>
			sInfo.fAngledAreaWidth 		= 2.125000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_Michael_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_Michael_ward_face
		BREAK
		CASE PW_MICHAEL_COUNTRYSIDE
			sInfo.eCharacter 			= CHAR_MICHAEL
			sInfo.eSavehouse			= SAVEHOUSE_MICHAEL_CS
			sInfo.vPlayerCoords 		= <<1969.1100, 3814.7168, 33.4280>>
			sInfo.fPlayerHead 			= 323.1107
			sInfo.vBuddyCoords 			= <<1974.2910, 3819.0266, 32.4363>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<1969.700806,3814.033447,32.592842>>
			sInfo.vAngledAreaCoords[1] 	= <<1968.748169,3815.676025,34.553722>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevTrail_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevTrail_ward_face
		BREAK
		CASE PW_TREVOR_COUNTRYSIDE
			sInfo.eCharacter 			= CHAR_TREVOR
			sInfo.eSavehouse			= SAVEHOUSE_TREVOR_CS
			sInfo.vPlayerCoords 		= <<1969.1100, 3814.7168, 33.4280>>
			sInfo.fPlayerHead 			= 323.1107
			sInfo.vBuddyCoords 			= <<1974.2910, 3819.0266, 32.4363>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<1969.700806,3814.033447,32.592842>>
			sInfo.vAngledAreaCoords[1] 	= <<1968.748169,3815.676025,34.553722>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevTrail_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevTrail_ward_face
		BREAK
		CASE PW_TREVOR_CITY
			sInfo.eCharacter 			= CHAR_TREVOR
			sInfo.eSavehouse			= SAVEHOUSE_TREVOR_VB
			sInfo.vPlayerCoords 		= << -1150.4913, -1513.3470, 10.6394 >>
			sInfo.fPlayerHead 			= 245.9826
			sInfo.vBuddyCoords 			= <<-1157.0200, -1518.3557, 9.6327>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-1150.449463,-1514.140015,9.634556>>
			sInfo.vAngledAreaCoords[1] 	= <<-1151.557983,-1512.427368,11.759556>>
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevApt_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevApt_ward_face
		BREAK
		CASE PW_TREVOR_STRIPCLUB
			sInfo.eCharacter 			= CHAR_TREVOR
			sInfo.eSavehouse			= SAVEHOUSE_TREVOR_SC
			sInfo.vPlayerCoords 		= <<105.3011, -1303.3383, 28.7688>>
			sInfo.fPlayerHead 			= 306.5809
			sInfo.vBuddyCoords 			= <<100.6848, -1294.5472, 29.2668>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<105.683533,-1304.552246,27.581297>>
			sInfo.vAngledAreaCoords[1] 	= <<104.314659,-1302.343384,30.143797>> 
			sInfo.fAngledAreaWidth 		= 2.312500
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_TrevStrip_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_TrevStrip_ward_face
		BREAK
		CASE PW_FRANKLIN_AUNTS
			sInfo.eCharacter 			= CHAR_FRANKLIN
			sInfo.eSavehouse			= SAVEHOUSE_FRANKLIN_SC
			sInfo.vPlayerCoords 		= << -17.9973, -1438.9110, 31.1018 >>
			sInfo.fPlayerHead 			= 200.2377
			sInfo.vBuddyCoords 			= <<-13.5733, -1445.6842, 29.6473>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<-18.429632,-1439.627197,30.101538>>
			sInfo.vAngledAreaCoords[1] 	= <<-18.462055,-1438.029175,32.345192>> 
			sInfo.fAngledAreaWidth 		= 1.000000
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_FrankAunt_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_FrankAunt_ward_face
		BREAK
		CASE PW_FRANKLIN_HILLS
			sInfo.eCharacter 			= CHAR_FRANKLIN
			sInfo.eSavehouse			= SAVEHOUSE_FRANKLIN_VH
			sInfo.vPlayerCoords 		= << 9.0157, 528.7267, 170.6347 >>
			sInfo.fPlayerHead 			= 120.8883
			sInfo.vBuddyCoords 			= <<6.5471, 536.3779, 169.6173>>
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.vAngledAreaCoords[0]	= <<8.094527,530.681396,169.617157>>
			sInfo.vAngledAreaCoords[1] 	= <<9.471528,527.573486,171.617157>> 
			sInfo.fAngledAreaWidth 		= 2.812500
			sInfo.bDataSet 				= TRUE
			sInfo.eWardrobeLight		= V_LiRg_FrankHill_ward_main
			sInfo.eWardrobeHeadLight	= V_LiRg_FrankHill_ward_face
		BREAK
		
		// Freemode wardrobe uses property info passed into the wardrobe script.
		CASE PW_FREEMODE
			sInfo.eCharacter 			= NO_CHARACTER
			sInfo.eSavehouse			= NUMBER_OF_SAVEHOUSE_LOCATIONS
			sInfo.vPlayerCoords 		= sWardrobeLauncherData.vCoords
			sInfo.fPlayerHead 			= sWardrobeLauncherData.fHeading
			sInfo.eStage				= INITIALISE
			sInfo.bActive 				= FALSE
			sInfo.bDataSet 				= TRUE
			IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				sInfo.eWardrobeLight 	= SM_PROP_SMUG_HANGAR_WARDROBE_LRIG
			ELSE	
				sInfo.eWardrobeLight	= V_LIRG_MPHIGH_WARD_MAIN
			ENDIF
			sInfo.eWardrobeHeadLight	= V_LIRG_MPHIGH_WARD_FACE
		BREAK
	ENDSWITCH
	
	// Update our ref copy
	sData.sWardrobeInfo = sInfo
	
	RETURN TRUE
	#endif
	#endif
ENDFUNC

PROC SET_WARDROBE_CAM_DATA_OUTFIT(WARDROBE_DATA_STRUCT &sData)

	// If we need to change offsets or add custom offsets do the following:
	//	Put the debug cam into position
	//	Output offsets using RAG/Script/Wardrobe/Output cam offsets.
	
	
	
	SWITCH sData.sWardrobeInfo.eWardrobe
	
		CASE PW_MICHAEL_MANSION
		CASE PW_FREEMODE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - OUTFIT - PW_MICHAEL_MANSION")
			sData.sWardrobeInfo.vCameraOffset			= <<0.1198,2.2929,0.3112>>
			sData.sWardrobeInfo.fCameraRotMax			= 33.0
			sData.sWardrobeInfo.fCameraFOV				= 51.600552
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_MICHAEL_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - OUTFIT - PW_MICHAEL_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0948,1.8239,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 25.0
			sData.sWardrobeInfo.fCameraFOV				= 62.949390
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
				
		CASE PW_TREVOR_CITY
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - OUTFIT - PW_TREVOR_CITY")
			sData.sWardrobeInfo.vCameraOffset			= <<0.1198,2.2929,0.3112>>
			sData.sWardrobeInfo.fCameraRotMax			= 33.0
			sData.sWardrobeInfo.fCameraFOV				= 51.600552
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_TREVOR_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - OUTFIT - PW_TREVOR_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0948,1.8239,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 25.0
			sData.sWardrobeInfo.fCameraFOV				= 62.949390
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		

		CASE PW_TREVOR_STRIPCLUB
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - OUTFIT - PW_TREVOR_STRIPCLUB")
			sData.sWardrobeInfo.vCameraOffset			= <<0.1198,2.2929,0.3112>>
			sData.sWardrobeInfo.fCameraRotMax			= 33.0
			sData.sWardrobeInfo.fCameraFOV				= 51.600552
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
	
		CASE PW_FRANKLIN_AUNTS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - OUTFIT - PW_FRANKLIN_AUNTS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.1198,2.2929,0.3112>>
			sData.sWardrobeInfo.fCameraRotMax			= 30.0
			sData.sWardrobeInfo.fCameraFOV				= 51.600552
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_FRANKLIN_HILLS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - OUTFIT - PW_FRANKLIN_HILLS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.1198,2.2929,0.3112>>
			sData.sWardrobeInfo.fCameraRotMax			= 33.0
			sData.sWardrobeInfo.fCameraFOV				= 51.600552
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC SET_WARDROBE_CAM_DATA_HEAD(WARDROBE_DATA_STRUCT &sData)

	// If we need to change offsets or add custom offsets do the following:
	//	Put the debug cam into position
	//	Output offsets using RAG/Script/Wardrobe/Output cam offsets.
	
	SWITCH sData.sWardrobeInfo.eWardrobe
		CASE PW_MICHAEL_MANSION
		CASE PW_FREEMODE
			CPRINTLN( DEBUG_PED_COMP, "WARDROBE - HEAD - PW_MICHAEL_MANSION")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8535,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.634010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
		CASE PW_MICHAEL_COUNTRYSIDE
			CPRINTLN( DEBUG_PED_COMP, "WARDROBE - HEAD - PW_MICHAEL_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8035,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
		CASE PW_TREVOR_CITY
			CPRINTLN( DEBUG_PED_COMP, "WARDROBE - HEAD - PW_TREVOR_CITY")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8035,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
		CASE PW_TREVOR_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - HEAD - PW_TREVOR_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8035,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 40.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
		CASE PW_TREVOR_STRIPCLUB
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HEAD - PW_TREVOR_STRIPCLUB")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8035,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
		CASE PW_FRANKLIN_AUNTS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HEAD - PW_FRANKLIN_AUNTS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8035,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 30.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
		CASE PW_FRANKLIN_HILLS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HEAD - PW_FRANKLIN_HILLS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0377,0.8035,0.6971>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010	
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.6
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC SET_WARDROBE_CAM_DATA_TORSO(WARDROBE_DATA_STRUCT &sData)

	// If we need to change offsets or add custom offsets do the following:
	//	Put the debug cam into position
	//	Output offsets using RAG/Script/Wardrobe/Output cam offsets.
	
	
	
	SWITCH sData.sWardrobeInfo.eWardrobe
	
		CASE PW_MICHAEL_MANSION
		CASE PW_FREEMODE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - TORSO - PW_MICHAEL_MANSION")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
		BREAK
		
		CASE PW_MICHAEL_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - TORSO - PW_MICHAEL_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
		BREAK
		
		CASE PW_TREVOR_CITY
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - TORSO - PW_TREVOR_CITY")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
		BREAK
		
		CASE PW_TREVOR_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - TORSO - PW_TREVOR_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 40.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
		BREAK
		
		CASE PW_TREVOR_STRIPCLUB
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - TORSO - PW_TREVOR_STRIPCLUB")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
		BREAK
		
		CASE PW_FRANKLIN_AUNTS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - TORSO - PW_FRANKLIN_AUNTS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 28.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
					
		BREAK
		
		CASE PW_FRANKLIN_HILLS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - TORSO - PW_FRANKLIN_HILLS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0784,1.6781,0.2271>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
		
		BREAK
		
	ENDSWITCH
	
ENDPROC


PROC SET_WARDROBE_CAM_DATA_LEGS(WARDROBE_DATA_STRUCT &sData)

	// If we need to change offsets or add custom offsets do the following:
	//	Put the debug cam into position
	//	Output offsets using RAG/Script/Wardrobe/Output cam offsets.
	
	
	SWITCH sData.sWardrobeInfo.eWardrobe
		CASE PW_MICHAEL_MANSION
		CASE PW_FREEMODE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - LEGS - PW_MICHAEL_MANSION")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0770,1.6468,-0.5692>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_MICHAEL_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - LEGS - PW_MICHAEL_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 35.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_TREVOR_CITY
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - LEGS - PW_TREVOR_CITY")
			sData.sWardrobeInfo.vCameraOffset			= <<-0.2476,1.4693,-0.5625>>//<<0.0770,1.6468,-0.5692>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 47.679413
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_TREVOR_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - LEGS - PW_TREVOR_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 35.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
			
		CASE PW_TREVOR_STRIPCLUB
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - LEGS - PW_TREVOR_STRIPCLUB")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0770,1.6468,-0.5692>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_FRANKLIN_AUNTS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - LEGS - PW_FRANKLIN_AUNTS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.5624,1.1620,-0.5542>> //<<0.0770,1.6468,-0.5692>>
			sData.sWardrobeInfo.fCameraRotMax			= 28.0
			sData.sWardrobeInfo.fCameraFOV				= 52.507298 //43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_FRANKLIN_HILLS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - LEGS - PW_FRANKLIN_HILLS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0770,1.6468,-0.5692>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC SET_WARDROBE_CAM_DATA_FEET(WARDROBE_DATA_STRUCT &sData)

	// If we need to change offsets or add custom offsets do the following:
	//	Put the debug cam into position
	//	Output offsets using RAG/Script/Wardrobe/Output cam offsets.
	
	sData.sWardrobeInfo.fCameraHeightOffset		= 0.25
	
	SWITCH sData.sWardrobeInfo.eWardrobe
		CASE PW_MICHAEL_MANSION
		CASE PW_FREEMODE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - FEET - PW_MICHAEL_MANSION")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_MICHAEL_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - FEET - PW_MICHAEL_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_TREVOR_CITY
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - FEET - PW_TREVOR_CITY")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_TREVOR_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - FEET - PW_TREVOR_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 35.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_TREVOR_STRIPCLUB
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - FEET - PW_TREVOR_STRIPCLUB")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
				
		CASE PW_FRANKLIN_AUNTS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - FEET - PW_FRANKLIN_AUNTS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 28.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		BREAK
		
		CASE PW_FRANKLIN_HILLS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - FEET - PW_FRANKLIN_HILLS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0760,1.6244,-0.0675>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.5
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC SET_WARDROBE_CAM_DATA_HAND(WARDROBE_DATA_STRUCT &sData)

	// If we need to change offsets or add custom offsets do the following:
	//	Put the debug cam into position
	//	Output offsets using RAG/Script/Wardrobe/Output cam offsets.
	
	
	SWITCH sData.sWardrobeInfo.eWardrobe
	
		CASE PW_MICHAEL_MANSION
		CASE PW_FREEMODE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HAND - PW_MICHAEL_MANSION")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_MICHAEL_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HAND - PW_MICHAEL_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_TREVOR_CITY
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HAND - PW_TREVOR_CITY")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_TREVOR_COUNTRYSIDE
			CPRINTLN(DEBUG_PED_COMP,"WARDROBE - HAND - PW_TREVOR_COUNTRYSIDE")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 40.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_TREVOR_STRIPCLUB
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HAND - PW_TREVOR_STRIPCLUB")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
		BREAK
		
		CASE PW_FRANKLIN_AUNTS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HAND - PW_FRANKLIN_AUNTS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 30.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1
			
		BREAK
		
		CASE PW_FRANKLIN_HILLS
			CPRINTLN(DEBUG_PED_COMP, "WARDROBE - HAND - PW_FRANKLIN_HILLS")
			sData.sWardrobeInfo.vCameraOffset			= <<0.0776,1.6605,-0.2204>>
			sData.sWardrobeInfo.fCameraRotMax			= 45.0
			sData.sWardrobeInfo.fCameraFOV				= 43.434010
			sData.sWardrobeInfo.fCameraHeightOffset		= -0.1

		BREAK
		
	ENDSWITCH
		
ENDPROC


PROC BUILD_BROWSE_LIST_FOR_WARDROBE_SP(SHOP_COMPONENT_ITEMS_STRUCT &sClothesInfo, CLOTHES_MENU_ENUM eClothesMenu, INT iMainMenuGroup, SetupClothingItemForShop &fpSetupClothingItemForShop, GetPedComponentItemRequisite &fpGetPedComponentItemRequisite)

	UNUSED_PARAMETER(fpGetPedComponentItemRequisite)
	UNUSED_PARAMETER(iMainMenuGroup)
	
	sClothesInfo.iItemCount = 0
	INT i
	REPEAT COUNT_OF(sClothesInfo.iSubMenusWithItems) i
		sClothesInfo.iSubMenusWithItems[i] = 0
		sClothesInfo.iSubMenusWithNEWItems[i] = 0
	ENDREPEAT
	
	SETUP_SP_CLOTHES_DLC(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)

	SETUP_SP_CLOTHES_OUTFITS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_SUITS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	IF eClothesMenu != CLO_MENU_MAIN
		SETUP_SP_CLOTHES_SUITSFULL(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF

	SETUP_SP_CLOTHES_TORSOS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_JACKETS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_CASUAL_JACKETS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	IF eClothesMenu != CLO_MENU_MAIN // These get added in Franklin's casual jackets
		SETUP_SP_CLOTHES_CASUALJACKETS_JACKETS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu != CLO_MENU_MAIN // These get added in Franklin's casual jackets
		SETUP_SP_CLOTHES_CAS_JACKET_TOPS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu != CLO_MENU_MAIN // These get added in the suits menu
		SETUP_SP_CLOTHES_SUITJACKETS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu != CLO_MENU_MAIN // These get added in the suits menu
		SETUP_SP_CLOTHES_SUITJACKETS_BUTTONED(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu != CLO_MENU_MAIN // These get added in the suits menu
		SETUP_SP_CLOTHES_SWEATERS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu != CLO_MENU_MAIN // These get added in the suits menu
		SETUP_SP_CLOTHES_TIES(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	SETUP_SP_CLOTHES_HOODIES(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_SHIRTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_TSHIRTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)

	SETUP_SP_CLOTHES_OPENSHIRTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	IF eClothesMenu != CLO_MENU_MAIN // These get added in the openshirts menu
		SETUP_SP_CLOTHES_OPENSHIRTS_M(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu != CLO_MENU_MAIN // These get added in the openshirts menu
		SETUP_SP_CLOTHES_UNDERSHIRTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	SETUP_SP_CLOTHES_POLOSHIRTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_TANKTOPS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)

	IF eClothesMenu != CLO_MENU_MAIN  // These get added in the suits menu
		SETUP_SP_CLOTHES_SUITVESTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	
	SETUP_SP_CLOTHES_VESTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)

	SETUP_SP_CLOTHES_LEGS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_SHORTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	
	IF eClothesMenu != CLO_MENU_MAIN  // These get added in the suits menu
		SETUP_SP_CLOTHES_SUITPANTS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF

	SETUP_SP_CLOTHES_FEET(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_SUITSHOES(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)

	SETUP_SP_CLOTHES_HATS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	IF eClothesMenu!= CLO_MENU_MAIN // These get added to Franklin's hats menu
		SETUP_SP_CLOTHES_HATS_FWD(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu!= CLO_MENU_MAIN // These get added to Franklin's hats menu
		SETUP_SP_CLOTHES_HATS_BACK(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	SETUP_SP_CLOTHES_MASKS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	SETUP_SP_CLOTHES_GLASSES(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	IF eClothesMenu!= CLO_MENU_MAIN // These get added to Glasses menu
		SETUP_SP_CLOTHES_GLASSES_SUB(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu!= CLO_MENU_MAIN // These get added to Glasses menu
		SETUP_SP_CLOTHES_SPORTSHADES(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	IF eClothesMenu!= CLO_MENU_MAIN // These get added to Glasses menu
		SETUP_SP_CLOTHES_STREETSHADES(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
	ENDIF
	SETUP_SP_CLOTHES_EARINGS(fpSetupClothingItemForShop, sClothesInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), eClothesMenu)
ENDPROC


PROC BUILD_BROWSE_LIST_FOR_WARDROBE_MP(SHOP_COMPONENT_ITEMS_STRUCT &sClothesInfo, CLOTHES_MENU_ENUM eClothesMenu, INT iMainMenuGroup, SetupClothingItemForShop &fpSetupClothingItemForShop, GetPedComponentItemRequisite &fpGetPedComponentItemRequisite)
	sClothesInfo.iItemCount = 0
	INT i
	REPEAT COUNT_OF(sClothesInfo.iSubMenusWithItems) i
		sClothesInfo.iSubMenusWithItems[i] = 0
		sClothesInfo.iSubMenusWithNEWItems[i] = 0
	ENDREPEAT
	
	g_iShopMenuLastDLCItem = 0
	
	
	IF iMainMenuGroup = -1
		PRINTLN("BUILD_BROWSE_LIST_FOR_WARDROBE_MP - Main menu not selected yet")
		EXIT
	ENDIF
	
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
	INT iCurrentPed = GET_SHOP_PED_ID_FOR_MODEL(ePedModel)
	
	IF eClothesMenu = CLO_MENU_MAIN
	
		CLOTHES_MENU_ENUM eTempMenu
		INT iStartItem = ENUM_TO_INT(CLO_MENU_MP_OUTFITS)
		INT iEndItem = ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU)
		FOR i = iStartItem TO iEndItem
		
			eTempMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, i)
		
			IF IS_CLOTHES_MENU_SUITABLE_FOR_MP_PLAYER_WARDROBE(eTempMenu, ePedModel)
				SWITCH iMainMenuGroup
					CASE 0	IF IS_MP_OUTFIT_MENU(eTempMenu)			IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 1	IF IS_MP_UPPER_MENU(eTempMenu)			IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 2	IF IS_MP_LOWER_MENU(eTempMenu)			IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 3	IF IS_MP_FEET_MENU(eTempMenu)			IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 4	IF IS_MP_HATS_MENU(eTempMenu)			IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 5	IF IS_MP_MASKS_MENU(eTempMenu)			IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 6	IF IS_MP_GLASSES_MENU(eTempMenu)		IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
					CASE 7	IF IS_MP_ACCESSORIES_MENU(eTempMenu)	IF IS_BIT_SET(g_iSubMenusWithItems[i/32], i%32) SET_BIT(sClothesInfo.iSubMenusWithItems[i/32], i%32) ENDIF ENDIF BREAK
				ENDSWITCH
			ENDIF
		ENDFOR
		
		
		/*g_bBailDLCChecksWhenMenuBitSet = TRUE
		REPEAT COUNT_OF(g_iSubMenusWithItems) i
			g_iSubMenusWithItems[i] = 0
		ENDREPEAT
	
		// Player will always buy items from the shop so no need to process the new item star unless it's an award item (TODO)
		// We always list the menus so add them all.
		CLOTHES_MENU_ENUM eTempMenu
		INT iStartItem = ENUM_TO_INT(CLO_MENU_JACKETS)
		INT iEndItem = ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU)
		FOR i = iStartItem TO iEndItem
		
			eTempMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, i)
		
			/*IF IS_CLOTHES_MENU_SUITABLE_FOR_MP_PLAYER_WARDROBE(eTempMenu, ePedModel)
				SWITCH iMainMenuGroup
					CASE 0	IF IS_MP_OUTFIT_MENU(eTempMenu)			SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 1	IF IS_MP_UPPER_MENU(eTempMenu)			SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 2	IF IS_MP_LOWER_MENU(eTempMenu)			SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 3	IF IS_MP_FEET_MENU(eTempMenu)			SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 4	IF IS_MP_HATS_MENU(eTempMenu)			SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 5	IF IS_MP_MASKS_MENU(eTempMenu)			SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 6	IF IS_MP_GLASSES_MENU(eTempMenu)		SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
					CASE 7	IF IS_MP_ACCESSORIES_MENU(eTempMenu)	SET_BIT(sClothesInfo.iSubMenusWithItems[(ENUM_TO_INT(i)/32)], (ENUM_TO_INT(i)%32)) ENDIF BREAK
				ENDSWITCH
			ENDIF
			
			IF IS_CLOTHES_MENU_SUITABLE_FOR_MP_PLAYER_WARDROBE(eTempMenu, ePedModel)
				SWITCH iMainMenuGroup
					CASE 0	IF IS_MP_OUTFIT_MENU(eTempMenu)			SETUP_MP_CLOTHES_OUTFITS(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 1	IF IS_MP_UPPER_MENU(eTempMenu)			SETUP_MP_CLOTHES_UPPER(fpSetupClothingItemForShop, fpGetPedComponentItemRequisite, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 2	IF IS_MP_LOWER_MENU(eTempMenu)			SETUP_MP_CLOTHES_LOWER(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 3	IF IS_MP_FEET_MENU(eTempMenu)			SETUP_MP_CLOTHES_FEET(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 4	IF IS_MP_HATS_MENU(eTempMenu)			SETUP_MP_CLOTHES_HATS(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 5	IF IS_MP_MASKS_MENU(eTempMenu)			SETUP_MP_CLOTHES_MASKS(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 6	IF IS_MP_GLASSES_MENU(eTempMenu)		SETUP_MP_CLOTHES_GLASSES(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
					CASE 7	IF IS_MP_ACCESSORIES_MENU(eTempMenu)	SETUP_MP_CLOTHES_ACCESSORIES(fpSetupClothingItemForShop, fpGetPedComponentItemRequisite, sClothesInfo, iCurrentPed, eTempMenu) ENDIF BREAK
				ENDSWITCH
			ENDIF
		ENDFOR
		
		g_bBailDLCChecksWhenMenuBitSet = FALSE*/
	
	ELIF IS_MP_UPPER_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_UPPER(fpSetupClothingItemForShop, fpGetPedComponentItemRequisite, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_OUTFIT_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_OUTFITS(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_LOWER_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_LOWER(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_FEET_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_FEET(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_HATS_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_HATS(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_MASKS_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_MASKS(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_GLASSES_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_GLASSES(fpSetupClothingItemForShop, sClothesInfo, iCurrentPed, eClothesMenu)
	ELIF IS_MP_ACCESSORIES_MENU(eClothesMenu)
		SETUP_MP_CLOTHES_ACCESSORIES(fpSetupClothingItemForShop, fpGetPedComponentItemRequisite, sClothesInfo, iCurrentPed, eClothesMenu)
	ENDIF
	
	IF eClothesMenu != CLO_MENU_MAIN
		SORT_DLC_CLOTHES_FOR_MENU(sClothesInfo, eClothesMenu)
	ENDIF
ENDPROC


// --------FUNCTIONS FOR SETTING A RANDOM COMBINATION OF CLOTHES---------------------------------

/// PURPOSE:
///    Resets the clothes data struct passed in
///    Only to be called as part of SET_RANDOM_CLOTHES_COMBO (used for singleplayer Q skips + SP switch scenes)
/// PARAMS
///    sData - clothes item struct we want to reset
PROC RESET_CLOTHES_DATA_STRUCT(SHOP_COMPONENT_ITEMS_STRUCT &sData)
	// reset the clothes data struct
	INT i 
    REPEAT MAX_CLOTHES_PER_MENU i 
            sData.eItems[i] = DUMMY_PED_COMP 
            sData.eTypes[i] = COMP_TYPE_TORSO 
    ENDREPEAT 
    sData.iItemCount = 0 
    REPEAT COUNT_OF(sData.iSubMenusWithItems) i
		sData.iSubMenusWithItems[i] = 0 
		sData.iSubMenusWithNEWItems[i] = 0 
    ENDREPEAT
ENDPROC

/// PURPOSE:
///   	Gets a random item from the wardrobe struct.
///    Tries to find a random one. If this fails it loops through and returns 1st valid one in struct.
/// 	Only to be called as part of SET_RANDOM_CLOTHES_COMBO (used for singleplayer Q skips + SP switch scenes)
/// PARAMS:
///    eType - the type of item we are using. (currently only feet, legs or torso)
///    sClothesData - clothes data struct
///    ePedModel - this ped's model.(GET_PLAYER_PED_MODEL)
///    bMustBeAvailable - to be valid, do items in this struct have to be available + acquired?
///    bAllowUnderwear - if false no underwear or bare chest allowed
///    bAllowProgrammer - only used for michael. if false no part of programmer outfit allowed
/// RETURNS:
///    INT the item found
FUNC INT GET_RANDOM_ITEM_FROM_STRUCT(PED_COMP_TYPE_ENUM eType, SHOP_COMPONENT_ITEMS_STRUCT &sClothesData, MODEL_NAMES ePedModel, BOOL bMustBeAvailable, BOOL bAllowUnderwear = TRUE, BOOL bAllowProgrammer = TRUE)

	INT iItemFound = -1
	INT iItem 
	PED_COMP_ITEM_DATA_STRUCT eItemInfo
	INT iRandomAttempts = 0
	INT iMaxRandomAttempts = 20
	BOOL bSuitable = TRUE
	
	WHILE (iRandomAttempts < iMaxRandomAttempts) AND iItemFound = -1

		// pick a random item
		bSuitable = TRUE
		iItem = GET_RANDOM_INT_IN_RANGE(0, sClothesData.iItemCount) 
		eItemInfo = GET_PED_COMP_DATA_FOR_ITEM_SP(ePedModel, eType, sClothesData.eItems[iItem])
		
		// check if it is valid
		IF IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_OUTFIT_ONLY_BIT)
		AND eType <> COMP_TYPE_OUTFIT
			// item is outfit only, try another one
			CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  iItem: ", iItem, "outfit only")
		ELSE
			// do underwear check
			IF bAllowUnderwear = FALSE
				IF IS_ITEM_UNDERWEAR(ePedModel, eType, sClothesData.eItems[iItem])
					bSuitable = FALSE
					CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  found random Item: ", sClothesData.eItems[iItem], " underwear item: invalid.")
				ENDIF
			ENDIF
			
			// do programmer outfit check
			IF bSuitable = TRUE
				IF bAllowProgrammer = FALSE
					IF ePedModel = PLAYER_ZERO
						IF IS_ITEM_PART_OF_PROGRAMMER_OUTFIT(sClothesData.eItems[iItem], eType)
							bSuitable = FALSE
							CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  found random Item: ", sClothesData.eItems[iItem], " programmer item: invalid.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// do availability check
			IF bSuitable = TRUE
				IF bMustBeAvailable = TRUE
					IF IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_AVAILABLE_BIT)
					AND IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_ACQUIRED_BIT)
						// item is not outfit only + is available + acquired (exit)
						CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  found random valid Item: ", sClothesData.eItems[iItem], " not outfitOnly. avail+acquired=true")
						iItemFound = iItem
					ENDIF
				ELSE
					// item is not outfit only and doesnt need to be available (exit)
					CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  found random valid Item: ", sClothesData.eItems[iItem], " not outfitOnly. mustBeAvail=false")
					iItemFound = iItem
				ENDIF
			ENDIF
		ENDIF 
		iRandomAttempts++
	ENDWHILE

	// not found suitable item in multiple attempts, just loop through and find 1st suitable one
	IF iItemFound = -1
		FOR iItem = 0 TO sClothesData.iItemCount -1
			bSuitable = TRUE
			eItemInfo = GET_PED_COMP_DATA_FOR_ITEM_SP(ePedModel, eType, sClothesData.eItems[iItem])
			
			// check if it is valid
			IF IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_OUTFIT_ONLY_BIT)
			AND eType <> COMP_TYPE_OUTFIT
			
				// item is outfit only, try another one
				CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  iItem: ", iItem, "outfit only")
			ELSE
			
				// do underwear check
				IF bAllowUnderwear = FALSE
					IF IS_ITEM_UNDERWEAR(ePedModel, eType, sClothesData.eItems[iItem])
						bSuitable = FALSE
						CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType)," looped Item: ", sClothesData.eItems[iItem], " underwear item: invalid.")
					ENDIF
				ENDIF
				
				// do programmer outfit check
				IF bSuitable = TRUE
					IF bAllowProgrammer = FALSE
						IF ePedModel = PLAYER_ZERO
							IF IS_ITEM_PART_OF_PROGRAMMER_OUTFIT(sClothesData.eItems[iItem], eType)
								bSuitable = FALSE
								CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  looped Item: ", sClothesData.eItems[iItem], " programmer item: invalid.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// do availability check
				IF bSuitable = TRUE
					IF bMustBeAvailable = TRUE
						IF IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_AVAILABLE_BIT)
						AND IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_ACQUIRED_BIT)
							// item is not outfit only + is available + acquired (exit)
							iItemFound = iItem
							iItem = sClothesData.iItemCount + 1 // exit
							CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  looped found valid Item: ", sClothesData.eItems[iItem], " not outfitOnly. avail+acquired=true")
						ENDIF
					ELSE
						// item is not outfit only and doesnt need to be available
						iItemFound = iItem
						iItem = sClothesData.iItemCount + 1 // exit
						CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  looped found valid Item: ", sClothesData.eItems[iItem], " not outfitOnly. mustBeAvail=false")
					ENDIF
				ENDIF
			ENDIF 
		ENDFOR
	ENDIF

	CPRINTLN(DEBUG_PED_COMP, "GET_RANDOM_ITEM_FROM_STRUCT ", GET_PED_COMP_TYPE_STRING(eType),"  returning Item: ", sClothesData.eItems[iItem])
	RETURN iItemFound
ENDFUNC

/// PURPOSE:
///   	Fills the wardrobe struct with items of the type specified by picking a random subitem type.
///    	e.g. For torsos: hoodies, jackets, vests etc
/// 	Only to be called as part of SET_RANDOM_CLOTHES_COMBO (used for singleplayer Q skips + SP switch scenes)
/// PARAMS:
///    eType - the type of item we are using. (currently only feet, legs or torso)
///    sClothesData - clothes data struct
///    iPlayerPed - player ped (GET_CURRENT_PLAYER_PED_INT)
///    ePedModel - this ped's model.(GET_PLAYER_PED_MODEL)
///    bMustBeAvailable - to be valid, do items in this submenu have to be available + acquired?
///    iFirstValidItem - the first valid item in this submenu
///    bSuitsOnly - use this to specify the only outfits to use are suits
///    bAllowUnderwear - if false no underwear or bare chest allowed
///    bAllowProgrammer - only used for michael. if false no part of programmer outfit allowed
/// RETURNS:
///    TRUE if this player has at least 1 suitable item in the random subemenu. FALSE otherwise
FUNC BOOL FILL_WARDROBE_MENU_WITH_TYPE(PED_COMP_TYPE_ENUM eType, SHOP_COMPONENT_ITEMS_STRUCT &sClothesData, INT iPlayerPed, MODEL_NAMES ePedModel, BOOL bMustBeAvailable, INT &iFirstValidItem, BOOL bSuitsOnly =FALSE, BOOL bAllowUnderwear = TRUE,  BOOL bAllowProgrammer = TRUE)

	PED_COMP_ITEM_DATA_STRUCT eItemInfo
	INT iItem
	INT iMenu
	SetupClothingItemForShop fpSetupClothingItemForShop = &SETUP_CLOTHING_ITEM_FOR_SHOP_SP
	BOOL bSuitable
	
	// reset the struct as we are about to refill it
	RESET_CLOTHES_DATA_STRUCT(sClothesData)

	SWITCH eType
	
		// Outfit
		CASE COMP_TYPE_OUTFIT
			// pick a random outfit menu
			IF bSuitsOnly = TRUE
				iMenu = 1
			ELSE
				iMenu = GET_RANDOM_INT_IN_RANGE(0, 2)
			ENDIF
		
			SWITCH iMenu
				CASE 0
				 	SETUP_SP_CLOTHES_OUTFITS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_OUTFITS_W)
				BREAK
				
				CASE 1
					SETUP_SP_CLOTHES_SUITSFULL(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_SUITSFULL_W)
				BREAK
			ENDSWITCH
		BREAK
	
		// Torso
		CASE COMP_TYPE_TORSO
		
			// pick a random torso menu
			iMenu = GET_RANDOM_INT_IN_RANGE(0, 10)
		
			SWITCH iMenu
				CASE 0
					 SETUP_SP_CLOTHES_TORSOS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_TORSO_W)
				BREAK
				
				CASE 1
					 SETUP_SP_CLOTHES_JACKETS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_JACKETS_W)
				BREAK
				
				CASE 2
					 SETUP_SP_CLOTHES_SUITJACKETS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_SUITJACKETS_W)
				BREAK
				
				CASE 3
					 SETUP_SP_CLOTHES_HOODIES(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_HOODIES_W)
				BREAK
				
				CASE 4
					 SETUP_SP_CLOTHES_SHIRTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_SHIRTS_W)
				BREAK
				
				CASE 5
					 SETUP_SP_CLOTHES_TSHIRTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_TSHIRTS_W)
				BREAK
				
				CASE 6
					 SETUP_SP_CLOTHES_POLOSHIRTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_POLOSHIRT_W)
				BREAK
				
				CASE 7
					 SETUP_SP_CLOTHES_TANKTOPS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_TANKTOPS_W)
				BREAK
				
				CASE 8
					 SETUP_SP_CLOTHES_VESTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_VESTS_W)
				BREAK
				
				CASE 9
					 SETUP_SP_CLOTHES_OPENSHIRTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_OPENSHIRTS_W)
				BREAK
			
			ENDSWITCH
		BREAK
		
		// Legs
		CASE COMP_TYPE_LEGS
		
			// pick a random legs menu
			iMenu = GET_RANDOM_INT_IN_RANGE(0, 3)
			
			SWITCH iMenu
				CASE 0
					 SETUP_SP_CLOTHES_LEGS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_LEGS_W)
				BREAK
				CASE 1
					 SETUP_SP_CLOTHES_SUITPANTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_SUITPANTS_W)
				BREAK
				CASE 2
					 SETUP_SP_CLOTHES_SHORTS(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_SHORTS_W)
				BREAK
			ENDSWITCH
		BREAK
		
		// Feet
		CASE COMP_TYPE_FEET
		
			// pick a random legs menu
			iMenu = GET_RANDOM_INT_IN_RANGE(0, 2)
			
			SWITCH iMenu
				CASE 0
					 SETUP_SP_CLOTHES_FEET(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_FEET_W)
				BREAK
				CASE 1
					 SETUP_SP_CLOTHES_SUITSHOES(fpSetupClothingItemForShop, sClothesData, iPlayerPed, CLO_MENU_SUITSHOES_W)
				BREAK
			ENDSWITCH
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE passed invalid type eType=", GET_PED_COMP_TYPE_STRING(eType))
			RETURN FALSE
		BREAK
	ENDSWITCH

	IF sClothesData.iItemCount > 0
		// the player has some items in this wardrobe section
		
		// check if this ped has an item in this menu that is suitable (not outfit only etc)
		FOR iItem = 0 TO sClothesData.iItemCount -1
			bSuitable = TRUE
			eItemInfo = GET_PED_COMP_DATA_FOR_ITEM_SP(ePedModel, eType, sClothesData.eItems[iItem])

			IF NOT IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_OUTFIT_ONLY_BIT)
				// item is not outfit only
						
				// do underwear check
				IF bAllowUnderwear = FALSE
					IF IS_ITEM_UNDERWEAR(ePedModel, eType, sClothesData.eItems[iItem])
						bSuitable = FALSE
						CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE ", GET_PED_COMP_TYPE_STRING(eType),"  found Item: ", sClothesData.eItems[iItem], " underwear item: invalid.")
					ENDIF
				ENDIF
				
				// do programmer outfit check
				IF bSuitable = TRUE
					IF bAllowProgrammer = FALSE
						IF ePedModel = PLAYER_ZERO
							IF IS_ITEM_PART_OF_PROGRAMMER_OUTFIT(sClothesData.eItems[iItem], eType)
								bSuitable = FALSE
								CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE ", GET_PED_COMP_TYPE_STRING(eType),"  found Item: ", sClothesData.eItems[iItem], " programmer item: invalid.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// do availability check
				IF bSuitable = TRUE		
					IF bMustBeAvailable = TRUE
						IF IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_AVAILABLE_BIT)
						AND IS_BIT_SET(eItemInfo.iProperties, PED_COMPONENT_ACQUIRED_BIT)
							// item is available and acquired- it is valid
							CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE ", GET_PED_COMP_TYPE_STRING(eType),"  player has valid item :available + acquired.")
							iFirstValidItem = iItem
							RETURN TRUE
						ENDIF
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE ", GET_PED_COMP_TYPE_STRING(eType),"  player has valid item.")
						iFirstValidItem = iItem
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE ", GET_PED_COMP_TYPE_STRING(eType),"  iItemCount=0. eType=", GET_PED_COMP_TYPE_STRING(eType), " iMenu= ", iMenu)
	ENDIF
	
	// the player has no valid items in this wardrobe section
	CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE no valid items eType=", GET_PED_COMP_TYPE_STRING(eType), " iMenu= ", iMenu)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If the item passed in is one of Michael's default grey suit items
///    it overwrites it with the normal or wrecked version based on the suit flowflag.
/// PARAMS:
///    ePedModel - model of ped we're using
///    eType - type of item we're setting
///    eItem - the item we are setting
PROC DO_MICHAEL_WRECKED_SUIT_CHECK(MODEL_NAMES ePedModel, PED_COMP_TYPE_ENUM eType, PED_COMP_NAME_ENUM &eItem)
	IF ePedModel = PLAYER_ZERO
		// do wrecked suit check
		IF eType = COMP_TYPE_TORSO
			IF (eItem >= TORSO_P0_GREY_SUIT AND eItem <= TORSO_P0_GREY_SUIT_01)
				// pick which of Michael's default grey jackets to use
				// based on Exile wrecked suit flowflag
				#if  USE_CLF_DLC
					CPRINTLN(DEBUG_PED_COMP, "Setting normal jacket for random item.")
					eItem = TORSO_P0_GREY_SUIT
				#endif
				#if USE_NRM_DLC
					CPRINTLN(DEBUG_PED_COMP, "Setting wrecked jacket for random item.")
					eItem = TORSO_P0_GREY_SUIT_01
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HAS_HAGGARD_SUIT]
						CPRINTLN(DEBUG_PED_COMP, "Setting wrecked jacket for random item.")
						eItem = TORSO_P0_GREY_SUIT_01
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "Setting normal jacket for random item.")
						eItem = TORSO_P0_GREY_SUIT
					ENDIF
				#endif
				#endif
			ENDIF
			
		ELIF eType = COMP_TYPE_LEGS
			IF (eItem >= LEGS_P0_GREY_SUIT AND eItem <= LEGS_P0_GREY_SUIT_1)
				// pick which of Michael's default grey suit pants to use
				// based on Exile wrecked suit flowflag				
				#if  USE_CLF_DLC
					CPRINTLN(DEBUG_PED_COMP, "Setting normal suit pants for random item.")
						eItem = LEGS_P0_GREY_SUIT
				#endif
				#if USE_NRM_DLC
					CPRINTLN(DEBUG_PED_COMP, "Setting wrecked suit pants for random item.")
						eItem = LEGS_P0_GREY_SUIT_1
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HAS_HAGGARD_SUIT]
						CPRINTLN(DEBUG_PED_COMP, "Setting wrecked suit pants for random item.")
						eItem = LEGS_P0_GREY_SUIT_1
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "Setting normal suit pants for random item.")
						eItem = LEGS_P0_GREY_SUIT
					ENDIF
				#endif
				#endif
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tries to set a random item on the ped, of the specified ped comp type
/// Only to be called as part of SET_RANDOM_CLOTHES_COMBO (used for singleplayer Q skips + SP switch scenes)
/// PARAMS:
///    mPed - the ped we want to put the item on
///    eType - the type of item we are using. (currently only feet, legs or torso)
///    sClothesData - clothes data struct
///    iPlayerPed - player ped (GET_CURRENT_PLAYER_PED_INT)
///    ePedModel - this ped's model.(GET_PLAYER_PED_MODEL)
///    bMustBeAvailable - does the random item have to be available and acquired?
///    bSuitsOnly- if this is true and eType is outfit we will only use full suit outfits
///    bAllowUnderwear - if false no underwear or bare chest allowed
///    bAllowProgrammer - only used for michael. if false no part of programmer outfit allowed
/// RETURNS:
///    TRUE if suitable random item was equipped. FALSE otherwise
FUNC BOOL SET_RANDOM_ITEM_OF_TYPE(PED_INDEX mPed, PED_COMP_TYPE_ENUM eType, SHOP_COMPONENT_ITEMS_STRUCT &sClothesData, INT iPlayerPed, MODEL_NAMES ePedModel, BOOL bMustBeAvailable,  BOOL bSuitsOnly = FALSE, BOOL bAllowUnderwear = TRUE, BOOL bAllowProgrammer = TRUE)
	INT iRandomAttempts = 0
	INT iItemFound
	INT iFirstValidItem
	PED_COMP_NAME_ENUM eItem
	
	WHILE NOT FILL_WARDROBE_MENU_WITH_TYPE(eType, sClothesData, iPlayerPed, ePedModel, bMustBeAvailable, iFirstValidItem, bSuitsOnly, bAllowUnderwear)
		// drops into here if ped has no valid items in this wardrobe section
		iRandomAttempts++
		IF iRandomAttempts > 20
			CPRINTLN(DEBUG_PED_COMP, "FILL_WARDROBE_MENU_WITH_TYPE ", GET_PED_COMP_TYPE_STRING(eType)," too many random attempts: exit")
			RETURN FALSE
		ENDIF
	ENDWHILE
	
	// this ped has a valid an item available in this menu
	// pick a random suitable item
	iItemFound = GET_RANDOM_ITEM_FROM_STRUCT(eType, sClothesData, ePedModel, bMustBeAvailable, bAllowUnderwear, bAllowProgrammer)
	eItem = sClothesData.eItems[iItemFound]
	DO_MICHAEL_WRECKED_SUIT_CHECK(ePedModel, eType, eItem)

	// put this item on
	iRandomAttempts = 0
	WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(mPed, eType, eItem, bMustBeAvailable)
		CPRINTLN(DEBUG_PED_COMP, "SET_RANDOM_ITEM_OF_TYPE: ", GET_PED_COMP_TYPE_STRING(eType),"  set current: not available / acquired / suitable, try again")
		
		// drops into here if this item isn't available / acquired / suitable
		// grab another random item
		iItemFound = GET_RANDOM_ITEM_FROM_STRUCT(eType, sClothesData, ePedModel, bMustBeAvailable, bAllowUnderwear, bAllowProgrammer)
		eItem = sClothesData.eItems[iItemFound]
		DO_MICHAEL_WRECKED_SUIT_CHECK(ePedModel, eType, eItem)

		// early out
		iRandomAttempts++
		IF iRandomAttempts > 20
			iItemFound = iFirstValidItem
			CPRINTLN(DEBUG_PED_COMP, "SET_RANDOM_ITEM_OF_TYPE: ", GET_PED_COMP_TYPE_STRING(eType),"  set current: too many random attempts: go with 1st valid")
		ENDIF
		IF iRandomAttempts > 21
			CPRINTLN(DEBUG_PED_COMP, "SET_RANDOM_ITEM_OF_TYPE: ", GET_PED_COMP_TYPE_STRING(eType),"  set current: 1st valid item failed, exit")
			RETURN FALSE // exra safety check, shouldn't get hit
		ENDIF
	ENDWHILE

	//  force valid outfit
	FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_SP(mPed, eType, sClothesData.eItems[iItemFound], bMustBeAvailable)
	
	CPRINTLN(DEBUG_PED_COMP, "SET_RANDOM_ITEM_OF_TYPE succesfully set item type: ", GET_PED_COMP_TYPE_STRING(eType), " ItemFound= ", sClothesData.eItems[iItemFound])
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Sets a random combination of clothes on the ped passed in
///    Picks random feet, then legs, then torso.  Forcing outfit to be valid each time.
///    If any step fails we revert to default outfit.
/// PARAMS:
///    mPed - the ped we want the random clothes combo for
///    bMustBeAvailable- does the player have to already own these items?
///    bOutfit - if true will only use full outfits, rather than setting random item combos
///    bSuitsOnly - if true and bOutfit is true, will only use full suit outfits
///    bAllowUnderwear - if false no underwear or bare chest allowed
///    bAllowProgrammer - only used for michael. if false no part of programmer outfit allowed
PROC SET_RANDOM_CLOTHES_COMBO(PED_INDEX mPed, BOOL bMustBeAvailable, BOOL bOutfit = FALSE, BOOL bSuitsOnly = FALSE, BOOL bAllowUnderwear = FALSE, BOOL bAllowProgrammer = TRUE)

	SHOP_COMPONENT_ITEMS_STRUCT sClothesData
	INT iPlayerPed = GET_CURRENT_PLAYER_PED_INT()
	MODEL_NAMES ePedModel= GET_PLAYER_PED_MODEL(INT_TO_ENUM(enumCharacterList, iPlayerPed))
	
	RESET_CLOTHES_DATA_STRUCT(sClothesData)
	
	IF DOES_ENTITY_EXIST(mPed)
		IF NOT IS_PED_INJURED(mPed)
			// Set the default outfit to start
			SET_PED_COMP_ITEM_CURRENT_SP(mPed, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
			
			// set random combination of components
			CPRINTLN(DEBUG_PED_COMP, "doing random clothes stuff now")
			
			IF bOutfit = TRUE
				// just pick a random outfit
				IF NOT SET_RANDOM_ITEM_OF_TYPE(mPed, COMP_TYPE_OUTFIT, sClothesData, iPlayerPed, ePedModel, bMustBeAvailable, bSuitsOnly, bAllowUnderwear)
					CPRINTLN(DEBUG_PED_COMP, "failed to set outfit, exit")
					EXIT
				ENDIF
			ELSE
			
				// random feet
				IF NOT SET_RANDOM_ITEM_OF_TYPE(mPed, COMP_TYPE_FEET, sClothesData, iPlayerPed, ePedModel, bMustBeAvailable, FALSE, bAllowUnderwear, bAllowProgrammer)
					CPRINTLN(DEBUG_PED_COMP, "failed to set feet, exit")
					EXIT
				ENDIF
				
				// random legs
				IF NOT SET_RANDOM_ITEM_OF_TYPE(mPed, COMP_TYPE_LEGS, sClothesData, iPlayerPed, ePedModel, bMustBeAvailable, FALSE, bAllowUnderwear, bAllowProgrammer)
					CPRINTLN(DEBUG_PED_COMP, "failed to set legs, exit")
					EXIT
				ENDIF
				
				// random top
				IF NOT SET_RANDOM_ITEM_OF_TYPE(mPed, COMP_TYPE_TORSO, sClothesData, iPlayerPed, ePedModel, bMustBeAvailable, FALSE, bAllowUnderwear, bAllowProgrammer)
					CPRINTLN(DEBUG_PED_COMP, "failed to set torso, exit")
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
