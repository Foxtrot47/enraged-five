USING "globals.sch"
USING "timer_public.sch"


//PROC MISSION_STAT_RESET_STAT_REGISTER(INT index)
//PROC ADD_DLC_ASSASSINATION_ENTRY(DLC_ASSASSINATION_STATCONFIG & config, DLC_ASSASSINATION_STATS stat, ENUM_MISSION_STAT_TYPES type,
//		BOOL bUpdateGlobalDetails)
//	IF config.iRegisteredStats = DLC_ASSASSINATION_MAX_STATS
//		EXIT
//	ENDIF
//	
//	INT i = config.iRegisteredStats
//	
//	SWITCH stat 
//		CASE MAX_DLC_MISSION_STATS
//			EXIT
//			BREAK
//		
//		DEFAULT
//			config.target[i] = stat
//			config.type[i] = type
//			config.StatCurrentValue[i] = 0
//			
//			INT j
//			REPEAT MAX_TRACKED_MISSION_STATS j
//				IF config.target[i] = INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[j].target))
//					CPRINTLN(DEBUG_MISSION_STATS, "Found matching tracked stat array at ", j, ".")
//					
//					config.invalidationReason[i] = g_MissionStatTrackingArray[j].invalidationReason
//				ENDIF
//			ENDREPEAT
//			
//			BREAK
//	ENDSWITCH
//	
//	IF bUpdateGlobalDetails
//		g_DLCMissionStatTrackingPrototypes[stat].type = type
//		g_DLCMissionStatTrackingPrototypes[stat].currentvalue = 0
//		
//		g_MissionStatTrackingArray[i].target = INT_TO_ENUM(ENUM_MISSION_STATS, ENUM_TO_INT(stat))
//		
//		g_MissionStatTrackingArray[i].ivalue = 0
//		g_MissionStatTrackingArray[i].fvalue = 0.0
//		
//		++g_iMissionStatsBeingTracked
//	ENDIF
//	
//	++config.iRegisteredStats
//ENDPROC


//PROC PREP_DLC_ASSASSINATION_STAT_CONFIG(DLC_ASSASSINATION_STATCONFIG & config, SP_MISSIONS assassinationMissionIndex, BOOL bUpdateGlobalDetails)
//	
//	DLC_ASSASSINATION_STATCONFIG dlcAssasinationStatConfig_blank
//	config = dlcAssasinationStatConfig_blank
//	
//	RESTART_TIMER_NOW(config.missionTimer)
//	
//	IF g_iMissionStatsBeingTracked != 0
//		CPRINTLN(DEBUG_MISSION_STATS, "PREP_DLC_ASSASSINATION_STAT_CONFIG - g_iMissionStatsBeingTracked is incorrectly set to ", g_iMissionStatsBeingTracked)
//		
//		g_iMissionStatsBeingTracked = 0
//	ENDIF
//	
//	INT j
//	REPEAT MAX_TRACKED_MISSION_STATS j
//		IF g_MissionStatTrackingArray[j].TrackingEntity != NULL
//			CPRINTLN(DEBUG_MISSION_STATS, "PREP_DLC_ASSASSINATION_STAT_CONFIG - g_MissionStatTrackingArray[", j, "].TrackingEntity ", NATIVE_TO_INT(g_MissionStatTrackingArray[j].TrackingEntity))
//			g_MissionStatTrackingArray[j].TrackingEntity = NULL
//		ENDIF
//	ENDREPEAT
//	
//	SWITCH(g_savedGlobalsDLC.iDLCAssassinationStage)
//		CASE 1
//			SWITCH assassinationMissionIndex
//				CASE SP_MISSION_CLF_ASS_POL		//assassin_police
//					config.dlcAssassinationIndex = 0
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_POL_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_POL_lawyered,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_POL_shanked,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_POL_loudExec,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_POL_unmarked,	MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_RET		//assassin_retrieve
//					config.dlcAssassinationIndex = 1
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_RET_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_RET_noManSta,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_RET_preForce,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_RET_sightsee,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_RET_carrier,	MISSION_STAT_TYPE_HEADSHOTS, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_CAB		//assassin_cablecar
//					config.dlcAssassinationIndex = 2
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_CAB_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_CAB_disconn,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_CAB_paraDr,		MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_CAB_lazyKill,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_CAB_tipTop,		MISSION_STAT_TYPE_WINDOWED_TIMER, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_CAB_outOfBat,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_GEN		//assassin_general
//					config.dlcAssassinationIndex = 3
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_GEN_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_GEN_hidPack,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_GEN_falseAl,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_GEN_overOut,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_GEN_miliHard,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_GEN_strMst,		MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_SUB		//assassin_subway
//					config.dlcAssassinationIndex = 4
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SUB_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SUB_StayOnTa,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SUB_ScaredOf,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SUB_Headhunt,	MISSION_STAT_TYPE_HEADSHOTS, bUpdateGlobalDetails)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 2
//			SWITCH assassinationMissionIndex
//				CASE SP_MISSION_CLF_ASS_DEA		//assassin_drugs
//					config.dlcAssassinationIndex = 5
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_DEA_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_DEA_untouch,	MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_DEA_noMercy,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_DEA_runningM,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_DEA_hearNoEv,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_DEA_demoliti,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_HEL		//assassin_helicopter
//					config.dlcAssassinationIndex = 6
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HEL_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HEL_flySwat,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HEL_headshot,	MISSION_STAT_TYPE_HEADSHOTS, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HEL_grounded,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HEL_untouch,	MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_BAR		//assassin_barber
//					config.dlcAssassinationIndex = 7
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_BAR_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_BAR_silence,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_BAR_OffTop,		MISSION_STAT_TYPE_HEADSHOTS, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_BAR_quartet,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_BAR_hotShave,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_STR		//assassin_street
//					config.dlcAssassinationIndex = 8
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_STR_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_STR_sticky,		MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_STR_pullOver,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_STR_driveByW,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_VIN		//assassin_vinyard
//					config.dlcAssassinationIndex = 9
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_VIN_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_VIN_EngineTr,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_VIN_RainingG,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_VIN_Capod,		MISSION_STAT_TYPE_HEADSHOTS, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_VIN_Accuracy,	MISSION_STAT_TYPE_ACCURACY, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_VIN_Unmark,		MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD, bUpdateGlobalDetails)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 3
//			SWITCH assassinationMissionIndex
//				CASE SP_MISSION_CLF_ASS_HNT		//assassin_hunter
//					config.dlcAssassinationIndex = 10
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HNT_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HNT_Scoped,		MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HNT_ProofKil,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HNT_Huntsman,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_HNT_UpClose,	MISSION_STAT_TYPE_UNIQUE_BOOL, bUpdateGlobalDetails)
//				BREAK
//				CASE SP_MISSION_CLF_ASS_SKY		//assassin_skydive
//					config.dlcAssassinationIndex = 11
//					
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SKY_time,		MISSION_STAT_TYPE_TOTALTIME, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SKY_RoughLan,	MISSION_STAT_TYPE_FRACTION, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SKY_OffCours,	MISSION_STAT_TYPE_FRACTION, bUpdateGlobalDetails)
//					ADD_DLC_ASSASSINATION_ENTRY(config, DAS_SKY_HeadTrau,	MISSION_STAT_TYPE_HEADSHOTS, bUpdateGlobalDetails)
//				BREAK
//				
//			ENDSWITCH
//		BREAK
//		
//		DEFAULT
//		BREAK
//	ENDSWITCH
//	
//	INT i
//	FOR i = (config.iRegisteredStats) TO (DLC_ASSASSINATION_MAX_STATS - 1)
//		config.target[i] = UNSET_DLC_MISSION_STAT_ENUM
//		config.StatCurrentValue[i] = -1
//	ENDFOR
//	
//	IF bUpdateGlobalDetails
//		FOR i = (config.iRegisteredStats) TO (MAX_TRACKED_MISSION_STATS - 1)
//			g_MissionStatTrackingArray[i].target = UNSET_MISSION_STAT_ENUM
//		ENDFOR
//	ENDIF
//	
//	CPRINTLN(DEBUG_MISSION_STATS, "prep dlc assassination stat config ", g_savedGlobalsDLC.iDLCAssassinationStage, " - ", config.dlcAssassinationIndex)
//	
//ENDPROC

//PROC MISSION_STAT_ADD_WATCH(ENUM_MISSION_STATS prototype)

//PROC INTERNAL_GENERATED_ASSOCIATE_STATS_WITH_FLOW()


//#IF IS_DEBUG_BUILD
//FUNC BOOL GET_TARGET_FOR_DLC_STAT(DLC_ASSASSINATION_STATS stat, INT &iTarget)
//	CONST_INT iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD_untouch	200
//	CONST_INT iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD_unmarked	150
//	
//	SWITCH stat
//		//	=Police Station Assassination=
//		CASE DAS_POL_lawyered			iTarget = 0 RETURN TRUE
//		CASE DAS_POL_shanked			iTarget = 0 RETURN TRUE
//		CASE DAS_POL_loudExec			iTarget = 0 RETURN TRUE
//		CASE DAS_POL_unmarked			iTarget = iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD_unmarked RETURN TRUE
//		CASE DAS_POL_time				iTarget = 170000	RETURN TRUE		//Complete within 02:50
//		
//		//	=Helicopter Takedown=
//		CASE DAS_HEL_flySwat			iTarget = 1		RETURN TRUE
//		CASE DAS_HEL_headshot			iTarget = 1		RETURN TRUE
//		CASE DAS_HEL_grounded			iTarget = 1		RETURN TRUE
//		CASE DAS_HEL_untouch			iTarget = iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD_untouch		RETURN TRUE
//		CASE DAS_HEL_time				iTarget = 170000	RETURN TRUE		//Complete within 02:50
//		
//		//	=General Murder=
//		CASE DAS_GEN_hidPack			iTarget = 1		RETURN TRUE
//		CASE DAS_GEN_falseAl			iTarget = 1		RETURN TRUE
//		CASE DAS_GEN_overOut			iTarget = 1		RETURN TRUE
//		CASE DAS_GEN_miliHard			iTarget = 1		RETURN TRUE
//		CASE DAS_GEN_strMst				iTarget = 1		RETURN TRUE
//		CASE DAS_GEN_time				iTarget = 360000	RETURN TRUE		//Complete within 06:00
//		
//		//	=Subway Sacrifice=
//		CASE DAS_SUB_StayOnTa			iTarget = 1		RETURN TRUE
//		CASE DAS_SUB_ScaredOf			iTarget = 1		RETURN TRUE
//		CASE DAS_SUB_Headhunt			iTarget = 1		RETURN TRUE
//		CASE DAS_SUB_time				iTarget = 370000	RETURN TRUE		//Complete within 06:10
//		
//		//	=Drugs Kill=
//		CASE DAS_DEA_time				iTarget = 140000	RETURN TRUE		//Complete within 02:20 
//		CASE DAS_DEA_untouch			iTarget = iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD_untouch		RETURN TRUE
//		CASE DAS_DEA_noMercy			iTarget = 1		RETURN TRUE
//		CASE DAS_DEA_runningM			iTarget = 1		RETURN TRUE
//		CASE DAS_DEA_hearNoEv			iTarget = 1		RETURN TRUE
//		CASE DAS_DEA_demoliti			iTarget = 1		RETURN TRUE
//		
//		//	=Kill and Retrieve=
//		CASE DAS_RET_noManSta			iTarget = 1		RETURN TRUE
//		CASE DAS_RET_preForce			iTarget = 1		RETURN TRUE
//		CASE DAS_RET_sightsee			iTarget = 1		RETURN TRUE
//		CASE DAS_RET_carrier			iTarget = 1		RETURN TRUE
//		CASE DAS_RET_time				iTarget = 400000	RETURN TRUE		//Complete within 06:40
//		
//		//	=Barber Shop Chop (mission doesn’t work currently)=
//		CASE DAS_BAR_silence			iTarget = 1		RETURN TRUE
//		CASE DAS_BAR_OffTop				iTarget = 1		RETURN TRUE
//		CASE DAS_BAR_quartet			iTarget = 1		RETURN TRUE
//		CASE DAS_BAR_hotShave			iTarget = 1		RETURN TRUE
//		CASE DAS_BAR_time				iTarget = 080000	RETURN TRUE		//Complete within 01:20 
//		
//		//	=Street Race Assassination=
//		CASE DAS_STR_sticky				iTarget = 1		RETURN TRUE
//		CASE DAS_STR_pullOver			iTarget = 1		RETURN TRUE
//		CASE DAS_STR_driveByW			iTarget = 1		RETURN TRUE
//		CASE DAS_STR_time				iTarget = 190000	RETURN TRUE		//Complete within 03:10
//		
//		//	=Cable Car=
//		CASE DAS_CAB_disconn			iTarget = 1		RETURN TRUE
//		CASE DAS_CAB_paraDr				iTarget = 1		RETURN TRUE
//		CASE DAS_CAB_lazyKill			iTarget = 1		RETURN TRUE
//		CASE DAS_CAB_tipTop				iTarget = 210000	RETURN TRUE		//Complete within 03:30
//		CASE DAS_CAB_outOfBat			iTarget = 1		RETURN TRUE
//		CASE DAS_CAB_time				iTarget = 195000	RETURN TRUE		//Complete within 03:15
//		
//		//	=Vineyard Killing=
//		CASE DAS_VIN_EngineTr			iTarget = 1		RETURN TRUE
//		CASE DAS_VIN_RainingG			iTarget = 1		RETURN TRUE
//		CASE DAS_VIN_Capod				iTarget = 2		RETURN TRUE
//		CASE DAS_VIN_Accuracy			iTarget = 100	RETURN TRUE
//		CASE DAS_VIN_Unmark				iTarget = iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD_unmarked		RETURN TRUE
//		CASE DAS_VIN_time				iTarget = 110000	RETURN TRUE		//Complete within 01:50
//		
//		//	=The Hunt=
//		CASE DAS_HNT_Scoped				iTarget = 1		RETURN TRUE
//		CASE DAS_HNT_ProofKil			iTarget = 1		RETURN TRUE
//		CASE DAS_HNT_Huntsman			iTarget = 1		RETURN TRUE
//		CASE DAS_HNT_UpClose			iTarget = 1		RETURN TRUE
//		CASE DAS_HNT_time				iTarget = 160000	RETURN TRUE		//Complete within 02:40
//		
//		//	=Skydive Death=
//		CASE DAS_SKY_RoughLan			iTarget = 3		RETURN TRUE
//		CASE DAS_SKY_OffCours			iTarget = 3		RETURN TRUE
//		CASE DAS_SKY_HeadTrau			iTarget = 1		RETURN TRUE
//		CASE DAS_SKY_time				iTarget = 90000	RETURN TRUE		//Complete within 01:30
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC
//#ENDIF
//
//PROC INTERNAL_GENERATED_DLC_MISSION_STAT_CONFIGURE_TYPES_AND_VALUES_FOR_INDEX(DLC_ASSASSINATION_STATS index, MissionStatInfo &tar)
//	SWITCH(index)
//	CASE DAS_POL_lawyered
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_POL_lawyered
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_shanked
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_POL_shanked
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_unmarked
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 60
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_POL_unmarked
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_loudExec
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_POL_loudExec
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 170000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_POL_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_hotShave
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_BAR_hotShave
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_RoughLan
//		tar.type = MISSION_STAT_TYPE_FRACTION
//		tar.currentvalue = 0
//		tar.success_threshold = 3
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_SKY_RoughLan
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_StayOnTa
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_SUB_StayOnTa
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_overOut
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_GEN_overOut
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_strMst
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_GEN_strMst
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 360000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_GEN_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_falseAl
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_GEN_falseAl
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_flySwat
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HEL_flySwat
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_hidPack
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_GEN_hidPack
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_headshot
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HEL_headshot
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 170000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_HEL_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_grounded
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HEL_grounded
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_untouch
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_HEL_untouch
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_hearNoEv
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_DEA_hearNoEv
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_sightsee
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_RET_sightsee
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_noMercy
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_DEA_noMercy
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_preForce
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_RET_preForce
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_ScaredOf
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_SUB_ScaredOf
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_demoliti
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_DEA_demoliti
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_Headhunt
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_SUB_Headhunt
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 370000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_SUB_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 140000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_DEA_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_runningM
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_DEA_runningM
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_untouch
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_DEA_untouch
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 400000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_RET_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	ENDSWITCH
//	SWITCH(index)
//	CASE DAS_BAR_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 80000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_BAR_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_carrier
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_RET_carrier
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_noManSta
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_RET_noManSta
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 90000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_SKY_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_quartet
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_BAR_quartet
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_silence
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_BAR_silence
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_OffTop
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_BAR_OffTop
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_Accuracy
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 100
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_VIN_Accuracy
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_Capod
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 3
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_VIN_Capod
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_outOfBat
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_CAB_outOfBat
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 195000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_CAB_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_pullOver
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_STR_pullOver
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_RainingG
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_VIN_RainingG
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_disconn
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_CAB_disconn
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_sticky
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_STR_sticky
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_lazyKill
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_CAB_lazyKill
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_time
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 190000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_STR_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_tipTop
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 210000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_CAB_tipTop
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_paraDr
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_CAB_paraDr
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_driveByW
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_STR_driveByW
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_Huntsman
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HNT_Huntsman
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_EngineTr
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_VIN_EngineTr
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_Unmark
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 200
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_VIN_Unmark
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_ProofKil
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HNT_ProofKil
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_UpClose
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HNT_UpClose
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 110000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_VIN_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_time
//		tar.type = MISSION_STAT_TYPE_TOTALTIME
//		tar.currentvalue = 0
//		tar.success_threshold = 160000
//		tar.less_than_threshold = True
//		tar.statname = MS_DAS_HNT_time
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_HeadTrau
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_SKY_HeadTrau
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_Scoped
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_HNT_Scoped
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_OffCours
//		tar.type = MISSION_STAT_TYPE_FRACTION
//		tar.currentvalue = 0
//		tar.success_threshold = 3
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_SKY_OffCours
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_miliHard
//		tar.type = MISSION_STAT_TYPE_UNIQUE_BOOL
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.statname = MS_DAS_GEN_miliHard
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = False
//		tar.lb_differentiator = False
//		tar.lb_weight_PPC = 1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	ENDSWITCH
//	SWITCH(index)
//	CASE DAS_BAR_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_BAR_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_POL_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_RET_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	ENDSWITCH
//	SWITCH(index)
//	CASE DAS_CAB_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_CAB_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_GEN_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SUB_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	ENDSWITCH
//	SWITCH(index)
//	CASE DAS_DEA_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_DEA_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HEL_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_STR_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	ENDSWITCH
//	SWITCH(index)
//	CASE DAS_VIN_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_VIN_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_HNT_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_ACCURACY
//		tar.type = MISSION_STAT_TYPE_ACCURACY
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 250
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_BULLETS_FIRED
//		tar.type = MISSION_STAT_TYPE_BULLETS_FIRED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -100
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_CAR_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_COP_LOSS_TIME
//		tar.type = MISSION_STAT_TYPE_WINDOWED_TIMER
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_HEADSHOTS
//		tar.type = MISSION_STAT_TYPE_HEADSHOTS
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 5000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_HEALTH_DAMAGE
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -1000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_INNOCENTS_KILLED
//		tar.type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -10000
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_KILLS
//		tar.type = MISSION_STAT_TYPE_PURE_COUNT
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 2500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_MAX_SPEED
//		tar.type = MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = 500
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	CASE DAS_SKY_SPECIAL_ABILITY_TIME
//		tar.type = MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
//		tar.currentvalue = 0
//		tar.success_threshold = 1
//		tar.less_than_threshold = False
//		tar.MinRange = 0
//		tar.MaxRange = 1
//		tar.bHidden = True
//		tar.lb_differentiator = True
//		tar.lb_weight_PPC = -3
//		tar.lb_min_legal = 0
//		tar.lb_max_legal = 1
//		tar.lb_precedence = 1
//	EXIT
//	ENDSWITCH
//ENDPROC
//
//
//#IF IS_DEBUG_BUILD
//FUNC STRING GET_DLC_MISSION_STAT_DEBUG_NAME(DLC_ASSASSINATION_STATS e)
//	SWITCH(e)
//		CASE DAS_POL_lawyered
//			RETURN "DAS_POL_lawyered"
//		CASE DAS_POL_shanked
//			RETURN "DAS_POL_shanked"
//		CASE DAS_POL_unmarked
//			RETURN "DAS_POL_unmarked"
//		CASE DAS_POL_loudExec
//			RETURN "DAS_POL_loudExec"
//		CASE DAS_POL_time
//			RETURN "DAS_POL_time"
//		CASE DAS_BAR_hotShave
//			RETURN "DAS_BAR_hotShave"
//		CASE DAS_SKY_RoughLan
//			RETURN "DAS_SKY_RoughLan"
//		CASE DAS_SUB_StayOnTa
//			RETURN "DAS_SUB_StayOnTa"
//		CASE DAS_GEN_overOut
//			RETURN "DAS_GEN_overOut"
//		CASE DAS_GEN_strMst
//			RETURN "DAS_GEN_strMst"
//		CASE DAS_GEN_time
//			RETURN "DAS_GEN_time"
//		CASE DAS_GEN_falseAl
//			RETURN "DAS_GEN_falseAl"
//		CASE DAS_HEL_flySwat
//			RETURN "DAS_HEL_flySwat"
//		CASE DAS_GEN_hidPack
//			RETURN "DAS_GEN_hidPack"
//		CASE DAS_HEL_headshot
//			RETURN "DAS_HEL_headshot"
//		CASE DAS_HEL_time
//			RETURN "DAS_HEL_time"
//		CASE DAS_HEL_grounded
//			RETURN "DAS_HEL_grounded"
//		CASE DAS_HEL_untouch
//			RETURN "DAS_HEL_untouch"
//		CASE DAS_DEA_hearNoEv
//			RETURN "DAS_DEA_hearNoEv"
//		CASE DAS_RET_sightsee
//			RETURN "DAS_RET_sightsee"
//		CASE DAS_DEA_noMercy
//			RETURN "DAS_DEA_noMercy"
//		CASE DAS_RET_preForce
//			RETURN "DAS_RET_preForce"
//		CASE DAS_SUB_ScaredOf
//			RETURN "DAS_SUB_ScaredOf"
//		CASE DAS_DEA_demoliti
//			RETURN "DAS_DEA_demoliti"
//		CASE DAS_SUB_Headhunt
//			RETURN "DAS_SUB_Headhunt"
//		CASE DAS_SUB_time
//			RETURN "DAS_SUB_time"
//		CASE DAS_DEA_time
//			RETURN "DAS_DEA_time"
//		CASE DAS_DEA_runningM
//			RETURN "DAS_DEA_runningM"
//		CASE DAS_DEA_untouch
//			RETURN "DAS_DEA_untouch"
//		CASE DAS_RET_time
//			RETURN "DAS_RET_time"
//	ENDSWITCH
//	SWITCH(e)
//		CASE DAS_BAR_time
//			RETURN "DAS_BAR_time"
//		CASE DAS_RET_carrier
//			RETURN "DAS_RET_carrier"
//		CASE DAS_RET_noManSta
//			RETURN "DAS_RET_noManSta"
//		CASE DAS_SKY_time
//			RETURN "DAS_SKY_time"
//		CASE DAS_BAR_quartet
//			RETURN "DAS_BAR_quartet"
//		CASE DAS_BAR_silence
//			RETURN "DAS_BAR_silence"
//		CASE DAS_BAR_OffTop
//			RETURN "DAS_BAR_OffTop"
//		CASE DAS_VIN_Accuracy
//			RETURN "DAS_VIN_Accuracy"
//		CASE DAS_VIN_Capod
//			RETURN "DAS_VIN_Capod"
//		CASE DAS_CAB_outOfBat
//			RETURN "DAS_CAB_outOfBat"
//		CASE DAS_CAB_time
//			RETURN "DAS_CAB_time"
//		CASE DAS_STR_pullOver
//			RETURN "DAS_STR_pullOver"
//		CASE DAS_VIN_RainingG
//			RETURN "DAS_VIN_RainingG"
//		CASE DAS_CAB_disconn
//			RETURN "DAS_CAB_disconn"
//		CASE DAS_STR_sticky
//			RETURN "DAS_STR_sticky"
//		CASE DAS_CAB_lazyKill
//			RETURN "DAS_CAB_lazyKill"
//		CASE DAS_STR_time
//			RETURN "DAS_STR_time"
//		CASE DAS_CAB_tipTop
//			RETURN "DAS_CAB_tipTop"
//		CASE DAS_CAB_paraDr
//			RETURN "DAS_CAB_paraDr"
//		CASE DAS_STR_driveByW
//			RETURN "DAS_STR_driveByW"
//		CASE DAS_HNT_Huntsman
//			RETURN "DAS_HNT_Huntsman"
//		CASE DAS_VIN_EngineTr
//			RETURN "DAS_VIN_EngineTr"
//		CASE DAS_VIN_Unmark
//			RETURN "DAS_VIN_Unmark"
//		CASE DAS_HNT_ProofKil
//			RETURN "DAS_HNT_ProofKil"
//		CASE DAS_HNT_UpClose
//			RETURN "DAS_HNT_UpClose"
//		CASE DAS_VIN_time
//			RETURN "DAS_VIN_time"
//		CASE DAS_HNT_time
//			RETURN "DAS_HNT_time"
//		CASE DAS_SKY_HeadTrau
//			RETURN "DAS_SKY_HeadTrau"
//		CASE DAS_HNT_Scoped
//			RETURN "DAS_HNT_Scoped"
//		CASE DAS_SKY_OffCours
//			RETURN "DAS_SKY_OffCours"
//		CASE DAS_GEN_miliHard
//			RETURN "DAS_GEN_miliHard"
//	ENDSWITCH
//	
//	TEXT_LABEL_63 str = "DAS_XXX_"
//	str += ENUM_TO_INT(e)
//	CERRORLN(DEBUG_MISSION_STATS, "GET_DLC_MISSION_STAT_DEBUG_NAME : Mission stat \"", str, "\" with no string found!")
//RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
//ENDFUNC
//#ENDIF
//
//FUNC STRING GET_DLC_MISSION_STAT_NAME(DLC_ASSASSINATION_STATS e)
//	SWITCH(e)
//		CASE DAS_POL_lawyered 
//			RETURN "MIASSTA_0" 
//		CASE DAS_POL_shanked 
//			RETURN "MIASSTA_1" 
//		CASE DAS_POL_unmarked 
//			RETURN "MIASSTA_2" 
//		CASE DAS_POL_loudExec 
//			RETURN "MIASSTA_3" 
//		CASE DAS_POL_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_BAR_hotShave 
//			RETURN "MIASSTA_5" 
//		CASE DAS_SKY_RoughLan 
//			RETURN "MIASSTA_6" 
//		CASE DAS_SUB_StayOnTa 
//			RETURN "MIASSTA_7" 
//		CASE DAS_GEN_overOut 
//			RETURN "MIASSTA_8" 
//		CASE DAS_GEN_strMst 
//			RETURN "MIASSTA_9" 
//		CASE DAS_GEN_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_GEN_falseAl 
//			RETURN "MIASSTA_10" 
//		CASE DAS_HEL_flySwat 
//			RETURN "MIASSTA_11" 
//		CASE DAS_GEN_hidPack 
//			RETURN "MIASSTA_12" 
//		CASE DAS_HEL_headshot 
//			RETURN "MIASSTA_13" 
//		CASE DAS_HEL_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_HEL_grounded 
//			RETURN "MIASSTA_14" 
//		CASE DAS_HEL_untouch 
//			RETURN "MIASSTA_15" 
//		CASE DAS_DEA_hearNoEv 
//			RETURN "MIASSTA_16" 
//		CASE DAS_RET_sightsee 
//			RETURN "MIASSTA_17" 
//		CASE DAS_DEA_noMercy 
//			RETURN "MIASSTA_18" 
//		CASE DAS_RET_preForce 
//			RETURN "MIASSTA_19" 
//		CASE DAS_SUB_ScaredOf 
//			RETURN "MIASSTA_20" 
//		CASE DAS_DEA_demoliti 
//			RETURN "MIASSTA_21" 
//		CASE DAS_SUB_Headhunt 
//			RETURN "MIASSTA_22" 
//		CASE DAS_SUB_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_DEA_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_DEA_runningM 
//			RETURN "MIASSTA_23" 
//		CASE DAS_DEA_untouch 
//			RETURN "MIASSTA_2" 
//		CASE DAS_RET_time 
//			RETURN "MIASSTA_4" 
//	ENDSWITCH
//	SWITCH(e)
//		CASE DAS_BAR_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_RET_carrier 
//			RETURN "MIASSTA_24" 
//		CASE DAS_RET_noManSta 
//			RETURN "MIASSTA_25" 
//		CASE DAS_SKY_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_BAR_quartet 
//			RETURN "MIASSTA_26" 
//		CASE DAS_BAR_silence 
//			RETURN "MIASSTA_27" 
//		CASE DAS_BAR_OffTop 
//			RETURN "MIASSTA_28" 
//		CASE DAS_VIN_Accuracy 
//			RETURN "MIASSTA_29" 
//		CASE DAS_VIN_Capod 
//			RETURN "MIASSTA_30" 
//		CASE DAS_CAB_outOfBat 
//			RETURN "MIASSTA_31" 
//		CASE DAS_CAB_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_STR_pullOver 
//			RETURN "MIASSTA_32" 
//		CASE DAS_VIN_RainingG 
//			RETURN "MIASSTA_33" 
//		CASE DAS_CAB_disconn 
//			RETURN "MIASSTA_34" 
//		CASE DAS_STR_sticky 
//			RETURN "MIASSTA_35" 
//		CASE DAS_CAB_lazyKill 
//			RETURN "MIASSTA_36" 
//		CASE DAS_STR_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_CAB_tipTop 
//			RETURN "MIASSTA_37" 
//		CASE DAS_CAB_paraDr 
//			RETURN "MIASSTA_38" 
//		CASE DAS_STR_driveByW 
//			RETURN "MIASSTA_39" 
//		CASE DAS_HNT_Huntsman 
//			RETURN "MIASSTA_40" 
//		CASE DAS_VIN_EngineTr 
//			RETURN "MIASSTA_41" 
//		CASE DAS_VIN_Unmark 
//			RETURN "MIASSTA_15" 
//		CASE DAS_HNT_ProofKil 
//			RETURN "MIASSTA_42" 
//		CASE DAS_HNT_UpClose 
//			RETURN "MIASSTA_43" 
//		CASE DAS_VIN_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_HNT_time 
//			RETURN "MIASSTA_4" 
//		CASE DAS_SKY_HeadTrau 
//			RETURN "MIASSTA_44" 
//		CASE DAS_HNT_Scoped 
//			RETURN "MIASSTA_45" 
//		CASE DAS_SKY_OffCours 
//			RETURN "MIASSTA_46" 
//		CASE DAS_GEN_miliHard 
//			RETURN "MIASSTA_47" 
//	ENDSWITCH
//	SCRIPT_ASSERT("GET_DLC_MISSION_STAT_NAME : Mission stat with no string found!")
//RETURN "MISSING_DLC_MISSION_STAT_NAME"
//ENDFUNC
//
//FUNC STRING GET_DLC_MISSION_STAT_DESCRIPTION(DLC_ASSASSINATION_STATS e)
//	SWITCH(e)
//	CASE DAS_POL_lawyered
//		RETURN "MIASSTD0"
//	CASE DAS_POL_shanked
//		RETURN "MIASSTD1"
//	CASE DAS_POL_unmarked
//		RETURN "MIASSTD2"
//	CASE DAS_POL_loudExec
//		RETURN "MIASSTD3"
//	CASE DAS_POL_time
//		RETURN "MIASSTD4"
//	CASE DAS_BAR_hotShave
//		RETURN "MIASSTD5"
//	CASE DAS_SKY_RoughLan
//		RETURN "MIASSTD6"
//	CASE DAS_SUB_StayOnTa
//		RETURN "MIASSTD7"
//	CASE DAS_GEN_overOut
//		RETURN "MIASSTD8"
//	CASE DAS_GEN_strMst
//		RETURN "MIASSTD9"
//	CASE DAS_GEN_time
//		RETURN "MIASSTD10"
//	CASE DAS_GEN_falseAl
//		RETURN "MIASSTD11"
//	CASE DAS_HEL_flySwat
//		RETURN "MIASSTD12"
//	CASE DAS_GEN_hidPack
//		RETURN "MIASSTD13"
//	CASE DAS_HEL_headshot
//		RETURN "MIASSTD14"
//	CASE DAS_HEL_time
//		RETURN "MIASSTD4"
//	CASE DAS_HEL_grounded
//		RETURN "MIASSTD16"
//	CASE DAS_HEL_untouch
//		RETURN "MIASSTD17"
//	CASE DAS_DEA_hearNoEv
//		RETURN "MIASSTD18"
//	CASE DAS_RET_sightsee
//		RETURN "MIASSTD19"
//	ENDSWITCH
//	SWITCH(e)
//	CASE DAS_DEA_noMercy
//		RETURN "MIASSTD20"
//	CASE DAS_RET_preForce
//		RETURN "MIASSTD21"
//	CASE DAS_SUB_ScaredOf
//		RETURN "MIASSTD22"
//	CASE DAS_DEA_demoliti
//		RETURN "MIASSTD23"
//	CASE DAS_SUB_Headhunt
//		RETURN "MIASSTD24"
//	CASE DAS_SUB_time
//		RETURN "MIASSTD25"
//	CASE DAS_DEA_time
//		RETURN "MIASSTD26"
//	CASE DAS_DEA_runningM
//		RETURN "MIASSTD27"
//	CASE DAS_DEA_untouch
//		RETURN "MIASSTD2"
//	CASE DAS_RET_time
//		RETURN "MIASSTD29"
//	CASE DAS_BAR_time
//		RETURN "MIASSTD30"
//	CASE DAS_RET_carrier
//		RETURN "MIASSTD31"
//	CASE DAS_RET_noManSta
//		RETURN "MIASSTD32"
//	CASE DAS_SKY_time
//		RETURN "MIASSTD33"
//	CASE DAS_BAR_quartet
//		RETURN "MIASSTD34"
//	CASE DAS_BAR_silence
//		RETURN "MIASSTD35"
//	CASE DAS_BAR_OffTop
//		RETURN "MIASSTD36"
//	CASE DAS_VIN_Accuracy
//		RETURN "MIASSTD37"
//	CASE DAS_VIN_Capod
//		RETURN "MIASSTD38"
//	ENDSWITCH
//	SWITCH(e)
//	CASE DAS_CAB_outOfBat
//		RETURN "MIASSTD39"
//	CASE DAS_CAB_time
//		RETURN "MIASSTD40"
//	CASE DAS_STR_pullOver
//		RETURN "MIASSTD41"
//	CASE DAS_VIN_RainingG
//		RETURN "MIASSTD42"
//	CASE DAS_CAB_disconn
//		RETURN "MIASSTD43"
//	CASE DAS_STR_sticky
//		RETURN "MIASSTD44"
//	CASE DAS_CAB_lazyKill
//		RETURN "MIASSTD45"
//	CASE DAS_STR_time
//		RETURN "MIASSTD46"
//	CASE DAS_CAB_tipTop
//		RETURN "MIASSTD47"
//	CASE DAS_CAB_paraDr
//		RETURN "MIASSTD48"
//	CASE DAS_STR_driveByW
//		RETURN "MIASSTD49"
//	CASE DAS_HNT_Huntsman
//		RETURN "MIASSTD50"
//	CASE DAS_VIN_EngineTr
//		RETURN "MIASSTD51"
//	CASE DAS_VIN_Unmark
//		RETURN "MIASSTD17"
//	CASE DAS_HNT_ProofKil
//		RETURN "MIASSTD53"
//	CASE DAS_HNT_UpClose
//		RETURN "MIASSTD54"
//	CASE DAS_VIN_time
//		RETURN "MIASSTD55"
//	CASE DAS_HNT_time
//		RETURN "MIASSTD56"
//	CASE DAS_SKY_HeadTrau
//		RETURN "MIASSTD57"
//	ENDSWITCH
//	SWITCH(e)
//	CASE DAS_HNT_Scoped
//		RETURN "MIASSTD58"
//	CASE DAS_SKY_OffCours
//		RETURN "MIASSTD59"
//	CASE DAS_GEN_miliHard
//		RETURN "MIASSTD60"
//	ENDSWITCH
//	
//	DEBUG_PRINTCALLSTACK()
//	SCRIPT_ASSERT("GET_DLC_MISSION_STAT_DESCRIPTION : Mission stat with no description found!")
//RETURN "MISSING_DLC_MISSION_STAT_DESCRIPTION"
//ENDFUNC
//
//
//PROC DLC_MISSION_STAT_ADD_WATCH(DLC_ASSASSINATION_STATS prototype)
//	g_bMissionStatSystemPrimed = TRUE
//	g_bMissionStatSystemMissionStarted = TRUE
//
//	 IF g_iMissionStatsBeingTracked > (MAX_TRACKED_MISSION_STATS-1)
//		SCRIPT_ASSERT("DLC_MISSION_STAT_ADD_WATCH: Attempted to register new watch stats when max was already reached")
//		EXIT
//	ENDIF
//	MISSION_STAT_RESET_STAT_REGISTER(g_iMissionStatsBeingTracked)
//	g_MissionStatTrackingArray[g_iMissionStatsBeingTracked].target = INT_TO_ENUM(ENUM_MISSION_STATS, ENUM_TO_INT(prototype))
//	++g_iMissionStatsBeingTracked
//	IF g_DLCMissionStatTrackingPrototypes[prototype].type = MISSION_STAT_TYPE_INNOCENTS_KILLED
//	g_bTrackingInnocentsLogged = TRUE
//	ENDIF
//ENDPROC
//
////PROC INTERNAL_FLOW_PRIME_STATS_FOR_MISSION(SP_MISSIONS m)
//PROC INTERNAL_FLOW_PRIME_STATS_FOR_DLC_MISSION(SP_MISSIONS m)
//	SWITCH(m)
//		CASE SP_MISSION_CLF_ASS_POL		//assassin_police
//			DLC_MISSION_STAT_ADD_WATCH(DAS_POL_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_POL_lawyered)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_POL_shanked)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_POL_loudExec)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_POL_unmarked)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_RET		//assassin_retrieve
//			DLC_MISSION_STAT_ADD_WATCH(DAS_RET_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_RET_noManSta)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_RET_preForce)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_RET_sightsee)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_RET_carrier)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_CAB		//assassin_cablecar
//			DLC_MISSION_STAT_ADD_WATCH(DAS_CAB_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_CAB_disconn)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_CAB_paraDr)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_CAB_lazyKill)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_CAB_tipTop)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_CAB_outOfBat)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_GEN		//assassin_general
//			DLC_MISSION_STAT_ADD_WATCH(DAS_GEN_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_GEN_hidPack)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_GEN_falseAl)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_GEN_overOut)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_GEN_miliHard)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_GEN_strMst)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_SUB		//assassin_subway
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SUB_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SUB_StayOnTa)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SUB_ScaredOf)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SUB_Headhunt)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_DEA		//assassin_drugs
//			DLC_MISSION_STAT_ADD_WATCH(DAS_DEA_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_DEA_untouch)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_DEA_noMercy)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_DEA_runningM)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_DEA_hearNoEv)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_DEA_demoliti)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_HEL		//assassin_helicopter
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HEL_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HEL_flySwat)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HEL_headshot)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HEL_grounded)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HEL_untouch)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_BAR		//assassin_barber
//			DLC_MISSION_STAT_ADD_WATCH(DAS_BAR_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_BAR_silence)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_BAR_OffTop)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_BAR_quartet)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_BAR_hotShave)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_STR		//assassin_street
//			DLC_MISSION_STAT_ADD_WATCH(DAS_STR_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_STR_sticky)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_STR_pullOver)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_STR_driveByW)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_VIN		//assassin_vinyard
//			DLC_MISSION_STAT_ADD_WATCH(DAS_VIN_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_VIN_EngineTr)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_VIN_RainingG)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_VIN_Capod)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_VIN_Accuracy)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_VIN_Unmark)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_HNT		//assassin_hunter
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HNT_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HNT_Scoped)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HNT_ProofKil)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HNT_Huntsman)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_HNT_UpClose)
//			EXIT
//		BREAK
//		CASE SP_MISSION_CLF_ASS_SKY		//assassin_skydive
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SKY_time)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SKY_RoughLan)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SKY_OffCours)
//			DLC_MISSION_STAT_ADD_WATCH(DAS_SKY_HeadTrau)
//			EXIT
//		BREAK
//		
//		CASE SP_MISSION_CLF_JET_1
//			EXIT
//		BREAK
//		
//		CASE SP_MISSION_CLF_JET_2
//			EXIT
//		BREAK
//		
//		CASE SP_MISSION_CLF_JET_3
//			EXIT
//		BREAK
//		
//		CASE SP_MISSION_CLF_JET_REP
//			EXIT
//		BREAK
//		
//	ENDSWITCH
//	
//	CASSERTLN(DEBUG_MISSION_STATS, "INTERNAL_FLOW_PRIME_STATS_FOR_DLC_MISSION : Mission has no stats registered, if this is intentional please inform Default Levels. so he can add this mission to the stat exclusion list. Thanks!")
//ENDPROC
//
////PROC INTERNAL_GENERATED_MISSION_STAT_CONFIGURE_TYPES_AND_VALUES_FOR_INDEX(ENUM_MISSION_STATS index, MissionStatInfo &tar)
