USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
#if USE_CLF_DLC
USING "player_scene_scheduleCLF.sch"	
#endif
#if  USE_NRM_DLC
USING "player_scene_scheduleNRM.sch"
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "player_scene_schedule.sch"
#endif
#endif
USING "prostitute_public.sch"

#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

CONST_INT iMinTimeBetweenLaunches 150000//10000
CONST_FLOAT fREMinDistanceRequiredForSpawn 75.0

VECTOR vThisWorldPointLocation

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	random_event_private.sch
//		AUTHOR			:	Paul Davies
//		DESCRIPTION		:	Handles private random event functions for timing checks
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

FUNC TEXT_LABEL_63 TIMEOFDAY_TO_TEXT_LABEL_63(TIMEOFDAY &tod)

	// Construct a texl label containing the time game datetime we are waiting for.
	TEXT_LABEL_63 tDateTime = ""
	INT iTempInt = GET_TIMEOFDAY_HOUR(tod)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += ":"
	
	iTempInt = GET_TIMEOFDAY_MINUTE(tod)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += ":"
	
	iTempInt = GET_TIMEOFDAY_SECOND(tod)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += "  "
	
	iTempInt = GET_TIMEOFDAY_DAY(tod)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += "/"
	
	iTempInt = ENUM_TO_INT(GET_TIMEOFDAY_MONTH(tod))
	IF iTempInt < 9
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt + 1
	tDateTime += "/"
	tDateTime += GET_TIMEOFDAY_YEAR(tod)
	
	RETURN tDateTime
	
ENDFUNC


PROC SET_RANDOM_EVENT_VARIATION_COMPLETE(SP_RANDOM_EVENTS eRandomEvent, INT iVariation)
	SET_BIT(g_savedGlobals.sRandomEventData.iREVariationComplete[eRandomEvent], iVariation)
ENDPROC

PROC SET_RANDOM_EVENT_TIME_FOR_NEXT_LAUNCH(SP_RANDOM_EVENTS eRandomEvent, TIMEOFDAY &todUnlockTime)
	g_savedGlobals.sRandomEventData.eTimeBlockUntil[eRandomEvent] = todUnlockTime
ENDPROC

/// PURPOSE:
///    Gets the time allowed between two random events triggering
/// PARAMS:
///    eRandomEvent - The random event to query.
/// RETURNS:
///    The time in hours allowed between each instance of the same random event
///    
FUNC INT GET_RANDOM_EVENT_BLOCKING_TIME(SP_RANDOM_EVENTS eRandomEvent)
	INT iReturnBlockingTime
	SWITCH eRandomEvent
		//blocking times for random events, please change them here.
		CASE RE_ABANDONEDCAR		iReturnBlockingTime = 30	BREAK
		CASE RE_ACCIDENT			iReturnBlockingTime = 30	BREAK
		CASE RE_ARREST   			iReturnBlockingTime = 30	BREAK
		CASE RE_ATMROBBERY			iReturnBlockingTime = 200	BREAK
		CASE RE_BIKETHIEF			iReturnBlockingTime = 30	BREAK
		CASE RE_BIKETHIEFSTAMP   	iReturnBlockingTime = 30	BREAK
		CASE RE_BORDERPATROL		iReturnBlockingTime = 30	BREAK
		CASE RE_BURIAL   			iReturnBlockingTime = 30	BREAK
		CASE RE_BUSTOUR  			iReturnBlockingTime = 0		BREAK
		CASE RE_CARTHEFT   			iReturnBlockingTime = 30	BREAK
		CASE RE_CHASETHIEVES   		iReturnBlockingTime = 30	BREAK
		CASE RE_CRASHRESCUE   		iReturnBlockingTime = 30	BREAK
		CASE RE_CULTSHOOTOUT		iReturnBlockingTime = 30	BREAK
		CASE RE_DEALGONEWRONG   	iReturnBlockingTime = 120	BREAK
		CASE RE_DOMESTIC   			iReturnBlockingTime = 60	BREAK
		CASE RE_DRUNKDRIVER			iReturnBlockingTime = 60	BREAK
		CASE RE_GANGFIGHT   		iReturnBlockingTime = 30	BREAK
		CASE RE_GANGINTIMIDATION	iReturnBlockingTime = 30	BREAK
		CASE RE_GETAWAYDRIVER   	iReturnBlockingTime = 60	BREAK
		CASE RE_HOMELANDSECURITY	iReturnBlockingTime = 30	BREAK
		CASE RE_HITCHLIFT  			iReturnBlockingTime = 35	BREAK
		CASE RE_LURED  				iReturnBlockingTime = 30	BREAK
		CASE RE_MUGGING  			iReturnBlockingTime = 40	BREAK
		CASE RE_PAPARAZZI   		iReturnBlockingTime = 30	BREAK
		CASE RE_PRISONERLIFT		iReturnBlockingTime = 30	BREAK
		CASE RE_PRISONVANBREAK  	iReturnBlockingTime = 30	BREAK
		CASE RE_SHOPROBBERY  		iReturnBlockingTime = 30	BREAK
		CASE RE_SIMEONYETARIAN		iReturnBlockingTime = 60	BREAK
		CASE RE_SECURITYVAN   		iReturnBlockingTime = 60	BREAK
		CASE RE_SNATCHED 			iReturnBlockingTime = 40	BREAK
		CASE RE_STAG   				iReturnBlockingTime = 40	BREAK
	ENDSWITCH
	RETURN iReturnBlockingTime
ENDFUNC

FUNC INT GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(SP_RANDOM_EVENTS eRandomEvent)
	INT iReturnVariationCount = 1
	SWITCH eRandomEvent
		//blocking times for random events, please change them here.
		CASE RE_ATMROBBERY 			iReturnVariationCount = 5	BREAK
		CASE RE_SHOPROBBERY  		iReturnVariationCount = 2	BREAK
		CASE RE_SECURITYVAN   		iReturnVariationCount = 10	BREAK
		CASE RE_CHASETHIEVES   		iReturnVariationCount = 4	BREAK
		CASE RE_HITCHLIFT  			iReturnVariationCount = 4 	BREAK
		CASE RE_ARREST   			iReturnVariationCount = 2	BREAK
		CASE RE_CARTHEFT   			iReturnVariationCount = 2	BREAK
		CASE RE_PRISONERLIFT		iReturnVariationCount = 2	BREAK
		CASE RE_ABANDONEDCAR		iReturnVariationCount = 2	BREAK
		CASE RE_MUGGING  			iReturnVariationCount = 3	BREAK
		CASE RE_BIKETHIEF			iReturnVariationCount = 2	BREAK
		CASE RE_DRUNKDRIVER			iReturnVariationCount = 2	BREAK
		CASE RE_BORDERPATROL		iReturnVariationCount = 3	BREAK
	ENDSWITCH
	
	RETURN iReturnVariationCount
	
ENDFUNC

FUNC SP_RANDOM_EVENTS GET_RANDOM_EVENT_ENUM_FROM_SCRIPT_NAME(TEXT_LABEL_63 pScriptName)

	SWITCH GET_HASH_KEY(pScriptName)
		CASE HASH("re_abandonedcar")		RETURN RE_ABANDONEDCAR 			BREAK
		CASE HASH("re_accident")			RETURN RE_ACCIDENT 				BREAK
		CASE HASH("re_arrests")				RETURN RE_ARREST 				BREAK
		CASE HASH("re_atmrobbery") 			RETURN RE_ATMROBBERY 			BREAK
		CASE HASH("re_bikethief") 			RETURN RE_BIKETHIEF				BREAK
		CASE HASH("re_border")				RETURN RE_BORDERPATROL			BREAK
		CASE HASH("re_burials")				RETURN RE_BURIAL				BREAK
		CASE HASH("re_bus_tours") 			RETURN RE_BUSTOUR				BREAK
		CASE HASH("re_CarTheft") 			RETURN RE_CARTHEFT				BREAK
		CASE HASH("re_chasethieves") 		RETURN RE_CHASETHIEVES			BREAK
		CASE HASH("re_crashrescue") 		RETURN RE_CRASHRESCUE			BREAK
		CASE HASH("re_cultshootout") 		RETURN RE_CULTSHOOTOUT			BREAK
		CASE HASH("re_DealGoneWrong") 		RETURN RE_DEALGONEWRONG 		BREAK
		CASE HASH("re_domestic") 			RETURN RE_DOMESTIC				BREAK
		CASE HASH("re_drunkdriver") 		RETURN RE_DRUNKDRIVER			BREAK
		CASE HASH("re_Gang_Intimidation") 	RETURN RE_GANGINTIMIDATION		BREAK
		CASE HASH("re_gangfight") 			RETURN RE_GANGFIGHT				BREAK
		CASE HASH("re_getaway_driver") 		RETURN RE_GETAWAYDRIVER			BREAK
		CASE HASH("re_hitch_lift") 			RETURN RE_HITCHLIFT 			BREAK
		CASE HASH("re_Homeland_Security") 	RETURN RE_HOMELANDSECURITY		BREAK
		CASE HASH("re_lured") 				RETURN RE_LURED					BREAK
		CASE HASH("re_muggings")			RETURN RE_MUGGING				BREAK
		CASE HASH("re_paparazzi") 			RETURN RE_PAPARAZZI				BREAK
		CASE HASH("re_prisonerlift")		RETURN RE_PRISONERLIFT			BREAK
		CASE HASH("re_prisonvanbreak") 		RETURN RE_PRISONVANBREAK		BREAK
		CASE HASH("re_SecurityVan")			RETURN RE_SECURITYVAN			BREAK
		CASE HASH("re_shoprobbery")			RETURN RE_SHOPROBBERY			BREAK
		CASE HASH("re_snatched") 			RETURN RE_SNATCHED				BREAK
		CASE HASH("re_stag_do") 			RETURN RE_STAG					BREAK
		CASE HASH("re_yetarian")			RETURN RE_SIMEONYETARIAN		BREAK
		CASE HASH("re_Duel")				RETURN RE_DUEL					BREAK
		CASE HASH("re_SeaPlane") 			RETURN RE_SEAPLANE				BREAK
		CASE HASH("re_monkey")	 			RETURN RE_MONKEYPHOTO			BREAK
	ENDSWITCH

	RETURN RE_NONE
	
ENDFUNC

FUNC SP_RANDOM_EVENTS GET_RANDOM_EVENT_ENUM_FROM_CURRENT_SCRIPT()
	
	TEXT_LABEL_63 pScriptName = GET_THIS_SCRIPT_NAME()
	
	SP_RANDOM_EVENTS eReturnEvent = GET_RANDOM_EVENT_ENUM_FROM_SCRIPT_NAME(pScriptName)
	
	#IF IS_DEBUG_BUILD
		IF eReturnEvent = RE_NONE
			CPRINTLN(DEBUG_RANDOM_EVENTS, "No random event found for script - ", GET_THIS_SCRIPT_NAME(), " please bug Paul D")
		ENDIF
	#ENDIF
	
	RETURN eReturnEvent
ENDFUNC

PROC SUPPRESS_PLAYERS_CAR_FOR_RANDOM_EVENT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), TRUE)
		ENDIF
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CAN_RE_RUN_THROUGH_CANDIDATE_SYSTEM()
	SWITCH Request_Mission_Launch(g_iRECandidateID,MCTID_MUST_LAUNCH , MISSION_TYPE_RANDOM_EVENT, FALSE, GET_ID_OF_THIS_THREAD())
		CASE MCRET_ACCEPTED
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_RANDOM_EVENTS, "Random Event Debug: ", GET_RANDOM_EVENT_DISPLAY_STRING_FROM_ID(g_eCurrentRandomEvent), " - Request_Mission_Launch is MCRET_ACCEPTED.")
			#ENDIF
			RETURN TRUE
		BREAK
		CASE MCRET_DENIED
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_RANDOM_EVENTS, "Random Event Debug: ", GET_RANDOM_EVENT_DISPLAY_STRING_FROM_ID(g_eCurrentRandomEvent), " - Request_Mission_Launch is MCRET_DENIED.")
			#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_CURRENT_RANDOM_EVENT(SP_RANDOM_EVENTS eRandomEvent)
	CPRINTLN(DEBUG_RANDOM_EVENTS, "Current random event set to ", GET_RANDOM_EVENT_DISPLAY_STRING_FROM_ID(g_eCurrentRandomEvent), ".")
	g_eCurrentRandomEvent = eRandomEvent
ENDPROC

FUNC SP_RANDOM_EVENTS GET_ACTIVE_RANDOM_EVENT()
	RETURN g_eCurrentRandomEvent
ENDFUNC

PROC APPLY_SPECIFIC_DELAY_TO_RANDOM_EVENT(SP_RANDOM_EVENTS eRandomEvent, INT iDays = 0, INT iHours = 0)
	TIMEOFDAY todNow = GET_CURRENT_TIMEOFDAY()
	ADD_TIME_TO_TIMEOFDAY(todNow, 0, 0, iHours, iDays)
	SET_RANDOM_EVENT_TIME_FOR_NEXT_LAUNCH(eRandomEvent, todNow)
	TEXT_LABEL_63 todTemp = TIMEOFDAY_TO_TEXT_LABEL_63(todNow)
	CPRINTLN(DEBUG_RANDOM_EVENTS, "Random Event Debug: Setting ", GET_RANDOM_EVENT_DISPLAY_STRING_FROM_ID(eRandomEvent), " next launch time to ", todTemp)
ENDPROC

//PROC SET_NON_CP_RANDOM_EVENT_AS_COMPLETE(SP_RANDOM_EVENTS eRandomEvent)
//	IF eRandomEvent = RE_PAPARAZZI
//		g_savedGlobals.sRandomEventData.bRE_EscapePaparazziComplete = FALSE
//	ENDIF
//ENDPROC

#IF IS_DEBUG_BUILD
	PROC RESET_COMPLETION_FOR_THIS_RANDOM_EVENT(SP_RANDOM_EVENTS eRandomEvent)
		INT iCountVar
		g_savedGlobals.sRandomEventData.iREVariationComplete[eRandomEvent] = 0
		IF GET_RANDOM_EVENT_COMPLETION_ID(eRandomEvent, iCountVar) <> UNUSED_DEFAULT
			FOR iCountVar = 1 TO GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(eRandomEvent)+1
				REMOVE_SCRIPT_FROM_COMPLETION_PERCENTAGE_TOTAL(GET_RANDOM_EVENT_COMPLETION_ID(eRandomEvent, iCountVar))
			ENDFOR
		ENDIF
	ENDPROC
#ENDIF
