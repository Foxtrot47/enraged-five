USING "rage_builtins.sch"
USING "globals.sch"
USING "blip_control_public.sch"
USING "Flow_Mission_Data_Public.sch"
USING "friend_flow_public.sch"

#IF IS_DEBUG_BUILD
	USING "flow_debug_game.sch"
#ENDIF


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	mission_control_private.sch
//		AUTHOR			:	Added by Keith but most likely used by Andrew
//		DESCRIPTION		:	Initially a reset function for the mission triggering system.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// Bitfield indexes for trigger scene settings.
CONST_INT	TSS_SCENE_ACTIVE					0
CONST_INT	TSS_BLIP_ACTIVE						1
CONST_INT	TSS_SCENE_LOADING					2
CONST_INT	TSS_SCENE_CREATED					3		
CONST_INT	TSS_CAN_TRIGGER_BEFORE_CREATION		4
CONST_INT	TSS_ONLY_RESET_ON_INIT				5
CONST_INT	TSS_FRIEND_REJECT_ACTIVE			6
CONST_INT	TSS_BATTLE_BUDDY_CALL_ACTIVE		7
CONST_INT	TSS_HAS_TRIGGERED					8
CONST_INT	TSS_TOD_TRIGGER_ACTIVATED			9
CONST_INT	TSS_DOING_TOD_SKIP					10
CONST_INT	TSS_CLEANUP_TOD_SKIP				11
CONST_INT	TSS_RESET_ON_TRIGGERER_INIT			12
CONST_INT	TSS_TOD_BLIP_ACTIVE					13
#IF IS_DEBUG_BUILD
CONST_INT	TSS_PRINTED_LEAVE_AREA_DEBUG		14
CONST_INT	TSS_PRINTED_BLOCKING_DEBUG			15
#ENDIF

#IF USE_CLF_DLC
PROC Reset_Mission_Trigger_CLF(INT paramTriggerIndex)
	CDEBUG3LN(DEBUG_TRIGGER, "Resetting mission trigger at index ", paramTriggerIndex, ".")
	
	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
		IF g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHING
		AND g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHED
			CLEAR_FRIEND_MISSION_ZONE(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
		IF g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHING
		AND g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHED
			CLEAR_FRIEND_MISSION_ZONE(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID)
		ENDIF
	ENDIF

	g_TriggerableMissionsTU[paramTriggerIndex].bLaunchMe				= FALSE
	g_TriggerableMissionsTU[paramTriggerIndex].eType					= MISSION_TRIGGER_TYPE_NULL
	g_TriggerableMissionsTU[paramTriggerIndex].eCandidateType 		= MCTID_CONTACT_POINT
	g_TriggerableMissionsTU[paramTriggerIndex].eMissionID				= SP_MISSION_NONE
	g_TriggerableMissionsTU[paramTriggerIndex].eStrand				= STRAND_NONE
	
	g_TriggerableMissionsTU[paramTriggerIndex].iCandidateIndex		= -1
	
	IF(g_TriggerableMissionsTU[paramTriggerIndex].bUsed)
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].eBlip, FALSE)
		g_TriggerableMissionsTU[paramTriggerIndex].eBlip				= STATIC_BLIP_NAME_DUMMY_FINAL
	ENDIF
	
	g_TriggerableMissionsTU[paramTriggerIndex].bUsed					= FALSE
	g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset = -1

	//Copied out of trigger_scene_private.sch RESET_TRIGGER_SCENE to avoid header cycles.
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_RESET_ON_TRIGGERER_INIT)
	
	#IF IS_DEBUG_BUILD
		CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
		CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
	#ENDIF
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC Reset_Mission_Trigger_NRM(INT paramTriggerIndex)
	CDEBUG3LN(DEBUG_TRIGGER, "Resetting mission trigger at index ", paramTriggerIndex, ".")
	
	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
		IF g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHING
		AND g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHED
			CLEAR_FRIEND_MISSION_ZONE(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
		IF g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHING
		AND g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHED
			CLEAR_FRIEND_MISSION_ZONE(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID)
		ENDIF
	ENDIF

	g_TriggerableMissionsTU[paramTriggerIndex].bLaunchMe				= FALSE
	g_TriggerableMissionsTU[paramTriggerIndex].eType					= MISSION_TRIGGER_TYPE_NULL
	g_TriggerableMissionsTU[paramTriggerIndex].eCandidateType 		= MCTID_CONTACT_POINT
	g_TriggerableMissionsTU[paramTriggerIndex].eMissionID				= SP_MISSION_NONE
	g_TriggerableMissionsTU[paramTriggerIndex].eStrand				= STRAND_NONE
	
	g_TriggerableMissionsTU[paramTriggerIndex].iCandidateIndex		= -1
	
	IF(g_TriggerableMissionsTU[paramTriggerIndex].bUsed)
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].eBlip, FALSE)
		g_TriggerableMissionsTU[paramTriggerIndex].eBlip				= STATIC_BLIP_NAME_DUMMY_FINAL
	ENDIF
	
	g_TriggerableMissionsTU[paramTriggerIndex].bUsed					= FALSE
	g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset = -1

	//Copied out of trigger_scene_private.sch RESET_TRIGGER_SCENE to avoid header cycles.
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
	CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_RESET_ON_TRIGGERER_INIT)
	
	#IF IS_DEBUG_BUILD
		CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
		CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
	#ENDIF
ENDPROC
#ENDIF


PROC Reset_Mission_Trigger(INT paramTriggerIndex)
#IF USE_CLF_DLC
	Reset_Mission_Trigger_CLF(paramTriggerIndex)
#ENDIF
#IF USE_NRM_DLC
	Reset_Mission_Trigger_NRM(paramTriggerIndex)
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
CDEBUG3LN(DEBUG_TRIGGER, "Resetting mission trigger at index ", paramTriggerIndex, ".")
	
	IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
		IF g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHING
		AND g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHED
			CLEAR_FRIEND_MISSION_ZONE(g_TriggerableMissions[paramTriggerIndex].eMissionID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
		IF g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHING
		AND g_eFriendMissionZoneState <> FRIEND_MISSION_ZONE_LAUNCHED
			CLEAR_FRIEND_MISSION_ZONE(g_TriggerableMissions[paramTriggerIndex].eMissionID)
		ENDIF
	ENDIF

	g_TriggerableMissions[paramTriggerIndex].bLaunchMe				= FALSE
	g_TriggerableMissions[paramTriggerIndex].eType					= MISSION_TRIGGER_TYPE_NULL
	g_TriggerableMissions[paramTriggerIndex].eCandidateType 		= MCTID_CONTACT_POINT
	g_TriggerableMissions[paramTriggerIndex].eMissionID				= SP_MISSION_NONE
	g_TriggerableMissions[paramTriggerIndex].eStrand				= STRAND_NONE
	
	g_TriggerableMissions[paramTriggerIndex].iCandidateIndex		= -1
	
	IF(g_TriggerableMissions[paramTriggerIndex].bUsed)
		IF g_TriggerableMissions[paramTriggerIndex].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF ENUM_TO_INT(g_TriggerableMissions[paramTriggerIndex].eBlip) < 0 OR ENUM_TO_INT(g_TriggerableMissions[paramTriggerIndex].eBlip) >= g_iTotalStaticBlips
				CPRINTLN(DEBUG_BLIP, "Reset_Mission_Trigger | Something very wrong | g_TriggerableMissions[paramTriggerIndex].eBlip = ", g_TriggerableMissions[paramTriggerIndex].eBlip)
			ENDIF
			SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissions[paramTriggerIndex].eBlip, FALSE)
		ELSE
			CPRINTLN(DEBUG_BLIP, "Reset_Mission_Trigger | g_TriggerableMissions[paramTriggerIndex].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL")
		ENDIF
		g_TriggerableMissions[paramTriggerIndex].eBlip				= STATIC_BLIP_NAME_DUMMY_FINAL
	ENDIF
	
	g_TriggerableMissions[paramTriggerIndex].bUsed					= FALSE
	g_TriggerableMissions[paramTriggerIndex].iTriggerableCharBitset = -1

	//Copied out of trigger_scene_private.sch RESET_TRIGGER_SCENE to avoid header cycles.
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
	CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_RESET_ON_TRIGGERER_INIT)
	
	#IF IS_DEBUG_BUILD
		CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
		CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
	#ENDIF
#ENDIF
#ENDIF
ENDPROC


/// PURPOSE:
///    Reset ALL mission triggers (this should only be called by flow launcher or other game reset).
PROC Reset_All_Mission_Triggers()
	CDEBUG3LN(DEBUG_TRIGGER, "Resetting all mission triggers.")
	
	// Clear out control variables
	g_OnMissionState 				= MISSION_TYPE_OFF_MISSION
	g_bMissionTriggerFired			= FALSE
	g_bPlayerLockedInToTrigger		= FALSE
	g_iRegisteredMissionTriggers	= 0

	g_eMissionTriggerProcessing		= SP_MISSION_NONE
	g_eMissionSceneToCleanup 		= SP_MISSION_NONE
	g_eMissionSceneToPreLoad		= SP_MISSION_NONE
	g_eMissionForceTrigger			= SP_MISSION_NONE
	g_bCleanupTriggerScene 			= FALSE
	g_bMissionTriggerLoading		= FALSE
	g_bTriggerSceneActive			= FALSE
	g_bLester1DoorKnocked			= FALSE						// resets the door knock for Lester1A
	g_bAgencyP1HelpDisplayed 		= FALSE
	g_eAgencyP1TimeNextHelp 		= INVALID_TIMEOFDAY
	
	//Clear flow requested ped hints.
	g_iPlayerFlowHintActive 		= 0
	
	// Clear out all triggers
	INT iTriggerIndex
#IF USE_CLF_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
	Reset_Mission_Trigger(iTriggerIndex)
	ENDREPEAT
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
	Reset_Mission_Trigger(iTriggerIndex)
	ENDREPEAT
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
	Reset_Mission_Trigger(iTriggerIndex)
	ENDREPEAT
#ENDIF
#ENDIF
ENDPROC


PROC Clear_Triggered_Flags_For_All_Trigger_Scenes()
	INT iTriggerIndex
#IF USE_CLF_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
	CLEAR_BIT(g_TriggerableMissionsTU[iTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
	ENDREPEAT
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
	CLEAR_BIT(g_TriggerableMissionsTU[iTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
	ENDREPEAT
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
	CLEAR_BIT(g_TriggerableMissions[iTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
	ENDREPEAT
#ENDIF
#ENDIF
ENDPROC


PROC Set_Leave_Area_Flag_For_Mission(SP_MISSIONS paramMissionID, BOOL paramFlagSetting)
#if USE_CLF_DLC
	IF paramFlagSetting		
		g_sMissionActiveData[paramMissionID].leaveArea = TRUE		
	ELSE		
		g_sMissionActiveData[paramMissionID].leaveArea = FALSE
	ENDIF
#endif
#if USE_NRM_DLC
	IF paramFlagSetting	
		g_sMissionActiveData[paramMissionID].leaveArea = TRUE	
	ELSE
		g_sMissionActiveData[paramMissionID].leaveArea = FALSE
	ENDIF
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF paramFlagSetting
		IF paramMissionID != SP_HEIST_JEWELRY_PREP_2A
		AND paramMissionID != SP_HEIST_JEWELRY_PREP_1B
		AND paramMissionID != SP_HEIST_RURAL_PREP_1
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_TRIGGER, "Leave area flag set by ", GET_THIS_SCRIPT_NAME(), " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
			#ENDIF
			g_sMissionActiveData[paramMissionID].leaveArea = TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_TRIGGER, "Leave area flag cleared by ", GET_THIS_SCRIPT_NAME(), " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
		#ENDIF
		g_sMissionActiveData[paramMissionID].leaveArea = FALSE
	ENDIF
#endif
#endif
ENDPROC


PROC Set_Leave_Area_Flag_For_All_Blipped_Missions()
	CDEBUG1LN(DEBUG_TRIGGER, "Setting leave area flags for all active blipped mission triggers.")

	INT iTriggerIndex
	
#IF USE_CLF_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
		AND NOT g_TriggerableMissionsTU[iTriggerIndex].bLaunchMe
			IF g_TriggerableMissionsTU[iTriggerIndex].eType = MISSION_TRIGGER_TYPE_SCENE	
				Set_Leave_Area_Flag_For_Mission(g_TriggerableMissionsTU[iTriggerIndex].eMissionID, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
		AND NOT g_TriggerableMissionsTU[iTriggerIndex].bLaunchMe
			IF g_TriggerableMissionsTU[iTriggerIndex].eType = MISSION_TRIGGER_TYPE_SCENE	
				Set_Leave_Area_Flag_For_Mission(g_TriggerableMissionsTU[iTriggerIndex].eMissionID, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
		IF g_TriggerableMissions[iTriggerIndex].bUsed
		AND NOT g_TriggerableMissions[iTriggerIndex].bLaunchMe
			IF g_TriggerableMissions[iTriggerIndex].eType = MISSION_TRIGGER_TYPE_SCENE	
				//Don't set leave area flags for moving mission blips.
				IF g_TriggerableMissions[iTriggerIndex].eMissionID != SP_HEIST_JEWELRY_PREP_2A
				AND g_TriggerableMissions[iTriggerIndex].eMissionID != SP_HEIST_JEWELRY_PREP_1B
				AND g_TriggerableMissions[iTriggerIndex].eMissionID != SP_HEIST_RURAL_PREP_1
					Set_Leave_Area_Flag_For_Mission(g_TriggerableMissions[iTriggerIndex].eMissionID, TRUE)
				ENDIF				
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF
#ENDIF
		
ENDPROC

