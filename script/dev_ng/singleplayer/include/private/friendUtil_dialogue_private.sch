//-	commands headers	-//
USING "rage_builtins.sch"
USING "globals.sch"

//- script headers	-//
USING "commands_script.sch"

//-	public headers	-//
USING "friends_public.sch"

//-	private headers	-//
USING "comms_control_public.sch"
USING "friends_private.sch"

//- project specific headers -//
#if NOT USE_SP_DLC
	USING "vector_id_data_gta5.sch"
	USING "code_control_data_gta5.sch"
#endif

#if USE_CLF_DLC
	USING "vector_id_data_CLF.sch"
	USING "code_control_data_CLF.sch"	
#endif

#if USE_NRM_DLC
	USING "vector_id_data_NRM.sch"
	USING "code_control_data_NRM.sch"	
#endif
#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "script_debug.sch"
	USING "shared_debug.sch"
#ENDIF

///private header for friends dialogue utils
///    sam.hackett@rockstarleeds.com
///    

// *******************************************************************************************
//	FRIEND CONVERSATION ENUMS
// *******************************************************************************************

ENUM enumFriendAudioBlock
	FAB_phone = 0,
	FAB_activity,
	FAB_chat
ENDENUM


ENUM enumFriendPhonePhrase

	// Outgoing call
	FPP_OUTGOING_GREET1,			// Outgoing - greet - a+b
	FPP_OUTGOING_GREET2,	
	FPP_OUTGOING_GREET2_MEETINGMICHAEL,
	FPP_OUTGOING_GREET2_MEETINGFRANKLIN,
	FPP_OUTGOING_GREET2_MEETINGTREVOR,
	FPP_OUTGOING_GREET2_MEETINGLAMAR,
	FPP_OUTGOING_YES1_OK,			// Outgoing - yes - c+d
	FPP_OUTGOING_YES1_IRATE,
	FPP_OUTGOING_YES1_VERYIRATE,
	FPP_OUTGOING_YES1_FIRSTTIME,
	FPP_OUTGOING_YES1_LIKELOW,
	FPP_OUTGOING_YES1_BUSY,
	FPP_OUTGOING_YES1_COMETOYOU,
	FPP_OUTGOING_YES2_OK,
	FPP_OUTGOING_YES2_COMETOYOU,
	FPP_OUTGOING_NO1_OK,			// Outgoing - no - c+d
	FPP_OUTGOING_NO1_RECENTFACE,
	FPP_OUTGOING_NO1_RECENTPHONE,
	FPP_OUTGOING_NO1_BLOCKED,
	FPP_OUTGOING_NO2,


	// Backup
	FPP_BACKUP,
	FPP_BACKUP_DOCKS_2,
	FPP_BACKUP_RURAL,
	FPP_BACKUP_AGENCY,
	FPP_BACKUP_FINALE_A,
	FPP_BACKUP_FINALE_B,
	FPP_BACKUP_FBI_1,
	FPP_BACKUP_FBI_2,
	FPP_BACKUP_FBI_4,
	FPP_BACKUP_FBI_5,

	
	// General
	FPP_CANCEL1,					//PLAYER phones to cancel hanging out
	FPP_CANCEL2_HILIKE,				//PLAYER phones to cancel hanging out - Friend replies, LIKE stat is HIGH B
	FPP_CANCEL2_MEDLIKE,			//PLAYER phones to cancel hanging out - Friend replies, LIKE stat is MEDIUM
	FPP_CANCEL2_LOWLIKE,			//PLAYER phones to cancel hanging out - Friend replies, LIKE stat is LOW
	FPP_CANCEL2_FIRSTTIME,			//PLAYER phones to cancel hanging out for the first time, FRIEND explains it's fine to inform the player it's acceptable


	// Lay low (if Michael/Trevor killed in finale, Lamar will not go out in the day)
	FPP_LAYLOW1,
	FPP_LAYLOW2,
	FPP_LAYLOW3,

	
	MAX_FRIEND_PHONE_PHRASE,
	NO_FRIEND_PHONE_PHRASE
ENDENUM


ENUM enumFriendActivityPhrase
	FAP_PICKUP_OK = 0,				// Pickup is triggered
	FAP_PICKUP_LATE,
	FAP_PICKUP_NOCAR,
	FAP_PICKUP_FULLCAR,
	FAP_PICKUP_ODDCAR,
	FAP_PICKUP_YOUDRIVE,
	
	FAP_GREET_OK,					// Friend has got in car with player
	FAP_GREET_IRATE,
	FAP_GREET_VIRATE,
	FAP_GREET_PLURAL,
	FAP_GREET_YOUDRIVE,
	
	FAP_AMBIENT_HELLO_OK,			// Player meets friend ambiently in world, friend says hello
	FAP_AMBIENT_HELLO_RECENT,
	FAP_AMBIENT_HELLO_IRATE,
	
	FAP_AMBIENT_BLOCKED_MISSION,	// Player meets friend ambiently in world, friend is blocked from hangout
	FAP_AMBIENT_BLOCKED_OK,

	FAP_AMBIENT_OFFER_QUESTION,		// Player meets friend ambiently in world, friend asks to hangout
	FAP_AMBIENT_OFFER_YES1,
	FAP_AMBIENT_OFFER_YES2,
	FAP_AMBIENT_OFFER_NO1,
	
	FAP_AMBIENT_GOODBYE_OK,			// Player meets friend ambiently in world, friend says goodbye
	FAP_AMBIENT_GOODBYE_IRATE,
	
	FAP_AMBIENT_STALK_1,			// Player meets friend ambiently in world, player follows friend
	FAP_AMBIENT_STALK_2,
	FAP_AMBIENT_STALK_3,
//	FAP_AMBIENT_STALK_4,
	FAP_AMBIENT_STALK_4_1,
	FAP_AMBIENT_STALK_4_2,
	FAP_AMBIENT_STALK_4_3,
	FAP_AMBIENT_STALK_4_4,

	FAP_AMBIENT_STALK_FT,

	FAP_AMBIENT_TRAILER_1,			// Player meets trevor ambiently at his safehouse
	FAP_AMBIENT_TRAILER_1r,
	FAP_AMBIENT_TRAILER_2,
	FAP_AMBIENT_TRAILER_3,
	FAP_AMBIENT_TRAILER_4,

	FAP_AMBIENT_POO_1a,				// Player meets trevor ambiently at his safehouse, he is on the toilet
	FAP_AMBIENT_POO_1b,
	FAP_AMBIENT_POO_1c,
	FAP_AMBIENT_POO_2,

//	FAC_SUGGEST_TENNIS,				// Suggestions for places to visit
//	FAC_SUGGEST_GOLF,				
//	FAC_SUGGEST_DARTS,				
//	FAC_SUGGEST_STRIPCLUB,			
//	FAC_SUGGEST_CINEMA,				
//	FAC_SUGGEST_BAR,
	
	FAP_VERY_DRUNK,					//Too drunk to speak
	FAP_SATISFIED,					//Happy to go home if player wants
	FAP_DONE,						//Have to go home now

//	FAC_LEAVE_TENNIS_WON,			//Leaving Tennis - PLAYER won
//	FAC_LEAVE_TENNIS_LOST,			//Leaving Tennis - PLAYER lost
//	FAC_LEAVE_TENNIS_TIED,			//Leaving Tennis - overall the games were tied
//	FAC_LEAVE_TENNIS_QUIT,			//Leaving Tennis - Game abandoned
//	FAC_LEAVE_TENNIS_DRUNK,			//Leaving Tennis - player drunk
//	FAC_LEAVE_BASKETBALL_WON,		//Leaving Basketball - PLAYER won
//	FAC_LEAVE_BASKETBALL_LOST,		//Leaving Basketball - PLAYER lost
//	FAC_LEAVE_BASKETBALL_QUIT,		//Leaving Basketball - Game abandoned
//	FAC_LEAVE_BASKETBALL_DRUNK,		//Leaving Basketball - player drunk
//	FAC_LEAVE_GOLF_WON,				//Leaving Golf - PLAYER won
//	FAC_LEAVE_GOLF_LOST,			//Leaving Golf - PLAYER lost
//	FAC_LEAVE_GOLF_TIED,			//Leaving Golf - overall the games were tied
//	FAC_LEAVE_GOLF_DRUNK,			//Leaving Golf - player drunk
//	FAC_LEAVE_GOLF_QUIT,			//Leaving Golf - Game abandoned
//	FAC_LEAVE_POOL_WON,				//Leaving Pool - PLAYER won
//	FAC_LEAVE_POOL_LOST,			//Leaving Pool - PLAYER lost
//	FAC_LEAVE_POOL_QUIT,			//Leaving Pool - Game abandoned
//	FAC_LEAVE_POOL_DRUNK,			//Leaving Pool - player drunk
//	FAC_LEAVE_AIRHOCKEY_WON,		//Leaving Air Hockey - PLAYER won
//	FAC_LEAVE_AIRHOCKEY_LOST,		//Leaving Air Hockey - PLAYER lost
//	FAC_LEAVE_AIRHOCKEY_TIED,		//Leaving Air Hockey - overall the games were tied
//	FAC_LEAVE_AIRHOCKEY_QUIT,		//Leaving Air Hockey - Game abandoned
//	FAC_LEAVE_AIRHOCKEY_DRUNK,		//Leaving Air Hockey - player drunk
//	FAC_LEAVE_ARMWRESTLE_WON,		//Leaving Arm Wrestling - PLAYER won
//	FAC_LEAVE_ARMWRESTLE_LOST,		//Leaving Arm Wrestling - PLAYER lost
//	FAC_LEAVE_ARMWRESTLE_TIED,		//Leaving Arm Wrestling - overall the games were tied
//	FAC_LEAVE_ARMWRESTLE_QUIT,		//Leaving Arm Wrestling - Game abandoned
//	FAC_LEAVE_ARMWRESTLE_DRUNK,		//Leaving Arm Wrestling - player drunk
//	FAC_LEAVE_DARTS_WON,			//Leaving Darts - PLAYER won
//	FAC_LEAVE_DARTS_LOST,			//Leaving Darts - PLAYER lost
//	FAC_LEAVE_DARTS_TIED,			//Leaving Darts - overall the games were tied
//	FAC_LEAVE_DARTS_QUIT,			//Leaving Darts - Game abandoned
//	FAC_LEAVE_DARTS_DRUNK,			//Leaving Darts - player drunk
//	FAC_LEAVE_STRIPCLUB_WON,		//Leaving Strip Club - PLAYER won
//	FAC_LEAVE_STRIPCLUB_QUIT,		//Leaving Strip Club - Game abandoned
//	FAC_LEAVE_STRIPCLUB_DRUNK,		//Leaving Strip Club - player drunk
//	FAC_LEAVE_CINEMA_WON,			//Leaving Cinema - PLAYER won
//	FAC_LEAVE_CINEMA_QUIT,			//Leaving Cinema - Game abandoned
//	FAC_LEAVE_CINEMA_DRUNK,			//Leaving Cinema - player drunk
//	FAC_LEAVE_FOOD_WON,				//Leaving Food - PLAYER won
//	FAC_LEAVE_FOOD_QUIT,			//Leaving Food- Game abandoned
//	FAC_LEAVE_FOOD_DRUNK,			//Leaving Food - player drunk
	
	FAP_MISSIONZONE_STORY,			//Driving near mission - Player says he might do story mission
	FAP_MISSIONZONE_SQUAD,			//Driving near mission - Player says he might do prep mission
	FAP_MISSIONZONE_DRUNK,			//Driving near mission - Player says he might do mission (drunk)
	FAP_MISSIONZONE_CANCEL,			//Driving near misison - Player says he'll go back later
	FAP_MISSIONZONE_CANCEL_DRUNK,	//Driving near misison - Player says he'll go back later (drunk)
	
	FAP_REJECTION_TIMEOUT,			//Soldier rejected as squad mission timed out
	FAP_REJECTION_OK,				//Friend rejected for mission
	FAP_REJECTION_CRAZY,			//Friend rejected for mission - which started violently
	FAP_REJECTION_STUCK,			//Friend rejected for mission - but stuck in car

	FAP_SQUAD_START,				//Battle buddy joined prep mission
	FAP_SQUAD_PASSED,				//Battle buddy completed prep mission
	
	FAP_COMMENT_HAIRCUT,			//Player changed haircut, friend comments on it
	FAP_COMMENT_CLOTHES,			//Player changed clothes, friend comments on it
	FAP_COMMENT_TATTOO,				//Player changed tattoo, friend comments on it

	FAP_COMMENT_MELTDOWN,			//Just for Franklin-Michael - Michael tells Franklin how he really liked seeing his film

	FAP_COMMENT_CARDAMAGE,			//Player changed tattoo, friend comments on it
	FAP_COMMENT_PEDDAMAGE,			//Player changed tattoo, friend comments on it

	FAP_DROPOFF_OPENER,				//Dropping PLAYER off - Player has returned Friend
	FAP_DROPOFF_OPENER_DRUNK,		//Dropping PLAYER off - Player has returned Friend drunk
	FAP_DROPOFF_OPENER_PLURAL,		//Dropping PLAYER off - Player has returned several Friends
	
	FAP_DROPOFF_OK,					//Dropping PLAYER off - Friend is happy
	FAP_DROPOFF_IRATE,				//Dropping PLAYER off - Friend is irate
	FAP_DROPOFF_VERY_IRATE,			//Dropping PLAYER off - Friend is very irate
	FAP_DROPOFF_DRUNK,				//Dropping PLAYER off - Friend is drunk
	FAP_DROPOFF_GOTCAR,				//Dropping PLAYER off - Player has friends car
	
	FAP_DEATH,						//The other friend has died
	
	MAX_FRIEND_ACTIVITY_PHRASES,
	NO_FRIEND_ACTIVITY_PHRASE
ENDENUM


ENUM enumFriendTextMessage
	FTM_FRIEND_LOST = 0,
	FTM_FRIEND_STOOD_UP,
	FTM_FRIEND_HOSPITAL,
	FTM_PLAYER_DIED,
	FTM_PLAYER_BUSTED,
	
	MAX_FRIEND_TEXT_MESSAGES,
	NO_FRIEND_TEXT_MESSAGE
ENDENUM


// *******************************************************************************************
//	CONVERSATION ENUM LABEL debug functions
// *******************************************************************************************

#IF IS_DEBUG_BUILD
	FUNC STRING GetLabel_enumFriendAudioBlock(enumFriendAudioBlock eAudioBlock)
		SWITCH eAudioBlock
			CASE FAB_phone					RETURN "FAB_phone" BREAK
			CASE FAB_activity					RETURN "FAB_activity" BREAK
			CASE FAB_chat						RETURN "FAB_chat" BREAK
		ENDSWITCH
		
		RETURN "<InvalidAudioBlock>"
	ENDFUNC

	FUNC STRING GetLabel_enumFriendPhonePhrase(enumFriendPhonePhrase thisFriendPhoneConv)
		SWITCH thisFriendPhoneConv
			CASE FPP_OUTGOING_GREET1					RETURN "FPP_OUTGOING_GREET1" BREAK
			CASE FPP_OUTGOING_GREET2					RETURN "FPP_OUTGOING_GREET2" BREAK
			CASE FPP_OUTGOING_GREET2_MEETINGMICHAEL		RETURN "FPP_OUTGOING_GREET2_MEETINGMICHAEL" BREAK
			CASE FPP_OUTGOING_GREET2_MEETINGFRANKLIN	RETURN "FPP_OUTGOING_GREET2_MEETINGFRANKLIN" BREAK
			CASE FPP_OUTGOING_GREET2_MEETINGTREVOR		RETURN "FPP_OUTGOING_GREET2_MEETINGTREVOR" BREAK
			CASE FPP_OUTGOING_GREET2_MEETINGLAMAR		RETURN "FPP_OUTGOING_GREET2_MEETINGLAMAR" BREAK
			CASE FPP_OUTGOING_YES1_OK					RETURN "FPP_OUTGOING_YES1_OK" BREAK
			CASE FPP_OUTGOING_YES1_IRATE				RETURN "FPP_OUTGOING_YES1_IRATE" BREAK
			CASE FPP_OUTGOING_YES1_VERYIRATE			RETURN "FPP_OUTGOING_YES1_VERYIRATE" BREAK
			CASE FPP_OUTGOING_YES1_FIRSTTIME			RETURN "FPP_OUTGOING_YES1_FIRSTTIME" BREAK
			CASE FPP_OUTGOING_YES1_LIKELOW				RETURN "FPP_OUTGOING_YES1_LIKELOW" BREAK
			CASE FPP_OUTGOING_YES1_BUSY					RETURN "FPP_OUTGOING_YES1_BUSY" BREAK
			CASE FPP_OUTGOING_YES1_COMETOYOU			RETURN "FPP_OUTGOING_YES1_COMETOYOU" BREAK
			CASE FPP_OUTGOING_YES2_OK					RETURN "FPP_OUTGOING_YES2_OK" BREAK
			CASE FPP_OUTGOING_YES2_COMETOYOU			RETURN "FPP_OUTGOING_YES2_COMETOYOU" BREAK
			CASE FPP_OUTGOING_NO1_OK					RETURN "FPP_OUTGOING_NO1_OK" BREAK
			CASE FPP_OUTGOING_NO1_RECENTFACE			RETURN "FPP_OUTGOING_NO1_RECENTFACE" BREAK
			CASE FPP_OUTGOING_NO1_RECENTPHONE			RETURN "FPP_OUTGOING_NO1_RECENTPHONE" BREAK
			CASE FPP_OUTGOING_NO1_BLOCKED				RETURN "FPP_OUTGOING_NO1_BLOCKED" BREAK
			CASE FPP_OUTGOING_NO2						RETURN "FPP_OUTGOING_NO2" BREAK
			
			CASE FPP_BACKUP								RETURN "FPP_BACKUP" BREAK
			CASE FPP_BACKUP_DOCKS_2						RETURN "FPP_BACKUP_DOCKS_2" BREAK
			CASE FPP_BACKUP_RURAL						RETURN "FPP_BACKUP_RURAL" BREAK
			CASE FPP_BACKUP_AGENCY						RETURN "FPP_BACKUP_AGENCY" BREAK
			CASE FPP_BACKUP_FINALE_A					RETURN "FPP_BACKUP_FINALE_A" BREAK
			CASE FPP_BACKUP_FINALE_B					RETURN "FPP_BACKUP_FINALE_B" BREAK
			CASE FPP_BACKUP_FBI_1						RETURN "FPP_BACKUP_FBI_1" BREAK
			CASE FPP_BACKUP_FBI_2						RETURN "FPP_BACKUP_FBI_2" BREAK
			CASE FPP_BACKUP_FBI_4						RETURN "FPP_BACKUP_FBI_4" BREAK
			CASE FPP_BACKUP_FBI_5						RETURN "FPP_BACKUP_FBI_5" BREAK

			CASE FPP_CANCEL1							RETURN "FPP_CANCEL1" BREAK
			CASE FPP_CANCEL2_HILIKE						RETURN "FPP_CANCEL2_HILIKE" BREAK
			CASE FPP_CANCEL2_MEDLIKE					RETURN "FPP_CANCEL2_MEDLIKE" BREAK
			CASE FPP_CANCEL2_LOWLIKE					RETURN "FPP_CANCEL2_LOWLIKE" BREAK
			CASE FPP_CANCEL2_FIRSTTIME					RETURN "FPP_CANCEL2_FIRSTTIME" BREAK
			
			CASE FPP_LAYLOW1							RETURN "FPP_LAYLOW1" BREAK
			CASE FPP_LAYLOW2							RETURN "FPP_LAYLOW2" BREAK
			CASE FPP_LAYLOW3							RETURN "FPP_LAYLOW3" BREAK
			
			CASE MAX_FRIEND_PHONE_PHRASE
				RETURN "MAX_FRIEND_PHONE_PHRASE"
			BREAK
			CASE NO_FRIEND_PHONE_PHRASE
				RETURN "NO_FRIEND_PHONE_PHRASE"
			BREAK
		ENDSWITCH
		
		RETURN "invalid thisFriendPhoneConv"
	ENDFUNC

	FUNC STRING GetLabel_enumFriendActivityPhrase(enumFriendActivityPhrase thisFriendActivityConv)
		SWITCH  thisFriendActivityConv
			CASE FAP_PICKUP_OK					RETURN "FAP_PICKUP_OK" BREAK    
			CASE FAP_PICKUP_LATE				RETURN "FAP_PICKUP_LATE" BREAK
			CASE FAP_PICKUP_NOCAR				RETURN "FAP_PICKUP_NOCAR" BREAK
			CASE FAP_PICKUP_FULLCAR				RETURN "FAP_PICKUP_FULLCAR" BREAK
			CASE FAP_PICKUP_ODDCAR				RETURN "FAP_PICKUP_ODDCAR" BREAK
			CASE FAP_PICKUP_YOUDRIVE			RETURN "FAP_PICKUP_YOUDRIVE" BREAK
			
			CASE FAP_GREET_OK					RETURN "FAP_GREET_OK" BREAK
			CASE FAP_GREET_IRATE				RETURN "FAP_GREET_IRATE" BREAK
			CASE FAP_GREET_VIRATE				RETURN "FAP_GREET_VIRATE" BREAK
			CASE FAP_GREET_PLURAL				RETURN "FAP_GREET_PLURAL" BREAK
			CASE FAP_GREET_YOUDRIVE				RETURN "FAP_GREET_YOUDRIVE" BREAK
				                               		
			CASE FAP_AMBIENT_HELLO_OK			RETURN "FAP_AMBIENT_HELLO_OK" BREAK
			CASE FAP_AMBIENT_HELLO_RECENT		RETURN "FAP_AMBIENT_HELLO_RECENT" BREAK
			CASE FAP_AMBIENT_HELLO_IRATE		RETURN "FAP_AMBIENT_HELLO_IRATE" BREAK

			CASE FAP_AMBIENT_BLOCKED_MISSION	RETURN "FAP_AMBIENT_BLOCKED_MISSION" BREAK
			CASE FAP_AMBIENT_BLOCKED_OK			RETURN "FAP_AMBIENT_BLOCKED_OK" BREAK
			
			CASE FAP_AMBIENT_OFFER_QUESTION		RETURN "FAP_AMBIENT_OFFER_QUESTION" BREAK
			CASE FAP_AMBIENT_OFFER_YES1			RETURN "FAP_AMBIENT_OFFER_YES1" BREAK
			CASE FAP_AMBIENT_OFFER_YES2			RETURN "FAP_AMBIENT_OFFER_YES2" BREAK
			CASE FAP_AMBIENT_OFFER_NO1			RETURN "FAP_AMBIENT_OFFER_NO1" BREAK
			
			CASE FAP_AMBIENT_GOODBYE_OK			RETURN "FAP_AMBIENT_GOODBYE_OK" BREAK
			CASE FAP_AMBIENT_GOODBYE_IRATE		RETURN "FAP_AMBIENT_GOODBYE_IRATE" BREAK
			
			CASE FAP_AMBIENT_STALK_1			RETURN "FAP_AMBIENT_STALK_1" BREAK
			CASE FAP_AMBIENT_STALK_2			RETURN "FAP_AMBIENT_STALK_2" BREAK
			CASE FAP_AMBIENT_STALK_3			RETURN "FAP_AMBIENT_STALK_3" BREAK
//			CASE FAP_AMBIENT_STALK_4			RETURN "FAP_AMBIENT_STALK_4" BREAK
			CASE FAP_AMBIENT_STALK_4_1			RETURN "FAP_AMBIENT_STALK_4_1" BREAK
			CASE FAP_AMBIENT_STALK_4_2			RETURN "FAP_AMBIENT_STALK_4_2" BREAK
			CASE FAP_AMBIENT_STALK_4_3			RETURN "FAP_AMBIENT_STALK_4_3" BREAK
			CASE FAP_AMBIENT_STALK_4_4			RETURN "FAP_AMBIENT_STALK_4_4" BREAK

			CASE FAP_AMBIENT_STALK_FT			RETURN "FAP_AMBIENT_STALK_FT" BREAK

			CASE FAP_AMBIENT_TRAILER_1			RETURN "FAP_AMBIENT_TRAILER_1" BREAK
			CASE FAP_AMBIENT_TRAILER_1r			RETURN "FAP_AMBIENT_TRAILER_1r" BREAK
			CASE FAP_AMBIENT_TRAILER_2			RETURN "FAP_AMBIENT_TRAILER_2" BREAK
			CASE FAP_AMBIENT_TRAILER_3			RETURN "FAP_AMBIENT_TRAILER_3" BREAK
			CASE FAP_AMBIENT_TRAILER_4			RETURN "FAP_AMBIENT_TRAILER_4" BREAK
			
			CASE FAP_AMBIENT_POO_1a				RETURN "FAP_AMBIENT_POO_1a" BREAK
			CASE FAP_AMBIENT_POO_1b				RETURN "FAP_AMBIENT_POO_1b" BREAK
			CASE FAP_AMBIENT_POO_1c				RETURN "FAP_AMBIENT_POO_1c" BREAK
			CASE FAP_AMBIENT_POO_2				RETURN "FAP_AMBIENT_POO_2" BREAK

//			CASE FAC_SUGGEST_TENNIS				RETURN "FAC_SUGGEST_TENNIS" BREAK
//			CASE FAC_SUGGEST_GOLF				RETURN "FAC_SUGGEST_GOLF" BREAK
//			CASE FAC_SUGGEST_DARTS				RETURN "FAC_SUGGEST_DARTS" BREAK
//			CASE FAC_SUGGEST_STRIPCLUB			RETURN "FAC_SUGGEST_STRIPCLUB" BREAK
//			CASE FAC_SUGGEST_CINEMA				RETURN "FAC_SUGGEST_CINEMA" BREAK
//			CASE FAC_SUGGEST_BAR				RETURN "FAC_SUGGEST_BAR" BREAK

			CASE FAP_VERY_DRUNK					RETURN "FAP_VERY_DRUNK" BREAK
			CASE FAP_SATISFIED					RETURN "FAP_SATISFIED" BREAK
			CASE FAP_DONE						RETURN "FAP_DONE" BREAK
				                               		
//			CASE FAC_LEAVE_TENNIS_WON			RETURN "FAC_LEAVE_TENNIS_WON" BREAK
//			CASE FAC_LEAVE_TENNIS_LOST			RETURN "FAC_LEAVE_TENNIS_LOST" BREAK
//			CASE FAC_LEAVE_TENNIS_TIED			RETURN "FAC_LEAVE_TENNIS_TIED" BREAK
//			CASE FAC_LEAVE_TENNIS_QUIT			RETURN "FAC_LEAVE_TENNIS_QUIT" BREAK
//			CASE FAC_LEAVE_TENNIS_DRUNK			RETURN "FAC_LEAVE_TENNIS_DRUNK" BREAK
//			CASE FAC_LEAVE_GOLF_WON				RETURN "FAC_LEAVE_GOLF_WON" BREAK
//			CASE FAC_LEAVE_GOLF_LOST			RETURN "FAC_LEAVE_GOLF_LOST" BREAK
//			CASE FAC_LEAVE_GOLF_TIED			RETURN "FAC_LEAVE_GOLF_TIED" BREAK
//			CASE FAC_LEAVE_GOLF_DRUNK			RETURN "FAC_LEAVE_GOLF_DRUNK" BREAK
//			CASE FAC_LEAVE_GOLF_QUIT			RETURN "FAC_LEAVE_GOLF_QUIT" BREAK
//			CASE FAC_LEAVE_DARTS_WON			RETURN "FAC_LEAVE_DARTS_WON" BREAK
//			CASE FAC_LEAVE_DARTS_LOST			RETURN "FAC_LEAVE_DARTS_LOST" BREAK
//			CASE FAC_LEAVE_DARTS_TIED			RETURN "FAC_LEAVE_DARTS_TIED" BREAK
//			CASE FAC_LEAVE_DARTS_QUIT			RETURN "FAC_LEAVE_DARTS_QUIT" BREAK
//			CASE FAC_LEAVE_DARTS_DRUNK			RETURN "FAC_LEAVE_DARTS_DRUNK" BREAK
//			CASE FAC_LEAVE_STRIPCLUB_WON		RETURN "FAC_LEAVE_STRIPCLUB_WON" BREAK
//			CASE FAC_LEAVE_STRIPCLUB_QUIT		RETURN "FAC_LEAVE_STRIPCLUB_QUIT" BREAK
//			CASE FAC_LEAVE_STRIPCLUB_DRUNK		RETURN "FAC_LEAVE_STRIPCLUB_DRUNK" BREAK
//			CASE FAC_LEAVE_CINEMA_WON			RETURN "FAC_LEAVE_CINEMA_WON" BREAK
//			CASE FAC_LEAVE_CINEMA_QUIT			RETURN "FAC_LEAVE_CINEMA_QUIT" BREAK
//			CASE FAC_LEAVE_CINEMA_DRUNK			RETURN "FAC_LEAVE_CINEMA_DRUNK" BREAK
				                               		
			CASE FAP_MISSIONZONE_STORY			RETURN "FAP_MISSIONZONE_STORY" BREAK
			CASE FAP_MISSIONZONE_SQUAD			RETURN "FAP_MISSIONZONE_SQUAD" BREAK
			CASE FAP_MISSIONZONE_DRUNK			RETURN "FAP_MISSIONZONE_DRUNK" BREAK
			CASE FAP_MISSIONZONE_CANCEL			RETURN "FAP_MISSIONZONE_CANCEL" BREAK
			CASE FAP_MISSIONZONE_CANCEL_DRUNK	RETURN "FAP_MISSIONZONE_CANCEL_DRUNK" BREAK
			
			CASE FAP_REJECTION_TIMEOUT			RETURN "FAP_REJECTION_TIMEOUT" BREAK
			CASE FAP_REJECTION_OK				RETURN "FAP_REJECTION_OK" BREAK
			CASE FAP_REJECTION_CRAZY			RETURN "FAP_REJECTION_CRAZY" BREAK
			CASE FAP_REJECTION_STUCK			RETURN "FAP_REJECTION_STUCK" BREAK

			CASE FAP_SQUAD_START				RETURN "FAP_SQUAD_START" BREAK
			CASE FAP_SQUAD_PASSED				RETURN "FAP_SQUAD_PASSED" BREAK

			CASE FAP_COMMENT_HAIRCUT			RETURN "FAP_COMMENT_HAIRCUT" BREAK
			CASE FAP_COMMENT_CLOTHES			RETURN "FAP_COMMENT_CLOTHES" BREAK
			CASE FAP_COMMENT_TATTOO				RETURN "FAP_COMMENT_TATTOO" BREAK

			CASE FAP_COMMENT_MELTDOWN			RETURN "FAP_COMMENT_MELTDOWN" BREAK

			CASE FAP_COMMENT_CARDAMAGE			RETURN "FAP_COMMENT_CARDAMAGE" BREAK
			CASE FAP_COMMENT_PEDDAMAGE			RETURN "FAP_COMMENT_PEDDAMAGE" BREAK

			CASE FAP_DROPOFF_OPENER				RETURN "FAP_DROPOFF_OPENER" BREAK
			CASE FAP_DROPOFF_OPENER_DRUNK		RETURN "FAP_DROPOFF_OPENER_DRUNK" BREAK
			CASE FAP_DROPOFF_OPENER_PLURAL		RETURN "FAP_DROPOFF_OPENER_PLURAL" BREAK

			CASE FAP_DROPOFF_OK					RETURN "FAP_DROPOFF_OK" BREAK
			CASE FAP_DROPOFF_IRATE				RETURN "FAP_DROPOFF_IRATE" BREAK
			CASE FAP_DROPOFF_VERY_IRATE			RETURN "FAP_DROPOFF_VERY_IRATE" BREAK
			CASE FAP_DROPOFF_DRUNK				RETURN "FAP_DROPOFF_DRUNK" BREAK
			CASE FAP_DROPOFF_GOTCAR				RETURN "FAP_DROPOFF_GOTCAR" BREAK

			CASE FAP_DEATH						RETURN "FAP_DEATH" BREAK
				                               		
			CASE MAX_FRIEND_ACTIVITY_PHRASES
				RETURN "MAX_FRIEND_ACTIVITY_PHRASES"
			BREAK
			CASE NO_FRIEND_ACTIVITY_PHRASE
				RETURN "NO_FRIEND_ACTIVITY_PHRASE"
			BREAK
			
		ENDSWITCH
		
		RETURN "invalid thisFriendActivityConv"
	ENDFUNC

	FUNC STRING GetLabel_enumFriendTextMessage(enumFriendTextMessage thisFriendTextMessage)
		SWITCH  thisFriendTextMessage
			CASE FTM_FRIEND_LOST				RETURN "FTM_FRIEND_LOST" BREAK
			CASE FTM_FRIEND_STOOD_UP			RETURN "FTM_FRIEND_STOOD_UP" BREAK
			CASE FTM_FRIEND_HOSPITAL			RETURN "FTM_FRIEND_HOSPITAL" BREAK
			CASE FTM_PLAYER_DIED				RETURN "FTM_PLAYER_DIED" BREAK
			CASE FTM_PLAYER_BUSTED				RETURN "FTM_PLAYER_BUSTED" BREAK
			
			CASE MAX_FRIEND_TEXT_MESSAGES
				RETURN "MAX_FRIEND_TEXT_MESSAGES"
			BREAK
			CASE NO_FRIEND_TEXT_MESSAGE
				RETURN "NO_FRIEND_TEXT_MESSAGE"
			BREAK
		ENDSWITCH
		
		RETURN "invalid thisFriendTextMessage"
	ENDFUNC

#ENDIF

// *******************************************************************************************
//	GENERAL DIALOGUE UTILS
// *******************************************************************************************

FUNC STRING PRIVATE_GetFriendCode(enumFriend eFriend)
	SWITCH eFriend
		CASE FR_MICHAEL
			RETURN "M"
		BREAK
		CASE FR_FRANKLIN
			RETURN "F"
		BREAK
		CASE FR_TREVOR
			RETURN "T"
		BREAK
		CASE FR_LAMAR
			RETURN "L"
		BREAK
		CASE FR_JIMMY
			RETURN "J"
		BREAK
		CASE FR_AMANDA
			RETURN "A"
		BREAK
	ENDSWITCH

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_GetFriendCode() Bad friend ID ", GetLabel_enumFriend(eFriend))
	SCRIPT_ASSERT("Invalid friend ID")
	RETURN "X"
ENDFUNC

FUNC STRING PRIVATE_Get_SpeakerLabel_From_Char(enumCharacterList friendCharID)
	SWITCH friendCharID
		CASE CHAR_MICHAEL
			RETURN "MICHAEL"
		BREAK
		CASE CHAR_FRANKLIN
			RETURN "FRANKLIN"
		BREAK
		CASE CHAR_TREVOR
			RETURN "TREVOR"
		BREAK
		CASE CHAR_LAMAR
			RETURN "LAMAR"
		BREAK
		CASE CHAR_JIMMY
			RETURN "JIMMY"
		BREAK
		CASE CHAR_AMANDA
			RETURN "AMANDA"
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_Get_SpeakerLabel_From_Char() Bad friend ID ", GetLabel_enumCharacterList(friendCharID))
	SCRIPT_ASSERT("invalid friendCharID in PRIVATE_Get_SpeakerLabel_From_Char()")
	RETURN "invalid"
ENDFUNC

FUNC INT PRIVATE_Get_SpeakerID_From_Char(enumCharacterList friendCharID)
	SWITCH friendCharID
		CASE CHAR_MICHAEL
			RETURN 0
		BREAK
		CASE CHAR_FRANKLIN
			RETURN 1
		BREAK
		CASE CHAR_TREVOR
			RETURN 2
		BREAK
		CASE CHAR_LAMAR
			RETURN 3
		BREAK
		CASE CHAR_JIMMY
			RETURN 4
		BREAK
		CASE CHAR_AMANDA
			RETURN 5
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_Get_SpeakerID_From_Char() Bad friend ID ", GetLabel_enumCharacterList(friendCharID))
	SCRIPT_ASSERT("invalid friendCharID in PRIVATE_Get_SpeakerID_From_Char()")
	RETURN 9
ENDFUNC
FUNC CC_TextPart PRIVATE_GetFriendTextMsgContextID(enumFriendTextMessage eFriendTextMessage, int iVariation)
	if  iVariation = 0 or iVariation = 1	
		switch eFriendTextMessage
			CASE FTM_FRIEND_LOST
				if iVariation = 0
					RETURN TPART_FRND_SIT_LOSTA
				else
					RETURN TPART_FRND_SIT_LOSTB
				endif
			BREAK
			CASE FTM_FRIEND_STOOD_UP		
				if iVariation = 0
					RETURN TPART_FRND_SIT_LATEA
				else
					RETURN TPART_FRND_SIT_LATEB
				endif	
			BREAK
			CASE FTM_FRIEND_HOSPITAL		
				if iVariation = 0
					RETURN TPART_FRND_SIT_HOSPA
				else
					RETURN TPART_FRND_SIT_HOSPB
				endif	
			BREAK
			CASE FTM_PLAYER_DIED			
				if iVariation = 0
					RETURN TPART_FRND_SIT_DIEDA
				else
					RETURN TPART_FRND_SIT_DIEDB
				endif	
			BREAK
			CASE FTM_PLAYER_BUSTED			
				if iVariation = 0
					RETURN TPART_FRND_SIT_BUSTA
				else
					RETURN TPART_FRND_SIT_BUSTB
				endif	
			BREAK		
		endswitch	
	endif	
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_GetFriendTextMsgContextID() Bad friend context, eMsg = ", ENUM_TO_INT(eFriendTextMessage), ", iVar = ", iVariation)
	SCRIPT_ASSERT("invalid enumFriendTextMessage in PRIVATE_GetFriendTextMsgContextID()")	
	RETURN TPART_FRND_SIT_BUSTA
ENDFUNC
PROC ADD_FRIEND_CHAR_FOR_DIALOGUE(structPedsForConversation& sPedForConv, enumCharacterList eFriendChar, PED_INDEX hPed = NULL, BOOL bVerbose = TRUE)
	
	TEXT_LABEL tLabel	= PRIVATE_Get_SpeakerLabel_From_Char(eFriendChar)
	INT iID				= PRIVATE_Get_SpeakerID_From_Char(eFriendChar)

	IF bVerbose
		#IF IS_DEBUG_BUILD
			TEXT_LABEL tChar = GetLabel_enumCharacterList(eFriendChar)
			CPRINTLN(DEBUG_FRIENDS, "ADD_FRIEND_FOR_CHAR_DIALOGUE(", tChar, ") = speaker: ", tLabel, " (", iID, ")")
		#ENDIF
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(sPedForConv, iID, hPed, tLabel)
ENDPROC

// *******************************************************************************************
//	Get block name / root stub
// *******************************************************************************************

FUNC TEXT_LABEL PRIVATE_FriendDialogue_GetAudioCode(enumFriend ePlayer, enumFriend eFriendA, enumFriend eFriendB, enumFriendAudioBlock eBlock)

	TEXT_LABEL tBase = "F"
	
	IF eBlock = FAB_phone
		tBase += "p"
		tBase += PRIVATE_GetFriendCode(ePlayer)
		tBase += PRIVATE_GetFriendCode(eFriendA)
		
	ELIF eBlock = FAB_activity
		tBase += "a"
		tBase += PRIVATE_GetFriendCode(ePlayer)
		tBase += PRIVATE_GetFriendCode(eFriendA)
	
	ELIF eBlock = FAB_chat
		tBase += "c"
		
		IF ENUM_TO_INT(eFriendB) < ENUM_TO_INT(eFriendA)
			enumFriend temp = eFriendA
			eFriendA = eFriendB
			eFriendB = temp
		ENDIF
		
		IF ENUM_TO_INT(eFriendA) < ENUM_TO_INT(ePlayer)
			enumFriend temp = ePlayer
			ePlayer = eFriendA
			eFriendA = temp
		ENDIF
		
		IF ENUM_TO_INT(eFriendB) < ENUM_TO_INT(eFriendA)
			enumFriend temp = eFriendA
			eFriendA = eFriendB
			eFriendB = temp
		ENDIF
		
		tBase += PRIVATE_GetFriendCode(ePlayer)
		tBase += PRIVATE_GetFriendCode(eFriendA)
		IF eFriendB <> NO_FRIEND
			tBase += PRIVATE_GetFriendCode(eFriendB)
		ENDIF
		
	ELSE
		tBase += "x"
		tBase += PRIVATE_GetFriendCode(ePlayer)
		tBase += PRIVATE_GetFriendCode(eFriendA)
		IF eFriendB <> NO_FRIEND
			tBase += PRIVATE_GetFriendCode(eFriendB)
		ENDIF
		SCRIPT_ASSERT("PRIVATE_Friend_GetAudioBlock() - Unknown audio block type")
	ENDIF
	
	RETURN tBase

ENDFUNC

// *******************************************************************************************
//	Get dialogue phrases
// *******************************************************************************************

FUNC BOOL PRIVATE_FriendDialogue_GetAnswerphonePhrase(enumFriend eFriend, TEXT_LABEL& tBlock, TEXT_LABEL& tRoot)
	
	tBlock = "ANAUD"
	
	SWITCH eFriend
		CASE FR_MICHAEL
			tRoot = "MIC_APH1"
		BREAK
		CASE FR_FRANKLIN
			tRoot = "ARI_APH1"
		BREAK
		CASE FR_TREVOR
			tRoot = "TRE_APH1"
		BREAK
		CASE FR_LAMAR
			tRoot = "LAM_APH1"
		BREAK
		CASE FR_JIMMY
			tRoot = "JIM_APH1"
		BREAK
		CASE FR_AMANDA
			tRoot = "AMA_APH1"
		BREAK
		
		DEFAULT
			tRoot = "XXX_APH1"
			#IF IS_DEBUG_BUILD
				tRoot = GetLabel_enumFriend(eFriend)
				SCRIPT_ASSERT("PRIVATE_FriendDialogue_GetAnswerphonePhrase() - invalid eBuddy")
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Phone phrases
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_GetFriendPhonePhrase(enumCharacterList ePlayerChar, enumCharacterList eFriendChar, enumFriendPhonePhrase ePhrase, TEXT_LABEL& tBlock, TEXT_LABEL& tRoot)
	
	// Get friend IDs
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(ePlayerChar)
	enumFriend eFriend = GET_FRIEND_FROM_CHAR(eFriendChar)
	IF ePlayer = NO_FRIEND
	OR eFriend = NO_FRIEND
		RETURN FALSE
	ENDIF

	// Get block and root stub
	tRoot = PRIVATE_FriendDialogue_GetAudioCode(ePlayer, eFriend, NO_FRIEND, FAB_phone)
	tBlock = tRoot
	tBlock += "AU"
	
	// Get specific part of root label
	tRoot += "_"
	
	SWITCH ePhrase
		CASE FPP_OUTGOING_GREET1					tRoot += "G1" 		BREAK
		CASE FPP_OUTGOING_GREET2					tRoot += "G2" 		BREAK
		CASE FPP_OUTGOING_GREET2_MEETINGMICHAEL		tRoot += "G2_mm" 	BREAK
		CASE FPP_OUTGOING_GREET2_MEETINGFRANKLIN	tRoot += "G2_mf" 	BREAK
		CASE FPP_OUTGOING_GREET2_MEETINGTREVOR		tRoot += "G2_mt" 	BREAK
		CASE FPP_OUTGOING_GREET2_MEETINGLAMAR		tRoot += "G2_ml" 	BREAK
		CASE FPP_OUTGOING_YES1_OK					tRoot += "Y1_ok" 	BREAK
		CASE FPP_OUTGOING_YES1_IRATE				tRoot += "Y1_ir" 	BREAK
		CASE FPP_OUTGOING_YES1_VERYIRATE			tRoot += "Y1_vi" 	BREAK
		CASE FPP_OUTGOING_YES1_FIRSTTIME			tRoot += "Y1_fs" 	BREAK
		CASE FPP_OUTGOING_YES1_LIKELOW				tRoot += "Y1_lo" 	BREAK
		CASE FPP_OUTGOING_YES1_BUSY					tRoot += "Y1_bs" 	BREAK
		CASE FPP_OUTGOING_YES1_COMETOYOU			tRoot += "Y1_cm" 	BREAK
		CASE FPP_OUTGOING_YES2_OK					tRoot += "Y2_ok" 	BREAK
		CASE FPP_OUTGOING_YES2_COMETOYOU			tRoot += "Y2_cm" 	BREAK
		CASE FPP_OUTGOING_NO1_OK					tRoot += "N1_ok" 	BREAK
		CASE FPP_OUTGOING_NO1_RECENTFACE			tRoot += "N1_fc" 	BREAK
		CASE FPP_OUTGOING_NO1_RECENTPHONE			tRoot += "N1_ph" 	BREAK
		CASE FPP_OUTGOING_NO1_BLOCKED				tRoot += "N1_bl" 	BREAK
		CASE FPP_OUTGOING_NO2						tRoot += "N2" 		BREAK
		
		CASE FPP_BACKUP								tRoot += "B1"	 	BREAK
		CASE FPP_BACKUP_DOCKS_2						tRoot += "B1_d2" 	BREAK
		CASE FPP_BACKUP_RURAL						tRoot += "B1_r"	 	BREAK
		CASE FPP_BACKUP_AGENCY						tRoot += "B1_a"	 	BREAK
		CASE FPP_BACKUP_FINALE_A					tRoot += "B1_b1" 	BREAK
		CASE FPP_BACKUP_FINALE_B					tRoot += "B1_b2"	BREAK
		CASE FPP_BACKUP_FBI_1						tRoot += "B1_f1"	BREAK
		CASE FPP_BACKUP_FBI_2						tRoot += "B1_f2"	BREAK
		CASE FPP_BACKUP_FBI_4						tRoot += "B1_f4"	BREAK
		CASE FPP_BACKUP_FBI_5						tRoot += "B1_f5"	BREAK

		CASE FPP_CANCEL1							tRoot += "Cncl" 	BREAK
		CASE FPP_CANCEL2_HILIKE						tRoot += "CnclH" 	BREAK
		CASE FPP_CANCEL2_MEDLIKE					tRoot += "CnclM" 	BREAK
		CASE FPP_CANCEL2_LOWLIKE					tRoot += "CnclL" 	BREAK
		CASE FPP_CANCEL2_FIRSTTIME					tRoot += "CnclF" 	BREAK

		CASE FPP_LAYLOW1							tRoot += "Lay1"	 	BREAK
		CASE FPP_LAYLOW2							tRoot += "Lay2"	 	BREAK
		CASE FPP_LAYLOW3							tRoot += "Lay3"	 	BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				tRoot += ENUM_TO_INT(ePhrase)
				SCRIPT_ASSERT("Private_GetFriendPhonePhrase() - invalid ePhrase")
			#ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Activity phrases
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_GetFriendActivityPhrase(enumCharacterList ePlayerChar, enumCharacterList eFriendChar, enumFriendActivityPhrase eActivityPhrase, TEXT_LABEL& tBlock, TEXT_LABEL& tRoot)
	
	// Get friend IDs
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(ePlayerChar)
	enumFriend eFriend = GET_FRIEND_FROM_CHAR(eFriendChar)
	IF ePlayer = NO_FRIEND
	OR eFriend = NO_FRIEND
		RETURN FALSE
	ENDIF

	// Get block and root stub
	tRoot = PRIVATE_FriendDialogue_GetAudioCode(ePlayer, eFriend, NO_FRIEND, FAB_activity)
	tBlock = tRoot
	tBlock += "AU"
	
	// Get specific part of root label
	tRoot += "_"

	SWITCH eActivityPhrase
		CASE FAP_PICKUP_OK					tRoot += "PkOk"		BREAK    
		CASE FAP_PICKUP_LATE				tRoot += "PkLt"		BREAK
		CASE FAP_PICKUP_NOCAR				tRoot += "PkFt"		BREAK
		CASE FAP_PICKUP_FULLCAR				tRoot += "PkFul"	BREAK
		CASE FAP_PICKUP_ODDCAR				tRoot += "PkOdd"	BREAK
		CASE FAP_PICKUP_YOUDRIVE			tRoot += "PkUDr"	BREAK
		
		CASE FAP_GREET_OK					tRoot += "GrOk"		BREAK
		CASE FAP_GREET_IRATE				tRoot += "GrIr"		BREAK
		CASE FAP_GREET_VIRATE				tRoot += "GrVi"		BREAK
		CASE FAP_GREET_PLURAL				tRoot += "GrPl"		BREAK
		CASE FAP_GREET_YOUDRIVE				tRoot += "GrUDr"	BREAK

		CASE FAP_AMBIENT_HELLO_OK			tRoot += "AmbH_ok"	BREAK
		CASE FAP_AMBIENT_HELLO_RECENT		tRoot += "AmbH_rc"	BREAK
		CASE FAP_AMBIENT_HELLO_IRATE		tRoot += "AmbH_ir"	BREAK

		CASE FAP_AMBIENT_BLOCKED_MISSION	tRoot += "AmbB_mi"	BREAK
		CASE FAP_AMBIENT_BLOCKED_OK			tRoot += "AmbB_ok"	BREAK
		
		CASE FAP_AMBIENT_OFFER_QUESTION		tRoot += "AmbO_qu"	BREAK
		CASE FAP_AMBIENT_OFFER_YES1			tRoot += "AmbO_Y1"	BREAK
		CASE FAP_AMBIENT_OFFER_YES2			tRoot += "AmbO_Y2"	BREAK
		CASE FAP_AMBIENT_OFFER_NO1			tRoot += "AmbO_N1"	BREAK
		
		CASE FAP_AMBIENT_GOODBYE_OK			tRoot += "AmbG_ok"	BREAK
		CASE FAP_AMBIENT_GOODBYE_IRATE		tRoot += "AmbG_ir"	BREAK
		
		CASE FAP_AMBIENT_STALK_1			tRoot += "AmbS1"	BREAK
		CASE FAP_AMBIENT_STALK_2			tRoot += "AmbS2"	BREAK
		CASE FAP_AMBIENT_STALK_3			tRoot += "AmbS3"	BREAK
//		CASE FAP_AMBIENT_STALK_4			tRoot += "AmbS4"	BREAK
		CASE FAP_AMBIENT_STALK_4_1			tRoot += "AmbS4_1"	BREAK
		CASE FAP_AMBIENT_STALK_4_2			tRoot += "AmbS4_2"	BREAK
		CASE FAP_AMBIENT_STALK_4_3			tRoot += "AmbS4_3"	BREAK
		CASE FAP_AMBIENT_STALK_4_4			tRoot += "AmbS4_4"	BREAK

		CASE FAP_AMBIENT_STALK_FT			tRoot += "AmbS_ft"	BREAK
		
		CASE FAP_AMBIENT_TRAILER_1			tRoot += "AmbT1"	BREAK
		CASE FAP_AMBIENT_TRAILER_1r			tRoot += "AmbT1_r"	BREAK
		CASE FAP_AMBIENT_TRAILER_2			tRoot += "AmbT2"	BREAK
		CASE FAP_AMBIENT_TRAILER_3			tRoot += "AmbT3"	BREAK
		CASE FAP_AMBIENT_TRAILER_4			tRoot += "AmbT4"	BREAK
		
		CASE FAP_AMBIENT_POO_1a				tRoot += "AmbP1a"	BREAK
		CASE FAP_AMBIENT_POO_1b				tRoot += "AmbP1b"	BREAK
		CASE FAP_AMBIENT_POO_1c				tRoot += "AmbP1c"	BREAK
		CASE FAP_AMBIENT_POO_2				tRoot += "AmbP2"	BREAK

		CASE FAP_VERY_DRUNK					tRoot += "VDrunk" 	BREAK
		CASE FAP_SATISFIED					tRoot += "Satis" 	BREAK
		CASE FAP_DONE						tRoot += "Done" 	BREAK

		CASE FAP_MISSIONZONE_STORY			tRoot += "ZnStory" 	BREAK
		CASE FAP_MISSIONZONE_SQUAD			tRoot += "ZnSquad" 	BREAK
		CASE FAP_MISSIONZONE_DRUNK			tRoot += "ZnDrunk" 	BREAK
		CASE FAP_MISSIONZONE_CANCEL			tRoot += "ZnCncl" 	BREAK
		CASE FAP_MISSIONZONE_CANCEL_DRUNK	tRoot += "ZnCnclD" 	BREAK

		CASE FAP_REJECTION_TIMEOUT			tRoot += "RjTime" 	BREAK
		CASE FAP_REJECTION_OK				tRoot += "RjOk" 	BREAK
		CASE FAP_REJECTION_CRAZY			tRoot += "RjCrazy" 	BREAK
		CASE FAP_REJECTION_STUCK			tRoot += "RjStuck" 	BREAK

		CASE FAP_SQUAD_START				tRoot += "SqStart" 	BREAK
		CASE FAP_SQUAD_PASSED				tRoot += "SqPass"	BREAK

		CASE FAP_COMMENT_HAIRCUT			tRoot += "NewH"		BREAK
		CASE FAP_COMMENT_CLOTHES			tRoot += "NewC"		BREAK
		CASE FAP_COMMENT_TATTOO				tRoot += "NewT"		BREAK

		CASE FAP_COMMENT_MELTDOWN			tRoot += "CinMlt"	BREAK

		CASE FAP_COMMENT_CARDAMAGE			tRoot += "CarDam"	BREAK
		CASE FAP_COMMENT_PEDDAMAGE			tRoot += "PedDam"	BREAK

		CASE FAP_DROPOFF_OPENER				tRoot += "DrpOp" 	BREAK
		CASE FAP_DROPOFF_OPENER_DRUNK		tRoot += "DrpOpDr" 	BREAK
		CASE FAP_DROPOFF_OPENER_PLURAL		tRoot += "DrpOpPl" 	BREAK

		CASE FAP_DROPOFF_OK					tRoot += "DrpA" 	BREAK
		CASE FAP_DROPOFF_IRATE				tRoot += "DrpB" 	BREAK
		CASE FAP_DROPOFF_VERY_IRATE			tRoot += "DrpC" 	BREAK
		CASE FAP_DROPOFF_DRUNK				tRoot += "DrpDr" 	BREAK
		CASE FAP_DROPOFF_GOTCAR				tRoot += "DrpCar" 	BREAK
			                               		
		CASE FAP_DEATH						tRoot += "Death" 	BREAK

		DEFAULT
			IF eActivityPhrase <> NO_FRIEND_ACTIVITY_PHRASE
				#IF IS_DEBUG_BUILD
					tRoot += "_"
					tRoot += ENUM_TO_INT(eActivityPhrase)
					
					SCRIPT_ASSERT("Private_GetFriendActivityPhrase() - invalid eActivityPhrase")
				#ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL Private_GetFriendActivitySuggestion(enumCharacterList ePlayerChar, enumCharacterList eFriendChar, enumActivityLocation eLoc, TEXT_LABEL& tBlock, TEXT_LABEL& tRoot)
	
	// Get friend IDs
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(ePlayerChar)
	enumFriend eFriend = GET_FRIEND_FROM_CHAR(eFriendChar)
	IF ePlayer = NO_FRIEND
	OR eFriend = NO_FRIEND
		RETURN FALSE
	ENDIF

	// Get block and root stub
	tRoot = PRIVATE_FriendDialogue_GetAudioCode(ePlayer, eFriend, NO_FRIEND, FAB_activity)
	tBlock = tRoot
	tBlock += "AU"
	
	// Get activity type part of root label
	tRoot += "_Sg"

	SWITCH(g_ActivityLocations[eLoc].type)
		CASE ATYPE_cinema		tRoot += "Cin"	BREAK
		
		CASE ATYPE_golf			tRoot += "Glf"	BREAK
		CASE ATYPE_stripclub	tRoot += "Str"	BREAK
		CASE ATYPE_tennis		tRoot += "Tns"	BREAK
		CASE ATYPE_darts		tRoot += "Drt"	BREAK
		CASE ATYPE_bar			tRoot += "Bar"	BREAK

		DEFAULT
			tRoot += "BAD"
			RETURN FALSE
		BREAK
	ENDSWITCH

	RETURN TRUE
	
ENDFUNC

FUNC BOOL Private_GetFriendActivityResult(enumCharacterList ePlayerChar, enumCharacterList eFriendChar,
													 enumActivityLocation activityLoc, enumActivityResult activityResult, BOOL bIsDrunk,
													 TEXT_LABEL& tBlock, TEXT_LABEL& tRoot)
	
	// Get friend IDs
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(ePlayerChar)
	enumFriend eFriend = GET_FRIEND_FROM_CHAR(eFriendChar)
	IF ePlayer = NO_FRIEND
	OR eFriend = NO_FRIEND
		RETURN FALSE
	ENDIF

	// Get block and root stub
	tRoot = PRIVATE_FriendDialogue_GetAudioCode(ePlayer, eFriend, NO_FRIEND, FAB_activity)
	tBlock = tRoot
	tBlock += "AU"
	
	// Get activity type part of root label
	enumActivityType activityType = g_ActivityLocations[activityLoc].type
	tRoot += "_"
	
	SWITCH(activityType)
		CASE ATYPE_cinema		tRoot += "Cin"	BREAK
		CASE ATYPE_bar			tRoot += "Bar"	BREAK
		
		CASE ATYPE_golf			tRoot += "Glf"	BREAK
		CASE ATYPE_stripclub	tRoot += "Str"	BREAK
		CASE ATYPE_tennis		tRoot += "Tns"	BREAK
		CASE ATYPE_darts		tRoot += "Drt"	BREAK
		
		DEFAULT
			tRoot += "BAD"
			RETURN FALSE
		BREAK
	ENDSWITCH

	// Get activity result part of root label
	IF bIsDrunk AND (activityType != ATYPE_golf AND activityType != ATYPE_tennis)
		tRoot += "Dr"
	ELSE
		SWITCH(activityResult)
			CASE AR_playerWon		tRoot += "Wn"	BREAK
			CASE AR_buddyA_won		tRoot += "Ls"	BREAK
			CASE AR_buddyB_won		tRoot += "Ls"	BREAK
			CASE AR_playerDraw		tRoot += "Ti"	BREAK
			CASE AR_playerQuit		tRoot += "Qt"	BREAK
			
			DEFAULT
				tRoot += "XX"
				RETURN FALSE
			BREAK
		ENDSWITCH
	ENDIF

	RETURN TRUE
	
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Chat conversations
//---------------------------------------------------------------------------------------------------

//FUNC BOOL Private_GetFriendChatData(structFriendChatData& chatData, enumCharacterList ePlayerChar, enumCharacterList eFriendCharA, enumCharacterList eFriendCharB)
//	
//	IF eFriendB = NO_FRIEND
//		// Get conversation state (single hangout)
//		enumFriendConnection eConnection = GET_CONNECTION_FROM_FRIENDS(ePlayer, eFriendA)
//		IF eConnection >= MAX_FRIEND_CONNECTIONS
//			SCRIPT_ASSERT("Private_GetFriendChat(): No connection between player/friendA")
//			RETURN FALSE
//		ENDIF
//		
//		chatData = g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnection].chatData
//
//	ELSE
//		// Get conversation state (multiple hangout)
//		enumFriendGroup eGroup = GET_GROUP_FROM_FRIENDS(ePlayer, eFriendA, eFriendB)
//		IF eGroup >= MAX_FRIEND_GROUPS
//			SCRIPT_ASSERT("Private_GetFriendChat(): No connection between player/friendA/friendB")
//			RETURN FALSE
//		ENDIF
//		
//		chatData = g_SavedGlobals.sFriendsData.g_FriendGroupData[eGroup].chatData
//	ENDIF
//	
//	RETURN TRUE
//
//ENDFUNC

FUNC BOOL Private_GetFriendChatIndex(structFriendChatData& chatData, enumFriendChatType eType, INT& iBank, INT &iIndex, INT iAllowedConditionsBitset)
	
	REPEAT CONST_iFriendChatMaxBanks iBank

		INT iFullCount=0, iMiniCount=0, iDrunkCount=0
		INT iFullTotal=0, iMiniTotal=0, iDrunkTotal=0
		enumFriendChatCondition eBankCondition = FCHAT_ConditionNever
		
		// Is this bank allowed?
		Private_GetFriendChatBankCondition(chatData.banks[iBank], eBankCondition)
		IF IS_BIT_SET(iAllowedConditionsBitset, ENUM_TO_INT(eBankCondition))
		
			// Get bank data
			Private_GetFriendChatBankCounts(chatData.banks[iBank], iFullCount, iMiniCount, iDrunkCount)
			Private_GetFriendChatBankTotals(chatData.banks[iBank], iFullTotal, iMiniTotal, iDrunkTotal)

			// Does the bank have any of the required chat type?
			IF eType = FCHAT_TypeFull
				IF iFullCount < iFullTotal
					iIndex = iFullCount
					RETURN TRUE
				ENDIF

			ELIF eType = FCHAT_TypeMini
				IF iMiniCount < iMiniTotal
					iIndex = iMiniCount
					RETURN TRUE
				ENDIF

			ELIF eType = FCHAT_TypeDrunk
				IF iDrunkCount < iDrunkTotal
					iIndex = iDrunkCount
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	iBank = 0
	iIndex = 0
	RETURN FALSE

ENDFUNC

PROC Private_IncrementFriendChatIndex(structFriendChatData& chatData, enumFriendChatType eType, INT iBank)
	
	INT iFullCount=0, iMiniCount=0, iDrunkCount=0
	INT iFullTotal=0, iMiniTotal=0, iDrunkTotal=0
	enumFriendChatCondition eBankCondition = FCHAT_ConditionNever
	
	// Get data
	Private_GetFriendChatBankCounts(chatData.banks[iBank], iFullCount, iMiniCount, iDrunkCount)
	Private_GetFriendChatBankTotals(chatData.banks[iBank], iFullTotal, iMiniTotal, iDrunkTotal)
	Private_GetFriendChatBankCondition(chatData.banks[iBank], eBankCondition)

	// Increment relevant counter
	IF eType = FCHAT_TypeFull
		iFullCount++
		IF iFullCount > iFullTotal
			iFullCount = iFullTotal
		ENDIF

	ELIF eType = FCHAT_TypeMini
		iMiniCount++
		IF iMiniCount > iMiniTotal
			iMiniCount = iMiniTotal
		ENDIF

	ELIF eType = FCHAT_TypeDrunk
		iDrunkCount++
		IF iDrunkCount > iDrunkTotal
			iDrunkCount = iDrunkTotal
		ENDIF
	ENDIF
	
	// Store data again
	CPRINTLN(DEBUG_FRIENDS, "Private_IncrementFriendChatIndex(", iFullCount, ", ", iMiniCount, ", ", iDrunkCount, ", - ", iFullTotal, ", ", iMiniTotal, ", ", iDrunkTotal, ")")
	Private_SetFriendChatBankData(chatData.banks[iBank],
									iFullCount, iMiniCount, iDrunkCount,
									iFullTotal, iMiniTotal, iDrunkTotal,
									eBankCondition)

ENDPROC


FUNC BOOL Private_GetFriendChat(enumCharacterList ePlayerChar, enumCharacterList eFriendAChar, enumCharacterList eFriendBChar, enumFriendChatType eChatType, INT iConditions, TEXT_LABEL& tBlock, TEXT_LABEL& tRoot, BOOL bIncrement = FALSE)
	
	// Get friend IDs
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(ePlayerChar)
	enumFriend eFriendA = GET_FRIEND_FROM_CHAR(eFriendAChar)
	enumFriend eFriendB = GET_FRIEND_FROM_CHAR(eFriendBChar)
	IF ePlayer = NO_FRIEND
	OR eFriendA = NO_FRIEND
	OR (eFriendB = NO_FRIEND AND eFriendBChar <> NO_CHARACTER)
		RETURN FALSE
	ENDIF

	//-- Get bank and index from 2:1 or 1:1 chat data
	INT iChatBank, iChatIndex
	
	IF eFriendB = NO_FRIEND
		// Get conversation state (single hangout)
		enumFriendConnection eConnection = GET_CONNECTION_FROM_FRIENDS(ePlayer, eFriendA)
		IF eConnection >= MAX_FRIEND_CONNECTIONS
			SCRIPT_ASSERT("Private_GetFriendChat(): No connection between player/friendA")
			RETURN FALSE
		ENDIF
	
		IF ePlayerChar = CHAR_FRANKLIN 
		AND eFriendAChar = CHAR_LAMAR 
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		AND GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE) 
	#endif
	#endif		
		AND eChatType = FCHAT_TypeMini
	
			// Special case: Franklin/Lamar end of game chat banks, stored in global data
			IF Private_GetFriendChatIndex(g_SavedGlobals.sFriendsData.g_FranklinLamarEndChat, eChatType, iChatBank, iChatIndex, iConditions)
				IF bIncrement
					Private_IncrementFriendChatIndex(g_SavedGlobals.sFriendsData.g_FranklinLamarEndChat, eChatType, iChatBank)
				ENDIF
				iChatBank += 6
			ELSE
				RETURN FALSE
			ENDIF
			
		ELSE
	
			// Normal 1:1 chat banks, stored in connections
			IF Private_GetFriendChatIndex(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnection].chatData, eChatType, iChatBank, iChatIndex, iConditions)
				IF bIncrement
					Private_IncrementFriendChatIndex(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnection].chatData, eChatType, iChatBank)
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		
		ENDIF

	ELSE
		// Get conversation state (multiple hangout)
		enumFriendGroup eGroup = GET_GROUP_FROM_FRIENDS(ePlayer, eFriendA, eFriendB)
		IF eGroup >= MAX_FRIEND_GROUPS
			SCRIPT_ASSERT("Private_GetFriendChat(): No connection between player/friendA/friendB")
			RETURN FALSE
		ENDIF
		
		// Normal 3-way chat banks, stored in groups
		IF Private_GetFriendChatIndex(g_SavedGlobals.sFriendsData.g_FriendGroupData[eGroup].chatData, eChatType, iChatBank, iChatIndex, iConditions)
			IF bIncrement
				Private_IncrementFriendChatIndex(g_SavedGlobals.sFriendsData.g_FriendGroupData[eGroup].chatData, eChatType, iChatBank)
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDIF

	//-- Build root...
	tRoot = PRIVATE_FriendDialogue_GetAudioCode(ePlayer, eFriendA, eFriendB, FAB_chat)
	tBlock = tRoot
	tBlock += "AU"
	tRoot += "_"

	// Bank code
	IF iChatBank = 0					tRoot += "A"
	ELIF iChatBank = 1					tRoot += "B"
	ELIF iChatBank = 2					tRoot += "C"
	ELIF iChatBank = 3					tRoot += "D"
	ELIF iChatBank = 4					tRoot += "E"
	ELIF iChatBank = 5					tRoot += "F"
	// (extended Franklin/Lamar special banks)
	ELIF iChatBank = 6					tRoot += "G"
	ELIF iChatBank = 7					tRoot += "H"
	ELIF iChatBank = 8					tRoot += "I"
	ELIF iChatBank = 9					tRoot += "J"
	ELIF iChatBank = 10					tRoot += "K"
	ELIF iChatBank = 11					tRoot += "L"
	ELSE
		tRoot += "X"
		CPRINTLN(DEBUG_FRIENDS, "Private_GetFriendChat(): Unknown chat data bank", iChatBank)
		SCRIPT_ASSERT("Private_GetFriendChat(): Unknown chat data bank")
		RETURN FALSE
	ENDIF
	
	// Type code
	IF eChatType = FCHAT_TypeFull		tRoot += "F"
	ELIF eChatType = FCHAT_TypeMini		tRoot += "M"
	ELIF eChatType = FCHAT_TypeDrunk	tRoot += "D"
	ELSE
		tRoot += "X"
		CPRINTLN(DEBUG_FRIENDS, "Private_GetFriendChat(): Unknown chat type", eChatType)
		SCRIPT_ASSERT("Private_GetFriendChat(): Unknown chat type")
		RETURN FALSE
	ENDIF

	// Index
	tRoot += iChatIndex
	
	RETURN TRUE

ENDFUNC

//---------------------------------------------------------------------------------------------------
//-- Ambient conversations
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_GetAmbFriendChat(enumCharacterList ePlayerChar, enumCharacterList eFriendChar, INT iIndex, TEXT_LABEL& tBlock, TEXT_LABEL& tRoot)
	
	// Get friend IDs
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(ePlayerChar)
	enumFriend eFriend = GET_FRIEND_FROM_CHAR(eFriendChar)
	IF ePlayer = NO_FRIEND
	OR eFriend = NO_FRIEND
		RETURN FALSE
	ENDIF

	// Get block and root stub
	tRoot = PRIVATE_FriendDialogue_GetAudioCode(ePlayer, eFriend, NO_FRIEND, FAB_chat)
	tBlock = tRoot
	tBlock += "AU"
	
	// Get specific part of root label
	tRoot += "_Amb"
	tRoot += iIndex

	RETURN TRUE

ENDFUNC

//---------------------------------------------------------------------------------------------------
//-- Text messages
//---------------------------------------------------------------------------------------------------

CONST_INT CONST_iMaxFriendTextVariations	2

FUNC BOOL Private_GetFriendTextMessageLabel(enumFriend eSender, enumFriendTextMessage eTxtMsg, INT iVariation, TEXT_LABEL& r_tLabel)

	IF eTxtMsg >= MAX_FRIEND_TEXT_MESSAGES
		CPRINTLN(DEBUG_FRIENDS, "Private_GetFriendTextMessageLabel() - Invalid text message ID: ", GetLabel_enumFriendTextMessage(eTxtMsg))
		SCRIPT_ASSERT("Private_GetFriendTextMessageLabel() - Invalid text message ID")
	ELSE
		IF iVariation >= CONST_iMaxFriendTextVariations
		OR iVariation < -1
			SCRIPT_ASSERT("Private_GetFriendTextMessageLabel() - iVariation out of range, setting to random variation")
			iVariation = -1
		ENDIF
		
		//-- Get text message code
		TEXT_LABEL str = "FTX_"
		str += PRIVATE_GetFriendCode(eSender)
		str += "_"	
			
		SWITCH eTxtMsg
			CASE FTM_FRIEND_LOST			str += "LOST"	BREAK
			CASE FTM_FRIEND_STOOD_UP		str += "LATE"	BREAK
			CASE FTM_FRIEND_HOSPITAL		str += "HOSP"	BREAK
			CASE FTM_PLAYER_DIED			str += "PDIED"	BREAK
			CASE FTM_PLAYER_BUSTED			str += "PBUST"	BREAK
			
			DEFAULT
				str += "X"
				str += ENUM_TO_INT(eTxtMsg)
				SCRIPT_ASSERT("Private_GetFriendTextMessageLabel() - Invalid eFriendTextMessage")			
				r_tLabel = str
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		//-- Choose variation
		TEXT_LABEL tVariationLabels[CONST_iMaxFriendTextVariations]
		tVariationLabels[0] = str		tVariationLabels[0] += "a"
		tVariationLabels[1] = str		tVariationLabels[1] += "b"
		
		IF iVariation = -1
			enumCharacterList eSenderChar = GET_CHAR_FROM_FRIEND(eSender)
			
			// Try to chose a message that hasn't been used yet
			IF CHECK_FOR_MESSAGE_PRESENCE_IN_CHARACTER_MESSAGE_LIST(tVariationLabels[0], eSenderChar)
				iVariation = 1
			
			ELIF CHECK_FOR_MESSAGE_PRESENCE_IN_CHARACTER_MESSAGE_LIST(tVariationLabels[1], eSenderChar)
				iVariation = 0
			
			ELSE
				iVariation = GET_RANDOM_INT_IN_RANGE(0, CONST_iMaxFriendTextVariations)
			ENDIF
		ENDIF
		
		r_tLabel = tVariationLabels[iVariation]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL Private_GetFriendTextMessageParts(enumFriend eSender, enumFriendTextMessage eTxtMsg, INT iVariation, CC_TextPart& r_eSenderPart, CC_TextPart& r_eContextPart)  //(enumFriend eSender, enumFriendTextMessage eFriendTextMessage,INT iBitsetPlayerTo, INT iVariation = -1,INT iThisFriendInitialDelay = 10000,CC_CommunicationType eCommunicationType = CT_FRIEND,CC_CodeID eCID = CID_BLANK)
	
	IF eTxtMsg >= MAX_FRIEND_TEXT_MESSAGES
		CPRINTLN(DEBUG_FRIENDS, "Private_GetFriendTextMessageParts() - Invalid text message ID: ", GetLabel_enumFriendTextMessage(eTxtMsg))
		SCRIPT_ASSERT("Private_GetFriendTextMessageParts() - Invalid text message ID")
	ELSE
		IF iVariation >= CONST_iMaxFriendTextVariations
		OR iVariation < -1
			SCRIPT_ASSERT("Private_GetFriendTextMessageParts() - iVariation out of range, setting to random variation")
			iVariation = -1
		ENDIF

		//-- Get sender part ID
		SWITCH eSender
			CASE FR_MICHAEL 	r_eSenderPart = TPART_FRND_MIKE			BREAK
			CASE FR_FRANKLIN	r_eSenderPart = TPART_FRND_FRANK		BREAK	
			CASE FR_TREVOR		r_eSenderPart =	TPART_FRND_TREVOR		BREAK
			CASE FR_LAMAR		r_eSenderPart = TPART_FRND_LAMAR		BREAK
			CASE FR_JIMMY		r_eSenderPart = TPART_FRND_JIMMY		BREAK
			CASE FR_AMANDA		r_eSenderPart = TPART_FRND_AMANDA		BREAK
		ENDSWITCH
				
		//-- Choose variation
		IF iVariation = -1
			TEXT_LABEL tVariationLabels[CONST_iMaxFriendTextVariations]
			IF  Private_GetFriendTextMessageLabel(eSender, eTxtMsg, 0, tVariationLabels[0])
			AND Private_GetFriendTextMessageLabel(eSender, eTxtMsg, 1, tVariationLabels[1])

				enumCharacterList eSenderChar = GET_CHAR_FROM_FRIEND(eSender)
				
				// Try to chose a message that hasn't been used yet
				IF CHECK_FOR_MESSAGE_PRESENCE_IN_CHARACTER_MESSAGE_LIST(tVariationLabels[0], eSenderChar)
					iVariation = 1
				
				ELIF CHECK_FOR_MESSAGE_PRESENCE_IN_CHARACTER_MESSAGE_LIST(tVariationLabels[1], eSenderChar)
					iVariation = 0
				
				ELSE
					iVariation = GET_RANDOM_INT_IN_RANGE(0, CONST_iMaxFriendTextVariations)
				ENDIF
				
			ELSE
				iVariation = GET_RANDOM_INT_IN_RANGE(0, CONST_iMaxFriendTextVariations)
			ENDIF
		ENDIF
		
		//-- Get context part ID
		r_eContextPart = PRIVATE_GetFriendTextMsgContextID(eTxtMsg, iVariation)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
		
ENDFUNC
// *******************************************************************************************
//	SEND TEXT MESSAGE
// *******************************************************************************************

// Really sorry I've butchered this one Sam. -BenR
// All text data has been moved to be local to the comms controller to save on memory useage.
// This was the only place I couldn't make it work due to the composite text message strings you are using.
// I plan to add support for composite text message IDs to the comms controller but for now your text messages aren't being queued.
FUNC BOOL REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER(enumCharacterList ePlayerChar, enumCharacterList eBuddyChar, enumFriendTextMessage eFriendTextMessage, BOOL bSendImmediately = FALSE, CC_CodeID eCID = CID_BLANK, CC_CommID eCommID = TEXT_FRIEND, VectorID eRestrictedArea = VID_BLANK)
	
	CC_CommunicationType 	eCommunicationType			= CT_FRIEND
	enumFriend 				eFriendFrom					= GET_FRIEND_FROM_CHAR(eBuddyChar)
	INT						iBitsetPlayerTo 			= GET_PLAYER_PED_BIT(ePlayerChar)	
	INT						iThisFriendInitialDelay		= 10000
		
	IF bSendImmediately = FALSE				
		
		// Add message to comms queue
		CC_TextPart eSenderPart, eContextPart
		IF Private_GetFriendTextMessageParts(eFriendFrom, eFriendTextMessage, -1, eSenderPart, eContextPart)

			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 tPlayer		= GetLabel_enumCharacterList(ePlayerChar)
				TEXT_LABEL_63 tBuddy		= GetLabel_enumCharacterList(eBuddyChar)
				TEXT_LABEL_63 tTextMessage	= GetLabel_enumFriendTextMessage(eFriendTextMessage)
				TEXT_LABEL_31 tCommID		= GET_COMM_ID_TEXT_MESSAGE_DEBUG_STRING(eCommID)
				TEXT_LABEL_31 tSenderPart	= GET_TEXT_PART_DEBUG_STRING(eSenderPart)
				TEXT_LABEL_31 tContextPart	= GET_TEXT_PART_DEBUG_STRING(eContextPart)
				CPRINTLN(DEBUG_FRIENDS, "REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER(", tPlayer, ", ", tBuddy, ", ", tTextMessage, ", bSendImmediately=FALSE, ", ENUM_TO_INT(eCID), ", ", tCommID, ")	// = ", tSenderPart, "/", tContextPart)
			#ENDIF
			
			IF eCommID = COMM_NONE
				SCRIPT_ASSERT("REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER() - Can't use eCommID = COMM_NONE, if sending in queue")
				RETURN FALSE
			ENDIF
	
			INT iSettings = 0
			IF eCommID = TEXT_FRIEND_GRIEF_MICHAEL
			OR eCommID = TEXT_FRIEND_GRIEF_FRANKLIN
			OR eCommID = TEXT_FRIEND_GRIEF_TREVOR
			OR eCommID = TEXT_FRIEND_GRIEF_LAMAR
			OR eCommID = TEXT_FRIEND_GRIEF_JIMMY
			OR eCommID = TEXT_FRIEND_GRIEF_AMANDA
				iSettings = COMM_FLAG_TXTMSG_NO_SILENT
			ENDIF
			
			FLOW_CHECK_IDS eFlowCheck = FLOW_CHECK_NONE
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF eCommID = TEXT_FRIEND_GRIEF_MICHAEL		eFlowCheck = FLOW_CHECK_TEXT_FRIEND_MICHAEL
			ELIF eCommID = TEXT_FRIEND_GRIEF_FRANKLIN	eFlowCheck = FLOW_CHECK_TEXT_FRIEND_FRANKLIN
			ELIF eCommID = TEXT_FRIEND_GRIEF_TREVOR		eFlowCheck = FLOW_CHECK_TEXT_FRIEND_TREVOR
			ELIF eCommID = TEXT_FRIEND_GRIEF_LAMAR		eFlowCheck = FLOW_CHECK_TEXT_FRIEND_LAMAR
			ELIF eCommID = TEXT_FRIEND_GRIEF_JIMMY		eFlowCheck = FLOW_CHECK_TEXT_FRIEND_JIMMY
			ELIF eCommID = TEXT_FRIEND_GRIEF_AMANDA		eFlowCheck = FLOW_CHECK_TEXT_FRIEND_AMANDA
			ENDIF
		#ENDIF
		#ENDIF
		
			IF REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(eCommID,
				eSenderPart,
				eContextPart,
				eCommunicationType,
				iBitsetPlayerTo,
				eBuddyChar,
				iThisFriendInitialDelay,
				DEFAULT, eRestrictedArea, eCID, eFlowCheck, iSettings)
				
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF	

		ENDIF
	
	ELSE

		// Send message now
		TEXT_LABEL tTextLabel
		IF Private_GetFriendTextMessageLabel(eFriendFrom, eFriendTextMessage, -1, tTextLabel)
	
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 tPlayer		= GetLabel_enumCharacterList(ePlayerChar)
				TEXT_LABEL_63 tBuddy		= GetLabel_enumCharacterList(eBuddyChar)
				TEXT_LABEL_63 tTextMessage	= GetLabel_enumFriendTextMessage(eFriendTextMessage)		
				CPRINTLN(DEBUG_FRIENDS, "REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER(", tPlayer, ", ", tBuddy, ", ", tTextMessage, ", bSendImmediately=TRUE, ", ENUM_TO_INT(eCID), ")	// = ", tTextLabel)
			#ENDIF

			IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(eBuddyChar, tTextLabel, TXTMSG_UNLOCKED)
				#if USE_CLF_DLC
					IF eCID <> CID_BLANK
						SWITCH eCID					
							CASE CID_CLF_HOSPITAL_CHARGE_RH		Execute_CLF_Hospital_Charge_RH()	BREAK
							CASE CID_CLF_HOSPITAL_CHARGE_SC		Execute_CLF_Hospital_Charge_SC()	BREAK
							CASE CID_CLF_HOSPITAL_CHARGE_DT		Execute_CLF_Hospital_Charge_DT()	BREAK
							CASE CID_CLF_HOSPITAL_CHARGE_SS		Execute_CLF_Hospital_Charge_SS()	BREAK
							CASE CID_CLF_HOSPITAL_CHARGE_PB		Execute_CLF_Hospital_Charge_PB()	BREAK						
						ENDSWITCH
					ENDIF
				#endif
				#if USE_NRM_DLC
					IF eCID <> CID_BLANK
//						SWITCH eCID					
//							CASE CID_HOSPITAL_CHARGE_RH		Execute_Hospital_Charge_RH()	BREAK
//							CASE CID_HOSPITAL_CHARGE_SC		Execute_Hospital_Charge_SC()	BREAK
//							CASE CID_HOSPITAL_CHARGE_DT		Execute_Hospital_Charge_DT()	BREAK
//							CASE CID_HOSPITAL_CHARGE_SS		Execute_Hospital_Charge_SS()	BREAK
//							CASE CID_HOSPITAL_CHARGE_PB		Execute_Hospital_Charge_PB()	BREAK						
//						ENDSWITCH
					ENDIF
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					IF eCID <> CID_BLANK
						SWITCH eCID					
							CASE CID_HOSPITAL_CHARGE_RH		Execute_Hospital_Charge_RH()	BREAK
							CASE CID_HOSPITAL_CHARGE_SC		Execute_Hospital_Charge_SC()	BREAK
							CASE CID_HOSPITAL_CHARGE_DT		Execute_Hospital_Charge_DT()	BREAK
							CASE CID_HOSPITAL_CHARGE_SS		Execute_Hospital_Charge_SS()	BREAK
							CASE CID_HOSPITAL_CHARGE_PB		Execute_Hospital_Charge_PB()	BREAK						
						ENDSWITCH
					ENDIF
				#endif
				#endif
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF

	ENDIF
	
	RETURN TRUE
ENDFUNC


// *******************************************************************************************
//	MISSION START COMMENTS
// *******************************************************************************************

FUNC BOOL PRIVATE_FriendDialogue_GetRawAudio(enumCharacterList eFriendChar, enumFriendActivityPhrase ePhrase, TEXT_LABEL& tAudio)
	
	SWITCH (ePhrase)
		CASE FAP_REJECTION_OK
			SWITCH(eFriendChar)
				CASE CHAR_MICHAEL		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_FRANKLIN		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_TREVOR		tAudio = "FaFM_CAAA"	RETURN TRUE

				CASE CHAR_LAMAR			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_JIMMY			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_AMANDA		tAudio = "FaFM_CAAA"	RETURN TRUE
			ENDSWITCH
		BREAK

		CASE FAP_REJECTION_CRAZY
			SWITCH(eFriendChar)
				CASE CHAR_MICHAEL		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_FRANKLIN		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_TREVOR		tAudio = "FaFM_CAAA"	RETURN TRUE

				CASE CHAR_LAMAR			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_JIMMY			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_AMANDA		tAudio = "FaFM_CAAA"	RETURN TRUE
			ENDSWITCH
		BREAK

		CASE FAP_SQUAD_START
			SWITCH(eFriendChar)
				CASE CHAR_MICHAEL		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_FRANKLIN		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_TREVOR		tAudio = "FaFM_CAAA"	RETURN TRUE

				CASE CHAR_LAMAR			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_JIMMY			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_AMANDA		tAudio = "FaFM_CAAA"	RETURN TRUE
			ENDSWITCH
		BREAK

		CASE FAP_SQUAD_PASSED
			SWITCH(eFriendChar)
				CASE CHAR_MICHAEL		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_FRANKLIN		tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_TREVOR		tAudio = "FaFM_CAAA"	RETURN TRUE

				CASE CHAR_LAMAR			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_JIMMY			tAudio = "FaFM_CAAA"	RETURN TRUE
				CASE CHAR_AMANDA		tAudio = "FaFM_CAAA"	RETURN TRUE
			ENDSWITCH
		BREAK
		
		DEFAULT
			// Didn't find phrase
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 tChar
				TEXT_LABEL_63 tPhrase
				tChar = GetLabel_enumCharacterList(eFriendChar)
				tPhrase = GetLabel_enumFriendActivityPhrase(ePhrase)
				CPRINTLN(DEBUG_FRIENDS, "PRIVATE_FriendDialogue_GetRawAudio(", tChar, ", ", tPhrase, ") - Phrase not supported")
				SCRIPT_ASSERT("PRIVATE_FriendDialogue_GetRawAudio() - Phrase not supported")
			#ENDIF
			tAudio = "FaFM_BVAA"
			RETURN FALSE
		BREAK
	ENDSWITCH

	// Found phrase but not char
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tChar = GetLabel_enumCharacterList(eFriendChar)
		TEXT_LABEL_63 tPhrase = GetLabel_enumFriendActivityPhrase(ePhrase)
		CPRINTLN(DEBUG_FRIENDS, "PRIVATE_FriendDialogue_GetRawAudio(", tChar, ", ", tPhrase, ") - Char not supported for this phrase")
		SCRIPT_ASSERT("PRIVATE_FriendDialogue_GetRawAudio() - Char not supported for this phrase")
	#ENDIF
	tAudio = "FaFM_BVAA"
	RETURN FALSE

ENDFUNC


// *******************************************************************************************
//	PLAY ANSWER PHONE MESSAGE
// *******************************************************************************************

FUNC BOOL PRIVATE_Friend_DoAnswerPhone(structPedsForConversation& convPeds, enumCharacterList eFriendCharID)
	
	// Setup the conversation participants
	ADD_PED_FOR_DIALOGUE(convPeds, 1, NULL, PRIVATE_Get_SpeakerLabel_From_Char(eFriendCharID))
		
	// Get dialogue block and lines
	BOOL		bForcePhone	= FALSE
	TEXT_LABEL	tAudioBlock
	TEXT_LABEL	tAudioRoot
	
	PRIVATE_FriendDialogue_GetAnswerphonePhrase(GET_FRIEND_FROM_CHAR(eFriendCharID), tAudioBlock, tAudioRoot)
	
	IF PLAYER_CALL_CHAR_CELLPHONE(convPeds, eFriendCharID, tAudioBlock, tAudioRoot, CONV_PRIORITY_NON_CRITICAL_CALL, bForcePhone)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// *******************************************************************************************
// ACTIVITY CANCEL CALL
// *******************************************************************************************

FUNC BOOL PRIVATE_Friend_GetCancelConvReply(INT iCurrentLikes, enumFriendPhonePhrase& replyPartA, enumFriendPhonePhrase &replyPartB)
	
	// select CANCEL conv
	replyPartA = FPP_CANCEL1
	
//	IF NOT g_SavedGlobals.sFriendsData.g_bCalledToCancelOnce		// TODO: Make the first time cancel dialogue work
//		replyPartB = FPP_CANCEL2_FIRSTTIME
//	ELSE
		IF iCurrentLikes > 70
			replyPartB = FPP_CANCEL2_HILIKE
		ELIF iCurrentLikes > 45
			replyPartB = FPP_CANCEL2_MEDLIKE
		ELSE
			replyPartB = FPP_CANCEL2_LOWLIKE
		ENDIF
//	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_Friend_DoCancelConv(structPedsForConversation& convPeds, enumCharacterList ePlayerCharID, enumCharacterList eFriendCharID)

	// Get appropriate cancel phrases
	enumFriendPhonePhrase eCancelPhraseA
	enumFriendPhonePhrase eCancelPhraseB
	
	PRIVATE_Friend_GetCancelConvReply(GET_FRIEND_LIKE(ePlayerCharID, eFriendCharID), eCancelPhraseA, eCancelPhraseB)
	
	// Get block and root for dialogue
	TEXT_LABEL tBlock
	TEXT_LABEL tRootA
	TEXT_LABEL tRootB
	
	Private_GetFriendPhonePhrase(ePlayerCharID, eFriendCharID, eCancelPhraseA, tBlock, tRootA)
	Private_GetFriendPhonePhrase(ePlayerCharID, eFriendCharID, eCancelPhraseB, tBlock, tRootB)
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_Friend_DoCancelConv(): dialogue roots: ", tRootA, ", ", tRootB)
	
	ADD_FRIEND_CHAR_FOR_DIALOGUE(convPeds, ePlayerCharID, PLAYER_PED_ID())
	ADD_FRIEND_CHAR_FOR_DIALOGUE(convPeds, eFriendCharID)
	
	IF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES(convPeds, eFriendCharID, tBlock,			// TODO: This doesn't work with the first time cancel (because it's not got variations?)
														tRootA, tRootA,
														tRootB, tRootB,
														CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)

		IF (eCancelPhraseB = FPP_CANCEL2_FIRSTTIME)
			g_SavedGlobals.sFriendsData.g_bCalledToCancelOnce = TRUE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

