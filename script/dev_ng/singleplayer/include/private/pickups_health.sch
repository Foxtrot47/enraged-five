// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "script_debug.sch"

CONST_INT NUMBER_OF_HEALTH_PICKUPS(76)

#IF IS_DEBUG_BUILD

	/// PURPOSE: Returns location name of heatlh pickup
	FUNC STRING GET_HEALTH_LOCATION_NAME(INT iIndex)

		SWITCH iIndex
			CASE 0  RETURN "01: Adventure Trail"                      		BREAK
			CASE 1  RETURN "02: Altruist Camp Guard Tower"            		BREAK
			CASE 2  RETURN "03: Army Control Tower"                   		BREAK
			CASE 3  RETURN "04: Bay Care Centre - Paleto"             		BREAK
			CASE 4  RETURN "05: Bridge Service Access"                		BREAK
			CASE 5  RETURN "06: Chumash Tennis Courts"                		BREAK
			CASE 6  RETURN "07: Church - Little Seoul"               		BREAK
			CASE 7  RETURN "08: Courtyard of IAA and FIB"             		BREAK
			CASE 8  RETURN "09: East LS Fire Station"                 		BREAK
			CASE 9  RETURN "10: Farm House Garden"                    		BREAK

			CASE 10 RETURN "11: Fishing Shack at Alamo Sea"           		BREAK
			CASE 11 RETURN "12: Fruit market"                         		BREAK
			CASE 12 RETURN "13: Harmony Motel"                        		BREAK
			CASE 13 RETURN "14: Hotel Courtyard"            		  		BREAK
			CASE 14 RETURN "15: House Foundations"          		  		BREAK
			CASE 15 RETURN "16: Inside Dam"							  		BREAK
			CASE 16 RETURN "17: Alley near closed Fleeca Bank in Grapeseed"	BREAK
			CASE 17 RETURN "18: Inside Los Santos Customs"            		BREAK
			CASE 18 RETURN "19: Movie Trailer at Movie Studio" 				BREAK
			CASE 19 RETURN "20: Lombank"                              		BREAK
			
			CASE 20 RETURN "21: Lost MC Clubhouse Exterior shelves"    		BREAK
			CASE 21 RETURN "22: Mirror Park Wash Station"             		BREAK
			CASE 22 RETURN "23: Bell Building at Kortz Center"        		BREAK
			CASE 23 RETURN "24: Lost Trailer Park Burned Outhouse"    		BREAK
			CASE 24 RETURN "25: Lost airfield hangar" 	             		BREAK
			CASE 25 RETURN "26: Mount Chilead Viewing Platform"       		BREAK
			CASE 26 RETURN "27: Observatory"                          		BREAK
			CASE 27 RETURN "28: Outside Barn in Grapeseed"            		BREAK
			CASE 28 RETURN "29: Outside Yellow House on Coast"        		BREAK
			CASE 29 RETURN "30: Behind Paleto Police Station"               BREAK
			
			CASE 30 RETURN "31: Pumpkin Patch Garden"                 		BREAK
			CASE 31 RETURN "32: Rear of Medical Center"               		BREAK
			CASE 32 RETURN "33: Rear of Pill Pharm Clinic"            		BREAK
			CASE 33 RETURN "34: Rehab Center in Vinewood"             		BREAK
			CASE 34 RETURN "35: Rock Arch"                            		BREAK
			CASE 35 RETURN "36: Rear of Odea's Pharmacy"              		BREAK
			CASE 36 RETURN "37: Sandy Shores Medical Center"         		BREAK
			CASE 37 RETURN "38: Security Booth at Bollingbroke Peniteniary" BREAK
			CASE 38 RETURN "39: Service Area inside Simeon's Dealership"    BREAK
			CASE 39 RETURN "40: Tent Village"                               BREAK
			
			CASE 40 RETURN "41: Upstairs at Marina Yacht Club"              BREAK
			CASE 41 RETURN "42: Vagos Ganghouse"                            BREAK
			CASE 42 RETURN "43: Vespucci Beach Toilets"                     BREAK
			CASE 43 RETURN "44: Vinewood Hills Construction site"			BREAK
			CASE 44 RETURN "45: Vinewood Organic Health Center"             BREAK
			CASE 45 RETURN "46: Vinewood Sign Power Station"                BREAK
			CASE 46 RETURN "47: Vinewood Trash Pile"                        BREAK
			CASE 47 RETURN "48: Vineyard House Rear Garden"                 BREAK
			CASE 48 RETURN "49: Warehouse at Docks"                         BREAK
			CASE 49 RETURN "50: Workman's Shack Under Freeway"              BREAK
			
			CASE 50 RETURN "51: Lifeguard Hut 1"              				BREAK
			CASE 51 RETURN "52: Interior"		              				BREAK
			CASE 52 RETURN "53: Lifeguard Hut 2"              				BREAK
			CASE 53 RETURN "54: Lifeguard Hut 3"              				BREAK
			CASE 54 RETURN "56: Lifeguard Hut 4"              				BREAK
			CASE 55 RETURN "59: On roof of Val-De-Grace"          			BREAK
			CASE 56 RETURN "60: Vinewood Hills - Garden Bushes"          	BREAK
			CASE 57 RETURN "61: O'Neill's farm - barn"          			BREAK
			CASE 58 RETURN "64: Next to cash register"      		 		BREAK
			CASE 59 RETURN "69: Power box" 									BREAK

			CASE 60 RETURN "70: Under alarms" 								BREAK
			CASE 61 RETURN "71: Grove street dumpsters" 					BREAK
			CASE 62 RETURN "74: Outdoor toilet" 							BREAK
			CASE 63 RETURN "75: Back of office near file storage" 			BREAK
			CASE 64 RETURN "76: Hobo shack" 								BREAK
			CASE 65 RETURN "77: Work station container" 					BREAK
			CASE 66 RETURN "78: Fire hose access point" 					BREAK
			CASE 67 RETURN "82: St. Fiacre rear garage"						BREAK
			CASE 68 RETURN "83: St. Fiacre front doors"						BREAK
			CASE 69 RETURN "84: LS Central Medical Center Emergency"		BREAK
			
			CASE 70 RETURN "85: LS Central Medical Center main entrance"	BREAK
			CASE 71 RETURN "86: Pillbox Hill Medical Center upper entrance"	BREAK
			CASE 72 RETURN "87: Pillbox Hill Medical Center lower entrance"	BREAK
			CASE 73 RETURN "88: Mount Zonah Medical Center emergency W"		BREAK
			CASE 74 RETURN "89: Mount Zonah Medical Center emergency E"	 	BREAK
			CASE 75 RETURN "90: Kortz Center on pillar"						BREAK
		ENDSWITCH
		
		RETURN "Invalid Location Index" 
	ENDFUNC
#ENDIF

/// PURPOSE: Returns coords of health pickup
FUNC VECTOR GET_HEALTH_PICKUP_COORDS(INT iIndex, BOOL bForChop=FALSE)
	
	// If any of these have a bForChop vector set, Chop will lead the player to the bForChop vector then orientate himself towards the actual pickup vector.
	// Small alterations are fine but if you move a pickup more than 1-2 metres away from its current position you must update the bForChop vector too or else we'll have Chop leading the player to nothing.
	// The bForChop vector must be on a section of navmesh that is connected to the main navmesh. See Kev E if unsure.
	SWITCH iIndex
		
		CASE 0  RETURN << 131.2, 6920.1, 20.8 >> BREAK
		
		CASE 1
			IF bForChop = TRUE
				RETURN << -1048.56946, 4917.41406, 208.42160 >>
			ELSE
				RETURN << -1046.2772, 4918.7393, 211.5379 >>
			ENDIF
		BREAK
		
		CASE 2  RETURN << -2358.4600, 3251.3970, 100.5504 >> BREAK
		
		CASE 3
			IF bForChop = TRUE
				RETURN << -247.62189, 6331.34082, 31.42602 >>
			ELSE
				RETURN << -249.9504, 6331.2192, 32.7262 >>
			ENDIF
		BREAK
		
		CASE 4
			IF bForChop = TRUE
				RETURN << -140.90, -2491.45, 43.45 >>
			ELSE
				RETURN << -141.0976, -2498.2390, 40.0124 >>
			ENDIF
		BREAK
		
		CASE 5
			IF bForChop = TRUE
				RETURN << -2924.69678, 46.50282, 10.60369 >>
			ELSE
				RETURN << -2925.7, 47.4, 11.6 >>
			ENDIF
		BREAK
		
		CASE 6  RETURN << -793.1981,-726.4421, 26.2800 >> BREAK
		
		CASE 7
			IF bForChop = TRUE
				RETURN << 124.00925, -673.00964, 41.02729 >>
			ELSE
				RETURN << 123.5196, -674.2917, 41.4945 >>
			ENDIF
		BREAK
		
		CASE 8
			IF bForChop = TRUE
				RETURN << 1195.73743, -1481.69446, 33.85950 >>
			ELSE
				RETURN << 1194.7092, -1482.4723, 33.9594 >>
			ENDIF
		BREAK
		
		CASE 9
			IF bForChop = TRUE
				RETURN << -35.88743, 1945.73779, 189.18681 >>
			ELSE
				RETURN << -35.5254, 1947.2894, 189.1860 >>
			ENDIF
		BREAK
		
		CASE 10
			IF bForChop
				RETURN << 753.4579, 4174.9956, 39.8011 >>
			ELSE
				RETURN << 751.72, 4175.14, 40.95 >>
			ENDIF
		BREAK
		
		CASE 11
			IF bForChop = TRUE
				RETURN << 1791.25647, 4592.38330, 36.68283 >>
			ELSE
				RETURN << 1789.8, 4592.4, 36.78 >>				// On floor now - B*136552
			ENDIF
		BREAK
		
		CASE 12 RETURN << 341.3110, 2618.9182, 43.5124 >> BREAK
		CASE 13 RETURN << 442.3755, -223.6020, 55.0215>>  BREAK
		
		CASE 14
			IF bForChop = TRUE
				RETURN << 1291.02, -1760.94, 51.05 >>
			ELSE
				RETURN << 1286.5, -1754.4, 52.0 >>
			ENDIF
		BREAK
		
		CASE 15 
			IF bForChop = TRUE
				RETURN <<1659.4376, 5.4271, 165.1176>>
			ELSE
				RETURN << 1658.6210, 6.7752, 166.1676 >>
			ENDIF
		BREAK
		
		CASE 16
			IF bForChop = TRUE
				RETURN <<1645.91, 4866.72, 40.98>>
			ELSE
				RETURN <<1647.3992, 4865.8394, 41.0>> // Moved to alleyway B*1355191
			ENDIF
		BREAK	
		
		CASE 17 RETURN << -1144.5892, -2004.4523, 12.3803 >> BREAK
		
		CASE 18
			IF bForChop = TRUE
				RETURN << -1047.3, -520.6, 35.0 >>
			ELSE
				RETURN << -1048.320, -521.200, 35.14 >>
			ENDIF
		BREAK
		
		CASE 19
			IF bForChop = TRUE
				RETURN << -1576.04590, -587.82574, 33.97909 >>
			ELSE
				RETURN << -1576.8167, -586.3887, 34.8528 >>
			ENDIF
		BREAK
		
		CASE 20
			IF bForChop = TRUE
				RETURN << 953.44, -123.42, 73.35 >>
			ELSE
				RETURN << 954.50, -121.34, 74.18 >>
			ENDIF
		BREAK
		
		CASE 21
			IF bForChop = TRUE
				RETURN << 1134.20093, -665.94135, 56.08261 >>
			ELSE
				RETURN << 1135.4014, -663.7875, 56.0880 >>
			ENDIF
		BREAK
		
		CASE 22
			IF bForChop = TRUE
				RETURN << -2196.19751, 248.91229, 173.61200 >>
			ELSE
				RETURN << -2195.0288, 250.4256, 173.6017 >>
			ENDIF
		BREAK
		
		CASE 23 RETURN << 29.2, 3635.4, 39.8 >>            BREAK
		
		CASE 24
			IF bForChop = TRUE
				RETURN <<1724.4291, 3299.0234, 40.2235>>
			ELSE
				RETURN << 1721.9596, 3300.4644, 40.3835 >>
			ENDIF
		BREAK
		
		CASE 25
			IF bForChop = TRUE
				RETURN << 2617.32056, 3659.45190, 100.38673 >>
			ELSE
				RETURN << 2612.7124, 3662.5642, 101.1074 >>
			ENDIF
		BREAK
		
		CASE 26 RETURN << -459.9625, 1101.0760, 328.0211 >> BREAK
		
		CASE 27
			IF bForChop = TRUE
				RETURN << 2432.60718, 4994.33350, 45.25828 >>
			ELSE
				RETURN << 2430.9065, 4995.5610, 45.2685 >>
			ENDIF
		BREAK
		
		CASE 28
			IF bForChop = TRUE
				RETURN << 3722.45557, 4521.83691, 20.39456 >>
			ELSE
				RETURN << 3724.5, 4524.5, 21.6 >>
			ENDIF
		BREAK
		
		CASE 29
			IF bForChop = TRUE
				RETURN << -446.9024, 5997.5981, 30.3407 >>
			ELSE
				RETURN << -446.0450, 6000.8818, 31.9163 >>
			ENDIF
		BREAK
		
		CASE 30 RETURN << 3291.4, 5192.5, 18.4 >>			BREAK
		
		CASE 31
			IF bForChop = TRUE
				RETURN << 390.81, -1436.07, 28.45 >>
			ELSE
				RETURN << 388.70, -1434.30, 29.80 >> // Repositioned - B*1339106
			ENDIF
		BREAK	
		
		CASE 32 RETURN << -445.26, -442.49, 32.26 >>		BREAK	// Moved back and put on floor - B*1336955
		
		CASE 33
			IF bForChop
				RETURN << -1501.46, 858.17, 180.59 >>
			ELSE
				RETURN << -1500.08, 857.59, 180.59 >> // Moved due to floating - B*1355129
			ENDIF
		BREAK
		
		CASE 34 RETURN << 2822.2, -741.4, 1.8 >>			BREAK
		
		CASE 35
			IF bForChop = TRUE
				RETURN << -3166.97827, 1102.62244, 19.80827 >>
			ELSE
				RETURN << -3165.6125, 1102.2405, 19.8928 >>
			ENDIF
		BREAK
		
		CASE 36
			IF bForChop = TRUE
				RETURN <<1842.8818, 3670.4568, 32.6800>>
			ELSE
				RETURN << 1841.1140, 3675.2529, 34.0860 >>
			ENDIF
		BREAK
		
		CASE 37
			IF bForChop = TRUE
				RETURN << 1828.79443, 2605.50391, 44.61582 >>
			ELSE
				RETURN << 1830.5350, 2603.8279, 45.7491 >>
			ENDIF
		BREAK
		
		CASE 38
			IF bForChop = TRUE
				RETURN << -32.61, -1089.67, 25.42 >> // B*1211347
			ELSE
				RETURN  << -40.0528, -1084.0160, 26.6224 >>
			ENDIF
		BREAK
		
		CASE 39
			IF bForChop = TRUE
				RETURN << 1439.01221, 6336.81201, 22.96477 >>
			ELSE
				RETURN << 1445.5439, 6334.0571, 23.9750 >>
			ENDIF
		BREAK
		
		CASE 40
			IF bForChop = TRUE
				RETURN << -782.5126, -1352.2454, 8.0001 >>
			ELSE
				RETURN << -784.4851, -1351.3640, 8.1001 >>
			ENDIF
		BREAK
		
		CASE 41
			IF bForChop = TRUE
				RETURN << 962.88055, -1826.41919, 30.07195 >>
			ELSE
				RETURN << 963.1673, -1831.1490, 36.2055 >>
			ENDIF
		BREAK
		
		CASE 42 
			IF bForChop = TRUE
				RETURN  <<-1246.1550, -1531.6471, 3.2962>>
			ELSE
				RETURN << -1246.4940, -1533.6770, 4.5262 >>
			ENDIF
		BREAK
		
		CASE 43
			IF bForChop = TRUE
				RETURN << -765.54657, 698.81012, 143.36932 >>
			ELSE
				RETURN << -761.1058, 701.9452, 145.0500 >>
			ENDIF
		BREAK
		
		CASE 44
			IF bForChop = TRUE
				RETURN << -512.10944, 28.86313, 43.61530 >>
			ELSE
				RETURN << -513.4391, 30.9069, 43.9018 >>
			ENDIF
		BREAK
		
		CASE 45
			IF bForChop = TRUE
				RETURN  << 781.38385, 1292.07300, 359.29965 >>
			ELSE
				RETURN << 780.3879, 1295.7050, 361.6941 >>
			ENDIF
		BREAK
		
		CASE 46 RETURN << 208.69, 337.76, 104.64 >>				BREAK // Position fix B*1355129
		
		CASE 47
			IF bForChop = TRUE
				RETURN << -1889.99, 2078.38, 140.00 >>
			ELSE
				RETURN << -1890.24, 2073.26, 140.11 >> // Position fix B*1355129
			ENDIF
		BREAK 
		
		CASE 48 RETURN << 153.6156, -3073.9983, 4.8962 >>		BREAK
		CASE 49
			IF bForChop
				RETURN <<-306.2449, -1180.5382, 22.7110>>
			ELSE
				RETURN << -304.0894, -1180.8860, 23.9493 >>
			ENDIF
		BREAK
		CASE 50 RETURN << -2006.9866, -556.4998, 11.8813 >>		BREAK
		
		CASE 51
			IF bForChop = TRUE
				RETURN <<-590.20795, -289.81961, 40.68631>>
			ELSE
				RETURN <<-588.1934, -290.4782, 43.7101>>
			ENDIF
		BREAK
		
		CASE 52 RETURN << -1795.8262, -855.7111, 8.2048 >>     BREAK
		CASE 53 RETURN << -1557.1080, -1155.2465, 2.9158 >>    BREAK
		
		CASE 54
			IF bForChop = TRUE
				RETURN << -1470.71, -1389.33, 1.58 >>
			ELSE
				RETURN << -1467.4833, -1387.5068, 3.1432 >>
			ENDIF
		BREAK
		
		CASE 55 RETURN << -684.8126, -180.6480, 48.0200 >>  	   BREAK
		
		CASE 56
			IF bForChop = TRUE
				RETURN <<-1311.41272, 640.11536, 136.92444>>
			ELSE
				RETURN <<-1307.0620, 641.4211, 138.0582>>
			ENDIF
		BREAK
		
		CASE 57 RETURN << 2493.6855, 4963.5410, 43.7358 >> BREAK // Used to be in Life Invader, moved out of non-combat mission interior for B*1563245
		
		CASE 58
			IF bForChop = TRUE
				RETURN <<1392.76, 3602.94, 33.98>>
			ELSE
				RETURN <<1392.6087, 3605.7986, 34.9939>>
			ENDIF
		BREAK
		
		CASE 59 
			IF bForChop = TRUE
				RETURN <<-527.33, 5295.70, 73.17>>
			ELSE
				RETURN <<-535.4436, 5297.4961, 76.3891>> 	
			ENDIF
		BREAK
		
		CASE 60 
			IF bForChop = TRUE
				RETURN <<-564.26, 5353.13, 69.23>>
			ELSE
				RETURN << -551.5837, 5349.4043, 75.2530 >>
			ENDIF
		BREAK
		
		CASE 61 			
			IF bForChop = TRUE
				RETURN <<3.18, -1827.10, 24.22>>
			ELSE
				RETURN <<0.1911, -1825.9047, 24.3295>>
			ENDIF
		BREAK
		
		CASE 62
			IF bForChop = TRUE
				RETURN <<62.25, 3681.06, 38.84>>
			ELSE
				RETURN << 63.8867, 3683.4900, 39.1543 >>
			ENDIF
		BREAK
		
		CASE 63 RETURN <<155.384, -741.179, 258.6519>> 	BREAK
		
		CASE 64
			IF bForChop = TRUE
				RETURN <<126.61, -1206.29, 28.30>>
			ELSE
				RETURN <<124.2650, -1205.8820, 28.2951>>
			ENDIF
		BREAK
		
		CASE 65 RETURN <<27.449, -625.310, 15.462>> 	BREAK
		
		CASE 66
			IF bForChop = TRUE
				RETURN <<-129.04, -662.04, 39.51>>
			ELSE
				RETURN << -129.0374, -659.2689, 41.0016 >>
			ENDIF
		BREAK
			
		CASE 67	IF bForChop RETURN << 1138.62, -1598.56, 33.69 >>	ELSE RETURN << 1138.63, -1596.61, 35.07 >>	ENDIF	BREAK
		CASE 68	IF bForChop RETURN << 1149.68, -1525.56, 33.84 >>	ELSE RETURN << 1147.55, -1527.83, 36.01 >>	ENDIF	BREAK
		CASE 69	IF bForChop RETURN << 297.48, -1448.79, 28.97 >>	ELSE RETURN << 296.440, -1450.75, 30.40 >>	ENDIF	BREAK
		CASE 70	IF bForChop RETURN << 324.21, -1392.68, 31.51 >>	ELSE RETURN << 322.27, -1393.75, 32.79 >>	ENDIF	BREAK
		CASE 71	IF bForChop RETURN << 293.20, -597.92, 42.28 >>		ELSE RETURN << 294.30, -598.10, 43.73 >>	ENDIF	BREAK
		CASE 72	IF bForChop RETURN << 358.00, -592.66, 27.79 >>		ELSE RETURN << 357.07, -590.63, 29.08 >>	ENDIF	BREAK
		CASE 73	IF bForChop RETURN << -495.45, -326.18, 33.50 >>	ELSE RETURN << -496.40, -324.70, 34.75 >>	ENDIF	BREAK
		CASE 74	IF bForChop RETURN << -450.91, -351.93, 33.50 >>	ELSE RETURN << -449.82, -353.49, 34.75 >>	ENDIF	BREAK
		
		CASE 75 RETURN << -2258.8330, 323.9413, 184.9015 >> BREAK
		
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid index passed to GET_HEALTH_PICKUP_COORDS")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Gets the interior name for the pickup
FUNC STRING GET_PICKUP_ROOM_NAME(INT iIndex)
	
	SWITCH iIndex
		CASE 2  RETURN "V_25_ControlRm"     BREAK
		CASE 8  RETURN "FireDeptRoom"       BREAK
		CASE 15 RETURN "Inside Dam"			BREAK
		CASE 17 RETURN "V_CarModRoom"       BREAK
		CASE 20 RETURN "V_70_Toilet"        BREAK
		CASE 38 RETURN "rm_garage"   		BREAK
		CASE 48 RETURN "GtaMloRoom001"   	BREAK
		CASE 51 RETURN "v_refit"   			BREAK
		CASE 58 RETURN "V_39_ShopRm"   		BREAK
		CASE 63 RETURN "V_FIB03_atr_off1"   BREAK
		CASE 65 RETURN "GtaMloRoomTun2"   	BREAK
		CASE 67 RETURN "V_34_ProcessRm"   	BREAK
		CASE 68 RETURN "V_34_Ent_office"   	BREAK
		CASE 69 RETURN "V_34_WareRm"   		BREAK
		DEFAULT
			RETURN ""
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns TRUE if Chop can reach the health pickup
FUNC BOOL CAN_CHOP_REACH_HEALTH(INT index)
	
	SWITCH (index)
		CASE 2 FALLTHRU	// Top of the army control tower, loads of stairs, player gets 4 star wanted rating
		CASE 17 FALLTHRU	// In Los Santos Customs mod shop
		CASE 55 FALLTHRU	// On a roof
		CASE 63 FALLTHRU	// In office building
		CASE 65 FALLTHRU	// Bottom of construction site
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
