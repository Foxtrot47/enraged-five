//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 10/02/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					 		Flow Help Text Private Header						│
//│																				│
//│				Backend procedures for the flow help text controller.			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛  

USING "rage_builtins.sch"
USING "globals.sch"


STRUCT Displayed_FlowHelp
	TEXT_LABEL_15 		tHelpText
	TEXT_LABEL_15		tAddText
	INT 				iStartTime
	INT 				iDuration
	INT					iExpirationTime
	INT					iCharBitset
	FlowHelpPriority	ePriority
	CC_CodeID			eCodeIDStart
	CC_CodeID			eCodeIDDisplayed
	INT					iSettingsFlags
	BOOL				bSkipTimeOnScreenCheck
ENDSTRUCT


CONST_INT	FLOW_HELP_NEVER_EXPIRES				-1
CONST_INT	FLOW_HELP_DISPLAY_FOREVER			-1
CONST_INT	FLOW_HELP_DELAY_BETWEEN_MESSAGES	250


PROC PRIVATE_Copy_Queued_Help_From_Index_To_Index(INT paramCopyToIndex, INT paramCopyFromIndex)

#if USE_CLF_DLC
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].tHelpText 		= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].tHelpText
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].tAddText			= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].tAddText
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].iStartTime 		= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].iStartTime
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].iExpirationTime 	= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].iExpirationTime
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].iDuration 		= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].iDuration
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].iCharBitset		= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].iCharBitset
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].ePriority			= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].ePriority
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].eCodeIDStart		= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].eCodeIDStart
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].eCodeIDDisplayed	= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].eCodeIDDisplayed
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyToIndex].iSettingsFlags	= g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramCopyFromIndex].iSettingsFlags
#endif

#if USE_NRM_DLC
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].tHelpText 			= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].tHelpText
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].tAddText			= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].tAddText
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].iStartTime 			= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].iStartTime
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].iExpirationTime 	= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].iExpirationTime
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].iDuration 			= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].iDuration
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].iCharBitset			= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].iCharBitset
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].ePriority			= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].ePriority
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].eCodeIDStart		= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].eCodeIDStart
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].eCodeIDDisplayed	= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].eCodeIDDisplayed
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyToIndex].iSettingsFlags		= g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramCopyFromIndex].iSettingsFlags
#endif

#if NOT USE_SP_DLC
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].tHelpText 		= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].tHelpText
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].tAddText			= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].tAddText
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].iStartTime 		= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].iStartTime
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].iExpirationTime 	= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].iExpirationTime
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].iDuration 		= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].iDuration
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].iCharBitset		= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].iCharBitset
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].ePriority			= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].ePriority
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].eCodeIDStart		= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].eCodeIDStart
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].eCodeIDDisplayed	= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].eCodeIDDisplayed
	g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyToIndex].iSettingsFlags	= g_savedGlobals.sFlowHelp.sHelpQueue[paramCopyFromIndex].iSettingsFlags
#endif

ENDPROC


PROC PRIVATE_Clear_Queued_Help_At_Index(INT paramIndex)	

#if USE_CLF_DLC
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].tHelpText 			= ""
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].tAddText 			= ""
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].iStartTime 			= 0
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].iDuration			= 0
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].iCharBitset			= BIT_NOBODY
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].iExpirationTime		= FLOW_HELP_NEVER_EXPIRES
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].ePriority			= FHP_LOW
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].eCodeIDStart		= CID_BLANK
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].eCodeIDDisplayed	= CID_BLANK
	g_savedGlobalsClifford.sFlowHelp.sHelpQueue[paramIndex].iSettingsFlags		= FHF_NONE
#endif

#if USE_NRM_DLC
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].tHelpText 			= ""
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].tAddText 				= ""
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].iStartTime 			= 0
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].iDuration				= 0
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].iCharBitset			= BIT_NOBODY
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].iExpirationTime		= FLOW_HELP_NEVER_EXPIRES
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].ePriority				= FHP_LOW
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].eCodeIDStart			= CID_BLANK
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].eCodeIDDisplayed		= CID_BLANK
	g_savedGlobalsnorman.sFlowHelp.sHelpQueue[paramIndex].iSettingsFlags		= FHF_NONE
#endif

#if NOT USE_SP_DLC
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].tHelpText 			= ""
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].tAddText 			= ""
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].iStartTime 			= 0
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].iDuration			= 0
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].iCharBitset			= BIT_NOBODY
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].iExpirationTime		= FLOW_HELP_NEVER_EXPIRES
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].ePriority			= FHP_LOW
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].eCodeIDStart		= CID_BLANK
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].eCodeIDDisplayed	= CID_BLANK
	g_savedGlobals.sFlowHelp.sHelpQueue[paramIndex].iSettingsFlags		= FHF_NONE
#endif

ENDPROC


PROC PRIVATE_Update_Flow_Help_Queue_Priority_Level()
	#IF IS_DEBUG_BUILD
		FlowHelpPriority eOldPriority[3] 
	#ENDIF
	
	INT i	
	
#if USE_CLF_DLC
	REPEAT 3 i		
		#IF IS_DEBUG_BUILD
			eOldPriority[i] = g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[i]
		#ENDIF
		g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[i] = FHP_LOW		
	ENDREPEAT	
	
	REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount i
		IF IS_BIT_SET(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
			IF ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[CHAR_MICHAEL])
				g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[CHAR_MICHAEL] = g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
			IF ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[CHAR_FRANKLIN])
				g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[CHAR_FRANKLIN] = g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_TREVOR))
			IF ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[CHAR_TREVOR])
				g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[CHAR_TREVOR] = g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
	ENDREPEAT	
	
	#IF IS_DEBUG_BUILD
		REPEAT 3 i
			IF eOldPriority[i] != g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[i]
				CPRINTLN(DEBUG_FLOW_HELP, "Queue priority for player character ", i," changed from ", ENUM_TO_INT(eOldPriority[i]), " to ", ENUM_TO_INT(g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[i]), ".")
			ENDIF
		ENDREPEAT
	#ENDIF
#endif

#if USE_NRM_DLC
	REPEAT 3 i		
		#IF IS_DEBUG_BUILD
			eOldPriority[i] = g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[i]
		#ENDIF
		g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[i] = FHP_LOW		
	ENDREPEAT	
	
	REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount i
		IF IS_BIT_SET(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
			IF ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[CHAR_MICHAEL])
				g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[CHAR_MICHAEL] = g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
			IF ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[CHAR_FRANKLIN])
				g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[CHAR_FRANKLIN] = g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_TREVOR))
			IF ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[CHAR_TREVOR])
				g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[CHAR_TREVOR] = g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
	ENDREPEAT	
	
	#IF IS_DEBUG_BUILD
		REPEAT 3 i
			IF eOldPriority[i] != g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[i]
				CPRINTLN(DEBUG_FLOW_HELP, "Queue priority for player character ", i," changed from ", ENUM_TO_INT(eOldPriority[i]), " to ", ENUM_TO_INT(g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[i]), ".")
			ENDIF
		ENDREPEAT
	#ENDIF
#endif

#if NOT USE_SP_DLC
	REPEAT 3 i		
		#IF IS_DEBUG_BUILD
			eOldPriority[i] = g_savedGlobals.sFlowHelp.eFlowHelpPriority[i]
		#ENDIF
		g_savedGlobals.sFlowHelp.eFlowHelpPriority[i] = FHP_LOW		
	ENDREPEAT	
	REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount i
		IF IS_BIT_SET(g_savedGlobals.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
			IF ENUM_TO_INT(g_savedGlobals.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobals.sFlowHelp.eFlowHelpPriority[CHAR_MICHAEL])
				g_savedGlobals.sFlowHelp.eFlowHelpPriority[CHAR_MICHAEL] = g_savedGlobals.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_savedGlobals.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
			IF ENUM_TO_INT(g_savedGlobals.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobals.sFlowHelp.eFlowHelpPriority[CHAR_FRANKLIN])
				g_savedGlobals.sFlowHelp.eFlowHelpPriority[CHAR_FRANKLIN] = g_savedGlobals.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_savedGlobals.sFlowHelp.sHelpQueue[i].iCharBitset, ENUM_TO_INT(CHAR_TREVOR))
			IF ENUM_TO_INT(g_savedGlobals.sFlowHelp.sHelpQueue[i].ePriority) > ENUM_TO_INT(g_savedGlobals.sFlowHelp.eFlowHelpPriority[CHAR_TREVOR])
				g_savedGlobals.sFlowHelp.eFlowHelpPriority[CHAR_TREVOR] = g_savedGlobals.sFlowHelp.sHelpQueue[i].ePriority
			ENDIF
		ENDIF
	ENDREPEAT	
	#IF IS_DEBUG_BUILD
		REPEAT 3 i
			IF eOldPriority[i] != g_savedGlobals.sFlowHelp.eFlowHelpPriority[i]
				CPRINTLN(DEBUG_FLOW_HELP, "Queue priority for player character ", i," changed from ", ENUM_TO_INT(eOldPriority[i]), " to ", ENUM_TO_INT(g_savedGlobals.sFlowHelp.eFlowHelpPriority[i]), ".")
			ENDIF
		ENDREPEAT
	#ENDIF
#endif

ENDPROC


PROC PRIVATE_Add_Help_To_Flow_Queue(STRING paramHelpLabel,  STRING paramSubstringLabel,	FlowHelpPriority paramPriority,	INT paramDelay,	INT paramExpiration, INT paramDuration, INT paramCharBitset, CC_CodeID paramCodeIDStart, CC_CodeID paramCodeIDDisplayed, INT paramSettings = FHF_NONE)
	
	CPRINTLN(DEBUG_FLOW_HELP, "Label ",paramHelpLabel, " added to help queue by script [", GET_THIS_SCRIPT_NAME(), "]. De:", paramDelay, " Ex:", paramExpiration, " Du:", paramDuration, ".")

	IF ARE_STRINGS_EQUAL(paramHelpLabel, "")
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. Label name was empty.")
		EXIT
	ENDIF
	IF paramDelay < 0
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. Delay time was negative.")
		EXIT
	ENDIF
	IF paramDuration < 500 AND paramDuration != FLOW_HELP_DISPLAY_FOREVER
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. The display duration was too short.")
		EXIT
	ENDIF
	IF paramExpiration < 0 AND paramExpiration != FLOW_HELP_NEVER_EXPIRES
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. The expiration time was invalid.")
		EXIT
	ENDIF
	IF paramCharBitset < 1 OR paramCharBitset > 7
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. The character bitset was out of the valid range.")
		EXIT
	ENDIF
	
	#if NOT USE_SP_DLC
		IF paramCodeIDStart = CID_MAX
			SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. CID_MAX is not a valid code ID to run.")
			EXIT
		ENDIF
		IF paramCodeIDDisplayed = CID_MAX
			SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. CID_MAX is not a valid code ID to run.")
			EXIT
		ENDIF	
	#endif
	
	//Check this help isn't already queued.
	INT iHelpIndex
	
#if USE_CLF_DLC	
	IF paramCodeIDStart = CID_CLF_MAX
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. CID_MAX is not a valid code ID to run.")
		EXIT
	ENDIF
	IF paramCodeIDDisplayed = CID_CLF_MAX
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. CID_MAX is not a valid code ID to run.")
		EXIT
	ENDIF

	REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount iHelpIndex
		IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[iHelpIndex].tHelpText, paramHelpLabel)
			CPRINTLN(DEBUG_FLOW_HELP, "Label ", paramHelpLabel, " was already queued. Won't queue it again.")
			EXIT
		ENDIF
	ENDREPEAT	
	IF g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount < FLOW_HELP_QUEUE_SIZE		
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].tHelpText 			= paramHelpLabel
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].tAddText			= paramSubstringLabel
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].iStartTime			= GET_GAME_TIMER() + paramDelay
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].iDuration 			= paramDuration
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].iCharBitset		= paramCharBitset
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].ePriority			= paramPriority
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].eCodeIDStart		= paramCodeIDStart
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].eCodeIDDisplayed	= paramCodeIDDisplayed
		g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].iSettingsFlags		= paramSettings
		
		IF paramExpiration != FLOW_HELP_NEVER_EXPIRES
			g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].iExpirationTime = GET_GAME_TIMER() + paramDelay + paramExpiration
		ELSE
			g_savedGlobalsClifford.sFlowHelp.sHelpQueue[g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount].iExpirationTime = FLOW_HELP_NEVER_EXPIRES
		ENDIF
		g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount++
		
		//Check if priority level has changed now that we've added a new item.
		PRIVATE_Update_Flow_Help_Queue_Priority_Level()		
	ELSE
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to queue flow help text as the queue was full.")
	ENDIF
#endif

#if USE_NRM_DLC	
	IF paramCodeIDStart = CID_NRM_MAX
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. CID_MAX is not a valid code ID to run.")
		EXIT
	ENDIF
	IF paramCodeIDDisplayed = CID_NRM_MAX
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to add help text to the queue. CID_MAX is not a valid code ID to run.")
		EXIT
	ENDIF


	REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount iHelpIndex
		IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[iHelpIndex].tHelpText, paramHelpLabel)
			CPRINTLN(DEBUG_FLOW_HELP, "Label ", paramHelpLabel, " was already queued. Won't queue it again.")
			EXIT
		ENDIF
	ENDREPEAT	
	IF g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount < FLOW_HELP_QUEUE_SIZE		
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].tHelpText 			= paramHelpLabel
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].tAddText			= paramSubstringLabel
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].iStartTime			= GET_GAME_TIMER() + paramDelay
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].iDuration 			= paramDuration
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].iCharBitset		= paramCharBitset
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].ePriority			= paramPriority
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].eCodeIDStart		= paramCodeIDStart
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].eCodeIDDisplayed	= paramCodeIDDisplayed
		g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].iSettingsFlags		= paramSettings
		
		IF paramExpiration != FLOW_HELP_NEVER_EXPIRES
			g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].iExpirationTime = GET_GAME_TIMER() + paramDelay + paramExpiration
		ELSE
			g_savedGlobalsnorman.sFlowHelp.sHelpQueue[g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount].iExpirationTime = FLOW_HELP_NEVER_EXPIRES
		ENDIF
		g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount++
		
		//Check if priority level has changed now that we've added a new item.
		PRIVATE_Update_Flow_Help_Queue_Priority_Level()		
	ELSE
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to queue flow help text as the queue was full.")
	ENDIF
#endif

#if NOT USE_SP_DLC
	REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount iHelpIndex
		IF ARE_STRINGS_EQUAL(g_savedGlobals.sFlowHelp.sHelpQueue[iHelpIndex].tHelpText, paramHelpLabel)
			CPRINTLN(DEBUG_FLOW_HELP, "Label ", paramHelpLabel, " was already queued. Won't queue it again.")
			EXIT
		ENDIF
	ENDREPEAT	
	IF g_savedGlobals.sFlowHelp.iFlowHelpCount < FLOW_HELP_QUEUE_SIZE		
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].tHelpText 			= paramHelpLabel
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].tAddText			= paramSubstringLabel
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].iStartTime			= GET_GAME_TIMER() + paramDelay
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].iDuration 			= paramDuration
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].iCharBitset		= paramCharBitset
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].ePriority			= paramPriority
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].eCodeIDStart		= paramCodeIDStart
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].eCodeIDDisplayed	= paramCodeIDDisplayed
		g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].iSettingsFlags		= paramSettings
		
		IF paramExpiration != FLOW_HELP_NEVER_EXPIRES
			g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].iExpirationTime = GET_GAME_TIMER() + paramDelay + paramExpiration
		ELSE
			g_savedGlobals.sFlowHelp.sHelpQueue[g_savedGlobals.sFlowHelp.iFlowHelpCount].iExpirationTime = FLOW_HELP_NEVER_EXPIRES
		ENDIF
		g_savedGlobals.sFlowHelp.iFlowHelpCount++
		
		//Check if priority level has changed now that we've added a new item.
		PRIVATE_Update_Flow_Help_Queue_Priority_Level()		
	ELSE
		SCRIPT_ASSERT("PRIVATE_Add_Help_To_Flow_Queue: Failed to queue flow help text as the queue was full.")
	ENDIF
#endif	

ENDPROC


PROC PRIVATE_Clear_Flow_Help_Queue()
	INT i
	
#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount i 
		PRIVATE_Clear_Queued_Help_At_Index(i)
	ENDREPEAT
	g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount = 0
	REPEAT ENUM_TO_INT(CHAR_TREVOR) i
		g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority[i] = FHP_LOW
	ENDREPEAT
#endif

#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount i 
		PRIVATE_Clear_Queued_Help_At_Index(i)
	ENDREPEAT
	g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount = 0
	REPEAT ENUM_TO_INT(CHAR_TREVOR) i
		g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority[i] = FHP_LOW
	ENDREPEAT
#endif

#if NOT USE_SP_DLC
	REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount i 
		PRIVATE_Clear_Queued_Help_At_Index(i)
	ENDREPEAT
	g_savedGlobals.sFlowHelp.iFlowHelpCount = 0
	REPEAT ENUM_TO_INT(CHAR_TREVOR) i
		g_savedGlobals.sFlowHelp.eFlowHelpPriority[i] = FHP_LOW
	ENDREPEAT
#endif	

	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
	
	CPRINTLN(DEBUG_FLOW_HELP, "Help text queue cleared by script [", GET_THIS_SCRIPT_NAME(), "].")

ENDPROC
