
/// PURPOSE: Returns coords of spaceship scrap pickup
FUNC VECTOR GET_UFOSCRAP_PICKUP_COORDS(INT iIndex, BOOL bForChop=FALSE)
		
	// If any of these have a bForChop vector set, Chop will lead the player to the bForChop vector then orientate himself towards the actual pickup vector.
	// Small alterations are fine but if you move a pickup more than 1-2 metres away from its current position you must update the bForChop vector too or else we'll have Chop leading the player to nothing.
	// The bForChop vector must be on a section of navmesh that is connected to the main navmesh. See Kev E if unsure.
	SWITCH iIndex
	
		CASE 0   RETURN << -1219.0, -3495.9, 12.9448 >> 			BREAK
		CASE 1
			IF bForChop = TRUE
				RETURN << 606.89252, -3250.18750, 5.06951 >>
			ELSE
				RETURN << 634.5865, -3232.7903, -16.5774 >>
			ENDIF
		BREAK
		CASE 2 RETURN <<1590.5997, -2810.1741, 3.4494>> BREAK
		CASE 3
			IF bForChop = TRUE
				RETURN << 327.99, -2757.61, 4.99 >>
			ELSE
				RETURN << 338.4039, -2762.1057, 42.6543 >>
			ENDIF
		BREAK
		CASE 4
			IF bForChop = TRUE
				RETURN << 1134.42163, -2607.02368, 14.77071 >>
			ELSE
				RETURN << 1133.6882, -2605.0728, 14.9729 >>
			ENDIF
		BREAK
		CASE 5
			IF bForChop = TRUE
				RETURN << 368.93164, -2118.53271, 15.40341 >>
			ELSE
				RETURN << 369.9450, -2116.7556, 16.1688 >>
			ENDIF
		BREAK
		CASE 6
			IF bForChop = TRUE
				RETURN << 1741.82, -1623.85, 111.41 >>
			ELSE
				RETURN << 1742.0242, -1618.7767, 111.8313 >>
			ENDIF
		BREAK
		CASE 7   RETURN << 287.7294, -1444.4000, 45.5095 >> 		BREAK
		CASE 8   RETURN << 17.5926, -1213.2073, 28.3678 >> 			BREAK
		CASE 9
			IF bForChop = TRUE
				RETURN << -910.01715, -1147.43994, 1.06785 >>
			ELSE
				RETURN << -900.3597, -1165.5511, 31.8047 >>
			ENDIF
		BREAK
		CASE 10
			IF bForChop = TRUE
				RETURN << 1237.72778, -1099.14966, 37.52579 >>
			ELSE
				RETURN << 1231.9734, -1102.3076, 34.4289 >>
			ENDIF
		BREAK
		CASE 11
			IF bForChop = TRUE
				RETURN <<87.69075, 810.37360, 210.12582>>
			ELSE
				RETURN <<81.2213, 814.0283, 213.2917>>
			ENDIF
		BREAK
		CASE 12
			IF bForChop = TRUE
				RETURN << -1900.9506, 1389.5819, 218.1509 >>
			ELSE
				RETURN << -1907.5149, 1388.6899, 217.9728 >>
			ENDIF
		BREAK
		CASE 13
			IF bForChop = TRUE
				RETURN << 467.36896, -730.75525, 26.36373 >>
			ELSE
				RETURN <<469.8529, -730.8, 26.3985>>
			ENDIF
		BREAK
		CASE 14  RETURN << 202.1081, -569.7198, 128.18 >> 			BREAK
		CASE 15
			IF bForChop = TRUE
				RETURN << 163.0531, -566.3394, 21.0290 >>
			ELSE
				RETURN << 159.39, -563.82, 21.0 >>
			ENDIF
		BREAK
		CASE 16
			IF bForChop = TRUE
				RETURN << -1183.15210, -518.53857, 38.53018 >>
			ELSE
				RETURN << -1182.5385, -525.5859, 39.9114 >>
			ENDIF
		BREAK
		CASE 17 RETURN <<-228.0476, -236.4184, 49.1361>> BREAK
		CASE 18  RETURN << -407.7026, -151.7918, 63.5505 >> 		BREAK
		CASE 19
			IF bForChop = TRUE
				RETURN << -1169.39746, -56.76701, 44.45705 >>
			ELSE
				RETURN << -1175.4877, -65.4649, 44.6563 >>
			ENDIF
		BREAK
		CASE 20
			IF bForChop = TRUE
				RETURN << 1679.05640, 39.44059, 160.77364 >>
			ELSE
				RETURN << 1684.6639, 40.7142, 153.4074 >>
			ENDIF
		BREAK
		CASE 21
			IF bForChop = TRUE
				RETURN << 1964.25, 553.68, 160.72 >>
			ELSE
				RETURN << 1965.58, 555.99, 160.79 >>
			ENDIF
		BREAK
		CASE 22
			IF bForChop = TRUE
				RETURN <<24.21706, 637.12122, 206.38968>>
			ELSE
				RETURN <<22.4492, 638.8155, 189.6085>>
			ENDIF
		BREAK
		CASE 23  RETURN << 2901.2041, 796.1192, 3.3556 >> 			BREAK
		CASE 24
			IF bForChop = TRUE
				RETURN <<-1531.89502, 870.32031, 180.67746>>
			ELSE
				RETURN <<-1529.7219, 871.4257, 180.6421>>
			ENDIF
		BREAK
		CASE 25  RETURN << -404.3191, 1100.8890, 331.5350 >> 		BREAK
		CASE 26  RETURN << -2809.3538, 1449.6429, 99.9280 >> 		BREAK
		CASE 27
			IF bForChop = TRUE
				RETURN <<3144.0452, 2184.4133, -5.2961>>
			ELSE
				RETURN <<3144.0452, 2184.4133, -5.2961>>
			ENDIF
		BREAK
		CASE 28  RETURN <<815.7574, 1850.8790, 120.1796>> 			BREAK
		CASE 29  RETURN << -1944.24, 1941.07, 162.8 >> 				BREAK
		
		CASE 30
			IF bForChop = TRUE
				RETURN <<-1452.23, 2127.41, 42.84>>
			ELSE
				RETURN <<-1436.87, 2130.22, 26.78>>
			ENDIF
		BREAK
		
		CASE 31  RETURN << 1367.4128, 2180.6316, 96.6914 >> 		BREAK
		CASE 32
			IF bForChop = TRUE
				RETURN << 170.16521, 2217.63696, 89.30811 >>
			ELSE
				RETURN << 172.1426, 2220.1313, 89.7842 >>
			ENDIF
		BREAK
		CASE 33  RETURN << 889.3209, 2870.0522, 55.2834 >> 			BREAK
		CASE 34	
			IF bForChop = TRUE	
				RETURN << 1980.20, 2914.79, 45.48 >>
			ELSE
				RETURN << 1963.55, 2922.81, 57.76 >>
			ENDIF
		BREAK
		CASE 35  RETURN << -390.3840, 2963.2605, 18.2713 >> 		BREAK
		CASE 36  RETURN << 71.6642, 3279.3682, 30.3918 >> 			BREAK
		CASE 37  RETURN << 1924.1864, 3471.2563, 50.3238 >> 		BREAK
		CASE 38  RETURN <<-583.1569, 3580.3752, 266.2471>> 			BREAK
		CASE 39
			IF bForChop = TRUE
				RETURN << 2514.30151, 3789.51929, 52.07700 >>
			ELSE
				RETURN << 2516.98, 3789.35, 53.79 >>
			ENDIF
		BREAK
		CASE 40
			IF bForChop = TRUE
				RETURN << 1517.61292, 3803.92603, 30.95605 >>
			ELSE
				RETURN <<1486.0475, 3857.2168, 22.2905>>
			ENDIF
		BREAK
		CASE 41
			IF bForChop = TRUE
				RETURN << -530.27386, 4474.35840, 59.45993 >>
			ELSE
				RETURN << -528.8246, 4440.6436, 31.7384 >>
			ENDIF
		BREAK
		CASE 42
			IF bForChop = TRUE
				RETURN << 3815.05493, 4447.34277, 2.06631 >>
			ELSE
				RETURN << 3820.8132, 4441.6831, 1.8007 >>
			ENDIF
		BREAK
		CASE 43
			IF bForChop = TRUE
				RETURN << -1946.95, 4584.36, 56.06 >>
			ELSE
				RETURN << -1943.0546, 4585.1943, 46.6362 >>
			ENDIF
		BREAK
		CASE 44  RETURN << 2437.5452, 4779.9595, 33.5101 >> 		BREAK
		CASE 45  RETURN << -1441.4948, 5414.8882, 23.3000 >> 		BREAK
		CASE 46  RETURN << 2196.2507, 5599.0342, 52.7129 >> 		BREAK
		CASE 47
			IF bForChop = TRUE
				RETURN <<-503.95, 5673.55, 51.24>>
			ELSE
				RETURN <<-503.2193, 5665.6104, 48.8487>>
			ENDIF
		BREAK
		CASE 48
			IF bForChop = TRUE
				RETURN << -378.41922, 6080.78223, 30.44296 >>
			ELSE
				RETURN << -381.2910, 6086.9492, 38.6147 >>
			ENDIF
		BREAK
		CASE 49  RETURN << 440.9546, 6459.6416, 27.7432 >> 			BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid index passed to GET_UFOSCRAP_PICKUP_COORDS")
	RETURN <<0,0,0>>
ENDFUNC		

/// PURPOSE:
///    Returns true if Chop can reach the scrap, else false
/// PARAMS:
///    index - the index of the pickup
FUNC BOOL CAN_CHOP_REACH_UFOSCRAP(INT index)
	SWITCH (index)
		CASE 2 FALLTHRU	// On island
		CASE 14	FALLTHRU	// On roof
		CASE 18	FALLTHRU	// On roof
		CASE 25	FALLTHRU	// On observatory roof beyond locked gates
			RETURN FALSE
		BREAK
		CASE 16
			IF IS_BIT_SET(g_iRestrictedAreaBitSet, 7) //AC_MOVIE_STUDIO
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
