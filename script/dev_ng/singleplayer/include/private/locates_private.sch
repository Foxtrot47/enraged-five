USING "rage_builtins.sch"
USING "commands_camera.sch"
USING "model_enums.sch"
USING "commands_audio.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_VEHICLE.sch"
USING "commands_zone.sch"
USING "dialogue_public.sch"
USING "globals.sch"
USING "script_player.sch"
USING "script_blips.sch"
USING "ambient_speech.sch"
USING "buddy_head_track_public.sch"

CONST_FLOAT BUDDY_WILL_GET_IN_SPECIFIC_VEHICLE_DIST 20.0
CONST_FLOAT BUDDY_SEPARATION_DIST 50.0

CONST_INT BS_PRINTED_LOSE_WANTED_LEVEL						0
CONST_INT BS_PLAYED_LOST_COPS_SPEECH						1
CONST_INT BS_INITIAL_GOD_TEXT_PRINTED						2
CONST_INT BS_PRINTED_GET_IN_VEHICLE							3
CONST_INT BS_PRINTED_GET_BACK_IN_VEHICLE					4
CONST_INT BS_PRINTED_PICKUP_ALL_BUDDY_TEXT					5
CONST_INT BS_USE_SPECIFIC_J_SKIP_COORDS						6
CONST_INT BS_DONT_DO_SAFE_FOR_CUTSCENE_CHECK				7
CONST_INT BS_LOCATES_HEADER_WIDGET_CREATED					8
CONST_INT BS_HAS_BEEN_IN_VEHICLE							9
CONST_INT BS_BUDDIES_WILL_WALK_TO_SPECIFIC_VEHICLE			10
CONST_INT BS_BUDDIES_WILL_ONLY_GET_IN_WHEN_PLAYER_DOES		11
CONST_INT BS_DO_LOCATES_ALMOST_STOPPED_CHECK				12
CONST_INT BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS		13
CONST_INT BS_PRINTED_PICKUP_BUDDY_TEXT_0					14
CONST_INT BS_PRINTED_PICKUP_BUDDY_TEXT_1					15
CONST_INT BS_PRINTED_PICKUP_BUDDY_TEXT_2					16
CONST_INT BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_0			17
CONST_INT BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_1			18
CONST_INT BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_2			19
CONST_INT BS_DONT_DO_J_SKIP									20
CONST_INT BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR			21
CONST_INT BS_PLAYER_STARTS_RIGHT_NEXT_TO_VEHICLE			22
CONST_INT BS_PLAYED_GET_BACK_IN_VEHICLE_SPEECH				23
CONST_INT BS_DONT_CLEAR_LOCATE_AREA_OF_VEHICLES				24
CONST_INT BS_WINDSCREEN_FLAGS_HAVE_BEEN_SET					25
CONST_INT BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START		26
CONST_INT BS_BUDDIES_GIVEN_DEFAULT_PASSENGER_INDEX			27
CONST_INT BS_BUDDIES_SET_TO_ACTION_MODE						28
CONST_INT BS_BLOCK_BUDDIES_SETTING_TO_ACTION_MODE			29
CONST_INT BS_DONT_REMOVE_BUDDIES_FROM_GROUP_ON_CLEANUP		30
CONST_INT BS_PLAYER_IN_UNSUITABLE_VEHICLE					31

ENUM IS_ANY_TEXT_DISPLAYED_FLAGS
	IAT_DO_ALL_CHECKS,
	IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF,
	IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF
ENDENUM

STRUCT LOCATES_HEADER_DATA
	BLIP_INDEX vehicleBlip
	BLIP_INDEX BuddyBlipID[3]
	BLIP_INDEX LocationBlip
	BLIP_INDEX GPSBlipID

	FLOAT fClearvehicleDistance = 10.0
	FLOAT fBuddyJoinDistance = 20.0
	FLOAT fSkipHeading

	INT iGodTextTime
	INT iLeftBuddyTime
	INT iPrintedMissingBuddiesNumber
	INT iLocatesBitSet // used for all the flags
	INT iAmbientDialogueBitSet //Used for ambient scripts that replace god text with dialogue
	
	BOOL bDontClearTasksIfEnteringNearestVehicle

	PED_INDEX SpeakingBuddyID
	PED_INDEX LocatesBuddyID[3]
	
	VEHICLE_INDEX vehStartCar

	VECTOR vSkipCoords
ENDSTRUCT

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID LocatesWidgetID = NULL
#ENDIF

#IF IS_DEBUG_BUILD
	PROC SET_LOCATES_HEADER_WIDGET_GROUP(WIDGET_GROUP_ID parent_widget_group_id)
		LocatesWidgetID = parent_widget_group_id
	ENDPROC

	PROC CREATE_LOCATES_HEADER_WIDGET(LOCATES_HEADER_DATA &locatesData)
	    IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_LOCATES_HEADER_WIDGET_CREATED)
			IF (LocatesWidgetID != NULL)
				SET_CURRENT_WIDGET_GROUP(LocatesWidgetID)
				
				START_WIDGET_GROUP("Locates Header")
		            ADD_WIDGET_FLOAT_SLIDER("locatesData.fBuddyJoinDistance", locatesData.fBuddyJoinDistance, 0.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("locatesData.fClearvehicleDistance", locatesData.fClearvehicleDistance, 0.0, 100.0, 0.1)
		            ADD_WIDGET_FLOAT_SLIDER("locatesData.fSkipHeading", locatesData.fSkipHeading, 0.0, 360.0, 0.1)
		            //ADD_WIDGET_INT_SLIDER("locatesData.iGodTextTime", locatesData.iGodTextTime, -10000, 10000, 1)
		            ADD_WIDGET_FLOAT_SLIDER("locatesData.vSkipCoords.x", locatesData.vSkipCoords.x, -9999.0, 9999.0, 0.1)
		            ADD_WIDGET_FLOAT_SLIDER("locatesData.vSkipCoords.y", locatesData.vSkipCoords.y, -9999.0, 9999.0, 0.1)
		            ADD_WIDGET_FLOAT_SLIDER("locatesData.vSkipCoords.z", locatesData.vSkipCoords.z, -9999.0, 9999.0, 0.1)
		        STOP_WIDGET_GROUP()
		        SET_BIT(locatesData.iLocatesBitSet, BS_LOCATES_HEADER_WIDGET_CREATED)
				
				CLEAR_CURRENT_WIDGET_GROUP(LocatesWidgetID)
			ENDIF 
	    ENDIF
	ENDPROC
#ENDIF

FUNC BOOL HAS_PRINTED_PICKUP_BUDDY_TEXT(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0
			RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0))
		BREAK
		CASE 1
			RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1))
		BREAK
		CASE 2
			RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2))
		BREAK
	ENDSWITCH
	RETURN(FALSE)
ENDFUNC
PROC CLEAR_BIT_PRINTED_PICKUP_BUDDY_TEXT(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0)
		BREAK
		CASE 1
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1)	
		BREAK
		CASE 2
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2)
		BREAK
	ENDSWITCH
ENDPROC
PROC SET_BIT_PRINTED_PICKUP_BUDDY_TEXT(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0
			SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0)
		BREAK
		CASE 1
			SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1)	
		BREAK
		CASE 2
			SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2)
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL IS_BIT_SET_PRINTED_PICKUP_BUDDY_TEXT(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0 RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0))
		CASE 1 RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1))
		CASE 2 RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2))
	ENDSWITCH
	RETURN(FALSE)
ENDFUNC


PROC CLEAR_BIT_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_0)
		BREAK
		CASE 1
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_1)	
		BREAK
		CASE 2
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_2)
		BREAK
	ENDSWITCH
ENDPROC
PROC SET_BIT_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0
			SET_BIT(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_0)
		BREAK
		CASE 1
			SET_BIT(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_1)	
		BREAK
		CASE 2
			SET_BIT(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_2)
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL IS_BIT_SET_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(INT iIndex, LOCATES_HEADER_DATA &locatesData)
	SWITCH iIndex
		CASE 0 RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_0))
		CASE 1 RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_1))
		CASE 2 RETURN(IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX_2))
	ENDSWITCH
	RETURN(FALSE)
ENDFUNC


// ***************************************************************************
PROC SAFE_CLEAR_THIS_PRINT(STRING sInString)
    IF NOT IS_STRING_NULL(sInString)
        CLEAR_THIS_PRINT(sInString)
    ENDIF
ENDPROC

PROC TURN_ON_GPS_FOR_BLIP(BLIP_INDEX inBlip, LOCATES_HEADER_DATA &locatesData)    
    IF DOES_BLIP_EXIST(inBlip)
        // switch off any existing route
        IF DOES_BLIP_EXIST(locatesData.GPSBlipID)
            SET_BLIP_ROUTE(locatesData.GPSBlipID, FALSE)  
        ENDIF
		
		SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		CLEAR_GPS_MULTI_ROUTE()
		
        locatesData.GPSBlipID = inBlip
        SET_BLIP_ROUTE(inBlip, TRUE)
    ENDIF
ENDPROC

FUNC BOOL IS_POINT_VISIBLE(VECTOR vecCoords, FLOAT radius, FLOAT fDist = 100.0)    
    IF IS_SPHERE_VISIBLE(vecCoords, radius)
        IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vecCoords, <<fDist, fDist, fDist>>, FALSE, FALSE, TM_ANY)
            RETURN(FALSE)
        ELSE
            IF IS_SCREEN_FADED_OUT()
                RETURN(FALSE)
            ELSE
                RETURN(TRUE)
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

PROC CLEAR_MISSION_LOCATION_BLIP(LOCATES_HEADER_DATA &locatesData)
    IF DOES_BLIP_EXIST(locatesData.LocationBlip)
        REMOVE_BLIP(locatesData.LocationBlip)
    ENDIF
ENDPROC

PROC GIVE_VEHICLE_MINIMUM_HEALTH(VEHICLE_INDEX vehicleID)
    FLOAT fHealth
    IF IS_VEHICLE_DRIVEABLE(vehicleID)
        fHealth = GET_VEHICLE_ENGINE_HEALTH(vehicleID)  
        IF (fHealth > 0.0)
            PRINTSTRING("GIVE_VEHICLE_MINIMUM_HEALTH - fHealth = ")
            PRINTFLOAT(fHealth)
            PRINTNL()
            //SET_ENTITY_HEALTH(vehicleID, 1)
            SET_VEHICLE_ENGINE_ON(vehicleID, FALSE, TRUE)
            SET_VEHICLE_ENGINE_HEALTH(vehicleID, -1000.0)
            SET_VEHICLE_DAMAGE(vehicleID, <<0.0, 0.0, 0.0>>, 1000.0, 0.0, TRUE)
        ENDIF
    ENDIF
ENDPROC

PROC CLEAR_MISSION_LOCATE_STUFF(LOCATES_HEADER_DATA &locatesData, BOOL bRemoveBuddiesFromGroup = TRUE, BOOL bResetWindscreenFlags = FALSE)
	IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_REMOVE_BUDDIES_FROM_GROUP_ON_CLEANUP)
		bRemoveBuddiesFromGroup = TRUE
	ENDIF	

    INT i
    CLEAR_MISSION_LOCATION_BLIP(locatesData)
    REPEAT 3 i
        IF DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
            REMOVE_BLIP(locatesData.BuddyBlipID[i])
        ENDIF
		CLEAR_BIT_PRINTED_PICKUP_BUDDY_TEXT(i, locatesData)
		CLEAR_BIT_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(i, locatesData)
    ENDREPEAT
	REPEAT 31 i
		IF i != BS_LOCATES_HEADER_WIDGET_CREATED //Don't want to delete the widget
			CLEAR_BIT(locatesData.iLocatesBitSet, i)
			CLEAR_BIT(locatesData.iAmbientDialogueBitSet, i)
		ENDIF
	ENDREPEAT
	
    IF DOES_BLIP_EXIST(locatesData.vehicleBlip)
        REMOVE_BLIP(locatesData.vehicleBlip)
    ENDIF
	
    locatesData.GPSBlipID = NULL

	locatesData.iPrintedMissingBuddiesNumber = 0
	locatesData.bDontClearTasksIfEnteringNearestVehicle = FALSE
    
    // switch ambient anim blocking back on
    REPEAT 3 i 
        IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
            SET_PED_CAN_PLAY_AMBIENT_ANIMS(locatesData.LocatesBuddyID[i], true) 
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(locatesData.LocatesBuddyID[i], true) 
			IF bResetWindscreenFlags
				SET_PED_CONFIG_FLAG(locatesData.LocatesBuddyID[i], PCF_WillFlyThroughWindscreen, TRUE)
				SET_PED_CONFIG_FLAG(locatesData.LocatesBuddyID[i], PCF_DisableJumpingFromVehiclesAfterLeader, FALSE)
			ENDIF
			
			SET_PED_CONFIG_FLAG(locatesData.LocatesBuddyID[i], PCF_TeleportToLeaderVehicle, FALSE)
			
			IF bRemoveBuddiesFromGroup
				IF IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
				AND locatesData.LocatesBuddyID[i] != PLAYER_PED_ID() //This is in case a switch took place, if a locates buddy is now the player then don't remove them from the group.
					REMOVE_PED_FROM_GROUP(locatesData.LocatesBuddyID[i])
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BLOCK_BUDDIES_SETTING_TO_ACTION_MODE)
				SET_PED_USING_ACTION_MODE(locatesData.LocatesBuddyID[i], FALSE)
			ENDIF
			
			//Fix for GTAV 682408: if the player uses the header, clears it, switches, then clears it again, one of the buddy IDs could now be the player. Make sure all the buddies are wiped on a clear.
			locatesData.LocatesBuddyID[i] = NULL
        ENDIF
    ENDREPEAT
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), true) 
		SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(player_ped_id(), true)
    ENDIF
    // set player will fly through windscreen
    IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF bResetWindscreenFlags
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		ENDIF
    ENDIF
	
	locatesData.vehStartCar = NULL
	
	#IF IS_DEBUG_BUILD
		PRINTLN("CLEAR_MISSION_LOCATE_STUFF called by: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
ENDPROC

PROC GOD_TEXT(LOCATES_HEADER_DATA &locatesData, STRING strMessage, BOOL bIsReminder = FALSE)
    IF NOT (bIsReminder)
        IF NOT IS_STRING_NULL(strMessage)
            IF NOT ARE_STRINGS_EQUAL(strMessage, "")
                PRINT_NOW(strMessage, DEFAULT_GOD_TEXT_TIME, 1)
            ENDIF
        ENDIF
    ELSE
        //PRINT_NOW(strMessage, DEFAULT_REMINDER_TEXT_TIME, 1)
    ENDIF
    locatesData.iGodTextTime = GET_GAME_TIMER()
ENDPROC

PROC GOD_TEXT_FOR_CREW_MEMBER(LOCATES_HEADER_DATA &locatesData, STRING strMessage, STRING strCrewName, BOOL bIsReminder = FALSE)
    IF NOT (bIsReminder)
        IF NOT IS_STRING_NULL(strMessage)
            IF NOT ARE_STRINGS_EQUAL(strMessage, "")
				PRINT_STRING_IN_STRING_NOW(strMessage, strCrewName, DEFAULT_GOD_TEXT_TIME, 1) 
            ENDIF
        ENDIF
    ELSE
        //PRINT_NOW(strMessage, DEFAULT_REMINDER_TEXT_TIME, 1)
    ENDIF
    locatesData.iGodTextTime = GET_GAME_TIMER()
ENDPROC

PROC EMPTY_VEHICLE(VEHICLE_INDEX invehicle)

    PED_INDEX temp_char
    INT i
    INT iMaxNumberOfPassengers

    // clear tasks of any passengers or drivers
    IF DOES_ENTITY_EXIST(invehicle)
        IF IS_VEHICLE_DRIVEABLE(invehicle)
            // driver
            temp_char = GET_PED_IN_VEHICLE_SEAT(invehicle)  
            IF DOES_ENTITY_EXIST(temp_char)
                IF NOT IS_PED_INJURED(temp_char)
                    CLEAR_PED_TASKS_IMMEDIATELY(temp_char)
                ELSE
                    DELETE_PED(temp_char)
                ENDIF
            ENDIF
            
            iMaxNumberOfPassengers = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(invehicle) 

            // passengers
            REPEAT iMaxNumberOfPassengers i
                IF NOT IS_VEHICLE_SEAT_FREE(invehicle, INT_TO_ENUM(VEHICLE_SEAT, i))
                    temp_char = GET_PED_IN_VEHICLE_SEAT(invehicle, INT_TO_ENUM(VEHICLE_SEAT, i)) 
                    IF DOES_ENTITY_EXIST(temp_char)
                        IF NOT IS_PED_INJURED(temp_char)
                            CLEAR_PED_TASKS_IMMEDIATELY(temp_char)
                        ELSE
                            DELETE_PED(temp_char)
                        ENDIF
                    ENDIF
                ENDIF
            ENDREPEAT
        ENDIF
    ENDIF

ENDPROC


FUNC BOOL IS_CHAR_USING_SAME_VEHICLE_AS_PLAYER(PED_INDEX PedID)
    VEHICLE_INDEX tempvehicle1
    VEHICLE_INDEX tempvehicle2
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        tempvehicle1 = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
        IF IS_VEHICLE_DRIVEABLE(tempvehicle1)
            IF NOT IS_PED_INJURED(PedID)
                tempvehicle2 = GET_VEHICLE_PED_IS_USING(PedID)
                IF IS_VEHICLE_DRIVEABLE(tempvehicle2)
                    IF (tempvehicle1 = tempvehicle2)
                        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), PedID, <<20.0, 20.0, 20.0>>, FALSE)
                        AND IS_ENTITY_AT_ENTITY(PedID, tempvehicle2, <<20.0, 20.0, 20.0>>, FALSE)
                            RETURN(TRUE)
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_CHAR_USING_VEHICLE(PED_INDEX PedID, VEHICLE_INDEX vehicleID)
    VEHICLE_INDEX temp_VEHICLE
    IF NOT IS_PED_INJURED(PedID)
        IF IS_VEHICLE_DRIVEABLE(vehicleID)
            temp_VEHICLE = GET_VEHICLE_PED_IS_USING(PedID)
            IF (temp_VEHICLE = vehicleID)
                RETURN(TRUE)
            ENDIF
        ENDIF   
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PED_IN_ANY_SPEEDING_VEHICLE(PED_INDEX ped, FLOAT fSpeedThreshold = 5.0)
	IF IS_PED_IN_ANY_VEHICLE(ped)
		VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(ped)
		
		IF NOT IS_ENTITY_DEAD(vehTemp)
			IF GET_ENTITY_SPEED(vehTemp) > fSpeedThreshold
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC STORE_EXISTING_VEHICLE_AND_EMPTY(VEHICLE_INDEX &Returnvehicle)
    // store existing vehicle if there is one
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
             Returnvehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        ENDIF
    ENDIF
    IF NOT DOES_ENTITY_EXIST(Returnvehicle)
        Returnvehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
    ENDIF
    // clear tasks of any passengers or drivers
    IF DOES_ENTITY_EXIST(Returnvehicle)
        IF IS_VEHICLE_DRIVEABLE(Returnvehicle)
            EMPTY_VEHICLE(Returnvehicle)
        ELSE
            Returnvehicle = NULL
        ENDIF
    ELSE
        Returnvehicle = NULL
    ENDIF
ENDPROC

PROC SAFE_WARP_PLAYER_TO_COORDINATE(VECTOR vecCoords, FLOAT fHead = 0.0)
    vector ped_coords
    vecCoords.z += 1.0
    SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vecCoords)
    SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)    
    REQUEST_COLLISION_AT_COORD(vecCoords)
    LOAD_ALL_OBJECTS_NOW()
    //LOAD_SCENE     @_No_of_params (vecCoords)
    ped_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
    GET_GROUND_Z_FOR_3D_COORD(ped_coords, vecCoords.z)
    SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<ped_coords.x, ped_coords.y, vecCoords.z>>)
    SET_ENTITY_HEADING(PLAYER_PED_ID(), fHead)
ENDPROC

PROC ENSURE_LOCATE_IS_CLEAR_OF_RANDOM_VEHICLES(VECTOR vecCoords, LOCATES_HEADER_DATA &locatesData)
	IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_CLEAR_LOCATE_AREA_OF_VEHICLES)
	    IF NOT IS_POINT_VISIBLE(vecCoords, locatesData.fClearvehicleDistance)
	        IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vecCoords, <<50.0, 50.0, 50.0>>, FALSE, TRUE)
	            VEHICLE_INDEX Tempvehicle   
	            Tempvehicle = GET_PLAYERS_LAST_VEHICLE()
	            IF DOES_ENTITY_EXIST(Tempvehicle)
	                IF NOT IS_ENTITY_DEAD(Tempvehicle)
	                    IF NOT IS_ENTITY_AT_COORD(Tempvehicle, vecCoords, <<locatesData.fClearvehicleDistance, locatesData.fClearvehicleDistance, locatesData.fClearvehicleDistance>>, FALSE, TRUE)
	                        CLEAR_AREA_OF_VEHICLES (vecCoords, locatesData.fClearvehicleDistance)   
	                    ENDIF
	                ENDIF
	            ELSE
	                CLEAR_AREA_OF_VEHICLES(vecCoords, locatesData.fClearvehicleDistance)
	            ENDIF
	        ENDIF
	    ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_CHARS_IN_SAME_VEHICLE(PED_INDEX inPed1, PED_INDEX inPed2)
    VEHICLE_INDEX Tempvehicle
    IF NOT IS_PED_INJURED(inPed1)
        IF IS_PED_IN_ANY_VEHICLE(inPed1)
            Tempvehicle = GET_VEHICLE_PED_IS_IN(inPed1)
            IF IS_VEHICLE_DRIVEABLE(Tempvehicle)
                IF NOT IS_PED_INJURED(inPed2)
                    IF IS_PED_IN_VEHICLE(inPed2, Tempvehicle)
                        RETURN(TRUE)
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL ARE_CHARS_SITTING_IN_SAME_VEHICLE(PED_INDEX inPed1, PED_INDEX inPed2)
    VEHICLE_INDEX Tempvehicle
    IF NOT IS_PED_INJURED(inPed1)
        IF IS_PED_SITTING_IN_ANY_VEHICLE(inPed1)
            Tempvehicle = GET_VEHICLE_PED_IS_IN(inPed1)
            IF IS_VEHICLE_DRIVEABLE(Tempvehicle)
                IF NOT IS_PED_INJURED(inPed2)
                    IF IS_PED_SITTING_IN_VEHICLE(inPed2, Tempvehicle)
                        RETURN(TRUE)
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_VEHICLE_ALMOST_STOPPED(VEHICLE_INDEX invehicle)
    FLOAT fTemp
    IF NOT IS_ENTITY_DEAD(invehicle)
        fTemp = GET_ENTITY_SPEED(invehicle)
        IF (fTemp > -0.5)
        AND (fTemp < 0.5)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_CHAR_ALMOST_STOPPED(PED_INDEX inChar)
    FLOAT fTemp
    IF NOT IS_PED_INJURED(inChar)
        fTemp = GET_ENTITY_SPEED(inChar)
        IF (fTemp > -0.5)
        AND (fTemp < 0.5)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL ARE_CHARS_RIDING_TOGETHER(PED_INDEX inPed1, PED_INDEX inPed2, FLOAT fDist = 15.0)
    VEHICLE_INDEX tempvehicle1
    VEHICLE_INDEX tempvehicle2
    VECTOR pos
    IF NOT IS_PED_INJURED(inPed1)
        IF NOT IS_PED_INJURED(inPed2)
            IF IS_PED_SITTING_IN_ANY_VEHICLE(inPed1)
                tempvehicle1 = GET_VEHICLE_PED_IS_IN(inPed1)
                IF IS_PED_SITTING_IN_ANY_VEHICLE(inPed2)
                    tempvehicle2 = GET_VEHICLE_PED_IS_IN(inPed2)
                    IF IS_VEHICLE_DRIVEABLE(tempvehicle1)
                    AND IS_VEHICLE_DRIVEABLE(tempvehicle2)
                        IF (tempvehicle1 = tempvehicle2)
                            RETURN(TRUE)
                        ELSE
                            pos = GET_ENTITY_COORDS(tempvehicle1)
                            IF IS_ENTITY_AT_COORD(tempvehicle2, <<pos.x, pos.y, pos.z>>, <<fDist, fDist, 5.0>>, FALSE, TRUE)
                                RETURN(TRUE)    
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_BUDDY_IN_SAME_GROUP_MOVEMENT(PED_INDEX inBuddy, BOOL bPedsNeedToBeInVehicleWithPlayer = TRUE)
    IF NOT IS_PED_INJURED(inBuddy)
        IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND bPedsNeedToBeInVehicleWithPlayer
            IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), inBuddy)
                SET_GROUP_SEPARATION_RANGE(PLAYER_GROUP_ID(), BUDDY_SEPARATION_DIST)
                RETURN(TRUE)
            ENDIF
        ELSE
            IF IS_PED_GROUP_MEMBER(inBuddy, PLAYER_GROUP_ID())
                SET_GROUP_SEPARATION_RANGE(PLAYER_GROUP_ID(), BUDDY_SEPARATION_DIST)
                RETURN(TRUE)
            ENDIF
        ENDIF
    ELSE
        RETURN(TRUE)
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL HAS_GOD_TEXT_JUST_BEEN_PRINTED(LOCATES_HEADER_DATA &locatesData)
    INT iDiff
    INT iCurrentTime
    iCurrentTime = GET_GAME_TIMER()
    iDiff = iCurrentTime - locatesData.iGodTextTime
    IF (iDiff < 35)
        RETURN(TRUE)
    ENDIF
    RETURN(FALSE)
ENDFUNC

/// PURPOSE:
///    Checks if god text is displaying or a conversation is ongoing or queued. Will also check if any prints coming from the locates
///    header were triggered this frame, this check does nothing if the print didn't come from the locates header.
/// PARAMS:
///    locatesData - The locates header struct: used for checking if any locates header prints were triggered this frame.
///    eIsAnyTextFlag - Lets you modify the checks depending on if subtitles are enabled or not:
///    					IAT_DO_ALL_CHECKS: does all checks (default).
///    					IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF: Use this if you want to play a conversation over god text when subtitles are turned off.
///    					IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF: Use this if you want to print god text over a conversation when subtitles are turned off.
FUNC BOOL IS_ANY_TEXT_BEING_DISPLAYED(LOCATES_HEADER_DATA &locatesData, IS_ANY_TEXT_DISPLAYED_FLAGS eIsAnyTextFlag = IAT_DO_ALL_CHECKS)
	//Check if any god text is being displayed (ignore if flag is set and subtitles are off).
	IF (eIsAnyTextFlag != IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF OR IS_SUBTITLE_PREFERENCE_SWITCHED_ON()) 
		//Normal messages.
		IF IS_MESSAGE_BEING_DISPLAYED()
			RETURN TRUE
		ENDIF
		
		//Check if the locates header printed something this frame.
		IF HAS_GOD_TEXT_JUST_BEEN_PRINTED(locatesData)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Check if any conversations are ongoing (ignore if flag is set and subtitles are off).
	IF (eIsAnyTextFlag != IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF OR IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			RETURN TRUE
		ENDIF
	ENDIF
	
    RETURN FALSE
ENDFUNC

FUNC BOOL DOES_SPECIFIC_VEHICLE_EXIST_AND_IS_CHAR_IN_IT(PED_INDEX inChar, VEHICLE_INDEX invehicle)
    IF NOT IS_PED_INJURED(inChar)
        IF DOES_ENTITY_EXIST(invehicle)
            IF IS_VEHICLE_DRIVEABLE(invehicle)
                IF IS_PED_SITTING_IN_VEHICLE(inChar, invehicle)
                    RETURN(TRUE)
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC     

FUNC BOOL IS_BUDDY_PLAYING_AMBIENT_SPEECH(LOCATES_HEADER_DATA &locatesData)
    IF NOT IS_PED_INJURED(locatesData.SpeakingBuddyID)
        IF IS_AMBIENT_SPEECH_PLAYING(locatesData.SpeakingBuddyID)
            RETURN(TRUE)
        ENDIF   
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL SAFE_IS_THIS_PRINT_BEING_DISPLAYED(STRING sInString)
    IF NOT IS_STRING_NULL(sInString)
        IF IS_THIS_PRINT_BEING_DISPLAYED(sInString)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

/// PURPOSE:
///    Checks all of the vehicle stuck timers, if any of them have ran out then the vehicle will be considered permanently stuck. 
FUNC BOOL IS_VEHICLE_PERMANENTLY_STUCK(VEHICLE_INDEX &veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_JAMMED, JAMMED_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, SIDE_TIME)
			RETURN TRUE
		ENDIF 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_PLAY_DIALOGUE_WITH_WANTED_LEVEL(LOCATES_HEADER_DATA &locatesData)
    IF IS_BUDDY_PLAYING_AMBIENT_SPEECH()
    OR IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
        RETURN(FALSE)
    ENDIF
    IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
    AND NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
    AND NOT SAFE_IS_THIS_PRINT_BEING_DISPLAYED("LOSE_WANTED")
        RETURN(FALSE)
    ENDIF
    RETURN(TRUE)
ENDFUNC 

PROC UPDATE_CHARS_TO_NOT_FLY_THROUGH_WINDSCREEN(LOCATES_HEADER_DATA &locatesData)
	IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_WINDSCREEN_FLAGS_HAVE_BEEN_SET)
	    INT i
	    IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	    ENDIF
	    REPEAT 3 i 
	        IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
	            IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
					SET_PED_CONFIG_FLAG(locatesData.LocatesBuddyID[i], PCF_WillFlyThroughWindscreen, FALSE)
					SET_PED_CONFIG_FLAG(locatesData.LocatesBuddyID[i], PCF_DisableJumpingFromVehiclesAfterLeader, TRUE)
					SET_PED_CONFIG_FLAG(locatesData.LocatesBuddyID[i], PCF_TeleportToLeaderVehicle, TRUE)
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(locatesData.LocatesBuddyID[i], FALSE) //GTAV 766697 - If the peds were warped directly into the car this flag wasn't set.
	            ENDIF
	        ENDIF
	    ENDREPEAT
		
		SET_BIT(locatesData.iLocatesBitSet, BS_WINDSCREEN_FLAGS_HAVE_BEEN_SET)
	ENDIF
ENDPROC

FUNC BOOL DOES_VEHICLE_HAVE_ENOUGH_SEATS(VEHICLE_INDEX vehicleID, LOCATES_HEADER_DATA &locatesData, INT iMinRequiredSeats = 0)   
    INT iNumberOfBuddies
    INT iMaxPassengers
    INT i   
    IF IS_VEHICLE_DRIVEABLE(vehicleID)
        iNumberOfBuddies = 0
        REPEAT 3 i
            IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
                iNumberOfBuddies++  
            ENDIF
        ENDREPEAT
		
        iMaxPassengers = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicleID)
		IF (iMinRequiredSeats > 0)
			IF (iMaxPassengers >= iMinRequiredSeats)
				IF iMinRequiredSeats > 1
					IF NOT IS_SEAT_WARP_ONLY(vehicleID, VS_BACK_LEFT) //Additional check: some cars have seats that are are only accessible by warping.
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF 	
		ELSE
			IF (iMaxPassengers >= iNumberOfBuddies)
				IF iNumberOfBuddies > 1
					IF NOT IS_SEAT_WARP_ONLY(vehicleID, VS_BACK_LEFT)
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF 
		ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL private_IS_VEHICLE_AN_OCCUPIED_BUS(VEHICLE_INDEX veh, LOCATES_HEADER_DATA &locatesData)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF GET_ENTITY_MODEL(veh) = BUS
		OR GET_ENTITY_MODEL(veh) = COACH
			INT iNumFreeSeatsRequired = 0
			INT iNumFreeSeats = 0
			INT i = 0
			
			REPEAT 3 i
				IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
					iNumFreeSeatsRequired++
				ENDIF
			ENDREPEAT
		
			PED_INDEX ped1 = GET_PED_IN_VEHICLE_SEAT(veh, VS_FRONT_RIGHT)
			
			IF NOT IS_PED_INJURED(ped1)
				IF ped1 = locatesData.LocatesBuddyID[0]
				OR ped1 = locatesData.LocatesBuddyID[1]
				OR ped1 = locatesData.LocatesBuddyID[2]
					iNumFreeSeats++
				ENDIF
			ELSE
				iNumFreeSeats++
			ENDIF
			
			PED_INDEX ped2 = GET_PED_IN_VEHICLE_SEAT(veh, VS_BACK_LEFT)
			
			IF NOT IS_PED_INJURED(ped2)
				IF ped2 = locatesData.LocatesBuddyID[0]
				OR ped2 = locatesData.LocatesBuddyID[1]
				OR ped2 = locatesData.LocatesBuddyID[2]
					iNumFreeSeats++
				ENDIF
			ELSE
				iNumFreeSeats++
			ENDIF
			
			PED_INDEX ped3 = GET_PED_IN_VEHICLE_SEAT(veh, VS_BACK_RIGHT)
			
			IF NOT IS_PED_INJURED(ped3)
				IF ped3 = locatesData.LocatesBuddyID[0]
				OR ped3 = locatesData.LocatesBuddyID[1]
				OR ped3 = locatesData.LocatesBuddyID[2]
					iNumFreeSeats++
				ENDIF
			ELSE
				iNumFreeSeats++
			ENDIF

			IF iNumFreeSeats < iNumFreeSeatsRequired
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_USING_A_VEHICLE_WITH_ENOUGH_SEATS(LOCATES_HEADER_DATA &locatesData)
    VEHICLE_INDEX temp_VEHICLE
    temp_VEHICLE = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
    IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(temp_VEHICLE, locatesData)
        RETURN(TRUE)
    ENDIF
    RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_A_VEHICLE_WITH_ENOUGH_SEATS(LOCATES_HEADER_DATA &locatesData, INT iMinRequiredSeats = 0)
    VEHICLE_INDEX temp_VEHICLE  
    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())       
        temp_VEHICLE = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(temp_VEHICLE, locatesData, iMinRequiredSeats)
            RETURN(TRUE)
        ENDIF
    ENDIF   
    RETURN(FALSE)   
ENDFUNC

FUNC BOOL IS_PLAYER_SITTING_IN_A_VEHICLE_WITH_ENOUGH_SEATS(LOCATES_HEADER_DATA &locatesData)
    VEHICLE_INDEX temp_VEHICLE  
    IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())       
        temp_VEHICLE = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(temp_VEHICLE, locatesData)
            RETURN(TRUE)
        ENDIF
    ENDIF   
    RETURN(FALSE)   
ENDFUNC

FUNC BOOL IS_BUDDY_GOOD_TO_JOIN_PLAYERS_GROUP(LOCATES_HEADER_DATA &locatesData, PED_INDEX inChar, FLOAT fJoinDistance, BOOL bIgnoreIfPlayerInvehicle  = FALSE)
    VEHICLE_INDEX Tempvehicle
    IF NOT IS_PED_INJURED(inChar)
        IF IS_PED_SITTING_IN_ANY_VEHICLE(inChar)
            Tempvehicle = GET_VEHICLE_PED_IS_IN(inChar)
            IF NOT IS_ENTITY_DEAD(Tempvehicle)
                IF IS_VEHICLE_DRIVEABLE(Tempvehicle)
                    IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), Tempvehicle)
                        IF IS_PLAYER_SITTING_IN_A_VEHICLE_WITH_ENOUGH_SEATS(locatesData)
                            RETURN(TRUE)
                        ENDIF
                    ELIF bIgnoreIfPlayerInvehicle
						RETURN(TRUE)
					ENDIF
                ELSE
                    //--Added to fix 18898. Buddies will rejoin player's group if they are sat in a non-driveable vehicle.
                    IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), inChar, <<fJoinDistance, fJoinDistance, 3.0>>, FALSE)
                        RETURN(TRUE)
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), inChar, <<fJoinDistance, fJoinDistance, 3.0>>, FALSE)      
                IF NOT (bIgnoreIfPlayerInvehicle)
					Tempvehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				
					IF DOES_ENTITY_EXIST(Tempvehicle)
						IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(Tempvehicle, locatesData)
							IF IS_VEHICLE_DRIVEABLE(Tempvehicle)
								IF IS_VEHICLE_ALMOST_STOPPED(Tempvehicle)
	                       			RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
                ELSE
                    RETURN(TRUE)
                ENDIF
            ENDIF
        ENDIF
    ENDIF  
    RETURN(FALSE)
ENDFUNC   

PROC UPDATE_AMBIENT_ANIM_BLOCKING(LOCATES_HEADER_DATA &locatesData)
    INT i   
    REPEAT 3 i
        IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
            IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
                IF IS_PED_SITTING_IN_ANY_VEHICLE(locatesData.LocatesBuddyID[i])
                //AND HAS_PED_BEEN_ADDED_FOR_SCRIPTED_SPEECH(locatesData.LocatesBuddyID[i])
                //AND IS_ANY_SPEECH_CURRENTLY_PLAYING_OR_ABOUT_TO_PLAY()
                    SET_PED_CAN_PLAY_AMBIENT_ANIMS(locatesData.LocatesBuddyID[i], false)
					SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(locatesData.LocatesBuddyID[i], false)
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
            IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
            //AND HAS_PED_BEEN_ADDED_FOR_SCRIPTED_SPEECH(PLAYER_PED_ID())
            //AND IS_ANY_SPEECH_CURRENTLY_PLAYING_OR_ABOUT_TO_PLAY()
                SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), false)
				SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(player_ped_id(), false)
            ENDIF
        ENDIF           
    ENDIF   
ENDPROC

FUNC BOOL IS_THIS_BLIP_A_VEHICLE(BLIP_INDEX iBlipID)
    eRADAR_BLIP_TYPE thisBlipType
    IF DOES_BLIP_EXIST(iBlipID)
        thisBlipType = GET_BLIP_INFO_ID_TYPE(iBlipID)   
        IF (thisBlipType = BLIPTYPE_VEHICLE)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC
    
FUNC BOOL IS_THIS_BLIP_A_LOCATION(BLIP_INDEX iBlipID)
    eRADAR_BLIP_TYPE thisBlipType
    IF DOES_BLIP_EXIST(iBlipID)
        thisBlipType = GET_BLIP_INFO_ID_TYPE(iBlipID)   
        IF (thisBlipType = BLIPTYPE_COORDS)
        OR (thisBlipType = BLIPTYPE_CONTACT)
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

    
//PROC SET_TAXI_DROPOFF_FOR_LOCATES_HEADER(LOCATES_HEADER_DATA &locatesData, VECTOR vPosition, FLOAT fHeading)    
//  IF DOES_BLIP_EXIST(locatesData.LocationBlip)
//      IF IS_THIS_BLIP_A_LOCATION(locatesData.LocationBlip)
//          SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(locatesData.LocationBlip, vPosition, fHeading)
//      ENDIF
//  ENDIF   
//ENDPROC
#IF IS_DEBUG_BUILD
	PROC SET_THESE_J_SKIP_COORDS(VECTOR vecCoords, FLOAT fHead, LOCATES_HEADER_DATA &locatesData)    
	    locatesData.vSkipCoords = vecCoords
	    locatesData.fSkipHeading = fHead    
	    SET_BIT(locatesData.iLocatesBitSet, BS_USE_SPECIFIC_J_SKIP_COORDS)
	ENDPROC

	PROC DONT_DO_J_SKIP(LOCATES_HEADER_DATA &locatesData)
		SET_BIT(locatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	ENDPROC

	PROC J_SKIP_LH(VECTOR vecCoords, LOCATES_HEADER_DATA &locatesData, BOOL bOnFoot = FALSE, VEHICLE_INDEX SpecificvehicleID = NULL)
	    INT i
	    VEHICLE_INDEX temp_VEHICLE
	    VECTOR pos
	    FLOAT fHeading
	    
		#IF IS_DEBUG_BUILD
	    	CREATE_LOCATES_HEADER_WIDGET(locatesData)
		#ENDIF
	    
		IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
		
			IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_USE_SPECIFIC_J_SKIP_COORDS)
				vecCoords.x = locatesData.vSkipCoords.x
				vecCoords.y = locatesData.vSkipCoords.y
				vecCoords.z = locatesData.vSkipCoords.z
				fHeading = locatesData.fSkipHeading
				CLEAR_BIT(locatesData.iLocatesBitSet, BS_USE_SPECIFIC_J_SKIP_COORDS)
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					PRINTLN("J_SKIP - called")
					DO_SCREEN_FADE_OUT(500)
					WHILE NOT IS_SCREEN_FADED_OUT()
						WAIT(0)
					ENDWHILE
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					STORE_EXISTING_VEHICLE_AND_EMPTY(temp_VEHICLE)     
					
					WAIT(500)
					
					LOAD_SCENE(vecCoords)
					
					IF (bOnFoot)
						PRINTLN("J_SKIP - onFoot = TRUE")
						SAFE_WARP_PLAYER_TO_COORDINATE(vecCoords)
						REPEAT 3 i
							IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
								CLEAR_PED_TASKS_IMMEDIATELY(locatesData.LocatesBuddyID[i])
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, (1.0 + TO_FLOAT(i)), 0.0>>)
									SET_PED_COORDS_KEEP_VEHICLE(locatesData.LocatesBuddyID[i], <<pos.x, pos.y, pos.z>>)  
									SET_ENTITY_HEADING(locatesData.LocatesBuddyID[i], fHeading)
								ENDIF
							ENDIF               
						ENDREPEAT           
					ELSE
						IF DOES_ENTITY_EXIST(SpecificvehicleID)
							PRINTLN("J_SKIP - SpecificvehicleID exists")
							IF IS_VEHICLE_DRIVEABLE(SpecificvehicleID)
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)
								REPEAT 3 i
									IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
										IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
											CLEAR_PED_TASKS_IMMEDIATELY(locatesData.LocatesBuddyID[i])
											SET_PED_INTO_VEHICLE(locatesData.LocatesBuddyID[i], SpecificvehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
											SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(locatesData.LocatesBuddyID[i], FALSE)
										ENDIF
									ENDIF
								ENDREPEAT
							ENDIF
							SAFE_WARP_PLAYER_TO_COORDINATE(vecCoords, fHeading) 
							IF IS_VEHICLE_DRIVEABLE(SpecificvehicleID)
								SET_ENTITY_COORDS(SpecificvehicleID, vecCoords)
							ENDIF
						ELSE
							PRINTLN("J_SKIP - searching for temp_VEHICLE")
							IF DOES_ENTITY_EXIST(temp_VEHICLE)
								IF NOT IS_VEHICLE_DRIVEABLE(temp_VEHICLE)
									PRINTLN("J_SKIP - temp_VEHICLE existed but not driveable")
									temp_VEHICLE = NULL
								ENDIF
							ENDIF
							IF NOT DOES_ENTITY_EXIST(temp_VEHICLE)
								PRINTLN("J_SKIP - creating new temp_VEHICLE")
								REQUEST_MODEL(EMPEROR)
								WHILE NOT HAS_MODEL_LOADED(EMPEROR)
									WAIT(0)
								ENDWHILE
								temp_VEHICLE = CREATE_VEHICLE(EMPEROR, vecCoords, 0.0, FALSE)
								SET_MODEL_AS_NO_LONGER_NEEDED(EMPEROR)
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), temp_VEHICLE)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(temp_VEHICLE)
							ELSE
								PRINTLN("J_SKIP - temp_VEHICLE exists")
								IF IS_VEHICLE_DRIVEABLE(temp_VEHICLE)
									PRINTLN("J_SKIP - temp_VEHICLE is driveable")
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), temp_VEHICLE)
								ENDIF       
							ENDIF
							REPEAT 3 i
								IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
									IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
										PRINTLN("J_SKIP - buddy exists, clearing tasks.")
										CLEAR_PED_TASKS_IMMEDIATELY(locatesData.LocatesBuddyID[i])
										IF IS_VEHICLE_DRIVEABLE(temp_VEHICLE)
											SET_PED_INTO_VEHICLE(locatesData.LocatesBuddyID[i], temp_VEHICLE, INT_TO_ENUM(VEHICLE_SEAT, i))
										ELSE
											PRINTLN("J_SKIP - buddy exists, clearing tasks.")
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							REPEAT 3 i
								IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
									IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
										IF IS_VEHICLE_DRIVEABLE(temp_VEHICLE)
											PRINTLN("J_SKIP - warping buddy into vehicle")
											SET_PED_INTO_VEHICLE(locatesData.LocatesBuddyID[i], temp_VEHICLE, INT_TO_ENUM(VEHICLE_SEAT, i))
											//VEHICLE_SEAT
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							IF IS_VEHICLE_DRIVEABLE(temp_VEHICLE)
								SET_ENTITY_COORDS(temp_VEHICLE, vecCoords)
								SET_ENTITY_HEADING(temp_VEHICLE, fHeading)
							ENDIF
						ENDIF
					ENDIF
					
					// wait for ambient clubs to stream in
					//WAIT_FOR_ANY_NEARBY_CLUBS_TO_STREAM_IN()
					
					DO_SCREEN_FADE_IN(500)
				ENDIF
			ENDIF
		ELSE
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
		ENDIF
		

	ENDPROC
	        
	PROC J_SKIP_NO_BUDDIES(VECTOR vecCoords, LOCATES_HEADER_DATA &locatesData, BOOL bOnFoot = FALSE, VEHICLE_INDEX SpecificvehicleID = NULL)
	    //PED_INDEX null_array[1]
	    J_SKIP_LH(vecCoords, locatesData, bOnFoot,SpecificvehicleID)
	ENDPROC     
#ENDIF

PROC DONT_DO_SAFE_FOR_CUTSCENE_CHECK(LOCATES_HEADER_DATA &locatesData)
    SET_BIT(locatesData.iLocatesBitSet, BS_DONT_DO_SAFE_FOR_CUTSCENE_CHECK)
ENDPROC

PROC DO_LOCATES_ALMOST_STOPPED_CHECK(LOCATES_HEADER_DATA &locatesData)
    SET_BIT(locatesData.iLocatesBitSet, BS_DO_LOCATES_ALMOST_STOPPED_CHECK) 
ENDPROC


        
        
FUNC BOOL LOCATES_CAN_PLAYER_START_CUTSCENE(LOCATES_HEADER_DATA &locatesData)
//    BOOL bDoUprightCheck
//    bDoUprightCheck = TRUE  
//    IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
//        IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
//        OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
//            bDoUprightCheck = FALSE
//        ENDIF
//    ENDIF

    IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_DO_LOCATES_ALMOST_STOPPED_CHECK)
        IF IS_CHAR_ALMOST_STOPPED(PLAYER_PED_ID())
            //IF (CAN_PLAYER_START_CUTSCENE(TRUE, bDoUprightCheck) OR IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_DO_SAFE_FOR_CUTSCENE_CHECK))
			IF (CAN_PLAYER_START_CUTSCENE() OR IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_DO_SAFE_FOR_CUTSCENE_CHECK))
                RETURN(TRUE)
            ENDIF
        ENDIF
    ELSE
        //IF (CAN_PLAYER_START_CUTSCENE(TRUE, bDoUprightCheck) OR IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_DO_SAFE_FOR_CUTSCENE_CHECK))
		IF (CAN_PLAYER_START_CUTSCENE() OR IS_BIT_SET(locatesData.iLocatesBitSet, BS_DONT_DO_SAFE_FOR_CUTSCENE_CHECK))
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC
            
FUNC BOOL IS_CHAR_NOT_IN_GROUP_BUT_USING_SPECIFIC_VEHICLE(PED_INDEX Ped, VEHICLE_INDEX vehicleID)
    VEHICLE_INDEX ReturnvehicleID
    IF NOT IS_PED_INJURED(Ped)
        IF NOT IS_PED_GROUP_MEMBER(Ped, PLAYER_GROUP_ID())
            //ReturnvehicleID = GET_VEHICLE_PED_IS_USING(Ped)
			ReturnvehicleID = GET_VEHICLE_PED_IS_ENTERING(Ped)

            IF IS_VEHICLE_DRIVEABLE(vehicleID)
                IF IS_ENTITY_AT_ENTITY(Ped, vehicleID, <<BUDDY_WILL_GET_IN_SPECIFIC_VEHICLE_DIST + 10.0, BUDDY_WILL_GET_IN_SPECIFIC_VEHICLE_DIST + 10.0, 10.0>>, FALSE)
                    IF (ReturnvehicleID = vehicleID)
                        RETURN(TRUE)
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC     
               
						  
//FUNC BOOL IS_SPECIFIC_CAR_NEARER_THAN_ANY_BUDDIES_AND_PLAYER_IS_NOT_IN_IT(LOCATES_HEADER_DATA &locatesData)
//	IF DOES_BLIP_EXIST(locatesData.vehicleBlip)
//		RETURN(TRUE)
//	ENDIF
//
//	// if specific car exists and player is not in it, but nearer to it than any buddies then return true
//	IF DOES_ENTITY_EXIST(SpecificvehicleID)
//		IF IS_VEHICLE_DRIVEABLE(SpecificVehicleID)
//			IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)
//				FLOAT fDistToVehicle
//				FLOAT fNearestBuddy
//				FLOAT fDist
//				VECTOR CarPos
//				VECTOR PlayerPos
//				VECTOR BuddyPos
//				VECTOR vec
//				PlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				CarPos = GET_ENTITY_COORDS(SpecificVehicleID)
//				fDistToVehicle = VMAG((PlayerPos - CarPos))
//				fNearestBuddy = 9999.9
//				REPEAT 3 i
//					IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
//						IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
//							IF NOT DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
//								BuddyPos = GET_ENTITY_COORDS(locatesData.LocatesBuddyID[i])
//								fDist = VMAG((PlayerPos - BuddyPos))
//								IF (fDist < fNearestBuddy)
//									fNearestBuddy = fDist
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				IF (fDistToVehicle < fNearestBuddy)
//					RETURN(TRUE)									
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//ENDFUNC
						  
FUNC BOOL HAVE_BUDDIES_JOINED_PLAYERS_GROUP(LOCATES_HEADER_DATA &locatesData, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sPickupBuddyText1, 
											STRING sPickupBuddyText2, STRING sPickupBuddyText3,  STRING sPickupAllBuddysText, FLOAT fJoinDistance, BOOL bShowBuddyRoute = FALSE, 
											VEHICLE_INDEX SpecificvehicleID = NULL,  BOOL bIgnoreIfPlayerInvehicle  = FALSE, BOOL bJoinGroupEvenIfNotInSameMovement = FALSE,
											STRING sHeistCrewName1=NULL, STRING sHeistCrewName2=NULL, STRING sHeistCrewName3=NULL, TRANSPORT_MODE transMode = TM_ANY_3D,
											BOOL bLoseWantedLevel = FALSE)

    INT i
    INT iBuddiesMissing
	INT iBuddiesNearlyMissing
    INT iTotalBuddies
	INT iNumBuddiesSetToDefaultSeat = 0
    INT iThisTime
    BOOL bAllBuddiesHaveJoinedPlayersGroup
    BOOL bBuddyIsMissing[3]
    BOOL bGiveMissingBuddyBlip
    BOOL bPlayerInvehicleWithTooFewSeats
    VEHICLE_INDEX tempvehicle
    SCRIPTTASKSTATUS TaskStatus
    
    locatesData.LocatesBuddyID[0] = Buddy1
    locatesData.LocatesBuddyID[1] = Buddy2
    locatesData.LocatesBuddyID[2] = Buddy3
    locatesData.SpeakingBuddyID = Buddy1

    STRING sPickupText[3]
    sPickupText[0] = sPickupBuddyText1
    sPickupText[1] = sPickupBuddyText2
    sPickupText[2] = sPickupBuddyText3
	
	STRING sCrewNames[3]
    sCrewNames[0] = sHeistCrewName1
    sCrewNames[1] = sHeistCrewName2
    sCrewNames[2] = sHeistCrewName3

    // check if any of the buddies arn't in the players group
    bAllBuddiesHaveJoinedPlayersGroup = TRUE
    iBuddiesMissing = 0
	iBuddiesNearlyMissing = 0
    iTotalBuddies = 0
    bBuddyIsMissing[0] = FALSE
    bBuddyIsMissing[1] = FALSE
    bBuddyIsMissing[2] = FALSE
	
    // count the buddies, and do an action mode check here.
    REPEAT 3 i
        IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
            iTotalBuddies++
        ENDIF
		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BLOCK_BUDDIES_SETTING_TO_ACTION_MODE)
			AND NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_SET_TO_ACTION_MODE)
				IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
					SET_PED_USING_ACTION_MODE(locatesData.LocatesBuddyID[i], TRUE)
				ENDIF
				
				IF i = 2
					SET_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_SET_TO_ACTION_MODE)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BLOCK_BUDDIES_SETTING_TO_ACTION_MODE)
			AND IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_SET_TO_ACTION_MODE)
				IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
					SET_PED_USING_ACTION_MODE(locatesData.LocatesBuddyID[i], FALSE)
				ENDIF
				
				IF i = 2
					CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_SET_TO_ACTION_MODE)
				ENDIF
			ENDIF
		ENDIF
    ENDREPEAT
	
	IF (iTotalBuddies = 0)
		RETURN(TRUE)
	ENDIF
	
	//Special case: buddies will get into the closest car in all cases.
	IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START)
		BOOL bCleanupForcedEntryIntoCar = FALSE
	
		//Fetch the closest car if it wasn't specified
		IF NOT DOES_ENTITY_EXIST(locatesData.vehStartCar)
			#IF IS_DEBUG_BUILD
				PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - FETCHING CLOSEST CAR")
			#ENDIF
		
			VEHICLE_INDEX vehClosest			
			INT iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING
			iSearchFlags |= VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK
			iSearchFlags |= VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
			iSearchFlags |= VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES
			iSearchFlags |= VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES
			iSearchFlags |= VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES
			iSearchFlags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED
			iSearchFlags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER
			iSearchFlags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS
			
			vehClosest = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, DUMMY_MODEL_FOR_SCRIPT, iSearchFlags) 
			
			IF IS_VEHICLE_DRIVEABLE(vehClosest)
				#IF IS_DEBUG_BUILD
					PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - CAR FOUND ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehClosest))
				#ENDIF
			
				locatesData.vehStartCar = vehClosest
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(locatesData.vehStartCar)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(locatesData.vehStartCar)) < 400.0
				IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF (NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) OR NOT bLoseWantedLevel)
						IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(locatesData.vehStartCar, locatesData, iTotalBuddies)
							REPEAT 3 i
								IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
									SET_PED_MAX_MOVE_BLEND_RATIO(locatesData.LocatesBuddyID[i], PEDMOVE_WALK)
								
									IF IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
							            REMOVE_PED_FROM_GROUP(locatesData.LocatesBuddyID[i])
										
										#IF IS_DEBUG_BUILD
							            	PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - REMOVE_PED_FROM_GROUP: ", i)
										#ENDIF
							        ENDIF
									
							        IF GET_SCRIPT_TASK_STATUS(locatesData.LocatesBuddyID[i], SCRIPT_TASK_ENTER_VEHICLE) = FINISHED_TASK
									AND NOT IS_CHAR_USING_VEHICLE(locatesData.LocatesBuddyID[i], locatesData.vehStartCar)
										IF NOT IS_PED_RAGDOLL(locatesData.LocatesBuddyID[i])
										AND NOT IS_PED_GETTING_UP(locatesData.LocatesBuddyID[i])
								            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(locatesData.LocatesBuddyID[i], TRUE)
								            TASK_ENTER_VEHICLE(locatesData.LocatesBuddyID[i], locatesData.vehStartCar, 60000, INT_TO_ENUM(VEHICLE_SEAT, i), PEDMOVE_WALK)
											SET_PED_GROUP_MEMBER_PASSENGER_INDEX(locatesData.LocatesBuddyID[i], INT_TO_ENUM(VEHICLE_SEAT, i))

											#IF IS_DEBUG_BUILD
								            	PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - told to enter vehicle as passenger ", GET_THIS_SCRIPT_NAME())
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							
							//Just return TRUE, this ensures the locates header still displays the destination blip even though we're overriding the buddies to do something else.
							RETURN TRUE
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - chosen vehicle doesn't have enough seats. ", GET_THIS_SCRIPT_NAME())
							#ENDIF
						
							bCleanupForcedEntryIntoCar = TRUE
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - player has a wanted level. ", GET_THIS_SCRIPT_NAME())
						#ENDIF
					
						bCleanupForcedEntryIntoCar = TRUE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - player is already in a car. ", GET_THIS_SCRIPT_NAME())
					#ENDIF
				
					bCleanupForcedEntryIntoCar = TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - chosen vehicle is too far away. ", 
							VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(locatesData.vehStartCar)), " ", GET_THIS_SCRIPT_NAME())
				#ENDIF
			
				bCleanupForcedEntryIntoCar = TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - chosen vehicle is not driveable. ", GET_THIS_SCRIPT_NAME())
			#ENDIF
		
			bCleanupForcedEntryIntoCar = TRUE
		ENDIF
		
		IF bCleanupForcedEntryIntoCar
			CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START)
			
			REPEAT 3 i
				IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
					IF NOT IS_PED_GETTING_INTO_A_VEHICLE(locatesData.LocatesBuddyID[i]) //Clearing tasks may stop them from closing car doors so check this first.
					AND NOT locatesData.bDontClearTasksIfEnteringNearestVehicle
						CLEAR_PED_TASKS(locatesData.LocatesBuddyID[i])
					ENDIF
					
					IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
						IF IS_BUDDY_GOOD_TO_JOIN_PLAYERS_GROUP(locatesData, locatesData.LocatesBuddyID[i], fJoinDistance, TRUE)
							SET_PED_AS_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				PRINTLN("BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START - header resetting to standard behaviour.")
			#ENDIF
		ENDIF
	ENDIF

	//All non-special case buddy handling.
	IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START)
	
	    IF NOT IS_PLAYER_SITTING_IN_A_VEHICLE_WITH_ENOUGH_SEATS(locatesData)
	    AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	    AND NOT DOES_ENTITY_EXIST(SpecificvehicleID)
	        tempvehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	        IF IS_VEHICLE_DRIVEABLE(tempvehicle)
	            IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
					//Potential alternative: display god text if you need a vehicle, don't display it for any means but have ambient speech.
					IF ((transMode = TM_IN_VEHICLE_2D) OR (transMode = TM_IN_VEHICLE_3D)) 
					
					ELSE
					
					ENDIF
					
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						INT iMaxNumPeds = 0
						INT iNumPedToSpeak = 0
					
						REPEAT 3 i
							IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
								iMaxNumPeds++
							ENDIF
						ENDREPEAT
					
						iNumPedToSpeak = GET_RANDOM_INT_IN_RANGE(0, iMaxNumPeds)
					
						IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[iNumPedToSpeak])
							PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[iNumPedToSpeak], "NEED_A_BIGGER_VEHICLE", SPEECH_PARAMS_FORCE)
						ENDIF
					
	                	GOD_TEXT(locatesData, "MORE_SEATS")
	                	SET_BIT(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
					ENDIF
	            ENDIF
				
	            bPlayerInvehicleWithTooFewSeats = TRUE
	        ENDIF
	    ELSE
	        bPlayerInvehicleWithTooFewSeats = FALSE
	        CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
	        SAFE_CLEAR_THIS_PRINT("MORE_SEATS")
	    ENDIF
		
		//Print text if the player tries to use a bus that's occupied (passenger peds will never leave the bus).
		IF NOT DOES_ENTITY_EXIST(SpecificvehicleID)
			IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[0])
			OR NOT IS_PED_INJURED(locatesData.LocatesBuddyID[1])
			OR NOT IS_PED_INJURED(locatesData.LocatesBuddyID[2])
				IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_IN_UNSUITABLE_VEHICLE)
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						tempvehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						
						IF private_IS_VEHICLE_AN_OCCUPIED_BUS(tempvehicle, locatesData)
							GOD_TEXT(locatesData, "CMN_VEHSUIT")
	                		SET_BIT(locatesData.iLocatesBitSet, BS_PLAYER_IN_UNSUITABLE_VEHICLE)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYER_IN_UNSUITABLE_VEHICLE)
	        			SAFE_CLEAR_THIS_PRINT("CMN_VEHSUIT")
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(SpecificvehicleID)
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
					SET_BIT(locatesData.iLocatesBitSet, BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR)
					
					#IF IS_DEBUG_BUILD
		            	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - Player pressed exit but IS_PED_SITTING_IN_VEHICLE still TRUE")
					#ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR)
					CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR)
				ENDIF
			ENDIF
		ENDIF

	    REPEAT 3 i
	        IF DOES_ENTITY_EXIST(locatesData.LocatesBuddyID[i])
	            IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])

	                // make buddy drown in water if not in group
	                IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
	                    SET_PED_DIES_IN_WATER(locatesData.LocatesBuddyID[i], TRUE)  
	                ELSE
	                    SET_PED_DIES_IN_WATER(locatesData.LocatesBuddyID[i], FALSE)
	                ENDIF
	                
	                IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	                    tempvehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	                    IF IS_VEHICLE_DRIVEABLE(tempvehicle)
							// if player is a vehicle with too few seats then remove from group
	                        IF IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
	                            IF NOT IS_PLAYER_SITTING_IN_A_VEHICLE_WITH_ENOUGH_SEATS(locatesData)
	                            AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	                                IF NOT IS_CHAR_USING_SAME_VEHICLE_AS_PLAYER(locatesData.LocatesBuddyID[i])
	                                //IF NOT ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), locatesData.LocatesBuddyID[i])
	                                    REMOVE_PED_FROM_GROUP(locatesData.LocatesBuddyID[i])
										
										#IF IS_DEBUG_BUILD
	                                   		PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - REMOVE_CHAR_FROM_GROUP - not enough seats in vehicle.")
										#ENDIF
	                                ENDIF
	                            ENDIF
	                        ENDIF
	                    ENDIF
						
						//If vehicle is in water then force the buddies out.
						IF tempvehicle != SpecificvehicleID
						AND NOT IS_ENTITY_DEAD(tempvehicle)
							IF IS_PED_SITTING_IN_VEHICLE(locatesData.LocatesBuddyID[i], tempvehicle)
								IF IS_ENTITY_IN_WATER(tempvehicle)
								AND NOT IS_VEHICLE_ON_ALL_WHEELS(tempvehicle)
									VECTOR vCarPos = GET_ENTITY_COORDS(tempvehicle)
								
									IF vCarPos.z < -1.0 
	                            		TASK_LEAVE_VEHICLE(locatesData.LocatesBuddyID[i], tempvehicle, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
									
										#IF IS_DEBUG_BUILD
											PRINTLN("locates_private.sch - Player's vehicle is in water, buddy told to exit.")
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
	                ENDIF
	                                
	                
	                bGiveMissingBuddyBlip = TRUE
	                IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
	                    IF IS_PED_SITTING_IN_ANY_VEHICLE(locatesData.LocatesBuddyID[i])
	                        tempvehicle = GET_VEHICLE_PED_IS_IN(locatesData.LocatesBuddyID[i])
							
							IF NOT IS_ENTITY_DEAD(tempvehicle)
								//If buddy is in a vehicle which is not specific, and the player isn't then leave it.
		                        IF IS_VEHICLE_DRIVEABLE(SpecificvehicleID)  
		                            IF tempvehicle != SpecificvehicleID
		                                IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), tempvehicle)
											IF GET_ENTITY_SPEED(tempvehicle) > 5.0
												TASK_LEAVE_VEHICLE(locatesData.LocatesBuddyID[i], tempvehicle, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT)
											ELSE
		                                   		TASK_LEAVE_VEHICLE(locatesData.LocatesBuddyID[i], tempvehicle, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
											ENDIF
											
											#IF IS_DEBUG_BUILD
		                                    	PRINTLN("locates_private - HAVE_BUDDIES_JOINED_PLAYERS_GROUP - Buddy told to leave vehicle: buddy is in a different vehicle to the specific ID.")
											#ENDIF
											
		                                    bGiveMissingBuddyBlip = FALSE
		                                ENDIF
		                            ENDIF
		                        ELSE
									VEHICLE_INDEX vehPlayer
									
									IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									ENDIF
								
									//If the buddy and the player are in different vehicles then tell the buddy to leave theirs.
									IF IS_VEHICLE_DRIVEABLE(vehPlayer)
										IF tempvehicle != vehPlayer
											IF GET_ENTITY_SPEED(tempvehicle) > 5.0
												TASK_LEAVE_VEHICLE(locatesData.LocatesBuddyID[i], tempvehicle, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT)
											ELSE
			                                	TASK_LEAVE_VEHICLE(locatesData.LocatesBuddyID[i], tempvehicle, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
											ENDIF
											
											#IF IS_DEBUG_BUILD
			                                	PRINTLN("locates_private - HAVE_BUDDIES_JOINED_PLAYERS_GROUP - Buddy told to leave vehicle: buddy and player are in different vehicles.")
											#ENDIF
											
			                                bGiveMissingBuddyBlip = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
	                    ENDIF
	                ENDIF

	                // if the player is using a vehicle with too few seats then set the seat priority too high
	                IF IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
						VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())

						IF DOES_ENTITY_EXIST(vehTemp)
		                    IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(vehTemp, locatesData)
		                        IF IS_BIT_SET_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(i, locatesData)
								OR NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_GIVEN_DEFAULT_PASSENGER_INDEX)
		                            SET_PED_GROUP_MEMBER_PASSENGER_INDEX(locatesData.LocatesBuddyID[i], INT_TO_ENUM(VEHICLE_SEAT, i))
		                            CLEAR_BIT_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(i, locatesData)
									iNumBuddiesSetToDefaultSeat++
									
									IF iNumBuddiesSetToDefaultSeat >= iTotalBuddies
										SET_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_GIVEN_DEFAULT_PASSENGER_INDEX)
									ENDIF
									
									#IF IS_DEBUG_BUILD
			                        	PRINTLN("locates_private - HAVE_BUDDIES_JOINED_PLAYERS_GROUP - Buddy ", i, "set to passenger seat ", i)
									#ENDIF
		                        ENDIF
		                    ELSE
		                        IF NOT IS_BIT_SET_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(i, locatesData)
									//The Sentinel2 has inaccessible seat nodes, so setting the seat to 2 will cause peds to warp in the back.
									IF GET_ENTITY_MODEL(vehTemp) = SENTINEL2
										SET_PED_GROUP_MEMBER_PASSENGER_INDEX(locatesData.LocatesBuddyID[i], INT_TO_ENUM(VEHICLE_SEAT, 4))
									ELSE
		                           		SET_PED_GROUP_MEMBER_PASSENGER_INDEX(locatesData.LocatesBuddyID[i], INT_TO_ENUM(VEHICLE_SEAT, 2))
									ENDIF
									
									#IF IS_DEBUG_BUILD
			                        	PRINTLN("locates_private - HAVE_BUDDIES_JOINED_PLAYERS_GROUP - Not enough seats, buddy ", i, "set to passenger seat ", 2)
									#ENDIF
									
		                            SET_BIT_BUDDY_GIVEN_SPECIFIC_PASSENGER_INDEX(i, locatesData)
		                        ENDIF
		                    ENDIF
						ENDIF
	                ENDIF

	                IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
	                AND NOT DOES_SPECIFIC_VEHICLE_EXIST_AND_IS_CHAR_IN_IT(locatesData.LocatesBuddyID[i], SpecificvehicleID)
	                AND NOT IS_CHAR_NOT_IN_GROUP_BUT_USING_SPECIFIC_VEHICLE(locatesData.LocatesBuddyID[i], SpecificvehicleID)               
	                			
	                    // if they are close enough make them join the player's group
	                    IF IS_BUDDY_GOOD_TO_JOIN_PLAYERS_GROUP(locatesData, locatesData.LocatesBuddyID[i], fJoinDistance, bIgnoreIfPlayerInvehicle)     
	                        IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
								IF NOT IS_PED_RAGDOLL(locatesData.LocatesBuddyID[i]) //Fix for 891812: prevents peds from repeatedly being added to the group while ragdolling.
								AND NOT IS_PED_GETTING_UP(locatesData.LocatesBuddyID[i])
								AND NOT IS_PED_JUMPING_OUT_OF_VEHICLE(locatesData.LocatesBuddyID[i])
								AND NOT IS_PED_GETTING_INTO_A_VEHICLE(locatesData.LocatesBuddyID[i])
		                            TaskStatus = GET_SCRIPT_TASK_STATUS(locatesData.LocatesBuddyID[i], SCRIPT_TASK_ENTER_VEHICLE)
	
		                            IF (TaskStatus = FINISHED_TASK)
		                                CLEAR_PED_TASKS(locatesData.LocatesBuddyID[i])
										
										#IF IS_DEBUG_BUILD
		                                	PRINTLN("locates_private - HAVE_BUDDIES_JOINED_PLAYERS_GROUP - CLEAR_CHAR_TASKS - 1")
										#ENDIF
		                            ENDIF
		                            SET_PED_AS_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())   
									
									#IF IS_DEBUG_BUILD
		                           		PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - SET_GROUP_MEMBER - 1")
									#ENDIF
		                            bGiveMissingBuddyBlip = FALSE
								ENDIF
	                        ENDIF
	                    ENDIF   
	                
	                    IF (bGiveMissingBuddyBlip)
	                        // give him a blip if he doesn't already have one
	                        IF NOT DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
	                            locatesData.iLeftBuddyTime = GET_GAME_TIMER()
	                            locatesData.BuddyBlipID[i] = CREATE_BLIP_FOR_ENTITY(locatesData.LocatesBuddyID[i], FALSE)
	                            SET_BLIP_DISPLAY(locatesData.BuddyBlipID[i], DISPLAY_BLIP)
								
								#IF IS_DEBUG_BUILD
	                            	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - add blip for char - 1")
								#ENDIF
								
	                            IF (bShowBuddyRoute)
	                                TURN_ON_GPS_FOR_BLIP(locatesData.BuddyBlipID[i], locatesData)
	                            ENDIF
	                        ENDIF                                               
	                    ENDIF
						
	                    bAllBuddiesHaveJoinedPlayersGroup = FALSE

	                ELSE
	                

	                    // remove blip if he had one - but only if he is in the same vehicle as player (if player is in a vehicle)
	                    IF DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
							
	                        IF IS_BUDDY_IN_SAME_GROUP_MOVEMENT(locatesData.LocatesBuddyID[i])
	                        OR DOES_SPECIFIC_VEHICLE_EXIST_AND_IS_CHAR_IN_IT(locatesData.LocatesBuddyID[i], SpecificvehicleID)
	                        OR (bJoinGroupEvenIfNotInSameMovement)
							OR (IS_VEHICLE_DRIVEABLE(SpecificvehicleID) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID))
	                            IF DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
	                                REMOVE_BLIP(locatesData.BuddyBlipID[i])
									
									#IF IS_DEBUG_BUILD
	                                	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - remove blip for char - 1")
									#ENDIF
									
	                                SAFE_CLEAR_THIS_PRINT(sPickupText[i]) 
	                            ENDIF                           
	                        ELSE
	                            IF (bShowBuddyRoute)
	                                TURN_ON_GPS_FOR_BLIP(locatesData.BuddyBlipID[i], locatesData)
	                            ENDIF
								
								#IF IS_DEBUG_BUILD
									PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - bAllBuddiesHaveJoinedPlayersGroup = FALSE 2")
								#ENDIF
	                            bAllBuddiesHaveJoinedPlayersGroup = FALSE
	                        ENDIF   
	                    ELSE
	                        // does a specific vehicle exist?
	                        IF IS_VEHICLE_DRIVEABLE(SpecificvehicleID)      
							
	                            IF NOT IS_PED_SITTING_IN_VEHICLE(locatesData.LocatesBuddyID[i], SpecificvehicleID)							
								
	                                // remove the char from the group if the following is true:
									// - The buddy is near the required vehicle.
									// - The player doesn't currently have a wanted level, or they've already been in the requried vehicle (this makes it the priority objective).
	                                IF IS_ENTITY_AT_ENTITY(locatesData.LocatesBuddyID[i], SpecificvehicleID, <<BUDDY_WILL_GET_IN_SPECIFIC_VEHICLE_DIST, BUDDY_WILL_GET_IN_SPECIFIC_VEHICLE_DIST, 5.0>>, FALSE)
	                                AND NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_WILL_ONLY_GET_IN_WHEN_PLAYER_DOES)
									AND NOT (bLoseWantedLevel AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) AND (NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)))
	                                    IF IS_PED_SITTING_IN_ANY_VEHICLE(locatesData.LocatesBuddyID[i])
	                                        IF NOT IS_PED_IN_VEHICLE(locatesData.LocatesBuddyID[i], SpecificvehicleID)
	                                            IF NOT IS_BUDDY_IN_SAME_GROUP_MOVEMENT(locatesData.LocatesBuddyID[i])
	                                                IF IS_CHAR_ALMOST_STOPPED(locatesData.LocatesBuddyID[i])
	                                                    TaskStatus = GET_SCRIPT_TASK_STATUS(locatesData.LocatesBuddyID[i], SCRIPT_TASK_LEAVE_VEHICLE)
	                                                    IF (TaskStatus = FINISHED_TASK)
	                                                        TASK_LEAVE_ANY_VEHICLE(locatesData.LocatesBuddyID[i])
															
															#IF IS_DEBUG_BUILD
																PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - TASK_LEAVE_ANY_VEHICLE")
															#ENDIF
	                                                    ENDIF
	                                                ENDIF
	                                            ENDIF
	                                        ENDIF
	                                    ELSE
	//                                      IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)
	//                                          SET_FORCE_PLAYER_TO_ENTER_THROUGH_DIRECT_DOOR(PLAYER_PED_ID(), TRUE)
	//                                      ENDIF
	                                        
											IF IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
												IF NOT IS_PED_JUMPING_OUT_OF_VEHICLE(locatesData.LocatesBuddyID[i])
												AND NOT IS_PED_RAGDOLL(locatesData.LocatesBuddyID[i])
												AND NOT IS_PED_GETTING_UP(locatesData.LocatesBuddyID[i])
												AND NOT IS_PED_GETTING_INTO_A_VEHICLE(locatesData.LocatesBuddyID[i])
												AND NOT IS_ENTITY_ON_FIRE(SpecificvehicleID)
				                                    REMOVE_PED_FROM_GROUP(locatesData.LocatesBuddyID[i])
													
													#IF IS_DEBUG_BUILD
				                                    	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - REMOVE_PED_FROM_GROUP - ped is close enough to enter specific vehicle.")
													#ENDIF
												ENDIF
			                                ENDIF
											
	                                        TaskStatus = GET_SCRIPT_TASK_STATUS(locatesData.LocatesBuddyID[i], SCRIPT_TASK_ENTER_VEHICLE)
	                                        IF (TaskStatus = FINISHED_TASK)
											AND NOT IS_CHAR_USING_VEHICLE(locatesData.LocatesBuddyID[i], SpecificvehicleID) //GTAV bug 672596: if the mission has already given peds a sequence the task check won't work.
												//Don't give the ped the enter task if they're in the process of jumping out a vehicle, as it cancels the ragdoll.
												IF NOT IS_PED_JUMPING_OUT_OF_VEHICLE(locatesData.LocatesBuddyID[i])
												AND NOT IS_PED_JUMPING_OUT_OF_VEHICLE(PLAYER_PED_ID())
												AND NOT IS_PED_IN_ANY_SPEEDING_VEHICLE(locatesData.LocatesBuddyID[i], 2.0)
												AND NOT IS_PED_RAGDOLL(locatesData.LocatesBuddyID[i])
												AND NOT IS_PED_GETTING_UP(locatesData.LocatesBuddyID[i])
												AND NOT IS_ENTITY_ON_FIRE(SpecificvehicleID)
		                                            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(locatesData.LocatesBuddyID[i], TRUE)
		                                            IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_WILL_WALK_TO_SPECIFIC_VEHICLE)
														SET_PED_MAX_MOVE_BLEND_RATIO(locatesData.LocatesBuddyID[i], PEDMOVE_WALK)
		                                            ENDIF
		                                            TASK_ENTER_VEHICLE(locatesData.LocatesBuddyID[i], SpecificvehicleID, 60000, INT_TO_ENUM(VEHICLE_SEAT, i))
		                                            SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(locatesData.LocatesBuddyID[i], FALSE)
													
													#IF IS_DEBUG_BUILD
		                                            	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - told to enter vehicle as passenger")
													#ENDIF
												ENDIF
	                                        ELSE
	                                            IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)
	                                                locatesData.BuddyBlipID[i] = CREATE_BLIP_FOR_ENTITY(locatesData.LocatesBuddyID[i], FALSE)
	                                                SET_BLIP_DISPLAY(locatesData.BuddyBlipID[i], DISPLAY_BLIP)
													
													#IF IS_DEBUG_BUILD
	                                               	 	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - add blip for char - 2")
														PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - bAllBuddiesHaveJoinedPlayersGroup = FALSE 3")
													#ENDIF
													
	                                                bAllBuddiesHaveJoinedPlayersGroup = FALSE
	                                            ENDIF
	                                        ENDIF
	                                    ENDIF
	                                ELSE
	                                    IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
	                                        IF IS_BUDDY_GOOD_TO_JOIN_PLAYERS_GROUP(locatesData, locatesData.LocatesBuddyID[i], fJoinDistance, bIgnoreIfPlayerInvehicle)
												IF NOT IS_PED_RAGDOLL(locatesData.LocatesBuddyID[i])
												AND NOT IS_PED_GETTING_UP(locatesData.LocatesBuddyID[i])
		                                            TaskStatus = GET_SCRIPT_TASK_STATUS(locatesData.LocatesBuddyID[i], SCRIPT_TASK_ENTER_VEHICLE)
		                                            IF (TaskStatus = FINISHED_TASK)
		                                                CLEAR_PED_TASKS(locatesData.LocatesBuddyID[i])
														
														#IF IS_DEBUG_BUILD
		                                                	PRINTLN("locates_private - HAVE_BUDDIES_JOINED_PLAYERS_GROUP - CLEAR_CHAR_TASKS - 2")
														#ENDIF
		                                            ENDIF
		                                            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(locatesData.LocatesBuddyID[i], FALSE)
		                                            SET_PED_AS_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
													
													#IF IS_DEBUG_BUILD
		                                           		PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - SET_GROUP_MEMBER - 2")
													#ENDIF
												ENDIF
	                                        ENDIF
	                                    ENDIF
	                                ENDIF
	                            ELSE
									
									//Fix for GTAV bug #234254: There seems to sometimes be a delay between the player pressing the exit vehicle button and
									//IS_PED_SITTING_IN_VEHICLE returning true, which can cause buddies to leave the car. If the exit button is pressed,
									//stop the buddies from being added to the group, reset the flag once IS_PED_SITTING_IN_VEHICLE returns false.
	                                IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), SpecificvehicleID)
	                                    IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())  
											IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR)
		                                       	SET_PED_AS_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
												
												#IF IS_DEBUG_BUILD
		                                        	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - SET_GROUP_MEMBER - 3")
												#ENDIF
											ENDIF
	                                    ELSE
											IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR)
												REMOVE_PED_FROM_GROUP(locatesData.LocatesBuddyID[i])
												SET_BIT(locatesData.iLocatesBitSet, BS_PLAYER_PRESSED_EXIT_BUT_STILL_IN_CAR)
												
												#IF IS_DEBUG_BUILD
		                                        	PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - REMOVE_CHAR_FROM_GROUP - 4")
												#ENDIF
											ENDIF
										ENDIF
	                                ELSE		
										//Don't remove from the group if the ped is fleeing the mission-critical vehicle due to it being on fire.
	                                    IF IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
										AND NOT IS_ENTITY_ON_FIRE(SpecificvehicleID)
	                                        REMOVE_PED_FROM_GROUP(locatesData.LocatesBuddyID[i])
											
											#IF IS_DEBUG_BUILD
	                                       		PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - REMOVE_CHAR_FROM_GROUP - 3")
											#ENDIF
	                                    ENDIF   
	                                ENDIF
	                            ENDIF		
	                        ENDIF
	                    ENDIF
	                ENDIF
	            ELSE
	                // remove blip if he had one
	                IF DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
	                    REMOVE_BLIP(locatesData.BuddyBlipID[i])
	                    PRINTSTRING("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - remove blip for char - 2")
	                    PRINTNL()
	                    SAFE_CLEAR_THIS_PRINT(sPickupText[i])
	                ENDIF
	            ENDIF
	        ENDIF
	    ENDREPEAT
	    
	    // count buddies missing
	    REPEAT 3 i
	        IF DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
	            bBuddyIsMissing[i] = TRUE
	            iBuddiesMissing++ 
	        ENDIF
	    ENDREPEAT
	    
	        
	    // print text - 'go back and get...'
	    IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
		
	        // check if any buddies are in the process of entering players vehicle
	        IF (iBuddiesMissing > 0)
	            REPEAT 3 i
	                IF (bBuddyIsMissing[i]) 
	                    IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])                    
	                        IF IS_CHAR_USING_SAME_VEHICLE_AS_PLAYER(locatesData.LocatesBuddyID[i])
	                        OR IS_ENTITY_AT_ENTITY(locatesData.LocatesBuddyID[i], PLAYER_PED_ID(), <<locatesData.fBuddyJoinDistance, locatesData.fBuddyJoinDistance, locatesData.fBuddyJoinDistance>>, FALSE)
	                            iBuddiesMissing--
	                            bBuddyIsMissing[i] = FALSE
	                        ENDIF
	                    ENDIF
	                ELSE
						IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])  
							IF NOT IS_ENTITY_AT_ENTITY(locatesData.LocatesBuddyID[i], PLAYER_PED_ID(), 
													  <<locatesData.fBuddyJoinDistance * 0.85, locatesData.fBuddyJoinDistance * 0.85, locatesData.fBuddyJoinDistance>>, FALSE)
							AND NOT IS_CHAR_USING_SAME_VEHICLE_AS_PLAYER(locatesData.LocatesBuddyID[i])
								//iBuddiesNearlyMissing++
							ENDIF
						ENDIF
					ENDIF
	            ENDREPEAT
	        ENDIF       
	        
	        iThisTime = GET_GAME_TIMER()
			
	//		PRINTSTRING("-----------------")
	//        PRINTNL()
	//		
	//		PRINTSTRING("locatesData.iPrintedMissingBuddiesNumber = ")
	//		PRINTINT(locatesData.iPrintedMissingBuddiesNumber)
	//        PRINTNL()
	//		
	//		PRINTSTRING("iBuddiesMissing = ")
	//        PRINTINT(iBuddiesMissing)
	//        PRINTNL()
	//                    
	//        PRINTSTRING("iTotalBuddies = ")
	//        PRINTINT(iTotalBuddies)
	//        PRINTNL()  	
	//		
	//		PRINTSTRING("locatesData.iLeftBuddyTime = ")
	//        PRINTINT(locatesData.iLeftBuddyTime)
	//        PRINTNL()
	//		
	//		PRINTSTRING("iThisTime = ")
	//        PRINTINT(iThisTime)
	//        PRINTNL()
			
	        IF ((iThisTime - locatesData.iLeftBuddyTime) > 1500)
	        OR (iTotalBuddies = 1)
	            IF (iBuddiesMissing > 0)                        
	                // what text to display?
	                IF (locatesData.iPrintedMissingBuddiesNumber < iBuddiesMissing)
						#IF IS_DEBUG_BUILD
							PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - printing 'pickup' text")
							PRINTLN("locatesData.iPrintedMissingBuddiesNumber = ", locatesData.iPrintedMissingBuddiesNumber)
							PRINTLN("iBuddiesMissing = ", iBuddiesMissing)
							PRINTLN("iTotalBuddies = ", iTotalBuddies)
						#ENDIF
	                
	                    IF ((iBuddiesMissing = iTotalBuddies) OR (iBuddiesNearlyMissing + iBuddiesMissing = iTotalBuddies))
	                    AND (iTotalBuddies > 1)
	                        IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_ALL_BUDDY_TEXT)
	                            GOD_TEXT(locatesData, sPickupAllBuddysText)
	                            SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_ALL_BUDDY_TEXT)
	                            locatesData.iPrintedMissingBuddiesNumber = iBuddiesMissing
	                        ELSE
								locatesData.iPrintedMissingBuddiesNumber = iBuddiesMissing
							ENDIF
	                    ELSE
	                        REPEAT 3 i              
	                            IF (bBuddyIsMissing[i]) 
	                                IF NOT IS_BIT_SET_PRINTED_PICKUP_BUDDY_TEXT(i, locatesData)
										IF NOT IS_STRING_NULL(sCrewNames[i])
	            							IF NOT ARE_STRINGS_EQUAL(sCrewNames[i], "") 
												GOD_TEXT_FOR_CREW_MEMBER(locatesData, sPickupText[i], sCrewNames[i])
												SET_BIT_PRINTED_PICKUP_BUDDY_TEXT(i, locatesData)
												locatesData.iPrintedMissingBuddiesNumber = iBuddiesMissing
											ENDIF
										ENDIF
										
										IF NOT IS_BIT_SET_PRINTED_PICKUP_BUDDY_TEXT(i, locatesData)
	                                    	GOD_TEXT(locatesData, sPickupText[i])
	                                   		SET_BIT_PRINTED_PICKUP_BUDDY_TEXT(i, locatesData)
											locatesData.iPrintedMissingBuddiesNumber = iBuddiesMissing
										ENDIF
	                                ELSE
										locatesData.iPrintedMissingBuddiesNumber = iBuddiesMissing
									ENDIF
	                            ENDIF
	                        ENDREPEAT 							
	                    ENDIF
	                ENDIF
	            ELSE
	                locatesData.iPrintedMissingBuddiesNumber = 0                    
	            ENDIF
	        ENDIF
	    ENDIF
	        
	    CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_WILL_WALK_TO_SPECIFIC_VEHICLE)

	    // all buddies are in group
	    IF (bAllBuddiesHaveJoinedPlayersGroup) 
	    AND NOT (bPlayerInvehicleWithTooFewSeats)
	        REPEAT 3 i
	            IF DOES_BLIP_EXIST(locatesData.BuddyBlipID[i])
	                REMOVE_BLIP(locatesData.BuddyBlipID[i])
					
					#IF IS_DEBUG_BUILD
	               		PRINTLN("HAVE_BUDDIES_JOINED_PLAYERS_GROUP - remove blip for char - 3")
					#ENDIF
					
	                SAFE_CLEAR_THIS_PRINT(sPickupText[i])                   
	            ENDIF
	        ENDREPEAT
			
	        SAFE_CLEAR_THIS_PRINT("MORE_SEATS")
	        RETURN(TRUE)    
	    ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	    // warp buddies to player
	    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	        VECTOR pos
	        REPEAT 3 i
	            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	                pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0,(1.0 + TO_FLOAT(i)), 0.0>>) 
	                IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
	                    SET_PED_COORDS_KEEP_VEHICLE(locatesData.LocatesBuddyID[i], <<pos.x, pos.y, pos.z>>)
	                ENDIF
	            ENDIF
	        ENDREPEAT
	    ENDIF
	#ENDIF	//	IS_DEBUG_BUILD

    RETURN(FALSE)

ENDFUNC

FUNC BOOL HAS_BUDDY_JOINED_PLAYERS_GROUP(LOCATES_HEADER_DATA &locatesData, PED_INDEX Buddy, STRING sPickupBuddyText, BOOL bShowBuddyRoute = FALSE, 
										 BOOL bIgnoreIfPlayerInvehicle  = FALSE, BOOL bJoinGroupEvenIfNotInSameMovement = FALSE, BOOL bLoseWantedLevel = FALSE)
    VEHICLE_INDEX Nullvehicle
    Nullvehicle = NULL
    RETURN(HAVE_BUDDIES_JOINED_PLAYERS_GROUP(locatesData, Buddy, NULL, NULL, sPickupBuddyText, sPickupBuddyText, sPickupBuddyText, sPickupBuddyText, locatesData.fBuddyJoinDistance, 
											 bShowBuddyRoute, Nullvehicle, bIgnoreIfPlayerInvehicle, bJoinGroupEvenIfNotInSameMovement, "", "", "", TM_ANY_3D, bLoseWantedLevel))
ENDFUNC


FUNC BOOL IS_PLAYER_AT_LOCATE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, VECTOR minCoords, Vector maxCoords, BOOL bShowCorona, TRANSPORT_MODE TransMode, 
							  PED_INDEX Buddy1=NULL, PED_INDEX Buddy2=NULL, PED_INDEX Buddy3=NULL, VEHICLE_INDEX vehicleID=NULL, 
							  STRING sGotoInstruction=NULL, STRING sDontLeaveBuddyBehind1=NULL, STRING sDontLeaveBuddyBehind2=NULL, STRING sDontLeaveBuddyBehind3=NULL, 
							  STRING sPickupAllBuddysText=NULL, STRING sGetInVehicle=NULL, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE,
							  STRING sGetBackInvehicle = NULL, BOOL bUseAngledArea = FALSE, INT iMinNoOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID,
							  STRING sHeistCrewName1=NULL, STRING sHeistCrewName2=NULL, STRING sHeistCrewName3=NULL, BOOL bShowLocationRoute = TRUE, FLOAT fAngledAreaWidth = 1.0)
    VEHICLE_INDEX temp_VEHICLE
    BOOL bReturn
    INT i
	VECTOR vBlipCoord = vecCoords + <<0.0, 0.0, 1.0>>
	
    locatesData.LocatesBuddyID[0] = Buddy1
    locatesData.LocatesBuddyID[1] = Buddy2
    locatesData.LocatesBuddyID[2] = Buddy3
    locatesData.SpeakingBuddyID = Buddy1

    UPDATE_AMBIENT_ANIM_BLOCKING(locatesData)
	
	#IF IS_DEBUG_BUILD
    	CREATE_LOCATES_HEADER_WIDGET(locatesData)  
	#ENDIF
    UPDATE_CHARS_TO_NOT_FLY_THROUGH_WINDSCREEN(locatesData)
	
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	
	//IF IS_VEHICLE_DRIVEABLE(vehicleID)
	//OR (vehicleID = NULL)	
	
		IF HAVE_BUDDIES_JOINED_PLAYERS_GROUP(locatesData, locatesData.LocatesBuddyID[0], locatesData.LocatesBuddyID[1], locatesData.LocatesBuddyID[2], sDontLeaveBuddyBehind1, 
											 sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, locatesData.fBuddyJoinDistance, bShowBuddyRoute, vehicleID,
											 FALSE, FALSE, sHeistCrewName1, sHeistCrewName2, sHeistCrewName3, TransMode, bLoseWantedLevel)
			
			SAFE_CLEAR_THIS_PRINT(sDontLeaveBuddyBehind1)
			SAFE_CLEAR_THIS_PRINT(sDontLeaveBuddyBehind2)
			SAFE_CLEAR_THIS_PRINT(sDontLeaveBuddyBehind3)
			SAFE_CLEAR_THIS_PRINT(sPickupAllBuddysText)     
			
			IF IS_SCREEN_FADED_IN() 			
				
				bReturn = FALSE
				
				IF IS_VEHICLE_DRIVEABLE(vehicleID)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicleID)
						SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE) // if player already started in vehicle
						
						//If this is the first time the ped has been in this vehicle, they'll need to be told to get back in if they get out.
						IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
							CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE) 
						ENDIF
						
						IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYED_GET_BACK_IN_VEHICLE_SPEECH)
							CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYED_GET_BACK_IN_VEHICLE_SPEECH)
						ENDIF
						
						SET_BIT(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
						bReturn = TRUE
					ENDIF
				ELSE
					IF ((TransMode = TM_IN_VEHICLE_2D) OR (TransMode = TM_IN_VEHICLE_3D))
						IF IS_PLAYER_IN_A_VEHICLE_WITH_ENOUGH_SEATS(locatesData, iMinNoOfSeats)
							SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE) // if player already started in vehicle
							
							//If this is the first time the ped has been in this vehicle, they'll need to be told to get back in if they get out.
							IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
								CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE) 
							ENDIF
							
							SET_BIT(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
							bReturn = TRUE
						ENDIF
					ELSE
						bReturn = TRUE
					ENDIF
				ENDIF
				
				IF (bReturn)											
					// clear 'get vehicle' text
					SAFE_CLEAR_THIS_PRINT(sGetInVehicle)
					SAFE_CLEAR_THIS_PRINT(sGetBackInvehicle)
					SAFE_CLEAR_THIS_PRINT("MORE_SEATS")
					
					// lose wanted level?
					IF (bLoseWantedLevel)
					AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		
						IF DOES_BLIP_EXIST(locatesData.LocationBlip)
							REMOVE_BLIP(locatesData.LocationBlip)       
							SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
						ENDIF
						IF DOES_BLIP_EXIST(locatesData.vehicleBlip)
							REMOVE_BLIP(locatesData.vehicleBlip)							
						ENDIF
						
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT IS_BUDDY_PLAYING_AMBIENT_SPEECH(locatesData)
						AND NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
							IF bShowWantedText
								GOD_TEXT(locatesData, "LOSE_WANTED")
								IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[0])
									PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[0], "VEHICLE_POLICE_PURSUIT", SPEECH_PARAMS_FORCE)    
								ENDIF
							ENDIF
							
							SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
							CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
						ENDIF
		
					ELSE
		
						// clear wanted level text
						IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
							SAFE_CLEAR_THIS_PRINT("LOSE_WANTED")
							CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
							SET_BIT(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
						ENDIF
						
						// play lose cops speech
						IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								// play ambient speech
								IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[0])
									PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[0], "LOSE_WANTED_LEVEL", SPEECH_PARAMS_FORCE)
								ENDIF
								CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
							ENDIF
						ENDIF
		
						// create blip
						IF NOT DOES_BLIP_EXIST(locatesData.LocationBlip)
							IF DOES_BLIP_EXIST(locatesData.vehicleBlip)
                                REMOVE_BLIP(locatesData.vehicleBlip)
                            ENDIF
							locatesData.LocationBlip =  CREATE_BLIP_FOR_COORD(vBlipCoord)
							IF NOT (BlipSprite = RADAR_TRACE_INVALID)
								SET_BLIP_SPRITE(locatesData.LocationBlip, BlipSprite)
							ENDIF
							
							IF bShowLocationRoute
								TURN_ON_GPS_FOR_BLIP(locatesData.LocationBlip, locatesData)
							ENDIF
							
						ELIF NOT ARE_VECTORS_ALMOST_EQUAL(vBlipCoord, GET_BLIP_COORDS(locatesData.LocationBlip), 0.1)
							SET_BLIP_COORDS(locatesData.LocationBlip, vBlipCoord)
							
							IF bShowLocationRoute
								TURN_ON_GPS_FOR_BLIP(locatesData.LocationBlip, locatesData)
							ENDIF
							
						ENDIF
						
						// print text
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)                     
								GOD_TEXT(locatesData, sGotoInstruction)
								SET_BIT(locatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
							ENDIF
						ENDIF
						
						// if player is in a vehicle with too few seats then hide chevron
						IF ((TransMode = TM_IN_VEHICLE_2D) OR (TransMode = TM_IN_VEHICLE_3D))
							IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
								bShowCorona = FALSE
							ENDIF
						ENDIF
						
						bReturn = FALSE
						
						//The entity commands use different parameters to the locates header, so do some conversion here.
						BOOL bIn3D = FALSE
						PED_TRANSPORT_MODE pedTransMode = TM_ANY
						
						IF TransMode = TM_ANY_3D OR TransMode = TM_ON_FOOT_3D OR TransMode = TM_IN_VEHICLE_3D
							bIn3D = TRUE
						ENDIF
						
						IF TransMode = TM_ON_FOOT_2D OR TransMode = TM_ON_FOOT_3D
							pedTransMode = TM_ON_FOOT
						ELIF TransMode = TM_IN_VEHICLE_2D OR TransMode = TM_IN_VEHICLE_3D
							pedTransMode = TM_IN_VEHICLE
						ENDIF
						
						
						// ANGLED AREA CHECK
						IF bUseAngledArea
							IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vecCoords, vecLocateSize, bShowcorona, bIn3D, pedTransMode) //displays corona over blip location
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), minCoords, maxCoords, fAngledAreaWidth, FALSE, bIn3D, pedTransMode)
								bReturn = TRUE
							ENDIF
						// AXIS ALIGNED AREA CHECK
						ELSE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vecCoords, vecLocateSize, bShowCorona, bIn3D, pedTransMode)
								bReturn = TRUE
							ENDIF
						ENDIF
						
						IF (bReturn)

							bReturn = TRUE
							REPEAT 3 i
								IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
									IF TransMode = TM_IN_VEHICLE_2D 
									OR TransMode = TM_IN_VEHICLE_3D
										temp_VEHICLE = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										
										IF NOT IS_PED_IN_VEHICLE(locatesData.LocatesBuddyID[i], temp_VEHICLE)
											bReturn = FALSE
										ENDIF
									ELSE
										IF vehicleID != NULL
											IF NOT IS_PED_IN_VEHICLE(locatesData.LocatesBuddyID[i], vehicleID)
												bReturn = FALSE
											ENDIF
										ELSE
											IF NOT IS_PED_GROUP_MEMBER(locatesData.LocatesBuddyID[i], PLAYER_GROUP_ID())
											OR NOT IS_BUDDY_IN_SAME_GROUP_MOVEMENT(locatesData.LocatesBuddyID[i])
												bReturn = FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF bReturn
								IF LOCATES_CAN_PLAYER_START_CUTSCENE(locatesData)
									#IF IS_DEBUG_BUILD
										PRINTLN("IS_PLAYER_AT_LOCATE - debug 7")
									#ENDIF
								
									SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
									SAFE_CLEAR_THIS_PRINT(sGetInVehicle)
									SAFE_CLEAR_THIS_PRINT(sDontLeaveBuddyBehind1)
									SAFE_CLEAR_THIS_PRINT(sDontLeaveBuddyBehind2)
									SAFE_CLEAR_THIS_PRINT(sDontLeaveBuddyBehind3)
									SAFE_CLEAR_THIS_PRINT(sPickupAllBuddysText)
									SAFE_CLEAR_THIS_PRINT("LOSE_WANTED")
									SAFE_CLEAR_THIS_PRINT("MORE_SEATS")
									SAFE_CLEAR_THIS_PRINT(sGetBackInvehicle)
									CLEAR_MISSION_LOCATE_STUFF(locatesData)
									RETURN(TRUE)
								ENDIF
							ENDIF
						ENDIF
		
					ENDIF
			
				ELSE
					
					IF DOES_ENTITY_EXIST(vehicleID) // only if using a specific car 
					
						// lose wanted level? (only if player has not yet got in mission specific vehicle)
						IF (bLoseWantedLevel)
						AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						AND (NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE) AND NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_STARTS_RIGHT_NEXT_TO_VEHICLE))
	
							// clear 'get in vehicle' text
							SAFE_CLEAR_THIS_PRINT(sGetInVehicle)
							SAFE_CLEAR_THIS_PRINT(sGetBackInvehicle)
	
							IF DOES_BLIP_EXIST(locatesData.LocationBlip)
							OR DOES_BLIP_EXIST(locatesData.vehicleBlip)
								REMOVE_BLIP(locatesData.LocationBlip)
								REMOVE_BLIP(locatesData.vehicleBlip)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("IS_PLAYER_AT_LOCATE_WITH_BUDDIES_IN_VEHICLE - remove blip 3")
								#ENDIF
								SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
							ENDIF
	
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							AND NOT IS_BUDDY_PLAYING_AMBIENT_SPEECH(locatesData)
							AND NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
								IF bShowWantedText //Allow wanted text to be disabled: this is for scripts that wish to use their own dialogue with the header.
									GOD_TEXT(locatesData, "LOSE_WANTED")
									IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[0])
										PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[0], "VEHICLE_POLICE_PURSUIT", SPEECH_PARAMS_FORCE)    
									ENDIF
								ENDIF
								
								SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
								CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
							ENDIF   
							
						ELSE
						
							// clear wanted level text
							IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
								SAFE_CLEAR_THIS_PRINT("LOSE_WANTED")
								CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
								SET_BIT(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
							ENDIF       
							
							// play lose cops speech
							IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									// play ambient speech
									IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[0])
										PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[0], "LOSE_WANTED_LEVEL", SPEECH_PARAMS_FORCE)
									ENDIF
									CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
								ENDIF
							ENDIF                       
							
							IF IS_VEHICLE_DRIVEABLE(vehicleID)
								// create blip
								IF NOT DOES_BLIP_EXIST(locatesData.vehicleBlip)
									IF DOES_BLIP_EXIST(locatesData.LocationBlip)
										REMOVE_BLIP(locatesData.LocationBlip)
										SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
									ENDIF
									locatesData.vehicleBlip = CREATE_BLIP_FOR_ENTITY(vehicleID, FALSE)
									SET_BLIP_DISPLAY(locatesData.vehicleBlip, DISPLAY_BLIP)
									
									#IF IS_DEBUG_BUILD
										PRINTLN("IS_PLAYER_AT_LOCATE_WITH_BUDDIES_IN_VEHICLE - add blip 2")
									#ENDIF

									IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
										TURN_ON_GPS_FOR_BLIP(locatesData.vehicleBlip, locatesData)
									ENDIF
								ENDIF
			
								// print text
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF) //or conversation_paused
									IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
										GOD_TEXT(locatesData, sGetInVehicle)
										SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
										CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE) 
										
										#IF IS_DEBUG_BUILD
											PRINTLN("set bit BS_PRINTED_GET_IN_VEHICLE 3")
										#ENDIF
									ELSE
										IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
											IF NOT IS_STRING_NULL(sGetBackInvehicle)
												IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
													GOD_TEXT(locatesData, sGetBackInvehicle)
													SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
												ENDIF
											ELSE
												IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
													GOD_TEXT(locatesData, sGetInVehicle)
													SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
												ENDIF   
											ENDIF
											
											IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYED_GET_BACK_IN_VEHICLE_SPEECH)
												IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[0])
													PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[0], "GET_IN_CAR", SPEECH_PARAMS_FORCE)
												ENDIF
												SET_BIT(locatesData.iLocatesBitSet, BS_PLAYED_GET_BACK_IN_VEHICLE_SPEECH)
											ENDIF
										ENDIF
									ENDIF   
								ENDIF
							ENDIF
						ENDIF					
					ELSE
						IF DOES_BLIP_EXIST(locatesData.LocationBlip)
							#IF IS_DEBUG_BUILD
								PRINTLN("IS_PLAYER_AT_LOCATE - Removing location blip, player not currently in suitable vehicle.")
							#ENDIF
							
							REMOVE_BLIP(locatesData.LocationBlip)
							SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
						ENDIF   
						
						// print text
						IF ((TransMode = TM_IN_VEHICLE_2D) OR (TransMode = TM_IN_VEHICLE_3D))
							IF (iMinNoOfSeats > 0)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)	
											INT iMaxNumPeds = 0
											INT iNumPedToSpeak = 0
										
											REPEAT 3 i
												IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[i])
													iMaxNumPeds++
												ENDIF
											ENDREPEAT
										
											iNumPedToSpeak = GET_RANDOM_INT_IN_RANGE(0, iMaxNumPeds)
										
											IF NOT IS_PED_INJURED(locatesData.LocatesBuddyID[iNumPedToSpeak])
												PLAY_PED_AMBIENT_SPEECH(locatesData.LocatesBuddyID[iNumPedToSpeak], "NEED_A_BIGGER_VEHICLE", SPEECH_PARAMS_FORCE)
											ENDIF
										
											GOD_TEXT(locatesData, "MORE_SEATS")
											SET_BIT(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
										ENDIF	
									ELSE 
										IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
											GOD_TEXT(locatesData, sGetInVehicle)
											SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
											CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE) 
											
											#IF IS_DEBUG_BUILD
												PRINTLN("set bit BS_PRINTED_GET_IN_VEHICLE 4")
											#ENDIF
										ELIF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
											IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
												GOD_TEXT(locatesData, sGetBackInvehicle)
												SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
												
												#IF IS_DEBUG_BUILD
													PRINTLN("set bit BS_PRINTED_GET_BACK_IN_VEHICLE 4")
												#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
										GOD_TEXT(locatesData, sGetInVehicle)
										SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
										CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
										
										#IF IS_DEBUG_BUILD
											PRINTLN("set bit BS_PRINTED_GET_IN_VEHICLE 5")
										#ENDIF
									ELSE
										IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
											IF NOT IS_STRING_NULL(sGetBackInvehicle)
												IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
													GOD_TEXT(locatesData, sGetBackInvehicle)
													SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
												ENDIF
											ELSE
												IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
													GOD_TEXT(locatesData, sGetInVehicle)
													SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
												ENDIF   
											ENDIF
										ENDIF
									ENDIF
								ENDIF 
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
						
			ENDIF
		ELSE        
			IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
				CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
			ENDIF       
			SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
			SAFE_CLEAR_THIS_PRINT(sGetInVehicle) 
			SAFE_CLEAR_THIS_PRINT(sGetBackInvehicle)
			SAFE_CLEAR_THIS_PRINT(sGetInVehicle)
			SAFE_CLEAR_THIS_PRINT("LOSE_WANTED")
			//SAFE_CLEAR_THIS_PRINT("MORE_SEATS") //Buddies seem to be removed from group if there's not enough seats, this call prevents the "more seats" line playing.
			IF DOES_BLIP_EXIST(locatesData.LocationBlip)
				REMOVE_BLIP(locatesData.LocationBlip)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("IS_PLAYER_AT_LOCATE - Removing location blip, HAVE_BUDDIES_JOINED_PLAYERS_GROUP returned false.")
				#ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(locatesData.vehicleBlip)
                REMOVE_BLIP(locatesData.vehicleBlip)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("IS_PLAYER_AT_LOCATE - Removing vehicle blip, HAVE_BUDDIES_JOINED_PLAYERS_GROUP returned false.")
				#ENDIF
			ENDIF
		ENDIF
	//ENDIF


    // ==== SKIP TO LOCATION (FOR DEBUG) =====
	#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(vehicleID)
			J_SKIP_LH(vecCoords, locatesData, FALSE, vehicleID)
		ELSE
			IF ((TransMode = TM_IN_VEHICLE_2D) OR (TransMode = TM_IN_VEHICLE_3D))
				J_SKIP_LH(vecCoords, locatesData, FALSE)
			ELSE
				IF (TransMode = TM_ON_FOOT_3D)
					J_SKIP_LH(vecCoords, locatesData, TRUE)
				ELSE
					J_SKIP_LH(vecCoords, locatesData)
				ENDIF
			ENDIF
		ENDIF
    #ENDIF	
	
   //ENSURE_LOCATE_IS_CLEAR_OF_RANDOM_VEHICLES(vecCoords, locatesData)

	CLEAR_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_WILL_ONLY_GET_IN_WHEN_PLAYER_DOES)
    CLEAR_BIT(locatesData.iLocatesBitSet, BS_DO_LOCATES_ALMOST_STOPPED_CHECK)

    RETURN(FALSE)

ENDFUNC

PROC PLAY_AMBIENT_LOCATES_DIALOGUE(LOCATES_HEADER_DATA &locatesData, structPedsForConversation sConversationPeds, STRING strTextBlock, STRING strMessage, PED_INDEX pedToTalk)
	IF NOT IS_STRING_NULL(strMessage)
        IF NOT ARE_STRINGS_EQUAL(strMessage, "")
			PLAY_AMBIENT_DIALOGUE_LINE(sConversationPeds, pedToTalk, strTextBlock, strMessage)
		ENDIF
	ENDIF

	locatesData.iGodTextTime = GET_GAME_TIMER()
ENDPROC

///Used for ambient scripts only, this triggers the given dialogue where god text would normally be displayed.
PROC MANAGE_AMBIENT_LOCATES_DIALOGUE(LOCATES_HEADER_DATA &locatesData, structPedsForConversation sConversationPeds, STRING strTextBlock,
									 STRING strLabelGetInVehicle, STRING strLabelGetBackInVehicle, STRING strLabelGoToLocation, STRING strLabelLoseCops, STRING strLeaveBuddy1, 
									 STRING strLeaveBuddy2, STRING strLeaveBuddy3, STRING strLeaveAllBuddies, STRING strNeedMoreSeats)
	IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData)
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLabelLoseCops, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_GET_IN_VEHICLE)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLabelGetInVehicle, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_GET_IN_VEHICLE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLabelGetBackInVehicle, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLabelGoToLocation, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLeaveBuddy1, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLeaveBuddy2, locatesData.LocatesBuddyID[1])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_1)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLeaveBuddy3, locatesData.LocatesBuddyID[2])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_2)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_PICKUP_ALL_BUDDY_TEXT)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_ALL_BUDDY_TEXT)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strLeaveAllBuddies, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PRINTED_PICKUP_ALL_BUDDY_TEXT)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
			IF NOT IS_BIT_SET(locatesData.iAmbientDialogueBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
				PLAY_AMBIENT_LOCATES_DIALOGUE(locatesData, sConversationPeds, strTextBlock, strNeedMoreSeats, locatesData.LocatesBuddyID[0])
				SET_BIT(locatesData.iAmbientDialogueBitSet, BS_PLAYER_IS_IN_VEHICLE_WITH_TOO_FEW_SEATS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

