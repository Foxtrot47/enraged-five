// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/09/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 			(A1)  - Family_Controller file for use – Family_Controller.sch			 ___
// ___ 																					 ___
// _________________________________________________________________________________________


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_streaming.sch"

//- script headers	-//

//- public headers	-//
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "timer_public.sch"
USING "minigames_helpers.sch"
USING "family_public.sch"

USING "comms_control_public.sch"

//- private headers	-//
USING "family_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "script_debug.sch"
	USING "shared_debug.sch"
	USING "family_debug.sch"
#ENDIF

// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM enumFamilyScenes
	FS_MICHAEL = 0,		//michaels house, with Son, Daughter, Wife, MexMaid and GARDENER
	FS_FRANKLIN_0,		//franklins city house....
	FS_FRANKLIN_1,		//franklins hills house....
	FS_TREVOR_0,		//trevors trailer....
	FS_TREVOR_1,		//trevors safehouse....
	
    MAX_FAMILY_SCENES,
    NO_FAMILY_SCENES
ENDENUM

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
STRUCT FriendControllerVars
		//- Flags(INT and BOOL) -//
	INT					iFamily_Controller_in_progress					= 0
	enumFamilyScenes	eFamilyScenesToCheck							= NO_FAMILY_SCENES
	
		//- Coordinates(VECTORS and FLOATS) -//
	VECTOR				vFamily_scene_coord[MAX_FAMILY_SCENES]
	FLOAT				fFamily_scene_head[MAX_FAMILY_SCENES]
	
	#IF IS_DEBUG_BUILD
		//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
	WIDGET_GROUP_ID	family_controller_widget, family_controller_subwidget
	BOOL			bToggle_family_controller_widget, bJumpTo_familyScene[MAX_FAMILY_SCENES], bSaveFamilyAnimProgress, bSaveFamilyAnimPreload
	BOOL			bSetDebugLinesTrue
	TEXT_WIDGET_ID	tCurrentFamilyEvent[MAX_FAMILY_MEMBER], tPreviousFamilyEvent[MAX_FAMILY_MEMBER]
	#ENDIF
ENDSTRUCT

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Family_Controller_Variables(FriendControllerVars &sVars)
	//- vectors -//
	sVars.vFamily_scene_coord[FS_MICHAEL]		= << -812.0607, 179.5117, 71.1531 >>
	sVars.vFamily_scene_coord[FS_FRANKLIN_0]	= << -14.3064, -1435.9974, 30.1160 >>
	sVars.vFamily_scene_coord[FS_FRANKLIN_1]	= <<-1.2632, 531.5870, 174.3424>>
	sVars.vFamily_scene_coord[FS_TREVOR_0]		= << 1974.6129, 3819.1438, 32.4374 >>
	sVars.vFamily_scene_coord[FS_TREVOR_1]		= << -1152.5707, -1517.6010, 9.6346 >>
	
	//- floats -//
	sVars.fFamily_scene_head[FS_MICHAEL]		= 222.8314
	sVars.fFamily_scene_head[FS_FRANKLIN_0]		= 188.5817
	sVars.fFamily_scene_head[FS_FRANKLIN_1]		= 167.2562
	sVars.fFamily_scene_head[FS_TREVOR_0]		= 92.6017
	sVars.fFamily_scene_head[FS_TREVOR_1]		= 90.6729
	
	//- ints -//
	
	//-- structs : FAMILY_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Initialises the family widget
PROC Create_Family_Controller_Widget(FriendControllerVars &sVars, WIDGET_GROUP_ID main_widget)
	SET_CURRENT_WIDGET_GROUP(main_widget)
	sVars.family_controller_widget = START_WIDGET_GROUP("Family Controller")
		ADD_WIDGET_BOOL("bToggle_family_controller_widget", sVars.bToggle_family_controller_widget)
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(main_widget)
ENDPROC

//PURPOSE:	Moniters for any changes in the family widget
PROC Watch_Family_Controller_Widget(FriendControllerVars &sVars)
	
	IF g_bDrawDebugFamilyStuff
		IF NOT sVars.bSetDebugLinesTrue
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			sVars.bSetDebugLinesTrue = TRUE
		ENDIF
	ELSE
		sVars.bSetDebugLinesTrue = FALSE
	ENDIF	
	IF NOT sVars.bToggle_family_controller_widget
		IF DOES_WIDGET_GROUP_EXIST(sVars.family_controller_subwidget)
			DELETE_WIDGET_GROUP(sVars.family_controller_subwidget)
		ENDIF
		
		IF g_bUpdatedFamilyEvents
			sVars.bToggle_family_controller_widget = TRUE
		ENDIF
	ELSE
		IF NOT DOES_WIDGET_GROUP_EXIST(sVars.family_controller_subwidget)
			
			enumFamilyMember eFamMem
			TEXT_LABEL_63 str
			
			SET_CURRENT_WIDGET_GROUP(sVars.family_controller_widget)
				sVars.family_controller_subwidget = START_WIDGET_GROUP("Family Controller")
				
					ADD_WIDGET_BOOL("g_bDrawDebugFamilyStuff", g_bDrawDebugFamilyStuff)
					ADD_WIDGET_BOOL("bJumpTo_familyScene[FS_MICHAEL]", sVars.bJumpTo_familyScene[FS_MICHAEL])
					ADD_WIDGET_BOOL("bJumpTo_familyScene[FS_FRANKLIN_0]", sVars.bJumpTo_familyScene[FS_FRANKLIN_0])
					ADD_WIDGET_BOOL("bJumpTo_familyScene[FS_FRANKLIN_1]", sVars.bJumpTo_familyScene[FS_FRANKLIN_1])
					ADD_WIDGET_BOOL("bJumpTo_familyScene[FS_TREVOR_0]", sVars.bJumpTo_familyScene[FS_TREVOR_0])
					ADD_WIDGET_BOOL("bJumpTo_familyScene[FS_TREVOR_1]", sVars.bJumpTo_familyScene[FS_TREVOR_1])
					
					START_WIDGET_GROUP("current family activities")
						REPEAT MAX_FAMILY_MEMBER eFamMem
							str = Get_String_From_FamilyMember(eFamMem)
							str += "'s current event"
							
							sVars.tCurrentFamilyEvent[eFamMem] = ADD_TEXT_WIDGET(str)
							SET_CONTENTS_OF_TEXT_WIDGET(sVars.tCurrentFamilyEvent[eFamMem], Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamMem]))
							
							sVars.tPreviousFamilyEvent[eFamMem] = ADD_TEXT_WIDGET("")
							SET_CONTENTS_OF_TEXT_WIDGET(sVars.tPreviousFamilyEvent[eFamMem], Get_String_From_FamilyEvent(Get_Previous_Event_For_FamilyMember(eFamMem)))
						ENDREPEAT
					STOP_WIDGET_GROUP()
					
					ADD_WIDGET_BOOL("g_bUpdatedFamilyEvents", g_bUpdatedFamilyEvents)
					
					ADD_WIDGET_BOOL("bSaveFamilyAnimProgress",  sVars.bSaveFamilyAnimProgress)
					ADD_WIDGET_BOOL("bSaveFamilyAnimPreload",  sVars.bSaveFamilyAnimPreload)
					
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(sVars.family_controller_widget)
		ELSE
			enumFamilyScenes eFamSce
			REPEAT MAX_FAMILY_SCENES eFamSce
				IF sVars.bJumpTo_familyScene[eFamSce]
					
					CPRINTLN(DEBUG_FAMILY, "sVars.bJumpTo_familyScene[", eFamSce, "] = TRUE")
					
					IF g_bWarpPlayerOnFamilyDebug
						SET_CLOCK_TIME(GET_RANDOM_INT_IN_RANGE(0, 24), 0, 0)
					
					
						INTERIOR_INSTANCE_INDEX iWarpRoom = GET_INTERIOR_AT_COORDS(sVars.vFamily_scene_coord[eFamSce])
						
						//Pin interior and wait for it to load.
						PIN_INTERIOR_IN_MEMORY(iWarpRoom)
						WHILE ((NOT IS_INTERIOR_READY(iWarpRoom))
						AND sVars.bJumpTo_familyScene[eFamSce])
							IF NOT IS_INTERIOR_READY(iWarpRoom)
								CPRINTLN(DEBUG_FAMILY, "interior ", NATIVE_TO_INT(iWarpRoom), " is not ready")
							ELSE
								CPRINTLN(DEBUG_FAMILY, "interior ", NATIVE_TO_INT(iWarpRoom), " is ready")
							ENDIF
							
							WAIT(0)
						ENDWHILE
						
						enumCharacterList eThisPlayerChar
						SELECTOR_SLOTS_ENUM eThisPlayerSlot
						
						SWITCH eFamSce
							CASE FS_MICHAEL
								eThisPlayerChar = CHAR_MICHAEL
								eThisPlayerSlot = SELECTOR_PED_MICHAEL
							BREAK
							CASE FS_FRANKLIN_0
							CASE FS_FRANKLIN_1
								eThisPlayerChar = CHAR_FRANKLIN
								eThisPlayerSlot = SELECTOR_PED_FRANKLIN
							BREAK
							CASE FS_TREVOR_0
								
								SWITCH g_eCurrentFamilyEvent[g_eDebugSelectedMember]
									CASE FE_T0_TREVOR_and_kidnapped_wife_walk		FALLTHRU
									CASE FE_T0_TREVOR_and_kidnapped_wife_stare		FALLTHRU
									CASE FE_T0_TREVOR_smoking_crystal				FALLTHRU
									#IF NOT IS_JAPANESE_BUILD
									CASE FE_T0_TREVOR_doing_a_shit					FALLTHRU
									#ENDIF
									//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh		FALLTHRU
									CASE FE_T0_TREVOR_blowing_shit_up				FALLTHRU
									CASE FE_T0_TREVOR_passed_out_naked_drunk
										eThisPlayerChar = CHAR_MICHAEL
										eThisPlayerSlot = SELECTOR_PED_MICHAEL
									BREAK
									DEFAULT
										eThisPlayerChar = CHAR_TREVOR
										eThisPlayerSlot = SELECTOR_PED_TREVOR
									BREAK
								ENDSWITCH
								
							BREAK
							CASE FS_TREVOR_1
								eThisPlayerChar = CHAR_TREVOR
								eThisPlayerSlot = SELECTOR_PED_TREVOR
							BREAK
							
							DEFAULT
								eThisPlayerChar = GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()
								eThisPlayerSlot = NUMBER_OF_SELECTOR_PEDS
								SCRIPT_ASSERT("invalid bJumpTo_familyScene[eFamSce]")
							BREAK
						ENDSWITCH
						
						IF NOT IS_PED_THE_CURRENT_PLAYER_PED(eThisPlayerChar)
							CPRINTLN(DEBUG_FAMILY, "Watch_Family_Controller_Widget: current player is \"", GET_CURRENT_PLAYER_PED_STRING(), "\", debug to \"", GET_PLAYER_PED_STRING(eThisPlayerChar), "\"")
							
							WHILE NOT SET_CURRENT_SELECTOR_PED(eThisPlayerSlot, TRUE)
								WAIT(0)
							ENDWHILE
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENDIF
						ENDIF
						
						//Warp the player
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), sVars.vFamily_scene_coord[eFamSce]+<<0,0,1>>)
						ENDIF
						
						
						WAIT(0)
						
						//Unpin interior now that we're in it.
						UNPIN_INTERIOR(iWarpRoom)
					ENDIF
					
					sVars.bJumpTo_familyScene[eFamSce] = FALSE
				ENDIF
			ENDREPEAT
			
			enumFamilyMember eFamMem
			REPEAT MAX_FAMILY_MEMBER eFamMem
				SET_CONTENTS_OF_TEXT_WIDGET(sVars.tCurrentFamilyEvent[eFamMem], Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamMem]))
				SET_CONTENTS_OF_TEXT_WIDGET(sVars.tPreviousFamilyEvent[eFamMem], Get_String_From_FamilyEvent(Get_Previous_Event_For_FamilyMember(eFamMem)))
			ENDREPEAT
			
			IF sVars.bSaveFamilyAnimProgress
				enumFamilyEvents eFamilyEvent
				REPEAT MAX_FAMILY_EVENTS eFamilyEvent
					
					enumFamilyMember eFamilyMember = FM_MICHAEL_SON
					
					WHILE NOT IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, eFamilyEvent)
						eFamilyMember = INT_TO_ENUM(enumFamilyMember, ENUM_TO_INT(eFamilyMember)+1)
					ENDWHILE
					
					TEXT_LABEL_63 tFamilyAnimDict = "", tFamilyAnimClip = ""
					ANIMATION_FLAGS eFamilyAnimFlag = AF_DEFAULT
					enumFamilyAnimProgress eFamilyAnimProgress = MAX_FAMILY_ANIM_PROGRESS
					
					IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
							tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag,
							eFamilyAnimProgress)
						
						SAVE_STRING_TO_DEBUG_FILE("<scriptItem title=\"")
						SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))
						SAVE_STRING_TO_DEBUG_FILE("\"	")
						
						IF (eFamilyAnimProgress = FAP_1_placeholder)
							SAVE_STRING_TO_DEBUG_FILE("highlight=\"TRUE\"")
						ENDIF
						
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
					ENDIF
				
				ENDREPEAT
				
				sVars.bSaveFamilyAnimProgress = FALSE
			ENDIF
			
			IF sVars.bSaveFamilyAnimPreload
				
				OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				TEXT_LABEL_63 tFamilyAnimDict
				
				enumFamilyEvents eFamilyEvent
				REPEAT MAX_FAMILY_EVENTS eFamilyEvent
					
					SAVE_STRING_TO_DEBUG_FILE("scene[")
					SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eFamilyEvent))
					SAVE_STRING_TO_DEBUG_FILE("] ")
					SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))
					SAVE_STRING_TO_DEBUG_FILE(": ")
					
					tFamilyAnimDict = ""
					IF PRIVATE_Preload_FamilyMember_Anim(eFamilyEvent, tFamilyAnimDict)
						SAVE_STRING_TO_DEBUG_FILE("\"")
						SAVE_STRING_TO_DEBUG_FILE(tFamilyAnimDict)
						SAVE_STRING_TO_DEBUG_FILE("\"")
						
						
						REQUEST_ANIM_DICT(tFamilyAnimDict)
						REMOVE_ANIM_DICT(tFamilyAnimDict)
					ELSE
						SAVE_STRING_TO_DEBUG_FILE("NONE")
					ENDIF
				
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
				ENDREPEAT
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				CLOSE_DEBUG_FILE()
				
				sVars.bSaveFamilyAnimPreload = FALSE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the family (debug pass etc)
PROC Family_Controller_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		//
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		//
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_J)	//jump to next family stage
		//
	ENDIF
	
ENDPROC

PROC Draw_Selected_Family_Controller_Debug(FriendControllerVars &sVars)
	
	IF g_bUpdatedFamilyEvents
		
		IF NOT GET_MISSION_FLAG() //TEMP
		
			SWITCH g_eDebugSelectedMember
				CASE FM_MICHAEL_SON			FALLTHRU
				CASE FM_MICHAEL_DAUGHTER	FALLTHRU
				CASE FM_MICHAEL_WIFE		FALLTHRU
				CASE FM_MICHAEL_MEXMAID		FALLTHRU
				CASE FM_MICHAEL_GARDENER	
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family_scene_m")) <= 0)
						sVars.bJumpTo_familyScene[FS_MICHAEL] = TRUE
					ENDIF
				BREAK
				CASE FM_FRANKLIN_AUNT		FALLTHRU
				CASE FM_FRANKLIN_LAMAR		FALLTHRU
				CASE FM_FRANKLIN_STRETCH	
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family_scene_f0")) <= 0)
						sVars.bJumpTo_familyScene[FS_FRANKLIN_0] = TRUE
					ENDIF
				BREAK
				
				CASE FM_TREVOR_0_RON		FALLTHRU
				CASE FM_TREVOR_0_TREVOR		FALLTHRU
				CASE FM_TREVOR_0_WIFE		FALLTHRU
				CASE FM_TREVOR_0_MOTHER
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family_scene_t0")) <= 0)
						sVars.bJumpTo_familyScene[FS_TREVOR_0] = TRUE
					ENDIF
				BREAK
				
				CASE FM_TREVOR_0_MICHAEL
//					IF (g_eCurrentFamilyEvent[g_eDebugSelectedMember] <> FE_M_MICHAEL_MIC2_washing_face)
						IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family_scene_t0")) <= 0)
							sVars.bJumpTo_familyScene[FS_TREVOR_0] = TRUE
						ENDIF
//					ELSE
//						IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family_scene_m")) <= 0)
//							bJumpTo_familyScene[FS_MICHAEL] = TRUE
//						ENDIF
//					ENDIF
				BREAK
				
				CASE FM_TREVOR_1_FLOYD		FALLTHRU
				CASE FM_TREVOR_1_WADE
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family_scene_t1")) <= 0)
						sVars.bJumpTo_familyScene[FS_TREVOR_1] = TRUE
					ENDIF
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
		ELSE
			g_bUpdatedFamilyEvents = FALSE
		ENDIF
	ENDIF
	
	
ENDPROC

#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	FAMILY SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
// PURPOSE:	Maintains Michael Family requests still outstanding
PROC Maintain_Michael_Family_Requests(FriendControllerVars &sVars, enumFamilyScenes eFamilyScene, STRING scriptname)
	
	IF IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_MICHAEL)
//	OR (IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_FRANKLIN) AND (g_eLastMissionPassed = SP_MISSION_MICHAEL_2))
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptname)) <= 0)
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sVars.vFamily_scene_coord[eFamilyScene]) < fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT
			
				REQUEST_SCRIPT(scriptname)
				
				WHILE NOT HAS_SCRIPT_LOADED(scriptname)
					WAIT(0)
				ENDWHILE
				
				structFamilyScene sFamilyScene
				
				sFamilyScene.sceneCoord	= sVars.vFamily_scene_coord[eFamilyScene]
				sFamilyScene.sceneHead	= sVars.fFamily_scene_head[eFamilyScene]
				
				START_NEW_SCRIPT_WITH_ARGS(scriptname,sFamilyScene,SIZE_OF(sFamilyScene), FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptname)
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// PURPOSE:	Maintains Franklin Family requests still outstanding
PROC Maintain_Franklin_Family_Requests(FriendControllerVars &sVars, enumFamilyScenes eFamilyScene, STRING scriptname)
	
	IF IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_FRANKLIN)
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptname)) <= 0)
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sVars.vFamily_scene_coord[eFamilyScene]) < fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT
			
				REQUEST_SCRIPT(scriptname)
				
				WHILE NOT HAS_SCRIPT_LOADED(scriptname)
					WAIT(0)
				ENDWHILE
				
				structFamilyScene sFamilyScene
				
				sFamilyScene.sceneCoord	= sVars.vFamily_scene_coord[eFamilyScene]
				sFamilyScene.sceneHead	= sVars.fFamily_scene_head[eFamilyScene]
				
				START_NEW_SCRIPT_WITH_ARGS(scriptname,sFamilyScene,SIZE_OF(sFamilyScene), FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptname)
				
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

// PURPOSE:	Maintains Trevor0 Family requests still outstanding
PROC Maintain_Trevor0_Family_Requests(FriendControllerVars &sVars, enumFamilyScenes eFamilyScene, STRING scriptname)
	
	IF IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_MICHAEL)
	OR IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_TREVOR)
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptname)) <= 0)
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sVars.vFamily_scene_coord[eFamilyScene]) < fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT
			
				REQUEST_SCRIPT(scriptname)
				
				WHILE NOT HAS_SCRIPT_LOADED(scriptname)
					WAIT(0)
				ENDWHILE
				
				structFamilyScene sFamilyScene
				
				sFamilyScene.sceneCoord	= sVars.vFamily_scene_coord[eFamilyScene]
				sFamilyScene.sceneHead	= sVars.fFamily_scene_head[eFamilyScene]
				
				START_NEW_SCRIPT_WITH_ARGS(scriptname,sFamilyScene,SIZE_OF(sFamilyScene), FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptname)
				
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

// PURPOSE:	Maintains Trevor1 Family requests still outstanding
PROC Maintain_Trevor1_Family_Requests(FriendControllerVars &sVars, enumFamilyScenes eFamilyScene, STRING scriptname)
	
	IF IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_TREVOR)
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptname)) <= 0)
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sVars.vFamily_scene_coord[eFamilyScene]) < fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT
			
				REQUEST_SCRIPT(scriptname)
				
				WHILE NOT HAS_SCRIPT_LOADED(scriptname)
					WAIT(0)
				ENDWHILE
				
				structFamilyScene sFamilyScene
				
				sFamilyScene.sceneCoord	= sVars.vFamily_scene_coord[eFamilyScene]
				sFamilyScene.sceneHead	= sVars.fFamily_scene_head[eFamilyScene]
				
				START_NEW_SCRIPT_WITH_ARGS(scriptname,sFamilyScene,SIZE_OF(sFamilyScene), FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptname)
				
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

// PURPOSE:	Maintains Trevor0 Family requests still outstanding
PROC Maintain_Armenian_Family_Requests(FriendControllerVars &sVars, enumFamilyScenes eFamilyScene, STRING scriptname)
	
	IF IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_FRANKLIN)
	OR IS_FAMILY_SCENE_ALLOWED_TO_RUN(CHAR_MICHAEL)
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptname)) <= 0)
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sVars.vFamily_scene_coord[eFamilyScene]) < fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT
			
				REQUEST_SCRIPT(scriptname)
				
				WHILE NOT HAS_SCRIPT_LOADED(scriptname)
					WAIT(0)
				ENDWHILE
				
				structFamilyScene sFamilyScene
				
				sFamilyScene.sceneCoord	= sVars.vFamily_scene_coord[eFamilyScene]
				sFamilyScene.sceneHead	= sVars.fFamily_scene_head[eFamilyScene]
				
				START_NEW_SCRIPT_WITH_ARGS(scriptname,sFamilyScene,SIZE_OF(sFamilyScene), FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptname)
				
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
PROC MAINTAIN_FAMILY_CONTROLLER(FriendControllerVars &sVars	#IF IS_DEBUG_BUILD , WIDGET_GROUP_ID	main_widget #ENDIF	)
	
	CONST_INT iCONST_FAMCONTROL_0_initialise			0
	CONST_INT iCONST_FAMCONTROL_1_checkForSafeTrigger	1
	CONST_INT iCONST_FAMCONTROL_2_maintainController	2
	CONST_INT iCONST_FAMCONTROL_3_waitForMission		3
	
	IF (g_eArm1PrestreamDenise = ARM1_PD_2_request)
		IF (VDIST2(sVars.vFamily_scene_coord[FS_FRANKLIN_0], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > (50*50))
			CPRINTLN(DEBUG_FAMILY, "g_eArm1PrestreamDenise = ARM1_PD_3_release")
			g_eArm1PrestreamDenise = ARM1_PD_3_release
		ENDIF
		
		IF IS_REPEAT_PLAY_ACTIVE()
			CPRINTLN(DEBUG_FAMILY, "g_eArm1PrestreamDenise = ARM1_PD_3_release")
			g_eArm1PrestreamDenise = ARM1_PD_3_release
		ENDIF
	ENDIF
	IF (g_eArm1PrestreamDenise = ARM1_PD_3_release)
	
		TEXT_LABEL_63 tFamilyAnimDict
		IF PRIVATE_Preload_FamilyMember_Anim(FE_F_AUNT_watching_TV, tFamilyAnimDict)
			REMOVE_ANIM_DICT(tFamilyAnimDict)
		ENDIF
		SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_DENISE)
		SET_SCRIPT_AS_NO_LONGER_NEEDED("family_scene_f0")

		CPRINTLN(DEBUG_FAMILY, "g_eArm1PrestreamDenise = ARM1_PD_0_null")
		g_eArm1PrestreamDenise = ARM1_PD_0_null
	ENDIF
	
	IF (g_eLam1PrestreamLamStretch = ARM1_PD_2_request)
		IF (VDIST2(sVars.vFamily_scene_coord[FS_FRANKLIN_0], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > (50*50))
			CPRINTLN(DEBUG_FAMILY, "g_eLam1PrestreamLamStretch = ARM1_PD_3_release")
			g_eLam1PrestreamLamStretch = ARM1_PD_3_release
		ENDIF
		
		IF IS_REPEAT_PLAY_ACTIVE()
			CPRINTLN(DEBUG_FAMILY, "g_eLam1PrestreamLamStretch = ARM1_PD_3_release")
			g_eLam1PrestreamLamStretch = ARM1_PD_3_release
		ENDIF
	ENDIF
	IF (g_eLam1PrestreamLamStretch = ARM1_PD_3_release)
		SET_SCRIPT_AS_NO_LONGER_NEEDED("family_scene_f0")

		CPRINTLN(DEBUG_FAMILY, "g_eLam1PrestreamLamStretch = ARM1_PD_0_null")
		g_eLam1PrestreamLamStretch = ARM1_PD_0_null
	ENDIF
	
	SWITCH sVars.iFamily_Controller_in_progress
		CASE iCONST_FAMCONTROL_0_initialise		//initialise family controller
		
			Initialise_Family_Controller_Variables(sVars)
			
			#IF IS_DEBUG_BUILD
			Create_Family_Controller_Widget(sVars, main_widget)
			
			IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS()
				CPRINTLN(DEBUG_FAMILY, "...family_controller.sch has activated g_bDrawDebugFamilyStuff = TRUE (command line param)")
				g_bDrawDebugFamilyStuff = TRUE
			ENDIF
			#ENDIF
			
			WAIT(0)
			SETTIMERB(0)
			
			sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_1_checkForSafeTrigger
		BREAK
		CASE iCONST_FAMCONTROL_1_checkForSafeTrigger		//maintain family controller
			IF TIMERB() > 0100
				SETTIMERB(0)
				
				IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_FAMILY_SCENE)
					sVars.eFamilyScenesToCheck = INT_TO_ENUM(enumFamilyScenes, 0)
					sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_2_maintainController
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			Watch_Family_Controller_Widget(sVars)
			Family_Controller_Debug_Options()
			Draw_Selected_Family_Controller_Debug(sVars)
			#ENDIF
		BREAK
		CASE iCONST_FAMCONTROL_2_maintainController		//maintain family controller
		
			IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_SWITCH)
				CPRINTLN(DEBUG_FAMILY, "family_controller cleaning up as game is no on mission.")
				sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_3_waitForMission
				EXIT
			ENDIF
			
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				CPRINTLN(DEBUG_FAMILY, "family_controller cleaning up as game is no on mission.")
				sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_3_waitForMission
				EXIT
			ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT g_flowUnsaved.bFlowControllerBusy
					SWITCH sVars.eFamilyScenesToCheck
						CASE FS_MICHAEL
							Maintain_Michael_Family_Requests(sVars, FS_MICHAEL,		"family_scene_m")
						BREAK
						CASE FS_FRANKLIN_0
							Maintain_Franklin_Family_Requests(sVars, FS_FRANKLIN_0,	"family_scene_f0")
						BREAK
						CASE FS_FRANKLIN_1
							Maintain_Franklin_Family_Requests(sVars, FS_FRANKLIN_1,	"family_scene_f1")
						BREAK
						CASE FS_TREVOR_0
							Maintain_Trevor0_Family_Requests(sVars, FS_TREVOR_0,	"family_scene_t0")
						BREAK
						CASE FS_TREVOR_1
							Maintain_Trevor1_Family_Requests(sVars, FS_TREVOR_1,	"family_scene_t1")
						BREAK
						
						DEFAULT
//							sVars.eFamilyScenesToCheck = INT_TO_ENUM(enumFamilyScenes, 0)
						BREAK
					ENDSWITCH
					
					sVars.eFamilyScenesToCheck = INT_TO_ENUM(enumFamilyScenes, ENUM_TO_INT(sVars.eFamilyScenesToCheck)+1)
					IF sVars.eFamilyScenesToCheck >= MAX_FAMILY_SCENES
						sVars.eFamilyScenesToCheck = INT_TO_ENUM(enumFamilyScenes, 0)
						sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_1_checkForSafeTrigger
					ENDIF
					
					#IF IS_DEBUG_BUILD
					Watch_Family_Controller_Widget(sVars)
					Family_Controller_Debug_Options()
					Draw_Selected_Family_Controller_Debug(sVars)
					#ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					DrawFamilyLiteralString("bFlowControllerBusy!", 0, HUD_COLOUR_RED)
				#ENDIF
				ENDIF
				
			ENDIF
			
			IF (g_eSceneBuddyEvents <> NO_FAMILY_EVENTS)
				IF NOT IS_ENTITY_DEAD(g_pScene_buddy)
					enumFamilyMember eFamilyMember
					eFamilyMember = GET_enumFamilyMember_from_ped(g_pScene_buddy)
					IF (eFamilyMember < MAX_FAMILY_MEMBER)
						IF (g_eCurrentFamilyEvent[eFamilyMember] = FAMILY_MEMBER_BUSY)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_pScene_buddy)
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_FAMILY, "set family_controller as owner of g_pScene_buddy ", Get_String_From_FamilyMember(eFamilyMember))
								#ENDIF
								
								SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(g_pScene_extra_buddy)
					enumFamilyMember eFamilyExtraMember
					eFamilyExtraMember = GET_enumFamilyMember_from_ped(g_pScene_extra_buddy)
					IF (eFamilyExtraMember <  MAX_FAMILY_MEMBER)
						IF (g_eCurrentFamilyEvent[eFamilyExtraMember] = FAMILY_MEMBER_BUSY)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_pScene_extra_buddy)
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_FAMILY, "set family_controller as owner of g_pScene_extra_buddy ", Get_String_From_FamilyMember(eFamilyExtraMember))
								#ENDIF
								
								SET_ENTITY_AS_MISSION_ENTITY(g_pScene_extra_buddy, TRUE, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE iCONST_FAMCONTROL_3_waitForMission
			
			BOOL bForceNextScenePostArm1
			bForceNextScenePostArm1 = FALSE
			
			IF NOT bForceNextScenePostArm1
				IF g_eArm1PrestreamDenise = ARM1_PD_1_mission_requested_prestream
				AND (VDIST2(sVars.vFamily_scene_coord[FS_FRANKLIN_0], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < (15*15))
					TEXT_LABEL_63 tFamilyAnimDict
					IF PRIVATE_Preload_FamilyMember_Anim(FE_F_AUNT_watching_TV, tFamilyAnimDict)
						REQUEST_ANIM_DICT(tFamilyAnimDict)
					ENDIF
					REQUEST_NPC_PED_MODEL(CHAR_DENISE)
					REQUEST_SCRIPT("family_scene_f0")
					
					CPRINTLN(DEBUG_FAMILY, "g_eArm1PrestreamDenise = ARM1_PD_2_request")
					g_eArm1PrestreamDenise = ARM1_PD_2_request
					bForceNextScenePostArm1 = TRUE
				ENDIF
				IF g_eLam1PrestreamLamStretch = ARM1_PD_1_mission_requested_prestream
				AND (VDIST2(sVars.vFamily_scene_coord[FS_FRANKLIN_0], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < (15*15))
					REQUEST_SCRIPT("family_scene_f0")
					
					CPRINTLN(DEBUG_FAMILY, "g_eLam1PrestreamLamStretch = ARM1_PD_2_request")
					g_eLam1PrestreamLamStretch = ARM1_PD_2_request
					bForceNextScenePostArm1 = TRUE
				ENDIF
				
				IF g_eArm1PrestreamDenise = ARM1_PD_2_request
				OR g_eLam1PrestreamLamStretch = ARM1_PD_2_request
					bForceNextScenePostArm1 = TRUE
				ENDIF
			ENDIF
			
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
				IF NOT bForceNextScenePostArm1
					CPRINTLN(DEBUG_FAMILY, "family_controller uncleaning up as game is no on mission.")
					sVars.eFamilyScenesToCheck = INT_TO_ENUM(enumFamilyScenes, 0)
					sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_1_checkForSafeTrigger
				ELSE
					CPRINTLN(DEBUG_FAMILY, "family_controller quicksetting up as game is no on mission.")
					Maintain_Franklin_Family_Requests(sVars, FS_FRANKLIN_0,	"family_scene_f0")
					
					sVars.eFamilyScenesToCheck = FS_FRANKLIN_0
					sVars.iFamily_Controller_in_progress = iCONST_FAMCONTROL_2_maintainController
				ENDIF
				
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
