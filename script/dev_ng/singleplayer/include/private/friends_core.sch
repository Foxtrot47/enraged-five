
USING "blip_control_public.sch"

// *******************************************************************************************
//	ENUM LABEL debug functions
// *******************************************************************************************

#IF IS_DEBUG_BUILD
	FUNC STRING GetLabel_enumFriend(enumFriend eFriend)
		SWITCH eFriend
			CASE FR_MICHAEL			RETURN "Michael"
			CASE FR_FRANKLIN		RETURN "Franklin"
			CASE FR_TREVOR			RETURN "Trevor"
			CASE FR_LAMAR			RETURN "Lamar"
			CASE FR_JIMMY			RETURN "Jimmy"
			CASE FR_AMANDA			RETURN "Amanda"
			CASE MAX_FRIENDS		RETURN "MAX_FRIENDS"
			CASE NO_FRIEND			RETURN "NO_FRIEND"
		ENDSWITCH
		RETURN "unknown friend string!?!"
	ENDFUNC

	FUNC STRING GetLabel_enumFriendConnection(enumFriendConnection eFriendConn)
		TEXT_LABEL_63 tFriendConn
		
		IF eFriendConn < MAX_FRIEND_CONNECTIONS
			enumFriend eFriendA, eFriendB
			eFriendA = g_SavedGlobals.sFriendsData.g_FriendConnectData[eFriendConn].friendA
			eFriendB = g_SavedGlobals.sFriendsData.g_FriendConnectData[eFriendConn].friendB
			
			tFriendConn = GetLabel_enumFriend(eFriendA)
			tFriendConn += " and "
			tFriendConn += GetLabel_enumFriend(eFriendB)
		
		ELIF eFriendConn = NO_FRIEND_CONNECTION
			tFriendConn = "NO_FRIEND_CONNECTION"
			
		ELIF eFriendConn = MAX_FRIEND_CONNECTIONS
			tFriendConn = "MAX_FRIEND_CONNECTIONS"
		
		ELSE
			tFriendConn = "<Invalid friend conneciton>"
			SCRIPT_ASSERT("GetLabel_enumFriendConnection() - invalid connection passed in")
		ENDIF		
		
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tFriendConn, GET_LENGTH_OF_LITERAL_STRING(tFriendConn))
	ENDFUNC
	
	FUNC STRING GetLabel_enumFriendGroup(enumFriendGroup eFriendGroup)
		TEXT_LABEL_31 tFriendGroup
		
		SWITCH eFriendGroup
			CASE FG_MICHAEL_FRANKLIN_TREVOR
				tFriendGroup = GetLabel_enumFriend(FR_MICHAEL)
				tFriendGroup += ", "
				tFriendGroup += GetLabel_enumFriend(FR_FRANKLIN)
				tFriendGroup += " and "
				tFriendGroup += GetLabel_enumFriend(FR_TREVOR)
			BREAK
			CASE FG_FRANKLIN_TREVOR_LAMAR
				tFriendGroup = GetLabel_enumFriend(FR_FRANKLIN)
				tFriendGroup += ", "
				tFriendGroup += GetLabel_enumFriend(FR_TREVOR)
				tFriendGroup += " and "
				tFriendGroup += GetLabel_enumFriend(FR_LAMAR)
			BREAK
			CASE NO_FRIEND_GROUP
				tFriendGroup = "No friend group"
			BREAK
			CASE MAX_FRIEND_GROUPS
				tFriendGroup = "Max friend groups"
			BREAK
		ENDSWITCH
		
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tFriendGroup, GET_LENGTH_OF_LITERAL_STRING(tFriendGroup))
	ENDFUNC
	
	FUNC STRING GetLabel_enumFriendBlockFlag(enumFriendBlockFlag flag)
		IF flag = FRIEND_BLOCK_FLAG_MISSION
			RETURN "MISSION"
		
		ELIF flag = FRIEND_BLOCK_FLAG_HIATUS
			RETURN "HIATUS"
		
		ELIF flag = FRIEND_BLOCK_FLAG_CLASH
			RETURN "CLASH"
		
		ENDIF
		
		RETURN "<unknown friend block flag>"
	ENDFUNC

	FUNC STRING GetLabel_FriendBlockBits(INT iBlockBits)
		TEXT_LABEL tBlockLabel = ""
		IF IS_BIT_SET(iBlockBits, ENUM_TO_INT(FRIEND_BLOCK_FLAG_MISSION))
			tBlockLabel += "M"
		ENDIF
		IF IS_BIT_SET(iBlockBits, ENUM_TO_INT(FRIEND_BLOCK_FLAG_HIATUS))
			tBlockLabel += "H"
		ENDIF
		IF IS_BIT_SET(iBlockBits, ENUM_TO_INT(FRIEND_BLOCK_FLAG_CLASH))
			tBlockLabel += "C"
		ENDIF
		IF iBlockBits = 0
			tBlockLabel = "/"
		ENDIF
		
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tBlockLabel, GET_LENGTH_OF_LITERAL_STRING(tBlockLabel))
	ENDFUNC
	
	FUNC STRING GetLabel_InvolvedFriends(SP_MISSIONS eMissionID)
		TEXT_LABEL_7 tMichael = ""
		TEXT_LABEL_7 tFranklin = ""
		TEXT_LABEL_7 tTrevor = ""
		TEXT_LABEL_7 tLamar = ""
		TEXT_LABEL_7 tJimmy = ""
		TEXT_LABEL_7 tAmanda = ""
		
		IF eMissionID <> SP_MISSION_NONE AND eMissionID <> SP_MISSION_MAX

			IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, BIT_MICHAEL)
				tMichael = "M"
			ENDIF
			IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, BIT_FRANKLIN)
				tFranklin = "F"
			ENDIF
			IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, BIT_TREVOR)
				tTrevor = "T"
			ENDIF
			IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, BIT_LAMAR)
				tLamar = "L"
			ENDIF
			IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, BIT_JIMMY)
				tJimmy = "J"
			ENDIF
			IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, BIT_AMANDA)
				tAmanda = "A"
			ENDIF
		
		ENDIF
		
		tMichael += tFranklin
		tMichael += tTrevor
		tMichael += tLamar
		tMichael += tJimmy
		tMichael += tAmanda
		
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tMichael, GET_LENGTH_OF_LITERAL_STRING(tMichael))
	ENDFUNC
	
	FUNC STRING GetLabel_enumCharacterList(enumCharacterList char)
		RETURN GET_PLAYER_PED_STRING(char)//GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET()
	ENDFUNC
	
	FUNC STRING GetLabel_SP_MISSIONS(SP_MISSIONS eMission)
		TEXT_LABEL_23 tMission = "<unknown>"
		
		IF eMission = SP_MISSION_NONE			tMission = "SP_MISSION_NONE"
		ELIF eMission = SP_MISSION_MAX			tMission = "SP_MISSION_MAX"
		ELIF eMission < SP_MISSION_MAX			tMission = g_sMissionStaticData[eMission].scriptName
		ENDIF
		
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tMission, GET_LENGTH_OF_LITERAL_STRING(tMission))
	ENDFUNC

	FUNC STRING GetLabel_FriendBits(INT bitfield)
		TEXT_LABEL_23 tBits
		
		IF bitfield = 0
			tBits = "No chars"
		ELSE
			IF IS_BITMASK_SET(bitfield, BIT_MICHAEL)	tBits += "M"		ENDIF
			IF IS_BITMASK_SET(bitfield, BIT_FRANKLIN)	tBits += "F"		ENDIF
			IF IS_BITMASK_SET(bitfield, BIT_TREVOR)		tBits += "T"		ENDIF
			IF IS_BITMASK_SET(bitfield, BIT_LAMAR)		tBits += "L"		ENDIF
			IF IS_BITMASK_SET(bitfield, BIT_JIMMY)		tBits += "J"		ENDIF
			IF IS_BITMASK_SET(bitfield, BIT_AMANDA)		tBits += "A"		ENDIF
		ENDIF
			
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tBits, GET_LENGTH_OF_LITERAL_STRING(tBits))
	ENDFUNC

	FUNC STRING GetLabel_enumFriendConnectionState(enumFriendConnectionState eFriendConnectionState)
		
		SWITCH eFriendConnectionState
			CASE FC_STATE_ContactWait			RETURN "ContactWait"				BREAK

			CASE FC_STATE_PhoneAccept			RETURN "PhoneAccept"				BREAK
			CASE FC_STATE_PhoneDecline			RETURN "PhoneDecline"				BREAK
			CASE FC_STATE_Init					RETURN "Init"						BREAK
			CASE FC_STATE_Active				RETURN "Active"						BREAK
			
			CASE MAX_FRIEND_STATE				RETURN "MAX_FRIEND_STATE"			BREAK
			CASE FC_STATE_Invalid				RETURN "invalid"					BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("invalid eFriendState in GetLabel_enumFriendConnectionState()")
		RETURN "invalid eFriendState in GetLabel_enumFriendConnectionState()"
	ENDFUNC

	FUNC STRING GetLabel_enumFriendConnectionMode(enumFriendConnectionMode eMode)
		
		SWITCH eMode
			CASE FC_MODE_Friend					RETURN "Friend"					BREAK
			CASE FC_MODE_Adhoc					RETURN "Adhoc"					BREAK
			CASE FC_MODE_Squad					RETURN "Squad"					BREAK
			
			CASE FC_MODE_Ambient				RETURN "Ambient"				BREAK
			CASE FC_MODE_ReplayGroup			RETURN "ReplayGroup"			BREAK
			CASE MAX_FRIEND_INIT_MODES			RETURN "MAX_FRIEND_INIT_MODES"	BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("invalid eInitMode in GetLabel_enumFriendConnectionMode()")
		RETURN "invalid eInitMode in GetLabel_enumFriendConnectionMode()"
	ENDFUNC
	
	FUNC STRING GetLabel_enumFriendMissionZoneState(enumFriendMissionZoneState eState)
		SWITCH eState
			CASE FRIEND_MISSION_ZONE_OFF		RETURN "Off"		BREAK
			CASE FRIEND_MISSION_ZONE_CALL		RETURN "Call"		BREAK
			CASE FRIEND_MISSION_ZONE_ON			RETURN "On"			BREAK
			CASE FRIEND_MISSION_ZONE_REJECT		RETURN "Reject"		BREAK
			CASE FRIEND_MISSION_ZONE_LAUNCHING	RETURN "Launching"	BREAK
			CASE FRIEND_MISSION_ZONE_LAUNCHED	RETURN "Launched"	BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("invalid eState in GetLabel_enumFriendMissionZoneState()")
		RETURN "invalid eState in GetLabel_enumFriendMissionZoneState()"
	ENDFUNC

	FUNC STRING GetLabel_enumFriendLocation(enumFriendLocation thisFriendLocation)
		
		SWITCH thisFriendLocation
			CASE FLOC_adhoc						RETURN "FLOC_adhoc"						BREAK
			
			CASE FLOC_michael_RH				RETURN "FLOC_michael_RH"				BREAK
			CASE FLOC_michael_CS				RETURN "FLOC_michael_CS"				BREAK
			CASE FLOC_franklin_SC				RETURN "FLOC_franklin_SC"				BREAK
			CASE FLOC_franklin_VH				RETURN "FLOC_franklin_VH"				BREAK
			CASE FLOC_trevor_CS					RETURN "FLOC_trevor_CS"					BREAK
			CASE FLOC_trevor_VB					RETURN "FLOC_trevor_VB"					BREAK
			CASE FLOC_trevor_SC					RETURN "FLOC_trevor_SC"					BREAK
			CASE FLOC_trevor_SCp				RETURN "FLOC_trevor_SCp"				BREAK
			CASE FLOC_lamar_SC					RETURN "FLOC_lamar_SC"					BREAK
			
			CASE FLOC_coffeeShop_RH				RETURN "FLOC_coffeeShop_RH"				BREAK
			CASE FLOC_shoppingPlaza_RH			RETURN "FLOC_shoppingPlaza_RH"			BREAK
			CASE FLOC_shoppingMall_RH			RETURN "FLOC_shoppingMall_RH"			BREAK
			CASE FLOC_minimartCarpark_RH		RETURN "FLOC_minimartCarpark_RH"		BREAK
			CASE FLOC_coffeeShop_DT				RETURN "FLOC_coffeeShop_DT"				BREAK
//			CASE FLOC_gasStation_DT				RETURN "FLOC_gasStation_DT"				BREAK
			CASE FLOC_minimartCarpark_DT		RETURN "FLOC_minimartCarpark_DT"		BREAK
			CASE FLOC_artPlaza_DT				RETURN "FLOC_artPlaza_DT"				BREAK
			CASE FLOC_bar_DT					RETURN "FLOC_bar_DT"					BREAK
			CASE FLOC_recCentre_SC				RETURN "FLOC_recCentre_SC"				BREAK
			CASE FLOC_shoppingPlaza_VB			RETURN "FLOC_shoppingPlaza_VB"			BREAK
//			CASE FLOC_beachFront_VB				RETURN "FLOC_beachFront_VB"				BREAK
			CASE FLOC_bar_VB					RETURN "FLOC_bar_VB"					BREAK
			CASE FLOC_minimartCarpark_MW		RETURN "FLOC_minimartCarpark_MW"		BREAK

			CASE FLOC_paletoMainSt_PA			RETURN "FLOC_paletoMainSt_PA"			BREAK
			CASE FLOC_minimartCarpark_SS		RETURN "FLOC_minimartCarpark_SS"		BREAK

			CASE MAX_FRIEND_LOCATIONS
				RETURN "MAX_FRIEND_LOCATIONS"
			BREAK
			CASE NO_FRIEND_LOCATION
				RETURN "NO_FRIEND_LOCATION"
			BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("GetLabel_enumFriendLocation() invalid thisFriendLocation")
		
		TEXT_LABEL str = "FL_"
		str += ENUM_TO_INT(thisFriendLocation)
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(str, GET_LENGTH_OF_LITERAL_STRING(str))
	ENDFUNC

	FUNC STRING GetLabel_enumActivityLocation(enumActivityLocation activityLoc)
		
		SWITCH activityLoc
			CASE ALOC_bar_bahamas				RETURN "ALOC_bar_bahamas"				BREAK
			CASE ALOC_bar_baybar				RETURN "ALOC_bar_baybar"				BREAK
			CASE ALOC_bar_biker					RETURN "ALOC_bar_biker"					BREAK
			CASE ALOC_bar_downtown				RETURN "ALOC_bar_downtown"				BREAK
			CASE ALOC_bar_himen					RETURN "ALOC_bar_himen"					BREAK
			CASE ALOC_bar_mojitos				RETURN "ALOC_bar_mojitos"				BREAK
			CASE ALOC_bar_singletons			RETURN "ALOC_bar_singletons"			BREAK
			
			CASE ALOC_cinema_vinewood			RETURN "ALOC_cinema_vinewood"			BREAK
			CASE ALOC_cinema_downtown			RETURN "ALOC_cinema_downtown"			BREAK
			CASE ALOC_cinema_morningwood		RETURN "ALOC_cinema_morningwood"		BREAK
			
			CASE ALOC_darts_hickBar				RETURN "ALOC_darts_hickBar"				BREAK
			
			CASE ALOC_golf_countryClub			RETURN "ALOC_golf_countryClub"			BREAK

			CASE ALOC_stripclub_southCentral	RETURN "ALOC_stripclub_southCentral"	BREAK
			
			CASE ALOC_tennis_beachCourt			RETURN "ALOC_tennis_beachCourt"			BREAK
			CASE ALOC_tennis_chumashHotel		RETURN "ALOC_tennis_chumashHotel"		BREAK
			CASE ALOC_tennis_LSUCourt1			RETURN "ALOC_tennis_LSUCourt1"			BREAK
			CASE ALOC_tennis_michaelHouse		RETURN "ALOC_tennis_michaelHouse"		BREAK
			CASE ALOC_tennis_richmanHotel1		RETURN "ALOC_tennis_richmanHotel1"		BREAK
			CASE ALOC_tennis_vespucciHotel		RETURN "ALOC_tennis_vespucciHotel"		BREAK
			CASE ALOC_tennis_vinewoodhotel1		RETURN "ALOC_tennis_vinewoodhotel1"		BREAK
			CASE ALOC_tennis_weazelCourt1		RETURN "ALOC_tennis_weazelCourt1"		BREAK

			CASE ALOC_suspendFriends					RETURN "ALOC_suspendFriends"					BREAK
			
			CASE MAX_ACTIVITY_LOCATIONS			RETURN "MAX_ACTIVITY_LOCATIONS"			BREAK
			CASE NO_ACTIVITY_LOCATION			RETURN "NO_ACTIVITY_LOCATION"			BREAK
		ENDSWITCH

		SCRIPT_ASSERT("GetLabel_enumActivityLocation() invalid activityLoc")
		
		TEXT_LABEL str = "LOC_"
		str += ENUM_TO_INT(activityLoc)
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(str, GET_LENGTH_OF_LITERAL_STRING(str))
	ENDFUNC

	FUNC STRING GetLabel_enumActivityType(enumActivityType activityType)
		
		SWITCH activityType
			CASE ATYPE_golf						RETURN "ATYPE_golf"						BREAK
			CASE ATYPE_tennis					RETURN "ATYPE_tennis"					BREAK
			CASE ATYPE_stripclub				RETURN "ATYPE_stripclub"				BREAK
			CASE ATYPE_darts					RETURN "ATYPE_darts"					BREAK
													
			CASE ATYPE_cinema					RETURN "ATYPE_cinema"					BREAK
			CASE ATYPE_bar						RETURN "ATYPE_bar"						BREAK
													
			CASE ATYPE_suspend					RETURN "ATYPE_suspend"					BREAK

			CASE MAX_ACTIVITY_TYPES				RETURN "MAX_ACTIVITY_TYPES"				BREAK
			CASE NO_ACTIVITY_TYPE				RETURN "NO_ACTIVITY_TYPE"				BREAK
		ENDSWITCH
			
		SCRIPT_ASSERT("GetLabel_enumActivityType() invalid activityType")
		
		TEXT_LABEL str = "AT_"
		str += ENUM_TO_INT(activityType)
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(str, GET_LENGTH_OF_LITERAL_STRING(str))
	ENDFUNC

#ENDIF

// *******************************************************************************************
//	FRIEND/CHAR/CONNECTION conversion functions
// *******************************************************************************************

/// PURPOSE:
///    Given a character ID (enumCharacterList) will return the corresponding friend ID (enumFriend)
/// PARAMS:
///    friendCharID - enumCharacterList
/// RETURNS:
///    enumFriend
FUNC enumFriend GET_FRIEND_FROM_CHARCLF(enumCharacterList friendCharID)
	
	IF friendCharID = NO_CHARACTER
		RETURN NO_FRIEND
	
	ELIF friendCharID < MAX_CLF_CHARACTERS 
		RETURN g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[friendCharID].friend
	
	ELSE
		
		IF (friendCharID = CHAR_CLF_BLANK_ENTRY)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (CHAR_BLANK_ENTRY)")
			RETURN NO_FRIEND
		
		ELIF (friendCharID = MAX_CLF_CHARACTERS)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (MAX_CHARACTERS)")
			RETURN MAX_FRIENDS

		ELIF (friendCharID = MAX_CLF_CHARACTERS_PLUS_DUMMY)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (MAX_CHARACTERS_PLUS_DUMMY)")
			RETURN MAX_FRIENDS
		
		ELSE
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (unknown ID)")
			RETURN MAX_FRIENDS
		ENDIF
		
	ENDIF
	
ENDFUNC
FUNC enumFriend GET_FRIEND_FROM_CHARNRM(enumCharacterList friendCharID)
	
	IF friendCharID = NO_CHARACTER
		RETURN NO_FRIEND
	
	ELIF friendCharID < MAX_NRM_CHARACTERS 
		RETURN g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[friendCharID].friend
	
	ELSE
		
		IF (friendCharID = CHAR_NRM_BLANK_ENTRY)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (CHAR_BLANK_ENTRY)")
			RETURN NO_FRIEND
		
		ELIF (friendCharID = MAX_NRM_CHARACTERS)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (MAX_CHARACTERS)")
			RETURN MAX_FRIENDS

		ELIF (friendCharID = MAX_NRM_CHARACTERS_PLUS_DUMMY)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (MAX_CHARACTERS_PLUS_DUMMY)")
			RETURN MAX_FRIENDS
		
		ELSE
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (unknown ID)")
			RETURN MAX_FRIENDS
		ENDIF
		
	ENDIF
	
ENDFUNC
FUNC enumFriend GET_FRIEND_FROM_CHAR(enumCharacterList friendCharID)
	
	#IF USE_CLF_DLC
		IF g_bLoadedClifford
			RETURN GET_FRIEND_FROM_CHARCLF(friendCharID)
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF g_bLoadedNorman
			RETURN GET_FRIEND_FROM_CHARNRM(friendCharID)
		ENDIF
	#ENDIF
	
	IF friendCharID = NO_CHARACTER
		RETURN NO_FRIEND
	
	ELIF friendCharID < GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()
		RETURN GLOBAL_CHARACTER_SHEET_GET_FRIEND(friendCharID)
	ELSE
		
		IF (friendCharID = CHAR_BLANK_ENTRY)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (CHAR_BLANK_ENTRY)")
			RETURN NO_FRIEND
		
		ELIF (friendCharID = GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (MAX_CHARACTERS)")
			RETURN MAX_FRIENDS

		ELIF (friendCharID = MAX_CHARACTERS_PLUS_DUMMY)
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (MAX_CHARACTERS_PLUS_DUMMY)")
			RETURN MAX_FRIENDS
		
		ELSE
			SCRIPT_ASSERT("GET_FRIEND_FROM_CHAR() - invalid friendCharID (unknown ID)")
			RETURN MAX_FRIENDS
		ENDIF
		
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Given a friend ID (enumFriend) will return the corresponding character ID (enumCharacterList)
/// PARAMS:
///    friendID - enumFriend
/// RETURNS:
///    enumCharacterList
FUNC enumCharacterList GET_CHAR_FROM_FRIEND(enumFriend friendID)
	SWITCH friendID
		CASE FR_MICHAEL
			RETURN CHAR_MICHAEL
		BREAK
		CASE FR_FRANKLIN
			RETURN CHAR_FRANKLIN
		BREAK
		CASE FR_TREVOR
			RETURN CHAR_TREVOR
		BREAK
		CASE FR_LAMAR
			RETURN CHAR_LAMAR
		BREAK
		CASE FR_JIMMY
			RETURN CHAR_JIMMY
		BREAK
		CASE FR_AMANDA
			RETURN CHAR_AMANDA
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("invalid friendID in GET_CHAR_FROM_FRIEND()")
	RETURN NO_CHARACTER
ENDFUNC

/// PURPOSE:
///    Get the friend who isn't currently the player, from the given FriendConnection
/// PARAMS:
///    thisFriendConnection - the connection to check
///    otherFriend - the firend who isn't the current player
/// RETURNS:
///    If neither friend is currently the player, returns false
FUNC BOOL GET_OTHER_FRIEND_FROM_CONNECTION(enumFriendConnection thisFriendConnection, enumFriend &otherFriend)
		
	enumFriend ePlayerFriend = GET_FRIEND_FROM_CHAR(g_eDefaultPlayerChar)
		
//	#IF IS_DEBUG_BUILD
//		TEXT_LABEL_63 tConnection = GetLabel_enumFriendConnection(thisFriendConnection)
//		CPRINTLN(DEBUG_FRIENDS, "GET_OTHER_FRIEND_FROM_CONNECTION(", tConnection, ")")
//	#ENDIF
	
	IF g_SavedGlobals.sFriendsData.g_FriendConnectData[thisFriendConnection].friendB = ePlayerFriend
		otherFriend = g_SavedGlobals.sFriendsData.g_FriendConnectData[thisFriendConnection].friendA
		RETURN TRUE
	ENDIF
	
	IF g_SavedGlobals.sFriendsData.g_FriendConnectData[thisFriendConnection].friendA = ePlayerFriend
		otherFriend = g_SavedGlobals.sFriendsData.g_FriendConnectData[thisFriendConnection].friendB
		RETURN TRUE
	ENDIF
	
	otherFriend = NO_FRIEND
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Given two friends, returns the FriendConnection that contains the data on their relationship
/// PARAMS:
///    eFriendA - 
///    eFriendB - 
/// RETURNS:
///    The FriendConnection
FUNC enumFriendConnection GET_CONNECTION_FROM_FRIENDS(enumFriend eFriendA, enumFriend eFriendB)
	
	IF ((eFriendA <> FR_MICHAEL) AND (eFriendA <> FR_FRANKLIN) AND (eFriendA <> FR_TREVOR))
		IF ((eFriendB = FR_MICHAEL) OR (eFriendB = FR_FRANKLIN) OR (eFriendB = FR_TREVOR))
			enumFriend eFriendBtoA = eFriendB
			eFriendB = eFriendA
			eFriendA = eFriendBtoA
		ENDIF
	ENDIF
	
	SWITCH eFriendA
		CASE FR_MICHAEL
			SWITCH eFriendB
				CASE FR_MICHAEL
					CPRINTLN(DEBUG_FRIENDS, "eFriendA and eFriendB cannot both be FR_MICHAEL GET_CONNECTION_FROM_FRIENDS()")
					SCRIPT_ASSERT("eFriendA and eFriendB cannot both be FR_MICHAEL GET_CONNECTION_FROM_FRIENDS()")
					RETURN NO_FRIEND_CONNECTION
				BREAK
				CASE FR_FRANKLIN
					RETURN FC_MICHAEL_FRANKLIN
				BREAK
				CASE FR_TREVOR
					RETURN FC_TREVOR_MICHAEL
				BREAK
				
				CASE FR_LAMAR
					RETURN NO_FRIEND_CONNECTION
				BREAK
				CASE FR_JIMMY
					RETURN FC_MICHAEL_JIMMY
				BREAK
				CASE FR_AMANDA
					RETURN FC_MICHAEL_AMANDA
				BREAK
				
				DEFAULT
					CPRINTLN(DEBUG_FRIENDS, "invalid eFriendB ", GetLabel_enumFriend(eFriendB), " in GET_CONNECTION_FROM_FRIENDS()")
					SCRIPT_ASSERT("invalid eFriendB in GET_CONNECTION_FROM_FRIENDS()")
					
					RETURN NO_FRIEND_CONNECTION
				BREAK
			ENDSWITCH
		BREAK
		CASE FR_FRANKLIN
			SWITCH eFriendB
				CASE FR_MICHAEL
					RETURN FC_MICHAEL_FRANKLIN
				BREAK
				CASE FR_FRANKLIN
					CPRINTLN(DEBUG_FRIENDS, "eFriendA and eFriendB cannot both be FR_FRANKLIN GET_CONNECTION_FROM_FRIENDS()")
					SCRIPT_ASSERT("eFriendA and eFriendB cannot both be FR_FRANKLIN GET_CONNECTION_FROM_FRIENDS()")
					RETURN NO_FRIEND_CONNECTION
				BREAK
				CASE FR_TREVOR
					RETURN FC_FRANKLIN_TREVOR
				BREAK
				
				CASE FR_LAMAR
					RETURN FC_FRANKLIN_LAMAR
				BREAK
				CASE FR_JIMMY
					RETURN FC_FRANKLIN_JIMMY
				BREAK
				CASE FR_AMANDA
					RETURN NO_FRIEND_CONNECTION
				BREAK
				
				DEFAULT
					CPRINTLN(DEBUG_FRIENDS, "invalid eFriendB ", GetLabel_enumFriend(eFriendB), " in GET_CONNECTION_FROM_FRIENDS()")
					SCRIPT_ASSERT("invalid eFriendB in GET_CONNECTION_FROM_FRIENDS()")

					RETURN NO_FRIEND_CONNECTION
				BREAK
			ENDSWITCH
		BREAK
		CASE FR_TREVOR
			SWITCH eFriendB
				CASE FR_MICHAEL
					RETURN FC_TREVOR_MICHAEL
				BREAK
				CASE FR_FRANKLIN
					RETURN FC_FRANKLIN_TREVOR
				BREAK
				CASE FR_TREVOR
					CPRINTLN(DEBUG_FRIENDS, "eFriendA and eFriendB cannot both be FR_TREVOR GET_CONNECTION_FROM_FRIENDS()")
					SCRIPT_ASSERT("eFriendA and eFriendB cannot both be FR_TREVOR GET_CONNECTION_FROM_FRIENDS()")
					RETURN NO_FRIEND_CONNECTION
				BREAK
				
				CASE FR_LAMAR
					RETURN FC_TREVOR_LAMAR
				BREAK
				CASE FR_JIMMY
					RETURN FC_TREVOR_JIMMY
				BREAK
				CASE FR_AMANDA
					RETURN NO_FRIEND_CONNECTION
				BREAK
				
				DEFAULT
					CPRINTLN(DEBUG_FRIENDS, "invalid eFriendB ", GetLabel_enumFriend(eFriendB), " in GET_CONNECTION_FROM_FRIENDS()")
					SCRIPT_ASSERT("invalid eFriendB in GET_CONNECTION_FROM_FRIENDS()")

					RETURN NO_FRIEND_CONNECTION
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	CPRINTLN(DEBUG_FRIENDS, "invalid eFriendA ", GetLabel_enumFriend(eFriendA), " in GET_CONNECTION_FROM_FRIENDS()")
	SCRIPT_ASSERT("invalid eFriendA in GET_CONNECTION_FROM_FRIENDS()")
	
	RETURN NO_FRIEND_CONNECTION
ENDFUNC

/// PURPOSE:
///    Gets friend IDs for the two friends involved in the friend-connection
/// PARAMS:
///    eConn - The friend-connection
///    friendA - Return value for friend A
///    friendB - Return value for friend B
/// RETURNS:
///    TRUE if the friend connection was valid
FUNC BOOL GET_FRIENDS_FROM_CONNECTION(enumFriendConnection eConn, enumFriend &friendA, enumFriend &friendB)
	
	IF eConn <> MAX_FRIEND_CONNECTIONS
	AND eConn <> NO_FRIEND_CONNECTION
		friendA = g_SavedGlobals.sFriendsData.g_FriendConnectData[eConn].friendA
		friendB = g_SavedGlobals.sFriendsData.g_FriendConnectData[eConn].friendB
		RETURN TRUE
	ENDIF

	CPRINTLN(DEBUG_FRIENDS, "GET_FRIENDS_FROM_CONNECTION(", GetLabel_enumFriendConnection(eConn), ") - Invalid connection")
	SCRIPT_ASSERT("GET_FRIENDS_FROM_CONNECTION() - Invalid connection - see friends TTY")
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Gets char IDs for the two friends involved in the friend-connection
/// PARAMS:
///    eConn - The friend-connection
///    friendA - Return value for friend A's char ID
///    friendB - Return value for friend B's char ID
/// RETURNS:
///    TRUE if the friend connection was valid
FUNC BOOL GET_FRIEND_CHARS_FROM_CONNECTION(enumFriendConnection eConn, enumCharacterList &friendCharA, enumCharacterList &friendCharB)
	
	IF eConn <> MAX_FRIEND_CONNECTIONS
	AND eConn <> NO_FRIEND_CONNECTION
		friendCharA = GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConn].friendA)
		friendCharB = GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConn].friendB)
		RETURN TRUE
	ENDIF

	CPRINTLN(DEBUG_FRIENDS, "GET_FRIEND_CHARS_FROM_CONNECTION(", GetLabel_enumFriendConnection(eConn), ") - Invalid connection")
	SCRIPT_ASSERT("GET_FRIEND_CHARS_FROM_CONNECTION() - Invalid connection - see friends TTY")
	
	RETURN FALSE

ENDFUNC

FUNC MODEL_NAMES GET_FRIEND_MODEL(enumFriend eFriend)
	RETURN g_SavedGlobals.sFriendsData.g_FriendData[eFriend].charSheet.game_model
ENDFUNC

/// PURPOSE:
///    Given three friends, returns the FriendGroup that contains the data on their relationship
/// PARAMS:
///    eFriendA - 
///    eFriendB - 
///    eFriendC - 
/// RETURNS:
///    The FriendGroup
FUNC enumFriendGroup GET_GROUP_FROM_FRIENDS(enumFriend eFriendA, enumFriend eFriendB, enumFriend eFriendC)
	
	IF eFriendA = eFriendB
	OR eFriendB = eFriendC
	OR eFriendC = eFriendA
		CPRINTLN(DEBUG_FRIENDS, "GET_GROUP_FROM_FRIENDS() - Friends cannot be the same")
		SCRIPT_ASSERT("GET_GROUP_FROM_FRIENDS() - Friends cannot be the same")
		RETURN NO_FRIEND_GROUP
	ENDIF
	
	IF	((eFriendA = FR_FRANKLIN) OR (eFriendB = FR_FRANKLIN) OR (eFriendC = FR_FRANKLIN))
	AND ((eFriendA = FR_TREVOR) OR (eFriendB = FR_TREVOR) OR (eFriendC = FR_TREVOR))
	
		IF eFriendA = FR_MICHAEL
		OR eFriendB = FR_MICHAEL
		OR eFriendC = FR_MICHAEL
			RETURN FG_MICHAEL_FRANKLIN_TREVOR
		ENDIF
	
		IF eFriendA = FR_LAMAR
		OR eFriendB = FR_LAMAR
		OR eFriendC = FR_LAMAR
			RETURN FG_FRANKLIN_TREVOR_LAMAR
		ENDIF
	
	ENDIF
		
	RETURN NO_FRIEND_GROUP
ENDFUNC


//-------------------------------------------------------------------------------------------------------------------------------------------
//-- Friend connection chat data
//-------------------------------------------------------------------------------------------------------------------------------------------

CONST_INT FRIEND_0xF	15
CONST_INT FRIEND_0xFF	255

PROC Private_SetFriendChatBankData(	INT& iBankData,
									INT iFullCount, INT iMiniCount, INT iDrunkCount,
									INT iFullTotal, INT iMiniTotal, INT iDrunkTotal,
									enumFriendChatCondition eCondition)

	iBankData = 0
	
	iBankData = iBankData | SHIFT_LEFT(iFullCount & FRIEND_0xF, 28)
	iBankData = iBankData | SHIFT_LEFT(iMiniCount & FRIEND_0xF, 24)
	iBankData = iBankData | SHIFT_LEFT(iDrunkCount & FRIEND_0xF, 20)

	iBankData = iBankData | SHIFT_LEFT(iFullTotal & FRIEND_0xF, 16)
	iBankData = iBankData | SHIFT_LEFT(iMiniTotal & FRIEND_0xF, 12)
	iBankData = iBankData | SHIFT_LEFT(iDrunkTotal & FRIEND_0xF, 8)
	
	iBankData = iBankData | (ENUM_TO_INT(eCondition) & FRIEND_0xFF)

ENDPROC

PROC Private_GetFriendChatBankCounts(INT iBankData, INT& iFullCount, INT& iMiniCount, INT& iDrunkCount)
	iFullCount	= SHIFT_RIGHT(iBankData, 28) & FRIEND_0xF
	iMiniCount	= SHIFT_RIGHT(iBankData, 24) & FRIEND_0xF
	iDrunkCount	= SHIFT_RIGHT(iBankData, 20) & FRIEND_0xF
ENDPROC

PROC Private_GetFriendChatBankTotals(INT iBankData, INT& iFullTotal, INT& iMiniTotal, INT& iDrunkTotal)
	iFullTotal	= SHIFT_RIGHT(iBankData, 16) & FRIEND_0xF
	iMiniTotal	= SHIFT_RIGHT(iBankData, 12) & FRIEND_0xF
	iDrunkTotal	= SHIFT_RIGHT(iBankData, 8) & FRIEND_0xF
ENDPROC

PROC Private_GetFriendChatBankCondition(INT iBankData, enumFriendChatCondition& eCondition)
	eCondition = INT_TO_ENUM(enumFriendChatCondition, iBankData & FRIEND_0xFF)
ENDPROC

PROC Private_InitFriendChatData(structFriendChatData& chatData)

	Private_SetFriendChatBankData(chatData.banks[0],	0, 0, 0,	0, 0, 0,	FCHAT_ConditionNever)
	Private_SetFriendChatBankData(chatData.banks[1],	0, 0, 0,	0, 0, 0,	FCHAT_ConditionNever)
	Private_SetFriendChatBankData(chatData.banks[2],	0, 0, 0,	0, 0, 0,	FCHAT_ConditionNever)
	Private_SetFriendChatBankData(chatData.banks[3],	0, 0, 0,	0, 0, 0,	FCHAT_ConditionNever)
	Private_SetFriendChatBankData(chatData.banks[4],	0, 0, 0,	0, 0, 0,	FCHAT_ConditionNever)
	Private_SetFriendChatBankData(chatData.banks[5],	0, 0, 0,	0, 0, 0,	FCHAT_ConditionNever)

ENDPROC

