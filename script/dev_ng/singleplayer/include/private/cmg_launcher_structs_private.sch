USING "cmg_structs_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	CLIFFORD MINIGAME FILE: cmg_launcher_structs_private.sch
//	Sam Hackett
//
//	This is included by all the cmg_launcher private scripts
//
//	It contains the type definitions used by the launcher, and by the looping ambient scenes the launcher can run.
//
//	See also:
//		cmg_launcher_main_private.sch
//		cmg_launcher_scenes_private.sch
//		cmg_launcher_public.sch
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     OVERRIDES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Trigger count overrides - You can define USERDEF consts, to override the default array sizes

#IF DEFINED(CMGL_USERDEF_TRIGGER_MAX)
	CONST_INT CMGL_TRIGGER_MAX					CMGL_USERDEF_TRIGGER_MAX			// Define CMGL_USERDEF_TRIGGER_MAX to override the maximum triggers (default = 2)
#ENDIF
#IF NOT DEFINED(CMGL_USERDEF_TRIGGER_MAX)
	CONST_INT CMGL_TRIGGER_MAX					2
#ENDIF

#IF DEFINED(CMGL_USERDEF_BJACK_TRIGGER_MAX)
	CONST_INT CMGL_BJACK_TRIGGER_MAX			CMGL_USERDEF_BJACK_TRIGGER_MAX		// Define CMGL_USERDEF_BJACK_TRIGGER_MAX to override the maximum blackjack triggers (default = 1)
#ENDIF
#IF NOT DEFINED(CMGL_USERDEF_BJACK_TRIGGER_MAX)
	CONST_INT CMGL_BJACK_TRIGGER_MAX			1
#ENDIF

#IF DEFINED(CMGL_USERDEF_POKER_TRIGGER_MAX)
	CONST_INT CMGL_POKER_TRIGGER_MAX			CMGL_USERDEF_POKER_TRIGGER_MAX		// Define CMGL_USERDEF_POKER_TRIGGER_MAX to override the maximum poker triggers (default = 1)
#ENDIF
#IF NOT DEFINED(CMGL_USERDEF_POKER_TRIGGER_MAX)
	CONST_INT CMGL_POKER_TRIGGER_MAX			1
#ENDIF

#IF DEFINED(CMGL_USERDEF_ROULE_TRIGGER_MAX)
	CONST_INT CMGL_ROULE_TRIGGER_MAX			CMGL_USERDEF_ROULE_TRIGGER_MAX		// Define CMGL_USERDEF_POKER_TRIGGER_MAX to override the maximum roulette triggers (default = 0)
#ENDIF
#IF NOT DEFINED(CMGL_USERDEF_ROULE_TRIGGER_MAX)
	CONST_INT CMGL_ROULE_TRIGGER_MAX			0
#ENDIF


// Mode overrides - You can define USERDEF consts, to override the default mode settings

#IF DEFINED(CMGL_USERDEF_IS_MULTIPLAYER)
	CONST_INT CMGL_IS_MULTIPLAYER				CMGL_USERDEF_IS_MULTIPLAYER			// Define CMGL_USERDEF_IS_MULTIPLAYER to override if launcher script is MP (default = TRUE)
#ENDIF
#IF NOT DEFINED(CMGL_USERDEF_IS_MULTIPLAYER)
	CONST_INT CMGL_IS_MULTIPLAYER				1
#ENDIF

#IF DEFINED(CMGL_USERDEF_IS_APARTMENT)
	CONST_INT CMGL_IS_APARTMENT					CMGL_USERDEF_IS_APARTMENT			// Define CMGL_USERDEF_IS_MULTIPLAYER to override if launcher script is the apartment (default = TRUE)
#ENDIF
#IF NOT DEFINED(CMGL_USERDEF_IS_APARTMENT)
	CONST_INT CMGL_IS_APARTMENT					1
#ENDIF


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     CONSTANTS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CONST_INT CMGL_SCRIPT_INSTANCE_ID_MAX		127	// This is the max unique multiplayer script instance IDs that MP will accept.

CONST_INT CMGL_APARTMENT_ID_MAX				65	// This is the max unique apartment instances this system can pack.		- If this increases, we may need to ask for a greater range of script instance IDs that can be passed to a launching MP script (currently 7-bits) - talk to John Gurney or Daniel Yelland if this is needed.
CONST_INT CMGL_GAME_ID_MAX					62	// This is the max unique instances each type of game can have			- Eg: a launcher can have this many blackjack triggers. The value is calculated by (CMGL_SCRIPT_INSTANCE_ID_MAX - CMGL_APARTMENT_ID_MAX)

CONST_INT CMGL_ENABLE_DEBUG_OUTPUT			0


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     SCENE (BJACK)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CONST_INT CMGL_BJACK_SCENE_MAX_PLAYERS 4

STRUCT CMGL_BJACK_SCENE
	PED_INDEX						hDealer
	PED_INDEX						hPlayers[CMGL_BJACK_SCENE_MAX_PLAYERS]
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     SCENE (POKER)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CONST_INT CMGL_POKER_SCENE_MAX_PLAYERS 5

STRUCT CMGL_POKER_SCENE
	PED_INDEX						hDealer
	PED_INDEX						hPlayers[CMGL_POKER_SCENE_MAX_PLAYERS]
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     SCENE (DEALER)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


STRUCT CMGL_DEALER_SCENE
	NETWORK_INDEX					hDealer
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     LOCAL
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


STRUCT CMGL_TRIGGER
	CMG_TYPE					eGameType
	INT							iGameID
	VECTOR						vPos
	FLOAT						fBuyIn
ENDSTRUCT

ENUM CMGL_TRIGGER_BLOCK_CONDITION
	CMGL_TRIGGER_BLOCK_CONDITION_None = 0,
	CMGL_TRIGGER_BLOCK_CONDITION_TotalBlock,
	CMGL_TRIGGER_BLOCK_CONDITION_WantedLevel,
	CMGL_TRIGGER_BLOCK_CONDITION_Money	
ENDENUM

ENUM CMGL_LOCAL_STATE
	CMGL_LOCAL_STATE_Init = 0,
	CMGL_LOCAL_STATE_TempCreateTables,
	CMGL_LOCAL_STATE_Idle,
	CMGL_LOCAL_STATE_Offer,
	CMGL_LOCAL_STATE_RequestMPSeat,
	CMGL_LOCAL_STATE_ScriptLaunch,
	CMGL_LOCAL_STATE_ScriptRun,
	CMGL_LOCAL_STATE_ReleaseMPSeat
ENDENUM

CONST_INT CMGL_FLAG_IsMultiplayer	8												// To set in CMGL_LOCAL.iFlags
CONST_INT CMGL_FLAG_IsApartment		9												// To set in CMGL_LOCAL.iFlags

STRUCT CMGL_LOCAL

	CMGL_LOCAL_STATE			eState
	INT							iFlags

	CMGL_TRIGGER				mTriggers[CMGL_TRIGGER_MAX]
	INT							iTriggerID
	INT							hContextPrompt
	THREADID					hScriptThread
	
	// Scenes (SP-mode only)
//	#IF NOT CMGL_IS_MULTIPLAYER
//		
//		#IF CMGL_BJACK_TRIGGER_MAX
//			CMGL_BJACK_SCENE			mBjackScenes[CMGL_BJACK_TRIGGER_MAX]
//		#ENDIF
//		
//		#IF CMGL_POKER_TRIGGER_MAX
//			CMGL_POKER_SCENE			mPokerScenes[CMGL_POKER_TRIGGER_MAX]
//		#ENDIF
//		
//	#ENDIF
	
	// Used for testing, these will be placed by artists eventually
	OBJECT_INDEX				hTempTables[3]
	PED_INDEX					hTempPeds[15]

ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     SERVER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


STRUCT CMGL_SERVER

	INT							iClientSeatReservationCount[CMGL_TRIGGER_MAX]		// Int:		 Counts reservations
	INT							iClientSeatReservations[CMGL_TRIGGER_MAX]			// Bitfield: Use iClientID as bit index
	INT							iClientSeatRequestResponses							// Bitfield: Use iClientID as bit index
	INT							iClientSeatReleaseResponses							// Bitfield: Use iClientID as bit index
	INT							iConnectedClientBits								// Bitfield: Use iClientID as bit index
	
	// Scenes (MP-mode only)
//	#IF CMGL_IS_MULTIPLAYER
//		
//		#IF CMGL_BJACK_TRIGGER_MAX
//			CMGL_DEALER_SCENE			mBjackScenes[CMGL_BJACK_TRIGGER_MAX]
//		#ENDIF
//		
//		#IF CMGL_POKER_TRIGGER_MAX
//			CMGL_DEALER_SCENE			mPokerScenes[CMGL_POKER_TRIGGER_MAX]
//		#ENDIF
//		
//	#ENDIF
	
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                     CLIENT
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CONST_INT CMGL_CLIENT_BIT_RequestSeat	8											// To set in CMGL_CLIENT::iPackedRequestData
CONST_INT CMGL_CLIENT_BIT_ReleaseSeat	9

STRUCT CMGL_CLIENT
	INT							iPackedRequestData									// 0-7:  Int:		Trigger index
ENDSTRUCT																			// 8-31: Bitfield:	Use CMGL_CLIENT_BIT_xxx as bit index


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

