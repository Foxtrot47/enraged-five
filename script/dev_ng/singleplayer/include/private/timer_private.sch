USING "globals.sch"
USING "script_maths.sch"
USING "timer_globals.sch"
USING "commands_network.sch"

ENUM TIMER_BITS
	TIMER_STARTED 			= BIT0,
	TIMER_PAUSED 			= BIT1,
	TIMER_FORCE_GAME_TIMER	= BIT2 // Forces the timer to use GET_GAME_TIMER(), useful when you're online and don't want GET_NETWORK_TIME()
ENDENUM

/// PURPOSE:
///    Returns the current game timer in seconds as a verbose float.
/// RETURNS:
///    Verbose float of the current game timer in seconds
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC FLOAT GetGameTimerSeconds(BOOL bForceGameTimer = FALSE)
	IF bForceGameTimer
		PRINTLN("[TimerBroken] GET_GAME_TIMER ", GET_GAME_TIMER())
		
		FLOAT fTimer = TO_FLOAT(GET_GAME_TIMER())
		
		PRINTLN("[TimerBroken] fTimer ", fTimer)
		
		FLOAT fResult = fTimer / 1000.0
		
		PRINTLN("[TimerBroken] fResult ", fResult)
		
		RETURN fResult
	ENDIF
	//IF g_bInMultiplayer
	IF NETWORK_IS_GAME_IN_PROGRESS() //B* 324544 - SiM - 1/13/2012
		INT iTimer
		//GET_NETWORK_TIMER(iTimer)
		iTimer = NATIVE_TO_INT(GET_NETWORK_TIME())
		
		PRINTLN("[TimerBroken] iTimer ", iTimer)
		
		FLOAT fTimer = TO_FLOAT(iTimer)
		
		PRINTLN("[TimerBroken] fTimer ", fTimer)
		
		FLOAT fReturn = fTimer / 1000.0
		
		PRINTLN("[TimerBroken] fReturn ", fReturn)
		
		RETURN fReturn
	ENDIF
	RETURN (TO_FLOAT(GET_GAME_TIMER()) / 1000.0)
ENDFUNC
