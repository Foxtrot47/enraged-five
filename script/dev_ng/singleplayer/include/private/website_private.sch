USING "rage_builtins.sch"
USING "globals.sch"
USING "cellphone_public.sch"
USING "commands_script.sch"
USING "commands_misc.sch"
USING "commands_graphics.sch"
USING "commands_pad.sch"
USING "finance_control_public.sch"
USING "flow_public_core.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "randomChar_Public.sch"
#endif
#endif
USING "website_public.sch"
USING "website_lfi.sch"
USING "mission_repeat_data.sch"
USING "NET_realty_new.sch"

USING "net_simple_interior_bunker.sch"
USING "net_simple_interior_hangar.sch"
USING "net_simple_interior_defunct_base.sch"
USING "net_simple_interior_business_hub.sch"
USING "net_simple_interior_arena_garage.sch"
USING "net_simple_interior_arcade.sch"
USING "net_simple_interior_auto_shop.sch"
USING "net_simple_interior_fixer_HQ.sch"
#IF FEATURE_DLC_2_2022
USING "net_simple_interior_multistorey_garage.sch"
#ENDIF

PROC TRIGGER_BROWSER_NAG_SCREEN(INT itemHash, INT itemPrice, NAG_SCREEN_REASON nsr = NSR_CASH)

	IF NOT NETWORK_IS_ACTIVITY_SESSION()	//1596812
	AND IS_STORE_AVAILABLE_TO_USER()		//1605697
		
		CPRINTLN(DEBUG_INTERNET, "TRIGGER_BROWSER_NAG_SCREEN(", itemHash, ", $", itemPrice,
				PICK_STRING(nsr = NSR_CASH, ", NSR_CASH",
				PICK_STRING(nsr = NSR_DLC, ", NSR_DLC",
				PICK_STRING(nsr = NSR_VOUCHER, ", NSR_VOUCHER", ""))),
				")")
		
		g_iBrowserGoToStoreItemHash = itemHash
		g_iBrowserGoToStoreItemPrice = itemPrice
		
		g_bBrowserGoToStoreTrigger = TRUE
		g_eBrosNagReason = nsr
	ENDIF
ENDPROC



/// PURPOSE: used by GET_EYEFIND_NEWS_STORY_ORGANISATION to set the news organisation to match scaleform
CONST_INT NEWS_ORG_DAILY_GLOBE				0
CONST_INT NEWS_ORG_DAILY_RAG  				1
CONST_INT NEWS_ORG_LIBERTY_TREE  			2
CONST_INT NEWS_ORG_LOS_SANTOS_METEOR  		3
CONST_INT NEWS_ORG_LOS_SANTOS_SHEPERD  		4
CONST_INT NEWS_ORG_LS24  					5
CONST_INT NEWS_ORG_LUDENDORFF_WEEKLY  		6
CONST_INT NEWS_ORG_PUBLIC_LIBERTY_ONLINE 	7
CONST_INT NEWS_ORG_PRATTLE  				8
CONST_INT NEWS_ORG_SENORA_BEACON  			9
CONST_INT NEWS_ORG_STARSTALK  				10
CONST_INT NEWS_ORG_TALKIN_BALLS  			11
CONST_INT NEWS_ORG_WEAZEL_NEWS  			12
CONST_INT NEWS_ORG_WNKA_INTERNATIONAL  		13 


FUNC BOOL IS_PRICE_FILTER_ACTIVE(BAWSAQ_COMPANIES b)
	INT i = 0
	IF g_savedGlobals.sFinanceData.iFiltersRegistered > 0
	
		IF NOT (g_savedGlobals.sFinanceData.iFiltersRegistered < MAX_SP_FINANCE_FILTERS)
			g_savedGlobals.sFinanceData.iFiltersRegistered  = MAX_SP_FINANCE_FILTERS - 1
		ENDIF
		CPRINTLN(DEBUG_INTERNET, "IS_PRICE_FILTER_ACTIVE: ", g_savedGlobals.sFinanceData.iFiltersRegistered)
		REPEAT g_savedGlobals.sFinanceData.iFiltersRegistered i
			IF g_savedGlobals.sFinanceData.FilteredBind[i] = b
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		IF g_savedGlobals.sFinanceData.iFiltersRegistered < 0
			CPRINTLN(DEBUG_INTERNET, "IS_PRICE_FILTER_ACTIVE: correct price filters to zero from ", g_savedGlobals.sFinanceData.iFiltersRegistered)
			g_savedGlobals.sFinanceData.iFiltersRegistered = 0
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL FLOW_BS_NEWS_STORY_OVERRIDE(SCALEFORM_INDEX pagemov,BOOL bPrimary)

	TEXT_LABEL doStory = ""
	INT decider = GET_RANDOM_INT_IN_RANGE(1,5)
	IF decider = 5
		RETURN FALSE
	ENDIF

	WHILE TRUE
	SWITCH decider
		CASE 1
				//	{---------AfterHitcher1---------}
				IF IS_PRICE_FILTER_ACTIVE(BS_CO_TNK)
					IF bPrimary
				//	[TNK_P_USR_H]
				//	Tinkle dials it up with an unexpected spike in share price.
						doStory = "TNK_P_USR_H"
					ELSE
						RETURN FALSE
					ENDIF	
				ENDIF
			BREAK				
		CASE 2
				IF bPrimary
			//	{---------AfterJewelHeist---------}
			//	[VAG_P_USF_H]
			//	Portola Drive jewelry store robbery sees Vangelico share price take a hit.
					IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_2)
						IF IS_PRICE_FILTER_ACTIVE(BS_CO_VAG)
							doStory = "VAG_P_USF_H"
						ENDIF
					ENDIF
				
				ELSE
					RETURN FALSE
				ENDIF	

			BREAK
		CASE 3 // assassination checks
		
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_5) 
				
					IF IS_PRICE_FILTER_ACTIVE(BS_CO_GOL)
					//	{---------AfterAssassin5---------}
						IF bPrimary
					//	[GOL_P_USR_H]
					//	Gold Coast Development stock soars on the news that rival real estate developer Enzo Bonelli has been murdered
							doStory = "GOL_P_USR_H"
						ELSE
							RETURN FALSE
						ENDIF
					ENDIF
				ELIF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_4)
					//	{---------AfterAssassin4---------}
						IF IS_PRICE_FILTER_ACTIVE(BS_CO_VAP)
						IF bPrimary
					//	[VAP_P_USF_H]
					//	Vapid Motor Company share price crashes as leveraged buyout deal falls apart
							doStory = "VAP_P_USF_H"
						ELSE
							RETURN FALSE
						ENDIF
						ENDIF
					
				ELIF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_3)
					//	{---------AfterAssassination3---------}
					
						IF bPrimary
					//	[FAC_P_USF_H]
					//	Facade stock left floundering in the wake of Jackson Skinner's murder.
							doStory = "FAC_P_USF_H"
						ELSE
					//	[FRT_P_USR_H]
					//	Fruit share price on the rise as rival Facade's Head of Product Development is found dead.
							doStory = "FRT_P_USR_H"
						ENDIF
				ELIF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_2)
					//	{---------AfterAssassination2---------}
					//[RWC_P_USF_H]
					//Redwood Cigarettes share price coughing and spluttering in the midst of jury rigging scandal

					//[DEB_P_USR_H]
					//Debonaire Cigarettes stock on fire as Redwood struggles to weather a PR shitstorm over class action lawsuit. 
					IF bPrimary
						doStory = "RWC_P_USF_H"
					ELSE
						doStory = "DEB_P_USR_H"
					ENDIF
				ELIF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_1)
					IF bPrimary
				//	{---------AfterAssassination1---------}
				//	[BET_P_USR_H]
				//	Mollis and Betta Pharmaceuticals on a strong rise as Bilkinton struggles to stay firm.

						doStory = "BET_P_USR_H"
					ELSE
				//	[BIL_P_USF_H]
				//	Bilkinton Research stock flops on the death of CEO, Bret Lowrey.  
						doStory = "BIL_P_USF_H"
					ENDIF
				ENDIF
			BREAK
		CASE 4 //lester mission checks
		
		
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_3)
					IF bPrimary
				//	[LFI_P_USR_H]
				//	Lifeinvader share price finally starting to show signs of recovery.
						doStory = "LFI_P_USR_H"
					ELSE
						RETURN FALSE
					ENDIF
				
				ELIF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1)
					IF bPrimary
				//	[LFI_P_USF_H]
				//	Lifeinvader share price trading down on the news of Jay Norris' death.
						doStory = "LFI_P_USF_H"
					ELSE
						RETURN FALSE
					ENDIF
				
				
				ENDIF
			BREAK
			
		DEFAULT
			CPRINTLN(DEBUG_INTERNET, "FLOW_BS_NEWS_STORY_OVERRIDE: No existing story found, bailing")
			RETURN FALSE
			
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(doStory)
		++decider
	ELSE
		IF DOES_TEXT_LABEL_EXIST(doStory)
			//hooray, valid story
			decider = 5
		ELSE
			++decider
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "FLOW_BS_NEWS_STORY_OVERRIDE: story is '", doStory,"' with decider at ", decider)
	ENDWHILE



	IF NOT IS_STRING_NULL_OR_EMPTY(doStory)
		IF DOES_TEXT_LABEL_EXIST(doStory)
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				IF bPrimary
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
				ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
				ENDIF
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(doStory)
			END_SCALEFORM_MOVIE_METHOD()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC







#IF IS_DEBUG_BUILD	
	// used to test Eye Find News Stories in appInternet.sc Rag Widget
	// in here as they are used by funcs in this file
	INT iDebug_ForcedSpecificEyeFindNewsStory = 1
	BOOL bDebug_ForceSpecificEyeFindNewsStory
#ENDIF

BOOL bIsInternetTextSupported = TRUE	// B*1582586 - check that the lang has text supported

/// PURPOSE:
///    Jumps to specified website
PROC GO_TO_WEBSITE(STRING Website)
	g_sPendingBrowserPage = Website
	g_iPendingBrowserTimer = g_iBrowserTimer+ 1000
ENDPROC

/// PURPOSE: data needed to store the current Eye Find News Story
STRUCT EYEFIND_NEWS_STORY_DATA
	INT iNewsOrganisation
	TEXT_LABEL_31 tl23temp_NewsHeadline
	TEXT_LABEL_31 tl23temp_Story
ENDSTRUCT
EYEFIND_NEWS_STORY_DATA sEyeFindCurrentNewStory

/// PURPOSE:
///    get the text block string needed by Eyefind to display the news
/// RETURNS:
///    STRING name of the text block to be requested
FUNC STRING GET_EYEFIND_REQUIRED_TEXT_BLOCK()

	EYEFIND_NEWS_STORY_STATE_ENUM eStory = g_savedGlobals.sFinanceData.eCurrentEyeFindNewsStoryState
	
	IF eStory = EYEFIND_NEWS_STORY_STATE_SP_PRO			// Newsbreak1
		RETURN "NWS_PRO"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ARM2		// Newsbreak2
		RETURN "NWS_AR2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ARM3		// Newsbreak3
		RETURN "NWS_AR3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM1		// Newsbreak4
		RETURN "NWS_FAM1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM3		// Newsbreak5
		RETURN "NWS_FAM3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_LAM1		// Newsbreak6
		RETURN "NWS_LAM1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_LST1		// Newsbreak7
		RETURN "NWS_LST1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_JWL		// Newsbreak8
		RETURN "NWS_JWLH"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_TRE2		// Newsbreak9
		RETURN "NWS_TRE2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CHI1		// Newsbreak10
		RETURN "NWS_CHI1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CHI2		// Newsbreak11
		RETURN "NWS_CHI2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_TRE3		// Newsbreak12
		RETURN "NWS_TRE3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM4		// Newsbreak13
		RETURN "NWS_FAM4"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB2		// Newsbreak14
		RETURN "NWS_FIB2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB3		// Newsbreak15
		RETURN "NWS_FIB3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FRA1		// Newsbreak16
		RETURN "NWS_FRA1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB4		// Newsbreak17
		RETURN "NWS_FIB4"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_DOCK	// Newsbreak18
		RETURN "NWS_DCKH"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CARS2		// Newsbreak19
		RETURN "NWS_CARS2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL1		// Newsbreak20
		RETURN "NWS_SOL1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MTN1		// Newsbreak21
		RETURN "NWS_MTN1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CARS3		// Newsbreak22
		RETURN "NWS_CARS3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_EXL1		// Newsbreak23
		RETURN "NWS_EXL1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_RB2A	// Newsbreak24
		RETURN "NWS_RBH"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_EXL3		// Newsbreak25
		RETURN "NWS_EXL3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB5		// Newsbreak26
		RETURN "NWS_FIB5"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC1		// Newsbreak27
		RETURN "NWS_MIC1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL2		// Newsbreak28
		RETURN "NWS_SOL2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM6		// Newsbreak29
		RETURN "NWS_FAM6"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_AG3A	// Newsbreak30
		RETURN "NWS_AGH"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC3		// Newsbreak31
		RETURN "NWS_MIC3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL3		// Newsbreak32
		RETURN "NWS_SOL3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC4		// Newsbreak33
		RETURN "NWS_MIC4"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_BS2A	// Newsbreak34
		RETURN "NWS_BSH"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FINALE	// Newsbreak35	
		RETURN "NWS_FIN"
		
	// RC mission unlocks
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP1		// Newsbreak36
		RETURN "NWS_PAP1"
	//ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP2	// Newsbreak37	- no story
		//
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP3A		// Newsbreak38
		RETURN "NWS_PAP3A"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP3B		// Newsbreak39
		RETURN "NWS_PAP3B"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_DRF1		// Newsbreak40
		RETURN "NWS_DRF1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_EPN8		// Newsbreak41
		RETURN "NWS_EPN8"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1A		// Newsbreak42
		RETURN "NWS_NIG1A"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1B		// Newsbreak43
		RETURN "NWS_NIG1B"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1C		// Newsbreak44
		RETURN "NWS_NIG1C"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1D		// Newsbreak45
		RETURN "NWS_NIG1D"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG2		// Newsbreak46
		RETURN "NWS_NIG2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG3		// Newsbreak47
		RETURN "NWS_NIG3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_EXT4		// Newsbreak48
		RETURN "NWS_EXT4"
		
	// Assassination (now SP_)
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS1		// Newsbreak49
		RETURN "NWS_ASS1"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS2		// Newsbreak50
		RETURN "NWS_ASS2"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS3		// Newsbreak51
		RETURN "NWS_ASS3"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS4		// Newsbreak52
		RETURN "NWS_ASS4"
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS5		// Newsbreak53
		RETURN "NWS_ASS5"
		
	// Oddjobs - Air trafficking
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_O_ATA3		// Newsbreak54
		RETURN "NWS_O_ATA"
		
	//Shrink (now SP_)
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SHK5		// Newsbreak55
		RETURN "NWS_SHK5"
	
	// Cult
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_O_CULT		// Newsbreak56
		RETURN "NWS_CULT"
	ELSE
		CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " GET_EYEFIND_REQUIRED_TEXT_BLOCK() : return FALSE!")
	ENDIF
	
	RETURN ""	
ENDFUNC
	
/// PURPOSE:
///    returns the INT value for the EyeFind New story's organisation
/// RETURNS:
///    INT value for organisation
FUNC INT GET_EYEFIND_NEWS_STORY_ORGANISATION(EYEFIND_NEWS_STORY_STATE_ENUM eStory, INT iStoryVariation)
	
	IF eStory = EYEFIND_NEWS_STORY_STATE_SP_PRO			// Newsbreak1
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_LIBERTY_TREE
		ELIF iStoryVariation = 6
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ARM2		// Newsbreak2
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ARM3		// Newsbreak3
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM1		// Newsbreak4
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_LS24
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM3		// Newsbreak5
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_LAM1		// Newsbreak6
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_LST1		// Newsbreak7
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LIBERTY_TREE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_JWL		// Newsbreak8
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 6
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_TRE2		// Newsbreak9
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CHI1		// Newsbreak10
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CHI2		// Newsbreak11
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_PRATTLE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_TRE3		// Newsbreak12
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM4		// Newsbreak13
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_TALKIN_BALLS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB2		// Newsbreak14
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB3		// Newsbreak15
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PRATTLE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FRA1		// Newsbreak16
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LS24
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_LS24
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB4		// Newsbreak17
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_DOCK	// Newsbreak18
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CARS2		// Newsbreak19
		IF iStoryVariation = 1
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 6
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 7
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 8
			RETURN NEWS_ORG_DAILY_RAG
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL1		// Newsbreak20
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LS24
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MTN1		// Newsbreak21
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LS24
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CARS3		// Newsbreak22
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_TALKIN_BALLS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_EXL1		// Newsbreak23
		IF iStoryVariation = 1
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_RB2A	// Newsbreak24
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_TALKIN_BALLS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_EXL3		// Newsbreak25
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_TALKIN_BALLS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB5		// Newsbreak26
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_RAG
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC1		// Newsbreak27
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LUDENDORFF_WEEKLY
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL2		// Newsbreak28
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM6		// Newsbreak29
		IF iStoryVariation = 1
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_TALKIN_BALLS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_AG3A	// Newsbreak30
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_TALKIN_BALLS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LIBERTY_TREE
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_TALKIN_BALLS
		ELIF iStoryVariation = 6
			RETURN NEWS_ORG_LIBERTY_TREE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC3		// Newsbreak31
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL3		// Newsbreak32
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PRATTLE
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE 
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC4		// Newsbreak33
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LS24
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LIBERTY_TREE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_BS2A	// Newsbreak34
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 6
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FINALE	// Newsbreak35	
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_LIBERTY_TREE
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	// RC mission unlocks
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP1		// Newsbreak36
		IF iStoryVariation = 1
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	//ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP2	// Newsbreak37	- no story
	
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP3A		// Newsbreak38
		IF iStoryVariation = 1
			RETURN NEWS_ORG_PRATTLE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP3B		// Newsbreak39
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LS24
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_DRF1		// Newsbreak40
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_EPN8		// Newsbreak41
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LS24
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1A		// Newsbreak42
		IF iStoryVariation = 1
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_TALKIN_BALLS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1B		// Newsbreak43
		IF iStoryVariation = 1
			RETURN NEWS_ORG_PRATTLE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LIBERTY_TREE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1C		// Newsbreak44
		IF iStoryVariation = 1
			RETURN NEWS_ORG_STARSTALK
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_PRATTLE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1D		// Newsbreak45
		IF iStoryVariation = 1
			RETURN NEWS_ORG_PRATTLE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LIBERTY_TREE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG2		// Newsbreak46
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG3		// Newsbreak47
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_RAG
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_EXT4		// Newsbreak48
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WEAZEL_NEWS
		ENDIF
	// Assassination (now SP_)
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS1		// Newsbreak49
		IF iStoryVariation = 1
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS2		// Newsbreak50
		IF iStoryVariation = 1
			RETURN NEWS_ORG_PUBLIC_LIBERTY_ONLINE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_DAILY_GLOBE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS3		// Newsbreak51
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LIBERTY_TREE
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS4		// Newsbreak52
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS5		// Newsbreak53
		IF iStoryVariation = 1
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_SHEPERD
		ENDIF
	// Oddjobs - Air trafficking
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_O_ATA3		// Newsbreak54
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ENDIF
	//Shrink (now SP_)
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SHK5		// Newsbreak55
		IF iStoryVariation = 1
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_O_CULT		// Newsbreak56
		IF iStoryVariation = 1
			RETURN NEWS_ORG_SENORA_BEACON
		ENDIF
		
	// default - this shouldn't happen but as a safeguard using EYEFIND_NEWS_STORY_STATE_SP_PRO			// Newsbreak1
	ELSE
		CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " GET_EYEFIND_NEWS_STORY_ORGANISATION() : unable to find story's org ! default settings returned")
		IF iStoryVariation = 1
			RETURN NEWS_ORG_DAILY_GLOBE
		ELIF iStoryVariation = 2
			RETURN NEWS_ORG_LOS_SANTOS_METEOR
		ELIF iStoryVariation = 3
			RETURN NEWS_ORG_WEAZEL_NEWS
		ELIF iStoryVariation = 4
			RETURN NEWS_ORG_DAILY_RAG
		ELIF iStoryVariation = 5
			RETURN NEWS_ORG_LIBERTY_TREE
		ELIF iStoryVariation = 6
			RETURN NEWS_ORG_WNKA_INTERNATIONAL
		ENDIF		
	ENDIF
	
	RETURN 0	// safer to return a valid org
ENDFUNC

/// PURPOSE:
///    sets the parameters needed to displayed the current news story in eyefind
/// PARAMS:
///    tl31Headline - the returned headline
///    tl31Story - the returned story
///    iNewsOrganistation - the int for the new organisation 
/// RETURNS:
///    TRUE if set successfully
FUNC BOOL SET_CURRENT_EYEFIND_NEWS_STORY(TEXT_LABEL_31 &tl31Headline, TEXT_LABEL_31 &tl31Story, INT &iNewsOrganisation)

	EYEFIND_NEWS_STORY_STATE_ENUM eStory = g_savedGlobals.sFinanceData.eCurrentEyeFindNewsStoryState
	INT iMinStoryVariation = 1
	INT iMaxStoryVariations
	INT iStoryVariation
	
	IF eStory = EYEFIND_NEWS_STORY_STATE_SP_PRO			// Newsbreak1
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 6	
		tl31Headline = "NWS_PRO_HL"
		tl31Story = "NWS_PRO_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ARM2		// Newsbreak2
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 5	
		tl31Headline = "NWS_AR2_HL"
		tl31Story = "NWS_AR2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ARM3		// Newsbreak3
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 4	
		tl31Headline = "NWS_AR3_HL"
		tl31Story = "NWS_AR3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM1		// Newsbreak4
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 5	
		tl31Headline = "NWS_FAM1_HL"
		tl31Story = "NWS_FAM1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM3		// Newsbreak5
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 4	
		tl31Headline = "NWS_FAM3_HL"		
		tl31Story = "NWS_FAM3_S"		
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_LAM1		// Newsbreak6
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 4				
		tl31Headline = "NWS_LAM1_HL"
		tl31Story = "NWS_LAM1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_LST1		// Newsbreak7
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 4				
		tl31Headline = "NWS_LST1_HL"
		tl31Story = "NWS_LST1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_JWL		// Newsbreak8
		
		// different stories depending on the way the Heist was played
		//{-------IF STEALTH-------}
		IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_JEWEL) = HEIST_CHOICE_JEWEL_STEALTH
			iMinStoryVariation = 1
			iMaxStoryVariations = 3		
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_H_JWL = HEIST_CHOICE_JEWEL_STEALTH stories")
		// {------IF GUNS BLAZING -----}	// HEIST_CHOICE_JEWEL_HIGH_IMPACT
		ELSE	
			iMinStoryVariation = 4
			iMaxStoryVariations = 6
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_H_JWL != HEIST_CHOICE_JEWEL_STEALTH stories")
		ENDIF	
		tl31Headline = "NWS_JWLH_HL"
		tl31Story = "NWS_JWLH_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_TRE2		// Newsbreak9
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 4				
		tl31Headline = "NWS_TRE2_HL"
		tl31Story = "NWS_TRE2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CHI1		// Newsbreak10
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 4				
		tl31Headline = "NWS_CHI1_HL"
		tl31Story = "NWS_CHI1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CHI2		// Newsbreak11
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 4				
		tl31Headline = "NWS_CHI2_HL"
		tl31Story = "NWS_CHI2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_TRE3		// Newsbreak12
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 3			
		tl31Headline = "NWS_TRE3_HL"
		tl31Story = "NWS_TRE3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM4		// Newsbreak13
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 4		
		tl31Headline = "NWS_FAM4_HL"
		tl31Story = "NWS_FAM4_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB2		// Newsbreak14
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 4		
		tl31Headline = "NWS_FIB2_HL"
		tl31Story = "NWS_FIB2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB3		// Newsbreak15
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 4		
		tl31Headline = "NWS_FIB3_HL"
		tl31Story = "NWS_FIB3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FRA1		// Newsbreak16

		//{------only trigger 63 if MC clip is killed------}
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FRANK1_MC_CLIPPED)
			iMinStoryVariation = 1
			iMaxStoryVariations = 3
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_FRA1 = FLOWFLAG_MISSION_FRANK1_MC_CLIPPED stories")
		ELSE
			iMinStoryVariation = 2
			iMaxStoryVariations = 3		
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_FRA1 != FLOWFLAG_MISSION_FRANK1_MC_CLIPPED stories")
		ENDIF
		tl31Headline = "NWS_FRA1_HL"
		tl31Story = "NWS_FRA1_S"
			
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB4		// Newsbreak17
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 4		
		tl31Headline = "NWS_FIB4_HL"
		tl31Story = "NWS_FIB4_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_DOCK	// Newsbreak18
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 4		
		tl31Headline = "NWS_DCKH_HL"
		tl31Story = "NWS_DCKH_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CARS2		// Newsbreak19
	
		//{------IF CHAD SURVIVES -----}
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN)
			iMinStoryVariation = 5
			iMaxStoryVariations = 8
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_CARS2 = FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN stories")
		ELSE
			iMinStoryVariation = 1
			iMaxStoryVariations = 4
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_CARS2 != FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN stories")
		ENDIF
		tl31Headline = "NWS_CARS2_HL"
		tl31Story = "NWS_CARS2_S"
			
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL1		// Newsbreak20
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_SOL1_HL"
		tl31Story = "NWS_SOL1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MTN1		// Newsbreak21
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_MTN1_HL"
		tl31Story = "NWS_MTN1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_CARS3		// Newsbreak22
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_CARS3_HL"
		tl31Story = "NWS_CARS3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_EXL1		// Newsbreak23
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_EXL1_HL"
		tl31Story = "NWS_EXL1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_RB2A	// Newsbreak24
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_RBH_HL"
		tl31Story = "NWS_RBH_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_EXL3		// Newsbreak25
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_EXL3_HL"
		tl31Story = "NWS_EXL3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FIB5		// Newsbreak26
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_FIB5_HL"
		tl31Story = "NWS_FIB5_S"
	
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC1		// Newsbreak27
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_MIC1_HL"
		tl31Story = "NWS_MIC1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL2		// Newsbreak28
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_SOL2_HL"
		tl31Story = "NWS_SOL2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FAM6		// Newsbreak29
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_FAM6_HL"
		tl31Story = "NWS_FAM6_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_AG3A	// Newsbreak30
	
		//{------- IF SUCCESS / HEIST_CHOICE_AGENCY_FIRETRUCK-------}
		//Did fire crew variation. Blew up building, went in as firemen and escaped with the data.
		IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY) = HEIST_CHOICE_AGENCY_FIRETRUCK
			iMinStoryVariation = 1
			iMaxStoryVariations = 3
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_H_AG3A = HEIST_CHOICE_AGENCY_FIRETRUCK stories")
			
		//{-------IF HELI CRASH	/ HEIST_CHOICE_AGENCY_HELICOPTER -------}
		//Did air drop variation. Parachuted in, extraction heli blown up, fought their way out.
		ELSE		
			iMinStoryVariation = 4
			iMaxStoryVariations = 6		
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_H_AG3A = HEIST_CHOICE_AGENCY_HELICOPTER stories")
		ENDIF
		tl31Headline = "NWS_AGH_HL"
		tl31Story = "NWS_AGH_S"

	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC3		// Newsbreak31
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_MIC3_HL"
		tl31Story = "NWS_MIC3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SOL3		// Newsbreak32
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_SOL3_HL"
		tl31Story = "NWS_SOL3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_MIC4		// Newsbreak33
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 3
		tl31Headline = "NWS_MIC4_HL"
		tl31Story = "NWS_MIC4_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_H_BS2A	// Newsbreak34
	
		//{------- IF TRICKED BANK / HEIST_CHOICE_FINALE_TRAFFCONT-------}
		//Did Traffic Control variation. Tricked the bank and fought Merryweather during the getaway.
		IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE) = HEIST_CHOICE_FINALE_TRAFFCONT
			iMinStoryVariation = 1
			iMaxStoryVariations = 3
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_H_BS2A = HEIST_CHOICE_FINALE_TRAFFCONT stories")
			
		//{------- IF FOUGHT BANK / HEIST_CHOICE_FINALE_HELI -------}
		//Did the Heli Lift variation. Massive shootout in bank vault and across city with swat teams.
		ELSE		
			iMinStoryVariation = 4
			iMaxStoryVariations = 6		
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_SP_H_BS2A = HEIST_CHOICE_FINALE_HELI stories")
		ENDIF
		tl31Headline = "NWS_BSH_HL"
		tl31Story = "NWS_BSH_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_FINALE	// Newsbreak35	
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 5
		tl31Headline = "NWS_FIN_HL"
		tl31Story = "NWS_FIN_S"
		
	// RC mission unlocks
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP1		// Newsbreak36
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2		
		tl31Headline = "NWS_PAP1_HL"
		tl31Story = "NWS_PAP1_S"
		
	//ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP2	// Newsbreak37	- no story
	
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP3A		// Newsbreak38
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_PAP3A_HL"
		tl31Story = "NWS_PAP3A_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_PAP3B		// Newsbreak39
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_PAP3B_HL"
		tl31Story = "NWS_PAP3B_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_DRF1		// Newsbreak40
		
		// Can only use the first story if Dreyfuss was killed
		IF IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
			iMinStoryVariation = 1
			iMaxStoryVariations = 1
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_DRF1 = RC_NWS_DREYFUSS_KILLED_DREY stories")
			
		// Player let Dreyfuss go
		ELSE		
			iMinStoryVariation = 2
			iMaxStoryVariations = 2
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_DRF1 != RC_NWS_DREYFUSS_KILLED_DREY stories")
		ENDIF
		tl31Headline = "NWS_DRF1_HL"
		tl31Story = "NWS_DRF1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_EPN8		// Newsbreak41
	
		// Can only use the first story if Michael steals the cash
		IF IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_EPS_STOLEN_CASH))
			iMinStoryVariation = 1
			iMaxStoryVariations = 1
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_EPN8 = RC_NWS_EPS_STOLEN_CASH stories")
			
		// Michael handed the cash over
		ELSE		
			iMinStoryVariation = 2
			iMaxStoryVariations = 2
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_EPN8 != RC_NWS_EPS_STOLEN_CASH stories")
		ENDIF
		tl31Headline = "NWS_EPN8_HL"
		tl31Story = "NWS_EPN8_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1A		// Newsbreak42
		
		// Can only use the second story if Willie wasn't killed
		IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLA_KILLED_WILLIE))
			iMinStoryVariation = 2
			iMaxStoryVariations = 2
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1A != RC_NWS_NGLA_KILLED_WILLIE stories")
			
		// Willie was killed
		ELSE		
			iMinStoryVariation = 1
			iMaxStoryVariations = 1
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1A = RC_NWS_NGLA_KILLED_WILLIE stories")
		ENDIF
		tl31Headline = "NWS_NIG1A_HL"
		tl31Story = "NWS_NIG1A_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1B		// Newsbreak43
	
		// Can only use the first story if Tyler Dixon wasn't killed
		IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLB_KILLED_TYLER))
			iMinStoryVariation = 1
			iMaxStoryVariations = 1
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1B != RC_NWS_NGLB_KILLED_TYLER stories")
			
		// Tyler was killed
		ELSE		
			iMinStoryVariation = 2
			iMaxStoryVariations = 2
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1B = RC_NWS_NGLB_KILLED_TYLER stories")
		ENDIF
		tl31Headline = "NWS_NIG1B_HL"
		tl31Story = "NWS_NIG1B_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1C		// Newsbreak44
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_NIG1C_HL"
		tl31Story = "NWS_NIG1C_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG1D		// Newsbreak45
		
		// Can only use the first story if Mark Fostenburg wasn't killed
		IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLD_KILLED_MARK))
			iMinStoryVariation = 1
			iMaxStoryVariations = 1
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1D != RC_NWS_NGLD_KILLED_MARK stories")
			
		// Mark was killed
		ELSE		
			iMinStoryVariation = 2
			iMaxStoryVariations = 2
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1D = RC_NWS_NGLD_KILLED_MARK stories")
		ENDIF
		tl31Headline = "NWS_NIG1D_HL"
		tl31Story = "NWS_NIG1D_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG2		// Newsbreak46
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_NIG2_HL"
		tl31Story = "NWS_NIG2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_NIG3		// Newsbreak47
		
		//{-------IF AL DI NAPOLI KILLED-------}		
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED)
			// news story talks about train death - so can't use the story if he's killed a different way
			IF IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGL3_DINAPOLI_NOT_KILLED_BY_TRAIN))
				iMinStoryVariation = 2
				iMaxStoryVariations = 2
				CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG3 = FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED = RC_NWS_NGL3_DINAPOLI_NOT_KILLED_BY_TRAIN stories")
			ELSE
				iMinStoryVariation = 1
				iMaxStoryVariations = 1
				CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG3 = FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED != RC_NWS_NGL3_DINAPOLI_NOT_KILLED_BY_TRAIN stories")
			ENDIF
			
		//{-------IF AL DI NAPOLI IS RELEASED-------}
		ELSE
			// suppress released story until the postRC_Nigel3.sc has cleaned up - as this handles player killing him after release
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("postRC_Nigel3")) = 0
				iMinStoryVariation = 3
				iMaxStoryVariations = 3		
				CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG3 != FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED stories")
			ELSE
				iMinStoryVariation = 4
				iMaxStoryVariations = 4		
				CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG3 != FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED : postRC_Nigel3 running stories")
			ENDIF
		ENDIF
		tl31Headline = "NWS_NIG3_HL"
		tl31Story = "NWS_NIG3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_RC_EXT4		// Newsbreak48
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_EXT4_HL"
		tl31Story = "NWS_EXT4_S"
		
	// Assassination (now SP_)
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS1		// Newsbreak49
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_ASS1_HL"
		tl31Story = "NWS_ASS1_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS2		// Newsbreak50
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_ASS2_HL"
		tl31Story = "NWS_ASS2_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS3		// Newsbreak51
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 1
		tl31Headline = "NWS_ASS3_HL"
		tl31Story = "NWS_ASS3_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS4		// Newsbreak52
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_ASS4_HL"
		tl31Story = "NWS_ASS4_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_ASS5		// Newsbreak53
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 2
		tl31Headline = "NWS_ASS5_HL"
		tl31Story = "NWS_ASS5_S"
		
	// Oddjobs - Air trafficking
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_O_ATA3		// Newsbreak54
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 1
		tl31Headline = "NWS_O_ATA_HL"
		tl31Story = "NWS_O_ATA_S"
		
	//Shrink (now SP_)
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_SP_SHK5		// Newsbreak55

		// different stories depending on if Dr Friedlander was killed
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SHRINK_KILLED)
			iMinStoryVariation = 1
			iMaxStoryVariations = 1
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1A = FLOWFLAG_SHRINK_KILLED stories")
			
		// Dr Friedlander was left alive
		ELSE		
			iMinStoryVariation = 2
			iMaxStoryVariations = 2
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : EYEFIND_NEWS_STORY_STATE_RC_NIG1A != FLOWFLAG_SHRINK_KILLED stories")
		ENDIF
		tl31Headline = "NWS_SHK5_HL"
		tl31Story = "NWS_SHK5_S"
		
	ELIF eStory = EYEFIND_NEWS_STORY_STATE_O_CULT		// Newsbreak56
	
		iMinStoryVariation = 1
		iMaxStoryVariations = 1
		tl31Headline = "NWS_CULT_HL"
		tl31Story = "NWS_CULT_S"
		
	// default - this shouldn't happen but as a safeguard using EYEFIND_NEWS_STORY_STATE_SP_PRO			// Newsbreak1
	ELSE
		
		iMinStoryVariation = 1
		iMaxStoryVariations = 6	
		tl31Headline = "NWS_PRO_HL"
		tl31Story = "NWS_PRO_S"
		CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : default settings! Newsbreak1 / NWS_PRO")
	ENDIF
	
	IF eStory != EYEFIND_NEWS_STORY_STATE_INVALID
		iStoryVariation = GET_RANDOM_INT_IN_RANGE(iMinStoryVariation, (iMaxStoryVariations + 1))
		#IF IS_DEBUG_BUILD
			// use debug widgets from appInternet.sc to override specific news stories
			IF bDebug_ForceSpecificEyeFindNewsStory
				IF iDebug_ForcedSpecificEyeFindNewsStory > 0
				AND iDebug_ForcedSpecificEyeFindNewsStory <= iMaxStoryVariations
					iStoryVariation = iDebug_ForcedSpecificEyeFindNewsStory
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : DEBUG : forced random story to : ", iStoryVariation)
				ELSE
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : DEBUG : unable to Override index out of range! ")
				ENDIF
			ENDIF
		#ENDIF
		tl31Headline += iStoryVariation
		tl31Story += iStoryVariation
		iNewsOrganisation = GET_EYEFIND_NEWS_STORY_ORGANISATION(eStory, iStoryVariation)
		
		CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " SET_CURRENT_EYEFIND_NEWS_STORY() : return TRUE : tl31Headline = ", tl31Headline,
															" tl31Story = ", tl31Story, " iNewsOrganisation = ", iNewsOrganisation)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

//********************************************************************************
//*******************************Avery GPS property call***************************
//********************************************************************************
FLOAT lastxlocval, lastylocval // 1638219
FUNC BOOL SET_ROUTE_TARGET_FOR_AVERY_SITE_GPS(VECTOR route_target)
	IF g_bInMultiplayer
	AND NOT NETWORK_CAN_SET_WAYPOINT()
		CPRINTLN(DEBUG_INTERNET, "SET_ROUTE_TARGET_FOR_AVERY_SITE_GPS: cannot set waypoint due to NETWORK_CAN_SET_WAYPOINT")
		RETURN FALSE
	ENDIF
	
	// Set new waypoint for the GPS
	IF lastxlocval != route_target.x 
	AND lastylocval != route_target.y
		CPRINTLN(DEBUG_INTERNET, "SET_ROUTE_TARGET_FOR_AVERY_SITE_GPS: setting waypoint at ",route_target.x ,", ", route_target.y)
		SET_NEW_WAYPOINT(route_target.x, route_target.y)
		
		lastxlocval = route_target.x 
		lastylocval = route_target.y
		RETURN TRUE
	ELSE
		CPRINTLN(DEBUG_INTERNET, "SET_ROUTE_TARGET_FOR_AVERY_SITE_GPS: workaround for #1638219 tripped, keeping waypoint at ",route_target.x ,", ", route_target.y)
		RETURN TRUE
	ENDIF
ENDFUNC

//********************************************************************************
//*******************************BANK Elements************************************
//********************************************************************************
INT iBankDWStep, iBankDWAmount, iBankMessageMode

/// PURPOSE:
///    What monetary value should be shown on each button
/// PARAMS:
///    iButtonNum - Which button are we checking
/// RETURNS:
///    Amount that should be shown and withdrawn/deposited for each button
///    Returns -1 on buttons that should not be shown
FUNC INT GET_VALUE_FOR_BUTTON(INT iButtonNum, BOOL bDepositing = FALSE)
	INT iWalletBalance = NETWORK_GET_VC_WALLET_BALANCE()
	SWITCH iButtonNum
		CASE 0 // Top left button - $50
			IF bDepositing
				IF iWalletBalance = 0
					RETURN -1
				ELIF iWalletBalance < 50
					RETURN iWalletBalance
				ELSE
					RETURN 50
				ENDIF
			ELSE
				RETURN 50
			ENDIF
		BREAK
		CASE 1 // Top right button - $10,000
			IF bDepositing
				IF iWalletBalance <= 2500
					RETURN -1
				ELIF (iWalletBalance > 2500 AND iWalletBalance < 10000)
					RETURN iWalletBalance
				ELSE
					RETURN 10000
				ENDIF
			ELSE
				RETURN 10000
			ENDIF
		BREAK
		CASE 2 // Middle left button - $500
			IF bDepositing
				IF iWalletBalance <= 50
					RETURN -1
				ELIF (iWalletBalance > 50 AND iWalletBalance < 500) 
					RETURN iWalletBalance
				ELSE
					RETURN 500
				ENDIF
			ELSE
				RETURN 500
			ENDIF
		BREAK
		CASE 3 // Middle Right button - $100,000
			IF bDepositing
				IF iWalletBalance <= 10000
					RETURN -1
				ELIF (iWalletBalance > 10000 AND iWalletBalance < 100000) 
					RETURN iWalletBalance
				ELSE
					RETURN 100000
				ENDIF
			ELSE
				RETURN 100000
			ENDIF
		BREAK
		CASE 4 // Bottom left button - $2,500
			IF bDepositing
				IF iWalletBalance <= 500
					RETURN -1
				ELIF (iWalletBalance > 500 AND iWalletBalance < 2500)
					RETURN iWalletBalance
				ELSE
					RETURN 2500
				ENDIF
			ELSE
				RETURN 2500
			ENDIF
		BREAK
		CASE 5 // Bottom right button - $1,000,000 (except on deposits this is now "all" for any amount over $100K)
			IF bDepositing
				IF iWalletBalance <= 100000
					RETURN -1
				ELSE
					RETURN iWalletBalance
				ENDIF
			ELSE
				RETURN 1000000
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Checks whether an MP player's wallet has enough funds for given deposit amount
/// PARAMS:
///    iAmount - How much money?
/// RETURNS:
///    TRUE if sufficient funds
FUNC BOOL DOES_WALLET_HAVE_ENOUGH_FUNDS(INT iAmount)
	INT iRemaining
	IF iAmount = -1
		RETURN FALSE
	ENDIF
	IF NETWORK_CAN_SPEND_MONEY2(iAmount, FALSE, FALSE, FALSE, iRemaining)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether an MP player's account balance is enough for given withdrawal amount
/// PARAMS:
///    iAmount - How much money?
/// RETURNS:
///    TRUE if sufficient funds
FUNC BOOL DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(INT iAmount)
	IF NETWORK_GET_VC_BANK_BALANCE() >= iAmount
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if minimum funds for player to withdraw are in account
/// RETURNS:
///    TRUE if we should allow withdrawals screen
FUNC BOOL DOES_PLAYER_HAVE_MIN_FOR_WITHDRAWALS()
	RETURN DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(0))
ENDFUNC

/// PURPOSE:
///    Check if minimum funds for player to deposit are in wallet
/// RETURNS:
///    TRUE if we should allow deposit screen
FUNC BOOL DOES_PLAYER_HAVE_MIN_FOR_DEPOSITS()
	RETURN DOES_WALLET_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(0))
ENDFUNC

/// PURPOSE:
///    Single player balance page
PROC SF_ATM_BALANCE_PAGE(SCALEFORM_INDEX pagemov)
	
	/*
		SLOT ID        Text Comment                     Current debug text
		0              button for PAGE1                 "Log off"
		1              customer name                    "Customer: Michael Townley"
		2              title of page                    "Account Transaction Log"
		3              current balance                  "Balance $19500"
	*/
	enumBankAccountName eban = g_savedGlobals.sCharSheetData.g_CharacterSheet[GET_CURRENT_PLAYER_PED_ENUM()].bank_account
		
	IF (eban = NO_ACCOUNT) OR (eban = MAX_ACCOUNTS)
		EXIT
	ENDIF
		
	// 0: Button for PAGE1 - "Log off"
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("W_BA_LGOF")
	END_SCALEFORM_MOVIE_METHOD()
		
	SWITCH eban
		
		CASE BANK_ACCOUNT_MICHAEL
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCNA_MIKE")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE BANK_ACCOUNT_TREVOR
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCNA_TREVOR")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		CASE BANK_ACCOUNT_FRANKLIN
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCNA_FRANKLIN")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("W_BA_ATL")
	END_SCALEFORM_MOVIE_METHOD()
																					
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(g_BankAccounts[eban].iBalance))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("W_BA_BAL")
	END_SCALEFORM_MOVIE_METHOD()
	
	
	CPRINTLN(DEBUG_INTERNET, "SP balance \"Balace  $", TO_FLOAT(g_BankAccounts[eban].iBalance), "\"")
	
		
	/*
		Transaction Log entries require four parameters instead of the usual two.
		Log entries start from Slot ID 4.
		You can add as many entries as you like, and the page will set it's height dynamically to match the number of entries.
		Cash value must be a positive number.

		SLOT ID      Transaction type (1 for credit, 0 for debit)  Cash value  Source of transaction ie company or individual
		4            0                                             200         Debit to Madam Chong's Massage Parlour
		5            0                                             300         Debit to Los Santos Pay 'n' Spray
		6            0                                             29          Debit to Whizz Phone Services
		7            1                                             10000       Credit from Mikhail Faustin
	*/
	
	INT offsetter = 4
	
	INT i = 0
	INT creddeb = 0
	INT total = MAX_BANK_ACCOUNT_LOG_ENTRIES
		
	total = g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].iLogActions
	
	IF NOT ( total < MAX_BANK_ACCOUNT_LOG_ENTRIES )
		total = MAX_BANK_ACCOUNT_LOG_ENTRIES
	ENDIF
		
	//changed this to use the right value that wraps, otherwise it would array overrun after 100 entries
	INT sat = g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].iLogIndexPoint-1//g_BankAccounts[eban].iLogActions-1
		
	IF sat > -1
		REPEAT total i 
			IF sat < 0
				sat = MAX_BANK_ACCOUNT_LOG_ENTRIES-1
			ENDIF
		
			creddeb = 0
			IF g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].LogEntries[sat].eType = BAA_CREDIT
				creddeb = 1
			ENDIF
				
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(offsetter)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(creddeb)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].LogEntries[sat].iDegree)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_BAAC_STRING_TAG(g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].LogEntries[sat].eBaacSource))
			END_SCALEFORM_MOVIE_METHOD()

			sat--
			offsetter++
		ENDREPEAT
	ENDIF
ENDPROC

PROC SF_BANK_TRANSACTIONS_PAGE(SCALEFORM_INDEX pagemov)
	
	enumBankAccountName eban = g_savedGlobals.sCharSheetData.g_CharacterSheet[GET_CURRENT_PLAYER_PED_ENUM()].bank_account
	IF (eban = NO_ACCOUNT) OR (eban = MAX_ACCOUNTS)
		EXIT
	ENDIF
		
	// Balance
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_BankAccounts[eban].iBalance)
	END_SCALEFORM_MOVIE_METHOD()
	
	
	CPRINTLN(DEBUG_INTERNET, "SP transaction A \"Balance  $", TO_FLOAT(g_BankAccounts[eban].iBalance), "\"")
	
		
	// Customer name
	SWITCH eban
		
		CASE BANK_ACCOUNT_MICHAEL
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCNA_MIKE")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE BANK_ACCOUNT_TREVOR
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCNA_TREVOR")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		CASE BANK_ACCOUNT_FRANKLIN
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACCNA_FRANKLIN")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH

	// Main header
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("W_BA_ATL")
	END_SCALEFORM_MOVIE_METHOD()
		
	// Balance title
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_BankAccounts[eban].iBalance)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("W_BA_BAL")
	END_SCALEFORM_MOVIE_METHOD()
	
	
	CPRINTLN(DEBUG_INTERNET, "SP transaction B \"Balance  $", TO_FLOAT(g_BankAccounts[eban].iBalance), "\"")
	
		
	/*
		Transaction Log entries require four parameters instead of the usual two.
		Log entries start from Slot ID 4.
		You can add as many entries as you like, and the page will set it's height dynamically to match the number of entries.
		Cash value must be a positive number.

		SLOT ID      Transaction type (1 for credit, 0 for debit)  Cash value  Source of transaction ie company or individual
		4            0                                             200         Debit to Madam Chong's Massage Parlour
		5            0                                             300         Debit to Los Santos Pay 'n' Spray
		6            0                                             29          Debit to Whizz Phone Services
		7            1                                             10000       Credit from Mikhail Faustin
	*/
	
	INT offsetter = 4
	
	INT i = 0
	INT creddeb = 0
	INT total = MAX_BANK_ACCOUNT_LOG_ENTRIES
		
	total = g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].iLogActions
	
	IF NOT ( total < MAX_BANK_ACCOUNT_LOG_ENTRIES )
		total = MAX_BANK_ACCOUNT_LOG_ENTRIES
	ENDIF
		
	//changed this to use the right value that wraps, otherwise it would array overrun after 100 entries
	INT sat = g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].iLogIndexPoint-1//g_BankAccounts[eban].iLogActions-1
		
	IF sat > -1
		REPEAT total i 
			IF sat < 0
				sat = MAX_BANK_ACCOUNT_LOG_ENTRIES-1
			ENDIF
		
			creddeb = 0
			IF g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].LogEntries[sat].eType = BAA_CREDIT
				creddeb = 1
			ENDIF
				
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(offsetter)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(creddeb)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].LogEntries[sat].iDegree)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_BAAC_STRING_TAG(g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[eban].LogEntries[sat].eBaacSource))
			END_SCALEFORM_MOVIE_METHOD()

			sat--
			offsetter++
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Display the transactions page for multiplayer
PROC SF_BANK_TRANSACTIONS_PAGE_MP(SCALEFORM_INDEX pagemov)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()

	// Account balance
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(NETWORK_GET_STRING_BANK_BALANCE())	//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NETWORK_GET_VC_BANK_BALANCE())
	END_SCALEFORM_MOVIE_METHOD()

	// Name
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))
	END_SCALEFORM_MOVIE_METHOD()

	// Transaction log
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_LOG")
	END_SCALEFORM_MOVIE_METHOD()
	
	// Balance
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NETWORK_GET_VC_BANK_BALANCE())
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_ACBA")
	END_SCALEFORM_MOVIE_METHOD()
	
	
	CPRINTLN(DEBUG_INTERNET, "page \"Balance  $", NETWORK_GET_VC_BANK_BALANCE(), "\": ", NETWORK_GET_STRING_BANK_BALANCE())
	
		
	// Updating elements
	// Starting slot
	INT i = 4

	IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret > MP_TOTAL_ATM_LOG_ENTRIES-1
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret = 0
	ENDIF
	
	INT iStartAt = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret - 1

	IF iStartAt < 0
		iStartAt = MP_TOTAL_ATM_LOG_ENTRIES - 1
	ENDIF
	
	INT iCoveredCountDown = MP_TOTAL_ATM_LOG_ENTRIES
	
	CPRINTLN(DEBUG_INTERNET, "SF_BANK_TRANSACTIONS_PAGE_MP: iCoveredCountDown = ", iCoveredCountDown)
	WHILE iCoveredCountDown > 0
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 sLogActionType = "", sLogData = ""
		SWITCH g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[iStartAt]
			CASE MALA_WITHDRAW		sLogActionType = "MALA_WITHDRAW" BREAK
			CASE MALA_DEPOSIT		sLogActionType = "MALA_DEPOSIT" BREAK
			CASE MALA_CASH_RECIEVED	sLogActionType = "MALA_CASH_RECIEVED" BREAK
			CASE MALA_CASH_SENT		sLogActionType = "MALA_CASH_SENT" BREAK
			CASE MALA_CASH_BOUGHT	sLogActionType = "MALA_CASH_BOUGHT" BREAK
			CASE MALA_PURCHASE		sLogActionType = "MALA_PURCHASE" BREAK
			CASE MALA_REFUND		sLogActionType = "MALA_REFUND" BREAK
			
			DEFAULT
				sLogActionType  = "MALA_UNKNOWN_"
				sLogActionType += ENUM_TO_INT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[iStartAt])
			BREAK
		ENDSWITCH
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt] != 0
		
			SWITCH g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[iStartAt]
				CASE MALA_CASH_BOUGHT
				CASE MALA_CASH_RECIEVED
				CASE MALA_DEPOSIT
				CASE MALA_REFUND
					sLogData = DEBUG_GET_STRING_FROM_EARN_CATEGORIES(INT_TO_ENUM(EARN_CATEGORIES, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]))
					IF IS_STRING_NULL_OR_EMPTY(sLogData)
						sLogData = DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(INT_TO_ENUM(SPEND_CATEGORIES, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]))
						IF NOT IS_STRING_NULL_OR_EMPTY(sLogData)
							CERRORLN(DEBUG_INTERNET, "spend catagory \"", sLogData, "\" returned for earn catagory")
						ELSE
							sLogData  = "EarnUnknown_"
							sLogData += g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]
						ENDIF
					ENDIF
					BREAK
				CASE MALA_WITHDRAW
				CASE MALA_CASH_SENT
				CASE MALA_PURCHASE
					sLogData = DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(INT_TO_ENUM(SPEND_CATEGORIES, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]))
					IF IS_STRING_NULL_OR_EMPTY(sLogData)
						sLogData = DEBUG_GET_STRING_FROM_EARN_CATEGORIES(INT_TO_ENUM(EARN_CATEGORIES, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]))
						IF NOT IS_STRING_NULL_OR_EMPTY(sLogData)
							CERRORLN(DEBUG_INTERNET, "earn catagory \"", sLogData, "\" returned for spend catagory")
						ELSE
							sLogData  = "SpendUnknown_"
							sLogData += g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]
						ENDIF
					ENDIF
					BREAK
				
				DEFAULT
					sLogData  = "Unknown_"
					sLogData += g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]
					BREAK
			ENDSWITCH
		ELSE
			sLogData = "Uninitialized"
		ENDIF
		CPRINTLN(DEBUG_INTERNET, "SF_BANK_TRANSACTIONS_PAGE_MP: iStartAt[", iStartAt,
				"] LogValues = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogValues[iStartAt],
				" LogSourceNames = \"", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[iStartAt],
				"\" LogActionType = ", sLogActionType,
				" LogData = ", sLogData)
		#ENDIF
		#IF NOT IS_DEBUG_BUILD
		CPRINTLN(DEBUG_INTERNET, "SF_BANK_TRANSACTIONS_PAGE_MP: iStartAt[", iStartAt,
				"] LogValues = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogValues[iStartAt],
				" LogSourceNames = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[iStartAt],
				" LogActionType = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[iStartAt],
				" LogData = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt])
		#ENDIF
		
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogValues[iStartAt] > 0
		AND INT_TO_ENUM(SPEND_CATEGORIES, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]) != MONEY_SPENT_BETTING
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
			
			SWITCH g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[iStartAt]
				CASE MALA_CASH_BOUGHT
				CASE MALA_CASH_RECIEVED
				CASE MALA_DEPOSIT
				CASE MALA_REFUND
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					BREAK
				CASE MALA_WITHDRAW
				CASE MALA_CASH_SENT
				CASE MALA_PURCHASE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					BREAK
			ENDSWITCH

			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogValues[iStartAt])
			SPEND_CATEGORIES s
			EARN_CATEGORIES e

			SWITCH g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[iStartAt]

				CASE MALA_WITHDRAW
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_PLCHLDR_WDR")
				BREAK

				CASE MALA_DEPOSIT
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_PLCHLDR_CAD")
				BREAK

				CASE MALA_CASH_RECIEVED
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MPATM_PLCHLDR_CRF")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[iStartAt])
					END_TEXT_COMMAND_SCALEFORM_STRING()
				BREAK

				CASE MALA_CASH_SENT
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MPATM_PLCHLDR_CST")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[iStartAt])
					END_TEXT_COMMAND_SCALEFORM_STRING()
				BREAK

				CASE MALA_CASH_BOUGHT
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_PLCHLDR_BRT")
				BREAK
				CASE MALA_PURCHASE
					IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt] != 0
						//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_STRING_FROM_HASH_KEY(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]))
						s = INT_TO_ENUM(SPEND_CATEGORIES, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt])
						SWITCH s
							CASE MONEY_SPENT_CONTACT_SERVICE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_CONTACT_SERVICE")
							BREAK
							CASE MONEY_SPENT_PROPERTY_UTIL 
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_PROPERTY_UTIL")
							BREAK
							CASE MONEY_SPENT_JOB_ACTIVITY 
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_JOB_ACTIVITY")
							BREAK
							CASE MONEY_SPENT_BETTING 
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_BETTING")
							BREAK
							CASE MONEY_SPENT_STYLE_ENT
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_STYLE_ENT")
							BREAK
							CASE MONEY_SPENT_HEALTHCARE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_HEALTHCARE")
							BREAK
							CASE MONEY_SPENT_FROM_DEBUG
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_FROM_DEBUG")
							BREAK
							CASE MONEY_SPENT_DROPPED_STOLEN
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_DROPPED_STOLEN")
							BREAK
							CASE MONEY_SPENT_VEH_MAINTENANCE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_VEH_MAINTENANCE")
							BREAK
							CASE MONEY_SPENT_HOLDUPS
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_HOLDUPS")
							BREAK
							CASE MONEY_SPENT_PASSIVEMODE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_PASSIVEMODE")
							BREAK
							//CASE MONEY_SPENT_BANKINTEREST
							//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_BANKINTEREST")
							//BREAK
							
							DEFAULT
								IF s = INT_TO_ENUM(SPEND_CATEGORIES, ENUM_TO_INT(MONEY_EARN_REFUND))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_REFUND")
									BREAK
									
								ELSE
									CASSERTLN(DEBUG_INTERNET, "spend catagory \"", DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(s), "\" missing from transaction log")
									BREAK
								ENDIF
						ENDSWITCH
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_PLCHLDR_PRCH")
					ENDIF
				BREAK
					
				CASE MALA_REFUND

					IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt] != 0
						//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_STRING_FROM_HASH_KEY(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt]))
						e = INT_TO_ENUM(EARN_CATEGORIES,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[iStartAt])
						SWITCH e
							CASE MONEY_EARN_JOBS     
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_JOBS")
								BREAK
							CASE MONEY_EARN_SELLING_VEH  
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_SELLING_VEH")
								BREAK
							CASE MONEY_EARN_BETTING        
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_BETTING")
								BREAK
							CASE MONEY_EARN_GOOD_SPORT     
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_GOOD_SPORT")
								BREAK
							CASE MONEY_EARN_PICKED_UP      
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_PICKED_UP")
								BREAK
							CASE MONEY_EARN_SHARED         
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_SHARED")
								BREAK
							CASE MONEY_EARN_JOBSHARED      
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_JOBSHARED")
								BREAK
							CASE MONEY_EARN_ROCKSTAR_AWARD 
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_ROCKSTAR_AWARD")
								BREAK
							CASE MONEY_EARN_REFUND
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_REFUND")
								BREAK
							CASE MONEY_EARN_FROM_JOB_BONUS
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_JOB_BONUS")
								BREAK
							CASE MONEY_EARN_FROM_HEIST_JOB
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_EARN_HEIST_JOB")
								BREAK
							
							DEFAULT
								IF e = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_PROPERTY_UTIL))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_PROPERTY_UTIL")
									BREAK
								ELIF e = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_STYLE_ENT))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_STYLE_ENT")
									BREAK
								ELIF e = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_CONTACT_SERVICE))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_CONTACT_SERVICE")
									BREAK
								ELIF e = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_HEALTHCARE))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_HEALTHCARE")
									BREAK
								ELIF e = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_VEH_MAINTENANCE))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_VEH_MAINTENANCE")
									BREAK
								ELIF e = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_JOB_ACTIVITY))
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MONEY_SPENT_JOB_ACTIVITY")
									BREAK
									
								ELSE
									CASSERTLN(DEBUG_INTERNET, "earn catagory \"", DEBUG_GET_STRING_FROM_EARN_CATEGORIES(e), "\" missing from transaction log")
									BREAK
								ENDIF
						ENDSWITCH
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_PLCHLDR_REF")
					ENDIF
				BREAK

			ENDSWITCH
			
			END_SCALEFORM_MOVIE_METHOD()
			++i
		ENDIF
		--iStartAt
		IF iStartAt = -1
			iStartAt = MP_TOTAL_ATM_LOG_ENTRIES-1
		ENDIF
		--iCoveredCountDown
	ENDWHILE
	
ENDPROC

/// PURPOSE:
///    Intercept button presses for withdrawals and deposits in case the player has insufficient funds
/// PARAMS:
///    iPressButton - Button index that has been pressed
PROC SF_BANK_MENU_UPDATE_MP(INT iPressedButton)
	CPRINTLN(DEBUG_INTERNET, "SF_BANK_MENU_UPDATE_MP called with press from button ", iPressedButton)

	// Deposit
	IF iPressedButton = 3
		IF DOES_PLAYER_HAVE_MIN_FOR_DEPOSITS()
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_DEPOSIT")
		ELSE
			// Do an error page
			iBankMessageMode = 2 // Deposit
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
		ENDIF
		
	// Withdraw
	ELIF iPressedButton = 4
		IF DOES_PLAYER_HAVE_MIN_FOR_WITHDRAWALS()
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_WITHDRAW")
		ELSE
			// Do an error page
			CPRINTLN(DEBUG_INTERNET, "SF_BANK_MENU_UPDATE_MP found insufficient funds for withdrawal, going to error page")
			iBankMessageMode = 1 // Withdrawal
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
		ENDIF
		
	// Transactions
	ELIF iPressedButton = 5
		GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_TRANSACTIONS")		
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Display the main menu page for multiplayer
PROC SF_BANK_MENU_PAGE_MP(SCALEFORM_INDEX pagemov, BOOL &DisableWithdrawButton)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
	// Account balance
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(NETWORK_GET_STRING_BANK_BALANCE())		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NETWORK_GET_VC_BANK_BALANCE())
	END_SCALEFORM_MOVIE_METHOD()
	
	
	CPRINTLN(DEBUG_INTERNET, "main \"Balance  $", NETWORK_GET_VC_BANK_BALANCE(), "\": ", NETWORK_GET_STRING_BANK_BALANCE())
	
		
	// Name
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))
	END_SCALEFORM_MOVIE_METHOD()
	
	// Choose a service
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_SER")
	END_SCALEFORM_MOVIE_METHOD()
	
	// Deposit
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_DIDM")
	END_SCALEFORM_MOVIE_METHOD()

	// Withdraw
	IF DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(0))
		DisableWithdrawButton = FALSE
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_WITM")
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		//disable withdraw - doesnt have enough money
		DisableWithdrawButton = TRUE
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_WITM")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Transaction log
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_LOG")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Update the withdrawal page state ready for the render calls
/// PARAMS:
///    iPressButton - index of the button that was pressed
PROC SF_BANK_WITHDRAWAL_UPDATE_MP(INT iPressButton)
	CPRINTLN(DEBUG_INTERNET, "SF_BANK_WITHDRAWAL_UPDATE_MP called with press from button ", iPressButton)
	
	// Check the account has sufficient funds for the selected button
	IF iPressButton > 2 AND iPressButton < 9
		IF DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(iPressButton-3))
			iBankMessageMode = 1 // Withdrawal
			iBankDWStep = 1 // Go to confirm
			iBankDWAmount = GET_VALUE_FOR_BUTTON(iPressButton-3)
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_MESSAGE")
		ELSE
			// Do an error page
			iBankMessageMode = 1 // Withdrawal
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the bank website multiplayer withdrawal page
/// PARAMS:
///    pagemov - current page movie
PROC SF_BANK_WITHDRAWAL_PAGE_MP(SCALEFORM_INDEX pagemov)

	// Check whether there are sufficient funds for minimum withdrawal
	IF DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(0))
	
		// OK to render buttons
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		// Account balance
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(NETWORK_GET_STRING_BANK_BALANCE())	//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NETWORK_GET_VC_BANK_BALANCE())
		END_SCALEFORM_MOVIE_METHOD()
		
		// Player name
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))
		END_SCALEFORM_MOVIE_METHOD()
		
		// Page title
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_WITMT")
		END_SCALEFORM_MOVIE_METHOD()
		
		INT iBCount
		FOR iBCount = 0 TO 5
			IF DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(iBCount))
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBCount+3)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ESDOLLA")
						ADD_TEXT_COMPONENT_FORMATTED_INTEGER(GET_VALUE_FOR_BUTTON(iBCount), INTEGER_FORMAT_COMMA_SEPARATORS)	
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDFOR
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_BACK")
		END_SCALEFORM_MOVIE_METHOD()
		
		CPRINTLN(DEBUG_INTERNET, "withdraw \"Balance  $", NETWORK_GET_VC_BANK_BALANCE(), "\": ", NETWORK_GET_STRING_BANK_BALANCE())
		
	ELSE
		// Insufficient funds - go to error screen
		iBankMessageMode = 2
		GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
	ENDIF		
ENDPROC

/// PURPOSE:
///    Update the deposit stage state ready for the render
/// PARAMS:
///    iPressButton - index of the button that was pressed
PROC SF_BANK_DEPOSIT_UPDATE_MP(INT iPressButton)
	CPRINTLN(DEBUG_INTERNET, "SF_BANK_DEPOSIT_UPDATE_MP called with press from button ", iPressButton)
	// Check the wallet has sufficient funds for the selected button
	IF iPressButton > 2 AND iPressButton < 9
		IF DOES_WALLET_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(iPressButton-3, TRUE))
			iBankMessageMode = 2 // Deposit
			iBankDWStep = 1 // Go to confirm
			iBankDWAmount = GET_VALUE_FOR_BUTTON(iPressButton-3, TRUE)
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_MESSAGE")
		ELSE
			// Do an error page
			iBankMessageMode = 2 // Deposit
			GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle displaying the bank website multiplayer deposit page
/// PARAMS:
///    pagemov - 
PROC SF_BANK_DEPOSIT_PAGE_MP(SCALEFORM_INDEX pagemov)

	// Check whether player has sufficient funds
	IF DOES_WALLET_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(0, TRUE))
		
		// OK to render button screen
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		// Account balance
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(NETWORK_GET_STRING_BANK_BALANCE())	//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NETWORK_GET_VC_BANK_BALANCE())
		END_SCALEFORM_MOVIE_METHOD()
		
		// Player name
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))
		END_SCALEFORM_MOVIE_METHOD()
		
		// Page title
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_DITMT")
		END_SCALEFORM_MOVIE_METHOD()
		
		INT iBCount
		FOR iBCount = 0 TO 5
			IF DOES_WALLET_HAVE_ENOUGH_FUNDS(GET_VALUE_FOR_BUTTON(iBCount, TRUE))
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBCount+3)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ESDOLLA")
						ADD_TEXT_COMPONENT_FORMATTED_INTEGER(GET_VALUE_FOR_BUTTON(iBCount, TRUE),INTEGER_FORMAT_COMMA_SEPARATORS)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDFOR
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_BACK")
		END_SCALEFORM_MOVIE_METHOD()
	
		
		CPRINTLN(DEBUG_INTERNET, "deposit \"Balance  $", NETWORK_GET_VC_BANK_BALANCE(), "\": ", NETWORK_GET_STRING_BANK_BALANCE())
		
	ELSE
		
		// Insufficient funds - go to error screen
		iBankMessageMode = 2
		GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
		
	ENDIF

ENDPROC

TYPEDEF PROC RenderBrowser(BOOL allowphone = FALSE, BOOL bPropertyVersion = FALSE)//, BOOL forceButtons = FALSE)

/// PURPOSE:
///    Update the message screen correctly for each message mode, depending on whether we're at the confirm amount or transaction complete stage
/// PARAMS:
///    iPressedButton - The index of the button pressed
PROC SF_BANK_MESSAGE_UPDATE_MP(SCALEFORM_INDEX pagemov, INT iPressedButton, RenderBrowser customRenderBrowser)
	
	CPRINTLN(DEBUG_INTERNET, "SF_BANK_MESSAGE_UPDATE_MP called with press from button ", iPressedButton, " while in message mode ", iBankMessageMode, " step ", iBankDWStep)
	
	SWITCH iBankMessageMode
		
		// Withdrawing ------------
		CASE 1 
			SWITCH iBankDWStep
				
				CASE 1 // Confirm
					// See if player wants to go ahead with the deposit
					IF iPressedButton = 0 // YES
						// Go to delay and complete
						IF DOES_ACCOUNT_HAVE_ENOUGH_FUNDS(iBankDWAmount)
							
							IF USE_SERVER_TRANSACTIONS()
								IF NET_GAMESERVER_TRANSACTION_IN_PROGRESS()
									CPRINTLN(DEBUG_FINANCE, "CONVERT_VC_TO_FM($",iBankDWAmount,") error three (NET_GAMESERVER_TRANSACTION_IN_PROGRESS)")
									iBankMessageMode = 3 // Network error
									GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
									EXIT
								ENDIF
								
								INT currentState, refreshSessionRequested
								NET_GAMESERVER_GET_SESSION_STATE_AND_STATUS(currentState, refreshSessionRequested)
								IF (INT_TO_ENUM(GAMESERVER_STATES, currentState) != GAMESERVER_READY)
									CPRINTLN(DEBUG_FINANCE, "CONVERT_VC_TO_FM($",iBankDWAmount,") error four (currentState[", currentState, "] != GAMESERVER_READY)")
									iBankMessageMode = 3 // Network error
									GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
									EXIT
								ENDIF
								
								IF NET_GAMESERVER_TRANSFER_BANK_TO_WALLET(GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), iBankDWAmount)
									NET_GAMESERVER_TRANSACTION_STATUS eNSTS
									eNSTS = NET_GAMESERVER_TRANSFER_BANK_TO_WALLET_GET_STATUS()
									
									#IF IS_DEBUG_BUILD
									INT iWaiting
									#ENDIF
									WHILE (eNSTS = TRANSACTION_STATUS_PENDING) 
										eNSTS = NET_GAMESERVER_TRANSFER_WALLET_TO_BANK_GET_STATUS()
										
										CALL customRenderBrowser()
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: waiting ", iWaiting)
										iWaiting++
										#ENDIF
										
										WAIT(0)
									ENDWHILE
									
									IF eNSTS = TRANSACTION_STATUS_SUCCESSFULL
										CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: success [", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "] !!!")
										NET_GAMESERVER_TRANSFER_CASH_SET_TELEMETRY_NONCE_SEED()
										//
									ELSE
										#IF IS_DEBUG_BUILD
										SWITCH eNSTS
											CASE TRANSACTION_STATUS_NONE
												CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error two (TRANSACTION_STATUS_NONE != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_PENDING
												CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error two (TRANSACTION_STATUS_PENDING != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_FAILED
												CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error two (TRANSACTION_STATUS_FAILED != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_SUCCESSFULL
												CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error two (TRANSACTION_STATUS_SUCCESSFULL != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_CANCELED
												CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error two (TRANSACTION_STATUS_CANCELED != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											DEFAULT
												CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error two (eNSTS_", eNSTS, " != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
										ENDSWITCH
										#ENDIF
										
										iBankMessageMode = 3 // Network error
										GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
										EXIT
									ENDIF
								ELSE
									CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: error one (not NET_GAMESERVER_TRANSFER_BANK_TO_WALLET[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
									iBankMessageMode = 2 // Deposit
									GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
									EXIT
								ENDIF
							ELSE
								CPRINTLN(DEBUG_INTERNET, "CONVERT_VC_TO_FM: ignore NET_GAMESERVER_TRANSFER_BANK_TO_WALLET")
							ENDIF
						
							WITHDRAW_VC(iBankDWAmount)
							IF (g_bInMultiplayer) AND NETWORK_IS_GAME_IN_PROGRESS()
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_INTERNET_BANK_BALANCE,1)
							ENDIF
							iBankDWStep = 2
							
							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
							END_SCALEFORM_MOVIE_METHOD()
							
							// Page title
							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_TRANCOM")
							END_SCALEFORM_MOVIE_METHOD()

							// YES button
							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_MENU")	//Menu
							END_SCALEFORM_MOVIE_METHOD()

							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "UPDATE_TEXT")
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
					ELIF iPressedButton = 1 // NO
						// Back to main menu
						GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_MAINMENU")
					ENDIF
				BREAK
				
				CASE 2 // Complete
					// Back to main
					IF iPressedButton = 0 // MENU
						// Back to main menu
						GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_MAINMENU")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		// Depositing  ------------
		CASE 2 
			SWITCH iBankDWStep
				CASE 1 // Confirm
					IF iPressedButton = 0
						// Go to delay and complete
						IF DOES_WALLET_HAVE_ENOUGH_FUNDS(iBankDWAmount)
	
							IF USE_SERVER_TRANSACTIONS()
								IF NET_GAMESERVER_TRANSACTION_IN_PROGRESS()
									CPRINTLN(DEBUG_FINANCE, "CONVERT_FM_TO_VC($",iBankDWAmount,") error three (NET_GAMESERVER_TRANSACTION_IN_PROGRESS)")
									iBankMessageMode = 3 // Network error
									GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
									EXIT
								ENDIF
								
								INT currentState, refreshSessionRequested
								NET_GAMESERVER_GET_SESSION_STATE_AND_STATUS(currentState, refreshSessionRequested)
								IF (INT_TO_ENUM(GAMESERVER_STATES, currentState) != GAMESERVER_READY)
									CPRINTLN(DEBUG_FINANCE, "CONVERT_FM_TO_VC($",iBankDWAmount,") error four (currentState[", currentState, "] != GAMESERVER_READY)")
									iBankMessageMode = 3 // Network error
									GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
									EXIT
								ENDIF
								
								IF NET_GAMESERVER_TRANSFER_WALLET_TO_BANK(GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), iBankDWAmount)
									NET_GAMESERVER_TRANSACTION_STATUS eNSTS
									eNSTS = NET_GAMESERVER_TRANSFER_WALLET_TO_BANK_GET_STATUS()
									
									#IF IS_DEBUG_BUILD
									INT iWaiting
									#ENDIF
									WHILE (eNSTS = TRANSACTION_STATUS_PENDING) 
										eNSTS = NET_GAMESERVER_TRANSFER_WALLET_TO_BANK_GET_STATUS()
										
										CALL customRenderBrowser()
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: waiting ", iWaiting)
										iWaiting++
										#ENDIF
										
										WAIT(0)
									ENDWHILE
									
									IF eNSTS = TRANSACTION_STATUS_SUCCESSFULL
										CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: success [", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "]!!!")
										NET_GAMESERVER_TRANSFER_CASH_SET_TELEMETRY_NONCE_SEED()
										//
									ELSE
										#IF IS_DEBUG_BUILD
										SWITCH eNSTS
											CASE TRANSACTION_STATUS_NONE
												CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error two (TRANSACTION_STATUS_NONE != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_PENDING
												CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error two (TRANSACTION_STATUS_PENDING != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_FAILED
												CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error two (TRANSACTION_STATUS_FAILED != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_SUCCESSFULL
												CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error two (TRANSACTION_STATUS_SUCCESSFULL != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											CASE TRANSACTION_STATUS_CANCELED
												CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error two (TRANSACTION_STATUS_CANCELED != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
											DEFAULT
												CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error two (eNSTS_", eNSTS, " != TRANSACTION_STATUS_SUCCESSFULL[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
											BREAK
										ENDSWITCH
										#ENDIF
										
										iBankMessageMode = 3 // Network error
										GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
										EXIT
									ENDIF
								ELSE
									CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: error one (not NET_GAMESERVER_TRANSFER_BANK_TO_WALLET[", GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR), ", $", iBankDWAmount, "])")
									iBankMessageMode = 2 // Deposit
									GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_ERROR")
									EXIT
								ENDIF
							ELSE
								CPRINTLN(DEBUG_INTERNET, "CONVERT_FM_TO_VC: ignore NET_GAMESERVER_TRANSFER_WALLET_TO_BANK")
							ENDIF
							
							DEPOSIT_VC(iBankDWAmount)
							IF (g_bInMultiplayer) AND NETWORK_IS_GAME_IN_PROGRESS()
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_INTERNET_BANK_BALANCE,1)
							ENDIF
							iBankDWStep = 2
							
							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
							END_SCALEFORM_MOVIE_METHOD()
							
							// Page title
							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_TRANCOM")
							END_SCALEFORM_MOVIE_METHOD()

							// MENU button
							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_MENU")	//Menu
							END_SCALEFORM_MOVIE_METHOD()

							BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "UPDATE_TEXT")
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
					ELIF iPressedButton = 1 // NO
						// Back to main menu
						GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_MAINMENU")
					ENDIF
				BREAK
				CASE 2 // Complete
					// Back to main
					IF iPressedButton = 0 // MENU
						// Back to main menu
						GO_TO_WEBSITE("WWW_MAZE_D_BANK_COM_S_MAINMENU")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Set data slots to display correct info based on current "message mode"
PROC SF_BANK_MESSAGE_PAGE_MP(SCALEFORM_INDEX pagemov)

	CPRINTLN(DEBUG_INTERNET, "SF_BANK_MESSAGE_PAGE_MP called, message mode ", iBankMessageMode, " step ", iBankDWStep)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()

	SWITCH iBankMessageMode
		
		// Withdrawing ------------
		CASE 1 
			SWITCH iBankDWStep
				
				CASE 1 // Confirm
					// Amount confirm message
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MPATC_CONFW")
							ADD_TEXT_COMPONENT_FORMATTED_INTEGER(iBankDWAmount, INTEGER_FORMAT_COMMA_SEPARATORS)
						END_TEXT_COMMAND_SCALEFORM_STRING()						
					END_SCALEFORM_MOVIE_METHOD()
					
					// YES button
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MO_YES")
					END_SCALEFORM_MOVIE_METHOD()
					
					// NO button
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MO_NO")
					END_SCALEFORM_MOVIE_METHOD()
				BREAK
				
				CASE 2 // Complete
					// Slots are set from update handler
				BREAK
			ENDSWITCH
		BREAK
		
		// Depositing  ------------
		
		CASE 2 
			SWITCH iBankDWStep
				CASE 1 // Confirm
					
					// Amount confirm message
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MPATM_CONF")
							ADD_TEXT_COMPONENT_FORMATTED_INTEGER(iBankDWAmount, INTEGER_FORMAT_COMMA_SEPARATORS)
						END_TEXT_COMMAND_SCALEFORM_STRING()	
					END_SCALEFORM_MOVIE_METHOD()
					
					// YES button
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MO_YES")
					END_SCALEFORM_MOVIE_METHOD()
					
					// NO button
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MO_NO")
					END_SCALEFORM_MOVIE_METHOD()
				BREAK
				
				CASE 2 // Complete
					// Slots are set from update handler
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Bank error page - display an error message depending on which message mode is active
PROC SF_BANK_ERROR_MP(SCALEFORM_INDEX pagemov)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
	SWITCH iBankMessageMode
		// Withdrawal error
		CASE 1
			// Insufficient funds to withdraw
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_NODO2")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		// Depositing error
		CASE 2
			// Insufficient funds to deposit
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_NODO")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		// Network error
		CASE 3
			// A network error occurred and your transaction could not be completed.
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_ERR")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Single-player bank page update
PROC SF_BANK_UPDATE(SCALEFORM_INDEX pagemov, INT pageID)

	SWITCH pageID
	
		/*
		Page IDs:

	    1 "PAGE1"
	    2 "TRANSACTIONS"
	    3 "MAINMENU"
	    4 "WITHDRAW"
	    5 "DEPOSIT"
	    6 "ERROR"
	    7 "MAINTENANCE" 
		*/
		
		CASE 1
			// WELCOME SCREEN
			// Automatic in single player
		BREAK
		
		CASE 2
			// TRANSACTIONS
			SF_BANK_TRANSACTIONS_PAGE(pagemov)
		BREAK
		
		CASE 3
			// MAINMENU
			// Page not accessible in singleplayer
		BREAK
		
		CASE 4
			// WITHDRAW
			// Page not accessible in singleplayer
		BREAK
		
		CASE 5
			// DEPOSIT
			// Page not accessible in singleplayer
		BREAK
	ENDSWITCH
		
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Multiplayer bank update for www.maze-bank.com
PROC SF_BANK_UPDATE_MP(SCALEFORM_INDEX pagemov, INT pageID)
	
	BOOL DisableWithdrawButton = FALSE
	
	SWITCH pageID
	
		/*
		Page IDs:

	    1 "PAGE1"
	    2 "TRANSACTIONS"
	    3 "MAINMENU"
	    4 "WITHDRAW"
	    5 "DEPOSIT"
	    6 "ERROR"
	    7 "MAINTENANCE"
		8 "MESSAGE"
		*/
		
		CASE 1
			// WELCOME SCREEN
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE 2
			// TRANSACTIONS
			SF_BANK_TRANSACTIONS_PAGE_MP(pagemov)
		BREAK
		
		CASE 3
			// MAINMENU
			iBankDWStep = 0
			iBankMessageMode = -1
			SF_BANK_MENU_PAGE_MP(pagemov, DisableWithdrawButton)
		BREAK
		
		CASE 4
			// WITHDRAW
			SF_BANK_WITHDRAWAL_PAGE_MP(pagemov)
		BREAK
		
		CASE 5
			// DEPOSIT
			SF_BANK_DEPOSIT_PAGE_MP(pagemov)
		BREAK
		
		CASE 6
			// ERROR
			SF_BANK_ERROR_MP(pagemov)
		BREAK
		
		CASE 7
			// MAINTENANCE
		BREAK
		
		CASE 8
			// MESSAGE
			SF_BANK_MESSAGE_PAGE_MP(pagemov)			
		BREAK
		
	ENDSWITCH
		
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
	
	
	// Withdraw
	IF DisableWithdrawButton
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPATM_WITM")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

//********************************************************************************
//*****************************BAWSAQ Elements************************************
//********************************************************************************

PROC SF_BS_FILL_GRAPH_FOR_ONLINE_STOCK(SCALEFORM_INDEX pagemov, INT listing, INT dayslot, INT dataslot)

	IF (NOT NETWORK_IS_SIGNED_ONLINE())
	OR (NOT NETWORK_HAS_VALID_ROS_CREDENTIALS())
		EXIT
	ENDIF
	
	IF g_BS_Listings[listing].bOnlineStock != TRUE
		EXIT
	ENDIF
	IF g_BS_Listings[listing].StatIndex = -1
		EXIT
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(dayslot)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_6")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_5")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_4")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_3")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_2")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_1")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_0")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.5)
	END_SCALEFORM_MOVIE_METHOD()
	
	FLOAT fPrice = GET_CURRENT_STOCK_PRICE(listing)
	//Recalculate average for price filter effect accuracy
	FLOAT ave = fPrice
	
	IF fPrice > g_BS_Listings[listing].fLoggedHighValue
		g_BS_Listings[listing].fLoggedHighValue = fPrice
	ENDIF
	IF fPrice < g_BS_Listings[listing].fLoggedLowValue
		g_BS_Listings[listing].fLoggedLowValue = fPrice
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(dataslot)
	
	INT i = 0
	FLOAT f = 0.0		
	BOOL bDoBlend = false
	IF fPrice != g_BS_Listings[i].fCurrentPrice	
		bDoBlend = true
	ENDIF
	
	FOR i=0 to 4//4 TO 0 STEP -1 
		STAT_COMMUNITY_GET_HISTORY(g_BS_OIndexData[g_BS_Listings[listing].StatIndex].OnlinePriceStat, i, f)							
		ave+=f
		IF bDoBlend
			FLOAT priceprop = i*0.2
			FLOAT blend = ((1.0-priceprop)*f) + (priceprop*fPrice)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(blend)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f)
		ENDIF
	ENDFOR
	
	//Store newly calculated average
	ave = ave / 6
	g_BS_Listings[listing].fLoggedPriceChangeFromWeeklyAverage = fPrice - ave
	g_BS_Listings[listing].fLoggedPriceChangeInPercent = (g_BS_Listings[listing].fLoggedPriceChangeFromWeeklyAverage/ave)*100
	
	//Add current price
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fPrice)
	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SF_BS_FILL_GRAPH_FOR_STOCK(SCALEFORM_INDEX pagemov, INT listing, INT dayslot, INT dataslot)

	IF g_BS_Listings[listing].bOnlineStock
			SF_BS_FILL_GRAPH_FOR_ONLINE_STOCK(pagemov, listing, dayslot, dataslot)
		EXIT
	ENDIF
	
	//37 Strings: Horizontal graph labels (eg. "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")
	INT i = 0
	INT iDay = ENUM_TO_INT(GET_CLOCK_DAY_OF_WEEK())+2
	IF iDay > 6
		iDay -= 7
	ENDIF
	//SUNDAY = 0,
	//MONDAY,
	//TUESDAY,
	//WEDNESDAY,
	//THURSDAY,
	//FRIDAY,
	//SATURDAY

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(dayslot)
		
	// Figure out what day it currently is, then start from that and upload labels for all the days
	// Might need to go backwards as the ordering is probably left-right
	REPEAT 7 i
		
		SWITCH iDay
			CASE 0
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_SU")
			BREAK
			CASE 1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_M")
			BREAK
			CASE 2
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_TU")
			BREAK
			CASE 3
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_W")
			BREAK
			CASE 4
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_TH")
			BREAK
			CASE 5
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_F")
			BREAK
			CASE 6
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_SA")
			BREAK
		ENDSWITCH

		iDay++
		
		IF iDay > 6
			iDay = 0
		ENDIF
	ENDREPEAT
			
	// Noise value
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	END_SCALEFORM_MOVIE_METHOD()
		
	//38 Up to 20 Numbers: dataset for graph (eg. 1437, 6549, 765, 3424 , 655, 7786, 3532, 684, etc)
	//g_BS_Listings[coindex].LoggedPrices[]
	//MAX_STOCK_PRICE_LOG_ENTRIES
	
	CDEBUG1LN(DEBUG_LCN, "Updating LCN graph data...")
		
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(dataslot)
		
		FOR i = 0 TO  (MAX_STOCK_PRICE_LOG_ENTRIES-1)// TO 0 STEP -1 
			INT iLogIndex = g_BS_Listings[listing].iLogIndexCaret + i
			IF iLogIndex >= MAX_STOCK_PRICE_LOG_ENTRIES
				iLogIndex -= MAX_STOCK_PRICE_LOG_ENTRIES
			ENDIF

			FLOAT fGraphValue = g_BS_Listings[listing].LoggedPrices[iLogIndex]
			
			// Log value.
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fGraphValue)
			CDEBUG3LN(DEBUG_LCN, "[Data point ", i, "] Value:", fGraphValue, " LogIndex:", iLogIndex, ".")
		ENDFOR
		
	END_SCALEFORM_MOVIE_METHOD()
	
	CDEBUG1LN(DEBUG_LCN, "...LCN graph data updated.")
ENDPROC


FUNC INT FIND_LOWEST_VALUE_INDEX_IN_TOP_FIVE(INT &paramTopFive[5])
	INT iLowestValueIndex = 0
	FLOAT fLowestValue = ABSF(g_BS_Listings[paramTopFive[0]].fLoggedPriceChangeFromWeeklyAverage)
	
	INT i
	REPEAT 5 i
		FLOAT fCurrentValue = ABSF(g_BS_Listings[paramTopFive[i]].fLoggedPriceChangeFromWeeklyAverage)
		IF fLowestValue > fCurrentValue
			iLowestValueIndex = i
			fLowestValue = fCurrentValue
		ENDIF
	ENDREPEAT
	
	RETURN iLowestValueIndex
ENDFUNC


#IF IS_DEBUG_BUILD
	PROC PRINT_TOP_FIVE(INT &paramTopFive[5])
		CDEBUG3LN(DEBUG_STOCKS, "--TOP5--")
		INT i
		REPEAT 5 i
			CDEBUG3LN(DEBUG_STOCKS, "[", i, "] Index:", paramTopFive[i], " Value:", g_BS_Listings[paramTopFive[i]].fLoggedPriceChangeFromWeeklyAverage)
		ENDREPEAT
		CDEBUG3LN(DEBUG_STOCKS, "--------")
	ENDPROC
#ENDIF


PROC SF_BS_TOP_FIVE(SCALEFORM_INDEX pagemov)

	// Find the top five
	INT iTopFive[5]
	INT i = 0
	INT j = 0
	
	//Put the first 5 online stocks we find in our list.
	WHILE j < 5
		IF g_BS_Listings[i].fCurrentPrice > 0
			IF g_BS_Listings[i].bOnlineStock = g_bOnlineMarket
				iTopFive[j] = i
				j++
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_STOCKS, "Failed to add listing for initial 5 stocks, current price is less than 0 for listing index: ", i)
		ENDIF
		i++
	ENDWHILE
	
	INT iLowestValueIndex = FIND_LOWEST_VALUE_INDEX_IN_TOP_FIVE(iTopFive)
	FLOAT fLowestValue = ABSF(g_BS_Listings[iTopFive[iLowestValueIndex]].fLoggedPriceChangeFromWeeklyAverage)
	
	CDEBUG2LN(DEBUG_STOCKS, "Starting search for top 5.")
	#IF IS_DEBUG_BUILD
		PRINT_TOP_FIVE(iTopFive)
	#ENDIF
	
	//Search through all listings
	REPEAT BS_CO_TOTAL_LISTINGS i
		
		IF g_BS_Listings[i].fCurrentPrice > 0
			IF g_BS_Listings[i].bOnlineStock = g_bOnlineMarket
			
				BOOL bNotInTopFive = TRUE
				REPEAT 5 j
					IF iTopFive[j] = i
						bNotInTopFive = FALSE
					ENDIF
				ENDREPEAT

				IF bNotInTopFive
					FLOAT fCurrentValue = ABSF(g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage)
					
					CDEBUG2LN(DEBUG_STOCKS, "Comparing Index:", i, " Value:", fCurrentValue, ".")
					
					//If the average change of this stock is greater than the lowest in the top 5 then replace the lowest of the current top 5.
					IF fCurrentValue > fLowestValue
					
						CDEBUG2LN(DEBUG_STOCKS, "Checked stock is greater than current lowest.")
					
						iTopFive[iLowestValueIndex] = i
						
						//Now find the new lowest in the top 5.
						iLowestValueIndex = FIND_LOWEST_VALUE_INDEX_IN_TOP_FIVE(iTopFive)
						fLowestValue = ABSF(g_BS_Listings[iTopFive[iLowestValueIndex]].fLoggedPriceChangeFromWeeklyAverage)
						
						CDEBUG2LN(DEBUG_STOCKS, "New lowest:", fLowestValue, ". New lowest index:", iLowestValueIndex)
						
						#IF IS_DEBUG_BUILD
							PRINT_TOP_FIVE(iTopFive)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_STOCKS, "Failed to check listing for top 5, current price is less than 0 for listing index: ", i)
		ENDIF
	ENDREPEAT
	
	CDEBUG2LN(DEBUG_STOCKS, "Starting sort of top 5.")
	
	//We've found the top 5. Now sort them.
	BOOL bFinished
	WHILE NOT bFinished
		bFinished = TRUE
		REPEAT 5 i
			REPEAT 5 j
				IF i > j
					IF ABSF(g_BS_Listings[iTopFive[i]].fLoggedPriceChangeFromWeeklyAverage) > ABSF(g_BS_Listings[iTopFive[j]].fLoggedPriceChangeFromWeeklyAverage)
						INT iSwap = iTopFive[i]
						iTopFive[i] = iTopFive[j]
						iTopFive[j] = iSwap
						bFinished = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		PRINT_TOP_FIVE(iTopFive)
	#ENDIF
	
	//Finally pass them to scaleform.

	/*Top Five Stocks:
    Slot ID: Number
    Text: String for company abreviation
    Number: Price
    Number: Change 

	22            1 String & 2 Numbers: (eg. "BRU", 212.69, 15.72)
	23            1 String & 2 Numbers: (eg. "VAP", 45.45, -2.3)
	24            1 String & 2 Numbers: (eg. "MAI", 215.01, 7.14)
	25            1 String & 2 Numbers: (eg. "PFI", 12.6, 25.25)
	26            1 String & 2 Numbers: (eg. "HVY", 185.12. -3.13)
	*/
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(22)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING( g_BS_Listings[iTopFive[0]].sShortName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( GET_CURRENT_STOCK_PRICE(iTopFive[0]) )
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( g_BS_Listings[iTopFive[0]].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(23)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING( g_BS_Listings[iTopFive[1]].sShortName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_CURRENT_STOCK_PRICE(iTopFive[1])  )
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( g_BS_Listings[iTopFive[1]].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(24)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING( g_BS_Listings[iTopFive[2]].sShortName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( GET_CURRENT_STOCK_PRICE(iTopFive[2]) )
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( g_BS_Listings[iTopFive[2]].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(25)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING( g_BS_Listings[iTopFive[3]].sShortName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( GET_CURRENT_STOCK_PRICE(iTopFive[3])  )
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( g_BS_Listings[iTopFive[3]].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(26)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING( g_BS_Listings[iTopFive[4]].sShortName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( GET_CURRENT_STOCK_PRICE(iTopFive[4])  )
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( g_BS_Listings[iTopFive[4]].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Now do graph for the number one slot on the top 5.
	SF_BS_FILL_GRAPH_FOR_STOCK(pagemov, iTopFive[0],27,28)

ENDPROC

/// PURPOSE:
///    Fill out listings for any bawsaq page that requires it
/// PARAMS:
///    indexOffset - the element index to start filling out entries from, defaults to 11
PROC SF_BS_ALL_LISTINGS(SCALEFORM_INDEX pagemov,INT indexOffset = 15)

	g_iLastSetFinanceSiteListOffset = indexOffset
	
	//The stock list starts from slot 11 onwards. A minimum of 6 list items fills the minimum height of a page (1 screen).

	FLOAT fStartAt = TO_FLOAT(indexOffset)

	//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",fStartAt,
	//														-1,-1,-1,-1,
	//														"BS_WB_NAMEH")									
	//fStartAt += 1.0
	
	//FOR EACH STOCK FILL AN ENTRY
	/*
	BAWSAQ_LISTING_STRUCT g_BS_Listings[BS_CO_TOTAL_LISTINGS]

	BAWSAQ_PORTFOLIO_ENTRY_STRUCT g_BS_Portfolios[BS_TR_TOTAL][MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	*/
	
	INT i = 0
	INT iAt = 0
	REPEAT BS_CO_TOTAL_LISTINGS i
		IF g_BS_Listings[i].fCurrentPrice > 0
			IF g_bOnlineMarket = g_BS_Listings[i].bOnlineStock
			
				/*
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",i + fStartAt,//  * Slot ID: Number
																g_BS_Listings[i].fCurrentPrice,//* Current Value: Number
																TO_FLOAT(g_BS_Listings[i].iVolumeTraded),//* Volume: Number
																g_BS_Listings[i].fLastPriceChange,//* Change Value: Number
																-1,//skip
																g_BS_Listings[i].sShortName,//* Abreviated Company Name: String
																g_BS_Listings[i].sLongName)//* Full Company Name: String 
				*/
				
				
																
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(iAt + fStartAt)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_CURRENT_STOCK_PRICE(i))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(55555)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[i].sShortName)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[i].sLongName)
				END_SCALEFORM_MOVIE_METHOD()
																											
				//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov, "APPEND_DATA_SLOT", i + fStartAt,g_BS_Listings[i].fLastPriceChangeInPercent)
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"APPEND_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(iAt + fStartAt)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[i].fLoggedPriceChangeInPercent)
				END_SCALEFORM_MOVIE_METHOD()
				++iAt
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_STOCKS, "Invalid current price for listing index: ", i)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Fill currently owned shares for any bawsaq page that requires it for the current player
/// PARAMS:
///    indexOffset - the element index to start filling out entries from, defaults to 11
PROC SF_BS_OWNED_LISTINGS(SCALEFORM_INDEX pagemov,INT indexOffset = 11)
	
	g_iLastSetFinanceSiteListOffset = indexOffset
	//BAWSAQ_PORTFOLIO_ENTRY_STRUCT g_BS_Portfolios[BS_TR_TOTAL][MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	
	//default to mike for now
	BAWSAQ_TRADERS pickeduser = BS_TR_MIKE
														
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			pickeduser = BS_TR_MIKE
			BREAK
		CASE CHAR_TREVOR
			pickeduser = BS_TR_TREVOR
			BREAK
		CASE CHAR_FRANKLIN
			pickeduser = BS_TR_FRANKLIN
			BREAK
	ENDSWITCH

	/*
	Find the correct owner to use
	BS_TR_MIKE,
	BS_TR_FRANKLIN,
	BS_TR_TREVOR,
	*/

	FLOAT fStartAt = TO_FLOAT(indexOffset)
	INT i = 0
	BOOL bTripped = FALSE
	
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		
		IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i) > 0
		//IF g_BS_Listings[GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i)].bOnlineStock = g_bOnlineMarket		
		
			bTripped = TRUE
			
			//g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)
			/*
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",fStartAt,//  * Slot ID: Number
															g_BS_Listings[g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)].fCurrentPrice,//* Current Value: Number // maybe change to current total value
															TO_FLOAT(g_BS_Listings[g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)].iVolumeTraded),//* Volume: Number // maybe change this to number owned
															g_BS_Listings[g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)].fLastPriceChange,//* Change Value: Number
															INVALID_SCALEFORM_PARAM,//skip
															g_BS_Listings[g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)].sShortName,//* Abreviated Company Name: String
															g_BS_Listings[g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)].sLongName)//* Full Company Name: String */
															//g_BS_Listings[g_BS_Portfolios[pickeduser][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)].fCurrentPrice
			
			INT iCompany = ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i))
			FLOAT fCurrentLoggedPrice = GET_CURRENT_STOCK_PRICE(iCompany)
			IF fCurrentLoggedPrice > 0
				FLOAT fProf = (fCurrentLoggedPrice * GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i))
				FLOAT fProfPerc = 0
				IF GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i) > 0
					fProfPerc =((fProf/GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i))*100.0) - 100.0
				ENDIF
				fProf -= GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i)
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(66666)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fProfPerc)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i)].sShortName)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i)].sLongName)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(pickeduser),i))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fProf)
				END_SCALEFORM_MOVIE_METHOD()

				/*
				SLOT ID,
				current value,
				volume,
				change value, 
				abreviated company name, 
				full company name,
				buy price, 
				profit lost
				*/
				
				//APPEND_DATA_SLOT(slotID:Number, My Buy Price Value:Number, Profit Lose Value:Number)
				//SLOT ID, my buy price value, profit lose value
				//slotID, 10.50, 12.33

				CPRINTLN(DEBUG_INTERNET, "Filling out portfolio index : ", FLOOR(fStartAt), " from portfolio index: ", i)
				++fStartAt
			ELSE
				CPRINTLN(DEBUG_INTERNET, "Failed to fill out portfolio entry for portfolio index: ", i)
			ENDIF
		//ENDIF
		ENDIF
	ENDREPEAT
	
	IF bTripped = FALSE
		//display fail listing if no entries
		CPRINTLN(DEBUG_INTERNET, "No entries in portfolio!!\n")
	ENDIF
ENDPROC

/// PURPOSE:
///    Fills out the button titles for the stock pages
PROC SF_BS_BUTTONBLOCK(SCALEFORM_INDEX pagemov,int indexOffset = 0 )

/*
	//bSkipHome
    String: button for page1 (eg. "Home")BS_WB_HOME
	
    String: button for news page (eg. "News")BS_WB_NEWS
    String: button for markets stocks listing (eg. "Markets")BS_WB_MARK
    String: button for players stock portfolio (eg. "My Stocks")BS_WB_MSTOCK
*/
	
	//SCRIPT_ASSERT("Buttonblock")
	FLOAT fStartAt = TO_FLOAT(indexOffset)

	/*CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",fStartAt,
															-1,-1,-1,-1,
															"BS_WB_HOME")*/
																										
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HOME")
	END_SCALEFORM_MOVIE_METHOD()
			
	fStartAt += 1.0

	//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",fStartAt,
	//														-1,-1,-1,-1,
	//														"BS_WB_NEWS")	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_NEWS")
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0
	
	//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",fStartAt,
	//														-1,-1,-1,-1,
	//														"BS_WB_MARK")	
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_MARK")
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0
	
	//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(pagemov,"SET_DATA_SLOT",fStartAt,
	//														-1,-1,-1,-1,
	//														"BS_WB_MSTOCK")			
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_MSTOCK")
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0
ENDPROC

PROC SF_BS_NEWS_FOR_CATEGORY(SCALEFORM_INDEX pagemov,eSTOCK_STORY_CATEGORY e)//eSTOCK_STORY_CATEGORY

//4		String: fake button for this page (eg. "Main")
//5 	String: button for WWW_BAWSAQ_COM_S_NEWS_CITY (eg. "City")
//6 	String: button for WWW_BAWSAQ_COM_S_NEWS_MONEY (eg. "Money")
//7 	String: button for WWW_BAWSAQ_COM_S_NEWS_TECHNOLOGY (eg. "Technology")

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_GENNEWS")
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_CITNEWS")
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_MONNEWS")
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_TECNEWS")
	END_SCALEFORM_MOVIE_METHOD()

//8 	String: page header (eg. "News Main")

	SWITCH e
		
		CASE eSS_main
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_GENNEWS")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE eSS_city
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_CITNEWS")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE eSS_money
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_MONNEWS")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
		
		CASE eSS_tech
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_TECNEWS")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH


	
//9		String: news article 1 header text (eg. "News Article 1")
//10	String: news article 1 body text (eg. "Lorum Ipsum Dolor...")
	
		
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[e][0].tHeader)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[e][0].tSummary)
	END_SCALEFORM_MOVIE_METHOD()
	
//11	String: news article 2 header text (eg. "News Article 2")
//12    String: news article 2 body text (eg. "Lorum Ipsum Dolor...")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[e][1].tHeader)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[e][1].tSummary)
	END_SCALEFORM_MOVIE_METHOD()
	
	
//13	String: news article 3 header text (eg. "News Article 3")
//14	String: news article 3 body text (eg. "Lorum Ipsum Dolor...")

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(13.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[e][2].tHeader)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[e][2].tSummary)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///   	Create and add the bawsaq dateline 
PROC SF_BS_DATELINE(SCALEFORM_INDEX pagemov,int indexOffset = 4)
	
	FLOAT ftickerindex = TO_FLOAT(indexOffset)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(ftickerindex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_DATELINE")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SF_BS_LISTING_HEADER(SCALEFORM_INDEX pagemov,BOOL portfolioPage,int indexOffset = 7)
	
	FLOAT fStartAt = TO_FLOAT(indexOffset)

//7		String: column heading for company name (eg. "Name")
//8		String: column heading for current price value (eg. "Current Price")
//9		String: column heading for volume value (eg. "Volume")
//10	String: column heading for movement value (eg. "Movement")
					
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_NAMEH")
	END_SCALEFORM_MOVIE_METHOD()
	fStartAt += 1.0

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CURPRICH")
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		IF portfolioPage
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_RETP")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_MOVH")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0
ENDPROC

	
PROC SF_BS_EMPTYPAGE(SCALEFORM_INDEX pagemov,int indexOffset = 5)
	
	FLOAT fStartAt = TO_FLOAT(indexOffset)
/*
	5	String: page header (eg. "MY STOCKS")
	6	String: message text (eg. "You currently have no stocks in your portfolio.")
*/
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_SOR")
	END_SCALEFORM_MOVIE_METHOD()
	
	fStartAt += 1.0
															
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartAt)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_GOBUY")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC STRING GET_STOCK_COMPANY_INFO_STRING_FROM_ENUM(BAWSAQ_COMPANIES coindex)

	SWITCH coindex
		CASE BS_CO_AMU 
			RETURN "BS_CO_AMUDESC"
		CASE BS_CO_BDG
			RETURN "BS_CO_BDGDESC"
		CASE BS_CO_BOL
			RETURN "BS_CO_BOLDESC"
		CASE BS_CO_BFA
			RETURN "BS_CO_BFADESC"
		CASE BS_CO_BIN
			RETURN "BS_CO_BINDESC"
		CASE BS_CO_BTR
			RETURN "BS_CO_BTRDESC"
		CASE BS_CO_BLE
			RETURN "BS_CO_BLEDESC"
		CASE BS_CO_BRU
			RETURN "BS_CO_BRUDESC"
		CASE BS_CO_CNT
			RETURN "BS_CO_CNTDESC"
		CASE BS_CO_CRE
			RETURN "BS_CO_CREDESC"
		CASE BS_CO_DGP
			RETURN "BS_CO_DGPDESC"
		CASE BS_CO_WAP
			RETURN "BS_CO_WAPDESC"
		CASE BS_CO_FAC
			RETURN "BS_CO_FACDESC"
		CASE BS_CO_FLC
			RETURN "BS_CO_FLCDESC"
		CASE BS_CO_HJK
			RETURN "BS_CO_HJKDESC"
		CASE BS_CO_FRT
			RETURN "BS_CO_FRTDESC"
		CASE BS_CO_AUG
			RETURN "BS_CO_AUGDESC"
		CASE BS_CO_BUL
			RETURN "BS_CO_BULDESC"
		CASE BS_CO_HAF
			RETURN "BS_CO_HAFDESC"
		CASE BS_CO_SSS
			RETURN "BS_CO_SSSDESC"
		CASE BS_CO_LSC
			RETURN "BS_CO_LSCDESC"
		CASE BS_CO_LST
			RETURN "BS_CO_LSTDESC"
		CASE BS_CO_LTD
			RETURN "BS_CO_LTDDESC"
		CASE BS_CO_MAI
			RETURN "BS_CO_MAIDESC"
		CASE BS_CO_MAZ
			RETURN "BS_CO_MAZDESC"
		CASE BS_CO_PKW
			RETURN "BS_CO_PKWDESC"
		CASE BS_CO_ARK
			RETURN "BS_CO_ARKDESC"
		CASE BS_CO_PIS
			RETURN "BS_CO_PISDESC"
		CASE BS_CO_PON
			RETURN "BS_CO_PONDESC"
		CASE BS_CO_RAI
			RETURN "BS_CO_RAIDESC"
		CASE BS_CO_RON
			RETURN "BS_CO_RONDESC"
		CASE BS_CO_SHT
			RETURN "BS_CO_SHTDESC"
		CASE BS_CO_SPU
			RETURN "BS_CO_SPUDESC"
		CASE BS_CO_TNK
			RETURN "BS_CO_TNKDESC"
		CASE BS_CO_WIW
			RETURN "BS_CO_WIWDESC"
		CASE BS_CO_UMA
			RETURN "BS_CO_UMADESC"
		CASE BS_CO_VAP
			RETURN "BS_CO_VAPDESC"
		CASE BS_CO_VOM
			RETURN "BS_CO_VOMDESC"
		CASE BS_CO_WZL
			RETURN "BS_CO_WZLDESC"
		CASE BS_CO_WIZ
			RETURN "BS_CO_WIZDESC"
		CASE BS_CO_GOL
			RETURN "BS_CO_GOLDESC"
		CASE BS_CO_ZIT
			RETURN "BS_CO_ZITDESC"
		CASE BS_CO_SHK
			RETURN "BS_CO_SHKDESC"
		CASE BS_CO_MOL
			RETURN "BS_CO_MOLDESC"
		CASE BS_CO_PMP
			RETURN "BS_CO_PMPDESC"
		CASE BS_CO_GOT
			RETURN "BS_CO_GOTDESC"
		CASE BS_CO_EYE
			RETURN "BS_CO_EYEDESC"
		CASE BS_CO_EMU
			RETURN "BS_CO_EMUDESC"
		CASE BS_CO_BEN
			RETURN "BS_CO_BENDESC"
		CASE BS_CO_BOM
			RETURN "BS_CO_BOMDESC"
		CASE BS_CO_BGR
			RETURN "BS_CO_BGRDESC"
		CASE BS_CO_CLK
			RETURN "BS_CO_CLKDESC"
		CASE BS_CO_BAN
			RETURN "BS_CO_BANDESC"
		CASE BS_CO_DOP
			RETURN "BS_CO_DOPDESC"
		CASE BS_CO_ECL
			RETURN "BS_CO_ECLDESC"
		CASE BS_CO_FUS
			RETURN "BS_CO_FUSDESC"
		CASE BS_CO_GAS
			RETURN "BS_CO_GASDESC"
		CASE BS_CO_GOP
			RETURN "BS_CO_GOPDESC"
		CASE BS_CO_GRU
			RETURN "BS_CO_GRUDESC"
		CASE BS_CO_KRP
			RETURN "BS_CO_KRPDESC"
		CASE BS_CO_LFI
			RETURN "BS_CO_LFIDESC"
		CASE BS_CO_MAX
			RETURN "BS_CO_MAXDESC"
		CASE BS_CO_POP
			RETURN "BS_CO_POPDESC"
		CASE BS_CO_PRO
			RETURN "BS_CO_PRODESC"
		CASE BS_CO_RWC
			RETURN "BS_CO_RWCDESC"
		CASE BS_CO_RIM
			RETURN "BS_CO_RIMDESC"
		CASE BS_CO_TBO
			RETURN "BS_CO_TBODESC"
		CASE BS_CO_UPA
			RETURN "BS_CO_UPADESC"
		CASE BS_CO_VAG
			RETURN "BS_CO_VAGDESC"
		CASE BS_CO_UNI
			RETURN "BS_CO_UNIDESC"
		CASE BS_CO_HVY
			RETURN "BS_CO_HVYDESC"
		CASE BS_CO_LOG
			RETURN "BS_CO_LOGDESC"
		CASE BS_CO_MER
			RETURN "BS_CO_MERDESC"
		CASE BS_CO_RA1
			RETURN "BS_CO_RA1DESC"
		CASE BS_CO_RA2
			RETURN "BS_CO_RA2DESC"
		CASE BS_CO_SHR
			RETURN "BS_CO_SHRDESC"
		CASE BS_CO_HAL
			RETURN "BS_CO_HALDESC"
		CASE BS_CO_MOR
			RETURN "BS_CO_MORDESC"
		CASE BS_CO_DEB
			RETURN "BS_CO_DEBDESC"
		CASE BS_CO_BSS_PRI
			RETURN "BS_CO_BILDESC"
	ENDSWITCH
	RETURN "BS_BU_STINFO"
ENDFUNC
	
PROC SF_BS_COMPANY_PAGE(SCALEFORM_INDEX pagemov, BAWSAQ_COMPANIES coindex)


	IF g_BS_Listings[coindex].bOnlineStock = FALSE
		//fill out the graph for the offline market
		SF_BS_FILL_GRAPH_FOR_STOCK(pagemov, ENUM_TO_INT(coindex),37,38)
	ELSE
		//SCRIPT_ASSERT("Online stock graph currently unimplemented!")
		//right side of the graph is zero hour
		//left side of the graph is -7 hours
		SF_BS_FILL_GRAPH_FOR_ONLINE_STOCK(pagemov, ENUM_TO_INT(coindex),37,38)
	ENDIF	
	
	/*
	SLOT ID       String or Number
	0             String: button for page1 (eg. "Home")
	1             String: button for news page (eg. "News")
	2             String: button for markets stocks listing (eg. "Markets")
	3             String: button for players stock portfolio (eg. "My Stocks")
	4             String: date (eg. "Oct. 13 2010") *No longer used
	5             String: page header - full comany name (eg. "ANOTHERCOMPANY PLC")
	6             Number: currentValue (eg. 120000)
	7             String: volume Label (eg. "Volume") 
	8             Number: volume (eg. 235)
	9             Number: change Net Value (eg. 17.23)
	10            String: abreviated company name (eg. "ABV")
	11            String: graph_title (eg. "Week Summary")
	12            String: friday (eg. "S") NOTE: there are now SIX days on the graph instead of FIVE. Day6 is slotID 20.
	13            String: monday (eg. "M") 
	14            String: tuesday (eg. "T") 
	15            String: wednesday (eg. "W")
	16            String: thursday (eg. "T") 
	17            String: button for 'buy' page WWW_BAWSAQ_COM_S_STOCK_TRADE_BUY (eg. "BUY")
	18            String: button for 'sell' page WWW_BAWSAQ_COM_S_STOCK_TRADE_SELL (eg. "SELL") 
	19            String: button for 'stock info' page WWW_BAWSAQ_COM_S_STOCK_INFO (eg. "COMPANY NEWS") *No longer used 
	20            String: day6 (eg. "F") *new 
	21            String: company Info (eg. "This company is brilliant!)
	22            Number: change Percent Value (eg. 99.99)
	23            String: high Label (eg. "High")
	24            Number: high Value (eg. 200.88)
	25            String: low Label (eg. "Low")
	26            Number: low Value (eg. 0.01)
	27            String: share Value Label (eg. "Value")
	28            Number: share Value Value (eg. 0.10)
	29            String: graph Last Label (eg. "Last:")
	30            Number: graph Last Value (eg. 200.88)
	31            String: graph Change Net Label (eg. "Change:")
	32            Number: graph Change Net Value (eg. 20.87)
	33            String: graph Volume Label (eg. "Volume"
	34            Number: graph Volume Value (eg. 450001)
	35            String: graph Time Label (eg. "Time:"
	36            String: graph Time  (eg. "4:30 PM")
	*/
		
	/*
    0      		  String: button for page1 (eg. "Home")
	1             String: button for news page (eg. "News")
	2             String: button for markets stocks listing (eg. "Markets")
	3             String: button for players stock portfolio (eg. "My Stocks")
	*/

	SF_BS_BUTTONBLOCK(pagemov)
	
	FLOAT fCurrentLoggedPrice = GET_CURRENT_STOCK_PRICE(ENUM_TO_INT(coindex))

	//4 String: date (eg. "Oct. 13 2010 - US MARKET OPEN")
	//SF_BS_DATELINE(pagemov,4)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_STCKINF")
	END_SCALEFORM_MOVIE_METHOD()
			
	//5 String: page header - full comany name (eg. "ANOTHERCOMPANY PLC")
	//BS_CO_LFB
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sLongName)
	END_SCALEFORM_MOVIE_METHOD()
		
	//6 Number: currentValue (eg. 120000)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
		
	//7 String: volume Label abreviation (eg. "Vol.") 
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()
			
	//8 Number: volume (eg. 235)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(777777)
	END_SCALEFORM_MOVIE_METHOD()
	
	//9 Number: changeValue (eg. -17.23)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	//10 String: abreviated company name (eg. "ABV")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()	
							
	//11 String: graph_title (eg. "PAST WEEK")
	IF g_bOnlineMarket
		// Last 5 hours for online
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_HRS")
		END_SCALEFORM_MOVIE_METHOD()	
	ELSE // Past week for offline
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_GR")
		END_SCALEFORM_MOVIE_METHOD()	
	ENDIF
		
	//17 String: button for 'buy' page WWW_BAWSAQ_COM_S_STOCK_TRADE_BUY (eg. "BUY")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(17.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_BUY")
	END_SCALEFORM_MOVIE_METHOD()
							
	//INT amtstock = 	
	//CPRINTLN(DEBUG_INTERNET, "Stock owned ")
	//PRINTINT(amtstock)
	//PRINTNL()
			  
	INT stockowned = 0
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			stockowned = GET_PLAYER_AMOUNT_OF_STOCK_OWNED(coindex,BS_TR_MIKE)
			BREAK
		CASE CHAR_TREVOR
			stockowned = GET_PLAYER_AMOUNT_OF_STOCK_OWNED(coindex,BS_TR_TREVOR)
			BREAK
		CASE CHAR_FRANKLIN
			stockowned = GET_PLAYER_AMOUNT_OF_STOCK_OWNED(coindex,BS_TR_FRANKLIN)
			BREAK
	ENDSWITCH  
	
	//18 String: button for 'sell' page WWW_BAWSAQ_COM_S_STOCK_TRADE_SELL (eg. "SELL") 
	//The player owns any coindex stock	
	IF stockowned > 0	  
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_SELL")
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	//19 String: button for 'stock info' page WWW_BAWSAQ_COM_S_STOCK_INFO (eg. "COMPANY NEWS")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(19.0)
		//This should possibly be switched to a news story about the co
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_STOCK_COMPANY_INFO_STRING_FROM_ENUM(coindex))
	END_SCALEFORM_MOVIE_METHOD()
	
	//20 String: day6 (eg. "F") *new							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(20.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_F")
	END_SCALEFORM_MOVIE_METHOD()
	
	//21 String: company Info (eg. "This company is brilliant!)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(21.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_STOCK_COMPANY_INFO_STRING_FROM_ENUM(coindex))
	END_SCALEFORM_MOVIE_METHOD()
			
	//22 Number: change Percent Value (eg. 99.99)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(22.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeInPercent)
	END_SCALEFORM_MOVIE_METHOD()
		
	//23 String: high Label (eg. "High")
	//24 Number: high Value (eg. 200.88)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(23.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_HIGHFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGHFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGH")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
								
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(24.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedHighValue)
	END_SCALEFORM_MOVIE_METHOD()


	//25 String: low Label (eg. "Low")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(25.0)
		
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_LOWFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOWFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOW")
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	//26 Number: low Value (eg. 0.01)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(26.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedLowValue)
	END_SCALEFORM_MOVIE_METHOD()

	//27 String: share Value Label (eg. "Value")
	//28 Number: share Value Value (eg. 0.10)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(27.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VAL")
	END_SCALEFORM_MOVIE_METHOD()
								
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(28.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
		
	//33 String: graph Volume Label (eg. "Volume"
	//34 Number: graph Volume Value (eg. 450001)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(33.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()
								
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(34.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(88888)
	END_SCALEFORM_MOVIE_METHOD()
			
	//29 String: graph Last Label (eg. "Last:")
	//30 Number: graph Last Value (eg. 200.88)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(29.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_H_LST")
	END_SCALEFORM_MOVIE_METHOD()
								
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(30.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice - g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
		//g_BS_Listings[coindex].fCurrentPrice - g_BS_Listings[coindex].fLastPriceChange)
	END_SCALEFORM_MOVIE_METHOD()
			
	//31 String: graph Change Net Label (eg. "Change:")
	//32 Number: graph Change Net Value (eg. 20.87)		
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(31.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_H_CHNG")
	END_SCALEFORM_MOVIE_METHOD()
		
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(32.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
					
	//Time
	//35 String: graph Time Label //Used to be time, is now going to be owned amount
	//36 String: graph Time  (eg. "4:30 PM")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(35.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_H_OWNED") 
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(36.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(stockowned))
	END_SCALEFORM_MOVIE_METHOD()
		

ENDPROC

PROC SF_BS_COMPANY_INFO(SCALEFORM_INDEX pagemov, BAWSAQ_COMPANIES coindex)
	

	// 5 String: page header - full company name (eg. "ANOTHERCOMPANY PLC")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sLongName)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 6 String: stock view body text (eg. "Though stocks were quick to recover...")						
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_STOCK_COMPANY_INFO_STRING_FROM_ENUM(coindex))
	END_SCALEFORM_MOVIE_METHOD()

	// 7 String: button back to STOCK_VIEW page (eg. "BACK TO STOCK VIEW"
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_BTSV")
	END_SCALEFORM_MOVIE_METHOD()						
ENDPROC

PROC SF_BS_BUYPAGE(SCALEFORM_INDEX pagemov, BAWSAQ_COMPANIES coindex)

	CPRINTLN(DEBUG_INTERNET, "SF_BS_BUYPAGE: FOR INDEX ", coindex)
	
	IF ENUM_TO_INT(coindex) = -1
		SCRIPT_ASSERT("Invalid index recived on buypage, probably scaleform returning -1 for clicked indexes again, bailing out of page update to prevent crash")
		EXIT
	ENDIF
	
	FLOAT fCurrentLoggedPrice = GET_CURRENT_STOCK_PRICE(ENUM_TO_INT(coindex))
	
	// 5 String: page header (eg. "BUY SHARES")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_BUY")
	END_SCALEFORM_MOVIE_METHOD()
							
	// 6 String: company name abreviated text (eg. "ABV")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
							
	// 7  String: full company name text (eg. "FULL COMPANY NAME")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sLongName)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 8 Number: currentValue (eg. 101.25)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 9 String: volume Label text (eg. "Vol.")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()
	
	// 10 Number: volume number (eg. 24.37)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(99999)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 11 Number: changeValue number (eg. -16.2)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()

	BAWSAQ_TRADERS bttarget
	enumBankAccountName batarget
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			bttarget = BS_TR_MIKE
			batarget = BANK_ACCOUNT_MICHAEL
			BREAK
		CASE CHAR_TREVOR
			bttarget = BS_TR_TREVOR
			batarget = BANK_ACCOUNT_TREVOR
			BREAK
		CASE CHAR_FRANKLIN
			bttarget = BS_TR_FRANKLIN
			batarget = BANK_ACCOUNT_FRANKLIN
			BREAK
	ENDSWITCH

	IF(g_iSharesToBuySell < 0)
		IF g_iPrevSharesToBuySell > 1000000000	//Was about to loop around HIGHEST_INT
			g_iSharesToBuySell = HIGHEST_INT
			CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell maxed by SF_BS_BUYPAGE range check - was looping around")
		ELSE
			g_iSharesToBuySell = 0
			CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell zeroed by SF_BS_BUYPAGE range check")
		ENDIF
	ENDIF
	g_iPrevSharesToBuySell = g_iSharesToBuySell

	// Check number of shares*price is not greater than current funds
	// Cap if needed
	CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell precap : ",g_iSharesToBuySell)
	
	// New cap
	g_iSharesBuySellCap = FLOOR(TO_FLOAT(g_BankAccounts[batarget].iBalance) / fCurrentLoggedPrice)
	IF g_iSharesBuySellCap < 0 
		g_iSharesBuySellCap = HIGHEST_INT
	ENDIF
	
	//Find out if the player owns any of these stocks
	BOOL bPlayerOwned = FALSE
	INT iSelected = -1
	INT i = 0
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
	//g_BS_Portfolios[BS_TR_TOTAL][MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		IF (GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i) = coindex) AND (NOT bPlayerOwned)
			IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i) > 0
				iSelected = i
				bPlayerOwned = TRUE
				//Clamp so that player can't own more than MAXINT shares
				g_iSharesBuySellCap = CLAMP_INT(g_iSharesBuySellCap,0,HIGHEST_INT - GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i))
			ENDIF
		ENDIF
	ENDREPEAT
	
	g_iSharesToBuySell = CLAMP_INT(g_iSharesToBuySell, 0, g_iSharesBuySellCap)
	g_fShareTotal = g_iSharesToBuySell*fCurrentLoggedPrice
	
	//fix for 1596660
	IF g_fShareTotal > TO_FLOAT(g_BankAccounts[batarget].iBalance)
		g_fShareTotal = TO_FLOAT(g_BankAccounts[batarget].iBalance)
	ENDIF
	
	
	CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell postcap : ",g_iSharesToBuySell)
		
	//if they do then fill out the following values
	IF NOT bPlayerOwned
		g_fOriginalProfit = 0
		g_fCurrentProfit = 0 
	ELSE
		g_fOriginalProfit = TO_FLOAT(GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),iSelected)) // change this value to owned shares, and string to match
		g_fCurrentProfit = GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),iSelected) // change this value to currently invested
	ENDIF
	
	// 12 Number: number of shares number (eg. 23.23)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_iSharesToBuySell)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 13 String: shares label Text (eg. "SHARES")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(13.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SHARES")
	END_SCALEFORM_MOVIE_METHOD()
	
	// 14 Number: grossSum number (eg. 19.4)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fShareTotal)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 15 String: grossSum label text (eg. "GROSS")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(15.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_GSUM")
	END_SCALEFORM_MOVIE_METHOD()
	
	// 16 Number: originalProfit number (eg. 12.2)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(16.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fOriginalProfit)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 17 String: originalProfit lable Text (eg. "ORIGINAL PROFIT")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(17.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SHOWN")
	END_SCALEFORM_MOVIE_METHOD()
												
	// 18 Number: profit number (eg. 17.6)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fCurrentProfit)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 19 String: profit label Text (eg. "PROFIT") 
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(19.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_IVEST")
	END_SCALEFORM_MOVIE_METHOD()
												
	// 20 String: buy button goes to confirm page (eg. "BUY"
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(20.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_BUY")
	END_SCALEFORM_MOVIE_METHOD()
	
	// 21 plus button						
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(21.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 22 minus button						
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(22.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
				
	// 23 Number: change Percent Value (eg. -22.765)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(23.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeInPercent)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 24 String: high Label (eg."High")
	// 25 Number: high Value (eg. 200.88)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(24.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_HIGHFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGHFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGH")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(25.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedHighValue)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 26 String: low Label (eg."Low")
	// 27 Number: low Value (eg. 0.01)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(26.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_LOWFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOWFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOW")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(27.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedLowValue)
	END_SCALEFORM_MOVIE_METHOD()

	// 28 String: share Value Label (eg."Value")
	// 29 Number: share Value (eg. 0.10)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(28.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VAL")
	END_SCALEFORM_MOVIE_METHOD()
							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(29.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SF_BS_SELLPAGE(SCALEFORM_INDEX pagemov, BAWSAQ_COMPANIES coindex)

	/*
		5              String: page header (eg. "SELL SHARES")
		6              String: company name abreviated text (eg. "ABV")
		7              String: full company name text (eg. "FULL COMPANY NAME")
		8              Number: currentValue (eg. 101.25)
		9              String: volume Label text (eg. "Volume")
		10             Number: volume number (eg. 24.37)
		11             Number: change Net Value (eg. -16.2)
		12             Number: number of shares (eg. 23)
		13             String: sell shares label Text (eg. "SELL SHARES")
		14             Number: grossSum number (eg. 19.4)
		15             String: grossSum label text (eg. "GROSS")
		16             Number: shares Owned (eg. 12)
		17             String: shares Owned label Text (eg. "Shares Owned")
		18             Number: total Invested (eg. 17.6)
		19             String: total Invested label Text (eg. Total Invested")
		20             String: sell button goes to confirm page (eg. "SELL")
		21             String: plus button for increment value (eg. a SPACE character) uses no text, added for tracking purposes
		22             String: minus button for decrement value (eg. a SPACE character) uses no text, added for tracking purposes
		23             Number: change Percent Value (eg.-22.765)
		24             String: high Label (eg."High")
		25             Number: high Value (eg.200.88)
		26             String: low Label (eg."Low")
		27             Number: low Value (eg.0.01)
		28             String: share Value Label (eg."Value")
		29             Number: share Value (eg.0.10)
	*/
	
	BAWSAQ_TRADERS bttarget
				
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			bttarget = BS_TR_MIKE
			//batarget = BANK_ACCOUNT_MICHAEL
			BREAK
		CASE CHAR_TREVOR
			bttarget = BS_TR_TREVOR
			//batarget = BANK_ACCOUNT_TREVOR
			BREAK
		CASE CHAR_FRANKLIN
			bttarget = BS_TR_FRANKLIN
			//batarget = BANK_ACCOUNT_FRANKLIN
			BREAK
	ENDSWITCH
	
	//find player owned index
	IF g_iStockSelectedPortfolioIndex = -1
		INT i = 0
		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i 
		
			IF (GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i) = coindex) AND (GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i) > 0)
				g_iStockSelectedPortfolioIndex = i
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF g_iStockSelectedPortfolioIndex = -1
		SCRIPT_ASSERT("SF_BS_SELLPAGE: failed to find the correct stock, player owns none of them maybe?")
	ENDIF
	
	FLOAT fCurrentLoggedPrice = GET_CURRENT_STOCK_PRICE(ENUM_TO_INT(coindex))

	// 5 String: page header (eg. "BUY SHARES")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_SELL")
	END_SCALEFORM_MOVIE_METHOD()
							
	// 6 String: company name abreviated text (eg. "ABV")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
							
	// 7 String: full company name text (eg. "FULL COMPANY NAME")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sLongName)
	END_SCALEFORM_MOVIE_METHOD()
							
							
	// 8 Number: currentValue (eg. 101.25)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
		//g_BS_Listings[coindex].fCurrentPrice)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 9 String: volume Label text (eg. "Vol.")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()
							
	// 10 Number: volume number (eg. 24.37)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(676767)
	END_SCALEFORM_MOVIE_METHOD()
				
	// 11 Number: changeValue number (eg. -16.2)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
	
	IF(g_iSharesToBuySell < 0)
		g_iSharesToBuySell = 0
		CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell zeroed by SF_BS_SELLPAGE range check")
	ENDIF
	
	//check shares are not greater than owned shares
	//cap if needed
	
	g_iSharesBuySellCap = GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),g_iStockSelectedPortfolioIndex)
	IF g_iSharesToBuySell > g_iSharesBuySellCap
		g_iSharesToBuySell = g_iSharesBuySellCap
		CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell capped by SF_BS_SELLPAGE range check to ", g_iSharesBuySellCap)
	ENDIF
	//See if selling that total would fuck up the finance system 2147483647.0
	//and if so cap the fucker
	FLOAT fPrice = fCurrentLoggedPrice
	FLOAT fTotal = g_iSharesToBuySell*fPrice
	FLOAT fBalance = TO_FLOAT(g_BankAccounts[bttarget].iBalance)
	FLOAT fLimit = 2147483647.0 
	CPRINTLN(DEBUG_INTERNET, "fTotal(",fTotal,") + fBalance(",fBalance,") (",fTotal+ fBalance,") > fLimit(",fLimit,")")
	IF (fTotal+ fBalance) > fLimit
		//cap the fucker
		g_iSharesToBuySell = FLOOR((fLimit-fBalance)/fPrice)
		g_iSharesBuySellCap = g_iSharesToBuySell
		CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell capped by SF_BS_SELLPAGE max check to ", g_iSharesBuySellCap)
	ENDIF
	
	
	
	g_fShareTotal     = g_iSharesToBuySell*fCurrentLoggedPrice
	g_fOriginalProfit = GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),g_iStockSelectedPortfolioIndex)
	g_fCurrentProfit  =  g_fShareTotal- (g_fOriginalProfit* (TO_FLOAT(g_iSharesToBuySell)/TO_FLOAT(GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),g_iStockSelectedPortfolioIndex))))//

	// 12 Number: number of shares number (eg. 23.23)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_iSharesToBuySell)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 13 String: shares label Text (eg. "SHARES")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(13.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SHARES")
	END_SCALEFORM_MOVIE_METHOD()
				
	// 14 Number: grossSum number (eg. 19.4)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget), g_iStockSelectedPortfolioIndex))
	END_SCALEFORM_MOVIE_METHOD()
	
	// 15 String: grossSum label text (eg. "GROSS")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(15.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SHOWN")
	END_SCALEFORM_MOVIE_METHOD()

	// 16 Number: originalProfit number (eg. 12.2)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(16.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fOriginalProfit)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 17 String: originalProfit lable Text (eg. "ORIGINAL PROFIT")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(17.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_IVEST")
	END_SCALEFORM_MOVIE_METHOD()
							
	// 18 Number: profit number (eg. 17.6)
	//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(pagemov,"SET_DATA_SLOT",18.0,g_fCurrentProfit)

	// 19 String: profit label Text (eg. "PROFIT") 
	IF g_fCurrentProfit > -0.000001

		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fCurrentProfit)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(19.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_PROF")
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(-g_fCurrentProfit)
		END_SCALEFORM_MOVIE_METHOD()
						
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(19.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_LOSS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// 20 String: buy button goes to confirm page (eg. "SELL")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(20.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_BU_SELL")
	END_SCALEFORM_MOVIE_METHOD()
							
	// 21 plus button						
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(21.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
							
	// 22 minus button						
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(22.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
							
	// 23 Number: change Percent Value (eg.-22.765)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(23.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeInPercent)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 24 String: high Label (eg."High")
	// 25 Number: high Value (eg.200.88)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(24.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_HIGHFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGHFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGH")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
						
	FLOAT hval = GET_HIGH_VALUE_FOR_COMPANY(coindex)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(25.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(hval)
	END_SCALEFORM_MOVIE_METHOD()

	// 26 String: low Label (eg."Low")
	// 27 Number: low Value (eg.0.01)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(26.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_LOWFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOWFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOW")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
						
	FLOAT lval = GET_LOW_VALUE_FOR_COMPANY(coindex)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(27.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(lval)
	END_SCALEFORM_MOVIE_METHOD()
	
	// 28 String: share Value Label (eg."Value")
	// 29 Number: share Value (eg.0.10)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(28.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VAL")
	END_SCALEFORM_MOVIE_METHOD()
							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(29.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(30.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SELLALL")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SF_BS_CONFIRMATION(SCALEFORM_INDEX pagemov, BAWSAQ_COMPANIES coindex)

/*
SLOT ID        String or Number
0              String: button for page1 (eg. "Home")
1              String: button for news page (eg. "News")
2              String: button for markets stocks listing (eg. "Markets")
3              String: button for players stock portfolio (eg. "My Stocks")

5              String: page header (eg. "BUY/SELL SHARES")
6              String: company name abreviated text (eg. "ABV")
7              String: 'shares' and Company Name (eg. "Shares ANOTHERCOMPANY PLC")
8              Number: current value (eg. 160.26)
9              String: volume label (eg. "VOLUME")
10             Number: volume (eg. 26)
11             Number: change value (eg. 13.2)

13             Number: number of shares (eg. 17)
14             String: confirm transaction label (eg. "Confirm Transaction")
15             Number: cost of share (eg. 26.0.)
16             String: cost label (eg. "COST")

18             String: button for confirm, goes to WWW_BAWSAQ_COM_S_CONFIRMED (eg."CONFIRM")
19             String: button for cancel, back to stock view - WWW_BAWSAQ_COM_S_STOCK_VIEW (eg. "CANCEL")
20             Number: change Percent Value  (eg. 17.23")
21             String: high Label(eg. "High")
22             Number: high Value(eg.  200.88")
23             String: low Label(eg. "Low")
24             Number: low Value(eg.  0.01")
25             String: share Value Label(eg. "Value")
26             Number: share Value (eg.  0.10")
*/

	//0 String: button for page1 (eg. "Home")
	//1 String: button for news page (eg. "News")
	//2 String: button for markets stocks listing (eg. "Markets")
	//3 String: button for players stock portfolio (eg. "My Stocks")
	SF_BS_BUTTONBLOCK(pagemov)
	
	//5 String: page header (eg. "BUY/SELL SHARES")
	IF(g_bBuySell)
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_BS")
		END_SCALEFORM_MOVIE_METHOD()		
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_SS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	FLOAT fCurrentLoggedPrice = GET_CURRENT_STOCK_PRICE(ENUM_TO_INT(coindex))
	
	//6 String: company name abreviated text (eg. "ABV")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()
							
	//7 String: 'shares' and Company Name (eg. "Shares ANOTHERCOMPANY PLC")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SHARES")
	END_SCALEFORM_MOVIE_METHOD()

	//8 Number: current value (eg. 160.26)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
				
	//9 String: volume label (eg. "VOLUME")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()
							
	//10 Number: volume (eg. 26)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(787878)
	END_SCALEFORM_MOVIE_METHOD()
	
	
	//11 Number: change value (eg. 13.2)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()
				
	//13 Number: number of shares (eg. 17)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(13.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_iSharesToBuySell)
	END_SCALEFORM_MOVIE_METHOD()

	//14 String: confirm transaction label (eg. "Confirm Transaction")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CONFTR")
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(15.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fShareTotal)
	END_SCALEFORM_MOVIE_METHOD()

	//16 String: cost label (eg. "COST")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(16.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_COST")
	END_SCALEFORM_MOVIE_METHOD()
							
	//18 String: button for confirm, goes to WWW_BAWSAQ_COM_S_CONFIRMED (eg."CONFIRM")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CONF")
	END_SCALEFORM_MOVIE_METHOD()
							
	//19 String: button for cancel, back to stock view - WWW_BAWSAQ_COM_S_STOCK_VIEW (eg. "CANCEL")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(19.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CAN")
	END_SCALEFORM_MOVIE_METHOD()
				
							
	//20 Number: change Percent Value  (eg. 17.23")
	//21 String: high Label(eg. "High")
	//22 Number: high Value(eg.  200.88")
	//23 String: low Label(eg. "Low")
	//24 Number: low Value(eg.  0.01")
	//25 String: share Value Label(eg. "Value")
	//26 Number: share Value (eg.  0.10")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(20.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeInPercent)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(21.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_HIGHFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGHFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGH")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(22.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedHighValue)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(23.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_LOWFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOWFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOW")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(24.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedLowValue)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(25.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VAL")
	END_SCALEFORM_MOVIE_METHOD()
								
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(26.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SF_BS_CONFIRMED_SELL_ALL(SCALEFORM_INDEX pagemov)
	
	// SLOT ID        String or Number
	// 0              String: button for page1 (eg. "Home")
	// 1              String: button for news page (eg. "News")
	// 2              String: button for markets stocks listing (eg. "Markets")
	// 3              String: button for players stock portfolio (eg. "Portfolio")
	
	SF_BS_BUTTONBLOCK(pagemov)
	
	//	4 String: button for markets stocks listing (eg. "Continue") 
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CONTBU")
	END_SCALEFORM_MOVIE_METHOD()
	
	//now fill out indices 5 to 23 with BS_SACO_n,
	//skip the indices that 
	INT max = 24 - 5
	INT i
	
	REPEAT max i
		
		INT indice = i + 5
		TEXT_LABEL_15 ba = "BS_SACO_"
		ba += indice
		
		SWITCH indice
			
			CASE 9 //price left // totval
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fLastStockSellAll_Totval)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 11 
				// Total profit
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(FLOOR(g_fLastStockSellAll_Profval))
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 17 
				// Total shares
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(g_iLastStockSellAll_Totshares))
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 19 
				// Types
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fLastStockSellAll_Valover)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 21 
				// Value over cost
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(g_iLastStockSellAll_Cotot))
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 23 
				// Vvalue under cost
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fLastStockSellAll_Valunder)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 13
				// Totval again
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fLastStockSellAll_Totval)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 14 
				// Total profit again
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fLastStockSellAll_Profval)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			CASE 15
				//Increase in your wealth relative to new current cash
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fLastStockSellAll_NewWealthPerc)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
			
			DEFAULT
				// Upload string with that indice
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(indice))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(ba)
				END_SCALEFORM_MOVIE_METHOD()
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	//fill out the specific number indices
ENDPROC


PROC SF_BS_CONFIRMED(SCALEFORM_INDEX pagemov, BAWSAQ_COMPANIES coindex)
		
/*
SLOT ID        String or Number
0              String: button for page1 (eg. "Home")
1              String: button for news page (eg. "News")
2              String: button for markets stocks listing (eg. "Markets")
3              String: button for players stock portfolio (eg. "My Stocks")
4              String: button for markets stocks listing (eg. "Continue") 
5              String: page header (eg. "BUY/SELL SHARES")
6              String: company name abreviated text (eg. "ABV")
7              String: 'shares' and Company Name (eg. "Shares ANOTHERCOMPANY PLC")
9              Number: number of shares (eg. 17)
10             String: confirm Transaction label (eg. "Transaction Complete")
11             Number: cost of share (eg. 27.16)
12             String: cost label (eg. "COST")
13             Number: current Value (eg. 160.26)		
14             Number: change Net Value (eg. 13.2)
15             Number: change Percent Value (eg. 17.23)
16             String: high Label (eg. "High")
17             Number: high Value (eg. 200.88)
18             String: low Label (eg. "Low")
19             Number: low Value (eg. 0.01)
20             String: volume Label (eg. "Volume")
21             Number: volume Value (eg. 10)
22             String: share Value Label (eg. "Value")
23             Number: share Value (eg. 0.10)
*/
	
	SF_BS_BUTTONBLOCK(pagemov)
		
	FLOAT fCurrentLoggedPrice = GET_CURRENT_STOCK_PRICE(ENUM_TO_INT(coindex))
		
	//4 String: button for markets stocks listing (eg. "Continue") 
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CONTBU")
	END_SCALEFORM_MOVIE_METHOD()

	//5 String: page header (eg. "BUY/SELL SHARES")
	IF(g_bBuySell)
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_BS")
		END_SCALEFORM_MOVIE_METHOD()					
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_SS")
		END_SCALEFORM_MOVIE_METHOD()					
	ENDIF

	//6 String: company name abreviated text (eg. "ABV")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[coindex].sShortName)
	END_SCALEFORM_MOVIE_METHOD()

	//7 String: 'shares' and Company Name (eg. "Shares ANOTHERCOMPANY PLC")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SHARES")
	END_SCALEFORM_MOVIE_METHOD()

	//9 Number: number of shares (eg. 17)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_iSharesToBuySell)
	END_SCALEFORM_MOVIE_METHOD()


	//10 String: confirm Transaction label (eg. "Transaction Complete")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_TRCOM")
	END_SCALEFORM_MOVIE_METHOD()

	//11 Number: cost of share (eg. 27.16)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_fShareTotal)
	END_SCALEFORM_MOVIE_METHOD()
	
	//12 String: cost label (eg. "COST")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_COST")
	END_SCALEFORM_MOVIE_METHOD()
												
	IF(g_bBuySell)
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_COST")
		END_SCALEFORM_MOVIE_METHOD()					
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_PROLOS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF		
							
//13 Number: current Value (eg. 160.26)      
//14 Number: change Net Value (eg. 13.2)
//15 Number: change Percent Value (eg. 17.23)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(13.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeFromWeeklyAverage)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(15.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedPriceChangeInPercent)
	END_SCALEFORM_MOVIE_METHOD()

//16 String: high Label (eg. "High")
//17 Number: high Value (eg. 200.88)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(16.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_HIGHFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGHFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_HIGH")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(17.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedHighValue)
	END_SCALEFORM_MOVIE_METHOD()

//18 String: low Label (eg. "Low")
//19 Number: low Value (eg. 0.01)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(18.0)
		IF g_bOnlineMarket AND DOES_TEXT_LABEL_EXIST("BS_WB_LOWFH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOWFH")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LOW")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(19.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[coindex].fLoggedLowValue)
	END_SCALEFORM_MOVIE_METHOD()

//20 String: volume Label (eg. "Volume")
//21 Number: volume Value (eg. 10)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(20.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VOLU")
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(21.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(898989)
	END_SCALEFORM_MOVIE_METHOD()

//22 String: share Value Label (eg. "Value")
//23 Number: share Value (eg. 0.10)

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(22.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_VAL")
	END_SCALEFORM_MOVIE_METHOD()
							
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(23.0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentLoggedPrice)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC 


PROC DO_BS_TICKERTAPE(SCALEFORM_INDEX pagemov, BOOL news = FALSE)  //http://rsgediwiki1/wiki/index.php/WWW_BAWSAQ_COM
	
	//g_bOnlineMarket
	
	//prices or news
	/*
	SET_DATA_SLOT_EMPTY()
	SET_DATA_SLOT(0, 212.69, 15.72, 0.55,"LORUM");
	SET_DATA_SLOT(1, 45.45, -3.42, 0.12, "IP");
	SET_DATA_SLOT(2, 0.01, 200.87, 9.99, "SUM");
	SET_DATA_SLOT(3, 212.69, 15.72, 0.55, "DOLOR");
	SET_DATA_SLOT(4, 45.45, -3.42, 0.12, "SIT");
	SET_DATA_SLOT(5, 0.01, 200.87, 9.99, "AMET");
	SET_DATA_SLOT(6, 212.69, -15.72, 0.55, "DONEC");
	SET_DATA_SLOT(7, 45.45, 3.42, 0.12, "NISI");
	SET_DATA_SLOT(8, 212.69, 15.72, 0.55, "ORCI");
	SET_TICKERTAPE(0.5, "STOCK")

	So for the data, your params for SET_DATA_SLOT are:
	â€¢	param int (slotID) 
	â€¢	param float or int - current value 
	â€¢	param float or int - change net value 
	â€¢	param float or int - change percent value 
	â€¢	param string â€“ Company abreviation
	And for setting the ticker scrolling, the params for SET_TICKERTAPE are:
	â€¢	param float - speed 
	â€¢	param string  - â€œSTOCKâ€

	For the news ticker itâ€™s the same deal, only you set the data as slotID, 
	string for each headline, and specify â€œNEWSâ€ for the 2nd param in SET_TICKERTAPE.
	*/

	CPRINTLN(DEBUG_INTERNET, "DO_BS_TICKERTAPE : Fired")
	INT i = 0
	INT slot = 0 
	
	CPRINTLN(DEBUG_INTERNET, "DO_BS_TICKERTAPE : SET_DATA_SLOT_EMPTY")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
	//BS_CO_TOTAL_LISTINGS
	IF NOT news
		REPEAT BS_CO_TOTAL_LISTINGS i
			IF g_BS_Listings[i].fCurrentPrice > 0
				IF g_BS_Listings[i].bOnlineStock = g_bOnlineMarket
					CPRINTLN(DEBUG_INTERNET, "DO_BS_TICKERTAPE : SET_DATA_SLOT ", i," : ", slot)
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[i].LoggedPrices[g_BS_Listings[i].iLogIndexCaret])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(g_BS_Listings[i].fLoggedPriceChangeInPercent)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_Listings[i].sShortName)
					END_SCALEFORM_MOVIE_METHOD()
					++slot
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_STOCKS, "Invalid current price for ticker tape for listing index: ", i)
			ENDIF
		ENDREPEAT
	ENDIF

	CPRINTLN(DEBUG_INTERNET, "DO_BS_TICKERTAPE : SET_TICKERTAPE")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_TICKERTAPE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.5)
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "DO_BS_TICKERTAPE : SET_DATA_SLOT_EMPTY")
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


FUNC BAWSAQ_COMPANIES DO_PORTFOLIO_INDEX_CONVERSION(BAWSAQ_TRADERS bttarget, INT nth, bool blinestate)
	INT count = nth
	CPRINTLN(DEBUG_INTERNET, "DO_PORTFOLIO_INDEX_CONVERSION: ",bttarget," : ", nth, " : ", blinestate)
	//bttarget
	INT i = 0
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
	
		IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i) > 0
		//IF  g_BS_Listings[GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i)].bOnlineStock  = blinestate
			IF count = 0
				CPRINTLN(DEBUG_INTERNET, "DO_PORTFOLIO_INDEX_CONVERSION: returning ", i , " -> ", GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i))
				//iPortfolioIndex = i
				RETURN GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(bttarget),i)
			ENDIF
			--count
		//ENDIF
		ENDIF
		
	ENDREPEAT
	SCRIPT_ASSERT("DO_PORTFOLIO_INDEX_CONVERSION: entry not found")
	RETURN BS_CO_AMU
ENDFUNC


/// PURPOSE:
///    Fill out the given stock market page
PROC SF_BAWSAQ(SCALEFORM_INDEX pagemov, int pageID)  //http://rsgediwiki1/wiki/index.php/WWW_BAWSAQ_COM

	CPRINTLN(DEBUG_INTERNET, " SF_BAWSAQ(pageID:", pageID, ")")
	
	BAWSAQ_TRADERS bttarget
	BOOL           countaset = FALSE
	INT            ncount = 0
	INT            scount = 0
	
	// Not error page
	IF pageID != 15 
	
		IF g_bBSWebsiteNoSpaceTrigger
			IF g_bOnlineMarket 
				GO_TO_WEBSITE("WWW_BAWSAQ_COM_S_ERROR")
			ELSE
				GO_TO_WEBSITE("WWW_LCN_D_EXCHANGE_COM_S_ERROR")
			ENDIF
		
		
			g_iStockSelectionFocus = -1
			//g_bBSWebsiteNoSpaceTrigger = FALSE
			EXIT
		ENDIF
	
		IF g_bOnlineMarket
			
			// Check for connection
			IF (NOT NETWORK_IS_SIGNED_ONLINE()) 
			OR g_bInMultiplayer 
			OR g_bStockMarketInitialisationInProgress
			OR g_bInvalidOnlinePricesRead
	
				#IF IS_DEBUG_BUILD
					IF (NOT NETWORK_IS_SIGNED_ONLINE()) 
						CPRINTLN(DEBUG_INTERNET, "BAWSAQ display failed because: (NOT NETWORK_IS_SIGNED_ONLINE()) ")
					ENDIF
					IF g_bInMultiplayer 
						CPRINTLN(DEBUG_INTERNET, "BAWSAQ display failed because: g_bInMultiplayer ")
					ENDIF
					IF g_bStockMarketInitialisationInProgress
						CPRINTLN(DEBUG_INTERNET, "BAWSAQ display failed because: g_bStockMarketInitialisationInProgress")
					ENDIF
					IF g_bInvalidOnlinePricesRead
						CPRINTLN(DEBUG_INTERNET, "BAWSAQ display failed because: g_bInvalidOnlinePricesRead")
					ENDIF
				#ENDIF

				IF g_iPendingBrowserTimer < g_iBrowserTimer
					CPRINTLN(DEBUG_INTERNET, "SF_BAWSAQ: Online market redirect to market down page triggered")
					GO_TO_WEBSITE("WWW_BAWSAQ_COM_S_ERROR")
				ENDIF
				EXIT
			ENDIF
		ELSE
			IF g_bInMultiplayer 
			OR g_bStockMarketInitialisationInProgress 
				CPRINTLN(DEBUG_INTERNET, "SF_BAWSAQ: Offline market redirect to market down page triggered, due to in MP and/or init still in progress")
				GO_TO_WEBSITE("WWW_LCN_D_EXCHANGE_COM_S_ERROR")
				
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	//0 "$SET_BAWSAQ_PLAYER_CASH" proxy
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"PROXY_FUNCTION")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			bttarget = BS_TR_MIKE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL))
		BREAK
		CASE CHAR_TREVOR
			bttarget = BS_TR_TREVOR
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ACCOUNT_BALANCE(BANK_ACCOUNT_TREVOR))
		BREAK
		CASE CHAR_FRANKLIN
			bttarget = BS_TR_FRANKLIN
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ACCOUNT_BALANCE(BANK_ACCOUNT_FRANKLIN))
		BREAK
		DEFAULT
			IF g_bInMultiplayer
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			ENDIF
		BREAK
	ENDSWITCH
	END_SCALEFORM_MOVIE_METHOD()
	
	BAWSAQ_COMPANIES benum

	FLOAT quote
	INT q
	FLOAT fBalance = TO_FLOAT(g_BankAccounts[bttarget].iBalance)
	
	SWITCH pageID
		
		// "PAGE1" - Generic frontpage
		CASE 1
			/*
				0  String: button for page1 (eg. "Home")
				1  String: button for news page (eg. "News")
				2  String: button for markets stocks listing (eg. "Markets")
				3  String: button for players stock portfolio (eg. "Portfolio")
				4  String: button for WWW_LCN_COM (just pass a SPACE as this is a graphic logo)
				12 String: Advert Placeholder (no need to set any text. Listed here so it becomes a button)
				13 String: Summary Column Header (eg. "Summary")
				14 String: Summary Article  Header (eg. "Summary Article Header")
				15 String: Summary Article Body (eg. "Lorum ipsum dolor...")
				16 String: Stock Tips column header (eg. "Stock Tips")
				17 String: Stock Tips Article header (eg. "Stock Tips Article Header")
				18 String: Stock Tips Article Body (eg. "Lorum ipsum dolor...")
				19 String: Top Five Stocks - Company column (eg. "Company")
				20 String: Top Five Stocks - Price column (eg. "Price")
				21 String: Top Five Stocks - Change column (eg. "Change ")
			*/
	
			SF_BS_BUTTONBLOCK(pagemov)	
			SF_BS_DATELINE(pagemov)
	
			//Do the stock top 5 and graph
			SF_BS_TOP_FIVE(pagemov)
	
			/*
				5  String: News Column Header (eg. "News")
				6  String: News Story 1 Header (eg. "News Story 1")
				7  String: News Story 1 Body (eg. "Lorum ipsum dolor...")
				8  String: News Story 2 Header (eg. "News Story 2")
				9  String: News Story 2 Body (eg. "Lorum ipsum dolor...")
				10 String: News Story 3 Header (eg. "News Story 3")
				11 String: News Story 3 Body (eg. "Lorum ipsum dolor...")
			*/
			
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NP_GENNEWS")
			END_SCALEFORM_MOVIE_METHOD()

			#IF IS_DEBUG_BUILD
			IF g_bForceBawsaqNews
			
//				6  String: News Story 1 Header (eg. "News Story 1")
//				7  String: News Story 1 Body (eg. "Lorum ipsum dolor...")
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][0].tHeader)
				END_SCALEFORM_MOVIE_METHOD()
				/*
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][0].tSummary)
				END_SCALEFORM_MOVIE_METHOD() */
				
//				8  String: News Story 2 Header (eg. "News Story 2")
//				9  String: News Story 2 Body (eg. "Lorum ipsum dolor...")
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][1].tHeader)
				END_SCALEFORM_MOVIE_METHOD()
				/*
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][1].tSummary)
				END_SCALEFORM_MOVIE_METHOD() */

//				10 String: News Story 3 Header (eg. "News Story 3")
//				11 String: News Story 3 Body (eg. "Lorum ipsum dolor...")
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][2].tHeader)
				END_SCALEFORM_MOVIE_METHOD()
				/*
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][2].tSummary)
				END_SCALEFORM_MOVIE_METHOD() */
			ELSE
			#ENDIF
			
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1) 
				AND (NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5))
					
					// Show the lifeinvader story spread
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NEWS_LFIH1")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NEWS_LFIB1")
					END_SCALEFORM_MOVIE_METHOD()
			
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NEWS_LFIH2")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NEWS_LFIB2")
					END_SCALEFORM_MOVIE_METHOD()
			
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NEWS_LFIH3")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_NEWS_LFIB3")
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					IF NOT FLOW_BS_NEWS_STORY_OVERRIDE(pagemov,TRUE)
						BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][0].tHeader)
						END_SCALEFORM_MOVIE_METHOD()
					ENDIF
					/*
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][0].tSummary)
					END_SCALEFORM_MOVIE_METHOD()
					*/
					IF NOT FLOW_BS_NEWS_STORY_OVERRIDE(pagemov,FALSE)
						BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][1].tHeader)
						END_SCALEFORM_MOVIE_METHOD()
					ENDIF
					
					/*
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(9.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][1].tSummary)
					END_SCALEFORM_MOVIE_METHOD()
					*/
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(10.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][2].tHeader)
					END_SCALEFORM_MOVIE_METHOD()
					/*
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_BS_NewsStack[0][2].tSummary)
					END_SCALEFORM_MOVIE_METHOD()
					*/
					
				ENDIF
			
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		BREAK
		
		// "STOCK_LISTINGS"
		CASE 2 
			/*
				0             String: button for page1 (eg. "Home")
				1             String: button for news page (eg. "News")
				2             String: button for markets stocks listing (eg. "Markets")
				3             String: button for players stock portfolio (eg. "My Stocks")
				4             String: date (eg. "Oct. 13 2010 - US MARKET OPEN")
				5             String: page header (eg. "MY STOCKS. Portfolio UP $6,325")
				6 (DEFUNCT)	  String: PLUS button title to change category (eg. "Sorted by Name. Click to change.")
				7             String: column heading for company name (eg. "Name")
				8             String: column heading for current price value (eg. "Current Price")
				9             String: column heading for volume value (eg. "Volume")
				10            String: column heading for movement value (eg. "Movement")

			The stock list starts from slot 11 onwards. A minimum of 6 list items fills the minimum height of a page (1 screen).

			* Slot ID: Number
			* Current Value: Number
			* Volume: Number
			* Change Value: Number
			* Abreviated Company Name: String
			* Full Company Name: String 

			*/
			g_bStockListMarketMode = TRUE //this is the market list screen
			g_iStockSelectionFocus = -1 // no stock is currently selected while on this screen
			g_iStockSelectedPortfolioIndex = -1//no portfolio is selected while on this screen
			
			g_iStockSellAllQuote = 0
			
			SF_BS_BUTTONBLOCK(pagemov)																
			SF_BS_DATELINE(pagemov,4)
		
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_MLIST")
			END_SCALEFORM_MOVIE_METHOD()
		
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_PERCHA")
			END_SCALEFORM_MOVIE_METHOD()

			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_STCKLST")
			END_SCALEFORM_MOVIE_METHOD()
															
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_LCSTWT")
			END_SCALEFORM_MOVIE_METHOD()
		
			SF_BS_LISTING_HEADER(pagemov,FALSE)
			SF_BS_ALL_LISTINGS(pagemov)
		BREAK
		
		// "STOCK_PORTFOLIO"
		CASE 3 
			
			/*
			SLOT ID        Text String
			0             String: button for page1 (eg. "Home")
			1             String: button for news page (eg. "News")
			2             String: button for markets stocks listing (eg. "Markets")
			3             String: button for players stock portfolio (eg. "My Stocks")
			4             String: date (eg. "Oct. 13 2010 - US MARKET OPEN")
			5             String: page header (eg. "MY STOCKS. Portfolio UP $6,325")
			6      DEFUNCT       String: PLUS button title to change category (eg. "Sorted by Name. Click to change.")
			7             String: column heading for company name (eg. "Name")
			8             String: column heading for current price value (eg. "Current Price")
			9             String: column heading for volume value (eg. "Volume")
			10            String: column heading for movement value (eg. "Movement")

			The stock list starts from slot 11 onwards.

			* Slot ID: Number
			* Current Value: Number
			* Volume: Number
			* Change Value: Number
			* Abreviated Company Name: String
			* Full Company Name: String 
			*/
			g_bStockListMarketMode = FALSE
			g_iStockSelectionFocus = -1 // no stock is currently selected while on this screen
			g_iStockSelectedPortfolioIndex = -1 
			
			g_iStockSellAllQuote = 0
			
			SF_BS_BUTTONBLOCK(pagemov)
			SF_BS_DATELINE(pagemov,4)

			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_MSTOCK")
			END_SCALEFORM_MOVIE_METHOD()
					
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_BUYPRC")
			END_SCALEFORM_MOVIE_METHOD()

			SF_BS_LISTING_HEADER(pagemov,TRUE)
		
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(11.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_PROF")
			END_SCALEFORM_MOVIE_METHOD()
		
		/*
			12 String: sub header1 (eg. "Search Market")
			13 String: sub header2 (eg. "Go to companies starting with:")
			14  String: button for sell all stocks (eg. "Sell All")
		*/
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(12.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SEARCH")
			END_SCALEFORM_MOVIE_METHOD()

			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(13.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_GOSTRT")
			END_SCALEFORM_MOVIE_METHOD()
		
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_TL_SELLALL")
			END_SCALEFORM_MOVIE_METHOD()		
		
			SF_BS_OWNED_LISTINGS(pagemov, 15)
		BREAK
	
		// "STOCK_VIEW"
		CASE 4
		
			g_iStockSellAllQuote = 0
			
			CPRINTLN(DEBUG_INTERNET, "Attempting to fill stock with selected index ", g_iLastSelectedWebIndex)
			IF (g_iLastSelectedWebIndex < 0)
				SCRIPT_ASSERT("website_private: g_iLastSelectedWebIndex is set to an invalid value!! Most likely GET_CURRENT_SELECTION scaleform command not working properly for stock screen!")
			
			// Valid index update page contents
			ELSE 
			
				CPRINTLN(DEBUG_INTERNET, "Setting stock selection focus")
				IF g_iStockSelectionFocus = -1 // only set if stock selection is not set
					IF g_iLastSelectedWebIndex > 10
				
						// This varying offset is because eddie added an extra column for the portfolio screen,
						// it is set in the functions that fill out this screen B*2253395
						g_iStockSelectionFocus = g_iLastSelectedWebIndex - g_iLastSetFinanceSiteListOffset 
						CPRINTLN(DEBUG_INTERNET, "g_iStockSelectionFocus : ", g_iStockSelectionFocus, " is nth stock of type")
						
						
						//its the nth owned of type g_bOnlineMarket
						IF NOT g_bStockListMarketMode
							
							// Index is a portfolio index, find the correct index and set the enum
							CPRINTLN(DEBUG_INTERNET, "Looking up portfolio index for owner : ", ENUM_TO_INT(bttarget))

							benum = DO_PORTFOLIO_INDEX_CONVERSION( bttarget, g_iStockSelectionFocus, g_bOnlineMarket)
							g_iStockSelectionFocus = ENUM_TO_INT(benum)
							g_iStockSelectedPortfolioIndex = -1
				
						ELSE
							//otherwise the index represents a raw stock listing
							//count to nth of same type
							
							//g_bOnlineMarket
							//g_BS_Listings[BS_CO_TOTAL_LISTINGS]
							//find the nth index that matches online
							
							countaset = FALSE
							ncount = 0 // The number of stocks we've searched past of the correct type.
							scount = 0 // The number of stocks we've searched past of any type.
						
							WHILE NOT countaset
								IF scount < ENUM_TO_INT(BS_CO_TOTAL_LISTINGS)
								
									CPRINTLN(DEBUG_INTERNET, "n = ", ncount, " s =  ", scount, " online s index type : ", g_BS_Listings[scount].bOnlineStock , " = ",g_bOnlineMarket )
									
									// We are removing any online listing that has an invalid current price from the server.
									// When we get back the selected scaleform index we need to skip over any invalid listings 
									// from the company enums so that we can find the correct company.
									IF g_bOnlineMarket 
									AND g_BS_Listings[scount].fCurrentPrice <= 0
										CDEBUG3LN(DEBUG_INTERNET, "Found an invalid online stock. Incrementing target index to ", g_iStockSelectionFocus, ".")
										g_iStockSelectionFocus++
									ENDIF
											
									IF (ncount = g_iStockSelectionFocus)
									AND (g_BS_Listings[scount].bOnlineStock = g_bOnlineMarket)
										countaset = TRUE
										g_iStockSelectionFocus = scount
										CPRINTLN(DEBUG_INTERNET, "ncount = g_iStockSelectionFocus : ", g_iStockSelectionFocus)
									ELSE
										//check for max // if not max then count up
										IF scount > ENUM_TO_INT(BS_CO_TOTAL_LISTINGS)-1
											//bail
											SCRIPT_ASSERT("Cannot find nth stock entry of type! ")
											countaset = TRUE
										ELSE
											IF g_BS_Listings[scount].bOnlineStock = g_bOnlineMarket
												++ncount
											ENDIF
											++scount 
										ENDIF
										
										
									ENDIF
								ELSE
									countaset = TRUE
									SCRIPT_ASSERT("INVALID STOCK SELECTION HIT")
								ENDIF
								
								
							ENDWHILE
						
							CPRINTLN(DEBUG_INTERNET, "g_iStockSelectionFocus set to ", g_iStockSelectionFocus)
							benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "Stock selection is already complete, filling out with old selected index")
					benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
				ENDIF
			
			
			
				IF g_bOnlineMarket != g_BS_Listings[benum].bOnlineStock
					g_bOnlineMarket = g_BS_Listings[benum].bOnlineStock
					IF g_bOnlineMarket//WWW_BAWSAQ_COM_S_STOCK_VIEW
						GO_TO_WEBSITE("WWW_BAWSAQ_COM_S_STOCK_VIEW")
					ELSE
						GO_TO_WEBSITE("WWW_LCN_D_EXCHANGE_COM_S_STOCK_VIEW")
					ENDIF
					EXIT
				ENDIF
			
				SF_BS_COMPANY_PAGE(pagemov, benum)
			ENDIF
		BREAK
		
		/* "STOCK_INFO" - DEFUNCT
		CASE 5

			SF_BS_BUTTONBLOCK(pagemov)
			SF_BS_DATELINE(pagemov)
			CPRINTLN(DEBUG_INTERNET, "Attempting to show stock detail with ", g_iStockSelectionFocus)
			benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
			SF_BS_COMPANY_INFO(pagemov, benum)
		BREAK
		*/
		
		// "STOCK_TRADE_BUY"
		CASE 6 		
			IF g_iStockSelectionFocus = -1
				SCRIPT_ASSERT("ON BUYPAGE WITH A STOCK SELECTION INDEX OF -1. Pass this bug to Default Levels.")
				
				
			ELSE
			
				SF_BS_BUTTONBLOCK(pagemov)											
				CPRINTLN(DEBUG_INTERNET, "Updating buypage g_iStockSelectionFocus is : ", g_iStockSelectionFocus)

					
				benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
				SF_BS_BUYPAGE(pagemov, benum)
			ENDIF
		BREAK
		
		// "STOCK_TRADE_SELL"	
		CASE 7 
			IF g_iStockSelectionFocus = -1
				SCRIPT_ASSERT("ON SELLPAGE WITH A STOCK SELECTION INDEX OF -1. Pass this bug to Default Levels.")
				
			ELSE
			
			
				SF_BS_BUTTONBLOCK(pagemov)
				CPRINTLN(DEBUG_INTERNET, "Updating sellpage g_iStockSelectionFocus is : ", g_iStockSelectionFocus)	

				benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
				SF_BS_SELLPAGE(pagemov, benum)
			ENDIF
		BREAK
		
		// "STOCK_TRADE_CONFIRM"
		CASE 8 
			IF g_iStockSelectionFocus = -1
				SCRIPT_ASSERT("ON CONFIRMPAGE WITH A STOCK SELECTION INDEX OF -1. Pass this bug to Default Levels.")
				
				
			ELSE
				benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
				SF_BS_CONFIRMATION(pagemov, benum)
			ENDIF
		BREAK
		
		// "CONFIRMED"
		CASE 9

			IF g_iStockSelectionFocus > -1
				benum = INT_TO_ENUM(BAWSAQ_COMPANIES,g_iStockSelectionFocus)
				SF_BS_CONFIRMED(pagemov, benum)
				
				// Clear the dynamic settings here for now
				
				g_iSharesToBuySell = 0
				CPRINTLN(DEBUG_INTERNET, "g_iSharesToBuySell zeroed by transition to confirm page\n")
				g_fShareTotal = 0
				g_fOriginalProfit = 0 
				g_fCurrentProfit = 0
				
			ELIF g_iStockSelectionFocus = -2
				
				// Might be coming from the sell all page, if so do that special fill out
				SF_BS_CONFIRMED_SELL_ALL(pagemov)
			ELSE 
				SCRIPT_ASSERT("Stock market confirmation page invalid indice. Pass this bug to Default Levels.")
			ENDIF
		BREAK
	
		/*
		// "NEWS"
		CASE 10
		BREAK
		*/
	
		// "STOCK_PORTFOLIO_EMPTY" 
		CASE 11
			SF_BS_BUTTONBLOCK(pagemov)
			SF_BS_DATELINE(pagemov)
			SF_BS_EMPTYPAGE(pagemov)
		BREAK
	
	
		// Main news
		CASE 10 
			SF_BS_BUTTONBLOCK(pagemov)
			//SF_BS_DATELINE(pagemov)
			SF_BS_NEWS_FOR_CATEGORY(pagemov,eSS_main)//eSTOCK_STORY_CATEGORY	
		BREAK
		
		// City news
		CASE 12 
			SF_BS_BUTTONBLOCK(pagemov)
			//SF_BS_DATELINE(pagemov)
			SF_BS_NEWS_FOR_CATEGORY(pagemov,eSS_city)//eSTOCK_STORY_CATEGORY
		BREAK
		
		// Money news
		CASE 13 
			SF_BS_BUTTONBLOCK(pagemov)
			//SF_BS_DATELINE(pagemov)
			SF_BS_NEWS_FOR_CATEGORY(pagemov,eSS_money)//eSTOCK_STORY_CATEGORY
		BREAK
		
		// Tech news
		CASE 14 
			SF_BS_BUTTONBLOCK(pagemov)
			//SF_BS_DATELINE(pagemov)
			SF_BS_NEWS_FOR_CATEGORY(pagemov,eSS_tech)//eSTOCK_STORY_CATEGORY
		BREAK

		CASE 15
	
			// 0 String: button for page1 (eg. "Home")
			// 1 String: button for news page (eg. "News")
			// 2 String: button for markets stocks listing (eg. "Markets")
			// 3 String: button for players stock portfolio (eg. "Portfolio")
			SF_BS_BUTTONBLOCK(pagemov)

			//4 String: button for WWW_LCN_COM/PORTFOLIO (just pass a SPACE as this is a graphic logo)
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			END_SCALEFORM_MOVIE_METHOD()
																
			// 5 String: page header (eg. "The stock server is unavailable")
			// 6 String: message text (eg. "Please try again later.")
			
			IF g_bInMultiplayer
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_FAI1MP")
				END_SCALEFORM_MOVIE_METHOD()

				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_FAI2MP")
				END_SCALEFORM_MOVIE_METHOD()
			ELSE
			
				IF g_bBSWebsiteNoSpaceTrigger
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_FAI1NS")
					END_SCALEFORM_MOVIE_METHOD()

					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_FAI2NS")
					END_SCALEFORM_MOVIE_METHOD()
					
					g_bBSWebsiteNoSpaceTrigger = FALSE //error has been seen
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_FAI1")
					END_SCALEFORM_MOVIE_METHOD()

					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_FAI2")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDIF
		BREAK
	
	
		CASE 16 //sell all
		
			//SLOT ID        String or Number
			//0              String: button for page1 (eg. "Home")
			//1              String: button for news page (eg. "News")
			//2              String: button for markets stocks listing (eg. "Markets")
			//3              String: button for players stock portfolio (eg. "Portfolio")
			SF_BS_BUTTONBLOCK(pagemov)
			
			// 4 String: button for WWW_LCN_D_EXCHANGE_COM_S_STOCK_PORTFOLIO (just pass a SPACE as this is a graphic logo)           
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(" ")
			END_SCALEFORM_MOVIE_METHOD()
			
			// 5 String: page header (eg. "SELL ALL YOUR SHARES")
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(5.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_SL_ALLSHRS")
			END_SCALEFORM_MOVIE_METHOD()
			
			// 6 String: message (eg. "Are you sure you wish to sell all your shares?")
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(6.0)
				//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_YU_SURE")
				
				
				quote = GET_LOGGED_PLAYER_PORTFOLIO_VALUE(bttarget)
				
				
				//cap the quote
				IF (quote+fBalance) > (2147483647.0)
					quote = 2147483647.0 - fBalance //fBalance cannot be more than maxsignedint
				ENDIF
				
				g_iStockSellAllQuote = FLOOR(quote)
				
				quote -= GET_CURRENT_PLAYER_PORTFOLIO_INVESTED(bttarget)
				q = FLOOR(quote)
				
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BS_YU_SURE")
					//profit or loss
					IF quote > 0
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("BS_TL_PROF")   
						ADD_TEXT_COMPONENT_FORMATTED_INTEGER(q, INTEGER_FORMAT_COMMA_SEPARATORS)
					ELSE
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("BS_TL_LOSS")
						ADD_TEXT_COMPONENT_FORMATTED_INTEGER(-1*q, INTEGER_FORMAT_COMMA_SEPARATORS)
					ENDIF
					//total			
					
				END_TEXT_COMMAND_SCALEFORM_STRING()
				
			END_SCALEFORM_MOVIE_METHOD()
			
			// 7 String: button for confirm, goes to WWW_BAWSAQ_COM_S_CONFIRMED (eg."CONFIRM")
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(7.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CONFTRSH")
			END_SCALEFORM_MOVIE_METHOD()
			
			// 8 String: button for cancel, back to portfolio - WWW_BAWSAQ_COM_S_STOCK_PORTFOLIO (eg. "CANCEL")
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(8.0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_WB_CAN")
			END_SCALEFORM_MOVIE_METHOD()
		BREAK
	ENDSWITCH
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
	
	IF g_bTickerPriceChange
		DO_BS_TICKERTAPE(pagemov)
		g_bTickerPriceChange = FALSE
	ENDIF
	
	IF (pageID = 6) OR (pageID = 7)
		IF g_iSharesToBuySell = 0
			//disable the buy button
			//DISABLE_BUTTON(targetSlot:Number, isDisabled:Boolean)
			//20
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(20)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			END_SCALEFORM_MOVIE_METHOD()
		ELSE
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(20)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Enums for bleeter unlocking by mission name.
ENUM BLEET_UNLOCK_ENUM
	BLEET_ARM1 = 0,
	BLEET_ARM2,
	BLEET_ARM3,
	BLEET_FRA0,
	BLEET_FAM1,
	BLEET_FAM2,
	BLEET_FAM3,
	BLEET_LS1,
	BLEET_LM1,
	BLEET_HEIST_JEWELA,
	BLEET_HEIST_JEWELB,
	BLEET_TRV1,
	BLEET_TRV2,
	BLEET_CHI1,
	BLEET_CHI2,
	BLEET_TRV3,
	BLEET_FAM4,
	BLEET_FAM5,
	BLEET_FBI1,
	BLEET_FBI2,
	BLEET_FBI3,
	BLEET_FBI4,
	BLEET_FRA1,
	BLEET_PORT_HEISTA,
	BLEET_PORT_HEISTB,
	BLEET_SOL1,
	BLEET_CS1,
	BLEET_CS2,
	BLEET_MAR1,
	BLEET_CS3,
	BLEET_EXL1,
	BLEET_EXL2,
	BLEET_EXL3,
	BLEET_PSH,
	BLEET_TRV4,
	BLEET_MIC1,
	BLEET_AH3A,
	BLEET_AH3B,
	BLEET_FAM6,
	BLEET_SOL3,
	BLEET_MIC4,
	BLEET_BSHA,
	BLEET_BSHB,
	BLEET_FSMT,
	BLEET_DRE1,
	BLEET_NI1A,
	BLEET_NI1B,
	BLEET_PAP2,
	BLEET_PAP3B,
	BLEET_END_BLEETS
ENDENUM

CONST_INT MAX_BLEETS			480
CONST_INT MAX_BLEET_PAGES		50
CONST_INT MAX_BLEET_HISTORY		20
CONST_INT RC_BLEET_ENUM_OFFSET	200

#IF IS_DEBUG_BUILD
	BOOL bBleeterDebugSpam = FALSE
#ENDIF

// Each mission unlocks a page of bleets. This array stores which mission is attached to
// each page, as the order you unlock them can changed on each play through.

INT iBleetPageContent[MAX_BLEET_PAGES]			// The set of unlocked bleets displayed on a page
INT iStartBleet[MAX_BLEET_PAGES]				// The start bleet for each unlocked set of bleets
INT iEndBleet[MAX_BLEET_PAGES]					// The end bleet in each unlock
INT iBleetMission[MAX_BLEET_PAGES]		// The enum used by the mission flow functionality to define a mission.
INT iCurrentBleetPageNumber
INT iLastBleetPageNumber
INT iBleetPageHistory[MAX_BLEET_HISTORY]		// History of bleet pages.
INT iBleetPageHistoryIndex						// Index of bleet pages.

STRUCT LATEST_TWO_BLEETS_STRUCT
	TEXT_LABEL sBleet1
	TEXT_LABEL sBleet2
ENDSTRUCT

SCALEFORM_INDEX	BleetScaleformIndex	

STRUCT MISSION_REPEAT_INFO
	RepeatPlayData  sMissionFlowData[160] // Sum of Normal and RC mission counts
ENDSTRUCT

/// PURPOSE:
///    Initialises the bleeter history
PROC BLEETER_INIT_HISTORY()

	INT i = 0
	WHILE i < MAX_BLEET_HISTORY
		iBleetPageHistory[i] = 0
		++i
	ENDWHILE
	
	iBleetPageHistoryIndex = 0
ENDPROC

/// PURPOSE:
///   Adds a bleeter page to the history.
PROC BLEETER_ADD_PAGE_TO_HISTORY( INT iPage )

	// If we've not filled up the history yet, then just add the current page and increment the index.
	IF iBleetPageHistoryIndex < (MAX_BLEET_HISTORY - 1)
	
		IF iBleetPageHistoryIndex < 0
			// If the history index is less than 0, reset it to 0 before adding a new page
			CPRINTLN(DEBUG_INTERNET, "BLEETER - Resetting iBleetPageHistoryIndex to 0")
			iBleetPageHistoryIndex = 0
		ENDIF

		++ iBleetPageHistoryIndex
		iBleetPageHistory[iBleetPageHistoryIndex] = iPage
		CPRINTLN(DEBUG_INTERNET, "BLEETER - HISTORY SETTING PAGE ", iPage, " IN HISTORY INDEX ", iBleetPageHistoryIndex)
		
	// We've got a full history, so we need to shuffle down all the entries, discarding the first.
	ELSE
		
		INT i = 1	// Start at second index
		
		WHILE i < MAX_BLEET_HISTORY
			iBleetPageHistory[i - 1] = iBleetPageHistory[i]
			++ i
		ENDWHILE
		
		// Add page to the last entry in the array.
		iBleetPageHistory[iBleetPageHistoryIndex] = iPage
		CPRINTLN(DEBUG_INTERNET, "BLEETER - HISTORY FULL. SETTING PAGE ", iPage, " IN HISTORY INDEX ", iBleetPageHistoryIndex)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Gets the next page in the bleeter history
/// RETURNS:
///    The previous page number, or 0 if the end of the history is reached.
FUNC INT BLEETER_GET_PREVIOUS_PAGE_IN_HISTORY()
		
	-- iBleetPageHistoryIndex
	
	IF iBleetPageHistoryIndex <= 0
		CPRINTLN(DEBUG_INTERNET, "BLEETER - REACHED END OF HISTORY")
		RETURN 0
	ELSE
		CPRINTLN(DEBUG_INTERNET, "BLEETER - HISTORY RETURNING ", iBleetPageHistory[iBleetPageHistoryIndex], " AS PREVIOUS PAGE. HISTORY INDEX ",iBleetPageHistoryIndex)
		RETURN iBleetPageHistory[iBleetPageHistoryIndex]
	ENDIF
ENDFUNC

/*
/// PURPOSE:
///    Gets the next page in the bleeter history
/// RETURNS:
///    The next page number, or 0 if the end of the history is reached.
FUNC INT BLEETER_GET_NEXT_PAGE_IN_HISTORY()
		
	++ iBleetPageHistoryIndex
	
	IF iBleetPageHistoryIndex >= MAX_BLEET_HISTORY
		RETURN 0
	ELSE
		RETURN iBleetPageHistory[iBleetPageHistoryIndex]
	ENDIF
ENDFUNC
*/

/// PURPOSE:
///    Creates a text key based on the bleet number.
/// PARAMS: 
///    The number of the bleet.
/// RETURNS: 
///    A text key string to access the bleet, eg. BLE_1
FUNC TEXT_LABEL BLEETER_GENERATE_BLEET_TEXT_LABEL( INT iBleet )

	STRING sRoot = "BLE_"
	TEXT_LABEL sTextKey
	
	// Check out of range
	IF iBleet < 0
	OR iBleet >= MAX_BLEETS
		CPRINTLN(DEBUG_INTERNET, "GENERATE_BLEETER_TEXT_LABEL - Bleet number out of range, using BLE_BLANK" )
		sTextKey = "BLE_BLANK"
		RETURN sTextKey
	ENDIF 
	
	// Concatenate the key with the int.
	sTextKey = ""
	sTextKey += sRoot
	sTextKey += iBleet
	
	RETURN sTextKey
ENDFUNC

FUNC INT BLEETER_GET_LAST_PAGE_NUMBER()

	IF iLastBleetPageNumber > 0
		RETURN iLastBleetPageNumber
	ENDIF
	
	RETURN iLastBleetPageNumber
ENDFUNC

/// PURPOSE:
///    Displays a page of bleets
/// PARAMS:
///    iStartBleet - Bleet number to start at.
PROC BLEETER_DISPLAY_PAGE_OF_BLEETS( INT iBleetPage, SCALEFORM_INDEX pagemov )
	
	TEXT_LABEL sBleetLabel
		
	IF iBleetPage < 1
	OR iBleetPage > MAX_BLEET_PAGES
		CPRINTLN(DEBUG_INTERNET, "BLEETER: PAGE NUMBER: ", iBleetPage )
		SCRIPT_ASSERT( "BLEETER: PAGE NUMBER OUT OF BOUNDS!")
	ENDIF
	
	INT iFirstBleet = iStartBleet[iBleetPageContent[iBleetPage - 1]]
	INT iLastBleet = iEndBleet[iBleetPageContent[iBleetPage - 1]]
	INT iBleetsThisPage = (iLastBleet - iFirstBleet) + 1
	
	CPRINTLN(DEBUG_INTERNET, "BLEETER: BLEETER_DISPLAY_PAGE_OF_BLEETS - iFirstBleet = ",iFirstBleet," iLastBleet = ", iLastBleet, " iBleetsThisPage = ", iBleetsThisPage)
	
	IF iBleetsThisPage <1
		SCRIPT_ASSERT( "BLEETER: BLEETER_DISPLAY_PAGE - iBleetsThisPage < 1")
		EXIT
	ENDIF
	
	IF iFirstBleet < 0
	OR iFirstBleet >= MAX_BLEETS
		SCRIPT_ASSERT( "BLEETER: BLEETER_DISPLAY_PAGE - Bleet number out of range")
		EXIT
	ENDIF
	
	INT iCount = 0
	INT iCurrentBleet
	WHILE iCount < iBleetsThisPage
	
		iCurrentBleet = iFirstBleet + iCount
		
		// If we've reached the oldest bleet, then quit. (Shouldn't really happen as the number of pages and bleets per page should match.)
		IF iCurrentBleet >= MAX_BLEETS
			CPRINTLN(DEBUG_MISSION, "BLEETER: BLEETER_DISPLAY_PAGE_OF_BLEETS - REACHED LAST BLEET IN LIST - EXITING")
			EXIT
		ENDIF
		
		sBleetLabel = BLEETER_GENERATE_BLEET_TEXT_LABEL(iCurrentBleet)
		
		CPRINTLN(DEBUG_MISSION, "BLEETER: BLEETER_DISPLAY_PAGE_OF_BLEETS - DISPLAYING BLEET ", iCurrentBleet, " ", sBleetLabel  )
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT( iCount + 2 ) // Bleets start from slot 2
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING( sBleetLabel )
		END_SCALEFORM_MOVIE_METHOD()
			
		++ iCount
	ENDWHILE	
ENDPROC

PROC BLEETER_DISPLAY_PAGE_NUMBERS( INT iCurrentPage, SCALEFORM_INDEX pagemov )

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT( 1 ) // Slot 1 is the page numbers.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT( iCurrentPage )
		
		INT iBleetStartPageNumber = (((iCurrentPage - 1)  / 10) * 10) + 1
		INT iBleetEndPageNumber = iBleetStartPageNumber + 10
		
		IF iCurrentPage < 10
			iBleetStartPageNumber = 1
			iBleetEndPageNumber = 10
		ELSE
			iBleetStartPageNumber = (((iCurrentPage - 1)  / 10) * 10) + 1
			iBleetEndPageNumber = iBleetStartPageNumber + 10
		ENDIF
		
		IF iBleetEndPageNumber > iLastBleetPageNumber
			iBleetEndPageNumber = iLastBleetPageNumber
		ENDIF
				
		INT iCount = iBleetStartPageNumber
		WHILE iCount <= iBleetEndPageNumber // Max pages
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT( iCount )
			++iCount
		ENDWHILE
		
	END_SCALEFORM_MOVIE_METHOD()
	
	// Disable/enable first and previous buttons if the first page is selected.
	IF iCurrentBleetPageNumber = 1
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()	
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Disable/enable last and next buttons if the last page is selected.
	IF iCurrentBleetPageNumber = BLEETER_GET_LAST_PAGE_NUMBER()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()	
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"DISABLE_BUTTON")	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC INIT_BLEETS(MISSION_REPEAT_INFO &sRepeatInfo)

	// Initialise the starting bleet for each unlock
	iStartBleet[ENUM_TO_INT(BLEET_ARM1)]	= 0
	iEndBleet[ENUM_TO_INT(BLEET_ARM1)]		= 9
	
	iStartBleet[ENUM_TO_INT(BLEET_ARM2)] 	= 10
	iEndBleet[ENUM_TO_INT(BLEET_ARM2)] 	 	= 19

	iStartBleet[ENUM_TO_INT(BLEET_ARM3)] 	= 20
	iEndBleet[ENUM_TO_INT(BLEET_ARM3)] 		= 29
	
	iStartBleet[ENUM_TO_INT(BLEET_FRA0)]	= 30
	iEndBleet[ENUM_TO_INT(BLEET_FRA0)]		= 39
	
	iStartBleet[ENUM_TO_INT(BLEET_FAM1)] 	= 40
	iEndBleet[ENUM_TO_INT(BLEET_FAM1)]		= 49
	
	iStartBleet[ENUM_TO_INT(BLEET_FAM2)] 	= 50
	iEndBleet[ENUM_TO_INT(BLEET_FAM2)] 		= 58
	
	iStartBleet[ENUM_TO_INT(BLEET_FAM3)]	= 60
	iEndBleet[ENUM_TO_INT(BLEET_FAM3)]		= 79
		
	iStartBleet[ENUM_TO_INT(BLEET_LS1)]		= 80
	iEndBleet[ENUM_TO_INT(BLEET_LS1)] 		= 89
	
	iStartBleet[ENUM_TO_INT(BLEET_LM1)] 	= 90
	iEndBleet[ENUM_TO_INT(BLEET_LM1)] 		= 98
	
	// Update 11A
	iStartBleet[ENUM_TO_INT(BLEET_HEIST_JEWELA)] = 99
	iEndBleet[ENUM_TO_INT(BLEET_HEIST_JEWELA)] 	= 108
	
	// Update 11B
	iStartBleet[ENUM_TO_INT(BLEET_HEIST_JEWELB)] = 99
	iEndBleet[ENUM_TO_INT(BLEET_HEIST_JEWELB)] 	= 108
	
	// Update 12
	iStartBleet[ENUM_TO_INT(BLEET_TRV1)] 	= 109
	iEndBleet[ENUM_TO_INT(BLEET_TRV1)] 		= 118
	
	// Update 13
	iStartBleet[ENUM_TO_INT(BLEET_TRV2)] 	= 119
	iEndBleet[ENUM_TO_INT(BLEET_TRV2)] 		= 128
	
	// Update 14
	iStartBleet[ENUM_TO_INT(BLEET_CHI1)] 	= 129
	iEndBleet[ENUM_TO_INT(BLEET_CHI1)] 		= 138
	
	// Update 15
	iStartBleet[ENUM_TO_INT(BLEET_CHI2)] 	= 139
	iEndBleet[ENUM_TO_INT(BLEET_CHI2)] 		= 148
	
	// Update 16
	iStartBleet[ENUM_TO_INT(BLEET_TRV3)] 	= 149
	iEndBleet[ENUM_TO_INT(BLEET_TRV3)] 		= 158
	
	// Update 17
	iStartBleet[ENUM_TO_INT(BLEET_FAM4)] 	= 159
	iEndBleet[ENUM_TO_INT(BLEET_FAM4)] 		= 168
	
	// Update 18
	iStartBleet[ENUM_TO_INT(BLEET_FAM5)] 	= 169
	iEndBleet[ENUM_TO_INT(BLEET_FAM5)] 		= 179

	// Update 19
	iStartBleet[ENUM_TO_INT(BLEET_FBI1)] 	= 180
	iEndBleet[ENUM_TO_INT(BLEET_FBI1)] 		= 189
	
	// Update 20
	iStartBleet[ENUM_TO_INT(BLEET_FBI2)] 	= 190
	iEndBleet[ENUM_TO_INT(BLEET_FBI2)] 		= 199

	// Update 21
	iStartBleet[ENUM_TO_INT(BLEET_FBI3)] 	= 200
	iEndBleet[ENUM_TO_INT(BLEET_FBI3)] 		= 209
	
	// Update 22
	iStartBleet[ENUM_TO_INT(BLEET_FBI4)] 	= 210
	iEndBleet[ENUM_TO_INT(BLEET_FBI4)] 		= 219
	
	// Update 23
	iStartBleet[ENUM_TO_INT(BLEET_FRA1)] 	= 220
	iEndBleet[ENUM_TO_INT(BLEET_FRA1)] 		= 230
	
	// Update 24a
	iStartBleet[ENUM_TO_INT(BLEET_PORT_HEISTA)] = 231
	iEndBleet[ENUM_TO_INT(BLEET_PORT_HEISTA)] 	= 240

	// Update 24b
	iStartBleet[ENUM_TO_INT(BLEET_PORT_HEISTB)] = 241
	iEndBleet[ENUM_TO_INT(BLEET_PORT_HEISTB)] 	= 250
	
	// Update 25 moved to later in the flow (Trevor 4)
	
	// Update 26
	iStartBleet[ENUM_TO_INT(BLEET_SOL1)] 	= 251
	iEndBleet[ENUM_TO_INT(BLEET_SOL1)] 		= 260
	
	// Update 27
	iStartBleet[ENUM_TO_INT(BLEET_CS1)] 	= 261
	iEndBleet[ENUM_TO_INT(BLEET_CS1)] 		= 270
	
	// This one changes depending on if you kill or spare Chad
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN)
		// Update 28a - Kills Chad
		iStartBleet[ENUM_TO_INT(BLEET_CS2)] = 271 
		iEndBleet[ENUM_TO_INT(BLEET_CS2)] 	= 280
	ELSE
		// Update 28b- Spares chad
		iStartBleet[ENUM_TO_INT(BLEET_CS2)] = 281
		iEndBleet[ENUM_TO_INT(BLEET_CS2)] 	= 290
	ENDIF
		
	// Update 29
	iStartBleet[ENUM_TO_INT(BLEET_MAR1)] 	= 291
	iEndBleet[ENUM_TO_INT(BLEET_MAR1)] 		= 300
	
	// Update 30 Sharmoota heist CUT

	// Update 31
	iStartBleet[ENUM_TO_INT(BLEET_CS3)] 	= 301
	iEndBleet[ENUM_TO_INT(BLEET_CS3)] 		= 310

	// Update 32
	iStartBleet[ENUM_TO_INT(BLEET_EXL1)] 	= 311
	iEndBleet[ENUM_TO_INT(BLEET_EXL1)] 		= 320
	
	// 3RD batch
	
	// Update 33
	iStartBleet[ENUM_TO_INT(BLEET_EXL2)] 	= 321
	iEndBleet[ENUM_TO_INT(BLEET_EXL2)] 		= 329
	
	// Update 34
	iStartBleet[ENUM_TO_INT(BLEET_EXL3)] 	= 330
	iEndBleet[ENUM_TO_INT(BLEET_EXL3)] 		= 339
	
	// Update 35
	iStartBleet[ENUM_TO_INT(BLEET_PSH)] 	= 340
	iEndBleet[ENUM_TO_INT(BLEET_PSH)] 		= 349
	
	// Update 36
	iStartBleet[ENUM_TO_INT(BLEET_TRV4)] 	= 350
	iEndBleet[ENUM_TO_INT(BLEET_TRV4)] 		= 359
	
	// Update 37
	iStartBleet[ENUM_TO_INT(BLEET_MIC1)] 	= 360
	iEndBleet[ENUM_TO_INT(BLEET_MIC1)] 		= 369
	
	// Update 38A
	iStartBleet[ENUM_TO_INT(BLEET_AH3A)] 	= 370
	iEndBleet[ENUM_TO_INT(BLEET_AH3A)	] 	= 379

	// Update 38B
	iStartBleet[ENUM_TO_INT(BLEET_AH3B)] 	= 370
	iEndBleet[ENUM_TO_INT(BLEET_AH3B)	] 	= 379
	
	// Update 39
	iStartBleet[ENUM_TO_INT(BLEET_FAM6)] 	= 380
	iEndBleet[ENUM_TO_INT(BLEET_FAM6)] 		= 389
	
	// Update 40
	iStartBleet[ENUM_TO_INT(BLEET_SOL3)] 	= 390
	iEndBleet[ENUM_TO_INT(BLEET_SOL3)] 		= 399
	
	// Update 41
	iStartBleet[ENUM_TO_INT(BLEET_MIC4)] 	= 400
	iEndBleet[ENUM_TO_INT(BLEET_MIC4)] 		= 409
	
	// Update 42
	iStartBleet[ENUM_TO_INT(BLEET_BSHA)] 	= 410
	iEndBleet[ENUM_TO_INT(BLEET_BSHA)] 		= 419
	
	// Update 42
	iStartBleet[ENUM_TO_INT(BLEET_BSHB)] 	= 410
	iEndBleet[ENUM_TO_INT(BLEET_BSHB)] 		= 419
	
	// Update 43
	iStartBleet[ENUM_TO_INT(BLEET_FSMT)] 	= 420
	iEndBleet[ENUM_TO_INT(BLEET_FSMT)] 		= 429
	
	// Update 44
	iStartBleet[ENUM_TO_INT(BLEET_DRE1)] 	= 430
	iEndBleet[ENUM_TO_INT(BLEET_DRE1)] 		= 439
	
	// Update 45
	iStartBleet[ENUM_TO_INT(BLEET_NI1A)] 	= 440
	iEndBleet[ENUM_TO_INT(BLEET_NI1A)] 		= 449
	
	// Update 46
	iStartBleet[ENUM_TO_INT(BLEET_NI1B)] 	= 450
	iEndBleet[ENUM_TO_INT(BLEET_NI1B)] 		= 458
	
	// Update 47
	iStartBleet[ENUM_TO_INT(BLEET_PAP2)] 	= 459
	iEndBleet[ENUM_TO_INT(BLEET_PAP2)] 		= 468
	
	// Update 48
	iStartBleet[ENUM_TO_INT(BLEET_PAP3B)] 	= 469
	iEndBleet[ENUM_TO_INT(BLEET_PAP3B)] 	= 478
	
	
	// Store the mission id of the mission that unlocks that batch of tweets
	// Update 1
	iBleetMission[ENUM_TO_INT(BLEET_ARM1)] = ENUM_TO_INT(SP_MISSION_ARMENIAN_1)
	iBleetMission[ENUM_TO_INT(BLEET_ARM2)] = ENUM_TO_INT(SP_MISSION_ARMENIAN_2)
	iBleetMission[ENUM_TO_INT(BLEET_ARM3)] = ENUM_TO_INT(SP_MISSION_ARMENIAN_3)
	iBleetMission[ENUM_TO_INT(BLEET_FRA0)] = ENUM_TO_INT(SP_MISSION_FRANKLIN_0)
	iBleetMission[ENUM_TO_INT(BLEET_FAM1)] = ENUM_TO_INT(SP_MISSION_FAMILY_1)
	iBleetMission[ENUM_TO_INT(BLEET_FAM2)] = ENUM_TO_INT(SP_MISSION_FAMILY_2)
	iBleetMission[ENUM_TO_INT(BLEET_FAM3)] = ENUM_TO_INT(SP_MISSION_FAMILY_3)
	iBleetMission[ENUM_TO_INT(BLEET_LS1)] = ENUM_TO_INT(SP_MISSION_LESTER_1)
	iBleetMission[ENUM_TO_INT(BLEET_LM1)]  = ENUM_TO_INT(SP_MISSION_LAMAR)
	
	// Update 2
	iBleetMission[ENUM_TO_INT(BLEET_HEIST_JEWELA)] = ENUM_TO_INT(SP_HEIST_JEWELRY_2) 
	iBleetMission[ENUM_TO_INT(BLEET_HEIST_JEWELB)] = ENUM_TO_INT(SP_HEIST_JEWELRY_2)
	iBleetMission[ENUM_TO_INT(BLEET_TRV1)]  = ENUM_TO_INT(SP_MISSION_TREVOR_1)
	iBleetMission[ENUM_TO_INT(BLEET_TRV2)]  = ENUM_TO_INT(SP_MISSION_TREVOR_2)
	iBleetMission[ENUM_TO_INT(BLEET_CHI1)]  = ENUM_TO_INT(SP_MISSION_CHINESE_1)
	iBleetMission[ENUM_TO_INT(BLEET_CHI2)]  = ENUM_TO_INT(SP_MISSION_CHINESE_2)
	iBleetMission[ENUM_TO_INT(BLEET_TRV3)]  = ENUM_TO_INT(SP_MISSION_TREVOR_3)
	iBleetMission[ENUM_TO_INT(BLEET_FAM4)]  = ENUM_TO_INT(SP_MISSION_FAMILY_4)
	iBleetMission[ENUM_TO_INT(BLEET_FAM5)]  = ENUM_TO_INT(SP_MISSION_FAMILY_5)
	iBleetMission[ENUM_TO_INT(BLEET_FBI1)]  = ENUM_TO_INT(SP_MISSION_FBI_1)
	iBleetMission[ENUM_TO_INT(BLEET_FBI2)]  = ENUM_TO_INT(SP_MISSION_FBI_2)
	iBleetMission[ENUM_TO_INT(BLEET_FBI3)]  = ENUM_TO_INT(SP_MISSION_FBI_3)
	iBleetMission[ENUM_TO_INT(BLEET_FBI4)]  = ENUM_TO_INT(SP_MISSION_FBI_4)
	iBleetMission[ENUM_TO_INT(BLEET_FRA1)] = ENUM_TO_INT(SP_MISSION_FRANKLIN_1)
	iBleetMission[ENUM_TO_INT(BLEET_PORT_HEISTA)] = ENUM_TO_INT(SP_HEIST_DOCKS_2A)
	iBleetMission[ENUM_TO_INT(BLEET_PORT_HEISTB)] = ENUM_TO_INT(SP_HEIST_DOCKS_2B)
	iBleetMission[ENUM_TO_INT(BLEET_SOL1)] = ENUM_TO_INT(SP_MISSION_SOLOMON_1)
	iBleetMission[ENUM_TO_INT(BLEET_CS1)] = ENUM_TO_INT(SP_MISSION_CARSTEAL_1)
	iBleetMission[ENUM_TO_INT(BLEET_CS2)] = ENUM_TO_INT(SP_MISSION_CARSTEAL_2)  	// These will be overriden by special code.
	iBleetMission[ENUM_TO_INT(BLEET_MAR1)] = ENUM_TO_INT(SP_MISSION_MARTIN_1)
	iBleetMission[ENUM_TO_INT(BLEET_CS3)] = ENUM_TO_INT(SP_MISSION_CARSTEAL_3)
	iBleetMission[ENUM_TO_INT(BLEET_EXL1)] = ENUM_TO_INT(SP_MISSION_EXILE_1)
	
	// Update 3
	iBleetMission[ENUM_TO_INT(BLEET_EXL2)]  = ENUM_TO_INT(SP_MISSION_EXILE_2)
	iBleetMission[ENUM_TO_INT(BLEET_EXL3)]  = ENUM_TO_INT(SP_MISSION_EXILE_3)
	iBleetMission[ENUM_TO_INT(BLEET_PSH)]  = ENUM_TO_INT(SP_HEIST_RURAL_2)
	iBleetMission[ENUM_TO_INT(BLEET_TRV4)]  = ENUM_TO_INT(SP_MISSION_TREVOR_4)
	iBleetMission[ENUM_TO_INT(BLEET_MIC1)]  = ENUM_TO_INT(SP_MISSION_MICHAEL_1)
	iBleetMission[ENUM_TO_INT(BLEET_AH3A)]  = ENUM_TO_INT(SP_HEIST_AGENCY_3A)
	iBleetMission[ENUM_TO_INT(BLEET_AH3B)]  = ENUM_TO_INT(SP_HEIST_AGENCY_3B)
	iBleetMission[ENUM_TO_INT(BLEET_FAM6)]  = ENUM_TO_INT(SP_MISSION_FAMILY_6)
	iBleetMission[ENUM_TO_INT(BLEET_SOL3)]  = ENUM_TO_INT(SP_MISSION_SOLOMON_3)
	iBleetMission[ENUM_TO_INT(BLEET_MIC4)]  = ENUM_TO_INT(SP_MISSION_MICHAEL_4)
	iBleetMission[ENUM_TO_INT(BLEET_BSHA)]  = ENUM_TO_INT(SP_HEIST_FINALE_2A)
	iBleetMission[ENUM_TO_INT(BLEET_BSHB)]  = ENUM_TO_INT(SP_HEIST_FINALE_2B)
	iBleetMission[ENUM_TO_INT(BLEET_FSMT)]  = ENUM_TO_INT(SP_MISSION_FINALE_C2)
	
	// RC missions - an offset is added to the mission IDs so we can distinguish them from the normal missions.
	iBleetMission[ENUM_TO_INT(BLEET_DRE1)]  = ENUM_TO_INT(RC_DREYFUSS_1) + RC_BLEET_ENUM_OFFSET
	iBleetMission[ENUM_TO_INT(BLEET_NI1A)]  = ENUM_TO_INT(RC_NIGEL_1A) + RC_BLEET_ENUM_OFFSET
	iBleetMission[ENUM_TO_INT(BLEET_NI1B)]  = ENUM_TO_INT(RC_NIGEL_1B) + RC_BLEET_ENUM_OFFSET
	iBleetMission[ENUM_TO_INT(BLEET_PAP2)]  = ENUM_TO_INT(RC_PAPARAZZO_2) + RC_BLEET_ENUM_OFFSET
	iBleetMission[ENUM_TO_INT(BLEET_PAP3B)]  = ENUM_TO_INT(RC_PAPARAZZO_3B) + RC_BLEET_ENUM_OFFSET

	// UNLOCKING MECHANISM

	INT iNumMissions
	INT iMissionCompleted

	// Commenting out due to TTY spam - enable for debugging...
	#IF IS_DEBUG_BUILD
	
		STRING sMissionName
		SP_MISSIONS 		eMissionID
		g_eRC_MissionIDs	eMissionIdRC
		
	#ENDIF
	
/*
	// Init the repeat play array
	iMissionCompleted = 0 
	WHILE iMissionCompleted < 160
		sMissionFlowData[iMissionCompleted].iMissionIndex = -1
		++ iMissionCompleted
	ENDWHILE
*/

	PopulateRepeatPlayArray(sRepeatInfo.sMissionFlowData, TRUE, TRUE, FALSE)
	
	iNumMissions = GetTotalCompletionOrder()
	
	CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Number of played missions = ", iNumMissions)
		
	// We always have at least one page of bleets.
	IF iNumMissions = 0
	OR IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: No missions unlocked or repeat play is active. Displaying initial page only.")
		iBleetPageContent[0] = ENUM_TO_INT(BLEET_ARM1)
		iCurrentBleetPageNumber = 1
		iLastBleetPageNumber = 1
		EXIT
	ENDIF
									
	// Because thw RC missions are a different enum set we go through the repeat play array
	// and add an offset to every RC mission found, so their enum values don't conflict
	// with the normal missions enums.
	
	//BOOL bReachedEnd = FALSE
	
	iMissionCompleted = 0
	WHILE iMissionCompleted < iNumMissions
		
		//IF sMissionFlowData[iMissionCompleted].iMissionIndex = -1
		//	bReachedEnd = TRUE
		//ELSE
		
			IF sRepeatInfo.sMissionFlowData[iMissionCompleted].eMissionType = CP_GROUP_RANDOMCHARS
				sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex += RC_BLEET_ENUM_OFFSET
			ENDIF
			
			#IF IS_DEBUG_BUILD
				
				IF bBleeterDebugSpam

					IF sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex < RC_BLEET_ENUM_OFFSET
						eMissionId = INT_TO_ENUM(SP_MISSIONS, sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex)
						sMissionName = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionId)
						CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Mission ", iMissionCompleted, " = ", sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex, " ",sMissionName )
					ELSE
						eMissionIdRC = INT_TO_ENUM(g_eRC_MissionIDs, sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex - RC_BLEET_ENUM_OFFSET)
						sMissionName = GET_RC_MISSION_DISPLAY_STRING_FROM_ID(eMissionIdRC)
						CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Mission ", iMissionCompleted, " = ", sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex, " ",sMissionName )
					ENDIF
				
				ENDIF
				
			#ENDIF
		
			++ iMissionCompleted
		
		//ENDIF
	
	ENDWHILE
			
	iLastBleetPageNumber = 0
	
	iMissionCompleted --
	
	INT iBleetUnlock
	
	// Interate through the missions in reverse order
	WHILE iMissionCompleted >= 0
	
		iBleetUnlock = 0
		
			#IF IS_DEBUG_BUILD
				IF bBleeterDebugSpam
					IF sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex < RC_BLEET_ENUM_OFFSET
						eMissionId = INT_TO_ENUM(SP_MISSIONS, sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex)
						sMissionName = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionId)
						CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Checking mission ID ", sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex, " ", sMissionName )
					ELSE 
						eMissionIdRC = INT_TO_ENUM(g_eRC_MissionIDs, sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex - RC_BLEET_ENUM_OFFSET)
						sMissionName = GET_RC_MISSION_DISPLAY_STRING_FROM_ID(eMissionIdRC)
						CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Checking RC mission ID ", sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex, " ", sMissionName )
					ENDIF
				ENDIF
			#ENDIF
		
			// Check each bleet unlock against the mission id.
			WHILE iBleetUnlock < MAX_BLEET_PAGES 
				
				IF iBleetMission[iBleetUnlock] = sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex
				
					IF iLastBleetPageNumber < MAX_BLEET_PAGES
						iBleetPageContent[iLastBleetPageNumber] = iBleetUnlock

						#IF IS_DEBUG_BUILD
							IF bBleeterDebugSpam
								
								IF sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex < RC_BLEET_ENUM_OFFSET
									eMissionId = INT_TO_ENUM(SP_MISSIONS, sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex)
									sMissionName = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionId)	
									CPRINTLN(DEBUG_INTERNET,"INIT_BLEETS:  NORMAL MISSIONS: Unlocking Bleetcount: ", iBleetUnlock)
									CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Unlocking mission ID ", sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex," ", sMissionName, " in page ", iLastBleetPageNumber + 1 )
								ELSE
									eMissionIdRC = INT_TO_ENUM(g_eRC_MissionIDs, sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex - RC_BLEET_ENUM_OFFSET)
									sMissionName = GET_RC_MISSION_DISPLAY_STRING_FROM_ID(eMissionIdRC)	
									CPRINTLN(DEBUG_INTERNET,"INIT_BLEETS: RC MISSIONS: Unlocking Bleetcount: ", iBleetUnlock)
									CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: Unlocking Random Char mission ID ", sRepeatInfo.sMissionFlowData[iMissionCompleted].iMissionIndex," ", sMissionName, " in page ", iLastBleetPageNumber + 1 )
								ENDIF
							ENDIF
						#ENDIF
						
						iLastBleetPageNumber++
						
						// Use this to force a break from the while loop, as we've no need to parse any more this iteration.
						iBleetUnlock = MAX_BLEET_PAGES + 1
					ELSE
						CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: iLastBleetPageNumber>=  MAX_BLEET_PAGES")
					ENDIF
				ENDIF
			
				++ iBleetUnlock
			ENDWHILE
		
		-- iMissionCompleted
	ENDWHILE
	
	IF iLastBleetPageNumber = 0
		CPRINTLN(DEBUG_INTERNET, "INIT_BLEETS: No missions matched. Displaying initial page only.")
		iBleetPageContent[0] = ENUM_TO_INT(BLEET_ARM1)
		iLastBleetPageNumber = 1
	ENDIF
	
	// Init the custom page history for the bleeter site.
	BLEETER_INIT_HISTORY()
	iCurrentBleetPageNumber = 1
ENDPROC

FUNC LATEST_TWO_BLEETS_STRUCT BLEETER_GET_MOST_RECENT_TWO_BLEETS(MISSION_REPEAT_INFO &sRepeatInfo)

 	LATEST_TWO_BLEETS_STRUCT sLatestBleets
	
	INIT_BLEETS(sRepeatInfo)
	
	sLatestBleets.sBleet1 = BLEETER_GENERATE_BLEET_TEXT_LABEL( iStartBleet[iBleetPageContent[0]])
	sLatestBleets.sBleet2 = BLEETER_GENERATE_BLEET_TEXT_LABEL( iStartBleet[iBleetPageContent[0]] + 1 )
	
	CPRINTLN(DEBUG_INTERNET, "BLEETER_GET_MOST_RECENT_TWO_BLEETS: Most recent bleet text keys are ", sLatestBleets.sBleet1," and ", sLatestBleets.sBleet2 )
	
	RETURN sLatestBleets
ENDFUNC

PROC BLEETER_UPDATE_BLEETS(	SCALEFORM_INDEX pagemov)
	
	BLEETER_DISPLAY_PAGE_OF_BLEETS( iCurrentBleetPageNumber, pagemov )
	BLEETER_DISPLAY_PAGE_NUMBERS( iCurrentBleetPageNumber, pagemov )
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SF_BLEETER(SCALEFORM_INDEX pagemov, int pageID, MISSION_REPEAT_INFO &sRepeatInfo)
		
	INT unnusedvarbla = pageID
	unnusedvarbla = unnusedvarbla
		
	BleetScaleformIndex = pagemov
		
	INIT_BLEETS(sRepeatInfo)	
	BLEETER_UPDATE_BLEETS( pagemov )
	BLEETER_ADD_PAGE_TO_HISTORY(iCurrentBleetPageNumber)
ENDPROC

FUNC BOOL BLEETER_BACK_BUTTON_INTERCEPT()

	CPRINTLN(DEBUG_INTERNET, "BLEETER - BACK BUTTON INTERCEPT")	
	
	INT iPage = BLEETER_GET_PREVIOUS_PAGE_IN_HISTORY()
	
	// Zero indicates we've reached the bottom of the page history.
	IF iPage > 0
		iCurrentBleetPageNumber = iPage
		BLEETER_UPDATE_BLEETS( BleetScaleformIndex )

		RETURN TRUE
	ENDIF
	
	// If you don't hijack the back button press so the browser can do it's thing, otherwise true
	RETURN FALSE  
ENDFUNC

PROC SCALEFORM_MOVIE_METHOD_ADD_PARAM_WEBSITE_INDEX(WEBSITE_INDEX_ENUM site, BOOL bWithSmugglerDLCPlane)
	SWITCH site
		CASE WWW_DOCKTEASE_COM						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) BREAK
		CASE WWW_DYNASTY8REALESTATE_COM				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) BREAK
		CASE WWW_ELITASTRAVEL_COM
			IF NOT bWithSmugglerDLCPlane
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(10)
			ENDIF
		BREAK
		CASE WWW_LEGENDARYMOTORSPORT_NET			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3) BREAK
		CASE WWW_BENNYSORIGINALMOTORWORKS_COM		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4) BREAK
		CASE WWW_PANDMCYCLES_COM					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5) BREAK
		CASE WWW_SOUTHERNSANANDREASSUPERAUTOS_COM	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6) BREAK
		CASE WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM
			IF NOT bWithSmugglerDLCPlane
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(11)
			ENDIF
		BREAK
		CASE WWW_DYNASTY8EXECUTIVEREALTY_COM		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8) BREAK
		CASE FORECLOSURES_MAZE_D_BANK_COM			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9) BREAK
		CASE WWW_ARENAWAR_TV						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(13) BREAK
		CASE WWW_THEDIAMONDCASINOANDRESORT_COM		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(14) BREAK
		
		DEFAULT
			CASSERTLN(DEBUG_INTERNET, "Invalid site ID \"", GET_WEBSITE_FROM_INDEX(site), "\"")
		BREAK
	ENDSWITCH
ENDPROC

PROC SCALEFORM_MOVIE_METHOD_ADD_EYEFIND_DISCOUNT(WEBSITE_INDEX_ENUM site, FLOAT fDiscount)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_WEBSITE_INDEX(site, FALSE)
	
	IF fDiscount = 0.0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_00")	//Discounts available on Selected Vehicles
	ELIF fDiscount = 5.0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_05")	//5% Off Selected Vehicles
	ELIF fDiscount = 10.0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_10")	//10% Off Selected Vehicles
	ELIF fDiscount = 15.0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_15")	//15% Off Selected Vehicles
	ELIF fDiscount = 20.0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_20")	//20% Off Selected Vehicles
	ELIF fDiscount = 25.0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_25")	//25% Off Selected Vehicles
	ELSE
		CASSERTLN(DEBUG_INTERNET, "Invalid discount value ", fDiscount, "% for site ID \"", GET_WEBSITE_FROM_INDEX(site), "\"")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC")		//~1~% Off Selected Vehicles
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CEIL(fDiscount))
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles Eyefind page setup

/// PURPOSE:
///    Handles Eyefind page setup
/// PARAMS:
///    pagemov - 
///    pageID - 
///    bUpdateEyeFindNewsStory - fix to override if the news story updates
PROC SF_EYEFIND(SCALEFORM_INDEX pagemov, INT pageID, MISSION_REPEAT_INFO &sRepeatInfo, BOOL bUpdateEyeFindNewsStory = TRUE)

	CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(pageID: ", pageID, ")")	
	
	LATEST_TWO_BLEETS_STRUCT ltbs
	INT iDay = ENUM_TO_INT(GET_CLOCK_DAY_OF_WEEK())
	INT hWeather = GET_NEXT_WEATHER_TYPE_HASH_NAME()
	INT iWeather
	BOOL bNewStoryAvailable = FALSE
	
	SWITCH pageID
		CASE 1 // Front
		CASE 4 // Back
		CASE 5 // News
	//	CASE 7 // Inbox
	//	CASE 8 // Compose Email


//1	Int	Temperature, eg 50.

//4	String	News article title.
//5	Int	News organisation image enum (Weazel, Daily Globe etc) - see news org enums below.
//6	String	New article body text.




//10	Int	Number of emails (displays on the inbox button).

			// Get current weather
			SWITCH hWeather
				CASE HASH("CLEARING")
				CASE HASH("NEUTRAL")
				CASE HASH("CLEAR")
					iWeather = 1
				BREAK
	
				CASE HASH("OVERCAST")
				CASE HASH("SMOG")
				CASE HASH("CLOUDY")	
					iWeather = 3
				BREAK
				
				CASE HASH("CLOUDS")	 iWeather = 2 BREAK
				CASE HASH("RAIN")	 iWeather = 4 BREAK
				CASE HASH("THUNDER") iWeather = 6 BREAK
				CASE HASH("SNOW")	 iWeather = 7 BREAK
		
				DEFAULT
					iWeather = 0
				BREAK
			ENDSWITCH

			//0	Int	Weather icon - see weather icons table below.
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWeather)
			END_SCALEFORM_MOVIE_METHOD()

			//2	String	Area, eg "Alamo Sea".
			IF IS_PLAYER_PLAYING(GET_PLAYER_INDEX())
	
				VECTOR VecCoors
				VecCoors = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()))
				IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
					VecCoors = GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
				ENDIF
				
				STRING areaName
				areaName = GET_NAME_OF_ZONE(VecCoors)
		
				CPRINTLN(DEBUG_INTERNET, "Get area name = ", areaName, ", VecCoors = ", VecCoors)
		
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(areaName)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
			
			//3	String	Day, eg "Saturday".
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
				SWITCH iDay
					CASE 0 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_SU") BREAK
					CASE 1 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_M")  BREAK
					CASE 2 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_TU") BREAK
					CASE 3 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_W")  BREAK
					CASE 4 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_TH") BREAK
					CASE 5 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_F")  BREAK
					CASE 6 SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BS_PH_SA") BREAK
				ENDSWITCH
			END_SCALEFORM_MOVIE_METHOD()
				
			// Set News Story

			// Set the current story if it's the first time
			IF ARE_STRINGS_EQUAL(sEyeFindCurrentNewStory.tl23temp_Story, "")
				IF SET_CURRENT_EYEFIND_NEWS_STORY(sEyeFindCurrentNewStory.tl23temp_NewsHeadline, sEyeFindCurrentNewStory.tl23temp_Story, sEyeFindCurrentNewStory.iNewsOrganisation)
					CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): set initial news story")	
					bNewStoryAvailable = TRUE
				ENDIF
			ELSE
				// don't allow story update if override is set
				IF bUpdateEyeFindNewsStory
					// Don't update the story if we clicked on Read More News button in Eye Find (this expands the current story)
					IF g_iWebPageIndexFeedback = 5
						CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): skip update current news story - accessing Read More News ")
						bNewStoryAvailable = TRUE
					ELSE
						IF SET_CURRENT_EYEFIND_NEWS_STORY(sEyeFindCurrentNewStory.tl23temp_NewsHeadline, sEyeFindCurrentNewStory.tl23temp_Story, sEyeFindCurrentNewStory.iNewsOrganisation)
							CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): updated current news story")	
							bNewStoryAvailable = TRUE
						ENDIF
					ENDIF
				ELSE
					bNewStoryAvailable = TRUE	// ensure the current story displays
					CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): skip update current news story - for bUpdateEyeFindNewsStory")
				ENDIF
			ENDIF
			
			// B*1582586 - check that the lang has text supported
			IF bIsInternetTextSupported
				IF bNewStoryAvailable
					CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): set display new story : headline : ", sEyeFindCurrentNewStory.tl23temp_NewsHeadline, " story = ", sEyeFindCurrentNewStory.tl23temp_Story, " org = ", sEyeFindCurrentNewStory.iNewsOrganisation)	
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sEyeFindCurrentNewStory.tl23temp_NewsHeadline)
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sEyeFindCurrentNewStory.iNewsOrganisation)
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sEyeFindCurrentNewStory.tl23temp_Story)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ELSE
				CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): skip current news story - for bIsInternetTextSupported")
			ENDIF
			
			//7	String	Bleet 1 - Use BLEETER_GET_MOST_RECENT_TWO_BLEETS() - see bleets below.
			ltbs = BLEETER_GET_MOST_RECENT_TWO_BLEETS(sRepeatInfo) 
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(ltbs.sBleet1)
			END_SCALEFORM_MOVIE_METHOD()
			//8	String	Bleet 2 - Use BLEETER_GET_MOST_RECENT_TWO_BLEETS() - see bleets below.
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(ltbs.sBleet2)
			END_SCALEFORM_MOVIE_METHOD()
			
			//9	Array	Displays three large adverts with a call to action for each on the homepage. See website offers below.
			IF g_bInMultiplayer
				IF g_smpTunables.bTurn_on_promotional_eyefind_homepage
					CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): display vehicle discounts [Legendary:", g_sMPTunables.fLEGENDARY_DISPLAYED_DISCOUNT, "%, Elitas:", g_sMPTunables.fELITAS_DISPLAYED_DISCOUNT, "%, SSASA:", g_sMPTunables.fSSASA_DISPLAYED_DISCOUNT, "%]")
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
						SCALEFORM_MOVIE_METHOD_ADD_EYEFIND_DISCOUNT(WWW_LEGENDARYMOTORSPORT_NET, g_sMPTunables.fLEGENDARY_DISPLAYED_DISCOUNT)
						SCALEFORM_MOVIE_METHOD_ADD_EYEFIND_DISCOUNT(WWW_ELITASTRAVEL_COM, g_sMPTunables.fELITAS_DISPLAYED_DISCOUNT)
						SCALEFORM_MOVIE_METHOD_ADD_EYEFIND_DISCOUNT(WWW_SOUTHERNSANANDREASSUPERAUTOS_COM, g_sMPTunables.fSSASA_DISPLAYED_DISCOUNT)
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): ignore vehicle discounts - 'bTurn_on_promotional_eyefind_homepage' is false")
					
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_LARGE_ADVERT_INDEX_0)	// SCALEFORM_MOVIE_METHOD_ADD_PARAM_WEBSITE_INDEX(WWW_ELITASTRAVEL_COM, TRUE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_SA")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_LARGE_ADVERT_INDEX_1)	// SCALEFORM_MOVIE_METHOD_ADD_PARAM_WEBSITE_INDEX(WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM, TRUE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_SA")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_LARGE_ADVERT_INDEX_2)	// SCALEFORM_MOVIE_METHOD_ADD_PARAM_WEBSITE_INDEX(FORECLOSURES_MAZE_D_BANK_COM, FALSE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EYEDISC_SA")
						//
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_3)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_4)
						//
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_5)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_6)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_7)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_8)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_smpTunables.iSF_EYEFIND_MP_SMALL_ADVERT_INDEX_9)
						
						
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ELSE
				CPRINTLN(DEBUG_INTERNET, " SF_EYEFIND(): skip vehicle discounts - not in multiplayer")
			ENDIF
			
		BREAK
		
		DEFAULT
			CERRORLN(DEBUG_INTERNET, " SF_EYEFIND(pageID: ", pageID, ") - unknown iPage??")	
		BREAK
	ENDSWITCH
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SF_EPSILON(SCALEFORM_INDEX pagemov, int pageID)///http://rsgediwiki1/wiki/index.php/WWW_EPSILONPROGRAM_COM
	
	// Do proxy settings
	SWITCH pageID
		
		CASE 1			
			
			// B*1553835 - This needs to be triggered here so the disable button works correctly
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "UPDATE_TEXT")
			END_SCALEFORM_MOVIE_METHOD()
			
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_5000)
				CPRINTLN(DEBUG_INTERNET, "EPSILON: Enabling 'Welcome to The Epsilon Program'")
				BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "DISABLE_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		BREAK
		
		// Check for Epsilon questionaire end page
		CASE 12
			CPRINTLN(DEBUG_INTERNET, "EPSILON: Questionnaire completed!")
			//Set epsilon step stat
			INT iCurrent
			STAT_GET_INT(NUM_EPSILON_STEP,iCurrent)
			IF iCurrent < 1
				STAT_SET_INT(NUM_EPSILON_STEP,1)
				SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH20),1)
			ENDIF
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE, TRUE)
		BREAK
	
		CASE 14
			
			// This is updated in the main browser script
			IF g_iEpsilonTutorialStage = -1
				g_iEpsilonTutorialStage = 1
				CPRINTLN(DEBUG_INTERNET, "EPSILON: Presentation started!")
			ENDIF
		BREAK
	
		CASE 15
			SCRIPT_ASSERT("The buttons on this debug page are not bound and are deprecated pending removal. Please go to the main page and scroll down to find the new links to the store and donation pages")
		BREAK
		
		CASE 16
		CASE 22

			SWITCH g_eEpsilonFailReason
			
				// UNSAVEABLE! THIS CONTENT IS NOT AVAILABLE TO YOU! YOUR TRANSGRESSION HAS BEEN REPORTED TO THE AUTHORITIES!
				CASE EPSILON_IP_INFRINGE
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EPSI_IP_FRIN")
					END_SCALEFORM_MOVIE_METHOD()
				BREAK
			
				// Sadly we have no robes in stock at this time. Please try again later.
				CASE EPSILON_OUT_OF_STOCK
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EPSI_NO_STOCK")
					END_SCALEFORM_MOVIE_METHOD()
				BREAK
				
				// Access denied! Your IP has been logged as that of an Unsaveable! 
				CASE EPSILON_SUPPRESSIVE
					BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EPSI_NO_MP_VISIT")
					END_SCALEFORM_MOVIE_METHOD()
				BREAK
			ENDSWITCH
		BREAK
	
		DEFAULT
			g_iEpsilonTutorialStage = -1
			// Unprime the presentation or questionairre if needed
		BREAK
	ENDSWITCH

	IF pageID <> 1
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "UPDATE_TEXT")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

FUNC STRING GETPROPERTY_TEXTURE_DICTIONARY_BY_CONST(INT iProp)

//	TEXT_LABEL_15 tlNew
//	tlNew = "DYN_MP_"
//	tlNew += iProp
//	STRING returnString
//	returnString = CONVERT_TEXT_LABEL_TO_STRING(tlNew)
//	CPRINTLN(DEBUG_INTERNET, "GETPROPERTY_TEXTURE_DICTIONARY_BY_CONST passed ", iProp, ", returning string: ", returnString)
//	RETURN returnString	

	SWITCH iProp
		CASE PROPERTY_HIGH_APT_1
			RETURN "DYN_MP_1"
			
		CASE PROPERTY_HIGH_APT_2
			RETURN "DYN_MP_2"	
			
		CASE PROPERTY_HIGH_APT_3
			RETURN "DYN_MP_3"	
			
		CASE PROPERTY_HIGH_APT_4
			RETURN "DYN_MP_4"	
			
		CASE PROPERTY_HIGH_APT_5
			RETURN "DYN_MP_5"
			
		CASE PROPERTY_HIGH_APT_6
			RETURN "DYN_MP_6"
			
		CASE PROPERTY_HIGH_APT_7
			RETURN "DYN_MP_7"
			
		CASE PROPERTY_MEDIUM_APT_1
			RETURN "DYN_MP_8"
			
		CASE PROPERTY_MEDIUM_APT_2
			RETURN "DYN_MP_9"
			
		CASE PROPERTY_MEDIUM_APT_3
			RETURN "DYN_MP_10"
			
		CASE PROPERTY_MEDIUM_APT_4
			RETURN "DYN_MP_11"
			
		CASE PROPERTY_MEDIUM_APT_5
			RETURN "DYN_MP_12"	
			
		CASE PROPERTY_MEDIUM_APT_6
			RETURN "DYN_MP_13"	
			
		CASE PROPERTY_MEDIUM_APT_7
			RETURN "DYN_MP_14"
			
		CASE PROPERTY_MEDIUM_APT_8
			RETURN "DYN_MP_15"	
			
		CASE PROPERTY_MEDIUM_APT_9
			RETURN "DYN_MP_16"	
			
		CASE PROPERTY_LOW_APT_1
			RETURN "DYN_MP_17"	
			
		CASE PROPERTY_LOW_APT_2
			RETURN "DYN_MP_18"
			
		CASE PROPERTY_LOW_APT_3
			RETURN "DYN_MP_19"	
			
		CASE PROPERTY_LOW_APT_4
			RETURN "DYN_MP_20"
			
		CASE PROPERTY_LOW_APT_5
			RETURN "DYN_MP_21"
			
		CASE PROPERTY_LOW_APT_6
			RETURN "DYN_MP_22"
			
		CASE PROPERTY_LOW_APT_7
			RETURN "DYN_MP_23"
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_1
			RETURN "DYN_MP_24"	
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_2
			RETURN "DYN_MP_25"	
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_3
			RETURN "DYN_MP_26"

		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_4
			RETURN "DYN_MP_27"
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_5
			RETURN "DYN_MP_28"
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_6
			RETURN "DYN_MP_29"
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_7
			RETURN "DYN_MP_30"	
			
		CASE PROPERTY_GARAGE_EAST_LOS_SANTOS_8
			RETURN "DYN_MP_31"
			
		CASE PROPERTY_GARAGE_SOUTH_LOS_SANTOS_1
			RETURN "DYN_MP_32"
			
		CASE PROPERTY_GARAGE_SOUTH_LOS_SANTOS_2
			RETURN "DYN_MP_33"
			
		CASE PROPERTY_HIGH_APT_8
			RETURN "DYN_MP_34"
			
		CASE PROPERTY_HIGH_APT_9
			RETURN "DYN_MP_35"
			
		CASE PROPERTY_HIGH_APT_10
			RETURN "DYN_MP_36"
			
		CASE PROPERTY_HIGH_APT_11
			RETURN "DYN_MP_37"
			
		CASE PROPERTY_HIGH_APT_12
			RETURN "DYN_MP_38"
			
		CASE PROPERTY_HIGH_APT_13	
			RETURN "DYN_MP_39"
			
		CASE PROPERTY_HIGH_APT_14
			RETURN "DYN_MP_40"
			
		CASE PROPERTY_HIGH_APT_15
			RETURN "DYN_MP_41"
			
		CASE PROPERTY_HIGH_APT_16
			RETURN "DYN_MP_42"
			
		CASE PROPERTY_HIGH_APT_17
			RETURN "DYN_MP_43"
			
		CASE PROPERTY_GARAGE_NEW_1
			RETURN "DYN_MP_44"
			
		CASE PROPERTY_GARAGE_NEW_2
			RETURN "DYN_MP_45"
			
		CASE PROPERTY_GARAGE_NEW_3
			RETURN "DYN_MP_46"
			
//		CASE PROPERTY_GARAGE_NEW_4
//			RETURN "DYN_MP_47"
			
		CASE PROPERTY_GARAGE_NEW_5
			RETURN "DYN_MP_48"
			
		CASE PROPERTY_GARAGE_NEW_6
			RETURN "DYN_MP_49"
			
		CASE PROPERTY_GARAGE_NEW_7
			RETURN "DYN_MP_50"
			
		CASE PROPERTY_GARAGE_NEW_8
			RETURN "DYN_MP_51"
			
		CASE PROPERTY_GARAGE_NEW_9
			RETURN "DYN_MP_52"
			
//		CASE PROPERTY_GARAGE_NEW_10
//			RETURN "DYN_MP_53"
//			
//		CASE PROPERTY_GARAGE_NEW_11
//			RETURN "DYN_MP_54"
//			
//		CASE PROPERTY_GARAGE_NEW_12
//			RETURN "DYN_MP_55"
//			
//		CASE PROPERTY_GARAGE_NEW_13
//			RETURN "DYN_MP_56"
			
		CASE PROPERTY_GARAGE_NEW_14
			RETURN "DYN_MP_57"
			
//		CASE PROPERTY_GARAGE_NEW_15
//			RETURN "DYN_MP_58"
			
		CASE PROPERTY_GARAGE_NEW_16
			RETURN "DYN_MP_59"
			
		CASE PROPERTY_GARAGE_NEW_17
			RETURN "DYN_MP_60"
			
		CASE PROPERTY_GARAGE_NEW_18
			RETURN "DYN_MP_61"
			
		CASE PROPERTY_GARAGE_NEW_19
			RETURN "DYN_MP_62"
			
		CASE PROPERTY_GARAGE_NEW_20
			RETURN "DYN_MP_63"
			
		CASE PROPERTY_GARAGE_NEW_21
			RETURN "DYN_MP_64"
			
		CASE PROPERTY_GARAGE_NEW_22
			RETURN "DYN_MP_65"
			
		CASE PROPERTY_GARAGE_NEW_23	
			RETURN "DYN_MP_66"
		
		CASE PROPERTY_BUS_HIGH_APT_1		//Eclipse Towers
			RETURN "DYN_MP_1"
		CASE PROPERTY_BUS_HIGH_APT_2		//Del Perro Heights
			RETURN "DYN_MP_7"
		CASE PROPERTY_BUS_HIGH_APT_3		//Richards Majestic
			RETURN "DYN_MP_40"
		CASE PROPERTY_BUS_HIGH_APT_4		//Tinsel Towers
			RETURN "DYN_MP_42"
		CASE PROPERTY_BUS_HIGH_APT_5		//4 Integrity Way
			RETURN "DYN_MP_38"
			
		CASE PROPERTY_IND_DAY_MEDIUM_1		
			RETURN "DYN_MP_72"
		CASE PROPERTY_IND_DAY_MEDIUM_2		
			RETURN "DYN_MP_73"
		CASE PROPERTY_IND_DAY_MEDIUM_3		
			RETURN "DYN_MP_74"
		CASE PROPERTY_IND_DAY_MEDIUM_4		
			RETURN "DYN_MP_75"
		CASE PROPERTY_IND_DAY_LOW_1		
			RETURN "DYN_MP_76"
		CASE PROPERTY_IND_DAY_LOW_2
			RETURN "DYN_MP_77"
		CASE PROPERTY_IND_DAY_LOW_3
			RETURN "DYN_MP_78"
		
		CASE PROPERTY_STILT_APT_1_BASE_B	RETURN "DYN_MP_80"
		CASE PROPERTY_STILT_APT_2_B			RETURN "DYN_MP_81"
		CASE PROPERTY_STILT_APT_3_B			RETURN "DYN_MP_82"
		CASE PROPERTY_STILT_APT_4_B			RETURN "DYN_MP_83"
		CASE PROPERTY_STILT_APT_5_BASE_A	RETURN "DYN_MP_84"
//		CASE PROPERTY_STILT_APT_6_A			RETURN ""
		CASE PROPERTY_STILT_APT_7_A			RETURN "DYN_MP_85"
		CASE PROPERTY_STILT_APT_8_A			RETURN "DYN_MP_86"
//		CASE PROPERTY_STILT_APT_9_A			RETURN ""
		CASE PROPERTY_STILT_APT_10_A		RETURN "DYN_MP_87"
//		CASE PROPERTY_STILT_APT_11_A		RETURN "DYN_MP_88"
		CASE PROPERTY_STILT_APT_12_A		RETURN "DYN_MP_89"
		CASE PROPERTY_STILT_APT_13_A		RETURN "DYN_MP_90"
		
		CASE PROPERTY_CUSTOM_APT_1_BASE		RETURN "DYN_MP_91"
		CASE PROPERTY_CUSTOM_APT_2			RETURN "DYN_MP_92"
		CASE PROPERTY_CUSTOM_APT_3			RETURN "DYN_MP_93"
//		CASE PROPERTY_CUSTOM_APT_4			RETURN ""
		CASE PROPERTY_YACHT_APT_1_BASE		RETURN ""
		
		CASE PROPERTY_OFFICE_1				RETURN "DYN_OFFICE_1"
		CASE PROPERTY_OFFICE_2_BASE			RETURN "DYN_OFFICE_2"
		CASE PROPERTY_OFFICE_3				RETURN "DYN_OFFICE_3"
		CASE PROPERTY_OFFICE_4				RETURN "DYN_OFFICE_4"
		
		CASE PROPERTY_CLUBHOUSE_1_BASE_A	RETURN "MP_CLUBH2"
		CASE PROPERTY_CLUBHOUSE_2_BASE_A	RETURN "MP_CLUBH1"
		CASE PROPERTY_CLUBHOUSE_3_BASE_A	RETURN "MP_CLUBH3"
		CASE PROPERTY_CLUBHOUSE_4_BASE_A	RETURN "MP_CLUBH4"
		CASE PROPERTY_CLUBHOUSE_5_BASE_A	RETURN "MP_CLUBH5"
		CASE PROPERTY_CLUBHOUSE_6_BASE_A	RETURN "MP_CLUBH6"
		CASE PROPERTY_CLUBHOUSE_7_BASE_B	RETURN "MP_CLUBH7"
		CASE PROPERTY_CLUBHOUSE_8_BASE_B	RETURN "MP_CLUBH8"
		CASE PROPERTY_CLUBHOUSE_9_BASE_B	RETURN "MP_CLUBH9"
		CASE PROPERTY_CLUBHOUSE_10_BASE_B	RETURN "MP_CLUBH11"
		CASE PROPERTY_CLUBHOUSE_11_BASE_B	RETURN "MP_CLUBH12"
		CASE PROPERTY_CLUBHOUSE_12_BASE_B	RETURN "MP_CLUBH10"
		
		#IF FEATURE_DLC_2_2022
		CASE PROPERTY_MULTISTOREY_GARAGE	RETURN "DYN_DLC_GARAGES"
		#ENDIF
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC POPULATE_MP_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	//constID
	//pagemov
	
	/*
	Call SET_DATA_SLOT with the following params for each pin:
    Param 1 - INT - this is the SlotID for your pin, starting from 0
    Param 2 - String - Name of property
    Param 3 - Float - X coord in world units
    Param 4 - Float - Y coord in world units
    Param 5 - INT - cost of propery (dollar sign and commas are added automatically)
    Param 6 - String - The TXD name for the property image texture (see #TXD LIST )
    Param 7 - String - The description of the property that is displayed on the details page 
    Param 8 - INT - The 'size type' of the property that can be used to sort the properties (2,6,10 garage, small, med, large apartment)
    Param 9 - INT - The 'location type' of the property that can be used to sort the properties (north LS, downtown LS, west LS, east LS, south LS and outskirts)
	*/
	
	TEXT_LABEL_31 tlSaleString = ""
	BOOL bSaleBanner = GET_SALE_INFORMATION_FOR_PROPERTY(constID, tlSaleString)
	BOOL bNewProperty = FALSE
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", \"", mpProperties[constID].tl_PropertyName,
			"\" $", GET_VALUE_OF_PROPERTY(constID),
			", texture:", GETPROPERTY_TEXTURE_DICTIONARY_BY_CONST(constID),
			", sale:", GET_STRING_FROM_BOOL(bSaleBanner),
			" \"", tlSaleString, "\"")
	
	
	g_iPropertyIDStoredInWebSlot[slot] = constID
	
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(mpProperties[constID].tl_PropertyName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(mpProperties[constID].vBlipLocation[0].x)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(mpProperties[constID].vBlipLocation[0].y)

		IF GET_VALUE_OF_PROPERTY(constID) < 1
		OR SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(constID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ITEM_FREE")
		ELIF bSaleBanner
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_ORIGINAL_VALUE(mpProperties[constID].iIndex))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(constID))*g_sMPTunables.fPropertyMultiplier))
		ENDIF

		//This is player name on purpose because _string checks the string table, can't find it and breaks it...
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GETPROPERTY_TEXTURE_DICTIONARY_BY_CONST(constID))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(mpProperties[constID].tl_PropertyDetails)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_PROPERTY_SIZE_TYPE(mpProperties[constID].iIndex))		//size type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_PROPERTY_LOCATION_TYPE(mpProperties[constID].iIndex))	//location type
		
		// 10	String	[OPTIONAL]  Text string for DLC pack identifiers (eg. "Part of the High Life Update").
		SWITCH mpProperties[constID].iIndex
//			CASE PROPERTY_BUS_HIGH_APT_1
//			CASE PROPERTY_BUS_HIGH_APT_2
//			CASE PROPERTY_BUS_HIGH_APT_3
//			CASE PROPERTY_BUS_HIGH_APT_4
//			CASE PROPERTY_BUS_HIGH_APT_5
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("WEB_BUSINESSPACK2") // Part of The High Life Update
//			BREAK
			
//			CASE PROPERTY_IND_DAY_LOW_1
//			CASE PROPERTY_IND_DAY_LOW_2
//			CASE PROPERTY_IND_DAY_LOW_3
//			CASE PROPERTY_IND_DAY_MEDIUM_1
//			CASE PROPERTY_IND_DAY_MEDIUM_2
//			CASE PROPERTY_IND_DAY_MEDIUM_3
//			CASE PROPERTY_IND_DAY_MEDIUM_4
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("WEB_INDEPENDENCEPACK") // Part of The High Life Update
//			BREAK
			
			CASE PROPERTY_BUS_HIGH_APT_1	//	[MP_PROP_67]	Eclipse Towers, Apt 3
			CASE PROPERTY_BUS_HIGH_APT_2	//	[MP_PROP_68]	Del Perro Heights, Apt 4
			CASE PROPERTY_BUS_HIGH_APT_3	//	[MP_PROP_69]	Richards Majestic, Apt 2
			CASE PROPERTY_BUS_HIGH_APT_4	//	[MP_PROP_70]	Tinsel Towers, Apt 42
			CASE PROPERTY_BUS_HIGH_APT_5	//	[MP_PROP_71]	4 Integrity Way, Apt 28
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DYN_UPDATEDINT") // UPDATED INTERIOR
			BREAK
		
			CASE PROPERTY_STILT_APT_1_BASE_B	
			CASE PROPERTY_STILT_APT_2_B			
			CASE PROPERTY_STILT_APT_3_B			
			CASE PROPERTY_STILT_APT_4_B			
			CASE PROPERTY_STILT_APT_5_BASE_A	
	//		CASE PROPERTY_STILT_APT_6_A			
			CASE PROPERTY_STILT_APT_7_A			
			CASE PROPERTY_STILT_APT_8_A			
	//		CASE PROPERTY_STILT_APT_9_A			
			CASE PROPERTY_STILT_APT_10_A		
//			CASE PROPERTY_STILT_APT_11_A		
			CASE PROPERTY_STILT_APT_12_A		
			CASE PROPERTY_STILT_APT_13_A
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DYN_STILTAPT") // STILT APARTMENT
				bNewProperty = TRUE
			BREAK
			CASE PROPERTY_CUSTOM_APT_1_BASE
			CASE PROPERTY_CUSTOM_APT_2
			CASE PROPERTY_CUSTOM_APT_3
	//		CASE PROPERTY_CUSTOM_APT_4
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DYN_CUSTOMAPT") // CUSTOM APARTMENT
				bNewProperty = TRUE
			BREAK
			#IF FEATURE_DLC_2_2022
			CASE PROPERTY_MULTISTOREY_GARAGE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DYN_CUSTOMGAR") // CUSTOM GARAGE
				bNewProperty = TRUE
			BREAK
			#ENDIF
			
			DEFAULT
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			BREAK
		ENDSWITCH
		
		// 11	Bool	[OPTIONAL] Shows a "sale" banner on the property listing.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSaleBanner)
		
		// 12	Int	Bit flags determining which custom interiors are available for the property. Use 0 for properties that do not have custom interiors.		There are up to 8 interiors so the expected values are in the range 0 - 255 ( = 8 bits) 
		INT iBit = 0
		IF GET_PROPERTY_CUSTOM_APARTMENT_BITFLAG(mpProperties[constID].iIndex, iBit)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBit)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ENDIF
		
		// 13	String	[OPTIONAL] Shows a red banner on the top of the property image. Use a text label to set the banner text, eg WEB_VEHICLE_CASH_BACK, WEB_VEHICLE_REBATE or WEB_VEHICLE_SALE. Use an empty string or do not specify this param at all to hide the banner.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlSaleString)
		
		//14	Int	[OPTIONAL] If Param 11 is TRUE, this int is formatted as a price and shown on the sale banner. It also shows a red strikethrough over the original property price. Omit this value or pass zero if it is not needed. On the details pages, the main price is crossed out and the sale price is shown next to it.
		IF bSaleBanner
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(constID))*g_sMPTunables.fPropertyMultiplier))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ENDIF
		
		//15	Bool	Determines whether this property appears in the "New" list of properties. By default, properties are always added to the new list unless this param is FALSE. This maintains backwards compatibility as the new list has always shown all properties.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bNewProperty)
		
		//16	Bool	If true, the Starter Pack icon/message is shown on the property listing.
		IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()				//Starter Pack
		AND COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(constID)
		AND SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(constID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		//17	Bool	If true, shows the tint buttons grid for the property.
		IF mpProperties[constID].iIndex = PROPERTY_MULTISTOREY_GARAGE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF
		#ENDIF
	END_SCALEFORM_MOVIE_METHOD()

	++slot
ENDPROC


FUNC INT GET_OFFICE_GARAGE_INTERIOR_VALUE(INT iInteriorIndex, INT iCurrentProperty)
	INT iGarageValue = 0
	INT iGarageInteriorValue = GET_MP_PROPERTY_INTERIOR_VALUE(iInteriorIndex, iCurrentProperty)
	
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF NOT IS_OFFICE_GARAGE_1_PURCHASED()
				iGarageValue = GET_MP_PROPERTY_VALUE(iCurrentProperty)
				IF iInteriorIndex = 0
					iGarageInteriorValue = 0
				ENDIF
			ELSE
				IF GET_OFFICE_GARAGE_INTERIOR_FROM_SF(iInteriorIndex) = GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1)
					iGarageInteriorValue = 0
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF NOT IS_OFFICE_GARAGE_2_PURCHASED()
				iGarageValue = GET_MP_PROPERTY_VALUE(iCurrentProperty)
				IF iInteriorIndex = 0
					iGarageInteriorValue = 0
				ENDIF
			ELSE
				IF GET_OFFICE_GARAGE_INTERIOR_FROM_SF(iInteriorIndex) = GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2)
					iGarageInteriorValue = 0
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF NOT IS_OFFICE_GARAGE_3_PURCHASED()
				iGarageValue = GET_MP_PROPERTY_VALUE(iCurrentProperty)
				IF iInteriorIndex = 0
					iGarageInteriorValue = 0
				ENDIF
			ELSE
				IF GET_OFFICE_GARAGE_INTERIOR_FROM_SF(iInteriorIndex) = GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3)
					iGarageInteriorValue = 0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN iGarageValue+iGarageInteriorValue
ENDFUNC
FUNC INT GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(INT iInteriorIndex, INT iCurrentProperty)
	INT iGarageValue = 0
	INT iGarageInteriorValue = GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(iInteriorIndex, iCurrentProperty)
	
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF NOT IS_OFFICE_GARAGE_1_PURCHASED()
				iGarageValue = GET_MP_PROPERTY_ORIGINAL_VALUE(iCurrentProperty)
				IF iInteriorIndex = 0
					iGarageInteriorValue = 0
				ENDIF
			ELSE
				IF GET_OFFICE_GARAGE_INTERIOR_FROM_SF(iInteriorIndex) = GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1)
					iGarageInteriorValue = 0
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF NOT IS_OFFICE_GARAGE_2_PURCHASED()
				iGarageValue = GET_MP_PROPERTY_ORIGINAL_VALUE(iCurrentProperty)
				IF iInteriorIndex = 0
					iGarageInteriorValue = 0
				ENDIF
			ELSE
				IF GET_OFFICE_GARAGE_INTERIOR_FROM_SF(iInteriorIndex) = GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2)
					iGarageInteriorValue = 0
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF NOT IS_OFFICE_GARAGE_3_PURCHASED()
				iGarageValue = GET_MP_PROPERTY_ORIGINAL_VALUE(iCurrentProperty)
				IF iInteriorIndex = 0
					iGarageInteriorValue = 0
				ENDIF
			ELSE
				IF GET_OFFICE_GARAGE_INTERIOR_FROM_SF(iInteriorIndex) = GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3)
					iGarageInteriorValue = 0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN iGarageValue+iGarageInteriorValue
ENDFUNC

FUNC INT GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(INT iCurrentProperty, OFFICE_INTERIOR_MOD_ENUM eMod, INT iVariable)
	INT iGarageModValue = GET_OFFICE_PROPERTY_MOD_VALUE(eMod, iVariable)
	
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF NOT IS_OFFICE_GARAGE_1_PURCHASED()
				IF iVariable = 0
					iGarageModValue = 0
				ENDIF
			ELSE
				IF (eMod = OFFICE_MOD_GARAGE1_LIGHTING)
					IF GET_OFFICE_GARAGE_LIGHTING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_LIGHTING)
						iGarageModValue = 0
					ENDIF
				ELIF (eMod = OFFICE_MOD_GARAGE1_NUMBERING)
					IF GET_OFFICE_GARAGE_NUMBERING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_NUMBERING)
						iGarageModValue = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF NOT IS_OFFICE_GARAGE_2_PURCHASED()
				IF iVariable = 0
					iGarageModValue = 0
				ENDIF
			ELSE
				IF (eMod = OFFICE_MOD_GARAGE2_LIGHTING)
					IF GET_OFFICE_GARAGE_LIGHTING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_LIGHTING)
						iGarageModValue = 0
					ENDIF
				ELIF (eMod = OFFICE_MOD_GARAGE2_NUMBERING)
					IF GET_OFFICE_GARAGE_NUMBERING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_NUMBERING)
						iGarageModValue = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF NOT IS_OFFICE_GARAGE_3_PURCHASED()
				IF iVariable = 0
					iGarageModValue = 0
				ENDIF
			ELSE
				IF (eMod = OFFICE_MOD_GARAGE3_LIGHTING)
					IF GET_OFFICE_GARAGE_LIGHTING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_LIGHTING)
						iGarageModValue = 0
					ENDIF
				ELIF (eMod = OFFICE_MOD_GARAGE3_NUMBERING)
					IF GET_OFFICE_GARAGE_NUMBERING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_NUMBERING)
						iGarageModValue = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN iGarageModValue
ENDFUNC
FUNC INT GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(INT iCurrentProperty, OFFICE_INTERIOR_MOD_ENUM eMod, INT iVariable)
	INT iGarageModValue = GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(eMod, iVariable)
	
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF NOT IS_OFFICE_GARAGE_1_PURCHASED()
				IF iVariable = 0
					iGarageModValue = 0
				ENDIF
			ELSE
				IF (eMod = OFFICE_MOD_GARAGE1_LIGHTING)
					IF GET_OFFICE_GARAGE_LIGHTING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_LIGHTING)
						iGarageModValue = 0
					ENDIF
				ELIF (eMod = OFFICE_MOD_GARAGE1_NUMBERING)
					IF GET_OFFICE_GARAGE_NUMBERING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_NUMBERING)
						iGarageModValue = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF NOT IS_OFFICE_GARAGE_2_PURCHASED()
				IF iVariable = 0
					iGarageModValue = 0
				ENDIF
			ELSE
				IF (eMod = OFFICE_MOD_GARAGE2_LIGHTING)
					IF GET_OFFICE_GARAGE_LIGHTING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_LIGHTING)
						iGarageModValue = 0
					ENDIF
				ELIF (eMod = OFFICE_MOD_GARAGE2_NUMBERING)
					IF GET_OFFICE_GARAGE_NUMBERING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_NUMBERING)
						iGarageModValue = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF NOT IS_OFFICE_GARAGE_3_PURCHASED()
				IF iVariable = 0
					iGarageModValue = 0
				ENDIF
			ELSE
				IF (eMod = OFFICE_MOD_GARAGE3_LIGHTING)
					IF GET_OFFICE_GARAGE_LIGHTING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_LIGHTING)
						iGarageModValue = 0
					ENDIF
				ELIF (eMod = OFFICE_MOD_GARAGE3_NUMBERING)
					IF GET_OFFICE_GARAGE_NUMBERING_FROM_SF(iVariable) = GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_NUMBERING)
						iGarageModValue = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN iGarageModValue
ENDFUNC

FUNC INT GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(INT iProperty, INT iModShopSF)
//	INT iModshop = GET_OFFICE_MODSHOP_INTERIOR_FROM_SF(iModShopSF-1)
	
	INT iOfficePropertyModShopPrice = 0
	INT iOfficeModShopPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_MODSHOP, iModshopSF)
	
	IF NOT IS_OFFICE_MODSHOP_PURCHASED()
		iOfficePropertyModShopPrice = GET_OFFICE_PROPERTY_MODSHOP_VALUE(iProperty)
		
		IF (iModshopSF = 1) //
			iOfficeModShopPrice = 0
		ELSE
			iOfficeModShopPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_MODSHOP, iModshopSF)
		ENDIF
	ELSE
		iOfficeModShopPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_MODSHOP, iModshopSF)
	ENDIF
	RETURN iOfficePropertyModShopPrice+iOfficeModShopPrice
ENDFUNC
FUNC INT GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(INT iProperty, INT iModShopSF)
//	INT iModshop = GET_OFFICE_MODSHOP_INTERIOR_FROM_SF(iModShopSF-1)
	
	INT iOfficePropertyModShopPrice = 0
	INT iOfficeModShopPrice = GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_MODSHOP, iModshopSF)
	
	IF NOT IS_OFFICE_MODSHOP_PURCHASED()
		iOfficePropertyModShopPrice = GET_OFFICE_PROPERTY_MODSHOP_ORIGINAL_VALUE(iProperty)
		
		IF (iModshopSF = 1) //
			iOfficeModShopPrice = 0
		ELSE
			iOfficeModShopPrice = GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_MODSHOP, iModshopSF)
		ENDIF
	ELSE
		iOfficeModShopPrice = GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_MODSHOP, iModshopSF)
	ENDIF
	RETURN iOfficePropertyModShopPrice+iOfficeModShopPrice
ENDFUNC

PROC POPULATE_MP_EXEC_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	INT iOwned = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0)
	INT iValue = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(constID))*g_sMPTunables.fPropertyMultiplier)
	
	BOOL bSaleBanner = IS_SALE_ACTIVE_FOR_PROPERTY(constID)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_EXEC_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", iOwned:", iOwned,
			", \"", mpProperties[constID].tl_PropertyName,
			"\" $", iValue,
			", pos:", mpProperties[constID].vBlipLocation[0],
			", sale:", GET_STRING_FROM_BOOL(bSaleBanner))
		
	INT iStyle_1_sale_cost = -1
	INT iStyle_2_sale_cost = -1
	INT iStyle_3_sale_cost = -1
	INT iStyle_4_sale_cost = -1
	INT iStyle_5_sale_cost = -1
	INT iStyle_6_sale_cost = -1
	INT iStyle_7_sale_cost = -1
	INT iStyle_8_sale_cost = -1
	INT iStyle_9_sale_cost = -1
	
	INT iFemale_PA_sale_cost = -1
	INT iMale_PA_sale_cost = -1
	INT iFont_sale_cost = -1
	INT iSign_sale_cost = -1
	INT iGun_Locker_sale_cost = -1
	INT iVault_sale_cost = -1
	INT iAccomadation_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(4, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(4, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(4, constID)
			iStyle_1_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(4, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(5, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(5, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(5, constID)
			iStyle_2_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(5, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(3, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(3, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(3, constID)
			iStyle_3_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(3, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(0, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(0, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(0, constID)
			iStyle_4_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(0, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(1, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(1, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(1, constID)
			iStyle_5_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(1, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(2, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(2, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(2, constID)
			iStyle_6_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(2, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(6, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(6, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(6, constID)
			iStyle_7_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(6, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(7, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(7, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(7, constID)
			iStyle_8_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(7, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(8, constID) != 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(8, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(8, constID)
			iStyle_9_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(8, constID)
		ENDIF
		
		IF iOwned != constID
			//no sale for free items
		ELSE
			IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_PERSONNEL, 0) != 0
			AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_PERSONNEL, 0) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_PERSONNEL, 0)
				iFemale_PA_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_PERSONNEL, 0)
			ENDIF
			IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_PERSONNEL, 1) != 0
			AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_PERSONNEL, 1) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_PERSONNEL, 1)
				iMale_PA_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_PERSONNEL, 1)
			ENDIF
			IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_FONT) != 0
			AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_FONT) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_FONT)
				iFont_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_FONT)
			ENDIF
			IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_NAME_ID) != 0
			AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_NAME_ID) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_NAME_ID)
				iSign_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_NAME_ID)
			ENDIF
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_LOCKER_GUN) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_LOCKER_GUN) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_GUN)
			iGun_Locker_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_GUN)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_LOCKER_CASH) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_LOCKER_CASH) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_CASH)
			iVault_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_CASH)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_ACCOMMODATION) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_ACCOMMODATION) > GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_ACCOMMODATION)
			iAccomadation_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_ACCOMMODATION)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(mpProperties[constID].tl_PropertyName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(mpProperties[constID].vBlipLocation[0].x)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(mpProperties[constID].vBlipLocation[0].y)

		//This is player name on purpose because _string checks the string table, can't find it and breaks it...
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GETPROPERTY_TEXTURE_DICTIONARY_BY_CONST(constID))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(mpProperties[constID].tl_PropertyDetails)
		
		IF bSaleBanner
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_ORIGINAL_VALUE(mpProperties[constID].iIndex))
		ELIF GET_VALUE_OF_PROPERTY(constID) < 1
		OR SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(constID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 	//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ITEM_FREE")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iValue)
		ENDIF
		
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iStyle_1_sale_cost = -1			//Style 1: "Executive: Rich"
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(4, constID))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(4, constID))
			ENDIF
		ENDIF
		IF iStyle_2_sale_cost = -1			//Style 2: "Executive: Cool"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(5, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(5, constID))
		ENDIF
		IF iStyle_3_sale_cost = -1			//Style 3: "Executive: Contrast"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(3, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(3, constID))
		ENDIF
		IF iStyle_4_sale_cost = -1			//Style 4: "Old Spice: Warm"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(0, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(0, constID))
		ENDIF
		IF iStyle_5_sale_cost = -1			//Style 5: "Old Spice: Classical"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(1, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(1, constID))
		ENDIF
		IF iStyle_6_sale_cost = -1			//Style 6: "Old Spice: Vintage"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(2, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(2, constID))
		ENDIF
		IF iStyle_7_sale_cost = -1			//Style 7: "Power Broker: Ice"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(6, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(6, constID))
		ENDIF
		IF iStyle_8_sale_cost = -1			//Style 8: "Power Broker: Conservative"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(7, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(7, constID))
		ENDIF
		IF iStyle_9_sale_cost = -1			//Style 9: "Power Broker: Polished"
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(8, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(8, constID))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)															//Female PA	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)															//Male PA
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)															//Font cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)															//Signage cost
		ELSE
			IF iFemale_PA_sale_cost = -1	//Female PA
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_PERSONNEL, 0))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_PERSONNEL, 0))
			ENDIF
			IF iMale_PA_sale_cost = -1		//Male PA
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_PERSONNEL, 1))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_PERSONNEL, 1))
			ENDIF
			IF iFont_sale_cost = -1			//Font
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_FONT))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_FONT))
			ENDIF
			IF iSign_sale_cost = -1			//Sign
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_NAME_ID))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_NAME_ID))
			ENDIF
		ENDIF
		IF iGun_Locker_sale_cost = -1		//Gun Locker
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_GUN))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_LOCKER_GUN))
		ENDIF
		IF iVault_sale_cost = -1			//Vault
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_CASH))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_LOCKER_CASH))
		ENDIF
		IF iAccomadation_sale_cost = -1		//Accomadation
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_ACCOMMODATION))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE(OFFICE_MOD_ACCOMMODATION))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF bSaleBanner
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iValue)			//Base Sale Cost
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)				//Base Sale Cost
		ENDIF
		
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_1_sale_cost)	//Style 1 sale cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_2_sale_cost)	//Style 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_3_sale_cost)	//Style 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_4_sale_cost)	//Style 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_5_sale_cost)	//Style 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_sale_cost)	//Style 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_sale_cost)	//Style 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_sale_cost)	//Style 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_9_sale_cost)	//Style 9 sale cost		
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFemale_PA_sale_cost)		//Female PA Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMale_PA_sale_cost)		//Male PA Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFont_sale_cost)			//Font Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSign_sale_cost)			//Sign Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGun_Locker_sale_cost)		//Gun Locker Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVault_sale_cost)			//Vault Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAccomadation_sale_cost)	//Accomadation sale cost
	END_SCALEFORM_MOVIE_METHOD()

	INT iGarage_base_sale_price = -1
	INT iGarage_1_Style_0_sale_cost = -1
	INT iGarage_1_Style_1_sale_cost = -1
	INT iGarage_1_Style_2_sale_cost = -1
	INT iGarage_1_Style_3_sale_cost = -1
	INT iGarage_1_Lighting_0_sale_cost = -1
	INT iGarage_1_Lighting_1_sale_cost = -1
	INT iGarage_1_Lighting_2_sale_cost = -1
	INT iGarage_1_Lighting_3_sale_cost = -1
	INT iGarage_1_Lighting_4_sale_cost = -1
	INT iGarage_1_Lighting_5_sale_cost = -1
	INT iGarage_1_Lighting_6_sale_cost = -1
	INT iGarage_1_Lighting_7_sale_cost = -1
	INT iGarage_1_Lighting_8_sale_cost = -1
	INT iGarage_1_Number_0_sale_cost = -1
	INT iGarage_1_Number_1_sale_cost = -1
	INT iGarage_1_Number_2_sale_cost = -1
	INT iGarage_1_Number_3_sale_cost = -1
	INT iGarage_1_Number_4_sale_cost = -1
	INT iGarage_1_Number_5_sale_cost = -1
	INT iGarage_1_Number_6_sale_cost = -1
	INT iGarage_1_Number_7_sale_cost = -1
	INT iGarage_1_Number_8_sale_cost = -1
	
	INT iGarage_2_Style_0_sale_cost = -1
	INT iGarage_2_Style_1_sale_cost = -1
	INT iGarage_2_Style_2_sale_cost = -1
	INT iGarage_2_Style_3_sale_cost = -1
	INT iGarage_2_Lighting_0_sale_cost = -1
	INT iGarage_2_Lighting_1_sale_cost = -1
	INT iGarage_2_Lighting_2_sale_cost = -1
	INT iGarage_2_Lighting_3_sale_cost = -1
	INT iGarage_2_Lighting_4_sale_cost = -1
	INT iGarage_2_Lighting_5_sale_cost = -1
	INT iGarage_2_Lighting_6_sale_cost = -1
	INT iGarage_2_Lighting_7_sale_cost = -1
	INT iGarage_2_Lighting_8_sale_cost = -1
	INT iGarage_2_Number_0_sale_cost = -1
	INT iGarage_2_Number_1_sale_cost = -1
	INT iGarage_2_Number_2_sale_cost = -1
	INT iGarage_2_Number_3_sale_cost = -1
	INT iGarage_2_Number_4_sale_cost = -1
	INT iGarage_2_Number_5_sale_cost = -1
	INT iGarage_2_Number_6_sale_cost = -1
	INT iGarage_2_Number_7_sale_cost = -1
	INT iGarage_2_Number_8_sale_cost = -1
	
	INT iGarage_3_Style_0_sale_cost = -1
	INT iGarage_3_Style_1_sale_cost = -1
	INT iGarage_3_Style_2_sale_cost = -1
	INT iGarage_3_Style_3_sale_cost = -1
	INT iGarage_3_Lighting_0_sale_cost = -1
	INT iGarage_3_Lighting_1_sale_cost = -1
	INT iGarage_3_Lighting_2_sale_cost = -1
	INT iGarage_3_Lighting_3_sale_cost = -1
	INT iGarage_3_Lighting_4_sale_cost = -1
	INT iGarage_3_Lighting_5_sale_cost = -1
	INT iGarage_3_Lighting_6_sale_cost = -1
	INT iGarage_3_Lighting_7_sale_cost = -1
	INT iGarage_3_Lighting_8_sale_cost = -1
	INT iGarage_3_Number_0_sale_cost = -1
	INT iGarage_3_Number_1_sale_cost = -1
	INT iGarage_3_Number_2_sale_cost = -1
	INT iGarage_3_Number_3_sale_cost = -1
	INT iGarage_3_Number_4_sale_cost = -1
	INT iGarage_3_Number_5_sale_cost = -1
	INT iGarage_3_Number_6_sale_cost = -1
	INT iGarage_3_Number_7_sale_cost = -1
	INT iGarage_3_Number_8_sale_cost = -1
	
	INT iMod_Shop_Option_0_sale_cost = -1
	INT iMod_Shop_Option_1_sale_cost = -1
	INT iMod_Shop_Option_2_sale_cost = -1
	INT iMod_Shop_Option_3_sale_cost = -1
	INT iMod_Shop_Option_4_sale_cost = -1
	INT iMod_Shop_Option_5_sale_cost = -1
	INT iMod_Shop_Option_6_sale_cost = -1
	INT iMod_Shop_Option_7_sale_cost = -1
	INT iMod_Shop_Option_8_sale_cost = -1
	INT iMod_Shop_Option_9_sale_cost = -1
	INT iMod_Shop_Option_10_sale_cost = -1
	INT iMod_Shop_Option_11_sale_cost = -1
	INT iMod_Shop_Option_12_sale_cost = -1
	INT iMod_Shop_Option_13_sale_cost = -1
	INT iMod_Shop_Option_14_sale_cost = -1
	INT iMod_Shop_Option_15_sale_cost = -1
	INT iMod_Shop_Option_16_sale_cost = -1
	INT iMod_Shop_Option_17_sale_cost = -1
	INT iMod_Shop_Option_18_sale_cost = -1
	INT iMod_Shop_Option_19_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF NOT IS_OFFICE_GARAGE_1_PURCHASED()
			IF GET_OFFICE_PROPERTY_BASE_GARAGE_ORIGINAL_VALUE(constID) != 0
			AND GET_OFFICE_PROPERTY_BASE_GARAGE_ORIGINAL_VALUE(constID) > GET_OFFICE_PROPERTY_BASE_GARAGE_VALUE(constID)
				iGarage_base_sale_price = GET_OFFICE_PROPERTY_BASE_GARAGE_VALUE(constID)
			ENDIF
		ENDIF
		
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL1) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL1) > GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL1)
			iGarage_1_Style_0_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL1)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL1) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL1) > GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL1)
			iGarage_1_Style_1_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL1)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL1) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL1) > GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL1)
			iGarage_1_Style_2_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL1)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL1) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL1) > GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL1)
			iGarage_1_Style_3_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL1)
		ENDIF

		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 0) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 0) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 0)
			iGarage_1_Lighting_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 0)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 1)
			iGarage_1_Lighting_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 2)
			iGarage_1_Lighting_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 3)
			iGarage_1_Lighting_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 4)
			iGarage_1_Lighting_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 5)
			iGarage_1_Lighting_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 6)
			iGarage_1_Lighting_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 7)
			iGarage_1_Lighting_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 8)
			iGarage_1_Lighting_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 8)
		ENDIF

		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 0) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 0) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 0)
			iGarage_1_Number_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 0)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 1)
			iGarage_1_Number_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 2)
			iGarage_1_Number_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 3)
			iGarage_1_Number_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 4)
			iGarage_1_Number_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 5)
			iGarage_1_Number_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 6)
			iGarage_1_Number_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 7)
			iGarage_1_Number_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 8)
			iGarage_1_Number_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 8)
		ENDIF
		
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL2) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL2) > GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL2)
			iGarage_2_Style_0_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL2)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL2) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL2) > GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL2)
			iGarage_2_Style_1_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL2)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL2) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL2) > GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL2)
			iGarage_2_Style_2_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL2)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL2) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL2) > GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL2)
			iGarage_2_Style_3_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL2)
		ENDIF
		
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 0) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 0) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 0)
			iGarage_2_Lighting_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 0)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 1)
			iGarage_2_Lighting_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 2)
			iGarage_2_Lighting_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 3)
			iGarage_2_Lighting_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 4)
			iGarage_2_Lighting_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 5)
			iGarage_2_Lighting_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 6)
			iGarage_2_Lighting_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 7)
			iGarage_2_Lighting_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 8)
			iGarage_2_Lighting_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 8)
		ENDIF
		
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 0) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 0) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 0)
			iGarage_2_Number_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 0)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 1)
			iGarage_2_Number_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 2)
			iGarage_2_Number_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 3)
			iGarage_2_Number_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 4)
			iGarage_2_Number_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 5)
			iGarage_2_Number_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 6)
			iGarage_2_Number_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 7)
			iGarage_2_Number_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 8)
			iGarage_2_Number_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 8)
		ENDIF
		
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL3) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL3) > GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL3)
			iGarage_3_Style_0_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL3)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL3) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL3) > GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL3)
			iGarage_3_Style_1_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL3)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL3) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL3) > GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL3)
			iGarage_3_Style_2_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL3)
		ENDIF
		IF GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL3) != 0
		AND GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL3) > GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL3)
			iGarage_3_Style_3_sale_cost = GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL3)
		ENDIF
		
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 0) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 0) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 0)
			iGarage_3_Lighting_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 0)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 1)
			iGarage_3_Lighting_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 2)
			iGarage_3_Lighting_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 3)
			iGarage_3_Lighting_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 4)
			iGarage_3_Lighting_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 5)
			iGarage_3_Lighting_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 6)
			iGarage_3_Lighting_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 7)
			iGarage_3_Lighting_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 8)
			iGarage_3_Lighting_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 8)
		ENDIF
		
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 0) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 0) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 0)
			iGarage_3_Number_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 0)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 1)
			iGarage_3_Number_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 2)
			iGarage_3_Number_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 3)
			iGarage_3_Number_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 4)
			iGarage_3_Number_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 5)
			iGarage_3_Number_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 6)
			iGarage_3_Number_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 7)
			iGarage_3_Number_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 8)
			iGarage_3_Number_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 8)
		ENDIF
		
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 1) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 1) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 1)
			iMod_Shop_Option_0_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 1)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 2) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 2) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 2)
			iMod_Shop_Option_1_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 2)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 3) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 3) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 3)
			iMod_Shop_Option_2_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 3)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 4) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 4) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 4)
			iMod_Shop_Option_3_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 4)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 5) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 5) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 5)
			iMod_Shop_Option_4_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 5)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 6) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 6) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 6)
			iMod_Shop_Option_5_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 6)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 7) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 7) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 7)
			iMod_Shop_Option_6_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 7)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 8) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 8) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 8)
			iMod_Shop_Option_7_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 8)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 9) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 9) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 9)
			iMod_Shop_Option_8_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 9)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 10) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 10) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 10)
			iMod_Shop_Option_9_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 10)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 11) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 11) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 11)
			iMod_Shop_Option_10_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 11)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 12) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 12) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 12)
			iMod_Shop_Option_11_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 12)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 13) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 13) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 13)
			iMod_Shop_Option_12_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 13)
		ENDIF																							
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 14) != 0			
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 14) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 14)
			iMod_Shop_Option_13_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 14)
		ENDIF																							
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 15) != 0			
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 15) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 15)
			iMod_Shop_Option_14_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 15)
		ENDIF																							
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 16) != 0			
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 16) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 16)
			iMod_Shop_Option_15_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 16)
		ENDIF																							
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 17) != 0			
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 17) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 17)
			iMod_Shop_Option_16_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 17)
		ENDIF																							
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 18) != 0			
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 18) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 18)
			iMod_Shop_Option_17_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 18)
		ENDIF																							
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 19) != 0			
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 19) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 19)
			iMod_Shop_Option_18_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 19)
		ENDIF
		IF GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 20) != 0
		AND GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 20) > GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 20)
			iMod_Shop_Option_19_sale_cost = GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 20)
		ENDIF
	ENDIF 
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_PROPERTY_GARAGE_DESCRIPTION(constID))				//Garage description
		
		IF iGarage_base_sale_price = -1		//Garage base price
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_BASE_GARAGE_VALUE(constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_BASE_GARAGE_ORIGINAL_VALUE(constID))
		ENDIF
		
		IF iGarage_1_Style_0_sale_cost = -1		//Garage 1 Style 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ENDIF
		IF iGarage_1_Style_1_sale_cost = -1		//Garage 1 Style 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ENDIF
		IF iGarage_1_Style_2_sale_cost = -1		//Garage 1 Style 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ENDIF
		IF iGarage_1_Style_3_sale_cost = -1		//Garage 1 Style 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL1))
		ENDIF
		
		IF iGarage_1_Lighting_0_sale_cost = -1		//Garage 1 Lighting 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 0))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 0))
		ENDIF
		IF iGarage_1_Lighting_1_sale_cost = -1		//Garage 1 Lighting 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 1))
		ENDIF
		IF iGarage_1_Lighting_2_sale_cost = -1		//Garage 1 Lighting 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 2))
		ENDIF
		IF iGarage_1_Lighting_3_sale_cost = -1		//Garage 1 Lighting 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 3))
		ENDIF
		IF iGarage_1_Lighting_4_sale_cost = -1		//Garage 1 Lighting 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 4))
		ENDIF
		IF iGarage_1_Lighting_5_sale_cost = -1		//Garage 1 Lighting 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 5))
		ENDIF
		IF iGarage_1_Lighting_6_sale_cost = -1		//Garage 1 Lighting 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 6))
		ENDIF
		IF iGarage_1_Lighting_7_sale_cost = -1		//Garage 1 Lighting 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 7))
		ENDIF
		IF iGarage_1_Lighting_8_sale_cost = -1		//Garage 1 Lighting 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_LIGHTING, 8))
		ENDIF
		
		IF iGarage_1_Number_0_sale_cost = -1		//Garage 1 Number 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 0))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 0))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iGarage_1_Number_1_sale_cost = -1		//Garage 1 Number 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 1))
		ENDIF
		IF iGarage_1_Number_2_sale_cost = -1		//Garage 1 Number 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 2))
		ENDIF
		IF iGarage_1_Number_3_sale_cost = -1		//Garage 1 Number 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 3))
		ENDIF
		IF iGarage_1_Number_4_sale_cost = -1		//Garage 1 Number 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 4))
		ENDIF
		IF iGarage_1_Number_5_sale_cost = -1		//Garage 1 Number 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 5))
		ENDIF
		IF iGarage_1_Number_6_sale_cost = -1		//Garage 1 Number 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 6))
		ENDIF
		IF iGarage_1_Number_7_sale_cost = -1		//Garage 1 Number 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 7))
		ENDIF
		IF iGarage_1_Number_8_sale_cost = -1		//Garage 1 Number 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL1, OFFICE_MOD_GARAGE1_NUMBERING, 8))
		ENDIF
		
		IF iGarage_2_Style_0_sale_cost = -1		//Garage 2 Style 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ENDIF
		IF iGarage_2_Style_1_sale_cost = -1		//Garage 2 Style 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ENDIF
		IF iGarage_2_Style_2_sale_cost = -1		//Garage 2 Style 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ENDIF
		IF iGarage_2_Style_3_sale_cost = -1		//Garage 2 Style 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL2))
		ENDIF
		
		IF iGarage_2_Lighting_0_sale_cost = -1		//Garage 2 Lighting 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 0))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 0))
		ENDIF
		IF iGarage_2_Lighting_1_sale_cost = -1		//Garage 2 Lighting 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 1))
		ENDIF
		IF iGarage_2_Lighting_2_sale_cost = -1		//Garage 2 Lighting 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 2))
		ENDIF
		IF iGarage_2_Lighting_3_sale_cost = -1		//Garage 2 Lighting 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 3))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iGarage_2_Lighting_4_sale_cost = -1		//Garage 2 Lighting 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 4))
		ENDIF
		IF iGarage_2_Lighting_5_sale_cost = -1		//Garage 2 Lighting 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 5))
		ENDIF
		IF iGarage_2_Lighting_6_sale_cost = -1		//Garage 2 Lighting 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 6))
		ENDIF
		IF iGarage_2_Lighting_7_sale_cost = -1		//Garage 2 Lighting 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 7))
		ENDIF
		IF iGarage_2_Lighting_8_sale_cost = -1		//Garage 2 Lighting 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_LIGHTING, 8))
		ENDIF

		IF iGarage_2_Number_0_sale_cost = -1		//Garage 2 Number 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 0))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 0))
		ENDIF
		IF iGarage_2_Number_1_sale_cost = -1		//Garage 2 Number 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 1))
		ENDIF
		IF iGarage_2_Number_2_sale_cost = -1		//Garage 2 Number 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 2))
		ENDIF
		IF iGarage_2_Number_3_sale_cost = -1		//Garage 2 Number 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 3))
		ENDIF
		IF iGarage_2_Number_4_sale_cost = -1		//Garage 2 Number 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 4))
		ENDIF
		IF iGarage_2_Number_5_sale_cost = -1		//Garage 2 Number 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 5))
		ENDIF
		IF iGarage_2_Number_6_sale_cost = -1		//Garage 2 Number 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 6))
		ENDIF
		IF iGarage_2_Number_7_sale_cost = -1		//Garage 2 Number 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 7))
		ENDIF
		IF iGarage_2_Number_8_sale_cost = -1		//Garage 2 Number 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL2, OFFICE_MOD_GARAGE1_NUMBERING, 8))
		ENDIF
		
		IF iGarage_3_Style_0_sale_cost = -1		//Garage 3 Style 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(0, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ENDIF
		IF iGarage_3_Style_1_sale_cost = -1		//Garage 3 Style 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(1, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iGarage_3_Style_2_sale_cost = -1		//Garage 3 Style 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(2, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ENDIF
		IF iGarage_3_Style_3_sale_cost = -1		//Garage 3 Style 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_GARAGE_INTERIOR_ORIGINAL_VALUE(3, PROPERTY_OFFICE_1_GARAGE_LVL3))
		ENDIF
		
		IF iGarage_3_Lighting_0_sale_cost = -1		//Garage 3 Lighting 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 0))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 0))
		ENDIF
		IF iGarage_3_Lighting_1_sale_cost = -1		//Garage 3 Lighting 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 1))
		ENDIF
		IF iGarage_3_Lighting_2_sale_cost = -1		//Garage 3 Lighting 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 2))
		ENDIF
		IF iGarage_3_Lighting_3_sale_cost = -1		//Garage 3 Lighting 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 3))
		ENDIF
		IF iGarage_3_Lighting_4_sale_cost = -1		//Garage 3 Lighting 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 4))
		ENDIF
		IF iGarage_3_Lighting_5_sale_cost = -1		//Garage 3 Lighting 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 5))
		ENDIF
		IF iGarage_3_Lighting_6_sale_cost = -1		//Garage 3 Lighting 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 6))
		ENDIF
		IF iGarage_3_Lighting_7_sale_cost = -1		//Garage 3 Lighting 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 7))
		ENDIF
		IF iGarage_3_Lighting_8_sale_cost = -1		//Garage 3 Lighting 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_LIGHTING, 8))
		ENDIF

		IF iGarage_3_Number_0_sale_cost = -1		//Garage 3 Number 0 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 0))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 0))
		ENDIF
		IF iGarage_3_Number_1_sale_cost = -1		//Garage 3 Number 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 1))
		ENDIF
		IF iGarage_3_Number_2_sale_cost = -1		//Garage 3 Number 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 2))
		ENDIF
		IF iGarage_3_Number_3_sale_cost = -1		//Garage 3 Number 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 3))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iGarage_3_Number_4_sale_cost = -1		//Garage 3 Number 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 4))
		ENDIF
		IF iGarage_3_Number_5_sale_cost = -1		//Garage 3 Number 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 5))
		ENDIF
		IF iGarage_3_Number_6_sale_cost = -1		//Garage 3 Number 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 6))
		ENDIF
		IF iGarage_3_Number_7_sale_cost = -1		//Garage 3 Number 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 7))
		ENDIF
		IF iGarage_3_Number_8_sale_cost = -1		//Garage 3 Number 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_VALUE_OFFICE_MOD(PROPERTY_OFFICE_1_GARAGE_LVL3, OFFICE_MOD_GARAGE1_NUMBERING, 8))
		ENDIF
		
		IF iMod_Shop_Option_0_sale_cost = -1		//Mod Shop Option 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 1))
		ENDIF
		IF iMod_Shop_Option_1_sale_cost = -1		//Mod Shop Option 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 2))
		ENDIF
		IF iMod_Shop_Option_2_sale_cost = -1		//Mod Shop Option 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 3))
		ENDIF
		IF iMod_Shop_Option_3_sale_cost = -1		//Mod Shop Option 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 4))
		ENDIF
		IF iMod_Shop_Option_4_sale_cost = -1		//Mod Shop Option 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 5))
		ENDIF
		IF iMod_Shop_Option_5_sale_cost = -1		//Mod Shop Option 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 6))
		ENDIF
		IF iMod_Shop_Option_6_sale_cost = -1		//Mod Shop Option 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 7))
		ENDIF
		IF iMod_Shop_Option_7_sale_cost = -1		//Mod Shop Option 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 8))
		ENDIF
		IF iMod_Shop_Option_8_sale_cost = -1		//Mod Shop Option 9 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 9))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 9))
		ENDIF
		IF iMod_Shop_Option_9_sale_cost = -1		//Mod Shop Option 10 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 10))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 10))
		ENDIF
		IF iMod_Shop_Option_10_sale_cost = -1		//Mod Shop Option 11 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 11))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 11))
		ENDIF
		IF iMod_Shop_Option_11_sale_cost = -1		//Mod Shop Option 12 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 12))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 12))
		ENDIF
		IF iMod_Shop_Option_12_sale_cost = -1		//Mod Shop Option 13 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 13))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 13))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iMod_Shop_Option_13_sale_cost = -1		//Mod Shop Option 14 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 14))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 14))
		ENDIF
		IF iMod_Shop_Option_14_sale_cost = -1		//Mod Shop Option 15 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 15))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 15))
		ENDIF
		IF iMod_Shop_Option_15_sale_cost = -1		//Mod Shop Option 16 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 16))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 16))
		ENDIF
		IF iMod_Shop_Option_16_sale_cost = -1		//Mod Shop Option 17 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 17))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 17))
		ENDIF
		IF iMod_Shop_Option_17_sale_cost = -1		//Mod Shop Option 18 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 18))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 18))
		ENDIF
		IF iMod_Shop_Option_18_sale_cost = -1		//Mod Shop Option 19 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 19))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 19))
		ENDIF
		IF iMod_Shop_Option_19_sale_cost = -1		//Mod Shop Option 20 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_VALUE_OFFICE_MOD_MODSHOP(constID, 20))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OFFICE_PROPERTY_MOD_ORIGINAL_ORIGINAL_VALUE_OFFICE_MOD_MODSHOP(constID, 20))
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_base_sale_price)			//Garage base sale price
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Style_0_sale_cost)		//Garage 1 Style 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Style_1_sale_cost)		//Garage 1 Style 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Style_2_sale_cost)		//Garage 1 Style 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Style_3_sale_cost)		//Garage 1 Style 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_0_sale_cost)	//Garage Lighting 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_1_sale_cost)	//Garage Lighting 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_2_sale_cost)	//Garage Lighting 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_3_sale_cost)	//Garage Lighting 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_4_sale_cost)	//Garage Lighting 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_5_sale_cost)	//Garage Lighting 5 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_6_sale_cost)	//Garage Lighting 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_7_sale_cost)	//Garage Lighting 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Lighting_8_sale_cost)	//Garage Lighting 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_0_sale_cost)		//Garage Number 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_1_sale_cost)		//Garage Number 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_2_sale_cost)		//Garage Number 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_3_sale_cost)		//Garage Number 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_4_sale_cost)		//Garage Number 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_5_sale_cost)		//Garage Number 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_6_sale_cost)		//Garage Number 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_7_sale_cost)		//Garage Number 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_Number_8_sale_cost)		//Garage Number 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Style_0_sale_cost)		//Garage 2 Style 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Style_1_sale_cost)		//Garage 2 Style 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Style_2_sale_cost)		//Garage 2 Style 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Style_3_sale_cost)		//Garage 2 Style 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_0_sale_cost)	//Garage Lighting 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_1_sale_cost)	//Garage Lighting 1 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_2_sale_cost)	//Garage Lighting 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_3_sale_cost)	//Garage Lighting 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_4_sale_cost)	//Garage Lighting 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_5_sale_cost)	//Garage Lighting 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_6_sale_cost)	//Garage Lighting 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_7_sale_cost)	//Garage Lighting 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Lighting_8_sale_cost)	//Garage Lighting 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_0_sale_cost)		//Garage Number 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_1_sale_cost)		//Garage Number 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_2_sale_cost)		//Garage Number 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_3_sale_cost)		//Garage Number 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_4_sale_cost)		//Garage Number 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_5_sale_cost)		//Garage Number 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_6_sale_cost)		//Garage Number 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_7_sale_cost)		//Garage Number 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_Number_8_sale_cost)		//Garage Number 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Style_0_sale_cost)		//Garage 3 Style 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Style_1_sale_cost)		//Garage 3 Style 1 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Style_2_sale_cost)		//Garage 3 Style 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Style_3_sale_cost)		//Garage 3 Style 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_0_sale_cost)	//Garage Lighting 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_1_sale_cost)	//Garage Lighting 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_2_sale_cost)	//Garage Lighting 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_3_sale_cost)	//Garage Lighting 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_4_sale_cost)	//Garage Lighting 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_5_sale_cost)	//Garage Lighting 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_6_sale_cost)	//Garage Lighting 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_7_sale_cost)	//Garage Lighting 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Lighting_8_sale_cost)	//Garage Lighting 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_0_sale_cost)		//Garage Number 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_1_sale_cost)		//Garage Number 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_2_sale_cost)		//Garage Number 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_3_sale_cost)		//Garage Number 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_4_sale_cost)		//Garage Number 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_5_sale_cost)		//Garage Number 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_6_sale_cost)		//Garage Number 6 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_7_sale_cost)		//Garage Number 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_Number_8_sale_cost)		//Garage Number 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_0_sale_cost)	//Mod Shop Option 0 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_1_sale_cost)		//Mod Shop Option 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_2_sale_cost)		//Mod Shop Option 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_3_sale_cost)		//Mod Shop Option 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_4_sale_cost)		//Mod Shop Option 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_5_sale_cost)		//Mod Shop Option 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_6_sale_cost)		//Mod Shop Option 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_7_sale_cost)		//Mod Shop Option 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_8_sale_cost)		//Mod Shop Option 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_9_sale_cost)		//Mod Shop Option 9 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_10_sale_cost)		//Mod Shop Option 10 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_11_sale_cost)		//Mod Shop Option 11 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_12_sale_cost)		//Mod Shop Option 12 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_13_sale_cost)		//Mod Shop Option 13 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_14_sale_cost)		//Mod Shop Option 14 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_15_sale_cost)		//Mod Shop Option 15 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_16_sale_cost)		//Mod Shop Option 16 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_17_sale_cost)		//Mod Shop Option 17 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_18_sale_cost)		//Mod Shop Option 18 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMod_Shop_Option_19_sale_cost)		//Mod Shop Option 19 sale cost
		
		IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()				//Starter Pack
		AND COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(constID)
		AND SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(constID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

PROC POPULATE_MP_BIKER_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	INT iOwned = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE)
	INT iValue = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(constID))*g_sMPTunables.fPropertyMultiplier)
	
	VECTOR pos = mpProperties[constID].vBlipLocation[0]
	IF (constID = PROPERTY_CLUBHOUSE_4_BASE_A)		//Great Chaparral Clubhouse (101 Route 68)
		pos = <<55.4027, 2760.8459, 56.6234>>
	ENDIF
	
	BOOL bSaleBanner = IS_SALE_ACTIVE_FOR_PROPERTY(constID)
	
	IF GET_VALUE_OF_PROPERTY(constID) < 1
	OR SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(constID)
		iValue = 0
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_BIKER_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", iOwned:", iOwned,
			", \"", mpProperties[constID].tl_PropertyName,
			"\" $", iValue,
			", pos:", pos,
			", sale:", GET_STRING_FROM_BOOL(bSaleBanner))	
		
		
	INT iWall_1_sale_cost = -1
	INT iWall_2_sale_cost = -1
	INT iHanging_1_sale_cost = -1
	INT iHanging_2_sale_cost = -1
	INT iFurniture_1_sale_cost = -1
	INT iFurniture_2_sale_cost = -1
	
	INT iMural_1_sale_cost = -1
	INT iMural_2_sale_cost = -1
	INT iMural_3_sale_cost = -1
	INT iMural_4_sale_cost = -1
	INT iMural_5_sale_cost = -1
	INT iMural_6_sale_cost = -1
	INT iMural_7_sale_cost = -1
	INT iMural_8_sale_cost = -1
	INT iMural_9_sale_cost = -1
	
	INT iFont_sale_cost = -1
	INT iSign_sale_cost = -1
	
	INT iGeneric_Emblem_1_sale_cost = -1
	INT iGeneric_Emblem_2_sale_cost = -1
	INT iGeneric_Emblem_3_sale_cost = -1
	INT iGeneric_Emblem_4_sale_cost = -1
	INT iGeneric_Emblem_5_sale_cost = -1
	INT iGeneric_Emblem_6_sale_cost = -1
	INT iGeneric_Emblem_7_sale_cost = -1
	INT iGeneric_Emblem_8_sale_cost = -1
	INT iGeneric_Emblem_9_sale_cost = -1
	INT iCrew_Emblem_sale_cost = -1
	
	INT iGun_Locker_sale_cost = -1
	INT iGarage_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_WALL, 0) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_WALL, 0) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_WALL, 0)
			iWall_1_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_WALL, 0)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_WALL, 1) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_WALL, 1) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_WALL, 1)
			iWall_2_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_WALL, 1)
		ENDIF
		
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_HANGING, 0) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_HANGING, 0) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_HANGING, 0)
			iHanging_1_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_HANGING, 0)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_HANGING, 1) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_HANGING, 1) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_HANGING, 1)
			iHanging_2_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_HANGING, 1)
		ENDIF
		
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FURNITURE, 0) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FURNITURE, 0) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FURNITURE, 0)
			iFurniture_1_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FURNITURE, 0)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FURNITURE, 1) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FURNITURE, 1) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FURNITURE, 1)
			iFurniture_2_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FURNITURE, 1)
		ENDIF
		
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(0, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(0, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(0, constID)
			iMural_1_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(0, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(1, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(1, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(1, constID)
			iMural_2_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(1, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(2, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(2, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(2, constID)
			iMural_3_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(2, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(3, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(3, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(3, constID)
			iMural_4_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(3, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(4, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(4, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(4, constID)
			iMural_5_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(4, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(5, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(5, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(5, constID)
			iMural_6_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(5, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(6, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(6, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(6, constID)
			iMural_7_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(6, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(7, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(7, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(7, constID)
			iMural_8_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(7, constID)
		ENDIF
		IF GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(8, constID) >= 0
		AND GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(8, constID) > GET_MP_PROPERTY_INTERIOR_VALUE(8, constID)
			iMural_9_sale_cost = GET_MP_PROPERTY_INTERIOR_VALUE(8, constID)
		ENDIF
		
		IF iOwned != constID
			//no sale for free items
		ELSE
			IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FONT) >= 0
			AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FONT) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FONT)
				iFont_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FONT)
			ENDIF
			IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_NAME_ID) >= 0
			AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_NAME_ID) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_NAME_ID, 1)
				iSign_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_NAME_ID, 1)
			ENDIF
		ENDIF
		
		IF iOwned != constID
		AND iCONST_DEFAULT_FREE_CLUBHOUSE_EMBLEM = 1
			//no sale for free items
		ELSE
			IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 1) >= 0
			AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 1) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 1)
				iGeneric_Emblem_1_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 1)
			ENDIF
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 2) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 2) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 2)
			iGeneric_Emblem_2_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 2)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 3) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 3) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 3)
			iGeneric_Emblem_3_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 3)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 4) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 4) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 4)
			iGeneric_Emblem_4_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 4)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 5) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 5) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 5)
			iGeneric_Emblem_5_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 5)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 6) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 6) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 6)
			iGeneric_Emblem_6_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 6)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 7) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 7) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 7)
			iGeneric_Emblem_7_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 7)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 8) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 8) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 8)
			iGeneric_Emblem_8_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 8)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 9) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 9) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 9)
			iGeneric_Emblem_9_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 9)
		ENDIF
		
		IF iOwned != constID
		AND iCONST_DEFAULT_FREE_CLUBHOUSE_EMBLEM = 0
			//no sale for free items
		ELSE
			IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 0) >= 0
			AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 0) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 0)
				iCrew_Emblem_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 0)
			ENDIF
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_LOCKER_GUN) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_LOCKER_GUN) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_LOCKER_GUN, 1)
			iGun_Locker_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_LOCKER_GUN, 1)
		ENDIF
		IF GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_GARAGE) >= 0
		AND GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_GARAGE) > GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_GARAGE, 1)
			iGarage_sale_cost = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_GARAGE, 1)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID)													//ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(mpProperties[constID].tl_PropertyName)					//Name
		IF GET_BASE_PROPERTY_FROM_PROPERTY(constID) = PROPERTY_CLUBHOUSE_1_BASE_A
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)														//Type 0: Traditional style clubhouse
		ELIF GET_BASE_PROPERTY_FROM_PROPERTY(constID) = PROPERTY_CLUBHOUSE_7_BASE_B
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)														//Type 1: Urban style clubhouse 
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)													//Type
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)				//X position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)				//Y position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GETPROPERTY_TEXTURE_DICTIONARY_BY_CONST(constID))	//Image ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(mpProperties[constID].tl_PropertyDetails)				//Description
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_PROPERTY_ADDRESS(constID))							//Address
		IF bSaleBanner
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_ORIGINAL_VALUE(mpProperties[constID].iIndex))
		ELIF (iValue = 0)												//Base cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 	//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ITEM_FREE")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iValue)
		ENDIF
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iWall_1_sale_cost = -1		//Wall 1 cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_WALL, 0))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_WALL, 0))
			ENDIF
		ENDIF
		IF iWall_2_sale_cost = -1			//Wall 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_WALL, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_WALL, 1))
		ENDIF
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iHanging_1_sale_cost = -1		//Hanging 1 cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_HANGING, 0))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_HANGING, 0))
			ENDIF
		ENDIF
		IF iHanging_2_sale_cost = -1			//Hanging 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_HANGING, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_HANGING, 1))
		ENDIF
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iFurniture_1_sale_cost = -1		//Furniture 1 cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FURNITURE, 0))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FURNITURE, 0))
			ENDIF
		ENDIF
		IF iFurniture_2_sale_cost = -1			//Furniture 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FURNITURE, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FURNITURE, 1))
		ENDIF
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iMural_1_sale_cost = -1		//Mural 1: 
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(0, constID))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(0, constID))
			ENDIF
		ENDIF
		IF iMural_2_sale_cost = -1			//Mural 2: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(1, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(1, constID))
		ENDIF
		IF iMural_3_sale_cost = -1			//Mural 3: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(2, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(2, constID))
		ENDIF
		IF iMural_4_sale_cost = -1			//Mural 4: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(3, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(3, constID))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iMural_5_sale_cost = -1			//Mural 5: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(4, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(4, constID))
		ENDIF
		IF iMural_6_sale_cost = -1			//Mural 6: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(5, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(5, constID))
		ENDIF
		IF iMural_7_sale_cost = -1			//Mural 7: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(6, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(6, constID))
		ENDIF
		IF iMural_8_sale_cost = -1			//Mural 8: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(7, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(7, constID))
		ENDIF
		IF iMural_9_sale_cost = -1			//Mural 9: 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_VALUE(8, constID))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MP_PROPERTY_INTERIOR_ORIGINAL_VALUE(8, constID))
		ENDIF
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)															//Font cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)															//Signage cost
		ELSE
			IF iFont_sale_cost = -1			//Font cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_FONT))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_FONT))
			ENDIF
			IF iSign_sale_cost = -1			//Signage cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_NAME_ID, 1))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_NAME_ID))
			ENDIF
		ENDIF
		IF iOwned != constID
		AND iCONST_DEFAULT_FREE_CLUBHOUSE_EMBLEM = 1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iGeneric_Emblem_1_sale_cost = -1	//Generic Emblem 1 cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 1))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 1))
			ENDIF
		ENDIF
		IF iGeneric_Emblem_2_sale_cost = -1	//Generic Emblem 2 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 2))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 2))
		ENDIF
		IF iGeneric_Emblem_3_sale_cost = -1	//Generic Emblem 3 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 3))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 3))
		ENDIF
		IF iGeneric_Emblem_4_sale_cost = -1	//Generic Emblem 4 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 4))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 4))
		ENDIF
		IF iGeneric_Emblem_5_sale_cost = -1	//Generic Emblem 5 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 5))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 5))
		ENDIF
		IF iGeneric_Emblem_6_sale_cost = -1	//Generic Emblem 6 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 6))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 6))
		ENDIF
		IF iGeneric_Emblem_7_sale_cost = -1	//Generic Emblem 7 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 7))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 7))
		ENDIF
		IF iGeneric_Emblem_8_sale_cost = -1	//Generic Emblem 8 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 8))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 8))
		ENDIF
		IF iGeneric_Emblem_9_sale_cost = -1	//Generic Emblem 9 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 9))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 9))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF iOwned != constID
		AND iCONST_DEFAULT_FREE_CLUBHOUSE_EMBLEM = 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			IF iCrew_Emblem_sale_cost = -1	//Crew Emblem cost
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_EMBLEM, 0))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_EMBLEM, 0))
			ENDIF
		ENDIF
		IF iGun_Locker_sale_cost = -1		//Gun Locker cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_LOCKER_GUN, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_LOCKER_GUN))
		ENDIF
		IF iGarage_sale_cost = -1			//Bike Shop cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_GARAGE, 1))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLUBHOUSE_PROPERTY_MOD_ORIGINAL_VALUE(CLUBHOUSE_MOD_GARAGE))
		ENDIF
		IF bSaleBanner
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iValue)					//Base Sale Cost
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)						//Base Sale Cost
		ENDIF
		
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_1_sale_cost)			//Wall 1 sale cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_2_sale_cost)				//Wall 2 sale cost
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHanging_1_sale_cost)		//Hanging 1 sale cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHanging_2_sale_cost)			//Hanging 2 sale cost
		
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFurniture_1_sale_cost)	//Furniture 1 sale cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFurniture_2_sale_cost)		//Furniture 2 sale cost
		
		IF iOwned != constID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_1_sale_cost)		//Mural 1 sale cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_2_sale_cost)			//Mural 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_3_sale_cost)			//Mural 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_4_sale_cost)			//Mural 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_5_sale_cost)			//Mural 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_6_sale_cost)			//Mural 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_7_sale_cost)			//Mural 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_8_sale_cost)			//Mural 8 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMural_9_sale_cost)			//Mural 9 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFont_sale_cost)				//Font sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSign_sale_cost)				//Signage sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_1_sale_cost)	//Generic Emblem 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_2_sale_cost)	//Generic Emblem 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_3_sale_cost)	//Generic Emblem 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_4_sale_cost)	//Generic Emblem 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_5_sale_cost)	//Generic Emblem 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_6_sale_cost)	//Generic Emblem 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_7_sale_cost)	//Generic Emblem 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_8_sale_cost)	//Generic Emblem 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGeneric_Emblem_9_sale_cost)	//Generic Emblem 9 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrew_Emblem_sale_cost)		//Crew Emblem sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGun_Locker_sale_cost)			//Gun Locker sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_sale_cost)				//Bike Shop sale cost
		
		IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()				//Starter Pack Flag
		AND COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(constID)
		AND SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(constID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

PROC POPULATE_MP_GUNRUNNING_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	FACTORY_ID eFactoryID = INT_TO_ENUM(FACTORY_ID, constID)
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eFactoryID)
	
	VECTOR pos = GET_BUNKER_BLIP_COORDS(eSimpleInteriorID)
	IF (eFactoryID = FACTORY_ID_BUNKER_3)		//Route 68 Bunker (was Harmony)
		pos = <<19.5202, 2968.9736, 52.0321>>
	ENDIF
	
	TEXT_LABEL_15 tl15Description = GET_FACTORY_DESCRIPTION_FROM_ID(eFactoryID, FACTORY_TYPE_WEAPONS)
	
	FACTORY_ID eOwned = GET_OWNED_BUNKER(PLAYER_ID())
	INT iBase_cost = GET_FACTORY_PRICE(eFactoryID)
	
	INT Decor_1_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_DECOR_0, eFactoryID)
	INT Decor_2_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_DECOR_1, eFactoryID)
	INT Decor_3_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_DECOR_2, eFactoryID)
	INT Personal_Quarters_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_PERSONAL_QUARTERS, eFactoryID)
	INT Firing_Range_1_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_FIRING_RANGE_0, eFactoryID)
	INT Firing_Range_2_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_FIRING_RANGE_1, eFactoryID)
	INT Gun_Locker_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_GUN_LOCKER, eFactoryID)
	INT Transportation_1_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_TRANSPORTATION_0, eFactoryID)
	INT Transportation_2_cost = GET_FACTORY_UPGRADE_COST(UPGRADE_ID_TRANSPORTATION_1, eFactoryID)
	
//	BOOL bSaleBanner = IS_SALE_ACTIVE_FOR_PROPERTY(constID)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_GUNRUNNING_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", eOwned:", eOwned,
			", \"", GET_FACTORY_NAME_FROM_ID(eFactoryID),
			"\" $", iBase_cost,
			", pos:", pos,
//			", sale:", GET_STRING_FROM_BOOL(bSaleBanner))	
			"")
			
	INT iBase_sale_cost = -1
	INT iDecor_1_sale_cost = -1
	INT iDecor_2_sale_cost = -1
	INT iDecor_3_sale_cost = -1
	INT iPersonal_Quarters_sale_cost = -1
	INT iFiring_Range_1_sale_cost = -1
	INT iFiring_Range_2_sale_cost = -1
	INT iGun_Locker_sale_cost = -1
	INT iTransportation_1_sale_cost = -1
	INT iTransportation_2_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_FACTORY_BASE_PRICE(eFactoryID) >= 0
		AND GET_FACTORY_BASE_PRICE(eFactoryID) > iBase_cost
		AND NOT SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryID)
			iBase_sale_cost = iBase_cost
			iBase_cost = GET_FACTORY_BASE_PRICE(eFactoryID)
		ENDIF
		
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_0, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_0, eFactoryID) > Decor_1_cost
		AND NOT (eOwned != eFactoryID)
			iDecor_1_sale_cost = Decor_1_cost
			Decor_1_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_0, eFactoryID)
		ENDIF
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_1, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_1, eFactoryID) > Decor_2_cost
			iDecor_2_sale_cost = Decor_2_cost
			Decor_2_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_1, eFactoryID)
		ENDIF
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_2, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_2, eFactoryID) > Decor_3_cost
			iDecor_3_sale_cost = Decor_3_cost
			Decor_3_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_DECOR_2, eFactoryID)
		ENDIF
		
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_PERSONAL_QUARTERS, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_PERSONAL_QUARTERS, eFactoryID) > Personal_Quarters_cost
			iPersonal_Quarters_sale_cost = Personal_Quarters_cost
			Personal_Quarters_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_PERSONAL_QUARTERS, eFactoryID)
		ENDIF
		
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_FIRING_RANGE_0, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_FIRING_RANGE_0, eFactoryID) > Firing_range_1_cost
			iFiring_Range_1_sale_cost = Firing_range_1_cost
			Firing_range_1_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_FIRING_RANGE_0, eFactoryID)
		ENDIF
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_FIRING_RANGE_1, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_FIRING_RANGE_1, eFactoryID) > Firing_range_2_cost
			iFiring_Range_2_sale_cost = Firing_range_2_cost
			Firing_range_2_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_FIRING_RANGE_1, eFactoryID)
		ENDIF
		
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_GUN_LOCKER, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_GUN_LOCKER, eFactoryID) > Gun_Locker_cost
			iGun_Locker_sale_cost = Gun_Locker_cost
			Gun_Locker_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_GUN_LOCKER, eFactoryID)
		ENDIF
		
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_TRANSPORTATION_0, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_TRANSPORTATION_0, eFactoryID) > Transportation_1_cost
			iTransportation_1_sale_cost = Transportation_1_cost
			Transportation_1_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_TRANSPORTATION_0, eFactoryID)
		ENDIF
		IF GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_TRANSPORTATION_1, eFactoryID) >= 0
		AND GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_TRANSPORTATION_1, eFactoryID) > Transportation_2_cost
			iTransportation_2_sale_cost = Transportation_2_cost
			Transportation_2_cost = GET_FACTORY_UPGRADE_BASE_COST(UPGRADE_ID_TRANSPORTATION_1, eFactoryID)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID)										// Bunker ID.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_FACTORY_NAME_FROM_ID(eFactoryID))		// Bunker GET_FACTORY_NAME_FROM_ID(eFactoryID).
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)												// (serves to distinguish bunkers from clubhouses which have a type of 0 or 1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)										// In-world coordinates of clubhouse so the map marker can be displayed in the correct position.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)										// In-world coordinates of clubhouse so the map marker can be displayed in the correct position.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_FACTORY_IMAGE_FROM_ID(eFactoryID))	// TXD reference for the clubhouse image thumbnails that appear in the left hand panel of the website, eg MP_BNKR1, MP_BNKR2 etc
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Description)							// Bunker description.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")											// Bunker address	(GET_FACTORY_ADDRESS_FROM_ID(eFactoryID))
		IF (iBase_cost = 0)																	// The base cost of the bunker before customisation. This will be the initial value displayed below the thumbnail image and clubhouse GET_FACTORY_NAME_FROM_ID(eFactoryID).
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 	//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ITEM_FREE")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBase_cost)
		ENDIF
		IF eOwned != eFactoryID																// Cost of the first style option (most likely zero as this is the default option)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Decor_1_cost)								// Cost of the first style option (most likely zero as this is the default option)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Decor_2_cost)									// Cost of the second style option
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Decor_3_cost)									// Cost of the third style options
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_cost)						// Cost of personal quarters option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Firing_Range_1_cost)							// Cost of first firing range option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Firing_Range_2_cost)							// Cost of second firing range option
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Gun_Locker_cost)								// Cost of gun locker option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Transportation_1_cost)							// Cost of first transportation option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Transportation_2_cost)							// Cost of second transportation option
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBase_sale_cost)								// Optional reduced base price. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDecor_1_sale_cost)							// Optional reduced price for the first style option (most likely not needed as this is the default option - pass -1 in if so)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDecor_2_sale_cost)							// Optional reduced price for the second style option. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDecor_3_sale_cost)							// Optional reduced price for the second style option. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_sale_cost)					// Optional reduced personal quarters price. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFiring_Range_1_sale_cost)						// Optional reduced firing range price for the first option. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFiring_Range_2_sale_cost)						// Optional reduced firing range price for the second option. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGun_Locker_sale_cost)							// Optional reduced gun locker price. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTransportation_1_sale_cost)					// Optional reduced transportation price for first option. Pass in -1 if the bunker is not on sale.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTransportation_2_sale_cost)					// Optional reduced transportation price for second option. Pass in -1 if the bunker is not on sale.
		
		IF SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryID)									//True if the bunker is part of the starter pack. This will show the starter pack icon on the progress panel.
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

PROC POPULATE_MP_SMUGGLER_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	HANGAR_ID eHangarID = INT_TO_ENUM(HANGAR_ID, constID)
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_HANGAR_ID(eHangarID)
	
	VECTOR pos = GET_HANGAR_BLIP_COORDS(eSimpleInteriorID)
	IF (eHangarID = ZANCUDO_HANGAR_A2)		//Fort Zancudo Hangar A2
		pos = <<-2055.4353, 3179.9709, 31.8103>>
	ELIF (eHangarID = ZANCUDO_HANGAR_3497)		//Fort Zancudo 3497
		pos = <<-1857.9939, 3106.2363, 31.8103>>
	ENDIF
	
	HANGAR_ID eOwned = GET_PLAYERS_OWNED_HANGAR(PLAYER_ID())
	INT iBase_cost = GET_HANGAR_PRICE(eHangarID)
	
	INT Flooring_0_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 0)
	INT Flooring_1_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 1)
	INT Flooring_2_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 2)
	INT Flooring_3_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 3)
	INT Flooring_4_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 4)
	INT Flooring_5_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 5)
	INT Flooring_6_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 6)
	INT Flooring_7_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 7)
	INT Flooring_8_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FLOORING, eHangarID, 8)
	INT Style_0_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 0)
	INT Style_1_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 1)
	INT Style_2_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 2)
	INT Style_3_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 3)
	INT Style_4_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 4)
	INT Style_5_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 5)
	INT Style_6_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 6)
	INT Style_7_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 7)
	INT Style_8_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_STYLE, eHangarID, 8)
	INT Style_0_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,0))
	INT Style_0_vibrant_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,1))
	INT Style_1_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,0))
	INT Style_1_vibrant_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,1))
	INT Style_2_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,0))
	INT Style_2_vibrant_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,1))
	INT Style_3_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,0))
	INT Style_3_vibrant_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,1))
	INT Style_4_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,0))
	INT Style_4_vibrant_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,1))
	INT Style_5_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,0))
	INT Style_5_vibrant_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,1))
	INT Style_6_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,0))
	INT Style_6_vibrant_lighting_1_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,1))
	INT Style_6_vibrant_lighting_2_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,2))
	INT Style_6_vibrant_lighting_3_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,3))
	INT Style_7_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,0))
	INT Style_7_vibrant_lighting_1_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,1))
	INT Style_7_vibrant_lighting_2_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,2))
	INT Style_7_vibrant_lighting_3_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,3))
	INT Style_8_standard_lighting_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,0))
	INT Style_8_vibrant_lighting_1_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,1))
	INT Style_8_vibrant_lighting_2_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,2))
	INT Style_8_vibrant_lighting_3_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,3))
	INT Personal_Quarters_Traditional_cost	= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 1)
	INT Personal_Quarters_Modern_cost		= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 2)
	INT Furniture_Standard_cost				= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 0)
	INT Furniture_Traditional_cost			= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 1)
	INT Furniture_Modern_cost				= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 2)
	INT Modshop_cost						= GET_HANGAR_UPGRADE_COST(eHANGAR_MOD_MODSHOP, eHangarID)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_SMUGGLER_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", eOwned:", eOwned,
			", \"", GET_HANGAR_NAME_FROM_ID(eHangarID),
			"\" $", iBase_cost,
			", pos:", pos)
			
	INT iBase_sale_cost = -1
	INT iFlooring_0_sale_cost = -1
	INT iFlooring_1_sale_cost = -1
	INT iFlooring_2_sale_cost = -1
	INT iFlooring_3_sale_cost = -1
	INT iFlooring_4_sale_cost = -1
	INT iFlooring_5_sale_cost = -1
	INT iFlooring_6_sale_cost = -1
	INT iFlooring_7_sale_cost = -1
	INT iFlooring_8_sale_cost = -1
	INT iStyle_0_sale_cost = -1
	INT iStyle_1_sale_cost = -1
	INT iStyle_2_sale_cost = -1
	INT iStyle_3_sale_cost = -1
	INT iStyle_4_sale_cost = -1
	INT iStyle_5_sale_cost = -1
	INT iStyle_6_sale_cost = -1
	INT iStyle_7_sale_cost = -1
	INT iStyle_8_sale_cost = -1
	INT iStyle_0_standard_lighting_sale_cost = -1
	INT iStyle_0_vibrant_lighting_sale_cost	 = -1
	INT iStyle_1_standard_lighting_sale_cost = -1
	INT iStyle_1_vibrant_lighting_sale_cost	 = -1
	INT iStyle_2_standard_lighting_sale_cost = -1
	INT iStyle_2_vibrant_lighting_sale_cost	 = -1
	INT iStyle_3_standard_lighting_sale_cost = -1
	INT iStyle_3_vibrant_lighting_sale_cost	 = -1
	INT iStyle_4_standard_lighting_sale_cost = -1
	INT iStyle_4_vibrant_lighting_sale_cost	 = -1
	INT iStyle_5_standard_lighting_sale_cost = -1
	INT iStyle_5_vibrant_lighting_sale_cost	 = -1
	INT iStyle_6_standard_lighting_sale_cost = -1
	INT iStyle_6_vibrant_lighting_1_sale_cost = -1
	INT iStyle_6_vibrant_lighting_2_sale_cost = -1
	INT iStyle_6_vibrant_lighting_3_sale_cost = -1
	INT iStyle_7_standard_lighting_sale_cost = -1
	INT iStyle_7_vibrant_lighting_1_sale_cost = -1
	INT iStyle_7_vibrant_lighting_2_sale_cost = -1
	INT iStyle_7_vibrant_lighting_3_sale_cost = -1
	INT iStyle_8_standard_lighting_sale_cost = -1
	INT iStyle_8_vibrant_lighting_1_sale_cost = -1
	INT iStyle_8_vibrant_lighting_2_sale_cost = -1
	INT iStyle_8_vibrant_lighting_3_sale_cost = -1
	INT iPersonal_Quarters_Traditional_sale_cost = -1
	INT iPersonal_Quarters_Modern_sale_cost = -1
	INT iFurniture_Standard_sale_cost = -1
	INT iFurniture_Traditional_sale_cost = -1
	INT iFurniture_Modern_sale_cost = -1
	INT iModshop_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_HANGAR_BASE_PRICE(eHangarID) >= 0
		AND GET_HANGAR_BASE_PRICE(eHangarID) > iBase_cost
	//	AND NOT SHOULD_HANGAR_BE_FREE_FOR_PLAYER(eHangarID)
			iBase_sale_cost = iBase_cost
			iBase_cost = GET_HANGAR_BASE_PRICE(eHangarID)
		ENDIF
		
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 0) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 0) > Flooring_0_cost
			iFlooring_0_sale_cost = Flooring_0_cost
			Flooring_0_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 0)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 1) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 1) > Flooring_1_cost
			iFlooring_1_sale_cost = Flooring_1_cost
			Flooring_1_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 1)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 2) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 2) > Flooring_2_cost
			iFlooring_2_sale_cost = Flooring_2_cost
			Flooring_2_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 2)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 3) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 3) > Flooring_3_cost
			iFlooring_3_sale_cost = Flooring_3_cost
			Flooring_3_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 3)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 4) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 4) > Flooring_4_cost
		AND NOT (eOwned != eHangarID)
			iFlooring_4_sale_cost = Flooring_4_cost
			Flooring_4_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 4)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 5) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 5) > Flooring_5_cost
			iFlooring_5_sale_cost = Flooring_5_cost
			Flooring_5_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 5)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 6) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 6) > Flooring_6_cost
			iFlooring_6_sale_cost = Flooring_6_cost
			Flooring_6_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 6)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 7) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 7) > Flooring_7_cost
			iFlooring_7_sale_cost = Flooring_7_cost
			Flooring_7_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 7)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 8) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 8) > Flooring_8_cost
			iFlooring_8_sale_cost = Flooring_8_cost
			Flooring_8_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FLOORING, eHangarID, 8)
		ENDIF
		
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 0) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 0) > Style_0_cost
		AND NOT (eOwned != eHangarID)
			iStyle_0_sale_cost = Style_0_cost
			Style_0_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 0)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 1) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 1) > Style_1_cost
			iStyle_1_sale_cost = Style_1_cost
			Style_1_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 1)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 2) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 2) > Style_2_cost
			iStyle_2_sale_cost = Style_2_cost
			Style_2_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 2)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 3) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 3) > Style_3_cost
			iStyle_3_sale_cost = Style_3_cost
			Style_3_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 3)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 4) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 4) > Style_4_cost
			iStyle_4_sale_cost = Style_4_cost
			Style_4_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 4)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 5) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 5) > Style_5_cost
			iStyle_5_sale_cost = Style_5_cost
			Style_5_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 5)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 6) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 6) > Style_6_cost
			iStyle_6_sale_cost = Style_6_cost
			Style_6_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 6)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 7) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 7) > Style_7_cost
			iStyle_7_sale_cost = Style_7_cost
			Style_7_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 7)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 8) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 8) > Style_8_cost
			iStyle_8_sale_cost = Style_8_cost
			Style_8_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_STYLE, eHangarID, 8)
		ENDIF
		
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,0)) > Style_0_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_0_PURCHASED(PLAYER_ID()))
			iStyle_0_standard_lighting_sale_cost = Style_0_standard_lighting_cost
			Style_0_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,1)) > Style_0_vibrant_lighting_cost
			iStyle_0_vibrant_lighting_sale_cost = Style_0_vibrant_lighting_cost
			Style_0_vibrant_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(0,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,0)) > Style_1_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_1_PURCHASED(PLAYER_ID()))
			iStyle_1_standard_lighting_sale_cost = Style_1_standard_lighting_cost
			Style_1_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,1)) > Style_1_vibrant_lighting_cost
			iStyle_1_vibrant_lighting_sale_cost = Style_1_vibrant_lighting_cost
			Style_1_vibrant_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(1,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,0)) > Style_2_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_2_PURCHASED(PLAYER_ID()))
			iStyle_2_standard_lighting_sale_cost = Style_2_standard_lighting_cost
			Style_2_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,1)) > Style_2_vibrant_lighting_cost
			iStyle_2_vibrant_lighting_sale_cost = Style_2_vibrant_lighting_cost
			Style_2_vibrant_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(2,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,0)) > Style_3_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_3_PURCHASED(PLAYER_ID()))
			iStyle_3_standard_lighting_sale_cost = Style_3_standard_lighting_cost
			Style_3_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,1)) > Style_3_vibrant_lighting_cost
			iStyle_3_vibrant_lighting_sale_cost = Style_3_vibrant_lighting_cost
			Style_3_vibrant_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(3,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,0)) > Style_4_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_4_PURCHASED(PLAYER_ID()))
			iStyle_4_standard_lighting_sale_cost = Style_4_standard_lighting_cost
			Style_4_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,1)) > Style_4_vibrant_lighting_cost
			iStyle_4_vibrant_lighting_sale_cost = Style_4_vibrant_lighting_cost
			Style_4_vibrant_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(4,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,0)) > Style_5_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_5_PURCHASED(PLAYER_ID()))
			iStyle_5_standard_lighting_sale_cost = Style_5_standard_lighting_cost
			Style_5_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,1)) > Style_5_vibrant_lighting_cost
			iStyle_5_vibrant_lighting_sale_cost = Style_5_vibrant_lighting_cost
			Style_5_vibrant_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(5,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,0)) > Style_6_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_6_PURCHASED(PLAYER_ID()))
			iStyle_6_standard_lighting_sale_cost = Style_6_standard_lighting_cost
			Style_6_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,1)) > Style_6_vibrant_lighting_1_cost
			iStyle_6_vibrant_lighting_1_sale_cost = Style_6_vibrant_lighting_1_cost
			Style_6_vibrant_lighting_1_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,2)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,2)) > Style_6_vibrant_lighting_2_cost
			iStyle_6_vibrant_lighting_2_sale_cost = Style_6_vibrant_lighting_2_cost
			Style_6_vibrant_lighting_2_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,2))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,3)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,3)) > Style_6_vibrant_lighting_3_cost
			iStyle_6_vibrant_lighting_3_sale_cost = Style_6_vibrant_lighting_3_cost
			Style_6_vibrant_lighting_3_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(6,3))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,0)) > Style_7_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_7_PURCHASED(PLAYER_ID()))
			iStyle_7_standard_lighting_sale_cost = Style_7_standard_lighting_cost
			Style_7_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,1)) > Style_7_vibrant_lighting_1_cost
			iStyle_7_vibrant_lighting_1_sale_cost = Style_7_vibrant_lighting_1_cost
			Style_7_vibrant_lighting_1_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,2)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,2)) > Style_7_vibrant_lighting_2_cost
			iStyle_7_vibrant_lighting_2_sale_cost = Style_7_vibrant_lighting_2_cost
			Style_7_vibrant_lighting_2_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,2))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,3)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,3)) > Style_7_vibrant_lighting_3_cost
			iStyle_7_vibrant_lighting_3_sale_cost = Style_7_vibrant_lighting_3_cost
			Style_7_vibrant_lighting_3_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(7,3))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,0)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,0)) > Style_8_standard_lighting_cost
		AND NOT (eOwned != eHangarID OR NOT IS_PLAYER_HANGAR_STYLE_8_PURCHASED(PLAYER_ID()))
			iStyle_8_standard_lighting_sale_cost = Style_8_standard_lighting_cost
			Style_8_standard_lighting_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,0))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,1)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,1)) > Style_8_vibrant_lighting_1_cost
			iStyle_8_vibrant_lighting_1_sale_cost = Style_8_vibrant_lighting_1_cost
			Style_8_vibrant_lighting_1_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,1))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,2)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,2)) > Style_8_vibrant_lighting_2_cost
			iStyle_8_vibrant_lighting_2_sale_cost = Style_8_vibrant_lighting_2_cost
			Style_8_vibrant_lighting_2_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,2))
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,3)) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,3)) > Style_8_vibrant_lighting_3_cost
			iStyle_8_vibrant_lighting_3_sale_cost = Style_8_vibrant_lighting_3_cost
			Style_8_vibrant_lighting_3_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_LIGHTING, eHangarID, GET_HANGER_LIGHTING_ID_FROM_STYLE_AND_LIGHTING(8,3))
		ENDIF
		
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 1) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 1) > Personal_Quarters_Traditional_cost
			iPersonal_Quarters_Traditional_sale_cost = Personal_Quarters_Traditional_cost
			Personal_Quarters_Traditional_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 1)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 2) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 2) > Personal_Quarters_Modern_cost
			iPersonal_Quarters_Modern_sale_cost = Personal_Quarters_Modern_cost
			Personal_Quarters_Modern_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_PERSONAL_QUARTERS, eHangarID, 2)
		ENDIF
		
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 0) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 0) > Furniture_Standard_cost
		AND NOT (eOwned != eHangarID)
			iFurniture_Standard_sale_cost = Furniture_Standard_cost
			Furniture_Standard_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 0)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 1) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 1) > Furniture_Traditional_cost
			iFurniture_Traditional_sale_cost = Furniture_Traditional_cost
			Furniture_Traditional_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 1)
		ENDIF
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 2) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 2) > Furniture_Modern_cost
			iFurniture_Modern_sale_cost = Furniture_Modern_cost
			Furniture_Modern_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_FURNITURE, eHangarID, 2)
		ENDIF
		
		IF GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_MODSHOP, eHangarID) >= 0
		AND GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_MODSHOP, eHangarID) > Modshop_cost
			iModshop_sale_cost = Modshop_cost
			Modshop_cost = GET_HANGAR_UPGRADE_BASE_COST(eHANGAR_MOD_MODSHOP, eHangarID)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID)										// 1	ID	int	Hangar ID.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_HANGAR_NAME_FROM_ID(eHangarID))			// 2	Name	string	Hangar name.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)												// 3	Type	int	3 (serves to distinguish bunkers from clubhouses which have a type of 0 or 1 and bunkers which have a type of 2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)										// 4	X position	float	In-world coordinates of clubhouse so the map marker can be displayed in the correct position.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)										// 5	Y position	float	In-world coordinates of clubhouse so the map marker can be displayed in the correct position.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_HANGAR_IMAGE_FROM_ID(eHangarID))	// 6	Image ID	string	TXD reference for the hangar image thumbnails that appear in the left hand panel of the website, eg MP_HNGR1, MP_HNGR2 etc
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_HANGAR_DESCRIPTION_FROM_ID(eHangarID))	// 7	Description	string	Hangar description.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")											// 8	Address	string	Hangar address
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBase_cost)									// 9	Base cost
		IF eOwned != eHangarID																// 10	Style 1 cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_0_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_1_cost)									// 11	Style 2 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_2_cost)									// 12	Style 3 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_3_cost)									// 13	Style 4 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_4_cost)									// 14	Style 5 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_5_cost)									// 15	Style 6 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_6_cost)									// 16	Style 7 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_7_cost)									// 17	Style 8 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_8_cost)									// 18	Style 9 cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF eOwned != eHangarID																// 19	Style 1 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_0_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_0_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_0_vibrant_lighting_cost)					// 20	Style 1 vibrant lighting cost
		IF eOwned != eHangarID																// 21	Style 2 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_1_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_1_standard_lighting_cost)			
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_1_vibrant_lighting_cost)					// 22	Style 2 vibrant lighting cost
		IF eOwned != eHangarID																// 23	Style 3 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_2_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_2_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_2_vibrant_lighting_cost)					// 24	Style 3 vibrant lighting cost
		IF eOwned != eHangarID																// 25	Style 4 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_3_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_3_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_3_vibrant_lighting_cost)					// 26	Style 4 vibrant lighting cost
		IF eOwned != eHangarID																// 27	Style 5 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_4_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_4_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_4_vibrant_lighting_cost)					// 28	Style 5 vibrant lighting cost
		IF eOwned != eHangarID																// 29	Style 6 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_5_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_5_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_5_vibrant_lighting_cost)					// 30	Style 6 vibrant lighting cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF eOwned != eHangarID																// 31	Style 7 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_6_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_6_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_6_vibrant_lighting_1_cost)				// 32	Style 7 vibrant lighting 1 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_6_vibrant_lighting_2_cost)				// 33	Style 7 vibrant lighting 2 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_6_vibrant_lighting_3_cost)				// 34	Style 7 vibrant lighting 3 cost
		IF eOwned != eHangarID																// 35	Style 8 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_7_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_7_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_7_vibrant_lighting_1_cost)				// 36	Style 8 vibrant lighting 1 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_7_vibrant_lighting_2_cost)				// 37	Style 8 vibrant lighting 2 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_7_vibrant_lighting_3_cost)				// 38	Style 8 vibrant lighting 3 cost
		IF eOwned != eHangarID																// 39	Style 9 standard lighting cost
		OR NOT IS_PLAYER_HANGAR_STYLE_8_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_8_standard_lighting_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_8_vibrant_lighting_1_cost)				// 40	Style 9 vibrant lighting 1 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_8_vibrant_lighting_2_cost)				// 41	Style 9 vibrant lighting 2 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_8_vibrant_lighting_3_cost)				// 42	Style 9 vibrant lighting 3 cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_0_cost)								// 43	Flooring 1 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_1_cost)								// 44	Flooring 2 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_2_cost)								// 45	Flooring 3 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_3_cost)								// 46	Flooring 4 cost
		IF eOwned != eHangarID									
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_4_cost)								// 47	Flooring 5 cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_5_cost)								// 48	Flooring 6 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_6_cost)								// 49	Flooring 7 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_7_cost)								// 50	Flooring 8 cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Flooring_8_cost)								// 51	Flooring 9 cost
		IF eOwned != eHangarID																// 52	Standard furniture cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Furniture_Standard_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Furniture_Traditional_cost)					// 53	Traditional furniture cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Furniture_Modern_cost)							// 54	Modern furniture cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_Traditional_cost)			// 55	Traditional sleeping quarters cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_Modern_cost)					// 56	Modern sleeping quarters cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Modshop_cost)									// 57	Aircraft Workshop cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBase_sale_cost)								// 58	Base sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_0_sale_cost)							// 59	Style 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_1_sale_cost)							// 60	Style 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_2_sale_cost)							// 61	Style 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_3_sale_cost)							// 62	Style 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_4_sale_cost)							// 63	Style 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_5_sale_cost)							// 64	Style 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_sale_cost)							// 65	Style 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_sale_cost)							// 66	Style 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_sale_cost)							// 67	Style 9 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_0_standard_lighting_sale_cost)			// 68	Style 1 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_0_vibrant_lighting_sale_cost)			// 69	Style 1 vibrant lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_1_standard_lighting_sale_cost)			// 70	Style 2 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_1_vibrant_lighting_sale_cost)			// 71	Style 2 vibrant lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_2_standard_lighting_sale_cost)			// 72	Style 3 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_2_vibrant_lighting_sale_cost)			// 73	Style 3 vibrant lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_3_standard_lighting_sale_cost)			// 74	Style 4 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_3_vibrant_lighting_sale_cost)			// 75	Style 4 vibrant lighting sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_4_standard_lighting_sale_cost)			// 76	Style 5 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_4_vibrant_lighting_sale_cost)			// 77	Style 5 vibrant lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_5_standard_lighting_sale_cost)			// 78	Style 6 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_5_vibrant_lighting_sale_cost)			// 79	Style 6 vibrant lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_standard_lighting_sale_cost)			// 80	Style 7 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_vibrant_lighting_1_sale_cost)			// 81	Style 7 vibrant lighting 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_vibrant_lighting_2_sale_cost)			// 82	Style 7 vibrant lighting 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_vibrant_lighting_3_sale_cost)			// 83	Style 7 vibrant lighting 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_standard_lighting_sale_cost)			// 84	Style 8 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_vibrant_lighting_1_sale_cost)			// 85	Style 8 vibrant lighting 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_vibrant_lighting_2_sale_cost)			// 86	Style 8 vibrant lighting 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_vibrant_lighting_3_sale_cost)			// 87	Style 8 vibrant lighting 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_standard_lighting_sale_cost)			// 88	Style 9 standard lighting sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_vibrant_lighting_1_sale_cost)			// 89	Style 9 vibrant lighting 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_vibrant_lighting_2_sale_cost)			// 90	Style 9 vibrant lighting 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_vibrant_lighting_3_sale_cost)			// 91	Style 9 vibrant lighting 3 sale cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_0_sale_cost)							// 92	Flooring 1 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_1_sale_cost)							// 93	Flooring 2 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_2_sale_cost)							// 94	Flooring 3 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_3_sale_cost)							// 95	Flooring 4 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_4_sale_cost)							// 96	Flooring 5 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_5_sale_cost)							// 97	Flooring 6 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_6_sale_cost)							// 98	Flooring 7 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_7_sale_cost)							// 99	Flooring 8 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFlooring_8_sale_cost)							// 100	Flooring 9 sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFurniture_Standard_sale_cost)					// 101	Standard furniture sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFurniture_Traditional_sale_cost)				// 102	Traditional furniture sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFurniture_Modern_sale_cost)					// 103	Modern furniture sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_Traditional_sale_cost)		// 104	Traditional sleeping quarters sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_Modern_sale_cost)			// 105	Modern sleeping quarters sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iModshop_sale_cost)							// 106	Aircraft Workshop sale cost
		
	//	IF SHOULD_HANGAR_BE_FREE_FOR_PLAYER(eHangarID)										// 107	Starter Pack flag
	//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	//	ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
	//	ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

PROC POPULATE_MP_GANG_OPS_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	DEFUNCT_BASE_ID eBaseID = INT_TO_ENUM(DEFUNCT_BASE_ID, constID)
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(eBaseID)
	
	VECTOR pos = GET_DEFUNCT_BASE_BLIP_COORDS(eSimpleInteriorID)
	
	DEFUNCT_BASE_ID eOwned = GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID())
	INT iBase_cost = GET_DEFUNCT_BASE_PRICE(eBaseID)
	
	INT Style_0_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 0)
	INT Style_1_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 1)
	INT Style_2_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 2)
	INT Style_3_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 3)
	INT Style_4_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 4)
	INT Style_5_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 5)
	INT Style_6_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 6)
	INT Style_7_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 7)
	INT Style_8_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 8)
	INT Graphics_0_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 0)
	INT Graphics_1_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 1)
	INT Graphics_2_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 2)
	INT Graphics_3_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 3)
	INT Graphics_4_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 4)
	INT Graphics_5_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 5)
	INT Graphics_6_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 6)
	INT Graphics_7_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 7)
	INT Graphics_8_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 8)
	INT Personal_Quarters_1_cost		= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 1)
	INT Personal_Quarters_2_cost		= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 2)
	INT Personal_Quarters_3_cost		= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 3)
	INT Orbital_Weapon_cost				= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_ORBITAL_WEAPON, eBaseID)
	INT Security_Room_cost				= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_SECURITY_ROOM, eBaseID)
	INT Lounge_0_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 0)
	INT Lounge_1_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 1)
	INT Lounge_2_cost					= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 2)
	INT Privacy_glass_cost				= GET_DEFUNCT_BASE_UPGRADE_COST(eDEFUNCT_BASE_MOD_PRIVACY_GLASS, eBaseID)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_GANG_OPS_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", eOwned:", eOwned,
			", \"", GET_DEFUNCT_BASE_NAME_FROM_ID(eBaseID),
			"\" $", iBase_cost,
			", pos:", pos)
			
	INT iBase_sale_cost = -1
	INT iStyle_0_sale_cost = -1
	INT iStyle_1_sale_cost = -1
	INT iStyle_2_sale_cost = -1
	INT iStyle_3_sale_cost = -1
	INT iStyle_4_sale_cost = -1
	INT iStyle_5_sale_cost = -1
	INT iStyle_6_sale_cost = -1
	INT iStyle_7_sale_cost = -1
	INT iStyle_8_sale_cost = -1
	INT iGraphics_0_sale_cost = -1
	INT iGraphics_1_sale_cost = -1
	INT iGraphics_2_sale_cost = -1
	INT iGraphics_3_sale_cost = -1
	INT iGraphics_4_sale_cost = -1
	INT iGraphics_5_sale_cost = -1
	INT iGraphics_6_sale_cost = -1
	INT iGraphics_7_sale_cost = -1
	INT iGraphics_8_sale_cost = -1
	INT iPersonal_Quarters_1_sale_cost = -1
	INT iPersonal_Quarters_2_sale_cost = -1
	INT iPersonal_Quarters_3_sale_cost = -1
	INT iOrbital_Weapon_sale_cost = -1
	INT iSecurity_Room_sale_cost = -1
	INT iLounge_0_sale_cost = -1
	INT iLounge_1_sale_cost = -1
	INT iLounge_2_sale_cost = -1
	INT iPrivacy_glass_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_DEFUNCT_BASE_BASE_PRICE(eBaseID) >= 0
		AND GET_DEFUNCT_BASE_BASE_PRICE(eBaseID) > iBase_cost
	//	AND NOT SHOULD_DEFUNCT_BASE_BE_FREE_FOR_PLAYER(eBaseID)
			iBase_sale_cost = iBase_cost
			iBase_cost = GET_DEFUNCT_BASE_BASE_PRICE(eBaseID)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 0) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 0) > Style_0_cost
		AND NOT (eOwned != eBaseID)
			iStyle_0_sale_cost = Style_0_cost
			Style_0_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 0)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 1) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 1) > Style_1_cost
			iStyle_1_sale_cost = Style_1_cost
			Style_1_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 1)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 2) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 2) > Style_2_cost
			iStyle_2_sale_cost = Style_2_cost
			Style_2_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 2)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 3) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 3) > Style_3_cost
			iStyle_3_sale_cost = Style_3_cost
			Style_3_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 3)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 4) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 4) > Style_4_cost
			iStyle_4_sale_cost = Style_4_cost
			Style_4_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 4)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 5) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 5) > Style_5_cost
			iStyle_5_sale_cost = Style_5_cost
			Style_5_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 5)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 6) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 6) > Style_6_cost
			iStyle_6_sale_cost = Style_6_cost
			Style_6_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 6)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 7) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 7) > Style_7_cost
			iStyle_7_sale_cost = Style_7_cost
			Style_7_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 7)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 8) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 8) > Style_8_cost
			iStyle_8_sale_cost = Style_8_cost
			Style_8_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_STYLE, eBaseID, 8)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 0) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 0) > Graphics_0_cost
		AND NOT (eOwned != eBaseID)
			iGraphics_0_sale_cost = Graphics_0_cost
			Graphics_0_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 0)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 1) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 1) > Graphics_1_cost
			iGraphics_1_sale_cost = Graphics_1_cost
			Graphics_1_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 1)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 2) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 2) > Graphics_2_cost
			iGraphics_2_sale_cost = Graphics_2_cost
			Graphics_2_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 2)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 3) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 3) > Graphics_3_cost
			iGraphics_3_sale_cost = Graphics_3_cost
			Graphics_3_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 3)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 4) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 4) > Graphics_4_cost
			iGraphics_4_sale_cost = Graphics_4_cost
			Graphics_4_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 4)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 5) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 5) > Graphics_5_cost
			iGraphics_5_sale_cost = Graphics_5_cost
			Graphics_5_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 5)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 6) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 6) > Graphics_6_cost
			iGraphics_6_sale_cost = Graphics_6_cost
			Graphics_6_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 6)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 7) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 7) > Graphics_7_cost
			iGraphics_7_sale_cost = Graphics_7_cost
			Graphics_7_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 7)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 8) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 8) > Graphics_8_cost
			iGraphics_8_sale_cost = Graphics_8_cost
			Graphics_8_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_GRAPHIC, eBaseID, 8)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 1) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 1) > Personal_Quarters_1_cost
			iPersonal_Quarters_1_sale_cost = Personal_Quarters_1_cost
			Personal_Quarters_1_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 1)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 2) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 2) > Personal_Quarters_2_cost
			iPersonal_Quarters_2_sale_cost = Personal_Quarters_2_cost
			Personal_Quarters_2_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 2)
		ENDIF
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 3) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 3) > Personal_Quarters_3_cost
			iPersonal_Quarters_3_sale_cost = Personal_Quarters_3_cost
			Personal_Quarters_3_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PERSONAL_QUARTERS, eBaseID, 3)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_ORBITAL_WEAPON, eBaseID) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_ORBITAL_WEAPON, eBaseID) > Orbital_Weapon_cost
			iOrbital_Weapon_sale_cost = Orbital_Weapon_cost
			Orbital_Weapon_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_ORBITAL_WEAPON, eBaseID)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_SECURITY_ROOM, eBaseID) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_SECURITY_ROOM, eBaseID) > Security_Room_cost
			iSecurity_Room_sale_cost = Security_Room_cost
			Security_Room_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_SECURITY_ROOM, eBaseID)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 0) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 0) > Lounge_0_cost
		AND NOT (eOwned != eBaseID)
			iLounge_0_sale_cost = Lounge_0_cost
			Lounge_0_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 0)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 1) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 1) > Lounge_1_cost
			iLounge_1_sale_cost = Lounge_1_cost
			Lounge_1_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 1)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 2) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 2) > Lounge_2_cost
			iLounge_2_sale_cost = Lounge_2_cost
			Lounge_2_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(EDEFUNCT_BASE_MOD_LOUNGE, eBaseID, 2)
		ENDIF
		
		IF GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PRIVACY_GLASS, eBaseID) >= 0
		AND GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PRIVACY_GLASS, eBaseID) > Privacy_glass_cost
			iPrivacy_glass_sale_cost = Privacy_glass_cost
			Privacy_glass_cost = GET_DEFUNCT_BASE_UPGRADE_BASE_COST(eDEFUNCT_BASE_MOD_PRIVACY_GLASS, eBaseID)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID+40)										// 1	ID	int	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_DEFUNCT_BASE_NAME_FROM_ID(eBaseID))			// 2	Name	string	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)													// 3	Type	int	4 (serves to distinguish between bunkers, clubhouses and hangars)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)											// 4	X Position	float	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)											// 5	Y Position	float	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_DEFUNCT_BASE_IMAGE_FROM_ID(eBaseID))	// 6	Texture	string	TXD reference for the hangar image thumbnails that appear in the left hand panel of the website. 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_DEFUNCT_BASE_DESCRIPTION_FROM_ID(eBaseID))	// 7	Description	string	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")												// 8	Location	string
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBase_cost)										// 9	Base Cost	The base cost of the facility before customisation. This will be the initial value displayed below the thumbnail image and hangar name.
		IF eOwned != eBaseID																	// 10	Style 1 Cost	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_0_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_1_cost)										// 11	Style 2 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_2_cost)										// 12	Style 3 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_3_cost)										// 13	Style 4 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_4_cost)										// 14	Style 5 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_5_cost)										// 15	Style 6 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_6_cost)										// 16	Style 7 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_7_cost)										// 17	Style 8 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_8_cost)										// 18	Style 9 Cost	
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		IF eOwned != eBaseID																	// 19	Graphics 1 Cost	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_0_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_1_cost)									// 20	Graphics 2 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_2_cost)									// 21	Graphics 3 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_3_cost)									// 22	Graphics 4 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_4_cost)									// 23	Graphics 5 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_5_cost)									// 24	Graphics 6 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_6_cost)									// 25	Graphics 7 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_7_cost)									// 26	Graphics 8 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_8_cost)									// 27	Graphics 9 Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Orbital_Weapon_cost)								// 28	Orbital Weapon Cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Security_Room_cost)								// 29	Security Room Cost	
		IF eOwned != eBaseID																	// 30	Lounge 1 Cost
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lounge_0_cost)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lounge_1_cost)										// 31	Lounge 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lounge_2_cost)										// 32	Lounge 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_1_cost)							// 33	Personal Quarters 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_2_cost)							// 34	Personal Quarters 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_3_cost)							// 35	Personal Quarters 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Privacy_glass_cost)								// 36	Security Room Cost	
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBase_sale_cost)									// 37	Base Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_0_sale_cost)								// 38	Style 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_1_sale_cost)								// 39	Style 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_2_sale_cost)								// 40	Style 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_3_sale_cost)								// 41	Style 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_4_sale_cost)								// 42	Style 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_5_sale_cost)								// 43	Style 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_6_sale_cost)								// 44	Style 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_7_sale_cost)								// 45	Style 8 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_8_sale_cost)								// 46	Style 9 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_0_sale_cost)								// 47	Graphics 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_1_sale_cost)								// 48	Graphics 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_2_sale_cost)								// 49	Graphics 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_3_sale_cost)								// 50	Graphics 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_4_sale_cost)								// 51	Graphics 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_5_sale_cost)								// 52	Graphics 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_6_sale_cost)								// 53	Graphics 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_7_sale_cost)								// 54	Graphics 8 Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_8_sale_cost)								// 55	Graphics 9 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOrbital_Weapon_sale_cost)							// 56	Orbital Weapon Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSecurity_Room_sale_cost)							// 57	Security Room Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLounge_0_sale_cost)								// 58	Lounge 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLounge_1_sale_cost)								// 59	Lounge 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLounge_2_sale_cost)								// 60	Lounge 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_1_sale_cost)					// 61	Personal Quarters Sale 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_2_sale_cost)					// 62	Personal Quarters Sale 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_3_sale_cost)					// 63	Personal Quarters Sale 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPrivacy_glass_sale_cost)							// 64	Security Room Sale Cost
		
	//	IF SHOULD_BASE_BE_FREE_FOR_PLAYER(eBaseID)												// 65	Starter Pack flag
	//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	//	ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
	//	ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

PROC POPULATE_MP_BUSINESS_BATTLES_PROPERTY_PIN(INT &slot,INT constID,SCALEFORM_INDEX pagemov)
	
	NIGHTCLUB_ID eNightclubID = INT_TO_ENUM(NIGHTCLUB_ID, constID)
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(eNightclubID)
	
	VECTOR pos = GET_NIGHTCLUB_BLIP_COORDS(eSimpleInteriorID)
	
	NIGHTCLUB_ID eOwned = GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID())
	INT iNightclub_cost = GET_NIGHTCLUB_PRICE(eNightclubID)
	
	INT iNightclubNameID = GET_NIGHTCLUB_NAME_ID(PLAYER_ID())
	
	INT Style_0_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 0)
	INT Style_1_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 1)
	INT Style_2_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 2)
	INT Lighting_0_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 0)
	INT Lighting_1_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 1)
	INT Lighting_2_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 2)
	INT Lighting_3_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 3)
	INT Lighting_4_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 4)
	INT DryIce_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DRYICE, eNightclubID, 1)
	INT Dancers_1_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 1)
	INT Dancers_2_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 2)
	INT Dancers_3_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 3)
	INT Dancers_4_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 4)
	INT Dancers_5_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 5)
	INT Dancers_6_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 6)
	INT Dancers_7_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 7)
	INT Dancers_8_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 8)
	INT Dancers_9_cost					= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 9)
	INT Storage_1_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1)
	INT Storage_2_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2)
	INT Storage_3_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3)
	INT Storage_4_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4)
	INT Garage_1_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1)
	INT Garage_2_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2)
	INT Garage_3_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3)
	INT Garage_4_cost					= GET_BUSINESS_HUB_UPGRADE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4)
	INT Name_0_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 0)
	INT Name_1_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 1)
	INT Name_2_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 2)
	INT Name_3_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 3)
	INT Name_4_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 4)
	INT Name_5_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 5)
	INT Name_6_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 6)
	INT Name_7_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 7)
	INT Name_8_cost						= GET_NIGHTCLUB_UPGRADE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 8)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_BUSINESS_BATTLES_PROPERTY_PIN slot:", slot,
			", constID:", constID,
			", eOwned:", eOwned,
			", \"", GET_NIGHTCLUB_NAME_FROM_ID(eNightclubID),
			"\" $", iNightclub_cost,
			", pos:", pos)
			
	INT iNightclub_sale_cost = -1
	INT iStyle_0_sale_cost = -1
	INT iStyle_1_sale_cost = -1
	INT iStyle_2_sale_cost = -1
	INT iLighting_0_sale_cost = -1
	INT iLighting_1_sale_cost = -1
	INT iLighting_2_sale_cost = -1
	INT iLighting_3_sale_cost = -1
	INT iLighting_4_sale_cost = -1
	INT iDryIce_sale_cost = -1
	INT iDancers_1_sale_cost = -1
	INT iDancers_2_sale_cost = -1
	INT iDancers_3_sale_cost = -1
	INT iDancers_4_sale_cost = -1
	INT iDancers_5_sale_cost = -1
	INT iDancers_6_sale_cost = -1
	INT iDancers_7_sale_cost = -1
	INT iDancers_8_sale_cost = -1
	INT iDancers_9_sale_cost = -1
	INT iStorage_1_sale_cost = -1
	INT iStorage_2_sale_cost = -1
	INT iStorage_3_sale_cost = -1
	INT iStorage_4_sale_cost = -1
	INT iGarage_1_sale_cost = -1
	INT iGarage_2_sale_cost = -1
	INT iGarage_3_sale_cost = -1
	INT iGarage_4_sale_cost = -1
	INT iName_0_sale_cost = -1
	INT iName_1_sale_cost = -1
	INT iName_2_sale_cost = -1
	INT iName_3_sale_cost = -1
	INT iName_4_sale_cost = -1
	INT iName_5_sale_cost = -1
	INT iName_6_sale_cost = -1
	INT iName_7_sale_cost = -1
	INT iName_8_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_NIGHTCLUB_BASE_PRICE(eNightclubID) >= 0
		AND GET_NIGHTCLUB_BASE_PRICE(eNightclubID) > iNightclub_cost
	//	AND NOT SHOULD_NIGHTCLUB_BE_FREE_FOR_PLAYER(eNightclubID)
			iNightclub_sale_cost = iNightclub_cost
			iNightclub_cost = GET_NIGHTCLUB_BASE_PRICE(eNightclubID)
		ENDIF
		
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 0) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 0) > Style_0_cost
		AND NOT (eOwned != eNightclubID)
			iStyle_0_sale_cost = Style_0_cost
			Style_0_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 0)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 1) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 1) > Style_1_cost
			iStyle_1_sale_cost = Style_1_cost
			Style_1_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 1)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 2) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 2) > Style_2_cost
			iStyle_2_sale_cost = Style_2_cost
			Style_2_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_STYLE, eNightclubID, 2)
		ENDIF
		
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 0) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 0) > Lighting_0_cost
		AND NOT (eOwned != eNightclubID)
			iLighting_0_sale_cost = Lighting_0_cost
			Lighting_0_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 0)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 1) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 1) > Lighting_1_cost
			iLighting_1_sale_cost = Lighting_1_cost
			Lighting_1_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 1)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 2) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 2) > Lighting_2_cost
			iLighting_2_sale_cost = Lighting_2_cost
			Lighting_2_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 2)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 3) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 3) > Lighting_3_cost
			iLighting_3_sale_cost = Lighting_3_cost
			Lighting_3_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 3)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 4) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 4) > Lighting_4_cost
			iLighting_4_sale_cost = Lighting_4_cost
			Lighting_4_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_LIGHTING, eNightclubID, 4)
		ENDIF
		
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DRYICE, eNightclubID, 1) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DRYICE, eNightclubID, 1) > DryIce_cost
			iDryIce_sale_cost = DryIce_cost
			DryIce_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DRYICE, eNightclubID, 1)
		ENDIF
		
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 1) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 1) > Dancers_1_cost
			iDancers_1_sale_cost = Dancers_1_cost
			Dancers_1_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 1)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 2) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 2) > Dancers_2_cost
			iDancers_2_sale_cost = Dancers_2_cost
			Dancers_2_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 2)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 3) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 3) > Dancers_3_cost
			iDancers_3_sale_cost = Dancers_3_cost
			Dancers_3_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 3)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 4) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 4) > Dancers_4_cost
			iDancers_4_sale_cost = Dancers_4_cost
			Dancers_4_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 4)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 5) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 5) > Dancers_5_cost
			iDancers_5_sale_cost = Dancers_5_cost
			Dancers_5_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 5)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 6) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 6) > Dancers_6_cost
			iDancers_6_sale_cost = Dancers_6_cost
			Dancers_6_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 6)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 7) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 7) > Dancers_7_cost
			iDancers_7_sale_cost = Dancers_7_cost
			Dancers_7_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 7)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 8) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 8) > Dancers_8_cost
			iDancers_8_sale_cost = Dancers_8_cost
			Dancers_8_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 8)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 9) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 9) > Dancers_9_cost
			iDancers_9_sale_cost = Dancers_9_cost
			Dancers_9_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_DANCERS, eNightclubID, 9)
		ENDIF
		
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1) > Storage_1_cost
			iStorage_1_sale_cost = Storage_1_cost
			Storage_1_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1)
		ENDIF
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2) > Storage_2_cost
			iStorage_2_sale_cost = Storage_2_cost
			Storage_2_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2)
		ENDIF
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3) > Storage_3_cost
			iStorage_3_sale_cost = Storage_3_cost
			Storage_3_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3)
		ENDIF
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4) > Storage_4_cost
			iStorage_4_sale_cost = Storage_4_cost
			Storage_4_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_WAREHOUSE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4)
		ENDIF
		
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1) > Garage_1_cost
			iGarage_1_sale_cost = Garage_1_cost
			Garage_1_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 1)
		ENDIF
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2) > Garage_2_cost
			iGarage_2_sale_cost = Garage_2_cost
			Garage_2_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 2)
		ENDIF
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3) > Garage_3_cost
			iGarage_3_sale_cost = Garage_3_cost
			Garage_3_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 3)
		ENDIF
		IF GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4) >= 0
		AND GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4) > Garage_4_cost
		AND HAS_MP_HACKER_TRUCK_ACCESS()
		AND IS_PLAYER_HACKER_TRUCK_PURCHASED(PLAYER_ID())
			iGarage_4_sale_cost = Garage_4_cost
			Garage_4_cost = GET_BUSINESS_HUB_UPGRADE_BASE_COST(eBUSINESS_HUB_MOD_GARAGE, INT_TO_ENUM(BUSINESS_HUB_ID, ENUM_TO_INT(eNightclubID)), 4)
		ENDIF
		
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 0) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 0) > Name_0_cost
		AND NOT (eOwned = NIGHTCLUB_ID_INVALID)
		AND NOT (iNightclubNameID = 0)
			iName_0_sale_cost = Name_0_cost
			Name_0_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 0)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 1) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 1) > Name_1_cost
		AND NOT (iNightclubNameID = 1)
			iName_1_sale_cost = Name_1_cost
			Name_1_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 1)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 2) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 2) > Name_2_cost
		AND NOT (iNightclubNameID = 2)
			iName_2_sale_cost = Name_2_cost
			Name_2_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 2)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 3) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 3) > Name_3_cost
		AND NOT (iNightclubNameID = 3)
			iName_3_sale_cost = Name_3_cost
			Name_3_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 3)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 4) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 4) > Name_4_cost
		AND NOT (iNightclubNameID = 4)
			iName_4_sale_cost = Name_4_cost
			Name_4_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 4)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 5) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 5) > Name_5_cost
		AND NOT (iNightclubNameID = 5)
			iName_5_sale_cost = Name_5_cost
			Name_5_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 5)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 6) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 6) > Name_6_cost
		AND NOT (iNightclubNameID = 6)
			iName_6_sale_cost = Name_6_cost
			Name_6_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 6)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 7) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 7) > Name_7_cost
		AND NOT (iNightclubNameID = 7)
			iName_7_sale_cost = Name_7_cost
			Name_7_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 7)
		ENDIF
		IF GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 8) >= 0
		AND GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 8) > Name_8_cost
		AND NOT (iNightclubNameID = 8)
			iName_8_sale_cost = Name_8_cost
			Name_8_cost = GET_NIGHTCLUB_UPGRADE_BASE_COST(eNIGHTCLUB_MOD_NAME, eNightclubID, 8)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID+10)											// 1	ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_NIGHTCLUB_NAME_FROM_ID(eNightclubID))			// 2	Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)														// 3	Type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)												// 4	X Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)												// 5	Y Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_NIGHTCLUB_IMAGE_FROM_ID(eNightclubID))		// 6	Texture
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_NIGHTCLUB_DESCRIPTION_FROM_ID(eNightclubID))	// 7	Description
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")													// 8	Location
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNightclub_cost)										// 9	Base Cost
		IF eOwned != eNightclubID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_0_cost)										// 10	Style 1 Cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_1_cost)											// 11	Style 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_2_cost)											// 12	Style 3 Cost
		IF eOwned != eNightclubID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lighting_0_cost)									// 13	Lighting 1 Cost
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lighting_1_cost)										// 14	Lighting 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lighting_2_cost)										// 15	Lighting 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lighting_3_cost)										// 16	Lighting 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lighting_4_cost)										// 17	Lighting 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DryIce_cost)										// 18	Lighting 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_1_cost)										// 19	Dancers 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_2_cost)										// 20	Dancers 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_3_cost)										// 21	Dancers 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_4_cost)										// 22	Dancers 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_5_cost)										// 23	Dancers 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_6_cost)										// 24	Dancers 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_7_cost)										// 25	Dancers 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_8_cost)										// 26	Dancers 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dancers_9_cost)										// 27	Dancers 9 Cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Storage_1_cost)										// 28	Storage 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Storage_2_cost)										// 29	Storage 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Storage_3_cost)										// 30	Storage 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Storage_4_cost)										// 31	Storage 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Garage_1_cost)											// 32	Garage 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Garage_2_cost)											// 33	Garage 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Garage_3_cost)											// 34	Garage 3 Cost
		//Hacker Truck garage. Pass in -1 if this is not available and the website button should be hidden.
		IF NOT HAS_MP_HACKER_TRUCK_ACCESS()
		OR NOT IS_PLAYER_HACKER_TRUCK_PURCHASED(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Garage_4_cost)										// 35	Garage 4 Cost
		ENDIF
		IF eOwned = NIGHTCLUB_ID_INVALID
		OR iNightclubNameID = 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_0_cost)										// 36	Name 1 Cost
		ENDIF																						   
		IF iNightclubNameID = 1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_1_cost)										// 37	Name 2 Cost
		ENDIF																						   
		IF iNightclubNameID = 2
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_2_cost)										// 38	Name 3 Cost
		ENDIF																						   
		IF iNightclubNameID = 3
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_3_cost)										// 39	Name 4 Cost
		ENDIF																						   
		IF iNightclubNameID = 4
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_4_cost)										// 40	Name 5 Cost
		ENDIF																						   
		IF iNightclubNameID = 5
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_5_cost)										// 41	Name 6 Cost
		ENDIF																						   
		IF iNightclubNameID = 6
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_6_cost)										// 42	Name 7 Cost
		ENDIF																						   
		IF iNightclubNameID = 7
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_7_cost)										// 43	Name 8 Cost
		ENDIF																						   
		IF iNightclubNameID = 8
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Name_8_cost)										// 44	Name 9 Cost
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNightclub_sale_cost)									// 45	Base Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_0_sale_cost)									// 46	Style 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_1_sale_cost)									// 47	Style 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_2_sale_cost)									// 48	Style 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLighting_0_sale_cost)									// 49	Lighting 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLighting_1_sale_cost)									// 50	Lighting 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLighting_2_sale_cost)									// 51	Lighting 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLighting_3_sale_cost)									// 52	Lighting 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLighting_4_sale_cost)									// 53	Lighting 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDryIce_sale_cost)									// 54	Lighting 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_1_sale_cost)									// 55	Dancers 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_2_sale_cost)									// 56	Dancers 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_3_sale_cost)									// 57	Dancers 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_4_sale_cost)									// 58	Dancers 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_5_sale_cost)									// 59	Dancers 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_6_sale_cost)									// 60	Dancers 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_7_sale_cost)									// 61	Dancers 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_8_sale_cost)									// 62	Dancers 8 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDancers_9_sale_cost)									// 63	Dancers 9 Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStorage_1_sale_cost)									// 64	Storage 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStorage_2_sale_cost)									// 65	Storage 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStorage_3_sale_cost)									// 66	Storage 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStorage_4_sale_cost)									// 67	Storage 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_1_sale_cost)									// 68	Garage 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_2_sale_cost)									// 69	Garage 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_3_sale_cost)									// 70	Garage 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_4_sale_cost)									// 71	Garage 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_0_sale_cost)										// 72	Name 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_1_sale_cost)										// 73	Name 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_2_sale_cost)										// 74	Name 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_3_sale_cost)										// 75	Name 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_4_sale_cost)										// 76	Name 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_5_sale_cost)										// 77	Name 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_6_sale_cost)										// 78	Name 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_7_sale_cost)										// 79	Name 8 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iName_8_sale_cost)										// 80	Name 9 Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

#IF FEATURE_CASINO_HEIST
PROC POPULATE_MP_ARCADE_PROPERTY_PIN(INT &slot, INT constID, SCALEFORM_INDEX pagemov)
	ARCADE_PROPERTY_ID eArcadeID = INT_TO_ENUM(ARCADE_PROPERTY_ID, constID)
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_ARCADE_PROPERTY_ID(eArcadeID)
	
	VECTOR pos = GET_ARCADE_BLIP_COORDS(eSimpleInteriorID)
	
	ARCADE_PROPERTY_ID eOwned = GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID())
	
	INT iArcade_cost = GET_ARCADE_PRICE(eArcadeID)
	
	INT Floor_0_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 0)
	INT Floor_1_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 1)
	INT Floor_2_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 2)
	INT Floor_3_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 3)
	INT Floor_4_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 4)
	INT Floor_5_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 5)
	INT Floor_6_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 6)
	INT Floor_7_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 7)
	INT Floor_8_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_FLOOR, eArcadeID, 8)
	
	INT Ceiling_0_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_CEILING, eArcadeID, 0)
	INT Ceiling_1_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_CEILING, eArcadeID, 1)
	INT Ceiling_2_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_CEILING, eArcadeID, 2)
	
	INT Wall_0_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 0)
	INT Wall_1_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 1)
	INT Wall_2_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 2)
	INT Wall_3_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 3)
	INT Wall_4_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 4)
	INT Wall_5_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 5)
	INT Wall_6_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 6)
	INT Wall_7_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 7)
	INT Wall_8_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_WALL, eArcadeID, 8)
	
	INT Personal_Quarters_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_PERSONAL_QUARTERS, eArcadeID, 1)
	
	INT Garage_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_GARAGE, eArcadeID, 1)
	
	INT Neon_Lights_1_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 0)
	INT Neon_Lights_2_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 1)
	INT Neon_Lights_3_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 2)
	INT Neon_Lights_4_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 3)
	INT Neon_Lights_5_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 4)
	INT Neon_Lights_6_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 5)
	INT Neon_Lights_7_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 6)
	INT Neon_Lights_8_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 7)
	INT Neon_Lights_9_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 8)
	
	INT Screens_cost = GET_ARCADE_UPGRADE_COST(eARCADE_MOD_SCREENS, eArcadeID, 1)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_ARCADE_PROPERTY_PIN slot: ", slot, ", constID: ", constID, ", eOwned: ", eOwned, ", \"", GET_ARCADE_NAME_FROM_ID(eArcadeID), "\" $", iArcade_cost, ", pos: ", pos)
	
	INT iArcade_sale_cost = -1
	
	INT iFloor_0_sale_cost = -1
	INT iFloor_1_sale_cost = -1
	INT iFloor_2_sale_cost = -1
	INT iFloor_3_sale_cost = -1
	INT iFloor_4_sale_cost = -1
	INT iFloor_5_sale_cost = -1
	INT iFloor_6_sale_cost = -1
	INT iFloor_7_sale_cost = -1
	INT iFloor_8_sale_cost = -1
	
	INT iCeiling_0_sale_cost = -1
	INT iCeiling_1_sale_cost = -1
	INT iCeiling_2_sale_cost = -1
	
	INT iWall_0_sale_cost = -1
	INT iWall_1_sale_cost = -1
	INT iWall_2_sale_cost = -1
	INT iWall_3_sale_cost = -1
	INT iWall_4_sale_cost = -1
	INT iWall_5_sale_cost = -1
	INT iWall_6_sale_cost = -1
	INT iWall_7_sale_cost = -1
	INT iWall_8_sale_cost = -1
	
	INT iPersonal_Quarters_sale_cost = -1
	
	INT iGarage_sale_cost = -1
	
	INT iNeon_Lights_1_sale_cost = -1
	INT iNeon_Lights_2_sale_cost = -1
	INT iNeon_Lights_3_sale_cost = -1
	INT iNeon_Lights_4_sale_cost = -1
	INT iNeon_Lights_5_sale_cost = -1
	INT iNeon_Lights_6_sale_cost = -1
	INT iNeon_Lights_7_sale_cost = -1
	INT iNeon_Lights_8_sale_cost = -1
	INT iNeon_Lights_9_sale_cost = -1
	
	INT iScreens_sale_cost = -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_ARCADE_BASE_PRICE(eArcadeID) >= 0
		AND GET_ARCADE_BASE_PRICE(eArcadeID) > iArcade_cost
			iArcade_sale_cost = iArcade_cost
			iArcade_cost = GET_ARCADE_BASE_PRICE(eArcadeID)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 0) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 0) > Floor_0_cost
		AND NOT (eOwned != eArcadeID)
			iFloor_0_sale_cost = Floor_0_cost
			Floor_0_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 0)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 1) > Floor_1_cost
			iFloor_1_sale_cost = Floor_1_cost
			Floor_1_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 1)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 2) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 2) > Floor_2_cost
			iFloor_2_sale_cost = Floor_2_cost
			Floor_2_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 2)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 3) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 3) > Floor_3_cost
			iFloor_3_sale_cost = Floor_3_cost
			Floor_3_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 3)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 4) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 4) > Floor_4_cost
			iFloor_4_sale_cost = Floor_4_cost
			Floor_4_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 4)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 5) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 5) > Floor_5_cost
			iFloor_5_sale_cost = Floor_5_cost
			Floor_5_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 5)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 6) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 6) > Floor_6_cost
			iFloor_6_sale_cost = Floor_6_cost
			Floor_6_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 6)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 7) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 7) > Floor_7_cost
			iFloor_7_sale_cost = Floor_7_cost
			Floor_7_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 7)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 8) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 8) > Floor_8_cost
			iFloor_8_sale_cost = Floor_8_cost
			Floor_8_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_FLOOR, eArcadeID, 8)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 0) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 0) > Ceiling_0_cost
		AND NOT (eOwned != eArcadeID)
			iCeiling_0_sale_cost = Ceiling_0_cost
			Ceiling_0_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 0)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 1) > Ceiling_1_cost
			iCeiling_1_sale_cost = Ceiling_1_cost
			Ceiling_1_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 1)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 2) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 2) > Ceiling_2_cost
			iCeiling_2_sale_cost = Ceiling_2_cost
			Ceiling_2_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_CEILING, eArcadeID, 2)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 0) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 0) > Wall_0_cost
		AND NOT (eOwned != eArcadeID)
			iWall_0_sale_cost = Wall_0_cost
			Wall_0_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 0)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 1) > Wall_1_cost
			iWall_1_sale_cost = Wall_1_cost
			Wall_1_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 1)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 2) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 2) > Wall_2_cost
			iWall_2_sale_cost = Wall_2_cost
			Wall_2_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 2)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 3) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 3) > Wall_3_cost
			iWall_3_sale_cost = Wall_3_cost
			Wall_3_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 3)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 4) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 4) > Wall_4_cost
			iWall_4_sale_cost = Wall_4_cost
			Wall_4_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 4)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 5) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 5) > Wall_5_cost
			iWall_5_sale_cost = Wall_5_cost
			Wall_5_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 5)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 6) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 6) > Wall_6_cost
			iWall_6_sale_cost = Wall_6_cost
			Wall_6_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 6)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 7) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 7) > Wall_7_cost
			iWall_7_sale_cost = Wall_7_cost
			Wall_7_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 7)
		ENDIF
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 8) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 8) > Wall_8_cost
			iWall_8_sale_cost = Wall_8_cost
			Wall_8_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_WALL, eArcadeID, 8)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_PERSONAL_QUARTERS, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_PERSONAL_QUARTERS, eArcadeID, 1) > Personal_Quarters_cost
			iPersonal_Quarters_sale_cost = Personal_Quarters_cost
			Personal_Quarters_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_PERSONAL_QUARTERS, eArcadeID, 1)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_GARAGE, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_GARAGE, eArcadeID, 1) > Garage_cost
		AND g_sArcadeDataStruct.iGarage <= 0
			iGarage_sale_cost = Garage_cost
			Garage_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_GARAGE, eArcadeID, 1)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 0) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 0) > Neon_Lights_1_cost
		AND NOT (eOwned != eArcadeID)
			iNeon_Lights_1_sale_cost = Neon_Lights_1_cost
			Neon_Lights_1_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 0)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 1) > Neon_Lights_2_cost
			iNeon_Lights_2_sale_cost = Neon_Lights_2_cost
			Neon_Lights_2_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 1)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 2) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 2) > Neon_Lights_3_cost
			iNeon_Lights_3_sale_cost = Neon_Lights_3_cost
			Neon_Lights_3_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 2)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 3) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 3) > Neon_Lights_4_cost
			iNeon_Lights_4_sale_cost = Neon_Lights_4_cost
			Neon_Lights_4_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 3)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 4) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 4) > Neon_Lights_5_cost
			iNeon_Lights_5_sale_cost = Neon_Lights_5_cost
			Neon_Lights_5_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 4)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 5) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 5) > Neon_Lights_6_cost
			iNeon_Lights_6_sale_cost = Neon_Lights_6_cost
			Neon_Lights_6_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 5)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 6) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 6) > Neon_Lights_7_cost
			iNeon_Lights_7_sale_cost = Neon_Lights_7_cost
			Neon_Lights_7_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 6)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 7) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 7) > Neon_Lights_8_cost
			iNeon_Lights_8_sale_cost = Neon_Lights_8_cost
			Neon_Lights_8_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 7)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 8) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 8) > Neon_Lights_9_cost
			iNeon_Lights_9_sale_cost = Neon_Lights_9_cost
			Neon_Lights_9_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_NEON_LIGHTS, eArcadeID, 8)
		ENDIF
		
		IF GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_SCREENS, eArcadeID, 1) >= 0
		AND GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_SCREENS, eArcadeID, 1) > Screens_cost
		AND g_sArcadeDataStruct.iScreens <= 0
			iScreens_sale_cost = Screens_cost
			Screens_cost = GET_ARCADE_UPGRADE_BASE_COST(eARCADE_MOD_SCREENS, eArcadeID, 1)
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID + 60)											// 1	ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_ARCADE_NAME_FROM_ID(eArcadeID))					// 2	Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)														// 3	Type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)												// 4	X Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)												// 5	Y Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_ARCADE_IMAGE_FROM_ID(eArcadeID))			// 6	Texture
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_ARCADE_DESCRIPTION_FROM_ID(eArcadeID))			// 7	Description
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")													// 8	Location
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArcade_cost)											// 9	Base Cost
		
		IF eOwned != eArcadeID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_0_cost)										// 10	Floor 1 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_1_cost)											// 11	Floor 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_2_cost)											// 12	Floor 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_3_cost)											// 13	Floor 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_4_cost)											// 14	Floor 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_5_cost)											// 15	Floor 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_6_cost)											// 16	Floor 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_7_cost)											// 17	Floor 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Floor_8_cost)											// 18	Floor 9 Cost
		
		IF eOwned != eArcadeID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Ceiling_0_cost)									// 19	Ceiling 1 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Ceiling_1_cost)										// 20	Ceiling 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Ceiling_2_cost)										// 21	Ceiling 3 Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		
		IF eOwned != eArcadeID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_0_cost)										// 22	Wall 1 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_1_cost)											// 23	Wall 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_2_cost)											// 24	Wall 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_3_cost)											// 25	Wall 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_4_cost)											// 26	Wall 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_5_cost)											// 27	Wall 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_6_cost)											// 28	Wall 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_7_cost)											// 29	Wall 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Wall_8_cost)											// 30	Wall 9 Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_cost)								// 31	Personal Quarters Cost
		
		IF eOwned != eArcadeID
		AND g_sArcadeDataStruct.iGarage > 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Garage_cost)										// 32	Garage Cost
		ENDIF
		
		IF eOwned != eArcadeID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_1_cost)								// 33	Neon Lights 1 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_2_cost)									// 34	Neon Lights 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_3_cost)									// 35	Neon Lights 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_4_cost)									// 36	Neon Lights 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_5_cost)									// 37	Neon Lights 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_6_cost)									// 38	Neon Lights 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_7_cost)									// 39	Neon Lights 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_8_cost)									// 40	Neon Lights 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Neon_Lights_9_cost)									// 41	Neon Lights 9 Cost
		
		IF eOwned != eArcadeID
		AND g_sArcadeDataStruct.iScreens > 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Screens_cost)										// 42	Screens Cost
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArcade_sale_cost)												// 43	Base Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_0_sale_cost)											// 44	Floor 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_1_sale_cost)											// 45	Floor 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_2_sale_cost)											// 46	Floor 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_3_sale_cost)											// 47	Floor 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_4_sale_cost)											// 48	Floor 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_5_sale_cost)											// 49	Floor 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_6_sale_cost)											// 50	Floor 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_7_sale_cost)											// 51	Floor 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFloor_8_sale_cost)											// 52	Floor 9 Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCeiling_0_sale_cost)											// 53	Ceiling 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCeiling_1_sale_cost)											// 54	Ceiling 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCeiling_2_sale_cost)											// 55	Ceiling 3 Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_0_sale_cost)												// 56	Wall 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_1_sale_cost)												// 57	Wall 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_2_sale_cost)												// 58	Wall 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_3_sale_cost)												// 59	Wall 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_4_sale_cost)												// 60	Wall 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_5_sale_cost)												// 61	Wall 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_6_sale_cost)												// 62	Wall 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_7_sale_cost)												// 63	Wall 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_8_sale_cost)												// 64	Wall 9 Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_sale_cost)									// 65	Personal Quarters Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_sale_cost)												// 66	Garage Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_1_sale_cost)										// 67	Neon Lights 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_2_sale_cost)										// 68	Neon Lights 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_3_sale_cost)										// 69	Neon Lights 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_4_sale_cost)										// 70	Neon Lights 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_5_sale_cost)										// 71	Neon Lights 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_6_sale_cost)										// 72	Neon Lights 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_7_sale_cost)										// 73	Neon Lights 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_8_sale_cost)										// 74	Neon Lights 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNeon_Lights_9_sale_cost)										// 75	Neon Lights 9 Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScreens_sale_cost)											// 76	Screens Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC
#ENDIF

PROC POPULATE_MP_AUTO_SHOP_PROPERTY_PIN(INT &slot, INT constID, SCALEFORM_INDEX pagemov)
	AUTO_SHOP_PROPERTY_ID eAutoShopID = INT_TO_ENUM(AUTO_SHOP_PROPERTY_ID, constID)
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_AUTO_SHOP_PROPERTY_ID(eAutoShopID)
	
	VECTOR pos = GET_AUTO_SHOP_BLIP_COORDS(eSimpleInteriorID)
	
	AUTO_SHOP_PROPERTY_ID eOwned = GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID())
	
	// The Auto Shop!
	INT iAutoShop_cost 				= GET_AUTO_SHOP_PRICE(eAutoShopID)
	
	// Walls!
	INT iWall_0_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 0)
	INT iWall_1_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 1)
	INT iWall_2_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 2)
	INT iWall_3_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 3)
	INT iWall_4_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 4)
	INT iWall_5_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 5)
	INT iWall_6_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 6)
	INT iWall_7_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 7)
	INT iWall_8_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 8)
	
	// Tint!
	INT iTint_0_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 0)
	INT iTint_1_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 1)
	INT iTint_2_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 2)
	INT iTint_3_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 3)
	INT iTint_4_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 4)
	INT iTint_5_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 5)
	INT iTint_6_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 6)
	INT iTint_7_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 7)
	INT iTint_8_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 8)
	
	// Emblem!
	INT iEmblem_0_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 0)
	INT iEmblem_1_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 1)
	INT iEmblem_2_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 2)
	INT iEmblem_3_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 3)
	INT iEmblem_4_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 4)
	INT iEmblem_5_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 5)
	INT iEmblem_6_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 6)
	INT iEmblem_7_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 7)
	INT iEmblem_8_cost 				= -1 // url:bugstar:7165474 - Tuner Pack  - Remove the No Crew Emblem Option.
	
	// Upgrades!
	INT iCrew_Name_cost 			= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_CREW_NAME, eAutoShopID, 1)
	INT iStaff_One_cost 			= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_STAFF_ONE, eAutoShopID, 1)
	INT iStaff_Two_cost 			= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_STAFF_TWO, eAutoShopID, 1)
	INT iCar_Lift_cost 				= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_CAR_LIFT, eAutoShopID, 1)
	INT iPersonal_Quarters_cost 	= GET_AUTO_SHOP_UPGRADE_COST(AUTO_SHOP_MOD_PERSONAL_QUARTERS, eAutoShopID, 1)
	INT iCar_Club_Membership_cost 	= GET_CAR_CLUB_MEMBERSHIP_SALE_COST()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_AUTO_SHOP_PROPERTY_PIN slot: ", slot, ", constID: ", constID, ", eOwned: ", eOwned, ", \"", GET_AUTO_SHOP_NAME_FROM_ID(eAutoShopID), "\" $", iAutoShop_cost, ", pos: ", pos)
	
	// ----- SALE PRICES -----
	// The Auto Shop!
	INT iAutoShop_sale_cost			= -1
	
	// Walls!
	INT iWall_0_sale_cost			= -1
	INT iWall_1_sale_cost			= -1
	INT iWall_2_sale_cost			= -1
	INT iWall_3_sale_cost			= -1
	INT iWall_4_sale_cost			= -1
	INT iWall_5_sale_cost			= -1
	INT iWall_6_sale_cost 			= -1
	INT iWall_7_sale_cost 			= -1
	INT iWall_8_sale_cost 			= -1
	
	
	// Tint!
	INT iTint_0_sale_cost 				= -1
	INT iTint_1_sale_cost 				= -1
	INT iTint_2_sale_cost 				= -1
	INT iTint_3_sale_cost 				= -1
	INT iTint_4_sale_cost 				= -1
	INT iTint_5_sale_cost 				= -1
	INT iTint_6_sale_cost 				= -1
	INT iTint_7_sale_cost 				= -1
	INT iTint_8_sale_cost 				= -1
	
	// Emblem!
	INT iEmblem_0_sale_cost 				= -1
	INT iEmblem_1_sale_cost 				= -1
	INT iEmblem_2_sale_cost 				= -1
	INT iEmblem_3_sale_cost 				= -1
	INT iEmblem_4_sale_cost 				= -1
	INT iEmblem_5_sale_cost 				= -1
	INT iEmblem_6_sale_cost 				= -1
	INT iEmblem_7_sale_cost 				= -1
	INT iEmblem_8_sale_cost 				= -1
	
	// Upgrades!
	INT iCrew_Name_sale_cost 				= -1
	INT iStaff_One_sale_cost 				= -1
	INT iStaff_Two_sale_cost 				= -1
	INT iCar_Lift_sale_cost 				= -1
	INT iPersonal_Quarters_sale_cost 		= -1
	INT iCar_Club_Membership_sale_cost		= -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_AUTO_SHOP_BASE_PRICE(eAutoShopID) >= 0
		AND GET_AUTO_SHOP_BASE_PRICE(eAutoShopID) > iAutoShop_cost
			iAutoShop_sale_cost = iAutoShop_cost
			iAutoShop_cost = GET_AUTO_SHOP_BASE_PRICE(eAutoShopID)
		ENDIF
		
		// ----- Walls -----
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 0) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 0) > iWall_0_cost
		AND NOT (eOwned != eAutoShopID) // iWall_0_cost is free on initial purchase
			iWall_0_sale_cost = iWall_0_cost
			iWall_0_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 0)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 1) > iWall_1_cost
			iWall_1_sale_cost = iWall_1_cost
			iWall_1_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 2) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 2) > iWall_2_cost
			iWall_2_sale_cost = iWall_2_cost
			iWall_2_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 2)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 3) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 3) > iWall_3_cost
			iWall_3_sale_cost = iWall_3_cost
			iWall_3_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 3)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 4) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 4) > iWall_4_cost
			iWall_4_sale_cost = iWall_4_cost
			iWall_4_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 4)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 5) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 5) > iWall_5_cost
			iWall_5_sale_cost = iWall_5_cost
			iWall_5_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 5)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 6) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 6) > iWall_6_cost
			iWall_6_sale_cost = iWall_6_cost
			iWall_6_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 6)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 7) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 7) > iWall_7_cost
			iWall_7_sale_cost = iWall_7_cost
			iWall_7_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 7)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 8) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 8) > iWall_8_cost
			iWall_8_sale_cost = iWall_8_cost
			iWall_8_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_WALL, eAutoShopID, 8)
		ENDIF
		
		// ----- Tints -----
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 0) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 0) > iTint_0_cost
		AND NOT (eOwned != eAutoShopID) // iTint_0_cost is free on initial purchase
			iTint_0_sale_cost = iTint_0_cost
			iTint_0_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 0)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 1) > iTint_1_cost
			iTint_1_sale_cost = iTint_1_cost
			iTint_1_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 2) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 2) > iTint_2_cost
			iTint_2_sale_cost = iTint_2_cost
			iTint_2_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 2)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 3) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 3) > iTint_3_cost
			iTint_3_sale_cost = iTint_3_cost
			iTint_3_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 3)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 4) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 4) > iTint_4_cost
			iTint_4_sale_cost = iTint_4_cost
			iTint_4_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 4)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 5) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 5) > iTint_5_cost
			iTint_5_sale_cost = iTint_5_cost
			iTint_5_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 5)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 6) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 6) > iTint_6_cost
			iTint_6_sale_cost = iTint_6_cost
			iTint_6_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 6)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 7) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 7) > iTint_7_cost
			iTint_7_sale_cost = iTint_7_cost
			iTint_7_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 7)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 8) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 8) > iTint_8_cost
			iTint_8_sale_cost = iTint_8_cost
			iTint_8_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_TINT, eAutoShopID, 8)
		ENDIF
		
		// ----- Emblems -----
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 0) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 0) > iEmblem_0_cost
			iEmblem_0_sale_cost = iEmblem_0_cost
			iEmblem_0_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 0)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 1) > iEmblem_1_cost
			iEmblem_1_sale_cost = iEmblem_1_cost
			iEmblem_1_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 2) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 2) > iEmblem_2_cost
			iEmblem_2_sale_cost = iEmblem_2_cost
			iEmblem_2_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 2)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 3) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 3) > iEmblem_3_cost
			iEmblem_3_sale_cost = iEmblem_3_cost
			iEmblem_3_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 3)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 4) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 4) > iEmblem_4_cost
			iEmblem_4_sale_cost = iEmblem_4_cost
			iEmblem_4_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 4)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 5) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 5) > iEmblem_5_cost
			iEmblem_5_sale_cost = iEmblem_5_cost
			iEmblem_5_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 5)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 6) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 6) > iEmblem_6_cost
			iEmblem_6_sale_cost = iEmblem_6_cost
			iEmblem_6_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 6)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 7) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 7) > iEmblem_7_cost
			iEmblem_7_sale_cost = iEmblem_7_cost
			iEmblem_7_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_EMBLEM, eAutoShopID, 7)
		ENDIF
		
		// ----- Upgrades -----
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_CREW_NAME, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_CREW_NAME, eAutoShopID, 1) > iCrew_Name_cost
			iCrew_Name_sale_cost = iCrew_Name_cost
			iCrew_Name_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_CREW_NAME, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_STAFF_ONE, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_STAFF_ONE, eAutoShopID, 1) > iStaff_One_cost
			iStaff_One_sale_cost = iStaff_One_cost
			iStaff_One_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_STAFF_ONE, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_STAFF_TWO, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_STAFF_TWO, eAutoShopID, 1) > iStaff_Two_cost
			iStaff_Two_sale_cost = iStaff_Two_cost
			iStaff_Two_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_STAFF_TWO, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_CAR_LIFT, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_CAR_LIFT, eAutoShopID, 1) > iCar_Lift_cost
			iCar_Lift_sale_cost = iCar_Lift_cost
			iCar_Lift_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_CAR_LIFT, eAutoShopID, 1)
		ENDIF
		
		IF GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_PERSONAL_QUARTERS, eAutoShopID, 1) >= 0
		AND GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_PERSONAL_QUARTERS, eAutoShopID, 1) > iPersonal_Quarters_cost
			iPersonal_Quarters_sale_cost = iPersonal_Quarters_cost
			iPersonal_Quarters_cost = GET_AUTO_SHOP_UPGRADE_BASE_COST(AUTO_SHOP_MOD_PERSONAL_QUARTERS, eAutoShopID, 1)
		ENDIF
		
		IF GET_CAR_CLUB_MEMBERSHIP_BASE_COST() >= 0
		AND GET_CAR_CLUB_MEMBERSHIP_BASE_COST() > iCar_Club_Membership_cost
			iCar_Club_Membership_sale_cost = iCar_Club_Membership_cost
			iCar_Club_Membership_cost = GET_CAR_CLUB_MEMBERSHIP_BASE_COST()
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID + 70)											// 1	ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_AUTO_SHOP_NAME_FROM_ID(eAutoShopID))			// 2	Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)														// 3	Type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)												// 4	X Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)												// 5	Y Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_AUTO_SHOP_IMAGE_FROM_ID(eAutoShopID))		// 6	Texture
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_AUTO_SHOP_DESCRIPTION_FROM_ID(eAutoShopID))		// 7	Description
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")													// 8	Location / Address
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAutoShop_cost)										// 9	Base Cost
		
		IF eOwned != eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_0_cost)										// 10	Wall 1 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_1_cost)											// 11	Wall 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_2_cost)											// 12	Wall 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_3_cost)											// 13	Wall 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_4_cost)											// 14	Wall 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_5_cost)											// 15	Wall 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_6_cost)											// 16	Wall 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_7_cost)											// 17	Wall 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_8_cost)											// 18	Wall 9 Cost
		
		IF eOwned != eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_0_cost)										// 19	Tint 1 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_1_cost)											// 20	Tint 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_2_cost)											// 21	Tint 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_3_cost)											// 22	Tint 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_4_cost)											// 23	Tint 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_5_cost)											// 24	Tint 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_6_cost)											// 25	Tint 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_7_cost)											// 26	Tint 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_8_cost)											// 27	Tint 9 Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		
		IF eOwned != eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_0_cost)										// 28	Emblem 1 Cost (Crew Emblem)
		ENDIF
		
		IF eOwned != eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_1_cost)										// 29	Emblem 2 Cost (Hot Rod emblem)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_2_cost)										// 30	Emblem 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_3_cost)										// 31	Emblem 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_4_cost)										// 32	Emblem 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_5_cost)										// 33	Emblem 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_6_cost)										// 34	Emblem 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_7_cost)										// 35	Emblem 8 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_8_cost)										// 36	Emblem 9 Cost
		
		IF g_sAutoShopDataStruct.iCrewName > 0
		AND eOwned = eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrew_Name_cost)									// 37	Crew Name Cost
		ENDIF
		
		IF g_sAutoShopDataStruct.iStaff1 > 0
		AND eOwned = eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStaff_One_cost)									// 38	Staff 1 Cost
		ENDIF
		
		IF g_sAutoShopDataStruct.iStaff2 > 0
		AND eOwned = eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStaff_Two_cost)									// 39	Staff 2 Cost
		ENDIF
		
		IF g_sAutoShopDataStruct.iCarLift > 0
		AND eOwned = eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCar_Lift_cost)									// 40	Car Lift Cost
		ENDIF
		
		IF g_sAutoShopDataStruct.iPersonalQuarters > 0
		AND eOwned = eAutoShopID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_cost)							// 41	Personal Quarters Cost
		ENDIF
		
		IF g_sAutoShopDataStruct.iCarClubMembership > 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCar_Club_Membership_cost)							// 42	Membership Cost
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAutoShop_sale_cost)									// 43	Base Sale Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_0_sale_cost)										// 44	Wall 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_1_sale_cost)										// 45	Wall 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_2_sale_cost)										// 46	Wall 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_3_sale_cost)										// 47	Wall 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_4_sale_cost)										// 48	Wall 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_5_sale_cost)										// 49	Wall 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_6_sale_cost)										// 50	Wall 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_7_sale_cost)										// 51	Wall 8 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWall_8_sale_cost)										// 52	Wall 9 Sale Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_0_sale_cost)										// 53	Tint 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_1_sale_cost)										// 54	Tint 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_2_sale_cost)										// 55	Tint 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_3_sale_cost)										// 56	Tint 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_4_sale_cost)										// 57	Tint 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_5_sale_cost)										// 58	Tint 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_6_sale_cost)										// 59	Tint 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_7_sale_cost)										// 60	Tint 8 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_8_sale_cost)										// 61	Tint 9 Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_0_sale_cost)									// 62	Emblem 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_1_sale_cost)									// 63	Emblem 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_2_sale_cost)									// 64	Emblem 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_3_sale_cost)									// 65	Emblem 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_4_sale_cost)									// 66	Emblem 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_5_sale_cost)									// 67	Emblem 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_6_sale_cost)									// 68	Emblem 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_7_sale_cost)									// 69	Emblem 8 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEmblem_8_sale_cost)									// 70	Emblem 9 Sale Cost
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCrew_Name_sale_cost)									// 71	Crew Name Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStaff_One_sale_cost)									// 72	Staff 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStaff_Two_sale_cost)									// 73	Staff 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCar_Lift_sale_cost)									// 74	Car Lift Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_sale_cost)							// 75	Personal Quarters Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCar_Club_Membership_sale_cost)						// 76	Car Club Membership Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	++slot
ENDPROC

#IF FEATURE_FIXER
PROC POPULATE_MP_FIXER_HQ_PROPERTY_PIN(INT &ref_iSlot, INT constID, SCALEFORM_INDEX pagemov)
	FIXER_HQ_ID eFixerHQID = INT_TO_ENUM(FIXER_HQ_ID, constID)
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_FIXER_HQ_ID(eFixerHQID)
	
	VECTOR pos = GET_FIXER_HQ_BLIP_COORDS(eSimpleInteriorID)
	FIXER_HQ_ID eOwned = GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID())
	
	// The Fixer HQ!
	INT iFixerHQ_cost 				= GET_FIXER_HQ_COST(eFixerHQID)
	
	// Art!
	INT iArt_0_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 0)
	INT iArt_1_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 1)
	INT iArt_2_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 2)
	
	// Wallpaper!
	INT iWallpaper_0_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 0)
	INT iWallpaper_1_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 1)
	INT iWallpaper_2_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 2)
	INT iWallpaper_3_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 3)
	INT iWallpaper_4_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 4)
	INT iWallpaper_5_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 5)
	INT iWallpaper_6_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 6)
	INT iWallpaper_7_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 7)
	INT iWallpaper_8_cost 			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 8)
	
	// Tint!
	INT iTint_0_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 0)
	INT iTint_1_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 1)
	INT iTint_2_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 2)
	INT iTint_3_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 3)
	INT iTint_4_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 4)
	INT iTint_5_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 5)
	INT iTint_6_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 6)
	INT iTint_7_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 7)
	INT iTint_8_cost 				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 8)
	
	// Upgrades!
	INT iVehWorkshop_cost			= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_VEH_WORKSHOP, eFixerHQID, 1)
	INT iArmory_cost				= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_ARMORY, eFixerHQID, 1)
	INT iPersonalQuarters_cost		= GET_FIXER_HQ_UPGRADE_COST(FIXER_HQ_UPGRADE_PERSONAL_QUARTERS, eFixerHQID, 1)
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_FIXER_HQ_PROPERTY_PIN slot: ", ref_iSlot, ", constID: ", constID, ", eOwned: ", eOwned, ", \"", GET_FIXER_HQ_NAME_FROM_ID(eFixerHQID), "\" $", iFixerHQ_Cost, ", pos: ", pos)
	
	// ----- SALE PRICES -----
	
	// The Fixer HQ!
	INT iFixerHQ_sale_cost 				= -1
	
	// Art!
	INT iArt_0_sale_cost 				= -1
	INT iArt_1_sale_cost 				= -1
	INT iArt_2_sale_cost 				= -1
	
	// Wallpaper!
	INT iWallpaper_0_sale_cost 			= -1
	INT iWallpaper_1_sale_cost 			= -1
	INT iWallpaper_2_sale_cost 			= -1
	INT iWallpaper_3_sale_cost 			= -1
	INT iWallpaper_4_sale_cost 			= -1
	INT iWallpaper_5_sale_cost 			= -1
	INT iWallpaper_6_sale_cost 			= -1
	INT iWallpaper_7_sale_cost 			= -1
	INT iWallpaper_8_sale_cost 			= -1
	
	// Tint!
	INT iTint_0_sale_cost 				= -1
	INT iTint_1_sale_cost 				= -1
	INT iTint_2_sale_cost 				= -1
	INT iTint_3_sale_cost 				= -1
	INT iTint_4_sale_cost 				= -1
	INT iTint_5_sale_cost 				= -1
	INT iTint_6_sale_cost 				= -1
	INT iTint_7_sale_cost 				= -1
	INT iTint_8_sale_cost 				= -1
	
	// Upgrades!
	INT iVehWorkshop_sale_cost			= -1
	INT iArmory_sale_cost				= -1
	INT iPersonalQuarters_sale_cost		= -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_FIXER_HQ_BASE_COST(eFixerHQID) >= 0
		AND GET_FIXER_HQ_BASE_COST(eFixerHQID) > iFixerHQ_cost
			iFixerHQ_sale_cost = iFixerHQ_cost
			iFixerHQ_cost = GET_FIXER_HQ_BASE_COST(eFixerHQID)
		ENDIF
		
		// ----- ART -----
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 0) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 0) > iArt_0_cost
		AND NOT (eOwned != eFixerHQID) // iArt_0_cost is free on initial purchase
			iArt_0_sale_cost = iArt_0_cost
			iArt_0_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 0)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 1) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 1) > iArt_1_cost
			iArt_1_sale_cost = iArt_1_cost
			iArt_1_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 1)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 2) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 2) > iArt_2_cost
			iArt_2_sale_cost = iArt_2_cost
			iArt_2_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ART, eFixerHQID, 2)
		ENDIF
		
		// ----- WALLPAPER -----
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 0) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 0) > iWallpaper_0_cost
		AND NOT (eOwned != eFixerHQID) // iWallpaper_0_cost is free on initial purchase
			iWallpaper_0_sale_cost = iWallpaper_0_cost
			iWallpaper_0_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 0)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 1) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 1) > iWallpaper_1_cost
			iWallpaper_1_sale_cost = iWallpaper_1_cost
			iWallpaper_1_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 1)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 2) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 2) > iWallpaper_2_cost
			iWallpaper_2_sale_cost = iWallpaper_2_cost
			iWallpaper_2_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 2)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 3) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 3) > iWallpaper_3_cost
			iWallpaper_3_sale_cost = iWallpaper_3_cost
			iWallpaper_3_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 3)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 4) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 4) > iWallpaper_4_cost
			iWallpaper_4_sale_cost = iWallpaper_4_cost
			iWallpaper_4_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 4)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 5) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 5) > iWallpaper_5_cost
			iWallpaper_5_sale_cost = iWallpaper_5_cost
			iWallpaper_5_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 5)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 6) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 6) > iWallpaper_6_cost
			iWallpaper_6_sale_cost = iWallpaper_6_cost
			iWallpaper_6_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 6)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 7) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 7) > iWallpaper_7_cost
			iWallpaper_7_sale_cost = iWallpaper_7_cost
			iWallpaper_7_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 7)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 8) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 8) > iWallpaper_8_cost
			iWallpaper_8_sale_cost = iWallpaper_8_cost
			iWallpaper_8_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_WALLPAPER, eFixerHQID, 8)
		ENDIF
		
		// ----- TINTS -----
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 0) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 0) > iTint_0_cost
		AND NOT (eOwned != eFixerHQID) // iTint_0_cost is free on initial purchase
			iTint_0_sale_cost = iTint_0_cost
			iTint_0_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 0)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 1) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 1) > iTint_1_cost
			iTint_1_sale_cost = iTint_1_cost
			iTint_1_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 1)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 2) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 2) > iTint_2_cost
			iTint_2_sale_cost = iTint_2_cost
			iTint_2_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 2)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 3) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 3) > iTint_3_cost
			iTint_3_sale_cost = iTint_3_cost
			iTint_3_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 3)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 4) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 4) > iTint_4_cost
			iTint_4_sale_cost = iTint_4_cost
			iTint_4_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 4)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 5) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 5) > iTint_5_cost
			iTint_5_sale_cost = iTint_5_cost
			iTint_5_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 5)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 6) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 6) > iTint_6_cost
			iTint_6_sale_cost = iTint_6_cost
			iTint_6_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 6)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 7) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 7) > iTint_7_cost
			iTint_7_sale_cost = iTint_7_cost
			iTint_7_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 7)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 8) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 8) > iTint_8_cost
			iTint_8_sale_cost = iTint_8_cost
			iTint_8_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_TINT, eFixerHQID, 8)
		ENDIF
		
		// ----- Upgrades -----
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_VEH_WORKSHOP, eFixerHQID, 1) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_VEH_WORKSHOP, eFixerHQID, 1) > iVehWorkshop_cost
			iVehWorkshop_sale_cost = iVehWorkshop_cost
			iVehWorkshop_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_VEH_WORKSHOP, eFixerHQID, 1)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ARMORY, eFixerHQID, 1) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ARMORY, eFixerHQID, 1) > iArmory_cost
			iArmory_sale_cost = iArmory_cost
			iArmory_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_ARMORY, eFixerHQID, 1)
		ENDIF
		
		IF GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_PERSONAL_QUARTERS, eFixerHQID, 1) >= 0
		AND GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_PERSONAL_QUARTERS, eFixerHQID, 1) > iPersonalQuarters_cost
			iPersonalQuarters_sale_cost = iPersonalQuarters_cost
			iPersonalQuarters_cost = GET_FIXER_HQ_UPGRADE_BASE_COST(FIXER_HQ_UPGRADE_PERSONAL_QUARTERS, eFixerHQID, 1)
		ENDIF
	ENDIF
	
	// https://hub.gametools.dev/display/RSGGTAV/dynasty8executiverealty.com#dynasty8executiverealty.com-DataSlots
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(constID + 20)											// 1	ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_FIXER_HQ_NAME_FROM_ID(eFixerHQID))				// 2	Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)														// 3	Type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.x)												// 4	X Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(pos.y)												// 5	Y Position
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_FIXER_HQ_IMAGE_FROM_ID(eFixerHQID))		// 6	Texture
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_FIXER_HQ_DESCRIPTION_FROM_ID(eFixerHQID))		// 7	Description
	END_SCALEFORM_MOVIE_METHOD()
	
	// ----- Cost, Art and wallpaper -----
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFixerHQ_cost)											// 8	Base Cost
		
		IF eOwned != eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArt_0_cost)										// 9	Art 0 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArt_1_cost)											// 10	Art 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArt_2_cost)											// 11	Art 2 Cost
		
		IF eOwned != eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_0_cost)									// 12	Wallpaper 0 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_1_cost)										// 13	Wallpaper 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_2_cost)										// 14	Wallpaper 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_3_cost)										// 15	Wallpaper 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_4_cost)										// 16	Wallpaper 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_5_cost)										// 17	Wallpaper 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_6_cost)										// 18	Wallpaper 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_7_cost)										// 19	Wallpaper 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_8_cost)										// 20	Wallpaper 8 Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	// ----- Tints -----
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		
		IF eOwned != eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_0_cost)										// 21	Tint 0 Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_1_cost)											// 22	Tint 1 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_2_cost)											// 23	Tint 2 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_3_cost)											// 24	Tint 3 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_4_cost)											// 25	Tint 4 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_5_cost)											// 26	Tint 5 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_6_cost)											// 27	Tint 6 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_7_cost)											// 28	Tint 7 Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_8_cost)											// 29	Tint 8 Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	// ----- Upgrades -----
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		
		IF g_sFixerHQDataStruct.iVehWorkshop > 0
		AND eOwned = eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehWorkshop_cost)									// 30	Veh Workshop Cost
		ENDIF
		
		IF g_sFixerHQDataStruct.iArmory > 0
		AND eOwned = eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArmory_cost)										// 31	Armory Cost
		ENDIF
		
		IF g_sFixerHQDataStruct.iPersonalQuarters > 0
		AND eOwned = eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonalQuarters_cost)							// 32	Perrsonal Quarters Cost
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	// ----- SALE COST -----
	
	// ----- Cost, Art and wallpaper -----
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFixerHQ_sale_cost)									// 33	Base Sale Cost
		
		IF eOwned != eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArt_0_sale_cost)									// 34	Art 0 Sale Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArt_1_sale_cost)										// 35	Art 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArt_2_sale_cost)										// 36	Art 2 Sale Cost
		
		IF eOwned != eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_0_sale_cost)							// 37	Wallpaper 0 Sale Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_1_sale_cost)								// 38	Wallpaper 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_2_sale_cost)								// 39	Wallpaper 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_3_sale_cost)								// 40	Wallpaper 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_4_sale_cost)								// 41	Wallpaper 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_5_sale_cost)								// 42	Wallpaper 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_6_sale_cost)								// 43	Wallpaper 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_7_sale_cost)								// 44	Wallpaper 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWallpaper_8_sale_cost)								// 45	Wallpaper 8 Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	// ----- Tints -----
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		
		IF eOwned != eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_0_sale_cost)									// 46	Tint 0 Sale Cost
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_1_sale_cost)										// 47	Tint 1 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_2_sale_cost)										// 48	Tint 2 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_3_sale_cost)										// 49	Tint 3 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_4_sale_cost)										// 50	Tint 4 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_5_sale_cost)										// 51	Tint 5 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_6_sale_cost)										// 52	Tint 6 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_7_sale_cost)										// 53	Tint 7 Sale Cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTint_8_sale_cost)										// 54	Tint 8 Sale Cost
	END_SCALEFORM_MOVIE_METHOD()
	
	// ----- Upgrades -----
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ref_iSlot)
		
		IF g_sFixerHQDataStruct.iVehWorkshop > 0
		AND eOwned = eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehWorkshop_sale_cost)							// 55	Veh Workshop Sale Cost
		ENDIF
		
		IF g_sFixerHQDataStruct.iArmory > 0
		AND eOwned = eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArmory_sale_cost)									// 56	Armory Sale Cost
		ENDIF
		
		IF g_sFixerHQDataStruct.iPersonalQuarters > 0
		AND eOwned = eFixerHQID
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonalQuarters_sale_cost)						// 57	Personal Quarters Sale Cost
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	++ref_iSlot
ENDPROC
#ENDIF

PROC POPULATE_MP_PROPERTY_SITE_PIN_MAP(SCALEFORM_INDEX pagemov)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
	INT iSlot = 0
	INT i
	INT iOwned[MAX_OWNED_PROPERTIES]
	REPEAT MAX_OWNED_PROPERTIES i
		iOwned[i] = GET_OWNED_PROPERTY(i)
		CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_PROPERTY_SITE_PIN_MAP already owned is : ", iOwned[i])
	ENDREPEAT

	INT scaleformIndex = 0
	FOR iSlot = 1 TO MAX_MP_PROPERTIES
		BOOL doUpdate = TRUE
		IF doUpdate
			IF IS_PROPERTY_SALE_BLOCKED_BY_TUNEABLES(iSlot)
				CPRINTLN(DEBUG_INTERNET, "DONT_POPULATE_MP_PROPERTY_PIN IS_PROPERTY_SALE_BLOCKED_BY_TUNEABLES(", iSlot, ")")
				doUpdate = FALSE
			ENDIF
		ENDIF
		IF doUpdate
			IF iSlot = PROPERTY_YACHT_APT_1_BASE
			OR IS_PROPERTY_OFFICE(iSlot)
			OR IS_PROPERTY_CLUBHOUSE(iSlot)
			OR IS_PROPERTY_OFFICE_GARAGE(iSlot)
			OR iSlot = PROPERTY_IMPEXP_VEH_WAREHOUSE
			OR iSlot = PROPERTY_HANGAR
			OR iSlot = PROPERTY_DEFUNC_BASE
			OR iSlot = PROPERTY_NIGHTCLUB
			OR IS_PROPERTY_MEGAWARE_GARAGE(iSlot)
			OR IS_PROPERTY_ARENAWARS_GARAGE(iSlot)
			OR iSlot = PROPERTY_CASINO_GARAGE
			OR iSlot = PROPERTY_ARCADE_GARAGE
				CDEBUG1LN(DEBUG_INTERNET, "DONT_POPULATE_MP_PROPERTY_PIN: ", iSlot,
						 PICK_STRING(iSlot = PROPERTY_YACHT_APT_1_BASE,		", is yacht", "")
						,PICK_STRING(IS_PROPERTY_OFFICE(iSlot),				", is office", "")
						,PICK_STRING(IS_PROPERTY_CLUBHOUSE(iSlot),			", is clubhouse", "")
						,PICK_STRING(IS_PROPERTY_OFFICE_GARAGE(iSlot),		", is office garage", "")
						,PICK_STRING(iSlot = PROPERTY_IMPEXP_VEH_WAREHOUSE,	", is imp-exp veh warehouse ", "")
						,PICK_STRING(iSlot = PROPERTY_HANGAR,				", is hangar ", "")
						,PICK_STRING(iSlot = PROPERTY_DEFUNC_BASE,			", is defunc base ", "")
						,PICK_STRING(iSlot = PROPERTY_NIGHTCLUB,			", is nightclub ", "")
						,PICK_STRING(IS_PROPERTY_MEGAWARE_GARAGE(iSlot),	", is megaware garage", "")
						,PICK_STRING(IS_PROPERTY_ARENAWARS_GARAGE(iSlot),	", is arenawars garage", "")
						,PICK_STRING(iSlot = PROPERTY_CASINO_GARAGE,		", is casino garage ", "")
						,PICK_STRING(iSlot = PROPERTY_ARCADE_GARAGE,		", is arcade garage ", "")
						)
				doUpdate = FALSE
			ENDIF
		ENDIF
		IF doUpdate
			i = 0
			REPEAT MAX_OWNED_PROPERTIES i //CDM may need to ignore office here??
				IF iOwned[i] != 0
					IF iOwned[i] = iSlot
						IF doUpdate
							CDEBUG1LN(DEBUG_INTERNET, "DONT_POPULATE_MP_PROPERTY_PIN iSlot = iOwned[", i, "]")
							doUpdate = FALSE
							i = MAX_OWNED_PROPERTIES
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		// B*7007833
		IF IS_STRING_NULL_OR_EMPTY(mpProperties[iSlot].tl_PropertyName)
			PRINTLN("POPULATE_MP_PROPERTY_SITE_PIN_MAP - Data is invalid for slot: ", iSlot, ". Do not update!")
			doUpdate = FALSE
		ENDIF
		
		IF doUpdate
			POPULATE_MP_PROPERTY_PIN(scaleformIndex,iSlot,pagemov)
		ENDIF
	ENDFOR
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC INT GET_BOOL_AS_INT_FOR_SCALEFORM(BOOl bState)
	IF bState
		RETURN 1
	ENDIF
	
	RETURN 0
ENDFUNC

PROC SETUP_OFFICE_GLOBALS_FROM_STATS()
	INT iOwned = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0)
	
	IF iOwned != 0
		g_sOfficeDataStruct.iOfficeID 		= iOwned
		g_sOfficeDataStruct.iStyle			= GET_SF_FROM_OFFICE_INTERIOR(GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_0))
		g_sOfficeDataStruct.iPersonnel		= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_OFFICE_PA_MALE(FALSE))
		g_sOfficeDataStruct.iFont			= GET_MP_INT_CHARACTER_STAT(MP_STAT_FONT_PLAYER_OFFICE)
		g_sOfficeDataStruct.iColour			= GET_MP_INT_CHARACTER_STAT(MP_STAT_COLOUR_PLAYER_OFFICE)
		g_sOfficeDataStruct.tl63OldOrgName	= GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_GB_GANG_NAME, MP_STAT_GB_GANG_NAME2)
		
		IF IS_STRING_NULL_OR_EMPTY(g_sOfficeDataStruct.tl63OldOrgName)
		OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			g_sOfficeDataStruct.tl63OldOrgName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_GB_OFFICE_NAME, MP_STAT_GB_OFFICE_NAME2)
			IF IS_STRING_NULL_OR_EMPTY(g_sOfficeDataStruct.tl63OldOrgName)
				g_sOfficeDataStruct.tl63OldOrgName = GET_DEFAULT_GANG_NAME(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		g_sOfficeDataStruct.iGunLocker				= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_OFFICE_GUN_LOCKER_PURCHASED())
		g_sOfficeDataStruct.iVault					= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_OFFICE_CASH_VAULT_PURCHASED())
		g_sOfficeDataStruct.iAccommodation			= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_OFFICE_ACCOMMODATION_PURCHASED())
		g_sOfficeDataStruct.iOfficeNameID			= GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_NAME_ID)
		
		IF IS_OFFICE_GARAGE_1_PURCHASED()
			g_sOfficeDataStruct.iGarage1Style		= GET_SF_FROM_OFFICE_GARAGE_INTERIOR(GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1))
			g_sOfficeDataStruct.iGarage1Lighting	= GET_SF_FROM_OFFICE_GARAGE_LIGHTING(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_LIGHTING))
			g_sOfficeDataStruct.iGarage1Number		= GET_SF_FROM_OFFICE_GARAGE_NUMBERING(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1_NUMBERING))
		ELSE
			g_sOfficeDataStruct.iGarage1Style		= -1
			g_sOfficeDataStruct.iGarage1Lighting	= -1
			g_sOfficeDataStruct.iGarage1Number		= -1
		ENDIF
		IF IS_OFFICE_GARAGE_2_PURCHASED()
			g_sOfficeDataStruct.iGarage2Style		= GET_SF_FROM_OFFICE_GARAGE_INTERIOR(GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2))
			g_sOfficeDataStruct.iGarage2Lighting	= GET_SF_FROM_OFFICE_GARAGE_LIGHTING(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_LIGHTING))
			g_sOfficeDataStruct.iGarage2Number		= GET_SF_FROM_OFFICE_GARAGE_NUMBERING(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2_NUMBERING))
		ELSE
			g_sOfficeDataStruct.iGarage2Style		= -1
			g_sOfficeDataStruct.iGarage2Lighting	= -1
			g_sOfficeDataStruct.iGarage2Number		= -1
		ENDIF
		IF IS_OFFICE_GARAGE_3_PURCHASED()
			g_sOfficeDataStruct.iGarage3Style		= GET_SF_FROM_OFFICE_GARAGE_INTERIOR(GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3))
			g_sOfficeDataStruct.iGarage3Lighting	= GET_SF_FROM_OFFICE_GARAGE_LIGHTING(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_LIGHTING))
			g_sOfficeDataStruct.iGarage3Number		= GET_SF_FROM_OFFICE_GARAGE_NUMBERING(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3_NUMBERING))
		ELSE
			g_sOfficeDataStruct.iGarage3Style		= -1
			g_sOfficeDataStruct.iGarage3Lighting	= -1
			g_sOfficeDataStruct.iGarage3Number		= -1
		ENDIF
		IF IS_OFFICE_MODSHOP_PURCHASED()
			g_sOfficeDataStruct.iModshop			= GET_SF_FROM_OFFICE_MODSHOP_INTERIOR(GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_MODSHOP))
		ELSE
			g_sOfficeDataStruct.iModshop			= -1
		ENDIF
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_OFFICE_GLOBALS_FROM_STATS office ", iOwned, " owned \"", g_sOfficeDataStruct.tl63OldOrgName, "\" (slot ", GET_ACTIVE_CHARACTER_SLOT(), ")")
		CPRINTLN(DEBUG_INTERNET, "	iStyle:", g_sOfficeDataStruct.iStyle,
				PICK_STRING(g_sOfficeDataStruct.iPersonnel = 0, ", iPersonnel:FEMALE", ", iPersonnel:MALE"),
				", iFont:", g_sOfficeDataStruct.iFont,
				", iColour:", g_sOfficeDataStruct.iColour,
				PICK_STRING(g_sOfficeDataStruct.iGunLocker = 0, ", iGunLocker:FALSE", ", iGunLocker:TRUE"),
				PICK_STRING(g_sOfficeDataStruct.iVault = 0, ", iVault:FALSE", ", iVault:TRUE"),
				PICK_STRING(g_sOfficeDataStruct.iAccommodation = 0, ", iAccommodation:FALSE", ", iAccommodation:TRUE"),
				", iOfficeNameID:", g_sOfficeDataStruct.iOfficeNameID)
		CPRINTLN(DEBUG_INTERNET, "	iGarage1Style:", g_sOfficeDataStruct.iGarage1Style, ", iGarage1Lighting:", g_sOfficeDataStruct.iGarage1Lighting, ", iGarage1Number:", g_sOfficeDataStruct.iGarage1Number,
		   		", iGarage2Style:", g_sOfficeDataStruct.iGarage2Style, ", iGarage2Lighting:", g_sOfficeDataStruct.iGarage2Lighting, ", iGarage2Number:", g_sOfficeDataStruct.iGarage2Number)
		CPRINTLN(DEBUG_INTERNET, "	iGarage3Style:", g_sOfficeDataStruct.iGarage3Style, ", iGarage3Lighting:", g_sOfficeDataStruct.iGarage3Lighting, ", iGarage3Number:", g_sOfficeDataStruct.iGarage3Number,
				", iModshop:", g_sOfficeDataStruct.iModshop)
	ELSE
		g_sOfficeDataStruct.iOfficeID 		= -1
		g_sOfficeDataStruct.iStyle			= -1
		g_sOfficeDataStruct.iPersonnel		= -1
		g_sOfficeDataStruct.iFont			= -1
		g_sOfficeDataStruct.iColour			= -1
		
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			g_sOfficeDataStruct.tl63OldOrgName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_GB_OFFICE_NAME, MP_STAT_GB_OFFICE_NAME2)
		
			IF IS_STRING_NULL_OR_EMPTY(g_sOfficeDataStruct.tl63OldOrgName)
				g_sOfficeDataStruct.tl63OldOrgName = GET_DEFAULT_GANG_NAME(PLAYER_ID(), TRUE)
			ENDIF
		ELSE
			g_sOfficeDataStruct.tl63OldOrgName	= GET_DEFAULT_GANG_NAME(PLAYER_ID(), TRUE)
		ENDIF
		
		g_sOfficeDataStruct.tl63NewOrgName 		= ""
		g_sOfficeDataStruct.iGunLocker			= -1
		g_sOfficeDataStruct.iVault				= -1
		g_sOfficeDataStruct.iAccommodation		= -1
		g_sOfficeDataStruct.iOfficeNameID		= 0
		
		g_sOfficeDataStruct.iGarage1Style		= -1
		g_sOfficeDataStruct.iGarage1Lighting	= -1
		g_sOfficeDataStruct.iGarage1Number		= -1
		g_sOfficeDataStruct.iGarage2Style		= -1
		g_sOfficeDataStruct.iGarage2Lighting	= -1
		g_sOfficeDataStruct.iGarage2Number		= -1
		g_sOfficeDataStruct.iGarage3Style		= -1
		g_sOfficeDataStruct.iGarage3Lighting	= -1
		g_sOfficeDataStruct.iGarage3Number		= -1
		
		g_sOfficeDataStruct.iModshop			= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_OFFICE_GLOBALS_FROM_STATS none owned \"", g_sOfficeDataStruct.tl63OldOrgName, "\"")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_EXEC_OFFICE(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	IF bReInitGlobals
		SETUP_OFFICE_GLOBALS_FROM_STATS()
	ENDIF
	
	//Set data for slot 0(Existing properties)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iOfficeID)					//Property ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iStyle)					//Style
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iPersonnel)				//PA (MALE OR FEMALE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iFont)						//Font
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iColour)					//Colour
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sOfficeDataStruct.tl63OldOrgName)	//Company Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sOfficeDataStruct.tl63NewOrgName) //String for name change
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGunLocker)				//Gun Locker
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iVault)					//Vault
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iAccommodation)			//Accommodation
		LANGUAGE_TYPE eLang = GET_CURRENT_LANGUAGE()
		IF eLang = LANGUAGE_POLISH
		OR eLang = LANGUAGE_RUSSIAN
		OR eLang = LANGUAGE_KOREAN
		OR eLang = LANGUAGE_CHINESE
		OR eLang = LANGUAGE_JAPANESE
		OR eLang = LANGUAGE_CHINESE_SIMPLIFIED

			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)									//Fonts Supported
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)										//Fonts Supported
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		IF g_sOfficeDataStruct.iGarage1Style != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)											//Garage 1 Equipped
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage1Style)			//Garage 1 Style
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage1Lighting)		//Garage 1 Lighting
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage1Number)		//Garage 1 Floor Number
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ENDIF
		IF g_sOfficeDataStruct.iGarage2Style != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)											//Garage 2 Equipped
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage2Style)			//Garage 2 Style
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage2Lighting)		//Garage 2 Lighting
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage2Number)		//Garage 2 Floor Number
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ENDIF
		IF g_sOfficeDataStruct.iGarage3Style != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)											//Garage 3 Equipped
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage3Style)			//Garage 3 Style
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage3Lighting)		//Garage 3 Lighting
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iGarage3Number)		//Garage 3 Floor Number
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ENDIF
		
		IF g_sOfficeDataStruct.iModshop != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)											//Modshop Equipped
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sOfficeDataStruct.iModshop)				//Mod Shop Style
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_EXEC_OFFICE already owned is:", g_sOfficeDataStruct.iOfficeID)
	
ENDPROC

PROC SETUP_FIXER_HQ_GLOBALS_FROM_STATS()
	FIXER_HQ_ID eOwned = GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID())
	
	IF IS_FIXER_HQ_ID_VALID(eOwned)
		g_sFixerHQDataStruct.iAgencyID				= ENUM_TO_INT(eOwned)
		g_sFixerHQDataStruct.iArt					= GET_MP_INT_CHARACTER_STAT(GET_MP_INT_STAT_FOR_FIXER_HQ_UPGRADE(FIXER_HQ_UPGRADE_ART))
		g_sFixerHQDataStruct.iWallpaper				= GET_MP_INT_CHARACTER_STAT(GET_MP_INT_STAT_FOR_FIXER_HQ_UPGRADE(FIXER_HQ_UPGRADE_WALLPAPER))
		g_sFixerHQDataStruct.iTint					= GET_MP_INT_CHARACTER_STAT(GET_MP_INT_STAT_FOR_FIXER_HQ_UPGRADE(FIXER_HQ_UPGRADE_TINT))
		g_sFixerHQDataStruct.iVehWorkshop			= GET_MP_INT_CHARACTER_STAT(GET_MP_INT_STAT_FOR_FIXER_HQ_UPGRADE(FIXER_HQ_UPGRADE_VEH_WORKSHOP))
		g_sFixerHQDataStruct.iArmory				= GET_MP_INT_CHARACTER_STAT(GET_MP_INT_STAT_FOR_FIXER_HQ_UPGRADE(FIXER_HQ_UPGRADE_ARMORY))
		g_sFixerHQDataStruct.iPersonalQuarters		= GET_MP_INT_CHARACTER_STAT(GET_MP_INT_STAT_FOR_FIXER_HQ_UPGRADE(FIXER_HQ_UPGRADE_PERSONAL_QUARTERS))
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_FIXER_HQ_GLOBALS_FROM_STATS ", eOwned," owned, iAgencyID: ", g_sFixerHQDataStruct.iAgencyID)
	ELSE
		g_sFixerHQDataStruct.iAgencyID				= -1
		g_sFixerHQDataStruct.iArt					= -1
		g_sFixerHQDataStruct.iWallpaper				= -1
		g_sFixerHQDataStruct.iTint					= -1
		g_sFixerHQDataStruct.iVehWorkshop			= -1
		g_sFixerHQDataStruct.iArmory				= -1
		g_sFixerHQDataStruct.iPersonalQuarters		= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_FIXER_HQ_GLOBALS_FROM_STATS none owned")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_FIXER_HQ(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	IF bReInitGlobals
		SETUP_FIXER_HQ_GLOBALS_FROM_STATS()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)												// Slot
		
		IF (g_sFixerHQDataStruct.iAgencyID = -1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iAgencyID + 20)		// 1 ID
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iArt)						// 2 Art
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iWallpaper)				// 3 Wallpaper
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iTint)					// 4 Tint
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iVehWorkshop)				// 5 Veh Workshop
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iArmory)					// 6 Armory
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sFixerHQDataStruct.iPersonalQuarters)		// 7 Personal Quarters
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_FIXER_HQ already owned is:", g_sFixerHQDataStruct.iAgencyID)
ENDPROC

#IF FEATURE_DLC_2_2022
PROC SETUP_MULTISTOREY_GARAGE_GLOBALS_FROM_STATS()
MULTISTOREY_GARAGE_ID eOwned = GET_PLAYERS_OWNED_MULTISTOREY_GARAGE(PLAYER_ID())
	
	IF IS_MULTISTOREY_GARAGE_ID_VALID(eOwned)
		g_sMultistoreyGarageDataStruct.iGarageID	= ENUM_TO_INT(eOwned)
		g_sMultistoreyGarageDataStruct.iStyle		= GET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_STYLE_STAT(MULTISTOREY_GARAGE_FLOOR_1))
		g_sMultistoreyGarageDataStruct.iTint		= GET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_TINT_STAT(MULTISTOREY_GARAGE_FLOOR_1))
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_MULTISTOREY_GARAGE_GLOBALS_FROM_STATS ", eOwned," owned, iGarageID: ", g_sMultistoreyGarageDataStruct.iGarageID)
	ELSE
		g_sMultistoreyGarageDataStruct.iGarageID	= -1
		g_sMultistoreyGarageDataStruct.iStyle		= -1
		g_sMultistoreyGarageDataStruct.iTint		= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_MULTISTOREY_GARAGE_GLOBALS_FROM_STATS none owned")
	ENDIF
ENDPROC
#ENDIF

PROC PRINT_HELP_FOR_WEBSITE_DYNASTY_8_EXECUTIVE()
	// --- FIXER HQ ---
	#IF FEATURE_FIXER
	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PRINTED_BUY_AGENCY_HELP_TEXT)
	AND NOT	DOES_PLAYER_OWN_ANY_FIXER_HQ(PLAYER_ID())
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FXR_WEB_HELP")
			PRINT_HELP("FXR_WEB_HELP")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PRINTED_BUY_AGENCY_HELP_TEXT, TRUE)
		ENDIF
	ELIF DOES_PLAYER_OWN_ANY_FIXER_HQ(PLAYER_ID())
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PRINTED_BUY_AGENCY_HELP_TEXT)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PRINTED_BUY_AGENCY_HELP_TEXT, TRUE)
		ENDIF
	ENDIF
	#ENDIF
ENDPROC

PROC POPULATE_MP_EXEC_PROPERTY_SITE_PIN_MAP(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
	POPULATE_OWNED_EXEC_OFFICE(pagemov, bReInitGlobals)
	
	INT scaleformIndex = 3
	INT i
	FOR i = PROPERTY_OFFICE_1 TO PROPERTY_OFFICE_4
		IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0) = i
			CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_EXEC_PROPERTY_SITE_PIN_MAP populating for owned propety: ", i, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
			POPULATE_MP_EXEC_PROPERTY_PIN(scaleformIndex,i,pagemov)
		ELSE
			IF !g_sMPTunables.bexec_disable_office_purchase
				CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_EXEC_PROPERTY_SITE_PIN_MAP populating for propety: ", i, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
				POPULATE_MP_EXEC_PROPERTY_PIN(scaleformIndex,i,pagemov)
			ENDIF	
		ENDIF	
	ENDFOR
	
	#IF FEATURE_FIXER
	IF CAN_PLAYER_PURCHASE_FIXER_HQ()
		POPULATE_OWNED_FIXER_HQ(pagemov, bReInitGlobals)
		
		scaleformIndex = 8 // https://hub.gametools.dev/display/RSGGTAV/dynasty8executiverealty.com#dynasty8executiverealty.com-DataSlots
		
		INT iFixerHQ
		REPEAT FIXER_HQ_ID_COUNT iFixerHQ
			IF iFixerHQ != ENUM_TO_INT(FIXER_HQ_ID_INVALID)
				CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_EXEC_PROPERTY_SITE_PIN_MAP populating for Fixer HQ: ", iFixerHQ)
				
				IF GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID()) = INT_TO_ENUM(FIXER_HQ_ID, iFixerHQ)
					POPULATE_MP_FIXER_HQ_PROPERTY_PIN(scaleformIndex, iFixerHQ, pagemov)
				ELSE
					IF NOT IS_FIXER_HQ_PURCHASE_DISABLED(INT_TO_ENUM(FIXER_HQ_ID, iFixerHQ))
						POPULATE_MP_FIXER_HQ_PROPERTY_PIN(scaleformIndex, iFixerHQ, pagemov)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SETUP_CLUBHOUSE_GLOBALS_FROM_STATS()
	INT iOwned = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE)
	
	IF iOwned != 0
		g_sClubhouseDataStruct.iClubhouseID 	= iOwned
		g_sClubhouseDataStruct.iWall			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_WALL)
		g_sClubhouseDataStruct.iHanging			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_HANGING)
		g_sClubhouseDataStruct.iFurniture		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_FURNATURE)
		g_sClubhouseDataStruct.iColourScheme	= GET_SF_FROM_CLUBHOUSE_INTERIOR(iOwned, GET_OWNED_PROPERTY_VARIATION(PROPERTY_OWNED_SLOT_CLUBHOUSE))
		g_sClubhouseDataStruct.iFont			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_FONT)
		g_sClubhouseDataStruct.iFontColour		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_COLOUR)
		g_sClubhouseDataStruct.iEmblem			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_EMBLEM)
		g_sClubhouseDataStruct.iHideSinage		= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_CLUBHOUSE_SINAGEHIDE_PURCHASED())
		
		g_sClubhouseDataStruct.tl63OldSignage	= GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(PLAYER_ID())
		IF IS_STRING_NULL_OR_EMPTY(g_sClubhouseDataStruct.tl63OldSignage)
		OR GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			g_sClubhouseDataStruct.tl63OldSignage = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_MC_CLBHOSE_NAME, MP_STAT_MC_CLBHOSE_NAME2)
			IF IS_STRING_NULL_OR_EMPTY(g_sClubhouseDataStruct.tl63OldSignage)
				g_sClubhouseDataStruct.tl63OldSignage = GET_DEFAULT_BIKER_GANG_NAME()
			ENDIF
		ENDIF
		
		TEXT_LABEL_63 stName
		stName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_MC_GANG_NAME, MP_STAT_MC_GANG_NAME2)
		
		IF IS_STRING_NULL_OR_EMPTY(stName)
			STRING sDefaultName = GET_DEFAULT_BIKER_GANG_NAME()
			SET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_MC_GANG_NAME, MP_STAT_MC_GANG_NAME2, sDefaultName)
			CPRINTLN(DEBUG_INTERNET, "SETUP_CLUBHOUSE_GLOBALS_FROM_STATS CLUBHOUSE - Setting default name.")
		ELSE
			CPRINTLN(DEBUG_INTERNET, "SETUP_CLUBHOUSE_GLOBALS_FROM_STATS CLUBHOUSE - Name already set.")
		ENDIF
		
		g_sClubhouseDataStruct.iGunLocker		= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_CLUBHOUSE_GUN_LOCKER_PURCHASED())
		g_sClubhouseDataStruct.iGarage			= GET_BOOL_AS_INT_FOR_SCALEFORM(IS_CLUBHOUSE_GARAGE_PURCHASED())
		g_sClubhouseDataStruct.iClubhouseNameID	= GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLBHOSE_NAME_ID)
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_CLUBHOUSE_GLOBALS_FROM_STATS CLUBHOUSE ", iOwned, " owned \"", g_sClubhouseDataStruct.tl63OldSignage, "\"")
		CPRINTLN(DEBUG_INTERNET, "	iWall:", g_sClubhouseDataStruct.iWall,
				", iHanging:", g_sClubhouseDataStruct.iHanging,
				", iFurniture:", g_sClubhouseDataStruct.iFurniture,
				", iColourScheme:", g_sClubhouseDataStruct.iColourScheme)
		CPRINTLN(DEBUG_INTERNET, "	iFont:", g_sClubhouseDataStruct.iFont,
				", iFontColour:", g_sClubhouseDataStruct.iFontColour,
				", iEmblem:", g_sClubhouseDataStruct.iEmblem,
				", iHideSinage:", g_sClubhouseDataStruct.iHideSinage,
				PICK_STRING(g_sClubhouseDataStruct.iGunLocker = 0, ", iGunLocker:FALSE", ", iGunLocker:TRUE"),
				PICK_STRING(g_sClubhouseDataStruct.iGarage = 0, ", iVault:FALSE", ", iVault:TRUE"),
				", iClubhouseNameID:", g_sClubhouseDataStruct.iClubhouseNameID)
	ELSE
		g_sClubhouseDataStruct.iClubhouseID 	= -1
		g_sClubhouseDataStruct.iWall			= -1
		g_sClubhouseDataStruct.iHanging			= -1
		g_sClubhouseDataStruct.iFurniture		= -1
		g_sClubhouseDataStruct.iColourScheme	= -1
		g_sClubhouseDataStruct.iFont			= -1
		g_sClubhouseDataStruct.iFontColour		= -1
		g_sClubhouseDataStruct.iEmblem			= iCONST_DEFAULT_FREE_CLUBHOUSE_EMBLEM
		g_sClubhouseDataStruct.iHideSinage		= -1
		
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			g_sClubhouseDataStruct.tl63OldSignage = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_MC_CLBHOSE_NAME, MP_STAT_MC_CLBHOSE_NAME2)
			IF IS_STRING_NULL_OR_EMPTY(g_sClubhouseDataStruct.tl63OldSignage)
				g_sClubhouseDataStruct.tl63OldSignage = GET_DEFAULT_BIKER_GANG_NAME()
			ENDIF
		ELSE
			g_sClubhouseDataStruct.tl63OldSignage = GET_DEFAULT_BIKER_GANG_NAME()
		ENDIF
		
		g_sClubhouseDataStruct.tl63NewSignage 	= ""
		g_sClubhouseDataStruct.iGunLocker		= -1
		g_sClubhouseDataStruct.iGarage			= -1
		g_sClubhouseDataStruct.iClubhouseNameID	= 0
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_CLUBHOUSE_GLOBALS_FROM_STATS none owned \"", g_sClubhouseDataStruct.tl63OldSignage, "\"")
	ENDIF
	
	g_sClubhouseDataStruct.tl63CrewEmblem = ""
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
		IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
			NETWORK_CLAN_GET_EMBLEM_TXD_NAME(aGamerHandle, g_sClubhouseDataStruct.tl63CrewEmblem)
			CPRINTLN(DEBUG_INTERNET, "SETUP_CLUBHOUSE_GLOBALS_FROM_STATS crew emblem \"", g_sClubhouseDataStruct.tl63CrewEmblem, "\"")
		ENDIF
	ENDIF
ENDPROC

PROC POPULATE_OWNED_BIKER_CLUBHOUSE(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	IF bReInitGlobals
		SETUP_CLUBHOUSE_GLOBALS_FROM_STATS()
	ENDIF
	
	LANGUAGE_TYPE eLang = GET_CURRENT_LANGUAGE()
	
	//Set data for slot 0(Existing properties)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iClubhouseID)				//1 	Clubhouse ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iWall)						//2 	Wall
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iHanging)					//3 	Hanging
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iFurniture)					//4 	Furniture
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iColourScheme)				//5 	Colour Scheme
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iFont)						//6 	Font
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iFontColour)				//7 	Font Colour
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iEmblem)					//8 	Emblem
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iHideSinage)				//9 	Hide Signage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sClubhouseDataStruct.tl63OldSignage)	//10	Signage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sClubhouseDataStruct.tl63NewSignage)	//11	New Signage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iGunLocker)					//12	Gun Locker
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sClubhouseDataStruct.iGarage)					//13	Bike Shop
		IF eLang = LANGUAGE_POLISH
		OR eLang = LANGUAGE_RUSSIAN
		OR eLang = LANGUAGE_KOREAN
		OR eLang = LANGUAGE_CHINESE
		OR eLang = LANGUAGE_JAPANESE
		OR	elang =  LANGUAGE_CHINESE_SIMPLIFIED
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)										//14	Fonts Supported
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)											//14	Fonts Supported
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sClubhouseDataStruct.tl63CrewEmblem)	//15	Crew Emblem
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_BIKER_CLUBHOUSE already owned is:", g_sClubhouseDataStruct.iClubhouseID)
	
ENDPROC



PROC SETUP_BUNKER_GLOBALS_FROM_STATS()
	FACTORY_ID eOwned = GET_OWNED_BUNKER(PLAYER_ID())
	
	IF NOT (eOwned = FACTORY_ID_INVALID
	OR eOwned = FACTORY_ID_MAX)
		g_sBunkerDataStruct.iBunkerID 			= ENUM_TO_INT(eOwned)
		g_sBunkerDataStruct.iDecor				= GET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_DECOR)
		g_sBunkerDataStruct.iAccommodation		= GET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_SAVEBED)
		g_sBunkerDataStruct.iFiringRange		= GET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_FIRING_RANGE)
		g_sBunkerDataStruct.iGunLocker			= GET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_GUNLOCKER)
		g_sBunkerDataStruct.iTransportation		= GET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_TRANSPORTATION)
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_BUNKER_GLOBALS_FROM_STATS BUNKER ", eOwned, " owned, iHanging:", g_sBunkerDataStruct.iFiringRange,
				", iFurniture:", g_sBunkerDataStruct.iGunLocker,
				", iColourScheme:", g_sBunkerDataStruct.iTransportation)
	ELSE
		g_sBunkerDataStruct.iBunkerID			= -1
		g_sBunkerDataStruct.iDecor				= -1
		g_sBunkerDataStruct.iAccommodation		= -1
		g_sBunkerDataStruct.iFiringRange		= -1
		g_sBunkerDataStruct.iGunLocker			= -1
		g_sBunkerDataStruct.iTransportation		= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_BUNKER_GLOBALS_FROM_STATS none owned \"", /*g_sBunkerDataStruct.tl63OldSignage,*/ "\"")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_GUNRUNNING_BUNKER(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	IF bReInitGlobals
		SETUP_BUNKER_GLOBALS_FROM_STATS()
	ENDIF
	
	//Set data for slot 15(Existing properties)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(15)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sBunkerDataStruct.iBunkerID)					//1 	Bunker ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sBunkerDataStruct.iDecor)					//2		Style
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sBunkerDataStruct.iAccommodation)			//3 	Personal Quarters
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sBunkerDataStruct.iFiringRange)				//4 	Firing Range
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sBunkerDataStruct.iGunLocker)				//5 	Gun Locker
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sBunkerDataStruct.iTransportation)			//6 	Transportation
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_GUNRUNNING_BUNKER already owned is:", g_sBunkerDataStruct.iBunkerID)
	
ENDPROC

PROC SETUP_HANGAR_GLOBALS_FROM_STATS()
	HANGAR_ID eOwned = GET_PLAYERS_OWNED_HANGAR(PLAYER_ID())
	
	IF NOT (eOwned = HANGAR_ID_INVALID
	OR eOwned = HANGAR_ID_MAX)
		g_sHangarDataStruct.iHangarID 			= ENUM_TO_INT(eOwned)
		g_sHangarDataStruct.iFlooring			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_FLOOR_DECAL)
		g_sHangarDataStruct.iSleepingQuarters	= GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_SAVEBED)
		g_sHangarDataStruct.iFurniture			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_FURNITURE)
		g_sHangarDataStruct.iWorkshop			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_MODSHOP)
		g_sHangarDataStruct.iStyle				= GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_COLOUR)
		g_sHangarDataStruct.iLighting			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_LIGHTING)
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_HANGAR_GLOBALS_FROM_STATS HANGAR ", eOwned, " owned, iHangarID:", g_sHangarDataStruct.iHangarID, ", iStyle:", g_sHangarDataStruct.iStyle, ", iLighting:", g_sHangarDataStruct.iLighting)
		CPRINTLN(DEBUG_INTERNET, "	iFlooring:", g_sHangarDataStruct.iFlooring, ", iFurniture:", g_sHangarDataStruct.iFurniture, ", iSleepingQuarters:", g_sHangarDataStruct.iSleepingQuarters, ", iWorkshop:", g_sHangarDataStruct.iWorkshop)
	ELSE
		g_sHangarDataStruct.iHangarID			= -1
		g_sHangarDataStruct.iFlooring			= -1
		g_sHangarDataStruct.iSleepingQuarters	= -1
		g_sHangarDataStruct.iFurniture			= -1
		g_sHangarDataStruct.iWorkshop			= -1
		g_sHangarDataStruct.iStyle				= -1
		g_sHangarDataStruct.iLighting			= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_HANGAR_GLOBALS_FROM_STATS none owned \"", /*g_sHangarDataStruct.tl63OldSignage,*/ "\"")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_SMUGGLER_HANGAR(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	IF bReInitGlobals
		SETUP_HANGAR_GLOBALS_FROM_STATS()
	ENDIF
	
	//Set data for slot 47(Existing properties)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(47)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iHangarID)					// 1	Hangar ID	int
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iStyle)					// 2	Hangar Style / Tint	int
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iLighting)					// 3	Hangar Lighting	int
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iFlooring)					// 4	Hangar Flooring	int
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iFurniture)				// 5	Furniture	int
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iSleepingQuarters)			// 6	Sleeping Quarters	int
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sHangarDataStruct.iWorkshop)					// 7	Workshop	int	
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_SMUGGLER_HANGAR already owned is:", g_sHangarDataStruct.iHangarID)
	
ENDPROC

PROC SETUP_DEFUNCT_BASE_GLOBALS_FROM_STATS()
	DEFUNCT_BASE_ID eOwned = GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID())
	
	IF IS_DEFUNCT_BASE_ID_VALID(eOwned)
		g_sDefunctBaseDataStruct.iBaseID 			= ENUM_TO_INT(eOwned)
		g_sDefunctBaseDataStruct.iStyle				= GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_FACILITY_STYLE)
		g_sDefunctBaseDataStruct.iGraphics			= GET_SF_FROM_DBASE_GRAPHICS(GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_FACILITY_GRAPHIC))
		g_sDefunctBaseDataStruct.iOrbitalWeapon		= GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_ORBITAL_WEAPON)
		g_sDefunctBaseDataStruct.iSecurityRoom		= GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_SECURITY_ROOM)
		g_sDefunctBaseDataStruct.iLounge			= GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_LOUNGE)
		g_sDefunctBaseDataStruct.iPrivicyGlass		= GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_PRIVACY_GLASS)
		g_sDefunctBaseDataStruct.iPersonalQuarters	= GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_PERSONAL_QUARTERS)
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_DEFUNCT_BASE_GLOBALS_FROM_STATS base ", eOwned, " owned, iBaseID:", g_sDefunctBaseDataStruct.iBaseID, ", iStyle:", g_sDefunctBaseDataStruct.iStyle, ", iGraphics:", g_sDefunctBaseDataStruct.iGraphics)
		CPRINTLN(DEBUG_INTERNET, "	iOrbitalWeapon:", g_sDefunctBaseDataStruct.iOrbitalWeapon, ", iPersonalQuarters:", g_sDefunctBaseDataStruct.iPersonalQuarters, ", iSecurityRoom:", g_sDefunctBaseDataStruct.iSecurityRoom, ", iLounge:", g_sDefunctBaseDataStruct.iLounge, ", iSecurityRoom:", g_sDefunctBaseDataStruct.iPrivicyGlass)
	ELSE
		g_sDefunctBaseDataStruct.iBaseID 			= -1
		g_sDefunctBaseDataStruct.iStyle				= -1
		g_sDefunctBaseDataStruct.iGraphics			= -1
		g_sDefunctBaseDataStruct.iOrbitalWeapon		= -1
		g_sDefunctBaseDataStruct.iSecurityRoom		= -1
		g_sDefunctBaseDataStruct.iLounge			= -1
		g_sDefunctBaseDataStruct.iPrivicyGlass		= -1
		g_sDefunctBaseDataStruct.iPersonalQuarters	= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_DEFUNCT_BASE_GLOBALS_FROM_STATS none owned")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_GANG_OPS_DEFUNCT_BASE(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	IF bReInitGlobals
		SETUP_DEFUNCT_BASE_GLOBALS_FROM_STATS()
	ENDIF
	
	//Set data for slot 60(Existing properties)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(60)
		IF (g_sDefunctBaseDataStruct.iBaseID = -1)																					// 1	ID	int	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iBaseID+40)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iStyle)					// 2	Style
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iGraphics)				// 3	Graphics
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iOrbitalWeapon)			// 4	Orbital Weapon
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iSecurityRoom)			// 5	Security Room
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iLounge)					// 6	Lounge
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iPersonalQuarters)		// 7	Personal Quarters
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sDefunctBaseDataStruct.iPrivicyGlass)			// 8	Privacy Glass
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_GANG_OPS_DEFUNCT_BASE already owned is:", g_sDefunctBaseDataStruct.iBaseID)
	
ENDPROC

PROC SETUP_NIGHTCLUB_GLOBALS_FROM_STATS()
	NIGHTCLUB_ID eOwned = GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID())
	
	IF IS_NIGHTCLUB_ID_VALID(eOwned)
		g_sNightclubDataStruct.iNightclubID 		= ENUM_TO_INT(eOwned)
		g_sNightclubDataStruct.iStyle				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_NIGHTCLUB_UPGRADE(eNIGHTCLUB_MOD_STYLE))
		g_sNightclubDataStruct.iLighting			= GET_SF_FROM_NIGHTCLUB_LIGHTING(GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_NIGHTCLUB_UPGRADE(eNIGHTCLUB_MOD_LIGHTING)))
		g_sNightclubDataStruct.iDancers				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_NIGHTCLUB_UPGRADE(eNIGHTCLUB_MOD_DANCERS))
		g_sNightclubDataStruct.iDryIce				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_NIGHTCLUB_UPGRADE(eNIGHTCLUB_MOD_DRYICE))
		g_sNightclubDataStruct.iNameID				= GET_SF_FROM_NIGHTCLUB_NAME(GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_NIGHTCLUB_UPGRADE(eNIGHTCLUB_MOD_NAME)))
		g_sNightclubDataStruct.iStorage				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BUSINESS_HUB_UPGRADE(eBUSINESS_HUB_MOD_WAREHOUSE))
		g_sNightclubDataStruct.iGarage				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BUSINESS_HUB_UPGRADE(eBUSINESS_HUB_MOD_GARAGE))
		
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_NIGHTCLUB_GLOBALS_FROM_STATS NIGHTCLUB ", eOwned, " owned, iNightclubID:", g_sNightclubDataStruct.iNightclubID, ", iStyle:", g_sNightclubDataStruct.iStyle, ", iLighting:", g_sNightclubDataStruct.iLighting)
		CPRINTLN(DEBUG_INTERNET, "	iDancers:", g_sNightclubDataStruct.iDancers, ", iDryIce:", g_sNightclubDataStruct.iDryIce, ", iNameID:", g_sNightclubDataStruct.iNameID, ", iStorage:", g_sNightclubDataStruct.iStorage, ", iGarage:", g_sNightclubDataStruct.iGarage)
	ELSE
		g_sNightclubDataStruct.iNightclubID 		= -1
		g_sNightclubDataStruct.iStyle				= -1
		g_sNightclubDataStruct.iLighting			= -1
		g_sNightclubDataStruct.iDancers				= -1
		g_sNightclubDataStruct.iDryIce				= -1
		g_sNightclubDataStruct.iNameID				= -1
		g_sNightclubDataStruct.iStorage				= -1
		g_sNightclubDataStruct.iGarage				= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_NIGHTCLUB_GLOBALS_FROM_STATS none owned \"", g_sClubhouseDataStruct.tl63OldSignage, "\"")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_BUSINESS_BATTLES_NIGHTCLUB(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	
	IF bReInitGlobals
		SETUP_NIGHTCLUB_GLOBALS_FROM_STATS()
	ENDIF
	
	//Set data for slot 60(Existing properties)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(70)
		IF (g_sNightclubDataStruct.iNightclubID = -1)											// 1	ID	int	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iNightclubID+10)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iStyle)						// 2	Style
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iLighting)					// 3	Lighting
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iDancers)					// 4	Dancers
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iStorage)					// 5	Storage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iGarage)					// 6	Garage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iNameID)					// 7	Name
		
		IF (g_sNightclubDataStruct.iNightclubID = -1)											// 8	Terrorbyte Storage	int
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)											// -1: Player hasn't bought a nightclub yet 
		ELSE
			INT iBigAssID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_HACKER_TRUCK)
			BOOL bOwnHackerTruck = FALSE
							
			IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[(iBigAssID/32)], (iBigAssID%32))
			OR (USE_SERVER_TRANSACTIONS() AND GET_MP_INT_CHARACTER_STAT(MP_STAT_INV_HACKERTRUCK_MODEL_0) != 0)
				bOwnHackerTruck = TRUE
			ENDIF
			
			IF NOT bOwnHackerTruck
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)											// 0: Player owns a nightclub but hasn't bought the Terrorbyte 
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)											// 1: Player owns a nightclub and the Terrorbyte
			ENDIF
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sNightclubDataStruct.iDryIce)					// 9	Dry Ice
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_BUSINESS_BATTLES_NIGHTCLUB already owned is:", g_sNightclubDataStruct.iNightclubID)
	
ENDPROC

PROC SETUP_ARENA_GARAGE_GLOBALS_FROM_STATS()
	ARENA_GARAGE_ID eOwned = GET_PLAYERS_OWNED_ARENA_GARAGE(PLAYER_ID())
	
	IF IS_ARENA_GARAGE_ID_VALID(eOwned)
		g_sArenaDataStruct.iArenaID 			= ENUM_TO_INT(eOwned)
		g_sArenaDataStruct.iStyle				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_STYLE))
		g_sArenaDataStruct.iGraphics			= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_GRAPHICS))
		g_sArenaDataStruct.iColour				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_COLOUR))
		
		IF NOT IS_ARENA_GARAGE_FLOOR_PURCHASED(ENUM_TO_INT(ARENA_GARAGE_FLOOR_2))
			g_sArenaDataStruct.iExpansionFloorB1	= 0
		ELSE
			g_sArenaDataStruct.iExpansionFloorB1	= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_EXPANSION_FLOOR_0))
		ENDIF
		IF NOT IS_ARENA_GARAGE_FLOOR_PURCHASED(ENUM_TO_INT(ARENA_GARAGE_FLOOR_3))
			g_sArenaDataStruct.iExpansionFloorB2	= 0
		ELSE
			g_sArenaDataStruct.iExpansionFloorB2	= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_EXPANSION_FLOOR_1))
		ENDIF
		
		g_sArenaDataStruct.iBennyMechanic		= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_BENNY_MECHANIC))
		g_sArenaDataStruct.iWeaponMechanic		= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_WEAPON_MECHANIC))
		g_sArenaDataStruct.iPersonalQuarters	= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARENA_GARAGE_UPGRADE(eARENA_GARAGE_MOD_PERSONAL_QUARTERS))
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_ARENA_GARAGE_GLOBALS_FROM_STATS ARENA_GARAGE ", eOwned, " owned, iArenaID:", g_sArenaDataStruct.iArenaID,
				", iStyle:", g_sArenaDataStruct.iStyle,
				", iGraphics:", g_sArenaDataStruct.iGraphics,
				", iColour:", g_sArenaDataStruct.iColour,
				", iExpansionFloorB1:", g_sArenaDataStruct.iExpansionFloorB1,
				", iExpansionFloorB2:", g_sArenaDataStruct.iExpansionFloorB2)
		CPRINTLN(DEBUG_INTERNET,
				"	iBennyMechanic:", g_sArenaDataStruct.iBennyMechanic,
				", iWeaponMechanic:", g_sArenaDataStruct.iWeaponMechanic,
				", iPersonalQuarters:", g_sArenaDataStruct.iPersonalQuarters)
	ELSE
		g_sArenaDataStruct.iArenaID				= -1
		g_sArenaDataStruct.iStyle				= -1
		g_sArenaDataStruct.iGraphics			= -1
		g_sArenaDataStruct.iColour				= -1
		g_sArenaDataStruct.iExpansionFloorB1	= -1
		g_sArenaDataStruct.iExpansionFloorB2	= -1
		g_sArenaDataStruct.iBennyMechanic		= -1
		g_sArenaDataStruct.iWeaponMechanic		= -1
		g_sArenaDataStruct.iPersonalQuarters	= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_ARENA_GARAGE_GLOBALS_FROM_STATS none owned \"", g_sClubhouseDataStruct.tl63OldSignage, "\"")
	ENDIF
ENDPROC

PROC POPULATE_MP_ARENA_PROPERTY_SITE_PIN_MAP(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	IF bReInitGlobals
		SETUP_ARENA_GARAGE_GLOBALS_FROM_STATS()
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
//	Slot 3: Data covering the player's previous garage purchase, allowing the player to renovate their garage..
//	1	Style option
//		-1: Player hasn't purchased a garage
//		0 - 2: purchased option
//	2	Graphics option
//		-1: Player hasn't purchased a garage
//		0 - 24: Purchased option
//	3	Colour option
//		-1: Player hasn't purchased a garage
//		0 - 8: Purchased option
//	4	Expansion floor B1 option**
//		-1: Player hasn't purchased a garage
//		0: Player hasn't purchased the expansion floor
//		1 - 25: Purchased option
//	5	Expansion floor B2 option**
//		-1: Player hasn't purchased a garage
//		0: Player hasn't purchased the expansion floor
//		1 - 25: Purchased option
//	6	Benny's Mechanic
//		-1: Player hasn't purchased a garage
//		0: Benny's Mechanic not purchased
//		1: Benny's Mechanic purchased
//	7	Weapons Mechanic
//		-1: Player hasn't purchased a garage
//		0: Weapons mechanic not purchased
//		1: Weapons mechanic purchased
//	8	Personal Quarters
//		-1: Player hasn't purchased a garage
//		0: Personal Quarters not purchased
//		1: Personal Quarters purchased
//	
//	** Note that the purchased options for the expansion floors are 1-indexed as they are optional
//		 and 0 represents the player choosing not to purchase that option. 
//		Other purchased options are 0-indexed.
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iStyle)				//	1	Style option
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iGraphics)			//	2	Graphics option
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iColour)			//	3	Colour option
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iExpansionFloorB1)	//	4	Expansion floor B1 option**			
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iExpansionFloorB2)	//	5	Expansion floor B2 option**
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iBennyMechanic)		//	6	Benny's Mechanic
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iWeaponMechanic)	//	7	Weapons Mechanic
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArenaDataStruct.iPersonalQuarters)	//	8	Personal Quarters
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_ARENA_PROPERTY_SITE_PIN_MAP iArenaID:", g_sArenaDataStruct.iArenaID,
			", iStyle:", g_sArenaDataStruct.iStyle,
			", iGraphics:", g_sArenaDataStruct.iGraphics,
			", iColour:", g_sArenaDataStruct.iColour,
			", iExpansionFloorB1:", g_sArenaDataStruct.iExpansionFloorB1)
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_ARENA_PROPERTY_SITE_PIN_MAP iExpansionFloorB2:", g_sArenaDataStruct.iExpansionFloorB2, ", iBennyMechanic:", g_sArenaDataStruct.iBennyMechanic, ", iWeaponMechanic:", g_sArenaDataStruct.iWeaponMechanic, ", iPersonalQuarters ", g_sArenaDataStruct.iPersonalQuarters)
	
	INT constID = ENUM_TO_INT(ARENA_GARAGE_1)
	ARENA_GARAGE_ID eArenaID = INT_TO_ENUM(ARENA_GARAGE_ID, constID)
	
	ARENA_GARAGE_ID eOwned = GET_PLAYERS_OWNED_ARENA_GARAGE(PLAYER_ID())
	INT Arena_cost = GET_ARENA_GARAGE_PRICE(eArenaID)
	INT iArena_sale_cost = -1
	
	INT iCost
		
	INT Style_cost[3], iStyle_sale_cost[3]
	REPEAT COUNT_OF(Style_cost) iCost
		Style_cost[iCost]					= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_STYLE, iCost)
		iStyle_sale_cost[iCost]				= -1
	ENDREPEAT
	
	INT Graphics_cost[9], iGraphics_sale_cost[9]
	REPEAT COUNT_OF(Graphics_cost) iCost
		Graphics_cost[iCost]				= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_GRAPHICS, iCost)
		iGraphics_sale_cost[iCost]			= -1
	ENDREPEAT
		
	INT Colour_cost[9], iColour_sale_cost[9]
	REPEAT COUNT_OF(Colour_cost) iCost
		Colour_cost[iCost]					= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_COLOUR, iCost)
		iColour_sale_cost[iCost]			= -1
	ENDREPEAT
		
	INT ExpansionFloorB1_cost[9], iExpansionFloorB1_sale_cost[9]
	REPEAT COUNT_OF(ExpansionFloorB1_cost) iCost
		ExpansionFloorB1_cost[iCost]		= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_0, iCost+1)
		iExpansionFloorB1_sale_cost[iCost]	= -1
	ENDREPEAT
		
	INT ExpansionFloorB2_cost[9], iExpansionFloorB2_sale_cost[9]
	REPEAT COUNT_OF(ExpansionFloorB2_cost) iCost
		ExpansionFloorB2_cost[iCost]		= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_1, iCost+1)
		iExpansionFloorB2_sale_cost[iCost]	= -1
	ENDREPEAT
		
	INT BennyMechanic_cost					= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_BENNY_MECHANIC, 1)
	INT iBennyMechanic_sale_cost			= -1
	
	INT WeaponMechanic_cost					= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_WEAPON_MECHANIC, 1)
	INT iWeaponMechanic_sale_cost			= -1
	
	INT Personal_Quarters_cost				= GET_ARENA_GARAGE_UPGRADE_COST(eARENA_GARAGE_MOD_PERSONAL_QUARTERS, 1)
	INT iPersonal_Quarters_sale_cost		= -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_ARENA_GARAGE_BASE_PRICE(eArenaID) >= 0
		AND GET_ARENA_GARAGE_BASE_PRICE(eArenaID) > Arena_cost
	//	AND NOT SHOULD_ARENA_GARAGE_BE_FREE_FOR_PLAYER(eArenaID)
			iArena_sale_cost = Arena_cost
			Arena_cost = GET_ARENA_GARAGE_BASE_PRICE(eArenaID)
		ENDIF
		
		REPEAT COUNT_OF(Style_cost) iCost
			IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_STYLE, iCost) >= 0
			AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_STYLE, iCost) > Style_cost[iCost]
				IF (iCost = 0)
					IF NOT (eOwned != eArenaID)
						iStyle_sale_cost[iCost] = Style_cost[iCost]
						Style_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_STYLE, iCost)
					ENDIF
				ELSE
					iStyle_sale_cost[iCost] = Style_cost[iCost]
					Style_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_STYLE, iCost)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(Graphics_cost) iCost
			IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_GRAPHICS, iCost) >= 0
			AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_GRAPHICS, iCost) > Graphics_cost[iCost]
				IF (iCost = 0)
					IF NOT (eOwned != eArenaID)
						iGraphics_sale_cost[iCost] = Graphics_cost[iCost]
						Graphics_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_GRAPHICS, iCost)
					ENDIF
				ELSE
					iGraphics_sale_cost[iCost] = Graphics_cost[iCost]
					Graphics_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_GRAPHICS, iCost)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(Colour_cost) iCost
			IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_COLOUR, iCost) >= 0
			AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_COLOUR, iCost) > Colour_cost[iCost]
				IF (iCost = 0)
					IF NOT (eOwned != eArenaID)
						iColour_sale_cost[iCost] = Colour_cost[iCost]
						Colour_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_COLOUR, iCost)
					ENDIF
				ELSE
					iColour_sale_cost[iCost] = Colour_cost[iCost]
					Colour_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_COLOUR, iCost)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(ExpansionFloorB1_cost) iCost
			IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_0, iCost+1) >= 0
			AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_0, iCost+1) > ExpansionFloorB1_cost[iCost]
				iExpansionFloorB1_sale_cost[iCost] = ExpansionFloorB1_cost[iCost]
				ExpansionFloorB1_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_0, iCost+1)
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(ExpansionFloorB2_cost) iCost
			IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_1, iCost+1) >= 0
			AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_1, iCost+1) > ExpansionFloorB2_cost[iCost]
				iExpansionFloorB2_sale_cost[iCost] = ExpansionFloorB2_cost[iCost]
				ExpansionFloorB2_cost[iCost] = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_EXPANSION_FLOOR_1, iCost+1)
			ENDIF
		ENDREPEAT
//		
		IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_BENNY_MECHANIC, 1) >= 0
		AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_BENNY_MECHANIC, 1) > BennyMechanic_cost
			iBennyMechanic_sale_cost = BennyMechanic_cost
			BennyMechanic_cost = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_BENNY_MECHANIC, 1)
		ENDIF
		
		IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_WEAPON_MECHANIC, 1) >= 0
		AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_WEAPON_MECHANIC, 1) > WeaponMechanic_cost
			iWeaponMechanic_sale_cost = WeaponMechanic_cost
			WeaponMechanic_cost = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_WEAPON_MECHANIC, 1)
		ENDIF
		
		IF GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_PERSONAL_QUARTERS, 1) >= 0
		AND GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_PERSONAL_QUARTERS, 1) > Personal_Quarters_cost
			iPersonal_Quarters_sale_cost = Personal_Quarters_cost
			Personal_Quarters_cost = GET_ARENA_GARAGE_UPGRADE_BASE_COST(eARENA_GARAGE_MOD_PERSONAL_QUARTERS, 1)
		ENDIF
	ENDIF
	
//	Slots 4: Prices for each of the garage options.
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Arena_cost)						//	1	Base price
		IF eOwned != eArenaID													//	2	Style options 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_cost[0])
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_cost[1])						//	3	Style options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Style_cost[2])						//	4	Style options 2
		IF eOwned != eArenaID													//	5	Graphics options 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[0])
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[1])					//	6	Graphics options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[2])					//	7	Graphics options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[3])					//	8	Graphics options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[4])					//	9	Graphics options 4
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[5])					//	10	Graphics options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[6])					//	11	Graphics options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[7])					//	12	Graphics options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Graphics_cost[8])					//	13	Graphics options 8
		IF eOwned != eArenaID													//	14	Colour options 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[0])
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[1])					//	15	Colour options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[2])					//	16	Colour options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[3])					//	17	Colour options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[4])					//	18	Colour options 4
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[5])					//	19	Colour options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[6])					//	20	Colour options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[7])					//	21	Colour options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colour_cost[8])					//	22	Colour options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[0])			//	23	Expansion floor B1 options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[1])			//	24	Expansion floor B1 options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[2])			//	25	Expansion floor B1 options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[3])			//	26	Expansion floor B1 options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[4])			//	27	Expansion floor B1 options 4
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[5])			//	28	Expansion floor B1 options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[6])			//	29	Expansion floor B1 options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[7])			//	30	Expansion floor B1 options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB1_cost[8])			//	31	Expansion floor B1 options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[0])			//	32	Expansion floor B2 options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[1])			//	33	Expansion floor B2 options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[2])			//	34	Expansion floor B2 options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[3])			//	35	Expansion floor B2 options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[4])			//	36	Expansion floor B2 options 4
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[5])			//	37	Expansion floor B2 options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[6])			//	38	Expansion floor B2 options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[7])			//	39	Expansion floor B2 options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ExpansionFloorB2_cost[8])			//	40	Expansion floor B2 options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BennyMechanic_cost)				//	41	Benny's Mechanic
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(WeaponMechanic_cost)				//	42	Weapons Mechanic
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Personal_Quarters_cost)			//	43	Personal Quarters
	END_SCALEFORM_MOVIE_METHOD()
	
//	Slots 5: Sale prices for each of the garage options.
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArena_sale_cost)					//	1	Base price
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_sale_cost[0])				//	2	Style options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_sale_cost[1])				//	3	Style options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle_sale_cost[2])				//	4	Style options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[0])			//	5	Graphics options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[1])			//	6	Graphics options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[2])			//	7	Graphics options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[3])			//	8	Graphics options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[4])			//	9	Graphics options 4
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[5])			//	10	Graphics options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[6])			//	11	Graphics options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[7])			//	12	Graphics options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGraphics_sale_cost[8])			//	13	Graphics options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[0])				//	14	Colour options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[1])				//	15	Colour options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[2])				//	16	Colour options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[3])				//	17	Colour options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[4])				//	18	Colour options 4
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[5])				//	19	Colour options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[6])				//	20	Colour options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[7])				//	21	Colour options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColour_sale_cost[8])				//	22	Colour options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[0])	//	23	Expansion floor B1 options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[1])	//	24	Expansion floor B1 options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[2])	//	25	Expansion floor B1 options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[3])	//	26	Expansion floor B1 options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[4])	//	27	Expansion floor B1 options 4
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[5])	//	28	Expansion floor B1 options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[6])	//	29	Expansion floor B1 options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[7])	//	30	Expansion floor B1 options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB1_sale_cost[8])	//	31	Expansion floor B1 options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[0])	//	32	Expansion floor B2 options 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[1])	//	33	Expansion floor B2 options 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[2])	//	34	Expansion floor B2 options 2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[3])	//	35	Expansion floor B2 options 3
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[4])	//	36	Expansion floor B2 options 4
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[5])	//	37	Expansion floor B2 options 5
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[6])	//	38	Expansion floor B2 options 6
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[7])	//	39	Expansion floor B2 options 7
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iExpansionFloorB2_sale_cost[8])	//	40	Expansion floor B2 options 8
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBennyMechanic_sale_cost)			//	41	Benny's Mechanic
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWeaponMechanic_sale_cost)			//	42	Weapons Mechanic
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPersonal_Quarters_sale_cost)		//	43	Personal Quarters
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SETUP_CASINO_APT_GLOBALS_FROM_STATS()
	CASINO_APT_ID eOwned = GET_PLAYERS_OWNED_CASINO_APARTMENT(PLAYER_ID())
	
	IF IS_CASINO_APT_ID_VALID(eOwned)
		g_sCasinoDataStruct.iCasinoID 			= ENUM_TO_INT(eOwned)
		g_sCasinoDataStruct.iColourOption		= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_COLOUR_OPTION))
		g_sCasinoDataStruct.iStyleOption		= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_STYLE_OPTION))
		g_sCasinoDataStruct.iLounge				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_LOUNGE))
		g_sCasinoDataStruct.iBar				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_BAR))
		IF (g_sCasinoDataStruct.iBar > 0)
			IF GET_PACKED_STAT_INT(PACKED_STAT_LUXSUITEARCADESETTING) = 0
				g_sCasinoDataStruct.iBar = 1
			ELIF GET_PACKED_STAT_INT(PACKED_STAT_LUXSUITEARCADESETTING) = 1
				g_sCasinoDataStruct.iBar = 2
			ENDIF
		ENDIF
		g_sCasinoDataStruct.iDealer				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_DEALER))
		g_sCasinoDataStruct.iBedroom			= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_BEDROOM))
		g_sCasinoDataStruct.iMediaroom			= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_MEDIAROOM))
		g_sCasinoDataStruct.iSpa				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_SPA))
		g_sCasinoDataStruct.iOffice				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_OFFICE))
		g_sCasinoDataStruct.iGarage				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_GARAGE))
		g_sCasinoDataStruct.iCasinoMembership	= 0
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_CASINO_APT_GLOBALS_FROM_STATS CASINO_APT ", eOwned, " owned, iCasinoID:", g_sCasinoDataStruct.iCasinoID,
				", iColourOption:", g_sCasinoDataStruct.iColourOption,
				", iStyleOption:", g_sCasinoDataStruct.iStyleOption,
				", iLounge:", g_sCasinoDataStruct.iLounge,
				", iBar:", g_sCasinoDataStruct.iBar,
				", iDealer:", g_sCasinoDataStruct.iDealer)
		CPRINTLN(DEBUG_INTERNET,
				"	iBedroom:", g_sCasinoDataStruct.iBedroom,
				", iMediaroom:", g_sCasinoDataStruct.iMediaroom,
				", iSafe:", g_sCasinoDataStruct.iSpa,
				", iGunlocker:", g_sCasinoDataStruct.iOffice,
				", iGarage:", g_sCasinoDataStruct.iGarage)
	ELSE
		g_sCasinoDataStruct.iCasinoID			= -1
		g_sCasinoDataStruct.iColourOption		= -1
		g_sCasinoDataStruct.iStyleOption		= -1
		g_sCasinoDataStruct.iLounge				= -1
		g_sCasinoDataStruct.iBar				= -1
		g_sCasinoDataStruct.iDealer				= -1
		g_sCasinoDataStruct.iBedroom			= -1
		g_sCasinoDataStruct.iMediaroom			= -1
		g_sCasinoDataStruct.iSpa				= -1
		g_sCasinoDataStruct.iOffice				= -1
		g_sCasinoDataStruct.iGarage				= -1
		g_sCasinoDataStruct.iCasinoMembership	= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_CASINO_APT_GLOBALS_FROM_STATS none owned \"", g_sClubhouseDataStruct.tl63OldSignage, "\"")
	ENDIF
	
	IF HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
		g_sCasinoDataStruct.iCasinoMembership	= 1
	ENDIF
	
	g_sCasinoDataStruct.tl63Gamertag			= GET_PLAYER_NAME(PLAYER_ID())
	PEDHEADSHOT_ID PlayerHeadshot				= Get_HeadshotID_For_Player(PLAYER_ID())
	IF PlayerHeadshot != NULL
		g_sCasinoDataStruct.tl23MugshotTexture	= GET_PEDHEADSHOT_TXD_STRING(PlayerHeadshot)
	ELSE
		g_sCasinoDataStruct.tl23MugshotTexture	= ""
	ENDIF
	CPRINTLN(DEBUG_INTERNET, "SETUP_CASINO_APT_GLOBALS_FROM_STATS CASINO_APT iCasinoMembership:", g_sCasinoDataStruct.iCasinoMembership,
				", tl63Gamertag:\"", g_sCasinoDataStruct.tl63Gamertag,
				"\", tl23MugshotTexture:\"", g_sCasinoDataStruct.tl23MugshotTexture, "\"")

ENDPROC

PROC POPULATE_MP_CASINO_PROPERTY_SITE_PIN_MAP(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	IF bReInitGlobals
		SETUP_CASINO_APT_GLOBALS_FROM_STATS()
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
//	Slot 10 - Previous Purchase Options
//	1	Colour
//		-1: Player hasn't purchased a suite
//		0 - 8: Purchased option
//	2	Style
//		-1: Player hasn't purchased a suite
//		0 - 8: Purchased option
//	3	Lounge
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	4	Bar
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option with the classic arcade machines
//		2: Player has purchased this option with the stylish arcade machines
//	5	Dealer
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	6	Bedroom
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	7	Media Room
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	8	Safe
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	9	Gun Locker
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	10	Garage
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	11	Helipad
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	12	Casino Membership
//		-1: Player hasn't purchased a suite
//		0: Player has purchased a suite but not this option.
//		1: Player has purchased this option
//	13	Gamertag
//		Player's gamer tag (string)
//	14	Mugshot Texture
//		Texture and dictionary name of the player's pedmugshot.
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(10)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iColourOption)					//	1	Colour
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iStyleOption)					//	2	Style
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iLounge)						//	3	Lounge
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iBar)							//	4	Bar
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iDealer)						//	5	Dealer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iBedroom)						//	6	Bedroom
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iMediaroom)					//	7	Media Room
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iSpa)							//	8	Safe
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iOffice)						//	9	Gun Locker
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iGarage)						//	10	Garage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)												//	11	UNUSED
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sCasinoDataStruct.iCasinoMembership)				//	12	Casino Membership
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sCasinoDataStruct.tl63Gamertag)		//	13	Gamertag
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(g_sCasinoDataStruct.tl23MugshotTexture)	//	14	Mugshot Texture
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sMPtunables.bVC_CASINO_SITE_SHOW_BANNER)		//	15	Show Banner
		INT iBlackjackReason, i3CardPokerReason													//	16	Private Dealer Locked
		IF IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_BLACKJACK, iBlackjackReason)
		OR IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_3CARDPOKER , i3CardPokerReason)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
		IF IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_BLACKJACK, iBlackjackReason)
		OR IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_3CARDPOKER , i3CardPokerReason)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_CASINO_PROPERTY_SITE_PIN_MAP iCasinoID:", g_sCasinoDataStruct.iCasinoID,
			", iColourOption:", g_sCasinoDataStruct.iColourOption,
			", iStyleOption:", g_sCasinoDataStruct.iStyleOption,
			", iLounge:", g_sCasinoDataStruct.iLounge,
			", iBar:", g_sCasinoDataStruct.iBar,
			", iDealer:", g_sCasinoDataStruct.iDealer)
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_CASINO_PROPERTY_SITE_PIN_MAP iBedroom:", g_sCasinoDataStruct.iBedroom,
			", iMediaroom:", g_sCasinoDataStruct.iMediaroom,
			", iSafe ", g_sCasinoDataStruct.iSpa,
			", iGunlocker ", g_sCasinoDataStruct.iOffice,
			", iGarage ", g_sCasinoDataStruct.iGarage)
	CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_CASINO_PROPERTY_SITE_PIN_MAP iCasinoMembership ", g_sCasinoDataStruct.iCasinoMembership,
			", tl63Gamertag \"", g_sCasinoDataStruct.tl63Gamertag,
			"\", tl23MugshotTexture \"", g_sCasinoDataStruct.tl23MugshotTexture, "\"")
	
	INT constID = ENUM_TO_INT(CASINO_APARTMENT_1)
	CASINO_APT_ID eCasinoID = INT_TO_ENUM(CASINO_APT_ID, constID)
	
	CASINO_APT_ID eOwned = GET_PLAYERS_OWNED_CASINO_APARTMENT(PLAYER_ID())
	INT Casino_cost = -1, iCasino_sale_cost = -1
	IF NOT IS_CASINO_APT_ID_VALID(eOwned)
		Casino_cost = GET_CASINO_APT_PRICE(eCasinoID)
	ENDIF
	
	INT iCost
	INT Colours_cost[ciCASINO_APT_MOD_COLOUR_COUNT], iColours_sale_cost[ciCASINO_APT_MOD_COLOUR_COUNT]
	REPEAT COUNT_OF(Colours_cost) iCost
		Colours_cost[iCost]					= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_COLOUR_OPTION, iCost)
		iColours_sale_cost[iCost]			= -1
	ENDREPEAT
	
	INT Styles_cost[ciCASINO_APT_MOD_STYLE_COUNT], iStyles_sale_cost[ciCASINO_APT_MOD_STYLE_COUNT]
	REPEAT COUNT_OF(Styles_cost) iCost
		Styles_cost[iCost]					= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_STYLE_OPTION, iCost)
		iStyles_sale_cost[iCost]			= -1
	ENDREPEAT
		
	INT Lounge_cost							= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_LOUNGE, 1)
	INT iLounge_sale_cost					= -1
		
	INT Bar_cost							= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_BAR, 1)
	INT iBar_sale_cost						= -1
		
	INT Dealer_cost							= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_DEALER, 1)
	INT iDealer_sale_cost					= -1
		
	INT Bedroom_cost						= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_BEDROOM, 1)
	INT iBedroom_sale_cost					= -1
	
	INT Mediaroom_cost						= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_MEDIAROOM, 1)
	INT iMediaroom_sale_cost				= -1
	
	INT Spa_cost							= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_SPA, 1)
	INT iSpa_sale_cost						= -1
	
	INT Office_cost							= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_OFFICE, 1)
	INT iOffice_sale_cost					= -1
	
	INT Garage_cost							= GET_CASINO_APT_UPGRADE_COST(eCASINO_APT_MOD_GARAGE, 1)
	INT iGarage_sale_cost					= -1
	
	INT Membership_cost						= GET_CASINO_MEMBERSHIP_COST()
	INT iMembership_sale_cost				= -1
	
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
		IF GET_CASINO_APT_BASE_PRICE(eCasinoID) != 0
		AND GET_CASINO_APT_BASE_PRICE(eCasinoID) > Casino_cost
	//	AND NOT SHOULD_CASINO_GARAGE_BE_FREE_FOR_PLAYER(eCasinoID)
		AND NOT IS_CASINO_APT_ID_VALID(eOwned)
			iCasino_sale_cost = Casino_cost
			Casino_cost = GET_CASINO_APT_BASE_PRICE(eCasinoID)
		ENDIF
		
		REPEAT COUNT_OF(Colours_cost) iCost
			IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_COLOUR_OPTION, iCost) != 0
			AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_COLOUR_OPTION, iCost) > Colours_cost[iCost]
				IF (iCost = 0)
					IF NOT (eOwned != eCasinoID)
						iColours_sale_cost[iCost] = Colours_cost[iCost]
						Colours_cost[iCost] = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_COLOUR_OPTION, iCost)
					ENDIF
				ELSE
					iColours_sale_cost[iCost] = Colours_cost[iCost]
					Colours_cost[iCost] = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_COLOUR_OPTION, iCost)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(Styles_cost) iCost
			IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_STYLE_OPTION, iCost) != 0
			AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_STYLE_OPTION, iCost) > Styles_cost[iCost]
				IF (iCost = 0)
					IF NOT (eOwned != eCasinoID)
						iStyles_sale_cost[iCost] = Styles_cost[iCost]
						Styles_cost[iCost] = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_STYLE_OPTION, iCost)
					ENDIF
				ELSE
					iStyles_sale_cost[iCost] = Styles_cost[iCost]
					Styles_cost[iCost] = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_STYLE_OPTION, iCost)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_LOUNGE, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_LOUNGE, 1) > Lounge_cost
			iLounge_sale_cost = Lounge_cost
			Lounge_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_LOUNGE, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_BAR, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_BAR, 1) > Bar_cost
			iBar_sale_cost = Bar_cost
			Bar_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_BAR, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_DEALER, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_DEALER, 1) > Dealer_cost
			iDealer_sale_cost = Dealer_cost
			Dealer_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_DEALER, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_BEDROOM, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_BEDROOM, 1) > Bedroom_cost
			iBedroom_sale_cost = Bedroom_cost
			Bedroom_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_BEDROOM, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_MEDIAROOM, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_MEDIAROOM, 1) > Mediaroom_cost
			iMediaroom_sale_cost = Mediaroom_cost
			Mediaroom_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_MEDIAROOM, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_SPA, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_SPA, 1) > Spa_cost
			iSpa_sale_cost = Spa_cost
			Spa_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_SPA, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_OFFICE, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_OFFICE, 1) > Office_cost
			iOffice_sale_cost = Office_cost
			Office_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_OFFICE, 1)
		ENDIF
		
		IF GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_GARAGE, 1) != 0
		AND GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_GARAGE, 1) > Garage_cost
			iGarage_sale_cost = Garage_cost
			Garage_cost = GET_CASINO_APT_UPGRADE_BASE_COST(eCASINO_APT_MOD_GARAGE, 1)
		ENDIF
		
		IF GET_CASINO_MEMBERSHIP_COST(TRUE) != 0
		AND GET_CASINO_MEMBERSHIP_COST(TRUE) > Membership_cost
			iMembership_sale_cost = Membership_cost
			Membership_cost = GET_CASINO_MEMBERSHIP_COST(TRUE)
		ENDIF
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CASINO_APT_UPGRADE(eCASINO_APT_MOD_BAR)) != 0
		iBar_sale_cost = 0
		Bar_cost = 0
	ENDIF	
	
//	Slots 11 - Option Prices. For options that are not on sale, please pass in -1
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(11)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Casino_cost)					//	1	Base price	Only added on when the player is purchasing a suite, not when they are renovating an existing suite.
		IF eOwned != eCasinoID												//	2	Colour option 1	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colours_cost[0])
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colours_cost[1])				//	3	Colour option 2	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Colours_cost[2])				//	4	Colour option 3
		IF eOwned != eCasinoID												//	5	Style option 1	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[0])
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[1])				//	6	Style option 2	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[2])				//	7	Style option 3	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[3])				//	8	Style option 4	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[4])				//	9	Style option 5	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[5])				//	10	Style option 6	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[6])				//	11	Style option 7	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[7])				//	12	Style option 8	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Styles_cost[8])				//	13	Style option 9	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Lounge_cost)					//	14	Lounge	
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(11)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Bar_cost)						//	15	Bar with classic arcade machines
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Bar_cost)						//	16	Bar with stylish arcade machines	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Dealer_cost)					//	17	Dealer	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Bedroom_cost)					//	18	Bedroom	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Mediaroom_cost)				//	19	Media room	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Spa_cost)						//	20	Safe	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Office_cost)					//	21	Gun locker	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Garage_cost)					//	22	Garage	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Membership_cost)				//	23	Casino membership	
	END_SCALEFORM_MOVIE_METHOD()
	
//	Slots 12 - Sale Prices. For options that are not on sale, please pass in -1
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(12)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCasino_sale_cost)				//	1	Base price
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColours_sale_cost[0])			//	2	Colour option 1	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColours_sale_cost[1])			//	3	Colour option 2	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColours_sale_cost[2])			//	4	Colour option 3	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[0])			//	5	Style option 1	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[1])			//	6	Style option 2	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[2])			//	7	Style option 3	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[3])			//	8	Style option 4	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[4])			//	9	Style option 5	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[5])			//	10	Style option 6	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[6])			//	11	Style option 7	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[7])			//	12	Style option 8	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyles_sale_cost[8])			//	13	Style option 9	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLounge_sale_cost)				//	14	Lounge	
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "APPEND_OFFICE_DATA_SLOT")SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(12)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBar_sale_cost)				//	15	Bar with classic arcade machines
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBar_sale_cost)				//	16	Bar with stylish arcade machines
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDealer_sale_cost)				//	17	Dealer	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBedroom_sale_cost)			//	18	Bedroom	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMediaroom_sale_cost)			//	19	Media room	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSpa_sale_cost)				//	20	Safe	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOffice_sale_cost)				//	21	Gun locker	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGarage_sale_cost)				//	22	Garage	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMembership_sale_cost)			//	23	Casino membership	
	END_SCALEFORM_MOVIE_METHOD()
	
//	Slots 13, 14 & 15 - Preset Suites (These are combinations of options that are available for instant purchase on the Suites Overview page).
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(13)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_COLOUR)		//	1	Colour	0 - 2 for the appropriate option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_STYLE)		//	2	Style	0 - 8 for the appropriate option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_LOUNGE)		//	3	Lounge	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_BAR)			//	4	Bar	0 - not selected, 1 - selected with classic arcade machines, 2 - selected with stylish arcade machines
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_DEALER)		//	5	Dealer	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_BEDROOM)		//	6	Bedroom	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_MEDIAROOM)	//	7	Media room	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_SAFE)			//	8	Spa	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_GUNLOCKER)	//	9	Office	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_HIGHROLLER_GARAGE)		//	10	Garage	0 - not selected, 1 - selected.
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(14)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_COLOUR)			//	1	Colour	0 - 2 for the appropriate option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_STYLE)			//	2	Style	0 - 8 for the appropriate option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_LOUNGE)			//	3	Lounge	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_BAR)			//	4	Bar	0 - not selected, 1 - selected with classic arcade machines, 2 - selected with stylish arcade machines
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_DEALER)			//	5	Dealer	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_BEDROOM)		//	6	Bedroom	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_MEDIAROOM)		//	7	Media room	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_SAFE)			//	8	Spa	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_GUNLOCKER)		//	9	Office	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_CRASHPAD_GARAGE)			//	10	Garage	0 - not selected, 1 - selected.
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(15)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_COLOUR)			//	1	Colour	0 - 2 for the appropriate option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_STYLE)				//	2	Style	0 - 8 for the appropriate option.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_LOUNGE)			//	3	Lounge	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_BAR)				//	4	Bar	0 - not selected, 1 - selected with classic arcade machines, 2 - selected with stylish arcade machines
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_DEALER)			//	5	Dealer	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_BEDROOM)			//	6	Bedroom	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_MEDIAROOM)			//	7	Media room	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_SAFE)				//	8	Spa	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_GUNLOCKER)			//	9	Office	0 - not selected, 1 - selected.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMPtunables.iVC_CASINO_SITE_PARTY_GARAGE)			//	10	Garage	0 - not selected, 1 - selected.
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

#IF FEATURE_CASINO_HEIST
PROC SETUP_ARCADE_GLOBALS_FROM_STATS()
	ARCADE_PROPERTY_ID eOwned = GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID())
	
	IF IS_ARCADE_PROPERTY_ID_VALID(eOwned)
		g_sArcadeDataStruct.iArcadeID = ENUM_TO_INT(eOwned)
		g_sArcadeDataStruct.iFloor = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_FLOOR))
		g_sArcadeDataStruct.iCeiling = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_CEILING))
		g_sArcadeDataStruct.iWall = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_WALL))
		g_sArcadeDataStruct.iPersonalQuarters = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_PERSONAL_QUARTERS))
		g_sArcadeDataStruct.iGarage = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_GARAGE))
		g_sArcadeDataStruct.iNeonLights = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_NEON_LIGHTS))
		g_sArcadeDataStruct.iScreens = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_ARCADE_UPGRADE(eARCADE_MOD_SCREENS))
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_ARCADE_GLOBALS_FROM_STATS NIGHTCLUB ", eOwned," owned, iArcadeID: ", g_sArcadeDataStruct.iArcadeID, ", iFloor:", g_sArcadeDataStruct.iFloor)
	ELSE
		g_sArcadeDataStruct.iArcadeID = -1
		g_sArcadeDataStruct.iFloor = -1
		g_sArcadeDataStruct.iCeiling = -1
		g_sArcadeDataStruct.iWall = -1
		g_sArcadeDataStruct.iPersonalQuarters = -1
		g_sArcadeDataStruct.iGarage = -1
		g_sArcadeDataStruct.iNeonLights = -1
		g_sArcadeDataStruct.iScreens = -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_ARCADE_GLOBALS_FROM_STATS none owned")
	ENDIF
ENDPROC

PROC POPULATE_OWNED_ARCADE(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	IF bReInitGlobals
		SETUP_ARCADE_GLOBALS_FROM_STATS()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(90)
		
		IF (g_sArcadeDataStruct.iArcadeID = -1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iArcadeID + 60)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iFloor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iCeiling)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iWall)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iPersonalQuarters)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iGarage)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iNeonLights)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sArcadeDataStruct.iScreens)
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_ARCADE already owned is:", g_sArcadeDataStruct.iArcadeID)
ENDPROC
#ENDIF

PROC SETUP_AUTO_SHOP_GLOBALS_FROM_STATS()
	AUTO_SHOP_PROPERTY_ID eOwned = GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID())
	
	IF IS_AUTO_SHOP_PROPERTY_ID_VALID(eOwned)
		g_sAutoShopDataStruct.iAutoShopID 			= ENUM_TO_INT(eOwned)
		g_sAutoShopDataStruct.iWall 				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_WALL))
		g_sAutoShopDataStruct.iTint 				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_TINT))
		g_sAutoShopDataStruct.iEmblem 				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_EMBLEM))
		g_sAutoShopDataStruct.iCrewName 			= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_CREW_NAME))
		g_sAutoShopDataStruct.iStaff1 				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_STAFF_ONE))
		g_sAutoShopDataStruct.iStaff2 				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_STAFF_TWO))
		g_sAutoShopDataStruct.iCarLift 				= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_CAR_LIFT))
		g_sAutoShopDataStruct.iPersonalQuarters 	= GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_AUTO_SHOP_UPGRADE(AUTO_SHOP_MOD_PERSONAL_QUARTERS))
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_AUTO_SHOP_GLOBALS_FROM_STATS ", eOwned," owned, iAutoShopID: ", g_sAutoShopDataStruct.iAutoShopID)
	ELSE
		g_sAutoShopDataStruct.iAutoShopID 			= -1
		g_sAutoShopDataStruct.iWall 				= -1
		g_sAutoShopDataStruct.iTint 				= -1
		g_sAutoShopDataStruct.iEmblem 				= -1
		g_sAutoShopDataStruct.iCrewName				= -1
		g_sAutoShopDataStruct.iStaff1 				= -1
		g_sAutoShopDataStruct.iStaff2 				= -1
		g_sAutoShopDataStruct.iCarLift 				= -1
		g_sAutoShopDataStruct.iPersonalQuarters 	= -1
		
		CPRINTLN(DEBUG_INTERNET, "SETUP_AUTO_SHOP_GLOBALS_FROM_STATS none owned")
	ENDIF
	
	g_sAutoShopDataStruct.iCarClubMembership 		= BOOL_TO_INT(HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID()))
ENDPROC

PROC POPULATE_OWNED_AUTO_SHOP(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	IF bReInitGlobals
		SETUP_AUTO_SHOP_GLOBALS_FROM_STATS()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
		
		IF (g_sAutoShopDataStruct.iAutoShopID = -1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iAutoShopID + 70)					// 1 ID
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iWall)									// 2 Style
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iTint)									// 3 Tint
		// url:bugstar:7169939
		IF (g_sAutoShopDataStruct.iEmblem = 0) // Crew Emblem purchased
		AND NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			PRINTLN("POPULATE_OWNED_AUTO_SHOP: overriding crew emblem as play is no longer in crew")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)									// Player can leave crew so crew emblem is invalid, have to show a default.
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iEmblem)									// 4 Emblem
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iCrewName)								// 5 Crew Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iStaff1)									// 6 Staff One
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iStaff2)									// 7 Staff Two
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iCarLift)								// 8 Car Lift
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iPersonalQuarters)						// 9 Personal Quarters
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sAutoShopDataStruct.iCarClubMembership)						// 10 Membership
		
		STRING strDisplayCrewName
		TEXT_LABEL_63 _tlDisplayCrewName
		
		STRING strDisplayCrewEmblem
		TEXT_LABEL_63 _tlDisplayCrewEmblem
		
		GAMER_HANDLE sGamerHandle = GET_LOCAL_GAMER_HANDLE()
		
		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			_tlDisplayCrewName = GET_LOCAL_PLAYER_CREW_NAME()
			strDisplayCrewName = Get_String_From_TextLabel(_tlDisplayCrewName)
			
			IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(sGamerHandle, _tlDisplayCrewEmblem)
				strDisplayCrewEmblem = Get_String_From_TextLabel(_tlDisplayCrewEmblem)
				CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_AUTO_SHOP - strDisplayCrewEmblem:", strDisplayCrewEmblem)
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_AUTO_SHOP - strDisplayCrewName:", strDisplayCrewName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(strDisplayCrewName)									// 11 Display Crew Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(strDisplayCrewEmblem)								// 12 Crew Emblem TXD
		
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "POPULATE_OWNED_AUTO_SHOP already owned is:", g_sAutoShopDataStruct.iAutoShopID)
ENDPROC

PROC POPULATE_MP_BIKER_PROPERTY_SITE_PIN_MAP(SCALEFORM_INDEX pagemov, BOOL bReInitGlobals = FALSE)
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
	END_SCALEFORM_MOVIE_METHOD()
	
	POPULATE_OWNED_BIKER_CLUBHOUSE(pagemov, bReInitGlobals)
	
	INT i
	INT scaleformIndex = 3
	//	Clubhouse ids are in the range 91 - 102
	FOR i = PROPERTY_CLUBHOUSE_1_BASE_A TO PROPERTY_CLUBHOUSE_12_BASE_B
		CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_BIKER_PROPERTY_SITE_PIN_MAP populating for clubhouse: ", i)		//, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
		IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE) = i
			POPULATE_MP_BIKER_PROPERTY_PIN(scaleformIndex,i,pagemov)
		ELSE
			IF NOT IS_PROPERTY_SALE_BLOCKED_BY_TUNEABLES(i)
				POPULATE_MP_BIKER_PROPERTY_PIN(scaleformIndex,i,pagemov)
			ENDIF
		ENDIF	
	ENDFOR
	
	POPULATE_OWNED_GUNRUNNING_BUNKER(pagemov, bReInitGlobals)
	
	scaleformIndex = 16
	//	Bunker ids are in the range 21 - 31
	FOR i = ENUM_TO_INT(FACTORY_ID_BUNKER_1) TO ENUM_TO_INT(FACTORY_ID_BUNKER_12)
		CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_GUNRUNNING_PROPERTY_SITE_PIN_MAP populating for bunker: ", i)		//, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
		IF GET_OWNED_BUNKER(PLAYER_ID()) = INT_TO_ENUM(FACTORY_ID, i)
			POPULATE_MP_GUNRUNNING_PROPERTY_PIN(scaleformIndex,i,pagemov)
		ELSE
			IF NOT IS_FACTORY_PURCHASE_DISABLED(INT_TO_ENUM(FACTORY_ID, i))
				POPULATE_MP_GUNRUNNING_PROPERTY_PIN(scaleformIndex,i,pagemov)
			ENDIF
		ENDIF	
	ENDFOR
	
	POPULATE_OWNED_SMUGGLER_HANGAR(pagemov, bReInitGlobals)
	
	scaleformIndex = 48
	//	Hangar ids are in the range 1 - 5
	FOR i = ENUM_TO_INT(LSIA_HANGAR_1) TO ENUM_TO_INT(ZANCUDO_HANGAR_3499)
		CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_SMUGGLER_PROPERTY_PIN populating for hangar: ", i)		//, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
		IF GET_PLAYERS_OWNED_HANGAR(PLAYER_ID()) = INT_TO_ENUM(HANGAR_ID, i)
			POPULATE_MP_SMUGGLER_PROPERTY_PIN(scaleformIndex,i,pagemov)
		ELSE
			IF NOT IS_HANGAR_PURCHASE_DISABLED(INT_TO_ENUM(HANGAR_ID, i))
				POPULATE_MP_SMUGGLER_PROPERTY_PIN(scaleformIndex,i,pagemov)
			ENDIF
		ENDIF	
	ENDFOR
	
	POPULATE_OWNED_GANG_OPS_DEFUNCT_BASE(pagemov, bReInitGlobals)
	
	scaleformIndex = 61
	//	Base ids are in the range 0 - 9 (add 40 to the id)
	FOR i = ENUM_TO_INT(DEFUNCT_BASE_1) TO (ENUM_TO_INT(DEFUNCT_BASE_ID_COUNT)-1)
		CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_GANG_OPS_PROPERTY_PIN populating for defunct base: ", i)		//, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
		IF GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID()) = INT_TO_ENUM(DEFUNCT_BASE_ID, i)
			POPULATE_MP_GANG_OPS_PROPERTY_PIN(scaleformIndex,i,pagemov)
		ELSE
			IF NOT IS_DEFUNCT_BASE_PURCHASE_DISABLED(INT_TO_ENUM(DEFUNCT_BASE_ID, i))
				POPULATE_MP_GANG_OPS_PROPERTY_PIN(scaleformIndex,i,pagemov)
			ENDIF
		ENDIF	
	ENDFOR
	
	POPULATE_OWNED_BUSINESS_BATTLES_NIGHTCLUB(pagemov, bReInitGlobals)
	
	scaleformIndex = 71
	//	Nightclub ids are in the range 0 - 9 (add 10 to the id)
	FOR i = ENUM_TO_INT(NIGHTCLUB_LA_MESA) TO (ENUM_TO_INT(NIGHTCLUB_ID_COUNT)-1)
		CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_BUSINESS_BATTLES_PROPERTY_PIN populating for nightclub: ", i)		//, " g_iPropertyIDStoredInWebSlot[i - 87] ", g_iPropertyIDStoredInWebSlot[i - 87])
		IF GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID()) = INT_TO_ENUM(NIGHTCLUB_ID, i)
			POPULATE_MP_BUSINESS_BATTLES_PROPERTY_PIN(scaleformIndex,i,pagemov)
		ELSE
			IF NOT IS_NIGHTCLUB_PURCHASE_DISABLED(INT_TO_ENUM(NIGHTCLUB_ID, i))
				POPULATE_MP_BUSINESS_BATTLES_PROPERTY_PIN(scaleformIndex,i,pagemov)
			ENDIF
		ENDIF	
	ENDFOR
	
	#IF FEATURE_CASINO_HEIST
	IF CAN_PLAYER_PURCHASE_ARCADE()
		POPULATE_OWNED_ARCADE(pagemov, bReInitGlobals)
		
		scaleformIndex = 91
		
		// Arcade ids are in the range 1 - 5 (add 60 to the id)
		FOR i = ENUM_TO_INT(ARCADE_PROPERTY_PALETO_BAY) TO (ENUM_TO_INT(ARCADE_PROPERTY_ID_COUNT) - 1)
			CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_ARCADE_PROPERTY_PIN populating for Arcade: ", i)
			
			IF GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID()) = INT_TO_ENUM(ARCADE_PROPERTY_ID, i)
				POPULATE_MP_ARCADE_PROPERTY_PIN(scaleformIndex, i, pagemov)
			ELSE
				IF NOT IS_ARCADE_PURCHASE_DISABLED(INT_TO_ENUM(ARCADE_PROPERTY_ID, i))
					POPULATE_MP_ARCADE_PROPERTY_PIN(scaleformIndex, i, pagemov)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	#ENDIF
	
	#IF FEATURE_TUNER
	IF CAN_PLAYER_PURCHASE_AUTO_SHOP()
		POPULATE_OWNED_AUTO_SHOP(pagemov, bReInitGlobals)
		
		scaleformIndex = 101
		
		// Auto Shop ids are in the range 1 - 5 (add 70 to the id)
		FOR i = ENUM_TO_INT(AUTO_SHOP_PROPERTY_LA_MESA) TO (ENUM_TO_INT(AUTO_SHOP_PROPERTY_ID_COUNT) - 1)
			CPRINTLN(DEBUG_INTERNET, "POPULATE_MP_AUTO_SHOP_PROPERTY_PIN populating for Auto Shop: ", i)
			
			IF GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID()) = INT_TO_ENUM(AUTO_SHOP_PROPERTY_ID, i)
				POPULATE_MP_AUTO_SHOP_PROPERTY_PIN(scaleformIndex, i, pagemov)
			ELSE
				IF NOT IS_AUTO_SHOP_PURCHASE_DISABLED(INT_TO_ENUM(AUTO_SHOP_PROPERTY_ID, i))
					POPULATE_MP_AUTO_SHOP_PROPERTY_PIN(scaleformIndex, i, pagemov)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC PLAY_EPSILON_BACKOUT(enumCharacterList c, structPedsForConversation &spfc)
	
	CPRINTLN(DEBUG_INTERNET, "PLAY_EPSILON_BACKOUT: Looking for backout line for char ", c)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		EXIT
	ENDIF

	// This is in EPSWEB.dstar under EPSW_BACKF and EPSW_BACKT 
	PED_INDEX p = PLAYER_PED_ID()
	IF IS_ENTITY_DEAD(p)
		EXIT
	ENDIF
	
	SWITCH c
		CASE CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(spfc, 1, p, "FRANKLIN")
			CPRINTLN(DEBUG_INTERNET, "PLAY_EPSILON_BACKOUT: trying to create conversation")
			WHILE NOT CREATE_CONVERSATION(spfc,
							"EPSWAUD",
							"EPSW_BACKF",
							CONV_PRIORITY_AMBIENT_LOW,
							DISPLAY_SUBTITLES,
							DO_NOT_ADD_TO_BRIEF_SCREEN)
				WAIT(0)
			ENDWHILE				
		BREAK
		CASE CHAR_TREVOR
			
			ADD_PED_FOR_DIALOGUE(spfc,2,p,"TREVOR")
			CPRINTLN(DEBUG_INTERNET, "PLAY_EPSILON_BACKOUT: Trying to create conversation!")
			WHILE NOT CREATE_CONVERSATION(spfc,
							"EPSWAUD",
							"EPSW_BACKT",
							CONV_PRIORITY_AMBIENT_LOW,
							DISPLAY_SUBTITLES,
							DO_NOT_ADD_TO_BRIEF_SCREEN)
				WAIT(0)
			ENDWHILE
		BREAK
	ENDSWITCH
ENDPROC

PROC SF_EPSILON_FAIL(EPSILON_FAIL_PAGE_REASON reason)
	
	g_eEpsilonFailReason = reason
	SWITCH reason
		
		CASE EPSILON_IP_INFRINGE
			IF NOT IS_PLAYER_DEAD(GET_PLAYER_INDEX())
				SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(),5)
			ENDIF
			GO_TO_WEBSITE("WWW_EPSILONPROGRAM_COM_S_ERROR")
		BREAK

		CASE EPSILON_OUT_OF_STOCK
			GO_TO_WEBSITE("WWW_EPSILONPROGRAM_COM_S_TRANSACTION_D_FAILED")
		BREAK
		
		CASE EPSILON_SUPPRESSIVE
			GO_TO_WEBSITE("WWW_EPSILONPROGRAM_COM_S_TRANSACTION_D_FAILED")
		BREAK
		
		DEFAULT
			GO_TO_WEBSITE("WWW_EPSILONPROGRAM_COM_S_ERROR")
		BREAK
	ENDSWITCH
ENDPROC

#IF NOT IS_JAPANESE_BUILD
PROC DO_HUSHSMUSH(SCALEFORM_INDEX pagemov, INT pageID)

	CPRINTLN(DEBUG_INTERNET, "Hushsmush page ", pageID)
	UNUSED_PARAMETER(pagemov)

	SWITCH pageID
		CASE 1
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
//					IF IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_PAID_MICHAEL))
//						CPRINTLN(DEBUG_INTERNET, "Trying to make sign-in button go straight to results...")
//						BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // Don't forward to payment page
//						END_SCALEFORM_MOVIE_METHOD()
//						
//						BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
//						END_SCALEFORM_MOVIE_METHOD()
//					ENDIF
				BREAK
				CASE CHAR_TREVOR
//					IF IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_PAID_TREVOR))
//						CPRINTLN(DEBUG_INTERNET, "Trying to make sign-in button go straight to results...")
//						BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // Don't forward to payment page
//						END_SCALEFORM_MOVIE_METHOD()
//						
//						BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
//						END_SCALEFORM_MOVIE_METHOD()
//					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//				GO_TO_WEBSITE("WWW_HUSHSMUSH_COM_S_PURCHASE_D_ERROR")
//			ENDIF
		BREAK
		
		DEFAULT
		BREAK
	ENDSWITCH

ENDPROC

PROC CHECK_HUSHSMUSH_CLICK(INT pageID, INT buttonID)

	CPRINTLN(DEBUG_INTERNET, "Hushsmush click on page ", pageID, ", button ", buttonID)

	SWITCH pageID
		CASE 2
			SWITCH buttonID
				CASE 1
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL
//							IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_PAID_MICHAEL))
//								IF GET_TOTAL_CASH(CHAR_MICHAEL) > 500
//									DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 500)
//									SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_PAID_MICHAEL))
//								ELSE
//									GO_TO_WEBSITE("WWW_HUSHSMUSH_COM_S_PURCHASE_D_ERROR")
//								ENDIF
//							ENDIF
						BREAK
						CASE CHAR_TREVOR
//							IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_PAID_TREVOR))
//								IF GET_TOTAL_CASH(CHAR_TREVOR) > 500
//									DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 500)
//									SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_PAID_TREVOR))
//								ELSE
//									GO_TO_WEBSITE("WWW_HUSHSMUSH_COM_S_PURCHASE_D_ERROR")
//								ENDIF
//							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 7
			SWITCH buttonID
				CASE 6 // Foxymama/Amanda
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//						IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_MAILED_FOXYMAMA))
////							WHILE NOT REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER(EMAIL_HUSHSMUSH_FOXYMAMA, CT_AMBIENT, BIT_MICHAEL, CHAR_ABIGAIL)
////								WAIT(0)
////							ENDWHILE
//							SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(HUSHSMUSH_MAILED_FOXYMAMA))
//						ENDIF
//					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC
#ENDIF

/// PURPOSE:
///    Header containing the dynamic site filling procedures which is called by app_internet
/// PARAMS:
///    siteID - 
///    pageID - 
///    pagemov - 
///    bUpdateEyeFindNewsStory - fix to override if the news story updates
PROC SF_UPDATE_DYNAMIC_SITE_CONTENT(WEBSITE_INDEX_ENUM siteID, int pageID, SCALEFORM_INDEX pagemov, MISSION_REPEAT_INFO &sRepeatInfo, structPedsForConversation &spfc,  BOOL bUpdateEyeFindNewsStory = TRUE)
	//B* 2187071, 2297766, 2288147:
	//This should prevent Scaleform from being spammed with commands every frame but can add a delay (WEB_BROWSER_PAGE_REFRESH_RATE ms)
	//The effect is that you can sometimes see a blank page for a few frames (4-5) before it gets filled with information
	//Previously, if a new update was requested very soon after the original update (i.e. Bawsaq Portfolio being empty, redirecting to pageID 11 = NO STOCKS)
	//		it would remain blank/not updated until the page was reloaded or auto-refreshed
	//Now, every time a refresh with an invalid time triggers, the page is forced to update next frame again.
	//This way, a scaleform update should be forced as soon as the timer has ended and we no longer need site and page-based exceptions
	IF GET_GAME_TIMER() > g_iTimeLastBrowserRefresh + WEB_BROWSER_PAGE_REFRESH_RATE

		CPRINTLN(DEBUG_INTERNET, "SF_UPDATE_DYNAMIC_SITE_CONTENT: Site = ", GET_WEBSITE_FROM_INDEX(siteID), " Page = ", pageID, " time = ", GET_GAME_TIMER() - (g_iTimeLastBrowserRefresh + WEB_BROWSER_PAGE_REFRESH_RATE))

		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		//url:bugstar:5519332 - Player can edit the Arena War website link to become "undefined" and change the image by composing an email in the internet phone app. Making the link not function until they re open the internet.
		IF (g_eWebSiteIndexFeedback != WWW_EYEFIND_INFO)
		OR (g_iWebPageIndexFeedback != 7 AND g_iWebPageIndexFeedback != 8)
			CPRINTLN(DEBUG_INTERNET, "SF_UPDATE_DYNAMIC_SITE_CONTENT ", GET_WEBSITE_FROM_INDEX(g_eWebSiteIndexFeedback), " triggered with webpageID ", g_iWebPageIndexFeedback, ", call SET_DATA_SLOT_EMPTY")
			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov, "SET_DATA_SLOT_EMPTY")
			END_SCALEFORM_MOVIE_METHOD()
		ELSE
			CPRINTLN(DEBUG_INTERNET, "SF_UPDATE_DYNAMIC_SITE_CONTENT ", GET_WEBSITE_FROM_INDEX(g_eWebSiteIndexFeedback), " triggered with webpageID ", g_iWebPageIndexFeedback, ", dont call SET_DATA_SLOT_EMPTY")
		ENDIF
		
		g_iWebPageIndexFeedback = pageID
		g_eWebSiteIndexFeedback = siteID
		
		//-----------------------------------------------------
		// Check for player backing out of Epsilon presentation
		IF g_iEpsilonTutorialStage <> -1 
			IF siteID <> WWW_EPSILONPROGRAM_COM
			OR (siteID = WWW_EPSILONPROGRAM_COM AND pageID <> 14)
				CPRINTLN(DEBUG_INTERNET, "EPSILON: Presentation has been aborted!")
				KILL_ANY_CONVERSATION()
				g_iEpsilonTutorialStage = -1
			ENDIF
		ENDIF
		
		// Website handling
		SWITCH siteID
			
			CASE WWW_NOT_EXIST
			BREAK
					
			CASE WWW_EYEFIND_INFO
				SF_EYEFIND(pagemov, pageID, sRepeatInfo, bUpdateEyeFindNewsStory)
			BREAK
	 		
			CASE WWW_BAWSAQ_COM
				g_bOnlineMarket = TRUE
				SF_BAWSAQ(pagemov, pageID)
			BREAK
			
			CASE WWW_EPSILONPROGRAM_COM
				
				IF !g_bInMultiplayer
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					
						CASE CHAR_FRANKLIN
							PLAY_EPSILON_BACKOUT(CHAR_FRANKLIN, spfc)
						BREAK
							
						CASE CHAR_TREVOR	
							PLAY_EPSILON_BACKOUT(CHAR_TREVOR, spfc)
						BREAK
					ENDSWITCH
				ENDIF

				g_bCultSiteVisitedSinceReset = TRUE
				SF_EPSILON(pagemov, pageID)
			BREAK
			
			CASE WWW_LCN_D_EXCHANGE_COM
				g_bOnlineMarket = FALSE
				//IF NOT g_bLifeInvaderCrashPage
					SF_BAWSAQ(pagemov, pageID)
				//ELSE
				//	SF_BAWSAQ_LCN_LFI_CRASH(pagemov)
				//ENDIF
			BREAK
			
			CASE WWW_BLEETER_COM
				SF_BLEETER(pagemov, pageID, sRepeatInfo)
			BREAK
			
			CASE WWW_LIFEINVADER_COM
				IF bIsInternetTextSupported
					DO_LIFEINVADER(pagemov, bIsInternetTextSupported)
				ELSE
					CPRINTLN(DEBUG_INTERNET, "Broken text for Lifeinvader! Error out!")
					GO_TO_WEBSITE("WWW_EYEFIND_INFO_S_ERROR")
				ENDIF
			BREAK
			
			#IF NOT IS_JAPANESE_BUILD
			CASE WWW_HUSHSMUSH_COM
				DO_HUSHSMUSH(pagemov, pageID)
			BREAK
			#ENDIF
			
			// Banking
			CASE WWW_MAZE_D_BANK_COM
				IF g_bInMultiplayer
					SF_BANK_UPDATE_MP(pagemov, pageID)
				ELSE
					SF_BANK_UPDATE(pagemov, pageID)
				ENDIF
			BREAK
			CASE WWW_FLEECA_COM
			CASE WWW_THEBANKOFLIBERTY_COM
				SF_BANK_UPDATE(pagemov, pageID)
			BREAK
		
			CASE WWW_EYEFIND_INFO_S_SEARCH
			BREAK
			
			CASE WWW_EYEFIND_INFO_S_ERROR 
			BREAK
		ENDSWITCH
		
		g_iTimeLastBrowserRefresh = GET_GAME_TIMER()
	ELSE
		CPRINTLN(DEBUG_INTERNET, "SF_UPDATE_DYNAMIC_SITE_CONTENT invalid time: Site = ", GET_WEBSITE_FROM_INDEX(siteID), " Page = ", pageID, " time = ", GET_GAME_TIMER() - (g_iTimeLastBrowserRefresh + WEB_BROWSER_PAGE_REFRESH_RATE))
		g_bForceBrowserRefresh = TRUE	//B* 2288147: Force the page to update next frame, or as soon as possible
	ENDIF
ENDPROC

FUNC BOOL BACK_BUTTON_INTERCEPT(WEBSITE_INDEX_ENUM siteID, SCALEFORM_INDEX pagemov)
	
	CPRINTLN(DEBUG_INTERNET, "INTERNET - BACK BUTTON INTERCEPT \"", GET_WEBSITE_FROM_INDEX(siteID), "\"")	
	SWITCH siteID
		CASE WWW_BLEETER_COM
			RETURN BLEETER_BACK_BUTTON_INTERCEPT()
		BREAK
		CASE WWW_LIFEINVADER_COM
			RETURN LFI_BACK_BUTTON_INTERCEPT(pagemov)
		BREAK
	ENDSWITCH

	CPRINTLN(DEBUG_INTERNET, "BACK_BUTTON_INTERCEPT: FALSE!")	
	RETURN FALSE
ENDFUNC
