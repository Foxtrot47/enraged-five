//-	commands headers	-//

//- script headers	-//

//-	public headers	-//

//-	private headers	-//
USING "friendActivity_system_private.sch"
USING "friendActivity_journey_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
#ENDIF


//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//-- Friend activity - Pickup state private functions and data types
//   sam.hackett@rockstarleeds.com
//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	PROC DEBUG_DisplayPickupInfo()
		DEBUG_DisplayFriendInfo(gActivity.mFriendA, 0)
		DEBUG_DisplayFriendInfo(gActivity.mFriendB, 2)
//		DEBUG_DisplayGlobalFriendIDs(20)
	ENDPROC
#ENDIF

//---------------------------------------------------------------------------------------------------
//-- Pickup - Switching/basic functions
//---------------------------------------------------------------------------------------------------

PROC Private_GrabMemberPeds()
	Private_SyncFriendPed(gActivity.mPlayer)
	Private_SyncFriendPed(gActivity.mFriendA)
	Private_SyncFriendPed(gActivity.mFriendB)
ENDPROC

FUNC BOOL Private_HandleSwitching()

	IF Private_IsPlayerSwitching()
		
		// If player char changes...
		enumCharacterList eActualPlayerChar = GET_CURRENT_PLAYER_PED_ENUM()
		
		IF gActivity.ePlayerChar <> eActualPlayerChar
			
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 tOld = GetLabel_enumFriend(GET_FRIEND_FROM_CHAR(gActivity.ePlayerChar))
				TEXT_LABEL_63 tNew = GetLabel_enumFriend(GET_FRIEND_FROM_CHAR(eActualPlayerChar))
				CPRINTLN(DEBUG_FRIENDS, " --- SWITCH    (", tOld, " -> ", tNew, ") ---")
			#ENDIF
			
			// Stop blocking scenarios at pickup point
			Private_ClearFriendScenarioBlocking(gActivity.mPlayer)
			Private_ClearFriendScenarioBlocking(gActivity.mFriendA)
			Private_ClearFriendScenarioBlocking(gActivity.mFriendB)
			
			// Player will have travelled, so update their pickup location to nearest one in use
			Private_UpdateFriendPickupLocToNearest(gActivity.mPlayer)

			// Switch members
			Private_SortMembers(FALSE)
			Private_GrabMemberPeds()
			
			// Ensure the two friends will never try to occupy the same waiting spot
			Private_SetFriendPickupLocOffset(gActivity.mFriendA, 0)
			Private_SetFriendPickupLocOffset(gActivity.mFriendB, 1)
			
			// Enable blocking scenarios at pickup point
			Private_SetFriendScenarioBlocking(gActivity.mPlayer)
			Private_SetFriendScenarioBlocking(gActivity.mFriendA)
			Private_SetFriendScenarioBlocking(gActivity.mFriendB)
			
			// Mark as switched
			gActivity.ePlayerChar = eActualPlayerChar
			gActivity.bUpdateAfterSwitch = TRUE

		ENDIF

	ELSE
		// Grab any instances of the peds that get made by anyone else
		Private_GrabMemberPeds()
		
		// Update switch overrides after switch ends
		IF gActivity.bUpdateAfterSwitch
			Private_ClearFriendSwitchOverride(gActivity.mPlayer)
			Private_SetFriendSwitchOverride(gActivity.mFriendA)
			Private_SetFriendSwitchOverride(gActivity.mFriendB)

			CPRINTLN(DEBUG_FRIENDS, " --- END SWITCH ---")
			gActivity.bUpdateAfterSwitch = FALSE

			IF Private_IsPlayerThirdParty()
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL Private_AreAnyFriendsPickedUp()
	IF gActivity.mPlayer.bWasPickedUp
	OR gActivity.mFriendA.bWasPickedUp
	OR gActivity.mFriendB.bWasPickedUp
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Pickup - Main loop
//---------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Update function for the "Pickup" state in the friend activity
/// PARAMS:
///    pickup - global data for the pickup state
/// RETURNS:
///    If the buddy has been successfully picked up.
PROC PROCESS_PICKUP_STATE()

	Private_ClearActivityFailReason()
	
	//-- Check for non-starters...
	//----------------------------
	// TODO: Need to check if player is in a taxi?

	
	//-- If replay (as in playback) system requested clear entities...
	IF REPLAY_SYSTEM_HAS_REQUESTED_A_SCRIPT_CLEANUP()
	
		Private_ClearObjective()
		Private_SetActivityFailReason(FAF_PlaybackAbort)
		EXIT
	
	//-- Check if replay (as in retry failed mission) has been initiated
	ELIF IS_REPLAY_BEING_PROCESSED()

		Private_ClearObjective()
		IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			Private_SetActivityFailReason(FAF_RetryAbort)
		ENDIF

	//-- If player is dead/arrested...
	ELIF NOT IS_PLAYER_PLAYING(PLAYER_ID())
	
		Private_ClearObjective()
		Private_SetActivityFailReason(FAF_PlayerDeathArrest)
	

	//-- If player has started prep mission...
	ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)		// BBUDDIES REPLACED
		
		Private_ClearObjective()
		
		Private_SetDialogueState(gActivity.mDialogue, FDIALOGUE_SQUAD)
		Private_SetDialogueIdleState(gActivity.mDialogue, FDIALOGUE_SQUAD_IDLE)

		gActivity.eLogFailCharA = NO_CHARACTER
		gActivity.eLogFailCharB = NO_CHARACTER

		Private_RejectMembersForMission(FALSE, FAP_REJECTION_OK, TRUE)
		Private_SetActivityState(ACTIVITY_STATE_Trapped)
	
	
	//-- If player has started squad-allowed mission...
//	ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)	// BBUDDIES REMOVED
//		
//		Private_ClearObjective()
//		
//		SET_SQUAD_MISSION_TO_CURRENT_ZONE()
//		Private_TransferMembersToSquad()
//		Private_SetActivityState(ACTIVITY_STATE_SquadMission)
	
	
	//-- If player has called soldiers...
//	ELIF ARE_ANY_SQUAD_CONNECTIONS_PENDING_OR_ACTIVE()				// BBUDDIES REMOVED
//		
//		Private_ClearObjective()
//		
//		Private_TransferMembersToSquad()
//		Private_SetActivityState(ACTIVITY_STATE_SquadRoaming)
	
	
	//-- If knockout scene has started...
	ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_GRIEFING)
		
		Private_ClearObjective()
//		Private_ClearDropoffScenarioBlocking()

		Private_DeleteMembersForGriefing()
		Private_SetActivityFailReason(FAF_PlayerOnMission)
	

	//-- If player has started story mission...
	ELIF    IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_SWITCH)
		
		Private_ClearObjective()
		
		Private_RejectMembersForMission(FALSE)
		Private_SetActivityFailReason(FAF_PlayerOnMission)
	

	//-- If player has switched to third party...
	ELIF NOT Private_HandleSwitching()

		CPRINTLN(DEBUG_FRIENDS, "FriendActivity: Player switched to third party")
		
		// Remove friends
		Private_CleanupFriend(gActivity.mPlayer, MC_Release)
		Private_CleanupFriend(gActivity.mFriendA, MC_Release)
		Private_CleanupFriend(gActivity.mFriendB, MC_Release)

		Private_ClearObjective()
		Private_SetActivityFailReason(FAF_PlayerSwitch)
		EXIT
	

	//-- Waiting to get mission flag...
	ELIF Private_AreAnyFriendsPickedUp() AND NOT Private_SetCurrentlyOnFriendMission(gActivity.iCandidateID)
		
		DISABLE_SELECTOR_THIS_FRAME()
		EXIT
	

	ELIF Private_LoadFriendPickupResources()


		//-- Process activity members
		Private_ProcessAddFriends(FALSE)
		Private_ProcessMembers()
		Private_ProcessRemoveMembers()
		
		Private_ProcessSystem()
		
		#IF IS_DEBUG_BUILD
			DEBUG_DisplayPickupInfo()
		#ENDIF
		
		// Change follow dist based on if player is normal/in interior/in fight
		Private_UpdateFriendGroupSpacing()

		//-- Set objective
		
		IF Private_AreAnyFriendsPickedUp()
			IF gActivity.bRestoreLocationBlips = FALSE
				Private_BackupLocationBlips()
				gActivity.bRestoreLocationBlips = TRUE
			ENDIF
		ENDIF
		
		IF Private_IsPlayerSwitching()								// Is switching
			
			Private_ClearObjective()
			
		ELIF NOT Private_AreAnyMembersValid()						// Are friends failed
		
			Private_ClearObjective()
			IF Private_AreAllMembersRemoved()
				Private_SetActivityFailReason(FAF_MemberFail)
			ENDIF
		
		ELIF (gActivity.mFriendA.eState = FRIEND_LOST OR gActivity.mFriendA.eState = FRIEND_FAIL_LOST OR gActivity.mFriendA.eState = FRIEND_PLUMMET)	// Any friend lost
		OR   (gActivity.mFriendB.eState = FRIEND_LOST OR gActivity.mFriendB.eState = FRIEND_FAIL_LOST OR gActivity.mFriendB.eState = FRIEND_PLUMMET)
		
			Private_DisplayObjective_Lost()
		
		ELIF gActivity.mPlayer.eState = FRIEND_PICKUP				// Any friend waiting to be picked up
		OR   gActivity.mFriendA.eState = FRIEND_PICKUP
		OR   gActivity.mFriendB.eState = FRIEND_PICKUP
		
			Private_DisplayObjective_Pickup()
		
		ELSE														// All friends have been picked up, proceed to next stage
			Private_ClearObjective()
			Private_SortMembers(TRUE)
			
			Private_SetActivityState(ACTIVITY_STATE_Journey)
		ENDIF

	ENDIF

ENDPROC

// TODO: Make sure anims (and text?) are released on friendActivity.sc cleanup

		
