//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   savegame_private.sch                                        //
//      AUTHOR          :   Kenneth Ross                                                //
//      DESCRIPTION     :   Contains a pre-savegame routine that should be called 		//
//                          before any savegame is made.								//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "email_public.sch"
USING "snapshot_private.sch"
USING "commands_player.sch"
USING "flow_private_core.sch"

/// PURPOSE: Returns TRUE if it is safe to save the game
FUNC BOOL SAFE_TO_SAVE_GAME(BOOL bIsQuicksave = FALSE)

	// Player death check
	IF IS_PED_INJURED(PLAYER_PED_ID())
		PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - player injured")
		RETURN FALSE
	ENDIF
	
	// Player alive checks
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
	OR IS_PED_SHOOTING(PLAYER_PED_ID())
	OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
	OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
	OR IS_PED_BEING_JACKED(PLAYER_PED_ID())
	OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
	OR IS_PED_GETTING_UP(PLAYER_PED_ID())
	OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
	//OR IS_PED_IN_COVER(PLAYER_PED_ID())
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())
	OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
	
		#IF IS_DEBUG_BUILD
//			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
//			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				IF (NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - player control is not on")
				ELIF (NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - not ready for cutscene")
				ELIF (IS_PED_SHOOTING(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - shooting")
				ELIF (IS_PED_IN_COMBAT(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - in combat")
				ELIF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - in melee combat")
				ELIF (IS_PED_BEING_JACKED(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - being jacked")
				ELIF (IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - getting into vehicle")
				ELIF (IS_PED_GETTING_UP(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - getting up")
				ELIF (IS_ENTITY_IN_AIR(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - in air")
				//ELIF (IS_PED_IN_COVER(PLAYER_PED_ID()))
				//	PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - in cover")
				ELIF (IS_PED_RAGDOLL(PLAYER_PED_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - ragdolling")
				ELIF (IS_PLAYER_BEING_ARRESTED(PLAYER_ID()))
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - arrested")
				ELSE
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - some other player fail reason???")
				ENDIF
//			ENDIF
		#ENDIF
		PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - A")
		RETURN FALSE
	ENDIF
	
	// Game checks
	IF IS_TRANSITION_ACTIVE()
	OR g_sSelectorUI.bOnScreen
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR IS_MEMORY_CARD_IN_USE()
	OR IS_AUTO_SAVE_IN_PROGRESS()	
	OR Is_Player_Timetable_Scene_In_Progress()
	OR IS_RESULT_SCREEN_DISPLAYING()
		
		#IF IS_DEBUG_BUILD
//			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
//			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				IF (IS_TRANSITION_ACTIVE())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - transition active")
				ELIF (g_sSelectorUI.bOnScreen)
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - switch ui on screen")
				ELIF (IS_PLAYER_PED_SWITCH_IN_PROGRESS())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - switch in progress")
				ELIF (IS_MEMORY_CARD_IN_USE())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - mem card in use")
				ELIF (IS_AUTO_SAVE_IN_PROGRESS())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - auto saving")
				ELIF (Is_Player_Timetable_Scene_In_Progress())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - timetable scene in progress")
				ELIF (IS_RESULT_SCREEN_DISPLAYING())
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - results screen displaying")
				ELSE 
					PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - some other game fail reason???")
				ENDIF
//			ENDIF
		#ENDIF
		PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - B")
		RETURN FALSE
	ENDIF
	
	//None quicksave checks.
	IF NOT bIsQuicksave
		IF GET_MISSION_FLAG()
		
			#IF IS_DEBUG_BUILD
//				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
//				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
//				OR g_bPlayerLockedInToTrigger
					IF (GET_MISSION_FLAG())
						PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - on mission")
					ELIF (g_bPlayerLockedInToTrigger)
						PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - locked into trigger")
					ELSE 
						PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - some other game fail reason???")
					ENDIF
//				ENDIF
			#ENDIF
			PRINTLN("SAFE_TO_SAVE_GAME() = FALSE - C")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// No problems found
	RETURN TRUE
ENDFUNC

/// PURPOSE: Save current command details for each mission strand. When the game is reloaded 
/// we will check that strand command pointers are pointing to matching commands. If not, the 
/// flow must have been changed between save and load and we will have to fall back on failsafe 
/// loading technique.
PROC Pre_Savegame_Store_Current_Strand_Command_Hash_IDs()

	#if USE_CLF_DLC
		INT iStrandIndex
		REPEAT MAX_STRANDS_CLF iStrandIndex
			g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandHashID = GET_FLOW_COMMAND_HASH_ID(g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos)
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		INT iStrandIndex
		REPEAT MAX_STRANDS_NRM iStrandIndex
			g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandHashID = GET_FLOW_COMMAND_HASH_ID(g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos)
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		INT iStrandIndex
		REPEAT MAX_STRANDS iStrandIndex
			g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandHashID = GET_FLOW_COMMAND_HASH_ID(g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos)
		ENDREPEAT
	#endif
	#endif
ENDPROC

/// PURPOSE: Save all the currents player details such as ammo, health, armour etc.
PROC Pre_Savegame_Store_Current_Player_Data()
	STORE_PLAYER_PED_INFO(PLAYER_PED_ID(), TRUE)
	STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID()) // Variations now have to be stored manually.
	
	// Update spawn coords for player vehicle.
	INT i
	REPEAT NUM_PLAYER_VEHICLE_IDS i
		IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			
			BOOL bInGarage = FALSE
			INT iGarage = 0
			TEXT_LABEL_31 tlGarageName = ""
			VECTOR vCoords = <<0,0,0>>
			SAVEHOUSE_NAME_ENUM eSavehouse = GET_CLOSEST_SAVEHOUSE(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_eCreatedPlayerVehiclePed[i], TRUE)
						
#if USE_CLF_DLC	
			IF eSavehouse != NUMBER_OF_CLF_SAVEHOUSE
			AND eSavehouse != SAVEHOUSEclf_TREVOR_VB
			AND eSavehouse != SAVEHOUSEclf_TREVOR_CS
			AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[i])
			AND IS_VEHICLE_ON_ALL_WHEELS(g_viCreatedPlayerVehicleIDs[i])
				WHILE NOT bInGarage
				AND GET_PLAYER_GARAGE_DATA(g_eCreatedPlayerVehiclePed[i], iGarage, tlGarageName, eSavehouse)
					IF IS_VEHICLE_IN_GARAGE_AREA(tlGarageName, g_viCreatedPlayerVehicleIDs[i])
						bInGarage = TRUE
						
						// We need to force the z-coord so that the vehicle doesnt appear on the roof when we use
						// the SET_VEHICLE_ON_GROUND_PROPERLY command.
						vCoords = GET_ENTITY_COORDS(g_viCreatedPlayerVehicleIDs[i], FALSE)
						UPDATE_VEHICLE_COORDS_FOR_PLAYERS_GARGE(g_eCreatedPlayerVehiclePed[i], iGarage, vCoords)
					ENDIF
					iGarage++
				ENDWHILE
			ENDIF
			IF bInGarage
				PRINTLN("VEHGEN - Updating default spawn coords for player vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i])))
				IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
					g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = vCoords
					g_savedGlobalsClifford.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				ELIF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
					g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = vCoords
					g_savedGlobalsClifford.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				ENDIF
			ELSE
				PRINTLN("VEHGEN - Resetting default spawn coords for player vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i])))
				// Reset
				IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
					PRINTLN("VEHGEN: Resetting default spawn position for CAR")
					g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					g_savedGlobalsClifford.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = 0.0
				ELIF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
					PRINTLN("VEHGEN: Resetting default spawn position for BIKE")
					g_savedGlobalsClifford.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					g_savedGlobalsClifford.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = 0.0
				ENDIF
			ENDIF
#endif
#if USE_NRM_DLC
			IF eSavehouse != NUMBER_OF_NRM_SAVEHOUSE
			AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[i])
			AND IS_VEHICLE_ON_ALL_WHEELS(g_viCreatedPlayerVehicleIDs[i])
				WHILE NOT bInGarage
				AND GET_PLAYER_GARAGE_DATA(g_eCreatedPlayerVehiclePed[i], iGarage, tlGarageName, eSavehouse)
					IF IS_VEHICLE_IN_GARAGE_AREA(tlGarageName, g_viCreatedPlayerVehicleIDs[i])
						bInGarage = TRUE
						
						// We need to force the z-coord so that the vehicle doesnt appear on the roof when we use
						// the SET_VEHICLE_ON_GROUND_PROPERLY command.
						vCoords = GET_ENTITY_COORDS(g_viCreatedPlayerVehicleIDs[i], FALSE)
						UPDATE_VEHICLE_COORDS_FOR_PLAYERS_GARGE(g_eCreatedPlayerVehiclePed[i], iGarage, vCoords)
					ENDIF
					iGarage++
				ENDWHILE
			ENDIF
			IF bInGarage
				PRINTLN("VEHGEN - Updating default spawn coords for player vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i])))
				IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
					g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = vCoords
					g_savedGlobalsnorman.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				ELIF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
					g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = vCoords
					g_savedGlobalsnorman.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				ENDIF
			ELSE
				PRINTLN("VEHGEN - Resetting default spawn coords for player vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i])))
				// Reset
				IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
					PRINTLN("VEHGEN: Resetting default spawn position for CAR")
					g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					g_savedGlobalsnorman.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = 0.0
				ELIF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
					PRINTLN("VEHGEN: Resetting default spawn position for BIKE")
					g_savedGlobalsnorman.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					g_savedGlobalsnorman.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = 0.0
				ENDIF
			ENDIF
#endif
		
#if not USE_CLF_DLC
#if not USE_NRM_DLC
			IF eSavehouse != NUMBER_OF_SAVEHOUSE_LOCATIONS
			AND eSavehouse != SAVEHOUSE_TREVOR_VB
			AND eSavehouse != SAVEHOUSE_TREVOR_CS
			AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[i])
			AND IS_VEHICLE_ON_ALL_WHEELS(g_viCreatedPlayerVehicleIDs[i])
				WHILE NOT bInGarage
				AND GET_PLAYER_GARAGE_DATA(g_eCreatedPlayerVehiclePed[i], iGarage, tlGarageName, eSavehouse)
					IF IS_VEHICLE_IN_GARAGE_AREA(tlGarageName, g_viCreatedPlayerVehicleIDs[i])
						bInGarage = TRUE
						
						// We need to force the z-coord so that the vehicle doesnt appear on the roof when we use
						// the SET_VEHICLE_ON_GROUND_PROPERLY command.
						vCoords = GET_ENTITY_COORDS(g_viCreatedPlayerVehicleIDs[i], FALSE)
						UPDATE_VEHICLE_COORDS_FOR_PLAYERS_GARGE(g_eCreatedPlayerVehiclePed[i], iGarage, vCoords)
					ENDIF
					iGarage++
				ENDWHILE
			ENDIF
			IF bInGarage
				PRINTLN("VEHGEN - Updating default spawn coords for player vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i])))
				IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
					g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = vCoords
					g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				ELIF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
					g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = vCoords
					g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				ENDIF
			ELSE
				PRINTLN("VEHGEN - Resetting default spawn coords for player vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i])))
				// Reset
				IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
					PRINTLN("VEHGEN: Resetting default spawn position for CAR")
					g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = 0.0
				ELIF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
					PRINTLN("VEHGEN: Resetting default spawn position for BIKE")
					g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = 0.0
				ENDIF
			ENDIF
#endif
#endif
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE: Run any procs before we save the game.
PROC PERFORM_PRE_SAVEGAME_ROUTINE(BOOL bIsAnAutosave, BOOL bOverrideSaveHouse, BOOL bSaveWillLoadIntoMission = FALSE)
	//Store email data
	SAVE_EMAIL_SYSTEM_STATE()
	
	//Compute and store current mission flow command hash IDs.
	//Used for error checking and flow state repairing on loading.
	Pre_Savegame_Store_Current_Strand_Command_Hash_IDs()
	
	//Store current players variations, ammo, health etc.
	Pre_Savegame_Store_Current_Player_Data()
	
	//Save out a bitflag to communicate to missions that they have been launched
	//directly from loading a save.
	
#if USE_CLF_DLC
	IF bSaveWillLoadIntoMission
		SET_BIT(g_savedGlobalsClifford.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)
	ENDIF	
	g_savedGlobalsClifford.sCommsControlData.iCommsGameTime = GET_GAME_TIMER()	
	
	//Fix for 1640548: If this isn't an autosave make sure the was faded flags are cleared. 
	//These can lead to a savegame that will never fade in.
	IF NOT bIsAnAutosave
		PRINTLN("PERFORM_PRE_SAVEGAME_ROUTINE: Clearing was faded flags before making a none autosave save.")
		g_sAutosaveData.bIgnoreScreenFade = FALSE
		g_savedGlobalsClifford.sFlowCustom.wasFadedOut = FALSE
		g_savedGlobalsClifford.sFlowCustom.wasFadedOut_switch = FALSE
	ENDIF	
	VECTOR vPos
	FLOAT fHeading
	OVERRIDE_SAVE_HOUSE(bOverrideSaveHouse, g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.vPos, g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.fHeading, bIsAnAutosave, vPos, fHeading)
#endif
	
#if USE_NRM_DLC
	IF bSaveWillLoadIntoMission
		SET_BIT(g_savedGlobalsnorman.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)
	ENDIF	
	g_savedGlobalsnorman.sCommsControlData.iCommsGameTime = GET_GAME_TIMER()	
	
	//Fix for 1640548: If this isn't an autosave make sure the was faded flags are cleared. 
	//These can lead to a savegame that will never fade in.
	IF NOT bIsAnAutosave
		PRINTLN("PERFORM_PRE_SAVEGAME_ROUTINE: Clearing was faded flags before making a none autosave save.")
		g_sAutosaveData.bIgnoreScreenFade = FALSE
		g_savedGlobalsnorman.sFlowCustom.wasFadedOut = FALSE
		g_savedGlobalsnorman.sFlowCustom.wasFadedOut_switch = FALSE
	ENDIF	
	VECTOR vPos
	FLOAT fHeading
	OVERRIDE_SAVE_HOUSE(bOverrideSaveHouse, g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.vPos, g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.fHeading, bIsAnAutosave, vPos, fHeading)
#endif

#if not USE_CLF_DLC
#if not use_NRM_dlc
	IF bSaveWillLoadIntoMission
		SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)
	ELSE
		CLEAR_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)
	ENDIF	
	g_savedGlobals.sCommsControlData.iCommsGameTime = GET_GAME_TIMER()	
	// if bOverrideSaveHouse is true this is a repeat play or save anywhere save.
	// we then override the savehouse with the snapshot data
	// if bOverrideSaveHouse is false, calling this command ensures the savehouse is not overridden	
	IF g_eRunningMission = SP_MISSION_TREVOR_1
	OR g_eMissionRunningOnMPSwitchStart = SP_MISSION_TREVOR_1
		//Trevor isn't given a savehouse until after Trevor1.
		Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, TRUE)
	ENDIF	
	//Fix for 1640548: If this isn't an autosave make sure the was faded flags are cleared. 
	//These can lead to a savegame that will never fade in.
	IF NOT bIsAnAutosave
		PRINTLN("PERFORM_PRE_SAVEGAME_ROUTINE: Clearing was faded flags before making a none autosave save.")
		g_sAutosaveData.bIgnoreScreenFade = FALSE
		g_savedGlobals.sFlowCustom.wasFadedOut = FALSE
		g_savedGlobals.sFlowCustom.wasFadedOut_switch = FALSE
	ENDIF	
	VECTOR vPos
	FLOAT fHeading
	OVERRIDE_SAVE_HOUSE(bOverrideSaveHouse, g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos, g_savedGlobals.sRepeatPlayData.mPlayerStruct.fHeading, bIsAnAutosave, vPos, fHeading)
#endif
#endif

ENDPROC
#if USE_CLF_DLC
PROC DO_MP_TRANSITION_STARTING_SAVECLF()

	IF GET_IS_AUTO_SAVE_OFF()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: Autosaves are turned off.")
		EXIT
	ENDIF

	IF IS_AUTO_SAVE_IN_PROGRESS()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: The game is current saving.")
		EXIT
	ENDIF
	
	IF IS_MEMORY_CARD_IN_USE()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: The memory card is in use.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[STRAND_CLF].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: Singleplayer gameflow not initialised.")
		EXIT
	ENDIF
	
	PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Clearing was faded flags as a safety measure.")
	g_sAutosaveData.bIgnoreScreenFade = FALSE
	g_savedGlobalsClifford.sFlowCustom.wasFadedOut = FALSE
	g_savedGlobalsClifford.sFlowCustom.wasFadedOut_switch = FALSE

	//Check to see if any strands are sitting on DO_MISSION_NOW flow commands.
	//If they are we need to set the wasFadedOut flag to enure the screen doesn't fade in
	//on loading until after the mission is running.
	INT iStrandIndex
	BOOL bMissionAutostartFound = FALSE
	REPEAT MAX_STRANDS_CLF iStrandIndex
		INT iCommandPos = g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
		IF iCommandPos != ILLEGAL_ARRAY_POSITION
			IF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_NOW
				PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Considering setting SET_FADE_IN_AFTER_LOAD as strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " is on a DO_MISSION_NOW_COMMAND.")
			
				//Only set this flag if this strand doesn't have a save command
				//point override set.
				BOOL bOverrideSetOnStrand = FALSE
				INT iOverrideIndex
				REPEAT g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides iOverrideIndex
					IF ENUM_TO_INT(g_savedGlobalsClifford.sFlowCustom.strandToOverride[iOverrideIndex]) = iStrandIndex
						IF NOT g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[iOverrideIndex]
							PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " had a save override set. Backing out of setting SET_FADE_IN_AFTER_LOAD.")
							bOverrideSetOnStrand = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
		
				IF NOT bOverrideSetOnStrand
					PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] SET_FADE_IN_AFTER_LOAD set for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), ".")
					SET_FADE_IN_AFTER_LOAD(FALSE)
					g_savedGlobalsClifford.sFlowCustom.wasFadedOut = TRUE
					g_savedGlobalsClifford.sFlowCustom.wasFadedOut_switch = FALSE
					bMissionAutostartFound = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	PERFORM_PRE_SAVEGAME_ROUTINE(TRUE, FALSE, bMissionAutostartFound)
	CLEAR_REPLAY_STATS()//these will have been processed at this point
	DO_AUTO_SAVE()
ENDPROC
#endif
#if USE_NRM_DLC
PROC DO_MP_TRANSITION_STARTING_SAVENRM()

	IF GET_IS_AUTO_SAVE_OFF()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: Autosaves are turned off.")
		EXIT
	ENDIF

	IF IS_AUTO_SAVE_IN_PROGRESS()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: The game is current saving.")
		EXIT
	ENDIF
	
	IF IS_MEMORY_CARD_IN_USE()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: The memory card is in use.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[STRAND_NRM_SURVIVE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: Singleplayer gameflow not initialised.")
		EXIT
	ENDIF
	
	PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Clearing was faded flags as a safety measure.")
	g_sAutosaveData.bIgnoreScreenFade = FALSE
	g_savedGlobalsnorman.sFlowCustom.wasFadedOut = FALSE
	g_savedGlobalsnorman.sFlowCustom.wasFadedOut_switch = FALSE

	//Check to see if any strands are sitting on DO_MISSION_NOW flow commands.
	//If they are we need to set the wasFadedOut flag to enure the screen doesn't fade in
	//on loading until after the mission is running.
	INT iStrandIndex
	BOOL bMissionAutostartFound = FALSE
	REPEAT MAX_STRANDS_NRM iStrandIndex
		INT iCommandPos = g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
		IF iCommandPos != ILLEGAL_ARRAY_POSITION
			IF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_NOW
				PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Considering setting SET_FADE_IN_AFTER_LOAD as strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " is on a DO_MISSION_NOW_COMMAND.")
			
				//Only set this flag if this strand doesn't have a save command
				//point override set.
				BOOL bOverrideSetOnStrand = FALSE
				INT iOverrideIndex
				REPEAT g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides iOverrideIndex
					IF ENUM_TO_INT(g_savedGlobalsnorman.sFlowCustom.strandToOverride[iOverrideIndex]) = iStrandIndex
						IF NOT g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[iOverrideIndex]
							PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " had a save override set. Backing out of setting SET_FADE_IN_AFTER_LOAD.")
							bOverrideSetOnStrand = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
		
				IF NOT bOverrideSetOnStrand
					PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] SET_FADE_IN_AFTER_LOAD set for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), ".")
					SET_FADE_IN_AFTER_LOAD(FALSE)
					g_savedGlobalsnorman.sFlowCustom.wasFadedOut = TRUE
					g_savedGlobalsnorman.sFlowCustom.wasFadedOut_switch = FALSE
					bMissionAutostartFound = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	PERFORM_PRE_SAVEGAME_ROUTINE(TRUE, FALSE, bMissionAutostartFound)
	CLEAR_REPLAY_STATS()//these will have been processed at this point
	DO_AUTO_SAVE()
ENDPROC
#endif
PROC DO_MP_TRANSITION_STARTING_SAVE()
#if USE_CLF_DLC
	DO_MP_TRANSITION_STARTING_SAVECLF()
#endif
#if USE_NRM_DLC
	DO_MP_TRANSITION_STARTING_SAVENRM()
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF GET_IS_AUTO_SAVE_OFF()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: Autosaves are turned off.")
		EXIT
	ENDIF

	IF IS_AUTO_SAVE_IN_PROGRESS()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: The game is current saving.")
		EXIT
	ENDIF
	
	IF IS_MEMORY_CARD_IN_USE()
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: The memory card is in use.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Skipped: Singleplayer gameflow not initialised.")
		EXIT
	ENDIF
	
	PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Clearing was faded flags as a safety measure.")
	g_sAutosaveData.bIgnoreScreenFade = FALSE
	g_savedGlobals.sFlowCustom.wasFadedOut = FALSE
	g_savedGlobals.sFlowCustom.wasFadedOut_switch = FALSE

	//Check to see if any strands are sitting on DO_MISSION_NOW flow commands.
	//If they are we need to set the wasFadedOut flag to enure the screen doesn't fade in
	//on loading until after the mission is running.
	INT iStrandIndex
	BOOL bMissionAutostartFound = FALSE
	REPEAT MAX_STRANDS iStrandIndex
		INT iCommandPos = g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
		IF iCommandPos != ILLEGAL_ARRAY_POSITION
			IF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_NOW
				PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Considering setting SET_FADE_IN_AFTER_LOAD as strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " is on a DO_MISSION_NOW_COMMAND.")
			
				//Only set this flag if this strand doesn't have a save command
				//point override set.
				BOOL bOverrideSetOnStrand = FALSE
				INT iOverrideIndex
				REPEAT g_savedGlobals.sFlowCustom.numberStoredOverrides iOverrideIndex
					IF ENUM_TO_INT(g_savedGlobals.sFlowCustom.strandToOverride[iOverrideIndex]) = iStrandIndex
						IF NOT g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[iOverrideIndex]
							PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] Strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " had a save override set. Backing out of setting SET_FADE_IN_AFTER_LOAD.")
							bOverrideSetOnStrand = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
		
				IF NOT bOverrideSetOnStrand
					PRINTLN("[DO_MP_TRANSITION_STARTING_SAVE] SET_FADE_IN_AFTER_LOAD set for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), ".")
					SET_FADE_IN_AFTER_LOAD(FALSE)
					g_savedGlobals.sFlowCustom.wasFadedOut = TRUE
					g_savedGlobals.sFlowCustom.wasFadedOut_switch = FALSE
					bMissionAutostartFound = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	PERFORM_PRE_SAVEGAME_ROUTINE(TRUE, FALSE, bMissionAutostartFound)
	CLEAR_REPLAY_STATS()//these will have been processed at this point
	DO_AUTO_SAVE()
#endif
#endif
ENDPROC


