//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	savegame_bed_data.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains the data for each savegame bed. 	//
//							Each savegame bed is linked directly to a savehouse, so we 	//
//							obtain the data by finding out which savehouse is closest. 	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"
USING "respawn_location_private.sch"
USING "lineactivation.sch"

ENUM ALARM_CLOCK_SOUND_ENUM
	ALARMCLOCK_BELL = 0,
	ALARMCLOCK_COWBELL,
	ALARMCLOCK_ELECTRONIC
ENDENUM

STRUCT SAVEGAME_BED_DATA_STRUCT
	
	SAVEHOUSE_NAME_ENUM eSavehouse
	enumCharacterList	eCharacter
	
	// Bed
	VECTOR vBedCoords
	
	// Locate
	VECTOR vAngledAreaCoords[2]
	FLOAT fAngledAreaWidth
	
	// Camera
	VECTOR vCameraCoords
	VECTOR vCameraRot
	FLOAT fCameraFOV
			
	// Scene
	VECTOR vSceneCoords
	VECTOR vSceneRot
	
	// Anims
	TEXT_LABEL_31 tlAnimDict 		//= "SAVE"
	TEXT_LABEL_31 tlGetIn
//	TEXT_LABEL_31 tlGetInLoop		//#1457212
	TEXT_LABEL_31 tlSleepLoop
	TEXT_LABEL_31 tlGetOut
	TEXT_LABEL_31 tlGetOutLoop
	
	// Player vehicle
	VECTOR vVehicleCoords
	FLOAT fVehicleHeading
	
	// Garage
	VECTOR vGarageDoorAngledArea1[2]
	VECTOR vGarageDoorAngledArea2[2]
	FLOAT fGarageDoorAngledAreaWidth[2]
	DOOR_NAME_ENUM eGarageDoor
	VECTOR vGarageAngledArea1[2]
	VECTOR vGarageAngledArea2[2]
	FLOAT fGarageAngledAreaWidth[2]
	VECTOR vGarageCamCoord
	VECTOR vGarageCarmRot
	FLOAT fGarageCamFOV
	BOOL bUseGarageCamera
	
	// Alarm clock
	ALARM_CLOCK_SOUND_ENUM eAlarm
	
	// Health pickups
	VECTOR vPickupCoords[NUM_OF_SAVEHOUSE_HEALTH_PICKUPS]
	VECTOR vPickupRot[NUM_OF_SAVEHOUSE_HEALTH_PICKUPS]
	INT iPickupIndex
	
	BOOL bDataSet
ENDSTRUCT


/// PURPOSE: Fills the specified struct will the savegame bed information and returns TRUE if successful
#if USE_CLF_DLC
FUNC BOOL GET_SAVEGAME_BED_DATACLF(SAVEGAME_BED_DATA_STRUCT &sData, VECTOR coords)

	SAVEGAME_BED_DATA_STRUCT sTempData
	sTempData.eCharacter = NO_CHARACTER
	
	SAVEHOUSE_NAME_ENUM eSavehouse = GET_CLOSEST_SAVEHOUSE(coords, NO_CHARACTER)
	enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
	
	// Determine shared savehouse...
	IF eSavehouse = SAVEHOUSEclf_TREVOR_CS
	AND eCurrentPed = CHAR_MICHAEL
		eSavehouse = SAVEHOUSEclf_MICHAEL_CS
	ELIF eSavehouse = SAVEHOUSEclf_MICHAEL_CS
	AND eCurrentPed = CHAR_TREVOR
		eSavehouse = SAVEHOUSEclf_TREVOR_CS
	ENDIF
	
	SWITCH eSavehouse
	
		CASE SAVEHOUSEclf_MICHAEL_BH
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_MICHAEL
			sTempData.eAlarm 				= ALARMCLOCK_BELL
			sTempData.vBedCoords 			= <<-814.1559, 180.8351, 76.1180>>
			
			sTempData.vVehicleCoords 		= << -826.4690, 176.2739, 70.0134 >>
			sTempData.fVehicleHeading 		= 148.9042
			
			sTempData.vAngledAreaCoords[0] 	= <<-814.763123,182.370819,77.766884>>
			sTempData.vAngledAreaCoords[1] 	= <<-813.658203,179.313690,75.740707>>
			sTempData.fAngledAreaWidth 		= 3.5
			
			sTempData.vCameraCoords			= <<-811.316162,177.880157,78.008293>>
			sTempData.vCameraRot			= <<-20.310999,0.227106,36.416462>>
			sTempData.fCameraFOV			= 36.740002
			
			sTempData.iPickupIndex 			= 0
			//sTempData.vPickupCoords[0]		= << -800.864, 183.964, 72.620 >>
			//sTempData.vPickupRot[0]			= <<0,0,173.000>>
			sTempData.vPickupCoords[0]		= << -795.843, 184.631, 72.708 >>
			sTempData.vPickupRot[0]			= <<0,0,104.500>>
			
			sTempData.vPickupCoords[1]		= << -804.898, 170.260, 77.00 >>
			sTempData.vPickupRot[1]			= <<0,0,19.750>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSEclf_FRANKLIN_SC
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_FRANKLIN
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= << -18.3267, -1441.9580, 30.1015 >>
			
			sTempData.vVehicleCoords 		= << -25.2539, -1445.4873, 29.6541 >>
			sTempData.fVehicleHeading 		= 178.5042
			
			sTempData.vAngledAreaCoords[0] 	= <<-16.819046,-1441.693481,30.101543>>
			sTempData.vAngledAreaCoords[1]	= <<-19.441185,-1441.722168,31.851543>> 
			sTempData.fAngledAreaWidth 		= 2.5
			
			sTempData.vCameraCoords			= << -16.3154, -1442.2847, 32.2352 >>
			sTempData.vCameraRot			= << -26.6736, 0.0000, 64.9884 >>
			sTempData.fCameraFOV			= 55.2750
			
			sTempData.iPickupIndex 			= 1
			sTempData.vPickupCoords[0]		= << -17.920, -1436.340, 30.193 >>
			sTempData.vPickupRot[0]			= <<0,0,201.250>>
			
			sTempData.vPickupCoords[1]		= << -12.598, -1427.605, 31.545 >>
			sTempData.vPickupRot[1]			= <<0,0,176>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSEclf_FRANKLIN_VH
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_FRANKLIN
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= <<-1.14, 524.20, 169.62>>
			
			sTempData.vVehicleCoords 		= << 14.4699, 547.8997, 175.1420 >>
			sTempData.fVehicleHeading 		= 346.8435
			
			sTempData.vAngledAreaCoords[0] 	= <<0.373280,522.836060,169.617157>>
			sTempData.vAngledAreaCoords[1]	= <<-1.187521,526.093933,171.617157>> 
			sTempData.fAngledAreaWidth 		= 2.625000
			
			sTempData.vCameraCoords			= <<-0.2781, 527.3559, 171.1478>>
			sTempData.vCameraRot			= <<-6.5671, 0.0000, 158.0865>>
			sTempData.fCameraFOV			= 45.0
			
			sTempData.iPickupIndex 			= 2
			sTempData.vPickupCoords[0]		= << -7.443, 516.836, 174.669 >>
			sTempData.vPickupRot[0]			= <<0,0,98.000>>
			
			sTempData.vPickupCoords[1]		= << -0.248, 527.069, 170.093 >>
			sTempData.vPickupRot[1]			= <<0,0,47.000>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSEclf_TREVOR_CS
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_TREVOR
			sTempData.eAlarm 				= ALARMCLOCK_COWBELL
			sTempData.vBedCoords 			= <<1968.144287,3816.733398,32.950245>>
			
			sTempData.vVehicleCoords 		= << 1992.8392, 3814.3296, 31.1118 >>
			sTempData.fVehicleHeading 		= 303.7466
			
			sTempData.vAngledAreaCoords[0] 	= <<1969.425903,3817.376709,32.428703>>
			sTempData.vAngledAreaCoords[1] 	= <<1967.377197,3816.176514,34.319530>>
			sTempData.fAngledAreaWidth 		= 2.312500
			
			sTempData.vCameraCoords			= << 1968.9554, 3815.1802, 34.0311 >>
			sTempData.vCameraRot			= << -16.6189, 0.0000, 11.4112 >>
			sTempData.fCameraFOV			= 52.2257
			
			sTempData.iPickupIndex 			= 3 // Same as SAVEHOUSE_MICHAEL_CS
			sTempData.vPickupCoords[0]		= << 1970.418, 3819.392, 32.700 >>
			sTempData.vPickupRot[0]			= <<0,0,44.500>>
			
			// Sleep loop used in Trevor mission.
			sTempData.vSceneCoords 			= <<1968.14, 3816.79, 32.45>>
			sTempData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sTempData.tlSleepLoop			= "t_sleep_loop_countryside"
			sTempData.tlGetOut 				= "T_GetOut_countryside"
			
			// Garage camera
			sTempData.vGarageAngledArea1[0]		= <<1970.505615,3820.223877,31.158207>>
			sTempData.vGarageAngledArea2[0]		= <<1968.218384,3824.195557,34.639156>>
			sTempData.fGarageAngledAreaWidth[0]	= 11.375000
			sTempData.vGarageAngledArea1[1]		= <<1969.072021,3819.370850,31.162352>>
			sTempData.vGarageAngledArea2[1]		= <<1966.650757,3823.269043,34.400070>>
			sTempData.fGarageAngledAreaWidth[1]	= 9.500000
			sTempData.vGarageDoorAngledArea1[0]		= <<1978.007324,3827.255127,31.382586>>
			sTempData.vGarageDoorAngledArea2[0]		= <<1962.736206,3818.507080,34.001640>> 
			sTempData.fGarageDoorAngledAreaWidth[0]	= 5.062500
			sTempData.vGarageDoorAngledArea1[1]		= <<1972.796753,3824.204102,31.340851>>
			sTempData.vGarageDoorAngledArea2[1]		= <<1962.736206,3818.507080,34.001640>>
			sTempData.fGarageDoorAngledAreaWidth[1]	= 5.062500
			sTempData.eGarageDoor 					= DOORNAME_T_TRAILER_CS_G
			
			sTempData.vGarageCamCoord			= <<1976.531860,3826.799805,33.276062>>
			sTempData.vGarageCarmRot			= <<-6.698779,0.000000,117.568207>>
			sTempData.fGarageCamFOV				= 50.0
			sTempData.bUseGarageCamera			= TRUE
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSEclf_MICHAEL_CS
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_MICHAEL
			sTempData.eAlarm 				= ALARMCLOCK_COWBELL
			sTempData.vBedCoords 			= <<1968.144287,3816.733398,32.950245>>
			
			sTempData.vVehicleCoords 		= << 1992.8392, 3814.3296, 31.1118 >>
			sTempData.fVehicleHeading 		= 303.7466
			
			sTempData.vAngledAreaCoords[0] 	= <<1969.425903,3817.376709,32.428703>>
			sTempData.vAngledAreaCoords[1] 	= <<1967.377197,3816.176514,34.319530>>
			sTempData.fAngledAreaWidth 		= 2.312500
			
			sTempData.vCameraCoords			= << 1968.9554, 3815.1802, 34.0311 >>
			sTempData.vCameraRot			= << -16.6189, 0.0000, 11.4112 >>
			sTempData.fCameraFOV			= 52.2257
			
			sTempData.iPickupIndex 			= 3 // Same as SAVEHOUSE_TREVOR_CS
			sTempData.vPickupCoords[0]		= << 1970.418, 3819.392, 32.700 >>
			sTempData.vPickupRot[0]			= <<0,0,44.500>>
			
			// Garage camera
			sTempData.vGarageAngledArea1[0]		= <<1970.505615,3820.223877,31.158207>>
			sTempData.vGarageAngledArea2[0]		= <<1968.218384,3824.195557,34.639156>>
			sTempData.fGarageAngledAreaWidth[0]	= 11.375000
			sTempData.vGarageAngledArea1[1]		= <<1969.072021,3819.370850,31.162352>>
			sTempData.vGarageAngledArea2[1]		= <<1966.650757,3823.269043,34.400070>>
			sTempData.fGarageAngledAreaWidth[1]	= 9.500000
			sTempData.vGarageDoorAngledArea1[0]		= <<1978.007324,3827.255127,31.382586>>
			sTempData.vGarageDoorAngledArea2[0]		= <<1962.736206,3818.507080,34.001640>> 
			sTempData.fGarageDoorAngledAreaWidth[0]	= 5.062500
			sTempData.vGarageDoorAngledArea1[1]		= <<1972.796753,3824.204102,31.340851>>
			sTempData.vGarageDoorAngledArea2[1]		= <<1962.736206,3818.507080,34.001640>>
			sTempData.fGarageDoorAngledAreaWidth[1]	= 5.062500
			sTempData.eGarageDoor 					= DOORNAME_T_TRAILER_CS_G
			
			sTempData.vGarageCamCoord			= <<1976.531860,3826.799805,33.276062>>
			sTempData.vGarageCarmRot			= <<-6.698779,0.000000,117.568207>>
			sTempData.fGarageCamFOV				= 50.0
			sTempData.bUseGarageCamera			= TRUE
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSEclf_TREVOR_VB
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_TREVOR
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= << -1148.39, -1512.35, 10.29 >>
			
			sTempData.vVehicleCoords 		= << -1153.9480, -1518.4862, 3.3665 >>
			sTempData.fVehicleHeading 		= 35.6849
			
			sTempData.vAngledAreaCoords[0] 	= << -1146.976807, -1511.461304, 8.382729 >>
			sTempData.vAngledAreaCoords[1] 	= << -1149.625244, -1513.277222, 11.632729 >> 
			sTempData.fAngledAreaWidth 		= 3.0000

			sTempData.vCameraCoords			= <<-1148.7634, -1514.8335, 10.8467>>
			sTempData.vCameraRot			= <<-6.5825, -0.0000, -11.6316>>
			sTempData.fCameraFOV			= 49.8514
			
			sTempData.iPickupIndex 			= 4
			sTempData.vPickupCoords[0]		= << -1149.982, -1519.517, 10.748 >>
			sTempData.vPickupRot[0]			= <<0,0,120.250>>
						
			sTempData.bDataSet 				= TRUE
		BREAK

		CASE SAVEHOUSEclf_TREVOR_SC
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_TREVOR
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= << 94.5723, -1290.0817, 28.7355 >>
			
			sTempData.vVehicleCoords 		= << 86.0899, -1278.4299, 28.1860 >>
			sTempData.fVehicleHeading 		= 98.3605
			
			sTempData.vAngledAreaCoords[0] 	= <<93.034920,-1290.571045,28.268782>>
			sTempData.vAngledAreaCoords[1] 	= <<95.728226,-1289.030762,30.768782>> 
			sTempData.fAngledAreaWidth 		= 2.500000
			
			sTempData.vCameraCoords			= << 97.9946, -1288.9171, 29.0447 >>
			sTempData.vCameraRot			= << 0.8789, 0.0000, 119.7997 >>
			sTempData.fCameraFOV			= 36.7654
			
			sTempData.iPickupIndex 			= 5
			sTempData.vPickupCoords[0]		= << 92.874, -1292.829, 28.359 >>
			sTempData.vPickupRot[0]			= <<0,0,310.250>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF sTempData.bDataSet
			PRINTSTRING("\nUsing data set for savehouse: ")PRINTSTRING(Get_Savehouse_Respawn_Name(sTempData.eSavehouse))PRINTNL()
		ELSE
			PRINTSTRING("\nUnable to find suitable savegame bed data.")PRINTNL()
		ENDIF
	#ENDIF
	
	IF sTempData.bDataSet
		sData = sTempData
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL GET_SAVEGAME_BED_DATANRM(SAVEGAME_BED_DATA_STRUCT &sData, VECTOR coords)

	SAVEGAME_BED_DATA_STRUCT sTempData
	sTempData.eCharacter = NO_CHARACTER
	
	SAVEHOUSE_NAME_ENUM eSavehouse = GET_CLOSEST_SAVEHOUSE(coords, NO_CHARACTER)
	enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
		
	SWITCH eSavehouse
	
		CASE SAVEHOUSENRM_BH
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_MICHAEL
			sTempData.eAlarm 				= ALARMCLOCK_BELL
			sTempData.vBedCoords 			= <<-814.1559, 180.8351, 76.1180>>
			
			sTempData.vVehicleCoords 		= << -826.4690, 176.2739, 70.0134 >>
			sTempData.fVehicleHeading 		= 148.9042
			
			sTempData.vAngledAreaCoords[0] 	= <<-814.763123,182.370819,77.766884>>
			sTempData.vAngledAreaCoords[1] 	= <<-813.658203,179.313690,75.740707>>
			sTempData.fAngledAreaWidth 		= 3.5
			
			sTempData.vCameraCoords			= <<-811.316162,177.880157,78.008293>>
			sTempData.vCameraRot			= <<-20.310999,0.227106,36.416462>>
			sTempData.fCameraFOV			= 36.740002
			
			sTempData.iPickupIndex 			= 0
			//sTempData.vPickupCoords[0]		= << -800.864, 183.964, 72.620 >>
			//sTempData.vPickupRot[0]			= <<0,0,173.000>>
			sTempData.vPickupCoords[0]		= << -795.843, 184.631, 72.708 >>
			sTempData.vPickupRot[0]			= <<0,0,104.500>>
			
			sTempData.vPickupCoords[1]		= << -804.898, 170.260, 77.00 >>
			sTempData.vPickupRot[1]			= <<0,0,19.750>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSENRM_CHATEAU
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_MICHAEL
			sTempData.eAlarm 				= ALARMCLOCK_BELL
			sTempData.vBedCoords 			= <<-89.4, 366.2, 112.5>>
			
			sTempData.vVehicleCoords 		= << -71,349.7,111.8 >>
			sTempData.fVehicleHeading 		= 113
			
			sTempData.vAngledAreaCoords[0] 	= <<-89.75105, 372.17017, 115.53381>>
			sTempData.vAngledAreaCoords[1] 	= <<-82.72226, 368.81714, 111.46375>>
			sTempData.fAngledAreaWidth 		= 3.5
			
			sTempData.vCameraCoords			= <<-79.3,361.8,113.6>>
			sTempData.vCameraRot			= <<-3.6,0,43.9>>
			sTempData.fCameraFOV			= 50
						
			sTempData.bDataSet 				= TRUE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF sTempData.bDataSet
			PRINTSTRING("\nUsing data set for savehouse: ")PRINTSTRING(Get_Savehouse_Respawn_Name(sTempData.eSavehouse))PRINTNL()
		ELSE
			PRINTSTRING("\nUnable to find suitable savegame bed data.")PRINTNL()
		ENDIF
	#ENDIF
	
	IF sTempData.bDataSet
		sData = sTempData
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC
#endif

FUNC BOOL GET_SAVEGAME_BED_DATA(SAVEGAME_BED_DATA_STRUCT &sData, VECTOR coords)
#if USE_CLF_DLC
	return GET_SAVEGAME_BED_DATACLF(sData, coords)
#endif
#if USE_NRM_DLC
	return GET_SAVEGAME_BED_DATANRM(sData, coords)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC

	SAVEGAME_BED_DATA_STRUCT sTempData
	sTempData.eCharacter = NO_CHARACTER
	
	SAVEHOUSE_NAME_ENUM eSavehouse = GET_CLOSEST_SAVEHOUSE(coords, NO_CHARACTER)
	enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
	
	// Determine shared savehouse...
	IF eSavehouse = SAVEHOUSE_TREVOR_CS
	AND eCurrentPed = CHAR_MICHAEL
		eSavehouse = SAVEHOUSE_MICHAEL_CS
	ELIF eSavehouse = SAVEHOUSE_MICHAEL_CS
	AND eCurrentPed = CHAR_TREVOR
		eSavehouse = SAVEHOUSE_TREVOR_CS
	ENDIF
	
	SWITCH eSavehouse
	
		CASE SAVEHOUSE_MICHAEL_BH
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_MICHAEL
			sTempData.eAlarm 				= ALARMCLOCK_BELL
			sTempData.vBedCoords 			= <<-814.1559, 180.8351, 76.1180>>
			
			sTempData.vVehicleCoords 		= << -826.4690, 176.2739, 70.0134 >>
			sTempData.fVehicleHeading 		= 148.9042
			
			sTempData.vAngledAreaCoords[0] 	= <<-814.763123,182.370819,77.766884>>
			sTempData.vAngledAreaCoords[1] 	= <<-813.658203,179.313690,75.740707>>
			sTempData.fAngledAreaWidth 		= 3.5
			
			sTempData.vCameraCoords			= <<-811.316162,177.880157,78.008293>>
			sTempData.vCameraRot			= <<-20.310999,0.227106,36.416462>>
			sTempData.fCameraFOV			= 36.740002
			
			sTempData.iPickupIndex 			= 0
			//sTempData.vPickupCoords[0]		= << -800.864, 183.964, 72.620 >>
			//sTempData.vPickupRot[0]			= <<0,0,173.000>>
			sTempData.vPickupCoords[0]		= << -795.843, 184.631, 72.708 >>
			sTempData.vPickupRot[0]			= <<0,0,104.500>>
			
			sTempData.vPickupCoords[1]		= << -804.898, 170.260, 77.00 >>
			sTempData.vPickupRot[1]			= <<0,0,19.750>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSE_FRANKLIN_SC
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_FRANKLIN
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= << -18.3267, -1441.9580, 30.1015 >>
			
			sTempData.vVehicleCoords 		= << -25.2539, -1445.4873, 29.6541 >>
			sTempData.fVehicleHeading 		= 178.5042
			
			sTempData.vAngledAreaCoords[0] 	= <<-16.819046,-1441.693481,30.101543>>
			sTempData.vAngledAreaCoords[1]	= <<-19.441185,-1441.722168,31.851543>> 
			sTempData.fAngledAreaWidth 		= 2.5
			
			sTempData.vCameraCoords			= << -16.3154, -1442.2847, 32.2352 >>
			sTempData.vCameraRot			= << -26.6736, 0.0000, 64.9884 >>
			sTempData.fCameraFOV			= 55.2750
			
			sTempData.iPickupIndex 			= 1
			sTempData.vPickupCoords[0]		= << -17.920, -1436.340, 30.193 >>
			sTempData.vPickupRot[0]			= <<0,0,201.250>>
			
			sTempData.vPickupCoords[1]		= << -12.598, -1427.605, 31.545 >>
			sTempData.vPickupRot[1]			= <<0,0,176>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSE_FRANKLIN_VH
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_FRANKLIN
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= <<-1.14, 524.20, 169.62>>
			
			sTempData.vVehicleCoords 		= << 14.4699, 547.8997, 175.1420 >>
			sTempData.fVehicleHeading 		= 346.8435
			
			sTempData.vAngledAreaCoords[0] 	= <<0.373280,522.836060,169.617157>>
			sTempData.vAngledAreaCoords[1]	= <<-1.187521,526.093933,171.617157>> 
			sTempData.fAngledAreaWidth 		= 2.625000
			
			sTempData.vCameraCoords			= <<-0.2781, 527.3559, 171.1478>>
			sTempData.vCameraRot			= <<-6.5671, 0.0000, 158.0865>>
			sTempData.fCameraFOV			= 45.0
			
			sTempData.iPickupIndex 			= 2
			sTempData.vPickupCoords[0]		= << -7.443, 516.836, 174.669 >>
			sTempData.vPickupRot[0]			= <<0,0,98.000>>
			
			sTempData.vPickupCoords[1]		= << -0.248, 527.069, 170.093 >>
			sTempData.vPickupRot[1]			= <<0,0,47.000>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSE_TREVOR_CS
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_TREVOR
			sTempData.eAlarm 				= ALARMCLOCK_COWBELL
			sTempData.vBedCoords 			= <<1968.144287,3816.733398,32.950245>>
			
			sTempData.vVehicleCoords 		= << 1992.8392, 3814.3296, 31.1118 >>
			sTempData.fVehicleHeading 		= 303.7466
			
			sTempData.vAngledAreaCoords[0] 	= <<1969.425903,3817.376709,32.428703>>
			sTempData.vAngledAreaCoords[1] 	= <<1967.377197,3816.176514,34.319530>>
			sTempData.fAngledAreaWidth 		= 2.312500
			
			sTempData.vCameraCoords			= << 1968.9554, 3815.1802, 34.0311 >>
			sTempData.vCameraRot			= << -16.6189, 0.0000, 11.4112 >>
			sTempData.fCameraFOV			= 52.2257
			
			sTempData.iPickupIndex 			= 3 // Same as SAVEHOUSE_MICHAEL_CS
			sTempData.vPickupCoords[0]		= << 1970.418, 3819.392, 32.700 >>
			sTempData.vPickupRot[0]			= <<0,0,44.500>>
			
			// Sleep loop used in Trevor mission.
			sTempData.vSceneCoords 			= <<1968.14, 3816.79, 32.45>>
			sTempData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sTempData.tlSleepLoop			= "t_sleep_loop_countryside"
			sTempData.tlGetOut 				= "T_GetOut_countryside"
			
			// Garage camera
			sTempData.vGarageAngledArea1[0]		= <<1970.505615,3820.223877,31.158207>>
			sTempData.vGarageAngledArea2[0]		= <<1968.218384,3824.195557,34.639156>>
			sTempData.fGarageAngledAreaWidth[0]	= 11.375000
			sTempData.vGarageAngledArea1[1]		= <<1969.072021,3819.370850,31.162352>>
			sTempData.vGarageAngledArea2[1]		= <<1966.650757,3823.269043,34.400070>>
			sTempData.fGarageAngledAreaWidth[1]	= 9.500000
			sTempData.vGarageDoorAngledArea1[0]		= <<1978.007324,3827.255127,31.382586>>
			sTempData.vGarageDoorAngledArea2[0]		= <<1962.736206,3818.507080,34.001640>> 
			sTempData.fGarageDoorAngledAreaWidth[0]	= 5.062500
			sTempData.vGarageDoorAngledArea1[1]		= <<1972.796753,3824.204102,31.340851>>
			sTempData.vGarageDoorAngledArea2[1]		= <<1962.736206,3818.507080,34.001640>>
			sTempData.fGarageDoorAngledAreaWidth[1]	= 5.062500
			sTempData.eGarageDoor 					= DOORNAME_T_TRAILER_CS_G
			
			sTempData.vGarageCamCoord			= <<1976.531860,3826.799805,33.276062>>
			sTempData.vGarageCarmRot			= <<-6.698779,0.000000,117.568207>>
			sTempData.fGarageCamFOV				= 50.0
			sTempData.bUseGarageCamera			= TRUE
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSE_MICHAEL_CS
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_MICHAEL
			sTempData.eAlarm 				= ALARMCLOCK_COWBELL
			sTempData.vBedCoords 			= <<1968.144287,3816.733398,32.950245>>
			
			sTempData.vVehicleCoords 		= << 1992.8392, 3814.3296, 31.1118 >>
			sTempData.fVehicleHeading 		= 303.7466
			
			sTempData.vAngledAreaCoords[0] 	= <<1969.425903,3817.376709,32.428703>>
			sTempData.vAngledAreaCoords[1] 	= <<1967.377197,3816.176514,34.319530>>
			sTempData.fAngledAreaWidth 		= 2.312500
			
			sTempData.vCameraCoords			= << 1968.9554, 3815.1802, 34.0311 >>
			sTempData.vCameraRot			= << -16.6189, 0.0000, 11.4112 >>
			sTempData.fCameraFOV			= 52.2257
			
			sTempData.iPickupIndex 			= 3 // Same as SAVEHOUSE_TREVOR_CS
			sTempData.vPickupCoords[0]		= << 1970.418, 3819.392, 32.700 >>
			sTempData.vPickupRot[0]			= <<0,0,44.500>>
			
			// Garage camera
			sTempData.vGarageAngledArea1[0]		= <<1970.505615,3820.223877,31.158207>>
			sTempData.vGarageAngledArea2[0]		= <<1968.218384,3824.195557,34.639156>>
			sTempData.fGarageAngledAreaWidth[0]	= 11.375000
			sTempData.vGarageAngledArea1[1]		= <<1969.072021,3819.370850,31.162352>>
			sTempData.vGarageAngledArea2[1]		= <<1966.650757,3823.269043,34.400070>>
			sTempData.fGarageAngledAreaWidth[1]	= 9.500000
			sTempData.vGarageDoorAngledArea1[0]		= <<1978.007324,3827.255127,31.382586>>
			sTempData.vGarageDoorAngledArea2[0]		= <<1962.736206,3818.507080,34.001640>> 
			sTempData.fGarageDoorAngledAreaWidth[0]	= 5.062500
			sTempData.vGarageDoorAngledArea1[1]		= <<1972.796753,3824.204102,31.340851>>
			sTempData.vGarageDoorAngledArea2[1]		= <<1962.736206,3818.507080,34.001640>>
			sTempData.fGarageDoorAngledAreaWidth[1]	= 5.062500
			sTempData.eGarageDoor 					= DOORNAME_T_TRAILER_CS_G
			
			sTempData.vGarageCamCoord			= <<1976.531860,3826.799805,33.276062>>
			sTempData.vGarageCarmRot			= <<-6.698779,0.000000,117.568207>>
			sTempData.fGarageCamFOV				= 50.0
			sTempData.bUseGarageCamera			= TRUE
			
			sTempData.bDataSet 				= TRUE
		BREAK
		
		CASE SAVEHOUSE_TREVOR_VB
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_TREVOR
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= << -1148.39, -1512.35, 10.29 >>
			
			sTempData.vVehicleCoords 		= << -1153.9480, -1518.4862, 3.3665 >>
			sTempData.fVehicleHeading 		= 35.6849
			
			sTempData.vAngledAreaCoords[0] 	= << -1146.976807, -1511.461304, 8.382729 >>
			sTempData.vAngledAreaCoords[1] 	= << -1149.625244, -1513.277222, 11.632729 >> 
			sTempData.fAngledAreaWidth 		= 3.0000

			sTempData.vCameraCoords			= <<-1148.7634, -1514.8335, 10.8467>>
			sTempData.vCameraRot			= <<-6.5825, -0.0000, -11.6316>>
			sTempData.fCameraFOV			= 49.8514
			
			sTempData.iPickupIndex 			= 4
			sTempData.vPickupCoords[0]		= << -1149.982, -1519.517, 10.748 >>
			sTempData.vPickupRot[0]			= <<0,0,120.250>>
						
			sTempData.bDataSet 				= TRUE
		BREAK

		CASE SAVEHOUSE_TREVOR_SC
			sTempData.eSavehouse 			= eSavehouse
			sTempData.eCharacter			= CHAR_TREVOR
			sTempData.eAlarm 				= ALARMCLOCK_ELECTRONIC
			sTempData.vBedCoords 			= << 94.5723, -1290.0817, 28.7355 >>
			
			sTempData.vVehicleCoords 		= << 86.0899, -1278.4299, 28.1860 >>
			sTempData.fVehicleHeading 		= 98.3605
			
			sTempData.vAngledAreaCoords[0] 	= <<93.034920,-1290.571045,28.268782>>
			sTempData.vAngledAreaCoords[1] 	= <<95.728226,-1289.030762,30.768782>> 
			sTempData.fAngledAreaWidth 		= 2.500000
			
			sTempData.vCameraCoords			= << 97.9946, -1288.9171, 29.0447 >>
			sTempData.vCameraRot			= << 0.8789, 0.0000, 119.7997 >>
			sTempData.fCameraFOV			= 36.7654
			
			sTempData.iPickupIndex 			= 5
			sTempData.vPickupCoords[0]		= << 92.874, -1292.829, 28.359 >>
			sTempData.vPickupRot[0]			= <<0,0,310.250>>
			
			sTempData.bDataSet 				= TRUE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF sTempData.bDataSet
			PRINTSTRING("\nUsing data set for savehouse: ")PRINTSTRING(Get_Savehouse_Respawn_Name(sTempData.eSavehouse))PRINTNL()
		ELSE
			PRINTSTRING("\nUnable to find suitable savegame bed data.")PRINTNL()
		ENDIF
	#ENDIF
	
	IF sTempData.bDataSet
		sData = sTempData
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

#endif
#endif
ENDFUNC
/// PURPOSE: Fills the specified struct will the savegame bed information and returns TRUE if successful
#if USE_CLF_DLC	
PROC GET_SAVEGAME_BED_ANIM_DATACLF(SAVEGAME_BED_DATA_STRUCT &sData, BOOL bAltEntry = FALSE)
	
	SWITCH sData.eSavehouse
	
		CASE SAVEHOUSEclf_MICHAEL_BH
			IF bAltEntry
				sData.vSceneCoords 			= << -814.256, 181.063, 75.740 >>
				sData.vSceneRot 			= << 0.000, 0.000, 21.1994 >>
				sData.tlAnimDict			= "SAVEM_Default@"
				sData.tlGetIn 				= "M_GetIn_L"
			//	sData.tlGetInLoop 			= "M_GetIn_L_Loop"
				sData.tlGetOut 				= "M_GetOut_L"
				sData.tlGetOutLoop 			= "M_GetOut_L_Loop"
			ELSE
				sData.vSceneCoords 			= << -814.181, 181.100, 75.740 >>
				sData.vSceneRot 			= << 0.000, 0.000, 21.1994 >>
				sData.tlAnimDict			= "SAVEM_Default@"
				sData.tlGetIn 				= "M_GetIn_R"
			//	sData.tlGetInLoop 			= "M_GetIn_R_Loop"
				sData.tlGetOut 				= "M_GetOut_R"
				sData.tlGetOutLoop 			= "M_GetOut_R_Loop"
			ENDIF
		BREAK
		
		CASE SAVEHOUSEclf_FRANKLIN_SC
			sData.vSceneCoords 			= << -18.18, -1441.60, 30.19 >>
			sData.vSceneRot 			= << 0.000, 0.000, 0.5729 >>
			sData.tlAnimDict			= "SAVEF_Default@"
			sData.tlGetIn 				= "F_GetIn_R"
		//	sData.tlGetInLoop 			= "F_GetIn_R_Loop"
			sData.tlGetOut 				= "F_GetOut_R"
			sData.tlGetOutLoop 			= "F_GetOut_R_Loop"
		BREAK
		
		CASE SAVEHOUSEclf_FRANKLIN_VH
			IF bAltEntry
				sData.vSceneCoords 			= << -1.049, 524.283, 170.064 >>
				sData.vSceneRot 			= << 0.000, 0.000, 24.000 >>
				sData.tlAnimDict			= "SAVEBighouse@"
				sData.tlGetIn 				= "F_GetIn_l_bighouse"
			//	sData.tlGetInLoop 			= "F_GetIn_l_Loop_bighouse"
				sData.tlGetOut 				= "F_GetOut_l_bighouse"
				sData.tlGetOutLoop 			= "F_GetOut_l_Loop_bighouse"
			ELSE
				sData.vSceneCoords 			= << -1.049, 524.283, 170.064 >>
				sData.vSceneRot 			= << 0.000, 0.000, 24.000 >>
				sData.tlAnimDict			= "SAVEBighouse@"
				sData.tlGetIn 				= "F_GetIn_r_bighouse"
			//	sData.tlGetInLoop 			= "F_GetIn_r_Loop_bighouse"
				sData.tlGetOut 				= "F_GetOut_r_bighouse"
				sData.tlGetOutLoop 			= "F_GetOut_r_Loop_bighouse"
			ENDIF
		BREAK
		
		CASE SAVEHOUSEclf_TREVOR_CS
			sData.vSceneCoords 			= <<1968.14, 3816.79, 32.4287>>
			sData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sData.tlAnimDict			= "SAVECountryside@"
			sData.tlGetIn 				= "T_GetIn_countryside"
		//	sData.tlGetInLoop 			= "T_GetIn_Loop_countryside"
			sData.tlGetOut 				= "T_GetOut_countryside"
			sData.tlGetOutLoop 			= "T_GetOut_Loop_countryside"
		BREAK
		
		CASE SAVEHOUSEclf_MICHAEL_CS
			sData.vSceneCoords 			= <<1968.14, 3816.79, 32.4287>>
			sData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sData.tlAnimDict			= "SAVECountryside@"
			sData.tlGetIn 				= "M_GetIn_countryside"
		//	sData.tlGetInLoop 			= "M_GetIn_Loop_countryside"
			sData.tlGetOut 				= "M_GetOut_countryside"
			sData.tlGetOutLoop 			= "M_GetOut_Loop_countryside"
		BREAK
		
		CASE SAVEHOUSEclf_TREVOR_VB
			IF bAltEntry
				sData.vSceneCoords 			= << -1148.126, -1512.222, 9.689 >>
				sData.vSceneRot 			= << 0.000, 0.000, 127.250 >>
				sData.tlAnimDict			= "SAVEVeniceB@"
				sData.tlGetIn 				= "T_GetIn_l_veniceB"
			//	sData.tlGetInLoop 			= "T_GetIn_l_Loop_veniceB"
				sData.tlGetOut 				= "T_GetOut_l_veniceB"
				sData.tlGetOutLoop 			= "T_GetOut_l_Loop_veniceB"
			ELSE
				sData.vSceneCoords 			= << -1148.438, -1512.246, 9.689 >>
				sData.vSceneRot 			= << 0.000, 0.000, 36.250 >>
				sData.tlAnimDict			= "SAVEVeniceB@"
				sData.tlGetIn 				= "T_GetIn_r_veniceB"
			//	sData.tlGetInLoop 			= "T_GetIn_r_Loop_veniceB"
				sData.tlGetOut 				= "T_GetOut_r_veniceB"
				sData.tlGetOutLoop 			= "T_GetOut_r_Loop_veniceB"
			ENDIF
		BREAK

		CASE SAVEHOUSEclf_TREVOR_SC
			sData.vSceneCoords 			= << 94.53, -1289.86, 28.27 >>
			sData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sData.tlAnimDict			= "SAVECouch@"
			sData.tlGetIn 				= "T_GetIn_couch"
		//	sData.tlGetInLoop 			= "T_GetIn_Loop_couch"
			sData.tlGetOut 				= "T_GetOut_couch"
			sData.tlGetOutLoop 			= "T_GetOut_Loop_couch"
		BREAK
	ENDSWITCH
ENDPROC
#endif
#if USE_NRM_DLC
PROC GET_SAVEGAME_BED_ANIM_DATANRM(SAVEGAME_BED_DATA_STRUCT &sData, BOOL bAltEntry = FALSE)

	SWITCH sData.eSavehouse
	
		CASE SAVEHOUSENRM_BH
			IF bAltEntry
				sData.vSceneCoords 			= << -814.256, 181.063, 75.740 >>
				sData.vSceneRot 			= << 0.000, 0.000, 21.1994 >>
				sData.tlAnimDict			= "SAVEM_Default@"
				sData.tlGetIn 				= "M_GetIn_L"
			//	sData.tlGetInLoop 			= "M_GetIn_L_Loop"
				sData.tlGetOut 				= "M_GetOut_L"
				sData.tlGetOutLoop 			= "M_GetOut_L_Loop"
			ELSE
				sData.vSceneCoords 			= << -814.181, 181.100, 75.740 >>
				sData.vSceneRot 			= << 0.000, 0.000, 21.1994 >>
				sData.tlAnimDict			= "SAVEM_Default@"
				sData.tlGetIn 				= "M_GetIn_R"
			//	sData.tlGetInLoop 			= "M_GetIn_R_Loop"
				sData.tlGetOut 				= "M_GetOut_R"
				sData.tlGetOutLoop 			= "M_GetOut_R_Loop"
			ENDIF
		BREAK
		
		CASE SAVEHOUSENRM_CHATEAU
			sData.vSceneCoords 			= << -18.18, -1441.60, 30.19 >>
			sData.vSceneRot 			= << 0.000, 0.000, 0.5729 >>
			sData.tlAnimDict			= "SAVEF_Default@"
			sData.tlGetIn 				= "F_GetIn_R"
		//	sData.tlGetInLoop 			= "F_GetIn_R_Loop"
			sData.tlGetOut 				= "F_GetOut_R"
			sData.tlGetOutLoop 			= "F_GetOut_R_Loop"
		BREAK
		
	ENDSWITCH
ENDPROC
#endif
PROC GET_SAVEGAME_BED_ANIM_DATA(SAVEGAME_BED_DATA_STRUCT &sData, BOOL bAltEntry = FALSE)
	#if USE_CLF_DLC
		GET_SAVEGAME_BED_ANIM_DATACLF(sData,bAltEntry)
		EXIT
	#endif
	#if USE_NRM_DLC
		GET_SAVEGAME_BED_ANIM_DATANRM(sData,bAltEntry)
		EXIT
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
	
	SWITCH sData.eSavehouse
	
		CASE SAVEHOUSE_MICHAEL_BH
			IF bAltEntry
				sData.vSceneCoords 			= << -814.256, 181.063, 75.740 >>
				sData.vSceneRot 			= << 0.000, 0.000, 21.1994 >>
				sData.tlAnimDict			= "SAVEM_Default@"
				sData.tlGetIn 				= "M_GetIn_L"
			//	sData.tlGetInLoop 			= "M_GetIn_L_Loop"
				sData.tlGetOut 				= "M_GetOut_L"
				sData.tlGetOutLoop 			= "M_GetOut_L_Loop"
			ELSE
				sData.vSceneCoords 			= << -814.181, 181.100, 75.740 >>
				sData.vSceneRot 			= << 0.000, 0.000, 21.1994 >>
				sData.tlAnimDict			= "SAVEM_Default@"
				sData.tlGetIn 				= "M_GetIn_R"
			//	sData.tlGetInLoop 			= "M_GetIn_R_Loop"
				sData.tlGetOut 				= "M_GetOut_R"
				sData.tlGetOutLoop 			= "M_GetOut_R_Loop"
			ENDIF
		BREAK
		
		CASE SAVEHOUSE_FRANKLIN_SC
			sData.vSceneCoords 			= << -18.18, -1441.60, 30.19 >>
			sData.vSceneRot 			= << 0.000, 0.000, 0.5729 >>
			sData.tlAnimDict			= "SAVEF_Default@"
			sData.tlGetIn 				= "F_GetIn_R"
		//	sData.tlGetInLoop 			= "F_GetIn_R_Loop"
			sData.tlGetOut 				= "F_GetOut_R"
			sData.tlGetOutLoop 			= "F_GetOut_R_Loop"
		BREAK
		
		CASE SAVEHOUSE_FRANKLIN_VH
			IF bAltEntry
				sData.vSceneCoords 			= << -1.049, 524.283, 170.064 >>
				sData.vSceneRot 			= << 0.000, 0.000, 24.000 >>
				sData.tlAnimDict			= "SAVEBighouse@"
				sData.tlGetIn 				= "F_GetIn_l_bighouse"
			//	sData.tlGetInLoop 			= "F_GetIn_l_Loop_bighouse"
				sData.tlGetOut 				= "F_GetOut_l_bighouse"
				sData.tlGetOutLoop 			= "F_GetOut_l_Loop_bighouse"
			ELSE
				sData.vSceneCoords 			= << -1.049, 524.283, 170.064 >>
				sData.vSceneRot 			= << 0.000, 0.000, 24.000 >>
				sData.tlAnimDict			= "SAVEBighouse@"
				sData.tlGetIn 				= "F_GetIn_r_bighouse"
			//	sData.tlGetInLoop 			= "F_GetIn_r_Loop_bighouse"
				sData.tlGetOut 				= "F_GetOut_r_bighouse"
				sData.tlGetOutLoop 			= "F_GetOut_r_Loop_bighouse"
			ENDIF
		BREAK
		
		CASE SAVEHOUSE_TREVOR_CS
			sData.vSceneCoords 			= <<1968.14, 3816.79, 32.4287>>
			sData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sData.tlAnimDict			= "SAVECountryside@"
			sData.tlGetIn 				= "T_GetIn_countryside"
		//	sData.tlGetInLoop 			= "T_GetIn_Loop_countryside"
			sData.tlGetOut 				= "T_GetOut_countryside"
			sData.tlGetOutLoop 			= "T_GetOut_Loop_countryside"
		BREAK
		
		CASE SAVEHOUSE_MICHAEL_CS
			sData.vSceneCoords 			= <<1968.14, 3816.79, 32.4287>>
			sData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sData.tlAnimDict			= "SAVECountryside@"
			sData.tlGetIn 				= "M_GetIn_countryside"
		//	sData.tlGetInLoop 			= "M_GetIn_Loop_countryside"
			sData.tlGetOut 				= "M_GetOut_countryside"
			sData.tlGetOutLoop 			= "M_GetOut_Loop_countryside"
		BREAK
		
		CASE SAVEHOUSE_TREVOR_VB
			IF bAltEntry
				sData.vSceneCoords 			= << -1148.126, -1512.222, 9.689 >>
				sData.vSceneRot 			= << 0.000, 0.000, 127.250 >>
				sData.tlAnimDict			= "SAVEVeniceB@"
				sData.tlGetIn 				= "T_GetIn_l_veniceB"
			//	sData.tlGetInLoop 			= "T_GetIn_l_Loop_veniceB"
				sData.tlGetOut 				= "T_GetOut_l_veniceB"
				sData.tlGetOutLoop 			= "T_GetOut_l_Loop_veniceB"
			ELSE
				sData.vSceneCoords 			= << -1148.438, -1512.246, 9.634 >>
				sData.vSceneRot 			= << 0.000, 0.000, 36.250 >>
				sData.tlAnimDict			= "SAVEVeniceB@"
				sData.tlGetIn 				= "T_GetIn_r_veniceB"
			//	sData.tlGetInLoop 			= "T_GetIn_r_Loop_veniceB"
				sData.tlGetOut 				= "T_GetOut_r_veniceB"
				sData.tlGetOutLoop 			= "T_GetOut_r_Loop_veniceB"
			ENDIF
		BREAK

		CASE SAVEHOUSE_TREVOR_SC
			sData.vSceneCoords 			= << 94.53, -1289.86, 28.27 >>
			sData.vSceneRot 			= << 0.000, 0.000, 29.7938 >>
			sData.tlAnimDict			= "SAVECouch@"
			sData.tlGetIn 				= "T_GetIn_couch"
		//	sData.tlGetInLoop 			= "T_GetIn_Loop_couch"
			sData.tlGetOut 				= "T_GetOut_couch"
			sData.tlGetOutLoop 			= "T_GetOut_Loop_couch"
		BREAK
	ENDSWITCH
	#endif
	#endif
ENDPROC
#if USE_CLF_DLC
FUNC BOOL GET_SAVEGAME_ROOM_NAME_FOR_PICKUPCLF(SAVEHOUSE_NAME_ENUM eSavehouse, INT iPickup, TEXT_LABEL_31 &tlRoomName)
	
	tlRoomName = ""
	
	SWITCH eSavehouse
		CASE SAVEHOUSEclf_MICHAEL_BH
			IF iPickup = 0
				tlRoomName = "v_michael_g_kitche"
			ELIF iPickup = 1
				tlRoomName = "v_michael_1_wc"
			ENDIF
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_SC
			IF iPickup = 0
				tlRoomName = "v_57_bathrm"
			ELIF iPickup = 1
				tlRoomName = "v_57_kitchrm"
			ENDIF
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_VH
			IF iPickup = 0
				tlRoomName = "kitchen"
			ELIF iPickup = 1
				tlRoomName = "bedroom"
			ENDIF
		BREAK
		CASE SAVEHOUSEclf_TREVOR_CS
		CASE SAVEHOUSEclf_MICHAEL_CS
			// We need to do a check on the loaded IPL here as the room name can change
			IF IS_IPL_ACTIVE("TrevorsTrailer")
				IF iPickup = 0
					tlRoomName = "V_TrailerRm"
				ELIF iPickup = 1
					tlRoomName = "V_TrailerToiletRm"
				ENDIF
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTidy")
				IF iPickup = 0
					tlRoomName = "V_TrailerTIDYRm"
				ELIF iPickup = 1
					tlRoomName = "V_TrailerToiletTIDYRm"
				ENDIF
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTrash")
				IF iPickup = 0
					tlRoomName = "V_TrailTRASHRm"
				ELIF iPickup = 1
					tlRoomName = "V_TraiToiletTRASHRm"
				ENDIF
			ENDIF
		BREAK
		CASE SAVEHOUSEclf_TREVOR_VB
			IF iPickup = 0
				tlRoomName = "rm_lounge"
			ELIF iPickup = 1
				tlRoomName = "rm_bathroom"
			ENDIF
		BREAK
		CASE SAVEHOUSEclf_TREVOR_SC
			IF iPickup = 0
				tlRoomName = "strp3off"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlRoomName, ""))
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL GET_SAVEGAME_ROOM_NAME_FOR_PICKUPNRM(SAVEHOUSE_NAME_ENUM eSavehouse, INT iPickup, TEXT_LABEL_31 &tlRoomName)
	
	tlRoomName = ""
	
	SWITCH eSavehouse
		CASE SAVEHOUSENRM_BH
			IF iPickup = 0
				tlRoomName = "v_michael_g_kitche"
			ELIF iPickup = 1
				tlRoomName = "v_michael_1_wc"
			ENDIF
		BREAK
		CASE SAVEHOUSENRM_CHATEAU
			IF iPickup = 0
				tlRoomName = "v_57_bathrm"
			ELIF iPickup = 1
				tlRoomName = "v_57_kitchrm"
			ENDIF
		BREAK		
	ENDSWITCH
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlRoomName, ""))
ENDFUNC
#endif
FUNC BOOL GET_SAVEGAME_ROOM_NAME_FOR_PICKUP(SAVEHOUSE_NAME_ENUM eSavehouse, INT iPickup, TEXT_LABEL_31 &tlRoomName)

#if USE_CLF_DLC
	return GET_SAVEGAME_ROOM_NAME_FOR_PICKUPCLF(eSavehouse,iPickup,tlRoomName)
#endif
#if USE_NRM_DLC
	return GET_SAVEGAME_ROOM_NAME_FOR_PICKUPNRM(eSavehouse,iPickup,tlRoomName)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	tlRoomName = ""
	
	SWITCH eSavehouse
		CASE SAVEHOUSE_MICHAEL_BH
			IF iPickup = 0
				tlRoomName = "v_michael_g_kitche"
			ELIF iPickup = 1
				tlRoomName = "v_michael_1_wc"
			ENDIF
		BREAK
		CASE SAVEHOUSE_FRANKLIN_SC
			IF iPickup = 0
				tlRoomName = "v_57_bathrm"
			ELIF iPickup = 1
				tlRoomName = "v_57_kitchrm"
			ENDIF
		BREAK
		CASE SAVEHOUSE_FRANKLIN_VH
			IF iPickup = 0
				tlRoomName = "kitchen"
			ELIF iPickup = 1
				tlRoomName = "bedroom"
			ENDIF
		BREAK
		CASE SAVEHOUSE_TREVOR_CS
		CASE SAVEHOUSE_MICHAEL_CS
			// We need to do a check on the loaded IPL here as the room name can change
			IF IS_IPL_ACTIVE("TrevorsTrailer")
				IF iPickup = 0
					tlRoomName = "V_TrailerRm"
				ELIF iPickup = 1
					tlRoomName = "V_TrailerToiletRm"
				ENDIF
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTidy")
				IF iPickup = 0
					tlRoomName = "V_TrailerTIDYRm"
				ELIF iPickup = 1
					tlRoomName = "V_TrailerToiletTIDYRm"
				ENDIF
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTrash")
				IF iPickup = 0
					tlRoomName = "V_TrailTRASHRm"
				ELIF iPickup = 1
					tlRoomName = "V_TraiToiletTRASHRm"
				ENDIF
			ENDIF
		BREAK
		CASE SAVEHOUSE_TREVOR_VB
			IF iPickup = 0
				tlRoomName = "rm_lounge"
			ELIF iPickup = 1
				tlRoomName = "rm_bathroom"
			ENDIF
		BREAK
		CASE SAVEHOUSE_TREVOR_SC
			IF iPickup = 0
				tlRoomName = "strp3off"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlRoomName, ""))
#endif
#endif	
	
ENDFUNC
#if USE_CLF_DLC
/// PURPOSE: Returns TRUE if a wardrobe has been set up near the savegame bed
FUNC BOOL DOES_SAVEHOUSE_HAVE_WARDROBECLF(SAVEHOUSE_NAME_ENUM eSavehouse)
	SWITCH eSavehouse
		CASE SAVEHOUSEclf_MICHAEL_BH	// Mansion
		CASE SAVEHOUSEclf_MICHAEL_CS	// Trailer
		CASE SAVEHOUSEclf_TREVOR_CS	// Trailer
		CASE SAVEHOUSEclf_TREVOR_VB	// Apartment
		CASE SAVEHOUSEclf_FRANKLIN_SC	// Aunts house
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL DOES_SAVEHOUSE_HAVE_WARDROBENRM(SAVEHOUSE_NAME_ENUM eSavehouse)
	SWITCH eSavehouse
		CASE SAVEHOUSENRM_BH		// Mansion
		CASE SAVEHOUSENRM_CHATEAU	// CHATEAU		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#endif
FUNC BOOL DOES_SAVEHOUSE_HAVE_WARDROBE(SAVEHOUSE_NAME_ENUM eSavehouse)
#if USE_CLF_DLC
	return DOES_SAVEHOUSE_HAVE_WARDROBECLF(eSavehouse)
#endif
#if USE_NRM_DLC
	return DOES_SAVEHOUSE_HAVE_WARDROBENRM(eSavehouse)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH eSavehouse
		CASE SAVEHOUSE_MICHAEL_BH	// Mansion
		CASE SAVEHOUSE_MICHAEL_CS	// Trailer
		CASE SAVEHOUSE_TREVOR_CS	// Trailer
		CASE SAVEHOUSE_TREVOR_VB	// Apartment
		CASE SAVEHOUSE_FRANKLIN_SC	// Aunts house
			RETURN TRUE
		BREAK
	ENDSWITCH
#endif
#endif
		
	RETURN FALSE
ENDFUNC

