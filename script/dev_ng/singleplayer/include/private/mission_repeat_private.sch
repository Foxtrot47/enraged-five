//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 14/09/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					Mission Repeat System - Private Script Header				│
//│																				│
//│			This header contains all private script commands used by			│
//│			the mission repeat system. This system allows replaying of 			│
//│			missions that have already been completed by the player.			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛
USING "flow_reset_GAME.sch"
USING "friends_public.sch"
USING "mission_repeat_data.sch"

#if not USE_CLF_DLC
#if not USE_NRM_DLC

ENUM RP_HEIST_STATE
	RPHS_POI_OVERVIEW,
	RPHS_POI_OVERVIEW_DONE,
	RPHS_GAMEPLAY_CHOICE,
	RPHS_GAMEPLAY_CHOICE_DONE,
	RPHS_CREW_CHOICE,
	RPHS_CREW_CHOICE_DONE,
	RPHS_EXIT_CUTSCENE,
	RPHS_FINISHED
ENDENUM

ENUM RP_HEIST_FLAGS
	RPH_START_BOARD_INTERACTIONS,
	RPH_LOAD_EXIT_CUTSCENE,
	RPH_DO_EXIT_CUTSCENE,
	RPH_SHUTDOWN_LAUNCHER
ENDENUM

#endif
#endif

// -----------------------------COMMON FUNCTIONS -----------------------------------------------------------

/// PURPOSE:
///    Requests permission for the repeat play mission to run
/// PARAMS:
///    iCandidateID - the candidate ID gets updated if the mission is given permission to run
/// RETURNS:
///    TRUE if the mission got permission to run. FALSE otherwise
FUNC BOOL REPEAT_PLAY_LAUNCH_MISSION(INT &iCandidateID)

	CPRINTLN(DEBUG_REPEAT, "REPEAT_PLAY_LAUNCH_MISSION: waiting for permission to run.")
		
	SWITCH g_RepeatPlayData.eMissionType
		CASE CP_GROUP_MISSIONS
			IF Request_Mission_Launch(iCandidateID, MCTID_SELECTED_BY_PLAYER) = MCRET_ACCEPTED
				RETURN TRUE
			ENDIF
		BREAK
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		CASE CP_GROUP_RANDOMCHARS
			// update the global candidate ID of this RC first, so RC_REQUEST_PERMISSION_TO_RUN works correctly
			g_RandomChars[g_RepeatPlayData.iMissionIndex].rcMissionCandidateID = iCandidateID 
			IF RC_REQUEST_PERMISSION_TO_RUN(INT_TO_ENUM(g_eRC_MissionIDs ,g_RepeatPlayData.iMissionIndex))
				iCandidateID = g_RandomChars[g_RepeatPlayData.iMissionIndex].rcMissionCandidateID // return the updated candidate ID
				RETURN TRUE
			ENDIF
		BREAK
		#endif
		#endif
		DEFAULT
			CPRINTLN(DEBUG_REPEAT,"REPEAT_PLAY_LAUNCH_MISSION passed a mission type that isn't repeat playable: ", g_RepeatPlayData.eMissionType)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Changes player character to someone that can launch the mission we are repeat playing
PROC REPEAT_PLAY_SWITCH_TO_SUITABLE_CHARACTER(INT iTriggerCharBitset)
	//Work out which character to start the mission as.
	CPRINTLN(DEBUG_REPEAT,"Switching to suitable character")
	SELECTOR_SLOTS_ENUM eCharToTrigger = SELECTOR_PED_MICHAEL
	enumCharacterList ePlayerChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	
	IF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, ePlayerChar)
		eCharToTrigger = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(ePlayerChar)
	
	ELIF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, CHAR_MICHAEL)
		eCharToTrigger = SELECTOR_PED_MICHAEL
		
	ELIF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, CHAR_FRANKLIN)
		eCharToTrigger = SELECTOR_PED_FRANKLIN
		
	ELIF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, CHAR_TREVOR)
		eCharToTrigger = SELECTOR_PED_TREVOR
	ELSE
		SCRIPT_ASSERT("mission_repeat_controller.sc: Tried to repeat a mission that doesn't seem to be triggerable by any playable characters.")
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		WHILE NOT SET_CURRENT_SELECTOR_PED(eCharToTrigger,TRUE)
			WAIT(0)
		ENDWHILE
	ENDIF
	CPRINTLN(DEBUG_REPEAT,"Switched to suitable character: ", eCharToTrigger)
ENDPROC

// ------------------------------HEIST FUNCTIONS--------------------------------------------------------------
/// PURPOSE:
///    Sets visibility of part of a heist planning board
/// PARAMS:
///    iHeist - the heist that uses this planning board
///    eDisplayGroup - the display group we are altering
///    bIsVisible - do we want the display group to be visible?
PROC REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(INT iHeist, g_eBoardDisplayGroups eDisplayGroup, BOOL bIsVisible)

	// All parameters valid. Update the state in the heist's global display group bitset.
	IF bIsVisible = TRUE
		SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], ENUM_TO_INT(eDisplayGroup))
	ELSE
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], ENUM_TO_INT(eDisplayGroup))
	ENDIF
	
	// Flag the planning board as being updated so the heist controller knows to make changes.
	SET_BIT(g_iBitsetHeistBoardUpdated, iHeist)
ENDPROC


/// PURPOSE:
///    Sets the specified flow flag type for the heist specified
/// PARAMS:
///    iHeist - the heist we are using
///    eFlag - the flow flag type we want to set
///    bNewValue - new value of the flow flag
/// RETURNS:
///    Whether it has managed to set the flow flag. (Will be false if this heist doesnt have that type of flowflag)

#if not USE_CLF_DLC
#if not USE_NRM_DLC
//========================================= HEIST ONLY IN GTA V CONTROLLERS ===========================================

FUNC BOOL REPEAT_PLAY_SET_HEIST_FLOW_FLAG(INT iHeist, RP_HEIST_FLAGS eFlag, BOOL bNewValue)


	SWITCH iHeist
		CASE HEIST_JEWEL
			SWITCH eFlag
				CASE RPH_START_BOARD_INTERACTIONS
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_LOAD_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_DO_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				CASE RPH_SHUTDOWN_LAUNCHER
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_JEWEL, bNewValue)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK

		CASE HEIST_DOCKS	
			SWITCH eFlag
				CASE RPH_START_BOARD_INTERACTIONS
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_LOAD_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_DO_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				CASE RPH_SHUTDOWN_LAUNCHER
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_DOCKS, bNewValue)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_RURAL_BANK	
			SWITCH eFlag
				CASE RPH_START_BOARD_INTERACTIONS
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_LOAD_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_DO_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				CASE RPH_SHUTDOWN_LAUNCHER
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_RURAL_BANK, bNewValue)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_AGENCY		
			SWITCH eFlag
				CASE RPH_START_BOARD_INTERACTIONS
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_LOAD_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_DO_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				CASE RPH_SHUTDOWN_LAUNCHER
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_AGENCY, bNewValue)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_FINALE
			SWITCH eFlag
				CASE RPH_START_BOARD_INTERACTIONS
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_LOAD_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				
				CASE RPH_DO_EXIT_CUTSCENE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE, bNewValue)
					RETURN TRUE
				BREAK
				CASE RPH_SHUTDOWN_LAUNCHER
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_FINALE, bNewValue)
					RETURN TRUE
				BREAK
			ENDSWITCH
			
		BREAK
		
		DEFAULT// not a heist
		BREAK
	ENDSWITCH
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Waits for exit cutscene to start and then tells board to turn off
/// PARAMS:
///    iHeist - the heist we are repeat playing
///    bLockExit - lock the board's exit

PROC REPEAT_PLAY_TOGGLE_BOARD_VIEWING(INT iHeist, BOOL bLockExit)
	
	BOOL bWaitForCutsceneStart
	
	// Wait for any currently playing conversations to finish before moving on
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
		CPRINTLN(DEBUG_REPEAT, "REPEAT_PLAY_TOGGLE_BOARD_VIEWING: is waiting for conversation to end")
	ENDWHILE
	
	//wait for the cut-scene to start before moving on?
	bWaitForCutsceneStart = REPEAT_PLAY_SET_HEIST_FLOW_FLAG(iHeist, RPH_DO_EXIT_CUTSCENE, TRUE)

	IF bWaitForCutsceneStart = TRUE
		WHILE NOT IS_CUTSCENE_PLAYING()
			WAIT(0)
			CPRINTLN(DEBUG_REPEAT, "REPEAT_PLAY_TOGGLE_BOARD_VIEWING: is waiting for cutscene to start")
		ENDWHILE
	ELSE
		CPRINTLN(DEBUG_REPEAT, "REPEAT_PLAY_TOGGLE_BOARD_VIEWING: not waiting for cut-scene to start")
		DO_SCREEN_FADE_OUT(0) // no exit cut-scene, fade out now
	ENDIF
	
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, iHeist, bLockExit)
	
	//Tell the heist controller to toggle the board view.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, iHeist, TRUE)
ENDPROC

/// PURPOSE:
///    Handles the player's plannng board interactions for repeat play heists
/// PARAMS:
///    iHeist - the heist we are repeat playing the board for
///    eInteraction - which interaction we are currently doing
///    iDisplayGroup - the display group of the board we are editing
///    eRPHeistState - the current state of the repeat play heist
/// RETURNS:
///    the new repeat play heist state
  
FUNC RP_HEIST_STATE REPEAT_PLAY_DO_BOARD_INTERACTION(INT iHeist, g_eBoardInteractions eInteraction, INT iDisplayGroup, RP_HEIST_STATE eRPHeistState)

	//Check if this interaction is being told to back out.
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_BOARD_UNDO)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_BOARD_UNDO, FALSE)
		RETURN RPHS_GAMEPLAY_CHOICE
	ENDIF
	
	// Get the board mode flowint flag.
	FLOW_INT_IDS theBoardModeFlag = GET_HEIST_BOARD_MODE_FLOWINT_ID(iHeist)

	// Get the current board mode flag for this heist and check it's valid.
	INT iFlaggedBoardMode = GET_MISSION_FLOW_INT_VALUE(theBoardModeFlag)
	IF iFlaggedBoardMode < 0 OR iFlaggedBoardMode >= ENUM_TO_INT(PBM_INVALID)
		CERRORLN(DEBUG_REPEAT, "REPEAT_PLAY_DO_BOARD_INTERACTION: The current board mode flagged for this heist is invald.")
	ENDIF
	//It's valid. Convert to an ENUM
	g_eBoardModes theFlaggedMode = INT_TO_ENUM(g_eBoardModes, iFlaggedBoardMode)
	
	// Set the board interaction flags.
	BOOL lockExit = TRUE
	BOOL viewWhenDone = TRUE
	
	// Which interaction is being triggered?
	SWITCH(eInteraction)
	
		//Manage point of interest overviews.
		CASE INTERACT_POI_OVERVIEW
			IF theFlaggedMode <> PBM_POI_OVERVIEW
				//The board has not been flagged to switch to crew select mode yet. Flag it.
				SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_POI_OVERVIEW))

				//Make sure the crew selected bit is cleared as we initialise the interaction.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, iHeist, FALSE)
				
				//Set board interaction modifier flags as we initialise.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, iHeist, lockExit)
			ELSE
				//The board is already flagged to switch to crew select mode.
				//Has the crew been selected yet?
				IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, iHeist)
					//Yes...
					//Set the board mode flag to viewing mode if requested.
					IF viewWhenDone
						SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_VIEW))
					ENDIF
					
					//Clean up the "Heist POI Overview Done" flag.
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, iHeist, FALSE)
					
					//Turn on the display group linked to this interaction.
					IF iDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
						SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], iDisplayGroup)
						//Tell the board controller that the flow has made a change that needs the board to be refreshed.
						SET_BIT(g_iBitsetHeistBoardUpdated, iHeist) 
					ENDIF
					
					RETURN RPHS_POI_OVERVIEW_DONE
				ENDIF
			ENDIF
		BREAK
	
		//Manage selecting a crew.
		CASE INTERACT_CREW
			IF theFlaggedMode <> PBM_CREW_SELECT
				//The board has not been flagged to switch to crew select mode yet. Flag it.
				SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_CREW_SELECT))
				
				//Ensure display group is turned off as we start the interaction.
				IF iDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
					CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], iDisplayGroup)
					SET_BIT(g_iBitsetHeistBoardUpdated, iHeist)
				ENDIF
				
				//Make sure the crew selected bit is cleared as we initialise the interaction.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, iHeist, FALSE)
				
				//Set board interaction modifier flags as we initialise.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, iHeist, lockExit)
			ELSE
				//The board is already flagged to switch to crew select mode.
				//Has the crew been selected yet?
				IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, iHeist)
					//Yes...
					//Set the board mode flag to viewing mode if requested.
					IF viewWhenDone
						SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_VIEW))
					ENDIF
					
					//Clean up the "Heist Crew Selected" flag.
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, iHeist, FALSE)
					
					//Turn on the display group linked to this interaction.
					IF iDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
						SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], iDisplayGroup)
						SET_BIT(g_iBitsetHeistBoardUpdated, iHeist)
					ENDIF
					
					RETURN RPHS_CREW_CHOICE_DONE
				ENDIF
			ENDIF
		BREAK
		
		//Manage selecting a gameplay choice.
		CASE INTERACT_GAMEPLAY_CHOICE
			//Look up which FLOWINT we need to check.
			FLOW_INT_IDS theChoiceFlowInt
			SWITCH(iHeist)
				CASE HEIST_JEWEL
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_JEWEL
				BREAK
				
				CASE HEIST_DOCKS
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_DOCKS
				BREAK
				
				CASE HEIST_RURAL_BANK
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_RURAL
				BREAK
				
				CASE HEIST_AGENCY
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_AGENCY
				BREAK
				
				CASE HEIST_FINALE
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_FINALE
				BREAK
			ENDSWITCH
		
			IF theFlaggedMode <> PBM_CHOICE_SELECT
				//The board has not been flagged to switch to gameplay choice select mode yet. Flag it.
				SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_CHOICE_SELECT))
				
				//Make sure the choice is cleared as we initialise the interaction.
				SET_MISSION_FLOW_INT_VALUE(theChoiceFlowInt, HEIST_CHOICE_EMPTY)
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, iHeist, FALSE)
				
				//Ensure display group is turned off as we start the interaction.
				IF iDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
					CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], iDisplayGroup)
					SET_BIT(g_iBitsetHeistBoardUpdated, iHeist)
				ENDIF
				
				//Set board interaction modifier flags as we initialise.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, iHeist, lockExit)
			ELSE
				//The board is already flagged to switch to gameplay choice select mode.
				//Has the choice been selected yet?
				IF GET_MISSION_FLOW_INT_VALUE(theChoiceFlowInt) <> HEIST_CHOICE_EMPTY
				AND GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, iHeist)
					//Yes... 
					//Set the board mode flag to viewing mode if requested.
					IF viewWhenDone
						SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_VIEW))
					ENDIF
					
					//Clean up the "Heist Choice Selected" flag.
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, iHeist, FALSE)
					
					//Turn on the display group linked to this interaction.
					IF iDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
						SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], iDisplayGroup)
						SET_BIT(g_iBitsetHeistBoardUpdated, iHeist)
					ENDIF

					//Follow interaction complete jump.
					IF iHeist = HEIST_DOCKS
						// docks heist has no crew select stage
						RETURN RPHS_CREW_CHOICE_DONE
					ENDIF
					
					RETURN RPHS_GAMEPLAY_CHOICE_DONE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	// no change
	RETURN eRPHeistState
ENDFUNC

/// PURPOSE:
///    Returns which heist mission the player has chosen to play.
/// PARAMS:
///    iHeist - the heist strand we are working with.
/// RETURNS:
///    the mission ID of the hesit mission we want to launch
FUNC INT REPEAT_PLAY_GET_SELECTED_HEIST_MISSION_ID(INT iHeist)
	SWITCH iHeist
		CASE HEIST_JEWEL
			RETURN ENUM_TO_INT(SP_HEIST_JEWELRY_2)
		BREAK

		CASE HEIST_DOCKS	
			IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_DOCKS) = HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
				RETURN ENUM_TO_INT(SP_HEIST_DOCKS_2A)
			ELSE
				RETURN ENUM_TO_INT(SP_HEIST_DOCKS_2B)
			ENDIF
		BREAK
		
		CASE HEIST_RURAL_BANK	
 			RETURN ENUM_TO_INT(SP_HEIST_RURAL_2)
		BREAK
		
		CASE HEIST_AGENCY		
			IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY) = HEIST_CHOICE_AGENCY_FIRETRUCK
				RETURN ENUM_TO_INT(SP_HEIST_AGENCY_3A)
			ELSE
				RETURN ENUM_TO_INT(SP_HEIST_AGENCY_3B)
			ENDIF
		BREAK
		
		CASE HEIST_FINALE		
			IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE) = HEIST_CHOICE_FINALE_TRAFFCONT
				RETURN ENUM_TO_INT(SP_HEIST_FINALE_2A)
			ELSE
				RETURN ENUM_TO_INT(SP_HEIST_FINALE_2B)
			ENDIF
		BREAK
		
		DEFAULT // not a heist
		BREAK
	ENDSWITCH
	RETURN 0 // not a heist
ENDFUNC

/// PURPOSE:
///    Activates any display groups that should be on when repeat playing this planning board
/// PARAMS:
///    iHeist - the heist strand we are working with.
PROC SET_HEIST_BOARD_DISPLAY_GROUPS_ON(INT iHeist)
	SWITCH iHeist
		CASE HEIST_JEWEL
			
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_0, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_1, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_2, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_3, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_4, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_5, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_6, TRUE)
			SWITCH GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_JEWEL)
				CASE HEIST_CHOICE_JEWEL_STEALTH
					REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_8, TRUE)
				BREAK
				CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT
					REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_7, TRUE)
				BREAK
			ENDSWITCH
		BREAK

		CASE HEIST_DOCKS	
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_DOCKS, PBDG_0, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_DOCKS, PBDG_1, TRUE)
		BREAK
		
		CASE HEIST_RURAL_BANK	
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_RURAL_BANK, PBDG_0, TRUE)
 			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_RURAL_BANK, PBDG_1, TRUE)
		BREAK
		
		CASE HEIST_AGENCY		
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_0, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_1, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_2, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_3, TRUE)
		BREAK
		
		CASE HEIST_FINALE	
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_FINALE, PBDG_0, TRUE)
			REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_FINALE, PBDG_1, TRUE)
		BREAK
		
		DEFAULT // not a heist
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Checks appropriate flowflag to see if the heist's exit cutscene has finished
/// PARAMS:
///    iHeist - the hesit we are repeat playing
/// RETURNS:
///    TRUE if cutscene finished , false otherwise
FUNC BOOL REPEAT_PLAY_HAS_HEIST_CUTSCENE_FINISHED(INT iHeist)
	
	SWITCH iHeist
		CASE HEIST_JEWEL
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE) = TRUE
				RETURN FALSE
			ENDIF
		BREAK

		CASE HEIST_DOCKS	
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE) = TRUE
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE HEIST_RURAL_BANK	
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE) = TRUE
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE HEIST_AGENCY	
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE) = TRUE
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE HEIST_FINALE	
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE) = TRUE
				RETURN FALSE
			ENDIF
		BREAK
		
		DEFAULT // not a heist
		BREAK
	ENDSWITCH

	RETURN TRUE // cutscene has finished, or this heist doesn't have a cut-scene
ENDFUNC

/// PURPOSE:
///    Returns how long a delay there should be after making the gameplay or crew choice
/// PARAMS:
///    iHeist - the heist we are repeat playing
///    eRPHeistState - should be RPHS_GAMEPLAY_CHOICE_DONE or RPHS_CREW_CHOICE_DONE
/// RETURNS:
///    How long we should delay in this state
FUNC INT REPEAT_PLAY_GET_HEIST_CHOICE_DONE_WAIT(INT iHeist, RP_HEIST_STATE eRPHeistState)
	SWITCH iHeist
		CASE HEIST_JEWEL
			SWITCH eRPHeistState
				CASE RPHS_POI_OVERVIEW_DONE
					RETURN 3000
				BREAK
				CASE RPHS_GAMEPLAY_CHOICE_DONE
					RETURN 3000
				BREAK
				CASE RPHS_CREW_CHOICE_DONE
					RETURN 2250
				BREAK
			ENDSWITCH
		BREAK

		CASE HEIST_DOCKS	
			SWITCH eRPHeistState
				CASE RPHS_POI_OVERVIEW_DONE
					RETURN 3000
				BREAK
				CASE RPHS_GAMEPLAY_CHOICE_DONE
					RETURN 3500
				BREAK
				CASE RPHS_CREW_CHOICE_DONE
					RETURN 2250
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_RURAL_BANK	
			SWITCH eRPHeistState
				CASE RPHS_POI_OVERVIEW_DONE
					RETURN 3000
				BREAK
				CASE RPHS_GAMEPLAY_CHOICE_DONE
					RETURN 1000
				BREAK
				CASE RPHS_CREW_CHOICE_DONE
					RETURN 2250
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_AGENCY		
			SWITCH eRPHeistState
				CASE RPHS_POI_OVERVIEW_DONE
					RETURN 3000
				BREAK
				CASE RPHS_GAMEPLAY_CHOICE_DONE
					RETURN 0
				BREAK
				CASE RPHS_CREW_CHOICE_DONE
					RETURN 2250
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HEIST_FINALE		
			SWITCH eRPHeistState
				CASE RPHS_POI_OVERVIEW_DONE
					RETURN 3000
				BREAK
				CASE RPHS_GAMEPLAY_CHOICE_DONE
					RETURN 0
				BREAK
				CASE RPHS_CREW_CHOICE_DONE
					RETURN 2250
				BREAK
			ENDSWITCH	
		BREAK
		
		DEFAULT // not a heist
		BREAK
	ENDSWITCH
	RETURN 0 // not a heist or no wait time set
ENDFUNC

/// PURPOSE:
///    Returns the int of the disaply group that this heist uses for the selected gameplay choice
/// PARAMS:
///    iHeist - the heist we are repeat playing
///    bGameplayChoice - is this the gameplay choice? (if false, it must be the crew choice)
/// RETURNS:
///    the int of the display group used in this part of the heist planning board
FUNC INT REPEAT_PLAY_GET_HEIST_CHOICE_DISPLAY_GROUP(INT iHeist, BOOL bGameplayChoice)
	SWITCH iHeist
		CASE HEIST_JEWEL
			IF bGameplayChoice = TRUE
				RETURN ENUM_TO_INT(PBDG_5)
			ELSE
				RETURN ENUM_TO_INT(PBDG_6) // Crew Selection
			ENDIF
		BREAK

		CASE HEIST_DOCKS	
			IF bGameplayChoice = TRUE
				RETURN ENUM_TO_INT(PBDG_2)
			ELSE
				RETURN ENUM_TO_INT(PBDG_2) // Crew Selection
			ENDIF
		BREAK
		
		CASE HEIST_RURAL_BANK	
			IF bGameplayChoice = TRUE
				RETURN ENUM_TO_INT(PBDG_2)
			ELSE
				RETURN ENUM_TO_INT(PBDG_2) // Crew Selection
			ENDIF
		BREAK
		
		CASE HEIST_AGENCY	
			IF bGameplayChoice = TRUE
				RETURN ENUM_TO_INT(PBDG_4)
			ELSE
				RETURN ENUM_TO_INT(PBDG_5) // Crew Selection
			ENDIF
		BREAK
		
		CASE HEIST_FINALE	
			IF bGameplayChoice = TRUE
				RETURN ENUM_TO_INT(PBDG_2)
			ELSE
				RETURN ENUM_TO_INT(PBDG_3) // Crew Selection
			ENDIF
		BREAK
		
		DEFAULT // not a heist
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Checks a flowflag to see if the heist planning board is still being setup
/// PARAMS:
///    iHeist - the hesit that we are repeat playing
/// RETURNS:
///    True if the planning board is still being setup, false if it is ready to be displayed
FUNC BOOL REPEAT_PLAY_IS_HEIST_BOARD_BEING_SETUP(INT iHeist)
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE() = TRUE // when the planning board is ready it takes control of the on mission flag
		SWITCH iHeist
			CASE HEIST_JEWEL
				RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION)
			BREAK

			CASE HEIST_DOCKS	
				RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION)
			BREAK
			
			CASE HEIST_RURAL_BANK	
				RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
			BREAK
			
			CASE HEIST_AGENCY		
				RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION)
			BREAK
			
			CASE HEIST_FINALE
				RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION)
			BREAK
			
			DEFAULT// not a heist
			BREAK
		ENDSWITCH
	ENDIF
	RETURN TRUE // heist still being setup, or mission is not a heist
ENDFUNC

/// PURPOSE:
///    Returns the name of the heist controller associated with the mission passed in
/// PARAMS:
///    eMission - the mission we are checking
/// RETURNS:
///    Name of the heist controller or "None" if there isn't an associated controller.
FUNC STRING GET_HEIST_CONTROLLER_SCRIPT(SP_MISSIONS eMission)
	SWITCH eMission
	
		// ------AGENCY------
		CASE SP_HEIST_AGENCY_1
		CASE SP_HEIST_AGENCY_2
		CASE SP_HEIST_AGENCY_PREP_1
		CASE SP_HEIST_AGENCY_3A
		CASE SP_HEIST_AGENCY_3B
			RETURN "heist_ctrl_agency" 
		BREAK
		// ------DOCKS / PORT OF LS------
		CASE SP_HEIST_DOCKS_1
		CASE SP_HEIST_DOCKS_2A
		CASE SP_HEIST_DOCKS_2B
			RETURN "heist_ctrl_docks" 
		BREAK
		
		// ------FINALE / BIG SCORE------
		CASE SP_HEIST_FINALE_1
		CASE SP_HEIST_FINALE_2_INTRO
		CASE SP_HEIST_FINALE_PREP_A
		CASE SP_HEIST_FINALE_PREP_B
		CASE SP_HEIST_FINALE_PREP_C1
		CASE SP_HEIST_FINALE_PREP_C2
		CASE SP_HEIST_FINALE_PREP_C3
		CASE SP_HEIST_FINALE_PREP_D
		CASE SP_HEIST_FINALE_2A
		CASE SP_HEIST_FINALE_2B
			RETURN "heist_ctrl_finale" 
		BREAK
		
		// ------JEWELLRY / JEWEL STORE------
		CASE SP_HEIST_JEWELRY_1
		CASE SP_HEIST_JEWELRY_PREP_1A
		CASE SP_HEIST_JEWELRY_PREP_2A
		CASE SP_HEIST_JEWELRY_2
			RETURN "heist_ctrl_jewel" 
		BREAK
		
		// ------RURAL / PALETO------
		CASE SP_HEIST_RURAL_1
		CASE SP_HEIST_RURAL_PREP_1
		CASE SP_HEIST_RURAL_2
			RETURN "heist_ctrl_rural" 
		BREAK
		
		DEFAULT
			RETURN "NONE" // other missions do not have an associated heist controller
		BREAK
	
	ENDSWITCH
	RETURN "NONE"
ENDFUNC


// --------------------------------REPEAT_PLAY_UNLOCKING FUNCTIONS -------------------------------------------

/// PURPOSE:
///    Called after either Agency Heist finale
PROC REPEAT_PLAY_DO_AGENCY_HEIST_COMPLETE()
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_AGENCY,TRUE)
	SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_LOCKED)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_HELIHOLE, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_BLUEPRINT, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_FIREMAN_GEAR, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_OVERALLS, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_CLEANUP ) 
	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_AGENCY_PREP_FIRETRUCK,FALSE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_REPAIR, BUILDINGSTATE_DESTROYED)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_C4, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_HELIHOLE, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_HOLE_PLUG, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_RUBBLE, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_HLOD, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_LOD, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_SLOD, BUILDINGSTATE_NORMAL)
ENDPROC


#endif
#endif
//============================================ END HEIST ONLY STUFF ===========================================

/// PURPOSE:
///    Wraps the 2 commands needed to set savehouse state
/// PARAMS:
///    eSavehouse - which savehouse we are altering
///    bRespawnAvailable - is a respawn available at this savehouse?
///    bLongRangeBlip - should this savehouse have a long range blip?
PROC REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_NAME_ENUM eSavehouse, BOOL bRespawnAvailable, BOOL bLongRangeBlip)
	SET_SAVEHOUSE_RESPAWN_AVAILABLE(eSavehouse, bRespawnAvailable)
	SET_SAVEHOUSE_RESPAWN_BLIP_LONG_RANGE(eSavehouse, bLongRangeBlip)
ENDPROC

/// PURPOSE:
///    Wraps the 2 commands needed to set special ability state
/// PARAMS:
///    ePed - the ped we are altering
///    bState - true to unlock, false to lock
PROC REPEAT_PLAY_SET_SPECIAL_ABILITY_STATE(enumCharacterList ePed, BOOL bState)
#if  USE_CLF_DLC
	IF bState = TRUE
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobalsClifford.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = TRUE
	ELSE
		SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobalsClifford.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = FALSE
	ENDIF
#endif
#if USE_NRM_DLC
	IF bState = TRUE
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobalsnorman.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = TRUE
	ELSE
		SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobalsnorman.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = FALSE
	ENDIF
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF bState = TRUE
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = TRUE
	ELSE
		SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = FALSE
	ENDIF
#endif
#endif
	
ENDPROC

/// PURPOSE:
///    Wraps the commands needed to activate a Random Character mission
/// PARAMS:
///    RC_MissionID - the RC mission we are activating
///    bFullyActivated - fully activate this mission?
PROC REPEAT_PLAY_ACTIVATE_RC_MISSION(g_eRC_MissionIDs RC_MissionID, BOOL bFullyActivated)

	SET_BIT(g_savedGlobals.sRandomChars.savedRC[RC_MissionID].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
	
	// Only set the activated flag if told to
	IF bFullyActivated = TRUE
		RANDOM_CHARACTER_ACTIVATE_MISSION(RC_MissionID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Wraps the commands needed to set a weapon lock state
/// PARAMS:
///    eWeapon - the weapon we are altering
///    bUnlocked - true unlocks the weapon, false locks it
PROC REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPON_TYPE eWeapon, BOOL bUnlocked)
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_MICHAEL, eWeapon, bUnlocked)
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_TREVOR, eWeapon, bUnlocked)
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_FRANKLIN, eWeapon, bUnlocked)
ENDPROC

/// PURPOSE:
///    Activates or deactivates the tracking of Franklin's outfit changes
/// PARAMS:
///    bActivate - if TRUE we start tracking, FALSE we stop
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC REPEAT_PLAY_SET_FRANKLIN_OUTFIT_BITSET_STATE(BOOL bActivate)
	IF bActivate = FALSE
		// de-activating
		Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_ACTIVE, FALSE)
	ELSE
		// Activating
		
		// Don't record Franklin's clothes, as this will already have been saved in the normal playthrough
	
		//Activate “change your clothes” request.
		Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_ACTIVE, TRUE)
	ENDIF
ENDPROC
#endif
#endif
/// PURPOSE:
///    Requests and launches a script
/// PARAMS:
///    sScriptName - name of script
PROC REPEAT_PLAY_LAUNCH_SCRIPT_AND_FORGET(STRING sScriptName, INT iStackSize = DEFAULT_STACK_SIZE)

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sScriptName)) > 0
		EXIT
	ENDIF
	
	REQUEST_SCRIPT(sScriptName)
	WHILE NOT HAS_SCRIPT_LOADED(sScriptName)
		CPRINTLN(DEBUG_REPEAT, "Waiting for script to load", sScriptName)
		WAIT(0)
	ENDWHILE

	START_NEW_SCRIPT(sScriptName, iStackSize)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(sScriptName)
ENDPROC

/// PURPOSE:
///    Skips over the specified RC mission and unlocks / locks anything that this should trigger.
/// PARAMS:
///    eRCMission - the mission we are skipping
///    bSkipToBefore - are we skipping to before this mission triggers (if false we skip to after this mission has been completed)
PROC REPEAT_PLAY_SKIP_RC_MISSION(g_eRC_MissionIDs eRCMission, BOOL bSkipToBefore = FALSE)
#if USE_CLF_DLC	
eRCMission = eRCMission
bSkipToBefore = bSkipToBefore
#endif
#if USE_NRM_DLC
eRCMission = eRCMission
bSkipToBefore = bSkipToBefore
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	CPRINTLN(DEBUG_REPEAT,"REPEAT_PLAY_SKIP_RC_MISSION skipping : ", GET_RC_MISSION_DISPLAY_STRING_FROM_ID(eRCMission))
	
	SWITCH eRCMission
		
		CASE RC_ABIGAIL_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_ABIGAIL, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_ABIGAIL_2
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_ABIGAIL, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_BARRY_1
			IF bSkipToBefore = TRUE
			
			ELSE
//				ADD_CONTACT_TO_PHONEBOOK(CHAR_BARRY, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_BARRY_2
			IF bSkipToBefore = TRUE
			
			ELSE
//				ADD_CONTACT_TO_PHONEBOOK(CHAR_BARRY, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_BARRY_3
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_BARRY, FRANKLIN_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_BARRY_3A
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK

		CASE RC_BARRY_3C
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
			
		CASE RC_BARRY_4
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
			
		CASE RC_DREYFUSS_1
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EPSILON_1
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE, TRUE)
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EPSILON_2
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_500, TRUE)
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARNIE, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_EPSILON_3
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_5000, TRUE)
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EPSILON_4
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_CARS_DONE, TRUE)
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JIMMY_BOSTON, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_EPSILON_5
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_10000, TRUE)
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EPSILON_6
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_ROBES_BOUGHT, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_ROBES_DONE, TRUE)
			ELSE
				
			ENDIF
		BREAK
		
		CASE RC_EPSILON_7
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EPSILON_8
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DESERT_DONE, TRUE)
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EXTREME_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DOM, FRANKLIN_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_EXTREME_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EXTREME_3
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_EXTREME_4
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_FANATIC_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARY_ANN, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_FANATIC_2
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARY_ANN, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_FANATIC_3
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARY_ANN, FRANKLIN_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_HAO_1
		BREAK
		
		CASE RC_HUNTING_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_HUNTER, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_HUNTING_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_JOSH_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSH, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_JOSH_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_JOSH_3
			IF bSkipToBefore = TRUE
			
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		
		CASE RC_JOSH_4
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_MAUDE_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MAUDE, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
			
		CASE RC_MINUTE_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JOE, TREVOR_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSEF, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_MINUTE_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_MINUTE_3
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MANUEL, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_MRS_PHILIPS_1
		BREAK
		
		CASE RC_MRS_PHILIPS_2
		BREAK
		
		CASE RC_NIGEL_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_NIGEL_1A
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_NIGEL_1B
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_NIGEL_1C
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_NIGEL_1D
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_NIGEL_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_NIGEL_3
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_OMEGA_1
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_OMEGA_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_PAPARAZZO_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_PAPARAZZO_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_PAPARAZZO_3
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_PAPARAZZO_3A
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_PAPARAZZO_3B
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_PAPARAZZO_4
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_RAMPAGE_1
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_RAMPAGE_2
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_RAMPAGE_3
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_RAMPAGE_4
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_RAMPAGE_5
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_THELASTONE
			IF bSkipToBefore = TRUE
			
			ELSE
			
			ENDIF
		BREAK
		
		CASE RC_TONYA_1
			IF bSkipToBefore = TRUE
			
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TOW_TONYA, FRANKLIN_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE RC_TONYA_2
		BREAK
		
		CASE RC_TONYA_3
		BREAK
		
		CASE RC_TONYA_4
		BREAK	

		CASE RC_TONYA_5
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_REPEAT,"REPEAT_PLAY_SKIP_RC_MISSION tried to skip invalid mission: ", eRCMission)
			SCRIPT_ASSERT("REPEAT_PLAY_SKIP_RC_MISSION trying to skip unknown mission. Bug for Andy Minghella")
		BREAK
		
	ENDSWITCH
#endif
#endif
ENDPROC

/// PURPOSE:
///    Blocks or unblocks a friend contact
/// PARAMS:
///    eFriendOwner - character who has this friend
///    eFriend - the friend
///    bBlock - TRUE = block, FALSE = unblock
PROC REPEAT_PLAY_BLOCK_FRIEND_CONTACT(enumCharacterList eFriendOwner, enumCharacterList eFriend, BOOL bBlock)
	IF bBlock
		//Block the friend for the player char.
		IF IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_HIATUS)
			EXIT
		ENDIF
		SET_FRIEND_BLOCK_FLAG(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_HIATUS)
		
	ELSE
		//Unblock the friend for the player char.
		IF NOT IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_HIATUS)						
			EXIT
		ENDIF
		CLEAR_FRIEND_BLOCK_FLAG(eFriendOwner,eFriend, FRIEND_BLOCK_FLAG_HIATUS)
	ENDIF
ENDPROC

/// PURPOSE:
///    Prevents friend clashes
/// PARAMS:
///    eFriendOwner - character who has this friend
///    eFriend - the friend
///    bBlock - TRUE = block, FALSE = unblock
PROC REPEAT_PLAY_BLOCK_FRIEND_CLASH(enumCharacterList eFriendOwner, enumCharacterList eFriend, BOOL bBlock = FALSE)
	
	IF bBlock
		//Block the friend for the player char.
		IF IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_CLASH)
			EXIT
		ENDIF
		SET_FRIEND_BLOCK_FLAG(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_CLASH)
	ELSE
		//Unblock the friend for the player char.
		IF NOT IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_CLASH)
			EXIT
		ENDIF
		CLEAR_FRIEND_BLOCK_FLAG(eFriendOwner, eFriend, FRIEND_BLOCK_FLAG_CLASH)
	ENDIF
ENDPROC

/// PURPOSE:
///    Skips over the specified story mission and unlocks / locks anything that this should trigger.
/// PARAMS:
///    eMission - the mission we are skipping
///    bSkipToBefore - are we skipping to before this mission triggers (if false we skip to after this mission has been completed)
///    bTargetMission - is this the mission we are wanting to repeat play? 
///    eFinaleCompleted - which finale did the player (used to not do setup for other finales)
#if USE_CLF_DLC
PROC REPEAT_PLAY_SKIP_STORY_MISSION_CLF(SP_MISSIONS eMission, BOOL bSkipToBefore, BOOL bTargetMission, SP_MISSIONS eFinaleCompleted)

	#IF IS_DEBUG_BUILD
		IF bSkipToBefore
			CPRINTLN(DEBUG_REPEAT,"<clf>Skipping before mission: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
		ELSE
			CPRINTLN(DEBUG_REPEAT,"<clf>Skipping mission: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
		ENDIF
	#ENDIF
	
	UNUSED_PARAMETER(bTargetMission)
	UNUSED_PARAMETER(eFinaleCompleted)
	
	SWITCH eMission
		CASE SP_MISSION_CLF_TRAIN
		CASE SP_MISSION_CLF_FIN		
		CASE SP_MISSION_CLF_IAA_LIE
		CASE SP_MISSION_CLF_IAA_TRA
		CASE SP_MISSION_CLF_IAA_JET
		CASE SP_MISSION_CLF_IAA_HEL
		CASE SP_MISSION_CLF_IAA_DRO
		CASE SP_MISSION_CLF_IAA_RTS
		CASE SP_MISSION_CLF_KOR_PRO
		CASE SP_MISSION_CLF_KOR_RES
		CASE SP_MISSION_CLF_KOR_SUB
		CASE SP_MISSION_CLF_KOR_SAT
		CASE SP_MISSION_CLF_KOR_5		
		CASE SP_MISSION_CLF_RUS_PLA
		CASE SP_MISSION_CLF_RUS_CAR
		CASE SP_MISSION_CLF_RUS_VAS
		CASE SP_MISSION_CLF_RUS_SAT
		CASE SP_MISSION_CLF_RUS_JET
		CASE SP_MISSION_CLF_RUS_CLK
		CASE SP_MISSION_CLF_ARA_1
		CASE SP_MISSION_CLF_ARA_DEF
		CASE SP_MISSION_CLF_ARA_FAKE	
		CASE SP_MISSION_CLF_ARA_TNK
		CASE SP_MISSION_CLF_CAS_SET
		CASE SP_MISSION_CLF_CAS_PR1
		CASE SP_MISSION_CLF_CAS_PR2
		CASE SP_MISSION_CLF_CAS_PR3
		CASE SP_MISSION_CLF_CAS_HEI		
		CASE SP_MISSION_CLF_ASS_POL
		CASE SP_MISSION_CLF_ASS_RET
		CASE SP_MISSION_CLF_ASS_CAB
		CASE SP_MISSION_CLF_ASS_GEN
		CASE SP_MISSION_CLF_ASS_SUB
		CASE SP_MISSION_CLF_ASS_HEL
		CASE SP_MISSION_CLF_ASS_VIN
		CASE SP_MISSION_CLF_ASS_HNT
		CASE SP_MISSION_CLF_ASS_SKY		
		CASE SP_MISSION_CLF_RC_ALEX_GRND
		CASE SP_MISSION_CLF_RC_ALEX_AIR
		CASE SP_MISSION_CLF_RC_ALEX_UNDW
		CASE SP_MISSION_CLF_RC_ALEX_RWRD
		CASE SP_MISSION_CLF_RC_MEL_MONT
		CASE SP_MISSION_CLF_RC_MEL_AIRP
		CASE SP_MISSION_CLF_RC_MEL_WING
		CASE SP_MISSION_CLF_RC_MEL_DIVE
		CASE SP_MISSION_CLF_RC_AGN_BODY
		CASE SP_MISSION_CLF_RC_AGN_RCPT
		CASE SP_MISSION_CLF_RC_AGN_ESCP
		CASE SP_MISSION_CLF_RC_CLA_HACK
		CASE SP_MISSION_CLF_RC_CLA_DRUG
		CASE SP_MISSION_CLF_RC_CLA_FIN
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				// nothing to do
			ENDIF
		BREAK
		

		DEFAULT
			CERRORLN(DEBUG_REPEAT, "REPEAT_PLAY_SKIP_STORY_MISSION trying to skip unknown mission. eMission =", eMission)
			SCRIPT_ASSERT("REPEAT_PLAY_SKIP_STORY_MISSION trying to skip unknown mission. Bug for Andy Minghella")
		BREAK
	
	ENDSWITCH
ENDPROC
#endif
#if USE_NRM_DLC
PROC REPEAT_PLAY_SKIP_STORY_MISSION_NRM(SP_MISSIONS eMission, BOOL bSkipToBefore, BOOL bTargetMission, SP_MISSIONS eFinaleCompleted)

	#IF IS_DEBUG_BUILD
		IF bSkipToBefore
			CPRINTLN(DEBUG_REPEAT,"<NRM>Skipping before mission: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
		ELSE
			CPRINTLN(DEBUG_REPEAT,"<NRM>Skipping mission: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
		ENDIF
	#ENDIF
	
	UNUSED_PARAMETER(bTargetMission)
	UNUSED_PARAMETER(eFinaleCompleted)
	
	SWITCH eMission
		
		CASE SP_MISSION_NRM_SUR_START		
		CASE SP_MISSION_NRM_SUR_AMANDA	
		CASE SP_MISSION_NRM_SUR_TRACEY		
		CASE SP_MISSION_NRM_SUR_MICHAEL	
		CASE SP_MISSION_NRM_SUR_HOME	
		CASE SP_MISSION_NRM_SUR_JIMMY
		CASE SP_MISSION_NRM_SUR_PARTY
		CASE SP_MISSION_NRM_SUR_CURE	
		CASE SP_MISSION_NRM_RESCUE_ENG	
		CASE SP_MISSION_NRM_RESCUE_MED	
		CASE SP_MISSION_NRM_RESCUE_GUN	
		CASE SP_MISSION_NRM_SUP_FUEL		
		CASE SP_MISSION_NRM_SUP_AMMO		
		CASE SP_MISSION_NRM_SUP_MEDS		
		CASE SP_MISSION_NRM_SUP_FOOD		
		CASE SP_MISSION_NRM_RADIO_A		
		CASE SP_MISSION_NRM_RADIO_B		
		CASE SP_MISSION_NRM_RADIO_C				
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				// nothing to do
			ENDIF
		BREAK
		

		DEFAULT
			CERRORLN(DEBUG_REPEAT, "REPEAT_PLAY_SKIP_STORY_MISSION trying to skip unknown mission. eMission =", eMission)
			SCRIPT_ASSERT("REPEAT_PLAY_SKIP_STORY_MISSION trying to skip unknown mission. Bug for Andy Minghella")
		BREAK
	
	ENDSWITCH
ENDPROC
#endif
PROC REPEAT_PLAY_SKIP_STORY_MISSION(SP_MISSIONS eMission, BOOL bSkipToBefore, BOOL bTargetMission, SP_MISSIONS eFinaleCompleted)
	#if USE_CLF_DLC
		REPEAT_PLAY_SKIP_STORY_MISSION_CLF(eMission,bSkipToBefore,bTargetMission,eFinaleCompleted)
		exit
	#endif
	#if USE_NRM_DLC
		REPEAT_PLAY_SKIP_STORY_MISSION_NRM(eMission,bSkipToBefore,bTargetMission,eFinaleCompleted)
		exit
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	
	#IF IS_DEBUG_BUILD
		IF bSkipToBefore
			CPRINTLN(DEBUG_REPEAT,"Skipping before mission: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
		ELSE
			CPRINTLN(DEBUG_REPEAT,"Skipping mission: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission))
		ENDIF
	#ENDIF
	
	#IF USE_TU_CHANGES
		BOOL bMissionSkipped = TRUE
		
		IF bSkipToBefore
		OR bTargetMission
			bMissionSkipped = FALSE
		ENDIF
	#ENDIF
				
	SWITCH eMission
		
		// --------------ARMENIAN STRAND ------------------------------------------
		CASE SP_MISSION_ARMENIAN_1
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_FRANKLIN_SC,	TRUE, TRUE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_PRO, FALSE, FALSE)	
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_FRANKLIN_PRO, FALSE, FALSE) 	
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_PRO, FALSE, FALSE) 
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TAXI,		MICHAEL_BOOK,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TAXI,		FRANKLIN_BOOK,	FALSE)
				Execute_Code_ID(CID_TAXI_HAILING_ENABLE, 0)
				Execute_Code_ID(CID_UNLOCK_WATER_VEHICLE_SCUBA_GEAR, 0)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_SAVEHOUSE_CAR, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_AIRPORT_VEHICLE, TRUE) 
				REPEAT_PLAY_SET_SPECIAL_ABILITY_STATE(CHAR_FRANKLIN, TRUE)
				
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET			("ambientBlimp")	Not needed
				REPEAT_PLAY_LAUNCH_SCRIPT_AND_FORGET("controller_taxi")
				Execute_Code_ID(CID_ACTIVATE_FAIRGROUND, 0)
				Execute_Code_ID(CID_ACTIVATE_SHOP_BARBERS, 0)
				Execute_Code_ID(CID_ACTIVATE_SHOP_CARMOD, 0) 
				Execute_Code_ID(CID_ACTIVATE_GUNSHOP_AND_RANGE, 0)	
				Execute_Code_ID(CID_ACTIVATE_MINIGAME_STRIPCLUB, 0)
				SET_DOOR_STATE(DOORNAME_STRIPCLUB_F, DOORSTATE_UNLOCKED)	
				SET_DOOR_STATE(DOORNAME_STRIPCLUB_R, DOORSTATE_UNLOCKED)
			ENDIF
		BREAK
		
		CASE SP_MISSION_ARMENIAN_2
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_ARM2_SCAFFOLDING, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_FRANKLIN_OUTFIT_BITSET_STATE(TRUE)
				SUPPRESS_MOTORBIKES(FALSE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PISTOL, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_MICROSMG, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_KNIFE, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PUMPSHOTGUN, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED) // Close the rear shutters
				Execute_Code_ID(CID_STRETCH_TEXT_SENT) // Check!
				ADD_CONTACT_TO_PHONEBOOK(CHAR_AMANDA, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JIMMY, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TRACEY, MICHAEL_BOOK, FALSE)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_LAMAR,	CHAR_FRANKLIN)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_JIMMY,	CHAR_MICHAEL)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_AMANDA, CHAR_MICHAEL)
			ENDIF
		BREAK
		
		CASE SP_MISSION_ARMENIAN_3
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_BH,	TRUE, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BJXL_CRASH_POST_ARM3, TRUE)
				
				// Set the car showroom destroyed
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE1, BUILDINGSTATE_DESTROYED) 	
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE2, BUILDINGSTATE_DESTROYED) 	
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE3, BUILDINGSTATE_DESTROYED) 
				
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET			("SH_Intro_M_Home")		 Not needed
				
				Execute_Code_ID(CID_QUEUE_BAGGER_TEXT_AND_UNLOCK_YETARIAN) // Check!
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PISTOL, TRUE)
			ENDIF
		BREAK

		// -------------ASSASSINATION STRAND --------------------------------------------
		CASE SP_MISSION_ASSASSIN_1
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ASS1_UNLOCKED, TRUE)
			ELSE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ASS1_UNLOCKED, TRUE)
				// SETFLOW_LAUNCH_SCRIPT_AND_FORGET	("SH_Intro_F_Hills")	//not needed
				Execute_Code_ID(CID_FRANKLIN_UNLOCK_HILLS_SAVEHOUSE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_ASSASSIN_2
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				// nothing to do
			ENDIF
		BREAK
		
		CASE SP_MISSION_ASSASSIN_3
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				// nothing to do
			ENDIF
		BREAK
		
		CASE SP_MISSION_ASSASSIN_4
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				// nothing to do
			ENDIF
		BREAK
		
		CASE SP_MISSION_ASSASSIN_5
			IF bSkipToBefore = TRUE
				// nothing to do
			ELSE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ASS_COMPLETED, TRUE)
			ENDIF
		BREAK
	
		// ---------------CAR STEAL STRAND-------------------------------------------
		CASE SP_MISSION_CARSTEAL_1
			IF bSkipToBefore = TRUE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_GARAGE, BUILDINGSTATE_DESTROYED)
				SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_F, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_S, DOORSTATE_UNLOCKED)
				SET_FRIEND_BLOCK_FLAG(CHAR_MICHAEL,	CHAR_FRANKLIN,	FRIEND_BLOCK_FLAG_MISSION, SP_MISSION_CARSTEAL_3)
				SET_FRIEND_BLOCK_FLAG(CHAR_FRANKLIN,CHAR_TREVOR,	FRIEND_BLOCK_FLAG_MISSION, SP_MISSION_CARSTEAL_3)
				SET_FRIEND_BLOCK_FLAG(CHAR_TREVOR,	CHAR_MICHAEL,	FRIEND_BLOCK_FLAG_MISSION, SP_MISSION_CARSTEAL_3)
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_ENTITYXF, BUILDINGSTATE_DESTROYED)				
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_CHEETAH, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE	(WEAPONTYPE_NIGHTSTICK, TRUE)	
				Execute_Code_ID(CID_CARMOD_UNLOCK_STAGE_3, 0)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DEVIN, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MOLLY, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DEVIN, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
	
		CASE SP_MISSION_CARSTEAL_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_ZTYPE, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
	
		CASE SP_MISSION_CARSTEAL_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_JB700, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_TUXEDO, BUILDINGSTATE_DESTROYED)
				SET_FRIEND_BLOCK_FLAG(CHAR_FRANKLIN,	CHAR_LAMAR,	FRIEND_BLOCK_FLAG_MISSION, SP_MISSION_CARSTEAL_4)
				SET_FRIEND_BLOCK_FLAG(CHAR_FRANKLIN,	CHAR_TREVOR,	FRIEND_BLOCK_FLAG_MISSION, SP_MISSION_CARSTEAL_4)
			ENDIF
		BREAK
		
		CASE SP_MISSION_CARSTEAL_4
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_ENTITYXF, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_CHEETAH, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_ZTYPE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_JB700, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARSTEAL_GARAGE, BUILDINGSTATE_CLEANUP) 
			ENDIF
		BREAK
		
		// ---------------CHINESE STRAND-------------------------------------------
		CASE SP_MISSION_CHINESE_1
			IF bSkipToBefore = TRUE
				SET_DOOR_STATE(DOORNAME_METHLAB_F_L, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_METHLAB_F_R, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_METHLAB_R, DOORSTATE_UNLOCKED)
			ELSE
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_GRENADELAUNCHER, TRUE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_CHENG, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK

		CASE SP_MISSION_CHINESE_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_CLEANUP)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_OCCLUSION, BUILDINGSTATE_CLEANUP)
				Execute_Code_ID(CID_ENABLE_LOST_SCENARIO_GROUP, 0)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_SAWNOFFSHOTGUN, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PETROLCAN, TRUE)
			ENDIF
		BREAK

		// --------------EXILE STRAND------------------------------------------------
		CASE SP_MISSION_EXILE_1
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_HAS_HAGGARD_SUIT, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_NORMAL)
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET		("exile_city_denial")  not needed
				Execute_Code_ID(CID_DEACTIVATE_EXILE_BLIPS, 0)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_FRANKLIN, CHAR_MICHAEL, TRUE)		
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_FRANKLIN, CHAR_TREVOR, TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_JIMMY, 	CHAR_FRANKLIN, TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_LAMAR, 	CHAR_TREVOR, TRUE)
			ELSE
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_CLEANUP)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGOPLANE, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(GADGETTYPE_PARACHUTE, TRUE)
				Execute_Code_ID(CID_UNLOCK_AIR_VEHICLE_PARACHUTE, 0)
				Execute_Code_ID(CID_UNLOCK_EXILE1_PICKUPS, 0)
			ENDIF
		BREAK
		
		CASE SP_MISSION_EXILE_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_HEAVYSNIPER, TRUE)
			ENDIF
		BREAK

		
		CASE SP_MISSION_EXILE_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH_2, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_RF_GASSTATION01, BUILDINGSTATE_NORMAL)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE, TRUE)
			ENDIF
		BREAK
		
		// --------------FAMILY STRAND------------------------------------------------
		CASE SP_MISSION_FAMILY_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_JIMMY,	CHAR_FRANKLIN)
				
				ADD_CONTACT_TO_PHONEBOOK(CHAR_FRANKLIN,	MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MICHAEL,	FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JIMMY,	FRANKLIN_BOOK, FALSE)
				//Execute_Code_ID(CID_ACTIVATE_CAR_SAVE_GARAGES, 0) not needed
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED) 	
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, BUILDINGSTATE_CLEANUP) 		
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD, BUILDINGSTATE_CLEANUP) 
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE1, BUILDINGSTATE_CLEANUP) 		
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE2, BUILDINGSTATE_CLEANUP) 		
				SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE3, BUILDINGSTATE_CLEANUP) 
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BJXL_CRASH_POST_ARM3, FALSE)
				REPEAT_PLAY_SET_SPECIAL_ABILITY_STATE(CHAR_MICHAEL, TRUE)
				Execute_Code_ID(CID_UNLOCK_LAMAR_1, 0) // check
				Execute_Code_ID(CID_FAMILY1_FRANKLIN_GATE_UNLOCK, 0) // check
			ENDIF
		BREAK

		CASE SP_MISSION_FAMILY_3 // family 3 comes before family 2 now
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_RF_STILT_HOUSE, BUILDINGSTATE_DESTROYED)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY, TRUE)	
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LESTER, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARTIN, Michael_BOOK, FALSE)
			ENDIF
		BREAK

		CASE SP_MISSION_FAMILY_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CRUISESHIP, BUILDINGSTATE_DESTROYED)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_HIDE_BARE_CHEST, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MOUNTAIN_BIKE_CH, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_SEASHARK_SM, TRUE)
			ENDIF
		BREAK

		CASE SP_MISSION_FAMILY_4
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_JIMMY, TREVOR_BOOK, FALSE)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_JIMMY, CHAR_TREVOR)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARTIN, Michael_BOOK, FALSE)
				
				SET_BUILDING_STATE(BUILDINGNAME_RF_STILT_HOUSE, BUILDINGSTATE_CLEANUP)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT, BUILDINGSTATE_DESTROYED) 
				
				#IF NOT IS_JAPANESE_BUILD
					SET_BUILDING_STATE	(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SEX_TOYS, BUILDINGSTATE_DESTROYED)
				#ENDIF
								
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_WHISKY, BUILDINGSTATE_DESTROYED ) 
				SET_BUILDING_STATE(BUILDINGNAME_ES_TRACEY_BEDROOM_FAME_OR_SHAME, BUILDINGSTATE_DESTROYED) 
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MICHAEL,	TREVOR_BOOK, 	FALSE)	
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TREVOR,	MICHAEL_BOOK, 	FALSE)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_MICHAEL,	CHAR_TREVOR)
				
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_TRACEYS_STUFF, BUILDINGSTATE_DESTROYED)
				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			ENDIF
		BREAK

		CASE SP_MISSION_FAMILY_5
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_LOUNGE_STUFF, 	BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MICHAEL_BED, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_DESTROYED) 	// Flowers dying on the dining room table.
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT, TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_AMANDA,	CHAR_MICHAEL, TRUE)	
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_JIMMY,	CHAR_MICHAEL, TRUE)
				Execute_Code_ID(CID_FAM5_JIMMYTAKE) // check
				Execute_Code_ID(CID_DEACTIVATE_MICHAEL_TENNIS, 0) // check
				Execute_Code_ID(CID_ACTIVE_SHINE_A_LIGHT, 0) // check
			ENDIF
		BREAK

		CASE SP_MISSION_FAMILY_6
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				Execute_Code_ID(CID_UNLOCK_AGENCY_HEIST_2) // check
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_AMANDA,	CHAR_MICHAEL, FALSE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_JIMMY,	CHAR_MICHAEL, FALSE)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_TRACEYS_STUFF, 	BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, 	BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, 		BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_LOUNGE_STUFF, 		BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MICHAEL_BED, 			BUILDINGSTATE_NORMAL) 
				SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_NORMAL)
			ENDIF
		BREAK

		// --------------FBI STRAND------------------------------------------------
		CASE SP_MISSION_FBI_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_COMBATPISTOL, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_RF_STILT_HOUSE, BUILDINGSTATE_NORMAL)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE	
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_APPISTOL, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_HEAVYSNIPER, TRUE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_STEVE, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TREVOR, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_FRANKLIN, TREVOR_BOOK, FALSE)
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_MICHAEL,	CHAR_FRANKLIN,	TRUE)	
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_FRANKLIN, CHAR_TREVOR,	TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_TREVOR,	CHAR_MICHAEL,	TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_3_CALLS_DONE, FALSE)
				Execute_Code_ID(CID_FBI_3_CALLS_COMPLETE) // check
			ENDIF
		BREAK

		CASE SP_MISSION_FBI_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_TORTURE_TOOLS, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_MICHAEL,	CHAR_FRANKLIN,	FALSE)
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_FRANKLIN, CHAR_TREVOR,	FALSE)
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_TREVOR,	CHAR_MICHAEL,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_STEVE, FRANKLIN_BOOK, FALSE)
			ENDIF
		BREAK

		
		CASE SP_MISSION_FBI_4_INTRO
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_STEVE, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DAVE, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DAVE, TREVOR_BOOK, FALSE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_RPG, TRUE)
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET	("FBI4_Prep3Amb", FRIEND_STACK_SIZE) not needed
				Execute_Code_ID(CID_FBI4_P3_DONE_REMINDER)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FBI4_TRASH, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_CALLS_DONE, FALSE)
				Execute_Code_ID(CID_FBI_4_CALLS_COMPLETE, 0)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FIB4_SWEATSHOP_KITBAG, BUILDINGSTATE_NORMAL)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FBI4_TOWING, TRUE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_4
			IF bSkipToBefore = TRUE
				Execute_Code_ID(CID_ACTIVATE_SHOP_CLOTHES_AMB, 0)
			ELSE
				Execute_Code_ID(CID_ACTIVATE_SHOP_CLOTHES_AMB, 0)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FIB4_SWEATSHOP_KITBAG, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK

		CASE SP_MISSION_FBI_4_PREP_5
			IF bSkipToBefore = TRUE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FIB4_SWEATSHOP_KITBAG, BUILDINGSTATE_NORMAL)
			ELSE
				SET_SHOP_IS_AVAILABLE(GUN_SHOP_01_DT, TRUE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_4
			IF bSkipToBefore = TRUE
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FBI4_TOWING, FALSE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FBI4_TRASH, FALSE)
			ELSE	
				Execute_Code_ID(CID_CAR1TEXT_UNLOCK)// check
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DEVIN, MICHAEL_BOOK, FALSE)
				SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_2, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_SHOWHOME, BUILDINGSTATE_NORMAL) // Franklin is unpacking
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_UNPACKING, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_ASSAULTRIFLE, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_ASSAULTSHOTGUN, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_STICKYBOMB, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_COMBATMG, TRUE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FBI_5
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_CARBINERIFLE, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_STUNGUN, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_LADIES, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_SETTLED, BUILDINGSTATE_DESTROYED) // Franklin now settled in.
				SET_BUILDING_STATE(BUILDINGNAME_ES_FIB5_MICHAEL_GARAGE_SCUBA, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CAPOLAVORO_BILLBOARD_GRAFFITI, BUILDINGSTATE_DESTROYED)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_BISHOPS_CHICKEN_BILLBOARD_GRAFFITI, BUILDINGSTATE_DESTROYED)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FRUIT_BILLBOARD_GRAFFITI, BUILDINGSTATE_DESTROYED)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MAZE_BILLBOARD_GRAFFITI, BUILDINGSTATE_DESTROYED)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_RON_OIL_BILLBOARD_GRAFFITI, BUILDINGSTATE_DESTROYED)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_VAPID_BILLBOARD_GRAFFITI, BUILDINGSTATE_DESTROYED)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_HAS_HAGGARD_SUIT, TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_FRANKLIN, CHAR_MICHAEL, FALSE)		
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_FRANKLIN, CHAR_TREVOR, FALSE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_JIMMY, CHAR_FRANKLIN, FALSE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_LAMAR, CHAR_TREVOR, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_CS,	FALSE, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_BH,	TRUE, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE_COUNTRY, FALSE)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			ENDIF
		BREAK

		// --------------FINALE STRAND------------------------------------------------
		CASE SP_MISSION_FINALE_INTRO
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK

		CASE SP_MISSION_FINALE_A
			IF eFinaleCompleted = eMission
			OR bTargetMission = TRUE
				IF bSkipToBefore = TRUE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE, TRUE)
				ELSE
					SET_BUILDING_STATE(BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_1, BUILDINGSTATE_DESTROYED)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_2, BUILDINGSTATE_NORMAL)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_3, BUILDINGSTATE_NORMAL)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_4, BUILDINGSTATE_NORMAL)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_REPEAT, "Not doing any skip setup for finale as it wasn't completed in flow.")
			ENDIF
		BREAK
		
		CASE SP_MISSION_FINALE_B
			IF eFinaleCompleted = eMission
			OR bTargetMission = TRUE
				IF bSkipToBefore = TRUE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE, TRUE)
				ELSE
					SET_BUILDING_STATE(BUILDINGNAME_IPL_KILLED_MICHAEL, BUILDINGSTATE_DESTROYED)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_REPEAT, "Not doing any skip setup for finale as it wasn't completed in flow.")
			ENDIF
		BREAK

		CASE SP_MISSION_FINALE_C1
			IF eFinaleCompleted = SP_MISSION_FINALE_C2
			OR bTargetMission = TRUE
				IF bSkipToBefore = TRUE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE, TRUE)
				ELSE
					//
				ENDIF
			ELSE
				CPRINTLN(DEBUG_REPEAT, "Not doing any skip setup for finale as it wasn't completed in flow.")
			ENDIF
		BREAK

		CASE SP_MISSION_FINALE_C2
			IF eFinaleCompleted = eMission
			OR bTargetMission = TRUE
				IF bSkipToBefore = TRUE
					REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_ADVANCEDRIFLE, TRUE)
				ELSE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_HAVE_FALLEN_OUT, FALSE)
					REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_MICHAEL,	CHAR_TREVOR, FALSE)	 
				ENDIF
			ELSE
				CPRINTLN(DEBUG_REPEAT, "Not doing any skip setup for finale as it wasn't completed in flow.")
			ENDIF
		BREAK

		CASE SP_MISSION_FINALE_CREDITS
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				Execute_Code_ID(CID_UNLOCK_END_GAME_OUTFITS)
				Execute_Code_ID(CID_TREVOR_UNLOCK_BLAZER3)
			ENDIF
		BREAK

		// --------------FRANKLIN STRAND------------------------------------------------
		CASE SP_MISSION_FRANKLIN_0
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_CITY_SAVEHOUSE_BANDANA, BUILDINGSTATE_DESTROYED)
				Execute_Code_ID(CID_STRETCH_TEXT_SENT) // check
			ENDIF
		BREAK
	
		CASE SP_MISSION_FRANKLIN_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_CITY_SAVEHOUSE_KITBAG, BUILDINGSTATE_DESTROYED)
				
				//--Execute_Code_ID(CID_LAMAR_TREVOR_FRIEND_UNLOCK) - does the stuff below
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_LAMAR, CHAR_TREVOR)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LAMAR, TREVOR_BOOK, FALSE)
				// --
				SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREV1_SMASHED_TRAILER,false)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET	("controller_Races", MICRO_STACK_SIZE) not needed
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_FRANKLIN, CHAR_LAMAR, TRUE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_FRANKLIN_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_CABLE_CAR_FLYER, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_FRANKLIN, CHAR_LAMAR, FALSE)
			ENDIF
		BREAK
		
		// --------------LAMAR STRAND------------------------------------------------
		CASE SP_MISSION_LAMAR
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_SAWNOFFSHOTGUN, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PUMPSHOTGUN, TRUE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_STRETCH, FRANKLIN_BOOK, FALSE)
				REPEAT_PLAY_SET_FRANKLIN_OUTFIT_BITSET_STATE(FALSE)
			ENDIF
		BREAK

		// --------------LESTER STRAND------------------------------------------------
		CASE SP_MISSION_LESTER_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LESTER, MICHAEL_BOOK, FALSE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_UPTOWN_1, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_UPTOWN_2, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_UPTOWN_3, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_UPTOWN_4, TRUE)
				Execute_Code_ID(CID_UNLOCK_LAMAR_1) // check
				Execute_Code_ID(CID_UNLOCK_SHOPS_POST_LESTER_1A, 0)
				Execute_Code_ID(CID_CARMOD_UNLOCK_STAGE_1, 0)
			ENDIF
		BREAK
		
		// --------------MARTIN STRAND------------------------------------------------
		CASE SP_MISSION_MARTIN_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_DESTROYED) 
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_DESTROYED)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_RESTRICTED_TO_CITY,	FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, 	FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, 	FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_BH,	FALSE, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_VB,	FALSE, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_SC,	FALSE, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_CS,	TRUE, TRUE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_CS,	TRUE, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE, FALSE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE_COUNTRY, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_CITY, FALSE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_COUNTRY, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARTIN, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_MARTIN, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK

		// --------------MICHAEL STRAND------------------------------------------------
		CASE SP_MISSION_MICHAEL_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_HAVE_FALLEN_OUT, TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_MICHAEL,	CHAR_FRANKLIN,	TRUE)	
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_MICHAEL,	CHAR_TREVOR,	TRUE)	
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_FRANKLIN,	CHAR_TREVOR,	TRUE)	
				ADD_CONTACT_TO_PHONEBOOK(CHAR_CHENGSR, TREVOR_BOOK, FALSE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_LUDENDORFF, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MICHAEL_HALL_PLANE_TICKETS, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		
		CASE SP_MISSION_MICHAEL_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
				REPEAT_PLAY_BLOCK_FRIEND_CONTACT(CHAR_MICHAEL,	CHAR_FRANKLIN,	FALSE)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MIC3_KORTZ_RENNOVATION, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_NORMAL)
			ENDIF
		BREAK
	
		CASE SP_MISSION_MICHAEL_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MIC3_KORTZ_RENNOVATION, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_NORMAL)
				REPEAT_PLAY_BLOCK_FRIEND_CLASH(CHAR_FRANKLIN,	CHAR_TREVOR,	FALSE)
				Execute_Code_ID(CID_MIC4_COMMS_COMPLETE) // check
			ENDIF
		BREAK

		CASE SP_MISSION_MICHAEL_4
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_C4, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_RUBBLE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_MICHAEL_UPSTAIRS_MOVIE_PREMIER, BUILDINGSTATE_DESTROYED)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_TUXEDO, TRUE)
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET("michael4LeadOut")  not needed
			ENDIF
		BREAK
		
		// --------------MICHAEL EVENTS STRAND------------------------------------------------
		CASE SP_MISSION_ME_AMANDA
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_ME_JIMMY
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_ME_TRACEY
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		// --------------PROLOGUE STRAND------------------------------------------------
		CASE SP_MISSION_PROLOGUE
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CRUISESHIP, BUILDINGSTATE_DESTROYED)
				SUPPRESS_MOTORBIKES(TRUE)
				Execute_Code_ID	(CID_TAXI_HAILING_DISABLE, 0)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_MICHAEL_PRO, TRUE, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_FRANKLIN_PRO, TRUE, FALSE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_PRO, TRUE, FALSE)
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET			("flowStartAccept")
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_SIMEON, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LAMAR, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TANISHA, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DENISE, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_HAO, FRANKLIN_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DR_FRIEDLANDER, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DAVE, MICHAEL_BOOK, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LESTER, MICHAEL_BOOK, FALSE)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_MICHAEL,CHAR_FRANKLIN)
				ADD_CHAR_AS_CHAR_FRIEND(CHAR_TREVOR,CHAR_FRANKLIN)
				Execute_Code_ID(CID_DISABLE_LOST_SCENARIO_GROUP,		0)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TRV_PRO_MASK_REMOVED, TRUE)
				Execute_Code_ID(CID_INITIALISE_BANK_DATA, 0)
				Execute_Code_ID(CID_INITIALISE_STAT_OFFSETS, 0)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
			ENDIF
		BREAK

		// --------------SHRINK STRAND------------------------------------------------
		CASE SP_MISSION_SHRINK_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_SHRINK_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_SHRINK_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_SHRINK_4
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		CASE SP_MISSION_SHRINK_5
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK
		
		// --------------SOLOMON STRAND------------------------------------------------
		CASE SP_MISSION_SOLOMON_1
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN, TRUE)
			ELSE
				Execute_Code_ID(CID_QUEUE_SOL1_CALL_TO_UNLOCK_MARTIN1, 0)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_REMOTESNIPER, TRUE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_SOLOMON, MICHAEL_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_SOLOMON_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN, TRUE)
			ENDIF
		BREAK
		
		CASE SP_MISSION_SOLOMON_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_SMG, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_BB_CCC_KINGS, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_BB_MOLLIS, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_BB_TACO_BOMB, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MELTDOWN_POSTERS, BUILDINGSTATE_DESTROYED)			
				SET_BUILDING_STATE(BUILDINGNAME_IPL_MELTDOWN_POSTERS_EMISSIVE, BUILDINGSTATE_DESTROYED)	
			ENDIF
		BREAK

		// --------------TREVOR STRAND------------------------------------------------
		CASE SP_MISSION_TREVOR_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL,	FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN,	FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR,	TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, 	TRUE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_CS, TRUE, TRUE)
				Execute_Code_ID(CID_UNLOCK_TATTOO_SHOPS_POST_TREV1, 0)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_DESTROYED)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREV1_SMASHED_TRAILER,true)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_SPECIAL_ABILITY_STATE(CHAR_TREVOR, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_COUNTRY, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PUMPSHOTGUN, TRUE)
				SET_DOOR_STATE(DOORNAME_HICK_BAR_F, DOORSTATE_UNLOCKED)
				SET_BUILDING_STATE	(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED) 	// Close the shutters
				SET_BUILDING_STATE	(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, BUILDINGSTATE_NORMAL) 		// Mend window
				SET_BUILDING_STATE	(BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD, BUILDINGSTATE_NORMAL) 	// Remove LOD board.
				SET_BUILDING_STATE	(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE1, BUILDINGSTATE_NORMAL) 		// Remove blocking object for the rubble
				SET_BUILDING_STATE	(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE2, BUILDINGSTATE_NORMAL) 		// Remove blocking object for the rubble
				SET_BUILDING_STATE	(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE3, BUILDINGSTATE_NORMAL) 		// Remove blocking object for the rubble
				SET_BUILDING_STATE	(BUILDINGNAME_IPL_CAR_SHOWROOM_INTERIOR, BUILDINGSTATE_DESTROYED) 		// Swap interior for a fake one.
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BJXL_CRASH_POST_ARM3, FALSE)
				SET_SHOP_IS_AVAILABLE(GUN_SHOP_02_SS, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK

		CASE SP_MISSION_TREVOR_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				Execute_Code_ID(CID_TREV2_OSCAR_PAY_TREVOR, 0) // check
				Execute_Code_ID(CID_UNLOCK_LOST_HANGAR_POST_TREV2, 0)
				Execute_Code_ID(CID_CARMOD_UNLOCK_STAGE_2, 0)
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET		("AF_Intro_T_Sandy") not needed
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_SNIPERRIFLE, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_MOLOTOV, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_DESTROYED)
				// SETFLOW_LAUNCH_SCRIPT_AND_FORGET		("controller_Trafficking") not needed
			ENDIF
		BREAK
		
		CASE SP_MISSION_TREVOR_3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TAXI, TREVOR_BOOK, FALSE)
				
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_A, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_A_OCCLUSION, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_B, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_B_OCCLUSION, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_C, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_C_OCCLUSION, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_D, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_D_OCCLUSION, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_E, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_E_OCCLUSION, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, BUILDINGSTATE_DESTROYED)
				
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_VB,	TRUE, TRUE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_CS,	TRUE, FALSE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_CITY, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_COUNTRY, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_RESTRICTED_TO_CITY,	TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL,	TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN,	TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY, FALSE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_STICKYBOMB, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_TANKTOP_SWEATPANTS_1, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_DT1_17, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_SC1_19, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_VB_0, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		
		CASE SP_MISSION_TREVOR_4
			IF bSkipToBefore = TRUE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_2, BUILDINGSTATE_NORMAL)
				
				#IF NOT IS_JAPANESE_BUILD
					SET_BUILDING_STATE	(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SEX_TOYS, BUILDINGSTATE_NORMAL)
				#ENDIF
				
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_TORTURE_TOOLS, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_WHISKY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA, BUILDINGSTATE_NORMAL)			
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SOFA, BUILDINGSTATE_NORMAL)
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FLOYDS_APPARTMENT_CRIME_TAPE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_CLEANUP )
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_SC, TRUE, TRUE)
				REPEAT_PLAY_SET_SAVEHOUSE_STATE(SAVEHOUSE_TREVOR_VB, FALSE, FALSE)	
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_CITY,	FALSE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB,	TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_VEH_T_UNLOCK_RASP_JAM, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH_2, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_NORMAL)
			ENDIF
		BREAK
	
		//-------------------------AGENCY HEIST STRAND------------------------------------------------
		CASE SP_HEIST_AGENCY_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_0, TRUE)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_1, TRUE)
				REPEAT_PLAY_LAUNCH_SCRIPT_AND_FORGET("buildingSiteAmbience", MICRO_STACK_SIZE)
				
				SET_BUILDING_STATE(BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_OVERALLS, BUILDINGSTATE_DESTROYED)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_2_AUTOTRIGGERED, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RUN_BUILDINGSITE_AMBIENCE_AUDIO, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_AGENCY_2_LIFTS, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		
		CASE SP_HEIST_AGENCY_2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_AGENCY_2_LIFTS, BUILDINGSTATE_NORMAL)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_AGENCY, PBDG_2, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RUN_BUILDINGSITE_AMBIENCE_AUDIO, FALSE)
				SET_BUILDING_STATE(BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_BLUEPRINT, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		
		CASE SP_HEIST_AGENCY_PREP_1 // leads to SP_HEIST_AGENCY_3A
			IF bSkipToBefore = TRUE
				// nothing to be done
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET			("agency_prep2Amb", FRIEND_STACK_SIZE) // not needed
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_FIREMAN_GEAR, BUILDINGSTATE_DESTROYED)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_AGENCY_PREP_FIRETRUCK,TRUE)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY	(HEIST_AGENCY, PBDG_7, TRUE)
				SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
			ENDIF
		BREAK

		CASE SP_HEIST_AGENCY_3A
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_DO_AGENCY_HEIST_COMPLETE()
			ENDIF
		BREAK
		
		CASE SP_HEIST_AGENCY_3B
			IF bSkipToBefore = TRUE
 				// nothing to be done
			ELSE
				REPEAT_PLAY_DO_AGENCY_HEIST_COMPLETE()
			ENDIF
		BREAK
		
		//-------------------------DOCKS HEIST STRAND------------------------------------------------
		CASE SP_HEIST_DOCKS_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_DESTROYED)		
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE, BUILDINGSTATE_NORMAL) 
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA, BUILDINGSTATE_NORMAL)			
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SOFA, BUILDINGSTATE_DESTROYED)	
			ENDIF
		BREAK

		CASE SP_HEIST_DOCKS_PREP_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				EXECUTE_CODE_ID(CID_POST_DOCKS_PREP1, 0)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_DOCKS, PBDG_5, TRUE)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_DOCKS, PBDG_7, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK

		CASE SP_HEIST_DOCKS_PREP_2B
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, TRUE)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_DOCKS, PBDG_6, TRUE)
			ENDIF
		BREAK

		CASE SP_HEIST_DOCKS_2A
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGOSHIP, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGOSHIP_OCCLUSION, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_ASSAULTSHOTGUN, TRUE)
				
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, FALSE)
				SET_BUILDING_STATE(BUILDINGNAME_RF_STILT_HOUSE, BUILDINGSTATE_NORMAL)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_DOCKS, TRUE)
			ENDIF
		BREAK

		CASE SP_HEIST_DOCKS_2B
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_SUB, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, FALSE)
				SET_BUILDING_STATE(BUILDINGNAME_RF_STILT_HOUSE, BUILDINGSTATE_NORMAL)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_DOCKS, TRUE)				
			ENDIF
		BREAK				
		
		//-------------------------FINALE HEIST STRAND------------------------------------------------
		CASE SP_HEIST_FINALE_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_STRIPCLUB_TREVORS_MESS, BUILDINGSTATE_DESTROYED)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_FINALE, PBDG_1, TRUE)
				Execute_Code_ID(CID_CAR3_REM_DONE) // check
			ENDIF
		BREAK
		
		CASE SP_HEIST_FINALE_2_INTRO
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				// nothing to be done
			ENDIF
		BREAK

		CASE SP_HEIST_FINALE_PREP_A
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_FINALE, PBDG_5, TRUE)
				Execute_Code_ID(CID_BIG_SCORE_PREPA_COMPLETED, 0)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_FINALE, PBDG_6, TRUE)
			ENDIF
		BREAK
	
		CASE SP_HEIST_FINALE_2A
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_APPISTOL, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_PUMPSHOTGUN, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_FINALE, TRUE)	
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_ADVANCEDRIFLE, TRUE)
			ENDIF
		BREAK

		CASE SP_HEIST_FINALE_PREP_B
			IF bSkipToBefore = TRUE
				// nothing to be done
				//SETFLOW_LAUNCH_SCRIPT_AND_FORGET("finale_heist_prepEamb", FRIEND_STACK_SIZE) // not needed
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_FINALE, PBDG_7, TRUE)
				Execute_Code_ID(CID_FAST_TRACK_TRAIN_PREP_UNLOCK, 0)
				Execute_Code_ID(CID_BIG_SCORE_PREPB_COMPLETED, 0)
			ENDIF
		BREAK
		
		CASE SP_HEIST_FINALE_PREP_C1
			IF bSkipToBefore = TRUE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPC_EMAIL_DONE, FALSE)
				Execute_Code_ID(CID_SEND_BIG_SCORE_PREPC_EMAIL, 0)
			ELSE
				Execute_Code_ID(CID_BIG_SCORE_PREPC_COMPLETED, 0)
			ENDIF
		BREAK

		CASE SP_HEIST_FINALE_PREP_C2
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				Execute_Code_ID(CID_BIG_SCORE_PREPC_COMPLETED, 0)
			ENDIF
		BREAK
		
		CASE SP_HEIST_FINALE_PREP_C3
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				Execute_Code_ID(CID_BIG_SCORE_PREPC_COMPLETED, 0)
			ENDIF
		BREAK
		
		CASE SP_HEIST_FINALE_PREP_D
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				Execute_Code_ID(CID_BIG_SCORE_PREPD_COMPLETED, 0)
			ENDIF
		BREAK
	
		CASE SP_HEIST_FINALE_2B
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_FINALE, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_ADVANCEDRIFLE, TRUE)
			ENDIF
		BREAK
	
		//-------------------------JEWELLERY HEIST STRAND------------------------------------------------
		CASE SP_HEIST_JEWELRY_1
			IF bSkipToBefore = TRUE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED )
				SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_DESTROYED ) 
				SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED ) 
				SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL ) 
				SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL )
				SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
			ELSE
				// nothing to be done
			ENDIF
		BREAK

		CASE SP_HEIST_JEWELRY_PREP_1A
			IF bSkipToBefore = TRUE
				SET_BUILDING_STATE(BUILDINGNAME_SB_BUGSTAR_DOCKS_1, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_SB_BUGSTAR_DOCKS_2, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_SB_BUGSTAR_DOCKS_3, BUILDINGSTATE_DESTROYED)
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_9, TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_SB_BUGSTAR_DOCKS_1, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_SB_BUGSTAR_DOCKS_2, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_SB_BUGSTAR_DOCKS_3, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_JEWEL_HEIST_SWEATSHOP_BUGSTAR_GEAR, BUILDINGSTATE_DESTROYED)
				EXECUTE_CODE_ID(CID_FAST_TRACK_BZ_GAS_PREP_UNLOCK, 0)
			ENDIF
		BREAK

		CASE SP_HEIST_JEWELRY_PREP_1B
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_11, TRUE)
			ENDIF
		BREAK
	
		CASE SP_HEIST_JEWELRY_PREP_2A
			IF bSkipToBefore = TRUE
				Execute_Code_ID(CID_UNLOCK_BZ_GAS_PREP) // check
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL, PBDG_10, TRUE)
			ENDIF
		BREAK
		
		CASE SP_HEIST_JEWELRY_2
			IF bSkipToBefore = TRUE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_JEWEL,	PBDG_4,	TRUE)
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_CLEANUP)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_JEWEL_HEIST_SWEATSHOP_BUGSTAR_GEAR, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
				SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
				SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
				SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL ) 
				SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL )
				Execute_Code_ID(CID_UNLOCK_JEWEL_HEIST_OUTFITS, 0)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_JEWEL, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE, FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LESTER, FRANKLIN_BOOK,	FALSE)
				
				ADD_CONTACT_TO_PHONEBOOK(CHAR_RON, TREVOR_BOOK,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_WADE, TREVOR_BOOK,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_ONEIL, TREVOR_BOOK,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_OSCAR, TREVOR_BOOK,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_CHEF, TREVOR_BOOK,	FALSE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_TAXI, TREVOR_BOOK,	FALSE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_CARBINERIFLE, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_SMOKEGRENADE, TRUE)
			ENDIF
		BREAK

		//-------------------------RURAL HEIST STRAND-----------------------------------------------
		CASE SP_HEIST_RURAL_1
			IF bSkipToBefore = TRUE
				SET_DOOR_STATE(DOORNAME_METHLAB_F_L, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_METHLAB_F_R, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_METHLAB_R, DOORSTATE_UNLOCKED)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING,	FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING,	FALSE)
			ELSE
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_RURAL_BANK, PBDG_1, TRUE)
				ADD_CONTACT_TO_PHONEBOOK(CHAR_LESTER, TREVOR_BOOK, FALSE)
			ENDIF
		BREAK
		
		CASE SP_HEIST_RURAL_PREP_1
			IF bSkipToBefore = TRUE
				// nothing to be done
			ELSE
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_RURALH_MILITARY_TRUCK, TRUE)
				REPEAT_PLAY_SET_BOARD_DISPLAY_GROUP_VISIBILITY(HEIST_RURAL_BANK, PBDG_3, TRUE)
			ENDIF
		BREAK

		CASE SP_HEIST_RURAL_2
			IF bSkipToBefore = TRUE
				SET_BUILDING_STATE(BUILDINGNAME_ES_PALETO_SWEATSHOP_ARMOUR, BUILDINGSTATE_DESTROYED) 
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_BNKHEIST_APT, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_BNKHEIST_APT_VFX, BUILDINGSTATE_DESTROYED)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_RURALH_MILITARY_TRUCK, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_RURAL_BANK, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_MG, TRUE)
				REPEAT_PLAY_SET_WEAPON_LOCK_STATE(WEAPONTYPE_MINIGUN, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_5, TRUE)
			ENDIF
		BREAK
	
		DEFAULT
			CERRORLN(DEBUG_REPEAT, "REPEAT_PLAY_SKIP_STORY_MISSION trying to skip unknown mission. eMission =", eMission)
			SCRIPT_ASSERT("REPEAT_PLAY_SKIP_STORY_MISSION trying to skip unknown mission. Bug for Andy Minghella")
			
			#IF USE_TU_CHANGES
				bMissionSkipped = FALSE
			#ENDIF
			
		BREAK
	
	ENDSWITCH
	
	
	#IF USE_TU_CHANGES
		IF bMissionSkipped
			g_savedGlobals.sFlow.missionSavedData[eMission].completed = TRUE
		ENDIF
	#ENDIF
	
	#endif
	#endif
ENDPROC


/// PURPOSE:
///    Resets all unlocks we are concerned with.
///    Called when starting a repeat play.
PROC REPEAT_PLAY_RESET()
	CPRINTLN(DEBUG_REPEAT, "Repat play is resetting game world.")
	g_flowUnsaved.bUpdatingGameflow = TRUE

	// Kill off any mission scripts that are currently running.
	//CLEANUP_RUNNING_MISSION_SCRIPTS()

	// Reset the single player mission flow state. (this resets all flow flags)
	RESET_SP_MISSION_FLOW()
	
	// Reset the state of the communication controller.
	RESET_COMMUNICATION_CONTROLLER()
	g_bPauseCommsQueues = TRUE
	g_iCallInProgress = -1

	// Clean up the state of the cellphone.
	RESET_CELLPHONE()
	
	// Reset the state of the data for the code controller.
	RESET_CODE_CONTROLLER()

	// Reset the complete status of all missions.
	RESET_MISSION_DATA()
	
#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	// Reset all heist globals.
	RESET_HEIST_DATA()
#endif	
#endif
	// Reset the state of all static blips.
	RESET_STATIC_BLIP_STATES()

	// Reset the default respawn location flags.
	RESET_RESPAWN_LOCATION_DATA_FLAGS()

	// Reset the default player info and stat flags.
	//RESET_PLAYER_DATA_FLAGS()

	// Reset the default shop flags.
	RESET_SHOP_DATA_FLAGS()

	// Reset the Mission Triggering System.
	RESET_ALL_MISSION_TRIGGERS()

	// Reset the default building and door flags.
	RESET_BUILDING_AND_DOOR_FLAGS()

	// Reset the context system.
	RESET_CONTEXT_SYSTEM()

	// Reset the help text chain controller.
	PRIVATE_Clear_Flow_Help_Queue()
	PAUSE_FLOW_HELP_QUEUE(FALSE)
	
	// Reset replay controller
	//RESET_REPLAY_CONTROLLER()

	// Reset the ambient saved flags.
	//RESET_AMBIENT_FLAGS()

	// Reset the vehicle gen flags.
	RESET_VEHICLE_GEN_FLAGS()

	// Reset the player's special ability system.
	RESET_PLAYER_SPECIAL_ABILITY()
	
#if not USE_CLF_DLC		
#if not USE_NRM_DLC
	// Reset the Random Character controller.
	RESET_RANDOM_CHARACTER_CONTROLLER()
	// Reset the state of all random events.
	//RESET_RANDOM_EVENT_STATES()	
	// Reset the Friend controller
	RESET_FRIEND_CONTROLLER()
#endif	
#endif
	// Reset the state of any cutscenes that are currently active.
	RESET_CUTSCENES()
	
	// Resets all the app data
	//RESET_APP_DATA() 
	
	// Reset 100% completion
	//RESET_COMPLETION_PERCENTAGE_TRACKING()
	
	INITIALISE_EMAIL_SYSTEM()
	
	RESET_PLAYER_STATE()
	
	g_flowUnsaved.bUpdatingGameflow = FALSE
ENDPROC

/// PURPOSE:
///    Skips over mission the player played before the mission we want to repeat play.
///    Unlocks / locks anything that is triggered by completing that mission.
///    Unlocks / locks anything that is triggered by starting the target mission.
/// PARAMS:
///    iTargetMissionID - the mission we want to repeat play.
///    eMissionType - which type of mission it is (Story / RC)
PROC REPEAT_PLAY_UNLOCKING(INT iTargetMissionID, enumGrouping eMissionType)

	INT iCurrentMission
	INT iTargetMission
	IF ENUM_TO_INT(SP_MISSION_MAX) <> 94
	OR ENUM_TO_INT(MAX_RC_MISSIONS) <> 63
		CPRINTLN(DEBUG_REPEAT, "REPEAT_PLAY_UNLOCKING needs to update skip list size. SP_MISSION_MAX= ", SP_MISSION_MAX, ". MAX_RC_MISSIONS= ", MAX_RC_MISSIONS, ".")
		SCRIPT_ASSERT("REPEAT_PLAY_UNLOCKING needs to update skip list size. Bug for Andy Minghella.")
	ENDIF
	
	CONST_INT MAX_SKIP_LIST 157
	// This needs to be SP_MISSION_MAX (94)  + MAX_RC_MISSIONS (63) = 157
	//(total story missions + total RC missions)
	RepeatPlayData mSkipList[MAX_SKIP_LIST]
	
	
	// List of flow flags we may want to reinstate
	// (These will just be ones that aren't set via the standard mission flow stuff)
	
	SP_MISSIONS eFinaleCompleted = SP_MISSION_NONE
		
	// ------------store flow flags we need to reinstate---------------------

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT iJewelHeistChoice
	BOOL bMcClipped 
	BOOL bKilledMulligan
	INT iAgencyHeistChoice
	INT iFinaleHeistChoice
	BOOL bDiNapoliKilled
	BOOl bFranklinHaircut
	INT iFranklinOutfit
	
	IF Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_COMPLETE)
		bFranklinHaircut = TRUE // Franklin has bought a new haircut 
	ENDIF
	iFranklinOutfit = g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID // outfit Franklin was wearing when Lamar commented on it
	iJewelHeistChoice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_JEWEL)
	CPRINTLN(DEBUG_REPEAT, "Stored iJewelHeistChoice as ", iJewelHeistChoice)	
	bMcClipped = GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FRANK1_MC_CLIPPED)
	CPRINTLN(DEBUG_REPEAT, "Stored bMcClipped as ", bMcClipped)	
	bKilledMulligan = GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN)
	CPRINTLN(DEBUG_REPEAT, "Stored bKilledMulligan as ", bKilledMulligan)	
	iAgencyHeistChoice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY)
	CPRINTLN(DEBUG_REPEAT, "Stored iAgencyHeistChoice as ", iAgencyHeistChoice)	
	iFinaleHeistChoice = GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE) 
	CPRINTLN(DEBUG_REPEAT, "Stored iFinaleHeistChoice as ", iFinaleHeistChoice)	
	bDiNapoliKilled = GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED)
	CPRINTLN(DEBUG_REPEAT, "Stored bDiNapoliKilled as ", bDiNapoliKilled)
	
	
	//---------- check which Finale the player completed in flow--------
	IF g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FINALE_A].completed = TRUE
		eFinaleCompleted = SP_MISSION_FINALE_A
	ELIF g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FINALE_B].completed = TRUE
		eFinaleCompleted = SP_MISSION_FINALE_B
	ELIF g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FINALE_C2].completed = TRUE
		eFinaleCompleted = SP_MISSION_FINALE_C2
	ENDIF
#endif
#endif

	// ----------Set Target Mission-------------------------------
	iTargetMission = GetMissionCompletionOrder(iTargetMissionID, eMissionType)
	CPRINTLN(DEBUG_REPEAT,"iTargetMissionID= ", iTargetMissionID)
	CPRINTLN(DEBUG_REPEAT,"iTargetMission= ", iTargetMission)
	IF eMissionType = CP_GROUP_MISSIONS
		CPRINTLN(DEBUG_REPEAT,"iTargetMissionID= ", iTargetMissionID, ", ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iTargetMissionID))) 
	ELSE
		CPRINTLN(DEBUG_REPEAT,"iTargetMissionID= ", iTargetMissionID, ", ", GET_RC_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(g_eRC_MissionIDs, iTargetMissionID))) 
	ENDIF
	

	// -----------set up the mission flow for repeat play -------------------------

#if USE_CLF_DLC		
	PopulateRepeatPlayArray(mSkipList, TRUE, FALSE, FALSE)	
#endif
#if USE_NRM_DLC	
	PopulateRepeatPlayArray(mSkipList, TRUE, FALSE, FALSE)	
#endif
#if not USE_CLF_DLC		
#if not USE_NRM_DLC
	PopulateRepeatPlayArray(mSkipList, TRUE, TRUE, FALSE)
#endif
#endif

	// reset the flow
	REPEAT_PLAY_RESET()
	
#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	// Reinstate any flow flags we've stored here
	IF bFranklinHaircut = TRUE 
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_COMPLETE, TRUE)
		CPRINTLN(DEBUG_REPEAT,"Reinstating Franklin haircut flow flag.")
	ENDIF
	g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID = iFranklinOutfit	
#endif	
#endif 

	BOOL bTargetMission = FALSE
	
	// loop through the missions the player has completed unlocking stuff as we go
	FOR iCurrentMission = 0 TO iTargetMission
	
		// set bool to say is target mission
		// use this to not do finale skip stuff 
		// if finale isnt target mission
		// and not been completed- do nothing!!
		IF iCurrentMission = iTargetMission
			CPRINTLN(DEBUG_REPEAT, "This is the target mission!")
			bTargetMission = TRUE
		ELSE
			bTargetMission = FALSE
		ENDIF
	
		// unlock the start of this mission
		SWITCH mSkipList[iCurrentMission].eMissionType
			CASE CP_GROUP_MISSIONS
				#if  USE_CLF_DLC	
					REPEAT_PLAY_SKIP_STORY_MISSION_CLF(INT_TO_ENUM(SP_MISSIONS, mSkipList[iCurrentMission].iMissionIndex), TRUE, bTargetMission, eFinaleCompleted)
					IF iCurrentMission < iTargetMission
						// for all missions other than the target, we unlock anything that is unlocked by completing this mission
						REPEAT_PLAY_SKIP_STORY_MISSION_CLF(INT_TO_ENUM(SP_MISSIONS, mSkipList[iCurrentMission].iMissionIndex), FALSE, bTargetMission, eFinaleCompleted)
						//add news stuff here						
					ENDIF
				#endif
				#if  USE_NRM_DLC	
					REPEAT_PLAY_SKIP_STORY_MISSION_NRM(INT_TO_ENUM(SP_MISSIONS, mSkipList[iCurrentMission].iMissionIndex), TRUE, bTargetMission, eFinaleCompleted)
					IF iCurrentMission < iTargetMission
						// for all missions other than the target, we unlock anything that is unlocked by completing this mission
						REPEAT_PLAY_SKIP_STORY_MISSION_NRM(INT_TO_ENUM(SP_MISSIONS, mSkipList[iCurrentMission].iMissionIndex), FALSE, bTargetMission, eFinaleCompleted)
						//add news stuff here						
					ENDIF
				#endif
				
				#if not USE_CLF_DLC	
				#if not USE_NRM_DLC
					REPEAT_PLAY_SKIP_STORY_MISSION(INT_TO_ENUM(SP_MISSIONS, mSkipList[iCurrentMission].iMissionIndex), TRUE, bTargetMission, eFinaleCompleted)
					
					IF iCurrentMission < iTargetMission
						// for all missions other than the target, we unlock anything that is unlocked by completing this mission
						REPEAT_PLAY_SKIP_STORY_MISSION(INT_TO_ENUM(SP_MISSIONS, mSkipList[iCurrentMission].iMissionIndex), FALSE, bTargetMission, eFinaleCompleted)
						
						// update news story for eye find news
						IF IS_FINALE_MISSION(mSkipList[iCurrentMission].iMissionIndex)
							IF ENUM_TO_INT(eFinaleCompleted) = mSkipList[iCurrentMission].iMissionIndex
								CPRINTLN(DEBUG_REPEAT, "Doing news story for completed finale mission. eFinaleCompleted= ", mSkipList[iCurrentMission].iMissionIndex)
								UPDATE_EYEFIND_CURRENT_NEWS_STORY_STATE(mSkipList[iCurrentMission].iMissionIndex, FALSE)
							ELSE
								CPRINTLN(DEBUG_REPEAT, "NOT doing news story for other finale mission. mission= ", mSkipList[iCurrentMission].iMissionIndex)
							ENDIF
						ELSE
							CPRINTLN(DEBUG_REPEAT, "Doing news story for completed story mission. eFinaleCompleted= ", mSkipList[iCurrentMission].iMissionIndex)
							UPDATE_EYEFIND_CURRENT_NEWS_STORY_STATE(mSkipList[iCurrentMission].iMissionIndex, FALSE)
						ENDIF
					ENDIF
				#endif
				#endif
			BREAK
			
			CASE CP_GROUP_RANDOMCHARS
				REPEAT_PLAY_SKIP_RC_MISSION(INT_TO_ENUM(g_eRC_MissionIDs ,mSkipList[iCurrentMission].iMissionIndex), TRUE)
			
				IF iCurrentMission < iTargetMission
					// for all missions other than the target, we unlock anything that is unlocked by completing this mission
					REPEAT_PLAY_SKIP_RC_MISSION(INT_TO_ENUM(g_eRC_MissionIDs ,mSkipList[iCurrentMission].iMissionIndex), FALSE)
				
					// update news story for eye find news
					CPRINTLN(DEBUG_REPEAT, "Doing news story for completed RC mission. eFinaleCompleted= ", mSkipList[iCurrentMission].iMissionIndex)
					UPDATE_EYEFIND_CURRENT_NEWS_STORY_STATE(mSkipList[iCurrentMission].iMissionIndex, TRUE)
				ENDIF
			BREAK
			
			DEFAULT
				CPRINTLN(DEBUG_REPEAT,"REPEAT_PLAY_UNLOCKING passed a mission type that isn't repeat playable: ", mSkipList[iCurrentMission].eMissionType)			
				CPRINTLN(DEBUG_REPEAT,"CurrentMission =  ", iCurrentMission, ". MissionID = ", mSkipList[iCurrentMission].iMissionIndex, ".")
			
			BREAK
		ENDSWITCH
	ENDFOR

	// reset the completion order variables
	INT iMission
	INT iMissionID
	
#if USE_CLF_DLC	
		
	g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted = 0
	FOR iMission = 0 TO MAX_MISSION_DATA_SLOTS -1
		g_savedGlobalsClifford.sFlow.missionSavedData[iMission].iCompletionOrder = -1
	ENDFOR	
	// and rebuild them so lifeinvader works
	FOR iMission = 0 TO iTargetMission
		IF mSkipList[iMission].eMissionType = CP_GROUP_MISSIONS
			iMissionID = mSkipList[iMission].iMissionIndex
			g_savedGlobalsClifford.sFlow.missionSavedData[iMissionID].iCompletionOrder = iMission
			g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted++
			CPRINTLN(DEBUG_REPEAT,"USMCO iMissionID ", iMissionID, " iCompletionOrder = ", g_savedGlobalsClifford.sFlow.missionSavedData[iMissionID].iCompletionOrder)
		ENDIF
	ENDFOR	
#endif	
#if USE_NRM_DLC
		
	g_savedGlobalsnorman.sFlowCustom.iMissionsCompleted = 0
	FOR iMission = 0 TO MAX_MISSION_DATA_SLOTS -1
		g_savedGlobalsnorman.sFlow.missionSavedData[iMission].iCompletionOrder = -1
	ENDFOR	
	// and rebuild them so lifeinvader works
	FOR iMission = 0 TO iTargetMission
		IF mSkipList[iMission].eMissionType = CP_GROUP_MISSIONS
			iMissionID = mSkipList[iMission].iMissionIndex
			g_savedGlobalsnorman.sFlow.missionSavedData[iMissionID].iCompletionOrder = iMission
			g_savedGlobalsnorman.sFlowCustom.iMissionsCompleted++
			CPRINTLN(DEBUG_REPEAT,"USMCO iMissionID ", iMissionID, " iCompletionOrder = ", g_savedGlobalsnorman.sFlow.missionSavedData[iMissionID].iCompletionOrder)
		ENDIF
	ENDFOR	
#endif	

#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	g_savedGlobals.sFlowCustom.iMissionsCompleted = 0
	FOR iMission = 0 TO MAX_MISSION_DATA_SLOTS -1
		g_savedGlobals.sFlow.missionSavedData[iMission].iCompletionOrder = -1
	ENDFOR
	g_savedGlobals.sRandomChars.iRCMissionsCompleted =0
	FOR iMission = 0 TO ENUM_TO_INT(MAX_RC_MISSIONS) -1
		g_savedGlobals.sRandomChars.savedRC[iMission].iCompletionOrder = -1
	ENDFOR
	// and rebuild them so lifeinvader works
	FOR iMission = 0 TO iTargetMission
		IF mSkipList[iMission].eMissionType = CP_GROUP_MISSIONS
			iMissionID = mSkipList[iMission].iMissionIndex
			g_savedGlobals.sFlow.missionSavedData[iMissionID].iCompletionOrder = iMission
			g_savedGlobals.sFlowCustom.iMissionsCompleted++
			CPRINTLN(DEBUG_REPEAT,"USMCO iMissionID ", iMissionID, " iCompletionOrder = ", g_savedGlobals.sFlow.missionSavedData[iMissionID].iCompletionOrder)

		ELIF mSkipList[iMission].eMissionType = CP_GROUP_RANDOMCHARS
			iMissionID = mSkipList[iMission].iMissionIndex
			g_savedGlobals.sRandomChars.savedRC[iMissionID].iCompletionOrder = iMission
			g_savedGlobals.sRandomChars.iRCMissionsCompleted++
			CPRINTLN(DEBUG_REPEAT,"URCMCO iMissionID ", iMissionID, " iCompletionOrder = ",  g_savedGlobals.sRandomChars.savedRC[iMissionID].iCompletionOrder)
		ENDIF
	ENDFOR
	
	// restore flowflag states for eyefind news
	IF g_savedGlobals.sFlow.missionSavedData[SP_HEIST_JEWELRY_2].iCompletionOrder > -1
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_JEWEL, iJewelHeistChoice) 
		CPRINTLN(DEBUG_REPEAT, "Restored iJewelHeistChoice as ", iJewelHeistChoice)
	ENDIF	
	IF g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FRANKLIN_1].iCompletionOrder > -1
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FRANK1_MC_CLIPPED, bMcClipped)
		CPRINTLN(DEBUG_REPEAT, "Restored bMcClipped as ", bMcClipped)
	ENDIF	
	IF g_savedGlobals.sFlow.missionSavedData[SP_MISSION_CARSTEAL_2].iCompletionOrder > -1
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN, bKilledMulligan)
		CPRINTLN(DEBUG_REPEAT, "Restored bKilledMulligan as ", bKilledMulligan)
	ENDIF	
	IF g_savedGlobals.sFlow.missionSavedData[SP_HEIST_AGENCY_3A].iCompletionOrder > -1
	OR g_savedGlobals.sFlow.missionSavedData[SP_HEIST_AGENCY_3B].iCompletionOrder > -1
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY, iAgencyHeistChoice) 
		CPRINTLN(DEBUG_REPEAT, "Restored iAgencyHeistChoice as ", iAgencyHeistChoice)
	ENDIF	
	IF g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_2A].iCompletionOrder > -1
	OR g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_2B].iCompletionOrder > -1
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE, iFinaleHeistChoice) 
		CPRINTLN(DEBUG_REPEAT, "Restored iFinaleHeistChoice as ", iFinaleHeistChoice)
	ENDIF	
	IF g_savedGlobals.sRandomChars.savedRC[RC_NIGEL_3].iCompletionOrder > -1
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED, bDiNapoliKilled)
		CPRINTLN(DEBUG_REPEAT, "Restored bDiNapoliKilled as ", bDiNapoliKilled)
	ENDIF
#endif
#endif

	
ENDPROC
