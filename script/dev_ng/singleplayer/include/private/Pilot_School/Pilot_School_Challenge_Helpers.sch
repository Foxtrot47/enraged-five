USING "globals.sch"
USING "rage_builtins.sch"
USING "stack_sizes.sch"

//-	commands headers	-//
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_xml.sch"
USING "commands_water.sch"

USING "CompletionPercentage_public.sch"
USING "area_checks.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "minigames_helpers.sch"
USING "script_oddjob_funcs.sch"

USING "stunt_plane_public.sch"
USING "Pilot_School_Definitions.sch"
USING "Pilot_School_Data.sch"
USING "PS_Checkpoint_lib.sch"
#IF IS_DEBUG_BUILD
	USING "Pilot_School_Debug_lib.sch"
#ENDIF
//USING "PS_Objective_lib.sch"
USING "ps_launcher_shared.sch"
USING "Pilot_School_HUD_lib.sch"

//***************************************************************************************************
//General stuff that needs to be defined at the top of this file because it's used by other functions
//***************************************************************************************************


LANDING_GEAR_STATE testState

// Use a custom near clip for the PC, because extreme aspect ratios (5760x1200) are possible, B*2075120
FUNC FLOAT PS_GET_NEAR_CLIP()
	IF IS_PC_VERSION()
		RETURN 0.5
	ENDIF
	
	RETURN 0.840
ENDFUNC

PROC PS_INIT_CHALLENGE_VARIABLES()
	
	#IF IS_DEBUG_BUILD
		bPlayerUsedDebugSkip = FALSE
	#ENDIF
	
	IF NOT bRetryChallenge
		bKeepVehicleSetup							= FALSE
	ENDIF
	
	PS_INIT_HINT_CAM()
	
	iPSMusicBits = 0
	PS_PreviewState = PS_PREVIEW_INIT
	ePS_TruckLandingState = PS_TRUCK_LANDING_STATE_01
	ePS_TruckCrashingState = PS_TRUCK_CRASHING_STATE_01
	eSubState = PS_SUBSTATE_ENTER
	
	//make sure we're not suppressing the vehicle shadow (used at the end of a lesson during the score card)
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		SET_ENTITY_VISIBLE(PS_Main.myVehicle, TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	CANCEL_TIMER(PS_Main.tCountdownTimer)
	CANCEL_TIMER(PS_Main.tPulse)
	CANCEL_TIMER(PS_CountDownUI.CountdownTimer)
	CANCEL_TIMER(PS_Main.tHealthTimer)
	CANCEL_TIMER(tRetryMenuTransition)
	CANCEL_TIMER(Fail_Timer_Runway)
	PS_CountDownUI.iBitFlags = 0
	
	vPilotSchoolPreviewCoords = <<0, 0, 0>>
	
	PS_Main.myObjectiveData.fPlaneTotalOrient = 0

	iNumMissedCheckpoints						= 0	
	iRangeTimerWarning							= 1
	iPreviewCheckpointIdx						= 0
	PS_UI_RaceHud.iTimeBonus									= 0
	iInstructionCounter 						= 1
	iCurrentChallengeScore						= 0

	fCheckpointDistAway 						= -1.0
	fPreviewVehicleSkipTime						= 0.0
	sPreviewRecordingName						= "PilotSchool"
	
	//lesson bools
	fCurPlaneSpeed								= -1
	iFailDialogue								= 1
	bLessonCompleted							= FALSE
	bLesonFinishedLineTriggered					= FALSE
//	bDontFadeToMenu								= TRUE //make sure we dont fade to menu unless we set this bool
	PS_Main.myObjectiveData.bRetryFlag 			= FALSE
	PS_UI_ObjectiveMeter.bIsLoopMeterActive 	= FALSE
	PS_UI_ObjectiveMeter.bIsRollMeterActive 	= FALSE
	PS_UI_ObjectiveMeter.bIsInvertedMeterActive = FALSE
	PS_UI_ObjectiveMeter.bIsLoopMeterActive 	= FALSE
	PS_UI_ObjectiveMeter.bIsImmelMeterActive 	= FALSE
	PS_UI_ObjectiveMeter.bIsObjMeterVisible 	= FALSE
	PS_UI_ObjectiveMeter.bCapObjectiveBar 		= FALSE
	bPlayerDataHasBeenUpdated					= FALSE
	bScreenFadeVisible							= TRUE //i dont think this is necessary
	bEnteredLocate 								= FALSE
	PS_UI_RaceHud.bIsTimerActive				= FALSE
	PS_UI_RaceHud.bIsDistActive					= FALSE
	bIsAltimeterActive 							= FALSE
	bIsAltitudeIndicatorActive					= FALSE
	PS_UI_RaceHud.bCheckpointCounterVisible		= FALSE
	PS_UI_RaceHud.bIsTimerVisible				= FALSE
	bIsAltimeterVisible							= FALSE
	bIsAltitudeIndicatorVisible					= FALSE
	bIsScoreCardVisible							= FALSE
	bIsEndCutActive								= FALSE
	bPlayerInVehicle							= FALSE
	bSkippedPreview								= FALSE
	bPreviewCleanupOkay							= FALSE
	bFinishedChallenge 							= FALSE
	bFinishedTruckLandingScene					= FALSE
	bRagdollingOnTruck							= FALSE
	bIsPSLeaderboardWriting						= FALSE
	
	PS_SoundID_Formation_Alarm					= -1
	PS_SoundID_Alt_Meter_Alarm 					= -1
	iScreenFadeTime								= PS_SCREEN_FADE_DEFAULT_TIME
	PS_UI_RaceHud.iPSHUDExtraTime								= 0
	
	PS_Inverted_State 							= PS_INVERTED_INIT
	PS_Knifing_State 							= PS_KNIFING_INIT
	PS_Looping_State 							= PS_LOOPING_INIT
	PS_Fly_Low_State 							= PS_FLY_LOW_INIT
	PS_Daring_State 							= PS_DARING_INIT
	PS_Plane_Course_State						= PS_PLANE_COURSE_INIT
	PS_Heli_Speed_State 						= PS_HELI_SPEED_INIT
	PS_Heli_Course_State 						= PS_HELI_COURSE_INIT
	PS_Parachute_State 							= PS_PARACHUTE_INIT
	PS_Moving_Chute_State 						= PS_MOVING_CHUTE_INIT
	
	INT i = 0
	REPEAT COUNT_OF(iPlayedOutEncourageDialogue) i
		iPlayedOutEncourageDialogue[i] = 0
	ENDREPEAT
	i = 0
	REPEAT COUNT_OF(iPlayedOutEncourageDialogue) i
		iPlayedOutRewardDialogue[i] = 0
	ENDREPEAT
	i = 0
	REPEAT COUNT_OF(iPlayedOutEncourageDialogue) i
		iPlayedOutDiscourageDialogue[i] = 0
	ENDREPEAT
ENDPROC


PROC PS_CLEANUP_PLAYER()
	CLEAR_PED_WETNESS(PLAYER_PED_ID())
	CLEAR_PED_ENV_DIRT(PLAYER_PED_ID())
	CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
	RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
ENDPROC

PROC PS_REQUEST_SHARED_ASSETS()
	
ENDPROC

FUNC BOOL PS_ARE_SHARED_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC

FUNC VECTOR PS_CONVERT_ROTATION_TO_DIRECTION(VECTOR vectorToCovert)
//	FLOAT fVecHeading = vectorToCovert.z
//	FLOAT fVecPitch = vectorToCovert.x
	FLOAT dir_x = -SIN(vectorToCovert.z) * COS(vectorToCovert.x) //COS(fVecHeading)
	FLOAT dir_y = COS(vectorToCovert.z) * COS(vectorToCovert.x) //SIN(fVecHeading)
	FLOAT dir_z = SIN(vectorToCovert.x) //dir_x*dir_y*TAN(fVecPitch)
	
	RETURN <<dir_x, dir_y, dir_z>>
	
ENDFUNC	

PROC PS_INIT_INTRO_SCENE(PS_LAUNCH_ARGS &psArgs)
	psArgs.ePS_LaunchScene = PS_LAUNCH_SCENE_INIT

ENDPROC

FUNC BOOL PS_UPDATE_INTRO_SCENE(PS_LAUNCH_ARGS &psArgs)
	SWITCH(psArgs.ePS_LaunchScene)	
		
		CASE PS_LAUNCH_SCENE_INIT
			IF DOES_CAM_EXIST(camMainMenu)
				DESTROY_CAM(camMainMenu)
			ENDIF
			IF DOES_CAM_EXIST(camMainMenu1)
				DESTROY_CAM(camMainMenu1)
			ENDIF
			camMainMenu1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1150.0171, -2714.2539, 20.6792>>, <<34.3772, 0.0000, 99.9707>>, 50.0000 , TRUE)
			RESTART_TIMER_NOW(psArgs.tLaunchSceneTimer)
			psArgs.ePS_LaunchScene = PS_LAUNCH_SCENE_SETUP_PLAYER
			BREAK
		
		CASE PS_LAUNCH_SCENE_SETUP_PLAYER
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				IF NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				ENDIF
			ENDIF
			RESTART_TIMER_NOW(psArgs.tLaunchSceneTimer)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			psArgs.ePS_LaunchScene = PS_LAUNCH_SCENE_01_HOLD_CAMERA_SHOT
			BREAK
		
		CASE PS_LAUNCH_SCENE_01_HOLD_CAMERA_SHOT
			IF TIMER_DO_WHEN_READY(psArgs.tLaunchSceneTimer, 1.0)
				psArgs.ePS_LaunchScene = PS_LAUNCH_SCENE_02_ACTIVATE_CAMERA_PAN
			ENDIF
			BREAK
		
		CASE PS_LAUNCH_SCENE_02_ACTIVATE_CAMERA_PAN
			//PLAY_SOUND_FRONTEND(-1, "PAN_SOFT_LONG", "HUD_MINI_GAME_SOUNDSET")
			ODDJOB_PLAY_SOUND("PAN_SOFT_LONG", iIntroPanSoundID, FALSE, NULL, 0, 0, 0, "HUD_MINI_GAME_SOUNDSET")
		
			camMainMenu = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, PS_MENU_CAM_COORDS, PS_MENU_CAM_ROT, PS_MENU_CAM_FOV, FALSE)
			IF DOES_CAM_EXIST(camMainMenu)
				SET_CAM_ACTIVE_WITH_INTERP(camMainMenu, camMainMenu1, 5000)
			ENDIF
			RESTART_TIMER_NOW(psArgs.tLaunchSceneTimer)
			psArgs.ePS_LaunchScene = PS_LAUNCH_SCENE_03_WAIT_FOR_CAMERA_PAN
			BREAK
			
		CASE PS_LAUNCH_SCENE_03_WAIT_FOR_CAMERA_PAN
			//IF TIMER_DO_WHEN_READY(psArgs.tLaunchSceneTimer, 5.0)
			IF NOT IS_CAM_INTERPOLATING(camMainMenu)
				ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
				psArgs.ePS_LaunchScene = PS_LAUNCH_SCENE_CLEANUP
			ENDIF
			BREAK
		
		CASE PS_LAUNCH_SCENE_CLEANUP
			//release assets
			IF DOES_CAM_EXIST(camMainMenu1)
				DESTROY_CAM(camMainMenu1)
			ENDIF
			RETURN FALSE
			BREAK
			
	ENDSWITCH
	//still updating
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Loads in the texture dictionary used for things like checkpoints and hud elements
PROC Pilot_School_Load_Texture_Dictionary()
	REQUEST_STREAMED_TEXTURE_DICT(txdDictName)
	WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED(txdDictName)
		WAIT(0)
	ENDWHILE
ENDPROC

FUNC BOOL PS_GET_RANDOM_CHANCE()
	//66%
	IF GET_RANDOM_INT_IN_RANGE() % 3 != 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_SET_GROUND_OFFSET(FLOAT tempfloat)
	GroundOffset = tempfloat
ENDPROC

PROC PS_SETUP_CHALLENGE()	
	PS_HUD_SETUP()
	CLEAR_HELP()	
	CLEAR_PRINTS()
	CLEAR_AREA(vStartPosition, 1000, TRUE)
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		DELETE_VEHICLE(PS_Main.myVehicle)
	ENDIF
	
	CLEAR_PED_WETNESS(PLAYER_PED_ID())
	
	ePSFailReason = PS_FAIL_NO_REASON
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
//	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	DISABLE_CELLPHONE(TRUE)
	SET_FRONTEND_RADIO_ACTIVE(FALSE)
	Pilot_School_Load_Texture_Dictionary()
	REQUEST_MODEL(VehicleToUse)
	PS_UI_BigMessage.siMovie = REQUEST_MG_BIG_MESSAGE()
	
	WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\DRUG_TRAFFIC_AIR")
		PRINTLN("Flight School - Waiting on audio bank to load")
		WAIT(0)
	ENDWHILE
	
	WHILE NOT HAS_MODEL_LOADED(VehicleToUse)
		PRINTLN("Flight School - Waiting on vehicle to load")
		WAIT(0)
	ENDWHILE
ENDPROC

PROC PS_CREATE_VEHICLE()
	PS_Main.myVehicle = CREATE_VEHICLE(VehicleToUse, vStartPosition, fStartHeading)
	PRINTLN("PS_CREATE_VEHICLE: creating vehicle at ", vStartPosition, " with heading of ", fStartHeading)	
	IF IS_THIS_MODEL_A_HELI(VehicleToUse)
		PRINTLN("setting vehicle extras!!")
		SET_VEHICLE_EXTRA(PS_Main.myVehicle, 1, FALSE) //FALSE turns them on
		SET_VEHICLE_EXTRA(PS_Main.myVehicle, 2, FALSE)
		SET_VEHICLE_EXTRA(PS_Main.myVehicle, 5, FALSE)
		SET_VEHICLE_EXTRA(PS_Main.myVehicle, 8, FALSE)
		SET_VEHICLE_EXTRA(PS_Main.myVehicle, 9, FALSE)
	ENDIF
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)	
		SET_VEHICLE_USED_FOR_PILOT_SCHOOL(PS_Main.myVehicle, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(PS_Main.myVehicle, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(PS_Main.myVehicle)
		SET_VEHICLE_NAME_DEBUG(PS_Main.myVehicle, "PS_Main.myVehicle")
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(PS_Main.myVehicle))
			ENABLE_STALL_WARNING_SOUNDS(PS_Main.myVehicle, FALSE)
		ENDIF
		IF bKeepVehicleSetup
			SET_VEHICLE_SETUP(PS_Main.myVehicle, PS_Main.myVehicleSetup)
		ELSE
			GET_VEHICLE_SETUP(PS_Main.myVehicle, PS_Main.myVehicleSetup)
		ENDIF
		
		//place the player in the vehicle.
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),PS_Main.myVehicle)
		ENDIF
		
		PRINTLN("NOT IS_VEHICLE_FUCKED")
	ELSE
		PRINTLN("IS_VEHICLE_FUCKED!!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Temporary debug exit for our challenge. Hit enter and you automatically get thrown to the main menu
/// RETURNS:
///    
FUNC BOOL TempDebugInput()
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	#ENDIF
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
	PROC PS_DEBUG_PRINT_PARACHUTE_STATE(PED_PARACHUTE_STATE thisParachuteState)
		SWITCH(thisParachuteState)
			CASE PPS_INVALID
				DEBUG_MESSAGE("PPS_INVALID")
				BREAK
			CASE PPS_SKYDIVING
				DEBUG_MESSAGE("PPS_SKYDIVING")
				BREAK
			CASE PPS_DEPLOYING
				DEBUG_MESSAGE("PPS_DEPLOYING")
				BREAK
			CASE PPS_PARACHUTING
				DEBUG_MESSAGE("PPS_PARACHUTING")
				BREAK
			CASE PPS_LANDING
				DEBUG_MESSAGE("PPS_LANDING")
				BREAK
			
		ENDSWITCH
	ENDPROC

	PROC PS_DEBUG_PRINT_TASK_STATE(SCRIPTTASKSTATUS thisTaskState)
		IF thisTaskState = WAITING_TO_START_TASK
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - WAITING_TO_START_TASK")
		ELIF thisTaskState = PERFORMING_TASK
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - PERFORMING_TASK")
		ELIF thisTaskState = DORMANT_TASK
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - DORMANT_TASK")
		ELIF thisTaskState = VACANT_STAGE
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - VACANT_STAGE")
		ELIF thisTaskState = GROUP_TASK_STAGE
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - GROUP_TASK_STAGE")
		ELIF thisTaskState = ATTRACTOR_SCRIPT_TASK_STAGE
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - ATTRACTOR_SCRIPT_TASK_STAGE")
		ELIF thisTaskState = SECONDARY_TASK_STAGE
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - SECONDARY_TASK_STAGE")
		ELIF thisTaskState = FINISHED_TASK
			DEBUG_MESSAGE("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ - FINISHED_TASK")
		ENDIF
	ENDPROC
#ENDIF

PROC PS_PARACHUTE_UPDATE_TARGET(VECTOR targetPosition)
	DRAW_MARKER(MARKER_RING, targetPosition, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(targetPosition, 100), FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, targetPosition, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(targetPosition, 100), FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, targetPosition, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(targetPosition, 100), FALSE, FALSE)		
ENDPROC

PROC PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(VECTOR targetPosition, structTimer& tmrAlpha)
	IF NOT IS_TIMER_STARTED(tmrAlpha)
		EXIT
	ENDIF
	
	FLOAT fTime = GET_TIMER_IN_SECONDS(tmrAlpha)
	
	IF fTime > 1.4
		CANCEL_TIMER(tmrAlpha)
		EXIT
	ENDIF
	
	INT iAlpha = PS_PARA_TARG_COL_A - CEIL(PS_PARA_TARG_COL_A * fTime / 1.4)
	
	DRAW_MARKER(MARKER_RING, targetPosition, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, iAlpha, FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, targetPosition, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, iAlpha, FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, targetPosition, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, iAlpha, FALSE, FALSE)		
ENDPROC

PROC PS_PARACHUTE_START_TARGET_FLARE(VECTOR flarePosition)
	PTFX_SkydivingTargetFlare = START_PARTICLE_FX_LOOPED_AT_COORD("scr_mgpilot_target_flare", flarePosition, <<0,0,0>>)
	SET_PARTICLE_FX_LOOPED_COLOUR(PTFX_SkydivingTargetFlare, 1.0, 1.0, 0)
	SET_PARTICLE_FX_LOOPED_ALPHA(PTFX_SkydivingTargetFlare, 0.75)
ENDPROC

PROC PS_PARACHUTE_KILL_TARGET_FLARE()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFX_SkydivingTargetFlare)
		REMOVE_PARTICLE_FX(PTFX_SkydivingTargetFlare)
	ENDIF
ENDPROC

PROC PS_PARACHUTE_FLY_CARGOPLANE_ON_PATH(VEHICLE_INDEX planeIndex, VECTOR &vPlaneFlightPath[], INT &iPlaneFlightCount)
	CONST_FLOAT fPLANE_CHECKPOINT_RADIUS	30.0
	IF NOT IS_ENTITY_AT_COORD(planeIndex, vPlaneFlightPath[iPlaneFlightCount], <<fPLANE_CHECKPOINT_RADIUS, fPLANE_CHECKPOINT_RADIUS, fPLANE_CHECKPOINT_RADIUS>>)
		IF IS_VEHICLE_DRIVEABLE(planeIndex)
			PED_INDEX planePilot_ped = GET_PED_IN_VEHICLE_SEAT(planeIndex, VS_DRIVER)
			IF NOT IS_ENTITY_DEAD(planePilot_ped)
				IF GET_SCRIPT_TASK_STATUS(planePilot_ped, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//					PRINTSTRING("PS_PARACHUTE_FLY_CARGOPLANE_ON_PATH - TASK_VEHICLE_MISSION_COORS_TARGET planePilot_ped to ")PRINTINT(iPlaneFlightCount)PRINTNL()
					CLEAR_PED_TASKS(planePilot_ped)
					TASK_VEHICLE_MISSION_COORS_TARGET(planePilot_ped, planeIndex, vPlaneFlightPath[iPlaneFlightCount], MISSION_GOTO, 40.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0*fPLANE_CHECKPOINT_RADIUS, 0)
				ELSE
//					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(planePilot_ped), vPlaneFlightPath[iPlaneFlightCount], 255, 255, 0, 255)
					IS_ENTITY_AT_COORD(planePilot_ped, vPlaneFlightPath[iPlaneFlightCount], <<fPLANE_CHECKPOINT_RADIUS, fPLANE_CHECKPOINT_RADIUS, fPLANE_CHECKPOINT_RADIUS>>)
//					#IF IS_DEBUG_BUILD
//						DEBUG_MESSAGE("Task status of plane is ", GET_STRING_FROM_INT(ENUM_TO_INT( PERFORMING_TASK)))
//					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		iPlaneFlightCount++
		IF iPlaneFlightCount >= COUNT_OF(vPlaneFlightPath)
			iPlaneFlightCount = 0
		ENDIF
	ENDIF
ENDPROC

PROC PS_PARACHUTE_FLY_CARGOPLANE_AWAY_FROM_HERE(VEHICLE_INDEX planeIndex, VECTOR vPlaneFlightDestination)
	CONST_FLOAT fPLANE_CHECKPOINT_RADIUS	30.0
	PRINTLN("Flying plane away from the airport")
	FREEZE_ENTITY_POSITION(planeIndex, FALSE)
	IF NOT IS_ENTITY_AT_COORD(planeIndex, vPlaneFlightDestination, <<fPLANE_CHECKPOINT_RADIUS, fPLANE_CHECKPOINT_RADIUS, fPLANE_CHECKPOINT_RADIUS>>)
		IF IS_VEHICLE_DRIVEABLE(planeIndex)
			PED_INDEX planePilot_ped = GET_PED_IN_VEHICLE_SEAT(planeIndex, VS_DRIVER)
			IF NOT IS_ENTITY_DEAD(planePilot_ped)
				IF GET_SCRIPT_TASK_STATUS(planePilot_ped, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
					CLEAR_PED_TASKS(planePilot_ped)
					PRINTLN("Tasking the plane to fly away")
					TASK_VEHICLE_MISSION_COORS_TARGET(planePilot_ped, planeIndex, vPlaneFlightDestination, MISSION_GOTO, 40.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0*fPLANE_CHECKPOINT_RADIUS, 0)
				ELSE
					PRINTLN("Checking to see if we can delete the plane yet")
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), planeIndex) > 500.0
						IF NOT IS_ENTITY_ON_SCREEN(planeIndex)
							DELETE_VEHICLE(planeIndex)
						ENDIF	
					ENDIF
				ENDIF 
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PS_IS_PLAYER_ALIVE()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLAYER_IN_VEHICLE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLAYER_VEHICLE_ON_RUNWAY()
	//Used widget in z_volumes.sch to get these vector coords
	VECTOR vMidPoint1 = <<-1367.200, -3107.144, 0>>
	VECTOR vMidPoint2 = <<-1383.200, -3134.856, 15>>
	VECTOR vMidPoint3 = <<-1356.894, -2245.594, 0>>
	VECTOR vMidPoint4 = <<-1648.882, -2753.091, 15>>
	FLOAT fArea1 = 600
	FLOAT fArea2 = 35
	VECTOR vTempPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	vMidPoint1.z = vTempPlayerCoords.z
	vMidPoint2.z = vTempPlayerCoords.z
	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vMidPoint1, vMidPoint2, fArea1) OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), vMidPoint3, vMidPoint4, fArea2)
ENDFUNC

FUNC BOOL PS_IS_RUNWAY_FAIL_TIMER_UP()
	IF IS_TIMER_STARTED(Fail_Timer_Runway)
		IF TIMER_DO_ONCE_WHEN_READY(Fail_Timer_Runway, PS_FAIL_TIME_RUNWAY)
			RETURN TRUE			
		ENDIF
	ELSE
		SCRIPT_ASSERT("Runway timer is not running when it should be!")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLANE_LEAVING_RUNWAY_DURING_TAKEOFF()
	//are we on the ground?
	IF GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) - GroundOffset > 0
		//if we lifted off, cancel the timer
		IF IS_TIMER_STARTED(Fail_Timer_Runway)
			CANCEL_TIMER(Fail_Timer_Runway)
			RETURN FALSE	
		ENDIF
		RETURN FALSE
		
	//So we're not in the air, but are we on the runway?
	ELIF NOT PS_IS_PLAYER_VEHICLE_ON_RUNWAY()
		//is the runway fail timer going?
		IF NOT IS_TIMER_STARTED(Fail_Timer_Runway)
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_1", TRUE) //"You need stay on the runway during takeoff"
			START_TIMER_NOW_SAFE(Fail_Timer_Runway)
		ENDIF
		RETURN TRUE
	ELSE
		//make sure to cancel the timer if we started it.
		IF IS_TIMER_STARTED(Fail_Timer_Runway)
			CANCEL_TIMER(Fail_Timer_Runway)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_LANDED_FAIL_TIMER_UP()
	IF IS_TIMER_STARTED(Fail_Timer_Landed)
		IF TIMER_DO_ONCE_WHEN_READY(Fail_Timer_Landed, PS_FAIL_TIME_LANDED)
			RETURN TRUE			
		ENDIF
	ELSE
		SCRIPT_ASSERT("Landed fail timer is not running when it should be!")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLAYER_LANDED()
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		IF (NOT (GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) - GroundOffset > 0))
			IF NOT IS_TIMER_STARTED(Fail_Timer_Landed)
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_2", TRUE) //"You need to keep the plane up off the ground"
				START_TIMER_NOW(Fail_Timer_Landed)
				RETURN TRUE
			ENDIF
			RETURN TRUE			
		ENDIF
	ENDIF
	IF IS_TIMER_STARTED(Fail_Timer_Landed)
		CANCEL_TIMER(Fail_Timer_Landed)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_RANGE_TIMER_UP()
	IF IS_TIMER_STARTED(Fail_Timer_Range)
		IF TIMER_DO_ONCE_WHEN_READY(Fail_Timer_Range, PS_FAIL_TIME_OUT_OF_RANGE)
			RETURN TRUE			
		ENDIF
		IF iRangeTimerWarning = 1 AND TIMER_DO_WHEN_READY(Fail_Timer_Range, PS_FAIL_TIME_OUT_OF_RANGE*0.5)		
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_6")
			iRangeTimerWarning+=1
		ENDIF
	ELSE
		SCRIPT_ASSERT("Range fail timer is not running when it should be!")
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_CALCULATE_OBJECTIVE_DISTANCE()
	vInRangeCoord = PS_GET_CURRENT_CHECKPOINT()//inrange coord might not always be current checkpoint
	VECTOR v1, v2 //flattened vectors
	v1 = PS_GET_CURRENT_CHECKPOINT()
	IF PS_GET_CHECKPOINT_PROGRESS() > 1
		v2 = PS_GET_PREV_CHECKPOINT()
	ELSE
		v2 = vStartPosition
	ENDIF
	v1.z = 0
	v2.z = 0
	fCheckpointDistAway = GET_DISTANCE_BETWEEN_COORDS(v1, v2)
ENDPROC

FUNC BOOL PS_IS_COORD_IN_FRONT_OF_PLAYER(VECTOR vCoord)
	IF PS_IS_PLANE_CURRENTLY_LEVEL()
		VECTOR posA		// thisVehicle position
		VECTOR TrashVector
		VECTOR vecAB 	// vector from thisVehicle to thisVector
		VECTOR vec2	// unit vector pointing in the direction thisVector is pointing
		FLOAT fTemp

		// get positions
		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			GET_ENTITY_MATRIX(PS_Main.myVehicle, vec2, TrashVector, TrashVector, posA)
			//put vehicle coord in posA and put the unit vector of its direction into vec 2
		ENDIF

		// get vec from thisVehicle to thisCoord
		vecAB = vCoord - posA

		// take the z out
		vecAB.z = 0.0
		vec2.z = 0.0

		// calculate dot product of vecAB and vec2
		fTemp = DOT_PRODUCT(vecAB, vec2)
		IF (fTemp <= 0.0) 
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL PS_IS_PLAYER_OUT_OF_RANGE()
//TODO: distance calculation every frame? NO THANK YOU. Please get this out of here and calc distance as needed!!
	PS_CALCULATE_OBJECTIVE_DISTANCE()
	IF (NOT IS_VECTOR_ZERO(vInRangeCoord)) AND fCheckpointDistAway > 0
//		PRINTLN("GET_PLAYER_DISTANCE_FROM_LOCATION(vInRangeCoord, FALSE) is... ", GET_PLAYER_DISTANCE_FROM_LOCATION(vInRangeCoord, FALSE))
//		PRINTLN("vInRangeCoord is... ", vInRangeCoord)
//		PRINTLN("fCheckpointDistAway is ... ", fCheckpointDistAway)
//		PRINTLN("fCheckpointDistAway + PS_FAIL_DIST_OUT_OF_RANGE is ... ", fCheckpointDistAway + PS_FAIL_DIST_OUT_OF_RANGE)
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vInRangeCoord, FALSE) > (fCheckpointDistAway + PS_FAIL_DIST_OUT_OF_RANGE)
			IF NOT IS_TIMER_STARTED(Fail_Timer_Range)
				//if we don't already have a checkpoint blip, then blip the coord we're too far form
				IF NOT PS_HAS_SPECIAL_CHECKPOINT()
					PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_SPECIAL, vInRangeCoord)
				ENDIF
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_4", TRUE) //"you're going out of bounds!"
				START_TIMER_NOW(Fail_Timer_Range)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_TIMER_STARTED(Fail_Timer_Range)
		//make sure we remove the out of range blip once we get back in range
		PS_RESET_SPECIAL_CHECKPOINT()
		CANCEL_TIMER(Fail_Timer_Range)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLAYER_OUT_OF_RANGE_SPECIAL()
	vInRangeCoord = PS_GET_LAST_CHECKPOINT() //special coord in the case we aren't using checkpoints
	fCheckpointDistAway = PS_FAIL_DIST_OUT_OF_RANGE_SPECIAL	//static distance
	IF (NOT IS_VECTOR_ZERO(vInRangeCoord)) AND fCheckpointDistAway > 0
		IF GET_PLAYER_DISTANCE_FROM_LOCATION(vInRangeCoord) > (fCheckpointDistAway + PS_FAIL_DIST_OUT_OF_RANGE)
			IF NOT IS_TIMER_STARTED(Fail_Timer_Range)
				//if we don't already have a checkpoint blip, then blip the coord we're too far form
				IF NOT PS_HAS_SPECIAL_CHECKPOINT()
					PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_SPECIAL, vInRangeCoord)
				ENDIF
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_4", TRUE) //"you're going out of bounds!"
				START_TIMER_NOW(Fail_Timer_Range)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_TIMER_STARTED(Fail_Timer_Range)
		//make sure we remove the out of range blip once we get back in range
		PS_RESET_SPECIAL_CHECKPOINT()
		CANCEL_TIMER(Fail_Timer_Range)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_WATER_TIMER_UP()
	IF IS_TIMER_STARTED(Fail_Timer_Water)
		IF TIMER_DO_ONCE_WHEN_READY(Fail_Timer_Water, PS_FAIL_TIME_WATER)
			RETURN TRUE			
		ENDIF
	ELSE
		SCRIPT_ASSERT("Water fail timer is not running when it should be!")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLAYER_IN_WATER()
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
	OR (DOES_ENTITY_EXIST(PS_Main.myVehicle) AND (NOT IS_ENTITY_DEAD(PS_Main.myVehicle))
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle) AND IS_ENTITY_IN_WATER(PS_Main.myVehicle))
		IF NOT IS_TIMER_STARTED(Fail_Timer_Water)
			START_TIMER_NOW(Fail_Timer_Water)
			RETURN TRUE
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_TIMER_STARTED(Fail_Timer_Water)
		CANCEL_TIMER(Fail_Timer_Water)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_HAS_PLAYER_BEEN_IDLING_TOO_LONG()
	IF NOT IS_TIMER_STARTED(Fail_Timer_Idling)
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) = 0.0
			START_TIMER_AT(Fail_Timer_Idling, 0.0)
		ENDIF
	ELIF IS_TIMER_STARTED(Fail_Timer_Idling)
		IF TIMER_DO_ONCE_WHEN_READY(Fail_Timer_Idling, PS_FAIL_TIME_IDLING)
			IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) = 0.0
			//double check before we fail
				RETURN TRUE
			ENDIF
		ELIF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) > 0.0
			CANCEL_TIMER(Fail_Timer_Idling)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Increments the enter/update/exit substate
PROC PS_INCREMENT_SUBSTATE()
	SWITCH eSubState
		CASE PS_SUBSTATE_ENTER
			eSubState = PS_SUBSTATE_UPDATE
			BREAK
		CASE PS_SUBSTATE_UPDATE
			eSubState = PS_SUBSTATE_EXIT
			BREAK
		CASE PS_SUBSTATE_EXIT
			eSubState = PS_SUBSTATE_ENTER
			BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Manually sets the enter/update/exit substate for special cases
/// PARAMS:
///    tempstate - 
PROC PS_SET_SUBSTATE(PS_CHALLENGE_SUBSTATE tempstate)
	eSubState = tempstate
ENDPROC

/// PURPOSE:
///    Gets current enter/update/exit substate
/// RETURNS:
///    
FUNC PS_CHALLENGE_SUBSTATE PS_GET_SUBSTATE()
	RETURN eSubState
ENDFUNC

/// PURPOSE:
///    Checks if the the next checkpoint is off the screen and calculates the position for the HUD gutter icon.
///    The coords are passed in a method so the HUD update can take care of it independently
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_CALCULATE_GUTTER_ICON_POSITION()
	FLOAT XScreenPos, YScreenPos
	VECTOR tempVec, chkptVec
	
//	IF iNextPath >= 0 AND iNextPath < PS_MAX_CHECKPOINTS AND NOT ARE_VECTORS_EQUAL(PS_Main.myCheckpoints[iNextPath].position, VECTOR_ZERO)
	IF PS_IS_CHECKPOINT_MGR_ACTIVE()
		chkptVec = PS_GET_CURRENT_CHECKPOINT()
		IF NOT PS_IS_LAST_CHECKPOINT_CLEARED() AND NOT ARE_VECTORS_EQUAL(chkptVec, VECTOR_ZERO)
			GET_HUD_SCREEN_POSITION_FROM_WORLD_POSITION(chkptVec, XScreenPos, YScreenPos)
		ENDIF
	ELSE
		GET_HUD_SCREEN_POSITION_FROM_WORLD_POSITION(vPS_MovingTarget, XScreenPos, YScreenPos)
	ENDIF
	
	//Keep the icon within bounds specified by a radius
	//first, convert from screen coords to a vector (origin is at the center of the screen (SCREEN_CENTER_X, SCREEN_CENTER_Y)
	
	//if on the right half of the screen
	IF XScreenPos >= SCREEN_CENTER_X
		tempVec.x = XScreenPos - SCREEN_CENTER_X
	//else on the left half of the screen
	ELSE
		tempVec.x = SCREEN_CENTER_X - XScreenPos
	ENDIF
	
	//If on the bottom half of the screen
	IF YScreenPos >= SCREEN_CENTER_Y
		tempVec.y = YScreenPos - SCREEN_CENTER_Y
	//else on the top half of the screen
	ELSE
		tempVec.y = SCREEN_CENTER_Y - YScreenPos
	ENDIF
	
	tempVec.z = 0.0
	
	//normalize vector so we just have the direction
	NORMALISE_VECTOR(tempVec)
	
	//mulitply the magnitude of our direction by our set radius
	tempVec.x *= SCREEN_RADIUS_FOR_GUTTER
	tempVec.y *= SCREEN_RADIUS_FOR_GUTTER

	//Convert back to screen coords
	
	//if on the right half of the screen
	IF XScreenPos >= SCREEN_CENTER_X
		XScreenPos = SCREEN_CENTER_X + tempVec.x
	//else on the left half of the screen
	ELSE
		XScreenPos = SCREEN_CENTER_X - tempVec.x
	ENDIF
	
	//If on the bottom half of the screen
	IF YScreenPos >= SCREEN_CENTER_Y
		 YScreenPos = SCREEN_CENTER_Y + tempVec.y
	//else on the top half of the screen
	ELSE
		YScreenPos = SCREEN_CENTER_Y - tempVec.y
	ENDIF
	PS_HUD_SET_GUTTER_ICON_POSITION(XScreenPos, YScreenPos)
ENDPROC

PROC PS_SET_TIMER_ACTIVE(BOOL bActive)
	PS_UI_RaceHud.bIsTimerActive = bActive
	PS_HUD_SET_TIMER_VISIBLE(bActive)
ENDPROC

PROC PS_SET_DIST_HUD_ACTIVE(BOOL bActive)
	PS_UI_RaceHud.bIsDistActive = bActive
	PS_HUD_SET_DIST_HUD_VISIBLE(bActive)
ENDPROC

PROC PS_START_HOURGLASS_TIMER(INT TimeToRun)
	//PS_HUD_SET_HOURGLASS_TIMER_PADDING(ROUND(TimeToRun*0.05)) //padding should be about ~5%
	PS_UI_RaceHud.bIsHourGlassActive = TRUE
	PS_UI_RaceHud.iHourGlassStartTime = CEIL(GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer) * 1000)
	iHourGlassTimeToRun = TimeToRun
ENDPROC

PROC PS_STOP_HOURGLASS_TIMER()
	PS_UI_RaceHud.bIsHourGlassActive = FALSE
ENDPROC

/// PURPOSE:
///    Turns gutter on. The sprite is only visible, however, when there is a checkpoint off the screen. Make sure this is not turned off if there
///    are no race checkpoints in the main struct of a challenge (PS_Struct_Data)
/// PARAMS:
///    bActive - 
PROC PS_HUD_SET_GUTTER_ICON_ACTIVE(BOOL bActive)
	PS_UI_GutterIcon.bIsGutterActive = bActive
	//if deactivating gutter, make sure its not visible.
	IF NOT PS_UI_GutterIcon.bIsGutterActive
		PS_HUD_SET_GUTTER_ICON_VISIBLE(bActive)
	ENDIF
ENDPROC

PROC PS_SET_ALTIMETER_ACTIVE(BOOL bActive)
	bIsAltimeterActive = bActive
	PS_HUD_SET_ALTIMETER_VISIBLE(bActive)
ENDPROC

PROC PS_SET_CHECKPOINT_COUNTER_ACTIVE(BOOL bActive)
	PS_HUD_SET_CHECKPOINT_COUNTER_VISIBLE(bActive)
ENDPROC

PROC PS_SET_ALTITUDE_INDICATOR_ACTIVE(BOOL bActive)
	bIsAltitudeIndicatorActive = bActive
	PS_HUD_SET_ALTITUDE_INDICTATOR_VISIBLE(bActive)
ENDPROC

PROC DEACTIVATE_ALL_OBJECTIVE_METERS()
	PS_UI_ObjectiveMeter.bIsLoopMeterActive = FALSE
	PS_UI_ObjectiveMeter.bIsRollMeterActive = FALSE
	PS_UI_ObjectiveMeter.bIsInvertedMeterActive = FALSE
	PS_UI_ObjectiveMeter.bIsLoopMeterActive = FALSE
	PS_UI_ObjectiveMeter.bIsImmelMeterActive = FALSE
ENDPROC

PROC PS_SET_LOOP_METER_ACTIVE(BOOL bActive)
	PS_RESET_OBJECTIVE_DATA()
	DEACTIVATE_ALL_OBJECTIVE_METERS()
	PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_INSIDE_LOOP)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(bActive)
	PS_UI_ObjectiveMeter.bIsLoopMeterActive = bActive
ENDPROC

PROC PS_SET_ROLL_METER_ACTIVE(BOOL bActive)
	PS_RESET_OBJECTIVE_DATA()
	DEACTIVATE_ALL_OBJECTIVE_METERS()
	PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_BARREL_ROLL)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(bActive)
	PS_UI_ObjectiveMeter.bIsRollMeterActive = bActive
ENDPROC

PROC PS_SET_INVERTED_METER_ACTIVE(BOOL bActive)
	PS_RESET_OBJECTIVE_DATA()
	DEACTIVATE_ALL_OBJECTIVE_METERS()
	PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_INVERTED)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(bActive)
	PS_UI_ObjectiveMeter.bIsInvertedMeterActive = bActive
ENDPROC

PROC PS_SET_KNIFE_METER_ACTIVE(BOOL bActive)
	PS_RESET_OBJECTIVE_DATA()
	DEACTIVATE_ALL_OBJECTIVE_METERS()
	PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_KNIFE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(bActive)
	PS_UI_ObjectiveMeter.bIsKnifeMeterActive = bActive
ENDPROC

PROC PS_SET_IMMELMAN_METER_ACTIVE(BOOL bActive, PS_OBJECTIVE_TYPE_ENUM thisObjBarType = PS_OBJECTIVE_TYPE_IMMELMAN_TURN)
	PS_RESET_OBJECTIVE_DATA()
	DEACTIVATE_ALL_OBJECTIVE_METERS()
	PS_HUD_SET_OBJECTIVE_TYPE(thisObjBarType)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(bActive)
	PS_UI_ObjectiveMeter.bIsImmelMeterActive = bActive
ENDPROC

//***************************************************************************************************
//Vehicle/ped stuff
//***************************************************************************************************


/// PURPOSE:
///    Keeps track of the plane rolling and how many times it has rolled.
/// PARAMS:
///    PS_Struct_Data - 
PROC Pilot_School_Do_Roll_Tracking()
	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		CONST_FLOAT		fVehicle_ROLL_ADDITION	180.0
		VEHICLE_INDEX vehicleIndex = PS_Main.myVehicle
		VECTOR fPlayerRot = GET_ENTITY_ROTATION(vehicleIndex) 
		FLOAT fPlayerCurrentRoll = fPlayerRot.y// GET_ENTITY_ROLL(vehicleIndex) //+fVehicle_ROLL_ADDITION

		FLOAT fPlayerCurrentPitch = GET_ENTITY_PITCH(vehicleIndex)
		IF ABSF(fPlayerCurrentPitch) > 45.0 AND ABSF(fPlayerCurrentPitch) < 135.0 
			PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
		ENDIF		
		
//		#IF IS_DEBUG_BUILD
//			DrawLiteralStringFloat("roll: ", fPlayerCurrentRoll, 0)
//			DrawLiteralStringFloat("roty: ", fPlayerRot.y, 1)
//		#ENDIF
		
		IF PS_Main.myObjectiveData.iTotalOrientCount < 0
			PS_Main.myObjectiveData.iTotalOrientCount		= 0
			PS_Main.myObjectiveData.iCurrentOrientCount	= 0
			fPlayerCurrentRoll = CLAMP(fPlayerCurrentRoll, -10.0, 10.0)
			PS_Main.myObjectiveData.fPlanePrevOrient		= fPlayerCurrentRoll
		ELSE
////			#IF IS_DEBUG_BUILD
////				DrawLiteralStringFloat("prev roll: ", PS_Main.myObjectiveData.fPlanePrevOrient, 1)
////			#ENDIF
			
			FLOAT fPlayerRollDiff = fPlayerCurrentRoll-PS_Main.myObjectiveData.fPlanePrevOrient
					
			//because of the jump from roll HUGE to TINY
			IF fPlayerRollDiff > 270.0
				fPlayerRollDiff -= 360.0
			ELIF fPlayerRollDiff < -270.0
				fPlayerRollDiff += 360.0
			ENDIF
		
			IF fPlayerRollDiff > 0
////				#IF IS_DEBUG_BUILD
////					DrawLiteralStringFloat("diff roll: ", fPlayerRollDiff, 2, HUD_COLOUR_REDLIGHT)
////					DrawLiteralStringFloat("ACW roll: ", ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient), 3, HUD_COLOUR_RED)
////				#ENDIF
				
				IF PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
//					IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > 60.0
//						PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
//					ENDIF
					PS_Main.myObjectiveData.fPlaneTotalOrient = fPlayerCurrentRoll//0.0
					PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
					PS_Main.myObjectiveData.iCurrentOrientCount = 0
				ENDIF
				
				PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = FALSE
			ELSE
////				#IF IS_DEBUG_BUILD
////					DrawLiteralStringFloat("diff roll: ", fPlayerRollDiff, 2, HUD_COLOUR_BLUELIGHT)
////					DrawLiteralStringFloat("CW roll: ", ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient), 3, HUD_COLOUR_BLUE)
////				#ENDIF
				
				IF NOT PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
//					IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > 60.0
//						PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
//					ENDIF
					PS_Main.myObjectiveData.fPlaneTotalOrient = fPlayerCurrentRoll//0.0
					PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
					PS_Main.myObjectiveData.iCurrentOrientCount = 0
				ENDIF
				
				PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = TRUE
			ENDIF			
////			#IF IS_DEBUG_BUILD
////				DrawLiteralStringInt("current roll: ", PS_Main.myObjectiveData.iCurrentOrientCount, 4, HUD_COLOUR_GREENLIGHT)
////				DrawLiteralStringInt("total roll: ", PS_Main.myObjectiveData.iCurrentOrientCount+PS_Main.myObjectiveData.iTotalOrientCount, 5, HUD_COLOUR_GREEN)
////			#ENDIF
			PS_Main.myObjectiveData.iCurrentOrientCount	= FLOOR(ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_ROLL_GOAL_ANGLE) ///PS_UI_ObjectiveMeter.fObjBarDemoninator)	
			PS_Main.myObjectiveData.fPlanePrevOrient		= fPlayerCurrentRoll
			PS_Main.myObjectiveData.fPlaneTotalOrient		+= fPlayerRollDiff
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   Basically the same as the loop meter, but we only want to do a half loop in an immelman
/// PARAMS:
///    PS_Struct_Data - 
///    iTotalOrientCount - How many times a loop has been performed. (checks against 345 degrees instead of 360)
///    iCurrentOrientCount - Should always be 0, unless the player has JUST completed a loop. Once that happens, it gets added to iTotalOrientCount and then reset to 0 again
///    fPlanePrevOrient - Keeps track of the last pitch we were at, so we can calculate the difference
///    fPlaneTotalOrient - Keeps adding the different between current pitch and last pitch. IOW, its the tracks the completion of a single loop and the HUD bar/meter updates directly from this var
PROC Pilot_School_Do_Immelman_Tracking()
	VEHICLE_INDEX vehicleIndex = PS_Main.myVehicle
	FLOAT fPlayerCurrentPitch = GET_ENTITY_PITCH(vehicleIndex)
	VECTOR vForward, vSide, vUp, vPos
	GET_ENTITY_MATRIX(vehicleIndex, vForward, vSide, vUp, vPos)
	
////	#IF IS_DEBUG_BUILD
////		DrawLiteralStringFloat("Loop: ", fPlayerCurrentPitch, 0)
////		DrawLiteralStringFloat("Roll: ", fPlayerCurrentRoll, 1, HUD_COLOUR_BLUELIGHT)
////		DrawLiteralStringFloat("Pitch: ", fPlayerCurrentPitch, 2, HUD_COLOUR_REDLIGHT)
////	#ENDIF
	
	IF PS_Main.myObjectiveData.iTotalOrientCount < 0
		//first time running through, so reset everything
		PS_Main.myObjectiveData.iTotalOrientCount		= 0
		PS_Main.myObjectiveData.iCurrentOrientCount	= 0
		PS_Main.myObjectiveData.fPlanePrevOrient		= fPlayerCurrentPitch
	ELSE
	
		IF ABSF(vSide.z) > 0.5 // sin(30)
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0
			PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			EXIT
		ENDIF
	
//		//check we're not rolling the plane
//		IF fPlayerCurrentRoll > 25 AND fPlayerCurrentRoll < 160 //somwhere between 25 and 160
//			PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = !PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
//		ELIF fPlayerCurrentRoll < -25 AND fPlayerCurrentRoll > -160 //somewhere between -25 and -160
//			PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = !PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
//		ENDIF
////		#IF IS_DEBUG_BUILD
////			//DrawLiteralStringFloat("prev Loop: ", PS_Main.myObjectiveData.fPlanePrevOrient, 1)
////		#ENDIF
		
		FLOAT fPlayerLoopDiff = fPlayerCurrentPitch-PS_Main.myObjectiveData.fPlanePrevOrient
		
		IF fPlayerCurrentPitch < 0
			fPlayerLoopDiff = 0
		ENDIF
////		#IF IS_DEBUG_BUILD
////			DrawLiteralStringFloat("prev Loop: ", fPlayerLoopDiff, 6)
////		#ENDIF
		//because of the jump from Loop HUGE to TINY
		IF fPlayerLoopDiff > 270.0
			fPlayerLoopDiff -= 360.0
		ELIF fPlayerLoopDiff < -270.0
			fPlayerLoopDiff += 360.0
		ENDIF
		
		//If the pitch dif is pos, we're looping with our nose going up, otherwise we're looping with our nose going down
		IF fPlayerLoopDiff > PS_LOOPING_MIN_PITCH_DIFFERENCE
////			#IF IS_DEBUG_BUILD
////				//DrawLiteralStringFloat("diff Loop: ", fPlayerLoopDiff, 2, HUD_COLOUR_REDLIGHT)
////				DrawLiteralStringFloat("ACW Loop: ", ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient), 3, HUD_COLOUR_RED)
////			#ENDIF
			
			IF PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
				IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
					PS_Main.myObjectiveData.fPlaneTotalOrient = 0
					PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
				ENDIF
				PS_Main.myObjectiveData.fPlaneTotalOrient = CLAMP(fPlayerCurrentPitch, 0, 30)//0.0
				PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
				PS_Main.myObjectiveData.iCurrentOrientCount = 0
			ENDIF
			
			PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = FALSE
		ELIF fPlayerLoopDiff < -PS_LOOPING_MIN_PITCH_DIFFERENCE
////			#IF IS_DEBUG_BUILD
////				//DrawLiteralStringFloat("diff Loop: ", fPlayerLoopDiff, 2, HUD_COLOUR_BLUELIGHT)
////				DrawLiteralStringFloat("CW Loop: ", ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient), 3, HUD_COLOUR_BLUE)
////			#ENDIF
			
			IF NOT PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
				IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
					PS_Main.myObjectiveData.fPlaneTotalOrient = 0
					PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
				ENDIF
				PS_Main.myObjectiveData.fPlaneTotalOrient = CLAMP(fPlayerCurrentPitch, 0, 30)//0.0
				PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
				PS_Main.myObjectiveData.iCurrentOrientCount = 0
			ENDIF
			
			PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = TRUE
		ELSE
			//check the roll of the plane to see if we're turning out of the loop
			//IF fPlayerCurrentRoll
		ENDIF
		
////		#IF IS_DEBUG_BUILD
////			DrawLiteralStringInt("current Loop: ", PS_Main.myObjectiveData.iCurrentOrientCount, 4, HUD_COLOUR_GREENLIGHT)
////			DrawLiteralStringInt("total Loop: ", PS_Main.myObjectiveData.iCurrentOrientCount+PS_Main.myObjectiveData.iTotalOrientCount, 5, HUD_COLOUR_GREEN)
////		#ENDIF
		
		PS_Main.myObjectiveData.iCurrentOrientCount	= FLOOR(CLAMP(ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_LOOP_GOAL_ANGLE, ABSF(fPlayerCurrentPitch), PS_LOOPING_LOOP_GOAL_ANGLE))
		fPlayerLoopDiff = CLAMP(fPlayerLoopDiff, -2.0, 2.0)
		PS_Main.myObjectiveData.fPlanePrevOrient		= fPlayerCurrentPitch
		FLOAT tempTotalOrient = PS_Main.myObjectiveData.fPlaneTotalOrient	+ fPlayerLoopDiff	
		IF tempTotalOrient > 0
			PS_Main.myObjectiveData.fPlaneTotalOrient	= tempTotalOrient
		ELSE
			PS_Main.myObjectiveData.fPlaneTotalOrient	= 0 
		ENDIF
	ENDIF
ENDPROC
//
//PROC PS_PARACHUTE_CREATE_TARGET(VECTOR vThisTarget)
//
//	PS_Main.dest_checkpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, vThisTarget, VECTOR_ZERO, CHECKPOINT_SCALE_MULTIPLIER, 255, 255, 0)
//	PS_Main.dest_blip = CREATE_BLIP_FOR_COORD(vThisTarget)
//
//ENDPROC
//
//PROC Pilot_School_Print_Fail_Reason()
//	SWITCH(ePSFailReason)	
//		CASE PS_FAIL_DOUBLE_REASON
//			#IF IS_DEBUG_BUILD
//				PRINT_NOW("PS_FAIL_0", 5000, 1)//"~r~Error! Multiple fail conditions triggered.~s~"
//			#ENDIF
//			BREAK	
//		CASE PS_FAIL_NO_REASON
//			#IF IS_DEBUG_BUILD
//				PRINT_NOW("PS_FAIL_1", 5000, 1)//"~r~Failed for no reason.~s~"
//			#ENDIF
//			BREAK
//		CASE PS_FAIL_TIME_UP
//			PRINT_NOW("PS_FAIL_2", 5000, 1)//"~r~Missed goal time.~s~"
//			BREAK
//		CASE PS_FAIL_TOO_FAR
//			PRINT_NOW("PS_FAIL_3", 5000, 1)//"~r~You landed too far away.~s~"
//			BREAK
//		CASE PS_FAIL_PLAYER_LEFT
//			PRINT_NOW("PS_FAIL_4", 5000, 1)//"~r~Player left vehicle.~s~
//			BREAK
//		CASE PS_FAIL_OUT_OF_RANGE
//			PRINT_NOW("PS_FAIL_5", 5000, 1)//"~r~Went out of range.~s~"
//			BREAK
//		CASE PS_FAIL_BEHIND_RACE
//			PRINT_NOW("PS_FAIL_6", 5000, 1)//"~r~Too far behind.~s~"
//			BREAK
//		CASE PS_FAIL_LANDED_VEHICLE
//			PRINT_NOW("PS_FAIL_7", 5000, 1)//"~r~Player landed vehicle.~s~"
//			BREAK
//		CASE PS_FAIL_DAMAGED_VEHICLE
//			PRINT_NOW("PS_FAIL_8", 5000, 1)//"~r~Vehicle was wrecked.~s~"
//			BREAK
//		CASE PS_FAIL_MISSED_GATES
//			PRINT_NOW("PS_FAIL_9", 5000, 1)//"~r~Player missed too many gates.~s~"
//			BREAK
//		CASE PS_FAIL_MISSED_FIRST_GATE
//			PRINT_NOW("PS_FAIL_9A", 5000, 1)//"~r~Player the first gate.~s~"
//			BREAK
//		CASE PS_FAIL_LEFT_RUNWAY
//			PRINT_NOW("PS_FAIL_10", 5000, 1)//"~r~Player left runway.~s~"
//			BREAK
//		CASE PS_FAIL_SUBMERGED
//			PRINT_NOW("PS_FAIL_11", 5000, 1)//"~r~Vehicle submerged in water.~s~"
//			BREAK
//		CASE PS_FAIL_STUCK_VEHICLE
//			PRINT_NOW("PS_FAIL_12", 5000, 1)//"~r~Vehicle got stuck.~s~"
//			BREAK
//		CASE PS_FAIL_TOO_HIGH
//			PRINT_NOW("PS_FAIL_13", 5000, 1)//"~r~Player was too high.~s~"
//			BREAK
//		CASE PS_FAIL_LANDED_IN_WATER
//			PRINT_NOW("PS_FAIL_14", 5000, 1)//"~r~You landed in the water.~s~"
//			BREAK
//		CASE PS_FAIL_REMOVED_PARACHUTE
//			PRINT_NOW("PS_FAIL_15", 5000, 1)//"~r~You removed the parachute.~s~"
//			BREAK
//		CASE PS_FAIL_IDLING
//			PRINT_NOW("PS_FAIL_16", 5000, 1)//"~r~You were idling for too long.~s~"
//			BREAK	
//	ENDSWITCH
//ENDPROC

PROC PS_SEND_PLAYER_BACK_TO_MENU()
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	DISABLE_CELLPHONE(TRUE)
	SET_GAME_PAUSED(FALSE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)	
		
//	IF DOES_CAM_EXIST(camMainMenu)
//		DESTROY_CAM(camMainMenu)
//	ENDIF
ENDPROC

FUNC BOOL PS_IS_LANDING_GEAR_DOWN()

	IF GET_LANDING_GEAR_STATE(PS_Main.myVehicle) = LGS_DEPLOYING OR GET_LANDING_GEAR_STATE(PS_Main.myVehicle) = LGS_LOCKED_DOWN
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
		
ENDFUNC

PROC PS_WATCH_LANDING_GEAR_STATE()	
	testState = GET_LANDING_GEAR_STATE(PS_Main.myVehicle)
	
	SWITCH testState
		CASE LGS_LOCKED_DOWN
			PRINTLN("Landing gear state is... LGS_LOCKED_DOWN")
		BREAK
		CASE LGS_RETRACTING
			PRINTLN("Landing gear state is... LGS_RETRACTING")
		BREAK
		CASE LGS_DEPLOYING
			PRINTLN("Landing gear state is... LGS_DEPLOYING")
		BREAK
		CASE LGS_LOCKED_UP
			PRINTLN("Landing gear state is... LGS_LOCKED_UP")
		BREAK
		CASE LGS_BROKEN
			PRINTLN("Landing gear state is... LGS_BROKEN")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///   Used to check for fail conditions in lessons with a stunt plane. Doesn't work for skydiving lessons!
/// PARAMS:
///    PS_Struct_Data - Main struct
///    bRunway - Fails if the player drives off the runway before taking off (timed)
///    bHasLanded - Fails if the player lands the plane (timed)
///    bInRange - Fails if the player leaves the lesson area and doesn't return in (timed)
///    bInVehicle - Fails if vehicle is broken or player steps out
///    bVehicleStuck - Fails if the player can't move or lands on the roof (timed)
///    bIsIdling - Fails if the player is idling too long (timed)
///    bIsInWater - Fails if player is in water (doesnt work with vehicle, as it breaks the vehicle)
///    bLandedUpsideDown - Fails if the player lands the plane or helicopter on its roof. Best used with helicopter (timed)
///    
FUNC BOOL PS_UPDATE_FAIL_CHECKS(PS_MAIN_STRUCT& PS_Struct_Data, BOOL bRunway = FALSE, BOOL bHasLanded = TRUE, BOOL bInRange = TRUE, BOOL bInVehicle = TRUE, BOOL bVehicleStuck = TRUE, BOOL bIsIdling = TRUE, BOOL bIsInWater = TRUE, BOOL bLandedUpsideDown = TRUE )
	BOOL bWanted = TRUE //always check. if we need to disable this for a lesson, we'll move it into the params
	
	//Checks player and vehicle, to see if theyre still alive, and together.
	IF IS_ENTITY_DEAD(PS_Struct_Data.myVehicle)
	OR IS_PED_INJURED(PLAYER_PED_ID()) 
	OR NOT IS_VEHICLE_DRIVEABLE(PS_Struct_Data.myVehicle) 
	OR GET_VEHICLE_NUM_OF_BROKEN_OFF_PARTS(PS_Struct_Data.myVehicle) > 0
		ePSFailReason = PS_FAIL_DAMAGED_VEHICLE
		STOP_SCRIPTED_CONVERSATION(TRUE)
		RETURN TRUE
	ENDIF
		
	IF bWanted
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			DEBUG_MESSAGE("FAILED BC PLAYER GAINED WANTED LEEL")
			ePSFailReason = PS_FAIL_WANTED
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bIsInWater
		IF PS_IS_PLAYER_IN_WATER()
			IF PS_IS_WATER_TIMER_UP()
				DEBUG_MESSAGE("FAILED BC PLAYER IS IN THE WATER")
				ePSFailReason = PS_FAIL_SUBMERGED
				STOP_SCRIPTED_CONVERSATION(TRUE)
				RETURN TRUE
	 		ENDIF
		ENDIF
	ENDIF
	
	IF bRunway
		IF PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT() OR (NOT PS_IS_COORD_IN_FRONT_OF_PLAYER(PS_GET_CHECKPOINT_FROM_ID(1)) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_GET_CHECKPOINT_FROM_ID(1)) > 5.0)
			DEBUG_MESSAGE("FAILED BC PLAYER MISSED THE FIRST GATE")
			ePSFailReason = PS_FAIL_MISSED_FIRST_GATE
			RETURN TRUE
		ENDIF
		
		IF  PS_IS_PLANE_LEAVING_RUNWAY_DURING_TAKEOFF()
			IF PS_IS_RUNWAY_FAIL_TIMER_UP()
				//failed bc player left the runway
				DEBUG_MESSAGE("FAILED BC PLAYER LEFT RUNWAY")
				ePSFailReason = PS_FAIL_LEFT_RUNWAY
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF bHasLanded
		IF PS_IS_PLAYER_LANDED()
			IF PS_IS_LANDED_FAIL_TIMER_UP()
				//failed bc player left the runway
				DEBUG_MESSAGE("FAILED BC PLAYER LANDED VEHICLE")
				ePSFailReason = PS_FAIL_LANDED_VEHICLE
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF bIsIdling
		IF PS_HAS_PLAYER_BEEN_IDLING_TOO_LONG()
			//failed bc player idled for too long.
			DEBUG_MESSAGE("FAILED BC PLAYER IDLED TOO LONG")
			ePSFailReason = PS_FAIL_IDLING
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bVehicleStuck
		IF PS_IS_PLAYER_ALIVE()
			IF IS_VEHICLE_STUCK_TIMER_UP(PS_Struct_Data.myVehicle, VEH_STUCK_JAMMED, STUNT_PLANE_VEH_STUCK_JAM_TIME) OR IS_VEHICLE_STUCK_TIMER_UP(PS_Struct_Data.myVehicle, VEH_STUCK_ON_SIDE, STUNT_PLANE_VEH_STUCK_SIDE_TIME)
				DEBUG_MESSAGE("FAILED BC VEHICLE IS STUCK")
				ePSFailReason = PS_FAIL_STUCK_VEHICLE
				RETURN TRUE
			ENDIF
			
			IF GET_ENTITY_MODEL(PS_Struct_Data.myVehicle) = CUBAN800
				
//				PS_WATCH_LANDING_GEAR_STATE()
				IF NOT (GET_LANDING_GEAR_STATE(PS_Struct_Data.myVehicle) = LGS_BROKEN)
					IF NOT IS_ENTITY_IN_AIR(PS_Struct_Data.myVehicle)
						IF GET_LANDING_GEAR_STATE(PS_Struct_Data.myVehicle) <> LGS_DEPLOYING AND GET_LANDING_GEAR_STATE(PS_Struct_Data.myVehicle) <> LGS_LOCKED_DOWN //when its out or locked down its always reported as deploying.
							IF NOT IS_STRING_NULL_OR_EMPTY(PS_Special_Fail_Reason)
								DEBUG_MESSAGE("FAILED BC VEHICLE LANDED WITHOUT GEAR")
								ePSFailReason = PS_FAIL_SPECIAL_REASON 
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					SCRIPT_ASSERT("Landed gear is broken!")
					DEBUG_MESSAGE("FAILED BC LANDING GEAR WAS DESTROYED")
					ePSFailReason = PS_FAIL_DAMAGED_VEHICLE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bInVehicle
		IF PS_IS_PLAYER_ALIVE()
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Struct_Data.myVehicle, TRUE) //NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), PS_Struct_Data.myVehicle)
				//failed bc player stepped out of the vehicle
				DEBUG_MESSAGE("FAILED BC PLAYER IS NOT IN VEHICLE")
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(PS_Struct_Data.myVehicle))
					ePSFailReason = PS_FAIL_PLAYER_LEFT_HELI
				ELSE
					ePSFailReason = PS_FAIL_PLAYER_LEFT_PLANE
				ENDIF
				RETURN TRUE
//					PS_CLEANUP_WITHOUT_RETURNING_PLAYER_TO_MENU()
			ENDIF
		ELSE
			//failed bc either player or plane died (doesn't matter which ATM)
			DEBUG_MESSAGE("FAILED BC PLAYER IS BROKEN")
			ePSFailReason = PS_FAIL_DAMAGED_VEHICLE
			RETURN TRUE
		ENDIF
	ENDIF
			
	IF bInRange
		IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND NOT PS_IS_LAST_CHECKPOINT_CLEARED()
			IF PS_IS_PLAYER_OUT_OF_RANGE()
				IF PS_IS_RANGE_TIMER_UP()
					DEBUG_MESSAGE("FAILED BC PLAYER IS OUT OF RANGE")
					ePSFailReason = PS_FAIL_OUT_OF_RANGE
					RETURN TRUE
		 		ENDIF
			ELSE
				iRangeTimerWarning = 1 //resets the out of range message
			ENDIF
		ELSE
			//last checkpoint is cleared or the checkpoint_mgr is not active
			IF PS_IS_PLAYER_OUT_OF_RANGE_SPECIAL()
				IF PS_IS_RANGE_TIMER_UP()
					DEBUG_MESSAGE("FAILED BC PLAYER IS OUT OF RANGE")
					ePSFailReason = PS_FAIL_OUT_OF_RANGE
					RETURN TRUE
		 		ENDIF
			ELSE
				iRangeTimerWarning = 1 //resets the out of range message
			ENDIF
		ENDIF
	ENDIF
	
	IF bLandedUpsideDown
		IF NOT IS_TIMER_STARTED(PS_Struct_Data.tmrUpsideDownGround)
			START_TIMER_NOW(PS_Struct_Data.tmrUpsideDownGround)
		ENDIF
	
		IF  IS_VEHICLE_STUCK_TIMER_UP(PS_Struct_Data.myVehicle, VEH_STUCK_ON_ROOF, PS_FAIL_TIME_STUCK)
			DEBUG_MESSAGE("FAILED BC VEHICLE IS ON ITS ROOF")
			ePSFailReason = PS_FAIL_STUCK_VEHICLE
			RETURN TRUE
		ENDIF
		
		IF NOT IS_ENTITY_IN_AIR(PS_Struct_Data.myVehicle) AND IS_ENTITY_UPSIDEDOWN(PS_Struct_Data.myVehicle)
			IF (GET_TIMER_IN_SECONDS_SAFE(PS_Struct_Data.tmrUpsideDownGround) >= 1.0)
				DEBUG_MESSAGE("FAILED BC VEHICLE IS ON ITS ROOF (NOT IN AIR AND UPSIDEDOWN)")
				ePSFailReason = PS_FAIL_STUCK_VEHICLE
				RETURN TRUE
			ELSE
				DEBUG_MESSAGE("Not immediately failing due to only being upsidedown/grounded for less than a second")
			ENDIF
		ELSE	
			RESTART_TIMER_NOW(PS_Struct_Data.tmrUpsideDownGround)
		ENDIF
		
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC PS_AUTO_PILOT(PS_MAIN_STRUCT& data, FLOAT fSpeed, BOOL bRotInterp, FLOAT xRot = 0.0, FLOAT yRot = 0.0)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	//hopefully one of these will work
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
	//
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)

	
	IF NOT IS_ENTITY_DEAD(data.myVehicle)
		bRotInterp = bRotInterp
		PLANE_AUTOPILOT(data.myVehicle, fSpeed, FALSE, xRot, yRot)
	ENDIF
ENDPROC

FUNC BOOL IS_PED_STOPPED_AT_COORD_IN_VEHICLE(PED_INDEX PedIndex, VECTOR VecStartCoors, VECTOR VecEndCoors, FLOAT fLocRadius, structTimer& pulse, BOOL HighlightArea = FALSE)
	VEHICLE_INDEX pedIndex_veh = GET_VEHICLE_PED_IS_USING(PedIndex)
		
	FLOAT fDist			= GET_DISTANCE_BETWEEN_COORDS(VecStartCoors, VecEndCoors)
	
	VECTOR VecMidCoors	= (VecStartCoors+VecEndCoors)/2.0
	VECTOR VecBounds	= <<fLocRadius, (fDist/2.0) + fLocRadius, fLocRadius>>
	VECTOR vCoordDiff	= VecEndCoors - VecStartCoors
	FLOAT fAngle		= GET_HEADING_FROM_VECTOR_2D(vCoordDiff.x, vCoordDiff.y)
	CONST_FLOAT iPULSE_FREQ		5.0
	CONST_FLOAT fRadAverage		2.0
	CONST_FLOAT fRadChange 		0.25
	
	START_TIMER_NOW(pulse)
	
	VECTOR vStep = vCoordDiff / TO_FLOAT(COUNT_OF(strip_blip))
	INT iBlip
	
	REPEAT COUNT_OF(strip_blip) iBlip
		IF NOT DOES_BLIP_EXIST(strip_blip[iBlip])
			VECTOR vBlipCoord = VecStartCoors + (vStep*TO_FLOAT(iBlip+1)) - <<0.0, 0.0, 7.5>>
			FLOAT fBlipCoordGroundZ = 0.0
			
			IF GET_GROUND_Z_FOR_3D_COORD(vBlipCoord, fBlipCoordGroundZ)
				vBlipCoord.z = fBlipCoordGroundZ
			ENDIF
			
			strip_blip[iBlip] = CREATE_BLIP_FOR_COORD(vBlipCoord)
//			SET_BLIP_SCALE(strip_blip[iBlip], BLIP_SIZE_COORD*0.5)
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		FLOAT fPulseValue = (GET_TIMER_IN_SECONDS(pulse) % iPULSE_FREQ)
		#IF PRINT_DEBUG_INFO_TO_SCREEN
			DrawLiteralStringFloat("fPulseValue: ", fPulseValue, 0, HUD_COLOUR_ORANGEDARK)
		#ENDIF
	#ENDIF
	
	INT iStartRed = 0, iStartGreen = 0, iStartBlue = 0
	INT iEndRed = 0, iEndGreen = 0, iEndBlue = 0
	INT iTempAlpha = 0
	
	REPEAT COUNT_OF(strip_blip) iBlip
		FLOAT fPercent = TO_FLOAT(iBlip) / TO_FLOAT(COUNT_OF(strip_blip))
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, iEndRed, iEndGreen, iEndBlue, iTempAlpha)
		GET_HUD_COLOUR(HUD_COLOUR_ORANGE, iStartRed, iStartGreen, iStartBlue, iTempAlpha)
		INT iRed, iGreen, iBlue
		iRed	= ROUND(TO_FLOAT(iStartRed) * fPercent)		+ ROUND(TO_FLOAT(iEndRed) * (1.0-fPercent))
		iGreen	= ROUND(TO_FLOAT(iStartGreen) * fPercent)	+ ROUND(TO_FLOAT(iEndGreen) * (1.0-fPercent))
		iBlue	= ROUND(TO_FLOAT(iStartBlue) * fPercent)	+ ROUND(TO_FLOAT(iEndBlue) * (1.0-fPercent))		
		INT iCol = 0
		SET_BITS_IN_RANGE(iCol, 24, 31, iRed)
		SET_BITS_IN_RANGE(iCol, 16, 23, iGreen)
		SET_BITS_IN_RANGE(iCol, 8,  15, iBlue)
		SET_BITS_IN_RANGE(iCol, 0,  7,  255)
		SET_BLIP_COLOUR(strip_blip[iBlip],	iCol)
		#IF PS_DRAW_DEBUG_LINES_AND_SPHERES 
		FLOAT fRadiusChange = SIN(((fPercent+fPulseValue)%iPULSE_FREQ)*360.0) * fRadChange
		FLOAT fRadius = fRadAverage - (fRadChange/2.0)
		fRadius += fRadiusChange
		DRAW_DEBUG_SPHERE(GET_BLIP_COORDS(strip_blip[iBlip]), fRadius,	iRed,		iGreen,		iBlue,		128+64)
		#ENDIF
	ENDREPEAT
	
	VECTOR VecStoppedBounds	= <<fLocRadius, fLocRadius, fLocRadius>>
	IF NOT bStoppedNearVecEndCoords
		IF IS_PED_AT_ANGLED_COORD(PedIndex, VecEndCoors, VecStoppedBounds*1.5, fAngle, HighlightArea, TRUE, TM_IN_VEHICLE)
			bStoppedNearVecEndCoords = TRUE
		ENDIF
	ELSE		
		CONST_FLOAT fSCORE_MAX -100.0
		FLOAT fCurrentDistGold = PS_Main.myChallengeData.GoldDistance
		FLOAT fCurrentDistBronze = PS_Main.myChallengeData.BronzeDistance
		FLOAT  fMaxlandingDistance = fCurrentdistGold - (fSCORE_MAX * fCurrentdistBronze)
		#IF PS_DRAW_DEBUG_LINES_AND_SPHERES DRAW_DEBUG_SPHERE(VecEndCoors, fMaxlandingDistance, 255, 255, 255, 32) #ENDIF
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedIndex_veh), VecEndCoors) >= fMaxlandingDistance
			bStoppedNearVecEndCoords = FALSE
		ENDIF
	ENDIF
	
	IF IS_PED_AT_ANGLED_COORD(PedIndex, VecMidCoors, VecBounds, fAngle, HighlightArea, TRUE, TM_IN_VEHICLE) OR bStoppedNearVecEndCoords
		FLOAT fVehSpeed = GET_ENTITY_SPEED(pedIndex_veh)
		#IF IS_DEBUG_BUILD
			#IF PRINT_DEBUG_INFO_TO_SCREEN
				DrawLiteralStringFloat("fVehSpeed: ", fVehSpeed, 1, HUD_COLOUR_PURPLE)
			#ENDIF
		#ENDIF
		
		IF IS_VEHICLE_STOPPED(pedIndex_veh)
		OR fVehSpeed <= 0.2
			
			REPEAT COUNT_OF(strip_blip) iBlip
				IF DOES_BLIP_EXIST(strip_blip[iBlip])
					REMOVE_BLIP(strip_blip[iBlip])
				ENDIF
			ENDREPEAT
			CANCEL_TIMER(Pulse)
			
			bStoppedNearVecEndCoords = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_STOPPED_AT_COORD_IN_HELICOPTER(PED_INDEX PedIndex, BLIP_INDEX blipIndex, VECTOR VecCoors, VECTOR VecLocDimensions, BOOL HighlightArea = FALSE)
	IF DOES_BLIP_EXIST(blipIndex)
		BLIP_SPRITE blipSprite = RADAR_TRACE_BOMB_A
		IF (GET_BLIP_SPRITE(blipIndex) <> blipSprite)
			SET_BLIP_SPRITE(blipIndex, blipSprite)
		ENDIF
	ENDIF
	
	
	CONST_FLOAT fZdifference	7.5
	
//	DRAW_SPHERE(VecCoors+<<0.0, 0.0, -VecLocDimensions.z*1.5>>, (VecLocDimensions.x+VecsLocDimensions.y)/2.0)
	
	INT iRed = 255, iGreen = 255, iBlue = 255, iTempAlpha = 255
	GET_HUD_COLOUR(HUD_COLOUR_ORANGE, iRed, iGreen, iBlue, iTempAlpha)
	
	#IF PS_DRAW_DEBUG_LINES_AND_SPHERES
	FLOAT fWidth	= 0.5		//0.75
	FLOAT fHeight	= 0.6		//1.0
	CONST_FLOAT fDEFAULT_POINTS_MULT	7.5
	VECTOR vPointsMult = <<fDEFAULT_POINTS_MULT, fDEFAULT_POINTS_MULT, fDEFAULT_POINTS_MULT>>
	CONST_FLOAT fDEFAULT_RADIUS	2.0
	vPointsMult = <<VecLocDimensions.x*fHeight, VecLocDimensions.y*fWidth, VecLocDimensions.z>>
	FLOAT fRadius = fDEFAULT_RADIUS
	VECTOR vPointsOffset[7]
	vPointsOffset[0] = << fHeight,-fWidth,	0.0>>
	vPointsOffset[1] = << 0.0,-fWidth,		0.0>>
	vPointsOffset[2] = <<-fHeight,-fWidth,	0.0>>
	vPointsOffset[3] = << 0.0,  0.0,		0.0>>
	vPointsOffset[4] = << fHeight, fWidth,	0.0>>
	vPointsOffset[5] = << 0.0, fWidth,		0.0>>
	vPointsOffset[6] = <<-fHeight, fWidth,	0.0>>
		
	INT iPoint
	REPEAT COUNT_OF(vPointsOffset) iPoint
		DRAW_DEBUG_SPHERE(VecCoors+<<0.0, 0.0,-fZdifference>>+(vPointsOffset[iPoint]*vPointsMult), fRadius, iRed, iGreen, iBlue, 128)
	ENDREPEAT
	
	VECTOR vDiff, vStep
	FLOAT fDist
	INT iUnit
	
	vDiff = (vPointsOffset[0]*vPointsMult) - (vPointsOffset[2]*vPointsMult)
	fDist = VMAG(vDiff)
	iUnit = FLOOR(fDist / fRadius)
	vStep = vDiff / TO_FLOAT(iUnit)
	REPEAT iUnit iPoint
		VECTOR vBlipCoord = VecCoors+<<0.0, 0.0,-fZdifference>>+(vPointsOffset[2]*vPointsMult) + (vStep*TO_FLOAT(iPoint+1))
		DRAW_DEBUG_SPHERE(vBlipCoord, fRadius*0.5, iRed, iGreen, iBlue, 255)
	ENDREPEAT
	
	vDiff = (vPointsOffset[1]*vPointsMult) - (vPointsOffset[5]*vPointsMult)
	fDist = VMAG(vDiff)
	iUnit = FLOOR(fDist / fRadius)
	vStep = vDiff / TO_FLOAT(iUnit)
	REPEAT iUnit iPoint
		VECTOR vBlipCoord = VecCoors+<<0.0, 0.0,-fZdifference>>+(vPointsOffset[5]*vPointsMult) + (vStep*TO_FLOAT(iPoint+1))
		DRAW_DEBUG_SPHERE(vBlipCoord, fRadius*0.5, iRed, iGreen, iBlue, 255)
	ENDREPEAT
	
	vDiff = (vPointsOffset[4]*vPointsMult) - (vPointsOffset[6]*vPointsMult)
	fDist = VMAG(vDiff)
	iUnit = FLOOR(fDist / fRadius)
	vStep = vDiff / TO_FLOAT(iUnit)
	REPEAT iUnit iPoint
		VECTOR vBlipCoord = VecCoors+<<0.0, 0.0,-fZdifference>>+(vPointsOffset[6]*vPointsMult) + (vStep*TO_FLOAT(iPoint+1))
		DRAW_DEBUG_SPHERE(vBlipCoord, fRadius*0.5, iRed, iGreen, iBlue, 255)
	ENDREPEAT
	#ENDIF
	
	IF NOT bEnteredLocate
		IF IS_ENTITY_AT_COORD(PedIndex, VecCoors, VecLocDimensions, HighlightArea, true, TM_IN_VEHICLE)
			bEnteredLocate = TRUE
		ENDIF
	ELSE
		IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_USING(PedIndex))
			bEnteredLocate = FALSE
			RETURN TRUE
		ENDIF
		
		IF NOT IS_ENTITY_AT_COORD(PedIndex, VecCoors, VecLocDimensions*2.0, HighlightArea, true, TM_IN_VEHICLE)
			bEnteredLocate = FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PS_UPDATE_PLAYER_DATA(VECTOR thisCheckpoint)
//	#IF IS_DEBUG_BUILD
//		SAVE_STRING_TO_DEBUG_FILE("old (")SAVE_INT_TO_DEBUG_FILE(Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData))
//		SAVE_STRING_TO_DEBUG_FILE("): ")SAVE_PILOT_SCHOOL_STRUCT_TO_DEBUG_FILE(PS_Main.myChallengeData, PS_Main.myPlayerData)
//	#ENDIF

	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
		IF PS_Main.myCheckpointMgr.lessonType > PS_LESSON_STUNT
			PS_Main.myPlayerData.ElapsedTime = TO_FLOAT(CEIL(PS_Main.myCheckpointMgr.savedFinishTime)) 
		ELSE
			PS_Main.myPlayerData.ElapsedTime = TO_FLOAT(CEIL(GET_TIMER_IN_SECONDS_SAFE(PS_Main.tRaceTimer)))
		ENDIF
	ELSE
		PS_Main.myPlayerData.ElapsedTime = -1
	ENDIF

	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
		IF IS_PED_RAGDOLL(PLAYER_PED_ID())
			//add percentage of health the player lost during the jump
			PS_Main.myPlayerData.Multiplier += GET_ENTITY_SPEED(PLAYER_PED_ID())/30
			PRINTLN(">>>>>PS_Main.myPlayerData.Multiplier is ", PS_Main.myPlayerData.Multiplier)
		ELSE
			PS_Main.myPlayerData.Multiplier = 1
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(VECTOR_ZERO, thisCheckpoint)
			IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
				PS_Main.myPlayerData.LandingDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle), thisCheckpoint, FALSE)
			ELSE
				IF (NOT DOES_ENTITY_EXIST(PS_Main.myVehicle)) OR GET_ENTITY_MODEL(PS_Main.myVehicle) != CARGOPLANE
					PS_Main.myPlayerData.LandingDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), thisCheckpoint, FALSE)
				ENDIF
			ENDIF
		ELSE
			PRINTSTRING("invalid dist 'ere")PRINTNL()
			SCRIPT_ASSERT("invalid dist 'ere")
			PS_Main.myPlayerData.LandingDistance = -2
		ENDIF
		
		IF g_current_selected_PilotSchool_class = PSC_parachuteOntoTarget OR g_current_selected_PilotSchool_class = PSC_chuteOntoMovingTarg
			PS_Main.myPlayerData.LandingDistance *= PS_Main.myPlayerData.Multiplier
		ENDIF
		
	ELSE
		PS_Main.myPlayerData.LandingDistance = -1
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_3_checkpointsPassed)
//		IF iNumMissedCheckpoints > -1
//			PS_Main.myPlayerData.CheckpointCount = PS_Main.myChallengeData.TotalCheckpoints - iNumMissedCheckpoints
////		IF iParachuteCheckpointCount >= 0
////			newPilotSchoolStruct.checkpointCount = iParachuteCheckpointCount
//		ELSE
//			PRINTSTRING("invalid checkpoint count???")PRINTNL()
//			SCRIPT_ASSERT("invalid checkpoint count???")
////			newPilotSchoolStruct.checkpointCount = -2
//		ENDIF
	ELSE
		PS_Main.myPlayerData.CheckpointCount  = -1
//		PS_Main.myChallengeData.TotalCheckpoints = -1
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
		
		PS_Main.myPlayerData.FormationTimer = GET_TIMER_IN_SECONDS_SAFE(PS_Main.tHealthTimer)
	ELSE
		PS_Main.myPlayerData.FormationTimer = - 1
	ENDIF
	
	
//	#IF IS_DEBUG_BUILD
//		SAVE_STRING_TO_DEBUG_FILE("new (")SAVE_INT_TO_DEBUG_FILE(Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData))
//		SAVE_STRING_TO_DEBUG_FILE("): ")SAVE_PILOT_SCHOOL_STRUCT_TO_DEBUG_FILE(PS_Main.myChallengeData, PS_Main.myPlayerData)
//		SAVE_NEWLINE_TO_DEBUG_FILE()
//	#ENDIF
	
	bPlayerDataHasBeenUpdated = TRUE
ENDPROC

/// PURPOSE:
///    Determines if we should award a medal and/or if we should save best time/dist
/// PARAMS:
///    thisCheckpoint - 
PROC PS_UPDATE_RECORDS(VECTOR thisCheckpoint, BOOL bSkipScoreUpdate = FALSE)
	IF NOT bSkipScoreUpdate
		IF bPlayerDataHasBeenUpdated
			EXIT
		ELSE
			PS_UPDATE_PLAYER_DATA(thisCheckpoint)	
		ENDIF
	
		IF NOT PS_Main.myChallengeData.bBronzeOverride AND NOT Pilot_School_Data_Has_Fail_Reason()
			Pilot_School_Data_Check_Failed_Goals(PS_Main.myChallengeData, PS_Main.myPlayerData)
		ENDIF
	ENDIF
	
	IF Pilot_School_Data_Has_Fail_Reason()
		EXIT
	ENDIF
	
	//tells us if we should be lenient handing out medals
	BOOL bStrictRequirements
	INT curScore, savedScore
	
	savedScore = Pilot_School_Data_Get_Score(PS_Main.myChallengeData, g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class])
	
	IF NOT bSkipScoreUpdate
		curScore = Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData)

		#IF IS_DEBUG_BUILD
			#IF PRINT_DEBUG_INFO_TO_SCREEN
				DEBUG_MESSAGE("%%%%%%%%%%%%% Current score is ... ", GET_STRING_FROM_INT(curScore))
				DEBUG_MESSAGE("%%%%%%%%%%%%% Saved score is ... ", GET_STRING_FROM_INT(savedScore))
			#ENDIF
		#ENDIF
		
		//find out if we have strict medal requirements (if lesson is based only on time, we usually still want to award a bronze medal)
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion) OR 
			Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_3_checkpointsPassed)
			bStrictRequirements = TRUE 
		ELSE
			//if we reach here, only time or dist matters
			IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
				IF PS_Main.myChallengeData.bBronzeOverride
					bStrictRequirements = FALSE
				ELSE
					bStrictRequirements = TRUE
				ENDIF
			ELSE
				bStrictRequirements = FALSE
			ENDIF
		ENDIF
	ELSE
		curScore = iChallengeScores[g_current_selected_PilotSchool_class]
	ENDIF
	
	BOOL bNewMedal = FALSE //bool to keep track if we should save a new medal at the end of the proc
	BOOL bCheckForNewTimeDist = FALSE //bool to keep track if we want to examine player data
	
	//find out if we got a medal
	IF NOT Pilot_School_Data_Has_Fail_Reason() AND curScore >= iSCORE_FOR_BRONZE //we got a valid score this lesson
		//if our new score is better than our saved score (which could be -1), then we definitely got a new medal 
		IF curScore > savedScore
			bNewMedal = TRUE 
			#IF IS_DEBUG_BUILD
				#IF PRINT_DEBUG_INFO_TO_SCREEN
					DEBUG_MESSAGE("%%%%%%%%%%%%% Current Score is greater than saved score... Awarding a new medal!")
				#ENDIF
			#ENDIF
		ELIF curScore = savedScore
			IF Pilot_School_Data_Has_Fail_Reason()
				bNewMedal = FALSE
			ELSE
				bNewMedal = TRUE
			ENDIF
			//check to see if we should save the best time
			bCheckForNewTimeDist = TRUE
			#IF IS_DEBUG_BUILD
				#IF PRINT_DEBUG_INFO_TO_SCREEN
					DEBUG_MESSAGE("%%%%%%%%%%%%% Current Score is equal to saved score... No new medal!")
				#ENDIF
			#ENDIF
		ENDIF
	ELSE
		//make sure we dont get rid of the old score in case we save shit
//		IF savedScore > 0
//			curScore = savedScore
//		ENDIF
		IF bStrictRequirements 
			bNewMedal = FALSE
			//check to see if we should save the best time
			bCheckForNewTimeDist = TRUE
			#IF IS_DEBUG_BUILD
				#IF PRINT_DEBUG_INFO_TO_SCREEN
					DEBUG_MESSAGE("%%%%%%%%%%%%% Current Score is less than Bronze, so no medal, but we should still check for best time/dist!")
				#ENDIF
			#ENDIF
		ELSE //only time matters
			//if we have an ignoreable reason
			IF Pilot_School_Data_Has_Ignorable_Fail_Reason()
				//if we dont already have a saved score, we get a new medal
				bNewMedal = TRUE
				#IF IS_DEBUG_BUILD
					#IF PRINT_DEBUG_INFO_TO_SCREEN
						DEBUG_MESSAGE("%%%%%%%%%%%%% Current Score is less than Bronze, we're awarding a bronze since only time matters and the lesson got completed!")
					#ENDIF
				#ENDIF
			ELSE
				bCheckForNewTimeDist = TRUE
				#IF IS_DEBUG_BUILD
					#IF PRINT_DEBUG_INFO_TO_SCREEN
						DEBUG_MESSAGE("%%%%%%%%%%%%% We don't have an ignorable fail reason!!! Checking for best time/dist ")
					#ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	iCurrentChallengeScore = curScore
	
	//save last time/dist data
	g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].LastElapsedTime = PS_Main.myPlayerData.ElapsedTime
	PS_Main.myPlayerData.LastElapsedTime = PS_Main.myPlayerData.ElapsedTime
	g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].LastLandingDistance = PS_Main.myPlayerData.LandingDistance
	PS_Main.myPlayerData.LastLandingDistance = PS_Main.myPlayerData.LandingDistance
	//save current medal even if we already have a better one
	
	IF curScore >= iSCORE_FOR_GOLD
		PS_Main.myPlayerData.eLastMedal = PS_GOLD
		g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].eLastMedal = PS_GOLD
	ELIF curScore >= iSCORE_FOR_SILVER
		PS_Main.myPlayerData.eLastMedal = PS_SILVER
		g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].eLastMedal = PS_SILVER
	ELIF curScore >= iSCORE_FOR_BRONZE
		PS_Main.myPlayerData.eLastMedal = PS_BRONZE
		g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].eLastMedal = PS_BRONZE
	ENDIF
	
	IF bNewMedal
		IF curScore >= iSCORE_FOR_GOLD
			PS_Main.myPlayerData.eMedal = PS_GOLD
			g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].eMedal = PS_GOLD
		ELIF curScore >= iSCORE_FOR_SILVER
			PS_Main.myPlayerData.eMedal = PS_SILVER
			g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].eMedal = PS_SILVER
		ELIF curScore >= iSCORE_FOR_BRONZE
			PS_Main.myPlayerData.eMedal = PS_BRONZE
			g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].eMedal = PS_BRONZE
		ENDIF
		
		iChallengeScores[g_current_selected_PilotSchool_class] = curScore
		
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS, TRUE)
		ENDIF
		
		PS_Main.myPlayerData.HasPassedLesson = TRUE
		PS_Main.myChallengeData.LockStatus = PSS_NEW
		bCheckForNewTimeDist = TRUE		
	ENDIF
	
	//FLOAT dataToWriteToLeaderboard
	BOOL bSaveNewTimeDist = FALSE
	//figure out if we should update best time/distance
	IF bCheckForNewTimeDist
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
			IF bNewMedal OR Pilot_School_Data_Has_Ignorable_Fail_Reason()
				IF g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].ElapsedTime < 0 OR PS_Main.myPlayerData.ElapsedTime < g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].ElapsedTime
					bSaveNewTimeDist = TRUE
				ENDIF
			ENDIF
		ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
			IF bNewMedal OR Pilot_School_Data_Has_Ignorable_Fail_Reason()
				IF g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].LandingDistance < 0 OR PS_Main.myPlayerData.LandingDistance < g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].LandingDistance
					bSaveNewTimeDist = TRUE
				ENDIF
			ENDIF
		ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
			IF bNewMedal OR Pilot_School_Data_Has_Ignorable_Fail_Reason()
				IF g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].FormationTimer < 0 OR PS_Main.myPlayerData.FormationTimer > g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].FormationTimer
					bSaveNewTimeDist = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT bPlayerUsedDebugSkip //only save new data if we didnt use debug skip (J-skip)
	#ENDIF
			IF bSaveNewTimeDist			
				#IF IS_DEBUG_BUILD
					#IF PRINT_DEBUG_INFO_TO_SCREEN
						DEBUG_MESSAGE("%%%%%%%%%%%%% Saving player data for ", PS_Challenges[g_current_selected_PilotSchool_class].Title)
						DEBUG_MESSAGE("%%%%%%%%%%%%% Score for this challenge is ", GET_STRING_FROM_INT(iChallengeScores[g_current_selected_PilotSchool_class]))
					#ENDIF
				#ENDIF
				g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class] = PS_Main.myPlayerData
			ENDIF
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	
	bPlayerDataHasBeenUpdated = TRUE
ENDPROC

PROC PS_GET_TARGET_GOAL_TIME(structTimer RaceTime, FLOAT& elapsedTime, TEXT_LABEL& sMedal, PODIUMPOS& ePodium)
	FLOAT currentTime, goldTime, silverTime, bronzeTime 
	UNUSED_PARAMETER(sMedal)
	IF IS_TIMER_STARTED(RaceTime)
		currentTime = GET_TIMER_IN_SECONDS(RaceTime)
	ENDIF
	goldTime = PS_Main.myChallengeData.GoldTime
	bronzeTime = PS_Main.myChallengeData.BronzeTime
	silverTime = PS_Main.myChallengeData.SilverTime
	
	IF currentTime < goldTime
		sMedal = "PS_HUDTARG_G"
		elapsedTime = goldTime
		ePodium = PODIUMPOS_GOLD
	ELIF currentTime < silverTime
		sMedal = "PS_HUDTARG_S"
		elapsedTime = silverTime
		ePodium = PODIUMPOS_SILVER
	ELIF currentTime < bronzeTime
		sMedal = "PS_HUDTARG_B"
		elapsedTime = bronzeTime
		ePodium = PODIUMPOS_BRONZE
	ELSE		
		elapsedTime = -1 //default value to make the BEST TIME hud element disappear
		sMedal = ""
		ePodium = PODIUMPOS_NONE
	ENDIF
ENDPROC

//******************************************************************************
//******************************************************************************
//Moving Truck target for skydiving lessong
//******************************************************************************
//******************************************************************************

PROC PS_ATTEMPT_TO_CLEAR_CARGO_PLANE()
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
					EXIT
				ENDIF
			ENDIF
			CLEAR_PED_TASKS(GET_PED_IN_VEHICLE_SEAT(PS_Main.myVehicle))
			IF NOT IS_ENTITY_ON_SCREEN(PS_Main.myVehicle)
				DELETE_VEHICLE(PS_Main.myVehicle)
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

PROC PS_PARACHUTE_UPDATE_TRUCK_TARGET(VECTOR& thisMovingTarget, BLIP_INDEX& thisMovingTargetBlip, BOOL bUpdateChaseCam = TRUE)

	IF NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
		thisMovingTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0, -5, 1>>)// GET_ENTITY_COORDS(PS_FlatBedTruck)
		IS_VEHICLE_FUCKED(PS_FlatBedTruck)
		IF DOES_BLIP_EXIST(thisMovingTargetBlip)
			SET_BLIP_COORDS(thisMovingTargetBlip, GET_ENTITY_COORDS(PS_FlatBedTruck))
		ELSE
			thisMovingTargetBlip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(PS_FlatBedTruck))
			SET_BLIP_SCALE(thisMovingTargetBlip, 1.2)
		ENDIF
		
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
		END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlip)
		
		DRAW_MARKER(MARKER_RING, thisMovingTarget, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(thisMovingTarget, 100), FALSE, FALSE)
		DRAW_MARKER(MARKER_RING, thisMovingTarget, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(thisMovingTarget, 100), FALSE, FALSE)
		DRAW_MARKER(MARKER_RING, thisMovingTarget, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(thisMovingTarget, 100), FALSE, FALSE)		
		
//		SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, PS_MOVE_TRUCK_TARGET_PTFX_SCALE * tempPercent)
		
		IF bUpdateChaseCam
//			PS_SET_HINT_CAM_COORD(PS_Main.myCheckpoints[1].position)
//			Pilot_School_HUD_Set_Chase_Cam(PS_Main.myCheckpoints[1].position)
		ENDIF
	ENDIF
ENDPROC

PROC PS_MOVING_CHUTE_STOP_TARGET_TRUCK(VECTOR& truckCoords)
	IF NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
		PED_INDEX targetDriver = GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck)
		VECTOR vTrash, vPosition, vForward
		GET_ENTITY_MATRIX(PS_FlatBedTruck, vForward, vTrash, vTrash, vPosition)
		IF NOT IS_PED_INJURED(targetDriver)
			vForward = 10.0 * vForward
			
			TASK_VEHICLE_DRIVE_TO_COORD(targetDriver, PS_FlatBedTruck, vPosition + vForward, GET_ENTITY_SPEED(PS_FlatBedTruck), DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(PS_FlatBedTruck), DRIVINGMODE_AVOIDCARS, 2.0, 2.0)
//			TASK_VEHICLE_DRIVE_WANDER(targetDriver, PS_FlatBedTruck, GET_ENTITY_SPEED(PS_FlatBedTruck), DRIVINGMODE_AVOIDCARS)
		ENDIF
//		BRING_VEHICLE_TO_HALT(PS_FlatBedTruck, 10.0, 10)
		truckCoords = GET_ENTITY_COORDS(PS_FlatBedTruck)
	ENDIF
ENDPROC

PROC PS_MOVING_CHUTE_CREATE_TARGET_TRUCK()
	PED_INDEX targetDriver
	
	IF NOT DOES_ENTITY_EXIST(PS_FlatBedTruck)
		PS_FlatBedTruck = CREATE_VEHICLE(PHANTOM, <<-959.5065, -3361.2590, 12.9445>>, 59.7732)
		SET_VEHICLE_COLOUR_COMBINATION(PS_FlatBedTruck, 4)
		SET_ENTITY_LOAD_COLLISION_FLAG(PS_FlatBedTruck, TRUE)
	ELSE
		SET_VEHICLE_FIXED(PS_FlatBedTruck)
		SET_ENTITY_COORDS(PS_FlatBedTruck, <<-959.5065, -3361.2590, 12.9445>>)
		SET_ENTITY_HEADING(PS_FlatBedTruck, 59.7732)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(PS_FlatBedTrailer)
		PS_FlatBedTrailer = CREATE_VEHICLE(TRFLAT, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0.0, -10.0, 0.0>>), 59.7732)
		SET_ENTITY_LOAD_COLLISION_FLAG(PS_FlatBedTrailer, TRUE)
		SET_ENTITY_LOD_DIST(PS_FlatBedTrailer, 2000)
	ELSE
		SET_VEHICLE_FIXED(PS_FlatBedTrailer)
	ENDIF
	
	IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(PS_FlatBedTruck)
		ATTACH_VEHICLE_TO_TRAILER(PS_FlatBedTruck, PS_FlatBedTrailer)
	ENDIF
	
	targetDriver = GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck)
	
	IF NOT DOES_ENTITY_EXIST(targetDriver)
		targetDriver = CREATE_PED_INSIDE_VEHICLE(PS_FlatBedTruck, PEDTYPE_MISSION, PilotModel, VS_DRIVER)
		SET_ENTITY_AS_MISSION_ENTITY(targetDriver)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(targetDriver, TRUE)
	ELSE
		
		IF NOT IS_PED_INJURED(targetDriver)
			CLEAR_PED_TASKS(targetDriver)
			SET_ENTITY_HEALTH(targetDriver, GET_ENTITY_MAX_HEALTH(targetDriver))
			RESET_PED_VISIBLE_DAMAGE(targetDriver)
			IF (NOT IS_PED_IN_VEHICLE(targetDriver, PS_FlatBedTruck)) OR GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck, VS_DRIVER) <> targetDriver
				IF IS_PED_IN_ANY_VEHICLE(targetDriver, TRUE)
					SPECIAL_FUNCTION_DO_NOT_USE(targetDriver)
				ENDIF
				SET_PED_INTO_VEHICLE(targetDriver, PS_FlatBedTruck, VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF
	
	TASK_STAND_STILL(targetDriver, -1)
ENDPROC

PROC PS_MOVING_CHUTE_START_TARGET_TRUCK(BLIP_INDEX& thisTargetBlip)
	IF NOT IS_ENTITY_DEAD(PS_FlatBedTruck)
		SET_VEHICLE_ENGINE_ON(PS_FlatBedTruck, TRUE, TRUE)
		IF (NOT IS_ENTITY_DEAD(PS_FlatBedTrailer))
		AND (NOT IS_VEHICLE_ATTACHED_TO_TRAILER(PS_FlatBedTruck))
			ATTACH_VEHICLE_TO_TRAILER(PS_FlatBedTruck, PS_FlatBedTrailer)
			//thisTargetBlip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(PS_FlatBedTrailer))
			thisTargetBlip = ADD_BLIP_FOR_ENTITY(PS_FlatBedTrailer)
		ENDIF
//		PTFX_SkydivingTargetFlare = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_mgpilot_target_flare", PS_FlatBedTruck, <<0,-5,0>>, <<0,0,0>>)
//		SET_PARTICLE_FX_LOOPED_COLOUR(PTFX_SkydivingTargetFlare, 1.0, 1.0, 0)
//		SET_PARTICLE_FX_LOOPED_ALPHA(PTFX_SkydivingTargetFlare, 0.75)
//		SET_PARTICLE_FX_LOOPED_SCALE(PTFX_SkydivingTargetFlare, 5.0)
		TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck, VS_DRIVER), PS_FlatBedTruck, "ps_trucktarget_cw", DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT + EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 5.0, TRUE)
		SET_ENTITY_LOD_DIST(PS_FlatBedTruck, 2000)
	ENDIF
ENDPROC

PROC PS_PARACHUTE_CLEANUP_TRUCK_TARGET()
	IF DOES_ENTITY_EXIST(PS_FlatBedTruck)
		IF NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck) AND NOT IS_VEHICLE_EMPTY(PS_FlatBedTruck)
			PED_INDEX tempPed = GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck)
			SPECIAL_FUNCTION_DO_NOT_USE(tempPed)
			DELETE_PED(tempPed)
		ENDIF
		IF IS_VEHICLE_ATTACHED_TO_TRAILER(PS_FlatBedTruck)
			VEHICLE_INDEX tempVehIdx
			GET_VEHICLE_TRAILER_VEHICLE(PS_FlatBedTruck ,tempVehIdx)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(tempVehIdx)
		ENDIF
		SET_VEHICLE_AS_NO_LONGER_NEEDED(PS_FlatBedTruck)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFX_SkydivingTargetFlare)
		REMOVE_PARTICLE_FX(PTFX_SkydivingTargetFlare)
	ENDIF
ENDPROC

PROC PS_INIT_TRUCK_LANDING_SCENE
	SETTIMERA(0)
	bFinishedTruckLandingScene = FALSE
	REQUEST_ANIM_DICT("Skydiving")
	WHILE NOT HAS_ANIM_DICT_LOADED("Skydiving")
		WAIT(0)
	ENDWHILE
ENDPROC

FUNC BOOL PS_IS_TRUCK_LANDING_SCENE_FINISHED()
	RETURN bFinishedTruckLandingScene
ENDFUNC

PROC PS_CLEANUP_TRUCK_LANDING_SCENE()
	//destroy cams
ENDPROC 

PROC PS_UPDATE_TRUCK_LANDING_SCENE()
	//update cutscene
	SWITCH(ePS_TruckLandingState)
		CASE PS_TRUCK_LANDING_STATE_01
//			ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED_ID(), PS_FlatBedTruck, 0, 0, <<0, -6, 1.75>>, <<0,0,0>>, <<0,0,GET_ENTITY_HEADING(PS_FlatBedTruck)>>, 1, TRUE)
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), PS_FlatBedTruck, 0, <<0, -6, 1.5>>, <<0,0,GET_ENTITY_HEADING(PS_FlatBedTruck)>>)
//			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), PS_FlatBedTrailer, 0, <<0, 0, 1.75>>, <<0,0,GET_ENTITY_HEADING(PS_FlatBedTrailer)>>)
			TASK_PLAY_ANIM(PLAYER_PED_ID(), "Skydiving", "land", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_HOLD_LAST_FRAME) //			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), PS_FlatBedTruck, 0, <<0, -1, 1.5>>, <<0,0,0>>)			
			PS_CutsceneCamera = CREATE_CAMERA()
			ATTACH_CAM_TO_ENTITY(PS_CutsceneCamera, PS_FlatBedTruck, <<4,2,3>>)
			POINT_CAM_AT_ENTITY(PS_CutsceneCamera, PLAYER_PED_ID(), <<0, 0, 0>>)
			SET_CAM_ACTIVE(PS_CutsceneCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SETTIMERA(0)
			ePS_TruckLandingState = PS_TRUCK_LANDING_STATE_02
			BREAK
		CASE PS_TRUCK_LANDING_STATE_02
			IF TIMERA() > 3000
				ePS_TruckLandingState = PS_TRUCK_LANDING_STATE_03
			ENDIF
			BREAK
		CASE PS_TRUCK_LANDING_STATE_03
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DETACH_ENTITY(PLAYER_PED_ID())
			bFinishedTruckLandingScene = TRUE
			ePS_TruckLandingState = PS_TRUCK_LANDING_STATE_IDLE
			BREAK
		DEFAULT
			BREAK
	ENDSWITCH
	//cut cam (if necessary)
	//attach player
	//set player into animation
	//direct player to the area on the back of the truck
	//make sure the player ends up on the flatbed of the truck at the end of the scene
ENDPROC

PROC PS_INIT_TRUCK_CRASHING_SCENE()
	SETTIMERA(0)
	bFinishedTruckCrashingScene = FALSE
	REQUEST_ANIM_DICT("Skydiving")
	WHILE NOT HAS_ANIM_DICT_LOADED("Skydiving")
		WAIT(0)
	ENDWHILE
ENDPROC

FUNC BOOL PS_IS_TRUCK_CRASHING_SCENE_FINISHED()
	RETURN bFinishedTruckCrashingScene
ENDFUNC

PROC PS_CLEANUP_TRUCK_CRASHING_SCENE()
	//destroy cams
ENDPROC 

PROC PS_UPDATE_TRUCK_CRASHING_SCENE()
	//update cutscene
	SWITCH(ePS_TruckCrashingState)
		CASE PS_TRUCK_CRASHING_STATE_01
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0, 4, 0.5>>))
			SET_ENTITY_HEADING(PLAYER_PED_ID(), -GET_ENTITY_HEADING(PS_FlatBedTruck))
			PS_CutsceneCamera = CREATE_CAMERA()
			ATTACH_CAM_TO_ENTITY(PS_CutsceneCamera, PS_FlatBedTruck, <<4,2,3>>)
			POINT_CAM_AT_ENTITY(PS_CutsceneCamera, PLAYER_PED_ID(), <<0, 0, 0>>)
			SET_CAM_ACTIVE(PS_CutsceneCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SETTIMERA(0)
			ePS_TruckCrashingState = PS_TRUCK_CRASHING_STATE_02
			BREAK
		CASE PS_TRUCK_CRASHING_STATE_02
			IF TIMERA() > 3000
				ePS_TruckCrashingState = PS_TRUCK_CRASHING_STATE_03
			ENDIF
			BREAK
		CASE PS_TRUCK_CRASHING_STATE_03
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DETACH_ENTITY(PLAYER_PED_ID())
			bFinishedTruckCrashingScene = TRUE
			ePS_TruckCrashingState = PS_TRUCK_CRASHING_STATE_IDLE
			BREAK
		DEFAULT
			BREAK
	ENDSWITCH
	//cut cam (if necessary)
	//attach player
	//set player into animation
	//direct player to the area on the back of the truck
	//make sure the player ends up on the flatbed of the truck at the end of the scene
ENDPROC

//******************************************************************************
//******************************************************************************
// Calculations for stunts and for HUD stuff
//******************************************************************************
//******************************************************************************


/// PURPOSE:
///    In a challenge with checkpoints, this will see if the checkpoint is off screen. If it is, it will update the 
///    2D screen position and set the icon to be visible.
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_UPDATE_CALCULATIONS()

	IF PS_UI_RaceHud.bIsTimerActive
		FLOAT fTimeToBeat
		TEXT_LABEL sNextMedal
		PODIUMPOS ePodium
		PS_HUD_UPDATE_TIME_BONUS(PS_Main.tRaceTimer, PS_UI_RaceHud.iTimeBonus)
		PS_GET_TARGET_GOAL_TIME(PS_Main.tRaceTimer, fTimeToBeat, sNextMedal, ePodium)
		PS_HUD_SET_TARGET_GOAL_TIME(fTimeToBeat, sNextMedal, ePodium)
		//Updates variables in the HUD scripts. If timer is active, they will be displayed automatically
	ENDIF
	
	IF PS_UI_RaceHud.bIsDistActive
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			PS_HUD_SET_DIST_HUD(GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), PS_GET_LAST_CHECKPOINT()))
		ENDIF
	ENDIF
	
	IF PS_UI_GutterIcon.bIsGutterActive
		IF PS_IS_CHECKPOINT_MGR_ACTIVE()
			FLOAT tempx, tempy
			IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(PS_GET_CURRENT_CHECKPOINT(), tempx, tempy)
				PS_CALCULATE_GUTTER_ICON_POSITION()
				PS_HUD_SET_GUTTER_ICON_VISIBLE(TRUE)
			ELSE
				PS_HUD_SET_GUTTER_ICON_VISIBLE(FALSE)
			ENDIF
		ELIF NOT ARE_VECTORS_EQUAL(vPS_MovingTarget, VECTOR_ZERO)
			FLOAT tempx, tempy
			IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPS_MovingTarget, tempx, tempy)
				PS_CALCULATE_GUTTER_ICON_POSITION()
				PS_HUD_SET_GUTTER_ICON_VISIBLE(TRUE)
			ELSE
				PS_HUD_SET_GUTTER_ICON_VISIBLE(FALSE)
			ENDIF
		ENDIF
	ENDIF

	IF bIsAltimeterActive
		FLOAT fTempHeight
		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_ENTITY_IN_AIR(PS_Main.myVehicle)
				//B* 1352821, not sure how intensive the probe is - SiM - 5/17/2013
				VECTOR vPlayerPos, vGroundPos, vWaterPos
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())				
				GET_GROUND_Z_FOR_3D_COORD(vPlayerPos, fTempHeight)
				vGroundPos = vPlayerPos
				vGroundPos.z = fTempHeight				
				
				fTempHeight = VDIST(vPlayerPos, vGroundPos)
				
				IF TEST_PROBE_AGAINST_WATER(vPlayerPos, vGroundPos, vWaterPos)
					fTempHeight = VDIST(vPlayerPos, vWaterPos)
				ENDIF				
				PS_HUD_SET_ALTIMETER(fTempHeight - GroundOffset)
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsAltitudeIndicatorActive
		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			PS_HUD_SET_ALTITUDE_INDICTATOR(GET_ENTITY_ROLL(PS_Main.myVehicle))
		ENDIF
	ENDIF
	
	
	IF PS_UI_ObjectiveMeter.bIsRollMeterActive
	OR PS_UI_ObjectiveMeter.bIsInvertedMeterActive
	OR PS_UI_ObjectiveMeter.bIsLoopMeterActive
	OR PS_UI_ObjectiveMeter.bIsKnifeMeterActive
		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID())		
			PS_UPDATE_OBJECTIVE_METER_DATA()
		ENDIF
	ENDIF

	IF PS_UI_ObjectiveMeter.bIsImmelMeterActive
		IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF PS_UI_ObjectiveMeter.eObjMeterType = PS_OBJECTIVE_TYPE_IMMELMAN_TURN
				Pilot_School_Do_Immelman_Tracking()
			ELIF PS_UI_ObjectiveMeter.eObjMeterType = PS_OBJECTIVE_TYPE_IMMELMAN_ROLL
				Pilot_School_Do_Roll_Tracking()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "J-SKIP")
			bPlayerUsedDebugSkip = TRUE
			RETURN TRUE
		ENDIF
		RETURN FALSE
	
	ENDFUNC
#ENDIF

PROC PS_LESSON_UDPATE()
	PS_UPDATE_CALCULATIONS()
	PS_HUD_UPDATE()
	PS_UPDATE_CHECKPOINT_FLASH()
	PS_UPDATE_CHECKPOINT_MARKER_FLASH()
	PS_UPDATE_CHECKPOINT_MGR()
	UPDATE_OBJHELPALOGUE()
	
	#IF IS_DEBUG_BUILD
		PS_DEBUG_UPDATE()
	#ENDIF
ENDPROC


PROC PS_UPDATE_LOOK_CAM()
	VECTOR vRightStick
	INT iLeftX, iLeftY, iRightX, iRightY
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	GET_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY, IS_PLAYER_CONTROL_ON(PLAYER_ID()))
	
	vRightStick.x = TO_FLOAT(iRightX) / 128.0
	vRightStick.y = TO_FLOAT(iRightY) / -128.0
	
	IF IS_LOOK_INVERTED()
		vRightStick.y *= -1.0
	ENDIF
	
	// Apply player input forces to the acceleration values.
	PS_Main.myJumpCam.vCameraVelocity.z -= vRightStick.x * GET_FRAME_TIME() * 130.0
	PS_Main.myJumpCam.vCameraVelocity.x += vRightStick.y * GET_FRAME_TIME() * 130.0
	
	// Apply dampening to the acceleration values.
	IF ABSF(PS_Main.myJumpCam.vCameraVelocity.z) > 0.001
		PS_Main.myJumpCam.vCameraVelocity.z -= PS_Main.myJumpCam.vCameraVelocity.z * GET_FRAME_TIME() * 4.0
	ELSE
		PS_Main.myJumpCam.vCameraVelocity.z = 0.0
	ENDIF
	
	IF ABSF(PS_Main.myJumpCam.vCameraVelocity.x) > 0.001
		PS_Main.myJumpCam.vCameraVelocity.x -= PS_Main.myJumpCam.vCameraVelocity.x * GET_FRAME_TIME() * 5.0
	ELSE
		PS_Main.myJumpCam.vCameraVelocity.x = 0.0
	ENDIF
	
	// Update the look offsets based on our velocity.
	PS_Main.myJumpCam.vLookCameraRot.z += PS_Main.myJumpCam.vCameraVelocity.z * GET_FRAME_TIME()
	IF PS_Main.myJumpCam.vLookCameraRot.z > 0.5 * 43.7465 // Range
		PS_Main.myJumpCam.vLookCameraRot.z = 0.5 * 43.7465
		PS_Main.myJumpCam.vCameraVelocity.z = 0.0
	ELIF PS_Main.myJumpCam.vLookCameraRot.z < -0.5 * 43.7465
		PS_Main.myJumpCam.vLookCameraRot.z = -0.5 * 43.7465
		PS_Main.myJumpCam.vCameraVelocity.z = 0.0
	ENDIF
	
	PS_Main.myJumpCam.vLookCameraRot.x += PS_Main.myJumpCam.vCameraVelocity.x * GET_FRAME_TIME()
	IF PS_Main.myJumpCam.vLookCameraRot.x > 0.5 * 21.6 // Range
		PS_Main.myJumpCam.vLookCameraRot.x = 0.5 * 21.6
		PS_Main.myJumpCam.vCameraVelocity.x = 0.0
	ELIF PS_Main.myJumpCam.vLookCameraRot.x < -0.5 * 21.6
		PS_Main.myJumpCam.vLookCameraRot.x = -0.5 * 21.6
		PS_Main.myJumpCam.vCameraVelocity.x = 0.0
	ENDIF
	
	VECTOR vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
	SET_CAM_COORD(PS_Main.previewCam1, PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset)
	SET_CAM_ROT(PS_Main.previewCam1, <<-33.0000 + PS_Main.myJumpCam.vLookCameraRot.x, 0.0, -88.515 + fStartHeading + PS_Main.myJumpCam.vLookCameraRot.z>>)
	
	SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
ENDPROC

PROC PS_SETUP_JUMP_CAM()
	VECTOR vCamRot
	
	IF NOT DOES_CAM_EXIST(PS_Main.previewCam2)
		PS_Main.previewCam2 = CREATE_CAMERA()
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
//		SET_CAM_COORD(PS_Main.previewCam1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, <<2.0, -0.3, -7.7824>>))
//		SET_CAM_ROT(PS_Main.previewCam1, <<-44.8000, 0.0000, -20.2737 + GET_ENTITY_HEADING(PS_Main.myVehicle)>>)
//		SET_CAM_FOV(PS_Main.previewCam1, 50.0000)
//		SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
		
		SET_CAM_COORD(PS_Main.previewCam2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, <<2.0, -0.3, -7.7824>>))
		vCamRot = GET_CAM_ROT(PS_Main.previewCam1)
		vCamRot.x = 0.0
		SET_CAM_ROT(PS_Main.previewCam2, vCamRot)
		SET_CAM_FOV(PS_Main.previewCam2, GET_CAM_FOV(PS_Main.previewCam1))
		SET_CAM_NEAR_CLIP(PS_Main.previewCam2, PS_GET_NEAR_CLIP())
	ENDIF
	
	//SET_CAM_ACTIVE(PS_Main.previewCam2, FALSE)
	//SET_CAM_ACTIVE(PS_Main.previewCam1, TRUE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	RESTART_TIMER_NOW(PS_Main.myJumpCam.camTimer)
ENDPROC


//******************************************************************************
//******************************************************************************
//Preview stuff
//******************************************************************************
//******************************************************************************

FUNC BOOL PS_PREVIEW_CHECK_FOR_SKIP()
	//check for timer
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		RETURN TRUE
	ELIF TIMER_DO_ONCE_WHEN_READY(tPS_PreviewTimer, fPreviewTime)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC PS_DRAW_PREVIEW_LESSON_TITLE(TEXT_PLACEMENT thisPlacement, TEXT_STYLE thisStyle, STRING thisTitle)
	SET_TEXT_STYLE(thisStyle)	
	SET_TEXT_RIGHT_JUSTIFY(FALSE)
	SET_TEXT_CENTRE(FALSE)	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(thisTitle)
	END_TEXT_COMMAND_DISPLAY_TEXT(thisPlacement.x, thisPlacement.y)
ENDPROC

SCALEFORM_INDEX sfTelevisionBorder

FUNC BOOL PS_PREVIEW_UPDATE_CUTSCENE()
	IF PS_PreviewState > PS_PREVIEW_INIT AND PS_PreviewState < PS_PREVIEW_CLEANUP
		PS_UPDATE_CHECKPOINT_FLASH()
		PS_UPDATE_CHECKPOINT_MGR()
	ENDIF
	
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	
	SWITCH(PS_PreviewState)
		CASE PS_PREVIEW_INIT				
			//draw early to account for delay
			IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTelevisionBorder, 255, 255, 255, 255)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			PS_Main.customPreviewState = PS_PREVIEW_CUSTOM_STATE_1
			IF PS_Main.myChallengeData.HasBeenPreviewed OR bRetryChallenge
//					SCRIPT_ASSERT("skipping preview!!")
				PS_PreviewState = PS_PREVIEW_CLEANUP
				BREAK
			ENDIF
			//setup title placement
			IF IS_PC_VERSION()	//B*2214452
				previewTitlePlacement.x = PIXEL_X_TO_FLOAT(300)
				previewTitlePlacement.y = PIXEL_Y_TO_FLOAT(538)
			ELSE
				previewTitlePlacement.x = PIXEL_X_TO_FLOAT(300)
				previewTitlePlacement.y = PIXEL_Y_TO_FLOAT(548)
			ENDIF
			SET_MINIGAME_MENU_TITLE(previewTitleStyle)
			previewTitleStyle.YScale = 0.54
//			SET_TEXT_CURSIVE(previewTitleStyle)
			//setup subtitle placement
			IF IS_PC_VERSION()	//B*2214452
				PS_PREVIEW_DIALOGUE_SETUP(PIXEL_X_TO_FLOAT(300), PIXEL_Y_TO_FLOAT(568), PIXEL_X_TO_FLOAT(300), 0.95)
			ELSE
				PS_PREVIEW_DIALOGUE_SETUP(PIXEL_X_TO_FLOAT(300), PIXEL_Y_TO_FLOAT(578))
			ENDIF
			
			RESTART_TIMER_AT(tPS_PreviewTimer, 0)
			
			SET_TIMECYCLE_MODIFIER("scanline_cam")
			
			PS_PreviewState = PS_PREVIEW_SETUP
			BREAK
		CASE PS_PREVIEW_SETUP			
			INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_SKIP", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			PS_PreviewState = PS_PREVIEW_PLAYING
			BREAK
		CASE PS_PREVIEW_PLAYING
			IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTelevisionBorder, 255, 255, 255, 255)
			ENDIF
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			IF IS_PC_VERSION()	//increase the height of the preview blue box for PC, see B*2096353
				//draw blue bar
				DRAW_RECT(PIXEL_X_TO_FLOAT(640), PIXEL_Y_TO_FLOAT(576),  PIXEL_X_TO_FLOAT(1280), PIXEL_Y_TO_FLOAT(95), 0, 0, 78, 196)
				//draw outline
				DRAW_RECT(PIXEL_X_TO_FLOAT(640), PIXEL_Y_TO_FLOAT(529),  PIXEL_X_TO_FLOAT(1280), PIXEL_Y_TO_FLOAT(1), 255, 255, 255, 196)
				DRAW_RECT(PIXEL_X_TO_FLOAT(640), PIXEL_Y_TO_FLOAT(623),  PIXEL_X_TO_FLOAT(1280), PIXEL_Y_TO_FLOAT(1), 255, 255, 255, 196)
			ELSE
				//draw blue bar
				DRAW_RECT(PIXEL_X_TO_FLOAT(640), PIXEL_Y_TO_FLOAT(576),  PIXEL_X_TO_FLOAT(1280), PIXEL_Y_TO_FLOAT(75), 0, 0, 78, 196)
				//draw outline
				DRAW_RECT(PIXEL_X_TO_FLOAT(640), PIXEL_Y_TO_FLOAT(539),  PIXEL_X_TO_FLOAT(1280), PIXEL_Y_TO_FLOAT(1), 255, 255, 255, 196)
				DRAW_RECT(PIXEL_X_TO_FLOAT(640), PIXEL_Y_TO_FLOAT(613),  PIXEL_X_TO_FLOAT(1280), PIXEL_Y_TO_FLOAT(1), 255, 255, 255, 196)
			ENDIF
			//draw logo
			DRAW_SPRITE("pilotSchool", "FlightSchool_Logo_256", PIXEL_X_TO_FLOAT(196), PIXEL_Y_TO_FLOAT(576), PIXEL_X_TO_FLOAT(128), PIXEL_Y_TO_FLOAT(128), 0, 255, 255, 255, 255)
			//draw title
			PS_DRAW_PREVIEW_LESSON_TITLE(previewTitlePlacement, previewTitleStyle, PS_Challenges[g_current_selected_PilotSchool_class].Title)
			IF (GET_TIMER_IN_SECONDS_SAFE(tPS_PreviewTimer) >= (CUTSCENE_SKIP_DELAY/1000))		
				UPDATE_SIMPLE_USE_CONTEXT(menuInstructions)
			ENDIF
			//draw subtitles
			PS_PREVIEW_DIALOGUE_UPDATE()
			BREAK
		CASE PS_PREVIEW_CLEANUP
			//terminate loop
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfTelevisionBorder)
			PS_PREVIEW_DIALOGUE_STOP()
			PS_PREVIEW_DIALOGUE_CLEANUP()
			CLEAR_TIMECYCLE_MODIFIER()
			PS_PreviewState = PS_PREVIEW_IDLE
			BREAK
		DEFAULT
			BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC			

FUNC INT PS_GET_CLOSEST_PREVIEW_CHECKPOINT(INT recordingid, PS_CHECKPOINT_STRUCT &checkpointarray[], FLOAT playbacktimems)
	VECTOR vehpos1, vehpos2
	INT closestidx, secondclosestidx
	INT i = 0
	FLOAT tempdist, closestdist
	//get two positions in the playback: the start point in our preview, and half a second farther to see what point we're approaching
	vehpos1 = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(recordingid, playbacktimems, sPreviewRecordingName)
	vehpos2 = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(recordingid, playbacktimems + 500, sPreviewRecordingName)
	tempdist = GET_DISTANCE_BETWEEN_COORDS(checkpointarray[i].position, vehpos1)
	closestidx = i
	secondclosestidx = i
	closestdist = tempdist
	
	//get the two closest checkpoints
	REPEAT COUNT_OF(checkpointarray) i
		tempdist = GET_DISTANCE_BETWEEN_COORDS(checkpointarray[i].position, vehpos1)
		IF tempdist < closestdist
			secondclosestidx = closestidx
			closestidx = i
			closestdist = tempdist			
		ENDIF
	ENDREPEAT
	
	//we have the two closest checkpoints
	//now figure out which checkpoint is the "next" checkpoint
	
	//see if we're getting closer to the closest checkpoint, if we're not, the next checkpoint is probably the secondclosest
	IF GET_DISTANCE_BETWEEN_COORDS(checkpointarray[closestidx].position, vehpos2) < GET_DISTANCE_BETWEEN_COORDS(checkpointarray[closestidx].position, vehpos1)
		RETURN closestidx
	ELIF GET_DISTANCE_BETWEEN_COORDS(checkpointarray[secondclosestidx].position, vehpos2) < GET_DISTANCE_BETWEEN_COORDS(checkpointarray[secondclosestidx].position, vehpos1)
		RETURN secondclosestidx
	ELSE
		//neither are getting closer, so perhaps return the highest index, then increment it if we're not overstepping our bounds
		IF closestidx > secondclosestidx
			IF closestidx+1 < COUNT_OF(checkpointarray)
				RETURN closestidx+1
			ELSE
				RETURN -1
			ENDIF
		ELIF closestidx < secondclosestidx
			IF secondclosestidx+1 < COUNT_OF(checkpointarray)
				RETURN secondclosestidx+1
			ELSE
				RETURN -1
			ENDIF
		ELSE
			//uhhh that means closestidx and secondclosestidx are equal
			RETURN closestidx
		ENDIF
	ENDIF
ENDFUNC

PROC PS_PREVIEW_RESET_CHALLENGE_AFTER_CUTSCENE(VECTOR startposition, FLOAT startheading)
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		STOP_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, startposition, FALSE)
		SET_ENTITY_HEADING(PS_Main.myVehicle, startheading)
		SET_VEHICLE_ON_GROUND_PROPERLY(PS_Main.myVehicle)
	ENDIF
ENDPROC

PROC PS_SETUP_PLAYER_DURING_CUTSCENE()
	IF NOT bPlayerInVehicle 
		IF g_current_selected_PilotSchool_class != PSC_chuteOntoMovingTarg 
			IF g_current_selected_PilotSchool_class != PSC_parachuteOntoTarget
				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 			
					IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),PS_Main.myVehicle)
						ENDIF
						SET_VEHICLE_ON_GROUND_PROPERLY(PS_Main.myVehicle)
						SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
						SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
						//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
						bPlayerInVehicle = TRUE
					ELSE
						PRINTLN("PS_SETUP_PLAYER_DURING_CUTSCENE: NOT IS_VEHICLE_DRIVEABLE")
					ENDIF
				ELSE
					PRINTLN("PS_SETUP_PLAYER_DURING_CUTSCENE: IS_PED_INJURED")
				ENDIF		
			ELSE
				PRINTLN("PS_SETUP_PLAYER_DURING_CUTSCENE: PSC_parachuteOntoTarget")
			ENDIF
		ELSE
			PRINTLN("PS_SETUP_PLAYER_DURING_CUTSCENE: PSC_chuteOntoMovingTarg")
		ENDIF
	ELSE
		PRINTLN("PS_SETUP_PLAYER_DURING_CUTSCENE: bPlayerInVehicle")
	ENDIF
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()	
ENDPROC

PROC PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
	//do additional hud stuff
	DISPLAY_RADAR(FALSE)	
	PS_SETUP_PLAYER_DURING_CUTSCENE()
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	// Set view cam for the helicopter and stunt plane to be set for the start of the lesson.
	//SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
	//SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, CAM_VIEW_MODE_THIRD_PERSON_FAR)
ENDPROC

PROC PS_PREVIEW_CLEANUP_CUTSCENE()
	IF DOES_ENTITY_EXIST(PS_FlatBedTruck)
		PS_PARACHUTE_CLEANUP_TRUCK_TARGET()
	ENDIF
	//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	IF DOES_ENTITY_EXIST(PS_Main.previewDummy)
		DELETE_PED(PS_Main.previewDummy)
	ENDIF
	IF DOES_CAM_EXIST(PS_Main.previewCam1)
		DESTROY_CAM(PS_Main.previewCam1)
	ENDIF
	IF DOES_CAM_EXIST(PS_Main.previewCam2)
		DESTROY_CAM(PS_Main.previewCam2)
	ENDIF
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	//do additional hud stuff
	DISPLAY_RADAR(TRUE)
	
	IF DOES_ENTITY_EXIST(PS_Main.prevPed)
		DELETE_PED(PS_Main.prevPed)
	ENDIF
	IF DOES_ENTITY_EXIST(PS_Main.previewDummy)
		DELETE_PED(PS_Main.previewDummy)
	ENDIF
	
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		STOP_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(PS_Main.myVehicle))
			CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_DEPLOY_INSTANT)
		ENDIF
		SET_ENTITY_COORDS_NO_OFFSET(PS_Main.myVehicle, vStartPosition)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
		SET_VEHICLE_ON_GROUND_PROPERLY(PS_Main.myVehicle)
		//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
		FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IS_SCRIPTED_CONVERSATION_ONGOING()
	ENDIF
	
	KILL_FACE_TO_FACE_CONVERSATION()
	PS_RESET_CHECKPOINT_MGR()
	PS_SETUP_PLAYER_DURING_CUTSCENE()
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
ENDPROC

/// PURPOSE:
///    This contains all of the flight ability stats we want the player to be set at after finishing the current lesson.
///    *note that the INITIAL flight ability stat is 10 less than the very first lesson. 
/// RETURNS:
///    
FUNC INT PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()	
	SWITCH(g_current_selected_PilotSchool_class)
		//initial = 20
		CASE PSC_Takeoff
			RETURN 30
		CASE PSC_Landing
			RETURN 40
		CASE PSC_Inverted
			RETURN 50
		CASE PSC_Knifing
			RETURN 60
		CASE PSC_loopTheLoop
			RETURN 70
		CASE PSC_FlyLow
			RETURN 75
		CASE PSC_DaringLanding
			RETURN 80
		CASE PSC_planeCourse
			RETURN 85
		CASE PSC_heliCourse
			RETURN 90
		CASE PSC_heliSpeedRun
			RETURN 95
		CASE PSC_parachuteOntoTarget
			RETURN 0
		CASE PSC_chuteOntoMovingTarg
			RETURN 0
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC PS_SET_FLIGHT_ABILITY_STAT()
	INT iStatValue
	SWITCH(g_current_selected_PilotSchool_class)
		CASE PSC_Takeoff
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 10 - PSC_Takeoff")
			BREAK
		CASE PSC_Landing
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 20 - PSC_Landing")
			BREAK
		CASE PSC_Inverted
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 30 - PSC_Inverted")
			BREAK
		CASE PSC_Knifing
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 40 - PSC_Knifing")
			BREAK
		CASE PSC_loopTheLoop
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 50 - PSC_loopTheLoop")
			BREAK
		CASE PSC_FlyLow
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 60 - PSC_FlyLow")
			BREAK
		CASE PSC_DaringLanding
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 70 - PSC_DaringLanding")
			BREAK
		CASE PSC_planeCourse
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 80 - PSC_planeCourse")
			BREAK
		CASE PSC_heliCourse
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 90 - PSC_heliCourse")
			BREAK
		CASE PSC_heliSpeedRun
			IF STAT_GET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), iStatValue)
				IF iStatValue < PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON()
					STAT_SET_INT(GET_SP_PLAYER_STAT_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY), PS_GET_FLIGHT_ABILITY_STAT_FOR_CURRENT_LESSON())
				ENDIF
			ENDIF
			PRINTLN("Setting Flying Ability Stat - 100 - PSC_heliSpeedRun")
			BREAK
		CASE PSC_parachuteOntoTarget
			PRINTLN("Setting Flying Ability Stat - x - PSC_parachuteOntoTarget")
			BREAK
		CASE PSC_chuteOntoMovingTarg
			PRINTLN("Setting Flying Ability Stat - x - PSC_chuteOntoMovingTarg")
			BREAK
	ENDSWITCH

ENDPROC

PROC PS_REGISTER_COMPLETION()
	SWITCH(g_current_selected_PilotSchool_class)
		CASE PSC_Takeoff
			DEBUG_MESSAGE("Need to register completion percentage - CP_OJ_PS0T - PSC_Takeoff")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS0T )
			BREAK
		CASE PSC_Landing
			DEBUG_MESSAGE("Need to register completion percentage - CP_OJ_PS0L - PSC_Landing")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS0L )
			BREAK
		CASE PSC_Inverted
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS1 - PSC_Inverted")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS1 )
			BREAK
		CASE PSC_Knifing
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS2 - PSC_Knifing")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS2 )
			BREAK
		CASE PSC_loopTheLoop
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS3 - PSC_loopTheLoop")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS3 )
			BREAK
		CASE PSC_FlyLow
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS4 - PSC_FlyLow")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS4 )
			BREAK
		CASE PSC_DaringLanding
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS5 - PSC_DaringLanding")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS5 )
			BREAK
		CASE PSC_planeCourse
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS6 - PSC_planeCourse")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS6 )
			BREAK
		CASE PSC_heliCourse
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS7 - PSC_heliCourse")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS7 )
			BREAK
		CASE PSC_heliSpeedRun
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS8 - PSC_heliSpeedRun")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS8 )
			BREAK
		CASE PSC_parachuteOntoTarget
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS9 - PSC_parachuteOntoTarget")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS9 )
			BREAK
		CASE PSC_chuteOntoMovingTarg
			DEBUG_MESSAGE("Registering completion percentage - CP_OJ_PS10 - PSC_chuteOntoMovingTarg")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_PS10 )
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_PLAY_SPECIFIC_LESSON_FAILED_LINE()
	TEXT_LABEL root = "PS_SPECF"
	TEXT_LABEL label = "PS_SPECF_"
	INT num = 1
	SWITCH ePSFailReason
		CASE PS_FAIL_PLAYER_LEFT_HELI
			num = 5 // "Were you expecting something with an ejection seat?"
			BREAK
		CASE PS_FAIL_PLAYER_LEFT_PLANE
			num = 5 // "Were you expecting something with an ejection seat?"
			BREAK
		CASE PS_FAIL_OUT_OF_RANGE
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				num = 2 //"Where the hell do you think you're going?"
			ELSE
				num = 3 //"There's nothing over there for you."
			ENDIF
			BREAK
		CASE PS_FAIL_LEFT_RUNWAY
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				num = 2 //"Where the hell do you think you're going?"
			ELSE
				num = 3 //"There's nothing over there for you."
			ENDIF
			BREAK
		CASE PS_FAIL_LANDED_VEHICLE
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 2 //"Where the hell do you think you're going?"
				ELSE
					num = 3 //"There's nothing over there for you."
				ENDIF
			ELSE
				num = 7 //"I believe that's a no parking zone."
			ENDIF
			BREAK
		CASE PS_FAIL_DAMAGED_VEHICLE
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 4 //"I don't think the crew is going to appreciate that."
				ELSE
					num = 6 //"Did you fall asleep in there?"
				ENDIF
			ELSE
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 9 //"That didn't look too graceful."
				ELSE
					num = 10 //"That was just... Wow."
				ENDIF
			ENDIF
			BREAK
		CASE PS_FAIL_MISSED_FIRST_GATE
			num = 8 //"Couldn't even make it past the first checkpoint?"
			BREAK
		CASE PS_FAIL_SUBMERGED
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 7 //"I believe that's a no parking zone."
				ELSE
					num = 6 //"Did you fall asleep in there?"
				ENDIF
			ELSE
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 9 //"That didn't look too graceful."
				ELSE
					num = 10 //"That was just... Wow."
				ENDIF
			ENDIF
			BREAK
		CASE PS_FAIL_STUCK_VEHICLE
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				num = 1 //"What in the hell was that?"
			ELSE
				num = 10 //"I believe that's a no parking zone."
			ENDIF
			
			BREAK
		CASE PS_FAIL_LANDED_IN_WATER
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 7 //"I believe that's a no parking zone."
				ELSE
					num = 6 //"Did you fall asleep in there?"
				ENDIF
			ELSE
				IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
					num = 9 //"That didn't look too graceful."
				ELSE
					num = 10 //"That was just... Wow."
				ENDIF
			ENDIF
			BREAK
		CASE PS_FAIL_IDLING
				num = 6 //"Did you fall asleep in there?"
			BREAK
		CASE PS_FAIL_COLLIDED_TRUCK
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				num = 9 //"That didn't look too graceful."
			ELSE
				num = 10 //"That was just... Wow."
			ENDIF
			BREAK
		CASE PS_FAIL_LANDING_GEAR
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				num = 9 //"That didn't look too graceful."
			ELSE
				num = 1 //"What in the hell was that?"
			ENDIF
			BREAK
		CASE PS_FAIL_REMOVED_PARACHUTE
			IF GET_RANDOM_INT_IN_RANGE() % 2 = 1
				num = 1 //"What in the hell was that?"
				
			ELSE
				num = 10 //"Not your finest hour."
			ENDIF
		BREAK
	ENDSWITCH
	label += num
	PRINTLN("Playing FAIL dialogue line SPECF_", num)
	PS_PLAY_DISPATCHER_INSTRUCTION(root, label)
	
ENDPROC

PROC PS_PLAY_RANDOM_GENERIC_LESSON_FAILED_LINE()
	TEXT_LABEL root = "PS_GENF"
	TEXT_LABEL label = "PS_GENF_"
	INT maxFailLines = 4
	INT num =	CLAMP_INT(GET_RANDOM_INT_IN_RANGE() % (maxFailLines + 1), 1, maxFailLines)
	//rand number between 1-4
	label += num
	
	PS_PLAY_DISPATCHER_INSTRUCTION(root, label)
	
ENDPROC

PROC PS_PLAY_LESSON_FINISHED_LINE(STRING sRoot, STRING sBronzeLabel, STRING sSilverLabel, STRING sGoldLabel, STRING sFailLabel1, STRING sFailLabel2 = NULL, STRING sFailLabel3 = NULL)
	IF iCurrentChallengeScore >= iSCORE_FOR_GOLD
		PS_PLAY_DISPATCHER_INSTRUCTION(sRoot, sGoldLabel)
	ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER
		PS_PLAY_DISPATCHER_INSTRUCTION(sRoot, sSilverLabel)
	ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
		PS_PLAY_DISPATCHER_INSTRUCTION(sRoot, sBronzeLabel)
	ELSE
		SWITCH ePSFailReason
			CASE PS_FAIL_PLAYER_DEAD
			CASE PS_FAIL_DAMAGED_VEHICLE
			CASE PS_FAIL_LANDED_IN_WATER
			CASE PS_FAIL_SUBMERGED
			CASE PS_FAIL_NO_REASON
				//don't play any conversations for these reasons as it is delaying the failed screen to appear, see B*2057016
				PRINTLN("Skipping lesson finished line from playing deliberately.")
			BREAK
			CASE PS_FAIL_PLAYER_LEFT_HELI
			CASE PS_FAIL_PLAYER_LEFT_PLANE
			CASE PS_FAIL_OUT_OF_RANGE
			CASE PS_FAIL_LEFT_RUNWAY
			CASE PS_FAIL_LANDED_VEHICLE
			//CASE PS_FAIL_DAMAGED_VEHICLE
			CASE PS_FAIL_MISSED_FIRST_GATE
			//CASE PS_FAIL_SUBMERGED
			CASE PS_FAIL_STUCK_VEHICLE
			//CASE PS_FAIL_LANDED_IN_WATER
			CASE PS_FAIL_IDLING
			CASE PS_FAIL_COLLIDED_TRUCK
			CASE PS_FAIL_LANDING_GEAR
			CASE PS_FAIL_REMOVED_PARACHUTE
				PS_PLAY_SPECIFIC_LESSON_FAILED_LINE()
				BREAK
			DEFAULT
				IF NOT IS_STRING_NULL_OR_EMPTY(sFailLabel3) AND iFailDialogue = 3
					PS_PLAY_DISPATCHER_INSTRUCTION(sRoot, sFailLabel3)
				ELIF NOT IS_STRING_NULL_OR_EMPTY(sFailLabel2) AND iFailDialogue = 2
					PS_PLAY_DISPATCHER_INSTRUCTION(sRoot, sFailLabel2)
				ELIF NOT IS_STRING_NULL_OR_EMPTY(sFailLabel1) AND iFailDialogue = 1
					PS_PLAY_DISPATCHER_INSTRUCTION(sRoot, sFailLabel1)
				ELSE
					PS_PLAY_RANDOM_GENERIC_LESSON_FAILED_LINE()
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
ENDPROC


PROC PS_SET_LEADERBOARD_WRITER_ACTIVE(BOOL thisBool)
	bIsPSLeaderboardWriting = thisBool
ENDPROC

FUNC BOOL PS_IS_LEADERBOARD_WRITER_ACTIVE()
	RETURN bIsPSLeaderboardWriting
ENDFUNC

PROC REINIT_PS_MENU_BUTTONS()	
	//TODO: add array overrun protection if necessary
	CLEANUP_SIMPLE_USE_CONTEXT(menuInstructions)
	IF (PS_Challenges[g_current_selected_PilotSchool_class].LockStatus = PSS_LOCKED)				
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_QUIT",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_PLAYER_ONLINE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = TRUE
		ELIF IS_PLAYER_ONLINE() AND NOT PS_IS_LEADERBOARD_WRITER_ACTIVE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = FALSE
		ELSE //player is online and writer is active
			bShowingOfflineLBButton = FALSE
		ENDIF
	ELSE		
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "FE_HLP4",			FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_QUIT",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_PLAYER_ONLINE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = TRUE
		ELIF IS_PLAYER_ONLINE() AND NOT PS_IS_LEADERBOARD_WRITER_ACTIVE()
			ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
			bShowingOfflineLBButton = FALSE
		ELSE //player is online and writer is active
			bShowingOfflineLBButton = FALSE
		ENDIF
	ENDIF
	
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(menuInstructions, TRUE)
ENDPROC

PROC PS_LESSON_END(BOOL bPassedLesson)
	
	IF Pilot_School_Data_Has_Fail_Reason()
		IF bPassedLesson = TRUE
			SCRIPT_ASSERT("Correcting false positive!")
		ENDIF
		bPassedLesson = FALSE
	ENDIF
	
	ODDJOB_STOP_SOUND(PS_SoundID_Formation_Alarm)
	IF bPassedLesson
		PS_REGISTER_COMPLETION()
//		PS_SET_FLIGHT_ABILITY_STAT() //taken care of outside of flight school
		//PLAY_SOUND_FRONTEND(-1, "FLIGHT_SCHOOL_LESSON_PASSED", "HUD_AWARDS")
	ELSE
		//PLAY_SOUND_FRONTEND(-1, "LOSER", "HUD_AWARDS")
		PS_RESET_CHECKPOINT_MGR()
	ENDIF
	
	SET_MAX_WANTED_LEVEL(0)
	
	IF IS_PED_INJURED(PLAYER_PED_ID()) OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	SET_PLAYERPAD_SHAKES_WHEN_CONTROLLER_DISABLED(FALSE)
	CLEAR_HELP()
	CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_1)
	CLEAR_OBJHELPALOGUE()
	PS_KILL_HINT_CAM()
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_SET_DIST_HUD_ACTIVE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_SET_CHECKPOINT_COUNTER_ACTIVE(FALSE)
	PS_HUD_SET_SCORECARD_ACTIVE(TRUE)
	PS_SET_ROLL_METER_ACTIVE(FALSE)
	PS_SET_LOOP_METER_ACTIVE(FALSE)
	PS_SET_IMMELMAN_METER_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_STOP_HOURGLASS_TIMER()
	PS_SET_ALTIMETER_ACTIVE(FALSE)
	DISPLAY_RADAR(FALSE)
	CANCEL_TIMER(Fail_Timer_Idling)
	
	#IF IS_DEBUG_BUILD
	IF NOT bPlayerUsedDebugSkip	
	#ENDIF
		IF bPassedLesson
			PS_SET_LEADERBOARD_WRITER_ACTIVE(TRUE)
			eLBLessonToWrite = g_current_selected_PilotSchool_class
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
ENDPROC

PROC PS_UPDATE_LEADERBOARD_WRITER()
	IF PS_IS_LEADERBOARD_WRITER_ACTIVE() AND IS_PLAYER_ONLINE()
		PRINTLN("TRYING TO DO LEABDERBAORD WRITE FOR FLIGHT SCHOOL!")
		PS_LOAD_SOCIAL_CLUB_LEADERBOARD(eLBLessonToWrite, PS_Challenges[eLBLessonToWrite].Title)
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[eLBLessonToWrite], FSG_0_timeTaken)
			PRINTLN("CALLING PS_DO_RANK_PREDICTION()")
			IF PS_DO_RANK_PREDICTION(eLBLessonToWrite, Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData), FLOOR(PS_Main.myPlayerData.elapsedTime * 1000.0))
				PS_SET_LEADERBOARD_WRITER_ACTIVE(FALSE)
				PRINTLN("Prediction finished, setting writer to false")
				IF eGameMode = PS_GAME_MODE_MENU AND !bShowQuitMenu AND !bLBToggle
					REINIT_PS_MENU_BUTTONS()
				ENDIF
			ENDIF
		ELSE
			PRINTLN("CALLING PS_DO_RANK_PREDICTION()")
			IF PS_DO_RANK_PREDICTION(eLBLessonToWrite, Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData), DEFAULT, PS_Main.myPlayerData.LandingDistance)
				PS_SET_LEADERBOARD_WRITER_ACTIVE(FALSE)
				PRINTLN("Prediction finished, setting writer to false")
				IF eGameMode = PS_GAME_MODE_MENU AND !bShowQuitMenu AND !bLBToggle
					PRINTLN("already on menu, re initialising menu buttons")
					REINIT_PS_MENU_BUTTONS()	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


