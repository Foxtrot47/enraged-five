

#IF IS_DEBUG_BUILD
USING "Pilot_School_HUD_lib.sch"
//USING "PS_Checkpoint_lib.sch"
USING "commands_pad.sch"


VECTOR vDebugCheckpoints[PS_MAX_CHECKPOINTS]
INT iNumDebugCheckpoints = 0
BOOL bDebugSaveWidgets = FALSE, bDebugShowAllCheckpoints = FALSE, bCheckpointUpdateRealtime = FALSE
WIDGET_GROUP_ID wObstacleCourseWidget

//stats logging
WIDGET_GROUP_ID wDataFile
BOOL bDebugClearDataFile = FALSE, bDebugLogDataFile = FALSE, bDebugCreatedHeader = FALSE

PROC DrawLiteralString(STRING sLiteral, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	//IF IS_KEYBOARD_KEY_PRESSED(KEY_L)
		INT red, green, blue, iAlpha
		GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
		SET_TEXT_SCALE(0.45, 0.45)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(iAlpha) * 0.5))
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.7795, 0.0305*TO_FLOAT(iColumn+1), "STRING", sLiteral)
	//ENDIF
ENDPROC

PROC DrawLiteralStringInt(STRING sLiteral, INT sInt, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	sNewLiteral += sInt
	
	DrawLiteralString(sNewLiteral, iColumn, eColour)
ENDPROC

PROC DrawLiteralStringFloat(STRING sLiteral, FLOAT sFloat, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	
	#IF IS_DEBUG_BUILD
	sNewLiteral += GET_STRING_FROM_FLOAT(sFloat)
	#ENDIF
	#IF IS_FINAL_BUILD
	sNewLiteral += ROUND(sFloat)
	#ENDIF
	
	DrawLiteralString(sNewLiteral, iColumn, eColour)
ENDPROC

#ENDIF


#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************

PROC PS_CLEAR_FLIGHT_SCHOOL_DATA_FILE()
	string sFilePath	= "X:/gta5/build/dev"
	string sFileName	= "flight_school_stats.txt"
	CLEAR_NAMED_DEBUG_FILE(sFilePath, sFileName)
ENDPROC

FUNC STRING PS_GET_FAIL_REASON_STRING()
	SWITCH(ePSFailReason)	
		CASE PS_FAIL_DOUBLE_REASON
			#IF IS_DEBUG_BUILD
				RETURN "Error! Multiple fail conditions triggered."
			#ENDIF
			BREAK	
		CASE PS_FAIL_NO_REASON
			#IF IS_DEBUG_BUILD
				RETURN "Failed for no reason."
			#ENDIF
			BREAK
		CASE PS_FAIL_TIME_UP
			RETURN "Ran out of time."
			BREAK
		CASE PS_FAIL_TOO_FAR
			RETURN "You landed too far away."
			BREAK
		CASE PS_FAIL_FORMATION
			RETURN "Didn't keep formation."
			BREAK
		CASE PS_FAIL_PLAYER_LEFT_HELI
			RETURN "Player abandoned helicopter."
			BREAK
		CASE PS_FAIL_PLAYER_LEFT_PLANE
			RETURN "Player abandoned plane."
			BREAK
		CASE PS_FAIL_OUT_OF_RANGE
			RETURN "Went out of range."
			BREAK
		CASE PS_FAIL_BEHIND_RACE
			RETURN "Too far behind."
			BREAK
		CASE PS_FAIL_LANDED_VEHICLE
			RETURN "Player landed vehicle."
			BREAK
		CASE PS_FAIL_DAMAGED_VEHICLE
			RETURN "Vehicle was wrecked."
			BREAK
		CASE PS_FAIL_MISSED_GATES
			RETURN "Player missed too many gates."
			BREAK
		CASE PS_FAIL_MISSED_FIRST_GATE
			RETURN "Player the first gate."
			BREAK
		CASE PS_FAIL_LEFT_RUNWAY
			RETURN "Player left runway."
			BREAK
		CASE PS_FAIL_SUBMERGED
			RETURN "Vehicle submerged in water."
			BREAK
		CASE PS_FAIL_STUCK_VEHICLE
			RETURN "Vehicle got stuck."
			BREAK
		CASE PS_FAIL_TOO_HIGH
			RETURN "Player was too high."
			BREAK
		CASE PS_FAIL_LANDED_IN_WATER
			RETURN "You landed in the water."
			BREAK
		CASE PS_FAIL_REMOVED_PARACHUTE
			RETURN "You removed the parachute."
			BREAK
		CASE PS_FAIL_IDLING
			RETURN "You were idling for too long."
			BREAK	
	ENDSWITCH
	
	RETURN PS_HUD_GET_FAIL_REASON_LABEL()
ENDFUNC

PROC PS_PRINT_LESSON_RECORD()
	
	//update tallies
	PS_Debug_Stats[PS_Main.myChallengeData.LessonEnum].lessonAttempts++
	
	IF bDebugLogDataFile
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "flight_school_stats.txt"
	
		OPEN_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("  ******************* Begin Record ", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("  ", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(PS_Main.myChallengeData.Title, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" - ", sFilePath, sFileName)
		IF iCurrentChallengeScore >= iSCORE_FOR_GOLD
			SAVE_STRING_TO_NAMED_DEBUG_FILE("GOLD", sFilePath, sFileName)
			PS_Debug_Stats[PS_Main.myChallengeData.LessonEnum].goldMedalsEarned++
		ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER
			SAVE_STRING_TO_NAMED_DEBUG_FILE("SILVER", sFilePath, sFileName)
			PS_Debug_Stats[PS_Main.myChallengeData.LessonEnum].silverMedalsEarned++
		ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
			PS_Debug_Stats[PS_Main.myChallengeData.LessonEnum].bronzeMedalsEarned++
			SAVE_STRING_TO_NAMED_DEBUG_FILE("BRONZE", sFilePath, sFileName)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("FAIL", sFilePath, sFileName)
		ENDIF	
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" - ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(PS_Main.myPlayerData.ElapsedTime), sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" - ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(PS_Main.myPlayerData.LandingDistance), sFilePath, sFileName)
		ENDIF

		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_3_checkpointsPassed)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" - ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(PS_Main.myPlayerData.CheckpointCount), sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("/", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(PS_Main.myChallengeData.TotalCheckpoints), sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" - ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(PS_Main.myPlayerData.FormationTimer), sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("/", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_FLOAT(PS_Main.myChallengeData.FormationTotal), sFilePath, sFileName)
		ENDIF
		
		IF iCurrentChallengeScore < iSCORE_FOR_BRONZE
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" - ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(PS_GET_FAIL_REASON_STRING(), sFilePath, sFileName)
		ENDIF
		
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("  ******************* End Record ", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC PS_PRINT_FLIGHT_SCHOOL_STATS()
	IF bDebugLogDataFile
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "flight_school_stats.txt"
		OPEN_NAMED_DEBUG_FILE(sFilePath, sFileName)
		
		//print stats
		//print lessons completed, medals completed, passes/attempts,
		//e.g.
		//Fly Low G-1 S-2 B-1 4/5
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("  *********************** SESSION STATS ***********************", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		
		INT i, medalSum
		REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES i
			SAVE_STRING_TO_NAMED_DEBUG_FILE("  ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(PS_Challenges[i].Title, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(": ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("G-", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(PS_Debug_Stats[i].goldMedalsEarned), sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("S-", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(PS_Debug_Stats[i].silverMedalsEarned), sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("B-", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(PS_Debug_Stats[i].bronzeMedalsEarned), sFilePath, sFileName)
			medalSum = PS_Debug_Stats[i].bronzeMedalsEarned + PS_Debug_Stats[i].silverMedalsEarned + PS_Debug_Stats[i].goldMedalsEarned
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(medalSum), sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("/", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_INT(PS_Debug_Stats[i].lessonAttempts), sFilePath, sFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		ENDREPEAT
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC PS_BEGIN_FLIGHT_SCHOOL_DATA_FILE()
	IF bDebugLogDataFile
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "flight_school_stats.txt"
		OPEN_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("*********************** FLIGHT SCHOOL SESSION ***********************", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		BOOL bPrintStats = FALSE
		INT i
		REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES i
			IF PS_Debug_Stats[i].lessonAttempts > 0
				bPrintStats = TRUE
			ENDIF
		ENDREPEAT
		IF bPrintStats
			PS_PRINT_FLIGHT_SCHOOL_STATS()
		ENDIF
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC PS_END_FLIGHT_SCHOOL_DATA_FILE()
	IF bDebugLogDataFile
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "flight_school_stats.txt"
		OPEN_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("*************************** END OF SESSION ***************************", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC PS_INIT_DATA_FILE_WIDGET()
	wDataFile = START_WIDGET_GROUP("Flight School Stats")
		ADD_WIDGET_BOOL("Log Flight School Stats", bDebugLogDataFile)
		ADD_WIDGET_BOOL("Clear Flight School Stats File", bDebugClearDataFile)
	STOP_WIDGET_GROUP()
ENDPROC

PROC PS_UPDATE_DATA_FILE_WIDGET()	
	IF bDebugClearDataFile
		PS_CLEAR_FLIGHT_SCHOOL_DATA_FILE()
		bDebugClearDataFile = FALSE
	ENDIF	
	IF bDebugLogDataFile AND NOT bDebugCreatedHeader
		PS_BEGIN_FLIGHT_SCHOOL_DATA_FILE()
		bDebugCreatedHeader = TRUE
	ELIF bDebugCreatedHeader AND NOT bDebugLogDataFile
		bDebugLogDataFile = TRUE
		PS_PRINT_FLIGHT_SCHOOL_STATS()
		PS_END_FLIGHT_SCHOOL_DATA_FILE()
		bDebugLogDataFile = FALSE
		bDebugCreatedHeader = FALSE
	ENDIF
ENDPROC

PROC PS_CLEANUP_DATA_FILE_WIDGET()
	DELETE_WIDGET_GROUP(wDataFile)
ENDPROC

PROC Set_Random_Values_For_PilotSchoolClass(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT& thisPlayerData)
	INT temprandint
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
		temprandint = GET_RANDOM_INT_IN_RANGE(-1, 2)
		IF temprandint = -1
			thisPlayerData.ElapsedTime = thisChallengeData.GoldTime
		ELIF temprandint = 0
			thisPlayerData.ElapsedTime = thisChallengeData.SilverTime
		ELIF temprandint = 1
			thisPlayerData.ElapsedTime = thisChallengeData.BronzeTime
		ENDIF
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
		temprandint = GET_RANDOM_INT_IN_RANGE(-1, 2)
		IF temprandint = -1
			thisPlayerData.LandingDistance = thisChallengeData.GoldDistance
		ELIF temprandint = 0
			thisPlayerData.LandingDistance = thisChallengeData.SilverDistance
		ELIF temprandint = 1
			thisPlayerData.LandingDistance = thisChallengeData.BronzeDistance
		ENDIF
	ENDIF

	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
		thisPlayerData.CheckpointCount = thisChallengeData.TotalCheckpoints	
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
		thisPlayerData.FormationTimer = thisChallengeData.FormationTotal
	ENDIF

ENDPROC

//******************************************************************************
//******************************************************************************
// Debug stuff for lessons
//******************************************************************************
//******************************************************************************

PROC PS_SET_PLAYER_DATA_GOLD_MEDAL()
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
		PS_Main.myPlayerData.ElapsedTime = PS_Main.myChallengeData.GoldTime
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
		PS_Main.myPlayerData.LandingDistance = PS_Main.myChallengeData.GoldDistance
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_3_checkpointsPassed)
		PS_Main.myPlayerData.CheckpointCount = PS_Main.myChallengeData.TotalCheckpoints
	ELSE
		PS_Main.myPlayerData.CheckpointCount = -1
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
		PS_Main.myPlayerData.FormationTimer = PS_Main.myChallengeData.FormationTotal
	ELSE
		PS_Main.myPlayerData.FormationTimer = -1
	ENDIF
ENDPROC

PROC PS_SET_PLAYER_DATA_SILVER_MEDAL()
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
		PS_Main.myPlayerData.ElapsedTime = PS_Main.myChallengeData.SilverTime
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
		PS_Main.myPlayerData.LandingDistance = PS_Main.myChallengeData.SilverDistance
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_3_checkpointsPassed)
		PS_Main.myPlayerData.CheckpointCount = ROUND(PS_Main.myChallengeData.TotalCheckpoints*0.85)
	ELSE
		PS_Main.myPlayerData.CheckpointCount = -1
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
		PS_Main.myPlayerData.FormationTimer = PS_Main.myChallengeData.FormationTotal*0.85
	ELSE
		PS_Main.myPlayerData.FormationTimer = -1
	ENDIF
ENDPROC

PROC PS_SET_PLAYER_DATA_BRONZE_MEDAL()
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
		PS_Main.myPlayerData.ElapsedTime = PS_Main.myChallengeData.SilverTime
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
		PS_Main.myPlayerData.LandingDistance = PS_Main.myChallengeData.BronzeDistance
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_3_checkpointsPassed)
		PS_Main.myPlayerData.CheckpointCount = ROUND(PS_Main.myChallengeData.TotalCheckpoints*0.75)
	ELSE
		PS_Main.myPlayerData.CheckpointCount = -1
	ENDIF
	
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
		PS_Main.myPlayerData.FormationTimer = PS_Main.myChallengeData.FormationTotal*0.75
	ELSE
		PS_Main.myPlayerData.FormationTimer = -1
	ENDIF
ENDPROC

PROC PS_FORCE_FAIL_LESSON()
	CALL PSC_DBG_FORCE_FAIL()
ENDPROC

PROC PS_FORCE_PASS_LESSON()
	CALL PSC_DBG_FORCE_PASS()
ENDPROC

PROC PS_DBG_LESSON_END(INT thisScore)
	
	bPlayerDataHasBeenUpdated = TRUE
	bPlayerUsedDebugSkip = TRUE
	iCurrentChallengeScore = thisScore
	PS_Main.myPlayerData.HasPassedLesson = TRUE
	PS_Main.myChallengeData.LockStatus = PSS_NEW
	SWITCH thisScore
		CASE iSCORE_FOR_GOLD
			PS_SET_PLAYER_DATA_GOLD_MEDAL()
			BREAK
		CASE iSCORE_FOR_SILVER
			PS_SET_PLAYER_DATA_SILVER_MEDAL()
			BREAK
		CASE iSCORE_FOR_BRONZE
			PS_SET_PLAYER_DATA_BRONZE_MEDAL()
			BREAK
	ENDSWITCH
	
	g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class] = PS_Main.myPlayerData
	
ENDPROC

/// PURPOSE:
///    Checks for debug input and updated any debug options that have been toggled
PROC PS_DEBUG_UPDATE()
	IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
		
		IF IS_DEBUG_KEY_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "pass gold")
			PS_DBG_LESSON_END(iSCORE_FOR_GOLD)			
			PS_FORCE_PASS_LESSON()
		ELIF IS_DEBUG_KEY_PRESSED(KEY_2, KEYBOARD_MODIFIER_SHIFT, "pass silver")
			PS_DBG_LESSON_END(iSCORE_FOR_SILVER)
			PS_FORCE_PASS_LESSON()
		ELIF IS_DEBUG_KEY_PRESSED(KEY_3, KEYBOARD_MODIFIER_SHIFT, "pass bronze")
			PS_DBG_LESSON_END(iSCORE_FOR_BRONZE)
			PS_FORCE_PASS_LESSON()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			PS_DBG_LESSON_END(iSCORE_FOR_BRONZE)
			PS_FORCE_PASS_LESSON()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			PS_FORCE_FAIL_LESSON()
		ENDIF
	ENDIF
ENDPROC

PROC PS_OBSTACLE_COURSE_CREATE_WIDGET(INT numCheckpoints)
	INT i = 0
	TEXT_LABEL sCheckpointName 
	
	iNumDebugCheckpoints = numCheckpoints
	
	START_WIDGET_GROUP("Flight School Obstacle Course")
		
		ADD_WIDGET_BOOL("Show all Checkpoints", bDebugShowAllCheckpoints)
		ADD_WIDGET_BOOL("Save Checkpoints", bDebugSaveWidgets)
		REPEAT numCheckpoints i
			IF i > 0
				sCheckpointName = "Checkpoint "
				sCheckpointName += GET_STRING_FROM_INT(i)
				vDebugCheckpoints[i] =  PS_Main.myCheckpointz[i].position //save off checkpoints 
				ADD_WIDGET_VECTOR_SLIDER(sCheckpointName, vDebugCheckpoints[i], -5000, 5000, 0.1)
			ENDIF
		ENDREPEAT
	
	STOP_WIDGET_GROUP()

ENDPROC

PROC PS_OBSTACLE_COURSE_SAVE_CHECKPOINTS()
	string sFilePath	= "X:/gta5/build/dev"
	string sFileName	= "temp_debug_checkpoints.txt"
	CLEAR_NAMED_DEBUG_FILE(sFilePath, sFileName)
	INT i = 0
	REPEAT iNumDebugCheckpoints i
		IF i > 0	
			SAVE_STRING_TO_NAMED_DEBUG_FILE("PS_REGISTER_CHECKPOINT(", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("PS_CHECKPOINT_OBSTACLE_HELI", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<<", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PS_Main.myCheckpointz[i].position.x, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PS_Main.myCheckpointz[i].position.y, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PS_Main.myCheckpointz[i].position.z, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(">>)", sFilePath, sFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC PS_OBSTACLE_COURSE_UPDATE_WIDGET()
	INT i = 0
	
	IF bDebugSaveWidgets
		bDebugSaveWidgets = FALSE
		PS_OBSTACLE_COURSE_SAVE_CHECKPOINTS()
	ENDIF
	
	IF bDebugShowAllCheckpoints
		 
		IF bCheckpointUpdateRealtime
			PS_HIDE_ALL_CHECKPOINTS()
			PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI)
		ELSE
			PS_SHOW_ALL_CHECKPOINTS()
		ENDIF
		
		bCheckpointUpdateRealtime = !bCheckpointUpdateRealtime
		
		bDebugShowAllCheckpoints = FALSE
		
	ENDIF
	
	IF bCheckpointUpdateRealtime
		REPEAT iNumDebugCheckpoints i
			IF i > 0
				IF PS_IS_CHECKPOINT_VALID(i) 
					IF NOT ARE_VECTORS_EQUAL(vDebugCheckpoints[i], PS_Main.myCheckpointz[i].position)
						PS_Main.myCheckpointz[i].position = vDebugCheckpoints[i]
						PS_HIDE_CHECKPOINT(i)
						PS_SHOW_CHECKPOINT(i)
					ENDIF					
					#IF PS_DRAW_DEBUG_LINES_AND_SPHERES 
					DRAW_DEBUG_SPHERE(PS_Main.myCheckpointz[i].position, 10)
					IF PS_IS_CHECKPOINT_VALID(i+1)
						DRAW_DEBUG_LINE(PS_Main.myCheckpointz[i].position, PS_Main.myCheckpointz[i+1].position)
					ENDIF
					#ENDIF
				ENDIF			
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC  

PROC PS_OBSTACLE_COURSE_DELETE_WIDGET()
	DELETE_WIDGET_GROUP(wObstacleCourseWidget)
ENDPROC

#ENDIF


