//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Main menu logic for pilot school. Loads in data to be shown in the menu, then allows 
//					the player to launch a challenge from the UI. Everything here should only deal with 
//					menu flow/logic.
//	
//****************************************************************************************************

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID PS_Widget
#ENDIF

USING "minigame_uiinputs.sch"
USING "flow_help_public.sch"
USING "Pilot_School_Challenge_Helpers.sch"
USING "Pilot_School_Definitions.sch"
USING "Pilot_School_Data.sch"
USING "PS_Menu_lib.sch"
#IF IS_DEBUG_BUILD
	USING "Pilot_School_Debug_lib.sch"
#ENDIF



PROC PS_MENU_INIT()

	DEBUG_MESSAGE("******************** INIT SCRIPT: Pilot_School_Menu.sch ********************")
	bLBToggle = FALSE
	CLEAR_HELP()
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	
	REQUEST_PILOT_SCHOOL_ASSETS()
	// TODO: REMOVE THIS TEMP TEXTURE DICTIONARY -TAYLOR
	REQUEST_STREAMED_TEXTURE_DICT("Shared")
	
	WHILE NOT HAS_LOADED_PILOT_SCHOOL_ASSETS()
		// TODO: REMOVE THIS TEMP TEXTURE DICTIONARY -TAYLOR
		OR NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Shared")
		PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE) //hack to make sure the hud is hidden EVERY frame.
		WAIT(0)
	ENDWHILE
	PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
	
	SET_MAX_WANTED_LEVEL(0)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		DELETE_VEHICLE(PS_Main.myVehicle)
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
//	SETUP_QUIT_UI(quitUI, "PS_QTITLE", "PS_QUIT")
	//SETUP_MINIGAME_INSTRUCTIONS(quitInstructions, FALSE, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "FE_HLP31", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "FE_HLP29")
//	INIT_SIMPLE_USE_CONTEXT(quitInstructions, FALSE, FALSE, FALSE, TRUE)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(quitInstructions, "FE_HLP32",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)

//	INIT_SIMPLE_USE_CONTEXT(quitInstructions, FALSE, FALSE, FALSE, TRUE)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(quitInstructions, "FE_HLP31",	FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(quitInstructions, "FE_HLP3",	FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//	ADD_SIMPLE_USE_CONTEXT_INPUT(quitInstructions, "FE_HLP29",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		
//	QUITUI_TRANSITION_OUT(quitUI, 0)
		
	bIsLaunchingChallenge = FALSE
	#IF IS_DEBUG_BUILD
		bIsLaunchingStuntTuner = FALSE
	#ENDIF	
		
	//setup placements
	INIT_PS_MENU(PS_UI_Placement)


ENDPROC

PROC PS_MENU_SETUP()
	
	PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
	PS_MENU_SETUP_CAMERA()	
	PS_MENU_LOAD_VARIABLES()
	PS_MENU_INIT() //need this here so can tell if menu entries are locked or not
	
	SET_CURSOR_POSITION(0.5, 0.5)

ENDPROC



FUNC BOOL PS_MENU_UPDATE()
	
	PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
	
	IF IS_USING_KEYBOARD_AND_MOUSE( FRONTEND_CONTROL)
		SET_MOUSE_CURSOR_THIS_FRAME()
	ENDIF
		
	IF bLBToggle
		IF IS_PLAYER_ONLINE()
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				bLBToggle = !bLBToggle
				PS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
				PS_MENU_SFX_PLAY_NAV_BACK()
				INIT_PS_MENU_BUTTONS()
			ENDIF
			IF NOT bLBViewProfile AND SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(psLBControl)
				bLBViewProfile = TRUE
				INIT_PS_LB_BUTTONS(bLBViewProfile)
			ENDIF
			UPDATE_SIMPLE_USE_CONTEXT(menuInstructions)
			PS_DISPLAY_SOCIAL_CLUB_LEADERBOARD(PS_UI_Leaderboard, g_current_selected_PilotSchool_class, PS_Challenges[g_current_selected_PilotSchool_class].Title)
		ELSE
			IF DO_SIGNED_OUT_WARNING(iBS)
				iBS = 0
				bLBToggle = !bLBToggle //leave lbs
				INIT_PS_MENU_BUTTONS()
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		IF bShowingOfflineLBButton AND IS_PLAYER_ONLINE()
			INIT_PS_MENU_BUTTONS()
			bShowingOfflineLBButton = FALSE
		ENDIF

		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) 
		AND (NOT IS_PLAYER_ONLINE() OR NOT PS_IS_LEADERBOARD_WRITER_ACTIVE()) 
		AND !bShowQuitMenu
			bLBToggle = !bLBToggle
			PS_MENU_SFX_PLAY_NAV_SELECT()
			bLBViewProfile = SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(psLBControl)
			INIT_PS_LB_BUTTONS(bLBViewProfile)
			RETURN TRUE
		ELIF PS_MENU_GET_INPUT(bIsLaunchingChallenge) //Keep going till we either push the button to Launch a challenge or Exit
			//are we transitioning, or is quit menu is fully on
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF bShowQuitMenu
				//if either, then display UI
				SET_WARNING_MESSAGE_WITH_HEADER("PS_QTITLE", "PS_QUIT", FE_WARNING_YES | FE_WARNING_NO)
			ELIF NOT IS_CAM_INTERPOLATING(camMainMenu)
				IF IS_CAM_ACTIVE(camMainMenu)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				PROCESS_SCREEN_PS_MENU(PS_UI_Placement)
			ENDIF

			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
	
	
ENDFUNC

PROC PS_MENU_CLEANUP()
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(PS_Widget)
			DELETE_WIDGET_GROUP(PS_Widget)
		ENDIF
	#ENDIF	
	
	//Launching or exiting?
	IF bIsLaunchingChallenge
		PS_Main.myChallengeData = PS_Challenges[g_current_selected_PilotSchool_class]
	ENDIF
	
	SET_MAX_WANTED_LEVEL(5)
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()

	CLEAR_HELP()
	
	CLEANUP_QUIT_UI(quitUI)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(PS_UI_Leaderboard)
	
	TRIGGER_SCREENBLUR_FADE_OUT(0)
	
	DISABLE_CELLPHONE(FALSE)
	KILL_FACE_TO_FACE_CONVERSATION()
	DEBUG_MESSAGE("******************** END SCRIPT: Pilot_School_Menu.sch ********************")
ENDPROC

