//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			11/29/11
//	Description:	Helper funcs for dealing with Objective text, Help text and Dialogue text for Flight School
//					
//	
//****************************************************************************************************


USING "timer_public.sch"
USING "dialogue_public.sch"
USING "hud_drawing.sch"


//****************************************************************************************************
//	OBJHELPALOGUE stuff for queuing up Objective/god text, help text and dialogue with delays
//****************************************************************************************************

CONST_INT		OBJHELPALOGUE_MAX_ITEMS		8

ENUM OBJHELPALOGUE_TYPE
	EMPTY_TEXT,
	HELP_TEXT,
	OBJ_TEXT,
	DIALOGUE_TEXT
ENDENUM

STRUCT OBJHELPALOGUE_ITEM
	OBJHELPALOGUE_TYPE	eType
	STRING sLabel1, sLabel2 
	FLOAT fDuration, fDelay
ENDSTRUCT

STRUCT OBJHELPALOGUE_QUEUE
	INT iNumQueued
	BOOL bIsRunning
	OBJHELPALOGUE_ITEM eQueue[OBJHELPALOGUE_MAX_ITEMS]
ENDSTRUCT

//here's our data! contains the main queue and a counter to keep track of items
OBJHELPALOGUE_QUEUE MyObjhelpalogueQueue


/// PURPOSE:
///    Initializes queue and timers for objhelpalogue
PROC INIT_OBJHELPALOGUE()
	
ENDPROC

/// PURPOSE:
///    Gets rid of unneeded data and stops update from running
PROC CLEANUP_OBJHELPALOGUE()
ENDPROC

FUNC BOOL IS_OBJHELPALOGUE_RUNNING()
	RETURN MyObjhelpalogueQueue.bIsRunning
ENDFUNC

/// PURPOSE:
///    Queues Help text to play thisDelay seconds after PLAY_OBJHELPALOGUE() is called
/// PARAMS:
///    thisTextLabel - Help text label
///    thisDuration - Duration (
///    thisDelay - Delay in seconds
PROC QUEUE_OBJHELPALOGUE_HELP(STRING thisTextLabel, FLOAT thisDuration, FLOAT thisDelay = 0.0)
	OBJHELPALOGUE_ITEM thisItem
	thisItem.eType = HELP_TEXT
	thisItem.sLabel1 = thisTextLabel
	thisItem.fDuration = thisDuration
	thisItem.fDelay = thisDelay
	MyObjhelpalogueQueue.eQueue[MyObjhelpalogueQueue.iNumQueued] = thisItem
	MyObjhelpalogueQueue.iNumQueued++
ENDPROC

/// PURPOSE:
///    Queues GOD/Objective text to play thisDelay seconds after PLAY_OBJHELPALOGUE() is called
/// PARAMS:
///    thisTextLabel - Obj/God text label
///    thisDelay - Delay in seconds
PROC QUEUE_OBJHELPALOGUE_OBJECTIVE(STRING thisTextLabel, FLOAT thisDuration, FLOAT thisDelay = 0.0)
	OBJHELPALOGUE_ITEM thisItem
	thisItem.eType = OBJ_TEXT
	thisItem.sLabel1 = thisTextLabel
	thisItem.fDuration = thisDuration
	thisItem.fDelay = thisDelay
	MyObjhelpalogueQueue.eQueue[MyObjhelpalogueQueue.iNumQueued] = thisItem
	MyObjhelpalogueQueue.iNumQueued++
ENDPROC

/// PURPOSE:
///    Queues Dialogue text to play thisDelay seconds after PLAY_OBJHELPALOGUE() is called
/// PARAMS:
///    thisRootLabel - Dialogue root
///    thisTextLabel - Dialogue label
///    thisDelay - Delay in seconds
PROC QUEUE_OBJHELPALOGUE_DIALOGUE(STRING thisTextLabel1, STRING thisTextLabel2 = NULL, FLOAT thisDelay = 0.0)
	OBJHELPALOGUE_ITEM thisItem
	thisItem.eType = DIALOGUE_TEXT
	thisItem.sLabel1 = thisTextLabel1
	thisItem.sLabel2 = thisTextLabel2
	thisItem.fDuration = 0
	thisItem.fDelay = thisDelay
	MyObjhelpalogueQueue.eQueue[MyObjhelpalogueQueue.iNumQueued] = thisItem
	MyObjhelpalogueQueue.iNumQueued++
ENDPROC

/// PURPOSE:
///    Gets rid of empty slots and updates the counter iNumQueued. Technically doesn't sort, just makes space for new queue slots
PROC SORT_OBJHELPALOGUE_QUEUE()
	INT i = 0 //id pointer that checks for empty slots
	INT j = 0 // when we find an emtpy slot, j is the id pointer to grab all the trailing objects in the queue
	INT tempIndex
	REPEAT MyObjhelpalogueQueue.iNumQueued i
		IF MyObjhelpalogueQueue.eQueue[i].eType = EMPTY_TEXT //if we found an empty slot
			tempIndex = i
			j = 0
			REPEAT MyObjhelpalogueQueue.iNumQueued j //move everything one slot up
				IF j > tempIndex
					MyObjhelpalogueQueue.eQueue[tempIndex] = MyObjhelpalogueQueue.eQueue[j]
					tempIndex++
				ENDIF
			ENDREPEAT
			MyObjhelpalogueQueue.iNumQueued-- //if we found an empty slot, then reduce the counter iNumQueued
		ENDIF
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Pops an OBHELPALOGUE item off the queue
/// PARAMS:
///    thisTextLabel - 
PROC REMOVE_OBJHELPALOGUE_ITEM_FROM_QUEUE(STRING thisTextLabel)
	INT i = 0
	REPEAT MyObjhelpalogueQueue.iNumQueued i
		
		IF (NOT IS_STRING_NULL_OR_EMPTY(MyObjhelpalogueQueue.eQueue[i].sLabel1) AND ARE_STRINGS_EQUAL(MyObjhelpalogueQueue.eQueue[i].sLabel1, thisTextLabel)) OR (NOT IS_STRING_NULL_OR_EMPTY(MyObjhelpalogueQueue.eQueue[i].sLabel2) AND ARE_STRINGS_EQUAL(MyObjhelpalogueQueue.eQueue[i].sLabel2, thisTextLabel))
			//clear out data and re-sort
			MyObjhelpalogueQueue.eQueue[i].eType = EMPTY_TEXT
			MyObjhelpalogueQueue.eQueue[i].sLabel1 = NULL
			MyObjhelpalogueQueue.eQueue[i].sLabel2 = NULL
			MyObjhelpalogueQueue.eQueue[i].fDuration = 0
			MyObjhelpalogueQueue.eQueue[i].fDelay = 0
			MyObjhelpalogueQueue.iNumQueued-- 
			EXIT
		ENDIF
	ENDREPEAT
	//find item with thisTextLabel and pop it from the queue
	//we should manually sort the queue whenever removing files to keep the counter iNumQueued updated
ENDPROC

/// PURPOSE:
///    Waits for PLAY_OBJHELPALOGUE() to be called, then displays queued text according to delays that have been set
PROC UPDATE_OBJHELPALOGUE()
	IF MyObjhelpalogueQueue.bIsRunning
////////////Update all the timers
//////////	
		IF MyObjhelpalogueQueue.iNumQueued = 0 //if we're out items, then stop the objhelpalogue beast from running
			MyObjhelpalogueQueue.bIsRunning = FALSE
			EXIT
		ENDIF
		
		FLOAT frameTime = GET_FRAME_TIME()
		INT i = 0
		
		REPEAT MyObjhelpalogueQueue.iNumQueued i
		
			//take care of stuff that is being shown
			
			IF MyObjhelpalogueQueue.eQueue[i].fDelay = 0 AND MyObjhelpalogueQueue.eQueue[i].fDuration > 0
				//decrement duration timer, if its < 0, then zero it out.
				MyObjhelpalogueQueue.eQueue[i].fDuration -= frameTime
				
				IF MyObjhelpalogueQueue.eQueue[i].fDuration <= 0
					MyObjhelpalogueQueue.eQueue[i].fDuration = 0
					IF MyObjhelpalogueQueue.eQueue[i].eType = HELP_TEXT
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(MyObjhelpalogueQueue.eQueue[i].sLabel1)
							CLEAR_HELP(FALSE)
						ENDIF
					ELIF MyObjhelpalogueQueue.eQueue[i].eType = OBJ_TEXT
						CLEAR_THIS_PRINT(MyObjhelpalogueQueue.eQueue[i].sLabel1)
					ENDIF
				ENDIF
			ENDIF
			
			//take care of stuff that is queued to be shown
			
			IF MyObjhelpalogueQueue.eQueue[i].fDelay > 0
				//decrement delay timer, if its < 0, then zero it out.
				MyObjhelpalogueQueue.eQueue[i].fDelay -= frameTime
				IF MyObjhelpalogueQueue.eQueue[i].fDelay <= 0
					MyObjhelpalogueQueue.eQueue[i].fDelay = 0
					IF MyObjhelpalogueQueue.eQueue[i].eType = HELP_TEXT
						CLEAR_HELP() //clear previous help
						PRINT_HELP_FOREVER(MyObjhelpalogueQueue.eQueue[i].sLabel1)
						IF MyObjhelpalogueQueue.eQueue[i].fDuration = -1
							MyObjhelpalogueQueue.eQueue[i].fDuration = TO_FLOAT(DEFAULT_GOD_TEXT_TIME/1000)
						ELIF MyObjhelpalogueQueue.eQueue[i].fDuration = -2
							//do nothing!
						ENDIF
					ELIF MyObjhelpalogueQueue.eQueue[i].eType = OBJ_TEXT
						IF MyObjhelpalogueQueue.eQueue[i].fDuration = -2
							PRINT(MyObjhelpalogueQueue.eQueue[i].sLabel1, DEFAULT_GOD_TEXT_TIME, 1)
							MyObjhelpalogueQueue.eQueue[i].fDuration = TO_FLOAT(DEFAULT_GOD_TEXT_TIME/1000)
						ELSE
							PRINT(MyObjhelpalogueQueue.eQueue[i].sLabel1, CEIL(MyObjhelpalogueQueue.eQueue[i].fDuration), 1)
						ENDIF
					ELIF MyObjhelpalogueQueue.eQueue[i].eType = DIALOGUE_TEXT
						//play dialogue line!
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
//////////
////////////^finished updating timers
		
		//Clean out the queue
		REPEAT MyObjhelpalogueQueue.iNumQueued i
			IF MyObjhelpalogueQueue.eQueue[i].fDuration = 0
				REMOVE_OBJHELPALOGUE_ITEM_FROM_QUEUE(MyObjhelpalogueQueue.eQueue[i].sLabel1)
			ENDIF
		ENDREPEAT
		
		SORT_OBJHELPALOGUE_QUEUE()
	
	ENDIF
	//only update if bIsRunning
	//decrement all of the delays > 0 
	//if the delay is = or < 0, then display
	//decrement all of the currently playing texts durations that are >= 0
	//if the duration reaches 0, then pop it off the queue
	//if the duration is >0, make sure the shit is still playing.
ENDPROC

/// PURPOSE:
///    Kicks off the OBJHELPALOGUE timers and starts playing queued texts
PROC PLAY_OBJHELPALOGUE()
	MyObjhelpalogueQueue.bIsRunning = TRUE
ENDPROC

/// PURPOSE:
///    Stops queued texts from being played and cancels timers
PROC STOP_OBJHELPALOGUE()
	MyObjhelpalogueQueue.bIsRunning = TRUE
	CLEAR_HELP()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION()
ENDPROC

/// PURPOSE:
///    Clears out the queue
PROC CLEAR_OBJHELPALOGUE()
	INT i = 0
	REPEAT MyObjhelpalogueQueue.iNumQueued i
		REMOVE_OBJHELPALOGUE_ITEM_FROM_QUEUE(MyObjhelpalogueQueue.eQueue[i].sLabel1)
	ENDREPEAT
ENDPROC



//****************************************************************************************************
//	Custom dialogue for preview stuff 
//****************************************************************************************************

STRUCT PREVIEW_DIALOGUE_DATA
	structPedsForConversation PreviewDialoguePed
	STRING sLabel1, sLabel2, sLabel3, sLabel4, sLabel5, sCurLabel
	TEXT_PLACEMENT txtPlacement
	TEXT_STYLE txtStyle
	
  ENDSTRUCT


PREVIEW_DIALOGUE_DATA	myPreviewDialogueStruct

/// PURPOSE:
///    Setup preview dialogue so god text subtitles are overridden
PROC PS_PREVIEW_DIALOGUE_SETUP(FLOAT thisPosX, FLOAT thisPosY, FLOAT wrapStartX = 0.0, FLOAT wrapEndX = 0.0)
	ADD_PED_FOR_DIALOGUE(myPreviewDialogueStruct.PreviewDialoguePed, 0, NULL, "pilotDispatch")
	myPreviewDialogueStruct.sLabel1 = NULL
	myPreviewDialogueStruct.sLabel2 = NULL
	myPreviewDialogueStruct.sLabel3 = NULL
	myPreviewDialogueStruct.sLabel4 = NULL
	myPreviewDialogueStruct.sLabel5 = NULL
	myPreviewDialogueStruct.txtPlacement.x = thisPosX
	myPreviewDialogueStruct.txtPlacement.y = thisPosY
	
	myPreviewDialogueStruct.txtStyle.aFont = FONT_STANDARD
	myPreviewDialogueStruct.txtStyle.XScale = 0.202 
	myPreviewDialogueStruct.txtStyle.YScale = 0.409
	myPreviewDialogueStruct.txtStyle.r = 255
	myPreviewDialogueStruct.txtStyle.g = 255
	myPreviewDialogueStruct.txtStyle.b = 255
	myPreviewDialogueStruct.txtStyle.a = 255
	myPreviewDialogueStruct.txtStyle.drop = DROPSTYLE_NONE
	myPreviewDialogueStruct.txtStyle.wrapStartX = wrapStartX
	myPreviewDialogueStruct.txtStyle.wrapEndX = wrapEndX
	myPreviewDialogueStruct.txtStyle.aTextType = TEXTTYPE_TS_STANDARDMEDIUM

	//Get position to display text at and store it
	//Get font size, style and store it
ENDPROC

/// PURPOSE:
///    Releases data and does any necessary cleanup
PROC PS_PREVIEW_DIALOGUE_CLEANUP()
	REMOVE_PED_FOR_DIALOGUE(myPreviewDialogueStruct.PreviewDialoguePed, 0)
ENDPROC

/// PURPOSE:
///    Play up to 3 lines of dialogue for preview video with custom subtitles
PROC PS_PREVIEW_DIALOGUE_PLAY(STRING thisRoot, STRING thisTextLabel1, STRING thisTextLabel2 = NULL, STRING thisTextLabel3 = NULL, STRING thisTextLabel4 = NULL, STRING thisTextLabel5 = NULL)
	thisTextLabel1 = thisTextLabel1
	thisTextLabel2 = thisTextLabel2
	thisTextLabel3 = thisTextLabel3
	
	IF IS_STRING_NULL(thisTextLabel1) //if 1st label is null, something is wrong
		SCRIPT_ASSERT("looks like the script is trying to play a dialogue line but its NULL")
	ELIF IS_STRING_NULL(thisTextLabel2) //if 2nd line is null, we're only playing 1 line
		myPreviewDialogueStruct.sLabel1 = thisTextLabel1
		myPreviewDialogueStruct.sCurLabel = thisTextLabel1
	ELIF IS_STRING_NULL(thisTextLabel3) //if only the 3rd line is null, we're playing 2 lines
		myPreviewDialogueStruct.sLabel1 = thisTextLabel1
		myPreviewDialogueStruct.sLabel2 = thisTextLabel2
		myPreviewDialogueStruct.sCurLabel = thisTextLabel1
	ELIF IS_STRING_NULL(thisTextLabel4) //if only the 4th line is null, we're playing 3 lines
		myPreviewDialogueStruct.sLabel1 = thisTextLabel1
		myPreviewDialogueStruct.sLabel2 = thisTextLabel2
		myPreviewDialogueStruct.sLabel3 = thisTextLabel3
		myPreviewDialogueStruct.sCurLabel = thisTextLabel1
	ELIF IS_STRING_NULL(thisTextLabel5) //if only the 5th line is null, we're playing 4 lines
		myPreviewDialogueStruct.sLabel1 = thisTextLabel1
		myPreviewDialogueStruct.sLabel2 = thisTextLabel2
		myPreviewDialogueStruct.sLabel3 = thisTextLabel3
		myPreviewDialogueStruct.sLabel4 = thisTextLabel4
		myPreviewDialogueStruct.sCurLabel = thisTextLabel1
	ELSE //otherwise, we're playing 3 lines
		myPreviewDialogueStruct.sLabel1 = thisTextLabel1
		myPreviewDialogueStruct.sLabel2 = thisTextLabel2
		myPreviewDialogueStruct.sLabel3 = thisTextLabel3
		myPreviewDialogueStruct.sLabel4 = thisTextLabel4
		myPreviewDialogueStruct.sLabel5 = thisTextLabel5
		myPreviewDialogueStruct.sCurLabel = thisTextLabel1
	ENDIF
	CREATE_CONVERSATION(myPreviewDialogueStruct.PreviewDialoguePed, "MGPSAUD", thisRoot, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
	
ENDPROC

PROC PS_DRAW_PREVIEW_DIALOGUE_TEXT(TEXT_PLACEMENT thisPlacement, TEXT_STYLE thisStyle, STRING thisText)
	SET_TEXT_STYLE(thisStyle)	
	SET_TEXT_RIGHT_JUSTIFY(FALSE)
	SET_TEXT_CENTRE(FALSE)	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(thisText)
	END_TEXT_COMMAND_DISPLAY_TEXT(thisPlacement.x, thisPlacement.y)
ENDPROC

/// PURPOSE:
///    Makes sure the correct subtitles are displaying with the dialogue
PROC PS_PREVIEW_DIALOGUE_UPDATE()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IS_SCRIPTED_CONVERSATION_LOADED()
		TEXT_LABEL_31 tempCurLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		IF NOT IS_STRING_NULL(tempCurLabel)	//check to make sure we're looking at a valid label
		
			IF NOT ARE_STRINGS_EQUAL(myPreviewDialogueStruct.sCurLabel, tempCurLabel) //if the curLabel is NOT whats currently playing, then update it
				IF ARE_STRINGS_EQUAL(tempCurLabel, myPreviewDialogueStruct.sLabel1)
					//clear subtitle
					myPreviewDialogueStruct.sCurLabel = myPreviewDialogueStruct.sLabel1
					//print the next subtitle
				ELIF NOT IS_STRING_NULL(myPreviewDialogueStruct.sLabel2) AND ARE_STRINGS_EQUAL(tempCurLabel, myPreviewDialogueStruct.sLabel2)
					//clear subtitle
					myPreviewDialogueStruct.sCurLabel = myPreviewDialogueStruct.sLabel2
					//print the next subtitle
				ELIF NOT IS_STRING_NULL(myPreviewDialogueStruct.sLabel3) AND ARE_STRINGS_EQUAL(tempCurLabel, myPreviewDialogueStruct.sLabel3)
					//clear subtitle
					myPreviewDialogueStruct.sCurLabel = myPreviewDialogueStruct.sLabel3
					//print the next subtitle
				ELIF NOT IS_STRING_NULL(myPreviewDialogueStruct.sLabel4) AND ARE_STRINGS_EQUAL(tempCurLabel, myPreviewDialogueStruct.sLabel4)
					//clear subtitle
					myPreviewDialogueStruct.sCurLabel = myPreviewDialogueStruct.sLabel4
				ELIF NOT IS_STRING_NULL(myPreviewDialogueStruct.sLabel5) AND ARE_STRINGS_EQUAL(tempCurLabel, myPreviewDialogueStruct.sLabel5)
					//clear subtitle
					myPreviewDialogueStruct.sCurLabel = myPreviewDialogueStruct.sLabel5
				ELIF IS_STRING_NULL(tempCurLabel)
					//should we do something here?
				ELSE
					//should we do something here?
				ENDIF
			ENDIF
			
			DEBUG_MESSAGE("++++++++++++++++++++ label that is current playing is ", tempCurLabel)
			DEBUG_MESSAGE("++++++++++++++++++++ label that is currently displaying is ", myPreviewDialogueStruct.sCurLabel)
			//draw whatever we're currently playing
			PS_DRAW_PREVIEW_DIALOGUE_TEXT(myPreviewDialogueStruct.txtPlacement, myPreviewDialogueStruct.txtStyle, myPreviewDialogueStruct.sCurLabel)
		
		ENDIF
		
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Stops dialogue
PROC PS_PREVIEW_DIALOGUE_STOP()
	myPreviewDialogueStruct.sLabel1 = NULL
	myPreviewDialogueStruct.sLabel2 = NULL
	myPreviewDialogueStruct.sLabel3 = NULL
	myPreviewDialogueStruct.sLabel4 = NULL
	myPreviewDialogueStruct.sLabel5 = NULL
	myPreviewDialogueStruct.sCurLabel = NULL
ENDPROC

