USING "commands_stats.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"

ENUM PS_LB_TYPE
	PS_LB_TIME,
	PS_LB_DISTANCE
ENDENUM

PS_LB_TYPE myLBType
BOOL							bIsPSLeaderboardWriting					= FALSE
SC_LEADERBOARD_CONTROL_STRUCT psLBControl
INT iBS = 0

PROC PS_WRITE_DATA_TO_LEADERBOARD(PILOT_SCHOOL_CLASSES_ENUM eLesson, INT iScore, INT iTime = 0, FLOAT fDistance = 0.0)
	PRINTLN("PS_WRITE_TIME_TO_LEADERBOARD: Writing leaderboards for ", 
		PICK_STRING(eLesson = PSC_Takeoff, "Takeoff", 
		PICK_STRING(eLesson = PSC_Landing, "Landing", 
		PICK_STRING(eLesson = PSC_Inverted, "Inverted",
		PICK_STRING(eLesson = PSC_Knifing, "Knifing",
		PICK_STRING(eLesson = PSC_loopTheLoop, "Loop the Loop",
		PICK_STRING(eLesson = PSC_FlyLow, "Fly Low",
		PICK_STRING(eLesson = PSC_DaringLanding, "Daring Landing",
		PICK_STRING(eLesson = PSC_heliCourse, "Heli Obstacle Course",
		PICK_STRING(eLesson = PSC_heliSpeedRun, "Heli Speed Run",
		PICK_STRING(eLesson = PSC_parachuteOntoTarget, "Skydiving",
		PICK_STRING(eLesson = PSC_chuteOntoMovingTarg, "Skydiving - Moving Target",
		PICK_STRING(eLesson = PSC_planeCourse, "Plane Obstacle Course", "Invalid Enum")))))))))))))
	
	IF iTime < 0
		iTime = 0
	ENDIF
	
	//if we have a really big distance, reduce it to 999,999
	IF fDistance < 0 OR fDistance > 999999
		fDistance = 999999
	ENDIF

	
	INT numBronzeMedals = 0, numSilverMedals = 0, numGoldMedals = 0
		
//	PRINTLN("iScore to send to LBs is... ", iScore)
	PRINTLN("iTime to send to LBs is... ", iTime)
	PRINTLN("fDistance to send to LBs is... ", fDistance)
	
	IF iScore >= iSCORE_FOR_GOLD
		numGoldMedals++
	ELIF iScore >= iSCORE_FOR_SILVER
		numSilverMedals++
	ELIF iScore >= iSCORE_FOR_BRONZE
		numBronzeMedals++
	ENDIF
	
	// NOW PASS THE DATA IN FOR SUBMISSION
	TEXT_LABEL_23 sIdentifier[1]
	TEXT_LABEL_31 categoryName[1]
	categoryName[0] = "Location"  //not sure it needed this??
	// Begin setting the leaderboard data types
	SWITCH eLesson
		CASE PSC_Takeoff //Takeoff
			myLBType = PS_LB_TIME
		 	sIdentifier[0] = "LESSON_01"
			BREAK
		CASE PSC_Landing //Landing
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_02"
			BREAK
		CASE PSC_Inverted //Inverted
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_03"
			BREAK
		CASE PSC_Knifing //Knifing
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_04"
			BREAK
		CASE PSC_DaringLanding //Daring Landing
			myLBType = PS_LB_DISTANCE
			sIdentifier[0]  = "LESSON_05"
			BREAK
		CASE PSC_FlyLow //Fly Low
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_06"
			BREAK
		CASE PSC_loopTheLoop//Loop the Loop
			myLBType = PS_LB_TIME 
			sIdentifier[0]  = "LESSON_07"
			BREAK
		CASE PSC_heliCourse //Heli Obstacle Course
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_08"
			BREAK
		CASE PSC_heliSpeedRun //Heli Speed Run
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_09"
			BREAK
		CASE PSC_parachuteOntoTarget //Skydiving
			myLBType = PS_LB_DISTANCE
			sIdentifier[0]  = "LESSON_10"
			BREAK
		CASE PSC_chuteOntoMovingTarg //Skydiving - Moving Target
			myLBType = PS_LB_DISTANCE
			sIdentifier[0]  = "LESSON_11"
			BREAK
		CASE PSC_planeCourse //Plane Obstacle Course
			myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_12"
			BREAK
	ENDSWITCH
	
	SWITCH(myLBType)
		CASE PS_LB_TIME
		
			IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME, sIdentifier, categoryName, 1, DEFAULT, TRUE)
				//
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_SCORE, -iTime, TO_FLOAT(-iTime))
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, -iTime, TO_FLOAT(-iTime)) 
				//LEADERBOARDS_WRITE_SET_INT(0, iScore)
				//Time in MS 
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_BEST_TIME, iTime, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, iTime, 0)
				//LEADERBOARDS_WRITE_SET_INT(1, iTime)
				//write accuracy
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DISTANCE, 0, 0)
//				// This is a Bullsh*t number just to test upload of data.
//				//LEADERBOARDS_WRITE_SET_INT(2, 50)
//				
				//write 1 if player got a bronze
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)
				//LEADERBOARDS_WRITE_SET_INT(3, 0)
				
				//write 1 if player got a silver
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
				//LEADERBOARDS_WRITE_SET_INT(4, 0)
				//writing 1 if gold medal
				
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
				//LEADERBOARDS_WRITE_SET_INT(5, 1)
				
				//write 1 for each attempt
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_NUM_MATCHES, 1, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 1, 0)
				//LEADERBOARDS_WRITE_SET_INT(6, 1)
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME), LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)
			ENDIF
			BREAK
		
		CASE PS_LB_DISTANCE
		
			IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST, sIdentifier, categoryName, 1, DEFAULT, TRUE)
				//)
				PRINTLN("fDistance is.... ", fDistance)
				PRINTLN("Calling LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE_DOUBLE, 0, ", -fDistance)
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_SCORE_DOUBLE, 0, -fDistance)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE_DOUBLE, 0, -fDistance)
				//LEADERBOARDS_WRITE_SET_INT(0, iScore)
				//Time in MS 
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_BEST_TIME, 0, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, 0, 0)
				//LEADERBOARDS_WRITE_SET_INT(1, iTime)
				//write accuracy
				PRINTLN("Calling LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DISTANCE, 0, ", fDistance)
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_DISTANCE, 0,  fDistance)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DISTANCE, 0,  fDistance)
				// This is a Bullsh*t number just to test upload of data.
				//LEADERBOARDS_WRITE_SET_INT(2, 50)
				
				//write 1 if player got a bronze
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)
				//LEADERBOARDS_WRITE_SET_INT(3, 0)
				
				//write 1 if player got a silver
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)
				//LEADERBOARDS_WRITE_SET_INT(4, 0)
				//writing 1 if gold medal
				
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)
				//LEADERBOARDS_WRITE_SET_INT(5, 1)
				
				//write 1 for each attempt
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_NUM_MATCHES, 1, 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 1, 0)
				//LEADERBOARDS_WRITE_SET_INT(6, 1)
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST), LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)
			ENDIF
			BREAK
		
	ENDSWITCH
ENDPROC

//Name: MINI_GAMES_FLIGHT_SCHOOL_BY_DIST
//ID: 192 - LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST
//Inputs: 8
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_BEST_TIME (long)
//	LB_INPUT_COL_DISTANCE (int)
//	LB_INPUT_COL_BRONZE_MEDALS (int)
//	LB_INPUT_COL_SILVER_MEDALS (int)
//	LB_INPUT_COL_GOLD_MEDALS (int)
//	LB_INPUT_COL_NUM_MATCHES (int)
//	LB_INPUT_COL_MEDAL (int)
//Columns: 8
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	BEST_TIME ( AGG_Min ) - InputId: LB_INPUT_COL_BEST_TIME
//	BEST_LANDING_ACC ( AGG_Max ) - InputId: LB_INPUT_COL_DISTANCE
//	NUM_BRONZE_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_BRONZE_MEDALS
//	NUM_SILVER_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_SILVER_MEDALS
//	NUM_GOLD_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_GOLD_MEDALS
//	NUM_ATTEMPTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_MATCHES
//	MEDAL ( AGG_Last ) - InputId: LB_INPUT_COL_MEDAL
//Instances: 3 
//	Location
//	Location,Challenge
//	Location,ScEvent
//
//-----------------------------------------------------
//Name: MINI_GAMES_FLIGHT_SCHOOL_BY_TIME
//ID: 850 - LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME
//Inputs: 8
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_BEST_TIME (long)
//	LB_INPUT_COL_DISTANCE (int)
//	LB_INPUT_COL_BRONZE_MEDALS (int)
//	LB_INPUT_COL_SILVER_MEDALS (int)
//	LB_INPUT_COL_GOLD_MEDALS (int)
//	LB_INPUT_COL_NUM_MATCHES (int)
//	LB_INPUT_COL_MEDAL (int)
//Columns: 8
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	BEST_TIME ( AGG_Min ) - InputId: LB_INPUT_COL_BEST_TIME
//	BEST_LANDING_ACC ( AGG_Max ) - InputId: LB_INPUT_COL_LAND_ACCURACY
//	NUM_BRONZE_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_BRONZE_MEDALS
//	NUM_SILVER_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_SILVER_MEDALS
//	NUM_GOLD_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_GOLD_MEDALS
//	NUM_ATTEMPTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_MATCHES
//	MEDAL ( AGG_Last ) - InputId: LB_INPUT_COL_MEDAL
//Instances: 3
//	Location
//	Location,Challenge
//	Location,ScEvent
//



PROC PS_LOAD_SOCIAL_CLUB_LEADERBOARD(PILOT_SCHOOL_CLASSES_ENUM eLesson, STRING sTitle)
	TEXT_LABEL_23 sUniqueIdentifier
	INT fmmcType
	SWITCH eLesson
		CASE PSC_Takeoff //Takeoff
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
		 	sUniqueIdentifier = "LESSON_01"
			sTitle = "PSLBT_01"
			BREAK
		CASE PSC_Landing //Landing
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_02"
			sTitle = "PSLBT_02"
			BREAK
		CASE PSC_Inverted //Inverted
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_03"
			sTitle = "PSLBT_03"
			BREAK
		CASE PSC_Knifing //Knifing
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_04"
			sTitle = "PSLBT_04"
			BREAK
		CASE PSC_DaringLanding //Daring Landing
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			sUniqueIdentifier = "LESSON_05"
			sTitle = "PSLBT_05"
			BREAK
		CASE PSC_FlyLow //Fly Low
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_06"
			sTitle = "PSLBT_06"
			BREAK
		CASE PSC_loopTheLoop //Loop the Loop
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_07"
			sTitle = "PSLBT_07"
			BREAK
		CASE PSC_heliCourse //Heli Obstacle Course
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_08"
			sTitle = "PSLBT_08"
			BREAK
		CASE PSC_heliSpeedRun //Heli Speed Run
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_09"
			sTitle = "PSLBT_09"
			BREAK
		CASE PSC_parachuteOntoTarget //Skydiving
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			sUniqueIdentifier = "LESSON_10"
			sTitle = "PSLBT_10"
			BREAK
		CASE PSC_chuteOntoMovingTarg //Skydiving - Moving Target
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			sUniqueIdentifier = "LESSON_11"
			sTitle = "PSLBT_11"
			BREAK
		CASE PSC_planeCourse //Plane Obstacle Course
			fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			sUniqueIdentifier = "LESSON_12"
			sTitle = "PSLBT_12"
			BREAK
	ENDSWITCH
	
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(psLBControl, fmmcType, sUniqueIdentifier, sTitle)
ENDPROC


FUNC BOOL PS_DO_RANK_PREDICTION(PILOT_SCHOOL_CLASSES_ENUM eLesson, INT iScore, INT iTime = 0, FLOAT fDistance = 0.0)
	
	IF scLB_rank_predict.bFinishedRead
	AND NOT scLB_rank_predict.bFinishedWrite
		
		PS_WRITE_DATA_TO_LEADERBOARD(eLesson, iScore, iTime, fDistance)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("GET_RANK_PREDICTION_DETAILS CURRENT RUN values (JUST AFTER WRITE) " ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.currentResult)
		#ENDIF
	
		scLB_rank_predict.bFinishedWrite = TRUE
	ENDIF
	IF GET_RANK_PREDICTION_DETAILS(psLBControl)
		sclb_useRankPrediction = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC PS_DISPLAY_SOCIAL_CLUB_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard, PILOT_SCHOOL_CLASSES_ENUM eLesson, STRING sTitle)
 	DISABLE_FRONTEND_THIS_FRAME() //prevent player from pulling up the pausmenu while LBs are loading
	PS_LOAD_SOCIAL_CLUB_LEADERBOARD(eLesson, sTitle)
	DRAW_SC_SCALEFORM_LEADERBOARD(uiLeaderboard, psLBControl)
ENDPROC

PROC PS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(psLBControl)
ENDPROC

PROC PS_CLEANUP_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard)
	CLEANUP_SC_LEADERBOARD_UI(uiLeaderboard)
ENDPROC


