
//****************************************************************************************************
//****************************************************************************************************
//
// Pilot_School_HUD_lib.sch
//
// Author:
//	Asa Dang
// 
// Description:
//	HUD library for Pilot School challenges. Contains methods to update HUD elements and to toggle HUD elements on and off.
//
//
//
// Search for term "TODO:" to see any modifications/edits still in progress (or for code marked where removal is questionable)
//
//****************************************************************************************************
//****************************************************************************************************

USING "timer_public.sch"
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "Pilot_School_Definitions.sch"
USING "Pilot_School_Data.sch"
//USING "Pilot_School_HUD_lib.sch"
USING "PS_Objective_lib.sch"
//USING "Pilot_School_Challenge_Helpers.sch"
USING "script_oddjob_funcs.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "hud_drawing.sch"
USING "script_camera.sch"
USING "shared_hud_displays.sch"

USING "screens_header.sch"
//USING "screen_placements.sch"
//USING "screen_placements_export.sch"

USING "PS_End_Results.sch"

//TEMP (some menu funcs are shared with the challenges... grrr)
//TODO: separate these!!!
//USING "Pilot_School_Menu.sch"

//***************************************************************************************************
//HUD Stuff
//***************************************************************************************************

USING "UIUtil.sch"

PROC INIT_PS_END_RESULTS(MEGA_PLACEMENT_TOOLS &thisPlacement)
	PRINTLN("Setting up new menu!")
	RESET_ALL_SPRITE_PLACEMENT_VALUES(thisPlacement.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(thisPlacement.aStyle)
	INIT_SCREEN_PS_END_RESULTS(thisPlacement)
ENDPROC

PROC PS_HUD_INIT_VARS()
	PS_UI_ObjectiveMeter.sObjBarTitle							= "BLANK"
	PS_UI_ObjectiveMeter.fObjBarNumerator						= 0.0
	PS_UI_ObjectiveMeter.fObjBarDemoninator						= 1.0
	PS_UI_ObjectiveMeter.fObjBarDisplayAtX						= 0.0460
	PS_UI_ObjectiveMeter.fObjBarDisplayAtY						= 0.6920
	PS_UI_ObjectiveMeter.bIsLoopMeterActive						= FALSE
	PS_UI_ObjectiveMeter.bIsLoopMeterActive						= FALSE
	PS_UI_ObjectiveMeter.bIsImmelMeterActive					= FALSE
	PS_UI_ObjectiveMeter.bIsObjMeterVisible						= FALSE
	PS_UI_ObjectiveMeter.bCapObjectiveBar						= FALSE
	

	PS_UI_GutterIcon.bIsGutterActive 							= FALSE
	PS_UI_GutterIcon.bIsGutterVisible 							= FALSE
	PS_UI_GutterIcon.fGutterX									= 0.0
	PS_UI_GutterIcon.fGutterY									= 0.0
	PS_UI_GutterIcon.fGutterHeight								= 0.05
	PS_UI_GutterIcon.fGutterWidth								= 0.05
	
	PS_UI_RaceHud.sPSHUDMedalGoal							= ""					
	PS_UI_RaceHud.ePSHUDMedal								= PODIUMPOS_NONE
	PS_UI_RaceHud.fPSHUDMedalTime							= -1
	PS_UI_RaceHud.bIsTimerVisible 						= FALSE
	PS_UI_RaceHud.bIsHourGlassActive 						= FALSE
	PS_UI_RaceHud.iPSHUDExtraTime							= 0
	PS_UI_RaceHud.iHourGlassStartTime 					= 0
	PS_UI_RaceHud.iTimeBonus								= 0
	PS_UI_RaceHud.bCheckpointCounterVisible 				= FALSE
	PS_UI_RaceHud.bIsTimerActive							= FALSE
	PS_UI_RaceHud.bIsDistActive							= FALSE
ENDPROC

PROC PS_HUD_SETUP()
	INIT_PS_END_RESULTS(PSER_Placement)
ENDPROC

/// PURPOSE:
///    Sets a flag that tells the icon to be visible on screen
/// PARAMS:
///    bActive - 
PROC PS_HUD_SET_GUTTER_ICON_VISIBLE(BOOL bVisible)
	PS_UI_GutterIcon.bIsGutterVisible = bVisible
ENDPROC

/// PURPOSE:
///    Displays the gutter icon on the screen at [XScreenPos, YScreenPos]
/// PARAMS:
///    XScreenPos - 
///    YScreenPos - 
PROC PS_HUD_UPDATE_GUTTER_ICON()
	FLOAT fGutterRotation = 0
	IF (PS_UI_GutterIcon.fGutterX <> 0.0) AND (PS_UI_GutterIcon.fGutterY <> 0.0)
		IF PS_UI_GutterIcon.fGutterX >= 0.7999
			fGutterRotation = 90
		ELIF PS_UI_GutterIcon.fGutterX <= 0.2
			fGutterRotation = -90
		ELIF PS_UI_GutterIcon.fGutterY <= 0.2
			fGutterRotation = 0
		ELIF PS_UI_GutterIcon.fGutterY >= 0.7999
			fGutterRotation = 180
		ELSE
			//no gutter rotation to calculate	
		ENDIF
		
		PS_UI_GutterIcon.fGutterHeight = 0.05	//height was getting overwritten, so setting it inside the func.
		
		PS_UI_GutterIcon.fGutterWidth = PS_UI_GutterIcon.fGutterHeight * (9.0/16.0)			
		IF NOT GET_IS_WIDESCREEN()			
			PS_UI_GutterIcon.fGutterWidth = PS_UI_GutterIcon.fGutterHeight * (3.0/4.0)
		ENDIF	
		IF NOT DOES_CAM_EXIST(cPS_HintCam)
			DRAW_SPRITE("pilotSchool", "hudArrow", PS_UI_GutterIcon.fGutterX, PS_UI_GutterIcon.fGutterY, PS_UI_GutterIcon.fGutterWidth, PS_UI_GutterIcon.fGutterHeight,	fGutterRotation, 255, 255, 0, 100)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Used for external scripts to be able to update the screen position for the gutter icon
/// PARAMS:
///    XScreenPos - 
///    YScreenPos - 
PROC PS_HUD_SET_GUTTER_ICON_POSITION(FLOAT XScreenPos, FLOAT YScreenPos)
	PS_UI_GutterIcon.fGutterX = XScreenPos
	PS_UI_GutterIcon.fGutterY = YScreenPos
ENDPROC

FUNC BOOL PS_IS_COUNTDOWN_SHOWING_GO()
	IF IS_BITMASK_AS_ENUM_SET(PS_CountDownUI.iBitFlags, CNTDWN_UI_Played_Go)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_HUD_DISPLAY_LESSON_NAME()
	TEXT_LABEL_23 temp
	INT iLessonID = ENUM_TO_INT(g_current_selected_PilotSchool_class)
	UPDATE_MISSION_NAME_DISPLAYING(temp, FALSE, FALSE, FALSE, FALSE, iLessonID)
ENDPROC

/// PURPOSE:
///    Updates drawing of the countdown to the screen.
/// PARAMS:
///    ps_data - 
///    cntdwn - 
/// RETURNS: True once the update is reaches the last stage.
///    
FUNC BOOL PS_HUD_UPDATE_COUNTDOWN(BOOL bGivePlayerControlOnGo = TRUE)
	PS_HUD_DISPLAY_LESSON_NAME()
	IF NOT IS_TIMER_STARTED(PS_Main.tCountdownTimer)
		RESTART_TIMER_NOW(PS_Main.tCountdownTimer)
	ELIF TIMER_DO_WHEN_READY(PS_Main.tCountdownTimer, 1.5)
		IF UPDATE_MINIGAME_COUNTDOWN_UI(PS_CountDownUI, TRUE, TRUE)
			CANCEL_TIMER(PS_Main.tCountdownTimer)
			RETURN TRUE
		ENDIF
		IF bGivePlayerControlOnGo AND PS_IS_COUNTDOWN_SHOWING_GO()
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up our ped conversation stuff 
PROC PS_SETUP_DISPATCHER()
	ADD_PED_FOR_DIALOGUE(PedDispatcher, 0, NULL, "PilotDispatch")
ENDPROC

/// PURPOSE:
///    Plays a ped conversation line from our dispatcher. All strings must be in MGPSAUD
/// PARAMS:
///    sRoot - 
///    sLabel - 
///    bHighPriority - 
/// RETURNS:
///    
FUNC BOOL PS_PLAY_DISPATCHER_INSTRUCTION(STRING sRoot, STRING sLabel, BOOL bHighPriority = FALSE)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_FACE_TO_FACE_CONVERSATION()
//		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			WAIT(0)
//		ENDWHILE
	ENDIF
	IF bHighPriority
		RETURN PLAY_SINGLE_LINE_FROM_CONVERSATION(PedDispatcher, "MGPSAUD", sRoot, sLabel, CONV_PRIORITY_VERY_HIGH )
	ELSE
		RETURN PLAY_SINGLE_LINE_FROM_CONVERSATION(PedDispatcher, "MGPSAUD", sRoot, sLabel, CONV_PRIORITY_VERY_LOW )
	ENDIF
ENDFUNC

PROC PS_HUD_UPDATE_OBJECTIVE_METER()

	//default bar color
	HUD_COLOURS barColor = HUD_COLOUR_RED

	SWITCH PS_UI_ObjectiveMeter.eObjMeterType
	
		CASE PS_OBJECTIVE_TYPE_BARREL_ROLL 
			PS_UI_ObjectiveMeter.fObjBarNumerator = ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_INSIDE_LOOP
			PS_UI_ObjectiveMeter.fObjBarNumerator = ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)
			BREAK
			
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_TURN
			PS_UI_ObjectiveMeter.fObjBarNumerator = ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)
			BREAK

		CASE PS_OBJECTIVE_TYPE_IMMELMAN_ROLL
			PS_UI_ObjectiveMeter.fObjBarNumerator = ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)
			BREAK
			
		CASE PS_OBJECTIVE_TYPE_INVERTED
			IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
				PS_UI_ObjectiveMeter.fObjBarNumerator = GET_TIMER_IN_SECONDS(PS_Main.myObjectiveData.tObjectiveTimer)
			ENDIF
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_KNIFE
		CASE PS_OBJECTIVE_TYPE_KNIFE_R
		CASE PS_OBJECTIVE_TYPE_KNIFE_L
			IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
				PS_UI_ObjectiveMeter.fObjBarNumerator = GET_TIMER_IN_SECONDS(PS_Main.myObjectiveData.tObjectiveTimer)
			ENDIF
			
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_FORMATION
			IF IS_TIMER_STARTED(PS_Main.tHealthTimer)
				PS_UI_ObjectiveMeter.fObjBarNumerator = PS_UI_ObjectiveMeter.fObjBarDemoninator - GET_TIMER_IN_SECONDS(PS_Main.tHealthTimer)
			ELSE
				//meter is full until timer is started
				PS_UI_ObjectiveMeter.fObjBarNumerator = PS_UI_ObjectiveMeter.fObjBarDemoninator
			ENDIF
			
			IF PS_UI_ObjectiveMeter.fObjBarNumerator < PS_UI_ObjectiveMeter.fObjBarDemoninator * 0.33
				IF PS_SoundID_Formation_Alarm = -1
					PS_SoundID_Formation_Alarm = GET_SOUND_ID()
					ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_ALTITUDE_ALARM_MASTER", PS_SoundID_Formation_Alarm, TRUE)
					PS_PLAY_DISPATCHER_INSTRUCTION("PS_FORM", "PS_FORM_5", TRUE)
				ENDIF
				barColor = HUD_COLOUR_RED
			ELSE
				barColor = HUD_COLOUR_GREEN
				ODDJOB_STOP_SOUND(PS_SoundID_Formation_Alarm)
			ENDIF
			BREAK
			
	ENDSWITCH
	
	IF NOT PS_UI_ObjectiveMeter.bCapObjectiveBar
		IF PS_UI_ObjectiveMeter.fObjBarNumerator > PS_UI_ObjectiveMeter.fObjBarDemoninator
			PS_UI_ObjectiveMeter.fObjBarNumerator = PS_UI_ObjectiveMeter.fObjBarNumerator % PS_UI_ObjectiveMeter.fObjBarDemoninator
		ENDIF
	ENDIF

	DRAW_TIMER_HUD(ROUND(PS_UI_ObjectiveMeter.fObjBarNumerator*100), ROUND(PS_UI_ObjectiveMeter.fObjBarDemoninator*100), PS_UI_ObjectiveMeter.sObjBarTitle, barColor)

ENDPROC


/// PURPOSE:
///    Tells the PS HUD to show/hide the objective/progress bar. Should also PS_HUD_SET_OBJECTIVE_TYPE() before the first time calling this
/// PARAMS:
///    bVisible - 
PROC PS_HUD_SET_OBJECTIVE_METER_VISIBLE(BOOL bVisible)
	PS_UI_ObjectiveMeter.bIsObjMeterVisible = bVisible
ENDPROC

FUNC BOOL PS_HUD_IS_OBJECTIVE_METER_VISIBLE()
	RETURN PS_UI_ObjectiveMeter.bIsObjMeterVisible
ENDFUNC

/// PURPOSE:
///    Resets the Objective meter for stunts that look at tPulse timer for the meter value
/// PARAMS:
///    PS_Struct_Data - 
PROC Pilot_School_HUD_Reset_Obj_Meter()	
	PS_UI_ObjectiveMeter.fObjBarNumerator = 0
	CANCEL_TIMER(PS_Main.tPulse)
ENDPROC

PROC PS_HUD_SET_OBJECTIVE_METER_CAPPED(BOOL thisBool)
	PS_UI_ObjectiveMeter.bCapObjectiveBar = thisBool
ENDPROC


FUNC PS_OBJECTIVE_TYPE_ENUM Pilot_School_HUD_Get_Obj_Meter_Type()
	RETURN PS_UI_ObjectiveMeter.eObjMeterType 
ENDFUNC

/// PURPOSE:
 ///    Tells the objective/progress meter what type of info to display.
 /// PARAMS:
 ///    eTempMeterType - 
PROC PS_HUD_SET_OBJECTIVE_TYPE(PS_OBJECTIVE_TYPE_ENUM eTempMeterType)
	
	PS_UI_ObjectiveMeter.bIsObjMeterVisible = TRUE
	PS_UI_ObjectiveMeter.eObjMeterType = eTempMeterType

	SWITCH PS_UI_ObjectiveMeter.eObjMeterType
	
		CASE PS_OBJECTIVE_TYPE_BARREL_ROLL
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_ROLL" //"~s~   Roll:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 360.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_INVERTED
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_INVERT"//"~s~Inverted:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = PS_INVERTED_HOLD_TIME
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_KNIFE
		CASE PS_OBJECTIVE_TYPE_KNIFE_L
		CASE PS_OBJECTIVE_TYPE_KNIFE_R
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_KNIFE"//"~s~Knifing:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 5.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_INSIDE_LOOP
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_LOOP"//"~s~Looping:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 360.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_TURN
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_IMMEL"//"~s~Immelman Turn:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 180.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_ROLL
			PS_UI_ObjectiveMeter.sObjBarTitle =  "PS_BAR_IMROLL"//"~s~Immelman Roll:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 360.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_FORMATION
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_HEALTH"//"~s~ Formation:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator =  PS_FAIL_TIME_FORMATION
		BREAK
		
	ENDSWITCH
	PS_UI_ObjectiveMeter.sObjBarTitle = PS_UI_ObjectiveMeter.sObjBarTitle
	PS_UI_ObjectiveMeter.fObjBarDisplayAtX = PS_UI_ObjectiveMeter.fObjBarDisplayAtX
	PS_UI_ObjectiveMeter.fObjBarDisplayAtY = PS_UI_ObjectiveMeter.fObjBarDisplayAtY
	PS_UI_ObjectiveMeter.fObjBarDemoninator = PS_UI_ObjectiveMeter.fObjBarDemoninator
	Pilot_School_HUD_Reset_Obj_Meter()

ENDPROC

PROC PS_HUD_STOP_ALTIMETER_ALARM()
	ODDJOB_STOP_SOUND(PS_SoundID_Alt_Meter_Alarm)
ENDPROC



PROC PS_HUD_ACTIVATE_OBJECTIVE_METER(PS_OBJECTIVE_TYPE_ENUM thisType, BOOL shouldReset = FALSE, BOOL startVisible = TRUE)
	IF shouldReset
		PS_RESET_OBJECTIVE_DATA()
	ENDIF
	
	PS_UI_ObjectiveMeter.eObjMeterType = thisType
	PS_UI_ObjectiveMeter.bIsObjMeterVisible = startVisible
	
	SWITCH PS_UI_ObjectiveMeter.eObjMeterType
	
		CASE PS_OBJECTIVE_TYPE_BARREL_ROLL
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_ROLL" //"~s~   Roll:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 360.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_INVERTED
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_INVERT"//"~s~Inverted:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = PS_INVERTED_HOLD_TIME
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_KNIFE
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_KNIFE"//"~s~Knifing:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 5.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_INSIDE_LOOP
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_LOOP"//"~s~Looping:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 360.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_TURN
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_IMMEL"//"~s~Immelman Turn:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 180.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_ROLL
			PS_UI_ObjectiveMeter.sObjBarTitle =  "PS_BAR_IMROLL"//"~s~Immelman Roll:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator = 360.0
		BREAK
		
		CASE PS_OBJECTIVE_TYPE_FORMATION
			PS_UI_ObjectiveMeter.sObjBarTitle = "PS_BAR_HEALTH"//"~s~ Formation:~s~"
			PS_UI_ObjectiveMeter.fObjBarDisplayAtX = 0.0460
			PS_UI_ObjectiveMeter.fObjBarDisplayAtY = 0.6920
			PS_UI_ObjectiveMeter.fObjBarDemoninator =  20.0
		BREAK
		
	ENDSWITCH
	PS_UI_ObjectiveMeter.sObjBarTitle = PS_UI_ObjectiveMeter.sObjBarTitle
	PS_UI_ObjectiveMeter.fObjBarDisplayAtX = PS_UI_ObjectiveMeter.fObjBarDisplayAtX
	PS_UI_ObjectiveMeter.fObjBarDisplayAtY = PS_UI_ObjectiveMeter.fObjBarDisplayAtY
	PS_UI_ObjectiveMeter.fObjBarDemoninator = PS_UI_ObjectiveMeter.fObjBarDemoninator
ENDPROC


/// PURPOSE:
///    Displays an altimeter on the side of the screen. Tells you what the plane's ALTITUDE is. Must update using Set_Altimeter()
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_HUD_UPDATE_ALITMETER()

	IF fAltitude > fAltitudeMax
		IF PS_SoundID_Alt_Meter_Alarm = -1
			PS_SoundID_Alt_Meter_Alarm = GET_SOUND_ID()
			ODDJOB_PLAY_SOUND("DRUG_TRAFFIC_AIR_ALTITUDE_ALARM_MASTER", PS_SoundID_Alt_Meter_Alarm, TRUE)
		ENDIF
	ELSE
		ODDJOB_STOP_SOUND(PS_SoundID_Alt_Meter_Alarm)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Used to turn the display on. If accessed directly, the calculations will still occur. Use Set_Altimeter_Active in Pilot_School_Helpers to turn it off completely.
/// PARAMS:
///    bVisible - 
PROC PS_HUD_SET_ALTIMETER_VISIBLE(BOOL bVisible)
	bIsAltimeterVisible = bVisible
ENDPROC

PROC PS_HUD_SET_MAX_ALTITUDE(FLOAT tempmax)
	fAltitudeMax  = tempmax
ENDPROC

/// PURPOSE:
///    Used to access the variables the Altimeter updates from
PROC PS_HUD_SET_ALTIMETER(FLOAT fMyAltitude)
	 fAltitude = fMyAltitude
ENDPROC

FUNC FLOAT PS_HUD_GET_ALTIMETER()
	RETURN fAltitude
ENDFUNC

/// PURPOSE:
///    Displays an altitude indicator overlayed on top of the mini-map.Data access thru Set_Altitude_Indicator()
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_HUD_UPDATE_ALTITUDE_INDICATOR()
	fRoll = fRoll
ENDPROC

/// PURPOSE:
///    Used to access the variables the Altitude Indicator updates from
PROC PS_HUD_SET_ALTITUDE_INDICTATOR(FLOAT fMyRoll)
	fRoll = fMyRoll
ENDPROC

PROC PS_HUD_SET_ALTITUDE_INDICTATOR_VISIBLE(BOOL bVisible)
	bIsAltitudeIndicatorVisible = bVisible
ENDPROC

/// PURPOSE:
///    Resets the main race timer that can be displayed on the PS HUD
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_HUD_RESET_TIMER()
 	RESTART_TIMER_NOW(PS_Main.tRaceTimer)
ENDPROC

/// PURPOSE:
///    Tells the PS HUD to show/hide the timer
/// PARAMS:
///    bTimerBool - 
PROC PS_HUD_SET_TIMER_VISIBLE(BOOL bTimerBool)
 	PS_UI_RaceHud.bIsTimerVisible = bTimerBool
ENDPROC

/// PURPOSE:
///    Tells the PS_HUD to show/hide the dist. counter
/// PARAMS:
///    bTimerBool - 
PROC PS_HUD_SET_DIST_HUD_VISIBLE(BOOL bTimerBool)
 	PS_UI_RaceHud.bIsDistVisible = bTimerBool
ENDPROC

/// PURPOSE:
///    Displays an altimeter on the side of the screen. Tells you what the plane's ALTITUDE is. Must update using Set_Altimeter()
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_HUD_UPDATE_DIST_HUD()

	IF fDistanceLeft >= 0
		DRAW_GENERIC_SCORE(FLOOR(fDistanceLeft), "PSER_DISTANCE")
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Used to access the variables the Dist HUD (daring landing) updates from
PROC PS_HUD_SET_DIST_HUD(FLOAT fMyDistance)
	 fDistanceLeft = fMyDistance
ENDPROC


/// PURPOSE:
///    Plays a ped conversation line from our dispatcher. All strings must be in MGPSAUD
/// PARAMS:
///    sRoot - 
///    sLabel - 
///    bHighPriority - 
/// RETURNS:
///    
FUNC BOOL PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES(STRING sRoot1, STRING sLabel1, STRING sRoot2, STRING sLabel2, STRING sRoot3, STRING sLabel3, BOOL bHighPriority = FALSE)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_FACE_TO_FACE_CONVERSATION()
		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			WAIT(0)
		ENDWHILE
	ENDIF
	IF bHighPriority
		RETURN CREATE_MULTIPART_CONVERSATION_WITH_3_LINES(PedDispatcher, "MGPSAUD", sRoot1, sLabel1, sRoot2, sLabel2, sRoot3, sLabel3, CONV_PRIORITY_VERY_HIGH)
	ELSE
		RETURN CREATE_MULTIPART_CONVERSATION_WITH_3_LINES(PedDispatcher, "MGPSAUD", sRoot1, sLabel1, sRoot2, sLabel2, sRoot3, sLabel3, CONV_PRIORITY_VERY_LOW)
	ENDIF
ENDFUNC

/// PURPOSE:
///    Plays a ped conversation line from our dispatcher. All strings must be in MGPSAUD
/// PARAMS:
///    sRoot - 
///    sLabel - 
///    bHighPriority - 
/// RETURNS:
///    
FUNC BOOL PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES(STRING sRoot1, STRING sLabel1, STRING sRoot2, STRING sLabel2, BOOL bHighPriority = FALSE)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_FACE_TO_FACE_CONVERSATION()
		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			WAIT(0)
		ENDWHILE
	ENDIF
	IF bHighPriority
		RETURN CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(PedDispatcher, "MGPSAUD", sRoot1, sLabel1, sRoot2, sLabel2, CONV_PRIORITY_VERY_HIGH)
	ELSE
		RETURN CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(PedDispatcher, "MGPSAUD", sRoot1, sLabel1, sRoot2, sLabel2, CONV_PRIORITY_VERY_LOW)
	ENDIF
ENDFUNC

FUNC BOOL PS_CHECK_AND_UPDATE_PLAYED_OUT_DIALOGUE(INT &thisArray[], INT thisID)

	INT i = 0
	REPEAT PS_FEEDBACK_DIALOGUE_REPLAY_COUNT i
		IF thisArray[i] = thisID
			//already in the array, so we've played it before
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	//not in the array, so update the array and return false!
	i = 0
	INT x = 0
	REPEAT PS_FEEDBACK_DIALOGUE_REPLAY_COUNT i
		IF i > 0
			x = PS_FEEDBACK_DIALOGUE_REPLAY_COUNT - i
			thisArray[x] = thisArray[x-1]
		ENDIF
	ENDREPEAT
	
	thisArray[0] = thisID //add the ID we're about to play to the array
	RETURN FALSE

ENDFUNC


FUNC BOOL PS_HAS_FEEDBACK_DIALOGUE_BEEN_PLAYED(PS_CHECKPOINT_PASS thisType, INT thisID)
	
	#IF IS_DEBUG_BUILD
		IF thisID = 0 
			SCRIPT_ASSERT("uhhh the ID is 0 for the dialogue we're trying to play... something is wrong!")
		ENDIF
	#ENDIF
	
	//find out what array we need to check
	SWITCH thisType
		CASE PS_CHECKPOINT_OK
			RETURN PS_CHECK_AND_UPDATE_PLAYED_OUT_DIALOGUE(iPlayedOutEncourageDialogue, thisID)
			BREAK
		CASE PS_CHECKPOINT_AWESOME
			RETURN PS_CHECK_AND_UPDATE_PLAYED_OUT_DIALOGUE(iPlayedOutRewardDialogue, thisID)
			BREAK
		CASE PS_CHECKPOINT_MISS
			RETURN PS_CHECK_AND_UPDATE_PLAYED_OUT_DIALOGUE(iPlayedOutDiscourageDialogue, thisID)
			BREAK
	ENDSWITCH
	//should never reach here...
	RETURN FALSE
ENDFUNC


PROC PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_CHECKPOINT_PASS thisScore)

//	CONST_INT			PS_DIALOGUE_ENCOURAGE_MAX					9
//	CONST_INT			PS_DIALOGUE_DISCOURAGE_MAX					11
//	CONST_INT			PS_DIALOGUE_REWARD_MAX						10
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		EXIT
	ENDIF
	
//	SCRIPT_ASSERT("playing rand dialogue!")
	TEXT_LABEL sDialogueRoot, sDialogueLabel
	INT randInt
	BOOL bHasNotGottenLabel = TRUE
	
	WHILE bHasNotGottenLabel
		randInt = GET_RANDOM_INT_IN_RANGE()
		SWITCH thisScore
			CASE PS_CHECKPOINT_OK
				sDialogueLabel = "PS_GENI_"
				sDialogueRoot = "PS_GENI"
				randInt = (randInt % PS_DIALOGUE_ENCOURAGE_MAX) + 1 
				sDialogueLabel += randInt
				BREAK
			CASE PS_CHECKPOINT_AWESOME
				sDialogueLabel = "PS_GENR_"
				sDialogueRoot = "PS_GENR"
				randInt = (randInt % PS_DIALOGUE_REWARD_MAX) + 1
				sDialogueLabel += randInt
				BREAK
			CASE PS_CHECKPOINT_MISS
				sDialogueLabel = "PS_GEND_"
				sDialogueRoot = "PS_GEND"
				randInt = (randInt % PS_DIALOGUE_DISCOURAGE_MAX) + 1
				sDialogueLabel += randInt				
				BREAK
		ENDSWITCH
		
		IF PS_HAS_FEEDBACK_DIALOGUE_BEEN_PLAYED(thisScore, randInt)
			bHasNotGottenLabel = TRUE //we already played this once in the last 3 times
		ELSE
			bHasNotGottenLabel = FALSE //hasn't been played recently
			PS_PLAY_DISPATCHER_INSTRUCTION(sDialogueRoot, sDialogueLabel)
			EXIT //just incase?
		ENDIF
	ENDWHILE
ENDPROC

PROC PS_HUD_SET_CHECKPOINT_COUNTER_VISIBLE(BOOL bActive)
	PS_UI_RaceHud.bCheckpointCounterVisible = bActive
ENDPROC

PROC PS_HUD_SET_TARGET_GOAL_TIME(FLOAT fTimeToBeat, TEXT_LABEL sNextMedal, PODIUMPOS& ePodium)
	PS_UI_RaceHud.sPSHUDMedalGoal = sNextMedal
	PS_UI_RaceHud.fPSHUDMedalTime = fTimeToBeat
	PS_UI_RaceHud.ePSHUDMedal = ePodium
ENDPROC

PROC PS_HUD_UPDATE_TIME_BONUS(structTimer& racetimer, INT& timebonus)
	IF timebonus <> 0
		PS_UI_RaceHud.iPSHUDExtraTime = timebonus
		//add or subtract bonus time
		ADJUST_TIMER(racetimer, TO_FLOAT(timebonus))
		//restart the timer that keeps track of how long the bonus is shown on the hud
		RESTART_TIMER_NOW(PS_UI_RaceHud.tHUDBonusTimer)
		//clear out timebonus
		timebonus = 0
	ENDIF
	IF PS_UI_RaceHud.iPSHUDExtraTime <> 0	
		IF TIMER_DO_ONCE_WHEN_READY(PS_UI_RaceHud.tHUDBonusTimer, PS_BONUS_VISIBLE_TIME)
			PS_UI_RaceHud.iPSHUDExtraTime = 0
		ENDIF
	ENDIF	
ENDPROC

FUNC INT PS_HUD_GET_HOURGLASS_TIMER_VALUE()
	IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
		RETURN (iHourGlassTimeToRun + PS_UI_RaceHud.iHourGlassStartTime) - CEIL(GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer) * 1000)	
	ELSE
		RETURN 0
	ENDIF
ENDFUNC

FUNC INT PS_HUD_GET_HOURGLASS_MAX_TIME()
	RETURN iHourGlassTimeToRun
ENDFUNC

PROC PS_HUD_SET_HOURGLASS_TIMER_PADDING(INT padding)
	iHourGlassPadding = padding
ENDPROC

FUNC INT PS_HUD_GET_HOURGLASS_TIMER_PADDING()
	RETURN iHourGlassPadding
ENDFUNC

PROC PS_HUD_ENABLE_SCREEN_FADING(BOOL bActive)
	bScreenFadeVisible = bActive
	eFadeState = PS_SCREEN_FADE_IDLE
	IF IS_TIMER_STARTED(tPS_FadeTimer)	
		CANCEL_TIMER(tPS_FadeTimer)
	ENDIF
	iScreenFadeTime = PS_SCREEN_FADE_DEFAULT_TIME
	fScreenFadeWaitTime = PS_SCREEN_FADE_DEFAULT_WAIT
ENDPROC

PROC PS_HUD_START_FADE(PS_SCREEN_FADE_STATE eState, INT fadeTime, FLOAT waitTime = 0.0)
	IF NOT bScreenFadeVisible
		SCRIPT_ASSERT("Calling set fade without first enabling screen fading!")
	ENDIF
	eFadeState = eState
	iScreenFadeTime = fadeTime
	fScreenFadeWaitTime = waitTime
ENDPROC

FUNC BOOL PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_STATE eState, INT fadeTime, FLOAT waitTime = -1.0)
//	RETURN TRUE
	IF NOT bScreenFadeVisible
		SCRIPT_ASSERT("Calling set fade without first enabling screen fading!")
	ENDIF
	//check if we're already faded out/in
	IF eState = PS_SCREEN_FADE_OUT
		IF IS_SCREEN_FADED_OUT()
			DEBUG_MESSAGE("++++++++++++++++++++++++++++++++++++++++++++++++++done fading out")
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_SCREEN_FADED_IN()
			DEBUG_MESSAGE("++++++++++++++++++++++++++++++++++++++++++++++++++done fading in")
			RETURN TRUE
		ENDIF
	ENDIF
	
	//if we're not faded out/in, then check if we should wait, or call set fade
	IF NOT (IS_SCREEN_FADING_IN() OR IS_SCREEN_FADING_OUT())
		DEBUG_MESSAGE("++++++++++++++++++++++++++++++++++++++++++++++++++calling set fade")
		PS_HUD_START_FADE(eState, fadeTime, waitTime)
		RETURN FALSE
	ENDIF

	DEBUG_MESSAGE("++++++++++++++++++++++++++++++++++++++++++++++++++waiting to fade in or out")
	RETURN FALSE
ENDFUNC

PROC PS_HUD_HIDE_GAMEHUD_ELEMENTS(BOOL bHideRadar = TRUE)
	IF bHideRadar
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF 
	HIDE_ALL_TOP_RIGHT_HUD()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
ENDPROC

/// PURPOSE:
/// Updates fade (call set_fade method) always resolves back to a fade in, unless set_fade() is called with a waitTime set to -1
///    
PROC PS_HUD_UPDATE_FADE()
	IF bScreenFadeVisible
		SWITCH(eFadeState)
			CASE PS_SCREEN_FADE_IDLE
				//do nothing
				BREAK

			CASE PS_SCREEN_FADE_OUT
				//wait for screen to fade fully out, then fade back in
				IF IS_SCREEN_FADED_OUT()
					eFadeState = PS_SCREEN_FADE_WAIT
				ELIF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(iScreenFadeTime)
					RESTART_TIMER_NOW(tPS_FadeTimer)
				ENDIF
				BREAK
				
			CASE PS_SCREEN_FADE_WAIT
				IF fScreenFadeWaitTime > 0.0
					IF NOT IS_TIMER_STARTED(tPS_FadeTimer)
						START_TIMER_AT(tPS_FadeTimer, 0)
					ELSE
						IF TIMER_DO_ONCE_WHEN_READY(tPS_FadeTimer, fScreenFadeWaitTime)
							CANCEL_TIMER(tPS_FadeTimer)
							eFadeState = PS_SCREEN_FADE_IN
						ENDIF
					ENDIF
				ELIF fScreenFadeWaitTime < 0.0
					eFadeState = PS_SCREEN_FADE_IDLE	
				ELSE
					eFadeState = PS_SCREEN_FADE_IN
				ENDIF
				BREAK
			
			CASE PS_SCREEN_FADE_IN
				//wait for screen to fade fully in, then switch to idle
				IF IS_SCREEN_FADED_IN()
					eFadeState = PS_SCREEN_FADE_IDLE
				ELIF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(iScreenFadeTime)
				ENDIF			
				BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC PS_HUD_SET_FINISH_CAMERA_ACTIVE(BOOL bIsActive)
	bIsEndCutActive = bIsActive
	SET_MINIGAME_SPLASH_SHOWING(bIsActive)
ENDPROC

FUNC BOOL PS_HUD_IS_SCORECARD_ACTIVE()
	RETURN bIsScoreCardVisible
ENDFUNC

PROC PS_HUD_SET_SCORECARD_ACTIVE(BOOL bActive)
	PS_HUD_SET_FINISH_CAMERA_ACTIVE(bActive)
	IF ePSFailReason = PS_FAIL_NO_REASON
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_YES",		FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_NO",			FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ELSE	
		INIT_SIMPLE_USE_CONTEXT(menuInstructions, FALSE, FALSE, TRUE, TRUE)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "PSER_CONTINUE",	FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "IB_QUIT",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ADD_SIMPLE_USE_CONTEXT_INPUT(menuInstructions, "PSER_RETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_X)
	ENDIF

	RESTART_TIMER_NOW(PS_Main.tCountdownTimer)
	SET_RESULT_SCREEN_DISPLAYING_STATE(bActive)
	bIsScoreCardVisible = bActive
ENDPROC

FUNC INT PS_HUD_FORMAT_INTEGER_SCORE(INT thisInt)
	IF thisInt < 0
		RETURN 0
	ENDIF
	//if the formatting is fine we'll get here
	RETURN thisInt
ENDFUNC

FUNC FLOAT PS_HUD_FORMAT_FLOAT_SCORE(FLOAT thisFloat)
	IF thisFloat < 0
		RETURN 0.0
	ENDIF
	//if the formatting is fine we'll get here
	RETURN thisFloat
ENDFUNC

FUNC STRING PS_HUD_GET_FAIL_REASON_LABEL()
	SWITCH(ePSFailReason)
		CASE PS_FAIL_DOUBLE_REASON
			#IF IS_DEBUG_BUILD
				RETURN "PS_FAIL_0"
			#ENDIF
			BREAK	
		CASE PS_FAIL_NO_REASON
			#IF IS_DEBUG_BUILD
				RETURN "PS_FAIL_1"
			#ENDIF
			BREAK
		CASE PS_FAIL_SPECIAL_REASON
			//this should be set on a lesson's init func
			RETURN PS_Special_Fail_Reason
			BREAK
		CASE PS_FAIL_TIME_UP
			RETURN "PS_FAIL_2"
			BREAK
		CASE PS_FAIL_TOO_FAR
			RETURN "PS_FAIL_3"
			BREAK
		CASE PS_FAIL_FORMATION
			RETURN "PS_FAIL_3A"
			BREAK
		CASE PS_FAIL_PLAYER_LEFT_PLANE
			RETURN "PS_FAIL_4"
			BREAK
		CASE PS_FAIL_PLAYER_LEFT_HELI
			RETURN "PS_FAIL_4A"
			BREAK
		CASE PS_FAIL_OUT_OF_RANGE
			RETURN "PS_FAIL_5"
			BREAK
		CASE PS_FAIL_BEHIND_RACE
			RETURN "PS_FAIL_6"
			BREAK
		CASE PS_FAIL_LANDED_VEHICLE
			RETURN "PS_FAIL_7"
			BREAK
		CASE PS_FAIL_DAMAGED_VEHICLE
			RETURN "PS_FAIL_8"
			BREAK
		CASE PS_FAIL_MISSED_GATES
			RETURN "PS_FAIL_9"
			BREAK
		CASE PS_FAIL_MISSED_FIRST_GATE
			RETURN "PS_FAIL_9A"
			BREAK
		CASE PS_FAIL_LEFT_RUNWAY
			RETURN "PS_FAIL_10"
			BREAK
		CASE PS_FAIL_SUBMERGED
			RETURN "PS_FAIL_11"
			BREAK
		CASE PS_FAIL_STUCK_VEHICLE
			RETURN "PS_FAIL_12"
			BREAK
		CASE PS_FAIL_TOO_HIGH
			RETURN "PS_FAIL_13"
			BREAK
		CASE PS_FAIL_LANDED_IN_WATER
			RETURN "PS_FAIL_14"
			BREAK
		CASE PS_FAIL_REMOVED_PARACHUTE
			RETURN "PS_FAIL_15"
			BREAK
		CASE PS_FAIL_IDLING
			RETURN "PS_FAIL_16"
			BREAK
		CASE PS_FAIL_WANTED
			RETURN "PS_FAIL_17"
			BREAK
		CASE PS_FAIL_COLLIDED_TRUCK
			RETURN "PS_FAIL_18"
			BREAK
		CASE PS_FAIL_LANDING_GEAR
			RETURN "PS_FAIL_19"
			BREAK
		CASE PS_FAIL_DEBUG
			#IF IS_DEBUG_BUILD
				RETURN "PS_DEBUG_FAIL"
			#ENDIF
			BREAK
		CASE PS_FAIL_PLAYER_DEAD
			RETURN "PS_FAIL_1"
			BREAK
	ENDSWITCH
	RETURN "PS_FAIL_1"
ENDFUNC
//
//PROC PRINT_PIX_INFO_FOR_SHIT(MEGA_PLACEMENT_TOOLS &thisPlacement)
//		SAVE_STRING_TO_DEBUG_FILE("PIXEL INFO")
//		SAVE_NEWLINE_TO_DEBUG_FILE()
//		
//		//rects	
//		STRING thisName
//		INT i= 0
//		REPEAT ENUM_TO_INT(PSER_LAST_RECT) i
//			thisName = thisPlacement.RectPlacement[INT_TO_ENUM(PSER_ENDSCREEN_RECT, i)].txtRectName
//			SAVE_STRING_TO_DEBUG_FILE("thisPlacement.RectPlacement[")
//			SAVE_STRING_TO_DEBUG_FILE(thisName)
//			SAVE_STRING_TO_DEBUG_FILE("].x = ")
//			SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_X_TO_PIXEL(thisPlacement.RectPlacement[INT_TO_ENUM(PSER_ENDSCREEN_RECT, i)].x))
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			ENDIF
//		ENDREPEAT
//	
//	
//	thisPlacement.RectPlacement[PSER_SCORE_TITLE_EDGE_RECT].x = 0.3000
//	thisPlacement.RectPlacement[PSER_SCORE_TITLE_EDGE_RECT].y = 0.1815
//	thisPlacement.RectPlacement[PSER_SCORE_TITLE_EDGE_RECT].w = 0.1980
//	thisPlacement.RectPlacement[PSER_SCORE_TITLE_EDGE_RECT].h = 0.0035
//	
//ENDPROC



PROC PROCESS_SCREEN_PS_END_RESULTS(MEGA_PLACEMENT_TOOLS &thisPlacement)
//	PRINT_PIX_INFO_FOR_SHIT(thisPlacement)
	
	//background
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MAIN_RECT])
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", thisPlacement.SpritePlacement[PSER_MAIN_BACKGROUND], FALSE)	
	
	//main title
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_SCORE_TITLE_RECT])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_SCORE_TITLE_EDGE_RECT])
	DRAW_TEXT(thisPlacement.TextPlacement[PSER_MAIN_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "PS_TITLE")

	//Art representation of score
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_SCORE_IMAGE_RECT])	
	SET_SPRITE_WHITE(thisPlacement.SpritePlacement[PSER_SCORE_IMAGE_SPRITE])
	
	//figure out what description image to display
	STRING sDescriptionImageName = "Locked_Icon_32"
	SWITCH g_current_selected_PilotSchool_class
	
		CASE PSC_Takeoff
			sDescriptionImageName = "FlightSchool_1a_RunwayTakeOff"
			BREAK
		CASE PSC_Landing
			sDescriptionImageName = "FlightSchool_2a_RunwayLanding"
			BREAK
		CASE PSC_Inverted
			sDescriptionImageName = "FlightSchool_1_Invert"
			BREAK
		CASE PSC_Knifing
			sDescriptionImageName = "FlightSchool_2_Knife"
			BREAK
		CASE PSC_loopTheLoop
			sDescriptionImageName = "FlightSchool_3_Loop"
			BREAK
		CASE PSC_FlyLow
			sDescriptionImageName = "FlightSchool_4_LowFly"
			BREAK
		CASE PSC_DaringLanding
			sDescriptionImageName = "FlightSchool_5_Land"
			BREAK
		CASE PSC_heliCourse
			sDescriptionImageName = "FlightSchool_7_HeliObstacle"
			BREAK
		CASE PSC_heliSpeedRun
			sDescriptionImageName = "FlightSchool_8_HeliSpeed"
			BREAK
		CASE PSC_parachuteOntoTarget
			sDescriptionImageName = "FlightSchool_9_SkyDive"
			BREAK
		CASE PSC_chuteOntoMovingTarg
			sDescriptionImageName = "FlightSchool_10_SkyDiveMoving"
			BREAK
		CASE PSC_planeCourse
			sDescriptionImageName = "FlightSchool_6_Obstacle"
			BREAK
	ENDSWITCH
	
	DRAW_2D_SPRITE("PS_Menu", sDescriptionImageName, thisPlacement.SpritePlacement[PSER_SCORE_IMAGE_SPRITE], FALSE)	
	
	
	//sub title
	SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_TEXT(thisPlacement.TextPlacement[PSER_SCORE_TITLE], thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "PSM_AWARD_TITLE")	
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	
	// Set up to iterate through column 1 cells
	STRING sLabel, sValue
	INT iCellCur = 0
	INT iCellMax = 7
	INT iCellsTaken = 0
	INT iCellSpans[7]
	PSER_ENDSCREEN_RECT eLastRect
	PSER_ENDSCREEN_TEXT eLastValue
	PSER_ENDSCREEN_TEXT eLastLabel
	PSER_ENDSCREEN_RECT eCurRect = PSER_DYNAMIC_ROW_RECT_0
	PSER_ENDSCREEN_TEXT eCurValue = PSER_DYNAMIC_ROW_VALUE_0
	PSER_ENDSCREEN_TEXT eCurLabel = PSER_DYNAMIC_ROW_LABEL_0
	
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
// Mission Success OR...
	IF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
		iCellSpans[iCellCur] = 1
		iCellsTaken += iCellSpans[iCellCur]
		// Gold
		IF iCurrentChallengeScore >= iSCORE_FOR_GOLD
			sValue = "PS_sGOLD"
			SET_TEXT_GOLD(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			SET_SPRITE_GOLD(thisPlacement.SpritePlacement[PSER_AWARD_ROW_SPRITE])
		// Silver
		ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER
			sValue = "PS_sSILVER"
			SET_TEXT_SILVER(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			SET_SPRITE_SILVER(thisPlacement.SpritePlacement[PSER_AWARD_ROW_SPRITE])
		// Bronze OR Success
		ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
			sValue = "PS_sBRONZE"
			SET_TEXT_BRONZE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			SET_SPRITE_BRONZE(thisPlacement.SpritePlacement[PSER_AWARD_ROW_SPRITE])
		ENDIF
		
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, 294.0000, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		// Draw Value
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT+PIXEL_X_TO_FLOAT(22))
		DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sValue, FALSE, TRUE)
		DRAW_2D_SPRITE("Shared", "MedalDot_32", thisPlacement.SpritePlacement[PSER_AWARD_ROW_SPRITE], FALSE)
		// Draw Label
		SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		DRAW_TEXT(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSER_AWARD")
		
// ...Mission Failure
	ELSE
		// Wrap to Rect set early for Line Count code
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		// Check String Length to set Row Height
		SET_TEXT_LEADING(12)
		SET_TEXT_STYLE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING("PSER_FAILED_3")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(PS_Main.myChallengeData.Title)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(PS_HUD_GET_FAIL_REASON_LABEL())
		iCellSpans[iCellCur] = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(ADJUST_SCREEN_X_POSITION(thisPlacement.TextPlacement[eCurLabel].x), ADJUST_SCREEN_Y_POSITION(thisPlacement.TextPlacement[eCurLabel].y))
		iCellsTaken += iCellSpans[iCellCur]
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, 294.0000, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		// Draw Label
		DRAW_TEXT_WITH_TWO_STRINGS(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSER_FAILED_3", PS_Main.myChallengeData.Title, PS_HUD_GET_FAIL_REASON_LABEL())
	ENDIF
	
	eLastRect = eCurRect
	eLastValue = eCurValue 
	eLastLabel = eCurLabel 
	eCurRect  += INT_TO_ENUM(PSER_ENDSCREEN_RECT, 1)
	eCurValue += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
	eCurLabel += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
	iCellCur++
		
		
// Current Time/Distance/Formation Completion
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken) OR 
		Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget) OR
		Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
		
		iCellSpans[iCellCur] = 1
		iCellsTaken += iCellSpans[iCellCur]
		
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[eLastRect].y + thisPlacement.RectPlacement[eLastRect].h/2) + 2, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		
		// Values wrapped and positioned here due to complex text drawing methods
		thisPlacement.TextPlacement[eCurValue].y = thisPlacement.TextPlacement[eLastValue].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		
		// Time
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
			sLabel = "PSER_TIME"
			DRAW_TEXT_TIMER(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, PS_HUD_FORMAT_INTEGER_SCORE(ROUND(PS_Main.myPlayerData.elapsedTime * 1000.0)), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE)
		// Distance
		ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
			sLabel = "PSER_DISTANCE"
			IF PS_Main.myPlayerData.Multiplier > 1
				DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myPlayerData.LandingDistance/PS_Main.myPlayerData.Multiplier), 2, FONT_RIGHT)
			ELSE
				DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myPlayerData.LandingDistance), 2, FONT_RIGHT)
			ENDIF
		ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion)
			sLabel = "PSER_FORMCOM"
			DRAW_TEXT_WITH_NUMBER(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_INTEGER_SCORE(GetPilotSchoolScore_4_formationCompletion(PS_Main.myChallengeData, PS_Main.myPlayerData)), FONT_RIGHT)
		ENDIF
	
		// Draw Label
		thisPlacement.TextPlacement[eCurLabel].y = thisPlacement.TextPlacement[eLastLabel].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sLabel)
		
		eLastRect = eCurRect
		eLastValue = eCurValue 
		eLastLabel = eCurLabel 
		eCurRect  += INT_TO_ENUM(PSER_ENDSCREEN_RECT, 1)
		eCurValue += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		eCurLabel += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		iCellCur++
	ENDIF		
		
		
//multiplier for skydiving
	IF PS_Main.myPlayerData.Multiplier > 1
		iCellSpans[iCellCur] = 1
		iCellsTaken += iCellSpans[iCellCur]
		
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[eLastRect].y + thisPlacement.RectPlacement[eLastRect].h/2) + 2, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		
		// Values wrapped and positioned here due to complex text drawing methods
		thisPlacement.TextPlacement[eCurValue].y = thisPlacement.TextPlacement[eLastValue].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		
		// Draw Value
		
		DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSER_X", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myPlayerData.Multiplier), 2, FONT_RIGHT)
		
		sLabel = "PSER_MULT"
		// Draw Label
		thisPlacement.TextPlacement[eCurLabel].y = thisPlacement.TextPlacement[eLastLabel].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sLabel)
		
		eLastRect = eCurRect
		eLastValue = eCurValue 
		eLastLabel = eCurLabel 
		eCurRect  += INT_TO_ENUM(PSER_ENDSCREEN_RECT, 1)
		eCurValue += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		eCurLabel += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		iCellCur++
	ENDIF
	
//scored distance (if we got a multiplier for skydiving)
	IF PS_Main.myPlayerData.Multiplier > 1
		iCellSpans[iCellCur] = 1
		iCellsTaken += iCellSpans[iCellCur]
		
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[eLastRect].y + thisPlacement.RectPlacement[eLastRect].h/2) + 2, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		
		// Values wrapped and positioned here due to complex text drawing methods
		thisPlacement.TextPlacement[eCurValue].y = thisPlacement.TextPlacement[eLastValue].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		
		// Distance
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
			sLabel = "PSER_SDISTANCE"
			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myPlayerData.LandingDistance), 2, FONT_RIGHT)
		ENDIF
	
		// Draw Label
		thisPlacement.TextPlacement[eCurLabel].y = thisPlacement.TextPlacement[eLastLabel].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sLabel)
		
		eLastRect = eCurRect
		eLastValue = eCurValue 
		eLastLabel = eCurLabel 
		eCurRect  += INT_TO_ENUM(PSER_ENDSCREEN_RECT, 1)
		eCurValue += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		eCurLabel += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		iCellCur++
	ENDIF	
				
// Best Time/Distance/Formation
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken) OR
		Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget) OR	
		Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion) 
		
		
		iCellSpans[iCellCur] = 1
		iCellsTaken += iCellSpans[iCellCur]
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[eLastRect].y + thisPlacement.RectPlacement[eLastRect].h/2) + 2, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		
		// Value wrapped and positioned here due to complex text drawing methods
		thisPlacement.TextPlacement[eCurValue].y = thisPlacement.TextPlacement[eLastValue].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		
		// Time
		IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
			sLabel = "PSM_BTIME"
			DRAW_TEXT_TIMER(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, PS_HUD_FORMAT_INTEGER_SCORE(ROUND(g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].elapsedTime * 1000.0)), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", FALSE, TRUE )
		// Distance
		ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
			sLabel = "PSM_BDIST"
			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].landingDistance), 2, FONT_RIGHT)
		ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_4_formationCompletion) 
			sLabel = "PSM_BFORM"
			DRAW_TEXT_WITH_NUMBER(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_HUD_FORMAT_INTEGER_SCORE(GetPilotSchoolScore_4_formationCompletion(PS_Main.myChallengeData, g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class])), FONT_RIGHT)
		ENDIF
		
		// Draw Label
		thisPlacement.TextPlacement[eCurLabel].y = thisPlacement.TextPlacement[eLastLabel].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, sLabel)
		
		eLastRect = eCurRect
		eLastValue = eCurValue 
		eLastLabel = eCurLabel 
		eCurRect  += INT_TO_ENUM(PSER_ENDSCREEN_RECT, 1)
		eCurValue += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		eCurLabel += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		iCellCur++
	ENDIF
	
// Checkpoints (if applicable)
	IF g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].CheckpointCount >= 0
		iCellSpans[iCellCur] = 1
		iCellsTaken += iCellSpans[iCellCur]
		
		// Draw Row
		PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[eLastRect].y + thisPlacement.RectPlacement[eLastRect].h/2) + 2, 254.0000, 27.0000 * iCellSpans[iCellCur]-2, TRUE)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
		
		// Draw Value
		thisPlacement.TextPlacement[eCurValue].y = thisPlacement.TextPlacement[eLastLabel].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		DRAW_TEXT_WITH_NUMBER(thisPlacement.TextPlacement[eCurValue], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", PS_Main.myPlayerData.CheckpointCount, FONT_RIGHT)
					
		// Draw Label
		thisPlacement.TextPlacement[eCurLabel].y = thisPlacement.TextPlacement[eLastLabel].y + PIXEL_Y_TO_FLOAT(27.0 * iCellSpans[iCellCur-1])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[eCurRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		DRAW_TEXT(thisPlacement.TextPlacement[eCurLabel], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PSER_CHECKPTS")
		
		eLastRect = eCurRect
//		eLastValue = eCurValue 
//		eLastLabel = eCurLabel 
		eCurRect  += INT_TO_ENUM(PSER_ENDSCREEN_RECT, 1)
//		eCurValue += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
//		eCurLabel += INT_TO_ENUM(PSER_ENDSCREEN_TEXT, 1)
		iCellCur++
	ENDIF
			
// Draw Filler Cell
	PIXEL_POSITION_AND_SIZE_RECT(thisPlacement.RectPlacement[eCurRect], 257.0000, FLOAT_Y_TO_PIXEL(thisPlacement.RectPlacement[PSER_DYNAMIC_ROW_RECT_0].y - thisPlacement.RectPlacement[PSER_DYNAMIC_ROW_RECT_0].h/2) + iCellsTaken * 27, 254.0000, 27.0000 * (iCellMax - iCellsTaken)-2, TRUE)
	DRAW_RECTANGLE(thisPlacement.RectPlacement[eCurRect])
	
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_BRONZE_RECT])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_SILVER_RECT])
	DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_GOLD_RECT])
	
	INT iBestChallengeScore = Pilot_School_Data_Get_Score(PS_Main.myChallengeData, g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class] )
	
	//medals
	IF iBestChallengeScore >= iSCORE_FOR_GOLD
		DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PSER_MEDAL_BRONZE_SPRITE], FALSE)
		DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PSER_MEDAL_SILVER_SPRITE], FALSE)
		DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Gold_128", thisPlacement.SpritePlacement[PSER_MEDAL_GOLD_SPRITE], FALSE)
	ELIF iBestChallengeScore >= iSCORE_FOR_SILVER
		DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PSER_MEDAL_BRONZE_SPRITE], FALSE)
		DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Silver_128", thisPlacement.SpritePlacement[PSER_MEDAL_SILVER_SPRITE], FALSE)
		DRAW_2D_SPRITE("shared", "Medal_Empty_128", thisPlacement.SpritePlacement[PSER_MEDAL_GOLD_SPRITE], FALSE)
	ELIF iBestChallengeScore >= iSCORE_FOR_BRONZE
		DRAW_2D_SPRITE("pilotSchool", "FlightSchool_Bronze_128", thisPlacement.SpritePlacement[PSER_MEDAL_BRONZE_SPRITE], FALSE)
		DRAW_2D_SPRITE("shared", "Medal_Empty_128", thisPlacement.SpritePlacement[PSER_MEDAL_SILVER_SPRITE], FALSE)
		DRAW_2D_SPRITE("shared", "Medal_Empty_128", thisPlacement.SpritePlacement[PSER_MEDAL_GOLD_SPRITE], FALSE)
	ELSE
		DRAW_2D_SPRITE("shared", "Medal_Empty_128", thisPlacement.SpritePlacement[PSER_MEDAL_BRONZE_SPRITE], FALSE)
		DRAW_2D_SPRITE("shared", "Medal_Empty_128", thisPlacement.SpritePlacement[PSER_MEDAL_SILVER_SPRITE], FALSE)
		DRAW_2D_SPRITE("shared", "Medal_Empty_128", thisPlacement.SpritePlacement[PSER_MEDAL_GOLD_SPRITE], FALSE)
	ENDIF
	
	//medal times
	SET_TEXT_BLACK(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	//bronze time
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_0_timeTaken)
		DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_BRONZE_SUB_RECT])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PSER_MEDAL_BRONZE_SUB_RECT])
			IF PS_Main.myChallengeData.BronzeTime > 0
				DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PSER_MEDAL_BRONZE_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, ROUND(PS_Main.myChallengeData.BronzeTime* 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
			ELSE
				DRAW_TEXT_WITH_ALIGNMENT(thisPlacement.TextPlacement[PSER_MEDAL_BRONZE_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sFINISH", TRUE, FALSE)
			ENDIF
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		
		//silver time
		DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_SILVER_SUB_RECT])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PSER_MEDAL_SILVER_SUB_RECT])
			DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PSER_MEDAL_SILVER_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, ROUND(PS_Main.myChallengeData.SilverTime * 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		
		//gold time
		DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_GOLD_SUB_RECT])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PSER_MEDAL_GOLD_SUB_RECT])
			DRAW_TEXT_TIMER(thisPlacement.TextPlacement[PSER_MEDAL_GOLD_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, ROUND(PS_Main.myChallengeData.GoldTime * 1000.0), TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS, "", TRUE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	
	ELIF Pilot_School_Data_Is_Goal_Bit_Set(PS_Main.myChallengeData, FSG_2_distanceFromTarget)
	
		DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_BRONZE_SUB_RECT])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PSER_MEDAL_BRONZE_SUB_RECT])
//			DRAW_TEXT(thisPlacement.TextPlacement[PSER_MEDAL_BRONZE_SUB_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sBRONZE")
			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PSER_MEDAL_BRONZE_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY,  "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myChallengeData.BronzeDistance), 2, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		
		//silver time
		DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_SILVER_SUB_RECT])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PSER_MEDAL_SILVER_SUB_RECT])
//			DRAW_TEXT(thisPlacement.TextPlacement[PSER_MEDAL_SILVER_SUB_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sSILVER")
			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PSER_MEDAL_SILVER_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY,  "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myChallengeData.SilverDistance), 2, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		
		//gold time
		DRAW_RECTANGLE(thisPlacement.RectPlacement[PSER_MEDAL_GOLD_SUB_RECT])
		SET_TEXT_WRAPPED_TO_RECT(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, thisPlacement.RectPlacement[PSER_MEDAL_GOLD_SUB_RECT])
//			DRAW_TEXT(thisPlacement.TextPlacement[PSER_MEDAL_GOLD_SUB_TEXT], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "PS_sGOLD")
			DRAW_TEXT_WITH_FLOAT(thisPlacement.TextPlacement[PSER_MEDAL_GOLD_SUB_TIME], thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY,  "NUMBER", PS_HUD_FORMAT_FLOAT_SCORE(PS_Main.myChallengeData.GoldDistance), 2, FONT_CENTRE)
		CLEAR_TEXT_WRAPPED(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
	ENDIF
	//reset text color	
	SET_TEXT_WHITE(thisPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)

ENDPROC

//PROC PS_DISPLAY_LEADERBOARD()
//		//leaderboard stuff		
////	SET_SC_LEADERBOARD_TITLE(PS_UI_Leaderboard, "SCLB_TITLE")
////	
////	SET_SC_LEADERBOARD_HEADER(PS_UI_Leaderboard, SECTION_WORLD, 0, SLOT_LAYOUT_3_STATS, "", "", "", "", ICON_POINT, ICON_TIME, ICON_POSITION)
////	SET_SC_LEADERBOARD_SLOT_3_COLUMNS(PS_UI_Leaderboard, SECTION_WORLD, 0, "Rob", SC_LEADERBOARD_MAKE_INT_PRETTY(g_savedGlobals.sFlightSchoolData.PlayerData[g_current_selected_PilotSchool_class].CheckpointCount), SC_LEADERBOARD_MAKE_INT_PRETTY(ROUND(g_savedGlobals.sFlightSchoolData.PlayerData[g_current_selected_PilotSchool_class].ElapsedTime)), SC_LEADERBOARD_MAKE_INT_PRETTY(1))
//////	SET_SC_LEADERBOARD_SLOT_3_COLUMNS(PS_UI_Leaderboard, SECTION_WORLD, 1, "NegaRob", SC_LEADERBOARD_MAKE_INT_PRETTY(sRoundInfo.sScoreData.iP1Score - 1), "Machine Gun", 2)
////	HIGHLIGHT_SC_LEADERBOARD_SLOT(PS_UI_Leaderboard, SECTION_WORLD, 0, TRUE)
////	SET_SC_LEADERBOARD_HEADER(PS_UI_Leaderboard, SECTION_FRIEND, 1, SLOT_LAYOUT_3_STATS, "", "", "", "", ICON_POINT, ICON_TIME, ICON_POSITION)
//////	SET_SC_LEADERBOARD_SLOT(PS_UI_Leaderboard, SECTION_FRIEND, 0, SLOT_LAYOUT_3_STATS, "Rob", SC_LEADERBOARD_MAKE_INT_PRETTY(sRoundInfo.sScoreData.iP1Score), "Pistol", 1)
//////	SET_SC_LEADERBOARD_SLOT(PS_UI_Leaderboard, SECTION_FRIEND, 1, SLOT_LAYOUT_3_STATS, "WWWWWWWWWWWWWWWW", SC_LEADERBOARD_MAKE_INT_PRETTY(sRoundInfo.sScoreData.iP1Score-10), "AK-47", 58)
//////	SET_SC_LEADERBOARD_SLOT(PS_UI_Leaderboard, SECTION_FRIEND, 2, SLOT_LAYOUT_3_STATS, "BPP", SC_LEADERBOARD_MAKE_INT_PRETTY(sRoundInfo.sScoreData.iP1Score-50), "Laser", 922)
//////	HIGHLIGHT_SC_LEADERBOARD_SLOT(PS_UI_Leaderboard, SECTION_FRIEND, 0, TRUE)
////	SET_SC_LEADERBOARD_HEADER(PS_UI_Leaderboard, SECTION_CREW, 2, SLOT_LAYOUT_LABEL_NO_ICON, "", "", "", "")
////	
//	DISPLAY_SC_LEADERBOARD_UI(PS_UI_Leaderboard)
//ENDPROC


PROC PS_HUD_SFX_PLAY_NAV_SELECT()
	PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

PROC PS_HUD_SFX_PLAY_NAV_BACK()
	PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

PROC PS_PLAY_WOOSH_SOUND()
	PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC PS_CLEANUP_PLAYER_AND_VEHICLES()

	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		IF bRetryChallenge
			PRINTLN("SETTING PLAYER TO START COORDS!")
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vStartPosition)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), vStartRotation.z)
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), PS_MENU_PLAYER_COORD)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), PS_MENU_PLAYER_HEADING)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
		DELETE_VEHICLE(PS_Main.myVehicle)
		PRINTLN("DELETE_VEHICLE(PS_Main.myVehicle)")
	ELSE
		PRINTLN("PS_Main.myVehicle doesn't exist")
	ENDIF
	
	IF DOES_ENTITY_EXIST(PS_Main.myPilotPed)
		DELETE_PED(PS_Main.myPilotPed)
	ENDIF
	
	INT i = 0
	REPEAT FORMATION_PLANE_COUNT i
		IF DOES_ENTITY_EXIST(PS_Main.vehFormationPlanes[i])
			DELETE_VEHICLE(PS_Main.vehFormationPlanes[i])
		ENDIF
		IF DOES_ENTITY_EXIST(PS_Main.pedFormationPilots[i])
			DELETE_PED(PS_Main.pedFormationPilots[i])
		ENDIF
	ENDREPEAT

	

ENDPROC 


/// PURPOSE:
///    Lets the script know if the medal toast is displaying
/// RETURNS:
///    TRUE if displaying, FALSE when finished.
FUNC INT UPDATE_PS_MEDAL_TOAST(PILOT_SCHOOL_MEDAL eMedal)
	PRINTLN("UPDATE_PS_MEDAL_TOAST called")
	
	IF (eMedal = PS_BRONZE)
		BEGIN_TEXT_COMMAND_THEFEED_POST(PS_Main.myChallengeData.Title)
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_FlightSchool", 0, HUD_COLOUR_BRONZE, "HUD_MED_UNLKED")
	ELIF (eMedal = PS_SILVER)
		BEGIN_TEXT_COMMAND_THEFEED_POST(PS_Main.myChallengeData.Title)
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_FlightSchool", 0, HUD_COLOUR_SILVER, "HUD_MED_UNLKED")
	ELIF (eMedal = PS_GOLD)
		BEGIN_TEXT_COMMAND_THEFEED_POST(PS_Main.myChallengeData.Title)
		RETURN END_TEXT_COMMAND_THEFEED_POST_AWARD("MPMedals_FEED", "Feed_Medal_FlightSchool", 0, HUD_COLOUR_GOLD, "HUD_MED_UNLKED")
	ENDIF

	RETURN -1
ENDFUNC


PROC PS_SETUP_SCORECARD()
		
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(MG_BIG_MESSAGE_CENTERED)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(PS_UI_BigMessage.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(PS_UI_BigMessage.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("PSER_SUCCESS")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(PS_Main.myChallengeData.Title)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(100.0) // Alpha
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // Centered
	END_SCALEFORM_MOVIE_METHOD()
	
	// Store the duration.
	PS_UI_BigMessage.iDuration = 5000
	
	CANCEL_TIMER(PS_UI_BigMessage.movieTimer)
	bTransitionUp = FALSE
	bTransitionOut = FALSE
ENDPROC

FUNC BOOL PS_NEW_DRAW_SCORECARD()
	// Make sure our timer is active.
	FLOAT fTimerVal = 0.0
	IF NOT IS_TIMER_STARTED(PS_UI_BigMessage.movieTimer)
		RESTART_TIMER_NOW(PS_UI_BigMessage.movieTimer)
	ELSE
		fTimerVal = GET_TIMER_IN_SECONDS(PS_UI_BigMessage.movieTimer)
	ENDIF
	
	// Do the transition up here too.
	IF NOT bTransitionUp
		IF fTimerVal > 1.0
//			PRINTLN("PS_NEW_DRAW_SCORECARD - Transition up: ", fTimerVal)
//			BEGIN_SCALEFORM_MOVIE_METHOD(PS_UI_BigMessage.siMovie, "TRANSITION_UP")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(MG_BIG_MESSAGE_ANIM_TIME_SLOW)
//			END_SCALEFORM_MOVIE_METHOD()
			bTransitionUp = TRUE
		ENDIF
	ENDIF
	
	// Draw victory message. When it's done, move on.
	IF NOT bTransitionOut
		IF fTimerVal > 4.75
			PRINTLN("PS_NEW_DRAW_SCORECARD - Transition out: ", fTimerVal)
			BEGIN_SCALEFORM_MOVIE_METHOD(PS_UI_BigMessage.siMovie, "TRANSITION_OUT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.5)
			END_SCALEFORM_MOVIE_METHOD()
			bTransitionOut = TRUE
		ENDIF
	ENDIF
	
	// Draw.
	IF HAS_SCALEFORM_MOVIE_LOADED(PS_UI_BigMessage.siMovie)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(PS_UI_BigMessage.siMovie, 255, 255, 255, 255)
//		FLOAT yat = GET_ASPECT_RATIO_MODIFIER()*0.25
//		DRAW_SCALEFORM_MOVIE(PS_UI_BigMessage.siMovie,0.5,yat,AR16_9/ GET_SCREEN_ASPECT_RATIO(),1,255,255,255,255)
	ENDIF
	
	// Are we done?
	IF fTimerVal > 5
		PRINTLN("PS_NEW_DRAW_SCORECARD - All done: ", fTimerVal)
		CANCEL_TIMER(PS_UI_BigMessage.movieTimer)
		bTransitionUp = FALSE
		bTransitionOut = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

INT iShotNumber = 0

FUNC VECTOR PS_GET_DYNAMIC_CAM_POINT()
//vOffsetPos 1 is... << 23.8064, 47.7415, 5.5222 >>
//vOffsetPos 2 is... << 0.2155, 43.6974, 3.3072 >>
//vOffsetPos 3 is... << 10.1127, 20.6987, -11.624 >>
//vOffsetPos 4 is... << 7.9834, 8.93713, 2.73109 >>
//	
//	VECTOR vPlayerPos = <<-223.4833, -1347.9786, 95.3049>> 
//	VECTOR vCameraPos = <<-199.6769, -1300.2372, 100.8271>>
//	VECTOR vOffsetPos = vCameraPos - vPlayerPos
//	PRINTLN("vOffsetPos 1 is... ", vOffsetPos)
//	
//	vPlayerPos = <<-223.3007, -1347.2432, 95.5018>>
//	vCameraPos = <<-223.0852, -1303.5458, 98.8090>>
//	vOffsetPos = vCameraPos - vPlayerPos
//	PRINTLN("vOffsetPos 2 is... ", vOffsetPos)
//	
//	vPlayerPos = <<-223.0476, -1346.2299, 95.7784>>
//	vCameraPos = <<-212.9349, -1325.5311, 84.1544>>
//	vOffsetPos = vCameraPos - vPlayerPos
//	PRINTLN("vOffsetPos 3 is... ", vOffsetPos)
//	
//	vPlayerPos = <<-222.9368, -1345.7886, 95.9008>>
//	vCameraPos = <<-214.9534, -1336.8514, 98.6319>>
//	vOffsetPos = vCameraPos - vPlayerPos
//	PRINTLN("vOffsetPos 4 is... ", vOffsetPos)

	
	IF g_current_selected_PilotSchool_class = PSC_DaringLanding
		
//		[Script] [02044574]  Shot 0 : << -164.526, -135.083, 3.8445 >>
//		[Script] [02044574]  Shot 1 : << -120.316, -71.0352, 5.9547 >>
//		[Script] [02044574]  Shot 2 : << -43.3296, -4.95361, 5.4488 >>
//		[Script] [02044574]  Shot 3 : << 90.8501, 73.9043, 4.0146 >>
//		[Script] [02044575] Offset is.. << -120.316, -71.0352, 5.9547 >>
//		
	
//
//		VECTOR tempVector = <<-2052.5925, 4494.4443, 60.8479>> - <<-1888.0665, 4629.5278, 57.0034>>
//		PRINTLN(" Shot 0 : ", tempVector)
//		tempVector = <<-2008.3827, 4558.4927, 62.9581>> - <<-1888.0665, 4629.5278, 57.0034>>
//		PRINTLN(" Shot 1 : ", tempVector)
//		tempVector = <<-1931.3961, 4624.5742, 62.4522>> - <<-1888.0665, 4629.5278, 57.0034>>
//		PRINTLN(" Shot 2 : ", tempVector)
//		tempVector = <<-1797.2164, 4703.4321, 61.0180>> - <<-1888.0665, 4629.5278, 57.0034>>
//		PRINTLN(" Shot 3 : ", tempVector)
//			
		VECTOR vprints, vcam, vpos
	
		IF PS_Main.myPlayerData.LandingDistance < 60.0
			PRINTLN("PS_Main.myPlayerData.LandingDistance < 60.0")
			vcam = <<-2072.5339, 4497.3003, 64.7865>>
			vpos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ELIF PS_Main.myPlayerData.LandingDistance > 225.0
			PRINTLN("PS_Main.myPlayerData.LandingDistance > 225.0")
			vcam = <<-1775.6553, 4775.4155, 63.8575>>
			vpos = <<-1798.6398, 4720.8994, 59.4579>>
		ELSE
			PRINTLN("PS_Main.myPlayerData.LandingDistance is between 60.0 and 225.0")
			vcam = <<-1792.3553, 4684.2622, 66.2932>>
			vpos = <<-1829.9429, 4686.9795, 56.0048>>
		ENDIF
				
		vprints = vcam - vpos
		PRINTLN("Offset is.. ", vprints)
		PRINTLN("Coord for camera should be ... ", GET_ENTITY_COORDS(PLAYER_PED_ID()) + vprints)
		RETURN vprints
		
	ELIF g_current_selected_PilotSchool_class = PSC_chuteOntoMovingTarg
		PRINTLN("Dynamic shot: ", <<-971.4432, -3377.0222, 15.4142>> - <<-969.2137, -3355.6130, 15.8357>>)
		RETURN (<<-971.4432, -3377.0222, 15.4142>> - <<-969.2137, -3355.6130, 15.8357>>)
	ELSE
		iShotNumber = GET_RANDOM_INT_IN_RANGE(0, 4)
		SWITCH iShotNumber
			CASE 0 PRINTLN(" Shot 0 : << 23.8064, 47.7415, 5.5222 >>")
				RETURN << 23.8064, 47.7415, 5.5222 >>
			CASE 1 PRINTLN(" Shot 1 : << 0.2155, 43.6974, 3.3072 >>")
				RETURN << 0.2155, 75.6974, 3.3072 >>
			CASE 2 PRINTLN(" Shot 2 : << 10.1127, 20.6987, -11.624 >>")
				RETURN << 10.1127, 55.6987, -5.624 >>
			CASE 3 PRINTLN(" Shot 3 : << 7.9834, 8.93713, 2.73109 >>")
				RETURN << 7.9834, 45.93713, 2.73109 >>
		ENDSWITCH
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC



FUNC BOOL IS_DYNAMIC_CAM_POINT_VALID(VECTOR vStart, VECTOR vOffset)
	SHAPETEST_INDEX camTest = START_SHAPE_TEST_CAPSULE(vStart, vStart + vOffset, 0.125)
	PRINTLN("Performing shapetest to see if cam at ", vStart + vOffset, " is valid")
	INT iHitSomething = 0
	VECTOR vPoop
	ENTITY_INDEX ePoop
	SHAPETEST_STATUS testResult = GET_SHAPE_TEST_RESULT(camTest, iHitSomething, vPoop, vPoop, ePoop)
	IF testResult = SHAPETEST_STATUS_NONEXISTENT
		PRINTLN("SHAPETEST_STATUS_NONEXISTENT so returning cam as invalid.")
		RETURN FALSE
	ELIF testResult = SHAPETEST_STATUS_RESULTS_NOTREADY
		PRINTLN("SHAPETEST_STATUS_RESULTS_NOTREADY so returning cam as invalid.")
		RETURN FALSE
	ELIF testResult = SHAPETEST_STATUS_RESULTS_READY
		IF iHitSomething = 1
			PRINTLN("SHAPETEST_STATUS_RESULTS_READY but shapetest hit someething so returning cam as invalid.")
			RETURN FALSE
		ELSE
			PRINTLN("SHAPETEST_STATUS_RESULTS_READY and returning cam as valid.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("no shapetest results?")
	RETURN FALSE
	
ENDFUNC


FUNC VECTOR GET_RANDOM_DYNAMIC_CAM_OFFSET(VECTOR vPos, FLOAT fRadius)
	VECTOR vOffset

	INT iDynamicCamOffsetIndex = 0
	//check offsets in order
	SWITCH iDynamicCamOffsetIndex
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
			PRINTLN("get back and up")
			vOffset = fRadius * CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<45, 0, 180>>)
			IF IS_DYNAMIC_CAM_POINT_VALID(vPos, vOffset)
				BREAK
			ENDIF
		
			PRINTLN("get back and left")
			vOffset = fRadius * CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<45, 0, 180 - 45>>)
			IF IS_DYNAMIC_CAM_POINT_VALID(vPos, vOffset)
				BREAK
			ENDIF
		
			PRINTLN("get back and right")
			vOffset = fRadius * CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<45, 0, 180 + 45>>)
			IF IS_DYNAMIC_CAM_POINT_VALID(vPos, vOffset)
				BREAK
			ENDIF
		
			PRINTLN("get fwd and under")
			vOffset = fRadius * CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-45, 0, 0>>)
			IF IS_DYNAMIC_CAM_POINT_VALID(vPos, vOffset)
				BREAK
			ENDIF
		
			PRINTLN("get game cam")
			vOffset = GET_GAMEPLAY_CAM_COORD() - vPos
			BREAK
	ENDSWITCH
	
	PRINTLN("returning offset ... ", vOffset)
	RETURN vOffset
ENDFUNC

FUNC VECTOR GET_VALID_RANDOM_DYNAMIC_CAM_OFFSET()
		//init and perform first attempt
	VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	FLOAT fRadius = 20.0
	VECTOR vOffset = GET_RANDOM_DYNAMIC_CAM_OFFSET(vPlayer, fRadius)
	BOOL isPointValid = IS_DYNAMIC_CAM_POINT_VALID(vPlayer, vOffset)
	INT attemptCount = 1
	
	WHILE NOT isPointValid AND attemptCount < 5
		vOffset = GET_RANDOM_DYNAMIC_CAM_OFFSET(vPlayer, fRadius)
		isPointValid = IS_DYNAMIC_CAM_POINT_VALID(vPlayer, vOffset)
		attemptCount++
		WAIT(0)
	ENDWHILE
	
	IF NOT isPointValid
		vOffset = GET_GAMEPLAY_CAM_COORD() - vPlayer
	ENDIF
	
	PRINTLN("Returning ", vOffset, " as the cam coord")
	RETURN vOffset
ENDFUNC

FUNC VECTOR GET_VALID_DYNAMIC_CAM_POINT()
	
	//init and perform first attempt
	VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vOffset = PS_GET_DYNAMIC_CAM_POINT()
	BOOL isPointValid = IS_DYNAMIC_CAM_POINT_VALID(vPlayer, vOffset)
	INT attemptCount = 1
	
	WHILE NOT isPointValid AND attemptCount < 5
		vOffset = PS_GET_DYNAMIC_CAM_POINT()
		isPointValid = IS_DYNAMIC_CAM_POINT_VALID(vPlayer, vOffset)
		attemptCount++
		WAIT(0)
	ENDWHILE
	
	IF NOT isPointValid
		vOffset = GET_VALID_RANDOM_DYNAMIC_CAM_OFFSET()
	ENDIF
	
	PRINTLN("Returning ", vOffset, " as the cam coord")
	RETURN vOffset
	
ENDFUNC

FLOAT fLerpValueX = 0.2, fLerpValueZ = 0.2
BOOL bTrackLeft, bTrackUp
VECTOR vprojected = <<0, 0, 0>>
VECTOR vcameraup = <<0, 0, 0>>
VECTOR vcamright = <<0, 0, 0>>
VECTOR vcamfwd = <<0, 0, 0>>

PROC PS_DETERMINE_PLANE_TRACKING_CAM_DIRECTION(CAMERA_INDEX trackingCam)
	VECTOR vPFwd, vPPos, vCRight, vCUp, vCPos, vCRot, vTemp
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PRINTLN("Getting entity matrix from the vehicle the player is in")
		GET_ENTITY_MATRIX(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vPFwd, vTemp, vTemp, vPPos) //unit vectors
	ELIF NOT IS_ENTITY_DEAD(PS_FlatBedTrailer) AND NOT IS_ENTITY_DEAD(PS_FlatBedTruck)
		AND (IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), PS_FlatBedTrailer) OR IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), PS_FlatBedTruck))
		PRINTLN("Getting entity matrix from the truck the player is on")
		GET_ENTITY_MATRIX(PS_FlatBedTruck, vPFwd, vTemp, vTemp, vPPos) //unit vectors		
	ELSE
		PRINTLN("Getting entity matrix from the player")
		GET_ENTITY_MATRIX(PLAYER_PED_ID(), vPFwd, vTemp, vTemp, vPPos) //unit vectors
	ENDIF
	PRINTLN("vPFwd is.... ", vPFwd)
	PRINTLN("vPPos is.... ", vPPos)
	
	vCRot = GET_CAM_ROT(trackingCam)
	vCPos = GET_CAM_COORD(trackingCam)
	vcamfwd = NORMALISE_VECTOR(CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCRot))
	vCRight = ROTATE_VECTOR_ABOUT_Z_ORTHO(vcamfwd, ROTSTEP_270)
	vCUp = -CROSS_PRODUCT(vcamfwd, vCRight)
	
	PRINTLN("vcamfwd is... ", vcamfwd)
	PRINTLN("vCRight is... ", vCRight)
	PRINTLN("vCUp is... ", vCUp)
	
	vcamfwd *= 10.0
	vcameraup = vCUp * 10.0
	vcamright = vCRight * 10.0
	//init lerp vals
	fLerpValueX = 0.2
	fLerpValueZ = 0.5

	VECTOR vPlaneToCam = (GET_CAM_COORD(trackingCam) - vPPos)
	VECTOR vPProj = vPPos + (ABSF(DOT_PRODUCT(vPlaneToCam, vPFwd)) * vPFwd)
	
	IF vPProj.z > vCPos.z
		bTrackUp = TRUE
		PRINTLN("Plane is going towards a point above the center of the screen")
		PRINTLN("Setting camera to TRACK UP")
	ELSE
		bTrackUp = FALSE
		PRINTLN("Setting camera to TRACK DOWN")
		PRINTLN("Plane is going towards a point below the center of the screen")	
	ENDIF
	
	PRINTLN("vPProj is... ", vPProj)
	PRINTLN("vCPos is... ", vCPos)
	vprojected = vPProj
	FLOAT fDotProductRight, fDotProductUp
	
	fDotProductRight = DOT_PRODUCT(vPProj, vCRight)
	fDotProductUp = DOT_PRODUCT(vPProj, vCUp)
	
	IF fDotProductRight < 0
		bTrackLeft = TRUE
		PRINTLN("Plane is going towards the left of the center of the screen")
		PRINTLN("Setting camera to TRACK LEFT")
	ELSE
		bTrackLeft = FALSE
		PRINTLN("Plane is going towards the right of the center of the screen")	
		PRINTLN("Setting camera to TRACK RIGHT")
	ENDIF
	
	PRINTLN("fDotProductRight is... ", fDotProductRight)
	PRINTLN("fDotProductUp is... ", fDotProductUp)


ENDPROC

PROC PS_UPDATE_PLANE_TRACKING_CAM(CAMERA_INDEX& trackingCam, VECTOR trackPos)

	IF NOT DOES_CAM_EXIST(trackingCam)
		PRINTLN("Tracking Camera doesnt exist")
		EXIT
	ENDIF
	IF NOT IS_CAM_ACTIVE(trackingCam)
		PRINTLN("Tracking Camera isn't active")
		EXIT
	ENDIF
	
	FLOAT  fDotProductUp
	VECTOR camRot, camPos, vCamUp, vCamToPlane
	FLOAT fNewPitch, fNewHeading
	
	camRot = GET_CAM_ROT(trackingCam)
	camPos = GET_CAM_COORD(trackingCam)

	vCamUp = CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<camRot.x + 90, camRot.y, camRot.z>>)
	vCamToPlane = trackPos - camPos
	vCamToPlane = NORMALISE_VECTOR(vCamToPlane)
	
	fNewPitch = ATAN2(trackPos.z - camPos.z, GET_DISTANCE_BETWEEN_COORDS(trackPos, camPos, FALSE))
	fNewHeading = GET_HEADING_FROM_VECTOR_2D(trackPos.x - camPos.x, trackPos.y - camPos.y)
	fNewHeading = WRAP(fNewHeading, -180, 180)
//	IF fNewHeading > 180
//		fNewHeading -= 360 //convert the heading from 0 to 360 to -180 to 180
//	ENDIF
	
	#IF PS_DRAW_DEBUG_LINES_AND_SPHERES		
	DRAW_DEBUG_SPHERE(trackPos, 1, 255, 0, 0)
	DRAW_DEBUG_SPHERE(camPos, 1, 0, 255, 0)	
	DRAW_DEBUG_SPHERE(vprojected, 1, 0, 0, 255)	
	DRAW_DEBUG_LINE(trackPos, camPos)
	DRAW_DEBUG_LINE(trackPos, vprojected)
	PRINTLN("vcameraup is... ", vcameraup)
	DRAW_DEBUG_LINE(camPos, camPos + vcameraup, 255, 0, 0)
	DRAW_DEBUG_LINE(camPos, camPos + vcamright, 192, 0, 0)
	DRAW_DEBUG_LINE(camPos, camPos + vcamfwd,128, 0, 0)
	#ENDIF
	#IF NOT PS_DRAW_DEBUG_LINES_AND_SPHERES
	vprojected = vprojected
	vcameraup = vcameraup
	vcamright = vcamright
	#ENDIF
	
	fDotProductUp = DOT_PRODUCT(CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<fNewPitch, camRot.y, camRot.z>>), vCamUp)
	
	FLOAT fNewPitchDiff = fNewPitch - camRot.x

	//because of the jump from Loop HUGE to TINY
	IF fNewPitchDiff > 270.0
		fNewPitchDiff -= 360.0
	ELIF fNewPitchDiff < -270.0
		fNewPitchDiff += 360.0
	ENDIF
	
	fNewPitch = camRot.x + fNewPitchDiff
			
	IF (bTrackUp AND fNewPitch > camRot.x) OR (!bTrackUp AND fNewPitch < camRot.x)
//		PRINTLN("Old pitch is... ", camRot.x, " and new pitch is... ", fNewPitch)
		IF fDotProductUp > 0
//			PRINTLN("Going up, so pitch should be updating")
		ELIF fDotProductUp < 0
//			PRINTLN("Going down, so pitch should be updating")			
		ELSE
//			PRINTLN("Dot product is 0. Is the pitch the same??")
		ENDIF
		fLerpValueX *= 0.9
		camRot.x = LERP_FLOAT(camRot.x, fNewPitch, fLerpValueX)
	ELSE
//		PRINTLN("Old pitch is... ", camRot.x, " and new pitch is... ", fNewPitch)
//		PRINTLN("fDotProductUp is... ", fDotProductUp)
//		PRINTLN("Not adjusting pitch")
	ENDIF
	
	
	FLOAT fNewHeadingDiff = fNewHeading - camRot.z
	
//	PRINTLN("camRot.z is ... ", camRot.z)
//	PRINTLN("fNewHeading is ... ", fNewHeading)
//	PRINTLN("fNewHeadingDiff is ... ", fNewHeadingDiff)

	//because of the jump from Loop HUGE to TINY
	IF fNewHeadingDiff > 270.0
		fNewHeadingDiff -= 360.0
	ELIF fNewHeadingDiff < -270.0
		fNewHeadingDiff += 360.0
	ENDIF
	
	fNewHeading = camRot.z + fNewHeadingDiff
	
//	PRINTLN("fNewHeading = camRot.z + fNewHeadingDiff is... ", fNewHeading)
	
	
	
	IF (bTrackLeft AND fNewHeadingDiff > 0) OR (!bTrackLeft AND fNewHeadingDiff < 0)
//		PRINTLN("Old heading is... ", camRot.z, " and new heading is... ", fNewHeading)
		
//		IF (!bTrackLeft AND fNewHeadingDiff < 0)
//			PRINTLN("Going right, so heading should be updating")
//		ELIF (bTrackLeft AND fNewHeadingDiff > 0)
//			PRINTLN("Going left, so heading should be updating")
//		ELSE
//			PRINTLN("Dot product is 0. Is the heading the same??")
//		ENDIF
		fLerpValueZ *= 0.9
//		PRINTLN("BEFORE LERP: Old heading (camRot.z) is ... ", camRot.z)
		camRot.z = LERP_FLOAT(camRot.z, fNewHeading, fLerpValueZ)
//		PRINTLN("AFTER LERP: New heading (camRot.z) is ... ", camRot.z)
//	ELSE
//		PRINTLN("Old heading is... ", camRot.z, " and new heading is... ", fNewHeading)
//		PRINTLN("camRot.z - fNewHeading is... ", camRot.z - fNewHeading)
//		PRINTLN("Not adjusting heading")
	ENDIF
	
	
	SET_CAM_ROT(trackingCam, camRot)
	

ENDPROC


//FUNC CAMERA_INDEX PS_GET_DYNAMIC_PLANE_CAMERA()
//	VECTOR vPlayerPos1 = <<-223.4833, -1347.9786, 30.1225>>
//	VECTOR vCameraPos1 = <-199.6769, -1300.2372, 100.8271>>
//	GET_DIRECTION
//ENDFUNC

VECTOR vTrackingPosition, vCamPosition, vCamRotation
FLOAT fCamFOV, fDelayToStartTracking = 0.0

/// PURPOSE:
///    Updates score card/end screen
/// PARAMS:
///    PS_Struct_Data - 
PROC PS_HUD_UPDATE_SCORE_CARD()
//	VECTOR vTempRotation
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	BOOL bPointCam = TRUE
	
	SWITCH eScoreCardState
		CASE PS_ENDING_INIT
			#IF IS_DEBUG_BUILD
				DEBUG_MESSAGE("Update_Ending_Screen : ENDING_INIT")
			#ENDIF
			
			PS_UI_MedalToast.siMovie = REQUEST_MG_MEDAL_TOAST()
			PS_UI_BigMessage.siMovie = REQUEST_MG_BIG_MESSAGE()
			CLEAR_HELP()
			CLEAR_PRINTS()
			RESTART_TIMER_NOW(PS_Main.tCountdownTimer)
			eScoreCardState = PS_ENDING_SETUP_CAMERA
			BREAK
		
		
		CASE PS_ENDING_SETUP_CAMERA
			IF IS_PLAYER_PLAYING(PLAYER_ID()) AND NOT Pilot_School_Data_Has_Fail_Reason()
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					ANIMPOSTFX_PLAY("MinigameEndMichael", 0, FALSE)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					ANIMPOSTFX_PLAY("MinigameEndFranklin", 0, FALSE)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					ANIMPOSTFX_PLAY("MinigameEndTrevor", 0, FALSE)
				ENDIF
				
				IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle) AND IS_ENTITY_IN_AIR(PS_Main.myVehicle)
					TASK_VEHICLE_MISSION_COORS_TARGET(PLAYER_PED_ID(), PS_Main.myVehicle, <<PS_Main.myChallengeData.vStaticCamPos.x, PS_Main.myChallengeData.vStaticCamPos.y, PS_Main.myChallengeData.vStaticCamPos.z+15>>, MISSION_GOTO, GET_ENTITY_SPEED(PS_Main.myVehicle), DRIVINGMODE_PLOUGHTHROUGH, 10.0, 10.0)
				ENDIF
				
//				MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)

				IF PS_Main.myChallengeData.eEndCamType = PS_END_CAMERA_STATIC_SHOT
					vCamPosition = PS_Challenges[g_current_selected_PilotSchool_class].vStaticCamPos
					vCamRotation = PS_Challenges[g_current_selected_PilotSchool_class].vStaticCamRot
					fCamFOV = PS_Challenges[g_current_selected_PilotSchool_class].fStaticCamFOV
					PS_EndCutCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPosition, vCamRotation, fCamFOV)
				ELIF PS_Main.myChallengeData.eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
					
					IF g_current_selected_PilotSchool_class = PSC_DaringLanding
						IF PS_Main.myPlayerData.LandingDistance < 60.0
							PRINTLN("PS_Main.myPlayerData.LandingDistance < 60.0")
							vCamPosition = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VALID_DYNAMIC_CAM_POINT()
							vCamRotation = <<-2.4588, 0.1874, -74.7957>> 
							bPointCam = TRUE
							fCamFOV = 41.1450
						ELIF PS_Main.myPlayerData.LandingDistance > 225.0
							PRINTLN("PS_Main.myPlayerData.LandingDistance > 225.0")
							vCamPosition = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VALID_DYNAMIC_CAM_POINT()
							vCamRotation = <<-5.4571, 0.2719, 159.8754>> 
							bPointCam = TRUE
							fCamFOV = 41.1450
						ELSE
							PRINTLN("PS_Main.myPlayerData.LandingDistance > 60 and < 225)")
							vCamPosition = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VALID_DYNAMIC_CAM_POINT()
							vCamRotation = <<-10.0662, 0.2719, 105.7053>>
							bPointCam = FALSE
							fCamFOV = 41.1450
						ENDIF
					ELIF g_current_selected_PilotSchool_class = PSC_chuteOntoMovingTarg
						fCamFOV = 38.0338 
						vCamPosition = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VALID_DYNAMIC_CAM_POINT()
					ELSE
						fCamFOV = 41.1450 
						vCamPosition = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VALID_DYNAMIC_CAM_POINT()
					ENDIF
					PRINTLN("Cam position is...", vCamPosition)
					PS_EndCutCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPosition, vCamRotation, fCamFOV)
				ELSE
					PS_EndCutCam1 = GET_GAME_CAMERA_COPY()
				ENDIF
				
				SET_CAM_ACTIVE(PS_EndCutCam1, TRUE)
				
				IF bPointCam
					PRINTLN("pointing cam")
					POINT_CAM_AT_COORD(PS_EndCutCam1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
				
				SHAKE_CAM(PS_EndCutCam1, "HAND_SHAKE", 0.07)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				DELETE_CHECKPOINT(PS_Main.flash_checkpoint)
				PS_Main.bFlashCheckpointMissed 		= FALSE
				PS_Main.bDrawCheckpointMarkerFlash 	= FALSE
			
				eScoreCardState = PS_ENDING_POINT_DYNAMIC_CAM
			ELSE
				IF DOES_CAM_EXIST(PS_EndCutCam)
					DESTROY_CAM(PS_EndCutCam)
				ENDIF
				IF DOES_CAM_EXIST(PS_EndCutCam1)
					DESTROY_CAM(PS_EndCutCam1)
				ENDIF
				PLAY_MISSION_COMPLETE_AUDIO("DEAD") 
//				SET_SCALEFORM_BIG_MESSAGE_WITH_STRING_IN_STRAPLINE(PS_UI_BigMessage, "PSER_FAIL", "STRING", PS_HUD_GET_FAIL_REASON_LABEL(), -1, HUD_COLOUR_FREEMODE, MG_BIG_MESSAGE_CENTERED)
				PRINTLN("GOING TO ENDING_FAIL_EFFECTS STATE!")
				MG_INIT_FAIL_FADE_EFFECT(PS_UI_FailFadeEffect, TRUE)
				sPSFailStrapline = PS_HUD_GET_FAIL_REASON_LABEL()
				eScoreCardState = PS_ENDING_FAIL_EFFECTS
			ENDIF
			BREAK
		
		CASE PS_ENDING_POINT_DYNAMIC_CAM
			STOP_CAM_POINTING(PS_EndCutCam1)
			START_TIMER_NOW_SAFE(PS_Main.myChallengeData.EndCamTimer)
			eScoreCardState = PS_ENDING_DETERMINE_TRACKING
			BREAK
			
		CASE PS_ENDING_DETERMINE_TRACKING	
			IF TIMER_DO_ONCE_WHEN_READY(PS_Main.myChallengeData.EndCamTimer, fDelayToStartTracking)
				PS_DETERMINE_PLANE_TRACKING_CAM_DIRECTION(PS_EndCutCam1)
				START_TIMER_NOW_SAFE(PS_Main.myChallengeData.EndCamTimer)
				START_TIMER_NOW_SAFE(PS_Main.myChallengeData.ForceEndPanCamTimer)
				START_TIMER_NOW_SAFE(PS_Main.myChallengeData.CamTrackingTimer)
				vTrackingPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
				eScoreCardState = PS_ENDING_HOLD_SHOT
			ENDIF
			BREAK
			
		CASE PS_ENDING_HOLD_SHOT
			IF GET_TIMER_IN_SECONDS_SAFE(PS_Main.myChallengeData.CamTrackingTimer) >= 2.0
				IF IS_ENTITY_ON_SCREEN(PLAYER_PED_ID()) AND IS_ENTITY_IN_AIR(PLAYER_PED_ID())
					vTrackingPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					RESTART_TIMER_AT(PS_Main.myChallengeData.EndCamTimer, 1.0)
				ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	AND IS_ENTITY_ON_SCREEN(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					vTrackingPosition = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					RESTART_TIMER_AT(PS_Main.myChallengeData.EndCamTimer, 1.0)
				ELIF DOES_ENTITY_EXIST(PS_FlatBedTrailer) AND NOT IS_ENTITY_DEAD(PS_FlatBedTrailer) AND IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), PS_FlatBedTrailer) AND IS_ENTITY_ON_SCREEN(PLAYER_PED_ID())
					vTrackingPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ELIF IS_ENTITY_ON_SCREEN(PLAYER_PED_ID())
					vTrackingPosition = <<0, 0, 0>>//GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
				IF NOT ARE_VECTORS_ALMOST_EQUAL(vTrackingPosition, <<0, 0, 0>>)
					PS_UPDATE_PLANE_TRACKING_CAM(PS_EndCutCam1, vTrackingPosition)
				ENDIF
			ENDIF
			IF GET_TIMER_IN_SECONDS_SAFE(PS_Main.myChallengeData.EndCamTimer) >= 2.0
			OR GET_TIMER_IN_SECONDS_SAFE(PS_Main.myChallengeData.ForceEndPanCamTimer) >= 3.0
				SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
				CANCEL_TIMER(PS_Main.myChallengeData.CamTrackingTimer)
				CANCEL_TIMER(PS_Main.myChallengeData.ForceEndPanCamTimer)
				MAKE_AUTOSAVE_REQUEST()
				QUITUI_TRANSITION_OUT(quitUI, 0)
				eScoreCardState = PS_ENDING_SFX_AND_SPLASH_SETUP				
			ENDIF
			BREAK
			
		CASE PS_ENDING_SFX_AND_SPLASH_SETUP 
			PS_SETUP_SCORECARD()
			MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
			eScoreCardState = PS_ENDING_QUICK_PAN_SETUP				
			BREAK
					
		CASE PS_ENDING_QUICK_PAN_SETUP
			WHILE NOT IS_MISSION_COMPLETE_READY_FOR_UI()
				wait(0)
			ENDWHILE
			RESTART_TIMER_NOW(PS_Main.myChallengeData.EndCamTimer)
			IF DOES_CAM_EXIST(PS_EndCutCam)
				DESTROY_CAM(PS_EndCutCam)
			ENDIF
			ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
			STOP_CAM_SHAKING(PS_EndCutCam1)
			vCamPosition = GET_CAM_COORD(PS_EndCutCam1)
			vCamRotation = GET_CAM_ROT(PS_EndCutCam1)
			PS_EndCutCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<vCamPosition.x, vCamPosition.y, vCamPosition.z + 1000 >>, <<90, vCamRotation.y, vCamRotation.z>>, fCamFOV)
			SET_CAM_ACTIVE_WITH_INTERP(PS_EndCutCam, PS_EndCutCam1, 500)
			PS_PLAY_WOOSH_SOUND()
			IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
				IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
					FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
					SET_ENTITY_VELOCITY(PS_Main.myVehicle, VECTOR_ZERO)
					SET_ENTITY_VISIBLE(PS_Main.myVehicle, FALSE)
					SET_ENTITY_COLLISION(PS_Main.myVehicle, FALSE)
				ENDIF
			ENDIF
			eScoreCardState = ENDING_TOAST_SETUP
			BREAK
			
			CASE ENDING_TOAST_SETUP
			//when the scaleform splash text is done
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			RESTART_TIMER_NOW(PS_Main.myChallengeData.EndCamTimer)
			eScoreCardState = PS_ENDING_QUICK_PAN_UPDATE				
			BREAK
		
			
		CASE PS_ENDING_QUICK_PAN_UPDATE
			PS_NEW_DRAW_SCORECARD()
			IF NOT IS_CAM_INTERPOLATING(PS_EndCutCam) AND NOT IS_CAM_INTERPOLATING(PS_EndCutCam1)
				//delete the plane and put the player in a safe place now that the camera is not looking at the plane or player.
				IF DOES_ENTITY_EXIST( GET_PLAYER_PED(PLAYER_ID()) )
					SET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID())))
				ENDIF
								
				IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
					DELETE_VEHICLE(PS_Main.myVehicle)
				ENDIF
				
				IF iCurrentChallengeScore >= iSCORE_FOR_GOLD
					PLAY_SOUND_FRONTEND(-1, "MEDAL_GOLD", "HUD_AWARDS")
					PRINTLN("PLAYING: MEDAL_GOLD")
				ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER
					PLAY_SOUND_FRONTEND(-1, "MEDAL_SILVER", "HUD_AWARDS")
					PRINTLN("PLAYING: MEDAL_SILVER")
				ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
					PLAY_SOUND_FRONTEND(-1, "MEDAL_BRONZE", "HUD_AWARDS")
					PRINTLN("PLAYING: MEDAL_BRONZE")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "FLIGHT_SCHOOL_LESSON_PASSED", "HUD_AWARDS")
				ENDIF
				
				eScoreCardState = PS_ENDING_SFX_AND_SPLASH_UPDATE
			ENDIF
			BREAK
			
			CASE PS_ENDING_SFX_AND_SPLASH_UPDATE
			IF PS_NEW_DRAW_SCORECARD()
				ANIMPOSTFX_STOP("MinigameTransitionIn")
				ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
				bRetryChallenge = FALSE
				bKeepVehicleSetup = FALSE	
				eScoreCardState=PS_ENDING_CLEANUP					
			ENDIF
			BREAK
								
		CASE PS_ENDING_FAIL_EFFECTS
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			IF MG_UPDATE_FAIL_FADE_EFFECT(PS_UI_FailFadeEffect, PS_UI_FailSplash, PS_UI_BigMessage, "PSER_FAIL", sPSFailStrapline, bRetryChallenge)
				PRINTLN("IS_CURRENT_MINIGAME_SET_TO_BYPASS_RESPAWN_CUTSCENE() is (1 for true)", IS_CURRENT_MINIGAME_SET_TO_BYPASS_RESPAWN_CUTSCENE())
				bKeepVehicleSetup = bRetryChallenge	
				eScoreCardState = PS_ENDING_CLEANUP				
			ENDIF		
			BREAK
			
		CASE PS_ENDING_CLEANUP
			//clear

			CLEANUP_MG_MEDAL_TOAST(PS_UI_MedalToast)
			SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)

			CLEANUP_MG_BIG_MESSAGE(PS_UI_BigMessage)
			IF DOES_CAM_EXIST(PS_EndCutCam1)
				DESTROY_CAM(PS_EndCutCam1)
			ENDIF
			PRINTLN("Medal for this player data is... ", PS_Main.myPlayerData.eMedal)
			IF iCurrentChallengeScore >= iSCORE_FOR_GOLD
				UPDATE_PS_MEDAL_TOAST(PS_GOLD)
			ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER
				UPDATE_PS_MEDAL_TOAST(PS_SILVER)
			ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
				UPDATE_PS_MEDAL_TOAST(PS_BRONZE)
			ENDIF
			
			PS_HUD_SET_SCORECARD_ACTIVE(FALSE)
			eScoreCardState = PS_ENDING_INIT
			BREAK	
	ENDSWITCH
ENDPROC

PROC PS_DELETE_VEHICLE_OFF_SCREEN(VEHICLE_INDEX& thisVehicle, PED_INDEX& thisPilot)
	IF NOT IS_VEHICLE_FUCKED(thisVehicle)
		IF NOT IS_ENTITY_ON_SCREEN(thisVehicle) AND NOT IS_ENTITY_ON_SCREEN(thisVehicle)
			IF DOES_ENTITY_EXIST(thisPilot) AND (NOT IS_ENTITY_DEAD(thisPilot)) AND IS_PED_IN_VEHICLE(thisPilot, thisVehicle)
//				PRINTLN("!!!!!!!!!!!!!!!!detaching ped, (", GET_MODEL_NAME_STRING(GET_ENTITY_MODEL(thisPilot)), ") from this formation vehicle: ", GET_MODEL_NAME_STRING(GET_ENTITY_MODEL(thisVehicle)))
//				SPECIAL_FUNCTION_DO_NOT_USE(thisPilot)
//				PRINTLN("!!!!!!!!!!!!!!!!deleting this ped: ", GET_MODEL_NAME_STRING(GET_ENTITY_MODEL(thisVehicle)))
				DELETE_PED(thisPilot)
			ENDIF
			IF DOES_ENTITY_EXIST(thisVehicle)
//				PRINTLN("!!!!!!!!!!!!!!!!deleting this formation vehicle: ", GET_MODEL_NAME_STRING(GET_ENTITY_MODEL(thisPilot)))
				DELETE_VEHICLE(thisVehicle)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC CAMERA_INDEX PS_GET_RENDERED_CAMERA_COPY()
	RETURN CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
ENDFUNC

PROC PS_HUD_UPDATE_FINISH_CAMERA()

	
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)	
		IF IS_ENTITY_ON_FIRE(PS_Main.myVehicle)
			STOP_ENTITY_FIRE(PS_Main.myVehicle)
		ENDIF
	ENDIF
	
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	
ENDPROC

//******************************************************************************
//******************************************************************************
// Hint Cam Stuff
//******************************************************************************
//******************************************************************************

PROC PS_INIT_HINT_CAM()
	PS_HINT_CAM.bActive = FALSE
	PS_HINT_CAM.Entity = NULL
	PS_HINT_CAM.Coord = VECTOR_ZERO
	PS_HINT_CAM.HintType = PS_HINT_FROM_STUNTPLANE
	PS_HINT_CAM.CustomCam = NULL
	PS_HINT_CAM.fCamTimer = 0
ENDPROC

PROC PS_SET_HINT_CAM_ACTIVE(BOOL bActive, PS_HINT_CAM_ENUM eType = PS_HINT_FROM_STUNTPLANE)
//	PS_HINT_CAM.bActive = bActive
	//using new camera system by alwyn. active state means the actual camera is active, not the system.
	bActive = bActive
	PS_HINT_CAM.HintType = eType //we still want to know what type of hint cam
	IF !bActive
		PS_HINT_CAM.Coord = VECTOR_ZERO
	ENDIF
//	IF NOT bActive
//		KILL_RACE_HINT_CAM(localChaseHintCamStruct)
//	ENDIF
//	SET_CINEMATIC_BUTTON_ACTIVE(!bActive)
ENDPROC

PROC PS_INCREASE_HINT_CAM_TIMER()
	PS_HINT_CAM.fCamTimer += GET_FRAME_TIME()
ENDPROC

FUNC FLOAT PS_GET_HINT_CAM_TIMER()
	RETURN PS_HINT_CAM.fCamTimer
ENDFUNC

PROC PS_RESET_HINT_CAM_TIMER()
	PS_HINT_CAM.fCamTimer = 0
ENDPROC

PROC PS_SET_HINT_CAM_TYPE(PS_HINT_CAM_ENUM eType)
	PS_HINT_CAM.HintType = eType
ENDPROC

PROC PS_SET_HINT_CAM_ENTITY(ENTITY_INDEX eThisEntity)
	PS_HINT_CAM.Entity = eThisEntity
ENDPROC

FUNC BOOL PS_IS_POINT_TOO_CLOSE_TO_PLAYER(VECTOR vPos)
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF

	FLOAT fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
	
	IF VDIST2(vPos, GET_PLAYER_COORDS(PLAYER_ID())) < (150.0 + fPlayerSpeed * fPlayerSpeed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PS_GET_NEXT_CHECKPOINT()

PROC PS_SET_HINT_CAM_COORD(VECTOR vThisPos)
	IF IS_VECTOR_ZERO(vThisPos)
		vThisPos = PS_GET_CURRENT_CHECKPOINT()	
	ENDIF
	VECTOR vNextPos = PS_GET_NEXT_CHECKPOINT()
	
	IF PS_IS_POINT_TOO_CLOSE_TO_PLAYER(vThisPos)
		PRINTLN("This point is too close: ", vThisPos, " Setting to next instead: ", vNextPos)
		PS_HINT_CAM.Coord = vNextPos
	ELSE
		PRINTLN("not too close: ", vThisPos)
		PS_HINT_CAM.Coord = vThisPos
	ENDIF
ENDPROC

PROC PS_CLEAR_HINT_CAM_TARGET()
	PS_HINT_CAM.Entity = NULL
	PS_HINT_CAM.Coord = VECTOR_ZERO
ENDPROC

PROC PS_KILL_HINT_CAM()
	PS_INIT_HINT_CAM()
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
ENDPROC

FUNC BOOL PS_IS_HINT_CAM_BUTTON_PRESSED()
	RETURN IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)// OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)//IS_BUTTON_PRESSED(PAD1, CIRCLE)
ENDFUNC

FUNC BOOL PS_IS_HINT_CAM_BUTTON_JUST_PRESSED()
	RETURN IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)// OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)//IS_BUTTON_PRESSED(PAD1, CIRCLE)
ENDFUNC

FUNC BOOL PS_IS_HINT_CAM_BUTTON_JUST_RELEASED()
	RETURN IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
ENDFUNC

PROC PS_UPDATE_HINT_CAM()
	IF PS_IS_HINT_CAM_BUTTON_JUST_PRESSED()
		IF NOT PS_HINT_CAM.bActive
			PS_HINT_CAM.bActive = TRUE
			bQuickTapActivates = TRUE
		ELSE
			bQuickTapActivates = FALSE
		ENDIF
	
	ELIF PS_IS_HINT_CAM_BUTTON_JUST_RELEASED()
		IF (NOT bQuickTapActivates) OR PS_GET_HINT_CAM_TIMER() >= PS_HINT_CAM_TIMER_THRESHOLD
			IF PS_HINT_CAM.bActive
				PS_HINT_CAM.bActive = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF PS_IS_HINT_CAM_BUTTON_PRESSED()
		PS_INCREASE_HINT_CAM_TIMER()
	ELSE
		PS_RESET_HINT_CAM_TIMER()
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND PS_HINT_CAM.bActive
		
	
		SWITCH PS_HINT_CAM.HintType
			
			CASE PS_HINT_FROM_STUNTPLANE	
				IF NOT IS_VECTOR_ZERO(PS_HINT_CAM.Coord) AND NOT PS_IS_POINT_TOO_CLOSE_TO_PLAYER(PS_HINT_CAM.Coord)
					CONTROL_RACE_HINT_CAM(localChaseHintCamStruct, PS_HINT_CAM.Coord)
				ELSE
					IF DOES_CAM_EXIST(PS_HINT_CAM.CustomCam)
						DESTROY_CAM(PS_HINT_CAM.CustomCam)
					ENDIF
					IF PS_IS_POINT_TOO_CLOSE_TO_PLAYER(PS_HINT_CAM.Coord)
						PS_HINT_CAM.Coord = PS_GET_NEXT_CHECKPOINT()
					ENDIF
					KILL_RACE_HINT_CAM(localChaseHintCamStruct)
					PS_HINT_CAM.bActive = FALSE
					DEBUG_MESSAGE("no coord to point at")
				ENDIF
				BREAK
			
			CASE PS_HINT_FROM_CARGOPLANE
				IF NOT DOES_CAM_EXIST(PS_HINT_CAM.CustomCam)
					PS_HINT_CAM.CustomCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, VECTOR_ZERO, VECTOR_ZERO, 45, TRUE)	
					ATTACH_CAM_TO_ENTITY(PS_HINT_CAM.CustomCam, PS_Main.myVehicle, <<0, 0, -10.0>>)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(PS_HINT_CAM.Entity)
					POINT_CAM_AT_ENTITY(PS_HINT_CAM.CustomCam, PS_HINT_CAM.Entity, VECTOR_ZERO)
					SET_CAM_FOV(PS_HINT_CAM.CustomCam, fCHASE_HINT_CAM_FOV)
				ELIF NOT IS_VECTOR_ZERO(PS_HINT_CAM.Coord)
					POINT_CAM_AT_COORD(PS_HINT_CAM.CustomCam, PS_HINT_CAM.Coord)
					SET_CAM_FOV(PS_HINT_CAM.CustomCam, fCHASE_HINT_CAM_FOV)
				ELSE
					DEBUG_MESSAGE("no entity or coord to point at")
				ENDIF
				BREAK
			
			CASE PS_HINT_FROM_SKYDIVING
				IF DOES_ENTITY_EXIST(PS_HINT_CAM.Entity)
					SET_GAMEPLAY_ENTITY_HINT(PS_HINT_CAM.Entity, VECTOR_ZERO)
				ELIF NOT IS_VECTOR_ZERO(PS_HINT_CAM.Coord)
					SET_GAMEPLAY_COORD_HINT(PS_HINT_CAM.Coord)
				ELSE
					DEBUG_MESSAGE("no entity or coord to point at")
				ENDIF
				BREAK
			
			CASE PS_HINT_FROM_FLOATING
				IF DOES_ENTITY_EXIST(PS_HINT_CAM.Entity) AND NOT IS_ENTITY_DEAD(PS_HINT_CAM.Entity) 
					SET_GAMEPLAY_ENTITY_HINT(PS_HINT_CAM.Entity, VECTOR_ZERO)
				ELIF NOT IS_VECTOR_ZERO(PS_HINT_CAM.Coord)
					SET_GAMEPLAY_COORD_HINT(PS_HINT_CAM.Coord)
				ELSE
					DEBUG_MESSAGE("no entity or coord to point at")
				ENDIF
				BREAK

			DEFAULT
				IF DOES_ENTITY_EXIST(PS_HINT_CAM.Entity)
					IF NOT IS_ENTITY_DEAD(PS_HINT_CAM.Entity)
						CONTROL_RACE_HINT_CAM(localChaseHintCamStruct, GET_ENTITY_COORDS(PS_HINT_CAM.Entity))
					ENDIF
//					SET_GAMEPLAY_ENTITY_HINT(PS_HINT_CAM.Entity, VECTOR_ZERO))
				ELIF NOT IS_VECTOR_ZERO(PS_HINT_CAM.Coord)
					CONTROL_RACE_HINT_CAM(localChaseHintCamStruct, PS_HINT_CAM.Coord)
//					SET_GAMEPLAY_COORD_HINT(PS_HINT_CAM.Coord)
				ELSE
					DEBUG_MESSAGE("no entity or coord to point at")
				ENDIF
				BREAK
		ENDSWITCH	
	ELSE
		IF DOES_CAM_EXIST(PS_HINT_CAM.CustomCam)
			SWITCH PS_HINT_CAM.HintType
				CASE PS_HINT_FROM_CARGOPLANE
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					BREAK
				DEFAULT
					BREAK
			ENDSWITCH
			DESTROY_CAM(PS_HINT_CAM.CustomCam)
		ELSE
			IF PS_IS_POINT_TOO_CLOSE_TO_PLAYER(PS_HINT_CAM.Coord)
				PS_HINT_CAM.Coord = PS_GET_NEXT_CHECKPOINT()
			ENDIF
			KILL_RACE_HINT_CAM(localChaseHintCamStruct)
			PS_HINT_CAM.bActive = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PS_GET_CURRENT_LESSON_PLAYER_BEST_TIME(INT &BestTime, HUD_COLOURS &aBestTimeColour)
	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[g_current_selected_PilotSchool_class], FSG_0_timeTaken)
		FLOAT tempBestTime
		tempBestTime = g_savedGlobals.sFlightSchoolData[GET_PLAYER_PED_ENUM(PLAYER_PED_ID())].PlayerData[g_current_selected_PilotSchool_class].ElapsedTime
		IF tempBestTime <= PS_Challenges[g_current_selected_PilotSchool_class].GoldTime
			aBestTimeColour = HUD_COLOUR_GOLD
			BestTime = FLOOR(tempBestTime*1000)
		ElIF tempBestTime <= PS_Challenges[g_current_selected_PilotSchool_class].SilverTime
			aBestTimeColour = HUD_COLOUR_SILVER
			BestTime = FLOOR(tempBestTime*1000)
		ELIF tempBestTime <= PS_Challenges[g_current_selected_PilotSchool_class].BronzeTime
			aBestTimeColour = HUD_COLOUR_BRONZE
			BestTime = FLOOR(tempBestTime*1000)
		ELSE
			//best time is not medal worthy!
			BestTime = -1
		ENDIF		
		
	ENDIF
ENDPROC

//******************************************************************************
//******************************************************************************
// HUD Update
//******************************************************************************
//******************************************************************************

PROC PS_UPDATE_RACE_HUD_ELEMENT()
	//def values
	INT TimerRunning = -1
	INT RacePos = -1
	INT MaxRacePos = -1
	INT LapNum = -1
	INT MaxLaps = -1
	INT ExtraTime = 0
	INT BestTime = -1
	INT GoalTime = -1
	INT FlashTime = -1
	HUD_COLOURS HudTextColour = HUD_COLOUR_WHITE
	HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW
	HUD_COLOURS aGoalTimeColor = HUD_COLOUR_WHITE
	HUD_COLOURS aBestTimeColor = HUD_COLOUR_WHITE
	INT MeterNumber = -1
	INT MeterMaxNum = -1
	STRING MeterTitle = ""
	HUD_COLOURS MeterColour = HUD_COLOUR_RED
	STRING Title = "" 
	TEXT_LABEL sGoalTitle = "PS_HUD_TIME"
	STRING CheckpointTitle = "PS_HUD_CHPR"
	PODIUMPOS Medal = PS_UI_RaceHud.ePSHUDMedal	// PODIUMPOS_NONE  This changes time to white, always.
//	INT CurCheckpoint = -1
//	INT MaxNumCheckpoints = -1	
//
//	IF PS_UI_RaceHud.bCheckpointCounterVisible AND PS_Main.myCheckpointMgr.isActive
//		MaxNumCheckpoints = PS_GET_TOTAL_CHECKPOINTS()
//		CurCheckpoint = PS_GET_CHECKPOINT_PROGRESS() - 1
//		//		DRAW_CHECKPOINT_HUD(CurCheckpoint, MaxNumCheckpoints, Title,aColour, FlashTime)
//	ENDIF
	IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
		TimerRunning = FLOOR(GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer) * 1000)
	ENDIF
	
	PS_GET_CURRENT_LESSON_PLAYER_BEST_TIME(BestTime, aBestTimeColor)
	
	sGoalTitle = PS_UI_RaceHud.sPSHUDMedalGoal
	GoalTime = FLOOR(PS_UI_RaceHud.fPSHUDMedalTime * 1000)
	ExtraTime = PS_UI_RaceHud.iPSHUDExtraTime*1000
		
	IF PS_UI_RaceHud.bIsHourGlassActive
		MeterNumber = PS_HUD_GET_HOURGLASS_TIMER_PADDING() + PS_HUD_GET_HOURGLASS_TIMER_VALUE()
		MeterMaxNum = PS_HUD_GET_HOURGLASS_MAX_TIME()
		MeterTitle = "PS_REMAINING"
	ENDIF
	
//	DRAW_CHECKPOINT_COUNTDOWN_RACE_HUD(TimerRunning, // RaceTime - The timer
//				Title,   // TimerTitle - The title of the timer. Defaults to TIME with "" passed in
//				LapNum,   // LapNumber - Number of laps
//				MaxLaps,   // LapMaxNumber - Max number of laps
//				"",   // LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
//				RacePos,   // PositionNum - The position Number
//				MaxRacePos,   // PositionMaxNumber - The position maximum number
//				"",   // PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
//				ExtraTime, // ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
//				HudTextColour, //PlacementColour - The position numbers can change colour
//				CurCheckpoint,       // CheckpointNumber - if you have a checkpoint bar the current number
//				MaxNumCheckpoints,    // CheckpointMaxNum - if you have a checkpoint bar, the maximum number
//				CheckpointTitle, // CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
//				CheckpointColour, // CheckpointColour - the colour the bar should be
//				MeterNumber,   // MeterNumber - if you want a meter displayed pass in the current value
//				MeterMaxNum,   // MeterMaxNum - If you want a meter displayed pass in the max value
//				MeterTitle,   // MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
//				MeterColour, // MeterColour - The meter colour
//				BestTime, // BestTime - If you want to show a best time then pass in a millisecond value
//				FixedTitle,   // BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
//				Medal, // MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
//				TRUE,       // DisplayMilliseconds - True if you want the main timer to display milliseconds
//				FlashTime)   // FlashingTime - How long you want the whole hud to flash for.

	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
	DRAW_STUNT_PLANE_HUD(TimerRunning,			///    RaceTime - The timer
				Title,				///    TimerTitle - The title of the timer. Defaults to TIME with "" passed in
				LapNum,				///    LapNumber - Number of laps
				MaxLaps,			///    LapMaxNumber - Max number of laps
				"",					///    LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
				RacePos,			///    PositionNum - The position Number
				MaxRacePos,			///    PositionMaxNumber - The position maximum number
				"",					///    PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
				ExtraTime,			///    ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
				HudTextColour,		///    PlacementColour - The position numbers can change colour
				/*CurCheckpoint*/ -1,		///    CheckpointNumber - if you have a checkpoint bar the current number
				/*MaxNumCheckpoints*/ -1,	///    CheckpointMaxNum - if you have a checkpoint bar, the maximum number
				CheckpointTitle,	///    CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
				CheckpointColour,	///    CheckpointColour - the colour the bar should be
				MeterNumber,		///    MeterNumber - if you want a meter displayed pass in the current value
				MeterMaxNum,		///    MeterMaxNum - If you want a meter displayed pass in the max value
				MeterTitle,			///    MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
				MeterColour,		///    MeterColour - The meter colour
				GoalTime,			///	   Goal Time
				sGoalTitle,			///    Goal Time Title	
				Medal,				///    Goal Medal
				aGoalTimeColor,		///    aGoalTimeColor - Goal timer color
				BestTime,			///    BestTime - If you want to show a best time then pass in a millisecond value
				"",					///    BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
				PODIUMPOS_NONE,		///    MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
				HUD_COLOUR_WHITE,		///	   aBestTimeColor - Best time color 
				TRUE,				///    DisplayMilliseconds - True if you want the main timer to display milliseconds
				FlashTime)			///    FlashingTime - How long you want the whole hud to flash for.
				


ENDPROC

PROC PS_HUD_UPDATE()		

	//keep the minimap blip thing off. must call every frame!
//	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
//
//	PS_DBG_DrawLiteralStringInt("Shot", iShotNumber, 0)
//
//	
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_NONE, "")
//		iShotNumber = IWRAP(iShotNumber+1, -1, 3)
//		PRINTLN("iShotNumber is... ", iShotNumber)
//	ENDIF
//	
	IF PS_UI_RaceHud.bIsTimerVisible
		PS_UPDATE_RACE_HUD_ELEMENT()	
	ENDIF
	
	IF PS_UI_RaceHud.bIsDistActive
		PS_HUD_UPDATE_DIST_HUD()	
	ENDIF
		
	PS_UPDATE_HINT_CAM()
	
	IF PS_UI_ObjectiveMeter.bIsObjMeterVisible
		PS_HUD_UPDATE_OBJECTIVE_METER()
	ELSE
	ENDIF
	
	IF PS_UI_GutterIcon.bIsGutterVisible
//		PS_HUD_UPDATE_GUTTER_ICON()
	ENDIF
	
	IF bIsAltimeterVisible
		PS_HUD_UPDATE_ALITMETER()
	ENDIF
	
	IF bIsAltitudeIndicatorVisible
		PS_HUD_UPDATE_ALTITUDE_INDICATOR()
	ENDIF

	IF bIsEndCutActive
		PS_HUD_UPDATE_FINISH_CAMERA()
	ENDIF
	
	IF bIsScoreCardVisible
		PS_HUD_UPDATE_SCORE_CARD()
	ENDIF	 
ENDPROC

PROC PS_HUD_CLEANUP()
	IF PS_SoundID_Alt_Meter_Alarm <> -1
		ODDJOB_STOP_SOUND(PS_SoundID_Alt_Meter_Alarm)
	ENDIF
	
	STOP_AUDIO_SCENES()
		
	PS_HUD_SET_FINISH_CAMERA_ACTIVE(FALSE)
ENDPROC

