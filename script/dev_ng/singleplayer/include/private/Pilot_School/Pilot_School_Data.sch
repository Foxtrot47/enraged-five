//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Contains shared funcs and procs for data management in the pilot school, including load/save player data
//					and retrieving any stored data (strings, scores, times, etc)
//	
//****************************************************************************************************


//USING "globals.sch"
//USING "rage_builtins.sch"
USING "minigame_uiinputs.sch"
USING "socialclub_leaderboard.sch"
USING "Pilot_School_Definitions.sch"
USING "Pilot_School_Objhelpalogue.sch"
USING "minigame_big_message.sch"
USING "screen_placements.sch"
USING "UIUtil.sch"
USING "script_usecontext.sch"
USING "ps_sc_leaderboard_lib.sch"

//************************************************************
// General Variables & Assets
//************************************************************

VECTOR							VECTOR_ZERO								= <<0.0, 0.0, 0.0>>
MODEL_NAMES 					PilotModel 								= S_M_M_PILOT_02
TEXT_LABEL						txdDictName								= "pilotSchool"
MODEL_NAMES 					VehicleToUse 							= STUNT
BOOL							bUnlockFlightSchoolReward				= FALSE

//************************************************************
// Game Logic
//************************************************************

INT								iPSMusicBits
UI_INPUT_DATA					PS_uiInput //tracks analog input
PS_CHALLENGE_SUBSTATE 			eSubState 								= PS_SUBSTATE_ENTER
structTimer						Fail_Timer_Runway, Fail_Timer_Landed, Fail_Timer_Range, Fail_Timer_Water, Fail_Timer_Idling
VECTOR 							vPilotSchoolSceneCoords
VECTOR							vStartPosition
VECTOR							vStartRotation 
FLOAT							fStartHeading

CAM_VIEW_MODE					ePS_CamViewMode

BLIP_INDEX						strip_blip[10]

STREAMVOL_ID streamVolume

SCENARIO_BLOCKING_INDEX			scenBlockHelipad 						= NULL

structTimer						streamingTimer

INT								iSavedTimeOfDay							= 0
INT								iNumMissedCheckpoints					= 0	
INT								iRangeTimerWarning						= 1
INT								iSceneID
INT								iFailDialogue							= 1
INT								iEndDialogueTimer						= 0
BOOL							bLessonCompleted						= FALSE
BOOL 							bLesonFinishedLineTriggered				= FALSE
BOOL							bRetryChallenge							= FALSE
BOOL							bKeepVehicleSetup						= FALSE
BOOL							bPlayerDataHasBeenUpdated				= FALSE
BOOL							bPlayerInVehicle						= FALSE
BOOL 							bStoppedNearVecEndCoords
BOOL 							bEnteredLocate 							= FALSE
BOOL 							bFinishedChallenge 						= FALSE
BOOL							bGameIsRunning							= TRUE
#IF IS_DEBUG_BUILD	
BOOL 							bPlayerUsedDebugSkip 					= FALSE
#ENDIF
PS_CHALLENGE_SUBSTATE			eGameSubState							= PS_SUBSTATE_ENTER
PILOT_SCHOOL_GAME_MODE			eGameMode								= PS_GAME_MODE_INTRO_SCENE

//pilot model
INT								iCurrentChallengeScore					= 0
FLOAT							GroundOffset
FLOAT							fCheckpointDistAway 					= -1.0

FLOAT							fPlaneLastOrient

PS_MAIN_STRUCT 					PS_Main
INT 							iChallengeScores[NUMBER_OF_PILOT_SCHOOL_CLASSES]

//camera views of stunt and helicopter before the player starts a lesson.
CAM_VIEW_MODE					cvmHelicopter
CAM_VIEW_MODE					cvmStuntPlane

//Out of range vector
VECTOR							vInRangeCoord							= <<0, 0, 0>>

//************************************************************
// Cutscene stuff
//************************************************************

// Skydiving Cutscene
//********************
//VECTOR							PS_CARGOPLANE_PLAYER_START_OFFSET		= <<0.0, -22.58, -3.15>>//<<0.0, -38.1477, -4.8>>//<<0, -39.0, -5.0>> //starts the player -20m behind the center of the plane (right around the cargo doors)
VECTOR 							PS_PARACHUTE_JUMP_CAM_OFFSET_1			= <<-12.6533, -49.3726, -16.0138>> //starts the camera 10m to the right, 40m behind and 10m below the plane
VECTOR 							PS_PARACHUTE_JUMP_CAM_OFFSET_2			= <<-5.0812, -36.9571, -0.4403>>//<<0, -50, -20>> //starts the camera 50m back and 20m below
VECTOR 							PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_1		= <<-10.9340, -47.6023, -14.3079>>
VECTOR 							PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_2		= <<-3.2242, -38.1790, -2.4549>>
PS_PARACHUTE_FLIGHT_CHALLENGE 	eParachutePreview
structTimer						tParachutePreviewTimer

// Preview Cutscenes
//********************
VECTOR 							vPilotSchoolPreviewCoords
VECTOR							vPilotSchoolPreviewRot
VECTOR							vPreviewCamCoords
VECTOR							vPreviewCamDirection
//VECTOR							vChallengeScenePos
//VECTOR							vChallengeSceneRot
FLOAT							fPreviewVehicleSkipTime					= 0.0
FLOAT							fPreviewTime							= PS_DEFAULT_PREVIEW_TIME
BOOL							bSkippedPreview							= FALSE
BOOL							bPreviewCleanupOkay						= FALSE
CAMERA_INDEX					cPS_Parachute_Jump_Cam
//structTimer						tJumpCamTimer
//PS_PARACHUTE_JUMP_CUT_STATE		ePS_Parachute_Jump_State
PED_PARACHUTE_STATE 			ppsPlayerParachuteState
TEXT_PLACEMENT					previewTitlePlacement
TEXT_STYLE						previewTitleStyle
STRING							sPreviewRecordingName = "PilotSchool"
CAMERA_INDEX					PS_CutsceneCamera
structTimer						tPS_PreviewTimer
PS_STARTING_CUTSCENE_STATE		PS_CutsceneSkipState			 		= PS_STARTING_CUTSCENE_STATE_01
PS_PREVIEW_STATE				PS_PreviewState							= PS_PREVIEW_INIT
INT								iPreviewCheckpointIdx					= 0
INT								iPS_PreviewVehicleRecordingID			= 0

//************************************************************
// HUD/UI Stuff
//************************************************************


//Dialogue stuff
//keeps track of dialogue we've already played
INT iPlayedOutEncourageDialogue[PS_FEEDBACK_DIALOGUE_REPLAY_COUNT]
INT iPlayedOutRewardDialogue[PS_FEEDBACK_DIALOGUE_REPLAY_COUNT]
INT iPlayedOutDiscourageDialogue[PS_FEEDBACK_DIALOGUE_REPLAY_COUNT]
 
// Front End UI
//********************
CONST_INT						PS_QUIT_UI_TRANS_TIME 					1000

VECTOR							PS_MENU_PLAYER_COORD 					= <<-1154.1101, -2715.2026, 18.8824>>
//UI_INPUT_DATA					uiInput
PS_CHALLENGE_DATA_STRUCT		PS_Challenges[NUMBER_OF_PILOT_SCHOOL_CLASSES]
PS_PLAYER_DATA_STRUCT			PS_PlayerData[NUMBER_OF_PILOT_SCHOOL_CLASSES]
MEGA_PLACEMENT_TOOLS			PS_UI_Placement
SCALEFORM_INDEX 				quitUI 									= NULL

//SIMPLE_USE_CONTEXT				quitInstructions
SIMPLE_USE_CONTEXT 				menuInstructions
BOOL 							bEndScreenQuitAlert								= FALSE

//structTimer						tPSQuitUITimer
#IF IS_DEBUG_BUILD
	BOOL 						bIsLaunchingStuntTuner 					= FALSE
#ENDIF
BOOL 							bIsLaunchingChallenge 					= FALSE
BOOL 							bShowQuitMenu 							= FALSE
//BOOL 							bDontFadeToMenu							= TRUE //We're going to use this if we need to fade out before returning to the menu (if the player is on screen)
BOOL							bLBToggle								= FALSE
BOOL							bLBViewProfile							= FALSE
//BOOL 							bIsQuitTrans 							= FALSE
//BOOL 							bIsWaitingToQuit 						= FALSE
//BOOL 							bShowQuitInstructions					= FALSE
INT								iHourGlassTimeToRun						= 0
INT								iHourGlassPadding						= 0 //so the hour glass doesn't look empty before reaching 0
structTimer 					tRetryMenuTransition


// End Results UI AKA Score Card
//********************
MEGA_PLACEMENT_TOOLS			PSER_Placement
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID				PSER_Widget
#ENDIF
BOOL							bIsScoreCardVisible 					= FALSE
STRING							PS_Special_Fail_Reason					= ""
SCRIPT_SCALEFORM_MEDAL_TOAST	PS_UI_MedalToast
ENDING_SCREEN_STATE				eScoreCardState							= PS_ENDING_INIT

SCALEFORM_INDEX					PS_UI_Leaderboard
CAMERA_INDEX					PS_EndCutCam, PS_EndCutCam1


PILOT_SCHOOL_CLASSES_ENUM		eLBLessonToWrite
BOOL							bShowingOfflineLBButton

//VECTOR							vFinalCoord

BOOL 							bIsEndCutActive 						= FALSE
BOOL							bTransitionUp
BOOL							bTransitionOut
MG_FAIL_FADE_EFFECT				PS_UI_FailFadeEffect
MG_FAIL_SPLASH					PS_UI_FailSplash
SCRIPT_SCALEFORM_BIG_MESSAGE	PS_UI_BigMessage
STRING							sPSFailStrapline

// HUD
//********************
PS_HUD_OBJECTIVE_BAR_STRUCT		PS_UI_ObjectiveMeter
PS_HUD_GUTTER_ICON_STRUCT		PS_UI_GutterIcon
PS_HUD_RACE_HUD_STRUCT			PS_UI_RaceHud

structPedsForConversation 		PedDispatcher
COUNTDOWN_UI					PS_CountDownUI 

//Altimeter
FLOAT 							fAltitude								= 0
FLOAT							fDistanceLeft							= 0 
FLOAT 							fAltitudeMax							= 100
//Altitude Indicator
FLOAT 							fRoll									= 0
//screen fade
PS_SCREEN_FADE_STATE			eFadeState								= PS_SCREEN_FADE_IDLE
FLOAT							fScreenFadeWaitTime						= PS_SCREEN_FADE_DEFAULT_WAIT
structTimer						tPS_FadeTimer
INT								iScreenFadeTime							= PS_SCREEN_FADE_DEFAULT_TIME
BOOL							bScreenFadeVisible 						= TRUE

//hint cam stuff
PS_HINT_CAM_STRUCT				PS_HINT_CAM
CAMERA_INDEX 					cPS_HintCam
CONST_FLOAT						PS_HINT_CAM_TIMER_THRESHOLD			1.0			// Threshold for going back from hint cam, like Alwyn's stuff does


BOOL							bIsAltimeterVisible 					= FALSE
BOOL							bIsAltitudeIndicatorVisible 			= FALSE
BOOL 							bIsAltimeterActive 						= FALSE
BOOL							bIsAltitudeIndicatorActive				= FALSE
BOOL							bQuickTapActivates


INT 							PS_SoundID_Alt_Meter_Alarm 				= -1
INT 							PS_SoundID_Formation_Alarm				= -1


INT								iInstructionCounter						= 1
INT								iPSObjectiveTextCounter					= 1
INT								iPSHelpTextCounter						= 1
INT								iPSDialogueCounter						= 1

//************************************************************
// Lesson specific stuff
//************************************************************

//Landing

VECTOR							vDialogueMarker1
VECTOR							vDialogueMarker2
VECTOR							vLandingSpot

//Inverted
//structTimer tInvertedLevelOutTimer

//Knifing
PS_OBJECTIVE_TYPE_ENUM			eCurrentKnifingObjective

//Looping
//keep track of tutorial dialogue
INT								iTutorialCounter						= 1
structTimer						tLevelOutTimer
structTimer						tRetryInstructionTimer

//Moving Chute
VEHICLE_INDEX					PS_FlatBedTruck
VEHICLE_INDEX					PS_FlatBedTrailer
PTFX_ID		 					PTFX_SkydivingTargetFlare
PS_TRUCK_LANDING_STATE 			ePS_TruckLandingState 					= PS_TRUCK_LANDING_STATE_01
PS_TRUCK_CRASHING_STATE 		ePS_TruckCrashingState 					= PS_TRUCK_CRASHING_STATE_01
BOOL 							bFinishedTruckLandingScene				= FALSE
BOOL 							bFinishedTruckCrashingScene				= FALSE
BOOL							bRagdollingOnTruck						= FALSE
VECTOR							vPS_MovingTarget							= VECTOR_ZERO


//daring landing
FLOAT 							fCurPlaneSpeed							= -1
BOOL 							bHasPlaneSpedUp 						= FALSE

//skydiving
structTimer						tmrLandingAlpha
//VECTOR							vAlphaPosition
PED_VARIATION_STRUCT 			structPlayerProps

PROC DataTempHack()
	
	#IF IS_DEBUG_BUILD
		PSER_Widget = PSER_Widget
	#ENDIF
	UNUSED_PARAMETER(vPreviewCamDirection)
	PS_PARACHUTE_JUMP_CAM_OFFSET_1 = PS_PARACHUTE_JUMP_CAM_OFFSET_1
	PS_PARACHUTE_JUMP_CAM_OFFSET_2 = PS_PARACHUTE_JUMP_CAM_OFFSET_2
	PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_1 = PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_1
	PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_2 = PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_2
	bRetryChallenge = bRetryChallenge
	PS_CountDownUI = PS_CountDownUI
	cPS_Parachute_Jump_Cam = cPS_Parachute_Jump_Cam
	iCurrentChallengeScore = iCurrentChallengeScore
	PS_Main = PS_Main
	PS_CutsceneSkipState = PS_CutsceneSkipState
	tPS_PreviewTimer = tPS_PreviewTimer
	PS_PreviewState = PS_PreviewState
	bPreviewCleanupOkay = bPreviewCleanupOkay
	bSkippedPreview = bSkippedPreview
	iPreviewCheckpointIdx = iPreviewCheckpointIdx
	fPreviewVehicleSkipTime =fPreviewVehicleSkipTime
	iPS_PreviewVehicleRecordingID = iPS_PreviewVehicleRecordingID
	ppsPlayerParachuteState = ppsPlayerParachuteState
	iPSObjectiveTextCounter = iPSObjectiveTextCounter
	iPSHelpTextCounter = iPSHelpTextCounter
	iPSDialogueCounter = iPSDialogueCounter
	PTFX_SkydivingTargetFlare = PTFX_SkydivingTargetFlare
	bFinishedTruckLandingScene = bFinishedTruckLandingScene
	bFinishedTruckCrashingScene = bFinishedTruckCrashingScene
	iNumMissedCheckpoints = iNumMissedCheckpoints
	fCurPlaneSpeed = fCurPlaneSpeed
ENDPROC

PROC REQUEST_PILOT_SCHOOL_ASSETS()
	REQUEST_ADDITIONAL_TEXT("PSCHOOL", MINIGAME_TEXT_SLOT)
	REQUEST_STREAMED_TEXTURE_DICT("pilotSchool")
	REQUEST_STREAMED_TEXTURE_DICT("PS_Menu")
	REQUEST_STREAMED_TEXTURE_DICT("MPMedals_FEED")
	REQUEST_MINIGAME_COUNTDOWN_UI(PS_CountDownUI)
	quitUI = REQUEST_QUIT_UI()
	PS_UI_Leaderboard = REQUEST_SC_LEADERBOARD_UI()
	
ENDPROC
		
FUNC BOOL HAS_LOADED_PILOT_SCHOOL_ASSETS()
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("pilotSchool")
		AND HAS_THIS_ADDITIONAL_TEXT_LOADED("PSCHOOL", MINIGAME_TEXT_SLOT)
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("PS_Menu")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPMedals_FEED")
		AND HAS_SCALEFORM_MOVIE_LOADED(quitUI)
		AND HAS_SCALEFORM_MOVIE_LOADED(PS_UI_Leaderboard)
		AND HAS_MINIGAME_COUNTDOWN_UI_LOADED(PS_CountDownUI)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC 

PROC RELEASE_PILOT_SCHOOL_ASSETS()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("pilotSchool")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("PS_Menu")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPMedals_FEED")
	RELEASE_MINIGAME_COUNTDOWN_UI(PS_CountDownUI)
	CLEANUP_QUIT_UI(quitUI)
	PS_CLEANUP_LEADERBOARD(PS_UI_Leaderboard)
ENDPROC

//INFO: 
//PARAM NOTES: The index (BitToCheckIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Checks if the integer (IntToCheck) passed has the bit set at the index (BitToCheckIndex) passed.More info..
FUNC BOOL Pilot_School_Data_Is_Goal_Bit_Set(PS_CHALLENGE_DATA_STRUCT thisData, PILOT_SCHOOL_GOAL_BITS BitIndex)
	RETURN IS_BIT_SET(thisData.GoalBits, ENUM_TO_INT(BitIndex))
ENDFUNC

//INFO: 
//PARAM NOTES:The index (BitToSetIndex) passed must be between 0 and 31 or the command will ASSERT.
//PURPOSE: Sets the bit at the index (BitToSetIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Set_Goal_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_GOAL_BITS BitIndex)
	SET_BIT(thisData.GoalBits, ENUM_TO_INT(BitIndex))
ENDPROC

//INFO: 
//PARAM NOTES:The index (BitToClearIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Clear the bit at the index (BitToClearIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Clear_Goal_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_GOAL_BITS BitIndex)
	CLEAR_BIT(thisData.GoalBits, ENUM_TO_INT(BitIndex))
ENDPROC

//INFO: 
//PARAM NOTES: The index (BitToCheckIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Checks if the integer (IntToCheck) passed has the bit set at the index (BitToCheckIndex) passed.More info..
FUNC BOOL Pilot_School_Data_Is_PreReq_Bit_Set(PS_CHALLENGE_DATA_STRUCT thisData, PILOT_SCHOOL_CLASSES_ENUM BitIndex)
	RETURN IS_BIT_SET(thisData.PreReqBits, ENUM_TO_INT(BitIndex))
ENDFUNC

//INFO: 
//PARAM NOTES:The index (BitToSetIndex) passed must be between 0 and 31 or the command will ASSERT.
//PURPOSE: Sets the bit at the index (BitToSetIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Set_PreReq_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_CLASSES_ENUM BitIndex)
	SET_BIT(thisData.PreReqBits, ENUM_TO_INT(BitIndex))
ENDPROC
//INFO: 

//PARAM NOTES:The index (BitToClearIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Clear the bit at the index (BitToClearIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Clear_PreReq_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_CLASSES_ENUM BitIndex)
	CLEAR_BIT(thisData.PreReqBits, ENUM_TO_INT(BitIndex))
ENDPROC

FUNC INT GetPilotSchoolScore_0_timeTaken(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	INT iTempPlayerTime = FLOOR(thisPlayerData.ElapsedTime*1000)//convert to ms and cut off the end
	IF iTempPlayerTime <= 0
		RETURN 0
	ELIF iTempPlayerTime <= FLOOR(thisChallengeData.GoldTime*1000)
		//GOLD
		RETURN iSCORE_FOR_GOLD+10
	ELIF iTempPlayerTime <= FLOOR(thisChallengeData.SilverTime*1000)
		//SILVER
		RETURN iSCORE_FOR_SILVER
	ELIF thisChallengeData.BronzeTime = -1 OR iTempPlayerTime <= FLOOR(thisChallengeData.BronzeTime*1000)
		//BRONZE
		RETURN iSCORE_FOR_BRONZE
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

FUNC INT GetPilotSchoolScore_2_distanceFromTarget(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	IF thisPlayerData.LandingDistance < 0
		RETURN -1
	ENDIF
	IF thisPlayerData.LandingDistance <= thisChallengeData.GoldDistance
		//gold
		RETURN iSCORE_FOR_GOLD+10
	ELIF thisPlayerData.LandingDistance > thisChallengeData.GoldDistance AND thisPlayerData.LandingDistance <= thisChallengeData.SilverDistance
		//Silver
		RETURN iSCORE_FOR_SILVER
	ELIF thisChallengeData.BronzeDistance = -1 OR (thisPlayerData.LandingDistance > thisChallengeData.SilverDistance AND thisPlayerData.LandingDistance <= thisChallengeData.BronzeDistance)
		//bronze
		RETURN iSCORE_FOR_BRONZE
	ELSE
		//grey you suck
		RETURN -1
	ENDIF	
ENDFUNC

FUNC INT GetPilotSchoolScore_3_checkpointsPassed(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	RETURN (thisPlayerData.CheckpointCount*100)/thisChallengeData.TotalCheckpoints
ENDFUNC

FUNC INT GetPilotSchoolScore_4_formationCompletion(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	RETURN 100* ROUND((thisChallengeData.FormationTotal - thisPlayerData.FormationTimer)/thisChallengeData.FormationTotal)
ENDFUNC

PROC Pilot_School_Data_Check_Failed_Goals(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	IF ePSFailReason = PS_FAIL_NO_REASON
		INT tempscore
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
			tempscore = GetPilotSchoolScore_4_formationCompletion(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER WAS OUT OF FORMATION!")
				ePSFailReason = PS_FAIL_FORMATION
			ENDIF	
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
			tempscore = GetPilotSchoolScore_3_checkpointsPassed(thisChallengeData, thisPlayerData)
			IF tempscore < 70	
				DEBUG_MESSAGE("FAILED BC PLAYER MISSED TOO MANY GATES")
				ePSFailReason = PS_FAIL_MISSED_GATES
			ENDIF
		ENDIF

		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
			tempscore = GetPilotSchoolScore_2_distanceFromTarget(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER IS TOO FAR FROM FINISH")
				ePSFailReason = PS_FAIL_TOO_FAR
			ENDIF	
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
			tempscore = GetPilotSchoolScore_0_timeTaken(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER RAN OUT OF TIME!")
				ePSFailReason = PS_FAIL_TIME_UP
			ENDIF	
		ENDIF
		
	ENDIF
ENDPROC

FUNC INT Pilot_School_Data_Get_Score(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData, BOOL bForMenuUse = FALSE)
	IF bForMenuUse
		IF thisChallengeData.LockStatus = PSS_LOCKED
			RETURN -1
		ENDIF
	ENDIF
	
	INT iScores[5]
	INT iGoal = 0

	//gather scores into the array, and keep track of how many goals we're comparing
	//time
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
		IF thisPlayerData.ElapsedTime < 0
			RETURN -1
		ENDIF
		iScores[iGoal] = GetPilotSchoolScore_0_timeTaken(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass time check
			RETURN -1
		ENDIF
		iGoal++
	ENDIF

	//landing dist
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
		iScores[iGoal] = GetPilotSchoolScore_2_distanceFromTarget(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass distance check
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//checkpoints
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
		iScores[iGoal] = GetPilotSchoolScore_3_checkpointsPassed(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass checkpoint count check
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//formation
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
		iScores[iGoal] = GetPilotSchoolScore_4_formationCompletion(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass formation completion check
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//use a bool to make sure we actually have a score to return
	BOOL bReturnLowest = FALSE
	//Set a medal ceiling so your score is calculated by the lowest goal reached
	INT iLowestMedalScore = 101
	INT i = 0
	IF iGoal > 1
		REPEAT iGoal i
			IF iScores[i] >= 0
				IF iScores[i] < iLowestMedalScore
					IF iScores[i] < iSCORE_FOR_BRONZE
						iScores[i] = -1
					ELIF iScores[i] < iSCORE_FOR_SILVER
						iLowestMedalScore = iScores[i] 
					ELIF iScores[i] < iSCORE_FOR_GOLD
						iLowestMedalScore = iScores[i] 
					ELSE
						iLowestMedalScore = iScores[i] 
					ENDIF
					IF iLowestMedalScore = iScores[i]
						//if we actually used one of the scores, make sure we return it
						bReturnLowest = TRUE
					ENDIF
				ELSE
				ENDIF
			ELSE
			ENDIF
		ENDREPEAT
	ELSE
		//we only have one score...
		iGoal = 0
		iLowestMedalScore = iScores[iGoal]
		bReturnLowest = TRUE
	ENDIF

	//make sure we actually got a valid score, and make sure its above a bronze.
	IF bReturnLowest AND iLowestMedalScore > 0	AND iLowestMedalScore < 101	
		RETURN iLowestMedalScore
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

//see if we have a fail reason that occurs AFTER the lesson (for cases where we want to know if the player at least completed the course)
FUNC BOOL Pilot_School_Data_Has_Ignorable_Fail_Reason()
//	SWITCH(ePSFailReason)
//		CASE PS_FAIL_TIME_UP
//			RETURN TRUE
//			BREAK
//	ENDSWITCH
	RETURN FALSE
ENDFUNC


FUNC BOOL Pilot_School_Data_Has_Fail_Reason()
	IF ePSFailReason = PS_FAIL_NO_REASON
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL Pilot_School_Data_Check_Mission_Success(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	Pilot_School_Data_Check_Failed_Goals(thisChallengeData, thisPlayerData)
	IF ePSFailReason = PS_FAIL_NO_REASON //if we haven't gotten a reason to fail yet
		IF Pilot_School_Data_Get_Score(thisChallengeData, thisPlayerData) >= iSCORE_FOR_BRONZE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
	PROC SAVE_PILOT_SCHOOL_STRUCT_TO_NAMED_DEBUG_FILE(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData, string sFilePath, string sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		<RecordNode name = \"", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(thisChallengeData.Title, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" status = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(thisChallengeData.LockStatus), sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" goals = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoalBits, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" preReq = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.PreReqBits, sFilePath, sFileName)
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" elapsedTime = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisPlayerData.ElapsedTime, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" timeGold = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoldTime, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" timeBronze = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.BronzeTime, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" landingDistance = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisPlayerData.LandingDistance, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" distGold = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoldDistance, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" distBronze = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.BronzeDistance, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkpointCount = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisPlayerData.CheckpointCount, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkTotal = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.TotalCheckpoints, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkpointCount = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisPlayerData.FormationTimer, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkTotal = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.FormationTotal, sFilePath, sFileName)
		ENDIF

		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"  />		<!--  ", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(thisChallengeData.Title), sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" -->", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
	ENDPROC
	
	PROC SAVE_PILOT_SCHOOL_STRUCT_TO_DEBUG_FILE(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "temp_debug.txt"		
		SAVE_PILOT_SCHOOL_STRUCT_TO_NAMED_DEBUG_FILE(thisChallengeData, thisPlayerData, sFilePath, sFileName)
	ENDPROC
#ENDIF

PROC Pilot_School_Data_Reset_PS_Struct(PS_PLAYER_DATA_STRUCT thisPlayerData)
	thisPlayerData = thisPlayerData
	thisPlayerData.ElapsedTime = -1
	thisPlayerData.LandingDistance = -1
	thisPlayerData.CheckpointCount = -1
ENDPROC

PROC PS_Load_Pilot_School_Data()
	
	PS_Challenges[PSC_Takeoff].Title = "MGFS_0"
	PS_Challenges[PSC_Takeoff].LessonEnum = PSC_Takeoff
	PS_Challenges[PSC_Takeoff].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 1)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_Takeoff],	(FSG_0_timeTaken))
	PS_Challenges[PSC_Takeoff].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_Takeoff].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_Takeoff].vStaticCamPos = <<-1041.1238, -1678.2046, 82.1664>>
	PS_Challenges[PSC_Takeoff].vStaticCamRot = <<-10.6176, -0.0000, 168.3095>>
	PS_Challenges[PSC_Takeoff].fStaticCamFOV = 36.0423
	PS_Challenges[PSC_Takeoff].GoldTime = 32.0000
	PS_Challenges[PSC_Takeoff].SilverTime = 50.0000
	PS_Challenges[PSC_Takeoff].BronzeTime = 60.0000
	
	PS_Challenges[PSC_Landing].Title = "MGFS_1"
	PS_Challenges[PSC_Landing].LessonEnum = PSC_Landing
	PS_Challenges[PSC_Landing].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 1)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_Landing],	(FSG_0_timeTaken))
	PS_Challenges[PSC_Landing].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_Landing].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_Landing].vStaticCamPos = <<-1626.0165, -2810.9880, 15.3684>>
	PS_Challenges[PSC_Landing].vStaticCamRot = <<1.6590, -0.0000, -52.8546>>
	PS_Challenges[PSC_Landing].fStaticCamFOV = 31.2089
	PS_Challenges[PSC_Landing].GoldTime = 35.0000
	PS_Challenges[PSC_Landing].SilverTime = 55.0000
	PS_Challenges[PSC_Landing].BronzeTime = 70.0000
	
	PS_Challenges[PSC_Inverted].Title = "MGFS_2"
	PS_Challenges[PSC_Inverted].LessonEnum = PSC_Inverted
	PS_Challenges[PSC_Inverted].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 1)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_Inverted],	(FSG_0_timeTaken))
	PS_Challenges[PSC_Inverted].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_Inverted].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
	PS_Challenges[PSC_Inverted].GoldTime = 100.0000
	PS_Challenges[PSC_Inverted].SilverTime = 120.0000
	PS_Challenges[PSC_Inverted].BronzeTime = 140.0000
	
	PS_Challenges[PSC_Knifing].Title = "MGFS_3"
	PS_Challenges[PSC_Knifing].LessonEnum = PSC_Knifing
	PS_Challenges[PSC_Knifing].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_Knifing],	(FSG_0_timeTaken))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_Knifing],	(PSC_Inverted))
	PS_Challenges[PSC_Knifing].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_Knifing].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
	PS_Challenges[PSC_Knifing].GoldTime = 54.0000
	PS_Challenges[PSC_Knifing].SilverTime = 75.0000
	PS_Challenges[PSC_Knifing].BronzeTime = 100.000
	
	PS_Challenges[PSC_loopTheLoop].Title = "MGFS_6"
	PS_Challenges[PSC_loopTheLoop].LessonEnum = PSC_loopTheLoop
	PS_Challenges[PSC_loopTheLoop].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_loopTheLoop],	(FSG_0_timeTaken))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_loopTheLoop],	(PSC_Inverted))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_loopTheLoop],	(PSC_Knifing))
	PS_Challenges[PSC_loopTheLoop].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_loopTheLoop].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
	PS_Challenges[PSC_loopTheLoop].GoldTime = 95.0000
	PS_Challenges[PSC_loopTheLoop].SilverTime = 100.0000
	PS_Challenges[PSC_loopTheLoop].BronzeTime = 125.0000

	PS_Challenges[PSC_FlyLow].Title = "MGFS_4"
	PS_Challenges[PSC_FlyLow].LessonEnum = PSC_FlyLow
	PS_Challenges[PSC_FlyLow].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_FlyLow],	(FSG_0_timeTaken))
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_FlyLow],	(FSG_3_checkpointsPassed))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_FlyLow],	(PSC_Inverted))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_FlyLow],	(PSC_Knifing))
	PS_Challenges[PSC_FlyLow].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_FlyLow].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_FlyLow].vStaticCamPos = <<-1083.5435, -3394.3379, 37.9642>>
	PS_Challenges[PSC_FlyLow].vStaticCamRot = <<-2.3317, -0.0000, -57.5431>>
	PS_Challenges[PSC_FlyLow].fStaticCamFOV = 36.0423
	PS_Challenges[PSC_FlyLow].GoldTime = 135.0000
	PS_Challenges[PSC_FlyLow].SilverTime = 155.0000
	PS_Challenges[PSC_FlyLow].BronzeTime = 180.0000
	PS_Challenges[PSC_FlyLow].TotalCheckpoints = 21
	
	PS_Challenges[PSC_DaringLanding].Title = "MGFS_5"
	PS_Challenges[PSC_DaringLanding].LessonEnum = PSC_DaringLanding
	PS_Challenges[PSC_DaringLanding].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_DaringLanding],	(FSG_2_distanceFromTarget))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_DaringLanding],	(PSC_Inverted))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_DaringLanding],	(PSC_Knifing))
	PS_Challenges[PSC_DaringLanding].eEndscreenType = ENDSCREEN_ENGINE_OFF
	PS_Challenges[PSC_DaringLanding].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
	PS_Challenges[PSC_DaringLanding].GoldDistance = 10.0
	PS_Challenges[PSC_DaringLanding].SilverDistance = 45.0
	PS_Challenges[PSC_DaringLanding].BronzeDistance = -1
	
	PS_Challenges[PSC_heliCourse].Title = "MGFS_7"
	PS_Challenges[PSC_heliCourse].LessonEnum = PSC_heliCourse
	PS_Challenges[PSC_heliCourse].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_heliCourse],	(FSG_0_timeTaken))
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_heliCourse],	(FSG_3_checkpointsPassed))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_heliCourse],	(PSC_Inverted))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_heliCourse],	(PSC_Knifing))
	PS_Challenges[PSC_heliCourse].eEndscreenType = ENDSCREEN_ENGINE_OFF
	PS_Challenges[PSC_heliCourse].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_heliCourse].vStaticCamPos = <<-1176.5013, -2806.8284, 27.4228>> //2nd shot <<-1206.4468, -2908.8740, 16.0006>>
	PS_Challenges[PSC_heliCourse].vStaticCamRot = <<-8.3478, 0.2480, -145.6417>> //2nd shot <<-0.2941, 0.2480, -62.5959>> 
	PS_Challenges[PSC_heliCourse].fStaticCamFOV = 31.6506 //2nd shot 35.5561 
	PS_Challenges[PSC_heliCourse].GoldTime = 160.0000
	PS_Challenges[PSC_heliCourse].SilverTime = 195.0000
	PS_Challenges[PSC_heliCourse].BronzeTime = 225.0000
	PS_Challenges[PSC_heliCourse].TotalCheckpoints = 21
	
	PS_Challenges[PSC_heliSpeedRun].Title = "MGFS_8"
	PS_Challenges[PSC_heliSpeedRun].LessonEnum = PSC_heliSpeedRun
	PS_Challenges[PSC_heliSpeedRun].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_heliSpeedRun],	(FSG_0_timeTaken))
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_heliSpeedRun],	(FSG_3_checkpointsPassed))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_heliSpeedRun],	(PSC_Inverted))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_heliSpeedRun],	(PSC_Knifing))
	PS_Challenges[PSC_heliSpeedRun].eEndscreenType = ENDSCREEN_ENGINE_OFF
	PS_Challenges[PSC_heliSpeedRun].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_heliSpeedRun].vStaticCamPos = <<-1222.3890, -2819.4983, 14.5588>>
	PS_Challenges[PSC_heliSpeedRun].vStaticCamRot = <<2.0598, 0.2480, -114.4249>>
	PS_Challenges[PSC_heliSpeedRun].fStaticCamFOV = 28.7074
	PS_Challenges[PSC_heliSpeedRun].GoldTime = 130.0000
	PS_Challenges[PSC_heliSpeedRun].SilverTime = 180.0000
	PS_Challenges[PSC_heliSpeedRun].BronzeTime = 210.0000
	PS_Challenges[PSC_heliSpeedRun].TotalCheckpoints = 16
	
	PS_Challenges[PSC_parachuteOntoTarget].Title = "MGFS_9"
	PS_Challenges[PSC_parachuteOntoTarget].LessonEnum = PSC_parachuteOntoTarget
	PS_Challenges[PSC_parachuteOntoTarget].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_parachuteOntoTarget], (FSG_2_distanceFromTarget))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_parachuteOntoTarget], (PSC_loopTheLoop))
	PS_Challenges[PSC_parachuteOntoTarget].eEndscreenType = ENDSCREEN_SKYDIVE_LANDING
	PS_Challenges[PSC_parachuteOntoTarget].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_parachuteOntoTarget].vStaticCamPos = <<-1037.2793, -2611.4773, 42.3121>>
	PS_Challenges[PSC_parachuteOntoTarget].vStaticCamRot = <<-4.7096, -0.0000, -60.1639>> 
	PS_Challenges[PSC_parachuteOntoTarget].fStaticCamFOV = 35.2742
	PS_Challenges[PSC_parachuteOntoTarget].GoldDistance = 3.0
	PS_Challenges[PSC_parachuteOntoTarget].SilverDistance = 10.0
	PS_Challenges[PSC_parachuteOntoTarget].BronzeDistance = 20.0000

	PS_Challenges[PSC_chuteOntoMovingTarg].Title = "MGFS_10"
	PS_Challenges[PSC_chuteOntoMovingTarg].LessonEnum = PSC_chuteOntoMovingTarg
	PS_Challenges[PSC_chuteOntoMovingTarg].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_chuteOntoMovingTarg],	(FSG_2_distanceFromTarget))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_chuteOntoMovingTarg],	(PSC_parachuteOntoTarget))
	PS_Challenges[PSC_chuteOntoMovingTarg].eEndscreenType = ENDSCREEN_SKYDIVE_LANDING
	PS_Challenges[PSC_chuteOntoMovingTarg].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
	PS_Challenges[PSC_chuteOntoMovingTarg].GoldDistance = 0.00
	PS_Challenges[PSC_chuteOntoMovingTarg].SilverDistance = 7.00
	PS_Challenges[PSC_chuteOntoMovingTarg].BronzeDistance = 20.0
	
	PS_Challenges[PSC_planeCourse].Title = "MGFS_11"
	PS_Challenges[PSC_planeCourse].LessonEnum = PSC_planeCourse
	PS_Challenges[PSC_planeCourse].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_planeCourse],	(FSG_0_timeTaken))
	Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSC_planeCourse],	(FSG_3_checkpointsPassed))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_planeCourse],	(PSC_Inverted))
	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSC_planeCourse],	(PSC_Knifing))
	PS_Challenges[PSC_planeCourse].eEndscreenType = ENDSCREEN_AUTOPILOT
	PS_Challenges[PSC_planeCourse].eEndCamType = PS_END_CAMERA_STATIC_SHOT
	PS_Challenges[PSC_planeCourse].vStaticCamPos = <<-1718.0992, -2953.9399, 39.7682>>
	PS_Challenges[PSC_planeCourse].vStaticCamRot = <<6.1396, 0.0000, -46.1617>>
	PS_Challenges[PSC_planeCourse].fStaticCamFOV = 35.2742
	PS_Challenges[PSC_planeCourse].GoldTime = 170.0000
	PS_Challenges[PSC_planeCourse].SilverTime = 195.0000
	PS_Challenges[PSC_planeCourse].BronzeTime = 220.0000
	PS_Challenges[PSC_planeCourse].TotalCheckpoints = 19

ENDPROC

