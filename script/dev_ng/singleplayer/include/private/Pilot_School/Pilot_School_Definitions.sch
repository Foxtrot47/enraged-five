//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description: 	Contains shared data structures, enum definitions and constants used by all Pilot School scripts
//	
//****************************************************************************************************

USING "stunt_plane_public.sch"
USING "globals.sch"
USING "script_debug.sch"
USING "end_screen.sch"
USING "minigame_big_message.sch"

//debug info
CONST_INT 			PRINT_DEBUG_INFO_TO_SCREEN					1

//Lessons
CONST_INT			MAX_PILOT_SCHOOL_GATES						50
CONST_INT			PS_MAX_CHECKPOINTS 							50
CONST_INT			iSCORE_FOR_GOLD								90
CONST_INT			iSCORE_FOR_SILVER							80
CONST_INT			iSCORE_FOR_BRONZE							70
CONST_INT 			PS_PERFECT_BONUS 							-2
CONST_INT			PS_SKIPPED_BONUS							5
CONST_FLOAT			PS_MAX_AUTOPILOT_SPEED						60.0
CONST_FLOAT			LOCATE_SIZE_CHECKPOINT						30.0
CONST_FLOAT			CHECKPOINT_SCALE_MULTIPLIER					15.0
CONST_FLOAT 		PS_FAIL_TIME_WATER							3.0
CONST_FLOAT			PS_FAIL_TIME_OUT_OF_RANGE					30.0
CONST_FLOAT			PS_FAIL_DIST_OUT_OF_RANGE_SPECIAL			6100.0
CONST_FLOAT			PS_FAIL_DIST_OUT_OF_RANGE					400.0
CONST_FLOAT			PS_FAIL_TIME_LANDED							5.0
CONST_FLOAT			PS_FAIL_TIME_RUNWAY							5.0
CONST_INT			PS_FAIL_TIME_STUCK							6000
CONST_INT			PS_FAIL_TIME_FORMATION						20
CONST_FLOAT			PS_FAIL_TIME_IDLING							120.0
CONST_INT			PS_END_DIALOGUE_TIME						4500

CONST_FLOAT 		PS_PARA_TARG_SCL_S						   	4.0   	// Draw scale of inner ground target ring
CONST_FLOAT 		PS_PARA_TARG_SCL_M   						9.0   	// Draw scale of middle ground target ring
CONST_FLOAT 		PS_PARA_TARG_SCL_L   						14.0	// Draw scale of outer ground target ring

CONST_INT 		PS_PARA_TARG_COL_R							240
CONST_INT 		PS_PARA_TARG_COL_G							200	
CONST_INT 		PS_PARA_TARG_COL_B							80	
CONST_INT 		PS_PARA_TARG_COL_A							128

//Previews
CONST_INT			iPS_PEVIEW_VEH_REC_ID_TAKEOFF				101
CONST_INT			iPS_PEVIEW_VEH_REC_ID_LANDING				102
CONST_INT			iPS_PEVIEW_VEH_REC_ID_INVERTED				103
CONST_INT			iPS_PEVIEW_VEH_REC_ID_KNIFING				104
CONST_INT			iPS_PEVIEW_VEH_REC_ID_LOOPING				105
CONST_INT			iPS_PEVIEW_VEH_REC_ID_FLY_LOW				106
CONST_INT			iPS_PEVIEW_VEH_REC_ID_DARING				107
CONST_INT			iPS_PEVIEW_VEH_REC_ID_PLANE_COURSE			108
CONST_INT			iPS_PEVIEW_VEH_REC_ID_HELI_COURSE			109
CONST_INT			iPS_PEVIEW_VEH_REC_ID_HELI_SPEED			110
CONST_INT			iPS_PEVIEW_VEH_REC_ID_CHUTE					111
CONST_INT			iPS_PEVIEW_VEH_REC_ID_MOVING_CHUTE			112
CONST_INT			iPS_PEVIEW_VEH_REC_ID_FORMATION				113
CONST_FLOAT			PS_DEFAULT_PREVIEW_TIME						5.0

//Dialogue
CONST_INT 			PS_FEEDBACK_DIALOGUE_REPLAY_COUNT			3
CONST_INT			PS_DIALOGUE_ENCOURAGE_MAX					9
CONST_INT			PS_DIALOGUE_DISCOURAGE_MAX					11
CONST_INT			PS_DIALOGUE_REWARD_MAX						10

//UI
CONST_FLOAT			PS_MENU_PLAYER_HEADING 						328.5558
CONST_FLOAT			SCREEN_CENTER_X								0.5
CONST_FLOAT			SCREEN_CENTER_Y								0.5
CONST_FLOAT			PS_SCORE_CARD_SPLASH_DELAY					0.5
CONST_FLOAT			PS_SCORE_CARD_SPLASH_DURATION				1.0
CONST_FLOAT			PS_SCORE_CARD_DELAY							0.5
CONST_FLOAT			PS_SCORE_CARD_DELAY_TIMEOUT					0.0
CONST_FLOAT			PS_FAILED_TEXT_TIME							3.0
CONST_FLOAT			PS_BONUS_VISIBLE_TIME						2.0
CONST_FLOAT 		PS_SCREEN_FADE_DEFAULT_WAIT					0.0
CONST_FLOAT			PS_COUNTDOWN_DIGIT_DISPLAY_TIME				1.0
CONST_INT			PS_SCREEN_FADE_DEFAULT_TIME					1000

//HUD
CONST_FLOAT			SCREEN_RADIUS_FOR_GUTTER					0.75

//Inverted
CONST_INT 			MAX_INVERTED_CHECKPOINTS 					2

CONST_FLOAT			PS_RETRY_STUNT_ANGLE						75.0			

//CONST_FLOAT 		PS_STUNT_ROLL_PERFECT_MIN					80.0			// The min amount the player needs to get the perfect stunt bonus
//CONST_FLOAT 		PS_STUNT_ROLL_PERFECT_MAX  					110.0			// The max amount the player can roll to get the perfect stunt bonus
//CONST_FLOAT 		PS_STUNT_INVERT_PERFECT_MIN					-170.0			// The min amount the player needs to get the perfect invert stunt bonus
//CONST_FLOAT 		PS_STUNT_INVERT_PERFECT_MAX					170.0			// The max amount the player can be inverted to get the perfet invert stunt bonus
CONST_FLOAT			PS_LEVEL_THRESHOLD							0.9660			// cos(15)
CONST_FLOAT			PS_STUNT_INVERT_PERFECT_THRESHOLD			0.1736          // sin(10)
CONST_FLOAT			PS_STUNT_ROLL_PERFECT_THRESHOLD				0.3420			// sin(20)
CONST_FLOAT 		PS_INVERTED_HOLD_TIME						15.0
CONST_FLOAT			PS_DEFAULT_LEVEL_OUT_HOLD_TIME				0.5

//Looping
CONST_FLOAT			LOOP_LEVEL_OUT_ANGLE						270.0//IMMELMAN
CONST_FLOAT			LOOP_PREPARE_LEVEL_OUT_ANGLE				130.0	
CONST_FLOAT			LOOP_PREPARE_IMMELMAN_ANGLE					15.0
CONST_FLOAT			LOOP_IMMELMAN_ANGLE							160.0
CONST_FLOAT 		LOOP_IMMELMAN_ANGLE_TOO_FAR					370.0
CONST_FLOAT			PS_LOOPING_MIN_PITCH_DIFFERENCE				1.0
CONST_FLOAT			PS_LOOPING_LOOP_GOAL_ANGLE					330.0
CONST_FLOAT			PS_LOOPING_ROLL_GOAL_ANGLE					350.0
CONST_INT			MAX_LOOPING_CHECKPOINTS 					3

//Plane Obstacle Course
CONST_INT			MAX_PLANECOURSE_CHECKPOINTS 19

CONST_FLOAT			PS_MOVE_TRUCK_TARGET_PTFX_SCALE 5.0

ENUM PS_FAIL_REASON
	PS_FAIL_NO_REASON,
	PS_FAIL_TIME_UP,
	PS_FAIL_TOO_FAR,
	PS_FAIL_PLAYER_LEFT_HELI,
	PS_FAIL_PLAYER_LEFT_PLANE,
	PS_FAIL_OUT_OF_RANGE,
	PS_FAIL_BEHIND_RACE,
	PS_FAIL_FORMATION,
	PS_FAIL_LANDED_VEHICLE,
	PS_FAIL_DAMAGED_VEHICLE,
	PS_FAIL_MISSED_GATES,
	PS_FAIL_MISSED_FIRST_GATE,
	PS_FAIL_LEFT_RUNWAY,
	PS_FAIL_SUBMERGED,
	PS_FAIL_STUCK_VEHICLE,
	PS_FAIL_TOO_HIGH,
	PS_FAIL_LANDED_IN_WATER,
	PS_FAIL_REMOVED_PARACHUTE,
	PS_FAIL_IDLING,
	PS_FAIL_WANTED,
	PS_FAIL_COLLIDED_TRUCK,
	PS_FAIL_LANDING_GEAR,
	PS_FAIL_SPECIAL_REASON,
	PS_FAIL_DEBUG,
	PS_FAIL_DOUBLE_REASON,
	PS_FAIL_PLAYER_DEAD
ENDENUM

ENUM PILOT_SCHOOL_STAGE_ENUM
	START_PILOT_SCHOOL,
	CHOOSE_PILOT_SCHOOL,
	PERFORM_PILOT_CLASS_ONE,
	PASSED_PILOT_SCHOOL,
	FAILED_PILOT_SCHOOL
ENDENUM

ENUM PILOT_SCHOOL_GAME_MODE
	PS_GAME_MODE_INTRO_SCENE,
	PS_GAME_MODE_LOAD_MENU,
	PS_GAME_MODE_MENU,
	PS_GAME_MODE_COURSE_INIT,
	PS_GAME_MODE_LOAD_PREVIEW,
	PS_GAME_MODE_PREVIEW,
	PS_GAME_MODE_LOAD_CHALLENGE,
	PS_GAME_MODE_CHALLENGE,
	PS_GAME_MODE_QUIT_FROM_MENU
ENDENUM

ENUM PS_MUSIC_EVENT_FLAGS
	PS_MUSIC_EVENT_PREPARE_START,
	PS_MUSIC_EVENT_TRIGGER_START,
	PS_MUSIC_EVENT_PREPARE_STOP,
	PS_MUSIC_EVENT_TRIGGER_STOP,
	PS_MUSIC_EVENT_PREPARE_FAIL,
	PS_MUSIC_EVENT_TRIGGER_FAIL
ENDENUM

ENUM PS_ENDSCREEN_ENUM
	ENDSCREEN_AUTOPILOT,
	ENDSCREEN_ENGINE_OFF,
	ENDSCREEN_VEH_RECORDING,
	ENDSCREEN_SKYDIVE_LANDING
ENDENUM

ENUM PS_END_CAMERA_ENUM
	PS_END_CAMERA_STATIC_SHOT,
	PS_END_CAMERA_DYNAMIC_SHOT,
	PS_END_CAMERA_FOLLOW_SHOT
ENDENUM

ENUM PS_TAKEOFF_FLIGHT_CHALLENGE
	PS_TAKEOFF_SUCCESS,
	PS_TAKEOFF_FAIL,
	PS_TAKEOFF_INIT,
	PS_TAKEOFF_COUNTDOWN,
	PS_TAKEOFF_THROTTLE,
	PS_TAKEOFF_LIFT_OFF,
	PS_TAKEOFF_RETRACT_GEAR,
	PS_TAKEOFF_FLY_OUT
ENDENUM

ENUM PS_LANDING_FLIGHT_CHALLENGE
	PS_LANDING_SUCCESS,
	PS_LANDING_FAIL,
	PS_LANDING_INIT,
	PS_LANDING_COUNTDOWN,
	PS_LANDING_APPROACH_ALT, 
	PS_LANDING_APPROACH_SPEED,
	PS_LANDING_RUNWAY,
	PS_LANDING_TAXI
ENDENUM

ENUM PS_INVERTED_FLIGHT_CHALLENGE
	PS_INVERTED_SUCCESS,
	PS_INVERTED_FAIL,
	PS_INVERTED_INIT, //setup plane and player in the runway
	PS_INVERTED_COUNTDOWN, //play countdown
	PS_INVERTED_TAKEOFF, //give player control and make them go to a checkpoint
	PS_INVERTED_TAKEOFF_REACT, //feedback
	PS_INVERTED_SINGLE_BARREL_PREP, //tell player what they are about to do
	PS_INVERTED_SINGLE_BARREL_PERFORM, //throw up the objective and wait
	PS_INVERTED_SINGLE_BARREL_REACT, //give feedback and segway to next section
	PS_INVERTED_LEVEL_OUT_ONE, //throw up objective and wait
	PS_INVERTED_MULTI_BARREL_PREP, 
	PS_INVERTED_MULTI_BARREL_PERFORM,
	PS_INVERTED_MULTI_BARREL_REACT,
	PS_INVERTED_LEVEL_OUT_TWO,
	PS_INVERTED_UPSIDE_DOWN_PREP,
	PS_INVERTED_UPSIDE_DOWN_PERFORM,
	PS_INVERTED_UPSIDE_DOWN_REACT,
	PS_INVERTED_LEVEL_OUT_THREE
ENDENUM

ENUM PS_KNIFING_FLIGHT_CHALLENGE
	PS_KNIFING_SUCCESS,
	PS_KNIFING_FAIL,
	PS_KNIFING_INIT,
	PS_KNIFING_COUNTDOWN,
	PS_KNIFING_TAKEOFF,
	PS_KNIFING_STAGE_ONE_PREP,
	PS_KNIFING_STAGE_ONE_START,
	PS_KNIFING_STAGE_ONE_HOLD,
	PS_KNIFING_STAGE_ONE_LEVEL_OUT,
	PS_KNIFING_STAGE_TWO_PREP,
	PS_KNIFING_STAGE_TWO_START,
	PS_KNIFING_STAGE_TWO_HOLD,
	PS_KNIFING_STAGE_TWO_LEVEL_OUT
ENDENUM

ENUM PS_LOOPING_FLIGHT_CHALLENGE
	PS_LOOPING_SUCCESS,
	PS_LOOPING_FAIL,
	PS_LOOPING_INIT,
	PS_LOOPING_COUNTDOWN,
	PS_LOOPING_TAKEOFF,
	PS_LOOPING_TAKEOFF_REACT,
	PS_LOOPING_LOOP_ONE_PREP,
	PS_LOOPING_LOOP_ONE_START,
	PS_LOOPING_LOOP_ONE_PERFORM,
	PS_LOOPING_LOOP_ONE_REACT,
	PS_LOOPING_LEVEL_OUT_ONE,
	PS_LOOPING_LOOP_TWO_PREP,
	PS_LOOPING_LOOP_TWO_START,
	PS_LOOPING_LOOP_TWO_PERFORM,
	PS_LOOPING_LOOP_TWO_REACT,
	PS_LOOPING_LEVEL_OUT_TWO,
	PS_LOOPING_IMMELMAN_PREP,
	PS_LOOPING_IMMELMAN_START,
	PS_LOOPING_IMMELMAN_PERFORM_STEP_ONE,
	PS_LOOPING_IMMELMAN_PERFORM_STEP_TWO,
	PS_LOOPING_IMMELMAN_REACT,
	PS_LOOPING_LEVEL_OUT_THREE
ENDENUM

ENUM PS_FLY_LOW_FLIGHT_CHALLENGE
	PS_FLY_LOW_INIT,
	PS_FLY_LOW_CUTSCENE_INIT,
	PS_FLY_LOW_CUTSCENE,
	PS_FLY_LOW_COUNTDOWN,
	PS_FLY_LOW_TAKEOFF,
	PS_FLY_LOW_SUCCESS,
	PS_FLY_LOW_FAIL,
	PS_FLY_LOW_OBSTACLE_COURSE
ENDENUM

ENUM PS_DARING_FLIGHT_CHALLENGE
	PS_DARING_INIT,
	PS_DARING_CUTSCENE_INIT,
	PS_DARING_CUTSCENE,
	PS_DARING_COUNTDOWN,
	PS_DARING_TAKEOFF,
	PS_DARING_SUCCESS,
	PS_DARING_FAIL,
	PS_DARING_LANDING
ENDENUM

ENUM PS_PLANE_COURSE_FLIGHT_CHALLENGE
	PS_PLANE_COURSE_INIT,
	PS_PLANE_COURSE_CUTSCENE_INIT,
	PS_PLANE_COURSE_CUTSCENE,
	PS_PLANE_COURSE_COUNTDOWN,
	PS_PLANE_COURSE_TAKEOFF,
	PS_PLANE_COURSE_SUCCESS,
	PS_PLANE_COURSE_FAIL,
	PS_PLANE_COURSE_OBSTACLE,
	PS_PLANE_COURSE_STAGE_TWO,
	PS_PLANE_COURSE_STAGE_THREE
ENDENUM

ENUM PS_HELI_SPEED_FLIGHT_CHALLENGE
	PS_HELI_SPEED_INIT,
	PS_HELI_SPEED_CUTSCENE_INIT,
	PS_HELI_SPEED_CUTSCENE,
	PS_HELI_SPEED_COUNTDOWN,
	PS_HELI_SPEED_TAKEOFF,
	PS_HELI_SPEED_SUCCESS,
	PS_HELI_SPEED_FAIL,
	PS_HELI_SPEED_OBSTACLE,
	PS_HELI_SPEED_STAGE_TWO,
	PS_HELI_SPEED_STAGE_THREE
ENDENUM

ENUM PS_HELI_COURSE_FLIGHT_CHALLENGE
	PS_HELI_COURSE_INIT,
	PS_HELI_COURSE_CUTSCENE_INIT,
	PS_HELI_COURSE_CUTSCENE,
	PS_HELI_COURSE_COUNTDOWN,
	PS_HELI_COURSE_TAKEOFF,
	PS_HELI_COURSE_SUCCESS,
	PS_HELI_COURSE_FAIL,
	PS_HELI_COURSE_OBSTACLE
ENDENUM

ENUM PS_PARACHUTE_FLIGHT_CHALLENGE
	PS_PARACHUTE_SUCCESS,
	PS_PARACHUTE_FAIL,
	PS_PARACHUTE_INIT,
	PS_PARACHUTE_COUNTDOWN,
	PS_PARACHUTE_LOOKING,
	PS_PARACHUTE_JUMPING,
	PS_PARACHUTE_SKYDIVING,
	PS_PARACHUTE_PROCESS_DATA,
	PS_PARACHUTE_WAIT
ENDENUM

ENUM PS_MOVING_CHUTE_FLIGHT_CHALLENGE
	PS_MOVING_CHUTE_SUCCESS,
	PS_MOVING_CHUTE_FAIL,
	PS_MOVING_CHUTE_INIT,
	PS_MOVING_CHUTE_COUNTDOWN,
	PS_MOVING_CHUTE_LOOKING,
	PS_MOVING_CHUTE_JUMPING,
	PS_MOVING_CHUTE_SKYDIVING,
	PS_MOVING_CHUTE_PROCESS_DATA,
	PS_MOVING_CHUTE_WAIT
ENDENUM

ENUM PS_FORMATION_FLIGHT_CHALLENGE
	PS_FORMATION_INIT,
	PS_FORMATION_CUTSCENE_INIT,
	PS_FORMATION_CUTSCENE,
	PS_FORMATION_COUNTDOWN,
	PS_FORMATION_TAKEOFF,
	PS_FORMATION_SUCCESS,
	PS_FORMATION_FAIL,
	PS_FORMATION_OBSTACLE
ENDENUM

//Enumerated index to keep track of the formation vehicles 
ENUM FORMATION_VEHICLE_ENUM
	FORMATION_PLANE_1 = 0,
	FORMATION_PLANE_2,
	FORMATION_PLANE_3,
	FORMATION_PLANE_4,
	
	FORMATION_PLANE_COUNT
ENDENUM

ENUM PS_STUNT_ENUM
	PS_STUNT_NORMAL,
	PS_STUNT_INVERTED,
	PS_STUNT_SIDEWAYS,
	PS_STUNT_LEFT,
	PS_STUNT_RIGHT
ENDENUM

ENUM PS_LESSON_ENUM
	PS_LESSON_NULL,
	PS_LESSON_STUNT,
	PS_LESSON_OBSTACLE_HELI,
	PS_LESSON_OBSTACLE_PLANE,
	PS_LESSON_LANDING,	
	PS_LESSON_SKYDIVING	
ENDENUM

ENUM PS_CHECKPOINT_ENUM
	PS_CHECKPOINT_NULL,
	PS_CHECKPOINT_OBSTACLE_HELI,
	PS_CHECKPOINT_OBSTACLE_PLANE,	
	PS_CHECKPOINT_STUNT_INVERTED,
	PS_CHECKPOINT_STUNT_SIDEWAYS_L,
	PS_CHECKPOINT_STUNT_SIDEWAYS_R,
	PS_CHECKPOINT_STUNT_LEFT,
	PS_CHECKPOINT_STUNT_RIGHT,
	PS_CHECKPOINT_FINISH,
	PS_CHECKPOINT_SPECIAL,
	PS_CHECKPOINT_LANDING
ENDENUM

ENUM PS_CHECKPOINT_SIZE
	PS_CHECKPOINT_SIZE_LARGE,
	PS_CHECKPOINT_SIZE_SMALL
ENDENUM

ENUM PS_PREVIEW_STATE
	PS_PREVIEW_INIT,
	PS_PREVIEW_FADE_START,
	PS_PREVIEW_SETUP,
	PS_PREVIEW_PLAYING,
	PS_PREVIEW_FADE_END,
	PS_PREVIEW_CLEANUP,
	PS_PREVIEW_IDLE
ENDENUM

ENUM PS_PREVIEW_CUSTOM_STATE
	PS_PREVIEW_CUSTOM_STATE_1,
	PS_PREVIEW_CUSTOM_STATE_2,
	PS_PREVIEW_CUSTOM_STATE_3,
	PS_PREVIEW_CUSTOM_STATE_4,
	PS_PREVIEW_CUSTOM_STATE_5,
	PS_PREVIEW_CUSTOM_STATE_6,
	PS_PREVIEW_CUSTOM_STATE_7
ENDENUM

ENUM PS_TRUCK_LANDING_STATE
	PS_TRUCK_LANDING_STATE_01,
	PS_TRUCK_LANDING_STATE_02,
	PS_TRUCK_LANDING_STATE_03,
	PS_TRUCK_LANDING_STATE_04,
	PS_TRUCK_LANDING_STATE_05,
	PS_TRUCK_LANDING_STATE_IDLE
ENDENUM

ENUM PS_TRUCK_CRASHING_STATE
	PS_TRUCK_CRASHING_STATE_01,
	PS_TRUCK_CRASHING_STATE_02,
	PS_TRUCK_CRASHING_STATE_03,
	PS_TRUCK_CRASHING_STATE_04,
	PS_TRUCK_CRASHING_STATE_05,
	PS_TRUCK_CRASHING_STATE_IDLE
ENDENUM

ENUM PS_PARACHUTE_JUMP_CUT_STATE
	PS_PARACHUTE_JUMP_01,
	PS_PARACHUTE_JUMP_02,
	PS_PARACHUTE_JUMP_03,
	PS_PARACHUTE_JUMP_04,
	PS_PARACHUTE_JUMP_05,
	PS_PARACHUTE_JUMP_06
ENDENUM

ENUM PS_STARTING_CUTSCENE_STATE
	PS_STARTING_CUTSCENE_SKIP_01,
	PS_STARTING_CUTSCENE_SKIP_02,
	PS_STARTING_CUTSCENE_STATE_01,
	PS_STARTING_CUTSCENE_STATE_02,
	PS_STARTING_CUTSCENE_STATE_03,
	PS_STARTING_CUTSCENE_STATE_04,
	PS_STARTING_CUTSCENE_STATE_05,
	PS_STARTING_CUTSCENE_STATE_06,
	PS_STARTING_CUTSCENE_STATE_07	
ENDENUM

ENUM PS_SCREEN_FADE_STATE
	PS_SCREEN_FADE_IDLE,
	PS_SCREEN_FADE_OUT,
	PS_SCREEN_FADE_WAIT,
	PS_SCREEN_FADE_IN
ENDENUM

ENUM PS_COUNTDOWN_ENUM
	PS_COUNT_DWN_INIT,
	PS_COUNT_DWN_3,
	PS_COUNT_DWN_2,
	PS_COUNT_DWN_1,
	PS_COUNT_DWN_GO,
	PS_COUNT_DWN_WAIT
ENDENUM

ENUM ENDING_SCREEN_STATE
	PS_ENDING_INIT,
	PS_ENDING_SETUP_CAMERA,
	PS_ENDING_POINT_DYNAMIC_CAM,
	PS_ENDING_DETERMINE_TRACKING,
	PS_ENDING_HOLD_SHOT,
	PS_ENDING_QUICK_PAN_SETUP,
	PS_ENDING_QUICK_PAN_UPDATE,
	PS_ENDING_SFX_AND_SPLASH_SETUP,
	PS_ENDING_SFX_AND_SPLASH_UPDATE,
	ENDING_TOAST_SETUP,
	ENDING_TOAST_UPDATE,
	PS_ENDING_FAIL_EFFECTS,
	PS_ENDING_SHOW_FAIL_OPTIONS,
	PS_ENDING_CLEANUP
ENDENUM

ENUM PS_ENDCUT_STATE
	PS_ENDCUT_INIT,
	PS_ENDCUT_AUTOPILOT,
	PS_ENDCUT_IDLE
ENDENUM

ENUM PS_END_CAM_STATE_ENUM
	PS_END_CAM_STATE_INIT,
	PS_END_CAM_STATE_HOLD_SHOT_1,
	PS_END_CAM_STATE_START_PAN,
	PS_END_CAM_STATE_UPDATE_PAN,
	PS_END_CAM_STATE_HOLD_SHOT_2,
	PS_END_CAM_STATE_IDLE
ENDENUM

ENUM PS_CHALLENGE_SUBSTATE
	PS_SUBSTATE_ENTER,
	PS_SUBSTATE_UPDATE,
	PS_SUBSTATE_EXIT
ENDENUM

ENUM PS_CHECKPOINT_PASS
	PS_CHECKPOINT_OK,
	PS_CHECKPOINT_AWESOME,
	PS_CHECKPOINT_MISS
ENDENUM

//hint cam stuff
ENUM PS_HINT_CAM_ENUM
	PS_HINT_FROM_STUNTPLANE,
	PS_HINT_FROM_CARGOPLANE,
	PS_HINT_FROM_SKYDIVING,
	PS_HINT_FROM_FLOATING
ENDENUM

ENUM PS_OBJECTIVE_TYPE_ENUM
	PS_OBJECTIVE_TYPE_BARREL_ROLL,
	PS_OBJECTIVE_TYPE_INVERTED,
	PS_OBJECTIVE_TYPE_KNIFE,
	PS_OBJECTIVE_TYPE_KNIFE_L,
	PS_OBJECTIVE_TYPE_KNIFE_R,
	PS_OBJECTIVE_TYPE_INSIDE_LOOP,
	PS_OBJECTIVE_TYPE_IMMELMAN_TURN,
	PS_OBJECTIVE_TYPE_IMMELMAN_ROLL,
	PS_OBJECTIVE_TYPE_FORMATION
ENDENUM

STRUCT PS_JUMP_CAM_STRUCT
	structTimer camTimer
	VECTOR vLookCameraRot
	VECTOR vFocalPoint
	VECTOR vFocalOffset
	VECTOR vCameraVelocity
	VECTOR vBaseLookCamRot
	FLOAT fBaseLookCamFOV
ENDSTRUCT

STRUCT PS_HINT_CAM_STRUCT
	BOOL bActive
	FLOAT fCamTimer
	ENTITY_INDEX Entity
	VECTOR Coord
	PS_HINT_CAM_ENUM HintType
	CAMERA_INDEX CustomCam
ENDSTRUCT

STRUCT PS_HUD_OBJECTIVE_BAR_STRUCT
	BOOL							bIsInvertedMeterActive
	BOOL							bIsRollMeterActive
	BOOL							bIsKnifeMeterActive
	BOOL							bIsLoopMeterActive
	BOOL							bIsImmelMeterActive
	BOOL							bIsObjMeterVisible
	BOOL							bCapObjectiveBar
	PS_OBJECTIVE_TYPE_ENUM			eObjMeterType
	STRING 							sObjBarTitle														
	FLOAT 							fObjBarNumerator						
	FLOAT 							fObjBarDemoninator						
	FLOAT 							fObjBarDisplayAtX						
	FLOAT 							fObjBarDisplayAtY
ENDSTRUCT

STRUCT PS_HUD_GUTTER_ICON_STRUCT
	BOOL							bIsGutterActive
	BOOL							bIsGutterVisible
	//Vars used for positioning gutter icon
	FLOAT 							fGutterX
	FLOAT 							fGutterY
	//Vars used for size of gutter icon
	FLOAT 							fGutterHeight
	FLOAT 							fGutterWidth
ENDSTRUCT

STRUCT PS_HUD_RACE_HUD_STRUCT
	TEXT_LABEL 						sPSHUDMedalGoal
	PODIUMPOS 						ePSHUDMedal
	FLOAT 							fPSHUDMedalTime
	structTimer						tHUDBonusTimer
	BOOL 							bIsTimerVisible
	BOOL 							bIsHourGlassActive
	INT								iPSHUDExtraTime
	INT								iHourGlassStartTime
	INT								iTimeBonus
	BOOL							bCheckpointCounterVisible
	BOOL							bIsTimerActive
	BOOL							bIsDistActive
	BOOL 							bIsDistVisible

ENDSTRUCT

////stunt detection
//STRUCT PS_OBJECTIVE_DATA_STRUCT
//	INT iTotalOrientCount, iCurrentOrientCount
//	FLOAT fPlanePrevOrient, fPlaneTotalOrient
//	BOOL bCurrentlyClockwiseOrient
//ENDSTRUCT

//stunt detection
STRUCT PS_OBJECTIVE_DATA_STRUCT
	INT iTotalOrientCount, iCurrentOrientCount
	FLOAT fPlanePrevOrient, fPlaneTotalOrient
	BOOL bUpdateData, bCurrentlyClockwiseOrient, bRetryFlag
	structTimer tObjectiveTimer
ENDSTRUCT

STRUCT PS_CHECKPOINT_STRUCT
	BLIP_INDEX	blip
	CHECKPOINT_INDEX checkpoint
	VECTOR position
	PS_CHECKPOINT_ENUM type
ENDSTRUCT

STRUCT PS_CHECKPOINT_MGR_STRUCT
	PS_LESSON_ENUM lessonType = 0
	BOOL isActive = FALSE, hasMissed = FALSE, isFinished = FALSE, waitingForFeedback = FALSE, bForPreview = FALSE
	PS_CHECKPOINT_PASS lastCheckpointScore = PS_CHECKPOINT_OK
	INT	curIdx, nextIdx, clearedCheckpoints, totalCheckpoints
	FLOAT distFromCurCheckpoint
	FLOAT savedFinishTime
ENDSTRUCT

STRUCT PS_CHALLENGE_DATA_STRUCT
	TEXT_LABEL					Title
	PILOT_SCHOOL_CLASSES_ENUM	LessonEnum
	PILOT_SCHOOL_STATUS_ENUM	LockStatus
	BOOL						HasBeenPreviewed = FALSE
	BOOL						bBronzeOverride = FALSE //skip the fail checks at the end. this should be set DURING the course.
	BOOL                        bPrintedChuteHelp, bPrintedLandingHelp
	PS_ENDSCREEN_ENUM 			eEndscreenType
	PS_END_CAMERA_ENUM 			eEndCamType
	structTimer					EndCamTimer, ForceEndPanCamTimer, CamTrackingTimer
	VECTOR						vStaticCamPos, vStaticCamRot
	FLOAT						fStaticCamFOV
	INT 						PreReqBits //what challenges need to be completed before this can be unlocked/selected
	INT							GoalBits //what goals are required to pass this challenge
	FLOAT						GoldTime, SilverTime, BronzeTime		//FSG_0_timeTaken 
	FLOAT						GoldDistance, SilverDistance, BronzeDistance		//FSG_2_distanceFromTarget
	INT							TotalCheckpoints				//FSG_3_checkpointsPassed
	FLOAT						FormationTotal	//FSG_4
ENDSTRUCT

STRUCT PS_MAIN_STRUCT
	VEHICLE_INDEX					myVehicle, myFormationLeaderVehicle, myGhostVehicle, playerVehicle, playerTrailer
	PED_INDEX						myPilotPed, prevPed, previewDummy//, challengerJumper_ped, tempPed
	BLIP_INDEX						dest_blip, next_blip, challenger_blip//TODO: move challenger blip to formation only
	CHECKPOINT_INDEX				dest_checkpoint, next_checkpoint, flash_checkpoint
	structTimer						tRaceTimer, tCountdownTimer, tPulse, tHealthTimer, tmrEndDelay, tmrUpsideDownGround//TODO: move health timer to formation only
	PS_OBJECTIVE_DATA_STRUCT 		myObjectiveData//, myObjectiveData // only need one of these
	PS_PLAYER_DATA_STRUCT			myPlayerData
	PS_CHALLENGE_DATA_STRUCT		myChallengeData
	PS_CHECKPOINT_MGR_STRUCT		myCheckpointMgr
	VEHICLE_SETUP_STRUCT 			myVehicleSetup
	PS_CHECKPOINT_STRUCT			myCheckpointz[PS_MAX_CHECKPOINTS]
	PS_JUMP_CAM_STRUCT				myJumpCam
	VEHICLE_INDEX					vehFormationPlanes[FORMATION_PLANE_COUNT]
	PED_INDEX						pedFormationPilots[FORMATION_PLANE_COUNT]
	BLIP_INDEX						blpFormationBlips[FORMATION_PLANE_COUNT]
	CAMERA_INDEX					previewCam1
	CAMERA_INDEX					previewCam2
	PS_PREVIEW_CUSTOM_STATE			customPreviewState
	INT								iFlashCheckpointAlpha
	BOOL							bFlashCheckpointMissed
	BOOL							bDrawCheckpointMarkerFlash
	VECTOR							vCheckpointMarkerFlashPosition
	HUD_COLOURS						eFlashHudColour
ENDSTRUCT

//-----------------------------------
//Function pointers used for the main game loop

TYPEDEF PROC CustomPS_Proc()
PROC CustomPS_Proc_NULL()
	//do nothing
ENDPROC

TYPEDEF FUNC BOOL CustomPS_Func()
FUNC BOOL CustomPS_Func_NULL()
	RETURN FALSE
ENDFUNC

CustomPS_Func PSC_UpdateFunc, PSC_PreviewFunc
CustomPS_Proc PSC_InitProc, PSC_SetupProc, PSC_CleanupProc
#IF IS_DEBUG_BUILD
	STRUCT PS_STATS_STRUCT
		INT lessonAttempts
		INT goldMedalsEarned
		INT silverMedalsEarned
		INT bronzeMedalsEarned
	ENDSTRUCT
	
	PS_STATS_STRUCT PS_Debug_Stats[NUMBER_OF_PILOT_SCHOOL_CLASSES]
	
	//debug functions
	CustomPS_Proc  PSC_DBG_FORCE_FAIL, PSC_DBG_FORCE_PASS
#ENDIF

//-----------------------------------

CAMERA_INDEX						camMainMenu						= NULL
CAMERA_INDEX						camMainMenu1					= NULL
VECTOR								vPS_MenuCamCoords				= <<0, 0, 0>>
PS_FAIL_REASON 						ePSFailReason					= PS_FAIL_NO_REASON

VECTOR								PS_MENU_CAM_COORDS 				= <<-1155.4,-2715.5,64.0>>//<< -1434.5499, -2851.6138, 119.5132 >>
VECTOR								PS_MENU_CAM_ROT		 			= << -10.3, 0.0, 113.1>>//<< -7.1277, 0.0782, -77.3225 >>
CONST_INT							PS_MENU_CAM_FOV					50
VECTOR								PS_MENU_LOAD_SCENE_COORDS 		= << -1175.8738, -2804.4434, 30.3758 >>

INT									iIntroPanSoundID				= -1

TYPEDEF FUNC BOOL 					PS_CutsceneSkipFuncPointer()

//challenge state machine enums
PS_TAKEOFF_FLIGHT_CHALLENGE 		PS_Takeoff_State 				= PS_TAKEOFF_INIT
PS_LANDING_FLIGHT_CHALLENGE 		PS_Landing_State 				= PS_LANDING_INIT
PS_INVERTED_FLIGHT_CHALLENGE 		PS_Inverted_State 				= PS_INVERTED_INIT
PS_KNIFING_FLIGHT_CHALLENGE 		PS_Knifing_State 				= PS_KNIFING_INIT
PS_LOOPING_FLIGHT_CHALLENGE 		PS_Looping_State 				= PS_LOOPING_INIT
PS_FLY_LOW_FLIGHT_CHALLENGE 		PS_Fly_Low_State 				= PS_FLY_LOW_INIT
PS_DARING_FLIGHT_CHALLENGE 			PS_Daring_State 				= PS_DARING_INIT
PS_PLANE_COURSE_FLIGHT_CHALLENGE 	PS_Plane_Course_State			= PS_PLANE_COURSE_INIT
PS_HELI_SPEED_FLIGHT_CHALLENGE 		PS_Heli_Speed_State 			= PS_HELI_SPEED_INIT
PS_HELI_COURSE_FLIGHT_CHALLENGE 	PS_Heli_Course_State 			= PS_HELI_COURSE_INIT
PS_PARACHUTE_FLIGHT_CHALLENGE 		PS_Parachute_State 				= PS_PARACHUTE_INIT
PS_MOVING_CHUTE_FLIGHT_CHALLENGE 	PS_Moving_Chute_State 			= PS_MOVING_CHUTE_INIT

//FUNC BOOL PS_CutsceneSkipFunc_NULL()
//	RETURN FALSE
//ENDFUNC

PROC DefinitionsTempHack()

ENDPROC
