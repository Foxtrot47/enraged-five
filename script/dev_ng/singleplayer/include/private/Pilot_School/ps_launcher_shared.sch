
ENUM PS_LAUNCH_SCENE_STATE
	PS_LAUNCH_SCENE_INIT,
	PS_LAUNCH_SCENE_SETUP_PLAYER,
	PS_LAUNCH_SCENE_01_HOLD_CAMERA_SHOT,
	PS_LAUNCH_SCENE_02_ACTIVATE_CAMERA_PAN,
	PS_LAUNCH_SCENE_03_WAIT_FOR_CAMERA_PAN,
	PS_LAUNCH_SCENE_04_DELAY,
	PS_LAUNCH_SCENE_CLEANUP
ENDENUM

STRUCT PS_LAUNCH_ARGS
	structTimer tLaunchSceneTimer
	PS_LAUNCH_SCENE_STATE ePS_LaunchScene
ENDSTRUCT
