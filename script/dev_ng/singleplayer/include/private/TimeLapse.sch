USING "script_clock.sch"
USING "cutscene_public.sch"
USING "commands_recording.sch"
USING "script_misc.sch"


///PURPOSE: Configure a timelapse settings struct with custom settings for a custom timelapse cutscene.
///    
PROC SETUP_TIMELAPSE_DATA(structTimelapseSettings &sSettings, INT iStartHour, INT iEndHour, VECTOR vCamPos1, VECTOR vCamRot1, VECTOR vCamPos2, VECTOR vCamRot2, FLOAT fCamFOV, INT iCamTransitionTime, STRING strWeather, STRING strCloudHat, BOOL bSkipToNight = FALSE, INT iCustomBehaviour = -1)
	sSettings.iLapseStartHour 	= iStartHour
	sSettings.iLapseEndHour 	= iEndHour
	sSettings.vCamPos1			= vCamPos1
	sSettings.vCamRot1			= vCamRot1
	sSettings.vCamPos2			= vCamPos2
	sSettings.vCamRot2			= vCamRot2
	sSettings.fCamFOV			= fCamFOV
	sSettings.iCamTime			= iCamTransitionTime
	sSettings.tWeatherType		= strWeather
	sSettings.tCloudHat			= strCloudHat
	sSettings.bSkipToNight		= bSkipToNight
	sSettings.iCustomBehaviour 	= iCustomBehaviour
ENDPROC

//PURPOSE: Starts or stops a cutscene. If TRUE is passed timera() is set to Zero
PROC SET_TODS_CUTSCENE_RUNNING(structTimelapse &sTimelapse, BOOL isRunning, BOOL doGameCamInterp = FALSE, INT durationFromInterp = 2000, BOOL turnOffGadgets = TRUE, BOOL bResetTime = TRUE, BOOL bResetCatchTime = FALSE, BOOL bRemovePlayersHelmet = TRUE)
	
	DEBUG_PRINTCALLSTACK()
	
	IF turnOffGadgets = TRUE
		SET_PLAYER_CONTROL(PLAYER_ID(), IS_PLAYER_CONTROL_ON(PLAYER_ID()),  SPC_DEACTIVATE_GADGETS)
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING()
	    SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
	ENDIF
	
	//Clear ped helmets #1528518
	IF bRemovePlayersHelmet
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
	
	IF (NOT isRunning) AND (bResetCatchTime)
	   STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
	ELSE
		RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	ENDIF
	
	IF isRunning
	    CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse set running by script [", GET_THIS_SCRIPT_NAME(), "]. Start:", sTimelapse.iTimeWindowStart, " End:", sTimelapse.iTimeWindowEnd)
	    CLEAR_HELP()
	ELSE
		CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse cleanup triggered by script [", GET_THIS_SCRIPT_NAME(), "].")
		IF DOES_CAM_EXIST(sTimelapse.splineCamera)
			IF IS_CAM_ACTIVE(sTimelapse.splineCamera)
				SET_CAM_ACTIVE(sTimelapse.splineCamera, FALSE)
				CPRINTLN(DEBUG_SYSTEM, "<TOD> Camera deactivated as timelapse cleans up.")
			ENDIF
			DESTROY_CAM(sTimelapse.splineCamera, TRUE)
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Camera destroyed as timelapse cleans up.")
		ENDIF

		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Unhiding player as TOD camera deactivates.")
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
		VEHICLE_INDEX vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayerLast)
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Unhiding player's last vehicle as TOD camera deactivates.")
			SET_ENTITY_VISIBLE(vehPlayerLast, TRUE)
		ENDIF
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
		
		IF IS_AUDIO_SCENE_ACTIVE("TOD_SHIFT_SCENE") 
			STOP_SOUND(sTimelapse.iSplineStageSound)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
			STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
			
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Audio scene \"TOD_SHIFT_SCENE\" active in ", GET_THIS_SCRIPT_NAME(), ".")
		ENDIF
	ENDIF

	//CLEAR_HELP(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(isRunning,DEFAULT,FALSE)
	FLUSH_TEXT_MESSAGE_FEED_ENTRIES()

	IF NOT isRunning
	AND bResetTime
		IF sTimelapse.bSkipToNightTime
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Resetting to start time ", sTimelapse.iTimeWindowStart, ":00 as timelapse cleans up.")
			SET_CLOCK_TIME(sTimelapse.iTimeWindowStart, 00, 00)
		ELSE
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Resetting to end time ", sTimelapse.iTimeWindowEnd, ":00 as timelapse cleans up.")
			SET_CLOCK_TIME(sTimelapse.iTimeWindowEnd, 00, 00)
		ENDIF
	ENDIF

	IF NOT isRunning
	    IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
	    ENDIF
	ENDIF
	
	
	// Kenneth R.
	// Doing a timelapse causes the mission vehgen impound timer to get hit almost instantly
	// so we need to reset the timestamp.
	IF NOT isRunning
		IF g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp != INVALID_TIMEOFDAY
			g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp = GET_CURRENT_TIMEOFDAY()
		ENDIF
	ENDIF
ENDPROC



FUNC BOOL IS_DAYLIGHT_HOURS()
	IF IS_TIME_BETWEEN_THESE_HOURS(TODS_SUNSET, TODS_SUNRISE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL SET_PLAYER_VEH_RADIO_OFF_FOR_TIMELAPSE()
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX VehicleIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	IF NOT IS_ENTITY_DEAD(VehicleIndex)
		RETURN FALSE
	ENDIF
	
	SET_VEH_RADIO_STATION(VehicleIndex, "OFF")
	RETURN TRUE
ENDFUNC


FUNC BOOL SKIP_TO_TIME_DURING_SPLINE_CAMERA(INT iDestHour, INT iDestMinute, STRING tWeatherType, STRING tCloudHat, structTimelapse &sTimelapse, FLOAT fForceCameraPhase = -1.0, INT iAdditionalHours = 0, BOOL bDoCascadeShadows = TRUE, FLOAT time = 1.0)
	
	FLOAT fCameraPhase = -1
	INT iSecondsDiff
	INT iMinutesDiff
	INT iHoursDiff
	INT iSecondsToSkipThisFrame
	TIMEOFDAY sTempTimeOfDay
	
	#IF IS_DEBUG_BUILD
	CONST_INT iDrawLiteralSceneStringRow	3
	TEXT_LABEL_63 str
	DrawLiteralSceneStringInt("iTimeSkipStage:", sTimelapse.iTimeSkipStage, iDrawLiteralSceneStringRow+0)
	DrawLiteralSceneStringInt("iDestHour:", iDestHour, iDrawLiteralSceneStringRow+1)
	DrawLiteralSceneStringInt("iDestMinute:", iDestMinute, iDrawLiteralSceneStringRow+2)
	#ENDIF
	
	SWITCH sTimelapse.iTimeSkipStage
	
		CASE 0
			//Save the starting time of day.
			sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
			
			//Work out the ending time of day.
			sTempTimeOfDay = GET_CURRENT_TIMEOFDAY()
			SET_TIMEOFDAY_HOUR(sTempTimeOfDay, iDestHour)
			SET_TIMEOFDAY_MINUTE(sTempTimeOfDay, iDestMinute)
			SET_TIMEOFDAY_SECOND(sTempTimeOfDay, 0)
			
			//Is start time currently after end time?
			IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(sTimelapse.sStartTimeOfDay, sTempTimeOfDay)
				//If so we need to add 1 day to end time.
				ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, 0, 0, 0, 1)
			ENDIF
			
			//Calculate difference between start and end time in seconds.
			INT iDaysDiff, iWeeksDiff, iMonthsDiff
			GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(sTimelapse.sStartTimeOfDay, sTempTimeOfDay, iSecondsDiff, iMinutesDiff, iHoursDiff, iDaysDiff, iWeeksDiff, iMonthsDiff)
			
			sTimelapse.iSecondsToSkipTotal = iSecondsDiff + (iMinutesDiff*60) + ((iHoursDiff+iAdditionalHours)*3600)
			
			ADVANCE_FRIEND_TIMERS(TO_FLOAT(sTimelapse.iSecondsToSkipTotal) / 3600.0)
			
			IF bDoCascadeShadows
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
				CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)		//#1439834
			ENDIF
			
			SET_PLAYER_VEH_RADIO_OFF_FOR_TIMELAPSE()
			
			sTimelapse.iSplineStageSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sTimelapse.iSplineStageSound, "TIME_LAPSE_MASTER")
			START_AUDIO_SCENE("TOD_SHIFT_SCENE") 
			
			FLUSH_TEXT_MESSAGE_FEED_ENTRIES()	//1296499
			
			sTimelapse.iTimeSkipStage++
		BREAK
		
		CASE 1	FALLTHRU	//BEFORE setting weather and cloud hat
		CASE 2				//AFTER setting weather and cloud hat
			IF (DOES_CAM_EXIST(sTimelapse.splineCamera) AND IS_CAM_INTERPOLATING(sTimelapse.splineCamera))
			OR NOT (fForceCameraPhase >= 0.99 OR fForceCameraPhase = -1)
				
				IF (DOES_CAM_EXIST(sTimelapse.splineCamera) AND IS_CAM_INTERPOLATING(sTimelapse.splineCamera))
					fCameraPhase = GET_CAM_SPLINE_PHASE(sTimelapse.splineCamera)
				ELIF NOT (fForceCameraPhase >= 0.99 OR fForceCameraPhase = -1)
					fCameraPhase = fForceCameraPhase
				ELSE
					fCameraPhase = -1
				ENDIF
				
				IF fCameraPhase >= 0.5	
					
					IF (sTimelapse.iTimeSkipStage = 1)
					
						// Set any weather types or cloud hats
						IF GET_HASH_KEY(tWeatherType) != 0
							//#218020
							SET_WEATHER_TYPE_OVERTIME_PERSIST(tWeatherType, time)
							CPRINTLN(DEBUG_SYSTEM, "<TOD> Setting weather to ", tWeatherType, ".")
						ENDIF
						IF GET_HASH_KEY(tCloudHat) != 0
							UNLOAD_ALL_CLOUD_HATS()
							LOAD_CLOUD_HAT(tCloudHat)
							CPRINTLN(DEBUG_SYSTEM, "<TOD> Setting cloud hat ", tCloudHat, ".")
						ENDIF
						
						sTimelapse.iTimeSkipStage = 2
					ENDIF
				ENDIF
			ENDIF
			
			
			// Finished spline so set final time and bail out
			IF fCameraPhase >= 0.99
			OR fCameraPhase = -1
				//Work out the time of day we need to jump to this frame.
				sTempTimeOfDay = sTimelapse.sStartTimeOfDay
				ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, sTimelapse.iSecondsToSkipTotal)
				
				#IF IS_DEBUG_BUILD
					PRINT_TIMEOFDAY(sTempTimeOfDay)
				#ENDIF
				
				//Update the game clock with the new time.
				SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTempTimeOfDay), GET_TIMEOFDAY_MINUTE(sTempTimeOfDay), GET_TIMEOFDAY_SECOND(sTempTimeOfDay))
				
				IF bDoCascadeShadows
					CASCADE_SHADOWS_INIT_SESSION()
				ENDIF
				
				STOP_SOUND(sTimelapse.iSplineStageSound)
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
//				STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
				
				IF bDoCascadeShadows
					CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				ENDIF
				
				RETURN TRUE
			ENDIF
			
			
			// To make things smoother, dont change time in the first and last 10% of the camera phase
			
			// [x-A/B-A] (clamp 0-1)
			FLOAT fA, fB, fX, fMappedPhase
			
			fX = fCameraPhase
			fA = 0.1
			fB = 0.9
			
			fMappedPhase = CLAMP(((fX - fA) / (fB - fA)), 0, 1)
			
			//Work out the time of day we need to jump to this frame.
			iSecondsToSkipThisFrame = ROUND(sTimelapse.iSecondsToSkipTotal * fMappedPhase)
			sTempTimeOfDay = sTimelapse.sStartTimeOfDay
			ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, iSecondsToSkipThisFrame)
			
			#IF IS_DEBUG_BUILD
			FLOAT fMinutesToSkip
			fMinutesToSkip = (TO_FLOAT(iSecondsToSkipThisFrame) / 60.0)
			
			HUD_COLOURS eColour
			eColour = HUD_COLOUR_PURE_WHITE
			DrawLiteralSceneStringFloat("fMinutesToSkip:", fMinutesToSkip, iDrawLiteralSceneStringRow+3, eColour)
			
			str = TIMEOFDAY_TO_TEXT_LABEL(sTempTimeOfDay)
			DrawLiteralSceneString(str, iDrawLiteralSceneStringRow+4)
			#ENDIF
			
			//Update the game clock with the new time.
			SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTempTimeOfDay), GET_TIMEOFDAY_MINUTE(sTempTimeOfDay), GET_TIMEOFDAY_SECOND(sTempTimeOfDay))
			
			IF GET_TIMEOFDAY_DAY(sTempTimeOfDay) <> GET_CLOCK_DAY_OF_MONTH()
				
				//SCRIPT_ASSERT("need to call SET_CLOCK_DATE()")
				PRINTLN("need to call SET_CLOCK_DATE()")
				
				SET_CLOCK_DATE(GET_TIMEOFDAY_DAY(sTempTimeOfDay), GET_TIMEOFDAY_MONTH(sTempTimeOfDay), GET_TIMEOFDAY_YEAR(sTempTimeOfDay))
			ENDIF
			
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			RESET_ADAPTATION()		//#1358413
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL SKIP_TO_TIME_DURING_PHASE(INT iDestHour, INT iDestMinute, STRING tWeatherType, STRING tCloudHat, structTimelapse &sTimelapse, FLOAT fCameraPhase, INT iAdditionalHours = 0)
	
	INT iSecondsDiff
	INT iMinutesDiff
	INT iHoursDiff
	INT iSecondsToSkipThisFrame
	TIMEOFDAY sTempTimeOfDay
	
	#IF IS_DEBUG_BUILD
	CONST_INT iDrawLiteralSceneStringRow	3
	TEXT_LABEL_63 str
	DrawLiteralSceneStringInt("iTimeSkipStage:", sTimelapse.iTimeSkipStage, iDrawLiteralSceneStringRow+0)
	DrawLiteralSceneStringInt("iDestHour:", iDestHour, iDrawLiteralSceneStringRow+1)
	DrawLiteralSceneStringInt("iDestMinute:", iDestMinute, iDrawLiteralSceneStringRow+2)
	#ENDIF
	
	SWITCH sTimelapse.iTimeSkipStage
		CASE 0
			//Save the starting time of day.
			sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
			
			//Work out the ending time of day.
			sTempTimeOfDay = GET_CURRENT_TIMEOFDAY()
			SET_TIMEOFDAY_HOUR(sTempTimeOfDay, iDestHour)
			SET_TIMEOFDAY_MINUTE(sTempTimeOfDay, iDestMinute)
			SET_TIMEOFDAY_SECOND(sTempTimeOfDay, 0)
			
			//Is start time currently after end time?
			IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(sTimelapse.sStartTimeOfDay, sTempTimeOfDay)
				//If so we need to add 1 day to end time.
				ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, 0, 0, 0, 1)
			ENDIF
			
			//Calculate difference between start and end time in seconds.
			INT iDaysDiff, iWeeksDiff, iMonthsDiff
			GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(sTimelapse.sStartTimeOfDay, sTempTimeOfDay, iSecondsDiff, iMinutesDiff, iHoursDiff, iDaysDiff, iWeeksDiff, iMonthsDiff)
			
			sTimelapse.iSecondsToSkipTotal = iSecondsDiff + (iMinutesDiff*60) + ((iHoursDiff+iAdditionalHours)*3600)
			
			ADVANCE_FRIEND_TIMERS(TO_FLOAT(sTimelapse.iSecondsToSkipTotal) / 3600.0)
			
			sTimelapse.iTimeSkipStage++
		BREAK
		
		CASE 1	FALLTHRU	//BEFORE setting weather and cloud hat
		CASE 2				//AFTER setting weather and cloud hat
			IF fCameraPhase >= 0.5
				IF (sTimelapse.iTimeSkipStage = 1)
				
					// Set any weather types or cloud hats
					IF GET_HASH_KEY(tWeatherType) != 0
						//#218020
						CONST_FLOAT time 1.0	//5.0
						SET_WEATHER_TYPE_OVERTIME_PERSIST(tWeatherType, time)
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Setting weather to ", tWeatherType, ".")
					ENDIF
					IF GET_HASH_KEY(tCloudHat) != 0
						UNLOAD_ALL_CLOUD_HATS()
						LOAD_CLOUD_HAT(tCloudHat)
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Setting cloud hat ", tCloudHat, ".")
					ENDIF
					
					sTimelapse.iTimeSkipStage = 2
				ENDIF
			ENDIF
			
			// Finished spline so set final time and bail out
			IF fCameraPhase >= 0.99
			OR fCameraPhase = -1
				//Work out the time of day we need to jump to this frame.
				sTempTimeOfDay = sTimelapse.sStartTimeOfDay
				ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, sTimelapse.iSecondsToSkipTotal)
				
				#IF IS_DEBUG_BUILD
					PRINT_TIMEOFDAY(sTempTimeOfDay)
				#ENDIF
				
				//Update the game clock with the new time.
				SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTempTimeOfDay), GET_TIMEOFDAY_MINUTE(sTempTimeOfDay), GET_TIMEOFDAY_SECOND(sTempTimeOfDay))
				
				RETURN TRUE
			ENDIF
			
			
			// To make things smoother, dont change time in the first and last 10% of the camera phase
			
			// [x-A/B-A] (clamp 0-1)
			FLOAT fA, fB, fX, fMappedPhase
			
			fX = fCameraPhase
			fA = 0.1
			fB = 0.9
			
			fMappedPhase = CLAMP(((fX - fA) / (fB - fA)), 0, 1)
			
			//Work out the time of day we need to jump to this frame.
			iSecondsToSkipThisFrame = ROUND(sTimelapse.iSecondsToSkipTotal * fMappedPhase)
			sTempTimeOfDay = sTimelapse.sStartTimeOfDay
			ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, iSecondsToSkipThisFrame)
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("fSecondsToSkip:", iSecondsToSkipThisFrame, iDrawLiteralSceneStringRow+3)
			
			str = TIMEOFDAY_TO_TEXT_LABEL(sTempTimeOfDay)
			DrawLiteralSceneString(str, iDrawLiteralSceneStringRow+4)
			#ENDIF
			
			//Update the game clock with the new time.
			SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTempTimeOfDay), GET_TIMEOFDAY_MINUTE(sTempTimeOfDay), GET_TIMEOFDAY_SECOND(sTempTimeOfDay))
			
			IF GET_TIMEOFDAY_DAY(sTempTimeOfDay) <> GET_CLOCK_DAY_OF_MONTH()
				
				//SCRIPT_ASSERT("need to call SET_CLOCK_DATE()")
				PRINTLN("need to call SET_CLOCK_DATE()")
				
				SET_CLOCK_DATE(GET_TIMEOFDAY_DAY(sTempTimeOfDay), GET_TIMEOFDAY_MONTH(sTempTimeOfDay), GET_TIMEOFDAY_YEAR(sTempTimeOfDay))
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL SKIP_TO_TIME_DURING_PHASE_WITH_SOUND(INT iDestHour, INT iDestMinute, STRING tWeatherType, STRING tCloudHat, structTimelapse &sTimelapse, FLOAT fCameraPhase, INT iAdditionalHours = 0)
	
	INT iSecondsDiff
	INT iMinutesDiff
	INT iHoursDiff
	INT iSecondsToSkipThisFrame
	TIMEOFDAY sTempTimeOfDay
	
	#IF IS_DEBUG_BUILD
	CONST_INT iDrawLiteralSceneStringRow	3
	TEXT_LABEL_63 str
	DrawLiteralSceneStringInt("iTimeSkipStage:", sTimelapse.iTimeSkipStage, iDrawLiteralSceneStringRow+0)
	DrawLiteralSceneStringInt("iDestHour:", iDestHour, iDrawLiteralSceneStringRow+1)
	DrawLiteralSceneStringInt("iDestMinute:", iDestMinute, iDrawLiteralSceneStringRow+2)
	#ENDIF
	
	SWITCH sTimelapse.iTimeSkipStage
		CASE 0
			//Save the starting time of day.
			sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
			
			//Work out the ending time of day.
			sTempTimeOfDay = GET_CURRENT_TIMEOFDAY()
			SET_TIMEOFDAY_HOUR(sTempTimeOfDay, iDestHour)
			SET_TIMEOFDAY_MINUTE(sTempTimeOfDay, iDestMinute)
			SET_TIMEOFDAY_SECOND(sTempTimeOfDay, 0)
			
			//Is start time currently after end time?
			IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(sTimelapse.sStartTimeOfDay, sTempTimeOfDay)
				//If so we need to add 1 day to end time.
				ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, 0, 0, 0, 1)
			ENDIF
			
			//Calculate difference between start and end time in seconds.
			INT iDaysDiff, iWeeksDiff, iMonthsDiff
			GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(sTimelapse.sStartTimeOfDay, sTempTimeOfDay, iSecondsDiff, iMinutesDiff, iHoursDiff, iDaysDiff, iWeeksDiff, iMonthsDiff)
			
			sTimelapse.iSecondsToSkipTotal = iSecondsDiff + (iMinutesDiff*60) + ((iHoursDiff+iAdditionalHours)*3600)
			
			ADVANCE_FRIEND_TIMERS(TO_FLOAT(sTimelapse.iSecondsToSkipTotal) / 3600.0)
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
			CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
			CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)		//#1439834
			
			SET_PLAYER_VEH_RADIO_OFF_FOR_TIMELAPSE()
			
			sTimelapse.iSplineStageSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sTimelapse.iSplineStageSound, "TIME_LAPSE_MASTER")
			START_AUDIO_SCENE("TOD_SHIFT_SCENE") 
			
			FLUSH_TEXT_MESSAGE_FEED_ENTRIES()	//1296499
			
			sTimelapse.iTimeSkipStage++
		BREAK
		
		CASE 1	FALLTHRU	//BEFORE setting weather and cloud hat
		CASE 2				//AFTER setting weather and cloud hat
			IF fCameraPhase >= 0.5
				IF (sTimelapse.iTimeSkipStage = 1)
				
					// Set any weather types or cloud hats
					IF GET_HASH_KEY(tWeatherType) != 0
						//#218020
						CONST_FLOAT time 1.0	//5.0
						SET_WEATHER_TYPE_OVERTIME_PERSIST(tWeatherType, time)
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Setting weather to ", tWeatherType, ".")
					ENDIF
					IF GET_HASH_KEY(tCloudHat) != 0
						UNLOAD_ALL_CLOUD_HATS()
						LOAD_CLOUD_HAT(tCloudHat)
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Setting cloud hat ", tCloudHat, ".")
					ENDIF
					
					sTimelapse.iTimeSkipStage = 2
				ENDIF
			ENDIF
			
			// Finished spline so set final time and bail out
			IF fCameraPhase >= 0.99
			OR fCameraPhase = -1
				//Work out the time of day we need to jump to this frame.
				sTempTimeOfDay = sTimelapse.sStartTimeOfDay
				ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, sTimelapse.iSecondsToSkipTotal)
				
				#IF IS_DEBUG_BUILD
					PRINT_TIMEOFDAY(sTempTimeOfDay)
				#ENDIF
				
				//Update the game clock with the new time.
				SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTempTimeOfDay), GET_TIMEOFDAY_MINUTE(sTempTimeOfDay), GET_TIMEOFDAY_SECOND(sTempTimeOfDay))
				
		//		CASCADE_SHADOWS_INIT_SESSION()
				
				STOP_SOUND(sTimelapse.iSplineStageSound)
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
//				STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
				
		//		CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
		//		CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				
				RETURN TRUE
			ENDIF
			
			
			// To make things smoother, dont change time in the first and last 10% of the camera phase
			
			// [x-A/B-A] (clamp 0-1)
			FLOAT fA, fB, fX, fMappedPhase
			
			fX = fCameraPhase
			fA = 0.1
			fB = 0.9
			
			fMappedPhase = CLAMP(((fX - fA) / (fB - fA)), 0, 1)
			
			//Work out the time of day we need to jump to this frame.
			iSecondsToSkipThisFrame = ROUND(sTimelapse.iSecondsToSkipTotal * fMappedPhase)
			sTempTimeOfDay = sTimelapse.sStartTimeOfDay
			ADD_TIME_TO_TIMEOFDAY(sTempTimeOfDay, iSecondsToSkipThisFrame)
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("fSecondsToSkip:", iSecondsToSkipThisFrame, iDrawLiteralSceneStringRow+3)
			
			str = TIMEOFDAY_TO_TEXT_LABEL(sTempTimeOfDay)
			DrawLiteralSceneString(str, iDrawLiteralSceneStringRow+4)
			#ENDIF
			
			//Update the game clock with the new time.
			SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTempTimeOfDay), GET_TIMEOFDAY_MINUTE(sTempTimeOfDay), GET_TIMEOFDAY_SECOND(sTempTimeOfDay))
			
			IF GET_TIMEOFDAY_DAY(sTempTimeOfDay) <> GET_CLOCK_DAY_OF_MONTH()
				
				//SCRIPT_ASSERT("need to call SET_CLOCK_DATE()")
				PRINTLN("need to call SET_CLOCK_DATE()")
				
				SET_CLOCK_DATE(GET_TIMEOFDAY_DAY(sTempTimeOfDay), GET_TIMEOFDAY_MONTH(sTempTimeOfDay), GET_TIMEOFDAY_YEAR(sTempTimeOfDay))
			ENDIF
			
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			RESET_ADAPTATION()		//#1358413
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

#if USE_CLF_DLC
PROC GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTESCLF(SP_MISSIONS missionEnum, structTimelapseSettings &sSettings)
	
	sSettings.iCustomBehaviour = -1
	
	SWITCH missionEnum
	
		CASE SP_MISSION_CLF_RUS_CAR
			sSettings.vCamPos1 = <<2578.0093, 416.3414, 113.9145>>
			sSettings.vCamRot1 = <<-7.7501, -0.0000, 127.7156>>
			
			sSettings.vCamPos2 = <<2600.8674, 395.9536, 113.9145>>
			sSettings.vCamRot2 = <<-7.7501, -0.0000, 138.2687>>
			
			sSettings.fCamFOV = 35.0000
			sSettings.iCamTime = 7000
			sSettings.bSkipToNight = FALSE
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"
		BREAK
	
		CASE SP_MISSION_CLF_RUS_SAT
			sSettings.vCamPos1 = <<-3241.7224, 1249.5206, 17.0910>>
			sSettings.vCamRot1 = <<8.9279, -0.0000, -122.2623>>
			
			sSettings.vCamPos2 = <<-3245.4387, 1241.9141, 17.1640>>
			sSettings.vCamRot2 = <<8.9279, -0.0000, -119.5630>>
			
			sSettings.fCamFOV = 50.0000
			sSettings.iCamTime = 7000
			sSettings.bSkipToNight = FALSE
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"
		BREAK
		
		CASE SP_MISSION_CLF_ASS_VIN
			sSettings.vCamPos1 = <<24.9604, 674.5290, 211.8357>>
			sSettings.vCamRot1 = <<-3.2910, 0.0000, -58.4796>>
			
			sSettings.vCamPos2 = <<25.2208, 674.6888, 217.1480>>
			sSettings.vCamRot2 = <<-3.2910, 0.0000, -58.4796>>
			
			sSettings.fCamFOV = 45.0000
			sSettings.iCamTime = 7000
			sSettings.bSkipToNight = FALSE
			sSettings.tWeatherType = "FOGGY"
			sSettings.tCloudHat = "cirrocumulus"
		BREAK
	
		DEFAULT
			SCRIPT_ASSERT("DO_TIMELAPSE: Mission enum passed does not have a TOD defined. Aborting TOD skip.")
			sSettings.vCamPos1 = <<0,0,0>>	sSettings.vCamRot1 = <<0,0,0>>
			sSettings.vCamPos2 = <<0,0,0>>	sSettings.vCamRot2 = <<0,0,0>>
			sSettings.fCamFOV = -1			sSettings.iCamTime = -1
			sSettings.bSkipToNight = FALSE
		BREAK
		
	ENDSWITCH
	
	//Set all TODS to be sunny.
	IF GET_HASH_KEY("RAIN") = GET_PREV_WEATHER_TYPE_HASH_NAME()
		IF IS_STRING_NULL_OR_EMPTY(sSettings.tWeatherType)
			sSettings.tWeatherType = "EXTRASUNNY"
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(sSettings.tCloudHat)
			sSettings.tCloudHat = "cirrocumulus"
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF
#if USE_NRM_DLC
PROC GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTESNRM(SP_MISSIONS missionEnum, structTimelapseSettings &sSettings)
	
	sSettings.iCustomBehaviour = -1
	
	SWITCH missionEnum
	
		case SP_MISSION_NRM_SUR_START	
		
		break
		DEFAULT
			SCRIPT_ASSERT("DO_TIMELAPSE: Mission enum passed does not have a TOD defined. Aborting TOD skip.")
			sSettings.vCamPos1 = <<0,0,0>>	sSettings.vCamRot1 = <<0,0,0>>
			sSettings.vCamPos2 = <<0,0,0>>	sSettings.vCamRot2 = <<0,0,0>>
			sSettings.fCamFOV = -1			sSettings.iCamTime = -1
			sSettings.bSkipToNight = FALSE
		BREAK
		
	ENDSWITCH
	
	//Set all TODS to be sunny.
	IF GET_HASH_KEY("RAIN") = GET_PREV_WEATHER_TYPE_HASH_NAME()
		IF IS_STRING_NULL_OR_EMPTY(sSettings.tWeatherType)
			sSettings.tWeatherType = "EXTRASUNNY"
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(sSettings.tCloudHat)
			sSettings.tCloudHat = "cirrocumulus"
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF
PROC GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTES(SP_MISSIONS missionEnum, structTimelapseSettings &sSettings)
#if USE_CLF_DLC
	GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTESCLF(missionEnum,sSettings)
	exit
#endif
#if USE_NRM_DLC
	GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTESNRM(missionEnum,sSettings)
	exit
#endif
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC	
	sSettings.iCustomBehaviour = -1
	
	SWITCH missionEnum
	
	//PLEASE ADD ANY NEW CASES TO THEIR RESPECTIVE #IF #ENDIF BLOCKS
	
		CASE SP_MISSION_ARMENIAN_2		//#1317728
		CASE SP_MISSION_ARMENIAN_3		//#1317729
			sSettings.vCamPos1 = <<-58.1338, -1115.6534, 25.8856>>
			sSettings.vCamRot1 = <<18.4907, 0.0000, 3.5660>>
			
			sSettings.vCamPos2 = <<-58.3857, -1115.0834, 26.0824>>
			sSettings.vCamRot2 = <<18.4907, 0.0000, 2.0628>>
			
			sSettings.fCamFOV = 40.0256
			
			sSettings.iCamTime = 4000	//3250
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_FAMILY_1		//LH
		CASE SP_MISSION_FAMILY_2		//#1221096
//			sSettings.vCamPos1 = <<-835.5538, 179.5871, 71.5406>>
//			sSettings.vCamRot1 = <<12.5999, 0.0000, -99.8206>>
//			sSettings.vCamPos2 = <<-835.1459, 179.5165, 71.6331>>
//			sSettings.vCamRot2 = <<12.5999, 0.0000, -99.8206>>
//			sSettings.fCamFOV = 47.9931

			sSettings.vCamPos1 = <<-830.1,171.4,71.5>>
			sSettings.vCamRot1 = <<17.5,-0.0,-76.0>>

			sSettings.vCamPos2 = <<-829.5,171.6,71.7>>
			sSettings.vCamRot2 = <<17.9,-0.0,-75.4>>
			
			sSettings.fCamFOV = 47.9931
			
			sSettings.iCamTime = 6600	//5500	//5000
			sSettings.bSkipToNight = FALSE
			
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"
			
		BREAK
		CASE SP_MISSION_FAMILY_5		//LH
		CASE SP_MISSION_FAMILY_6		//LH
			
			sSettings.vCamPos1 = <<-776.5807, 181.1033, 72.2059>>
			sSettings.vCamRot1 = <<11.8135, -0.0000, 95.7275>>

			sSettings.vCamPos2 = <<-776.8288, 181.0926, 72.2551>>
			sSettings.vCamRot2 = <<11.8134, -0.0000, 95.7284>>

			sSettings.fCamFOV = 46.7255	
			
			sSettings.iCamTime = 5500	//5000
			sSettings.bSkipToNight = FALSE
			
			//#218020
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"			
		BREAK

		CASE SP_MISSION_FAMILY_4 //(sw)
			SWITCH GET_CURRENT_PLAYER_PED_ENUM() 
				CASE CHAR_MICHAEL		//on mission/ on estab switch
					sSettings.vCamPos1 = <<-846.3013, 186.7770, 72.0316>>
					sSettings.vCamRot1 = <<4.202026,0.000000,-111.288170>>
					
					sSettings.vCamPos2 = <<-846.1907, 186.7339, 72.0403>>
					sSettings.vCamRot2 = <<4.2020, 0.0000, -111.2882>>
					
					sSettings.fCamFOV = 47.00
					sSettings.iCamTime = 5500	//5000
					sSettings.bSkipToNight = FALSE
				BREAK
				CASE CHAR_TREVOR		//family2 - move blip
					sSettings.vCamPos1 = <<-846.3013, 186.7770, 72.0316>>
					sSettings.vCamRot1 = <<4.202026,0.000000,-111.288170>>
					
					sSettings.vCamPos2 = <<-846.1907, 186.7339, 72.0403>>
					sSettings.vCamRot2 = <<4.2020, 0.0000, -111.2882>>
					
					sSettings.fCamFOV = 47.00
					sSettings.iCamTime = 5500	//5000
					sSettings.bSkipToNight = FALSE
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE SP_MISSION_FRANKLIN_0		//LH
//			sSettings.vCamPos1 = <<-1.834550,-1480.022095,31.896914>>		//<<-10.327773,-1451.917603,30.769310>>
//			sSettings.vCamRot1 = <<12.213200,1.174389,32.771057>>			//<<8.013208,-0.000000,27.059366>>
//			
//			sSettings.vCamPos2 = <<-1.655947,-1480.240356,33.439064>>		//<<-10.810732,-1450.972168,30.918766>>
//			sSettings.vCamRot2 = <<7.906299,1.174390,32.771049>>			//<<8.013208,-0.000000,27.059366>>
//			
//			sSettings.fCamFOV = 45.000000			//55.096817
			
			//franklin0			
			sSettings.vCamPos1 = <<-8.0845, -1450.3672, 36.8585>>
			sSettings.vCamRot1 = <<13.3776, -0.0000, 4.5130>>
			sSettings.vCamPos2 = <<-9.0519, -1450.4437, 36.8585>>
			sSettings.vCamRot2 = <<13.3776, -0.0000, 4.5130>>
			sSettings.fCamFOV = 39.9659
			
			sSettings.iCamTime = 7200			//6000
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_FINALE_C2		//LH
//			sSettings.vCamPos1 = <<-1.834550,-1480.022095,31.896914>>		//<<-10.327773,-1451.917603,30.769310>>
//			sSettings.vCamRot1 = <<12.213200,1.174389,32.771057>>			//<<8.013208,-0.000000,27.059366>>
//			
//			sSettings.vCamPos2 = <<-1.655947,-1480.240356,33.439064>>		//<<-10.810732,-1450.972168,30.918766>>
//			sSettings.vCamRot2 = <<7.906299,1.174390,32.771049>>			//<<8.013208,-0.000000,27.059366>>
//			
//			sSettings.fCamFOV = 45.000000			//55.096817
			
			//franklin0			
			sSettings.vCamPos1 = <<-1576.1322, 5160.8877, 24.1175>>
			sSettings.vCamRot1 = <<4.8389, -0.0110, 92.1057>>
			sSettings.vCamPos2 = <<-1576.1322, 5160.8877, 24.1175>>
			sSettings.vCamRot2 = <<4.8389, -0.0110, 92.1057>>
			sSettings.fCamFOV = 40.0		
			
			sSettings.iCamTime = 7200			//6000
			sSettings.bSkipToNight = FALSE
		BREAK		

		CASE SP_MISSION_LAMAR		//LH
//			sSettings.vCamPos1 = <<-1.834550,-1480.022095,31.896914>>		//<<-10.327773,-1451.917603,30.769310>>
//			sSettings.vCamRot1 = <<12.213200,1.174389,32.771057>>			//<<8.013208,-0.000000,27.059366>>
//			
//			sSettings.vCamPos2 = <<-1.655947,-1480.240356,33.439064>>		//<<-10.810732,-1450.972168,30.918766>>
//			sSettings.vCamRot2 = <<7.906299,1.174390,32.771049>>			//<<8.013208,-0.000000,27.059366>>
//			
//			sSettings.fCamFOV = 45.000000			//55.096817

			//lamar
			sSettings.vCamPos1 = <<-7.2050, -1471.6555, 31.1614>>
			sSettings.vCamRot1 = <<9.3776, -0.0000, 11.4737>>
			
			sSettings.vCamPos2 = <<-7.2909, -1471.2322, 31.2546>>
			sSettings.vCamRot2 = <<9.3776, -0.0000, 11.4737>>
			sSettings.fCamFOV = 38.5265


			sSettings.iCamTime = 5000			
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_SOLOMON_3
			sSettings.vCamPos1 = <<-1060.6584, -464.0052, 44.9940>>	
			sSettings.vCamRot1 = <<5.6441, 0.0000, -138.7474>>	
		
			sSettings.vCamPos2 = <<-1060.6584, -464.0052, 44.9940>>	
			sSettings.vCamRot2 = <<4.8242, -0.0000, -118.3167>>			

			sSettings.fCamFOV = 37.00000
			
			sSettings.iCamTime = 5000			
			sSettings.bSkipToNight = FALSE
		BREAK
		
//		CASE SP_MISSION_LAMAR
//			sSettings.vCamPos1 = <<-661.343811,-1648.603394,27.200146>>
//			sSettings.vCamRot1 = <<9.130991,-0.000000,-58.545334>>
//			
//			sSettings.vCamPos2 = <<-661.268860,-1648.556763,26.650558>>
//			sSettings.vCamRot2 = <<9.130991,-0.000000,-58.545334>>
//			
//			sSettings.fCamFOV = 37.316353
//			
//			sSettings.iCamTime = 5000			
//			sSettings.bSkipToNight = FALSE
//		BREAK
		
		CASE SP_HEIST_RURAL_1	//LH
		CASE SP_HEIST_RURAL_2	//LH
//			sSettings.vCamPos1 = <<1376.323364,3583.774170,34.899723>>
//			sSettings.vCamRot1 = <<13.630786,-0.972987,-34.807240>>
//		
//			sSettings.vCamPos2 = <<1374.807251,3581.478516,34.899723>>
//			sSettings.vCamRot2 = <<13.630786,-0.972987,-34.807240>>
//			sSettings.fCamFOV = 43.494564
			
			sSettings.vCamPos1 = <<1406.1116, 3590.2307, 34.4113>>
			sSettings.vCamRot1 = <<17.5005, -0.0000, 55.9579>>
			
			sSettings.vCamPos2 = <<1405.6732, 3590.5247, 34.4113>>
			sSettings.vCamRot2 = <<18.4979, -0.0000, 55.9579>>
			sSettings.fCamFOV = 56.3199
			
			sSettings.iCamTime = 6000
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_HEIST_AGENCY_3A			//LH
		CASE SP_HEIST_JEWELRY_1			//LH
		CASE SP_HEIST_JEWELRY_2			//LH
			
			sSettings.vCamPos1 = <<739.149292,-990.396118,29.851938>>
			sSettings.vCamRot1 = <<17.199169,0.000000,44.211842>>
			
			sSettings.vCamPos2 = <<738.419678,-989.646240,30.175814>>
			sSettings.vCamRot2 = <<17.199169,0.000000,44.211842>>
		
			sSettings.fCamFOV = 47.563618
			sSettings.iCamTime = 6000	//5000	//4100
			
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"			
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_HEIST_AGENCY_3B			//MW on LH instruction (bug 1317828)
		
			sSettings.vCamPos1 = <<2503.9, -288.8, 112.8>>
			sSettings.vCamRot1 = <<-0.2,0.0,-169.6>>
			
			sSettings.vCamPos2 = <<2503.9, -288.8, 112.8>>
			sSettings.vCamRot2 = <<-0.2,0.0,-169.6>>
		
			sSettings.fCamFOV = 47.6
			sSettings.iCamTime = 5000	//4100
			
			sSettings.tWeatherType = "EXTRASUNNY"
			sSettings.tCloudHat = "cirrocumulus"			
			sSettings.bSkipToNight = TRUE
		
		BREAK
		
		CASE SP_MISSION_MARTIN_1
			sSettings.vCamPos1 = <<-458.566620,1010.097168,316.373596>>
			sSettings.vCamRot1 = <<14.471864,0.000000,-18.288841>>
			
			sSettings.vCamPos2 = <<-457.3, 1011.6, 316.9>>
			sSettings.vCamRot2 = <<14.5, 0.0, -18.5>>
			sSettings.fCamFOV = 50	
					
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_MICHAEL_4		//LH
//			sSettings.vCamPos1 = <<357.621002,133.342468,105.362190>>			//<<357.706573,118.935471,103.824699>>
//			sSettings.vCamRot1 = <<26.801767,-3.256729,-33.103321>>			//<<6.959806,-1.022222,-4.665223>>
//			
//			sSettings.vCamPos2 = <<359.024170,135.494644,106.660057>>			//<<357.444031,117.900902,113.730827>>
//			sSettings.vCamRot2 = <<26.801767,-3.256729,-33.103321>>			//<<6.959806,-1.022222,-4.665223>>
//			sSettings.fCamFOV = 45.000000											//23.228098		

//			sSettings.vCamPos1 = <<293.4596, 179.2712, 105.2319>>
//			sSettings.vCamRot1 = <<27.1734, 0.0000, -18.6357>>
//
//			sSettings.vCamPos2 = <<293.6478, 179.8293, 105.2319>>
//			sSettings.vCamRot2 = <<27.0933, 0.0000, -18.5078>>
//
//			sSettings.fCamFOV = 56.3255
								
			sSettings.vCamPos1 =<<-723.998230,-155.718445,38.123623>>
			sSettings.vCamRot1 = <<30.120478,0.000000,-99.112427>>
			
			sSettings.vCamPos2 = <<-722.4385, -155.7817, 37.4443>> //fix for 1792905//<<-722.475098,-156.011337,37.444321>>
			sSettings.vCamRot2 = <<2.0429, -0.0000, -101.5132>>//<<1.551403,0.000000,-99.049629>>
			sSettings.fCamFOV = 45.000000
			
											
			sSettings.iCamTime = 6000	//5000
			
			sSettings.bSkipToNight = FALSE
		BREAK
		
		
//		CASE SP_MISSION_CARSTEAL_1		
//			sSettings.vCamPos1 = <<499.482605,-1317.125488,28.500771>>
//			sSettings.vCamRot1 = <<16.330931,-0.000000,87.672691>>
//			
//			sSettings.vCamPos2 = <<497.894165,-1317.125488,28.500771>>
//			sSettings.vCamRot2 = <<16.330931,-0.000000,87.672691>>
//			sSettings.fCamFOV = 53.444447	
//			sSettings.iCamTime = 5000
//			sSettings.bSkipToNight = FALSE
//		BREAK
		
		CASE SP_MISSION_CARSTEAL_2
			SWITCH GET_CURRENT_PLAYER_PED_ENUM() 
				CASE CHAR_TREVOR		//LH
					sSettings.vCamPos1 = <<416.3221, -960.2586, 30.6696>>
					sSettings.vCamRot1 = <<21.5747, -0.0000, -135.2969>>
					
					sSettings.vCamPos2 = <<416.8894, -960.8320, 30.9887>>
					sSettings.vCamRot2 = <<21.9916, 0.0000, -135.2969>>
					
					sSettings.fCamFOV = 47.1057
					sSettings.iCamTime = 6000
					sSettings.bSkipToNight = FALSE
				BREAK
				CASE CHAR_FRANKLIN		//LH
					sSettings.vCamPos1 = <<1393.3628, -2052.5813, 65.4054>>
					sSettings.vCamRot1 = <<2.5107, -0.0000, 51.1167>>
					
					sSettings.vCamPos2 = <<1393.4564, -2052.6563, 68.1460>>
					sSettings.vCamRot2 = <<2.5107, -0.0000, 51.1167>>
					
					sSettings.fCamFOV = 35.9859
					
					sSettings.iCamTime = 6000
					sSettings.bSkipToNight = FALSE
				BREAK
			ENDSWITCH
		BREAK
			
		CASE SP_MISSION_FBI_2	
			sSettings.vCamPos1 = <<59.3, -751.7, 46.8>>
			sSettings.vCamRot1 = <<61.9, 0.0, -63.8>>
			
			sSettings.vCamPos2 =  <<59.3, -751.7, 46.8>>
			sSettings.vCamRot2 = <<61.9, 0.0, -63.8>>
			
			sSettings.fCamFOV = 46.6
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_FBI_5		//LH
			sSettings.vCamPos1 = <<3841.9304, 4464.6582, 2.6587>>
			sSettings.vCamRot1 = <<8.4859, 0.0024, 143.0775>>
			
			sSettings.vCamPos2 = <<3842.5398, 4464.1841, 2.6587>>
			sSettings.vCamRot2 = <<8.4859, 0.0024, 142.0089>>
			
			sSettings.fCamFOV = 34.1544
			sSettings.iCamTime = 3000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_HEIST_DOCKS_1		//LH
// // // // // change for bug #1497796 // // // // // // // // //
//			sSettings.vCamPos1 = <<-1215.8743, -1510.9213, 13.9245>>	
//			sSettings.vCamRot1 = <<4.7026, 0.0000, 67.0013>>			
//																
//			sSettings.vCamPos2 = <<-1215.8634, -1510.9259, 14.0682>>	
//			sSettings.vCamRot2 = <<4.7026, 0.0000, 67.0013>>			
//			sSettings.fCamFOV = 38.0302									
// // // // // // // // // // // // // // // // // // // // // //
			
			sSettings.vCamPos1 = <<-1193.1,-1525.3,4.4>>
			sSettings.vCamRot1 = <<9.4,-0.0,-78.8>>
			
			sSettings.vCamPos2 = <<-1192.2,-1525.1,4.4>>
			sSettings.vCamRot2 = <<9.4,-0.0,-78.5>>
			sSettings.fCamFOV = 30.4
			
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_HEIST_DOCKS_2A		//LH
			sSettings.vCamPos1 = <<-24.2, -2417.6, 7.8>>
			sSettings.vCamRot1 = <<10.4, 0.0000, 80.4>>
			
			sSettings.vCamPos2 = <<-23.8, -2414.8, 7.8>>
			sSettings.vCamRot2 = <<10.4, 0.0000, 81.4>>
			
			sSettings.fCamFOV = 48.1095
			
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		
//		CASE SP_HEIST_FINALE_1		//LH
		CASE SP_HEIST_FINALE_2A		//LH
		CASE SP_HEIST_FINALE_2B		//LH
		CASE SP_MISSION_TREVOR_4 // 1281688
			sSettings.vCamPos1 = <<146.973328,-1310.644409,30.851767>>
			sSettings.vCamRot1 = <<16.268848,-0.000021,45.004749>>
			
			sSettings.vCamPos2 = <<146.973328,-1310.644409,30.851767>>
			sSettings.vCamRot2 = <<16.268848,-0.000021,45.004749>>
			
			sSettings.fCamFOV = 39.402699
			sSettings.iCamTime = 6000	//5000
			
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_TREVOR_1		//1317795 & 1317794
		CASE SP_MISSION_EXILE_2			//LH
		CASE SP_MISSION_EXILE_3			//rob
			sSettings.vCamPos1 = <<1998.2150, 3816.9827, 33.0117>>
			sSettings.vCamRot1 = <<5.9088, -0.0000, 89.1989>>
			
			sSettings.vCamPos2 = <<1997.1537, 3817.0042, 33.1215>>
			sSettings.vCamRot2 = <<5.9088, -0.0000, 89.1989>>
			sSettings.fCamFOV = 36.8186
			
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
			
			
			IF (missionEnum = SP_MISSION_TREVOR_1)
				IF (GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR)
					SCRIPT_ASSERT("DO_TIMELAPSE: Mission enum passed does not have a TOD defined. Aborting TOD skip.")
					sSettings.vCamPos1 = <<0,0,0>>	sSettings.vCamRot1 = <<0,0,0>>
					sSettings.vCamPos2 = <<0,0,0>>	sSettings.vCamRot2 = <<0,0,0>>
					sSettings.fCamFOV = -1			sSettings.iCamTime = -1
					sSettings.bSkipToNight = FALSE
				ENDIF
			ENDIF
		BREAK
		CASE SP_MISSION_TREVOR_2		//1317801
			sSettings.vCamPos1 = <<1576.0, 3363.9, 55.5>>
			sSettings.vCamRot1 = <<0.5, 0.0000, -123.8>>
			
			sSettings.vCamPos2 = <<1575.2, 3364.4, 49.2>>
			sSettings.vCamRot2 = <<-4.3, -0.0000, -124.1>>
			sSettings.fCamFOV = 50.0
			
			sSettings.iCamTime = 8000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_MISSION_TREVOR_3		//LH - on mission (1323493 and 1323495)
			sSettings.vCamPos1 = <<1566.0,3378.8,42.8>> 
			sSettings.vCamRot1 = <<-2.4,0,-126.3>>
			
			sSettings.vCamPos2 = <<1567.6,3376.9,45.0>>
			sSettings.vCamRot2 = <<1.6,0.0,-124.6>>
			sSettings.fCamFOV = 34.7
			sSettings.iCamTime = 8000
			sSettings.bSkipToNight = FALSE
		BREAK		
			
		CASE SP_MISSION_FINALE_A	//LH
			sSettings.vCamPos1 = <<1339.572876,-2550.872803,56.282543>>
			sSettings.vCamRot1 = <<-2.348373,-0.000021,159.735992>>

			sSettings.vCamPos2 = <<1334.490845,-2549.345459,47.691090>>
			sSettings.vCamRot2 = <<2.789334,-0.000021,-156.439682>>

			sSettings.fCamFOV = 25.660002
			
			sSettings.iCamTime = 8000
			sSettings.bSkipToNight = FALSE
			
//			sSettings.iCustomBehaviour = 1
		BREAK		
		CASE SP_MISSION_FINALE_B	//LH
			sSettings.vCamPos1 = <<2383.6511, 2624.7871, 46.9527>>
			sSettings.vCamRot1 = <<14.6191, -0.0000, 64.3660>>

			sSettings.vCamPos2 = <<2383.929199,2622.801514,47.270699>>//<<2383.922607,2622.794189,47.270649>>//<<2383.923096,2622.793945,47.270905>>
			sSettings.vCamRot2 = <<33.177135,-0.130730,12.314363>>//<<33.613609,-0.000955,11.752253>>//<<33.639381,-3.159677,11.397706>>

			sSettings.fCamFOV = 35.318432//35.231113//35.234501
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		
		CASE SP_MISSION_FINALE_C1
			sSettings.vCamPos1 = << 1755.5189, -1474.9811, 126.1743 >> 
			sSettings.vCamRot1 = << 4.5341, -0.0000, 89.6405 >>
			
			sSettings.vCamPos2 = << 1772.2605, -1475.0853, 125.9465 >> 
			sSettings.vCamRot2 = << 5.8327, -0.0000, 89.6405 >>
			sSettings.fCamFOV = 32.7
			sSettings.iCamTime = 6500	//4500
			sSettings.bSkipToNight = FALSE
		BREAK
				
//		CASE SP_MISSION_FINALE_3B
//			sSettings.vCamPos1 = <<1070.737915,-1952.050781,32.787224>>
//			sSettings.vCamRot1 = <<32.277859,0.000000,-169.175262>>
//			
//			sSettings.vCamPos2 = <<1070.737915,-1957.651001,32.787224>>
//			sSettings.vCamRot2 = <<32.277859,0.000000,-169.175262>>
//			sSettings.fCamFOV = 55.000000
//			sSettings.iCamTime = 6000	//5000
//			sSettings.bSkipToNight = FALSE
//		BREAK
		
		CASE SP_MISSION_ASSASSIN_1		//Assassin_Valet
			sSettings.vCamPos1 = <<-1514.2566, -947.7281, 15.0253>>
			sSettings.vCamRot1 = <<-3.5807, -0.0001, -46.3209>>
			
			sSettings.vCamPos2 = <<-1512.6014, -949.2796, 14.8827>>
			sSettings.vCamRot2 = <<-4.7682, -0.0001, -34.1393>>
			sSettings.fCamFOV = 29.9555
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_MISSION_ASSASSIN_2		//assassin_multi
			sSettings.vCamPos1 = <<-698.2578, -934.4238, 31.6909>>
			sSettings.vCamRot1 = <<-22.0467, -0.0000, 32.1888>>
			
			sSettings.vCamPos2 = <<-697.8695, -935.0406, 31.9861>>
			sSettings.vCamRot2 = <<-20.6093, 0.0000, 32.1888>>
			sSettings.fCamFOV = 30.0000
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_MISSION_ASSASSIN_3		//assassin_hooker
			sSettings.vCamPos1 = <<221.8613, -830.6434, 45.3154>>
			sSettings.vCamRot1 = <<-13.9263, -0.0000, 140.7092>>
			
			sSettings.vCamPos2 = <<227.5233, -832.8285, 45.3154>>
			sSettings.vCamRot2 = <<-13.9263, -0.0000, 140.7092>>
			sSettings.fCamFOV = 35.0000
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_MISSION_ASSASSIN_4		//Assassin_Bus
			sSettings.vCamPos1 = <<-23.3747, -120.3161, 64.1390>>
			sSettings.vCamRot1 = <<-5.7232, 0.0000, 5.1015>>
			
			sSettings.vCamPos2 = <<-22.5824, -122.5200, 64.1390>>
			sSettings.vCamRot2 = <<-5.7232, 0.0000, 6.6362>>
			sSettings.fCamFOV = 35.0000
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
		CASE SP_MISSION_ASSASSIN_5		//assassin_construction
			sSettings.vCamPos1 = <<803.5977, -1075.8688, 37.1981>>
			sSettings.vCamRot1 = <<-10.7685, 0.0000, -38.1705>>
			
			sSettings.vCamPos2 = <<803.5977, -1075.8688, 32.7981>>
			sSettings.vCamRot2 = <<-10.7685, 0.0000, -38.1705>>
			
			sSettings.fCamFOV = 35.0000
			sSettings.iCamTime = 6000	//5000
			sSettings.bSkipToNight = FALSE
		BREAK
	
		
		DEFAULT
			SCRIPT_ASSERT("DO_TIMELAPSE: Mission enum passed does not have a TOD defined. Aborting TOD skip.")
			sSettings.vCamPos1 = <<0,0,0>>	sSettings.vCamRot1 = <<0,0,0>>
			sSettings.vCamPos2 = <<0,0,0>>	sSettings.vCamRot2 = <<0,0,0>>
			sSettings.fCamFOV = -1			sSettings.iCamTime = -1
			sSettings.bSkipToNight = FALSE
		BREAK
		
	ENDSWITCH
	
	//Set all TODS to be sunny.
	IF GET_HASH_KEY("RAIN") = GET_PREV_WEATHER_TYPE_HASH_NAME()
		IF IS_STRING_NULL_OR_EMPTY(sSettings.tWeatherType)
			sSettings.tWeatherType = "EXTRASUNNY"
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(sSettings.tCloudHat)
			sSettings.tCloudHat = "cirrocumulus"
		ENDIF
	ENDIF
#endif
#endif
ENDPROC

PROC GET_SP_MISSION_TOD_WINDOW_TIME(SP_MISSIONS missionEnum, INT &iStartHour, INT &iEndHour)
	SWITCH missionEnum
		#if USE_CLF_DLC
			CASE SP_MISSION_CLF_TRAIN
				iStartHour = g_sMissionStaticData[missionEnum].endHour // TOD skip start time is the end of the mission trigger's available hours.
				iEndHour = g_sMissionStaticData[missionEnum].startHour // TOD skip end time is the beginning of the mission trigger's legal hours.
			BREAK
		#endif
		#if USE_NRM_DLC
			CASE SP_MISSION_NRM_SUR_START
				iStartHour = g_sMissionStaticData[missionEnum].endHour // TOD skip start time is the end of the mission trigger's available hours.
				iEndHour = g_sMissionStaticData[missionEnum].startHour // TOD skip end time is the beginning of the mission trigger's legal hours.
			BREAK
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			CASE SP_MISSION_TREVOR_4
				iStartHour = 20
				iEndHour = 22
			BREAK
			CASE SP_MISSION_FINALE_C2
				iStartHour = 20
				iEndHour = 20
			BREAK	
		#endif
		#endif
		DEFAULT
			// Time window data moved to flow so the mission trigger can use it.
			// X:/gta5/script/dev/singleplayer/include/private/Mission_Flow/Flow_Mission_Data_GTA5.sch
			iStartHour = g_sMissionStaticData[missionEnum].endHour // TOD skip start time is the end of the mission trigger's available hours.
			iEndHour = g_sMissionStaticData[missionEnum].startHour // TOD skip end time is the beginning of the mission trigger's legal hours.
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE: Runs a short cutscene timelapse based on settings configured in a timelapse setting stuct.
/// RETURNS: TRUE when Timelapse is complete or time is already within the valid window.
///  
FUNC BOOL DO_TIMELAPSE_WITH_SETTINGS(structTimelapse &sTimelapse, structTimelapseSettings &sSettings, BOOL bTriggerTODRegardlessOfTimeForClothes = FALSE, BOOL bClearTheArea = TRUE, BOOL bMidMissionToD = FALSE, BOOL bIgnoreButtonPressSkip = FALSE, BOOL bKeepPlayerVisibleForTOD = FALSE)
	
	CONST_FLOAT fCONST_TIMELAPSE_CLEAR_AREA_RADIUS	5000.0	//300.0	//#1448970
	
	sTimelapse.iTimeWindowStart = sSettings.iLapseStartHour
	sTimelapse.iTimeWindowEnd = sSettings.iLapseEndHour
	
	IF IS_REPEAT_PLAY_ACTIVE()
	AND NOT bTriggerTODRegardlessOfTimeForClothes
	AND NOT bMidMissionToD
		// Playing mission via repeat play (Skip the timelapse)
		IF sTimelapse.bSkipToNightTime
			CPRINTLN(DEBUG_REPEAT, "Repeat play active: Skipping timelapse. To: ", sTimelapse.iTimeWindowStart)
			SET_CLOCK_TIME(sTimelapse.iTimeWindowStart, 00, 00)
		ELSE
			CPRINTLN(DEBUG_REPEAT, "Repeat play active: Skipping timelapse. To: ", sTimelapse.iTimeWindowEnd)
			SET_CLOCK_TIME(sTimelapse.iTimeWindowEnd, 00, 00)
		ENDIF
	
		RETURN TRUE
	ELSE
		CONST_INT iTIMELAPSE_0_request		0
		CONST_INT iTIMELAPSE_1_create		1
		CONST_INT iTIMELAPSE_22_run			22		//custom
		CONST_INT iTIMELAPSE_2_run			2
		CONST_INT iTIMELAPSE_3_hold			3
		CONST_INT iTIMELAPSE_4_end			4
		
		CONST_INT iTIMELAPSE_null			-1
		
		#IF IS_DEBUG_BUILD
		CONST_INT iDrawLiteralSceneStringRow	0
//		TEXT_LABEL_63 str
		DrawLiteralSceneStringInt("iTimelapseCut:", sTimelapse.iTimelapseCut, iDrawLiteralSceneStringRow+0)
		#ENDIF
		
		// Playing mission normally
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_0_request)
			IF sTimelapse.iTimeWindowStart != NULL_HOUR AND sTimelapse.iTimeWindowEnd != NULL_HOUR 
				IF bTriggerTODRegardlessOfTimeForClothes
				OR NOT IS_TIME_BETWEEN_THESE_HOURS(sTimelapse.iTimeWindowEnd, sTimelapse.iTimeWindowStart)
					CPRINTLN(DEBUG_SYSTEM, "<TOD> waiting on request AUDIO BANK \"TIME_LAPSE\".")
					IF REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
						sTimelapse.bCutsceneSkipped = FALSE
						sTimelapse.iTimelapseCut = iTIMELAPSE_1_create
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Time within valid window. Start hour:", sTimelapse.iTimeWindowEnd, " End hour:", sTimelapse.iTimeWindowStart, " Current hour:", GET_CLOCK_HOURS())
					sTimelapse.iTimelapseCut = iTIMELAPSE_null
					RETURN TRUE
				ENDIF
			ELSE
				SCRIPT_ASSERT("DO_TIMELAPSE: Tried to run a timelapse for a mission that didn't have start/end hours set.")
				sTimelapse.iTimelapseCut = iTIMELAPSE_null
				RETURN TRUE
			ENDIF
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_1_create)
			IF bTriggerTODRegardlessOfTimeForClothes
				#IF IS_DEBUG_BUILD
					IF sTimelapse.bSkipToNightTime
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating FORCED NIGHT timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window end hour ", sTimelapse.iTimeWindowEnd, ".")
					ELSE
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating FORCED DAY timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window start hour ", sTimelapse.iTimeWindowStart, ".")
					ENDIF
				#ENDIF

				DESTROY_CAM(sTimelapse.splineCamera)
				sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
				
				ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
					
				IF (sSettings.iCustomBehaviour < 0)
					ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos2, sSettings.vCamRot2, sSettings.iCamTime)
				ELSE
					ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
				ENDIF
				
				SET_CAM_FOV(sTimelapse.splineCamera, sSettings.fCamFOV)
				SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
				
				
				IF NOT bKeepPlayerVisibleForTOD
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player as TOD camera activates.")
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
					VEHICLE_INDEX vehPlayerLast
					vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
					IF DOES_ENTITY_EXIST(vehPlayerLast)
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player's last vehicle as TOD camera activates.")
						IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayerLast)
							SET_ENTITY_AS_MISSION_ENTITY(vehPlayerLast)
						ENDIF
						SET_ENTITY_VISIBLE(vehPlayerLast, FALSE)
					ENDIF
				ENDIF
				
				FLOAT fGet_cam_far_clip = GET_CAM_FAR_CLIP(sTimelapse.splineCamera)
				
				CLEAR_ROOM_FOR_GAME_VIEWPORT()
				CLEAR_AREA_OF_COPS(sSettings.vCamPos1, fGet_cam_far_clip)	//#1012871
				
				REMOVE_PARTICLE_FX_IN_RANGE(sSettings.vCamPos1, fGet_cam_far_clip)
				REMOVE_DECALS_IN_RANGE(sSettings.vCamPos1, fGet_cam_far_clip)
				
				IF bClearTheArea
					CLEAR_AREA(sSettings.vCamPos1, fCONST_TIMELAPSE_CLEAR_AREA_RADIUS, TRUE, TRUE)
				ENDIF
				
				SET_TIMEOFDAY(sTimelapse.currentTimeOfDay, 0, 0, 0, GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
				SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
				
				sTimelapse.iTimelapseCut = iTIMELAPSE_2_run
				CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse running...")
			ELIF NOT IS_TIME_BETWEEN_THESE_HOURS(sTimelapse.iTimeWindowStart, sTimelapse.iTimeWindowEnd) //IS_DAYLIGHT_HOURS()
			 	IF sTimelapse.bSkipToNightTime
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating NIGHT timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window end hour ", sTimelapse.iTimeWindowEnd, ".")
				
					DESTROY_CAM(sTimelapse.splineCamera)
					sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
					
					ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
					
					IF (sSettings.iCustomBehaviour < 0)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos2, sSettings.vCamRot2, sSettings.iCamTime)
					ELSE
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
					ENDIF
					
					SET_CAM_FOV(sTimelapse.splineCamera, sSettings.fCamFOV)
					SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
					
					IF NOT bKeepPlayerVisibleForTOD
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player as TOD camera activates.")
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						ENDIF
						VEHICLE_INDEX vehPlayerLast
						vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
						IF DOES_ENTITY_EXIST(vehPlayerLast)
							CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player's last vehicle as TOD camera activates.")
							IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayerLast)
								SET_ENTITY_AS_MISSION_ENTITY(vehPlayerLast)
							ENDIF
							SET_ENTITY_VISIBLE(vehPlayerLast, FALSE)
						ENDIF
					ENDIF
					
					FLOAT fGet_cam_far_clip = GET_CAM_FAR_CLIP(sTimelapse.splineCamera)
				
					CLEAR_ROOM_FOR_GAME_VIEWPORT()
					CLEAR_AREA_OF_COPS(sSettings.vCamPos1, fGet_cam_far_clip)	//#1012871
					
					REMOVE_PARTICLE_FX_IN_RANGE(sSettings.vCamPos1, fGet_cam_far_clip)
					REMOVE_DECALS_IN_RANGE(sSettings.vCamPos1, fGet_cam_far_clip)
					
					IF bClearTheArea
						CLEAR_AREA(sSettings.vCamPos1, fCONST_TIMELAPSE_CLEAR_AREA_RADIUS, TRUE, TRUE)
					ENDIF
					
					SET_TIMEOFDAY(sTimelapse.currentTimeOfDay, 0, 0, 8, GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
											
					sTimelapse.iTimelapseCut = iTIMELAPSE_2_run
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse running...")
				ELSE
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Night timelapse not running as current hour ", GET_CLOCK_HOURS(), " is already in valid window [", sTimelapse.iTimeWindowEnd, "->", sTimelapse.iTimeWindowStart, "].")
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
					sTimelapse.iTimelapseCut = iTIMELAPSE_null
					RETURN TRUE
				ENDIF
			ELSE							
				IF NOT sTimelapse.bSkipToNightTime
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating DAY timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window start hour ", sTimelapse.iTimeWindowStart, ".")
			
					DESTROY_CAM(sTimelapse.splineCamera)
					sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
					
					ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
					
					IF (sSettings.iCustomBehaviour < 0)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos2, sSettings.vCamRot2, sSettings.iCamTime)
					ELSE
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
					ENDIF
					
					SET_CAM_FOV(sTimelapse.splineCamera, sSettings.fCamFOV)
					SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
					SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
					
					IF NOT bKeepPlayerVisibleForTOD
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player as TOD camera activates.")
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						ENDIF
						VEHICLE_INDEX vehPlayerLast
						vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
						IF DOES_ENTITY_EXIST(vehPlayerLast)
							CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player's last vehicle as TOD camera activates.")
							IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayerLast)
								SET_ENTITY_AS_MISSION_ENTITY(vehPlayerLast)
							ENDIF
							SET_ENTITY_VISIBLE(vehPlayerLast, FALSE)
						ENDIF
					ENDIF
					
					FLOAT fGet_cam_far_clip = GET_CAM_FAR_CLIP(sTimelapse.splineCamera)
				
					CLEAR_ROOM_FOR_GAME_VIEWPORT()
					CLEAR_AREA_OF_COPS(sSettings.vCamPos1, fGet_cam_far_clip)	//#1012871
					
					REMOVE_PARTICLE_FX_IN_RANGE(sSettings.vCamPos1, fGet_cam_far_clip)
					REMOVE_DECALS_IN_RANGE(sSettings.vCamPos1, fGet_cam_far_clip)
					
					IF bClearTheArea
						CLEAR_AREA(sSettings.vCamPos1, fCONST_TIMELAPSE_CLEAR_AREA_RADIUS, FALSE, TRUE)
					ENDIF
					
					SET_TIMEOFDAY(sTimelapse.currentTimeOfDay, 0, 0, 0, GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
					
					sTimelapse.iTimelapseCut = iTIMELAPSE_2_run
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse running...")
				ELSE
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Day timelapse not running as current hour ", GET_CLOCK_HOURS(), " is already in valid window [", sTimelapse.iTimeWindowStart, "->", sTimelapse.iTimeWindowEnd, "].")
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
					sTimelapse.iTimelapseCut = iTIMELAPSE_null
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_2_run)
			IF bClearTheArea
				CLEAR_AREA(sSettings.vCamPos1, fCONST_TIMELAPSE_CLEAR_AREA_RADIUS, FALSE, TRUE)
			ENDIF
			
			IF NOT sTimelapse.bSkipToNightTime
				IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(sTimelapse.iTimeWindowEnd, 0, sSettings.tWeatherType, sSettings.tCloudHat, sTimelapse)
					IF (sSettings.iCustomBehaviour < 0)
						sTimelapse.iGameTimeHold = GET_GAME_TIMER()
						sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse holding...")
					ELSE
						sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos2, sSettings.vCamRot2, sSettings.iCamTime)
						SET_CAM_FOV(sTimelapse.splineCamera, sSettings.fCamFOV)
						SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
						SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
						
						sTimelapse.iGameTimeHold = GET_GAME_TIMER()
						sTimelapse.iTimelapseCut = iTIMELAPSE_22_run
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Custom timelapse running...")
					ENDIF
				ENDIF
			ELSE
				IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(sTimelapse.iTimeWindowStart, 0, sSettings.tWeatherType, sSettings.tCloudHat, sTimelapse)
					IF (sSettings.iCustomBehaviour < 0)
						sTimelapse.iGameTimeHold = GET_GAME_TIMER()
						sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse holding...")
					ELSE
						sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos1, sSettings.vCamRot1, sSettings.iCamTime)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sSettings.vCamPos2, sSettings.vCamRot2, sSettings.iCamTime)
						SET_CAM_FOV(sTimelapse.splineCamera, sSettings.fCamFOV)
						SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
						SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
						
						sTimelapse.iGameTimeHold = GET_GAME_TIMER()
						sTimelapse.iTimelapseCut = iTIMELAPSE_22_run
						CPRINTLN(DEBUG_SYSTEM, "<TOD> Custom timelapse running...")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bIgnoreButtonPressSkip
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				OR sTimelapse.bCutsceneSkipped
					sTimelapse.bCutsceneSkipped = TRUE
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse skipped by user input.")
					
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
					ENDIF
					IF NOT IS_SCREEN_FADED_OUT()
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)		
						RETURN FALSE
					ENDIF
					
					IF (sTimelapse.iTimeSkipStage <> 0)
						SKIP_TO_TIME_DURING_SPLINE_CAMERA(sTimelapse.iTimeWindowStart, 0, sSettings.tWeatherType, sSettings.tCloudHat, sTimelapse, 1.0)
					ENDIF
					
					//SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					sTimelapse.iTimelapseCut = iTIMELAPSE_4_end
				ENDIF
			ENDIF
		ENDIF
	
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_22_run)
			IF bClearTheArea
				CLEAR_AREA(sSettings.vCamPos1, fCONST_TIMELAPSE_CLEAR_AREA_RADIUS, FALSE, TRUE)
			ENDIF
			
			IF (DOES_CAM_EXIST(sTimelapse.splineCamera)
			AND IS_CAM_INTERPOLATING(sTimelapse.splineCamera))
				//
			ELSE
				sTimelapse.iGameTimeHold = GET_GAME_TIMER()
				sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold
				CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse holding...")
			ENDIF
			
			IF NOT bIgnoreButtonPressSkip
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				OR sTimelapse.bCutsceneSkipped
					sTimelapse.bCutsceneSkipped = TRUE
					CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse skipped by user input.")
					
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
					ENDIF
					IF NOT IS_SCREEN_FADED_OUT()
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)		
						RETURN FALSE
					ENDIF
					
					//SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					sTimelapse.iTimelapseCut = iTIMELAPSE_4_end
				ENDIF
			ENDIF
		ENDIF
		
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold)
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("iTimelapseCut:", GET_GAME_TIMER()-sTimelapse.iTimelapseCut, iDrawLiteralSceneStringRow+1)
			#ENDIF
			
			IF bClearTheArea
				CLEAR_AREA(sSettings.vCamPos1, fCONST_TIMELAPSE_CLEAR_AREA_RADIUS, FALSE, TRUE)
			ENDIF
			
			IF (sTimelapse.iGameTimeHold+1000) > GET_GAME_TIMER()
				sTimelapse.iTimelapseCut = iTIMELAPSE_4_end
				CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse ending...")
			ENDIF
		ENDIF
		
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_4_end)
			sTimelapse.bCutsceneSkipped = FALSE
			sTimelapse.iTimelapseCut = iTIMELAPSE_null
			CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse ended. Waiting for script to trigger cleanup.")
			RETURN TRUE
		ENDIF
		
		IF (sTimelapse.iTimelapseCut = iTIMELAPSE_null)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF

ENDFUNC

/// PURPOSE: Runs a short cutscene timelapse based on data bound to a specific story mission.
///    		Fastforwards to the appropriate time for mission start.
/// RETURNS: TRUE when Timelapse is complete or mission is within acceptable time window.
///    
FUNC BOOL DO_TIMELAPSE(SP_MISSIONS missionEnum, structTimelapse &sTimelapse, BOOL bTriggerTODRegardlessOfTimeForClothes = FALSE, BOOL bClearTheArea = TRUE, BOOL bMidMissionToD = FALSE, BOOL bIgnoreButtonPressSkip = FALSE, BOOL bKeepPlayerVisibleForTOD = FALSE)
	structTimelapseSettings sTimelapseSettings
	
	GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTES(missionEnum, sTimelapseSettings)
	GET_SP_MISSION_TOD_WINDOW_TIME(missionEnum, sTimelapseSettings.iLapseStartHour, sTimelapseSettings.iLapseEndHour)
	
	RETURN DO_TIMELAPSE_WITH_SETTINGS(sTimelapse, sTimelapseSettings, bTriggerTODRegardlessOfTimeForClothes, bClearTheArea, bMidMissionToD, bIgnoreButtonPressSkip, bKeepPlayerVisibleForTOD)
ENDFUNC
