// website_lfi.sch - store the Lifeinvader stuff here because it's so big it's crashing the script editor...
//USING "flow_public_core.sch" // Comment this in to get the RC/SP mission enums recognised

//*************************************************
// --------------------------------------------
// LIFEINVADER STUFF STARTS HERE
// --------------------------------------------
//*************************************************

// NOTE:
// THE FORMATTING/SPACING IS GOING TO BE BALLS FOR SOME OF THESE FUNCTIONS BECAUSE MULTIPLE
// PEOPLE WORKED ON THEM AND COORDINATED VIA EMAIL WHICH HATES SCRIPT FILE FORMATTING
// I'M NOT WASTING TIME FIXING IT ALL
// BASICALLY: DEAL WITH IT

// ---------------------------------------
// ENUMS AND VARIABLES
// ---------------------------------------

ENUM LFICharList
	LFI_MICHAEL = 0,
	LFI_FRANKLIN,
	LFI_TREVOR,

	// Michael's friends
	LFI_TDESANTA,
	LFI_JDESANTA,
	LFI_ADESANTA,
	LFI_LESTERCREST,
	LFI_MARYANNQUINN,
	LFI_HAYDENDUBOSE,
	LFI_KYLECHAVIS,

	// Michael's stalking brands
	LFI_REDWOOD,

	// Michaels's misc characters
	LFI_FREDDY,
	LFI_RALPH,
	LFI_LAUREN,
	LFI_FOSTER,
	LFI_BREE,
	LFI_KIM,
	LFI_LISAWALLIN,
	LFI_IAN,
	LFI_AHRON,
	LFI_RYAN,
	LFI_COLIN,
	LFI_EVAN,
	LFI_ROSS,
	LFI_JAY,
	LFI_AMY,
	LFI_FABIEN,
	LFI_LISABARCLAY,
	LFI_DRSTANOVICZ,
	LFI_KERI,
	LFI_PETER,
	LFI_SANDRA,
	LFI_FIONA,
	LFI_CHERYL,
	LFI_GUADALOUPE,
	LFI_MAURIE,
	LFI_KANYA,
	LFI_NILES,
	LFI_GILES,
	LFI_DRWETHERBROOK,
	LFI_JONATHAN,
	LFI_RICHARD,
	LFI_MOTHER,
	LFI_MELINDA,
	LFI_JASPER,
	LFI_REBECCA,
	LFI_DENA,
	LFI_ALEX,
	LFI_SUSAN,
	LFI_ALICE,
	LFI_BILLY,
	LFI_LIZ,
	LFI_KELLY,
	LFI_BRIAN,
	LFI_GEMMA,
	LFI_GERRY,
	LFI_GARYSCALES,
	LFI_MFRIENDS432,
	LFI_MFRIENDS142,
	LFI_MFRIENDS205,
	LFI_MFRIENDS63,
	LFI_MFRIENDS58,
	LFI_MFRIENDS86,
	LFI_MFRIENDS116,
	LFI_MFRIENDSXX1,
	LFI_MFRIENDSXX2,

	// Michael's misc brands for stalking
	LFI_FAMEORSHAME,
	LFI_RIGHTEOUSSLAUGHTER,
	LFI_GAMMI,
	LFI_PEDALANDMETAL,
	LFI_PROLAPS,
	LFI_LOSSANTOSGOLFCLUB,
	LFI_FACADE,
	LFI_EGOCHASER,
	LFI_HEAT,
	LFI_PENDULUS,
	LFI_AMMU,

	// Franklin's friends
	LFI_LAMAR,
	LFI_SIMEON,
	LFI_TONYA,
	LFI_DENISE,
	LFI_STRETCH,
	LFI_TANISHA,
	LFI_TAVELL,
	LFI_DOM,
	LFI_BEV,
	LFI_DEVIN,
	LFI_DEMARCUS,
		
	LFI_FFRIENDS93,
	LFI_FFRIENDS77,
	LFI_FFRIENDS42,
	LFI_FFRIENDS118,
	LFI_FFRIENDS72,
	LFI_FFRIENDS226,
	LFI_FFRIENDS175,
	LFI_FFRIENDS27,
	LFI_FFRIENDS64,
	LFI_FFRIENDS1644,
	LFI_FFRIENDS118B,
	LFI_FFRIENDSXX1,
	LFI_FFRIENDSXX2,
	LFI_FFRIENDSXX3,
	LFI_FFRIENDSXX4,
	LFI_FFRIENDSXX5,

	// Franklin's stalking
	LFI_FEUD,
	LFI_SPRUNK,
	LFI_INKINC,
	LFI_HERRKUTZ,
	LFI_LSCUSTOMS,

	// Franklin misc
	LFI_ANDREB,
	LFI_NICOLSONB,
	LFI_JAMALR,
	LFI_JBB,
	LFI_DARRYLK,
	LFI_DANAE,
	LFI_LEONV,
	LFI_GERALDG,
	LFI_OWENY,
	LFI_BARBARAW,
	LFI_ANAKH,
	LFI_YERGHATT,
	LFI_SACHAY,
	LFI_TYSONF,
	LFI_KEEARAN,
	LFI_SHARONDAM,
	LFI_MADISONF,
	LFI_COLIND,
	LFI_NIAB,
	LFI_LAHRONDAW,
	LFI_MATTIEH,
	LFI_MAGENTAA,
	LFI_JULIEP,
	LFI_PIPPYE,
	LFI_RENATTAS,
	LFI_LELAY,
	LFI_CATRINAW,
	LFI_MONETTEA,
	LFI_GARRYH,
	LFI_JEFFC,
	LFI_KARENL,
	LFI_MARKP,
	LFI_ALANF,
	LFI_RAYG,
	LFI_RAYN,
	LFI_SIMONH,
	LFI_MOLLYS,
	LFI_DEJAM,
	LFI_HAILEYB,
	LFI_ERIKD,
	LFI_GRAHAMR,
	LFI_NELSONW,
	LFI_MODFEUD,
	LFI_JAXS,
	LFI_ADRICH,
	LFI_KARLR,
	LFI_TAYEB,
	LFI_TRENTONM,
	LFI_LONNYG,
	LFI_REGISW,
	LFI_RESHAYM,
	LFI_ANTOINEP,
	LFI_BENTONC,
	LFI_STERLINL,
	LFI_SARAP,
	LFI_DARNELLS,
	LFI_PAULO,
	LFI_KATEM,
	LFI_JOSHUAW,
	LFI_VINCEH,
	LFI_TODDR,
	LFI_STEVEW,
	LFI_MIKEH,
	LFI_HARVEYP,
	LFI_ANDYW,
	LFI_TAYLOTH,
	LFI_DONNAHK,
	LFI_LANCEW,
	LFI_PHILG,
	LFI_JAMIEE,
	LFI_JONH,
	LFI_GERRYC,
	LFI_AARONF,
	LFI_CONNORS,
	LFI_POPPAL,
	LFI_HAILEYW,
	LFI_SAMW,
	LFI_GARYH,
	LFI_HANKS,  

	// Franklin misc brands for stalking
	LFI_PRDE,
	LFI_GRTR,
	LFI_VANG,
	LFI_STST,
	LFI_BWSQ,
	LFI_CCCK,
	LFI_ALCO,
	LFI_MXRE,
	LFI_STCE,

	// Trevor's friends
	LFI_RON,
	LFI_WADE,

	// Trevor's stalking brands
	LFI_LUDENDORFF,

	// Trevor's misc characters
	LFI_ASHLEY,
	LFI_CLETUS,
	LFI_JENNY,
	LFI_FLOYD,
	LFI_HANK,
	LFI_TFRIENDS10,
	LFI_TFRIENDS27,
	LFI_TFRIENDSXX,

	// Trevor's misc brands for stalking
	LFI_FATALINCURSIONS,
	LFI_BCR,

	// Special 'characters', i.e. "no family" (not real profiles, but still need to return a name string or picture txd)
	LFI_NOFAM,

	// THIS IS ALWAYS THE FINAL ELEMENT, DON'T REMOVE THIS
	LFI_MAX
ENDENUM

ENUM LFIWallDisplay
	LFIWall_Messages,
	LFIWall_Friends,
	LFIWall_Stalking
ENDENUM


INT iCurrentLFID = -1
INT iPreviousLFID = -1
LFIWallDisplay LFIWall_CurrentDisplay = LFIWall_Messages

INT iLastSlot = -1 // Keeps track of the last slot number that was used when iterating through messages/friends/stalks, used when populating the sidebar afterwards

CONST_INT MAX_LFI_POSTS 100
LFICharList CharWallPostID[MAX_LFI_POSTS] // This will store a character enum for each message that a character has written on the current character's webpage

CONST_INT MAX_LFI_HISTORY 20

// These two must be kept in sync!!
INT iLFIPageHistory[MAX_LFI_HISTORY]
LFIWallDisplay LFIWallHistory[MAX_LFI_HISTORY]

INT iLFIPageHistoryIndex

// ---------------------------------------
// USEFUL INFO FUNCTIONS
// ---------------------------------------

FUNC BOOL DOES_CHAR_HAVE_FULL_LFI_PAGE(LFICharList LFICurrentChar)

	// Special case for Dom - because you can get to his page from Sprunk before Extreme 1 is complete
	// Check if Extreme 1 is done - if not, change all of this stuff to an inaccessible profile
	IF LFICurrentChar = LFI_DOM
		IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_EXTREME_1), CP_GROUP_RANDOMCHARS)
			RETURN FALSE
		ENDIF
	ENDIF

      // If the character we're checking is in this list, they have a full profile to look at
      SWITCH LFICurrentChar
            // Michael & Friends
            CASE LFI_MICHAEL
			CASE LFI_TDESANTA
			CASE LFI_JDESANTA
			CASE LFI_ADESANTA
			CASE LFI_LESTERCREST
			CASE LFI_MARYANNQUINN
			CASE LFI_HAYDENDUBOSE
			CASE LFI_KYLECHAVIS
      		CASE LFI_REDWOOD
      
            // Franklin & Friends
            CASE LFI_FRANKLIN
            CASE LFI_LAMAR
            CASE LFI_SIMEON
            CASE LFI_TONYA
            CASE LFI_DENISE
            CASE LFI_STRETCH
            CASE LFI_TANISHA
            CASE LFI_TAVELL
            CASE LFI_DOM
            CASE LFI_BEV
            CASE LFI_DEVIN
            CASE LFI_DEMARCUS
            CASE LFI_FEUD
            CASE LFI_SPRUNK
            CASE LFI_INKINC
            CASE LFI_HERRKUTZ
            CASE LFI_LSCUSTOMS
            
			// Trevor & Friends
            CASE LFI_TREVOR
	  		CASE LFI_RON
	  		CASE LFI_WADE
			CASE LFI_LUDENDORFF
				RETURN TRUE
      ENDSWITCH
      
      RETURN FALSE

ENDFUNC

FUNC BOOL IS_LFI_CHAR_A_BRAND(LFICharList LFICurrentChar)

	SWITCH LFICurrentChar
		CASE LFI_REDWOOD
		CASE LFI_FEUD
        CASE LFI_SPRUNK
        CASE LFI_INKINC
        CASE LFI_HERRKUTZ
        CASE LFI_LSCUSTOMS
		CASE LFI_LUDENDORFF
		CASE LFI_AMMU
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MAX_MESSAGES_FOR_ID(int LFI_ID)

      SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
		    CASE LFI_MICHAEL
				RETURN 77
			BREAK
			CASE LFI_TDESANTA
				RETURN 65
			BREAK
			CASE LFI_JDESANTA
				RETURN 65
			BREAK
			CASE LFI_ADESANTA
				RETURN 49
			BREAK
			CASE LFI_LESTERCREST
				RETURN 25
			BREAK
			CASE LFI_MARYANNQUINN
				RETURN 28
			BREAK
			CASE LFI_HAYDENDUBOSE
				RETURN 25
			BREAK
			CASE LFI_KYLECHAVIS
				RETURN 17
			BREAK
      		CASE LFI_REDWOOD
				RETURN 21
			BREAK
           
			CASE LFI_FRANKLIN
                  RETURN 57
            BREAK
            CASE LFI_LAMAR
                  RETURN 29
            BREAK
            CASE LFI_SIMEON
                  RETURN 21
            BREAK
            CASE LFI_TONYA
                  RETURN 40
            BREAK
            CASE LFI_DENISE
                  RETURN 26
            BREAK
            CASE LFI_STRETCH
                  RETURN 30
            BREAK
            CASE LFI_TANISHA
                  RETURN 29
            BREAK
            CASE LFI_TAVELL
                  RETURN 23
            BREAK
            CASE LFI_DOM
                  RETURN 37
            BREAK
            CASE LFI_BEV
                  RETURN 23
            BREAK
            CASE LFI_DEVIN
                  RETURN 34
            BREAK
            CASE LFI_DEMARCUS
                  RETURN 26
            BREAK
            CASE LFI_FEUD
                  RETURN 27
            BREAK
            CASE LFI_SPRUNK
                  RETURN 18
            BREAK
            CASE LFI_INKINC
                  RETURN 25
            BREAK
            CASE LFI_HERRKUTZ
                  RETURN 27
            BREAK
            CASE LFI_LSCUSTOMS
                  RETURN 24
            BREAK
			
			CASE LFI_TREVOR
				RETURN 9
            BREAK
	  		CASE LFI_RON
				RETURN 5
            BREAK
	  		CASE LFI_WADE
				RETURN 8
            BREAK
			CASE LFI_LUDENDORFF
				RETURN 9
            BREAK
      ENDSWITCH
      
      RETURN 0

ENDFUNC

// ---------------------------------------
// HANDLES INFO BAR AT THE TOP OF A PROFILE
// ---------------------------------------
// (also used as functions to get names for posted messages and stuff)

FUNC TEXT_LABEL GET_LFI_PROFILE_USERNAME(int LFI_ID)
      
      TEXT_LABEL tlReturn = "LFI_"
      
      SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
            // Michael, friends and companies
            CASE LFI_MICHAEL
                  tlReturn += "MISA"
            BREAK
            CASE LFI_TDESANTA
			       tlReturn += "TRSA"
            BREAK
			CASE LFI_JDESANTA
			       tlReturn += "JISA"
            BREAK
			CASE LFI_ADESANTA
			       tlReturn += "AMSA"
            BREAK
			CASE LFI_LESTERCREST
			       tlReturn += "LECE"
            BREAK
			CASE LFI_MARYANNQUINN
			       tlReturn += "MAQU"
            BREAK
			CASE LFI_HAYDENDUBOSE
			       tlReturn += "HADU"
            BREAK
			CASE LFI_KYLECHAVIS
			       tlReturn += "KYCH"
            BREAK
      		CASE LFI_REDWOOD
			       tlReturn += "REDW"
            BREAK
            
			// Michael non-stalking people and companies
            CASE LFI_FREDDY
			        tlReturn += "FRSL"
            BREAK
			CASE LFI_RALPH
			        tlReturn += "RADA"
            BREAK
			CASE LFI_LAUREN
			        tlReturn += "LADE"
            BREAK
			CASE LFI_FOSTER
			        tlReturn += "FOMA"
            BREAK
			CASE LFI_BREE
			        tlReturn += "BRYO"
            BREAK
			CASE LFI_KIM
			        tlReturn += "KITR"
            BREAK
			CASE LFI_LISAWALLIN
			        tlReturn += "LIWA"
            BREAK
			CASE LFI_IAN
			        tlReturn += "IAKE"
            BREAK
			CASE LFI_AHRON
			        tlReturn += "AHWA"
            BREAK
			CASE LFI_RYAN
			        tlReturn += "RYBE"
            BREAK
			CASE LFI_COLIN
			        tlReturn += "COEA"
            BREAK
			CASE LFI_EVAN
			        tlReturn += "EVAR"
            BREAK
			CASE LFI_ROSS
			        tlReturn += "ROCO"
            BREAK
			CASE LFI_JAY
			        tlReturn += "JAWA"
            BREAK
			CASE LFI_AMY
			        tlReturn += "AMTU"
            BREAK
			CASE LFI_FABIEN
			        tlReturn += "FALA"
            BREAK
			CASE LFI_LISABARCLAY
			        tlReturn += "LIBA"
            BREAK
			CASE LFI_DRSTANOVICZ
			        tlReturn += "DRST"
            BREAK
			CASE LFI_KERI
			        tlReturn += "KEBA"
            BREAK
			CASE LFI_PETER
			        tlReturn += "PEKE"
            BREAK
			CASE LFI_SANDRA
			        tlReturn += "SACA"
            BREAK
			CASE LFI_FIONA
			        tlReturn += "FILA"
            BREAK
			CASE LFI_CHERYL
			        tlReturn += "CHMA"
            BREAK
			CASE LFI_GUADALOUPE
			        tlReturn += "GURO"
            BREAK
			CASE LFI_MAURIE
			        tlReturn += "MALE"
            BREAK
			CASE LFI_KANYA
			        tlReturn += "KASU"
            BREAK
			CASE LFI_NILES
			        tlReturn += "NIHA"
            BREAK
			CASE LFI_GILES
			        tlReturn += "GIHE"
            BREAK
			CASE LFI_DRWETHERBROOK
			        tlReturn += "DRWE"
            BREAK
			CASE LFI_JONATHAN
			        tlReturn += "JOAU"
            BREAK
			CASE LFI_RICHARD
			        tlReturn += "RITR"
            BREAK
			CASE LFI_MOTHER
			        tlReturn += "MOTR"
            BREAK
			CASE LFI_MELINDA
			        tlReturn += "MEDU"
            BREAK
			CASE LFI_JASPER
			        tlReturn += "JAOH"
            BREAK
			CASE LFI_REBECCA
			        tlReturn += "REPO"
            BREAK
			CASE LFI_DENA
			        tlReturn += "DESP"
            BREAK
			CASE LFI_ALEX
			        tlReturn += "ALDE"
            BREAK
			CASE LFI_SUSAN
			        tlReturn += "SUCH"
            BREAK
			CASE LFI_ALICE
			        tlReturn += "ALTI"
            BREAK
			CASE LFI_BILLY
			        tlReturn += "BIPE"
            BREAK
			CASE LFI_LIZ
			        tlReturn += "LIMA"
            BREAK
			CASE LFI_KELLY
			        tlReturn += "KERU"
            BREAK
			CASE LFI_BRIAN
			        tlReturn += "BRMO"
            BREAK
			CASE LFI_GEMMA
			        tlReturn += "GEKI"
            BREAK
			CASE LFI_GERRY
			        tlReturn += "GEHU"
            BREAK
			CASE LFI_GARYSCALES
				tlReturn += "GASC"
			BREAK
			
			CASE LFI_FAMEORSHAME
			        tlReturn += "FASH"
            BREAK
			CASE LFI_RIGHTEOUSSLAUGHTER
			        tlReturn += "RISL"
            BREAK
			CASE LFI_GAMMI
			        tlReturn += "GAFO"
            BREAK
			CASE LFI_PEDALANDMETAL
			        tlReturn += "PECY"
            BREAK
			CASE LFI_PROLAPS
			        tlReturn += "PRLP"
            BREAK
			CASE LFI_LOSSANTOSGOLFCLUB
			        tlReturn += "LOSA"
            BREAK
			CASE LFI_FACADE
			        tlReturn += "FACA"
            BREAK
			CASE LFI_EGOCHASER
			        tlReturn += "EGCH"
            BREAK
			CASE LFI_HEAT
			        tlReturn += "HEAT"
            BREAK
			CASE LFI_PENDULUS
			        tlReturn += "PEND"
            BREAK
			CASE LFI_AMMU
				tlReturn += "AMMU"
			BREAK 

			CASE LFI_MFRIENDS432
			        tlReturn += "TRSA_CNT"
            BREAK
			CASE LFI_MFRIENDS142
			        tlReturn += "JISA_CNT"
            BREAK
			CASE LFI_MFRIENDS205
			        tlReturn += "AMSA_CNT"
            BREAK
			CASE LFI_MFRIENDS63
			        tlReturn += "LECE_CNT"
            BREAK
			CASE LFI_MFRIENDS58
			        tlReturn += "MAQU_CNT"
            BREAK
			CASE LFI_MFRIENDS86
			        tlReturn += "HADU_CNT"
            BREAK
			CASE LFI_MFRIENDS116
			        tlReturn += "KYCH_CNT"
            BREAK
			CASE LFI_MFRIENDSXX1
			        tlReturn += "REDW_CNT"
            BREAK
			CASE LFI_MFRIENDSXX2
			        tlReturn += "BUGS_CNT"
            BREAK
		ENDSWITCH 
		
		SWITCH INT_TO_ENUM(LFICharList, LFI_ID)	
            // Franklin, friends and companies
            CASE LFI_FRANKLIN
                  tlReturn += "FRCL"
            BREAK
            CASE LFI_LAMAR
                  tlReturn += "LADA"
            BREAK
            CASE LFI_SIMEON
                  tlReturn += "SIYE"
            BREAK
            CASE LFI_TONYA
                  tlReturn += "TOWI"
            BREAK
            CASE LFI_DENISE
                  tlReturn += "DECL"
            BREAK
            CASE LFI_STRETCH
                  tlReturn += "HAJO"
            BREAK
            CASE LFI_TANISHA
                  tlReturn += "TAJA"
            BREAK
            CASE LFI_TAVELL
                  tlReturn += "TACL"
            BREAK
            CASE LFI_DOM
                  tlReturn += "DOBE"
            BREAK
            CASE LFI_BEV
                  tlReturn += "BEFE"
            BREAK
            CASE LFI_DEVIN
                  tlReturn += "DEWE"
            BREAK
            CASE LFI_DEMARCUS
                  tlReturn += "DEBR"
            BREAK
            CASE LFI_FEUD
                  tlReturn += "FEUD"
            BREAK
            CASE LFI_SPRUNK
                  tlReturn += "SPK"
            BREAK
            CASE LFI_INKINC
                  tlReturn += "INK"
            BREAK
            CASE LFI_HERRKUTZ
                  tlReturn += "HEKU"
            BREAK
            CASE LFI_LSCUSTOMS
                  tlReturn += "LSCU"
            BREAK
            
            // Franklin non-stalking people and companies
            CASE LFI_ANDREB
                  tlReturn += "ANBA"
            BREAK
            CASE LFI_NICOLSONB
                  tlReturn += "NIBE"
            BREAK
            CASE LFI_JAMALR
                  tlReturn += "JARE"
            BREAK
            CASE LFI_JBB
                  tlReturn += "JBBR"
            BREAK
            CASE LFI_DARRYLK
                  tlReturn += "DAKN"
            BREAK
            CASE LFI_DANAE
                  tlReturn += "DAEL"
            BREAK
	        CASE LFI_LEONV
			       tlReturn += "LEVA"
            BREAK
	        CASE LFI_GERALDG
			       tlReturn += "GEG"
            BREAK
			CASE LFI_OWENY
			       tlReturn += "OWYA"
            BREAK
			CASE LFI_BARBARAW
			       tlReturn += "BAWA"
            BREAK
			CASE LFI_ANAKH
			       tlReturn += "ANHO"
            BREAK
			CASE LFI_YERGHATT
			       tlReturn += "YETA"
            BREAK
			CASE LFI_SACHAY
			       tlReturn += "SAYE"
            BREAK
			CASE LFI_TYSONF
			       tlReturn += "TYFI"
            BREAK
			CASE LFI_KEEARAN
			       tlReturn += "KENE"
            BREAK
			CASE LFI_SHARONDAM
			       tlReturn += "SHMI"
            BREAK
			CASE LFI_MADISONF
			       tlReturn += "MAFO"
            BREAK
			CASE LFI_COLIND
			       tlReturn += "CODA"
            BREAK
			CASE LFI_NIAB
			       tlReturn += "NIBR"
            BREAK
			CASE LFI_LAHRONDAW
			       tlReturn += "LAWH"
            BREAK
			CASE LFI_MATTIEH
			       tlReturn += "MAHO"
            BREAK
			CASE LFI_MAGENTAA
			       tlReturn += "MAAN"
            BREAK
			CASE LFI_JULIEP
			       tlReturn += "JUPO"
            BREAK
			CASE LFI_PIPPYE
			       tlReturn += "PIEG"
            BREAK
			CASE LFI_RENATTAS
			       tlReturn += "RESI"
            BREAK
			CASE LFI_LELAY
			       tlReturn += "LEYA"
            BREAK
			CASE LFI_CATRINAW
			       tlReturn += "CAWE"
            BREAK
			CASE LFI_MONETTEA
			       tlReturn += "MOAL"
            BREAK
			CASE LFI_GARRYH
			       tlReturn += "GAHI"
            BREAK
			CASE LFI_JEFFC
			       tlReturn += "JECH"
            BREAK
			CASE LFI_KARENL
			       tlReturn += "KALI"
            BREAK
			CASE LFI_MARKP
			       tlReturn += "MAPE"
            BREAK
			CASE LFI_ALANF
			       tlReturn += "ALFE"
            BREAK
			CASE LFI_RAYG
			       tlReturn += "RAGO"
            BREAK
			CASE LFI_RAYN
			       tlReturn += "RANA"
            BREAK
			CASE LFI_SIMONH
			       tlReturn += "SIHA"
            BREAK
			CASE LFI_MOLLYS
			       tlReturn += "MOSC"
            BREAK
			CASE LFI_DEJAM
			       tlReturn += "DEME"
            BREAK
			CASE LFI_HAILEYB
			       tlReturn += "HABR"
            BREAK
			CASE LFI_ERIKD
			       tlReturn += "ERDU"
            BREAK
			CASE LFI_GRAHAMR
			       tlReturn += "GRRA"
            BREAK
			CASE LFI_NELSONW
			       tlReturn += "NEWE"
            BREAK
			CASE LFI_MODFEUD
			       tlReturn += "MOFE"
            BREAK
			CASE LFI_JAXS
			       tlReturn += "JASA"
            BREAK
			CASE LFI_ADRICH
			       tlReturn += "ADHO"
            BREAK
			CASE LFI_KARLR
			       tlReturn += "KARA"
            BREAK
			CASE LFI_TAYEB
			       tlReturn += "TABU"
            BREAK
			CASE LFI_TRENTONM
			       tlReturn += "TRMA"
            BREAK
			CASE LFI_LONNYG
			       tlReturn += "LOGI"
            BREAK
			CASE LFI_REGISW
			       tlReturn += "REWE"
            BREAK
			CASE LFI_RESHAYM
			       tlReturn += "REMI"
            BREAK
			CASE LFI_ANTOINEP
			       tlReturn += "ANPO"
            BREAK
			CASE LFI_BENTONC
			       tlReturn += "BECO"
            BREAK
			CASE LFI_STERLINL
			       tlReturn += "STLA"
            BREAK
			CASE LFI_SARAP
			       tlReturn += "SAPR"
            BREAK
			CASE LFI_DARNELLS
			       tlReturn += "DAST"
            BREAK
			CASE LFI_PAULO
			       tlReturn += "PAOH"
            BREAK
			CASE LFI_KATEM
			       tlReturn += "KAMO"
            BREAK
			CASE LFI_JOSHUAW
			       tlReturn += "JOWA"
            BREAK
			CASE LFI_VINCEH
			       tlReturn += "VIHA"
            BREAK
			CASE LFI_TODDR
			       tlReturn += "TORA"
            BREAK
			CASE LFI_STEVEW
			       tlReturn += "STWE"
            BREAK
			CASE LFI_MIKEH
			       tlReturn += "MIHA"
            BREAK
			CASE LFI_HARVEYP
			       tlReturn += "HAPR"
            BREAK
			CASE LFI_ANDYW
			       tlReturn += "ANWH"
            BREAK
			CASE LFI_TAYLOTH
			       tlReturn += "TAHO"
            BREAK
			CASE LFI_DONNAHK
			       tlReturn += "DOHE"
            BREAK
			CASE LFI_LANCEW
			       tlReturn += "LCWH"
            BREAK
			CASE LFI_PHILG
			       tlReturn += "PHSC"
            BREAK
			CASE LFI_JAMIEE
			       tlReturn += "JAEL"
            BREAK
			CASE LFI_JONH
			       tlReturn += "JOHE"
            BREAK
			CASE LFI_GERRYC
			       tlReturn += "GECO"
            BREAK
			CASE LFI_AARONF
			       tlReturn += "AAFI"
            BREAK
			CASE LFI_CONNORS
			       tlReturn += "COSH"
            BREAK
			CASE LFI_POPPAL
			       tlReturn += "POLE"
            BREAK
			CASE LFI_HAILEYW
			       tlReturn += "HAWA"
            BREAK
			CASE LFI_SAMW
			       tlReturn += "FN_NM_SAWA"
            BREAK
			CASE LFI_GARYH
			       tlReturn += "GAHA"
            BREAK
			CASE LFI_HANKS
			       tlReturn += "HASI"
            BREAK

            CASE LFI_PRDE
                  tlReturn += "PRDE"
            BREAK
            CASE LFI_GRTR
                  tlReturn += "GRTR"
            BREAK
            CASE LFI_VANG
                  tlReturn += "VANG"
            BREAK
            CASE LFI_STST
                  tlReturn += "STST"
            BREAK
            CASE LFI_BWSQ
                  tlReturn += "BWSQ"
            BREAK
            CASE LFI_CCCK
                  tlReturn += "CCCK"
            BREAK
            CASE LFI_ALCO
                  tlReturn += "ALCO"
            BREAK
            CASE LFI_MXRE
                  tlReturn += "MXRE"
            BREAK
            CASE LFI_STCE
                  tlReturn += "STCE"
            BREAK
			
			CASE LFI_FFRIENDS93
                  tlReturn += "LADA_CNT"
            BREAK
			CASE LFI_FFRIENDS77
                  tlReturn += "SIYE_CNT"
            BREAK
			CASE LFI_FFRIENDS42
                  tlReturn += "TOWI_CNT"
            BREAK
			CASE LFI_FFRIENDS118
                  tlReturn += "DECL_CNT"
            BREAK
			CASE LFI_FFRIENDS72
                  tlReturn += "HAJO_CNT"
            BREAK
			CASE LFI_FFRIENDS226
                  tlReturn += "TAJA_CNT"
            BREAK
			CASE LFI_FFRIENDS175
                  tlReturn += "TACL_CNT"
            BREAK
			CASE LFI_FFRIENDS27
                  tlReturn += "DOBE_CNT"
            BREAK
			CASE LFI_FFRIENDS64
                  tlReturn += "BEFE_CNT"
            BREAK
			CASE LFI_FFRIENDS1644
                  tlReturn += "DEWE_CNT"
            BREAK
			CASE LFI_FFRIENDS118B
                  tlReturn += "DEBR_CNT"
            BREAK
			CASE LFI_FFRIENDSXX1
                  tlReturn += "FEUD_CNT"
            BREAK
			CASE LFI_FFRIENDSXX2
                  tlReturn += "SPK_CNT"
            BREAK
			CASE LFI_FFRIENDSXX3
                  tlReturn += "INK_CNT"
            BREAK
			CASE LFI_FFRIENDSXX4
                  tlReturn += "HEKU_CNT"
            BREAK
			CASE LFI_FFRIENDSXX5
                  tlReturn += "LSCU_CNT"
            BREAK
		ENDSWITCH
		
		SWITCH INT_TO_ENUM(LFICharList, LFI_ID)	
            // Trevor, friends and companies
            CASE LFI_TREVOR
                  tlReturn += "TRPH"
            BREAK
            CASE LFI_RON
				  tlReturn += "ROJA"
			BREAK
	  		CASE LFI_WADE
				tlReturn += "WAHE"
			BREAK
      
      		// Trevor's stalking brands
      		CASE LFI_LUDENDORFF
				tlReturn += "LUDE"
			BREAK
			
			// Trevor non-stalking people and companies
			CASE LFI_ASHLEY
				tlReturn += "ASBU"
			BREAK
			CASE LFI_CLETUS
				tlReturn += "CLEW"
			BREAK
			CASE LFI_JENNY
				tlReturn += "JETI"
			BREAK
			CASE LFI_FLOYD
				tlReturn += "FLHE"
			BREAK
			CASE LFI_HANK
				tlReturn += "HATE"
			BREAK
			
			CASE LFI_FATALINCURSIONS
				tlReturn += "FAIN"
			BREAK
			
	  		CASE LFI_BCR
				tlReturn += "BCRC"
			BREAK
			
			CASE LFI_TFRIENDS10
				tlReturn += "ROJA_CNT"
			BREAK
			CASE LFI_TFRIENDS27
				tlReturn += "WAHE_CNT"
			BREAK
			CASE LFI_TFRIENDSXX
				tlReturn += "LUDE_CNT"
			BREAK

            // Specials, i.e. "no family", won't be visitable profiles but still need to return a string
            CASE LFI_NOFAM
                  tlReturn += "NOFM"
            BREAK
      ENDSWITCH
	  
	  IF ARE_STRINGS_EQUAL(tlReturn, "LFI_")
	  	CPRINTLN(DEBUG_INTERNET, "Lifeinvader - unknown char name string!")
	  	tlReturn += "UNKNOWN"
	  ENDIF
      
	  // Comment back in for debug
      //CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for username...")
      
      RETURN tlReturn

ENDFUNC

FUNC TEXT_LABEL GET_LFI_PROFILE_PORTRAIT(INT LFI_ID)

      TEXT_LABEL tlReturn = "LARGE_"

      SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
            CASE LFI_MICHAEL
                  tlReturn += "22"
            BREAK
            CASE LFI_FRANKLIN
                  tlReturn += "1"
            BREAK
            CASE LFI_LAMAR
                  tlReturn += "2"
            BREAK
            CASE LFI_SIMEON
                  tlReturn += "3"
            BREAK
            CASE LFI_TONYA
                  tlReturn += "4"
            BREAK
            CASE LFI_DENISE
                  tlReturn += "5"
            BREAK
            CASE LFI_STRETCH
                  tlReturn += "6"
            BREAK
            CASE LFI_TANISHA
                  tlReturn += "7"
            BREAK
            CASE LFI_TAVELL
                  tlReturn += "8"
            BREAK
            CASE LFI_DOM
                  tlReturn += "9"
            BREAK
            CASE LFI_BEV
                  tlReturn += "10"
            BREAK
            CASE LFI_DEVIN
                  tlReturn += "11"
            BREAK
            CASE LFI_DEMARCUS
                  tlReturn += "12"
            BREAK
            CASE LFI_FEUD
                  tlReturn += "13"
            BREAK
            CASE LFI_SPRUNK
                  tlReturn += "14"
            BREAK
            CASE LFI_INKINC
                  tlReturn += "15"
            BREAK
            CASE LFI_HERRKUTZ
                  tlReturn += "16"
            BREAK
            CASE LFI_LSCUSTOMS
                  tlReturn += "17"
            BREAK
            CASE LFI_TREVOR
                  tlReturn += "18"
            BREAK
			
			CASE LFI_RON
                  tlReturn += "19"
            BREAK
			CASE LFI_WADE
                  tlReturn += "20"
            BREAK
			CASE LFI_LUDENDORFF
                  tlReturn += "21"
            BREAK
			
			CASE LFI_TDESANTA
                  tlReturn += "23"
            BREAK
			CASE LFI_JDESANTA
                  tlReturn += "24"
            BREAK
			CASE LFI_ADESANTA
                  tlReturn += "25"
            BREAK
			CASE LFI_LESTERCREST
                  tlReturn += "26"
            BREAK
			CASE LFI_MARYANNQUINN
                  tlReturn += "27"
            BREAK
			CASE LFI_HAYDENDUBOSE
                  tlReturn += "28"
            BREAK
			CASE LFI_KYLECHAVIS
                  tlReturn += "29"
            BREAK
			CASE LFI_REDWOOD
                  tlReturn += "30"
            BREAK
			
            DEFAULT
                  tlReturn = "BLANK"
            BREAK
      ENDSWITCH
      
	  // Comment back in for debug
     // CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for profile pic...")
      
      RETURN tlReturn

ENDFUNC


FUNC TEXT_LABEL GET_LFI_PROFILE_OCCUPATION(INT LFI_ID)

	TEXT_LABEL tlReturn = ""

	IF IS_LFI_CHAR_A_BRAND(INT_TO_ENUM(LFICharList,LFI_ID))
		// These are Stalk pages so don't need an occupation
		// Comment back in for debug
		//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning blank for occupation because this is a brand page...")
		RETURN tlReturn // Should return nothing
	ENDIF
	
	IF NOT DOES_CHAR_HAVE_FULL_LFI_PAGE(INT_TO_ENUM(LFICharList,LFI_ID))
		// Comment back in for debug
		//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning blank for occupation because this is not a full profile...")
		RETURN tlReturn // Should return nothing
	ENDIF
	
	tlReturn = GET_LFI_PROFILE_USERNAME(LFI_ID)
	
	tlReturn += "_OC"
	
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for occupation...")
	
	RETURN tlReturn

ENDFUNC

FUNC TEXT_LABEL GET_LFI_PROFILE_RELATIONSHIP(INT LFI_ID)
	
	TEXT_LABEL tlReturn = ""

	IF IS_LFI_CHAR_A_BRAND(INT_TO_ENUM(LFICharList,LFI_ID))
		// These are Stalk pages so don't need an occupation
		// Comment back in for debug
		//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning blank for relationship because this is a brand page...")
		RETURN tlReturn // Should return nothing
	ENDIF
	
	IF NOT DOES_CHAR_HAVE_FULL_LFI_PAGE(INT_TO_ENUM(LFICharList,LFI_ID))
		// Comment back in for debug
		//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning blank for relationship because this is not a full profile...")
		RETURN tlReturn // Should return nothing
	ENDIF
	
	tlReturn = GET_LFI_PROFILE_USERNAME(LFI_ID)
	
	tlReturn += "_RP"
	
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for relationship...")
	
	RETURN tlReturn

ENDFUNC

FUNC TEXT_LABEL GET_LFI_PROFILE_PHOTO_PIC(INT LFI_ID)

      TEXT_LABEL tlReturn = "MED_"

      SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
            CASE LFI_MICHAEL
                  tlReturn += "33"
            BREAK
            CASE LFI_TDESANTA
                  tlReturn += "40"
            BREAK
            CASE LFI_JDESANTA
                  tlReturn += "33"
            BREAK
            CASE LFI_ADESANTA
                  tlReturn += "33"
            BREAK
            CASE LFI_LESTERCREST
                  tlReturn += "34"
            BREAK
            CASE LFI_MARYANNQUINN
                  tlReturn += "35"
            BREAK
            CASE LFI_HAYDENDUBOSE
                  tlReturn += "36"
            BREAK
            CASE LFI_KYLECHAVIS
                  tlReturn += "37"
            BREAK
            CASE LFI_REDWOOD
                  tlReturn += "38"
            BREAK
            CASE LFI_FRANKLIN
                  tlReturn += "6"
            BREAK
            CASE LFI_LAMAR
                  tlReturn += "6"
            BREAK
            CASE LFI_SIMEON
                  tlReturn += "8"
            BREAK
            CASE LFI_TONYA
                  tlReturn += "7"
            BREAK
            CASE LFI_DENISE
                  tlReturn += "9"
            BREAK
            CASE LFI_STRETCH
                  tlReturn += "6"
            BREAK
            CASE LFI_TANISHA
                  tlReturn += "7"
            BREAK
            CASE LFI_TAVELL
                  tlReturn += "10"
            BREAK
            CASE LFI_DOM
                  tlReturn += "11"
            BREAK
            CASE LFI_BEV
                  tlReturn += "12"
            BREAK
            CASE LFI_DEVIN
                  tlReturn += "13"
            BREAK
            CASE LFI_DEMARCUS
                  tlReturn += "14"
            BREAK
            CASE LFI_FEUD
                  tlReturn += "6"
            BREAK
            CASE LFI_SPRUNK
                  tlReturn += "15"
            BREAK
            CASE LFI_INKINC
                  tlReturn += "16"
            BREAK
            CASE LFI_HERRKUTZ
                  tlReturn += "17"
            BREAK
            CASE LFI_LSCUSTOMS
                  tlReturn += "18"
            BREAK
            CASE LFI_TREVOR
                  tlReturn += "30"
            BREAK
            CASE LFI_RON
                  tlReturn += "31"
            BREAK
            CASE LFI_WADE
                  tlReturn += "32"
            BREAK
            CASE LFI_LUDENDORFF
                  tlReturn += "32"
            BREAK
            DEFAULT
                  tlReturn = "BLANK"
            BREAK
      ENDSWITCH
      
	  // Comment back in for debug
      //CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for profile pic...")
      
      RETURN tlReturn

ENDFUNC


FUNC TEXT_LABEL GET_LFI_PROFILE_FRIENDS_PIC(INT LFI_ID)

	TEXT_LABEL tlReturn = "MED_"

	SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
		CASE LFI_MICHAEL
			tlReturn += "61"
		BREAK
		CASE LFI_TDESANTA
              tlReturn += "59"
        BREAK
        CASE LFI_JDESANTA
              tlReturn += "58"
        BREAK
        CASE LFI_ADESANTA
              tlReturn += "61"
        BREAK
        CASE LFI_LESTERCREST
              tlReturn += "57"
        BREAK
        CASE LFI_MARYANNQUINN
              tlReturn += "57"
        BREAK
        CASE LFI_HAYDENDUBOSE
              tlReturn += "57"
        BREAK
        CASE LFI_KYLECHAVIS
              tlReturn += "59"
        BREAK
        CASE LFI_REDWOOD
              tlReturn += "57"
        BREAK
		CASE LFI_FRANKLIN
			tlReturn += "43"
		BREAK
		CASE LFI_LAMAR
			tlReturn += "43"
		BREAK
		CASE LFI_SIMEON
			tlReturn += "42"
		BREAK
		CASE LFI_TONYA
			tlReturn += "45"
		BREAK
		CASE LFI_DENISE
			tlReturn += "41"
		BREAK
		CASE LFI_STRETCH
			tlReturn += "46"
		BREAK
		CASE LFI_TANISHA
			tlReturn += "42"
		BREAK
		CASE LFI_TAVELL
			tlReturn += "44"
		BREAK
		CASE LFI_DOM
			tlReturn += "41"
		BREAK
		CASE LFI_BEV
			tlReturn += "41"
		BREAK
		CASE LFI_DEVIN
			tlReturn += "41"
		BREAK
		CASE LFI_DEMARCUS
			tlReturn += "41"
		BREAK
		CASE LFI_FEUD
			tlReturn += "42"
		BREAK
		CASE LFI_SPRUNK
			tlReturn += "47"
		BREAK
		CASE LFI_INKINC
			tlReturn += "43"
		BREAK
		CASE LFI_HERRKUTZ
			tlReturn += "41"
		BREAK
		CASE LFI_LSCUSTOMS
			tlReturn += "42"
		BREAK
		CASE LFI_TREVOR
			tlReturn += "54"
		BREAK
	 	CASE LFI_RON
              tlReturn += "55"
        BREAK
        CASE LFI_WADE
              tlReturn += "53"
        BREAK
        CASE LFI_LUDENDORFF
              tlReturn += "53"
        BREAK
		DEFAULT
			tlReturn = "BLANK"
		BREAK
	ENDSWITCH
	
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for profile pic...")
	
	RETURN tlReturn

ENDFUNC

FUNC TEXT_LABEL GET_LFI_PROFILE_STALKING_PIC(INT LFI_ID)

	TEXT_LABEL tlReturn = "MED_"

	SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
		CASE LFI_MICHAEL
			tlReturn += "63"
		BREAK
		CASE LFI_TDESANTA
			tlReturn += "23"
        BREAK
        CASE LFI_JDESANTA
			tlReturn += "24"
        BREAK
        CASE LFI_ADESANTA
			tlReturn += "25"
        BREAK
        CASE LFI_LESTERCREST
              tlReturn += "5"
        BREAK
        CASE LFI_MARYANNQUINN
              tlReturn += "27"
        BREAK
        CASE LFI_HAYDENDUBOSE
              tlReturn += "5"
        BREAK
        CASE LFI_KYLECHAVIS
              tlReturn += "28"
        BREAK
        CASE LFI_REDWOOD
              tlReturn += "22"
        BREAK
		CASE LFI_FRANKLIN
			tlReturn += "48"
		BREAK
		CASE LFI_LAMAR
			tlReturn += "52"
		BREAK
		CASE LFI_SIMEON
			tlReturn += "1"
		BREAK
		CASE LFI_TONYA
			tlReturn += "50"
		BREAK
		CASE LFI_DENISE
			tlReturn += "2"
		BREAK
		CASE LFI_STRETCH
			tlReturn += "50"
		BREAK
		CASE LFI_TANISHA
			tlReturn += "3"
		BREAK
		CASE LFI_TAVELL
			tlReturn += "48"
		BREAK
		CASE LFI_DOM
			tlReturn += "49"
		BREAK
		CASE LFI_BEV
			tlReturn += "4"
		BREAK
		CASE LFI_DEVIN
			tlReturn += "5"
		BREAK
		CASE LFI_DEMARCUS
			tlReturn += "50"
		BREAK
		CASE LFI_FEUD
			tlReturn += "49"
		BREAK
		CASE LFI_SPRUNK
			tlReturn += "5"
		BREAK
		CASE LFI_INKINC
			tlReturn += "51"
		BREAK
		CASE LFI_HERRKUTZ
			tlReturn += "50"
		BREAK
		CASE LFI_LSCUSTOMS
			tlReturn += "48"
		BREAK
		CASE LFI_TREVOR
			tlReturn += "56"
		BREAK
		CASE LFI_RON
              tlReturn += "20"
        BREAK
        CASE LFI_WADE
              tlReturn += "21"
        BREAK
        CASE LFI_LUDENDORFF
              tlReturn += "22"
        BREAK
		DEFAULT
			tlReturn = "BLANK"
		BREAK
	ENDSWITCH
	
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for profile pic...")
	
	RETURN tlReturn

ENDFUNC

PROC SET_LFI_PROFILE(SCALEFORM_INDEX pagemov)

	// Must create new text labels and pass those into Scaleform functions...
	// You can't directly put TEXT_LABEL returning functions into the Scaleform functions, the compiler doesn't like it
	text_label tlName = GET_LFI_PROFILE_USERNAME(iCurrentLFID)
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlName , " for character profile name...")
	text_label tlPortrait = GET_LFI_PROFILE_PORTRAIT(iCurrentLFID)
	text_label tlOcc = GET_LFI_PROFILE_OCCUPATION(iCurrentLFID)
	text_label tlRel = GET_LFI_PROFILE_RELATIONSHIP(iCurrentLFID)
	text_label tlPhotos = GET_LFI_PROFILE_PHOTO_PIC(iCurrentLFID)
	text_label tlFriends = GET_LFI_PROFILE_FRIENDS_PIC(iCurrentLFID)
	text_label tlStalk = GET_LFI_PROFILE_STALKING_PIC(iCurrentLFID)
	
	BOOL bCanClickBackToMyProfile
	IF iCurrentLFID >= 3
		bCanClickBackToMyProfile = TRUE // Not on Michael, Franklin or Trevor's page, so enable this button
	ELSE
		IF LFIWall_CurrentDisplay = LFIWall_Messages
			bCanClickBackToMyProfile = FALSE // Already on 'my' profile, so disable
		ELSE
			bCanClickBackToMyProfile = TRUE // Player can click this to go back to the main profile page with messages
		ENDIF
	ENDIF
	
	// Only let the player see the friends list of characters that have full LFI profiles
	BOOL bCanClickFriends = FALSE
	IF DOES_CHAR_HAVE_FULL_LFI_PAGE(INT_TO_ENUM(LFICharList, iCurrentLFID))
		bCanClickFriends = TRUE
	ENDIF
	
	// Only let the player see the stalking list of characters that have full LFI profiles
	BOOL bCanClickStalking = FALSE
	IF DOES_CHAR_HAVE_FULL_LFI_PAGE(INT_TO_ENUM(LFICharList, iCurrentLFID))
		bCanClickStalking = TRUE
	ENDIF
	
	// Special case for Dom - because you can get to his page from Sprunk before Extreme 1 is complete
	// Check if Extreme 1 is done - if not, change all of this stuff to an inaccessible profile
	IF iCurrentLFID = ENUM_TO_INT(LFI_DOM)
		IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_EXTREME_1), CP_GROUP_RANDOMCHARS)
			CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Trying to get to Dom's profile before Extreme 1 is complete! Blanking...")
			tlPortrait = "BLANK"
			tlOcc = ""
			tlRel = ""
			tlPhotos = "BLANK"
			tlFriends = "BLANK"
			tlStalk = "BLANK"
			bCanClickFriends = FALSE
			bCanClickStalking = FALSE
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) //Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPortrait) // Portrait txd
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlOcc) // Occupation
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlRel) //relationship
		//These are player name on purpose because _string checks the string table, can't find it and breaks it...
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPhotos) // photos image
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlFriends) // friends image
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlStalk) // stalking image
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanClickBackToMyProfile) // Can Back To My Profile be clicked
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE) // Can Photos be clicked - NEVER!
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanClickFriends) // Can Friends be clicked
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanClickStalking) // Can Stalking be clicked
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

// ---------------------------------------
// HANDLES TITLE FOR A PROFILE
// ---------------------------------------


/// PURPOSE:
///    Sets a "title" above the posts on a wall, be they messages, the friends list or stalking list
/// PARAMS:
///    pagemov - 
PROC SET_WALL_TITLE(SCALEFORM_INDEX pagemov)

	TEXT_LABEL tlTitle
	
	IF DOES_CHAR_HAVE_FULL_LFI_PAGE(INT_TO_ENUM(LFICharList, iCurrentLFID))
		
		SWITCH LFIWall_CurrentDisplay
			CASE LFIWall_Messages
				tlTitle = "LFI_MSGHEADER"
			BREAK
			CASE LFIWall_Friends
				tlTitle = "LFI_FRIENDHEAD"
			BREAK
			CASE LFIWall_Stalking
				tlTitle = "LFI_STLKHEADER"
			BREAK
		ENDSWITCH
		
		// Special case for Dom - because you can get to his page from Sprunk before Extreme 1 is complete
		// Check if Extreme 1 is done - if not, change all of this stuff to an inaccessible profile
		IF iCurrentLFID = ENUM_TO_INT(LFI_DOM)
			IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_EXTREME_1), CP_GROUP_RANDOMCHARS)
				tlTitle = "LFI_PRIV1"
			ENDIF
		ENDIF
		
	ELSE
		IF IS_LFI_CHAR_A_BRAND(INT_TO_ENUM(LFICharList, iCurrentLFID))
			tlTitle = "LFI_NOT_STLK" // Brands use "You are not stalking this page."
		ELSE
			tlTitle = "LFI_PRIV1" // Chars use "The profile is private."
		ENDIF
		
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlTitle) // Title
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

// ---------------------------------------
// HANDLES MESSAGES PAGE FOR A PROFILE
// ---------------------------------------

/// PURPOSE:
///    Gets what character wrote what message when given a char whose page we're browsing 
FUNC LFICharList GET_CHARACTER_FOR_MESSAGE(INT LFI_ID, INT messageID)

	SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
		// Michael & friends
		CASE LFI_MICHAEL
			SWITCH messageID
				CASE 1
					RETURN LFI_LESTERCREST
				BREAK
				CASE 2
					RETURN LFI_LESTERCREST
				BREAK
				CASE 3
					RETURN LFI_ADESANTA
				BREAK
				CASE 4
					RETURN LFI_TDESANTA
				BREAK
				CASE 5
					RETURN LFI_JDESANTA
				BREAK
				CASE 6
					RETURN LFI_ADESANTA
				BREAK
				CASE 7
					RETURN LFI_TDESANTA
				BREAK
				CASE 8
					RETURN LFI_JDESANTA
				BREAK
				CASE 9
					RETURN LFI_ADESANTA
				BREAK
				CASE 10
					RETURN LFI_TDESANTA
				BREAK
				CASE 11
					RETURN LFI_JDESANTA
				BREAK
				CASE 12
					RETURN LFI_ADESANTA
				BREAK
				CASE 13
					RETURN LFI_TDESANTA
				BREAK
				CASE 14
					RETURN LFI_JDESANTA
				BREAK
				CASE 15
					RETURN LFI_TDESANTA
				BREAK
				CASE 16
					RETURN LFI_ADESANTA
				BREAK
				CASE 17
					RETURN LFI_TDESANTA
				BREAK
				CASE 18
					RETURN LFI_LESTERCREST
				BREAK
				CASE 19
					RETURN LFI_TDESANTA
				BREAK
				CASE 20
					RETURN LFI_JDESANTA
				BREAK
				CASE 21
					RETURN LFI_ADESANTA
				BREAK
				CASE 22
					RETURN LFI_LESTERCREST
				BREAK
				CASE 23
					RETURN LFI_LESTERCREST
				BREAK
				CASE 24
					RETURN LFI_TDESANTA
				BREAK
				CASE 25
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 26
					RETURN LFI_LESTERCREST
				BREAK
				CASE 27
					RETURN LFI_LESTERCREST
				BREAK
				CASE 28
					RETURN LFI_LESTERCREST
				BREAK
				CASE 29
					RETURN LFI_JDESANTA
				BREAK
				CASE 30
					RETURN LFI_LESTERCREST
				BREAK
				CASE 31
					RETURN LFI_ADESANTA
				BREAK
				CASE 32
					RETURN LFI_TDESANTA
				BREAK
				CASE 33
					RETURN LFI_TDESANTA
				BREAK
				CASE 34
					RETURN LFI_LESTERCREST
				BREAK
				CASE 35
					RETURN LFI_ADESANTA
				BREAK
				CASE 36
					RETURN LFI_LESTERCREST
				BREAK
				CASE 37
					RETURN LFI_LESTERCREST
				BREAK
				CASE 38
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 39
					RETURN LFI_JDESANTA
				BREAK
				CASE 40
					RETURN LFI_TDESANTA
				BREAK
				CASE 41
					RETURN LFI_JDESANTA
				BREAK
				CASE 42
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 43
					RETURN LFI_ADESANTA
				BREAK
				CASE 44
					RETURN LFI_JDESANTA
				BREAK
				CASE 45
					RETURN LFI_JDESANTA
				BREAK
				CASE 46
					RETURN LFI_JDESANTA
				BREAK
				CASE 47
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 48
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 49
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 50
					RETURN LFI_JDESANTA
				BREAK
				CASE 51
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 52
					RETURN LFI_TDESANTA
				BREAK
				CASE 53
					RETURN LFI_TDESANTA
				BREAK
				CASE 54
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 55
					RETURN LFI_ADESANTA
				BREAK
				CASE 56
					RETURN LFI_JDESANTA
				BREAK
				CASE 57
					RETURN LFI_ADESANTA
				BREAK
				CASE 58
					RETURN LFI_JDESANTA
				BREAK
				CASE 59
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 60
					RETURN LFI_ADESANTA
				BREAK
				CASE 61
					RETURN LFI_JDESANTA
				BREAK
				CASE 62
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 63
					RETURN LFI_ADESANTA
				BREAK
				CASE 64
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 65
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 66
					RETURN LFI_TDESANTA
				BREAK
				CASE 67
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 68
					RETURN LFI_ADESANTA
				BREAK
				CASE 69
					RETURN LFI_REDWOOD
				BREAK
				CASE 70
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 71
					RETURN LFI_ADESANTA
				BREAK
				CASE 72
					RETURN LFI_TDESANTA
				BREAK
				CASE 73
					RETURN LFI_JDESANTA
				BREAK
				CASE 74
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 75
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 76
					RETURN LFI_ADESANTA
				BREAK
				CASE 77
					RETURN LFI_JDESANTA
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_TDESANTA
			SWITCH messageID
				CASE 1
					RETURN LFI_TDESANTA
				BREAK
				CASE 2
					RETURN LFI_TDESANTA
				BREAK
				CASE 3
					RETURN LFI_JDESANTA
				BREAK
				CASE 4
					RETURN LFI_TDESANTA
				BREAK
				CASE 5
					RETURN LFI_TDESANTA
				BREAK
				CASE 6
					RETURN LFI_TDESANTA
				BREAK
				CASE 7
					RETURN LFI_TDESANTA
				BREAK
				CASE 8
					RETURN LFI_TDESANTA
				BREAK
				CASE 9
					RETURN LFI_TDESANTA
				BREAK
				CASE 10
					RETURN LFI_TDESANTA
				BREAK
				CASE 11
					RETURN LFI_TDESANTA
				BREAK
				CASE 12
					RETURN LFI_TDESANTA
				BREAK
				CASE 13
					RETURN LFI_TDESANTA
				BREAK
				CASE 14
					RETURN LFI_TDESANTA
				BREAK
				CASE 15
					RETURN LFI_TDESANTA
				BREAK
				CASE 16
					RETURN LFI_TDESANTA
				BREAK
				CASE 17
					RETURN LFI_TDESANTA
				BREAK
				CASE 18
					RETURN LFI_TDESANTA
				BREAK
				CASE 19
					RETURN LFI_JDESANTA
				BREAK
				CASE 20
					RETURN LFI_FREDDY
				BREAK
				CASE 21
					RETURN LFI_TDESANTA
				BREAK
				CASE 22
					RETURN LFI_TDESANTA
				BREAK
				CASE 23
					RETURN LFI_TDESANTA
				BREAK
				CASE 24
					RETURN LFI_TDESANTA
				BREAK
				CASE 25
					RETURN LFI_JDESANTA
				BREAK
				CASE 26
					RETURN LFI_TDESANTA
				BREAK
				CASE 27
					RETURN LFI_TDESANTA
				BREAK
				CASE 28
					RETURN LFI_TDESANTA
				BREAK
				CASE 29
					RETURN LFI_ADESANTA
				BREAK
				CASE 30
					RETURN LFI_TDESANTA
				BREAK
				CASE 31
					RETURN LFI_TDESANTA
				BREAK
				CASE 32
					RETURN LFI_GARYSCALES
				BREAK
				CASE 33
					RETURN LFI_TDESANTA
				BREAK
				CASE 34
					RETURN LFI_TDESANTA
				BREAK
				CASE 35
					RETURN LFI_RALPH
				BREAK
				CASE 36
					RETURN LFI_TDESANTA
				BREAK
				CASE 37
					RETURN LFI_TDESANTA
				BREAK
				CASE 38
					RETURN LFI_JDESANTA
				BREAK
				CASE 39
					RETURN LFI_TDESANTA
				BREAK
				CASE 40
					RETURN LFI_LAUREN
				BREAK
				CASE 41
					RETURN LFI_TDESANTA
				BREAK
				CASE 42
					RETURN LFI_FOSTER
				BREAK
				CASE 43
					RETURN LFI_TDESANTA
				BREAK
				CASE 44
					RETURN LFI_TDESANTA
				BREAK
				CASE 45
					RETURN LFI_BREE
				BREAK
				CASE 46
					RETURN LFI_TDESANTA
				BREAK
				CASE 47
					RETURN LFI_TDESANTA
				BREAK
				CASE 48
					RETURN LFI_TDESANTA
				BREAK
				CASE 49
					RETURN LFI_TDESANTA
				BREAK
				CASE 50
					RETURN LFI_KIM
				BREAK
				CASE 51
					RETURN LFI_TDESANTA
				BREAK
				CASE 52
					RETURN LFI_KIM
				BREAK
				CASE 53
					RETURN LFI_TDESANTA
				BREAK
				CASE 54
					RETURN LFI_TDESANTA
				BREAK
				CASE 55
					RETURN LFI_TDESANTA
				BREAK
				CASE 56
					RETURN LFI_LISAWALLIN
				BREAK
				CASE 57
					RETURN LFI_TDESANTA
				BREAK
				CASE 58
					RETURN LFI_BREE
				BREAK
				CASE 59
					RETURN LFI_TDESANTA
				BREAK
				CASE 60
					RETURN LFI_TDESANTA
				BREAK
				CASE 61
					RETURN LFI_TDESANTA
				BREAK
				CASE 62
					RETURN LFI_TDESANTA
				BREAK
				CASE 63
					RETURN LFI_JDESANTA
				BREAK
				CASE 64
					RETURN LFI_IAN
				BREAK
				CASE 65
					RETURN LFI_TDESANTA
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_JDESANTA
			SWITCH messageID
				CASE 1
					RETURN LFI_JDESANTA
				BREAK
				CASE 2
					RETURN LFI_JDESANTA
				BREAK
				CASE 3
					RETURN LFI_JDESANTA
				BREAK
				CASE 4
					RETURN LFI_TDESANTA
				BREAK
				CASE 5
					RETURN LFI_JDESANTA
				BREAK
				CASE 6
					RETURN LFI_JDESANTA
				BREAK
				CASE 7
					RETURN LFI_JDESANTA
				BREAK
				CASE 8
					RETURN LFI_JDESANTA
				BREAK
				CASE 9
					RETURN LFI_JDESANTA
				BREAK
				CASE 10
					RETURN LFI_JDESANTA
				BREAK
				CASE 11
					RETURN LFI_JDESANTA
				BREAK
				CASE 12
					RETURN LFI_JDESANTA
				BREAK
				CASE 13
					RETURN LFI_JDESANTA
				BREAK
				CASE 14
					RETURN LFI_JDESANTA
				BREAK
				CASE 15
					RETURN LFI_AHRON
				BREAK
				CASE 16
					RETURN LFI_JDESANTA
				BREAK
				CASE 17
					RETURN LFI_AHRON
				BREAK
				CASE 18
					RETURN LFI_JDESANTA
				BREAK
				CASE 19
					RETURN LFI_RYAN
				BREAK
				CASE 20
					RETURN LFI_TDESANTA
				BREAK
				CASE 21
					RETURN LFI_TDESANTA
				BREAK
				CASE 22
					RETURN LFI_JDESANTA
				BREAK
				CASE 23
					RETURN LFI_TDESANTA
				BREAK
				CASE 24
					RETURN LFI_JDESANTA
				BREAK
				CASE 25
					RETURN LFI_JDESANTA
				BREAK
				CASE 26
					RETURN LFI_JDESANTA
				BREAK
				CASE 27
					RETURN LFI_JDESANTA
				BREAK
				CASE 28
					RETURN LFI_JDESANTA
				BREAK
				CASE 29
					RETURN LFI_TDESANTA
				BREAK
				CASE 30
					RETURN LFI_JDESANTA
				BREAK
				CASE 31
					RETURN LFI_JDESANTA
				BREAK
				CASE 32
					RETURN LFI_JDESANTA
				BREAK
				CASE 33
					RETURN LFI_JDESANTA
				BREAK
				CASE 34
					RETURN LFI_ADESANTA
				BREAK
				CASE 35
					RETURN LFI_JDESANTA
				BREAK
				CASE 36
					RETURN LFI_COLIN
				BREAK
				CASE 37
					RETURN LFI_JDESANTA
				BREAK
				CASE 38
					RETURN LFI_EVAN
				BREAK
				CASE 39
					RETURN LFI_TDESANTA
				BREAK
				CASE 40
					RETURN LFI_ROSS
				BREAK
				CASE 41
					RETURN LFI_TDESANTA
				BREAK
				CASE 42
					RETURN LFI_JDESANTA
				BREAK
				CASE 43
					RETURN LFI_COLIN
				BREAK
				CASE 44
					RETURN LFI_TDESANTA
				BREAK
				CASE 45
					RETURN LFI_JDESANTA
				BREAK
				CASE 46
					RETURN LFI_TDESANTA
				BREAK
				CASE 47
					RETURN LFI_TDESANTA
				BREAK
				CASE 48
					RETURN LFI_JAY
				BREAK
				CASE 49
					RETURN LFI_COLIN
				BREAK
				CASE 50
					RETURN LFI_JDESANTA
				BREAK
				CASE 51
					RETURN LFI_EVAN
				BREAK
				CASE 52
					RETURN LFI_JDESANTA
				BREAK
				CASE 53
					RETURN LFI_AMY
				BREAK
				CASE 54
					RETURN LFI_JDESANTA
				BREAK
				CASE 55
					RETURN LFI_JDESANTA
				BREAK
				CASE 56
					RETURN LFI_AMY
				BREAK
				CASE 57
					RETURN LFI_JDESANTA
				BREAK
				CASE 58
					RETURN LFI_TDESANTA
				BREAK
				CASE 59
					RETURN LFI_JDESANTA
				BREAK
				CASE 60
					RETURN LFI_JDESANTA
				BREAK
				CASE 61
					RETURN LFI_COLIN
				BREAK
				CASE 62
					RETURN LFI_JDESANTA
				BREAK
				CASE 63
					RETURN LFI_TDESANTA
				BREAK
				CASE 64
					RETURN LFI_JDESANTA
				BREAK
				CASE 65
					RETURN LFI_TDESANTA
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_ADESANTA
				SWITCH messageID
				CASE 1
					RETURN LFI_ADESANTA
				BREAK
				CASE 2
					RETURN LFI_ADESANTA
				BREAK
				CASE 3
					RETURN LFI_ADESANTA
				BREAK
				CASE 4
					RETURN LFI_ADESANTA
				BREAK
				CASE 5
					RETURN LFI_ADESANTA
				BREAK
				CASE 6
					RETURN LFI_ADESANTA
				BREAK
				CASE 7
					RETURN LFI_ADESANTA
				BREAK
				CASE 8
					RETURN LFI_ADESANTA
				BREAK
				CASE 9
					RETURN LFI_ADESANTA
				BREAK
				CASE 10
					RETURN LFI_ADESANTA
				BREAK
				CASE 11
					RETURN LFI_ADESANTA
				BREAK
				CASE 12
					RETURN LFI_ADESANTA
				BREAK
				CASE 13
					RETURN LFI_FABIEN
				BREAK
				CASE 14
					RETURN LFI_ADESANTA
				BREAK
				CASE 15
					RETURN LFI_FABIEN
				BREAK
				CASE 16
					RETURN LFI_JDESANTA
				BREAK
				CASE 17
					RETURN LFI_LESTERCREST
				BREAK
				CASE 18
					RETURN LFI_TDESANTA
				BREAK
				CASE 19
					RETURN LFI_ADESANTA
				BREAK
				CASE 20
					RETURN LFI_JDESANTA
				BREAK
				CASE 21
					RETURN LFI_ADESANTA
				BREAK
				CASE 22
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 23
					RETURN LFI_ADESANTA
				BREAK
				CASE 24
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 25
					RETURN LFI_LISABARCLAY
				BREAK
				CASE 26
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 27
					RETURN LFI_ADESANTA
				BREAK
				CASE 28
					RETURN LFI_FABIEN
				BREAK
				CASE 29
					RETURN LFI_ADESANTA
				BREAK
				CASE 30
					RETURN LFI_DRSTANOVICZ
				BREAK
				CASE 31
					RETURN LFI_ADESANTA
				BREAK
				CASE 32
					RETURN LFI_KERI
				BREAK
				CASE 33
					RETURN LFI_ADESANTA
				BREAK
				CASE 34
					RETURN LFI_ADESANTA
				BREAK
				CASE 35
					RETURN LFI_ADESANTA
				BREAK
				CASE 36
					RETURN LFI_ADESANTA
				BREAK
				CASE 37
					RETURN LFI_ADESANTA
				BREAK
				CASE 38
					RETURN LFI_FABIEN
				BREAK
				CASE 39
					RETURN LFI_ADESANTA
				BREAK
				CASE 40
					RETURN LFI_PETER
				BREAK
				CASE 41
					RETURN LFI_ADESANTA
				BREAK
				CASE 42
					RETURN LFI_FABIEN
				BREAK
				CASE 43
					RETURN LFI_SANDRA
				BREAK
				CASE 44
					RETURN LFI_SANDRA
				BREAK
				CASE 45
					RETURN LFI_FIONA
				BREAK
				CASE 46
					RETURN LFI_JDESANTA
				BREAK
				CASE 47
					RETURN LFI_ADESANTA
				BREAK
				CASE 48
					RETURN LFI_ADESANTA
				BREAK
				CASE 49
					RETURN LFI_ADESANTA
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_LESTERCREST
			SWITCH messageID
				CASE 1
					RETURN LFI_LESTERCREST
				BREAK
				CASE 2
					RETURN LFI_LESTERCREST
				BREAK
				CASE 3
					RETURN LFI_LESTERCREST
				BREAK
				CASE 4
					RETURN LFI_LESTERCREST
				BREAK
				CASE 5
					RETURN LFI_LESTERCREST
				BREAK
				CASE 6
					RETURN LFI_LESTERCREST
				BREAK
				CASE 7
					RETURN LFI_LESTERCREST
				BREAK
				CASE 8
					RETURN LFI_LESTERCREST
				BREAK
				CASE 9
					RETURN LFI_LESTERCREST
				BREAK
				CASE 10
					RETURN LFI_LESTERCREST
				BREAK
				CASE 11
					RETURN LFI_LESTERCREST
				BREAK
				CASE 12
					RETURN LFI_CHERYL
				BREAK
				CASE 13
					RETURN LFI_GUADALOUPE
				BREAK
				CASE 14
					RETURN LFI_MAURIE
				BREAK
				CASE 15
					RETURN LFI_LESTERCREST
				BREAK
				CASE 16
					RETURN LFI_KANYA
				BREAK
				CASE 17
					RETURN LFI_NILES
				BREAK
				CASE 18
					RETURN LFI_LESTERCREST
				BREAK
				CASE 19
					RETURN LFI_LESTERCREST
				BREAK
				CASE 20
					RETURN LFI_GILES
				BREAK
				CASE 21
					RETURN LFI_LESTERCREST
				BREAK
				CASE 22
					RETURN LFI_LESTERCREST
				BREAK
				CASE 23
					RETURN LFI_LESTERCREST
				BREAK
				CASE 24
					RETURN LFI_LESTERCREST
				BREAK
				CASE 25
					RETURN LFI_LESTERCREST
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_MARYANNQUINN
			SWITCH messageID
				CASE 1
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 2
					RETURN LFI_DRWETHERBROOK
				BREAK
				CASE 3
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 4
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 5
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 6
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 7
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 8
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 9
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 10
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 11
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 12
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 13
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 14
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 15
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 16
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 17
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 18
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 19
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 20
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 21
					RETURN LFI_JONATHAN
				BREAK
				CASE 22
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 23
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 24
					RETURN LFI_MOTHER
				BREAK
				CASE 25
					RETURN LFI_RICHARD
				BREAK
				CASE 26
					RETURN LFI_JONATHAN
				BREAK
				CASE 27
					RETURN LFI_MARYANNQUINN
				BREAK
				CASE 28
					RETURN LFI_MARYANNQUINN
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_HAYDENDUBOSE
			SWITCH messageID
				CASE 1
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 2
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 3
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 4
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 5
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 6
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 7
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 8
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 9
					RETURN LFI_MELINDA
				BREAK
				CASE 10
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 11
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 12
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 13
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 14
					RETURN LFI_JASPER
				BREAK
				CASE 15
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 16
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 17
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 18
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 19
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 20
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 21
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 22
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 23
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 24
					RETURN LFI_HAYDENDUBOSE
				BREAK
				CASE 25
					RETURN LFI_HAYDENDUBOSE
				BREAK
			ENDSWITCH
		BREAK

		CASE LFI_KYLECHAVIS
			SWITCH messageID
				CASE 1
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 2
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 3
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 4
					RETURN LFI_REBECCA
				BREAK
				CASE 5
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 6
					RETURN LFI_DENA
				BREAK
				CASE 7
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 8
					RETURN LFI_ADESANTA
				BREAK
				CASE 9
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 10
					RETURN LFI_ALEX
				BREAK
				CASE 11
					RETURN LFI_SUSAN
				BREAK
				CASE 12
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 13
					RETURN LFI_ALICE
				BREAK
				CASE 14
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 15
					RETURN LFI_KYLECHAVIS
				BREAK
				CASE 16
					RETURN LFI_BILLY
				BREAK
				CASE 17
					RETURN LFI_LIZ
				BREAK
			ENDSWITCH
		BREAK


		CASE LFI_REDWOOD
			SWITCH messageID
				CASE 1
					RETURN LFI_REDWOOD
				BREAK
				CASE 2
					RETURN LFI_REDWOOD
				BREAK
				CASE 3
					RETURN LFI_REDWOOD
				BREAK
				CASE 4
					RETURN LFI_REDWOOD
				BREAK
				CASE 5
					RETURN LFI_REDWOOD
				BREAK
				CASE 6
					RETURN LFI_REDWOOD
				BREAK
				CASE 7
					RETURN LFI_REDWOOD
				BREAK
				CASE 8
					RETURN LFI_REDWOOD
				BREAK
				CASE 9
					RETURN LFI_REDWOOD
				BREAK
				CASE 10
					RETURN LFI_REDWOOD
				BREAK
				CASE 11
					RETURN LFI_KELLY
				BREAK
				CASE 12
					RETURN LFI_BRIAN
				BREAK
				CASE 13
					RETURN LFI_GEMMA
				BREAK
				CASE 14
					RETURN LFI_REDWOOD
				BREAK
				CASE 15
					RETURN LFI_GERRY
				BREAK
				CASE 16
					RETURN LFI_REDWOOD
				BREAK
				CASE 17
					RETURN LFI_REDWOOD
				BREAK
				CASE 18
					RETURN LFI_REDWOOD
				BREAK
				CASE 19
					RETURN LFI_REDWOOD
				BREAK
				CASE 20
					RETURN LFI_REDWOOD
				BREAK
				CASE 21
					RETURN LFI_REDWOOD
				BREAK
			ENDSWITCH
		BREAK 
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			SWITCH messageID
				CASE 1
					RETURN LFI_TANISHA
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_TAVELL
				BREAK
				CASE 4
					RETURN LFI_DEVIN
				BREAK
				CASE 5
					RETURN LFI_ANDREB
				BREAK
				CASE 6
					RETURN LFI_DEVIN
				BREAK
				CASE 7
					RETURN LFI_NICOLSONB
				BREAK
				CASE 8
					RETURN LFI_JAMALR
				BREAK
				CASE 9
					RETURN LFI_DEVIN
				BREAK
				CASE 10
					RETURN LFI_LAMAR
				BREAK
				CASE 11
					RETURN LFI_TANISHA
				BREAK
				CASE 12
					RETURN LFI_DEMARCUS
				BREAK
				CASE 13
					RETURN LFI_STRETCH
				BREAK
				CASE 14
					RETURN LFI_TAVELL
				BREAK
				CASE 15
					RETURN LFI_STRETCH
				BREAK
				CASE 16
					RETURN LFI_LAMAR
				BREAK
				CASE 17
					RETURN LFI_STRETCH
				BREAK
				CASE 18
					RETURN LFI_DEMARCUS
				BREAK
				CASE 19
					RETURN LFI_LAMAR
				BREAK
				CASE 20
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 21
					RETURN LFI_NICOLSONB
				BREAK
				CASE 22
					RETURN LFI_JBB
				BREAK
				CASE 23
					RETURN LFI_LAMAR
				BREAK
				CASE 24
					RETURN LFI_TONYA
				BREAK
				CASE 25
					RETURN LFI_ANDREB
				BREAK
				CASE 26
					RETURN LFI_DOM
				BREAK
				CASE 27
					RETURN LFI_DARRYLK
				BREAK
				CASE 28
					RETURN LFI_DOM
				BREAK
				CASE 29
					RETURN LFI_DENISE
				BREAK
				CASE 30
					RETURN LFI_BEV
				BREAK
				CASE 31
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 32
					RETURN LFI_BEV
				BREAK
				CASE 33
					RETURN LFI_LAMAR
				BREAK
				CASE 34
					RETURN LFI_DANAE
				BREAK
				CASE 35
					RETURN LFI_DENISE
				BREAK
				CASE 36
					RETURN LFI_LAMAR
				BREAK
				CASE 37
					RETURN LFI_TAVELL
				BREAK
				CASE 38
					RETURN LFI_DENISE
				BREAK
				CASE 39
					RETURN LFI_LAMAR
				BREAK
				CASE 40
					RETURN LFI_SIMEON
				BREAK
				CASE 41
					RETURN LFI_LAMAR
				BREAK
				CASE 42
					RETURN LFI_DARRYLK
				BREAK
				CASE 43
					RETURN LFI_LAMAR
				BREAK
				CASE 44
					RETURN LFI_SIMEON
				BREAK
				CASE 45
					RETURN LFI_TANISHA
				BREAK
				CASE 46
					RETURN LFI_SIMEON
				BREAK
				CASE 47
					RETURN LFI_LAMAR
				BREAK
				CASE 48
					RETURN LFI_LAMAR
				BREAK
				CASE 49
					RETURN LFI_TAVELL
				BREAK
				CASE 50
					RETURN LFI_DEMARCUS
				BREAK
				CASE 51
					RETURN LFI_STRETCH
				BREAK
				CASE 52
					RETURN LFI_TONYA
				BREAK
				CASE 53
					RETURN LFI_DENISE
				BREAK
				CASE 54
					RETURN LFI_JAMALR
				BREAK
				CASE 55
					RETURN LFI_LAMAR
				BREAK
				CASE 56
					RETURN LFI_DENISE
				BREAK
				CASE 57
					RETURN LFI_LAMAR
				BREAK
			ENDSWITCH
		BREAK
		
		CASE LFI_LAMAR
			SWITCH messageID
				CASE 1
                      RETURN LFI_TONYA
                BREAK
                CASE 2
                      RETURN LFI_LAMAR
                BREAK
                CASE 3
                      RETURN LFI_DARRYLK
                BREAK
                CASE 4
                      RETURN LFI_LAMAR
                BREAK
                CASE 5
                      RETURN LFI_DARRYLK
                BREAK
                CASE 6
                      RETURN LFI_STRETCH
                BREAK
                CASE 7
                      RETURN LFI_JAMALR
                BREAK
                CASE 8
                      RETURN LFI_JBB
                BREAK
                CASE 9
                      RETURN LFI_LAMAR
                BREAK
                CASE 10
                      RETURN LFI_STRETCH
                BREAK
                CASE 11
                      RETURN LFI_DEMARCUS
                BREAK
                CASE 12
                      RETURN LFI_JAMALR
                BREAK
                CASE 13
                      RETURN LFI_SIMEON
                BREAK
                CASE 14
                      RETURN LFI_LEONV
                BREAK
                CASE 15
                      RETURN LFI_LAMAR
                BREAK
                CASE 16
                      RETURN LFI_LAMAR
                BREAK
                CASE 17
                      RETURN LFI_LAMAR
                BREAK
                CASE 18
                      RETURN LFI_TANISHA
                BREAK
                CASE 19
                      RETURN LFI_ANDREB
                BREAK
                CASE 20
                      RETURN LFI_SIMEON
                BREAK
                CASE 21
                      RETURN LFI_STRETCH
                BREAK
                CASE 22
                      RETURN LFI_LAMAR
                BREAK
                CASE 23
                      RETURN LFI_DEMARCUS
                BREAK
                CASE 24
                      RETURN LFI_ANDREB
                BREAK
                CASE 25
                      RETURN LFI_JAMALR
                BREAK
                CASE 26
                      RETURN LFI_LAMAR
                BREAK
                CASE 27
                      RETURN LFI_LAMAR
                BREAK
                CASE 28
                      RETURN LFI_STRETCH
                BREAK
                CASE 29
                      RETURN LFI_GERALDG
                BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_SIMEON
			SWITCH messageID
				CASE 1
					RETURN LFI_SIMEON
				BREAK
				CASE 2
					RETURN LFI_OWENY
				BREAK
				CASE 3
					RETURN LFI_BARBARAW
				BREAK
				CASE 4
					RETURN LFI_SIMEON
				BREAK
				CASE 5
					RETURN LFI_SIMEON
				BREAK
				CASE 6
					RETURN LFI_SIMEON
				BREAK
				CASE 7
					RETURN LFI_ANAKH
				BREAK
				CASE 8
					RETURN LFI_SIMEON
				BREAK
				CASE 9
					RETURN LFI_SIMEON
				BREAK
				CASE 10
					RETURN LFI_YERGHATT
				BREAK
				CASE 11
					RETURN LFI_SIMEON
				BREAK
				CASE 12
					RETURN LFI_SACHAY
				BREAK
				CASE 13
					RETURN LFI_SIMEON
				BREAK
				CASE 14
					RETURN LFI_ANAKH
				BREAK
				CASE 15
					RETURN LFI_SIMEON
				BREAK
				CASE 16
					RETURN LFI_SIMEON
				BREAK
				CASE 17
					RETURN LFI_SACHAY
				BREAK
				CASE 18
					RETURN LFI_SIMEON
				BREAK
				CASE 19
					RETURN LFI_SIMEON
				BREAK
				CASE 20
					RETURN LFI_LAMAR
				BREAK
				CASE 21
					RETURN LFI_SIMEON
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_TONYA
			SWITCH messageID
				CASE 1
					RETURN LFI_TONYA
				BREAK
				CASE 2
					RETURN LFI_TONYA
				BREAK
				CASE 3
					RETURN LFI_TONYA
				BREAK
				CASE 4
					RETURN LFI_TONYA
				BREAK
				CASE 5
					RETURN LFI_TONYA
				BREAK
				CASE 6
					RETURN LFI_JBB
				BREAK
				CASE 7
					RETURN LFI_TONYA
				BREAK
				CASE 8
					RETURN LFI_KEEARAN
				BREAK
				CASE 9
					RETURN LFI_TONYA
				BREAK
				CASE 10
					RETURN LFI_TONYA
				BREAK
				CASE 11
					RETURN LFI_SHARONDAM
				BREAK
				CASE 12
					RETURN LFI_TONYA
				BREAK
				CASE 13
					RETURN LFI_TONYA
				BREAK
				CASE 14
					RETURN LFI_JBB
				BREAK
				CASE 15
					RETURN LFI_TONYA
				BREAK
				CASE 16
					RETURN LFI_TONYA
				BREAK
				CASE 17
					RETURN LFI_COLIND
				BREAK
				CASE 18
					RETURN LFI_TONYA
				BREAK
				CASE 19
					RETURN LFI_TONYA
				BREAK
				CASE 20
					RETURN LFI_TONYA
				BREAK
				CASE 21
					RETURN LFI_TONYA
				BREAK
				CASE 22
					RETURN LFI_TONYA
				BREAK
				CASE 23
					RETURN LFI_TONYA
				BREAK
				CASE 24
					RETURN LFI_TONYA
				BREAK
				CASE 25
					RETURN LFI_TONYA
				BREAK
				CASE 26
					RETURN LFI_TONYA
				BREAK
				CASE 27
					RETURN LFI_TONYA
				BREAK
				CASE 28
					RETURN LFI_NIAB
				BREAK
				CASE 29
					RETURN LFI_TONYA
				BREAK
				CASE 30
					RETURN LFI_TONYA
				BREAK
				CASE 31
					RETURN LFI_TANISHA
				BREAK
				CASE 32
					RETURN LFI_TONYA
				BREAK
				CASE 33
					RETURN LFI_TONYA
				BREAK
				CASE 34
					RETURN LFI_LAHRONDAW
				BREAK
				CASE 35
					RETURN LFI_TONYA
				BREAK
				CASE 36
					RETURN LFI_JBB
				BREAK
				CASE 37
					RETURN LFI_TONYA
				BREAK
				CASE 38
					RETURN LFI_TONYA
				BREAK
				CASE 39
					RETURN LFI_TONYA
				BREAK
				CASE 40
					RETURN LFI_TONYA
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_DENISE
			SWITCH messageID
				CASE 1
					RETURN LFI_DENISE
				BREAK
				CASE 2
					RETURN LFI_DENISE
				BREAK
				CASE 3
					RETURN LFI_DENISE
				BREAK
				CASE 4
					RETURN LFI_MATTIEH
				BREAK
				CASE 5
					RETURN LFI_DENISE
				BREAK
				CASE 6
					RETURN LFI_DENISE
				BREAK
				CASE 7
					RETURN LFI_DENISE
				BREAK
				CASE 8
					RETURN LFI_MATTIEH
				BREAK
				CASE 9
					RETURN LFI_DENISE
				BREAK
				CASE 10
					RETURN LFI_DENISE
				BREAK
				CASE 11
					RETURN LFI_MAGENTAA
				BREAK
				CASE 12
					RETURN LFI_DENISE
				BREAK
				CASE 13
					RETURN LFI_LAMAR
				BREAK
				CASE 14
					RETURN LFI_DENISE
				BREAK
				CASE 15
					RETURN LFI_DENISE
				BREAK
				CASE 16
					RETURN LFI_DENISE
				BREAK
				CASE 17
					RETURN LFI_DENISE
				BREAK
				CASE 18
					RETURN LFI_JULIEP
				BREAK
				CASE 19
					RETURN LFI_DENISE
				BREAK
				CASE 20
					RETURN LFI_DENISE
				BREAK
				CASE 21
					RETURN LFI_DENISE
				BREAK
				CASE 22
					RETURN LFI_PIPPYE
				BREAK
				CASE 23
					RETURN LFI_DENISE
				BREAK
				CASE 24
					RETURN LFI_DENISE
				BREAK
				CASE 25
					RETURN LFI_DENISE
				BREAK
				CASE 26
					RETURN LFI_DENISE
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_STRETCH
			SWITCH messageID
				CASE 1
					RETURN LFI_STRETCH
				BREAK
				CASE 2
					RETURN LFI_RENATTAS
				BREAK
				CASE 3
					RETURN LFI_LAMAR
				BREAK
				CASE 4
					RETURN LFI_DEMARCUS
				BREAK
				CASE 5
					RETURN LFI_LAMAR
				BREAK
				CASE 6
					RETURN LFI_JBB
				BREAK
				CASE 7
					RETURN LFI_STRETCH
				BREAK
				CASE 8
					RETURN LFI_STRETCH
				BREAK
				CASE 9
					RETURN LFI_STRETCH
				BREAK
				CASE 10
					RETURN LFI_LAMAR
				BREAK
				CASE 11
					RETURN LFI_TONYA
				BREAK
				CASE 12
					RETURN LFI_STRETCH
				BREAK
				CASE 13
					RETURN LFI_LAMAR
				BREAK
				CASE 14
					RETURN LFI_STRETCH
				BREAK
				CASE 15
					RETURN LFI_DEMARCUS
				BREAK
				CASE 16
					RETURN LFI_STRETCH
				BREAK
				CASE 17
					RETURN LFI_LAMAR
				BREAK
				CASE 18
					RETURN LFI_STRETCH
				BREAK
				CASE 19
					RETURN LFI_STRETCH
				BREAK
				CASE 20
					RETURN LFI_STRETCH
				BREAK
				CASE 21
					RETURN LFI_STRETCH
				BREAK
				CASE 22
					RETURN LFI_STRETCH
				BREAK
				CASE 23
					RETURN LFI_LAMAR
				BREAK
				CASE 24
					RETURN LFI_STRETCH
				BREAK
				CASE 25
					RETURN LFI_STRETCH
				BREAK
				CASE 26
					RETURN LFI_LAMAR
				BREAK
				CASE 27
					RETURN LFI_STRETCH
				BREAK
				CASE 28
					RETURN LFI_STRETCH
				BREAK
				CASE 29
					RETURN LFI_STRETCH
				BREAK
				CASE 30
					RETURN LFI_LAMAR
				BREAK
			ENDSWITCH
		BREAK
	
		CASE LFI_TANISHA
			SWITCH messageID
				CASE 1
					RETURN LFI_TANISHA
				BREAK
				CASE 2
					RETURN LFI_TANISHA
				BREAK
				CASE 3
					RETURN LFI_LAMAR
				BREAK
				CASE 4
					RETURN LFI_TANISHA
				BREAK
				CASE 5
					RETURN LFI_TANISHA
				BREAK
				CASE 6
					RETURN LFI_LELAY
				BREAK
				CASE 7
					RETURN LFI_TANISHA
				BREAK
				CASE 8
					RETURN LFI_TANISHA
				BREAK
				CASE 9
					RETURN LFI_TANISHA
				BREAK
				CASE 10
					RETURN LFI_TONYA
				BREAK
				CASE 11
					RETURN LFI_TANISHA
				BREAK
				CASE 12
					RETURN LFI_TANISHA
				BREAK
				CASE 13
					RETURN LFI_LAMAR
				BREAK
				CASE 14
					RETURN LFI_TANISHA
				BREAK
				CASE 15
					RETURN LFI_TANISHA
				BREAK
				CASE 16
					RETURN LFI_TANISHA
				BREAK
				CASE 17
					RETURN LFI_JBB
				BREAK
				CASE 18
					RETURN LFI_TANISHA
				BREAK
				CASE 19
					RETURN LFI_TANISHA
				BREAK
				CASE 20
					RETURN LFI_LAMAR
				BREAK
				CASE 21
					RETURN LFI_TANISHA
				BREAK
				CASE 22
					RETURN LFI_DANAE
				BREAK
				CASE 23
					RETURN LFI_TANISHA
				BREAK
				CASE 24
					RETURN LFI_TANISHA
				BREAK
				CASE 25
					RETURN LFI_CATRINAW
				BREAK
				CASE 26
					RETURN LFI_DENISE
				BREAK
				CASE 27
					RETURN LFI_TANISHA
				BREAK
				CASE 28
					RETURN LFI_MONETTEA
				BREAK
				CASE 29
					RETURN LFI_TANISHA
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_TAVELL
			SWITCH messageID
				CASE 1
					RETURN LFI_TAVELL
				BREAK
				CASE 2
					RETURN LFI_TAVELL
				BREAK
				CASE 3
					RETURN LFI_TAVELL
				BREAK
				CASE 4
					RETURN LFI_TAVELL
				BREAK
				CASE 5
					RETURN LFI_TAVELL
				BREAK
				CASE 6
					RETURN LFI_TAVELL
				BREAK
				CASE 7
					RETURN LFI_TAVELL
				BREAK
				CASE 8
					RETURN LFI_TAVELL
				BREAK
				CASE 9
					RETURN LFI_TAVELL
				BREAK
				CASE 10
					RETURN LFI_TAVELL
				BREAK
				CASE 11
					RETURN LFI_LAMAR
				BREAK
				CASE 12
					RETURN LFI_TAVELL
				BREAK
				CASE 13
					RETURN LFI_TAVELL
				BREAK
				CASE 14
					RETURN LFI_TAVELL
				BREAK
				CASE 15
					RETURN LFI_TAVELL
				BREAK
				CASE 16
					RETURN LFI_TAVELL
				BREAK
				CASE 17
					RETURN LFI_NICOLSONB
				BREAK
				CASE 18
					RETURN LFI_TAVELL
				BREAK
				CASE 19
					RETURN LFI_TAVELL
				BREAK
				CASE 20
					RETURN LFI_TAVELL
				BREAK
				CASE 21
					RETURN LFI_DENISE
				BREAK
				CASE 22
					RETURN LFI_TAVELL
				BREAK
				CASE 23
					RETURN LFI_TAVELL
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_DOM
			SWITCH messageID
				CASE 1
					RETURN LFI_GARYH
				BREAK
				CASE 2
					RETURN LFI_JEFFC
				BREAK
				CASE 3
					RETURN LFI_DOM
				BREAK
				CASE 4
					RETURN LFI_DOM
				BREAK
				CASE 5
					RETURN LFI_DOM
				BREAK
				CASE 6
					RETURN LFI_DOM
				BREAK
				CASE 7
					RETURN LFI_DOM
				BREAK
				CASE 8
					RETURN LFI_DOM
				BREAK
				CASE 9
					RETURN LFI_DOM
				BREAK
				CASE 10
					RETURN LFI_DOM
				BREAK
				CASE 11
					RETURN LFI_DOM
				BREAK
				CASE 12
					RETURN LFI_DOM
				BREAK
				CASE 13
					RETURN LFI_DOM
				BREAK
				CASE 14
					RETURN LFI_DOM
				BREAK
				CASE 15
					RETURN LFI_DOM
				BREAK
				CASE 16
					RETURN LFI_DOM
				BREAK
				CASE 17
					RETURN LFI_DOM
				BREAK
				CASE 18
					RETURN LFI_KARENL
				BREAK
				CASE 19
					RETURN LFI_DOM
				BREAK
				CASE 20
					RETURN LFI_DOM
				BREAK
				CASE 21
					RETURN LFI_DOM
				BREAK
				CASE 22
					RETURN LFI_DOM
				BREAK
				CASE 23
					RETURN LFI_DOM
				BREAK
				CASE 24
					RETURN LFI_JEFFC
				BREAK
				CASE 25
					RETURN LFI_DOM
				BREAK
				CASE 26
					RETURN LFI_DOM
				BREAK
				CASE 27
					RETURN LFI_DOM
				BREAK
				CASE 28
					RETURN LFI_JEFFC
				BREAK
				CASE 29
					RETURN LFI_DOM
				BREAK
				CASE 30
					RETURN LFI_DOM
				BREAK
				CASE 31
					RETURN LFI_MARKP
				BREAK
				CASE 32
					RETURN LFI_DOM
				BREAK
				CASE 33
					RETURN LFI_GARYH
				BREAK
				CASE 34
					RETURN LFI_DOM
				BREAK
				CASE 35
					RETURN LFI_DOM
				BREAK
				CASE 36
					RETURN LFI_DOM
				BREAK
				CASE 37
					RETURN LFI_DOM
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_BEV
			SWITCH messageID
				CASE 1
					RETURN LFI_BEV
				BREAK
				CASE 2
					RETURN LFI_BEV
				BREAK
				CASE 3
					RETURN LFI_BEV
				BREAK
				CASE 4
					RETURN LFI_BEV
				BREAK
				CASE 5
					RETURN LFI_BEV
				BREAK
				CASE 6
					RETURN LFI_BEV
				BREAK
				CASE 7
					RETURN LFI_BEV
				BREAK
				CASE 8
					RETURN LFI_BEV
				BREAK
				CASE 9
					RETURN LFI_BEV
				BREAK
				CASE 10
					RETURN LFI_BEV
				BREAK
				CASE 11
					RETURN LFI_BEV
				BREAK
				CASE 12
					RETURN LFI_BEV
				BREAK
				CASE 13
					RETURN LFI_BEV
				BREAK
				CASE 14
					RETURN LFI_BEV
				BREAK
				CASE 15
					RETURN LFI_BEV
				BREAK
				CASE 16
					RETURN LFI_MADISONF
				BREAK
				CASE 17
					RETURN LFI_BEV
				BREAK
				CASE 18
					RETURN LFI_BEV
				BREAK
				CASE 19
					RETURN LFI_ALANF
				BREAK
				CASE 20
					RETURN LFI_BEV
				BREAK
				CASE 21
					RETURN LFI_BEV
				BREAK
				CASE 22
					RETURN LFI_RAYG
				BREAK
				CASE 23
					RETURN LFI_BEV
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_DEVIN
			SWITCH messageID
				CASE 1
					RETURN LFI_DEVIN
				BREAK
				CASE 2
					RETURN LFI_DEVIN
				BREAK
				CASE 3
					RETURN LFI_RAYN
				BREAK
				CASE 4
					RETURN LFI_DEVIN
				BREAK
				CASE 5
					RETURN LFI_DEVIN
				BREAK
				CASE 6
					RETURN LFI_DEVIN
				BREAK
				CASE 7
					RETURN LFI_DEVIN
				BREAK
				CASE 8
					RETURN LFI_DEVIN
				BREAK
				CASE 9
					RETURN LFI_DEVIN
				BREAK
				CASE 10
					RETURN LFI_DEVIN
				BREAK
				CASE 11
					RETURN LFI_DEVIN
				BREAK
				CASE 12
					RETURN LFI_DEVIN
				BREAK
				CASE 13
					RETURN LFI_DEVIN
				BREAK
				CASE 14
					RETURN LFI_DEVIN
				BREAK
				CASE 15
					RETURN LFI_SIMONH
				BREAK
				CASE 16
					RETURN LFI_DEVIN
				BREAK
				CASE 17
					RETURN LFI_DEVIN
				BREAK
				CASE 18
					RETURN LFI_DEVIN
				BREAK
				CASE 19
					RETURN LFI_DEVIN
				BREAK
				CASE 20
					RETURN LFI_RAYN
				BREAK
				CASE 21
					RETURN LFI_DEVIN
				BREAK
				CASE 22
					RETURN LFI_DEVIN
				BREAK
				CASE 23
					RETURN LFI_DEVIN
				BREAK
				CASE 24
					RETURN LFI_DEVIN
				BREAK
				CASE 25
					RETURN LFI_RAYN
				BREAK
				CASE 26
					RETURN LFI_DEVIN
				BREAK
				CASE 27
					RETURN LFI_DEVIN
				BREAK
				CASE 28
					RETURN LFI_DEVIN
				BREAK
				CASE 29
					RETURN LFI_DEVIN
				BREAK
				CASE 30
					RETURN LFI_MOLLYS
				BREAK
				CASE 31
					RETURN LFI_DEVIN
				BREAK
				CASE 32
					RETURN LFI_DEVIN
				BREAK
				CASE 33
					RETURN LFI_DEVIN
				BREAK
				CASE 34
					RETURN LFI_MOLLYS
				BREAK
			ENDSWITCH
		BREAK
				
		CASE LFI_DEMARCUS
			SWITCH messageID
				CASE 1
					RETURN LFI_DEMARCUS
				BREAK
				CASE 2
					RETURN LFI_DEMARCUS
				BREAK
				CASE 3
					RETURN LFI_DEMARCUS
				BREAK
				CASE 4
					RETURN LFI_DEMARCUS
				BREAK
				CASE 5
					RETURN LFI_DEMARCUS
				BREAK
				CASE 6
					RETURN LFI_DEMARCUS
				BREAK
				CASE 7
					RETURN LFI_DEMARCUS
				BREAK
				CASE 8
					RETURN LFI_DEMARCUS
				BREAK
				CASE 9
					RETURN LFI_DEMARCUS
				BREAK
				CASE 10
					RETURN LFI_DEMARCUS
				BREAK
				CASE 11
					RETURN LFI_LAMAR
				BREAK
				CASE 12
					RETURN LFI_DEJAM
				BREAK
				CASE 13
					RETURN LFI_DEMARCUS
				BREAK
				CASE 14
					RETURN LFI_DEMARCUS
				BREAK
				CASE 15
					RETURN LFI_DEMARCUS
				BREAK
				CASE 16
					RETURN LFI_DEMARCUS
				BREAK
				CASE 17
					RETURN LFI_DEMARCUS
				BREAK
				CASE 18
					RETURN LFI_DEJAM
				BREAK
				CASE 19
					RETURN LFI_DEMARCUS
				BREAK
				CASE 20
					RETURN LFI_DEMARCUS
				BREAK
				CASE 21
					RETURN LFI_JBB
				BREAK
				CASE 22
					RETURN LFI_HAILEYW
				BREAK
				CASE 23
					RETURN LFI_DEMARCUS
				BREAK
				CASE 24
					RETURN LFI_DEMARCUS
				BREAK
				CASE 25
					RETURN LFI_STRETCH
				BREAK
				CASE 26
					RETURN LFI_DEMARCUS
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_FEUD
			SWITCH messageID
				CASE 1
					RETURN LFI_TYSONF
				BREAK
				CASE 2
					RETURN LFI_GRAHAMR
				BREAK
				CASE 3
					RETURN LFI_FEUD
				BREAK
				CASE 4
					RETURN LFI_ERIKD
				BREAK
				CASE 5
					RETURN LFI_TYSONF
				BREAK
				CASE 6
					RETURN LFI_NELSONW
				BREAK
				CASE 7
					RETURN LFI_MODFEUD
				BREAK
				CASE 8
					RETURN LFI_JAXS
				BREAK
				CASE 9
					RETURN LFI_LAMAR
				BREAK
				CASE 10
					RETURN LFI_MODFEUD
				BREAK
				CASE 11
					RETURN LFI_ADRICH
				BREAK
				CASE 12
					RETURN LFI_KARLR
				BREAK
				CASE 13
					RETURN LFI_TYSONF
				BREAK
				CASE 14
					RETURN LFI_TAYEB
				BREAK
				CASE 15
					RETURN LFI_LONNYG
				BREAK
				CASE 16
					RETURN LFI_DARRYLK
				BREAK
				CASE 17
					RETURN LFI_MODFEUD
				BREAK
				CASE 18
					RETURN LFI_REGISW
				BREAK
				CASE 19
					RETURN LFI_TRENTONM
				BREAK
				CASE 20
					RETURN LFI_RESHAYM
				BREAK
				CASE 21
					RETURN LFI_ANTOINEP
				BREAK
				CASE 22
					RETURN LFI_RESHAYM
				BREAK
				CASE 23
					RETURN LFI_BENTONC
				BREAK
				CASE 24
					RETURN LFI_GRAHAMR
				BREAK
				CASE 25
					RETURN LFI_STERLINL
				BREAK
				CASE 26
					RETURN LFI_SARAP 
				BREAK
				CASE 27
					RETURN LFI_DARNELLS 
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_SPRUNK
			SWITCH messageID
				CASE 1
					RETURN LFI_SPRUNK
				BREAK
				CASE 2
					RETURN LFI_PAULO
				BREAK
				CASE 3
					RETURN LFI_KATEM
				BREAK
				CASE 4
					RETURN LFI_SPRUNK
				BREAK
				CASE 5
					RETURN LFI_SPRUNK
				BREAK
				CASE 6
					RETURN LFI_JOSHUAW
				BREAK
				CASE 7
					RETURN LFI_SPRUNK
				BREAK
				CASE 8
					RETURN LFI_SPRUNK
				BREAK
				CASE 9
					RETURN LFI_SPRUNK
				BREAK
				CASE 10
					RETURN LFI_SPRUNK
				BREAK
				CASE 11
					RETURN LFI_SPRUNK
				BREAK
				CASE 12
					RETURN LFI_SPRUNK
				BREAK
				CASE 13
					RETURN LFI_VINCEH
				BREAK
				CASE 14
					RETURN LFI_SPRUNK
				BREAK
				CASE 15
					RETURN LFI_SPRUNK
				BREAK
				CASE 16
					RETURN LFI_SPRUNK
				BREAK
				CASE 17
					RETURN LFI_SPRUNK
				BREAK
				CASE 18
					RETURN LFI_TODDR
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_INKINC
			SWITCH messageID
				CASE 1
					RETURN LFI_INKINC
				BREAK
				CASE 2
					RETURN LFI_INKINC
				BREAK
				CASE 3
					RETURN LFI_INKINC
				BREAK
				CASE 4
					RETURN LFI_INKINC
				BREAK
				CASE 5
					RETURN LFI_INKINC
				BREAK
				CASE 6
					RETURN LFI_INKINC
				BREAK
				CASE 7
					RETURN LFI_INKINC
				BREAK
				CASE 8
					RETURN LFI_STEVEW 
				BREAK
				CASE 9
					RETURN LFI_INKINC
				BREAK
				CASE 10
					RETURN LFI_INKINC
				BREAK
				CASE 11
					RETURN LFI_INKINC
				BREAK
				CASE 12
					RETURN LFI_MIKEH
				BREAK
				CASE 13
					RETURN LFI_INKINC
				BREAK
				CASE 14
					RETURN LFI_HARVEYP
				BREAK
				CASE 15
					RETURN LFI_INKINC
				BREAK
				CASE 16
					RETURN LFI_INKINC
				BREAK
				CASE 17
					RETURN LFI_INKINC
				BREAK
				CASE 18
					RETURN LFI_INKINC
				BREAK
				CASE 19
					RETURN LFI_INKINC
				BREAK
				CASE 20
					RETURN LFI_INKINC
				BREAK
				CASE 21
					RETURN LFI_ANDYW
				BREAK
				CASE 22
					RETURN LFI_INKINC
				BREAK
				CASE 23
					RETURN LFI_TAYLOTH
				BREAK
				CASE 24
					RETURN LFI_INKINC
				BREAK
				CASE 25
					RETURN LFI_INKINC
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_HERRKUTZ
			SWITCH messageID
				CASE 1
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 2
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 3
					RETURN LFI_DONNAHK
				BREAK
				CASE 4
					RETURN LFI_LANCEW
				BREAK
				CASE 5
					RETURN LFI_PHILG
				BREAK
				CASE 6
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 7
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 8
					RETURN LFI_JAMIEE
				BREAK
				CASE 9
					RETURN LFI_DONNAHK
				BREAK
				CASE 10
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 11
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 12
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 13
					RETURN LFI_JONH
				BREAK
				CASE 14
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 15
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 16
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 17
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 18
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 19
					RETURN LFI_GERRYC
				BREAK
				CASE 20
					RETURN LFI_DONNAHK
				BREAK
				CASE 21
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 22
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 23
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 24
					RETURN LFI_AARONF
				BREAK
				CASE 25
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 26
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 27
					RETURN LFI_CONNORS
				BREAK
			ENDSWITCH
		BREAK
			
		CASE LFI_LSCUSTOMS
			SWITCH messageID
				CASE 1
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 2
					RETURN LFI_POPPAL
				BREAK
				CASE 3
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 4
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 5
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 6
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 7
					RETURN LFI_HAILEYW
				BREAK
				CASE 8
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 9
					RETURN LFI_LAMAR
				BREAK
				CASE 10
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 11
					RETURN LFI_SAMW
				BREAK
				CASE 12
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 13
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 14
					RETURN LFI_GARYH
				BREAK
				CASE 15
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 16
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 17
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 18
					RETURN LFI_HANKS
				BREAK
				CASE 19
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 20
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 21
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 22
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 23
					RETURN LFI_LSCUSTOMS
				BREAK
				CASE 24
					RETURN LFI_LSCUSTOMS
				BREAK
			ENDSWITCH
		BREAK
		
		 // Trevor & friends
            CASE LFI_TREVOR
                  SWITCH messageID
                        CASE 1
							RETURN LFI_RON
                        BREAK
						CASE 2
							RETURN LFI_RON
                        BREAK
						CASE 3
							RETURN LFI_RON
                        BREAK
						CASE 4
							RETURN LFI_RON
                        BREAK
						CASE 5
							RETURN LFI_RON
                        BREAK
						CASE 6
							RETURN LFI_RON
                        BREAK
						CASE 7
							RETURN LFI_RON
                        BREAK
						CASE 8
							RETURN LFI_RON
                        BREAK
						CASE 9
							RETURN LFI_RON
                        BREAK
						
                  ENDSWITCH
            BREAK
			
			CASE LFI_RON
                  SWITCH messageID
                        CASE 1
							RETURN LFI_ASHLEY
                        BREAK
						CASE 2
							RETURN LFI_RON
                        BREAK
						CASE 3
							RETURN LFI_CLETUS
                        BREAK
						CASE 4
							RETURN LFI_WADE
                        BREAK
						CASE 5
							RETURN LFI_WADE
                        BREAK
                  ENDSWITCH
            BREAK
			
			 CASE LFI_WADE
                  SWITCH messageID
                        CASE 1
							RETURN LFI_JENNY
                        BREAK
						CASE 2
							RETURN LFI_WADE
                        BREAK
						CASE 3
							RETURN LFI_FLOYD
                        BREAK
						CASE 4
							RETURN LFI_FLOYD
                        BREAK
						CASE 5
							RETURN LFI_FLOYD
                        BREAK
						CASE 6
							RETURN LFI_WADE
                        BREAK
						CASE 7
							RETURN LFI_WADE
                        BREAK
						CASE 8
							RETURN LFI_HANK
                        BREAK
						
                  ENDSWITCH
            BREAK
			
			CASE LFI_LUDENDORFF
                  SWITCH messageID
                        CASE 1
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 2
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 3
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 4
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 5
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 6
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 7
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 8
							RETURN LFI_LUDENDORFF
                        BREAK
						CASE 9
							RETURN LFI_LUDENDORFF
                        BREAK
                  ENDSWITCH
            BREAK
		
	ENDSWITCH
	
	RETURN LFI_MAX

ENDFUNC

/// PURPOSE:
///   In the text label "LFI_F1_7", this would get the "1" - F1 is Franklin, F2 is Lamar, etc
FUNC INT GET_CHARACTER_MESSAGE_THREAD_NUMBER(INT LFI_ID)

      SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
            // Michael & friends
            CASE LFI_MICHAEL
                  RETURN 1
            BREAK
			CASE LFI_TDESANTA
				RETURN 2
			BREAK
			CASE LFI_JDESANTA
				RETURN 3
			BREAK
			CASE LFI_ADESANTA
				RETURN 4
			BREAK
			CASE LFI_LESTERCREST
				RETURN 5
			BREAK
			CASE LFI_MARYANNQUINN
				RETURN 6
			BREAK
			CASE LFI_HAYDENDUBOSE
				RETURN 7
			BREAK
			CASE LFI_KYLECHAVIS
				RETURN 8
			BREAK
			CASE LFI_REDWOOD
				RETURN 9
			BREAK
            
            // Franklin & friends
            CASE LFI_FRANKLIN
                  RETURN 1
            BREAK
            CASE LFI_LAMAR
                  RETURN 2
            BREAK
            CASE LFI_SIMEON
                  RETURN 3
            BREAK
            CASE LFI_TONYA
                  RETURN 4
            BREAK
            CASE LFI_DENISE
                  RETURN 5
            BREAK
            CASE LFI_STRETCH
                  RETURN 6
            BREAK
            CASE LFI_TANISHA
                  RETURN 7
            BREAK
            CASE LFI_TAVELL
                  RETURN 8
            BREAK
            CASE LFI_DOM
                  RETURN 9
            BREAK
            CASE LFI_BEV
                  RETURN 10
            BREAK
            CASE LFI_DEVIN
                  RETURN 11
            BREAK
            CASE LFI_DEMARCUS
                  RETURN 12
            BREAK
            CASE LFI_FEUD
                  RETURN 13
            BREAK
            CASE LFI_SPRUNK
                  RETURN 14
            BREAK
            CASE LFI_INKINC
                  RETURN 15
            BREAK
            CASE LFI_HERRKUTZ
                  RETURN 16
            BREAK
            CASE LFI_LSCUSTOMS
                  RETURN 17
            BREAK
            
            // Trevor & friends
            CASE LFI_TREVOR
				RETURN 1
            BREAK
            CASE LFI_RON
				RETURN 2
			BREAK
            CASE LFI_WADE
				RETURN 3
			BREAK
            CASE LFI_LUDENDORFF
				RETURN 4
			BREAK
      ENDSWITCH
      
      RETURN 0

ENDFUNC


FUNC TEXT_LABEL GET_MESSAGE_FOR_MESSAGE_ID(INT currentID, INT messageID)

	TEXT_LABEL tlReturn = "LFI_"

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			tlReturn += "M"
		BREAK
		CASE CHAR_FRANKLIN
			tlReturn += "F"
		BREAK
		CASE CHAR_TREVOR
			tlReturn += "T"
		BREAK
		DEFAULT
			tlReturn += "UNKNOWN"
		BREAK
	ENDSWITCH
	
	tlReturn += currentID
	tlReturn += "_"
	tlReturn += messageID
	
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Lifeinvader: Returning ", tlReturn , " for message...")
	
	RETURN tlReturn

ENDFUNC

FUNC TEXT_LABEL GET_SMALL_PORTRAIT(LFICharList LFICharToGet)

      text_label tlReturn = "SMALL_"
      
      SWITCH LFICharToGet
            // Michael & friends
            CASE LFI_MICHAEL
                  tlReturn += "164"
                  BREAK
            CASE LFI_TDESANTA
                  tlReturn += "165"
                  BREAK
            CASE LFI_JDESANTA
                  tlReturn += "166"
                  BREAK
            CASE LFI_ADESANTA
                  tlReturn += "167"
                  BREAK
            CASE LFI_LESTERCREST
                  tlReturn += "168"
                  BREAK
            CASE LFI_MARYANNQUINN
                  tlReturn += "169"
                  BREAK
            CASE LFI_HAYDENDUBOSE
                  tlReturn += "170"
                  BREAK
            CASE LFI_KYLECHAVIS
                  tlReturn += "171"
                  BREAK
            CASE LFI_REDWOOD
                  tlReturn += "172"
                  BREAK
                  
            CASE LFI_FREDDY
                  tlReturn += "91"
                  BREAK
            CASE LFI_RALPH
                  tlReturn += "92"
                  BREAK
            CASE LFI_LAUREN
                  tlReturn += "93"
                  BREAK
            CASE LFI_FOSTER
                  tlReturn += "94"
                  BREAK
            CASE LFI_BREE
                  tlReturn += "95"
                  BREAK
            CASE LFI_KIM
                  tlReturn += "96"
                  BREAK
            CASE LFI_LISAWALLIN
                  tlReturn += "97"
                  BREAK
            CASE LFI_IAN
                  tlReturn += "98"
                  BREAK
            CASE LFI_AHRON
                  tlReturn += "99"
                  BREAK
            CASE LFI_RYAN
                  tlReturn += "100"
                  BREAK
            CASE LFI_COLIN
                  tlReturn += "101"
                  BREAK
            CASE LFI_EVAN
                  tlReturn += "102"
                  BREAK
            CASE LFI_ROSS
                  tlReturn += "103"
                  BREAK
            CASE LFI_JAY
                  tlReturn += "104"
                  BREAK
            CASE LFI_AMY
                  tlReturn += "105"
                  BREAK
            CASE LFI_FABIEN
                  tlReturn += "106"
                  BREAK
            CASE LFI_LISABARCLAY
                  tlReturn += "107"
                  BREAK
            CASE LFI_DRSTANOVICZ
                  tlReturn += "108"
                  BREAK
            CASE LFI_KERI
                  tlReturn += "109"
                  BREAK
            CASE LFI_PETER
                  tlReturn += "110"
                  BREAK
            CASE LFI_SANDRA
                  tlReturn += "111"
                  BREAK
            CASE LFI_FIONA
                  tlReturn += "112"
                  BREAK
            CASE LFI_CHERYL
                  tlReturn += "113"
                  BREAK
            CASE LFI_GUADALOUPE
                  tlReturn += "114"
                  BREAK
            CASE LFI_MAURIE
                  tlReturn += "115"
                  BREAK
            CASE LFI_KANYA
                  tlReturn += "116"
                  BREAK
            CASE LFI_NILES
                  tlReturn += "117"
                  BREAK
            CASE LFI_GILES
                  tlReturn += "118"
                  BREAK
            CASE LFI_DRWETHERBROOK
                  tlReturn += "119"
                  BREAK
            CASE LFI_JONATHAN
                  tlReturn += "120"
                  BREAK
            CASE LFI_RICHARD
                  tlReturn += "121"
                  BREAK
            CASE LFI_MOTHER
                  tlReturn += "142"
                  BREAK
            CASE LFI_MELINDA
                  tlReturn += "122"
                  BREAK
            CASE LFI_JASPER
                  tlReturn += "123"
                  BREAK
            CASE LFI_REBECCA
                  tlReturn += "124"
                  BREAK
            CASE LFI_DENA
                  tlReturn += "125"
                  BREAK
            CASE LFI_ALEX
                  tlReturn += "126"
                  BREAK
            CASE LFI_SUSAN
                  tlReturn += "127"
                  BREAK
            CASE LFI_ALICE
                  tlReturn += "128"
                  BREAK
            CASE LFI_BILLY
                  tlReturn += "129"
                  BREAK
            CASE LFI_LIZ
                  tlReturn += "130"
                  BREAK
            CASE LFI_KELLY
                  tlReturn += "131"
                  BREAK
            CASE LFI_BRIAN
                  tlReturn += "132"
                  BREAK
            CASE LFI_GEMMA
                  tlReturn += "133"
                  BREAK
            CASE LFI_GERRY
                  tlReturn += "134"
                  BREAK
			CASE LFI_GARYSCALES
				tlReturn += "141"
			BREAK
            
            CASE LFI_FAMEORSHAME
                  tlReturn += "182"
                  BREAK
            CASE LFI_RIGHTEOUSSLAUGHTER
                  tlReturn += "183"
                  BREAK
            CASE LFI_GAMMI
                  tlReturn += "184"
                  BREAK
            CASE LFI_PEDALANDMETAL
                  tlReturn += "185"
                  BREAK
            CASE LFI_PROLAPS
                  tlReturn += "186"
                  BREAK
            CASE LFI_LOSSANTOSGOLFCLUB
                  tlReturn += "137"
                  BREAK
            CASE LFI_FACADE
                  tlReturn += "138"
                  BREAK
            CASE LFI_EGOCHASER
                  tlReturn += "139"
                  BREAK
            CASE LFI_HEAT
                  tlReturn += "140"
                  BREAK
            CASE LFI_PENDULUS
                  tlReturn += "187"
            BREAK
			CASE LFI_STCE
                  tlReturn += "85"
            BREAK
            
            // Franklin & friends
            CASE LFI_FRANKLIN
                  tlReturn += "143"
            BREAK
            CASE LFI_LAMAR
                  tlReturn += "144"
            BREAK
            CASE LFI_SIMEON
                  tlReturn += "145"
            BREAK
            CASE LFI_TONYA
                  tlReturn += "146"
            BREAK
            CASE LFI_DENISE
                  tlReturn += "147"
            BREAK
            CASE LFI_STRETCH
                  tlReturn += "148"
            BREAK
            CASE LFI_TANISHA
                  tlReturn += "149"
            BREAK
            CASE LFI_TAVELL
                  tlReturn += "150"
            BREAK
            CASE LFI_DOM
                  tlReturn += "151"
            BREAK
            CASE LFI_BEV
                  tlReturn += "152"
            BREAK
            CASE LFI_DEVIN
                  tlReturn += "153"
            BREAK
            CASE LFI_DEMARCUS
                  tlReturn += "154"
            BREAK
            CASE LFI_FEUD
                  tlReturn += "155"
            BREAK
            CASE LFI_SPRUNK
                  tlReturn += "156"
            BREAK
            CASE LFI_INKINC
                  tlReturn += "157"
            BREAK
            CASE LFI_HERRKUTZ
                  tlReturn += "158"
            BREAK
            CASE LFI_LSCUSTOMS
                  tlReturn += "159"
            BREAK
                  
            CASE LFI_ANDREB
                  tlReturn += "1"
                  BREAK
            CASE LFI_NICOLSONB
                  tlReturn += "2"
                  BREAK
            CASE LFI_JAMALR
                  tlReturn += "3"
                  BREAK
            CASE LFI_JBB
                  tlReturn += "4"
                  BREAK
            CASE LFI_DARRYLK
                  tlReturn += "5"
                  BREAK
            CASE LFI_DANAE
                  tlReturn += "6"
                  BREAK
            CASE LFI_LEONV
                  tlReturn += "7"
                  BREAK
            CASE LFI_GERALDG
                  tlReturn += "8"
                  BREAK
            CASE LFI_OWENY
                  tlReturn += "9"
                  BREAK
            CASE LFI_BARBARAW
                  tlReturn += "10"
                  BREAK
            CASE LFI_ANAKH
                  tlReturn += "11"
                  BREAK
            CASE LFI_YERGHATT
                  tlReturn += "12"
                  BREAK
            CASE LFI_SACHAY
                  tlReturn += "13"
                  BREAK
            CASE LFI_TYSONF
                  tlReturn += "39"
                  BREAK
            CASE LFI_KEEARAN
                  tlReturn += "14"
                  BREAK
            CASE LFI_SHARONDAM
                  tlReturn += "15"
                  BREAK
            CASE LFI_MADISONF
                  tlReturn += "31"
                  BREAK
            CASE LFI_COLIND
                  tlReturn += "16"
                  BREAK
            CASE LFI_NIAB
                  tlReturn += "17"
                  BREAK
            CASE LFI_LAHRONDAW
                  tlReturn += "18"
                  BREAK
            CASE LFI_MATTIEH
                  tlReturn += "19"
                  BREAK
            CASE LFI_MAGENTAA
                  tlReturn += "20"
                  BREAK
            CASE LFI_JULIEP
                  tlReturn += "21"
                  BREAK
            CASE LFI_PIPPYE
                  tlReturn += "22"
                  BREAK
            CASE LFI_RENATTAS
                  tlReturn += "23"
                  BREAK
            CASE LFI_LELAY
                  tlReturn += "24"
                  BREAK
            CASE LFI_CATRINAW
                  tlReturn += "25"
                  BREAK
            CASE LFI_MONETTEA
                  tlReturn += "26"
                  BREAK
            CASE LFI_GARRYH
                  tlReturn += "27"
                  BREAK
            CASE LFI_JEFFC
                  tlReturn += "28"
                  BREAK
            CASE LFI_KARENL
                  tlReturn += "29"
                  BREAK
            CASE LFI_MARKP
                  tlReturn += "30"
                  BREAK
            CASE LFI_ALANF
                  tlReturn += "32"
                  BREAK
            CASE LFI_RAYG
                  tlReturn += "33"
                  BREAK
            CASE LFI_RAYN
                  tlReturn += "34"
                  BREAK
            CASE LFI_SIMONH
                  tlReturn += "35"
                  BREAK
            CASE LFI_MOLLYS
                  tlReturn += "36"
                  BREAK
            CASE LFI_DEJAM
                  tlReturn += "37"
                  BREAK
            CASE LFI_HAILEYB
                  tlReturn += "38"
                  BREAK
            CASE LFI_ERIKD
                  tlReturn += "41"
                  BREAK
            CASE LFI_GRAHAMR
                  tlReturn += "40"
                  BREAK
            CASE LFI_NELSONW
                  tlReturn += "42"
                  BREAK
            CASE LFI_MODFEUD
                  tlReturn += "43"
                  BREAK
            CASE LFI_JAXS
                  tlReturn += "44"
                  BREAK
            CASE LFI_ADRICH
                  tlReturn += "45"
                  BREAK
            CASE LFI_KARLR
                  tlReturn += "46"
                  BREAK
            CASE LFI_TAYEB
                  tlReturn += "47"
                  BREAK
            CASE LFI_TRENTONM
                  tlReturn += "48"
                  BREAK
            CASE LFI_LONNYG
                  tlReturn += "49"
                  BREAK
            CASE LFI_REGISW
                  tlReturn += "50"
                  BREAK
            CASE LFI_RESHAYM
                  tlReturn += "51"
                  BREAK
            CASE LFI_ANTOINEP
                  tlReturn += "52"
                  BREAK
            CASE LFI_BENTONC
                  tlReturn += "53"
                  BREAK
            CASE LFI_STERLINL
                  tlReturn += "54"
                  BREAK
            CASE LFI_SARAP
                  tlReturn += "55"
                  BREAK
            CASE LFI_DARNELLS
                  tlReturn += "56"
                  BREAK
            CASE LFI_PAULO
                  tlReturn += "57"
                  BREAK
            CASE LFI_KATEM
                  tlReturn += "58"
                  BREAK
            CASE LFI_JOSHUAW
                  tlReturn += "59"
                  BREAK
            CASE LFI_VINCEH
                  tlReturn += "60"
                  BREAK
            CASE LFI_TODDR
                  tlReturn += "61"
                  BREAK
            CASE LFI_STEVEW
                  tlReturn += "62"
                  BREAK
            CASE LFI_MIKEH
                  tlReturn += "63"
                  BREAK
            CASE LFI_HARVEYP
                  tlReturn += "64"
                  BREAK
            CASE LFI_ANDYW
                  tlReturn += "65"
                  BREAK
            CASE LFI_TAYLOTH
                  tlReturn += "66"
                  BREAK
            CASE LFI_DONNAHK
                  tlReturn += "67"
                  BREAK
            CASE LFI_LANCEW
                  tlReturn += "68"
                  BREAK
            CASE LFI_PHILG
                  tlReturn += "69"
                  BREAK
            CASE LFI_JAMIEE
                  tlReturn += "70"
                  BREAK
            CASE LFI_JONH
                  tlReturn += "71"
                  BREAK
            CASE LFI_GERRYC
                  tlReturn += "72"
                  BREAK
            CASE LFI_AARONF
                  tlReturn += "73"
                  BREAK
            CASE LFI_CONNORS
                  tlReturn += "74"
                  BREAK
            CASE LFI_POPPAL
                  tlReturn += "75"
                  BREAK
            CASE LFI_HAILEYW
                  tlReturn += "76"
                  BREAK
            CASE LFI_SAMW
                  tlReturn += "77"
                  BREAK
            CASE LFI_GARYH
                  tlReturn += "78"
                  BREAK
            CASE LFI_HANKS
                  tlReturn += "79"
                  BREAK
            
            CASE LFI_PRDE
                  tlReturn += "174"
                  BREAK
            CASE LFI_GRTR
                  tlReturn += "175"
                  BREAK
            CASE LFI_VANG
                  tlReturn += "176"
                  BREAK
            CASE LFI_STST
                  tlReturn += "177"
                  BREAK
            CASE LFI_BWSQ
                  tlReturn += "178"
                  BREAK
            CASE LFI_CCCK
                  tlReturn += "81"
                  BREAK
            CASE LFI_ALCO
                  tlReturn += "82"
                  BREAK
            CASE LFI_MXRE
                  tlReturn += "83"
                  BREAK
            
            // Trevor & friends
            CASE LFI_TREVOR
                  tlReturn += "160"
            BREAK
            CASE LFI_RON
                  tlReturn += "161"
            BREAK
            CASE LFI_WADE
                  tlReturn += "162"
            BREAK
            CASE LFI_LUDENDORFF
                  tlReturn += "163"
            BREAK
                  
                  CASE LFI_ASHLEY
                  tlReturn += "86"
                  BREAK
                  CASE LFI_CLETUS
                  tlReturn += "87"
                  BREAK
                  CASE LFI_JENNY
                  tlReturn += "88"
                  BREAK
                  CASE LFI_FLOYD
                  tlReturn += "89"
                  BREAK
                  CASE LFI_HANK
                  tlReturn += "90"
                  BREAK
                  
				  CASE LFI_AMMU
                  tlReturn += "181"
                  BREAK
				  
                  CASE LFI_FATALINCURSIONS
                  tlReturn += "180"
                  BREAK
                  CASE LFI_BCR
                  tlReturn += "179"
                  BREAK
            
            // Special cases
            CASE LFI_NOFAM
                  tlReturn += "80"
            BREAK
            
            DEFAULT
                  tlReturn = "BLANK"
            BREAK
      ENDSWITCH
      
      RETURN tlReturn

ENDFUNC


PROC POST_MESSAGE_ID(SCALEFORM_INDEX pagemov, INT iSlot, INT iMessage)

	text_label tlName
	text_label tlMessage
	text_label tlSmallPhoto

	BOOL bCanMessageBeClicked
	IF iCurrentLFID = ENUM_TO_INT(GET_CHARACTER_FOR_MESSAGE(iCurrentLFID, iMessage))
		// If the same character whose profile we're looking at posted the message, don't let the player click it
		bCanMessageBeClicked = FALSE
	ELSE
		// Otherwise, a different character posted it, so let the player click the message to go to that profile
		bCanMessageBeClicked = TRUE
	ENDIF
	
	tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_CHARACTER_FOR_MESSAGE(iCurrentLFID, iMessage)))
	tlMessage = GET_MESSAGE_FOR_MESSAGE_ID(GET_CHARACTER_MESSAGE_THREAD_NUMBER(iCurrentLFID), iMessage)
	tlSmallPhoto = GET_SMALL_PORTRAIT(GET_CHARACTER_FOR_MESSAGE(iCurrentLFID, iMessage))
	
	CharWallPostID[iSlot] = GET_CHARACTER_FOR_MESSAGE(iCurrentLFID, iMessage)
	// Comment back in for debug
	//CPRINTLN(DEBUG_INTERNET, "Storing ", tlName ," in slot ", iSlot)
	
	IF iCurrentLFID = ENUM_TO_INT(LFI_MARYANNQUINN)
		CPRINTLN(DEBUG_INTERNET, "Printing Mary Ann message ", tlMessage)
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) // Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LFI_POSTEDUPDATE") // Status text - always the same?
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlMessage) // Message text
		//This is player name on purpose because _string checks the string table, can't find it and breaks it...
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlSmallPhoto) // portrait txd
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanMessageBeClicked) // Can message be clicked
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

FUNC INT GET_STARTING_MESSAGE_NUMBER_FOR_INITIAL_MESSAGES(LFICharList LFI_ID)

	SWITCH LFI_ID
		CASE LFI_MICHAEL
			RETURN 50 // #50 (LFI_M1_50) is the first initial message, and so on for every char
		BREAK
		CASE LFI_TDESANTA
			RETURN 27
		BREAK
		CASE LFI_JDESANTA
			RETURN 33
		BREAK
		CASE LFI_ADESANTA
			RETURN 25
		BREAK
		CASE LFI_LESTERCREST
			RETURN 12
		BREAK
		CASE LFI_MARYANNQUINN
			RETURN 7
		BREAK
		CASE LFI_HAYDENDUBOSE
			RETURN 8
		BREAK
		CASE LFI_KYLECHAVIS
			RETURN 4
		BREAK
		CASE LFI_REDWOOD
			RETURN 9
		BREAK
		
		CASE LFI_FRANKLIN
			RETURN 43 // #43 (LFI_F1_43) is the first initial message, and so on for every char
		BREAK
		CASE LFI_LAMAR
			RETURN 20
		BREAK
		CASE LFI_SIMEON
			RETURN 15
		BREAK
		CASE LFI_TONYA
			RETURN 19
		BREAK
		CASE LFI_DENISE
			RETURN 15
		BREAK
		CASE LFI_STRETCH
			RETURN 15
		BREAK
		CASE LFI_TANISHA
			RETURN 14
		BREAK
		CASE LFI_TAVELL
			RETURN 10
		BREAK
		CASE LFI_DOM
			RETURN -1 // Has no messages when starting
		BREAK
		CASE LFI_BEV
			RETURN -1 // Has no messages when starting
		BREAK
		CASE LFI_DEVIN
			RETURN -1 // Has no messages when starting
		BREAK
		CASE LFI_DEMARCUS
			RETURN 10
		BREAK
		CASE LFI_FEUD
			RETURN 10
		BREAK
		CASE LFI_SPRUNK
			RETURN 5
		BREAK
		CASE LFI_INKINC
			RETURN 7
		BREAK
		CASE LFI_HERRKUTZ
			RETURN 10
		BREAK
		CASE LFI_LSCUSTOMS
			RETURN 9
		BREAK
		
		CASE LFI_TREVOR
			RETURN 6
		BREAK
		CASE LFI_RON
			RETURN 2
		BREAK
		CASE LFI_WADE
			RETURN 7
		BREAK
		CASE LFI_LUDENDORFF
			RETURN 3
		BREAK
	ENDSWITCH
	
	RETURN 1

ENDFUNC

/// PURPOSE:
///   See GET_STARTING_MESSAGE_NUMBER_FOR_STORY_MISSIONID below - same thing, just with RC missions  
FUNC INT GET_STARTING_MESSAGE_NUMBER_FOR_RC_MISSIONID(LFICharList LFI_ID, INT iMissionID)

	SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
		// Michael & friends
		CASE LFI_MICHAEL
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_BARRY_1
					RETURN 46
				BREAK
				CASE RC_EPSILON_7
					RETURN 47
				BREAK
				CASE RC_FANATIC_2
					RETURN 48
				BREAK
				CASE RC_FANATIC_1
					RETURN 49
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_JDESANTA
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_PAPARAZZO_2
					RETURN 32
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_MARYANNQUINN
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_FANATIC_3
					RETURN 1
				BREAK
				CASE RC_FANATIC_2
					RETURN 3
				BREAK
				CASE RC_FANATIC_1
					RETURN 5
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_HAYDENDUBOSE
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_JOSH_2
					RETURN 6
				BREAK
			ENDSWITCH
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_PAPARAZZO_2
					RETURN 32
				BREAK
				CASE RC_PAPARAZZO_3A
					RETURN 30
				BREAK
				CASE RC_EXTREME_2
					RETURN 28
				BREAK
				CASE RC_EXTREME_3
					RETURN 26
				BREAK
				CASE RC_TONYA_2
					RETURN 24
				BREAK
				CASE RC_TONYA_4
					RETURN 22
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TONYA
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_TONYA_5
					RETURN 1
				BREAK
				CASE RC_TONYA_2
					RETURN 4
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DOM
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_EXTREME_4
					RETURN 1
				BREAK
				CASE RC_EXTREME_3
					RETURN 3
				BREAK
				CASE RC_EXTREME_2
					RETURN 6
				BREAK
				CASE RC_EXTREME_1
					RETURN 9
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_BEV
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_PAPARAZZO_3B
					RETURN 1
				BREAK
				CASE RC_PAPARAZZO_3A
					RETURN 3
				BREAK
				CASE RC_PAPARAZZO_2
					RETURN 4
				BREAK
				CASE RC_PAPARAZZO_1
					RETURN 6
				BREAK
			ENDSWITCH
		BREAK
		
		// Trevor & friends
		CASE LFI_TREVOR
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_MRS_PHILIPS_1
					RETURN 5
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1

ENDFUNC

/// PURPOSE:
///    This is getting the first message number for the string of messages that unlock when a given mission is completed
///    e.g. Franklin 1 should unlock these text keys on Franklin's message page: LFI_F1_11, LFI_F1_12, LFI_F1_13, LFI_F1_14
///    So given Franklin 1's mission ID and Franklin's LFI char ID, this will return 11 ^
FUNC INT GET_STARTING_MESSAGE_NUMBER_FOR_STORY_MISSIONID(LFICharList LFI_ID, INT iMissionID)

	SWITCH LFI_ID
		// Michael and Friends
		CASE LFI_MICHAEL
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_HEIST_FINALE_2A
				CASE SP_HEIST_FINALE_2B
					RETURN 1
				BREAK
				CASE SP_HEIST_FINALE_2_INTRO
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 5
				BREAK
				CASE SP_MISSION_ME_TRACEY
					RETURN 7
				BREAK
				CASE SP_MISSION_ME_JIMMY
					RETURN 8
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 9
				BREAK
				CASE SP_HEIST_AGENCY_1
					RETURN 12
				BREAK
				CASE SP_MISSION_FBI_5
					RETURN 15
				BREAK
				CASE SP_MISSION_CARSTEAL_1
					RETURN 18
				BREAK
				CASE SP_HEIST_RURAL_1
					RETURN 22
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 23
				BREAK
				CASE SP_MISSION_EXILE_3
					RETURN 24
				BREAK
				CASE SP_MISSION_EXILE_2
					RETURN 25
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 26
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 27
				BREAK
				CASE SP_HEIST_DOCKS_2A
				CASE SP_HEIST_DOCKS_2B
					RETURN 28
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 29
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 31
				BREAK
				CASE SP_HEIST_JEWELRY_2
					RETURN 34
				BREAK
				CASE SP_MISSION_ME_AMANDA
					RETURN 35
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 36
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 39
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 42
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 44
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 45
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TDESANTA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FRANKLIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 4
				BREAK
				CASE SP_MISSION_ME_TRACEY
					RETURN 5
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 6
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 8
				BREAK
				CASE SP_MISSION_SOLOMON_1
					RETURN 9
				BREAK
				CASE SP_MISSION_FBI_5
					RETURN 10
				BREAK
				CASE SP_MISSION_EXILE_2
					RETURN 11
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 12
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 13
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 14
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 15
				BREAK
				CASE SP_HEIST_JEWELRY_2
					RETURN 16
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 17
				BREAK
				CASE SP_MISSION_TREVOR_2
					RETURN 18
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 19
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 23
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 25
				BREAK
				CASE SP_MISSION_ARMENIAN_2
					RETURN 26
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_JDESANTA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 5
				BREAK
				CASE SP_MISSION_ME_JIMMY
					RETURN 7
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 8
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 10
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 11
				BREAK
				CASE SP_MISSION_EXILE_3
					RETURN 12
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 13
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 14
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 15
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 17
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 21
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 22
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 23
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 25
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 27
				BREAK
				CASE SP_MISSION_ARMENIAN_1
					RETURN 30
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_ADESANTA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 3
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 5
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 6
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 8
				BREAK
				CASE SP_MISSION_EXILE_2
					RETURN 9
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 10
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 11
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 12
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 14
				BREAK
				CASE SP_MISSION_ME_AMANDA
					RETURN 16
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 17
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 18
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 20
				BREAK
				CASE SP_HEIST_JEWELRY_2
					RETURN 21
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 22
				BREAK
				CASE SP_MISSION_ARMENIAN_1
					RETURN 23
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 24
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LESTERCREST
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_HEIST_FINALE_2A
				CASE SP_HEIST_FINALE_2B
					RETURN 1
				BREAK
				CASE SP_HEIST_AGENCY_3A
				CASE SP_HEIST_AGENCY_3B
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 3
				BREAK
				CASE SP_HEIST_RURAL_PREP_1
					RETURN 4
				BREAK
				CASE SP_HEIST_RURAL_1
					RETURN 5
				BREAK
				CASE SP_HEIST_DOCKS_2A
				CASE SP_HEIST_DOCKS_2B
					RETURN 6
				BREAK
				CASE SP_HEIST_DOCKS_1
					RETURN 8
				BREAK
				CASE SP_MISSION_ASSASSIN_3
					RETURN 9
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 11
				BREAK
			ENDSWITCH
		BREAK
		// No story mission triggers for LFI_MARYANNQUINN
		CASE LFI_HAYDENDUBOSE
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_HEIST_FINALE_2A
				CASE SP_HEIST_FINALE_2B
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 3
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 4
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 5
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_KYLECHAVIS
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FAMILY_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 2
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_REDWOOD
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ASSASSIN_2
					RETURN 1
				BREAK
				CASE SP_MISSION_ASSASSIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 5
				BREAK
				CASE SP_MISSION_EXILE_3
					RETURN 6
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 7
				BREAK
				CASE SP_MISSION_TREVOR_1
					RETURN 8
				BREAK
			ENDSWITCH
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ARMENIAN_1
					RETURN 42
				BREAK
				CASE SP_MISSION_ARMENIAN_2
					RETURN 38
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 36
				BREAK
				CASE SP_MISSION_ASSASSIN_2
					RETURN 34
				BREAK
				CASE SP_MISSION_FRANKLIN_0
					RETURN 19
				BREAK
				CASE SP_MISSION_LAMAR
					RETURN 15
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 11
				BREAK
				CASE SP_MISSION_CARSTEAL_1
					RETURN 9
				BREAK
				CASE SP_MISSION_CARSTEAL_2
					RETURN 6
				BREAK
				CASE SP_MISSION_CARSTEAL_3
					RETURN 4
				BREAK
				CASE SP_MISSION_CARSTEAL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LAMAR
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ARMENIAN_2
					RETURN 16
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 12
				BREAK
				CASE SP_MISSION_FRANKLIN_0
					RETURN 9
				BREAK
				CASE SP_MISSION_LAMAR
					RETURN 6
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 4
				BREAK
				CASE SP_MISSION_CARSTEAL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_SIMEON
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ARMENIAN_3
					RETURN 12
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 9
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 6
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TONYA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 7
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 10
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 13
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 16
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DENISE
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 6
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 7
				BREAK
				CASE SP_MISSION_ASSASSIN_1
					RETURN 10
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 13
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_STRETCH
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 5
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 8
				BREAK
				CASE SP_MISSION_LAMAR
					RETURN 11
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TANISHA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 5
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 9
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TAVELL
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_ASSASSIN_2
					RETURN 4
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 7
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DEVIN
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_SOLOMON_3
					RETURN 2
				BREAK
				CASE SP_MISSION_CARSTEAL_3
					RETURN 4
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 7
				BREAK
				CASE SP_MISSION_CARSTEAL_1
					RETURN 10
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DEMARCUS
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 4
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 7
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_FEUD
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 5
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 7
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_SPRUNK
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 4
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_INKINC
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 2
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 4
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 6
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_HERRKUTZ
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 4
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 7
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 10
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LSCUSTOMS
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 5
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 7
				BREAK
			ENDSWITCH
		BREAK
		
		// Trevor and Pals
		CASE LFI_TREVOR
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_EXILE_3
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 2
				BREAK
				CASE SP_MISSION_TREVOR_2
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_RON
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_TREVOR_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_WADE
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_4
					RETURN 2
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 3
				BREAK
				CASE SP_HEIST_DOCKS_1
					RETURN 4
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 5
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LUDENDORFF
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1

ENDFUNC

FUNC INT GET_NUMBER_OF_MESSAGES_FOR_RC_MISSIONID(LFICharList LFI_ID, INT iMissionID)

	SWITCH LFI_ID
		// Michael & friends
		CASE LFI_MICHAEL
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_BARRY_1
					RETURN 1
				BREAK
				CASE RC_EPSILON_7
					RETURN 1
				BREAK
				CASE RC_FANATIC_2
					RETURN 1
				BREAK
				CASE RC_FANATIC_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_JDESANTA
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_PAPARAZZO_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_MARYANNQUINN
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_FANATIC_3
					RETURN 2
				BREAK
				CASE RC_FANATIC_2
					RETURN 2
				BREAK
				CASE RC_FANATIC_1
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_HAYDENDUBOSE
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_JOSH_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_PAPARAZZO_2
					RETURN 2
				BREAK
				CASE RC_PAPARAZZO_3A
					RETURN 2
				BREAK
				CASE RC_EXTREME_2
					RETURN 2
				BREAK
				CASE RC_EXTREME_3
					RETURN 2
				BREAK
				CASE RC_TONYA_2
					RETURN 2
				BREAK
				CASE RC_TONYA_4
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TONYA
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_TONYA_5
					RETURN 3
				BREAK
				CASE RC_TONYA_2
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DOM
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_EXTREME_4
					RETURN 2
				BREAK
				CASE RC_EXTREME_3
					RETURN 3
				BREAK
				CASE RC_EXTREME_2
					RETURN 3
				BREAK
				CASE RC_EXTREME_1
					RETURN 29
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_BEV
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_PAPARAZZO_3B
					RETURN 2
				BREAK
				CASE RC_PAPARAZZO_3A
					RETURN 1
				BREAK
				CASE RC_PAPARAZZO_2
					RETURN 2
				BREAK
				CASE RC_PAPARAZZO_1
					RETURN 18
				BREAK
			ENDSWITCH
		BREAK
		
		// Trevor & friends
		CASE LFI_TREVOR
			SWITCH INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
				CASE RC_MRS_PHILIPS_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1

ENDFUNC

FUNC INT GET_NUMBER_OF_MESSAGES_FOR_STORY_MISSIONID(LFICharList LFI_ID, INT iMissionID)

	//CPRINTLN(DEBUG_INTERNET, "Looking for story iMissionID ", iMissionID, " in GET_NUMBER_OF_MESSAGES_FOR_STORY_MISSIONID for character ", LFI_ID)

	SWITCH LFI_ID
		// Michael and Friends
		CASE LFI_MICHAEL
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_HEIST_FINALE_2_INTRO
					RETURN 1
				BREAK
				CASE SP_HEIST_FINALE_2A
				CASE SP_HEIST_FINALE_2B
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_ME_TRACEY
					RETURN 1
				BREAK
				CASE SP_MISSION_ME_JIMMY
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 3
				BREAK
				CASE SP_HEIST_AGENCY_1
					RETURN 3
				BREAK
				CASE SP_MISSION_FBI_5
					RETURN 3
				BREAK
				CASE SP_MISSION_CARSTEAL_1
					RETURN 4
				BREAK
				CASE SP_HEIST_RURAL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_3
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_2
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 1
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 1
				BREAK
				CASE SP_HEIST_DOCKS_2A
				CASE SP_HEIST_DOCKS_2B
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 3
				BREAK
				CASE SP_HEIST_JEWELRY_2
					RETURN 1
				BREAK
				CASE SP_MISSION_ME_AMANDA
					RETURN 1
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 3
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 3
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 1
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TDESANTA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FRANKLIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_ME_TRACEY
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 1
				BREAK
				CASE SP_MISSION_SOLOMON_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FBI_5
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_2
					RETURN 1
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 1
				BREAK
				CASE SP_HEIST_JEWELRY_2
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_2
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 4
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 2
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 1
				BREAK
				CASE SP_MISSION_ARMENIAN_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_JDESANTA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_ME_JIMMY
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_3
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 4
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 1
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 2
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 3
				BREAK
				CASE SP_MISSION_ARMENIAN_1
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_ADESANTA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_6
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_2
					RETURN 1
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_4
					RETURN 2
				BREAK
				CASE SP_MISSION_ME_AMANDA
					RETURN 1
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 2
				BREAK
				CASE SP_MISSION_FAMILY_2
					RETURN 1
				BREAK
				CASE SP_HEIST_JEWELRY_2
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_1
					RETURN 1
				BREAK
				CASE SP_MISSION_ARMENIAN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LESTERCREST
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_HEIST_FINALE_2A
				CASE SP_HEIST_FINALE_2B
					RETURN 1
				BREAK
				CASE SP_HEIST_AGENCY_3A
				CASE SP_HEIST_AGENCY_3B
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_2
					RETURN 1
				BREAK
				CASE SP_HEIST_RURAL_PREP_1
					RETURN 1
				BREAK
				CASE SP_HEIST_RURAL_1
					RETURN 1
				BREAK
				CASE SP_HEIST_DOCKS_2A
				CASE SP_HEIST_DOCKS_2B
					RETURN 2
				BREAK
				CASE SP_HEIST_DOCKS_1
					RETURN 1
				BREAK
				CASE SP_MISSION_ASSASSIN_3
					RETURN 2
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		// No story mission triggers for LFI_MARYANNQUINN
		CASE LFI_HAYDENDUBOSE
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_HEIST_FINALE_2A
				CASE SP_HEIST_FINALE_2B
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_5
					RETURN 1
				BREAK
				CASE SP_MISSION_LESTER_1
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_KYLECHAVIS
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FAMILY_4
					RETURN 1
				BREAK
				CASE SP_MISSION_FAMILY_3
					RETURN 1
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_REDWOOD
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ASSASSIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_ASSASSIN_1
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_3
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		
		CASE LFI_FRANKLIN
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ARMENIAN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_ARMENIAN_2
					RETURN 4
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 2
				BREAK
				CASE SP_MISSION_ASSASSIN_2
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_0
					RETURN 3
				BREAK
				CASE SP_MISSION_LAMAR
					RETURN 4
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 4
				BREAK
				CASE SP_MISSION_CARSTEAL_1
					RETURN 2
				BREAK
				CASE SP_MISSION_CARSTEAL_2
					RETURN 3
				BREAK
				CASE SP_MISSION_CARSTEAL_3
					RETURN 2
				BREAK
				CASE SP_MISSION_CARSTEAL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LAMAR
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ARMENIAN_2
					RETURN 4
				BREAK
				CASE SP_MISSION_ARMENIAN_3
					RETURN 4
				BREAK
				CASE SP_MISSION_FRANKLIN_0
					RETURN 3
				BREAK
				CASE SP_MISSION_LAMAR
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 2
				BREAK
				CASE SP_MISSION_CARSTEAL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_FRANKLIN_2
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_SIMEON
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_ARMENIAN_3
					RETURN 3
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TONYA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 3
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DENISE
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 3
				BREAK
				CASE SP_MISSION_ASSASSIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_STRETCH
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FRANKLIN_2
					RETURN 4
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 3
				BREAK
				CASE SP_MISSION_LAMAR
					RETURN 4
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TANISHA
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_FRANKLIN_2
					RETURN 4
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 4
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 5
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TAVELL
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_ASSASSIN_2
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DEVIN
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_SOLOMON_3
					RETURN 2
				BREAK
				CASE SP_MISSION_CARSTEAL_3
					RETURN 3
				BREAK
				CASE SP_MISSION_FBI_4
					RETURN 3
				BREAK
				CASE SP_MISSION_CARSTEAL_1
					RETURN 25
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DEMARCUS
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 3
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_FRANKLIN_1
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_FEUD
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 2
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 2
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_SPRUNK
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 2
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_INKINC
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 1
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 2
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 2
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_HERRKUTZ
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 3
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 3
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 3
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 3
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LSCUSTOMS
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_4
					RETURN 2
				BREAK
				CASE SP_MISSION_MICHAEL_1
					RETURN 2
				BREAK
				CASE SP_MISSION_MARTIN_1
					RETURN 2
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		
		// Trevor and Pals
		CASE LFI_TREVOR
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_EXILE_3
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_2
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_RON
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_TREVOR_1
					RETURN 1
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_WADE
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_4
					RETURN 1
				BREAK
				CASE SP_MISSION_EXILE_1
					RETURN 1
				BREAK
				CASE SP_HEIST_DOCKS_1
					RETURN 1
				BREAK
				CASE SP_MISSION_TREVOR_3
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LUDENDORFF
			SWITCH INT_TO_ENUM(SP_MISSIONS, iMissionID)
				CASE SP_MISSION_MICHAEL_1
					RETURN 2
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0

ENDFUNC

FUNC INT GET_RC_MISSIONID_FOR_COMPLETION(INT iMissionCompletionID)

	INT i
	FOR i = 0 TO (ENUM_TO_INT(MAX_RC_MISSIONS)-1)
		IF g_savedGlobals.sRandomChars.savedRC[i].iCompletionOrder = iMissionCompletionID
			//CPRINTLN(DEBUG_INTERNET, "Got an RC mission for ", iMissionCompletionID, ", it is ", i)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1

ENDFUNC

FUNC INT GET_STORY_MISSIONID_FOR_COMPLETION(INT iMissionCompletionID)

	INT i
	FOR i = 0 TO (ENUM_TO_INT(SP_MISSION_MAX)-1)
		IF g_savedGlobals.sFlow.missionSavedData[i].iCompletionOrder = iMissionCompletionID
			//CPRINTLN(DEBUG_INTERNET, "Got a story mission for ", iMissionCompletionID, ", it is ", i)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1

ENDFUNC


PROC SET_POSTED_MESSAGES(SCALEFORM_INDEX pagemov)
	
	INT iSlot = 5
	INT iMessage = -1
	
	INT iTotalMissionCompleted = GetTotalCompletionOrder()
	CPRINTLN(DEBUG_INTERNET, "iTotalMissionCompleted = GetTotalCompletionOrder() is ", iTotalMissionCompleted)
	
	WHILE iTotalMissionCompleted > 0
	
		INT iMissionID = -1
		BOOL bIsStoryMission = TRUE
		
		// Try to get the story mission ID for this completed mission
		iMissionID = GET_STORY_MISSIONID_FOR_COMPLETION(iTotalMissionCompleted)
		
		// If this still = -1, the mission completion ID we just checked wasn't a story mission or doesn't unlock any messages, so check RC missions
		IF iMissionID = -1
			// Try to get the RC mission ID for this completed mission
			iMissionID = GET_RC_MISSIONID_FOR_COMPLETION(iTotalMissionCompleted)
			bIsStoryMission = FALSE
		ENDIF
		
		IF iMissionID != -1 // If this is still -1, the mission doesn't unlock any messages, so don't print anything
		AND bIsStoryMission = TRUE
			INT i
			iMessage = GET_STARTING_MESSAGE_NUMBER_FOR_STORY_MISSIONID(INT_TO_ENUM(LFICharList, iCurrentLFID), iMissionID)
			
			IF iMessage != -1 // If this is -1, there were no messages for that mission ID, so this shouldn't try to print any
				CPRINTLN(DEBUG_INTERNET, "Trying to print story mission messages for iMissionID ", iMissionID)
				FOR i = 1 TO GET_NUMBER_OF_MESSAGES_FOR_STORY_MISSIONID(INT_TO_ENUM(LFICharList, iCurrentLFID), iMissionID)
					POST_MESSAGE_ID(pagemov, iSlot, iMessage)
					iMessage++
					iSlot++
				ENDFOR
			ENDIF
			
		ENDIF
		
		IF iMissionID != -1 // If this is still -1, the mission doesn't unlock any messages, so don't print anything
		AND bIsStoryMission = FALSE
			
			BOOL bDoMessage = TRUE
			
			// SPECIAL CASE TIME!
			// If this is Michael's message page, and we're trying to print Mary Ann's message for Fanatic 2, only do it if Fanatic 1 was completed before Fanatic 2
			// Otherwise it doesn't make sense!
			IF iCurrentLFID = ENUM_TO_INT(LFI_MICHAEL)
				IF iMissionID = ENUM_TO_INT(RC_FANATIC_2)
					// If Fanatic 1 hasn't been completed...
					IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_FANATIC_1), CP_GROUP_RANDOMCHARS) 
					// Or if Fanatic 2 was completed before Fanatic 1
					OR g_savedGlobals.sRandomChars.savedRC[RC_FANATIC_2].iCompletionOrder < g_savedGlobals.sRandomChars.savedRC[RC_FANATIC_1].iCompletionOrder
						bDoMessage = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoMessage = TRUE // Every time except the one above this will still be true
				INT i
				iMessage = GET_STARTING_MESSAGE_NUMBER_FOR_RC_MISSIONID(INT_TO_ENUM(LFICharList, iCurrentLFID), iMissionID)
				
				IF iMessage != -1 // If this is -1, there were no messages for that mission ID, so this shouldn't try to print any
					CPRINTLN(DEBUG_INTERNET, "Trying to print RC mission messages for iMissionID ", iMissionID)
					FOR i = 1 TO GET_NUMBER_OF_MESSAGES_FOR_RC_MISSIONID(INT_TO_ENUM(LFICharList, iCurrentLFID), iMissionID)
						
						BOOL bPostMessage = TRUE
						
						// Another special case...
						// If this is Mary Ann...
						IF iCurrentLFID = ENUM_TO_INT(LFI_MARYANNQUINN)
	
							// If we're printing her post-Fanatic 1 messages, don't post the second message if Fanatic 2 was completed first
							IF iMissionID = ENUM_TO_INT(RC_FANATIC_1)
								IF iMessage = 6
									IF HasMissionBeenCompleted(ENUM_TO_INT(RC_FANATIC_2), CP_GROUP_RANDOMCHARS) 
									AND g_savedGlobals.sRandomChars.savedRC[RC_FANATIC_2].iCompletionOrder < g_savedGlobals.sRandomChars.savedRC[RC_FANATIC_1].iCompletionOrder
										// Fanatic 2 was completed first - don't do Mary Ann's message about "meeting Adam up at Galileo Park near the Vinewood sign"
										bPostMessage = FALSE
									ENDIF
								ENDIF
							ENDIF
							// If we're printing her post-Fanatic 2 messages, don't post the second message if Fanatic 3 was completed first
							IF iMissionID = ENUM_TO_INT(RC_FANATIC_2)
								IF iMessage = 3
									IF HasMissionBeenCompleted(ENUM_TO_INT(RC_FANATIC_3), CP_GROUP_RANDOMCHARS) 
									AND g_savedGlobals.sRandomChars.savedRC[RC_FANATIC_3].iCompletionOrder < g_savedGlobals.sRandomChars.savedRC[RC_FANATIC_2].iCompletionOrder
										// Fanatic 3 was completed first - don't do Mary Ann's message about "a triathlon trial run"
										bPostMessage = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF bPostMessage = TRUE
							POST_MESSAGE_ID(pagemov, iSlot, iMessage)
							iSlot++
						ENDIF
						iMessage++
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		// However, if we're checking the 1st mission completed, it's the Prologue - in which case, now start printing all of the pre-game-start messages
		IF iTotalMissionCompleted = 1
			INT i
			iMessage = GET_STARTING_MESSAGE_NUMBER_FOR_INITIAL_MESSAGES(INT_TO_ENUM(LFICharList, iCurrentLFID))
		
			IF iMessage != -1 // Characters that don't have any messages that unlock at the start of the game will return -1 - don't do anything for these chars here
				FOR i = GET_STARTING_MESSAGE_NUMBER_FOR_INITIAL_MESSAGES(INT_TO_ENUM(LFICharList, iCurrentLFID)) TO GET_MAX_MESSAGES_FOR_ID(iCurrentLFID)
					POST_MESSAGE_ID(pagemov, iSlot, iMessage)
					iMessage++
					iSlot++
				ENDFOR
			ENDIF
		ENDIF
		
		// ANOTHER SPECIAL CASE TIME
		// If this is Hayden Dubose, we want to display his special RE Domestic unlock message if a) it's been completed 
		// and b) if it's been completed during when this current story/RC mission was completed
		IF iCurrentLFID = ENUM_TO_INT(LFI_HAYDENDUBOSE)
			IF g_savedGlobals.sRandomChars.g_iREDomesticCompOrder != -1 // If this doesn't = -1, RE Domestic been completed
				IF iTotalMissionCompleted = g_savedGlobals.sRandomChars.g_iREDomesticCompOrder
					POST_MESSAGE_ID(pagemov, iSlot, 7)
					iSlot++
				ENDIF
			ENDIF
		ENDIF
		
		iMessage = -1 // Reset for safety
		iTotalMissionCompleted--
		//WAIT(0)
	ENDWHILE
	
//	FOR iMessage = 1 TO GET_MAX_MESSAGES_FOR_ID(iCurrentLFID)
//	
//		BOOL bDoPost = TRUE
//		
//		
//		
//		// TEST TEST TEST
//		IF iCurrentLFID = ENUM_TO_INT(LFI_FRANKLIN)
//			if iMessage < 43
//				bDoPost = FALSE
//			ENDIF
//		ENDIF
//		// TEST TEST TEST
//		
//		IF bDoPost = TRUE
//			POST_MESSAGE_ID(pagemov, iSlot, iMessage)
//		ENDIF
//		iSlot++
//		
//	ENDFOR
	
	iLastSlot = iSlot // Stick the last slot we were using in... the last slot var
	CPRINTLN(DEBUG_INTERNET, "Sidebar will begin being generated from slot #", iLastSlot)

ENDPROC

// ---------------------------------------
// HANDLES FRIENDS PAGE FOR A PROFILE
// ---------------------------------------

FUNC INT GET_MAX_FRIENDS_FOR_ID(INT LFI_ID)

      SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
            CASE LFI_MICHAEL
				RETURN 7
            BREAK
            CASE LFI_TDESANTA
				RETURN 13
			BREAK
            CASE LFI_JDESANTA
				RETURN 11
			BREAK
            CASE LFI_ADESANTA
				RETURN 13
			BREAK
            CASE LFI_LESTERCREST
				RETURN 8
			BREAK
            CASE LFI_MARYANNQUINN
				RETURN 6
			BREAK
            CASE LFI_HAYDENDUBOSE
				RETURN 6
			BREAK
            CASE LFI_KYLECHAVIS
				RETURN 11
			BREAK
            CASE LFI_REDWOOD
				RETURN 6
			BREAK
            CASE LFI_FRANKLIN
				RETURN 17
            BREAK
            CASE LFI_LAMAR
				RETURN 15
            BREAK
            CASE LFI_SIMEON
				RETURN 8
            BREAK
            CASE LFI_TONYA
				RETURN 12
            BREAK
            CASE LFI_DENISE
				RETURN 9
            BREAK
            CASE LFI_STRETCH
				RETURN 10
            BREAK
            CASE LFI_TANISHA
				RETURN 14
            BREAK
            CASE LFI_TAVELL
				RETURN 8
            BREAK
            CASE LFI_DOM
				RETURN 6
            BREAK
            CASE LFI_BEV
				RETURN 5
            BREAK
            CASE LFI_DEVIN
				RETURN 5
            BREAK
            CASE LFI_DEMARCUS
				RETURN 11
            BREAK
            CASE LFI_FEUD
				RETURN 25
            BREAK
            CASE LFI_SPRUNK
				RETURN 8
            BREAK
            CASE LFI_INKINC
				RETURN 10
            BREAK
            CASE LFI_HERRKUTZ
				RETURN 10
            BREAK
            CASE LFI_LSCUSTOMS
				RETURN 8
            BREAK
            CASE LFI_TREVOR
				RETURN 2
			BREAK
            CASE LFI_RON
				RETURN 5
			BREAK
            CASE LFI_WADE
				RETURN 6
			BREAK
            CASE LFI_LUDENDORFF
				RETURN 2
            BREAK
      ENDSWITCH
      
      RETURN 0

ENDFUNC


FUNC LFICharList GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(LFICharList LFICurrentChar, int iSlot)

	SWITCH LFICurrentChar
		// Michael & friends
            CASE LFI_MICHAEL
                  SWITCH iSlot
                        CASE 1
							RETURN LFI_TDESANTA
                        BREAK
                        CASE 2
                            RETURN LFI_JDESANTA
                        BREAK
                        CASE 3
                           	RETURN LFI_ADESANTA
                        BREAK
                        CASE 4
                            RETURN LFI_LESTERCREST
                        BREAK
                        CASE 5
                            RETURN LFI_MARYANNQUINN
                        BREAK
                        CASE 6
                            RETURN LFI_HAYDENDUBOSE
                        BREAK
                        CASE 7
                            RETURN LFI_KYLECHAVIS
                        BREAK
                  ENDSWITCH
            BREAK
            
            CASE LFI_TDESANTA
                  SWITCH iSlot
                        CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_JDESANTA
                        BREAK
                        CASE 3
                           	RETURN LFI_ADESANTA
                        BREAK
                        CASE 4
                            RETURN LFI_FREDDY
                        BREAK
                        CASE 5
                            RETURN LFI_RALPH
                        BREAK
                        CASE 6
                            RETURN LFI_LAUREN
                        BREAK
                        CASE 7
                            RETURN LFI_FOSTER
                        BREAK
                        CASE 8
                            RETURN LFI_BREE
                        BREAK
                        CASE 9
                            RETURN LFI_KIM
                        BREAK
                        CASE 10
                            RETURN LFI_LISAWALLIN
                        BREAK
                        CASE 11
                            RETURN LFI_IAN
                        BREAK
                        CASE 12
                            RETURN LFI_GARYSCALES
                        BREAK
                        CASE 13
                            RETURN LFI_MFRIENDS432
                        BREAK
                  ENDSWITCH
            BREAK
			
            CASE LFI_JDESANTA
                  SWITCH iSlot
                        CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_TDESANTA
                        BREAK
                        CASE 3
                           	RETURN LFI_ADESANTA
                        BREAK
                        CASE 4
                            RETURN LFI_AHRON
                        BREAK
                        CASE 5
                            RETURN LFI_RYAN
                        BREAK
                        CASE 6
                            RETURN LFI_COLIN
                        BREAK
                        CASE 7
                            RETURN LFI_EVAN
                        BREAK
                        CASE 8
                            RETURN LFI_ROSS
                        BREAK
                        CASE 9
                            RETURN LFI_JAY
                        BREAK
                        CASE 10
                            RETURN LFI_AMY
                        BREAK
                        CASE 11
                            RETURN LFI_MFRIENDS142
                        BREAK
                  ENDSWITCH
            BREAK
			
            CASE LFI_ADESANTA
                  SWITCH iSlot
                      CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_TDESANTA
                        BREAK
                        CASE 3
                           	RETURN LFI_JDESANTA
                        BREAK
                        CASE 4
                            RETURN LFI_HAYDENDUBOSE
                        BREAK
                        CASE 5
                            RETURN LFI_KYLECHAVIS
                        BREAK
                        CASE 6
                            RETURN LFI_FABIEN
                        BREAK
                        CASE 7
                            RETURN LFI_LISABARCLAY
                        BREAK
                        CASE 8
                            RETURN LFI_DRSTANOVICZ
                        BREAK
                        CASE 9
                            RETURN LFI_KERI
                        BREAK
                        CASE 10
                            RETURN LFI_PETER
                        BREAK
                        CASE 11
                            RETURN LFI_SANDRA
                        BREAK
                        CASE 12
                            RETURN LFI_FIONA
                        BREAK
                        CASE 13
                            RETURN LFI_MFRIENDS205
                        BREAK
                  ENDSWITCH
            BREAK
            
            CASE LFI_LESTERCREST
                  SWITCH iSlot
                      CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_CHERYL
                        BREAK
                        CASE 3
                           	RETURN LFI_GUADALOUPE
                        BREAK
                        CASE 4
                            RETURN LFI_MAURIE
                        BREAK
                        CASE 5
                            RETURN LFI_KANYA
                        BREAK
                        CASE 6
                            RETURN LFI_NILES
                        BREAK
                        CASE 7
                            RETURN LFI_GILES
                        BREAK
                        CASE 8
                            RETURN LFI_MFRIENDS63
                        BREAK
                  ENDSWITCH
            BREAK
			
            CASE LFI_MARYANNQUINN
                  SWITCH iSlot
                      CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_DRWETHERBROOK
                        BREAK
                        CASE 3
                           	RETURN LFI_JONATHAN
                        BREAK
                        CASE 4
                            RETURN LFI_RICHARD
                        BREAK
                        CASE 5
                            RETURN LFI_MOTHER
                        BREAK
                        CASE 6
                            RETURN LFI_MFRIENDS58
                        BREAK
                  ENDSWITCH
            BREAK
			
            CASE LFI_HAYDENDUBOSE
                  SWITCH iSlot
                      CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_ADESANTA
                        BREAK
                        CASE 3
                           	RETURN LFI_KYLECHAVIS
                        BREAK
                        CASE 4
                            RETURN LFI_MELINDA
                        BREAK
                        CASE 5
                            RETURN LFI_JASPER
                        BREAK
                        CASE 6
                            RETURN LFI_MFRIENDS86
                        BREAK
                  ENDSWITCH
            BREAK
			
            CASE LFI_KYLECHAVIS
                  SWITCH iSlot
                      	CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_ADESANTA
                        BREAK
                        CASE 3
                           	RETURN LFI_HAYDENDUBOSE
                        BREAK
                        CASE 4
                            RETURN LFI_REBECCA
                        BREAK
                        CASE 5
                            RETURN LFI_DENA
                        BREAK
                        CASE 6
                            RETURN LFI_ALEX
                        BREAK
                        CASE 7
                           	RETURN LFI_SUSAN
                        BREAK
                        CASE 8
                            RETURN LFI_ALICE
                        BREAK
                        CASE 9
                            RETURN LFI_BILLY
                        BREAK
                        CASE 10
                            RETURN LFI_LIZ
                        BREAK
                        CASE 11
                            RETURN LFI_MFRIENDS116
                        BREAK
                  ENDSWITCH
            BREAK
			
            CASE LFI_REDWOOD
                  SWITCH iSlot
                      CASE 1
							RETURN LFI_MICHAEL
                        BREAK
                        CASE 2
                            RETURN LFI_KELLY
                        BREAK
                        CASE 3
                           	RETURN LFI_BRIAN
                        BREAK
                        CASE 4
                            RETURN LFI_GEMMA
                        BREAK
                        CASE 5
                            RETURN LFI_GERRY
                        BREAK
                        CASE 6
                            RETURN LFI_MFRIENDSXX1				// THIS WAS LISTED AS XX FRIENDS IN DOCUMENT 
                        BREAK
                  ENDSWITCH
            BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			SWITCH iSlot
				CASE 1
					RETURN LFI_LAMAR
				BREAK
				CASE 2
					RETURN LFI_SIMEON
				BREAK
				CASE 3
					RETURN LFI_TONYA
				BREAK
				CASE 4
					RETURN LFI_DENISE
				BREAK
				CASE 5
					RETURN LFI_STRETCH
				BREAK
				CASE 6
					RETURN LFI_TANISHA
				BREAK
				CASE 7
					RETURN LFI_TAVELL
				BREAK
				CASE 8
					RETURN LFI_DOM
				BREAK
				CASE 9
					RETURN LFI_BEV
				BREAK
				CASE 10
					RETURN LFI_DEVIN
				BREAK
				CASE 11
					RETURN LFI_DEMARCUS
				BREAK
				CASE 12
					RETURN LFI_ANDREB
				BREAK
				CASE 13
					RETURN LFI_NICOLSONB
				BREAK
				CASE 14
					RETURN LFI_JAMALR
				BREAK
				CASE 15
					RETURN LFI_JBB
				BREAK
				CASE 16
					RETURN LFI_DARRYLK
				BREAK
				CASE 17
					RETURN LFI_DANAE
				BREAK
			ENDSWITCH
		BREAK
		
		 CASE LFI_LAMAR
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_SIMEON
				BREAK
				CASE 3
					RETURN LFI_TONYA
				BREAK
				CASE 4
					RETURN LFI_DENISE
				BREAK
				CASE 5
					RETURN LFI_STRETCH
				BREAK
				CASE 6
					RETURN LFI_TANISHA
				BREAK
				CASE 7
					RETURN LFI_TAVELL
				BREAK
				CASE 8
					RETURN LFI_DEMARCUS
				BREAK
				CASE 9
					RETURN LFI_LEONV
				BREAK
				CASE 10
					RETURN LFI_NICOLSONB
				BREAK
				CASE 11
					RETURN LFI_JAMALR
				BREAK
				CASE 12
					RETURN LFI_JBB
				BREAK
				CASE 13
					RETURN LFI_DARRYLK
				BREAK
				CASE 14
					RETURN LFI_DANAE
				BREAK
				CASE 15
					RETURN LFI_FFRIENDS93
				BREAK
			ENDSWITCH
        BREAK
		
        CASE LFI_SIMEON
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_OWENY
				BREAK
				CASE 4
					RETURN LFI_BARBARAW
				BREAK
				CASE 5
					RETURN LFI_ANAKH
				BREAK
				CASE 6
					RETURN LFI_YERGHATT
				BREAK
				CASE 7
					RETURN LFI_SACHAY
				BREAK
				CASE 8
					RETURN LFI_FFRIENDS77
				BREAK
			ENDSWITCH
        BREAK
		
        CASE LFI_TONYA
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_TANISHA
				BREAK
				CASE 4
					RETURN LFI_STRETCH
				BREAK
				CASE 5
					RETURN LFI_DEMARCUS
				BREAK
				CASE 6
					RETURN LFI_JBB
				BREAK
				CASE 7
					RETURN LFI_KEEARAN
				BREAK
				CASE 8
					RETURN LFI_SHARONDAM
				BREAK
				CASE 9
					RETURN LFI_COLIND
				BREAK
				CASE 10
					RETURN LFI_NIAB
				BREAK
				CASE 11
					RETURN LFI_LAHRONDAW
				BREAK
				CASE 12
					RETURN LFI_FFRIENDS42
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_DENISE
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_TANISHA
				BREAK
				CASE 4
					RETURN LFI_TAVELL
				BREAK
				CASE 5
					RETURN LFI_MAGENTAA
				BREAK
				CASE 6
					RETURN LFI_MATTIEH
				BREAK
				CASE 7
					RETURN LFI_JULIEP
				BREAK
				CASE 8
					RETURN LFI_PIPPYE
				BREAK
				CASE 9
					RETURN LFI_FFRIENDS118
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_STRETCH
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_TANISHA
				BREAK
				CASE 4
					RETURN LFI_TAVELL
				BREAK
				CASE 5
					RETURN LFI_TONYA
				BREAK
				CASE 6
					RETURN LFI_DEMARCUS
				BREAK
				CASE 7
					RETURN LFI_DARRYLK
				BREAK
				CASE 8
					RETURN LFI_JBB
				BREAK
				CASE 9
					RETURN LFI_RENATTAS
				BREAK
				CASE 10
					RETURN LFI_FFRIENDS72
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_TANISHA
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_TONYA
				BREAK
				CASE 4
					RETURN LFI_STRETCH
				BREAK
				CASE 5
					RETURN LFI_DEMARCUS
				BREAK
				CASE 6
					RETURN LFI_TAVELL
				BREAK
				CASE 7
					RETURN LFI_DENISE
				BREAK
				CASE 8
					RETURN LFI_LELAY
				BREAK
				CASE 9
					RETURN LFI_CATRINAW
				BREAK
				CASE 10
					RETURN LFI_MONETTEA
				BREAK
				CASE 11
					RETURN LFI_DANAE
				BREAK
				CASE 12
					RETURN LFI_NIAB
				BREAK
				CASE 13
					RETURN LFI_LAHRONDAW
				BREAK
				CASE 14
					RETURN LFI_FFRIENDS226
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_TAVELL
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_TANISHA
				BREAK
				CASE 4
					RETURN LFI_STRETCH
				BREAK
				CASE 5
					RETURN LFI_DEMARCUS
				BREAK
				CASE 6
					RETURN LFI_DENISE
				BREAK
				CASE 7
					RETURN LFI_NICOLSONB
				BREAK
				CASE 8
					RETURN LFI_FFRIENDS175
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_DOM
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_GARRYH
				BREAK
				CASE 3
					RETURN LFI_JEFFC
				BREAK
				CASE 4
					RETURN LFI_KARENL
				BREAK
				CASE 5
					RETURN LFI_MARKP
				BREAK
				CASE 6
					RETURN LFI_FFRIENDS27
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_BEV
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_MADISONF
				BREAK
				CASE 3
					RETURN LFI_RAYG
				BREAK
				CASE 4
					RETURN LFI_ALANF
				BREAK
				CASE 5
					RETURN LFI_FFRIENDS64
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_DEVIN
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_RAYN
				BREAK
				CASE 3
					RETURN LFI_SIMONH
				BREAK
				CASE 4
					RETURN LFI_MOLLYS
				BREAK
				CASE 5
					RETURN LFI_FFRIENDS1644
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_DEMARCUS
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_STRETCH
				BREAK
				CASE 4
					RETURN LFI_TANISHA
				BREAK
				CASE 5
					RETURN LFI_TONYA
				BREAK
				CASE 6
					RETURN LFI_TAVELL
				BREAK
				CASE 7
					RETURN LFI_DEJAM
				BREAK
				CASE 8
					RETURN LFI_JBB
				BREAK
				CASE 9
					RETURN LFI_DARRYLK
				BREAK
				CASE 10
					RETURN LFI_HAILEYB
				BREAK
				CASE 11
					RETURN LFI_FFRIENDS118B
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_FEUD
			SWITCH iSlot
				CASE 1
					RETURN LFI_LAMAR
				BREAK
				CASE 2
					RETURN LFI_STRETCH
				BREAK
				CASE 3
					RETURN LFI_TAVELL
				BREAK
				CASE 4
					RETURN LFI_FRANKLIN
				BREAK
				CASE 5
					RETURN LFI_DEMARCUS
				BREAK
				CASE 6
					RETURN LFI_TYSONF
				BREAK
				CASE 7
					RETURN LFI_GRAHAMR
				BREAK
				CASE 8
					RETURN LFI_ERIKD
				BREAK
				CASE 9
					RETURN LFI_NELSONW
				BREAK
				CASE 10
					RETURN LFI_MODFEUD
				BREAK
				CASE 11
					RETURN LFI_ADRICH
				BREAK
				CASE 12
					RETURN LFI_KARLR
				BREAK
				CASE 13
					RETURN LFI_TAYEB
				BREAK
				CASE 14
					RETURN LFI_TRENTONM
				BREAK
				CASE 15
					RETURN LFI_LONNYG
				BREAK
				CASE 16
					RETURN LFI_DARRYLK
				BREAK
				CASE 17
					RETURN LFI_REGISW
				BREAK
				CASE 18
					RETURN LFI_RESHAYM
				BREAK
				CASE 19
					RETURN LFI_ANTOINEP
				BREAK
				CASE 20
					RETURN LFI_BENTONC
				BREAK
				CASE 21
					RETURN LFI_STERLINL
				BREAK
				CASE 22
					RETURN LFI_SARAP
				BREAK
				CASE 23
					RETURN LFI_DARNELLS
				BREAK
				CASE 24
					RETURN LFI_JAXS
				BREAK
				CASE 25
					RETURN LFI_FFRIENDSXX1
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_SPRUNK
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_DOM
				BREAK
				CASE 3
					RETURN LFI_PAULO
				BREAK
				CASE 4
					RETURN LFI_KATEM
				BREAK
				CASE 5
					RETURN LFI_JOSHUAW
				BREAK
				CASE 6
					RETURN LFI_VINCEH
				BREAK
				CASE 7
					RETURN LFI_TODDR
				BREAK
				CASE 8
					RETURN LFI_FFRIENDSXX2
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_INKINC
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_DEMARCUS
				BREAK
				CASE 3
					RETURN LFI_STRETCH
				BREAK
				CASE 4
					RETURN LFI_TONYA
				BREAK
				CASE 5
					RETURN LFI_MIKEH
				BREAK
				CASE 6
					RETURN LFI_HARVEYP
				BREAK
				CASE 7
					RETURN LFI_ANDYW
				BREAK
				CASE 8
					RETURN LFI_TAYLOTH
				BREAK
				CASE 9
					RETURN LFI_STEVEW
				BREAK
				CASE 10
					RETURN LFI_FFRIENDSXX3
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_HERRKUTZ
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_DONNAHK
				BREAK
				CASE 3
					RETURN LFI_LANCEW
				BREAK
				CASE 4
					RETURN LFI_PHILG
				BREAK
				CASE 5
					RETURN LFI_JAMIEE
				BREAK
				CASE 6
					RETURN LFI_JONH
				BREAK
				CASE 7
					RETURN LFI_GERRYC
				BREAK
				CASE 8
					RETURN LFI_AARONF
				BREAK
				CASE 9
					RETURN LFI_CONNORS
				BREAK
				CASE 10
					RETURN LFI_FFRIENDSXX4
				BREAK
			ENDSWITCH
        BREAK
        CASE LFI_LSCUSTOMS
			SWITCH iSlot
				CASE 1
					RETURN LFI_FRANKLIN
				BREAK
				CASE 2
					RETURN LFI_LAMAR
				BREAK
				CASE 3
					RETURN LFI_POPPAL
				BREAK
				CASE 4
					RETURN LFI_HAILEYW
				BREAK
				CASE 5
					RETURN LFI_SAMW
				BREAK
				CASE 6
					RETURN LFI_GARYH
				BREAK
				CASE 7
					RETURN LFI_HANKS
				BREAK
				CASE 8
					RETURN LFI_FFRIENDSXX5
				BREAK
			ENDSWITCH
        BREAK
		
        CASE LFI_TREVOR
			SWITCH iSlot
				CASE 1
					RETURN LFI_RON
				BREAK
				CASE 2
					RETURN LFI_WADE
				BREAK
			ENDSWITCH
		BREAK
        CASE LFI_RON
			SWITCH iSlot
				CASE 1
					RETURN LFI_TREVOR
				BREAK
				CASE 2
					RETURN LFI_WADE
				BREAK
				CASE 3
					RETURN LFI_ASHLEY
				BREAK
				CASE 4
					RETURN LFI_CLETUS
				BREAK
				CASE 5
					RETURN LFI_TFRIENDS10
				BREAK
			ENDSWITCH
		BREAK
        CASE LFI_WADE
			SWITCH iSlot
				CASE 1
					RETURN LFI_TREVOR
				BREAK
				CASE 2
					RETURN LFI_RON
				BREAK
				CASE 3
					RETURN LFI_JENNY
				BREAK
				CASE 4
					RETURN LFI_FLOYD
				BREAK
				CASE 5
					RETURN LFI_HANK
				BREAK
				CASE 6
					RETURN LFI_TFRIENDS27
				BREAK
			ENDSWITCH
		BREAK
        CASE LFI_LUDENDORFF
			SWITCH iSlot
				CASE 1
					RETURN LFI_TREVOR
				BREAK
				CASE 2
					RETURN LFI_TFRIENDSXX
				BREAK
			ENDSWITCH
        BREAK
		
	ENDSWITCH
	
	RETURN LFI_MAX

ENDFUNC

PROC SET_FRIENDS_LIST(SCALEFORM_INDEX pagemov)

	TEXT_LABEL tlName
	TEXT_LABEL tlSmallPhoto

	INT iSlot = 5
	INT iFriend
	FOR iFriend = 1 TO GET_MAX_FRIENDS_FOR_ID(iCurrentLFID)
	
		BOOL bAddFriend = TRUE
		
		// Some friends shouldn't be added to Michael or Franklin's friend list until certain missions are completed
		// So check for these and don't do anything if they aren't supposed to be there
		
		IF iCurrentLFID = 0 // Michael
			IF GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend) = LFI_MARYANNQUINN
				IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_FANATIC_1), CP_GROUP_RANDOMCHARS)
					bAddFriend = FALSE // Fanatic 1 hasn't been completed, don't add Mary Ann
				ENDIF
			ELIF GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend) = LFI_LESTERCREST
				IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1)
					bAddFriend = FALSE // Lester 1 hasn't been completed, don't add Lester
				ENDIF
			ENDIF
		ELIF iCurrentLFID = 1 // Franklin
			IF GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend) = LFI_DOM
				IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_EXTREME_1), CP_GROUP_RANDOMCHARS)
					bAddFriend = FALSE // Extreme 1 hasn't been completed, don't add Dom
				ENDIF
			ELIF GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend) = LFI_BEV
				IF NOT HasMissionBeenCompleted(ENUM_TO_INT(RC_PAPARAZZO_1), CP_GROUP_RANDOMCHARS)
					bAddFriend = FALSE // Pap 1 hasn't been completed, don't add Bev
				ENDIF
			ELIF GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend) = LFI_DEVIN
				IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
					bAddFriend = FALSE // Car Steal 1 hasn't been completed, don't add Devin
				ENDIF
			ENDIF
		ENDIF
		
		IF bAddFriend = TRUE
			// oh my god what have i done
			tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend))) // i am SO sorry
			tlSmallPhoto = GET_SMALL_PORTRAIT(GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend))
			
			CharWallPostID[iSlot] = GET_FRIEND_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iFriend)
			CPRINTLN(DEBUG_INTERNET, "Storing ", tlName ," in slot ", iSlot)
			
			// All of the chars except the main player chars have "x friends with private profiles" as their last friend
			// This shouldn't be clickable - so if this is the last friend in the list and it's NOT a main player char, stop it being clickable
			BOOL bClickable = TRUE
			IF iFriend = GET_MAX_FRIENDS_FOR_ID(iCurrentLFID)
				IF iCurrentLFID != 0
				AND iCurrentLFID != 1
				AND iCurrentLFID != 2
					bClickable = FALSE
				ENDIF
			ENDIF

			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) // Name
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") // Status text - always the same?
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlSmallPhoto) // portrait txd
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bClickable) // Can friend be clicked
			END_SCALEFORM_MOVIE_METHOD()
			iSlot++
		ENDIF
	ENDFOR
	
	iLastSlot = iSlot // Stick the last slot we were using in... the last slot var
	CPRINTLN(DEBUG_INTERNET, "Sidebar will begin being generated from slot #", iLastSlot)

ENDPROC

// ---------------------------------------
// HANDLES STALKING PAGE FOR A PROFILE
// ---------------------------------------

FUNC INT GET_MAX_STALKING_FOR_ID(INT LFI_ID)

	SWITCH INT_TO_ENUM(LFICharList, LFI_ID)
		CASE LFI_MICHAEL
			RETURN 1
		BREAK
		CASE LFI_FRANKLIN
			RETURN 5
		BREAK
		CASE LFI_LAMAR
			RETURN 3
		BREAK
		CASE LFI_SIMEON
			RETURN 2
		BREAK
		CASE LFI_TONYA
			RETURN 2
		BREAK
		CASE LFI_DENISE
			RETURN 1
		BREAK
		CASE LFI_STRETCH
			RETURN 2
		BREAK
		CASE LFI_TANISHA
			RETURN 2
		BREAK
		CASE LFI_TAVELL
			RETURN 2
		BREAK
		CASE LFI_DOM
			RETURN 2
		BREAK
		CASE LFI_BEV
			RETURN 2
		BREAK
		CASE LFI_DEVIN
			RETURN 2
		BREAK
		CASE LFI_DEMARCUS
			RETURN 2
		BREAK
		CASE LFI_FEUD
			RETURN 2
		BREAK
		CASE LFI_SPRUNK
			RETURN 1
		BREAK
		CASE LFI_INKINC
			RETURN 1
		BREAK
		CASE LFI_HERRKUTZ
			RETURN 1
		BREAK
		CASE LFI_LSCUSTOMS
			RETURN 1
		BREAK
		
		// Michael friends
		CASE LFI_TDESANTA
			RETURN 2
		BREAK

		CASE LFI_JDESANTA
			RETURN 2
		BREAK

		CASE LFI_ADESANTA
			RETURN 2
		BREAK

		CASE LFI_LESTERCREST
			RETURN 2
		BREAK

		CASE LFI_MARYANNQUINN
			RETURN 2
		BREAK

		CASE LFI_HAYDENDUBOSE
			RETURN 2
		BREAK

		CASE LFI_KYLECHAVIS
			RETURN 2
		BREAK

		// Michael's stalking brands
		CASE LFI_REDWOOD
			RETURN 1
		BREAK

		// Trevor's friends
		
		CASE LFI_TREVOR
			RETURN 1
		BREAK
		CASE LFI_RON
			RETURN 2
		BREAK

		CASE LFI_WADE
			RETURN 2
		BREAK

		// Trevor's stalking brands
		CASE LFI_LUDENDORFF
			RETURN 1
		BREAK

	ENDSWITCH
      
	RETURN 0

ENDFUNC

FUNC LFICharList GET_STALKING_IN_SLOT_FOR_GIVEN_CHAR(LFICharList LFICurrentChar, int iSlot)

	SWITCH LFICurrentChar
		// Michael & friends
		CASE LFI_MICHAEL
			SWITCH iSlot
				CASE 1
					RETURN LFI_REDWOOD
				BREAK
			ENDSWITCH
		BREAK
		
		CASE LFI_TDESANTA
			SWITCH iSlot
				CASE 1
					RETURN LFI_FAMEORSHAME
				BREAK
				CASE 2
					RETURN LFI_STCE
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_JDESANTA
			SWITCH iSlot
				CASE 1
					RETURN LFI_RIGHTEOUSSLAUGHTER
				BREAK
				CASE 2
					RETURN LFI_ALCO
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_ADESANTA
			SWITCH iSlot
				CASE 1
					RETURN LFI_GAMMI
				BREAK
				CASE 2
					RETURN LFI_LOSSANTOSGOLFCLUB
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LESTERCREST
			SWITCH iSlot
				CASE 1
					RETURN LFI_BWSQ
				BREAK
				CASE 2
					RETURN LFI_FACADE
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_MARYANNQUINN
			SWITCH iSlot
				CASE 1
					RETURN LFI_PEDALANDMETAL
				BREAK
				CASE 2
					RETURN LFI_EGOCHASER
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_HAYDENDUBOSE
			SWITCH iSlot
				CASE 1
					RETURN LFI_BWSQ
				BREAK
				CASE 2
					RETURN LFI_LOSSANTOSGOLFCLUB
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_KYLECHAVIS
			SWITCH iSlot
				CASE 1
					RETURN LFI_PROLAPS
				BREAK
				CASE 2
					RETURN LFI_HEAT
				BREAK
			ENDSWITCH
		BREAK

		// Michael's stalking brands
		CASE LFI_REDWOOD
			SWITCH iSlot
				CASE 1
					RETURN LFI_AMMU
				BREAK
			ENDSWITCH
		BREAK

		// Franklin & friends
		CASE LFI_FRANKLIN
			SWITCH iSlot
				CASE 1
					RETURN LFI_FEUD
				BREAK
				CASE 2
					RETURN LFI_SPRUNK
				BREAK
				CASE 3
					RETURN LFI_INKINC
				BREAK
				CASE 4
					RETURN LFI_HERRKUTZ
				BREAK
				CASE 5
					RETURN LFI_LSCUSTOMS
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LAMAR
			SWITCH iSlot
				CASE 1
					RETURN LFI_FEUD
				BREAK
				CASE 2
					RETURN LFI_INKINC
				BREAK
				CASE 3
					RETURN LFI_LSCUSTOMS
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_SIMEON
			SWITCH iSlot
				CASE 1
					RETURN LFI_PRDE
				BREAK
				CASE 2
					RETURN LFI_CCCK
				BREAK
				ENDSWITCH
		BREAK
		CASE LFI_TONYA
			SWITCH iSlot
				CASE 1
					RETURN LFI_INKINC
				BREAK
				CASE 2
					RETURN LFI_ALCO
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DENISE
			SWITCH iSlot
				CASE 1
					RETURN LFI_GRTR
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_STRETCH
			SWITCH iSlot
				CASE 1
					RETURN LFI_FEUD
				BREAK
				CASE 2
					RETURN LFI_INKINC
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TANISHA
			SWITCH iSlot
				CASE 1
					RETURN LFI_VANG
				BREAK
				CASE 2
					RETURN LFI_MXRE
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_TAVELL
			SWITCH iSlot
				CASE 1
					RETURN LFI_FEUD
				BREAK
				CASE 2
					RETURN LFI_ALCO
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DOM
			SWITCH iSlot
				CASE 1
					RETURN LFI_SPRUNK
				BREAK
				CASE 2
					RETURN LFI_BWSQ
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_BEV
			SWITCH iSlot
				CASE 1
					RETURN LFI_STST
				BREAK
				CASE 2
					RETURN LFI_STCE
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DEVIN
			SWITCH iSlot
				CASE 1
					RETURN LFI_BWSQ
				BREAK
				CASE 2
					RETURN LFI_STCE
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_DEMARCUS
			SWITCH iSlot
				CASE 1
					RETURN LFI_FEUD
				BREAK
				CASE 2
					RETURN LFI_INKINC
				BREAK
			ENDSWITCH
		BREAK
		
		// Franklin's stalking brands
		CASE LFI_FEUD
			SWITCH iSlot
				CASE 1
					RETURN LFI_SPRUNK
				BREAK
				CASE 2
					RETURN LFI_ALCO
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_SPRUNK
			SWITCH iSlot
				CASE 1
					RETURN LFI_BWSQ
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_INKINC
			SWITCH iSlot
				CASE 1
					RETURN LFI_HERRKUTZ
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_HERRKUTZ
			SWITCH iSlot
				CASE 1
					RETURN LFI_INKINC
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_LSCUSTOMS
			SWITCH iSlot
				CASE 1
					RETURN LFI_SPRUNK
				BREAK
			ENDSWITCH
		BREAK
		
		// Trevor's friends
		CASE LFI_TREVOR
			SWITCH iSlot
				CASE 1
					RETURN LFI_LUDENDORFF
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_RON
			SWITCH iSlot
				CASE 1
					RETURN LFI_BCR
				BREAK
				CASE 2
					RETURN LFI_FATALINCURSIONS
				BREAK
			ENDSWITCH
		BREAK
		CASE LFI_WADE
			SWITCH iSlot
				CASE 1
					RETURN LFI_FATALINCURSIONS
				BREAK
				CASE 2
					RETURN LFI_BCR
				BREAK
			ENDSWITCH
		BREAK
		
		// Trevor's stalking brands
		CASE LFI_LUDENDORFF
			SWITCH iSlot
				CASE 1
					RETURN LFI_AMMU
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH

	RETURN LFI_MAX

ENDFUNC

PROC SET_STALKING_LIST(SCALEFORM_INDEX pagemov)

	TEXT_LABEL tlName
	TEXT_LABEL tlSmallPhoto

	INT iSlot = 5
	INT iStalk
	FOR iStalk = 1 TO GET_MAX_STALKING_FOR_ID(iCurrentLFID)
	
		BOOL bAddStalk = TRUE

		
		IF bAddStalk = TRUE
			// oh my god what have i done
			tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_STALKING_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iStalk))) // i am SO sorry
			tlSmallPhoto = GET_SMALL_PORTRAIT(GET_STALKING_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iStalk))
			
			CharWallPostID[iSlot] = GET_STALKING_IN_SLOT_FOR_GIVEN_CHAR(INT_TO_ENUM(LFICharList, iCurrentLFID), iStalk)
			CPRINTLN(DEBUG_INTERNET, "Storing ", tlName ," in slot ", iSlot)

			BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) // Name
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") // Status text - always the same?
				//This is player name on purpose because _string checks the string table, can't find it and breaks it...
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlSmallPhoto) // portrait txd
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // Can friend be clicked
			END_SCALEFORM_MOVIE_METHOD()
			iSlot++
		ENDIF
	ENDFOR
	
	iLastSlot = iSlot // Stick the last slot we were using in... the last slot var
	CPRINTLN(DEBUG_INTERNET, "Sidebar will begin being generated from slot #", iLastSlot)

ENDPROC

// ---------------------------------------
// HANDLES SIDEBAR STUFF
// ---------------------------------------


FUNC INT GET_NUMBER_OF_FAMILY_FOR_SIDEBAR(LFICharList LFICurrentChar)

	SWITCH LFICurrentChar
		// Michael & friends
		CASE LFI_MICHAEL
			RETURN 3
		BREAK
		CASE LFI_TDESANTA
			RETURN 3
		BREAK
		CASE LFI_JDESANTA
			RETURN 3
		BREAK
		CASE LFI_ADESANTA
			RETURN 3
		BREAK
		CASE LFI_LESTERCREST
			RETURN 0
		BREAK
		CASE LFI_MARYANNQUINN
			RETURN 1
		BREAK
		CASE LFI_HAYDENDUBOSE
			RETURN 1
		BREAK
		CASE LFI_KYLECHAVIS
			RETURN 1
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			RETURN 5
		BREAK
		CASE LFI_LAMAR
			RETURN 0
		BREAK
		CASE LFI_SIMEON
			RETURN 2
		BREAK
		CASE LFI_TONYA
			RETURN 0
		BREAK
		CASE LFI_DENISE
			RETURN 2
		BREAK
		CASE LFI_STRETCH
			RETURN 0
		BREAK
		CASE LFI_TANISHA
			RETURN 0
		BREAK
		CASE LFI_TAVELL
			RETURN 2
		BREAK
		CASE LFI_DOM
			RETURN 0
		BREAK
		CASE LFI_BEV
			RETURN 1
		BREAK
		CASE LFI_DEVIN
			RETURN 0
		BREAK
		CASE LFI_DEMARCUS
			RETURN 1
		BREAK
		
		// Trevor & friends
		CASE LFI_TREVOR
			RETURN 0
		BREAK
		CASE LFI_RON
			RETURN 0
		BREAK
		CASE LFI_WADE
			RETURN 1
		BREAK
		
	ENDSWITCH
	
	RETURN -1 // Just so we know it's fucked up if we see this

ENDFUNC

FUNC LFICharList GET_FAMILY_FOR_CHARACTER_SIDEBAR(LFICharList LFICurrentChar)

	SWITCH LFICurrentChar
		// Michael & friends
		CASE LFI_MICHAEL
			RETURN LFI_ADESANTA
		BREAK
		CASE LFI_TDESANTA
			RETURN LFI_MICHAEL
		BREAK
		CASE LFI_JDESANTA
			RETURN LFI_ADESANTA
		BREAK
		CASE LFI_ADESANTA
			RETURN LFI_MICHAEL
		BREAK
		CASE LFI_LESTERCREST
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_MARYANNQUINN
			RETURN LFI_MOTHER
		BREAK
		CASE LFI_HAYDENDUBOSE
			RETURN LFI_MELINDA
		BREAK
		CASE LFI_KYLECHAVIS
			RETURN LFI_SUSAN
		BREAK
		CASE LFI_REDWOOD
			RETURN LFI_MICHAEL
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			RETURN LFI_DENISE
		BREAK
		CASE LFI_LAMAR
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_SIMEON
			RETURN LFI_SACHAY
		BREAK
		CASE LFI_TONYA
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_DENISE
			RETURN LFI_TAVELL
		BREAK
		CASE LFI_STRETCH
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_TANISHA
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_TAVELL
			RETURN LFI_FRANKLIN
		BREAK
		CASE LFI_DOM
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_BEV
			RETURN LFI_ALANF
		BREAK
		CASE LFI_DEVIN
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_DEMARCUS
			RETURN LFI_HAILEYB
		BREAK
		CASE LFI_FEUD
			RETURN LFI_FRANKLIN
		BREAK
		CASE LFI_SPRUNK
			RETURN LFI_FRANKLIN
		BREAK
		CASE LFI_INKINC
			RETURN LFI_FRANKLIN
		BREAK
		CASE LFI_HERRKUTZ
			RETURN LFI_AARONF
		BREAK
		CASE LFI_LSCUSTOMS
			RETURN LFI_FRANKLIN
		BREAK
		
		// Trevor & friends
		CASE LFI_TREVOR
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_RON
			RETURN LFI_NOFAM
		BREAK
		CASE LFI_WADE
			RETURN LFI_FLOYD
		BREAK
		CASE LFI_LUDENDORFF
			RETURN LFI_TREVOR
		BREAK
		
	ENDSWITCH
	
	RETURN LFI_MAX

ENDFUNC

FUNC INT GET_NUMBER_OF_FRIENDS_FOR_SIDEBAR(LFICharList LFICurrentChar)

	INT iNumOfFriends
	
	SWITCH LFICurrentChar
		// Michael & friends/brands
		CASE LFI_MICHAEL
			iNumOfFriends = 5 // Start with 5
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1)
				iNumOfFriends++ // Lester is now a friend, +1
			ENDIF
			IF HasMissionBeenCompleted(ENUM_TO_INT(RC_FANATIC_1), CP_GROUP_RANDOMCHARS)
				iNumOfFriends++ // Mary Ann is now a friend, +1
			ENDIF
			RETURN iNumOfFriends
		BREAK
		CASE LFI_TDESANTA
			RETURN 443
		BREAK
		CASE LFI_JDESANTA
			RETURN 152
		BREAK
		CASE LFI_ADESANTA
			RETURN 217
		BREAK
		CASE LFI_LESTERCREST
			RETURN 71
		BREAK
		CASE LFI_MARYANNQUINN
			RETURN 63
		BREAK
		CASE LFI_HAYDENDUBOSE
			RETURN 91
		BREAK
		CASE LFI_KYLECHAVIS
			RETURN 126
		BREAK
		CASE LFI_REDWOOD
			RETURN 1000
		BREAK

		// Franklin & friends/brands
		CASE LFI_FRANKLIN
			// This will have to return 14, 15, 16 or 17 depending on where you are in flow - do later
			iNumOfFriends = 14 // Start with 14
			IF HasMissionBeenCompleted(ENUM_TO_INT(RC_EXTREME_1), CP_GROUP_RANDOMCHARS)
				iNumOfFriends++
			ENDIF
			IF HasMissionBeenCompleted(ENUM_TO_INT(RC_PAPARAZZO_1), CP_GROUP_RANDOMCHARS)
				iNumOfFriends++
			ENDIF
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
				iNumOfFriends++
			ENDIF
			RETURN iNumOfFriends
		BREAK		
		CASE LFI_LAMAR
			RETURN 107
		BREAK
		CASE LFI_SIMEON
			RETURN 84
		BREAK
		CASE LFI_TONYA
			RETURN 53
		BREAK
		CASE LFI_DENISE
			RETURN 126
		BREAK
		CASE LFI_STRETCH
			RETURN 78
		BREAK
		CASE LFI_TANISHA
			RETURN 239
		BREAK
		CASE LFI_TAVELL
			RETURN 182
		BREAK
		CASE LFI_DOM
			RETURN 32
		BREAK
		CASE LFI_BEV
			RETURN 68
		BREAK
		CASE LFI_DEVIN
			RETURN 1648
		BREAK
		CASE LFI_DEMARCUS
			RETURN 131
		BREAK
		
		CASE LFI_FEUD
			RETURN 1000
		BREAK
		CASE LFI_SPRUNK
			RETURN 1000
		BREAK
		CASE LFI_HERRKUTZ
			RETURN 1000
		BREAK
		CASE LFI_INKINC
			RETURN 1000
		BREAK
		CASE LFI_LSCUSTOMS
			RETURN 1000
		BREAK
		
		// Trevor & friends/brands
		CASE LFI_TREVOR
			RETURN 2
		BREAK
		CASE LFI_RON
			RETURN 14
		BREAK
		CASE LFI_WADE
			RETURN 32
		BREAK
		CASE LFI_LUDENDORFF
			RETURN 1000
		BREAK
		
	ENDSWITCH
	
	RETURN -1 // Just so we know it's fucked up if we see this

ENDFUNC

FUNC LFICharList GET_FRIENDS_FOR_SIDEBAR(LFICharList LFICurrentChar)

	SWITCH LFICurrentChar
		// Michael & friends
		CASE LFI_MICHAEL
			RETURN LFI_KYLECHAVIS
		BREAK
		CASE LFI_TDESANTA
			RETURN LFI_LAUREN
		BREAK
		CASE LFI_JDESANTA
			RETURN LFI_AHRON
		BREAK
		CASE LFI_ADESANTA
			RETURN LFI_FABIEN
		BREAK
		CASE LFI_LESTERCREST
			RETURN LFI_MAURIE
		BREAK
		CASE LFI_MARYANNQUINN
			RETURN LFI_RICHARD
		BREAK
		CASE LFI_HAYDENDUBOSE
			RETURN LFI_JASPER
		BREAK
		CASE LFI_KYLECHAVIS
			RETURN LFI_REBECCA
		BREAK
		CASE LFI_REDWOOD
			RETURN LFI_KELLY
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			RETURN LFI_LAMAR
		BREAK
		CASE LFI_LAMAR
			RETURN LFI_FRANKLIN
		BREAK
		CASE LFI_SIMEON
			RETURN LFI_ANAKH
		BREAK
		CASE LFI_TONYA
			RETURN LFI_LAMAR
		BREAK
		CASE LFI_DENISE
			RETURN LFI_MAGENTAA
		BREAK
		CASE LFI_STRETCH
			RETURN LFI_DEMARCUS
		BREAK
		CASE LFI_TANISHA
			RETURN LFI_FRANKLIN
		BREAK
		CASE LFI_TAVELL
			RETURN LFI_LAMAR
		BREAK
		CASE LFI_DOM
			RETURN LFI_JEFFC
		BREAK
		CASE LFI_BEV
			RETURN LFI_RAYG
		BREAK
		CASE LFI_DEVIN
			RETURN LFI_RAYN
		BREAK
		CASE LFI_DEMARCUS
			RETURN LFI_LAMAR
		BREAK
		CASE LFI_FEUD
			RETURN LFI_DEMARCUS
		BREAK
		CASE LFI_SPRUNK
			RETURN LFI_TODDR
		BREAK
		CASE LFI_INKINC
			RETURN LFI_HARVEYP
		BREAK
		CASE LFI_HERRKUTZ
			RETURN LFI_DONNAHK
		BREAK
		CASE LFI_LSCUSTOMS
			RETURN LFI_GARYH
		BREAK
		
		// Trevor & friends
		CASE LFI_TREVOR
			RETURN LFI_WADE
		BREAK
		CASE LFI_RON
			RETURN LFI_TREVOR
		BREAK
		CASE LFI_WADE
			RETURN LFI_HANK
		BREAK
		
	ENDSWITCH
	
	RETURN LFI_MAX

ENDFUNC

FUNC INT GET_NUMBER_OF_STALKING_FOR_SIDEBAR(LFICharList LFICurrentChar)

	// This is the same! Yay!
	RETURN GET_MAX_STALKING_FOR_ID(ENUM_TO_INT(LFICurrentChar))
	

//	SWITCH LFICurrentChar
//		// Michael & friends
//		CASE LFI_MICHAEL
//			RETURN 2
//		BREAK
//		CASE LFI_TDESANTA
//			RETURN 2
//		BREAK
//		CASE LFI_JDESANTA
//			RETURN 2
//		BREAK
//		CASE LFI_ADESANTA
//			RETURN 2
//		BREAK
//		CASE LFI_LESTERCREST
//			RETURN 2
//		BREAK
//		CASE LFI_MARYANNQUINN
//			RETURN 2
//		BREAK
//		CASE LFI_HAYDENDUBOSE
//			RETURN 2
//		BREAK
//		CASE LFI_KYLECHAVIS
//			RETURN 2
//		BREAK
//
//		
//		// Franklin & friends
//		CASE LFI_FRANKLIN
//			RETURN 5
//		BREAK
//		
//		CASE LFI_LAMAR
//			RETURN 2
//		BREAK
//		
//	ENDSWITCH
//	
//	RETURN -1 // Just so we know it's fucked up if we see this

ENDFUNC

FUNC LFICharList GET_STALKING_FOR_SIDEBAR(LFICharList LFICurrentChar)

	SWITCH LFICurrentChar
		// Michael & friends
		CASE LFI_MICHAEL
			RETURN LFI_REDWOOD
		BREAK
		CASE LFI_TDESANTA
			RETURN LFI_STCE
		BREAK
		CASE LFI_JDESANTA
			RETURN LFI_ALCO
		BREAK
		CASE LFI_ADESANTA
			RETURN LFI_LOSSANTOSGOLFCLUB
		BREAK
		CASE LFI_LESTERCREST
			RETURN LFI_FACADE
		BREAK
		CASE LFI_MARYANNQUINN
			RETURN LFI_EGOCHASER
		BREAK
		CASE LFI_HAYDENDUBOSE
			RETURN LFI_LOSSANTOSGOLFCLUB
		BREAK
		CASE LFI_KYLECHAVIS
			RETURN LFI_HEAT
		BREAK
		CASE LFI_REDWOOD
			RETURN LFI_GERRY
		BREAK
		
		// Franklin & friends
		CASE LFI_FRANKLIN
			RETURN LFI_LSCUSTOMS
		BREAK
		CASE LFI_LAMAR
			RETURN LFI_FEUD
		BREAK
		CASE LFI_SIMEON
			RETURN LFI_CCCK
		BREAK
		CASE LFI_TONYA
			RETURN LFI_ALCO
		BREAK
		CASE LFI_DENISE
			RETURN LFI_GRTR
		BREAK
		CASE LFI_STRETCH
			RETURN LFI_FEUD
		BREAK
		CASE LFI_TANISHA
			RETURN LFI_MXRE
		BREAK
		CASE LFI_TAVELL
			RETURN LFI_ALCO
		BREAK
		CASE LFI_DOM
			RETURN LFI_BWSQ
		BREAK
		CASE LFI_BEV
			RETURN LFI_STCE
		BREAK
		CASE LFI_DEVIN
			RETURN LFI_STCE
		BREAK
		CASE LFI_DEMARCUS
			RETURN LFI_FEUD
		BREAK
		CASE LFI_FEUD
			RETURN LFI_STRETCH
		BREAK
		CASE LFI_SPRUNK
			RETURN LFI_VINCEH
		BREAK
		CASE LFI_INKINC
			RETURN LFI_STEVEW
		BREAK
		CASE LFI_HERRKUTZ
			RETURN LFI_GERRYC
		BREAK
		CASE LFI_LSCUSTOMS
			RETURN LFI_SAMW
		BREAK
		
		// Trevor & friends
		CASE LFI_TREVOR
			RETURN LFI_LUDENDORFF
		BREAK
		CASE LFI_RON
			RETURN LFI_FATALINCURSIONS
		BREAK
		CASE LFI_WADE
			RETURN LFI_BCR
		BREAK
		
	ENDSWITCH
	
	RETURN LFI_MAX

ENDFUNC

/// PURPOSE:
///    The amount of friends brand pages have can be HUUUUGE we store them as strings and display them in the "Friends" sidebar title   
FUNC TEXT_LABEL GET_BRAND_STALKING_NUMBER_FOR_SIDEBAR(LFICharList LFICurrentChar)

	TEXT_LABEL tlReturn = "LFI_"
	
	SWITCH LFICurrentChar
		// Michael & friends
		CASE LFI_REDWOOD
			tlReturn += "REDWOODCNT"
		BREAK
		CASE LFI_SPRUNK
			tlReturn += "SPRUNKCNT"
		BREAK
		CASE LFI_FEUD
			tlReturn += "FEUDCNT"
		BREAK
		CASE LFI_INKINC
			tlReturn += "INKCNT"
		BREAK
		CASE LFI_HERRKUTZ
			tlReturn += "HKCNT"
		BREAK
		CASE LFI_LSCUSTOMS
			tlReturn += "LSCCNT"
		BREAK
		CASE LFI_LUDENDORFF
			tlReturn += "LUDENCNT"
		BREAK
		DEFAULT
			tlReturn += "UNKNOWN"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn

ENDFUNC

PROC SET_SIDEBAR(SCALEFORM_INDEX pagemov)

	TEXT_LABEL tlTitle
	TEXT_LABEL tlName
	TEXT_LABEL tlSmallPhoto
	INT iNumberOfPeeps

	INT iSlot = iLastSlot
	INT iSidebarID
	FOR iSidebarID = 0 TO 2
	
	// People profiles have a Family, Friends and Stalking set of boxes on the side
	// Brands just have a set of Friends
	IF NOT IS_LFI_CHAR_A_BRAND(INT_TO_ENUM(LFICharList, iCurrentLFID))

		SWITCH iSidebarID
			CASE 0 // Family box
				tlTitle = "LFI_FAMILY"
				tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_FAMILY_FOR_CHARACTER_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))) // oh god so sorry
				tlSmallPhoto = GET_SMALL_PORTRAIT(GET_FAMILY_FOR_CHARACTER_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))
				iNumberOfPeeps = GET_NUMBER_OF_FAMILY_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID))
			BREAK
			CASE 1 // Friends box
				tlTitle = "LFI_FRIENDS"
				tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_FRIENDS_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))) // oh god so sorry
				tlSmallPhoto = GET_SMALL_PORTRAIT(GET_FRIENDS_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))
				iNumberOfPeeps = GET_NUMBER_OF_FRIENDS_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID))
			BREAK
			CASE 2 // Stalking box
				tlTitle = "LFI_STALKING"
				tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_STALKING_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))) // oh god so sorry
				tlSmallPhoto = GET_SMALL_PORTRAIT(GET_STALKING_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))
				iNumberOfPeeps = GET_NUMBER_OF_STALKING_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID))
			BREAK
		ENDSWITCH

		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlTitle) // Box title
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) // Character name
			//This is player name on purpose because _string checks the string table, can't find it and breaks it...
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlSmallPhoto) // portrait txd
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumberOfPeeps) // Number of family/friends/etc
		END_SCALEFORM_MOVIE_METHOD()
		
	ELSE
	
		// BRAND SIDEBARS ARE SET UP A BIT DIFFERENTLY
		// They only have "friends" - but we need to use the 'family', 'friends' and 'stalking' slots to display pictures and names in the right place
		// So the boxes for the sidebar will get the names/pics in the same order to keep things consistent, but the first box will use the 'Friends' string
		// and number of 'Friends' so it displays things correctly and is a bit less confusing
		// (a BIT)
	
		SWITCH iSidebarID
			CASE 0 // Friends box 1
				tlTitle = GET_BRAND_STALKING_NUMBER_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID))
				tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_FAMILY_FOR_CHARACTER_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))) // oh god so sorry
				tlSmallPhoto = GET_SMALL_PORTRAIT(GET_FAMILY_FOR_CHARACTER_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))
				iNumberOfPeeps = 0
			BREAK
			CASE 1 // Friends box 2
				tlTitle = ""
				tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_FRIENDS_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))) // oh god so sorry
				tlSmallPhoto = GET_SMALL_PORTRAIT(GET_FRIENDS_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))
				iNumberOfPeeps = 0
			BREAK
			CASE 2 // Friends box 2
				tlTitle = ""
				tlName = GET_LFI_PROFILE_USERNAME(ENUM_TO_INT(GET_STALKING_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))) // oh god so sorry
				tlSmallPhoto = GET_SMALL_PORTRAIT(GET_STALKING_FOR_SIDEBAR(INT_TO_ENUM(LFICharList, iCurrentLFID)))
				iNumberOfPeeps = 0
			BREAK
		ENDSWITCH
		
		IF ARE_STRINGS_EQUAL(tlName, "LFI_UNKNOWN")
			tlName = ""
		ENDIF
		
		IF ARE_STRINGS_EQUAL(tlSmallPhoto, "BLANK")
			tlSmallPhoto = ""
		ENDIF

		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlTitle) // Box title
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) // Character name
			//This is player name on purpose because _string checks the string table, can't find it and breaks it...
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlSmallPhoto) // portrait txd
//			IF iSidebarID = 0
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumberOfPeeps) // Number of family/friends/etc
//			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	
	ENDIF
	
	iSlot++
		
	ENDFOR

ENDPROC

// ---------------------------------------
// HANDLES MAIN LIFEINVADER PAGE UPDATES & HISTORY
// ---------------------------------------


PROC UPDATE_LIFEINVADER_PAGE(SCALEFORM_INDEX pagemov, BOOL bForceUpdate = FALSE)

	IF iCurrentLFID != iPreviousLFID
	OR bForceUpdate = TRUE // Sometimes we want to specifically update the page even if the ID hasn't changed, to switch messages/friends/stalking etc
		CPRINTLN(DEBUG_INTERNET, "Updating Lifeinvader page...")

		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		SET_LFI_PROFILE(pagemov) // Do the profile information
		
		SET_WALL_TITLE(pagemov)
		
		IF DOES_CHAR_HAVE_FULL_LFI_PAGE(INT_TO_ENUM(LFICharList, iCurrentLFID))
			// Do whichever 'wall' we're looking at - by default, character messages get displayed
			SWITCH LFIWall_CurrentDisplay
				CASE LFIWall_Messages
					SET_POSTED_MESSAGES(pagemov)
				BREAK
				CASE LFIWall_Friends
					SET_FRIENDS_LIST(pagemov)
				BREAK
				CASE LFIWall_Stalking
					SET_STALKING_LIST(pagemov)
				BREAK
			ENDSWITCH
			
			// Now do the sidebar boxes
			SET_SIDEBAR(pagemov)
		ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(pagemov,"UPDATE_TEXT")
		END_SCALEFORM_MOVIE_METHOD()
		
		iPreviousLFID = iCurrentLFID
	ELSE
		CPRINTLN(DEBUG_INTERNET, "No need to update because the LF ID hasn't changed and we're not forcing it...")
	ENDIF

ENDPROC

/// PURPOSE:
///   Adds a Lifeinvader click to the history.
PROC LFI_ADD_PAGE_TO_HISTORY( INT iCharID, LFIWallDisplay currentWall )

	// If we've not filled up the history yet, then just add the current char, what wall post is displaying, and increment the index.
	IF iLFIPageHistoryIndex < (MAX_LFI_HISTORY - 1)

		IF iLFIPageHistoryIndex < 0
			// If the history index is less than 0, reset it to 0 before adding a new page
			CPRINTLN(DEBUG_INTERNET, "LFI - Resetting iLFIPageHistoryIndex to 0")
			iLFIPageHistoryIndex = 0
		ENDIF

		++ iLFIPageHistoryIndex
		iLFIPageHistory[iLFIPageHistoryIndex] = iCharID
		LFIWallHistory[iLFIPageHistoryIndex] = currentWall
		CPRINTLN(DEBUG_INTERNET, "LFI - HISTORY SETTING PAGE ", iCharID, " IN HISTORY INDEX ", iLFIPageHistoryIndex)
		
	// We've got a full history, so we need to shuffle down all the entries, discarding the first.
	ELSE
		
		INT i = 1	// Start at second index
		
		WHILE i < MAX_LFI_HISTORY
			iLFIPageHistory[i - 1] = iLFIPageHistory[i]
			++ i
		ENDWHILE
		
		// Add page to the last entry in the array.
		iLFIPageHistory[iLFIPageHistoryIndex] = iCharID
		CPRINTLN(DEBUG_INTERNET, "LFI - HISTORY FULL. SETTING PAGE ", iCharID, " IN HISTORY INDEX ", iLFIPageHistoryIndex)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Initialises the Liveinvader history
PROC LFI_INIT_HISTORY()

	INT i = 0
	WHILE i < MAX_LFI_HISTORY
		iLFIPageHistory[i] = -1
		++i
	ENDWHILE
	
	iLFIPageHistoryIndex = 0
ENDPROC

PROC HANDLE_LIFEINVADER_PROFILE_BUTTON_CLICK(SCALEFORM_INDEX pagemov, int newID)

	SWITCH newID
		CASE 0
			CPRINTLN(DEBUG_INTERNET, "Back to the player char's profile w/ messages")
			
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()		
				CASE CHAR_MICHAEL
					iCurrentLFID = 0
					CPRINTLN(DEBUG_INTERNET, "iCurrentLFID = ", iCurrentLFID)
				BREAK
				
				CASE CHAR_FRANKLIN
					iCurrentLFID = 1
					CPRINTLN(DEBUG_INTERNET, "iCurrentLFID = ", iCurrentLFID)
				BREAK
				
				CASE CHAR_TREVOR
					iCurrentLFID = 2
					CPRINTLN(DEBUG_INTERNET, "iCurrentLFID = ", iCurrentLFID)
				BREAK
			ENDSWITCH
			LFIWall_CurrentDisplay = LFIWall_Messages // Go back to messages by default
		BREAK
		CASE 1
			CPRINTLN(DEBUG_INTERNET, "The player has managed to click on the photos button somehow...")
		BREAK
		CASE 2
			CPRINTLN(DEBUG_INTERNET, "Show friends")
			LFIWall_CurrentDisplay = LFIWall_Friends
		BREAK
		CASE 3
			CPRINTLN(DEBUG_INTERNET, "Show stalking")
			LFIWall_CurrentDisplay = LFIWall_Stalking
		BREAK
	ENDSWITCH
	
	LFI_ADD_PAGE_TO_HISTORY(iCurrentLFID, LFIWall_CurrentDisplay)
	
	UPDATE_LIFEINVADER_PAGE(pagemov, TRUE) // Force an update even though the ID hasn't changed because we need to change the 'wall'

ENDPROC

PROC CHANGE_LIFEINVADER_PROFILE(SCALEFORM_INDEX pagemov, int newID)

	iCurrentLFID = ENUM_TO_INT(CharWallPostID[newID])
	LFIWall_CurrentDisplay = LFIWall_Messages // Display character's messages first
	
	INT i
	FOR i = 0 TO (MAX_LFI_POSTS-1)
		CharWallPostID[i] = LFI_MAX // Reset the tracked message array so nothing is left hanging around
	ENDFOR
	
	LFI_ADD_PAGE_TO_HISTORY(iCurrentLFID, LFIWall_CurrentDisplay)
	
	TEXT_LABEL tlTemp = GET_LFI_PROFILE_USERNAME(iCurrentLFID)
	CPRINTLN(DEBUG_INTERNET, "Got ", tlTemp ," from message ID ", newID)
	UPDATE_LIFEINVADER_PAGE(pagemov)

ENDPROC

PROC RESET_LIFEINVADER()

	iCurrentLFID = -1
	iPreviousLFID = -1
	iLastSlot = -1
	
	LFIWall_CurrentDisplay = LFIWall_Messages // Display messages by default
	
	INT i
	FOR i = 0 TO (MAX_LFI_POSTS-1)
		CharWallPostID[i] = LFI_MAX // Reset the tracked message array
	ENDFOR
	
	LFI_INIT_HISTORY()

ENDPROC

PROC DO_LIFEINVADER(SCALEFORM_INDEX pagemov, BOOL &bTextOK)
	
	IF iCurrentLFID = -1 
		CPRINTLN(DEBUG_INTERNET, "Initialising Lifeinvader...")
		
		LFI_INIT_HISTORY()
		
		IF HAS_ADDITIONAL_TEXT_LOADED(SHOP_TEXT_SLOT)
			CPRINTLN(DEBUG_INTERNET, "Clearing SHOP_TEXT_SLOT on Lifeinvader init")
			CLEAR_ADDITIONAL_TEXT(SHOP_TEXT_SLOT, FALSE)
		ENDIF
		
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			// NOTE: We are going to store all the Lifeinvader text in the shop text slot because it's not being used anymore and it can't be loaded at all times
		
			CASE CHAR_MICHAEL
				iCurrentLFID = 0
				// using bIsInternetTextSupported as return value for REQUEST_ADDITIONAL_TEXT commented out for now incase it's needed again at a later date
				//bTextOK = REQUEST_ADDITIONAL_TEXT("LFI_M", SHOP_TEXT_SLOT)
				REQUEST_ADDITIONAL_TEXT("LFI_M", SHOP_TEXT_SLOT)
				bTextOK = TRUE
				CPRINTLN(DEBUG_INTERNET, "iCurrentLFID = ", iCurrentLFID)
			BREAK
			
			CASE CHAR_FRANKLIN
				iCurrentLFID = 1
				// using bIsInternetTextSupported as return value for REQUEST_ADDITIONAL_TEXT commented out for now incase it's needed again at a later date
				//bTextOK = REQUEST_ADDITIONAL_TEXT("LFI_F", SHOP_TEXT_SLOT)
				REQUEST_ADDITIONAL_TEXT("LFI_F", SHOP_TEXT_SLOT)
				bTextOK = TRUE
				CPRINTLN(DEBUG_INTERNET, "iCurrentLFID = ", iCurrentLFID)
			BREAK
			
			CASE CHAR_TREVOR
				iCurrentLFID = 2
				// using bIsInternetTextSupported as return value for REQUEST_ADDITIONAL_TEXT commented out for now incase it's needed again at a later date
				//bTextOK = REQUEST_ADDITIONAL_TEXT("LFI_T", SHOP_TEXT_SLOT)
				REQUEST_ADDITIONAL_TEXT("LFI_T", SHOP_TEXT_SLOT)
				bTextOK = TRUE
				CPRINTLN(DEBUG_INTERNET, "iCurrentLFID = ", iCurrentLFID)
			BREAK
		ENDSWITCH
		
		LFIWall_CurrentDisplay = LFIWall_Messages
		
		LFI_ADD_PAGE_TO_HISTORY(iCurrentLFID, LFIWall_CurrentDisplay)
		
	ELSE
		UPDATE_LIFEINVADER_PAGE(pagemov)
	ENDIF

ENDPROC

/// PURPOSE:
///    Gets the next page in the Lifeinvader history
/// RETURNS:
///    The previous page number, or 0 if the end of the history is reached.
FUNC INT LFI_GET_PREVIOUS_PAGE_IN_HISTORY()
	
	IF iLFIPageHistoryIndex <= 0
		CPRINTLN(DEBUG_INTERNET, "LFI - REACHED END OF HISTORY")
		RETURN 0
	ELSE
		CPRINTLN(DEBUG_INTERNET, "LFI - HISTORY RETURNING ", iLFIPageHistory[iLFIPageHistoryIndex], " AS PREVIOUS PAGE. HISTORY INDEX ",iLFIPageHistoryIndex)
		RETURN iLFIPageHistory[iLFIPageHistoryIndex]
	ENDIF
ENDFUNC

/// PURPOSE:
///    Gets the next page in the Lifeinvader history
/// RETURNS:
///    The previous page number, or 0 if the end of the history is reached.
FUNC LFIWallDisplay LFI_GET_PREVIOUS_WALL_IN_HISTORY()
	
	IF iLFIPageHistoryIndex <= 0
		CPRINTLN(DEBUG_INTERNET, "LFI - REACHED END OF HISTORY")
		RETURN LFIWall_CurrentDisplay
	ELSE
		CPRINTLN(DEBUG_INTERNET, "LFI - HISTORY RETURNING ", iLFIPageHistory[iLFIPageHistoryIndex], " AS PREVIOUS PAGE. HISTORY INDEX ",iLFIPageHistoryIndex)
		RETURN LFIWallHistory[iLFIPageHistoryIndex]
	ENDIF
ENDFUNC

FUNC BOOL LFI_BACK_BUTTON_INTERCEPT(SCALEFORM_INDEX pagemov)

	CPRINTLN(DEBUG_INTERNET, "Lifeinvader - BACK BUTTON INTERCEPT")
	
	--iLFIPageHistoryIndex
	
	INT iPage = LFI_GET_PREVIOUS_PAGE_IN_HISTORY()
	LFIWallDisplay LFIWall_PrevWall = LFI_GET_PREVIOUS_WALL_IN_HISTORY()
	
	// Zero indicates we've reached the bottom of the page history.
	IF iLFIPageHistoryIndex > 0
		iCurrentLFID = iPage
		LFIWall_CurrentDisplay = LFIWall_PrevWall
		UPDATE_LIFEINVADER_PAGE( pagemov, TRUE )
		
		// B*1478327 - Setting this to FALSE to prevent LFI cancelling webpage (was a Bleeter bug but this system is copied from it)
		// Potentially look at reinstating history functionality if there is time...
		RETURN TRUE
	ENDIF
	
	// If you don't hijack the back button press so the browser can do it's thing, otherwise true
	RETURN FALSE  
ENDFUNC


//*************************************************
// --------------------------------------------
// LIFEINVADER STUFF ENDS 
// --------------------------------------------
//*************************************************
