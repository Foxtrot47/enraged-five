USING "rage_builtins.sch"
USING "globals.sch"
//USING "comms_control_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	randomChar_Private_GTA5.sch
//		CREATED			:	Keith
//		MAINTAINED		: 	Andrew Minghella
//		DESCRIPTION		:	Contains all Random Character private functions.
//		NOTES			:	Mission Specific Data
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// PURPOSE: Sets up the static mission details struct for the specified mission ID
PROC Fill_RC_Static_Mission_Struct(g_structRCMissionsStatic &paramDetails, STRING paramScriptName, TEXT_LABEL_7 tRCNameLabel, INT paramStatVariation, g_eRC_MissionStrands paramRCStrand, g_eRC_MissionGroupIDs paramRCGroup, VECTOR paramCoords, 
											BLIP_SPRITE paramBlipSprite, STRING paramBlipHelp, FLOW_FLAG_IDS paramFlowFlag, INT paramHours, 
											STRING paramAmbScript, CC_CodeID paramCodeID, BOOL paramActivateViaFlow, g_eRC_MissionIDs paramNextRC, g_eRC_MissionGroupIDs paramNextRCGroup, INT paramPlayableChars, INT paramStartTime, INT paramEndTime, BOOL bRepeatable, BOOL bBlockedInExile, BOOL bUsesShitskips)
											
	paramDetails.rcStrandID					= paramRCStrand
	paramDetails.rcScriptName 				= paramScriptName
	paramDetails.tRCNameLabel				= tRCNameLabel
	paramDetails.rcStatVariation			= paramStatVariation
	paramDetails.rcMissionGroup				= paramRCGroup
	paramDetails.rcCoords 					= paramCoords
	paramDetails.rcBlipSprite				= paramBlipSprite
	paramDetails.rcBlipHelp					= paramBlipHelp
	paramDetails.rcFlowFlagReq 				= paramFlowFlag
	paramDetails.rcHoursToWaitReq 			= paramHours
	paramDetails.rcAmbientPassScript 		= paramAmbScript
	paramDetails.rcPassCodeID				= paramCodeID
	paramDetails.rcMustBeActivatedInFlow	= paramActivateViaFlow
	paramDetails.rcNextMission 				= paramNextRC
	paramDetails.rcNextMissionGroup			= paramNextRCGroup
	paramDetails.rcPlayableChars			= paramPlayableChars
	paramDetails.rcStartTime				= paramStartTime
	paramDetails.rcEndTime					= paramEndTime
	paramDetails.bRCRepeatable				= bRepeatable
	paramDetails.bBlockedInExile            = bBlockedInExile
	paramDetails.bUsesShitskips				= bUsesShitskips
ENDPROC

// PURPOSE: Resets all Random Character stored mission details
PROC Reset_Random_Character_Stored_Mission_Details()
	CPRINTLN(DEBUG_RANDOM_CHAR, "Reset_Random_Character_Stored_Mission_Details called")
	
	// Cycle through every random character mission and reset all data
	INT iMission
	REPEAT MAX_RC_MISSIONS iMission
		// Unsaved dynamic data
		g_RandomChars[iMission].rcPassed = FALSE
		g_RandomChars[iMission].rcFailed = FALSE
		g_RandomChars[iMission].rcIsRunning = FALSE
		g_RandomChars[iMission].rcMissionCandidateID = -1 // Has to be -1 for the mission candidate system to regsiter the mission
		g_RandomChars[iMission].rcLeaveAreaCheck = FALSE
		
		// Saved Data
		g_savedGlobals.sRandomChars.savedRC[iMission].rcFlags = 0
		g_savedGlobals.sRandomChars.savedRC[iMission].rcTimeReqSet = FALSE
		g_savedGlobals.sRandomChars.savedRC[iMission].iFailsNoProgress =0
	ENDREPEAT
	
	// RC event flags
	g_savedGlobals.sRandomChars.savedRCEvents = 0
	g_savedGlobals.sRandomChars.iRCMissionsCompleted = 0
	g_savedGlobals.sRandomChars.g_iCurrentEpsilonPayment = 0
ENDPROC

// PURPOSE: Sets up the dynamic details for the Random Character missions
PROC Initialise_Random_Character_Dynamic_Mission_Details()
	CPRINTLN(DEBUG_RANDOM_CHAR, "Initialise_Random_Character_Dynamic_Mission_Details called")
	
	// Cycle through every random character mission and initialise the dynamic data
	INT iMission
	REPEAT MAX_RC_MISSIONS iMission
		// Unsaved dynamic data
		g_RandomChars[iMission].rcPassed = FALSE
		g_RandomChars[iMission].rcFailed = FALSE
		g_RandomChars[iMission].rcIsRunning = FALSE
		g_RandomChars[iMission].rcBlipState = RC_BLIP_TURNED_OFF
		g_RandomChars[iMission].rcBlipIndex = -1		  // Has to be -1 so the random character controller knows to request a new blip index
		g_RandomChars[iMission].rcMissionCandidateID = -1 // Has to be -1 for the mission candidate system to regsiter the mission
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Returns the stat ID for this RC mission. Used in bugstar data lookup
/// PARAMS:
///    eRCMission - the RC mission we want the stat ID of
/// RETURNS:
///    TEXT_LABEL_7 stat ID of this RC mission
FUNC TEXT_LABEL_7 GET_RC_STAT_ID(g_eRC_MissionIDs eRCMission)

	TEXT_LABEL_7 tStatID = ""
	
		SWITCH eRCMission
	
		// ABIGAIL - SUBMARINE SCRAPS				
		CASE RC_ABIGAIL_1  			tStatID = "ABI1" BREAK
		CASE RC_ABIGAIL_2  			tStatID = "ABI2" BREAK
		
		// BARRY					
		CASE RC_BARRY_1  			tStatID = "BA1"  BREAK
		CASE RC_BARRY_2  			tStatID = "BA2"  BREAK
		CASE RC_BARRY_3  			tStatID = "BA3"  BREAK
		CASE RC_BARRY_3A  			tStatID = "BA3A" BREAK
		CASE RC_BARRY_3C  			tStatID = "BA3C" BREAK
		CASE RC_BARRY_4  			tStatID = "BA4"  BREAK
		
		// DREYFUSS		
		CASE RC_DREYFUSS_1  		tStatID = "DRE1" BREAK
		
		// EPSILONISM	
		CASE RC_EPSILON_1  			tStatID = "EPS1" BREAK
		CASE RC_EPSILON_2  			tStatID = "EPS2" BREAK
		CASE RC_EPSILON_3  			tStatID = "EPS3" BREAK
		CASE RC_EPSILON_4  			tStatID = "EPS4" BREAK
		CASE RC_EPSILON_5  			tStatID = "EPS5" BREAK
		CASE RC_EPSILON_6  			tStatID = "EPS6" BREAK
		CASE RC_EPSILON_7  			tStatID = "EPS7" BREAK
		CASE RC_EPSILON_8  			tStatID = "EPS8" BREAK
		
		// EXTREME 
		CASE RC_EXTREME_1  			tStatID = "EXT1" BREAK
		CASE RC_EXTREME_2  			tStatID = "EXT2" BREAK
		CASE RC_EXTREME_3  			tStatID = "EXT3" BREAK
		CASE RC_EXTREME_4  			tStatID = "EXT4" BREAK
		
		// FANATIC
		CASE RC_FANATIC_1  			tStatID = "FAN1" BREAK
		CASE RC_FANATIC_2  			tStatID = "FAN2" BREAK
		CASE RC_FANATIC_3  			tStatID = "FAN3" BREAK
		
		// HAO - STREET RACES
		CASE RC_HAO_1				tStatID = "HAO1" BREAK
		
		// HUNTING		
		CASE RC_HUNTING_1			tStatID = "HUN1" BREAK
		CASE RC_HUNTING_2			tStatID = "HUN2" BREAK
		
		// JOSH	
		CASE RC_JOSH_1				tStatID = "JOS1" BREAK
		CASE RC_JOSH_2				tStatID = "JOS2" BREAK
		CASE RC_JOSH_3				tStatID = "JOS3" BREAK
		CASE RC_JOSH_4				tStatID = "JOS4" BREAK
		
		// MAUDE - BAIL BONDS			
		CASE RC_MAUDE_1  			tStatID = "MAU1" BREAK
		
		// MINUTE MAN
		CASE RC_MINUTE_1			tStatID = "MIN1" BREAK
		CASE RC_MINUTE_2    		tStatID = "MIN2" BREAK
		CASE RC_MINUTE_3			tStatID = "MIN3" BREAK
		
		// MRS. PHILIPS	
		CASE RC_MRS_PHILIPS_1  		tStatID = "MRS1" BREAK
		CASE RC_MRS_PHILIPS_2  		tStatID = "MRS2" BREAK
	
		// NIGEL
		CASE RC_NIGEL_1				tStatID = "NI1"  BREAK
		CASE RC_NIGEL_1A			tStatID = "NI1A" BREAK
		CASE RC_NIGEL_1B			tStatID = "NI1B" BREAK
		CASE RC_NIGEL_1C			tStatID = "NI1C" BREAK
		CASE RC_NIGEL_1D			tStatID = "NI1D" BREAK
		CASE RC_NIGEL_2   			tStatID = "NI2"  BREAK
		CASE RC_NIGEL_3   			tStatID = "NI3"  BREAK
		
		// OMEGA	
		CASE RC_OMEGA_1  			tStatID = "OME1" BREAK
		CASE RC_OMEGA_2  			tStatID = "OME2" BREAK
		
		// PAPARAZZO	
		CASE RC_PAPARAZZO_1			tStatID = "PA1"  BREAK
		CASE RC_PAPARAZZO_2 		tStatID = "PA2"  BREAK
		CASE RC_PAPARAZZO_3			tStatID = "PA3"  BREAK
		CASE RC_PAPARAZZO_3A		tStatID = "PA3A" BREAK
		CASE RC_PAPARAZZO_3B		tStatID = "PA3B" BREAK
		CASE RC_PAPARAZZO_4			tStatID = "PA4"  BREAK
		
		// RAMPAGES
		CASE RC_RAMPAGE_1			tStatID = "RAM1" BREAK
		CASE RC_RAMPAGE_2			tStatID = "RAM2" BREAK
		CASE RC_RAMPAGE_3			tStatID = "RAM3" BREAK
		CASE RC_RAMPAGE_4			tStatID = "RAM4" BREAK
		CASE RC_RAMPAGE_5			tStatID = "RAM5" BREAK
		
		// SASQUATCH
		CASE RC_THELASTONE			tStatID = "SAS1" BREAK
		
		// TONYA
		CASE RC_TONYA_1				tStatID = "TON1" BREAK
		CASE RC_TONYA_2				tStatID = "TON2" BREAK
		CASE RC_TONYA_3				tStatID = "TON3" BREAK
		CASE RC_TONYA_4				tStatID = "TON4" BREAK
		CASE RC_TONYA_5				tStatID = "TON5" BREAK
		
		DEFAULT
			SCRIPT_ASSERT("ERROR: GET_RC_STAT_ID has been passed an illegal Random Character mission ID")
		BREAK
	ENDSWITCH
	
	RETURN tStatID
ENDFUNC



/// PURPOSE:
///    Returns the Mission name Label for this RC mission 
/// PARAMS:
///    eRCMission - RC mission ID
/// RETURNS:
///    TEXT_LABEL_7 Mission name Label for this RC mission
FUNC TEXT_LABEL_7 GET_RC_MISSION_NAME_LABEL(g_eRC_MissionIDs eRCMission)

	TEXT_LABEL_7 tMissionNameLabel = ""
	TEXT_LABEL_7 tStatID
	
	tStatID = GET_RC_STAT_ID(eRCMission)
	IF IS_STRING_NULL_OR_EMPTY(tStatID)
		SCRIPT_ASSERT("ERROR: GET_RC_MISSION_NAME_LABEL has been passed an illegal Random Character mission ID")
	ELSE
		tMissionNameLabel = "RC_"
		tMissionNameLabel += tStatID
	ENDIF
	
	RETURN tMissionNameLabel
ENDFUNC

/// PURPOSE:
///    Returns whether this RC mission can be played in Repeat Play
/// PARAMS:
///    eRCMission - RC mission ID
/// RETURNS:
///    TRUE if it can be repeat played, false otherwise
FUNC BOOL IS_RC_REPEATABLE(g_eRC_MissionIDs eRCMission)

	SWITCH eRCMission
	
		// ABIGAIL - SONAR COLLECTIONS				
		CASE RC_ABIGAIL_1		RETURN FALSE BREAK
		CASE RC_ABIGAIL_2		RETURN FALSE BREAK

		// BARRY					
		CASE RC_BARRY_1			RETURN TRUE  BREAK
		CASE RC_BARRY_2			RETURN TRUE  BREAK
		CASE RC_BARRY_3  		RETURN FALSE BREAK
		CASE RC_BARRY_3A  		RETURN TRUE  BREAK
		CASE RC_BARRY_3C  		RETURN TRUE  BREAK
		CASE RC_BARRY_4  		RETURN FALSE BREAK
		
		// DREYFUSS		
		CASE RC_DREYFUSS_1  	RETURN TRUE BREAK
		
		// EPSILONISM	
		CASE RC_EPSILON_1  		RETURN FALSE BREAK
		CASE RC_EPSILON_2  		RETURN FALSE BREAK
		CASE RC_EPSILON_3  		RETURN FALSE BREAK
		CASE RC_EPSILON_4  		RETURN TRUE  BREAK
		CASE RC_EPSILON_5  		RETURN FALSE BREAK
		CASE RC_EPSILON_6  		RETURN TRUE  BREAK
		CASE RC_EPSILON_7  		RETURN FALSE BREAK
		CASE RC_EPSILON_8  		RETURN TRUE  BREAK
		
		// EXTREME 
		CASE RC_EXTREME_1  		RETURN TRUE BREAK
		CASE RC_EXTREME_2 	 	RETURN TRUE BREAK
		CASE RC_EXTREME_3 	 	RETURN TRUE BREAK
		CASE RC_EXTREME_4 	 	RETURN TRUE BREAK
		
		// FANATIC
		CASE RC_FANATIC_1  		RETURN TRUE BREAK
		CASE RC_FANATIC_2  		RETURN TRUE BREAK
		CASE RC_FANATIC_3  		RETURN TRUE BREAK
		
		// HAO - STREET RACES					
		CASE RC_HAO_1			RETURN TRUE BREAK
		
		// HUNTING		
		CASE RC_HUNTING_1		RETURN TRUE  BREAK
		CASE RC_HUNTING_2		RETURN TRUE  BREAK
		
		// JOSH	
		CASE RC_JOSH_1			RETURN FALSE BREAK
		CASE RC_JOSH_2			RETURN TRUE  BREAK
		CASE RC_JOSH_3			RETURN TRUE  BREAK
		CASE RC_JOSH_4			RETURN TRUE  BREAK
		
		// MAUDE - BAIL BONDS					
		CASE RC_MAUDE_1			RETURN FALSE BREAK
		
		// MINUTE MAN
		CASE RC_MINUTE_1		RETURN TRUE BREAK
		CASE RC_MINUTE_2   	 	RETURN TRUE BREAK
		CASE RC_MINUTE_3		RETURN TRUE BREAK
		
		// MRS. PHILIPS				
		CASE RC_MRS_PHILIPS_1	RETURN FALSE BREAK
		CASE RC_MRS_PHILIPS_2	RETURN FALSE BREAK

		// NIGEL
		CASE RC_NIGEL_1			RETURN FALSE BREAK
		CASE RC_NIGEL_1A		RETURN TRUE  BREAK
		CASE RC_NIGEL_1B		RETURN TRUE  BREAK
		CASE RC_NIGEL_1C		RETURN TRUE  BREAK
		CASE RC_NIGEL_1D		RETURN TRUE  BREAK
		CASE RC_NIGEL_2   		RETURN TRUE  BREAK
		CASE RC_NIGEL_3   		RETURN TRUE  BREAK
		
		// OMEGA	
		CASE RC_OMEGA_1  		RETURN FALSE BREAK
		CASE RC_OMEGA_2  		RETURN FALSE BREAK
		
		// PAPARAZZO	
		CASE RC_PAPARAZZO_1		RETURN TRUE  BREAK
		CASE RC_PAPARAZZO_2 	RETURN TRUE  BREAK
		CASE RC_PAPARAZZO_3		RETURN FALSE BREAK
		CASE RC_PAPARAZZO_3A	RETURN TRUE  BREAK
		CASE RC_PAPARAZZO_3B	RETURN TRUE  BREAK
		CASE RC_PAPARAZZO_4		RETURN TRUE  BREAK
		
		// RAMPAGE	
		CASE RC_RAMPAGE_1		RETURN TRUE BREAK
		CASE RC_RAMPAGE_3		RETURN TRUE BREAK
		CASE RC_RAMPAGE_4		RETURN TRUE BREAK
		CASE RC_RAMPAGE_5		RETURN TRUE BREAK
		CASE RC_RAMPAGE_2		RETURN TRUE BREAK
		
		// SASQUATCH
		CASE RC_THELASTONE		RETURN TRUE BREAK
		
		// TONYA
		CASE RC_TONYA_1			RETURN TRUE BREAK
		CASE RC_TONYA_2			RETURN TRUE BREAK
		CASE RC_TONYA_3			RETURN TRUE BREAK
		CASE RC_TONYA_4			RETURN TRUE BREAK
		CASE RC_TONYA_5			RETURN TRUE BREAK
		
		DEFAULT
			SCRIPT_ASSERT("ERROR: IS_RC_REPEATABLE has been passed an illegal Random Character mission ID")
		BREAK
	ENDSWITCH
	
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    Sets up the static RC mission details
/// PARAMS:
///    paramID - which RC mission you want details of
///    paramDetails - the array you want to populate with the RC mission details
PROC Retrieve_Random_Character_Static_Mission_Details(g_eRC_MissionIDs paramID, g_structRCMissionsStatic &paramDetails)

	// NOTE: IF YOU ARE ADDING OR REMOVING ANY RANDOM CHARACTER MISSIONS PLEASE INFORM ANDY MINGHELLA- AS REPEAT PLAY WILL NEED UPDATING
	SWITCH paramID
	
		// ABIGAIL												    		    ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_ABIGAIL_1  			Fill_RC_Static_Mission_Struct(paramDetails, "Abigail1", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_ABIGAIL,	NO_RC_MISSION_GROUP,		<< -1604.668, 5239.10, 3.01 >>, RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_PURCHASED_MARINA_PROPERTY, 	0, 		"ambient_Diving",		CID_BLANK,							FALSE,			RC_ABIGAIL_2,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_ABIGAIL_2  			Fill_RC_Static_Mission_Struct(paramDetails, "Abigail2", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_ABIGAIL,	NO_RC_MISSION_GROUP,		<< -1592.84, 5214.04, 3.01 >>, 	RADAR_TRACE_ABIGAIL,			"",			FLOWFLAG_DIVING_SCRAPS_DONE, 			0, 		"",						CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK

		// BARRY																ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_BARRY_1  			Fill_RC_Static_Mission_Struct(paramDetails, "Barry1", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_BARRY,		NO_RC_MISSION_GROUP,		<< 190.26, -956.35, 29.63 >>, 	RADAR_TRACE_BARRY,				"",			FLOWFLAG_RES_AND_RCS_UNLOCKED, 			0, 		"",					 	CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_BARRY_2  			Fill_RC_Static_Mission_Struct(paramDetails, "Barry2", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_BARRY,		NO_RC_MISSION_GROUP,		<< 190.26, -956.35, 29.63 >>, 	RADAR_TRACE_BARRY,				"",			FLOWFLAG_NONE, 							0,		"",					 	CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_BARRY_3  			Fill_RC_Static_Mission_Struct(paramDetails, "Barry3", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_BARRY,		NO_RC_MISSION_GROUP,		<< 414.00, -761.00, 29.00 >>, 	RADAR_TRACE_BARRY,				"",			FLOWFLAG_NONE, 							0,		"", 					CID_BARRY3_SEND_TEXT,				TRUE,			NO_RC_MISSION,	RC_GROUP_BARRY_3,			BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_BARRY_3A  			Fill_RC_Static_Mission_Struct(paramDetails, "Barry3A", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		1,			RCS_BARRY,		RC_GROUP_BARRY_3,			<< 1199.27, -1255.63, 34.23 >>, RADAR_TRACE_BARRY,				"BARSTASH",	FLOWFLAG_BARRY3_TEXT_RECEIVED, 			0,		"", 					CID_BARRY4_SEND_TEXT,				FALSE,			RC_BARRY_4,		NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_BARRY_3C  			Fill_RC_Static_Mission_Struct(paramDetails, "Barry3C", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		3,			RCS_BARRY,		RC_GROUP_BARRY_3,			<< -468.90, -1713.06, 18.21 >>, RADAR_TRACE_BARRY,				"",			FLOWFLAG_BARRY3_TEXT_RECEIVED, 			0,		"", 					CID_BARRY4_SEND_TEXT,				FALSE,			RC_BARRY_4,		NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_BARRY_4  			Fill_RC_Static_Mission_Struct(paramDetails, "Barry4", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_BARRY,		NO_RC_MISSION_GROUP,		<< 237.65, -385.41, 44.40 >>, 	RADAR_TRACE_BARRY,				"",			FLOWFLAG_BARRY4_TEXT_RECEIVED, 			0,		"postRC_Barry4", 		CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0800, 2000,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK

		// DREYFUSS													    		ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	    FlowFlag Requirement				Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_DREYFUSS_1  		Fill_RC_Static_Mission_Struct(paramDetails, "Dreyfuss1", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_DREYFUSS,	NO_RC_MISSION_GROUP,		<< -1458.97, 485.99, 115.38 >>, RADAR_TRACE_RANDOM_CHARACTER,	"LETTERS_HINT", FLOWFLAG_LETTER_SCRAPS_DONE, 		0, 		"", 					CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK

		// EPSILONISM														    ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_EPSILON_1  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon1",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< -1622.89, 4204.87, 83.30 >>, RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE,	0, 		"", 					CID_BLANK,							TRUE,			RC_EPSILON_2,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_EPSILON_2  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon2",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< 242.70, 362.70, 104.74 >>,   RADAR_TRACE_EPSILON,     		"",			FLOWFLAG_EPSILON_DONATED_500, 			16,		"", 					CID_BLANK,							FALSE,			RC_EPSILON_3,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,	    0000, 2359,		    IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_EPSILON_3  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon3",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< 1835.53, 4705.86, 38.1 >>, 	RADAR_TRACE_EPSILON,			"",			FLOWFLAG_EPSILON_DONATED_5000, 			16,		"epsCars", 				CID_BLANK,							FALSE,			RC_EPSILON_4,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_EPSILON_4  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon4",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< 1826.13, 4698.88, 38.92 >>, 	RADAR_TRACE_EPSILON,			"",			FLOWFLAG_EPSILON_CARS_DONE,				16,		"postRC_Epsilon4",		CID_BLANK,							FALSE,			RC_EPSILON_5,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_EPSILON_5  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon5",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< 637.02, 119.7093, 89.50 >>, 	RADAR_TRACE_EPSILON,			"",			FLOWFLAG_EPSILON_DONATED_10000, 		16,		"epsRobes", 			CID_BLANK,							FALSE,			RC_EPSILON_6,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_EPSILON_6  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon6",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<<-2892.93, 3192.37, 11.66>>,   RADAR_TRACE_EPSILON,			"",			FLOWFLAG_EPSILON_6_TEXT_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_EPSILON_7,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK		
		CASE RC_EPSILON_7  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon7",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< 524.43, 3079.82, 39.48 >>, 	RADAR_TRACE_EPSILON,			"",			FLOWFLAG_NONE,							16,		"epsDesert", 			CID_BLANK,							FALSE,			RC_EPSILON_8,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_EPSILON_8  			Fill_RC_Static_Mission_Struct(paramDetails, "Epsilon8",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EPSILON,	NO_RC_MISSION_GROUP,		<< -697.75, 45.38, 43.03 >>, 	RADAR_TRACE_EPSILON,			"",			FLOWFLAG_EPSILON_DESERT_DONE, 			16,		"epsilonTract", 		CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK

		// EXTREME SPORTS														ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_EXTREME_1  			Fill_RC_Static_Mission_Struct(paramDetails, "Extreme1",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EXTREME,	NO_RC_MISSION_GROUP,		<< -188.22, 1296.10, 302.86 >>, RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"", 					CID_ACTIVATE_MINIGAME_BASE_JUMP,	TRUE,			RC_EXTREME_2,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_EXTREME_2  			Fill_RC_Static_Mission_Struct(paramDetails, "Extreme2",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EXTREME,	NO_RC_MISSION_GROUP,		<< -954.19, -2760.05, 14.64 >>, RADAR_TRACE_DOM,				"",			FLOWFLAG_EXTREME2_TEXT_RECEIVED, 		0,		"", 					CID_EXTREME3_SEND_DOM_TEXT,			FALSE,			RC_EXTREME_3,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_EXTREME_3  			Fill_RC_Static_Mission_Struct(paramDetails, "Extreme3",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EXTREME,	NO_RC_MISSION_GROUP,		<< -63.8, -809.5, 321.8 >>, 	RADAR_TRACE_DOM,				"",			FLOWFLAG_EXTREME3_TEXT_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_EXTREME_4,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_EXTREME_4  			Fill_RC_Static_Mission_Struct(paramDetails, "Extreme4",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_EXTREME,	NO_RC_MISSION_GROUP,		<< 1731.41, 96.96, 170.39 >>,   RADAR_TRACE_DOM,				"",			FLOWFLAG_EXTREME4_BJUMPS_FINISHED, 		16,		"", 					CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		
		// EXERCISE FANATIC														ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_FANATIC_1  			Fill_RC_Static_Mission_Struct(paramDetails, "Fanatic1",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_FANATIC,	NO_RC_MISSION_GROUP,		<< -1877.82, -440.649, 45.05>>, RADAR_TRACE_FANATIC,			"",			FLOWFLAG_RES_AND_RCS_UNLOCKED, 			0, 		"", 					CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_MICHAEL,		0700, 2000,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_FANATIC_2  			Fill_RC_Static_Mission_Struct(paramDetails, "Fanatic2",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_FANATIC,	NO_RC_MISSION_GROUP,		<< 809.66, 1279.76, 360.49 >>, 	RADAR_TRACE_FANATIC,			"",			FLOWFLAG_NONE, 							0,		"", 					CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR, 		0700, 2000,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_FANATIC_3  			Fill_RC_Static_Mission_Struct(paramDetails, "Fanatic3",   	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_FANATIC,	NO_RC_MISSION_GROUP,		<< -915.6, 6139.2, 5.5 >>, 		RADAR_TRACE_FANATIC,			"",			FLOWFLAG_NONE, 							0,		"", 					CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0700, 2000,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		
		// HAO - STREET RACES												    ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_HAO_1 				Fill_RC_Static_Mission_Struct(paramDetails, "Hao1", 		GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_HAO,		NO_RC_MISSION_GROUP,		<<-72.29, -1260.63, 28.14>>, 	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"controller_Races",		CID_ACTIVATE_MINIGAME_STREET_RACES,	TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		2000, 0500,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		
		// HUNTING																ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_HUNTING_1			Fill_RC_Static_Mission_Struct(paramDetails, "Hunting1", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_HUNTING,	NO_RC_MISSION_GROUP,		<< 1804.32, 3931.33, 32.82 >>,	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"", 					CID_HUNTING1_SEND_TEXT,				TRUE,			RC_HUNTING_2,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_HUNTING_2			Fill_RC_Static_Mission_Struct(paramDetails, "Hunting2", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_HUNTING,	NO_RC_MISSION_GROUP,		<<-684.17, 5839.16, 16.09 >>,	RADAR_TRACE_CLETUS,				"",			FLOWFLAG_HUNTING1_TEXT_RECEIVED, 		0,		"", 					CID_ACTIVATE_MINIGAME_HUNTING,		FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK

		// JOSH																    ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_JOSH_1				Fill_RC_Static_Mission_Struct(paramDetails, "Josh1", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_JOSH,		NO_RC_MISSION_GROUP,		<< -1104.93, 291.25, 64.30 >>, 	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"forSaleSigns", 		CID_BLANK,							TRUE,			RC_JOSH_2,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_JOSH_2				Fill_RC_Static_Mission_Struct(paramDetails, "Josh2", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_JOSH,		NO_RC_MISSION_GROUP,		<< 565.39, -1772.88, 29.77 >>,	RADAR_TRACE_JOSH,				"",			FLOWFLAG_FOR_SALE_SIGNS_DESTROYED, 		0, 		"", 					CID_BLANK,							FALSE,			RC_JOSH_3,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_JOSH_3				Fill_RC_Static_Mission_Struct(paramDetails, "Josh3", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_JOSH,		NO_RC_MISSION_GROUP,		<< 565.39, -1772.88, 29.77 >>,	RADAR_TRACE_JOSH,				"",			FLOWFLAG_NONE, 							16,		"", 					CID_BLANK,							FALSE,			RC_JOSH_4,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_JOSH_4				Fill_RC_Static_Mission_Struct(paramDetails, "Josh4", 	  	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_JOSH,		NO_RC_MISSION_GROUP,		<< -1104.93, 291.25, 64.30 >>, 	RADAR_TRACE_JOSH,				"",			FLOWFLAG_NONE, 							36, 	"", 					CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK

		// MAUDE																ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_MAUDE_1				Fill_RC_Static_Mission_Struct(paramDetails, "Maude1",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_MAUDE,		NO_RC_MISSION_GROUP,		<< 2726.1, 4145, 44.3 >>,		RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"BailBond_Launcher", 	CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK

		// MINUTE MAN BLUES													    ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_MINUTE_1			Fill_RC_Static_Mission_Struct(paramDetails, "Minute1",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_MINUTE,		NO_RC_MISSION_GROUP,		<< 327.85, 3405.70, 35.73 >>,	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"", 					CID_BLANK,							TRUE,			RC_MINUTE_2,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK
		CASE RC_MINUTE_2    		Fill_RC_Static_Mission_Struct(paramDetails, "Minute2",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_MINUTE,		NO_RC_MISSION_GROUP,		<< 18.00, 4527.00, 105.00 >>,	RADAR_TRACE_MINUTE,				"",			FLOWFLAG_NONE, 							10,		"", 					CID_BLANK,							FALSE,			RC_MINUTE_3,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK
		CASE RC_MINUTE_3			Fill_RC_Static_Mission_Struct(paramDetails, "Minute3",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_MINUTE,		NO_RC_MISSION_GROUP,		<<-303.82,6211.29,31.05>>,		RADAR_TRACE_MINUTE,				"",			FLOWFLAG_NONE, 							10,		"", 					CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK

		// MRS. PHILIPS												    		ScriptName	  	NameLabel								StatVar		Strand			 RCGroup					BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup				    Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_MRS_PHILIPS_1		Fill_RC_Static_Mission_Struct(paramDetails, "MrsPhilips1", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_MRS_PHILIPS, NO_RC_MISSION_GROUP,		<<1972.59, 3816.43, 32.42>>, 	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"ambient_MrsPhilips", 	CID_BLANK,							TRUE,			NO_RC_MISSION,  NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_MRS_PHILIPS_2		Fill_RC_Static_Mission_Struct(paramDetails, "MrsPhilips2", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_MRS_PHILIPS, NO_RC_MISSION_GROUP,		<<0, 0, 0>>, 					RADAR_TRACE_INVALID,			"",			FLOWFLAG_NONE, 							0, 		"", 					CID_BLANK,							TRUE,			NO_RC_MISSION,  NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK

		// NIGEL															    ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_NIGEL_1				Fill_RC_Static_Mission_Struct(paramDetails, "Nigel1",     	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		NO_RC_MISSION_GROUP,		<< -1097.16, 790.01, 164.52 >>, RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"", 					CID_NIGEL1_SEND_EMAIL,				TRUE,			NO_RC_MISSION,	RC_GROUP_NIGEL_1,			BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_NIGEL_1A			Fill_RC_Static_Mission_Struct(paramDetails, "Nigel1A",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		RC_GROUP_NIGEL_1,			<< -558.65, 284.49, 90.86 >>, 	RADAR_TRACE_CELEBRITY_THEFT,	"NIGITEMS",	FLOWFLAG_NIGEL1_EMAIL_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_NIGEL_2,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_NIGEL_1B			Fill_RC_Static_Mission_Struct(paramDetails, "Nigel1B",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		RC_GROUP_NIGEL_1,			<< -1034.15, 366.08, 80.11 >>, 	RADAR_TRACE_CELEBRITY_THEFT,	"",			FLOWFLAG_NIGEL1_EMAIL_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_NIGEL_2,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0700, 2000,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_NIGEL_1C			Fill_RC_Static_Mission_Struct(paramDetails, "Nigel1C",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		RC_GROUP_NIGEL_1,			<< -623.91, -266.17, 37.76 >>, 	RADAR_TRACE_CELEBRITY_THEFT,	"",			FLOWFLAG_NIGEL1_EMAIL_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_NIGEL_2,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0700, 2000,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_NIGEL_1D			Fill_RC_Static_Mission_Struct(paramDetails, "Nigel1D",    	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		RC_GROUP_NIGEL_1,			<< -1096.85, 67.68, 52.95 >>,   RADAR_TRACE_CELEBRITY_THEFT,	"",			FLOWFLAG_NIGEL1_EMAIL_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_NIGEL_2,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0700, 2000,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		CASE RC_NIGEL_2   			Fill_RC_Static_Mission_Struct(paramDetails, "Nigel2",     	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		NO_RC_MISSION_GROUP,		<< -1310.70, -640.22, 26.54 >>,	RADAR_TRACE_CELEBRITY_THEFT,	"",			FLOWFLAG_NONE, 							8,		"", 					CID_BLANK,							FALSE,			RC_NIGEL_3,		NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE) BREAK
		CASE RC_NIGEL_3   			Fill_RC_Static_Mission_Struct(paramDetails, "Nigel3",     	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_NIGEL,		NO_RC_MISSION_GROUP,		<< -44.75, -1288.67, 28.21 >>,	RADAR_TRACE_CELEBRITY_THEFT,	"",			FLOWFLAG_NONE, 							16,		"postRC_Nigel3", 		CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	TRUE)  BREAK
		
		// OMEGA												    			ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript	 		CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_OMEGA_1  			Fill_RC_Static_Mission_Struct(paramDetails, "Omega1", 	 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_OMEGA,		NO_RC_MISSION_GROUP,		<< 2468.51, 3437.39, 49.90 >>, 	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"spaceshipParts",		CID_BLANK,							TRUE,			RC_OMEGA_2,		NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_OMEGA_2  			Fill_RC_Static_Mission_Struct(paramDetails, "Omega2", 	 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_OMEGA,		NO_RC_MISSION_GROUP,		<< 2319.44, 2583.58, 46.76 >>, 	RADAR_TRACE_OMEGA,				"",			FLOWFLAG_SPACESHIP_PARTS_DONE, 			0, 		"", 			 	    CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK

		// PAPARAZZO															ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_PAPARAZZO_1			Fill_RC_Static_Mission_Struct(paramDetails, "Paparazzo1", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_PAPARAZZO,	NO_RC_MISSION_GROUP,		<< -149.75, 285.81, 93.67 >>,	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0, 		"", 					CID_BLANK,							TRUE,			RC_PAPARAZZO_2,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_PAPARAZZO_2 		Fill_RC_Static_Mission_Struct(paramDetails, "Paparazzo2", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_PAPARAZZO,	NO_RC_MISSION_GROUP,		<< -70.71, 301.43, 106.79>>, 	RADAR_TRACE_PAPARAZZO,			"",			FLOWFLAG_NONE, 							8,		"", 					CID_BLANK,							FALSE,			RC_PAPARAZZO_3,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_PAPARAZZO_3			Fill_RC_Static_Mission_Struct(paramDetails, "Paparazzo3", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_PAPARAZZO,	NO_RC_MISSION_GROUP,		<< -257.22, 292.85, 90.63 >>, 	RADAR_TRACE_PAPARAZZO,			"",			FLOWFLAG_NONE, 							8,		"", 					CID_PAPARAZZO3_SEND_TEXT,			TRUE,			NO_RC_MISSION,	RC_GROUP_PAPARAZZO_3,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_PAPARAZZO_3A		Fill_RC_Static_Mission_Struct(paramDetails, "Paparazzo3A", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_PAPARAZZO,	RC_GROUP_PAPARAZZO_3,		<< 305.52, 157.19, 102.94>>, 	RADAR_TRACE_PAPARAZZO,			"PAPPHOTO",	FLOWFLAG_PAPARAZZO3_TEXT_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_PAPARAZZO_4,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_PAPARAZZO_3B		Fill_RC_Static_Mission_Struct(paramDetails, "Paparazzo3B", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_PAPARAZZO,	RC_GROUP_PAPARAZZO_3,		<< 1040.96, -534.42, 60.17 >>,  RADAR_TRACE_PAPARAZZO,			"",			FLOWFLAG_PAPARAZZO3_TEXT_RECEIVED, 		0,		"", 					CID_BLANK,							FALSE,			RC_PAPARAZZO_4,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		CASE RC_PAPARAZZO_4			Fill_RC_Static_Mission_Struct(paramDetails, "Paparazzo4", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_PAPARAZZO,	NO_RC_MISSION_GROUP,		<< -484.20, 229.68, 82.21 >>, 	RADAR_TRACE_PAPARAZZO,			"",			FLOWFLAG_NONE, 							8,		"", 					CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		
		// RAMPAGES																ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile
		CASE RC_RAMPAGE_1			Fill_RC_Static_Mission_Struct(paramDetails, "Rampage1", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_RAMPAGE,	NO_RC_MISSION_GROUP,		<< 908.00, 3643.70, 32.20 >>,	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_NONE, 							0,		"", 					CID_BLANK,							TRUE,			RC_RAMPAGE_3,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_RAMPAGE_3			Fill_RC_Static_Mission_Struct(paramDetails, "Rampage3", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_RAMPAGE,	NO_RC_MISSION_GROUP,		<< 465.10, -1849.30, 27.80 >>,	RADAR_TRACE_RAMPAGE,			"",			FLOWFLAG_NONE, 							0,		"", 					CID_BLANK,							TRUE,			RC_RAMPAGE_4,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_RAMPAGE_4			Fill_RC_Static_Mission_Struct(paramDetails, "Rampage4", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_RAMPAGE,	NO_RC_MISSION_GROUP,		<< -161.00, -1669.70, 33.00 >>,	RADAR_TRACE_RAMPAGE,			"",			FLOWFLAG_NONE, 							0,		"", 					CID_BLANK,							FALSE,			RC_RAMPAGE_5,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
		CASE RC_RAMPAGE_5			Fill_RC_Static_Mission_Struct(paramDetails, "Rampage5", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_RAMPAGE,	NO_RC_MISSION_GROUP,		<< -1298.20, 2504.14, 21.09 >>,	RADAR_TRACE_RAMPAGE,			"",			FLOWFLAG_NONE, 							0,		"", 					CID_BLANK,							FALSE,			RC_RAMPAGE_2,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	FALSE) BREAK
		CASE RC_RAMPAGE_2			Fill_RC_Static_Mission_Struct(paramDetails, "Rampage2", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_RAMPAGE,	NO_RC_MISSION_GROUP,		<< 1181.50, -400.10, 67.50 >>,	RADAR_TRACE_RAMPAGE,			"",			FLOWFLAG_NONE, 							0,		"rampage_controller", 	CID_BLANK,							FALSE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_TREVOR,			0000, 2359,			IS_RC_REPEATABLE(paramID), TRUE,	FALSE) BREAK
	
		// SASQUATCH															ScriptName	  	NameLabel								StatVar		Strand			RCGroup						BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup					Playable Chars		Start/End Time		Repeatable				   Exile	Shitskips
		CASE RC_THELASTONE			Fill_RC_Static_Mission_Struct(paramDetails, "TheLastOne", 	GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_THELASTONE,	NO_RC_MISSION_GROUP,		<< -1298.98, 4640.16, 105.67>>,	RADAR_TRACE_RANDOM_CHARACTER,	"",			FLOWFLAG_GAME_100_PERCENT_COMPLETE, 	1,		"", 					CID_BLANK,							TRUE,			NO_RC_MISSION,	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE)  BREAK
		
		// TONYA													    		ScriptName	  	NameLabel								StatVar		Strand			 RCGroup					BlipCoords						BlipSprite						Blip Help	FlowFlag Requirement					Delay	AmbPassScript			CodeID								WaitForFlow		NextRC			NextRCGroup				    Playable Chars		Start/End Time		Repeatable					Exile	Shitskips
		CASE RC_TONYA_1				Fill_RC_Static_Mission_Struct(paramDetails, "Tonya1", 		GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_TONYA, 		NO_RC_MISSION_GROUP,		<<-14.39, -1472.69, 29.58>>, 	RADAR_TRACE_RANDOM_CHARACTER,	"AM_H_RCFS",FLOWFLAG_NONE, 							0, 		"ambient_TonyaCall", 	CID_ACTIVATE_RE_AND_RC_MISSIONS,	TRUE,			RC_TONYA_2,  	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK
		CASE RC_TONYA_2				Fill_RC_Static_Mission_Struct(paramDetails, "Tonya2", 		GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_TONYA, 		NO_RC_MISSION_GROUP,		<<-14.39, -1472.69, 29.58>>, 	RADAR_TRACE_TONYA,				"",			FLOWFLAG_NONE, 							48, 	"ambient_Tonya", 		CID_TONYA3_SEND_TEXT,				FALSE,			RC_TONYA_3,  	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK
		CASE RC_TONYA_3				Fill_RC_Static_Mission_Struct(paramDetails, "Tonya3", 		GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_TONYA, 		NO_RC_MISSION_GROUP,		<<0, 0, 0>>, 					RADAR_TRACE_INVALID,			"",			FLOWFLAG_NONE, 							0, 		"", 					CID_TONYA4_SEND_TEXT,				FALSE,			RC_TONYA_4,  	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK
		CASE RC_TONYA_4				Fill_RC_Static_Mission_Struct(paramDetails, "Tonya4", 		GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_TONYA, 		NO_RC_MISSION_GROUP,		<<0, 0, 0>>, 					RADAR_TRACE_INVALID,			"",			FLOWFLAG_NONE, 							0, 		"", 					CID_BLANK,							FALSE,			RC_TONYA_5,  	NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK
		CASE RC_TONYA_5				Fill_RC_Static_Mission_Struct(paramDetails, "Tonya5", 		GET_RC_MISSION_NAME_LABEL(paramID),		0,			RCS_TONYA, 		NO_RC_MISSION_GROUP,		<<-14.39, -1472.69, 29.58>>, 	RADAR_TRACE_TONYA,				"",			FLOWFLAG_NONE, 							48, 	"", 					CID_BLANK,							FALSE,			NO_RC_MISSION,  NO_RC_MISSION_GROUP,		BIT_FRANKLIN,		0000, 2359,			IS_RC_REPEATABLE(paramID), FALSE,	TRUE) BREAK

		DEFAULT
			SCRIPT_ASSERT("ERROR: Retrieve_Random_Character_Static_Mission_Details has been passed an illegal Random Character mission ID")
		BREAK
	ENDSWITCH
ENDPROC
