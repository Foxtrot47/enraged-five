USING "rage_builtins.sch"
USING "commands_stats.sch"
USING "net_stat_system.sch"
USING "savegame_public.sch"
USING "commands_datafile.sch"
//USING "net_transition_sessions.sch"


/// PURPOSE:
///    Sets all 100% complete items as debug complete
PROC SetAllItemsAsDebugComplete()

   INT tIndex = 0
   
	#if USE_CLF_DLC
		WHILE tIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_CLF))
	        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tIndex].CP_Marked_As_Complete = TRUE		
	        tIndex ++
	    ENDWHILE
    	g_savedGlobalsClifford.sCompletionPercentageData.b_g_OneHundredPercentReached = FALSE
	#endif
	
	#if not USE_CLF_DLC
		WHILE tIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
	        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tIndex].CP_Marked_As_Complete = TRUE		
	        tIndex ++
	    ENDWHILE
    	g_savedGlobals.sCompletionPercentageData.b_g_OneHundredPercentReached = FALSE	
	#endif
    

    PRINTNL()
    PRINTSTRING ("CP_CONTROLLER - Marked all elements as complete.")
    PRINTNL()
ENDPROC

 PROC RecalculateResultantPercentageTotal_CLF()

    //Have debug global flag that tells controller to update widget total? Widget bools would update automatically.
    INT tempIndex = 0

    //Now declared in CompletionPercentage_globals.sch so they can be accessed by appCheckList.sc
    temp_Group_Num_of_Missions = 0 
    temp_Group_Num_of_Minigames = 0
    temp_Group_Num_of_Oddjobs = 0
    temp_Group_Num_of_RandomChars = 0
    temp_Group_Num_of_RandomEvents = 0
    temp_Group_Num_of_Miscellaneous = 0
    temp_Group_Num_of_Friends = 0
   
    //Reset totals prior to recalculation.
    FLOAT temp_Group_Num_of_Missions_Percentage = 0
    FLOAT temp_Group_Num_of_Minigames_Percentage = 0
    FLOAT temp_Group_Num_of_Oddjobs_Percentage = 0
    FLOAT temp_Group_Num_of_RandomChars_Percentage = 0
    FLOAT temp_Group_Num_of_RandomEvents_Percentage = 0
    FLOAT temp_Group_Num_of_Miscellaneous_Percentage = 0
    FLOAT temp_Group_Num_of_Friends_Percentage = 0

    g_savedGlobalsClifford.sCompletionPercentageData.g_Resultant_CompletionPercentage = 0

    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
	
        IF g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete = TRUE
            SWITCH (g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping)
                
				CASE CP_GROUP_MISSIONS
                   temp_Group_Num_of_Missions ++            
                   temp_Group_Num_of_Missions_Percentage = temp_Group_Num_of_Missions_Percentage + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
				BREAK                    
                
                CASE CP_GROUP_MINIGAMES
                    temp_Group_Num_of_Minigames ++
                    temp_Group_Num_of_Minigames_Percentage = temp_Group_Num_of_Minigames_Percentage + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
				
                CASE CP_GROUP_ODDJOBS
                    temp_Group_Num_of_Oddjobs ++
                    temp_Group_Num_of_Oddjobs_Percentage = temp_Group_Num_of_Oddjobs_Percentage + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK

                CASE CP_GROUP_RANDOMCHARS
                    temp_Group_Num_of_RandomChars ++
                    temp_Group_Num_of_RandomChars_Percentage = temp_Group_Num_of_RandomChars_Percentage + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK

                CASE CP_GROUP_RANDOMEVENTS  //All events are technically optional now... but we do need to track them as part of 100 percent to calculate them.                
                    temp_Group_Num_of_RandomEvents ++
                    temp_Group_Num_of_RandomEvents_Percentage = temp_Group_Num_of_RandomEvents_Percentage +
                        ( g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting * 4.0 )
                        //#1039795  - Need only complete a quarter of these events to receive full percentage available.
                BREAK  
				
                CASE CP_GROUP_MISCELLANEOUS
                    temp_Group_Num_of_Miscellaneous ++
                    temp_Group_Num_of_Miscellaneous_Percentage = temp_Group_Num_of_Miscellaneous_Percentage + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
				
                CASE CP_GROUP_FRIENDS
                    temp_Group_Num_of_Friends ++
                    temp_Group_Num_of_Friends_Percentage = temp_Group_Num_of_Friends_Percentage + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
				BREAK

	            DEFAULT   
	                #if IS_DEBUG_BUILD 
	                    PRINTNL()
	                    PRINTSTRING ("CP_PUBLIC - reporting a suspect GROUPING entry in the CompletionPercentage tracker at position.")
	                    PRINTINT (tempIndex)
	                    PRINTNL()
	                    PRINTSTRING ("CP_PUBLIC - It will not count towards 100 percent completion. This is probably deliberate if group has been set to NO_GROUP.")
					#endif
	           	BREAK

            ENDSWITCH
        ENDIF
        tempIndex ++
    ENDWHILE   

    //Perhaps dump list here?

    IF g_Group_Num_of_Missions > 0 //Don't test for groupings currently at zero
        IF temp_Group_Num_of_Missions = g_Group_Num_of_Missions     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
                    
        //Think about adding a OR greater than safety check here just in case the temp number is greater than the g_Group when a player character dies.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Missions complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Missions_Percentage = g_c_Missions_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Minigames > 0
        IF temp_Group_Num_of_Minigames = g_Group_Num_of_Minigames   //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.        
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Minigames complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Minigames_Percentage = g_c_Minigames_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Oddjobs > 0
        IF temp_Group_Num_of_Oddjobs = g_Group_Num_of_Oddjobs     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Oddjobs complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Oddjobs_Percentage = g_c_Oddjobs_CombinedWeighting
        ENDIF
    ENDIF
	
    IF g_Group_Num_of_RandomChars > 0
        IF temp_Group_Num_of_RandomChars = g_Group_Num_of_RandomChars     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of RandomChar elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_RandomChars_Percentage = g_c_RandomChars_CombinedWeighting
        ENDIF
    ENDIF
        
    //#1039795  - Need only complete a quarter of these random events to receive full percentage available.

    IF g_Group_Num_of_RandomEvents > 0
 
        IF temp_Group_Num_of_RandomEvents = g_Group_Num_of_RandomEvents     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        OR  (((g_Group_Num_of_RandomEvents * 10) /  temp_Group_Num_of_RandomEvents) < 41)  //Check for over a quarter of random events being completed 
        OR temp_Group_Num_of_RandomEvents > g_Quarter_Threshold_Group_Num_Of_RandomEvents //Added for safety and inputting into stat.                                                                                   //The multiplication by 10 and checking for 39 is effectively checking for > 3.9 but with ints.
        OR temp_Group_Num_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents

            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of RandomEvent elements complete is at least a quarter of the total accessible amount in group. #1039795 Assigning overall combined weighting.")
                PRINTNL()                
            #endif

            IF NOT IS_BIT_SET (g_savedGlobalsClifford.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_FOUND_ALL_RANDOM_EV)
                IF temp_Group_Num_of_RandomEvents = g_Group_Num_of_RandomEvents

                    #if IS_DEBUG_BUILD
                        PRINTSTRING ("CP_PRIVATE - Sending presence event for completing all random events")
                        PRINTNL()                    
                    #endif

                    PRESENCE_EVENT_UPDATESTAT_INT (NUM_RNDEVENTS_COMPLETED, g_Group_Num_of_RandomEvents)
                    SET_BIT (g_savedGlobalsClifford.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_FOUND_ALL_RANDOM_EV)

                ENDIF
            ENDIF
            temp_Group_Num_of_RandomEvents_Percentage = g_c_RandomEvents_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Miscellaneous > 0                         
        IF temp_Group_Num_of_Miscellaneous = g_Group_Num_of_Miscellaneous     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        	#if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Miscellaneous elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Miscellaneous_Percentage = g_c_Miscellaneous_CombinedWeighting
        ENDIF
    ENDIF


    IF g_Group_Num_of_Friends > 0
        IF temp_Group_Num_of_Friends = g_Group_Num_of_Friends     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Friend elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Friends_Percentage = g_c_Friends_CombinedWeighting
        ENDIF
    ENDIF

    g_savedGlobalsClifford.sCompletionPercentageData.g_Resultant_CompletionPercentage = ( temp_Group_Num_of_Missions_Percentage +  temp_Group_Num_of_Minigames_Percentage
                                                                                + temp_Group_Num_of_Oddjobs_Percentage +  temp_Group_Num_of_RandomChars_Percentage
                                                                                + temp_Group_Num_of_RandomEvents_Percentage +  temp_Group_Num_of_Miscellaneous_Percentage
                                                                                + temp_Group_Num_of_Friends_Percentage )
    


    INT i_Manipulated_Temp_Group_of_RandomEvents  //Need to manipulate the stats so we only count a quarter of the random events as mandatory.

    IF temp_Group_Num_of_RandomEvents > g_Quarter_Threshold_Group_Num_Of_RandomEvents
    OR temp_Group_Num_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
        i_Manipulated_Temp_Group_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
    ELSE
        i_Manipulated_Temp_Group_of_RandomEvents = temp_Group_Num_of_RandomEvents
    ENDIF

    STAT_SET_INT (NUM_MISSIONS_COMPLETED, temp_Group_Num_of_Missions)
    STAT_SET_INT (NUM_MISSIONS_AVAILABLE, g_Group_Num_of_Missions)
    STAT_SET_INT (NUM_MINIGAMES_COMPLETED, temp_Group_Num_of_Minigames)
    STAT_SET_INT (NUM_MINIGAMES_AVAILABLE, g_Group_Num_of_Minigames)
    // Could remove these stats.  See #1534353
    STAT_SET_INT (NUM_ODDJOBS_COMPLETED, temp_Group_Num_of_Oddjobs)
    STAT_SET_INT (NUM_ODDJOBS_AVAILABLE, g_Group_Num_of_Oddjobs)  
    STAT_SET_INT (NUM_RNDPEOPLE_COMPLETED, temp_Group_Num_of_RandomChars)
    STAT_SET_INT (NUM_RNDPEOPLE_AVAILABLE, g_Group_Num_of_RandomChars)
    STAT_SET_INT (NUM_RNDEVENTS_COMPLETED, i_Manipulated_Temp_Group_of_RandomEvents)
    STAT_SET_INT (NUM_RNDEVENTS_AVAILABLE, g_Quarter_Threshold_Group_Num_Of_RandomEvents)
    STAT_SET_INT (NUM_MISC_COMPLETED, (temp_Group_Num_of_Friends +  temp_Group_Num_of_Miscellaneous))
    STAT_SET_INT (NUM_MISC_AVAILABLE, (g_Group_Num_of_Friends +  g_Group_Num_of_Miscellaneous))


    //Front End 100 percent checklist.  This needs work to account for the Random Events mayhem. They can't all be counted, They'll all be optional, so this may not matter as they won't be
    //part of the temp_group or g_Group for RandomEvents - #1039795  - Need only complete a quarter of these events to receive full percentage available.

    //Need to watch float to int transitions here with regards to fractions getting rounded to zero, hence the arithmetic method here.
    g_i_Story_Missions_Percentage  = ((temp_Group_Num_of_Missions * 100) / g_Group_Num_of_Missions)
    g_i_Oddjobs_Percentage = ((temp_Group_Num_of_Oddjobs + temp_Group_Num_Of_Minigames) * 100) / (g_Group_Num_of_Oddjobs + g_Group_Num_Of_Minigames)
	g_i_Ambient_Missions_Percentage = ((temp_Group_Num_of_RandomChars + i_Manipulated_Temp_Group_of_RandomEvents) * 100) / (g_Group_Num_of_RandomChars + g_Quarter_Threshold_Group_Num_Of_RandomEvents)
	g_i_Misc_Percentage = ((temp_Group_Num_of_Miscellaneous + temp_Group_Num_Of_Friends) * 100) / (g_Group_Num_of_Miscellaneous + g_Group_Num_Of_Friends)


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("Random Events Quartering Check: ")
        PRINTINT (i_Manipulated_Temp_Group_of_RandomEvents)
        PRINTSTRING (" from ")
        PRINTINT (g_Quarter_Threshold_Group_Num_Of_RandomEvents)
        PRINTNL()
        PRINTSTRING ("FE Stat - Story Missions PC ")
        PRINTINT (g_i_Story_Missions_Percentage)
        PRINTNL()

        PRINTSTRING ("FE Stat Oddjobs - Oddjobs + Minigames PC ")
        PRINTINT (g_i_Oddjobs_Percentage)
        PRINTNL()

        PRINTSTRING ("FE Stat Ambient - RandomChars + RandomEvents Missions PC ")
        PRINTINT (g_i_Ambient_Missions_Percentage)
        PRINTNL()


        PRINTSTRING ("FE Stat Misc  - Miscellaneous + Friends Missions PC ")
        PRINTINT (g_i_Misc_Percentage)
        PRINTNL()


        PRINTSTRING( "Resultant Overall Percentage Complete is ")
        PRINTFLOAT (g_savedGlobalsClifford.sCompletionPercentageData.g_Resultant_CompletionPercentage)
        PRINTNL()

    #endif

    //IMPORTANT!
    //Register stat here after recalculation of percentage total.
    STAT_SET_FLOAT (TOTAL_PROGRESS_MADE, g_savedGlobalsClifford.sCompletionPercentageData.g_Resultant_CompletionPercentage)    
    STAT_SET_INT (PERCENT_STORY_MISSIONS, g_i_Story_Missions_Percentage)
    STAT_SET_INT (PERCENT_AMBIENT_MISSIONS, g_i_Ambient_Missions_Percentage)
    STAT_SET_INT (PERCENT_ODDJOBS, g_i_Oddjobs_Percentage)  

                                                           
        IF NOT DATAFILE_IS_SAVE_PENDING()  //make sure another cloud upload operation is already in progress.

            IF NOT g_bInMultiplayer //Fix for cloud conflict with Bobby's MP use. Bug 889955.
        
                IF (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR) = FALSE
                AND NOT NETWORK_IS_GAME_IN_PROGRESS()
				
                        IF NETWORK_IS_CLOUD_AVAILABLE()
                            g_Trigger_CloudUpload_of_CP_Elements = FALSE //Let CompletionPercentage_Controller know that it should trigger a Cloud Upload.

                            #if IS_DEBUG_BUILD                                        
                                PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE but .json export no longer required. Profile Stats now used. #1498076") 
                                PRINTNL()                                        
                            #endif
                        ELSE
                            #if IS_DEBUG_BUILD                                        
                                PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE returned FALSE. No upload action taken.") 
                                PRINTNL()                                       
                            #endif
                        ENDIF
                        //No longer called during debug launches -BenR
                        #if IS_DEBUG_BUILD
                            IF NOT g_flowUnsaved.bUpdatingGameflow 
                        #endif                            
                            //This means a mission has delayed a mission passed routine. Saving during these periods is unsafe (for example during a planning board sequence).
                            //A save will always run directly after the stat block is lifted. -BenR.
                            IF NOT g_bMissionStatSystemBlocker                                 
                                MAKE_AUTOSAVE_REQUEST() //Added in accordance with bug #1079936.                                    
                            ENDIF                                    
                        #if IS_DEBUG_BUILD
                            ENDIF
                        #endif
                ELSE
                    #if IS_DEBUG_BUILD                                        
                        PRINTSTRING("[CompletionPercentage_Private] Recalculate operation cued in MP, will NOT cue cloud upload for this.") 
                        PRINTNL()                                        
                    #endif                    
                ENDIF            
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD        
                PRINTSTRING("[CompletionPercentage_Private] Cannot upload to the cloud at this time as DATAFILE_IS_SAVE_PENDING() has returned true.")
                PRINTNL()
            #endif
        ENDIF
 ENDPROC
PROC RecalculateResultantPercentageTotal_NRM()

    //Have debug global flag that tells controller to update widget total? Widget bools would update automatically.
    INT tempIndex = 0

    //Now declared in CompletionPercentage_globals.sch so they can be accessed by appCheckList.sc
    temp_Group_Num_of_Missions = 0 
    temp_Group_Num_of_Minigames = 0
    temp_Group_Num_of_Oddjobs = 0
    temp_Group_Num_of_RandomChars = 0
    temp_Group_Num_of_RandomEvents = 0
    temp_Group_Num_of_Miscellaneous = 0
    temp_Group_Num_of_Friends = 0
   
    //Reset totals prior to recalculation.
    FLOAT temp_Group_Num_of_Missions_Percentage = 0
    FLOAT temp_Group_Num_of_Minigames_Percentage = 0
    FLOAT temp_Group_Num_of_Oddjobs_Percentage = 0
    FLOAT temp_Group_Num_of_RandomChars_Percentage = 0
    FLOAT temp_Group_Num_of_RandomEvents_Percentage = 0
    FLOAT temp_Group_Num_of_Miscellaneous_Percentage = 0
    FLOAT temp_Group_Num_of_Friends_Percentage = 0

    g_savedGlobalsnorman.sCompletionPercentageData.g_Resultant_CompletionPercentage = 0

    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
	
        IF g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete = TRUE
            SWITCH (g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping)
                
				CASE CP_GROUP_MISSIONS
                   temp_Group_Num_of_Missions ++            
                   temp_Group_Num_of_Missions_Percentage = temp_Group_Num_of_Missions_Percentage + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
				BREAK                    
                
                CASE CP_GROUP_MINIGAMES
                    temp_Group_Num_of_Minigames ++
                    temp_Group_Num_of_Minigames_Percentage = temp_Group_Num_of_Minigames_Percentage + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
				
                CASE CP_GROUP_ODDJOBS
                    temp_Group_Num_of_Oddjobs ++
                    temp_Group_Num_of_Oddjobs_Percentage = temp_Group_Num_of_Oddjobs_Percentage + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK

                CASE CP_GROUP_RANDOMCHARS
                    temp_Group_Num_of_RandomChars ++
                    temp_Group_Num_of_RandomChars_Percentage = temp_Group_Num_of_RandomChars_Percentage + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK

                CASE CP_GROUP_RANDOMEVENTS  //All events are technically optional now... but we do need to track them as part of 100 percent to calculate them.                
                    temp_Group_Num_of_RandomEvents ++
                    temp_Group_Num_of_RandomEvents_Percentage = temp_Group_Num_of_RandomEvents_Percentage +
                        ( g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting * 4.0 )
                        //#1039795  - Need only complete a quarter of these events to receive full percentage available.
                BREAK  
				
                CASE CP_GROUP_MISCELLANEOUS
                    temp_Group_Num_of_Miscellaneous ++
                    temp_Group_Num_of_Miscellaneous_Percentage = temp_Group_Num_of_Miscellaneous_Percentage + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
				
                CASE CP_GROUP_FRIENDS
                    temp_Group_Num_of_Friends ++
                    temp_Group_Num_of_Friends_Percentage = temp_Group_Num_of_Friends_Percentage + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
				BREAK

	            DEFAULT   
	                #if IS_DEBUG_BUILD 
	                    PRINTNL()
	                    PRINTSTRING ("CP_PUBLIC - reporting a suspect GROUPING entry in the CompletionPercentage tracker at position.")
	                    PRINTINT (tempIndex)
	                    PRINTNL()
	                    PRINTSTRING ("CP_PUBLIC - It will not count towards 100 percent completion. This is probably deliberate if group has been set to NO_GROUP.")
					#endif
	           	BREAK

            ENDSWITCH
        ENDIF
        tempIndex ++
    ENDWHILE   

    //Perhaps dump list here?

    IF g_Group_Num_of_Missions > 0 //Don't test for groupings currently at zero
        IF temp_Group_Num_of_Missions = g_Group_Num_of_Missions     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
                    
        //Think about adding a OR greater than safety check here just in case the temp number is greater than the g_Group when a player character dies.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Missions complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Missions_Percentage = g_c_Missions_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Minigames > 0
        IF temp_Group_Num_of_Minigames = g_Group_Num_of_Minigames   //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.        
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Minigames complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Minigames_Percentage = g_c_Minigames_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Oddjobs > 0
        IF temp_Group_Num_of_Oddjobs = g_Group_Num_of_Oddjobs     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Oddjobs complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Oddjobs_Percentage = g_c_Oddjobs_CombinedWeighting
        ENDIF
    ENDIF
	
    IF g_Group_Num_of_RandomChars > 0
        IF temp_Group_Num_of_RandomChars = g_Group_Num_of_RandomChars     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of RandomChar elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_RandomChars_Percentage = g_c_RandomChars_CombinedWeighting
        ENDIF
    ENDIF
        
    //#1039795  - Need only complete a quarter of these random events to receive full percentage available.

    IF g_Group_Num_of_RandomEvents > 0
 
        IF temp_Group_Num_of_RandomEvents = g_Group_Num_of_RandomEvents     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        OR  (((g_Group_Num_of_RandomEvents * 10) /  temp_Group_Num_of_RandomEvents) < 41)  //Check for over a quarter of random events being completed 
        OR temp_Group_Num_of_RandomEvents > g_Quarter_Threshold_Group_Num_Of_RandomEvents //Added for safety and inputting into stat.                                                                                   //The multiplication by 10 and checking for 39 is effectively checking for > 3.9 but with ints.
        OR temp_Group_Num_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents

            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of RandomEvent elements complete is at least a quarter of the total accessible amount in group. #1039795 Assigning overall combined weighting.")
                PRINTNL()                
            #endif

            IF NOT IS_BIT_SET (g_savedGlobalsnorman.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_FOUND_ALL_RANDOM_EV)
                IF temp_Group_Num_of_RandomEvents = g_Group_Num_of_RandomEvents

                    #if IS_DEBUG_BUILD
                        PRINTSTRING ("CP_PRIVATE - Sending presence event for completing all random events")
                        PRINTNL()                    
                    #endif

                    PRESENCE_EVENT_UPDATESTAT_INT (NUM_RNDEVENTS_COMPLETED, g_Group_Num_of_RandomEvents)
                    SET_BIT (g_savedGlobalsnorman.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_FOUND_ALL_RANDOM_EV)

                ENDIF
            ENDIF
            temp_Group_Num_of_RandomEvents_Percentage = g_c_RandomEvents_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Miscellaneous > 0                         
        IF temp_Group_Num_of_Miscellaneous = g_Group_Num_of_Miscellaneous     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        	#if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Miscellaneous elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Miscellaneous_Percentage = g_c_Miscellaneous_CombinedWeighting
        ENDIF
    ENDIF


    IF g_Group_Num_of_Friends > 0
        IF temp_Group_Num_of_Friends = g_Group_Num_of_Friends     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Friend elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Friends_Percentage = g_c_Friends_CombinedWeighting
        ENDIF
    ENDIF

    g_savedGlobalsnorman.sCompletionPercentageData.g_Resultant_CompletionPercentage = ( temp_Group_Num_of_Missions_Percentage +  temp_Group_Num_of_Minigames_Percentage
                                                                                + temp_Group_Num_of_Oddjobs_Percentage +  temp_Group_Num_of_RandomChars_Percentage
                                                                                + temp_Group_Num_of_RandomEvents_Percentage +  temp_Group_Num_of_Miscellaneous_Percentage
                                                                                + temp_Group_Num_of_Friends_Percentage )
    


    INT i_Manipulated_Temp_Group_of_RandomEvents  //Need to manipulate the stats so we only count a quarter of the random events as mandatory.

    IF temp_Group_Num_of_RandomEvents > g_Quarter_Threshold_Group_Num_Of_RandomEvents
    OR temp_Group_Num_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
        i_Manipulated_Temp_Group_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
    ELSE
        i_Manipulated_Temp_Group_of_RandomEvents = temp_Group_Num_of_RandomEvents
    ENDIF

    STAT_SET_INT (NUM_MISSIONS_COMPLETED, temp_Group_Num_of_Missions)
    STAT_SET_INT (NUM_MISSIONS_AVAILABLE, g_Group_Num_of_Missions)
    STAT_SET_INT (NUM_MINIGAMES_COMPLETED, temp_Group_Num_of_Minigames)
    STAT_SET_INT (NUM_MINIGAMES_AVAILABLE, g_Group_Num_of_Minigames)
    // Could remove these stats.  See #1534353
    STAT_SET_INT (NUM_ODDJOBS_COMPLETED, temp_Group_Num_of_Oddjobs)
    STAT_SET_INT (NUM_ODDJOBS_AVAILABLE, g_Group_Num_of_Oddjobs)  
    STAT_SET_INT (NUM_RNDPEOPLE_COMPLETED, temp_Group_Num_of_RandomChars)
    STAT_SET_INT (NUM_RNDPEOPLE_AVAILABLE, g_Group_Num_of_RandomChars)
    STAT_SET_INT (NUM_RNDEVENTS_COMPLETED, i_Manipulated_Temp_Group_of_RandomEvents)
    STAT_SET_INT (NUM_RNDEVENTS_AVAILABLE, g_Quarter_Threshold_Group_Num_Of_RandomEvents)
    STAT_SET_INT (NUM_MISC_COMPLETED, (temp_Group_Num_of_Friends +  temp_Group_Num_of_Miscellaneous))
    STAT_SET_INT (NUM_MISC_AVAILABLE, (g_Group_Num_of_Friends +  g_Group_Num_of_Miscellaneous))


    //Front End 100 percent checklist.  This needs work to account for the Random Events mayhem. They can't all be counted, They'll all be optional, so this may not matter as they won't be
    //part of the temp_group or g_Group for RandomEvents - #1039795  - Need only complete a quarter of these events to receive full percentage available.

    //Need to watch float to int transitions here with regards to fractions getting rounded to zero, hence the arithmetic method here.
    g_i_Story_Missions_Percentage  = ((temp_Group_Num_of_Missions * 100) / g_Group_Num_of_Missions)
    g_i_Oddjobs_Percentage = ((temp_Group_Num_of_Oddjobs + temp_Group_Num_Of_Minigames) * 100) / (g_Group_Num_of_Oddjobs + g_Group_Num_Of_Minigames)
	g_i_Ambient_Missions_Percentage = ((temp_Group_Num_of_RandomChars + i_Manipulated_Temp_Group_of_RandomEvents) * 100) / (g_Group_Num_of_RandomChars + g_Quarter_Threshold_Group_Num_Of_RandomEvents)
	g_i_Misc_Percentage = ((temp_Group_Num_of_Miscellaneous + temp_Group_Num_Of_Friends) * 100) / (g_Group_Num_of_Miscellaneous + g_Group_Num_Of_Friends)


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("Random Events Quartering Check: ")
        PRINTINT (i_Manipulated_Temp_Group_of_RandomEvents)
        PRINTSTRING (" from ")
        PRINTINT (g_Quarter_Threshold_Group_Num_Of_RandomEvents)
        PRINTNL()
        PRINTSTRING ("FE Stat - Story Missions PC ")
        PRINTINT (g_i_Story_Missions_Percentage)
        PRINTNL()

        PRINTSTRING ("FE Stat Oddjobs - Oddjobs + Minigames PC ")
        PRINTINT (g_i_Oddjobs_Percentage)
        PRINTNL()

        PRINTSTRING ("FE Stat Ambient - RandomChars + RandomEvents Missions PC ")
        PRINTINT (g_i_Ambient_Missions_Percentage)
        PRINTNL()


        PRINTSTRING ("FE Stat Misc  - Miscellaneous + Friends Missions PC ")
        PRINTINT (g_i_Misc_Percentage)
        PRINTNL()


        PRINTSTRING( "Resultant Overall Percentage Complete is ")
        PRINTFLOAT (g_savedGlobalsnorman.sCompletionPercentageData.g_Resultant_CompletionPercentage)
        PRINTNL()

    #endif

    //IMPORTANT!
    //Register stat here after recalculation of percentage total.
    STAT_SET_FLOAT (TOTAL_PROGRESS_MADE, g_savedGlobalsnorman.sCompletionPercentageData.g_Resultant_CompletionPercentage)    
    STAT_SET_INT (PERCENT_STORY_MISSIONS, g_i_Story_Missions_Percentage)
    STAT_SET_INT (PERCENT_AMBIENT_MISSIONS, g_i_Ambient_Missions_Percentage)
    STAT_SET_INT (PERCENT_ODDJOBS, g_i_Oddjobs_Percentage)  

                                                           
        IF NOT DATAFILE_IS_SAVE_PENDING()  //make sure another cloud upload operation is already in progress.

            IF NOT g_bInMultiplayer //Fix for cloud conflict with Bobby's MP use. Bug 889955.
        
                IF (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR) = FALSE
                AND NOT NETWORK_IS_GAME_IN_PROGRESS()
				
                        IF NETWORK_IS_CLOUD_AVAILABLE()
                            g_Trigger_CloudUpload_of_CP_Elements = FALSE //Let CompletionPercentage_Controller know that it should trigger a Cloud Upload.

                            #if IS_DEBUG_BUILD                                        
                                PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE but .json export no longer required. Profile Stats now used. #1498076") 
                                PRINTNL()                                        
                            #endif
                        ELSE
                            #if IS_DEBUG_BUILD                                        
                                PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE returned FALSE. No upload action taken.") 
                                PRINTNL()                                       
                            #endif
                        ENDIF
                        //No longer called during debug launches -BenR
                        #if IS_DEBUG_BUILD
                            IF NOT g_flowUnsaved.bUpdatingGameflow 
                        #endif                            
                            //This means a mission has delayed a mission passed routine. Saving during these periods is unsafe (for example during a planning board sequence).
                            //A save will always run directly after the stat block is lifted. -BenR.
                            IF NOT g_bMissionStatSystemBlocker                                 
                                MAKE_AUTOSAVE_REQUEST() //Added in accordance with bug #1079936.                                    
                            ENDIF                                    
                        #if IS_DEBUG_BUILD
                            ENDIF
                        #endif
                ELSE
                    #if IS_DEBUG_BUILD                                        
                        PRINTSTRING("[CompletionPercentage_Private] Recalculate operation cued in MP, will NOT cue cloud upload for this.") 
                        PRINTNL()                                        
                    #endif                    
                ENDIF            
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD        
                PRINTSTRING("[CompletionPercentage_Private] Cannot upload to the cloud at this time as DATAFILE_IS_SAVE_PENDING() has returned true.")
                PRINTNL()
            #endif
        ENDIF
 ENDPROC
 PROC RecalculateResultantPercentageTotal()
 
	#if USE_CLF_DLC
		RecalculateResultantPercentageTotal_CLF()
		exit
	#endif
	#if USE_NRM_DLC
		RecalculateResultantPercentageTotal_NRM()
		exit
	#endif

	//Have debug global flag that tells controller to update widget total? Widget bools would update automatically.
    INT tempIndex = 0

    //Now declared in CompletionPercentage_globals.sch so they can be accessed by appCheckList.sc
    temp_Group_Num_of_Missions = 0 
    temp_Group_Num_of_Minigames = 0
    temp_Group_Num_of_Oddjobs = 0
    temp_Group_Num_of_RandomChars = 0
    temp_Group_Num_of_RandomEvents = 0
    temp_Group_Num_of_Miscellaneous = 0
    temp_Group_Num_of_Friends = 0
   
    //Reset totals prior to recalculation.

    FLOAT temp_Group_Num_of_Missions_Percentage = 0
    FLOAT temp_Group_Num_of_Minigames_Percentage = 0
    FLOAT temp_Group_Num_of_Oddjobs_Percentage = 0
    FLOAT temp_Group_Num_of_RandomChars_Percentage = 0
    FLOAT temp_Group_Num_of_RandomEvents_Percentage = 0
    FLOAT temp_Group_Num_of_Miscellaneous_Percentage = 0
    FLOAT temp_Group_Num_of_Friends_Percentage = 0
	
	//Store old CP to compare achievement progress
	FLOAT fOldCP = g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage
	g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage = 0

    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
        IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete = TRUE
            SWITCH (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping)
                CASE CP_GROUP_MISSIONS
                   temp_Group_Num_of_Missions ++            
                   temp_Group_Num_of_Missions_Percentage = temp_Group_Num_of_Missions_Percentage + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK                  
                CASE CP_GROUP_MINIGAMES
                    temp_Group_Num_of_Minigames ++
                    temp_Group_Num_of_Minigames_Percentage = temp_Group_Num_of_Minigames_Percentage + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
                CASE CP_GROUP_ODDJOBS
                    temp_Group_Num_of_Oddjobs ++
                    temp_Group_Num_of_Oddjobs_Percentage = temp_Group_Num_of_Oddjobs_Percentage + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
                CASE CP_GROUP_RANDOMCHARS
                    temp_Group_Num_of_RandomChars ++
                    temp_Group_Num_of_RandomChars_Percentage = temp_Group_Num_of_RandomChars_Percentage + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
                CASE CP_GROUP_RANDOMEVENTS  //All events are technically optional now... but we do need to track them as part of 100 percent to calculate them.                
                    temp_Group_Num_of_RandomEvents ++
                    temp_Group_Num_of_RandomEvents_Percentage = temp_Group_Num_of_RandomEvents_Percentage +
                        ( g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting * 4.0 )
                        //#1039795  - Need only complete a quarter of these events to receive full percentage available.
                BREAK
                CASE CP_GROUP_MISCELLANEOUS
                    temp_Group_Num_of_Miscellaneous ++
                    temp_Group_Num_of_Miscellaneous_Percentage = temp_Group_Num_of_Miscellaneous_Percentage + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
                CASE CP_GROUP_FRIENDS
                    temp_Group_Num_of_Friends ++
                    temp_Group_Num_of_Friends_Percentage = temp_Group_Num_of_Friends_Percentage + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting 
                BREAK
            DEFAULT     
                #if IS_DEBUG_BUILD 
                    PRINTNL()
                    PRINTSTRING ("CP_PUBLIC - reporting a suspect GROUPING entry in the CompletionPercentage tracker at position.")
                    PRINTINT (tempIndex)
                    PRINTNL()
                    PRINTSTRING ("CP_PUBLIC - It will not count towards 100 percent completion. This is probably deliberate if group has been set to NO_GROUP.")
                #endif
            BREAK
            ENDSWITCH
        ENDIF
		tempIndex ++
    ENDWHILE 

    //Perhaps dump list here?
    IF g_Group_Num_of_Missions > 0 //Don't test for groupings currently at zero
        IF temp_Group_Num_of_Missions = g_Group_Num_of_Missions     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        //Think about adding a OR greater than safety check here just in case the temp number is greater than the g_Group when a player character dies.
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Missions complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Missions_Percentage = g_c_Missions_CombinedWeighting
        ENDIF
    ENDIF
    IF g_Group_Num_of_Minigames > 0
        IF temp_Group_Num_of_Minigames = g_Group_Num_of_Minigames   //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
            #if IS_DEBUG_BUILD			
                PRINTSTRING ("CP_PRIVATE Number of Minigames complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()           
			#endif
            temp_Group_Num_of_Minigames_Percentage = g_c_Minigames_CombinedWeighting
        ENDIF
    ENDIF

    IF g_Group_Num_of_Oddjobs > 0
        IF temp_Group_Num_of_Oddjobs = g_Group_Num_of_Oddjobs     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Oddjobs complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_Oddjobs_Percentage = g_c_Oddjobs_CombinedWeighting
        ENDIF
    ENDIF



    IF g_Group_Num_of_RandomChars > 0
        IF temp_Group_Num_of_RandomChars = g_Group_Num_of_RandomChars     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of RandomChar elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif
            temp_Group_Num_of_RandomChars_Percentage = g_c_RandomChars_CombinedWeighting
        ENDIF
    ENDIF
        
    //#1039795  - Need only complete a quarter of these random events to receive full percentage available.

    IF g_Group_Num_of_RandomEvents > 0 
        IF temp_Group_Num_of_RandomEvents = g_Group_Num_of_RandomEvents     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
        OR  (((g_Group_Num_of_RandomEvents * 10) /  temp_Group_Num_of_RandomEvents) < 41)  //Check for over a quarter of random events being completed 
        OR temp_Group_Num_of_RandomEvents > g_Quarter_Threshold_Group_Num_Of_RandomEvents //Added for safety and inputting into stat.                                                                                   //The multiplication by 10 and checking for 39 is effectively checking for > 3.9 but with ints.
        OR temp_Group_Num_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of RandomEvent elements complete is at least a quarter of the total accessible amount in group. #1039795 Assigning overall combined weighting.")
                PRINTNL()
            #endif
            IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_FOUND_ALL_RANDOM_EV)
                IF temp_Group_Num_of_RandomEvents = g_Group_Num_of_RandomEvents
                    #if IS_DEBUG_BUILD
                        PRINTSTRING ("CP_PRIVATE - Sending presence event for completing all random events")
                        PRINTNL()
                    #endif
                    PRESENCE_EVENT_UPDATESTAT_INT (NUM_RNDEVENTS_COMPLETED, g_Group_Num_of_RandomEvents)
                    SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_FOUND_ALL_RANDOM_EV)
                ENDIF
            ENDIF
            temp_Group_Num_of_RandomEvents_Percentage = g_c_RandomEvents_CombinedWeighting
        ENDIF
    ENDIF

	IF g_Group_Num_of_Miscellaneous > 0                         
        IF temp_Group_Num_of_Miscellaneous = g_Group_Num_of_Miscellaneous     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
			#if IS_DEBUG_BUILD

                PRINTSTRING ("CP_PRIVATE Number of Miscellaneous elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
            #endif

            temp_Group_Num_of_Miscellaneous_Percentage = g_c_Miscellaneous_CombinedWeighting
        ENDIF
    ENDIF
    IF g_Group_Num_of_Friends > 0
        IF temp_Group_Num_of_Friends = g_Group_Num_of_Friends     //if all entries of this grouping have been completed, assign maximum weighting to prevent rounding errors.
                   
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PRIVATE Number of Friend elements complete equals total amount in group. Assigning overall combined weighting.")
                PRINTNL()
			#endif
            temp_Group_Num_of_Friends_Percentage = g_c_Friends_CombinedWeighting
        ENDIF
    ENDIF

	
	 g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage = ( temp_Group_Num_of_Missions_Percentage +  temp_Group_Num_of_Minigames_Percentage
                                                                                + temp_Group_Num_of_Oddjobs_Percentage +  temp_Group_Num_of_RandomChars_Percentage
                                                                                + temp_Group_Num_of_RandomEvents_Percentage +  temp_Group_Num_of_Miscellaneous_Percentage
                                                                                + temp_Group_Num_of_Friends_Percentage )
	
	    INT i_Manipulated_Temp_Group_of_RandomEvents  //Need to manipulate the stats so we only count a quarter of the random events as mandatory.
    IF temp_Group_Num_of_RandomEvents > g_Quarter_Threshold_Group_Num_Of_RandomEvents
    OR temp_Group_Num_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
        i_Manipulated_Temp_Group_of_RandomEvents = g_Quarter_Threshold_Group_Num_Of_RandomEvents
    ELSE
        i_Manipulated_Temp_Group_of_RandomEvents = temp_Group_Num_of_RandomEvents
    ENDIF

    STAT_SET_INT (NUM_MISSIONS_COMPLETED, temp_Group_Num_of_Missions)
    STAT_SET_INT (NUM_MISSIONS_AVAILABLE, g_Group_Num_of_Missions)
    STAT_SET_INT (NUM_MINIGAMES_COMPLETED, temp_Group_Num_of_Minigames)
    STAT_SET_INT (NUM_MINIGAMES_AVAILABLE, g_Group_Num_of_Minigames)
    // Could remove these stats.  See #1534353
    STAT_SET_INT (NUM_ODDJOBS_COMPLETED, temp_Group_Num_of_Oddjobs)
    STAT_SET_INT (NUM_ODDJOBS_AVAILABLE, g_Group_Num_of_Oddjobs)   
    STAT_SET_INT (NUM_RNDPEOPLE_COMPLETED, temp_Group_Num_of_RandomChars)
    STAT_SET_INT (NUM_RNDPEOPLE_AVAILABLE, g_Group_Num_of_RandomChars)
    STAT_SET_INT (NUM_RNDEVENTS_COMPLETED, i_Manipulated_Temp_Group_of_RandomEvents)
    STAT_SET_INT (NUM_RNDEVENTS_AVAILABLE, g_Quarter_Threshold_Group_Num_Of_RandomEvents)
    STAT_SET_INT (NUM_MISC_COMPLETED, (temp_Group_Num_of_Friends +  temp_Group_Num_of_Miscellaneous))
    STAT_SET_INT (NUM_MISC_AVAILABLE, (g_Group_Num_of_Friends +  g_Group_Num_of_Miscellaneous))

    //Front End 100 percent checklist.  This needs work to account for the Random Events mayhem. They can't all be counted, They'll all be optional, so this may not matter as they won't be
    //part of the temp_group or g_Group for RandomEvents - #1039795  - Need only complete a quarter of these events to receive full percentage available.


    //Need to watch float to int transitions here with regards to fractions getting rounded to zero, hence the arithmetic method here.
    g_i_Story_Missions_Percentage  = ((temp_Group_Num_of_Missions * 100) / g_Group_Num_of_Missions)
    g_i_Oddjobs_Percentage = ((temp_Group_Num_of_Oddjobs + temp_Group_Num_Of_Minigames) * 100) / (g_Group_Num_of_Oddjobs + g_Group_Num_Of_Minigames)
    g_i_Ambient_Missions_Percentage = ((temp_Group_Num_of_RandomChars + i_Manipulated_Temp_Group_of_RandomEvents) * 100) / (g_Group_Num_of_RandomChars + g_Quarter_Threshold_Group_Num_Of_RandomEvents)
    g_i_Misc_Percentage = ((temp_Group_Num_of_Miscellaneous + temp_Group_Num_Of_Friends) * 100) / (g_Group_Num_of_Miscellaneous + g_Group_Num_Of_Friends)

    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("Random Events Quartering Check: ")
        PRINTINT (i_Manipulated_Temp_Group_of_RandomEvents)
        PRINTSTRING (" from ")
        PRINTINT (g_Quarter_Threshold_Group_Num_Of_RandomEvents)
        PRINTNL()
        PRINTSTRING ("FE Stat - Story Missions PC ")
        PRINTINT (g_i_Story_Missions_Percentage)
        PRINTNL()
        PRINTSTRING ("FE Stat Oddjobs - Oddjobs + Minigames PC ")
        PRINTINT (g_i_Oddjobs_Percentage)
        PRINTNL()
        PRINTSTRING ("FE Stat Ambient - RandomChars + RandomEvents Missions PC ")
        PRINTINT (g_i_Ambient_Missions_Percentage)
        PRINTNL()
        PRINTSTRING ("FE Stat Misc  - Miscellaneous + Friends Missions PC ")
		PRINTINT (g_i_Misc_Percentage)
        PRINTNL()
        PRINTSTRING( "Resultant Overall Percentage Complete is ")	
		PRINTFLOAT (g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage)		
        PRINTNL()

    #endif


    STAT_SET_FLOAT (TOTAL_PROGRESS_MADE, g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage)			 
	STAT_SET_INT (PERCENT_STORY_MISSIONS, g_i_Story_Missions_Percentage)
    STAT_SET_INT (PERCENT_AMBIENT_MISSIONS, g_i_Ambient_Missions_Percentage)
    STAT_SET_INT (PERCENT_ODDJOBS, g_i_Oddjobs_Percentage)  

	//Update ahcievement progress
	IF fOldCp > 0
	AND FLOOR(fOldCP) < FLOOR(g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage)
		CPRINTLN(DEBUG_ACHIEVEMENT,"[ACHIEVEMENT]: ACH11 - increasing achievement progress from ",fOldCP," to ",g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage)
		SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH11), FLOOR(g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage))
	ENDIF


    //Do not check in until 889962 is resolved.
    

    //IF NOT SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()  // Fixes for 1371101. John Hynd has kindly added DATAFILE_IS_SAVE_PENDING in url:bugstar:1373747
    //IF NOT IS_TRANSITION_SESSION_LAUNCHING()
                   
                                                           
        IF NOT DATAFILE_IS_SAVE_PENDING()  //make sure another cloud upload operation is already in progress.

                IF NOT g_bInMultiplayer //Fix for cloud conflict with Bobby's MP use. Bug 889955.
            
                    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR) = FALSE
                    AND NOT NETWORK_IS_GAME_IN_PROGRESS()



                            IF NETWORK_IS_CLOUD_AVAILABLE()

                            //IF NETWORK_IS_SIGNED_ONLINE()  //Check for bug 889024
                            //AND NETWORK_IS_SIGNED_IN()




                                //MASTER CLOUD TRIGGER!

                                //g_Trigger_CloudUpload_of_CP_Elements = TRUE //Let CompletionPercentage_Controller know that it should trigger a Cloud Upload.




                                //#if IS_DEBUG_BUILD
                                        
                                    //PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE returned TRUE. Setting global trigger...") 
                                    //PRINTNL()
                                        
                                //#endif



                                g_Trigger_CloudUpload_of_CP_Elements = FALSE //Let CompletionPercentage_Controller know that it should trigger a Cloud Upload.




                                #if IS_DEBUG_BUILD
                                        
                                    PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE but .json export no longer required. Profile Stats now used. #1498076") 
                                    PRINTNL()
                                        
                                #endif

                                



                            ELSE


                                #if IS_DEBUG_BUILD
                                        
                                    PRINTSTRING("[CompletionPercentage_Private] Primary NETWORK_IS_CLOUD_AVAILABLE returned FALSE. No upload action taken.") 
                                    PRINTNL()
                                        
                                #endif


                            ENDIF


                            //No longer called during debug launches -BenR
                            #if IS_DEBUG_BUILD
                                IF NOT g_flowUnsaved.bUpdatingGameflow 
                            #endif
                            
                                //This means a mission has delayed a mission passed routine. Saving during these periods is unsafe (for example during a planning board sequence).
                                //A save will always run directly after the stat block is lifted. -BenR.
                                IF NOT g_bMissionStatSystemBlocker 
                                
                                    MAKE_AUTOSAVE_REQUEST() //Added in accordance with bug #1079936.
                                    
                                ENDIF
                                    
                            #if IS_DEBUG_BUILD
                                ENDIF
                            #endif

                    ELSE

                            #if IS_DEBUG_BUILD
                                        
                                PRINTSTRING("[CompletionPercentage_Private] Recalculate operation cued in MP, will NOT cue cloud upload for this.") 
                                PRINTNL()
                                        
                            #endif

                    
                    ENDIF
            
                ENDIF //end of NOT g_bInMultiplayer condition check.



        ELSE


            #if IS_DEBUG_BUILD
        
                PRINTSTRING("[CompletionPercentage_Private] Cannot upload to the cloud at this time as DATAFILE_IS_SAVE_PENDING() has returned true.")
                PRINTNL()

            #endif

        ENDIF //end of NOT datafile_is_save_pending condition check.


    //ENDIF
    //ENDIF

 ENDPROC





 
