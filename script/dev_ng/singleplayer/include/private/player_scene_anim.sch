USING "familyAnim_private.sch"
USING "player_scene_assets.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_anim.sch										//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

// *******************************************************************************************
//	SCENE TIMETABLE
// *******************************************************************************************

PROC InitialisePlaceholderPlayerAnim(PED_REQUEST_SCENE_ENUM ePedScene, TEXT_LABEL_63 &tPlayerAnimDict, TEXT_LABEL_63 &tPlayerAnimLoop, TEXT_LABEL_63 &tPlayerAnimOut,
		STRING sPlayerAnimDict, STRING sPlayerAnimLoop, STRING sPlayerAnimOut)	//, enumPlayerSceneAnimProgress &ePlayerSceneAnimProgress)
	
	PRINTSTRING("placeholder anim for scene ")
	#IF IS_DEBUG_BUILD
	PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(ePedScene))
	#ENDIF
	#IF NOT IS_DEBUG_BUILD
	PRINTSTRING("pr_scene_")
	PRINTINT(ENUM_TO_INT(ePedScene))
	#ENDIF
	PRINTSTRING(" (\"")
	PRINTSTRING(sPlayerAnimDict)
	PRINTSTRING("\", \"")
	PRINTSTRING(sPlayerAnimLoop)
	PRINTSTRING("\", \"")
	PRINTSTRING(sPlayerAnimOut)
	PRINTSTRING("\")")
	PRINTNL()
	
	tPlayerAnimDict		= sPlayerAnimDict
	tPlayerAnimLoop		= sPlayerAnimLoop
	tPlayerAnimOut		= sPlayerAnimOut
	
	//ePlayerSceneAnimProgress = PAP_1_placeholder
	
ENDPROC

PROC InitialisePlaceholderPlayerSmokeAnim(PED_REQUEST_SCENE_ENUM ePedScene, TEXT_LABEL_63 &tPlayerAnimDict, TEXT_LABEL_63 &tPlayerAnimLoop, TEXT_LABEL_63 &tPlayerAnimOut)	//, enumPlayerSceneAnimProgress &ePlayerSceneAnimProgress)
	
	tPlayerAnimDict		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"	//"amb@hooker",
	tPlayerAnimLoop		= "IDLE_A"										//"smoke_stand_a",
	tPlayerAnimOut		= "IDLE_A"										//"smoke_stub_out",
	
	PRINTSTRING("smoking placeholder anim for scene ")
	#IF IS_DEBUG_BUILD
	PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(ePedScene))
	#ENDIF
	#IF NOT IS_DEBUG_BUILD
	PRINTSTRING("pr_scene_")
	PRINTINT(ENUM_TO_INT(ePedScene))
	#ENDIF
	PRINTSTRING(" (\"")
	PRINTSTRING(tPlayerAnimDict)
	PRINTSTRING("\", \"")
	PRINTSTRING(tPlayerAnimLoop)
	PRINTSTRING("\", \"")
	PRINTSTRING(tPlayerAnimOut)
	PRINTSTRING("\")")
	PRINTNL()
	//ePlayerSceneAnimProgress = PAP_1_placeholder
	
ENDPROC

// *******************************************************************************************
//	SCENE PRIVATE EVENT FUNCTIONS
// *******************************************************************************************



FUNC BOOL BLENDOUT_OF_SYNCH_TASK_FOR_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene, INT &iSimulatePlayerInputGait)

	SWITCH ePedScene
		CASE PR_SCENE_M7_WIFETENNIS			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA		iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
//		CASE PR_SCENE_M7_RESTAURANT			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_GAMING		iSimulatePlayerInputGait = (0750) RETURN TRUE BREAK
		CASE PR_SCENE_T_ESCORTED_OUT		iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_DUMPSTER			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_a			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_T6_DIGGING			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE		iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
//		CASE PR_SCENE_T_DOCKS_a				iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_M4_WASHFACE			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
//		CASE PR_SCENE_M_HOOKERMOTEL			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_M7_HOOKERS			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
		CASE PR_SCENE_F0_GARBAGE			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
//		CASE PR_SCENE_F1_GARBAGE			iSimulatePlayerInputGait = (1500) RETURN TRUE BREAK
			
	ENDSWITCH
	
	iSimulatePlayerInputGait = -1
	RETURN FALSE
ENDFUNC

PROC P_GET_PLAYER_SYNCHRONIZED_SCENE_DATA(PED_REQUEST_SCENE_ENUM ePedScene, SYNCED_SCENE_PLAYBACK_FLAGS &scpf_Flags, FLOAT &fBlendIn, FLOAT &fBlendOut, RAGDOLL_BLOCKING_FLAGS &ragdollFlags, IK_CONTROL_FLAGS &ikFlags)
	fBlendIn = INSTANT_BLEND_IN
	fBlendOut = WALK_BLEND_OUT
	scpf_Flags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
	ragdollFlags = RBF_NONE
	ikFlags = AIK_NONE
	
	SWITCH ePedScene
		CASE PR_SCENE_M_BENCHCALL_a
		CASE PR_SCENE_M_BENCHCALL_b
		CASE PR_SCENE_M6_ONPHONE
		CASE PR_SCENE_M6_DEPRESSED
			fBlendIn = INSTANT_BLEND_IN
			fBlendOut = WALK_BLEND_OUT
		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_T6_DIGGING
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
			fBlendOut = SLOW_BLEND_OUT		//1175231
		BREAK
		CASE PR_SCENE_T_CR_DUMPSTER
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
//		CASE PR_SCENE_M7_RESTAURANT
//			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
//		BREAK
//		CASE PR_SCENE_T_DOCKS_a
//			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
//		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_M4_WASHFACE
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
//		CASE PR_SCENE_M_HOOKERMOTEL
//			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
//		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			ikFlags = AIK_DISABLE_HEAD_IK
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			ikFlags = AIK_DISABLE_HEAD_IK
		BREAK
		CASE PR_SCENE_M7_HOOKERS
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_F0_GARBAGE
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		
		CASE PR_SCENE_F_LAMTAUNT_P5
			ikFlags = AIK_DISABLE_HEAD_IK
		BREAK
		
//		CASE PR_SCENE_F1_GARBAGE
//			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
//		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	INT iTempSimulatePlayerInputGait
	IF BLENDOUT_OF_SYNCH_TASK_FOR_TIMETABLE_SCENE(ePedScene, iTempSimulatePlayerInputGait)
		IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(scpf_Flags, SYNCED_SCENE_TAG_SYNC_OUT)
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
			
			SCRIPT_ASSERT("blendout of synch task without TAG_SYNC_OUT??")
		ENDIF
	ENDIF
	#ENDIF
	
ENDPROC

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene,
		TEXT_LABEL_63 &tPlayerAnimDict, TEXT_LABEL_63 &tPlayerAnimLoop, TEXT_LABEL_63 &tPlayerAnimOut,
		ANIMATION_FLAGS &playerAnimLoopFlag, ANIMATION_FLAGS &playerAnimOutFlag)	//, enumPlayerSceneAnimProgress &ePlayerSceneAnimProgress)
	
	playerAnimLoopFlag	= AF_LOOPING | AF_NOT_INTERRUPTABLE
	playerAnimOutFlag	= AF_DEFAULT
	
	//ePlayerSceneAnimProgress = PAP_0_default
	
	SWITCH ePedScene
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3
			tPlayerAnimDict		= "SWITCH@FRANKLIN@STRIPCLUB"
			tPlayerAnimLoop		= "002113_02_FRAS_15_STRIPCLUB_IDLE"
			tPlayerAnimOut		= "002113_02_FRAS_15_STRIPCLUB_EXIT"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_ARM3
//			InitialisePlaceholderPlayerSmokeAnim(PED_REQUEST_SCENE_ENUM ePedScene, TEXT_LABEL_63 &tPlayerAnimDict, TEXT_LABEL_63 &tPlayerAnimLoop, TEXT_LABEL_63 &tPlayerAnimOut, enumPlayerSceneAnimProgress &ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1
//			tPlayerAnimDict		= "SWITCH@FRANKLIN@STRIPCLUB2"
//			tPlayerAnimLoop		= "IG_16_BASE"
//			tPlayerAnimOut		= "IG_16_EXIT"
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3
//			tPlayerAnimDict		= "SWITCH@FRANKLIN@STRIPCLUB3"
//			tPlayerAnimLoop		= "IG_17_BASE"
//			tPlayerAnimOut		= "IG_17_EXIT"
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_FAMILY1
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Fa_FAMILY3
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Fa_FBI1
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Fa_FBI4
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Ma_FAMILY4_a
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_M_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Ma_CARSTEAL1
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_M_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Fa_AGENCY1
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
		

//		CASE PR_SCENE_Ma_EXILE2
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_T_SMOKEMETH,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
		CASE PR_SCENE_M4_WASHFACE
			tPlayerAnimDict		= "SWITCH@MICHAEL@WASH_FACE"
			tPlayerAnimLoop		= "LOOP_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_Ma_AGENCY2A
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_T_SWEATSHOP,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Ma_AGENCY3A
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_T_SWEATSHOP,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
//		CASE PR_SCENE_Fa_RURAL2A
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F_TRAFFIC_a,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK

		
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
			tPlayerAnimDict		= "SWITCH@TREVOR@ON_TOILET"
			tPlayerAnimLoop		= "trev_on_toilet_loop"
			tPlayerAnimOut		= "trev_on_toilet_exit"
			
			playerAnimLoopFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE | AF_LOOPING
			playerAnimOutFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_JERKOFF
			tPlayerAnimDict		= "SWITCH@TREVOR@JERKING_OFF"
			tPlayerAnimLoop		= "trev_jerking_off_loop"
			tPlayerAnimOut		= "trev_jerking_off_exit"
			
			playerAnimLoopFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE | AF_LOOPING
			playerAnimOutFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE
			
			RETURN TRUE
		BREAK

		#ENDIF
		CASE PR_SCENE_M2_BEDROOM
			tPlayerAnimDict		= "SWITCH@MICHAEL@BEDROOM"
			tPlayerAnimLoop		= "BED_LOOP_Michael"
			tPlayerAnimOut		= "BED_EXIT_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@BEDROOM2"
			tPlayerAnimLoop		= "BED_LOOP_Michael"
			tPlayerAnimOut		= "BED_EXIT_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a
			tPlayerAnimDict		= "SAFE@MICHAEL@IG_3"
			tPlayerAnimLoop		= "BASE_MICHAEL"
			tPlayerAnimOut		= "EXIT_MICHAEL"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@SITTING"
			tPlayerAnimLoop		= "IDLE"
			tPlayerAnimOut		= "EXIT_FORWARD"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WAKEUPSCREAM
//		CASE PR_SCENE_M4_HOUSEBED_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@WAKES_UP_SCREAMING"
			tPlayerAnimLoop		= "001671_02_MICS2_1_WAKES_UP_SCREAMING_IDLE"
			tPlayerAnimOut		= "001671_02_MICS2_1_WAKES_UP_SCREAMING_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_HOUSEBED
			tPlayerAnimDict		= "SAVECountryside@"
			tPlayerAnimLoop		= "M_Sleep_Loop_countryside"
			tPlayerAnimOut		= "M_GetOut_countryside"
			
//x:\gta5\art\anim\export_mb\SAVE\Countryside@\M_GetIn_countryside.anim
//x:\gta5\art\anim\export_mb\SAVE\Countryside@\M_GetIn_countryside_CAM.anim
//x:\gta5\art\anim\export_mb\SAVE\Countryside@\M_GetOut_countryside.anim
//x:\gta5\art\anim\export_mb\SAVE\Countryside@\M_GetOut_countryside_CAM.anim
//x:\gta5\art\anim\export_mb\SAVE\Countryside@\M_Sleep_Loop_countryside.anim
//x:\gta5\art\anim\export_mb\SAVE\Countryside@\M_Sleep_Loop_countryside_CAM.anim


			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED
			tPlayerAnimDict		= "SWITCH@MICHAEL@WAKES_UP_SCARED"
			tPlayerAnimLoop		= "001672_02_MICS2_1_WAKES_UP_SCARED_IDLE"
			tPlayerAnimOut		= "001672_02_MICS2_1_WAKES_UP_SCARED_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WATCHINGTV
			tPlayerAnimDict		= "SWITCH@MICHAEL@WATCHING_TV"
			tPlayerAnimLoop		= "LOOP_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE
			tPlayerAnimDict		= "SWITCH@TREVOR@WATCHING_TV"
			tPlayerAnimLoop		= "LOOP"
			tPlayerAnimOut		= "EXIT"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M6_HOUSETV_b
		CASE PR_SCENE_M6_HOUSETV_a
			tPlayerAnimDict		= "SWITCH@MICHAEL@SITTING"
			tPlayerAnimLoop		= "IDLE"
			tPlayerAnimOut		= "EXIT_FORWARD"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_KIDS_TV
			tPlayerAnimDict		= "SWITCH@MICHAEL@ON_SOFA"
			tPlayerAnimLoop		= "BASE_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_POOLSIDE_a
		CASE PR_SCENE_M_POOLSIDE_b
		CASE PR_SCENE_F1_POOLSIDE_a
		CASE PR_SCENE_F1_POOLSIDE_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@SUNLOUNGER"
			tPlayerAnimLoop		= "SunLounger_Idle"		//(Michael Idling on the Sunlounger Loop)
			tPlayerAnimOut		= "SunLounger_GetUp"	//(Michael Getting up off the Sunlounger into his standing idle)
			
			playerAnimLoopFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE | AF_LOOPING
			playerAnimOutFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_CARSLEEP_a
		CASE PR_SCENE_M2_CARSLEEP_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@SLEEP_IN_CAR"
			tPlayerAnimLoop		= "BASE_MICHAEL"
			tPlayerAnimOut		= "SLEEP_IN_CAR_MICHAEL"
			
			//ePlayerSceneAnimProgress = PAP_0_default
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_CARSLEEP
			tPlayerAnimDict		= "SWITCH@MICHAEL@SLEEP_IN_CAR"
			tPlayerAnimLoop		= "BASE_PREMIER_MICHAEL"
			tPlayerAnimOut		= "SLEEP_IN_CAR_PREMIER_MICHAEL"
			
			//ePlayerSceneAnimProgress = PAP_0_default
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_CANAL_a
		CASE PR_SCENE_M_CANAL_b
		CASE PR_SCENE_M_CANAL_c
		CASE PR_SCENE_M_PIER_b
		CASE PR_SCENE_M2_SMOKINGGOLF
		CASE PR_SCENE_M6_MORNING_a
			tPlayerAnimDict		= "SWITCH@MICHAEL@SMOKING2"
			tPlayerAnimLoop		= "LOOP"
			tPlayerAnimOut		= "EXIT"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			tPlayerAnimDict		= "SWITCH@MICHAEL@CAFE"
			tPlayerAnimLoop		= "LOOP_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_LUNCH_b
//			tPlayerAnimDict		= "SWITCH@MICHAEL@CAFE2"
//			tPlayerAnimLoop		= "LOOP_Michael"
//			tPlayerAnimOut		= "EXIT_Michael"
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT
//		CASE PR_SCENE_M4_LUNCH_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@EXIT_RESTAURANT"
			tPlayerAnimLoop		= "mic_exit_restaurant_loop"
			tPlayerAnimOut		= "mic_exit_restaurant_exit"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M_BAR_a
//		CASE PR_SCENE_M_BAR_b
		CASE PR_SCENE_M6_LIQUORSTORE
			tPlayerAnimDict		= "SWITCH@MICHAEL@DRUNK_BAR"
			tPlayerAnimLoop		= "Drunk_Idle_PED"
			tPlayerAnimOut		= "Drunk_Exit_PED"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_VWOODPARK_a
		CASE PR_SCENE_M_VWOODPARK_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@PARKBENCH_SMOKE_RANGER"
			tPlayerAnimLoop		= "parkbench_smoke_ranger_loop"
			tPlayerAnimOut		= "parkbench_smoke_ranger_exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_PARKEDHILLS_a
		CASE PR_SCENE_M_PARKEDHILLS_b
		CASE PR_SCENE_M4_PARKEDBEACH
			tPlayerAnimDict		= "SWITCH@MICHAEL@SITTING_ON_CAR_BONNET"
			tPlayerAnimLoop		= "sitting_on_car_bonnet_loop"
			tPlayerAnimOut		= "sitting_on_car_bonnet_exit"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_RURAL1
		CASE PR_SCENE_M6_PARKEDHILLS_a
		CASE PR_SCENE_M6_PARKEDHILLS_b
		CASE PR_SCENE_M6_PARKEDHILLS_c
		CASE PR_SCENE_M6_PARKEDHILLS_d
		CASE PR_SCENE_M6_PARKEDHILLS_e
			tPlayerAnimDict		= "SWITCH@MICHAEL@SITTING_ON_CAR_PREMIERE"
			tPlayerAnimLoop		= "SITTING_ON_CAR_PREMIERE_LOOP_PLAYER"
			tPlayerAnimOut		= "SITTING_ON_CAR_PREMIERE_EXIT_PLAYER"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_PHARMACY
			tPlayerAnimDict		= "SWITCH@MICHAEL@PHARMACY"
			tPlayerAnimLoop		= "mics1_ig_11_loop"
			tPlayerAnimOut		= "mics1_ig_11_exit"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M4_DOORSTUMBLE
//			tPlayerAnimDict		= "SWITCH@MICHAEL@STUMBLES_THROUGH_DOORS"
//			tPlayerAnimLoop		= "000610_03_MICS2_4_STUMBLES_THROUGH_DOORS_IDLE"	//(Michael Generally Aggitated)
//			tPlayerAnimOut		= "000610_03_MICS2_4_STUMBLES_THROUGH_DOORS_EXIT"		//(Michael Hitting the Steering wheel)
//
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M_TRAFFIC_a
			tPlayerAnimDict		= "SWITCH@MICHAEL@STUCKINTRAFFIC"
			tPlayerAnimLoop		= ""
			tPlayerAnimOut		= "StuckInTraffic_Aggitated"	//(Michael Generally Aggitated)
//			tPlayerAnimOut		= "StuckInTraffic_HitWheel"		//(Michael Hitting the Steering wheel)
//			tPlayerAnimOut		= "StuckInTraffic_HitHorn"		//(Michael Hitting the Steering wheel Horn)
			
//			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_SECONDARY
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_TRAFFIC_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@STUCKINTRAFFIC"
			tPlayerAnimLoop		= ""
//			tPlayerAnimOut		= "StuckInTraffic_Aggitated"	//(Michael Generally Aggitated)
			tPlayerAnimOut		= "StuckInTraffic_HitWheel"		//(Michael Hitting the Steering wheel)
//			tPlayerAnimOut		= "StuckInTraffic_HitHorn"		//(Michael Hitting the Steering wheel Horn)
			
//			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_SECONDARY
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_TRAFFIC_c
			tPlayerAnimDict		= "SWITCH@MICHAEL@STUCKINTRAFFIC"
			tPlayerAnimLoop		= ""
//			tPlayerAnimOut		= "StuckInTraffic_Aggitated"	//(Michael Generally Aggitated)
//			tPlayerAnimOut		= "StuckInTraffic_HitWheel"		//(Michael Hitting the Steering wheel)
			tPlayerAnimOut		= "StuckInTraffic_HitHorn"		//(Michael Hitting the Steering wheel Horn)
			
//			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_SECONDARY
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_WIFEEXITSCAR
		//	SWITCH@MICHAEL@AMANDA_EXITS_CAR/000606_02_MICS1_5_AMANDA_EXITS_CAR_IDLE_MIC.anim
		//	SWITCH@MICHAEL@AMANDA_EXITS_CAR/000606_02_MICS1_5_AMANDA_EXITS_CAR_EXIT_MIC.anim
			
			tPlayerAnimDict		= "SWITCH@MICHAEL@AMANDA_EXITS_CAR"
			tPlayerAnimLoop		= "000606_02_MICS1_5_AMANDA_EXITS_CAR_IDLE_MIC"
			tPlayerAnimOut		= "000606_02_MICS1_5_AMANDA_EXITS_CAR_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA
			tPlayerAnimDict		= "SWITCH@MICHAEL@OPENS_DOOR_FOR_AMA"
			tPlayerAnimLoop		= "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_IDLE_MIC"
			tPlayerAnimOut		= "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			tPlayerAnimDict		= "SWITCH@MICHAEL@DROPPING_OFF_JMY"
			tPlayerAnimLoop		= "001839_02_MICS3_20_DROPPING_OFF_JMY_IDLE_MIC"
			tPlayerAnimOut		= "001839_02_MICS3_20_DROPPING_OFF_JMY_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
//		CASE PR_SCENE_M_HOOKERCAR
			tPlayerAnimDict		= "SWITCH@MICHAEL@TRACY_EXITS_CAR"
			tPlayerAnimLoop		= "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_IDLE_MIC"
			tPlayerAnimOut		= "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_MIC"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_PIER_a
			tPlayerAnimDict		= "SWITCH@MICHAEL@PIER"
			tPlayerAnimLoop		= "pier_lean_smoke_idle"
			tPlayerAnimOut		= "pier_lean_smoke_outro"
			
//			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_REPOSITION_WHEN_FINISHED | AF_REORIENT_WHEN_FINISHED
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_MARINA
			tPlayerAnimDict		= "SWITCH@MICHAEL@MARINA"
			tPlayerAnimLoop		= "loop"
			tPlayerAnimOut		= "exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE
			tPlayerAnimDict		= "SWITCH@MICHAEL@ARGUE_WITH_AMANDA"
			tPlayerAnimLoop		= "argue_with_amanda_loop_michael"
			tPlayerAnimOut		= "argue_with_amanda_exit_michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_HOOKERMOTEL
			tPlayerAnimDict		= "SWITCH@MICHAEL@WALK_AND_TALK"
			tPlayerAnimLoop		= ""
			tPlayerAnimOut		= "Im_A_Married_Man_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_COFFEE_a
		CASE PR_SCENE_M_COFFEE_b
		CASE PR_SCENE_M_COFFEE_c
		CASE PR_SCENE_M4_CINEMA
		CASE PR_SCENE_M7_COFFEE
			tPlayerAnimDict		= "SWITCH@MICHAEL@CAFE"
			tPlayerAnimLoop		= "Cafe_Idle_PED"
			tPlayerAnimOut		= "Cafe_Exit_PED"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_BENCHCALL_a
		CASE PR_SCENE_M_BENCHCALL_b
		CASE PR_SCENE_M6_ONPHONE
		CASE PR_SCENE_M6_DEPRESSED
			/*
x:\gta5\art\anim\export_mb\SWITCH@\MICHAEL@\BENCH\CELLPHONE_CALL_LISTEN_BASE.anim
x:\gta5\art\anim\export_mb\SWITCH@\MICHAEL@\BENCH\EXIT_FORWARD.anim
x:\gta5\art\anim\export_mb\SWITCH@\MICHAEL@\BENCH\CELLPHONE_CALL_OUT.anim
			*/
			tPlayerAnimDict		= "SWITCH@MICHAEL@BENCH"
			tPlayerAnimLoop		= "bench_on_phone_idle"				//"bench_on_phone_idle"
			tPlayerAnimOut		= "EXIT_FORWARD"					//"bench_on_phone_outro"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS | AF_HOLD_LAST_FRAME
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_SUNBATHING
			tPlayerAnimDict		= "SWITCH@MICHAEL@ON_CLUBCHAIR"
			tPlayerAnimLoop		= "Switch_ON_CLUBCHAIR_BASE"
			tPlayerAnimOut		= "Switch_ON_CLUBCHAIR"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_DRINKINGBEER
			tPlayerAnimDict		= "SAFE@MICHAEL@IG_5"
			tPlayerAnimLoop		= "BASE_MICHAEL"
			tPlayerAnimOut		= "EXIT_1_MICHAEL"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_RONBORING
			tPlayerAnimDict		= "SWITCH@MICHAEL@RONEX_IG_5_P2"
			tPlayerAnimLoop		= "BASE_MICHAEL"
			tPlayerAnimOut		= "RONEX_IG5_P2_MICHAEL"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M7_RESTAURANT
			tPlayerAnimDict		= "SWITCH@MICHAEL@RESTAURANT"
			tPlayerAnimLoop		= "001510_02_GC_MICS3_IG_1_BASE_MICHAEL"
			tPlayerAnimOut		= "001510_02_GC_MICS3_IG_1_EXIT_MICHAEL"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			tPlayerAnimDict		= "SWITCH@MICHAEL@LOUNGE_CHAIRS"
			tPlayerAnimLoop		= "001523_01_MICS3_9_LOUNGE_CHAIRS_IDLE_MIC"
			tPlayerAnimOut		= "001523_01_MICS3_9_LOUNGE_CHAIRS_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a
			tPlayerAnimDict		= "SWITCH@MICHAEL@GOODBYE_TO_SOLOMAN"
			tPlayerAnimLoop		= "001400_01_MICS3_5_BYE_TO_SOLOMAN_IDLE"
			tPlayerAnimOut		= "001400_01_MICS3_5_BYE_TO_SOLOMAN_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b
			tPlayerAnimDict		= "SWITCH@MICHAEL@GOODBYE_TO_SOLOMAN"
			tPlayerAnimLoop		= "LOOP_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			tPlayerAnimDict		= "SWITCH@MICHAEL@AMA_TENNIS"
			tPlayerAnimLoop		= "001833_01_MICS3_18_AMA_TENNIS_IDLE_MIC"
			tPlayerAnimOut		= "001833_01_MICS3_18_AMA_TENNIS_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			tPlayerAnimDict		= "SWITCH@MICHAEL@AROUND_THE_TABLE_SELFISH"
			tPlayerAnimLoop		= "AROUND_THE_TABLE_SELFISH_BASE_Michael"
			tPlayerAnimOut		= "AROUND_THE_TABLE_SELFISH_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_REJECTENTRY
			tPlayerAnimDict		= "SWITCH@MICHAEL@REJECTED_ENTRY"
			tPlayerAnimLoop		= "001396_01_MICS3_6_REJECTED_ENTRY_IDLE_MIC"
			tPlayerAnimOut		= "001396_01_MICS3_6_REJECTED_ENTRY_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITBARBER
			tPlayerAnimDict		= "SWITCH@MICHAEL@EXITS_BARBER"
			tPlayerAnimLoop		= "001406_01_MICS3_7_EXITS_BARBER_IDLE"
			tPlayerAnimOut		= "001406_01_MICS3_7_EXITS_BARBER_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP
			tPlayerAnimDict		= "SWITCH@MICHAEL@EXITS_FANCYSHOP"
			tPlayerAnimLoop		= "001405_01_MICS3_8_EXITS_FANCYSHOP_IDLE"
			tPlayerAnimOut		= "001405_01_MICS3_8_EXITS_FANCYSHOP_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA
			tPlayerAnimDict		= "SWITCH@MICHAEL@SMOKING"
			tPlayerAnimLoop		= "LOOP_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_GETSREADY
			tPlayerAnimDict		= "SWITCH@MICHAEL@GETS_READY"
			tPlayerAnimLoop		= "001520_02_MICS3_14_GETS_READY_IDLE_MIC"
			tPlayerAnimOut		= "001520_02_MICS3_14_GETS_READY_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_READSCRIPT
			tPlayerAnimDict		= "SWITCH@MICHAEL@READS_SCRIPT"
			tPlayerAnimLoop		= "001404_01_MICS3_16_READS_SCRIPT_IDLE"
			tPlayerAnimOut		= "001404_01_MICS3_16_READS_SCRIPT_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO
			tPlayerAnimDict		= "SWITCH@MICHAEL@BAR_EMPLOYEE_CONVO"
			tPlayerAnimLoop		= "001387_03_MICS3_2_BAR_EMPLOYEE_CONVO_IDLE_MIC"
			tPlayerAnimOut		= "001387_03_MICS3_2_BAR_EMPLOYEE_CONVO_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_HOOKERS
			tPlayerAnimDict		= "SWITCH@MICHAEL@PROSTITUTE"
			tPlayerAnimLoop		= "BASE_MICHAEL"
			tPlayerAnimOut		= "EXIT_MICHAEL"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TALKTOGUARD
			tPlayerAnimDict		= "SWITCH@MICHAEL@TALKS_TO_GUARD"
			tPlayerAnimLoop		= "001393_02_MICS3_3_TALKS_TO_GUARD_IDLE_MIC"
			tPlayerAnimOut		= "001393_02_MICS3_3_TALKS_TO_GUARD_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOT_JIMMY
			tPlayerAnimDict		= "SWITCH@MICHAEL@ON_SET_W_JMY"
			tPlayerAnimLoop		= ""
			tPlayerAnimOut		= "001513_03_GC_MICS3_IG_4_ON_SET_W_JMY_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_TV
			tPlayerAnimDict		= "SWITCH@MICHAEL@TV_W_KIDS"
			tPlayerAnimLoop		= "001520_02_MICS3_14_TV_W_KIDS_IDLE_MIC"
			tPlayerAnimOut		= "001520_02_MICS3_14_TV_W_KIDS_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			tPlayerAnimDict		= "SWITCH@MICHAEL@BIKING_WITH_JIMMY"
			tPlayerAnimLoop		= "LOOP_Michael"
			tPlayerAnimOut		= "EXIT_Michael"
			


//SWITCH@/MICHAEL@/BIKING_WITH_JIMMY/LOOP_Cruiser.anim
//SWITCH@/MICHAEL@/BIKING_WITH_JIMMY/EXIT_Cruiser.anim

//SWITCH@/MICHAEL@/BIKING_WITH_JIMMY/LOOP_TriBike.anim
//SWITCH@/MICHAEL@/BIKING_WITH_JIMMY/EXIT_TriBike.anim
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			tPlayerAnimDict		= "SWITCH@MICHAEL@GAMING_W_JMY"
			tPlayerAnimLoop		= "001518_02_MICS3_11_GAMING_W_JMY_IDLE_MIC"
			tPlayerAnimOut		= "001518_02_MICS3_11_GAMING_W_JMY_EXIT_MIC"
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_M_S_FAMILY4
//			tPlayerAnimDict		= "SWITCH@MICHAEL@FAMILY_4_INTP1"
//			tPlayerAnimLoop		= "idle_LOOP"
//			tPlayerAnimOut		= "idle_LOOP"
//			
//			RETURN TRUE
//		BREAK
		
	ENDSWITCH
	SWITCH ePedScene
		CASE PR_SCENE_F1_CLEANINGAPT
			tPlayerAnimDict		= "SWITCH@FRANKLIN@CLEANING_APT"
			tPlayerAnimLoop		= "001918_01_FRAS_V2_1_CLEANING_APT_IDLE"
			tPlayerAnimOut		= "001918_01_FRAS_V2_1_CLEANING_APT_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONCELL
			tPlayerAnimDict		= "SWITCH@FRANKLIN@ON_CELL"
			tPlayerAnimLoop		= "001914_01_FRAS_V2_2_ON_CELL_IDLE"
			tPlayerAnimOut		= "001914_01_FRAS_V2_2_ON_CELL_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_SNACKING
			tPlayerAnimDict		= "SWITCH@FRANKLIN@SNACKING"
			tPlayerAnimLoop		= "001922_01_FRAS_V2_3_SNACKING_IDLE"
			tPlayerAnimOut		= "001922_01_FRAS_V2_3_SNACKING_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			tPlayerAnimDict		= "SWITCH@FRANKLIN@ON_LAPTOP"
			tPlayerAnimLoop		= "001927_01_FRAS_V2_4_ON_LAPTOP_IDLE"
			tPlayerAnimOut		= "001927_01_FRAS_V2_4_ON_LAPTOP_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_IRONING
			tPlayerAnimDict		= "SWITCH@FRANKLIN@IRONING"
			tPlayerAnimLoop		= "001947_01_GC_FRAS_V2_IG_6_BASE"
			tPlayerAnimOut		= "001947_01_GC_FRAS_V2_IG_6_EXIT"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F0_WATCHINGTV
//			//PR_SCENE_F1_WATCHINGTV		//PR_SCENE_M2_KIDS_TV
//			tPlayerAnimDict		= "SWITCH@FRANKLIN@WATCHING_TV"
//			tPlayerAnimLoop		= "001915_01_FRAS_V2_8_WATCHING_TV_IDLE"
//			tPlayerAnimOut		= "001915_01_FRAS_V2_8_WATCHING_TV_EXIT"
//			
//			tPlayerAnimDict		= "SWITCH@MICHAEL@WATCHING_TV"
//			tPlayerAnimLoop		= "LOOP_Michael"
//			tPlayerAnimOut		= "EXIT_Michael"
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F1_WATCHINGTV
			tPlayerAnimDict		= "SWITCH@FRANKLIN@WATCHING_TV"
			tPlayerAnimLoop		= "001915_01_FRAS_V2_8_WATCHING_TV_IDLE"
			tPlayerAnimOut		= "001915_01_FRAS_V2_8_WATCHING_TV_EXIT"
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_F0_SAVEHOUSE
//			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F0_SH_ASLEEP
		CASE PR_SCENE_F1_SH_ASLEEP
			tPlayerAnimDict		= "SWITCH@FRANKLIN@BED"
			tPlayerAnimLoop		= "Sleep_LOOP"
			tPlayerAnimOut		= "Sleep_GetUp_RubEyes"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_NAPPING
			tPlayerAnimDict		= "SWITCH@FRANKLIN@NAPPING"
			tPlayerAnimLoop		= "002333_01_FRAS_V2_10_NAPPING_IDLE"
			tPlayerAnimOut		= "002333_01_FRAS_V2_10_NAPPING_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_GETTINGREADY
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GETTING_READY"
			tPlayerAnimLoop		= "002334_02_FRAS_V2_11_GETTING_DRESSED_IDLE"
			tPlayerAnimOut		= "002334_02_FRAS_V2_11_GETTING_DRESSED_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_READING
		CASE PR_SCENE_F1_SH_READING
			tPlayerAnimDict		= "SWITCH@FRANKLIN@BED"
			tPlayerAnimLoop		= "Bed_Reading_LOOP"		//(Franklin reading the book loop)
			tPlayerAnimOut		= "Bed_Reading_GetUp"		//(Franklin Gets up and drops the book back on the bed)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a
		CASE PR_SCENE_F0_SH_PUSHUP_b
		CASE PR_SCENE_F1_SH_PUSHUP
			tPlayerAnimDict		= "SWITCH@FRANKLIN@PRESS_UPS"
			tPlayerAnimLoop		= "PressUps_LOOP"			//"PressUps_INTO"
			tPlayerAnimOut		= "PressUps_OUT"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS /*| AF_DISABLE_LEG_IK*/ 
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS /*| AF_DISABLE_LEG_IK*/ 
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_F1_SAVEHOUSE
//			RETURN GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F0_SAVEHOUSE,
//					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
//					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
//		BREAK
		
		 CASE PR_SCENE_F_MD_KUSH_DOC
			tPlayerAnimDict		= "SWITCH@FRANKLIN@002110_04_MAGD_3_WEED_EXCHANGE"
			tPlayerAnimLoop		= "002110_04_MAGD_3_WEED_EXCHANGE_SHOPKEEPER"
			tPlayerAnimOut		= "002110_04_MAGD_3_WEED_EXCHANGE_FRANKLIN"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a
			
			tPlayerAnimDict		= "SWITCH@FRANKLIN@DISPENSARY"
			tPlayerAnimLoop		= "exit_dispensary_idle"
			tPlayerAnimOut		= "exit_dispensary_outro_ped"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_b
			tPlayerAnimDict		= "SWITCH@FRANKLIN@DISPENSARY"
			tPlayerAnimLoop		= "exit_dispensary_idle"
			tPlayerAnimOut		= "exit_dispensary_outro"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_c
			tPlayerAnimDict		= "SWITCH@FRANKLIN@DISPENSARY"
			tPlayerAnimLoop		= "exit_dispensary_idle"
			tPlayerAnimOut		= "exit_dispensary_outro"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_GARBAGE
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GARBAGE"
			tPlayerAnimLoop		= "Garbage_Idle_PLYR"			//(Franklin idles holding the rubbish bag)
			tPlayerAnimOut		= "Garbage_Toss_PLYR"			//(Franklin goes and throws the rubbish onto the pavement and ends in his idle pose)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_GARBAGE
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GARBAGE_B"
			tPlayerAnimLoop		= "Garbage_Idle_PLYR"			//(Franklin idles holding the rubbish bag)
			tPlayerAnimOut		= "Garbage_Toss_PLYR"			//(Franklin goes and throws the rubbish onto the pavement and ends in his idle pose)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_THROW_CUP
			tPlayerAnimDict		= "SWITCH@FRANKLIN@THROW_CUP"
			tPlayerAnimLoop		= "throw_cup_loop"
			tPlayerAnimOut		= "throw_cup_exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			tPlayerAnimDict		= "SWITCH@FRANKLIN@HIT_CUP_HAND"
			tPlayerAnimLoop		= "hit_cup_hand_loop"
			tPlayerAnimOut		= "hit_cup_hand_exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_GYM
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GYM"
			tPlayerAnimLoop		= "001942_02_GC_FRAS_IG_5_BASE"
			tPlayerAnimOut		= "001942_02_GC_FRAS_IG_5_EXIT"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F0_WALKCHOP
//		CASE PR_SCENE_F1_WALKCHOP
		CASE PR_SCENE_F_WALKCHOP_a
		CASE PR_SCENE_F_WALKCHOP_b
			tPlayerAnimDict		= "SWITCH@FRANKLIN@PLAYS_W_DOG"
			tPlayerAnimLoop		= "001916_01_FRAS_V2_9_PLAYS_W_DOG_IDLE"
			tPlayerAnimOut		= "001916_01_FRAS_V2_9_PLAYS_W_DOG_EXIT"
			//ePlayerSceneAnimProgress = PAP_1_placeholder
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_PLAYCHOP
		CASE PR_SCENE_F1_PLAYCHOP
			tPlayerAnimDict		= "SWITCH@FRANKLIN@PLAYS_W_DOG"
			tPlayerAnimLoop		= "001916_01_FRAS_V2_9_PLAYS_W_DOG_IDLE"
			tPlayerAnimOut		= "001916_01_FRAS_V2_9_PLAYS_W_DOG_EXIT"
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F0_BIKE
		CASE PR_SCENE_F1_BIKE
		CASE PR_SCENE_F_BIKE_c
		CASE PR_SCENE_F_BIKE_d
			tPlayerAnimDict		= "SWITCH@FRANKLIN@ADMIRE_MOTORCYCLE"
			tPlayerAnimLoop		= "BASE_FRANKLIN"
			tPlayerAnimOut		= "EXIT_FRANKLIN"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_CLEANCAR
		CASE PR_SCENE_F1_CLEANCAR
			tPlayerAnimDict		= "SWITCH@FRANKLIN@CLEANING_CAR"
			tPlayerAnimLoop		= "001946_01_GC_FRAS_V2_IG_5_BASE"
			tPlayerAnimOut		= "001946_01_GC_FRAS_V2_IG_5_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT
			tPlayerAnimDict		= "SWITCH@FRANKLIN@TANISHA_ARGUE"
			tPlayerAnimLoop		= "BASE_Franklin"
			tPlayerAnimOut		= "Tanisha_Argue_Franklin"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_NEWHOUSE
			tPlayerAnimDict		= "SWITCH@FRANKLIN@WALK_AROUND_HOUSE"
			tPlayerAnimLoop		= "IDLE_FRANKLIN"
			tPlayerAnimOut		= "EXIT_FRANKLIN"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_BYETAXI
			tPlayerAnimDict		= "SWITCH@FRANKLIN@BYE_TAXI"
			tPlayerAnimLoop		= "001938_01_FRAS_V2_7_BYE_TAXI_IDLE_FRA"
			tPlayerAnimOut		= "001938_01_FRAS_V2_7_BYE_TAXI_EXIT_FRA"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_LAMGRAFF
//			tPlayerAnimDict		= "SWITCH@FRANKLIN@LAMAR_TAGGING_WALL"
//			tPlayerAnimLoop		= "lamar_tagging_wall_LOOP_FRANKLIN"
//			tPlayerAnimOut		= "lamar_tagging_wall_EXIT_FRANKLIN"
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F_CLUB
			tPlayerAnimDict		= "SWITCH@FRANKLIN@PICKUP_LINE"
			tPlayerAnimLoop		= "base_Franklin"
			tPlayerAnimOut		= "switch_P1_Franklin"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE
			tPlayerAnimDict		= "SWITCH@FRANKLIN@CHOPSHOP"
			tPlayerAnimLoop		= "BASE"
			tPlayerAnimOut		= "CheckShoe"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CS_WIPEHANDS
			tPlayerAnimDict		= "SWITCH@FRANKLIN@CHOPSHOP"
			tPlayerAnimLoop		= "BASE"
			tPlayerAnimOut		= "WipeHands"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT
			tPlayerAnimDict		= "SWITCH@FRANKLIN@CHOPSHOP"
			tPlayerAnimLoop		= "BASE"
			tPlayerAnimOut		= "WipeRight"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_a_01
		CASE PR_SCENE_F_BAR_b_01
		CASE PR_SCENE_F_BAR_e_01
			tPlayerAnimDict		= "SWITCH@FRANKLIN@EXIT_BUILDING"
			tPlayerAnimLoop		= "loop"
			tPlayerAnimOut		= "switch_01"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_c_02
		CASE PR_SCENE_F_BAR_d_02
			tPlayerAnimDict		= "SWITCH@FRANKLIN@EXIT_BUILDING"
			tPlayerAnimLoop		= "loop"
			tPlayerAnimOut		= "switch_02"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_LAMTAUNT_P1
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P1"
			tPlayerAnimLoop		= "gang_taunt_loop_franklin"
			tPlayerAnimOut		= "gang_taunt_exit_franklin"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P3"
			tPlayerAnimLoop		= "gang_taunt_with_lamar_loop_frank"
			tPlayerAnimOut		= "gang_taunt_with_lamar_exit_frank"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P5"
			tPlayerAnimLoop		= "fras_ig_6_p5_loop_frank"
			tPlayerAnimOut		= "fras_ig_6_p5_exit_frank"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			tPlayerAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P3"
			tPlayerAnimLoop		= "fras_ig_10_p3_loop_frank"
			tPlayerAnimOut		= "fras_ig_10_p3_exit_frank"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_a
		CASE PR_SCENE_F_S_AGENCY_2A_b
			tPlayerAnimDict		= "missheist_agency2aig_9"
			tPlayerAnimLoop		= "Franklin_call_Michael_IDLE_PLAYER"
			tPlayerAnimOut		= "Franklin_call_Michael_EXIT_PLAYER"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_S_FBI1end
//			tPlayerAnimDict		= "MISSFBI1LEADINOUTFBI_1_EXT"
//			tPlayerAnimLoop		= "_LeadIn_Franklin"
//			tPlayerAnimOut		= "_LeadIn_Franklin"
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE
			tPlayerAnimDict		= "SWITCH@TREVOR@MOCKS_LAPDANCE"
			tPlayerAnimLoop		= "001443_01_TRVS_28_IDLE_TRV"
			tPlayerAnimOut		= "001443_01_TRVS_28_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_BAR
			tPlayerAnimDict		= "SWITCH@TREVOR@BAR"
			tPlayerAnimLoop		= "LOOP_Trevor"
			tPlayerAnimOut		= "EXIT_Trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_CHASE
			tPlayerAnimDict		= "SWITCH@TREVOR@CHASE_STRIPPERS"
			tPlayerAnimLoop		= "LOOP_Trevor"
			tPlayerAnimOut		= "EXIT_Trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_STRIPCLUB_out
			tPlayerAnimDict		= "SWITCH@TREVOR@STRIPCLUB"
			tPlayerAnimLoop		= "trev_leave_stripclub_idle"
			tPlayerAnimOut		= "trev_leave_stripclub_outro"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT
			tPlayerAnimDict		= "SWITCH@TREVOR@ESCORTED_OUT"
			tPlayerAnimLoop		= "001215_02_TRVS_12_ESCORTED_OUT_IDLE_TRV"
			tPlayerAnimOut		= "001215_02_TRVS_12_ESCORTED_OUT_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT
		CASE PR_SCENE_T_CN_PARK_b
//		CASE PR_SCENE_T_CN_PARK_c
			tPlayerAnimDict		= "SWITCH@TREVOR@PUKING_INTO_FOUNTAIN"
			tPlayerAnimLoop		= "trev_fountain_puke_loop"
			tPlayerAnimOut		= "trev_fountain_puke_exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SMOKEMETH
		CASE PR_SCENE_Ta_RC_MRSP2
			tPlayerAnimDict		= "SWITCH@TREVOR@TREV_SMOKING_METH"
			tPlayerAnimLoop		= "TREV_SMOKING_METH_LOOP"
			tPlayerAnimOut		= "TREV_SMOKING_METH_EXIT"
			
			playerAnimLoopFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE | AF_LOOPING
			playerAnimOutFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_HEADINSINK
			tPlayerAnimDict		= "SWITCH@TREVOR@HEAD_IN_SINK"
			tPlayerAnimLoop		= "trev_sink_idle"
			tPlayerAnimOut		= "trev_sink_exit"
			
			playerAnimLoopFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE | AF_LOOPING
			playerAnimOutFlag	= /* |*/ AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION /*| AF_EXTRACT_INITIAL_OFFSET*/ | AF_NOT_INTERRUPTABLE
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DOCKS_a
		CASE PR_SCENE_T_DOCKS_b
		CASE PR_SCENE_T_DOCKS_c
		CASE PR_SCENE_T_DOCKS_d
			tPlayerAnimDict		= "SWITCH@TREVOR@AT_THE_DOCKS"
			tPlayerAnimLoop		= "001209_01_TRVS_3_AT_THE_DOCKS_IDLE"
			tPlayerAnimOut		= "001209_01_TRVS_3_AT_THE_DOCKS_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_KONEIGHBOUR
			tPlayerAnimDict		= "SWITCH@TREVOR@KO_NEIGHBOUR"
			tPlayerAnimLoop		= "001500_03_TRVS_19_KO_NEIGHBOUR_LOOP_TRV"
			tPlayerAnimOut		= "001500_03_TRVS_19_KO_NEIGHBOUR_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD
			tPlayerAnimDict		= "SWITCH@TREVOR@GARBAGE_FOOD"
			tPlayerAnimLoop		= "LOOP_Trevor"
			tPlayerAnimOut		= "EXIT_Trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			tPlayerAnimDict		= "SWITCH@TREVOR@THROW_FOOD"
			tPlayerAnimLoop		= "LOOP_Trevor"
			tPlayerAnimOut		= "EXIT_Trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK
		CASE PR_SCENE_T_SC_ALLEYDRUNK
		CASE PR_SCENE_T_CN_WAKETRASH_b
		CASE PR_SCENE_T_CR_WAKEBEACH
		CASE PR_SCENE_T_CN_WAKEBARN
		CASE PR_SCENE_T_CN_WAKETRAIN
		CASE PR_SCENE_T_CR_WAKEROOFTOP
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
		CASE PR_SCENE_T_NAKED_GARDEN
		CASE PR_SCENE_T_CN_CHATEAU_b
		CASE PR_SCENE_T_CN_CHATEAU_c
		CASE PR_SCENE_T_CR_CHATEAU_d
			tPlayerAnimDict		= "SWITCH@TREVOR@SLOUCHED_GET_UP"
			tPlayerAnimLoop		= "TREV_SLOUCHED_GET_UP_IDLE"
			tPlayerAnimOut		= "TREV_SLOUCHED_GET_UP_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_NAKED_ISLAND
			tPlayerAnimDict		= "SWITCH@TREVOR@NAKED_ISLAND"
			tPlayerAnimLoop		= "loop"
			tPlayerAnimOut		= "exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FIGHTBBUILD
		
		//	SWITCH@TREVOR@PUSHES_BODYBUILDER/001426_03_TRVS_5_PUSHES_BODYBUILDER_IDLE_TRV.anim
		//	SWITCH@TREVOR@PUSHES_BODYBUILDER/001426_03_TRVS_5_PUSHES_BODYBUILDER_IDLE_BB1.anim
		//	SWITCH@TREVOR@PUSHES_BODYBUILDER/001426_03_TRVS_5_PUSHES_BODYBUILDER_IDLE_BB2.anim
			
		//	SWITCH@TREVOR@PUSHES_BODYBUILDER/001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_TRV.anim
		//	SWITCH@TREVOR@PUSHES_BODYBUILDER/001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_BB1.anim
		//	SWITCH@TREVOR@PUSHES_BODYBUILDER/001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_BB2.anim
			
			tPlayerAnimDict		= "SWITCH@TREVOR@PUSHES_BODYBUILDER"
			tPlayerAnimLoop		= "001426_03_TRVS_5_PUSHES_BODYBUILDER_IDLE_TRV"
			tPlayerAnimOut		= "001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE
			tPlayerAnimDict		= "SWITCH@TREVOR@RUDE_AT_CAFE"
			tPlayerAnimLoop		= "001218_03_TRVS_23_RUDE_AT_CAFE_IDLE_TRV"
			tPlayerAnimOut		= "001218_03_TRVS_23_RUDE_AT_CAFE_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS
			tPlayerAnimDict		= "SWITCH@TREVOR@ANNOYS_SUNBATHERS"
			tPlayerAnimLoop		= "trev_annoys_sunbathers_loop_trevor"
			tPlayerAnimOut		= "trev_annoys_sunbathers_exit_trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SCARETRAMP
			tPlayerAnimDict		= "SWITCH@TREVOR@SCARES_TRAMP"
			tPlayerAnimLoop		= "TREV_SCARES_TRAMP_IDLE_TREVOR"
			tPlayerAnimOut		= "TREV_SCARES_TRAMP_EXIT_TREVOR"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE
			tPlayerAnimDict		= "SWITCH@TREVOR@NAKED_ON_BRIDGE"
			tPlayerAnimLoop		= "002055_01_TRVS_17_NAKED_ON_BRIDGE_IDLE"
			tPlayerAnimOut		= "002055_01_TRVS_17_NAKED_ON_BRIDGE_EXIT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_DUMPSTER
			tPlayerAnimDict		= "SWITCH@TREVOR@DUMPSTER"
			tPlayerAnimLoop		= "002002_01_TRVS_14_DUMPSTER_IDLE"
			tPlayerAnimOut		= "002002_01_TRVS_14_DUMPSTER_EXIT"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_CR_FUNERAL
			tPlayerAnimDict		= "SWITCH@TREVOR@FUNERAL_HOME"
			tPlayerAnimLoop		= "trvs_ig_11_loop"
			tPlayerAnimOut		= "trvs_ig_11_exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP
			tPlayerAnimDict		= "SWITCH@TREVOR@BRIDGE"
			tPlayerAnimLoop		= "HOLD_LOOP_trevor"
			tPlayerAnimOut		= "THROW_EXIT_trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_YELLATDOORMAN
			tPlayerAnimDict		= "SWITCH@TREVOR@YELLS_AT_DOORMAN"
			tPlayerAnimLoop		= "001430_01_TRVS_21_YELLS_AT_DOORMAN_IDLE_TRV"
			tPlayerAnimOut		= "001430_01_TRVS_21_YELLS_AT_DOORMAN_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			tPlayerAnimDict		= "SWITCH@TREVOR@BLOCK_CAMERA"
			tPlayerAnimLoop		= "001220_03_GC_TRVS_IG_7_BASE_TREVOR"
			tPlayerAnimOut		= "001220_03_GC_TRVS_IG_7_EXIT_TREVOR"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			tPlayerAnimDict		= "SWITCH@TREVOR@GUITAR_BEATDOWN"
			tPlayerAnimLoop		= "001370_02_TRVS_8_GUITAR_BEATDOWN_IDLE_TRV"
			tPlayerAnimOut		= "001370_02_TRVS_8_GUITAR_BEATDOWN_EXIT_TRV"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_LINGERIE
			tPlayerAnimDict		= "SWITCH@TREVOR@LINGERIE_SHOP"
			tPlayerAnimLoop		= "trev_exit_lingerie_shop_idle"
			tPlayerAnimOut		= "trev_exit_lingerie_shop_outro"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_CR_MACHINE
//			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE
			tPlayerAnimDict		= "SWITCH@TREVOR@RAND_TEMPLE"
			tPlayerAnimLoop		= ""
			tPlayerAnimOut		= "TAI_CHI_Trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_UNDERPIER
			tPlayerAnimDict		= "SWITCH@TREVOR@UNDER_PIER"
			tPlayerAnimLoop		= "LOOP_Trevor"
			tPlayerAnimOut		= "EXIT_Trevor"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DRUNKHOWLING
			tPlayerAnimDict		= "SWITCH@TREVOR@DRUNK_HOWLING"
			tPlayerAnimLoop		= "loop"
			tPlayerAnimOut		= "exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING
			tPlayerAnimDict		= "SWITCH@TREVOR@DRUNK_HOWLING_SC"
			tPlayerAnimLoop		= "loop"
			tPlayerAnimOut		= "exit"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
			tPlayerAnimDict		= "SWITCH@TREVOR@BED"
			tPlayerAnimLoop		= "Bed_Sleep_TREVOR"
			tPlayerAnimOut		= "Bed_GetUp_1_TREVOR"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			tPlayerAnimDict		= "SWITCH@TREVOR@BED"
			tPlayerAnimLoop		= "Bed_Sleep_TREVOR"
			tPlayerAnimOut		= "Bed_GetUp_2_TREVOR"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A
			tPlayerAnimDict		= "SWITCH@TREVOR@FLOYD_CRYING"
			tPlayerAnimLoop		= "Console_LOOP_TREVOR"
			tPlayerAnimOut		= "Console_Wasnt_Fun_TREVOR"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			tPlayerAnimDict		= "SWITCH@TREVOR@FLOYD_CRYING"
			tPlayerAnimLoop		= "Console_LOOP_TREVOR"
			tPlayerAnimOut		= "Console_Get_Along_TREVOR"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			tPlayerAnimDict		= "SWITCH@TREVOR@BEAR_IN_FLOYDS_FACE"
			tPlayerAnimLoop		= "bear_in_floyds_face_loop_trev"
			tPlayerAnimOut		= "bear_in_floyds_face_exit_trev"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL
			tPlayerAnimDict		= "SWITCH@TREVOR@BEAR_FLOYDS_FACE_SMELL"
			tPlayerAnimLoop		= "bear_floyds_face_smell_loop_trev"
			tPlayerAnimOut		= "bear_floyds_face_smell_exit_trev"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			tPlayerAnimDict		= "SWITCH@TREVOR@PINEAPPLE"
			tPlayerAnimLoop		= "Pineapple_LOOP_TREVOR"
			tPlayerAnimOut		= "Pineapple_EXIT_TREVOR"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_SMOKECRYSTAL
			tPlayerAnimDict		= "SWITCH@MICHAEL@SMOKING2"
			tPlayerAnimLoop		= "LOOP"
			tPlayerAnimOut		= "EXIT"
			
			playerAnimLoopFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			playerAnimOutFlag	|= AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP
//			tPlayerAnimDict		= "TIMETABLE@TREVOR@GRENADE_THROWING"
//			tPlayerAnimLoop		= ""
//			tPlayerAnimOut		= "GRENADE_THROWING_trev"
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T6_EVENING
//			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T6_METHLAB
			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T6_HUNTING1
//		CASE PR_SCENE_T6_HUNTING2
//		CASE PR_SCENE_T6_HUNTING3
//			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T6_TRAF_AIR
//			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T6_DISPOSEBODY_A
//			InitialisePlaceholderPlayerSmokeAnim(ePedScene, tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut)	//, ePlayerSceneAnimProgress)
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T6_DIGGING
			tPlayerAnimDict		= "SWITCH@TREVOR@DIGGING"
			tPlayerAnimLoop		= "001433_01_TRVS_26_DIGGING_IDLE"
			tPlayerAnimOut		= "001433_01_TRVS_26_DIGGING_EXIT"
			
			playerAnimOutFlag	|= AF_TAG_SYNC_OUT
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			tPlayerAnimDict		= "SWITCH@TREVOR@FLUSHES_FOOT"
			tPlayerAnimLoop		= "002057_03_TRVS_27_FLUSHES_FOOT_IDLE"
			tPlayerAnimOut		= "002057_03_TRVS_27_FLUSHES_FOOT_EXIT"
			
			playerAnimOutFlag	|= AF_TAG_SYNC_OUT
			
			RETURN TRUE
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			
//			PRINTSTRING("<")
//			PRINTSTRING(GET_THIS_SCRIPT_NAME())
//			PRINTSTRING("> no anim for eScene in ")
//			PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(ePedScene))
//			PRINTNL()
			
			PED_SCENE_STRUCT sPedScene
			PLAYER_TIMETABLE_SCENE_STRUCT sDefaultScene
			
			sPedScene.eScene = ePedScene
			SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sDefaultScene)
			
			IF sDefaultScene.eLoopTask = SCRIPT_TASK_play_anim
			OR sDefaultScene.eLoopTask = SCRIPT_TASK_play_anim
				TEXT_LABEL_63 sDefault
				sDefault = "no anim for eScene in GET_PLAYER_ANIM_FOR_PLAYER_TIMETABLE_SCENE: "
				sDefault += Get_String_From_Ped_Request_Scene_Enum(ePedScene)
				
				SCRIPT_ASSERT(sDefault)
				
				tPlayerAnimDict	= Get_String_From_Ped_Request_Scene_Enum(ePedScene)
				tPlayerAnimLoop	= Get_String_From_Ped_Request_Scene_Enum(ePedScene)
				tPlayerAnimOut	= Get_String_From_Ped_Request_Scene_Enum(ePedScene)
				
				RETURN TRUE
			ENDIF
			
			#ENDIF
			
			tPlayerAnimDict	= "NULL"
			tPlayerAnimLoop	= "NULL"
			tPlayerAnimOut	= "NULL"
			//ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for GET_PLAYER_ANIM_FOR_PLAYER_TIMETABLE_SCENE: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(ePedScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	tPlayerAnimDict	= "NULL"
	tPlayerAnimLoop	= "NULL"
	tPlayerAnimOut	= "NULL"
	//ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PLAYER_WARDROBE_ANIM_FOR_TIMETABLE_SCENE(PED_INDEX pedID, PED_REQUEST_SCENE_ENUM ePedScene)
	SWITCH ePedScene
		CASE PR_SCENE_M2_BEDROOM
//		CASE PR_SCENE_M2_SAVEHOUSE0_b		//1556272
		CASE PR_SCENE_M4_WAKESUPSCARED
		CASE PR_SCENE_M4_WAKEUPSCREAM
//		CASE PR_SCENE_M7_GETSREADY
			
			IF DOES_ENTITY_EXIST(pedID)
			ENDIF
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


PROC InitialiseBuddyWithAnim(TEXT_LABEL_63 &tBuddyAnimDict, TEXT_LABEL_63 &tBuddyAnimLoop, TEXT_LABEL_63 &tBuddyAnimOut,
		STRING sBuddyAnimDict, STRING sBuddyAnimLoop, STRING sBuddyAnimOut)
	tBuddyAnimDict		= sBuddyAnimDict
	tBuddyAnimLoop		= sBuddyAnimLoop
	tBuddyAnimOut		= sBuddyAnimOut
	
ENDPROC

/// PURPOSE: Returns the flag needed for the synchronized scene

PROC F_GET_BUDDY_SYNCHRONIZED_SCENE_FLAG(PED_REQUEST_SCENE_ENUM ePedScene, SYNCED_SCENE_PLAYBACK_FLAGS &scpf_Flags, IK_CONTROL_FLAGS &ikFlags)
	
	scpf_Flags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
	ikFlags = AIK_NONE
	
	SWITCH ePedScene 
		CASE PR_SCENE_M2_WIFEEXITSCAR
		CASE PR_SCENE_M7_OPENDOORFORAMA
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			ikFlags = AIK_DISABLE_HEAD_IK
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			ikFlags = AIK_DISABLE_HEAD_IK
		BREAK
//		CASE PR_SCENE_F_KUSH_DOC_a
//			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
//		BREAK
//		CASE PR_SCENE_F_MD_KUSH_DOC
//			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1
		CASE PR_SCENE_F_LAMTAUNT_P3
		CASE PR_SCENE_F_LAMTAUNT_P5
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
		CASE PR_SCENE_F_CLUB
			scpf_Flags |= SYNCED_SCENE_TAG_SYNC_OUT
		BREAK
	ENDSWITCH
	
ENDPROC


FUNC BOOL IS_BUDDY_ASLEEP_FOR_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene)
	SWITCH ePedScene
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_BUDDY_QUITING_EXIT_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene, FLOAT &fExitPhase)
	SWITCH ePedScene
		CASE PR_SCENE_M7_FAKEYOGA
			fExitPhase = 0.75
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	fExitPhase = -1
	RETURN FALSE
ENDFUNC
/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene,
		TEXT_LABEL_63 &tBuddyAnimDict, TEXT_LABEL_63 &tBuddyAnimLoop, TEXT_LABEL_63 &tBuddyAnimOut,
		ANIMATION_FLAGS &buddyAnimLoopFlag, ANIMATION_FLAGS &buddyAnimOutFlag)
	
	buddyAnimLoopFlag	= AF_LOOPING | AF_NOT_INTERRUPTABLE
	buddyAnimOutFlag	= AF_NOT_INTERRUPTABLE
	
	SWITCH ePedScene
		CASE PR_SCENE_M2_BEDROOM
			tBuddyAnimDict		= "SWITCH@MICHAEL@BEDROOM"
			tBuddyAnimLoop		= "BED_LOOP_Amanda"
			tBuddyAnimOut		= "BED_REACT_Amanda"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b
			tBuddyAnimDict		= "SWITCH@MICHAEL@BEDROOM2"
			tBuddyAnimLoop		= "BED_LOOP_Amanda"
			tBuddyAnimOut		= "BED_EXIT_Amanda"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a
			InitialiseBuddyWithAnim(tBuddyAnimDict, tBuddyAnimLoop, tBuddyAnimOut,
					"SWITCH@MICHAEL@SITTING", "IDLE", "EXIT_FORWARD")
			
			buddyAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			buddyAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_KIDS_TV
			tBuddyAnimDict		= "SWITCH@MICHAEL@ON_SOFA"
			tBuddyAnimLoop		= "BASE_Jimmy"
			tBuddyAnimOut		= "EXIT_Jimmy"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_LUNCH_a
			tBuddyAnimDict		= "SWITCH@MICHAEL@CAFE"
			tBuddyAnimLoop		= "LOOP_Amanda"
			tBuddyAnimOut		= "EXIT_Amanda"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_LUNCH_b
//			tBuddyAnimDict		= "SWITCH@MICHAEL@CAFE2"
//			tBuddyAnimLoop		= "LOOP_Amanda"
//			tBuddyAnimOut		= "EXIT_Amanda"
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE
			tBuddyAnimDict		= "SWITCH@MICHAEL@ARGUE_WITH_AMANDA"
			tBuddyAnimLoop		= "argue_with_amanda_loop_amanda"
			tBuddyAnimOut		= "argue_with_amanda_exit_amanda"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_WIFEEXITSCAR
			tBuddyAnimDict		= "SWITCH@MICHAEL@AMANDA_EXITS_CAR"
			tBuddyAnimLoop		= "000606_02_MICS1_5_AMANDA_EXITS_CAR_IDLE_AMA"
			tBuddyAnimOut		= "000606_02_MICS1_5_AMANDA_EXITS_CAR_EXIT_AMA"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_RONBORING
			tBuddyAnimDict		= "SWITCH@MICHAEL@RONEX_IG_5_P2"
			tBuddyAnimLoop		= "BASE_RON"
			tBuddyAnimOut		= "RONEX_IG5_P2_RON"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M7_OPENDOORFORAMA
			tBuddyAnimDict		= "SWITCH@MICHAEL@OPENS_DOOR_FOR_AMA"
			tBuddyAnimLoop		= "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_IDLE_AMA"
			tBuddyAnimOut		= "001895_02_MICS3_17_OPENS_DOOR_FOR_AMA_EXIT_AMA"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			tBuddyAnimDict		= "SWITCH@MICHAEL@DROPPING_OFF_JMY"
			tBuddyAnimLoop		= "001839_02_MICS3_20_DROPPING_OFF_JMY_IDLE_JMY"
			tBuddyAnimOut		= "001839_02_MICS3_20_DROPPING_OFF_JMY_EXIT_JMY"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
//		CASE PR_SCENE_M_HOOKERCAR
			tBuddyAnimDict		= "SWITCH@MICHAEL@TRACY_EXITS_CAR"
			tBuddyAnimLoop		= "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_IDLE_TRA"
			tBuddyAnimOut		= "001840_01_MICS3_IG_21_TRACY_EXITS_CAR_TRA"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M7_GETSREADY
			tBuddyAnimDict		= "SWITCH@MICHAEL@GETS_READY"
			tBuddyAnimLoop		= "001520_02_MICS3_14_GETS_READY_IDLE_AMA"
			tBuddyAnimOut		= "001520_02_MICS3_14_GETS_READY_EXIT_AMA"
			
			buddyAnimOutFlag	|= AF_USE_KINEMATIC_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			tBuddyAnimDict		= "SWITCH@MICHAEL@RESTAURANT"
			tBuddyAnimLoop		= "001510_02_GC_MICS3_IG_1_BASE_AMANDA"
			tBuddyAnimOut		= "001510_02_GC_MICS3_IG_1_EXIT_AMANDA"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			tBuddyAnimDict		= "SWITCH@MICHAEL@AROUND_THE_TABLE_SELFISH"
			tBuddyAnimLoop		= "AROUND_THE_TABLE_SELFISH_BASE_Jimmy"
			tBuddyAnimOut		= "AROUND_THE_TABLE_SELFISH_Jimmy"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			tBuddyAnimDict		= "SWITCH@MICHAEL@LOUNGE_CHAIRS"
			tBuddyAnimLoop		= "001523_01_MICS3_9_LOUNGE_CHAIRS_IDLE_AMA"
			tBuddyAnimOut		= "001523_01_MICS3_9_LOUNGE_CHAIRS_EXIT_AMA"
			
			buddyAnimOutFlag	|= AF_USE_KINEMATIC_PHYSICS
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M7_HOOKERS
			tBuddyAnimDict		= "SWITCH@MICHAEL@PROSTITUTE"
			tBuddyAnimLoop		= "BASE_HOOKER"
			tBuddyAnimOut		= "EXIT_HOOKER"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOT_JIMMY
			tBuddyAnimDict		= "SWITCH@MICHAEL@ON_SET_W_JMY"
			tBuddyAnimLoop		= ""
			tBuddyAnimOut		= "001513_03_GC_MICS3_IG_4_ON_SET_W_JMY_EXIT_JMY"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_KIDS_TV
//			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_M2_KIDS_TV,
//					tBuddyAnimDict, tBuddyAnimLoop, tBuddyAnimOut,
//					buddyAnimLoopFlag, buddyAnimOutFlag)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			tBuddyAnimDict		= "SWITCH@MICHAEL@GAMING_W_JMY"
			tBuddyAnimLoop		= "001518_02_MICS3_11_GAMING_W_JMY_IDLE_JMY"
			tBuddyAnimOut		= "001518_02_MICS3_11_GAMING_W_JMY_EXIT_JMY"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			tBuddyAnimDict		= "SWITCH@MICHAEL@AMA_TENNIS"
			tBuddyAnimLoop		= "001833_01_MICS3_18_AMA_TENNIS_IDLE_AMA"
			tBuddyAnimOut		= "001833_01_MICS3_18_AMA_TENNIS_EXIT_AMA"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F0_WALKCHOP
//		CASE PR_SCENE_F1_WALKCHOP
		CASE PR_SCENE_F_WALKCHOP_a
		CASE PR_SCENE_F_WALKCHOP_b
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F1_PLAYCHOP,
					tBuddyAnimDict, tBuddyAnimLoop, tBuddyAnimOut,
					buddyAnimLoopFlag, buddyAnimOutFlag)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_F0_PLAYCHOP
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(PR_SCENE_F1_PLAYCHOP,
					tBuddyAnimDict, tBuddyAnimLoop, tBuddyAnimOut,
					buddyAnimLoopFlag, buddyAnimOutFlag)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F1_PLAYCHOP
			tBuddyAnimDict		= "SWITCH@FRANKLIN@PLAYS_W_DOG"
			tBuddyAnimLoop		= "001916_01_FRAS_V2_9_PLAYS_W_DOG_IDLE_ROT"
			tBuddyAnimOut		= "001916_01_FRAS_V2_9_PLAYS_W_DOG_EXIT_ROT"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT
			tBuddyAnimDict		= "SWITCH@FRANKLIN@TANISHA_ARGUE"
			tBuddyAnimLoop		= "BASE_Tanisha"
			tBuddyAnimOut		= "Tanisha_Argue_Tanisha"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F1_BYETAXI
			tBuddyAnimDict		= "SWITCH@FRANKLIN@BYE_TAXI"
			tBuddyAnimLoop		= "001938_01_FRAS_V2_7_BYE_TAXI_IDLE_GIRL"
			tBuddyAnimOut		= "001938_01_FRAS_V2_7_BYE_TAXI_EXIT_GIRL"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CLUB
			tBuddyAnimDict		= "SWITCH@FRANKLIN@PICKUP_LINE"
			tBuddyAnimLoop		= "base_Hooker"
			tBuddyAnimOut		= "switch_ped_Hooker"
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_F_LAMGRAFF
//			tBuddyAnimDict		= "SWITCH@FRANKLIN@LAMAR_TAGGING_WALL"
//			tBuddyAnimLoop		= "lamar_tagging_wall_LOOP_LAMAR"
//			tBuddyAnimOut		= "lamar_tagging_wall_EXIT_LAMAR"
//			
//			RETURN TRUE
//		BREAK
		
		CASE PR_SCENE_F_LAMTAUNT_P1
			tBuddyAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P1"
			tBuddyAnimLoop		= "gang_taunt_loop_lamar"
			tBuddyAnimOut		= "gang_taunt_exit_lamar"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2
//			tBuddyAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P2"
//			tBuddyAnimLoop		= "fras_ig_6_p2_loop_lamar"
//			tBuddyAnimOut		= "fras_ig_6_p2_exit_lamar"
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3
			tBuddyAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P3"
			tBuddyAnimLoop		= "gang_taunt_with_lamar_loop_lamar"
			tBuddyAnimOut		= "gang_taunt_with_lamar_exit_lamar"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5
			tBuddyAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P5"
			tBuddyAnimLoop		= "fras_ig_6_p5_loop_lamar"
			tBuddyAnimOut		= "fras_ig_6_p5_exit_lamar"
				
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			tBuddyAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P3"
			tBuddyAnimLoop		= "fras_ig_10_p3_loop_lamar"
			tBuddyAnimOut		= "fras_ig_10_p3_exit_lamar"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_KUSH_DOC_a
			tBuddyAnimDict		= "SWITCH@FRANKLIN@DISPENSARY"
			tBuddyAnimLoop		= ""
			tBuddyAnimOut		= "exit_dispensary_outro_ped_f_a"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC
			tBuddyAnimDict		= "SWITCH@FRANKLIN@002110_04_MAGD_3_WEED_EXCHANGE"
			tBuddyAnimLoop		= "002110_04_MAGD_3_WEED_EXCHANGE_SHOPKEEPER"
			tBuddyAnimOut		= "002110_04_MAGD_3_WEED_EXCHANGE_SHOPKEEPER"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_SC_BAR
			tBuddyAnimDict		= "SWITCH@TREVOR@BAR"
			tBuddyAnimLoop		= "LOOP_Bartender"
			tBuddyAnimOut		= "EXIT_Bartender"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_CHASE
			tBuddyAnimDict		= "SWITCH@TREVOR@CHASE_STRIPPERS"
			tBuddyAnimLoop		= "LOOP_BOUNCER"
			tBuddyAnimOut		= "EXIT_BOUNCER"
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_STRIPCLUB_a
//			InitialiseBuddyWithAnim(tBuddyAnimDict, tBuddyAnimLoop, tBuddyAnimOut, "s""i""t", "plyr_sit_LOOP","plyr_sit_LOOP")
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_DRUNKHOWLING
//		CASE PR_SCENE_T_SC_DRUNKHOWLING
			InitialiseBuddyWithAnim(tBuddyAnimDict, tBuddyAnimLoop, tBuddyAnimOut, "SWITCH@MICHAEL@PIER", "pier_lean_smoke_idle", "pier_lean_smoke_idle")
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_KONEIGHBOUR
			tBuddyAnimDict		= "SWITCH@TREVOR@KO_NEIGHBOUR"
			tBuddyAnimLoop		= "001500_03_TRVS_19_KO_NEIGHBOUR_LOOP_NBR"
			tBuddyAnimOut		= "001500_03_TRVS_19_KO_NEIGHBOUR_EXIT_NBR"

			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_THROW_FOOD
//			tBuddyAnimDict		= "SWITCH@TREVOR@THROW_FOOD"
//			tBuddyAnimLoop		= "LOOP_Ped"
//			tBuddyAnimOut		= "EXIT_Ped"
//			
//			RETURN TRUE
//		BREAK
		
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			tBuddyAnimDict		= "SWITCH@TREVOR@BLOCK_CAMERA"
			tBuddyAnimLoop		= "001220_03_GC_TRVS_IG_7_BASE_GENERIC"
			tBuddyAnimOut		= "001220_03_GC_TRVS_IG_7_EXIT_GENERIC"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			tBuddyAnimDict		= "SWITCH@TREVOR@GUITAR_BEATDOWN"
			tBuddyAnimLoop		= "001370_02_TRVS_8_GUITAR_BEATDOWN_IDLE_BUSKER"
			tBuddyAnimOut		= "001370_02_TRVS_8_GUITAR_BEATDOWN_EXIT_BUSKER"
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
			tBuddyAnimDict		= "SWITCH@TREVOR@BED"
			tBuddyAnimLoop		= "Bed_Sleep_FLOYD"
			tBuddyAnimOut		= "Bed_GetUp_1_FLOYD"
			
			buddyAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			buddyAnimOutFlag	|= AF_OVERRIDE_PHYSICS

			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			tBuddyAnimDict		= "SWITCH@TREVOR@BED"
			tBuddyAnimLoop		= "Bed_Sleep_FLOYD"
			tBuddyAnimOut		= "Bed_GetUp_2_FLOYD"
			
			buddyAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			buddyAnimOutFlag	|= AF_OVERRIDE_PHYSICS

			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A
			tBuddyAnimDict		= "SWITCH@TREVOR@FLOYD_CRYING"
			tBuddyAnimLoop		= "Console_LOOP_FLOYD"
			tBuddyAnimOut		= "Console_Wasnt_Fun_FLOYD"
			
			buddyAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			buddyAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			tBuddyAnimDict		= "SWITCH@TREVOR@FLOYD_CRYING"
			tBuddyAnimLoop		= "Console_LOOP_FLOYD"
			tBuddyAnimOut		= "Console_Get_Along_FLOYD"
			
			buddyAnimLoopFlag	|= AF_OVERRIDE_PHYSICS
			buddyAnimOutFlag	|= AF_OVERRIDE_PHYSICS
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			tBuddyAnimDict		= "SWITCH@TREVOR@BEAR_IN_FLOYDS_FACE"
			tBuddyAnimLoop		= "bear_in_floyds_face_loop_floyd"
			tBuddyAnimOut		= "bear_in_floyds_face_exit_floyd"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL
			tBuddyAnimDict		= "SWITCH@TREVOR@BEAR_FLOYDS_FACE_SMELL"
			tBuddyAnimLoop		= "bear_floyds_face_smell_loop_floyd"
			tBuddyAnimOut		= "bear_floyds_face_smell_exit_floyd"
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			tBuddyAnimDict		= "SWITCH@TREVOR@PINEAPPLE"
			tBuddyAnimLoop		= "Pineapple_LOOP_FLOYD"
			tBuddyAnimOut		= "Pineapple_EXIT_FLOYD"
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for buddy timetable setup: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(ePedScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	tBuddyAnimDict	= "NULL"
	tBuddyAnimLoop	= "NULL"
	tBuddyAnimOut	= "NULL"
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SPEECH_FACIAL_ANIM_FOR_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene,
		TEXT_LABEL_63 &tFacialClip)
	
	TEXT_LABEL rootLabel = g_ConversationData.ConversationSegmentToGrab
	INT iRootFilenameNum = -1
	
	SWITCH ePedScene
		CASE PR_SCENE_M7_LOUNGECHAIRS		//1314197
			
//			rootLabel += "A"
//			iRootFilenameNum = GET_VARIATION_CHOSEN_FOR_SCRIPTED_LINE(rootLabel)
			
			INT rootLabelLength
			TEXT_LABEL rootLabelChar
			rootLabelLength = GET_LENGTH_OF_LITERAL_STRING(rootLabel)
			rootLabelChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(rootLabel, rootLabelLength-1, rootLabelLength)
			rootLabel += "_1A"
			
			SWITCH GET_HASH_KEY(rootLabelChar)
				CASE HASH("a")
					iRootFilenameNum = 1
				BREAK
				CASE HASH("b")
					iRootFilenameNum = 2
				BREAK
				CASE HASH("c")
					iRootFilenameNum = 3
				BREAK
				CASE HASH("d")
					iRootFilenameNum = 4
				BREAK
				
				DEFAULT
					iRootFilenameNum = GET_VARIATION_CHOSEN_FOR_SCRIPTED_LINE(rootLabel)
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
			SAVE_STRING_TO_DEBUG_FILE("GET_VARIATION_CHOSEN_FOR_SCRIPTED_LINE(\"")
			SAVE_STRING_TO_DEBUG_FILE(rootLabel)
			SAVE_STRING_TO_DEBUG_FILE("\"): ")
			SAVE_INT_TO_DEBUG_FILE(iRootFilenameNum)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			
			SWITCH iRootFilenameNum
				CASE 1	/*	It's so good to spend some time together.
							It's great, but I've got to run. You relax and enjoy yourself. 	*/
					tFacialClip	= "EXIT_MIC_YOU_ENJOY_YOURSELF_FACIAL"
					RETURN TRUE
				BREAK
				CASE 2	/*	So... how do you feel about remodeling the house?
							Can we talk about it later? I've got a meeting. Sorry. 	*/
					tFacialClip	= "EXIT_MIC_GOTTA_MEETING_SORRY_FACIAL"
					RETURN TRUE
				BREAK
				CASE 3	/*	Hey, you want another drink, Michael?
							Oh, baby, I would love one, but I'm kind of late for a meeting. 	*/
					tFacialClip	= "EXIT_MIC_LATE_FOR_A_MEETING_FACIAL"
					RETURN TRUE
				BREAK
			ENDSWITCH
			
			SCRIPT_ASSERT("invalid iRootFilenameNum scene:PR_SCENE_M7_LOUNGECHAIRS")
			tFacialClip	= ""
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	tFacialClip	= ""
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_TASK_FACIAL_ANIM_FOR_TIMETABLE_SCENE(PED_REQUEST_SCENE_ENUM ePedScene,
		TEXT_LABEL_63 &tFacialClip)
	
	SWITCH ePedScene
		CASE PR_SCENE_F1_SNACKING		//1546382
			tFacialClip	= "001922_01_FRAS_V2_3_SNACKING_EXIT_FACE"
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	tFacialClip	= ""
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	SCENE PRIVATE EVENT FUNCTIONS
// *******************************************************************************************



FUNC BOOL HasAnimEventPassed(STRING pEventName, FLOAT fPlayerSceneAnimOutTime,
		STRING pAnimDictName, STRING pAnimName,
		INT &iDrawSceneRot)
	FLOAT ReturnStartPhase, ReturnEndPhase
	IF FIND_ANIM_EVENT_PHASE(pAnimDictName, pAnimName, pEventName, ReturnStartPhase, ReturnEndPhase)
		
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str  = ("\"")
		str += (pEventName)
		str += ("\" [")
		str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
		str += ("]")
		#ENDIF
		
		IF fPlayerSceneAnimOutTime < ReturnStartPhase
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_GREENLIGHT)
			#ENDIF
			
			iDrawSceneRot++
			
		ELSE
			
			#IF IS_DEBUG_BUILD
			str += " break"
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_GREENDARK)
			#ENDIF
			
			iDrawSceneRot++
			
			RETURN TRUE
			
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(pEventName, "WalkInterruptible")
		IF HasAnimEventPassed("END_IN_WALK", fPlayerSceneAnimOutTime,
				pAnimDictName, pAnimName, iDrawSceneRot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HasAnimPhasePassed_SCRIPT(PED_REQUEST_SCENE_ENUM eScene, STRING pEventName, FLOAT fPlayerSceneAnimOutTime,
		STRING pAnimDictName, STRING pAnimName,
		INT &iDrawSceneRot)
	
	FLOAT fEventPhase = -1
	
	UNUSED_PARAMETER(pAnimDictName)
	UNUSED_PARAMETER(pAnimName)
	UNUSED_PARAMETER(iDrawSceneRot)
	
	IF ARE_STRINGS_EQUAL(pEventName, "victim_fall")
		SWITCH eScene
			CASE PR_SCENE_T_CR_BRIDGEDROP	fEventPhase = 0.571					BREAK
			CASE PR_SCENE_T_KONEIGHBOUR		fEventPhase = 0.805	/*0.870*/		BREAK
			CASE PR_SCENE_T_GUITARBEATDOWN	fEventPhase = 0.813	/*0.905*/		BREAK
			
			DEFAULT
				fEventPhase = -1
				RETURN FALSE
			BREAK
		ENDSWITCH
		
	ELIF ARE_STRINGS_EQUAL(pEventName, "victim_die")
		SWITCH eScene
			CASE PR_SCENE_T_KONEIGHBOUR		fEventPhase = 0.904					BREAK
			CASE PR_SCENE_T_GUITARBEATDOWN	fEventPhase = 0.905					BREAK
			
			DEFAULT
				fEventPhase = -1
				RETURN FALSE
			BREAK
		ENDSWITCH
		
	ELIF ARE_STRINGS_EQUAL(pEventName, "close_veh_doors")
		SWITCH eScene
			CASE PR_SCENE_M2_WIFEEXITSCAR	fEventPhase = 0.70					BREAK
			CASE PR_SCENE_M7_OPENDOORFORAMA	fEventPhase = 0.95					BREAK
			
			DEFAULT
				fEventPhase = -1
				RETURN FALSE
			BREAK
		ENDSWITCH
		
	ELIF ARE_STRINGS_EQUAL(pEventName, "WalkInterruptible")
		SWITCH eScene
			CASE PR_SCENE_M4_WAKESUPSCARED	fEventPhase = 0.95					BREAK
			
			DEFAULT
				fEventPhase = -1
				RETURN FALSE
			BREAK
		ENDSWITCH
		
	ELSE
		fEventPhase = -1
		RETURN FALSE
	ENDIF
	
	
	
	IF (fEventPhase >= 0 AND fEventPhase <= 1)
		
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str  = (pEventName)
		str += (" phase [")
		str += GET_STRING_FROM_FLOAT(fEventPhase)
		str += ("]")
		#ENDIF
		
		IF fPlayerSceneAnimOutTime < fEventPhase
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			iDrawSceneRot++
			#ENDIF
		ELSE
			
			#IF IS_DEBUG_BUILD
			str  = (pEventName)
			str += (" break [")
			str += GET_STRING_FROM_FLOAT(fEventPhase)
			str += ("]")
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			iDrawSceneRot++
			
			
			FLOAT ReturnStartPhase, ReturnEndPhase
			IF NOT FIND_ANIM_EVENT_PHASE(pAnimDictName, pAnimName, pEventName,
					ReturnStartPhase, ReturnEndPhase)
				str  = ("not in clip...")
				DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot++
			ELSE
				IF (ReturnStartPhase <> fEventPhase)
					str  = ("doesnt match clip...")
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot++
				ELSE
					str  = ("matches clip...")
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_BLUELIGHT)
					iDrawSceneRot++
				ENDIF
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC





FUNC BOOL HasQuitAnimPhasePassed(PED_REQUEST_SCENE_ENUM eScene, FLOAT fPlayerSceneAnimOutTime,
		INT &iDrawSceneRot)
	
	FLOAT fEventPhase = -1
	SWITCH eScene
		CASE PR_SCENE_F0_GARBAGE		fEventPhase = 0.9					BREAK
		CASE PR_SCENE_T_FLOYD_BEAR		fEventPhase = 0.6					BREAK
		
		#IF NOT USE_TU_CHANGES
		DEFAULT
				
				
PRINTSTRING("is TU wardrobe scene ???? ")PRINTNL()
				
				
				fEventPhase = -1	RETURN FALSE	BREAK
		#ENDIF
		
		#IF USE_TU_CHANGES
		DEFAULT
			IF GET_PLAYER_WARDROBE_ANIM_FOR_TIMETABLE_SCENE(PLAYER_PED_ID(), eScene)
				
				
PRINTSTRING("is wardrobe scene - ")PRINTNL()
				
				
				fEventPhase = 0.95
			ELSE
				
				
PRINTSTRING("is NOT wardrobe scene - ")PRINTNL()
				
				
				fEventPhase = -1
				RETURN FALSE
			ENDIF
		BREAK
			
		#ENDIF
		
	ENDSWITCH 
	eScene = eScene
	
	IF (fEventPhase >= 0 AND fEventPhase <= 1)
		
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str  = ("quit event phase [")
		str += GET_STRING_FROM_FLOAT(fEventPhase)
		str += ("]")
		#ENDIF
		
		IF fPlayerSceneAnimOutTime < fEventPhase
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			#ENDIF
			
			iDrawSceneRot++
		ELSE
			
			#IF IS_DEBUG_BUILD
			str += " break"
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
			#ENDIF
			
			iDrawSceneRot++
			
			RETURN TRUE
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL GetPlayerAnimComponentForTimetableScene(PED_REQUEST_SCENE_ENUM ePedScene,
		FLOAT &fPlayerSceneAnimTime, PED_COMPONENT &ComponentID[], INT &DrawableID[], INT &TextureID[])
	
	#IF NOT IS_JAPANESE_BUILD
	SWITCH ePedScene
		CASE PR_SCENE_T_SHIT
			fPlayerSceneAnimTime	= 0.55	//0.5
			
			ComponentID[0]			= PED_COMP_LEG
			DrawableID[0]			= 23	//LEGS_P2_SWEAT_PANTS
			TextureID[0]			= 0
			
			ComponentID[1]			= PED_COMP_FEET
			DrawableID[1]			= 11
			TextureID[1]			= 0
			
			ComponentID[2]			= PED_COMP_SPECIAL
			DrawableID[2]			= 0
			TextureID[2]			= 0
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	#ENDIF
	#IF IS_JAPANESE_BUILD
	ePedScene = ePedScene
	#ENDIF
	
	fPlayerSceneAnimTime	= -1
	ComponentID[0]			= INT_TO_ENUM(PED_COMPONENT, -1)
	DrawableID[0]			= -1
	TextureID[0]			= -1
	RETURN FALSE
ENDFUNC

