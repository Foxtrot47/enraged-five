// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	battlebuddy_private.sch
//		AUTHOR			:	Sam Hackett
//		DESCRIPTION		:	Interface for taking control of battle buddies
//
// *****************************************************************************************
// *****************************************************************************************
			
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

USING "battlebuddy_public.sch"

//-------------------------------------------------------------------------------------------------------------------------------------------
//	MISSION INTERFACE
//-------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_SQUAD_MISSION(SP_MISSIONS eMission, INT iAllowedChars, BOOL bForceResetFlags = FALSE)
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tMission = GetLabel_SP_MISSIONS(eMission)
		TEXT_LABEL	  tChars = GetLabel_FriendBits(iAllowedChars)
		CPRINTLN(DEBUG_FRIENDS, "SET_SQUAD_MISSION(", tMission, ", ", tChars, ")")
	#ENDIF
	
	IF eMission = SP_MISSION_NONE
		CPRINTLN(DEBUG_FRIENDS, "SET_SQUAD_MISSION() - Cannot set, eMission is null")
		SCRIPT_ASSERT("SET_SQUAD_MISSION() - Cannot set, eMission is null")
	
	ELIF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_IS_PREP)
		CPRINTLN(DEBUG_FRIENDS, "SET_SQUAD_MISSION() - Cannot set, eMission is not prep mission")
		SCRIPT_ASSERT("SET_SQUAD_MISSION() - Cannot set, eMission is not prep mission")
	
	ELSE
		g_BattleBuddyMission		= eMission
		g_BattleBuddyAllowedChars	= iAllowedChars

		IF bForceResetFlags OR NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
			INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(eMission)
		ENDIF
	ENDIF

ENDPROC

PROC SET_SQUAD_MISSION_TO_CURRENT_ZONE()

	SET_SQUAD_MISSION(g_eFriendMissionZoneID, g_iFriendMissionZoneAcceptBitset)
	
ENDPROC


//-------------------------------------------------------------------------------------------------------------------------------------------
//	SNAPSHOT INTERFACE
//-------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL TAKE_BATTLEBUDDY_SNAPSHOT(structBattleBuddySnapshot& snapshot)
	
	snapshot.eMission		= g_BattleBuddyMission
	snapshot.iAllowedChars	= g_BattleBuddyAllowedChars
	snapshot.iReplayChars	= 0
	snapshot.iFailedChars	= 0

//	// Set replay chars
//	IF IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))			// BBUDDIES REMOVED
//		snapshot.iReplayChars	= g_bitfieldBattleBuddyAvailable
//	ENDIF
//		
//	// Set failed chars
//	enumCharacterList eLoopChar
//	REPEAT MAX_BATTLE_BUDDIES eLoopChar
//	
//		IF IS_TIMER_STARTED(g_SavedGlobals.sFriendsData.g_SoldierFailTimers[eLoopChar])
//		AND GET_TIMER_IN_SECONDS(g_SavedGlobals.sFriendsData.g_SoldierFailTimers[eLoopChar]) < 5.0*60.0
//			SET_BIT(snapshot.iFailedChars, ENUM_TO_INT(eLoopChar))
//		ENDIF
//	
//	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL tReplayChars = GetLabel_FriendBits(snapshot.iReplayChars)
		TEXT_LABEL tFailedChars = GetLabel_FriendBits(snapshot.iFailedChars)
		CPRINTLN(DEBUG_FRIENDS, "TAKE_BATTLEBUDDY_SNAPSHOT() = AvailableChars(", tReplayChars, ") FailedChars(", tFailedChars, ")")
		CPRINTLN(DEBUG_REPLAY, "TAKE_BATTLEBUDDY_SNAPSHOT() = AvailableChars(", tReplayChars, ") FailedChars(", tFailedChars, ")")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC


PROC OVERWRITE_BATTLEBUDDY_SNAPSHOT()
	TAKE_BATTLEBUDDY_SNAPSHOT(g_startSnapshot.mBattleBuddyStruct)
	TAKE_BATTLEBUDDY_SNAPSHOT(g_stageSnapshot.mBattleBuddyStruct)
ENDPROC


FUNC BOOL CAN_RESTORE_BATTLEBUDDY_SNAPSHOT()
//	IF NOT IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))		// BBUDDIES REMOVED
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
	RETURN TRUE																								// BBUDDIES REPLACED
ENDFUNC


PROC RESTORE_BATTLEBUDDY_SNAPSHOT(structBattleBuddySnapshot& snapshot)

	#IF IS_DEBUG_BUILD
		TEXT_LABEL tReplayChars = GetLabel_FriendBits(snapshot.iReplayChars)
		TEXT_LABEL tFailedChars = GetLabel_FriendBits(snapshot.iFailedChars)
		CPRINTLN(DEBUG_FRIENDS, "RESTORE_BATTLEBUDDY_SNAPSHOT() = AvailableChars(", tReplayChars, ") FailedChars(", tFailedChars, ")")
		CPRINTLN(DEBUG_REPLAY, "RESTORE_BATTLEBUDDY_SNAPSHOT() = AvailableChars(", tReplayChars, ") FailedChars(", tFailedChars, ")")
	#ENDIF
	
//	// Make sure friend script isn't running/has shutdown
//	IF IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))
//		SCRIPT_ASSERT("Cannot restore snapshot when friend script still runnning (check CAN_RESTORE_BATTLEBUDDY_SNAPSHOT() until true)")		// BBUDDIES REMOVED
//		EXIT
//	ENDIF
//	
//	// Deactivate all connections
//	enumFriendConnection eConnection
//	REPEAT MAX_FRIEND_CONNECTIONS eConnection
//		enumFriendConnectionState eState = g_FriendConnectState[eConnection].state
//	
//		// Deactivate loading/active connections
//		IF eState = FC_STATE_Init
//		OR eState = FC_STATE_Active
//			CPRINTLN(DEBUG_REPLAY, "RESTORE_BATTLEBUDDY_SNAPSHOT() - Clearing state of connection ", GetLabel_enumFriendConnection(eConnection))
//			g_FriendConnectState[eConnection].state = FC_STATE_ContactWait
//		ENDIF
//		
//		// Cancel any queued communications
//		CC_CommID eCommID = TEXT_FRIEND
//		
//		IF IS_COMMUNICATION_REGISTERED(eCommID)
//			CC_CommunicationStatus status = GET_COMMUNICATION_STATUS(eCommID)
//
//			IF status = CS_WAITING_FOR_GLOBAL_DELAY
//			OR status = CS_WAITING_FOR_CHARACTER_DELAY
//			OR status = CS_WAITING_FOR_QUEUE_TIMER
//			OR status = CS_WAITING_FOR_HIGHER_PRIORITY
//			OR status = CS_ERROR
//				CPRINTLN(DEBUG_REPLAY, "RESTORE_BATTLEBUDDY_SNAPSHOT() - Cancelling communication for connection ", GetLabel_enumFriendConnection(eConnection))
//				CANCEL_COMMUNICATION(eCommID)
//			ENDIF
//		ENDIF
//		
//	ENDREPEAT
//	
//	// Restore soldier fail timers
//	enumCharacterList eLoopChar
//	REPEAT NUM_OF_PLAYABLE_PEDS eLoopChar
//		IF IS_BIT_SET(snapshot.iFailedChars, ENUM_TO_INT(eLoopChar))
//			RESTART_TIMER_NOW(g_SavedGlobals.sFriendsData.g_SoldierFailTimers[eLoopChar])
//		ELSE
//			CANCEL_TIMER(g_SavedGlobals.sFriendsData.g_SoldierFailTimers[eLoopChar])
//		ENDIF
//	ENDREPEAT
//	
//	// Set connections to create replay members
//	IF snapshot.iReplayChars <> 0
//
//		SET_SQUAD_MISSION(snapshot.eMission, snapshot.iAllowedChars, TRUE)
//
//		enumCharacterList ePlayerChar = GET_CURRENT_PLAYER_PED_ENUM()
//		enumCharacterList eChar
//		
//		REPEAT MAX_BATTLE_BUDDIES eChar
//			IF eChar <> ePlayerChar
//				IF IS_BIT_SET(snapshot.iReplayChars, ENUM_TO_INT(eChar))
//					
//					// Get connection
//					enumFriend ePlayerID = GET_FRIEND_FROM_CHAR(ePlayerChar)
//					enumFriend eFriendID = GET_FRIEND_FROM_CHAR(eChar)
//					
//					IF ePlayerID <> NO_FRIEND AND eFriendID <> NO_FRIEND
//
//						eConnection = GET_CONNECTION_FROM_FRIENDS(ePlayerID, eFriendID)
//						IF eConnection <> NO_FRIEND_CONNECTION
//						
//							// Set connection state/mode to init replay
//							#IF IS_DEBUG_BUILD
//								TEXT_LABEL_63 tConn = GetLabel_enumFriendConnection(eConnection)
//								TEXT_LABEL_63 tState = GetLabel_enumFriendConnectionState(FC_STATE_Init)
//								TEXT_LABEL_63 tMode = GetLabel_enumFriendConnectionMode(FC_MODE_ReplayGroup)
//								CPRINTLN(DEBUG_FRIENDS, "SET_CONNECTION_STATE(", tConn, ", ", tState, ")")
//								CPRINTLN(DEBUG_FRIENDS, "SET_CONNECTION_MODE(", tConn, ", ", tMode, ")")
//							#ENDIF
//		
//							g_FriendConnectState[eConnection].state = FC_STATE_Init
//							g_FriendConnectState[eConnection].mode = FC_MODE_ReplayGroup
//						
//						ENDIF
//					
//					ENDIF
//				
//				ENDIF
//			ENDIF
//		ENDREPEAT
//
//	ENDIF

	snapshot.eMission = snapshot.eMission			// BBUDDIES REPLACED

ENDPROC


FUNC BOOL IS_BATTLEBUDDY_SNAPSHOT_RESTORED(structBattleBuddySnapshot& snapshot)

//	IF snapshot.iReplayChars <> 0											// BBUDDIES REMOVED
//
//		enumCharacterList ePlayerChar = GET_CURRENT_PLAYER_PED_ENUM()
//		
//		enumCharacterList eChar
//		REPEAT MAX_BATTLE_BUDDIES eChar
//			IF eChar <> ePlayerChar
//				IF IS_BIT_SET(snapshot.iReplayChars, ENUM_TO_INT(eChar))
//					
//					// Get connection
//					enumFriendConnection eConnection	= NO_FRIEND_CONNECTION
//					enumFriend ePlayerID				= GET_FRIEND_FROM_CHAR(ePlayerChar)
//					enumFriend eFriendID				= GET_FRIEND_FROM_CHAR(eChar)
//					IF ePlayerID <> NO_FRIEND AND eFriendID <> NO_FRIEND
//
//						eConnection = GET_CONNECTION_FROM_FRIENDS(ePlayerID, eFriendID)
//						IF eConnection <> NO_FRIEND_CONNECTION
//					
//							IF g_FriendConnectState[eConnection].state = FC_STATE_Init
//								CPRINTLN(DEBUG_REPLAY, "IS_BATTLEBUDDY_SNAPSHOT_RESTORED() - waiting on connection ", GetLabel_enumFriendConnection(eConnection))
//								RETURN FALSE
//							
//							ELIF g_FriendConnectState[eConnection].state = FC_STATE_Active
//								PED_INDEX hBuddyPed = GET_BATTLEBUDDY_PED(eChar)
//								IF IS_PED_INJURED(hBuddyPed)
//									CPRINTLN(DEBUG_REPLAY, "IS_BATTLEBUDDY_SNAPSHOT_RESTORED() - waiting on ped creation ", GetLabel_enumCharacterList(eChar))
//									RETURN FALSE
//								
//								ELIF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(hBuddyPed)
//									CPRINTLN(DEBUG_REPLAY, "IS_BATTLEBUDDY_SNAPSHOT_RESTORED() - waiting on ped streaming ", GetLabel_enumCharacterList(eChar))
//									RETURN FALSE
//
//								ENDIF									
//							ENDIF
//						
//						ENDIF
//					
//					ENDIF
//				
//				ENDIF
//			ENDIF
//		ENDREPEAT
//		
//	ENDIF

	snapshot.eMission = snapshot.eMission	// BBUDDIES REPLACED
	
	RETURN TRUE

ENDFUNC

