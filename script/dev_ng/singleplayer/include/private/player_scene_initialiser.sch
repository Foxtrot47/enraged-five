USING "familyCoords_private.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_InitCoords.sch									//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    
/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL initialise_player_ped_position_for_scene(PED_REQUEST_SCENE_ENUM eScene,
		VECTOR &vCreateCoords)	//, FLOAT &fCreateHead, TEXT_LABEL_31 &tRoom)

	FLOAT fCreateHead
	TEXT_LABEL_31 tRoom

	SWITCH eScene
		CASE PR_SCENE_DEAD
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDead
			sDead = "dead eScene for player scene position: "
			sDead += Get_String_From_Ped_Request_Scene_Enum(eScene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sDead)PRINTNL()
//			SCRIPT_ASSERT(sDead)
			#ENDIF
			
			RETURN FALSE
		BREAK
		CASE PR_SCENE_HOSPITAL
			
			HOSPITAL_NAME_ENUM eSelectedHospital
			GetSelectedHospital(eSelectedHospital)
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> initialise_player_ped_position_for_scene event ")
			PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eScene))
			PRINTSTRING(" has hospital \"")
			PRINTSTRING(Get_Hospital_Respawn_Name(eSelectedHospital))
			PRINTSTRING("\"")
			PRINTNL()
			#ENDIF
			
			IF eSelectedHospital < NUMBER_OF_HOSPITAL_LOCATIONS
				vCreateCoords	= g_sHospitals[eSelectedHospital].vSpawnCoords
	//			fCreateHead	= g_sHospitals[eSelectedHospital].fSpawnHeading
	//			tRoom	= ""
				RETURN TRUE
			ELSE
				vCreateCoords	= g_sHospitals[0].vSpawnCoords
	//			fCreateHead	= g_sHospitals[eSelectedHospital].fSpawnHeading
	//			tRoom	= ""
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PR_SCENE_M_OVERRIDE
			vCreateCoords	= g_sOverrideScene[CHAR_MICHAEL].vCreateCoords
			fCreateHead	= g_sOverrideScene[CHAR_MICHAEL].fCreateHead
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_OVERRIDE
			vCreateCoords	= g_sOverrideScene[CHAR_FRANKLIN].vCreateCoords
			fCreateHead	= g_sOverrideScene[CHAR_FRANKLIN].fCreateHead
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_OVERRIDE
			vCreateCoords	= g_sOverrideScene[CHAR_TREVOR].vCreateCoords
			fCreateHead	= g_sOverrideScene[CHAR_TREVOR].fCreateHead
			tRoom = ""
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_DEFAULT
			vCreateCoords	= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_MICHAEL] + << 0,0,-1 >>
			fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_MICHAEL]
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_DEFAULT
			vCreateCoords	= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_FRANKLIN] + << 0,0,-1 >>
			fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_FRANKLIN]
			tRoom	= ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DEFAULT
			vCreateCoords	= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_TREVOR] + << 0,0,-1 >>
			fCreateHead	= g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_TREVOR]
			tRoom	= ""
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				tRoom = "v_strip3"
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_PHONECALL_ARM3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				tRoom = "v_strip3"
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ma_ARM3
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1		RETURN initialise_player_ped_position_for_scene(PR_SCENE_Fa_STRIPCLUB_ARM3, vCreateCoords) BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM1		RETURN initialise_player_ped_position_for_scene(PR_SCENE_Fa_PHONECALL_ARM3, vCreateCoords) BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3		RETURN initialise_player_ped_position_for_scene(PR_SCENE_Fa_STRIPCLUB_ARM3, vCreateCoords) BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM3		RETURN initialise_player_ped_position_for_scene(PR_SCENE_Fa_PHONECALL_ARM3, vCreateCoords) BREAK
//		CASE PR_SCENE_Fa_FAMILY1
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Fa_FAMILY3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FBI2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FAMILY1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI4intro
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI4intro
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI4
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI5
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI5
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ma_FAMILY4_a
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ma_FAMILY4_b
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Ta_FAMILY4
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALEC
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY1
			RETURN initialise_player_ped_position_for_scene(PR_SCENE_F_TRAFFIC_a, vCreateCoords) BREAK
		CASE PR_SCENE_Fa_AGENCYprep1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY3B
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ta_CARSTEAL1
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Fa_CARSTEAL1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FBI2
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FBI4
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_DOCKS2B
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FAMILY6
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FINALEprepD
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Ma_FAMILY6
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_MARTIN1
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Ma_TREVOR3
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Fa_TREVOR3
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		
		
		CASE PR_SCENE_Ma_FRANKLIN2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FRANKLIN2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FBI1end
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		
//		CASE PR_SCENE_Ma_MARTIN1
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ta_MARTIN1
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Fa_RURAL2A
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_RURAL2A
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK
		CASE PR_SCENE_Ta_RC_MRSP2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				tRoom = "v_trailer"
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_RURAL1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1a
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_FTa_FRANKLIN1b
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1c
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1d
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1e
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_Ma_FRANKLIN2
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ta_FRANKLIN2
//			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_Ma_EXILE2		vCreateCoords = <<-803.7340, 168.1480, 76.3542>>	RETURN TRUE BREAK
		CASE PR_SCENE_Fa_EXILE2
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_EXILE3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_EXILE3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M4_WASHFACE	vCreateCoords = <<-803.7340, 168.1480, 76.3542>>	RETURN TRUE BREAK
		CASE PR_SCENE_Fa_MICHAEL3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_MICHAEL3
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_DOCKS2A
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_DOCKS2A
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALE1
			vCreateCoords = << 0,0,0 >>	RETURN TRUE BREAK		//0
				RETURN FALSE	BREAK
		CASE PR_SCENE_Ta_FINALE1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL4
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALE2intro
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALE2intro
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_DOCKS2B
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALE1
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_AGENCY3A
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALE2A
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALE2A
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALE2B
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ta_FINALE2B
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALEA
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALEB
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Ma_FINALEC
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_Fa_FINALEC
			IF GetLastKnownPedInfoPostMission(eScene, vCreateCoords, fCreateHead)
				RETURN TRUE
			ENDIF
		BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT			vCreateCoords = <<1970.7460, 3818.9980, 33.4200>>		RETURN TRUE BREAK		//122.6900	tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_T_JERKOFF			vCreateCoords = <<1971.1860, 3818.9680, 33.4287>>		RETURN TRUE BREAK		//-60.3100	tRoom = "v_trailer"	RETURN TRUE BREAK
		#ENDIF
		CASE PR_SCENE_T_HEADINSINK
			vCreateCoords = <<1975.312,3817.041,33.530>>		//#1295948
			//vCreateCoords = <<1972.5560, 3817.6980, 32.0187>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M_MD_FBI2			vCreateCoords = << 1424.3041, -1898.6106, 69.8678 >>	RETURN TRUE BREAK		//-172.697		RETURN TRUE BREAK			
		CASE PR_SCENE_F_MD_FRANKLIN2
			vCreateCoords = <<1357.3,-2267.1,61.1>>		//<<1377.4924, -2327.0713, 60.1910>>
			RETURN TRUE BREAK

		CASE PR_SCENE_M2_BEDROOM		vCreateCoords = <<-814.2460, 181.2640, 75.7407>>		RETURN TRUE BREAK		//-158.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b	vCreateCoords = <<-813.7660, 181.0540, 76.7504>>		RETURN TRUE BREAK		//-152.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a	vCreateCoords = <<-805.1700, 173.9900, 72.6949>>		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_b	vCreateCoords = <<-774.9665, 183.9946, 71.8350+0.5>>		RETURN TRUE BREAK		//126.9310		RETURN TRUE BREAK
		CASE PR_SCENE_M2_SMOKINGGOLF	vCreateCoords = <<-1313.7480, 121.4050, 56.6578>>		RETURN TRUE BREAK		//-45.0000		RETURN TRUE BREAK
		
		CASE PR_SCENE_M4_WAKEUPSCREAM	vCreateCoords = <<-812.9260, 181.6140, 76.7408-0.0097>>		RETURN TRUE BREAK		//-113.7480	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED	vCreateCoords = <<-813.1960, 181.7640, 76.7407>>		RETURN TRUE BREAK		//-173.7480	tRoom = "v_michael"	RETURN TRUE BREAK
//		CASE PR_SCENE_M4_HOUSEBED_b		vCreateCoords = <<-803.0300, 172.7300, 72.8400>>		RETURN TRUE BREAK		//-102.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSEBED		vCreateCoords = <<1968.0200, 3816.7600, 32.4291>>		RETURN TRUE BREAK
		CASE PR_SCENE_M4_WATCHINGTV		vCreateCoords = <<-802.3999, 172.4400, 72.8447>>		RETURN TRUE BREAK		//-56.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_KIDS_TV		vCreateCoords = <<-805.1730, 173.9870, 72.6876>>		RETURN TRUE BREAK		//22.0000	tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_a		vCreateCoords = << -780.694, 187.325, 72.812-1+1 >>		RETURN TRUE BREAK
		CASE PR_SCENE_M_POOLSIDE_b		vCreateCoords = <<-1353.3110, 355.9345, 64.0704>>		RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_a		vCreateCoords = <<-826.5596, 155.8342, 68.3283>>		RETURN TRUE BREAK
		CASE PR_SCENE_M2_CARSLEEP_b		vCreateCoords = <<-887.9781, 133.6231, 58.3423>>		RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_a			vCreateCoords = <<-1210.3170, -955.7397, 1.6553>>		RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_b			vCreateCoords = <<-848.0614, 855.9160, 202.5614>>		RETURN TRUE BREAK		//-54.3470		RETURN TRUE BREAK
		CASE PR_SCENE_M_CANAL_c			vCreateCoords = <<-1268.6400, -711.4000, 22.4619>>		RETURN TRUE BREAK		//117.0000		RETURN TRUE BREAK
		CASE PR_SCENE_M2_LUNCH_a		vCreateCoords = <<-117.0210, 361.4320, 111.8857>>		RETURN TRUE BREAK		//-36.000			RETURN TRUE BREAK
//		CASE PR_SCENE_M7_LUNCH_b		vCreateCoords = <<-512.2636, -17.9697, 45.6085>>		RETURN TRUE BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT	vCreateCoords = <<394.6800, 176.8100, 103.8401>>		RETURN TRUE BREAK		//70.0000		RETURN TRUE BREAK
		CASE PR_SCENE_M4_LUNCH_b		vCreateCoords = <<-1368.0250, 54.7852, 52.7046>>		RETURN TRUE BREAK
		CASE PR_SCENE_M4_CINEMA		vCreateCoords = <<-1415.5200, -198.5900, 47.2507>>			RETURN TRUE BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR
			vCreateCoords = <<-613.7560, -211.7579, 36.1054+1>>		+ <<0.0203, 0.0260, -0.0069>>

			vCreateCoords += <<0.0164, 0.0261, -0.0009>>
			fCreateHead += 0.0004

			vCreateCoords += <<0.0168, 0.0264, 0.0011>>
			fCreateHead += 0.0015
			
			vCreateCoords += <<0.0173, 0.0268, 0.0011>>
			fCreateHead += 0.0002
			
			vCreateCoords += <<0.0179, 0.0271, 0.0011>>
			fCreateHead += -0.0009			
			
			RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a	vCreateCoords = <<-1360.1561, -559.5457, 29.0697>>		RETURN TRUE BREAK		//-57.0000		RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b	vCreateCoords = <<-590.0963, 270.1470, 81.2426>>		RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a	vCreateCoords = <<166.4449, -211.2980, 53.0941>>		RETURN TRUE BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b	vCreateCoords = <<401.7279, 308.2417, 102.5000>>		RETURN TRUE BREAK		//69.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_PIER_a			vCreateCoords = << -1731.9399, -1125.1300, 12.0176+1>>	RETURN TRUE BREAK		//143.4931			RETURN TRUE BREAK
		CASE PR_SCENE_M_PIER_b			vCreateCoords = <<-1927.7800, -579.0700, 11.1705>>		RETURN TRUE BREAK		//123.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_a		vCreateCoords = << -464.22, -1592.98, 38.1269 >>			RETURN TRUE BREAK		//168.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_b		vCreateCoords = <<-1744.1995, -625.3162, 9.8308>>		RETURN TRUE BREAK		//63.4995			RETURN TRUE BREAK
		CASE PR_SCENE_M_TRAFFIC_c		vCreateCoords = <<-1426.9100, -39.0000, 51.8742>>		RETURN TRUE BREAK		//-159.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_a		vCreateCoords = <<260.9800, 1117.8101, 220.1383>>		RETURN TRUE BREAK		//-6.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_VWOODPARK_b		vCreateCoords = <<-1819.5800, -677.5900, 10.4119>>		RETURN TRUE BREAK		//99.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_a		vCreateCoords = << -95.550, -415.100, 35.6806>>			RETURN TRUE BREAK		//133.000			RETURN TRUE BREAK
		CASE PR_SCENE_M_BENCHCALL_b		vCreateCoords = <<-1292.7010, -697.2287, 24.2677>>		RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_a	vCreateCoords = <<814.9800, 1270.0100, 360.4754>>		RETURN TRUE BREAK		//-162.3110			RETURN TRUE BREAK
		CASE PR_SCENE_M_PARKEDHILLS_b	vCreateCoords = <<-1668.2600, 488.3000, 128.8760>>		RETURN TRUE BREAK		//172.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_a	vCreateCoords = <<667.7000, 3503.7000, 33.9937>>		RETURN TRUE BREAK		//-59.2500			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_b	vCreateCoords = <<2405.1101, 4296.9600, 35.1743>>		RETURN TRUE BREAK		//91.0140			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_c	vCreateCoords = <<100.9571, 3363.9929, 34.4471>>		RETURN TRUE BREAK		//-152.9650			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_d	vCreateCoords = <<2445.2058, 3800.6694, 40.0793>>		RETURN TRUE BREAK		//-10.0990			RETURN TRUE BREAK
		CASE PR_SCENE_M6_PARKEDHILLS_e	vCreateCoords = <<1775.4447, 4584.7432, 37.6512>>		RETURN TRUE BREAK		//158.5974			RETURN TRUE BREAK
		CASE PR_SCENE_M2_DRIVING_a
			/*
			VECTOR vDriveM2AJumpOffset
			Get_Vector_From_DebugJumpAngle(GET_RANDOM_FLOAT_IN_RANGE(0, 360), 125, vDriveM2AJumpOffset)
			
			vCreateCoords = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY)+vDriveM2AJumpOffset
			//GET_HEADING_FROM_VECTOR_2D(-vDriveM2AJumpOffset.x, -vDriveM2AJumpOffset.y)
			*/
			
			vCreateCoords = <<-760.5784, 229.5830, 74.6747>>
			//99.180	//72.1803
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DRIVING_b
			/*
			VECTOR vDriveM2BJumpOffset
			Get_Vector_From_DebugJumpAngle(GET_RANDOM_FLOAT_IN_RANGE(0, 360), 125, vDriveM2BJumpOffset)
			
			vCreateCoords = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_LESTER)+vDriveM2BJumpOffset
			//GET_HEADING_FROM_VECTOR_2D(-vDriveM2BJumpOffset.x, -vDriveM2BJumpOffset.y)
			*/
			
			vCreateCoords = <<1316.4146, -1599.3450, 51.3924>>
			//218.4774
			tRoom = ""
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_DRIVING_a		vCreateCoords = <<2365.1050, 3904.7151, 35.2000>>			RETURN TRUE BREAK		//17.6172			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_b		vCreateCoords = <<-483.2213, 5876.3921, 33.0000>>		RETURN TRUE BREAK		//142.3730			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_c		vCreateCoords = <<-180.0459, 6464.7988, 30.2000>>		RETURN TRUE BREAK		//-34.8780			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_d		vCreateCoords = <<1663.8590, 4876.2842, 41.6000>>		RETURN TRUE BREAK		//-172.4190			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_e		vCreateCoords = <<2156.0642, 3253.6323, 46.9000>>		RETURN TRUE BREAK		//-107.4390			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_f		vCreateCoords = <<2782.8359, 3476.8198, 54.8000>>		RETURN TRUE BREAK		//163.3110			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_g		vCreateCoords = <<2543.8311, 2618.3198, 37.5000>>		RETURN TRUE BREAK		//-68.8120			RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRIVING_h		vCreateCoords = <<1295.5291, 1529.8110, 96.6000>>		RETURN TRUE BREAK		//4.6930			RETURN TRUE BREAK
		CASE PR_SCENE_M6_RONBORING		vCreateCoords = <<1978.0699, 3819.5640, 32.4290>>		RETURN TRUE BREAK		//78.6500		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M2_PHARMACY 		vCreateCoords = << 68.7900, -1561.2699, 29.4564>>		RETURN TRUE BREAK		//8.0000			RETURN TRUE BREAK
//		CASE PR_SCENE_M4_DOORSTUMBLE	vCreateCoords = <<554.0200, 141.8630, 98.8955>>			RETURN TRUE BREAK		//67.5000			RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_a		vCreateCoords = <<-511.7300, -21.8700, 45.5884>>		RETURN TRUE BREAK		//69.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_b		vCreateCoords = << 	-628.800, 242.463, 81.8695>>		RETURN TRUE BREAK		//0.000			RETURN TRUE BREAK
		CASE PR_SCENE_M_COFFEE_c		vCreateCoords = <<-834.5300, -350.7100, 38.6537>>		RETURN TRUE BREAK		//-74.7818			RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_a		vCreateCoords = <<-1073.1270, -1538.8320, 4.1100>>		RETURN TRUE BREAK		//-48.3600			RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_b		vCreateCoords = <<123.0931, 649.6752, 207.7751>>		RETURN TRUE BREAK		//144.1780			RETURN TRUE BREAK
		CASE PR_SCENE_M2_CYCLING_c		vCreateCoords = << -820.9000, 85.3000, 51.9813 >>		RETURN TRUE BREAK		//288.0000			RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_a			vCreateCoords = <<-1209.1970,-824.5692, 15.28>>			RETURN TRUE BREAK		//-46.291			RETURN TRUE BREAK
//		CASE PR_SCENE_M_BAR_b			vCreateCoords = <<-429.8782, -24.0212, 46.2039>>		RETURN TRUE BREAK		//-88.1930			RETURN TRUE BREAK
		CASE PR_SCENE_M2_MARINA			vCreateCoords = <<-831.3530, -1358.7480, 4.9732>>		RETURN TRUE BREAK		//101.5000			RETURN TRUE BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE	vCreateCoords = <<-812.3460, 179.8700, 72.1592>>		RETURN TRUE BREAK		//99.7200		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M4_PARKEDBEACH	vCreateCoords = <<-2015.6801, -495.4000, 11.7326>>		RETURN TRUE BREAK		//120.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M_HOOKERMOTEL		vCreateCoords = <<-1499.9800, -677.0900, 27.0668>>		RETURN TRUE BREAK		//132.0000			RETURN TRUE BREAK
//		CASE PR_SCENE_M_HOOKERCAR		vCreateCoords = <<851.6744, -1198.7061, 25.0109>>		RETURN TRUE BREAK		//-88.6774			RETURN TRUE BREAK
		CASE PR_SCENE_M6_MORNING_a		vCreateCoords = <<1971.8621, 3813.9099, 32.9309>>		RETURN TRUE BREAK
//		CASE PR_SCENE_M6_MORNING_b		vCreateCoords = <<1974.1980, 3820.1880, 32.4266>>		RETURN TRUE BREAK		//-142.8420		tRoom = "v_Trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_CARSLEEP		vCreateCoords = <<669.7389, 3503.8301, 32.9168>>		RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSETV_a		vCreateCoords = <<1974.7679, 3821.2419, 32.4384+0.5>>		RETURN TRUE BREAK		//-104.5890		tRoom = "v_trailer"	RETURN TRUE BREAK
//		CASE PR_SCENE_M6_HOUSETV_b		vCreateCoords = <<1969.7531, 3815.7180, 32.4000>>		RETURN TRUE BREAK		//-138.8460		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_SUNBATHING		vCreateCoords = <<1981.2380, 3821.5430, 31.9677>>		RETURN TRUE BREAK
		CASE PR_SCENE_M6_DRINKINGBEER	vCreateCoords = <<1976.7500, 3822.7600, 33.2800>>		RETURN TRUE BREAK		//-147.6540		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_ONPHONE		vCreateCoords = <<1981.3320, 3821.5200, 31.9535>>		RETURN TRUE BREAK
		CASE PR_SCENE_M6_DEPRESSED		vCreateCoords = <<1974.3120, 3821.1001, 32.8864>>		RETURN TRUE BREAK		//-161.0894		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_M6_BOATING		vCreateCoords = << 1940.0519, 4018.8535, 28.9009 >>		RETURN TRUE BREAK		//226.5579-270 		RETURN TRUE BREAK
		CASE PR_SCENE_M6_LIQUORSTORE	vCreateCoords = <<1992.1500, 3056.4199, 47.0342>>		RETURN TRUE BREAK
		CASE PR_SCENE_M7_RESTAURANT		vCreateCoords = <<-115.9200, 363.5000, 112.8857>>		RETURN TRUE BREAK		//-6.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS	vCreateCoords = <<-1353.7910, 355.1845, 64.0800>>		RETURN TRUE BREAK		//72.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a	vCreateCoords = <<-718.8135, 256.7636, 79.8384>>		RETURN TRUE BREAK		//-167.2500			RETURN TRUE BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b	vCreateCoords = <<-718.8735, 256.4936, 79.8259>>		RETURN TRUE BREAK		//-147.1920			RETURN TRUE BREAK
		CASE PR_SCENE_M7_WIFETENNIS		vCreateCoords = <<-770.6851, 157.8133, 67.5042>>		RETURN TRUE BREAK		//59.0820			RETURN TRUE BREAK
		CASE PR_SCENE_M7_ROUNDTABLE		vCreateCoords = <<-796.7593, 180.4725, 71.8266>>		RETURN TRUE BREAK		//26.0870		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M7_REJECTENTRY	vCreateCoords = <<-718.0311, 254.9289, 79.7959>>		RETURN TRUE BREAK
		CASE PR_SCENE_M7_HOOKERS		vCreateCoords = <<533.1877, 109.0133, 96.4624>>			RETURN TRUE BREAK		//-13.8153			RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITBARBER		vCreateCoords = <<-823.2000, -187.0830, 37.7753>>		RETURN TRUE BREAK		//-62.5000			RETURN TRUE BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP	vCreateCoords = <<-715.6204, -155.5691, 37.4023>>		RETURN TRUE BREAK		//119.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M7_FAKEYOGA		vCreateCoords = <<-790.3314, 186.4809, 71.8350>>		RETURN TRUE BREAK		//68.3776			RETURN TRUE BREAK
		CASE PR_SCENE_M7_COFFEE			vCreateCoords = <<-1367.3500, -208.8400, ((44.4134+44.4160)/2.0) - 0.01>>		RETURN TRUE BREAK
		CASE PR_SCENE_M7_GETSREADY		vCreateCoords = <<-812.8960, 181.1140, 76.7233>>		RETURN TRUE BREAK		//-164.0000		tRoom = "v_michael"	RETURN TRUE BREAK
//		CASE PR_SCENE_M7_PARKEDHILLS	RETURN initialise_player_ped_position_for_scene(PR_SCENE_M_PARKEDHILLS_a, vCreateCoords) BREAK
		CASE PR_SCENE_M7_READSCRIPT		vCreateCoords = <<-781.2640, 187.1150, 72.8425>>		RETURN TRUE BREAK		//88.0000			RETURN TRUE BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO	vCreateCoords = <<-1135.5466, -450.7346, 35.4977>>		RETURN TRUE BREAK
		CASE PR_SCENE_M7_TALKTOGUARD	vCreateCoords = <<-1050.0129, -481.4982, 36.7625>>		RETURN TRUE BREAK		//-24.2962			RETURN TRUE BREAK
		CASE PR_SCENE_M7_LOT_JIMMY		vCreateCoords = <<-1180.0620, -498.2454, 35.5670>>		RETURN TRUE BREAK		//-22.3200			RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_TV
			
			VECTOR vInitOffset
			FLOAT fInitHead
			
			IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, FE_M7_SON_watching_TV_with_tracey,
					vInitOffset, fInitHead)
				vCreateCoords = vInitOffset + << -812.0607, 179.5117, 71.1531 >> //+ <<0,0,1>>
				//fInitHead + 222.8314
				tRoom = "v_michael"
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING	vCreateCoords = <<-808.3299, 170.7319, 76.7408>>		RETURN TRUE BREAK		//112.8410		tRoom = "v_michael"	RETURN TRUE BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA	vCreateCoords = <<-656.9569, -276.9062, 35.6524>>		RETURN TRUE BREAK		//-103.8158			RETURN TRUE BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			vCreateCoords = <<-250.9305, -80.8638, 48.4993>>
			RETURN TRUE BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY	vCreateCoords = <<-817.7500, 170.0200, 70.4911>>		RETURN TRUE BREAK		//-0.0301			RETURN TRUE BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
			vCreateCoords = <<-248.9653, -81.0186, 48.6174>>
			//-30.1850
			
			vCreateCoords += <<0.0091, 0.0273, 0.0134>>
			fCreateHead += 0.0030
			
			RETURN TRUE BREAK
		CASE PR_SCENE_M_S_FAMILY4
			vCreateCoords	= <<-803.27, 183.78, 71.61>>
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	SWITCH eScene
		CASE PR_SCENE_F0_SH_ASLEEP		vCreateCoords = <<-17.2168, -1441.2240, 30.1015>>		RETURN TRUE BREAK		//-179.6530		tRoom = "v_franklins"		RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_ASLEEP		vCreateCoords = <<-0.4500, 525.4900, 169.6400>>			RETURN TRUE BREAK		//-147.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_NAPPING		vCreateCoords = <<-0.1090, 524.3119, 170.3068>>			RETURN TRUE BREAK		//-81.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_GETTINGREADY	vCreateCoords = <<9.0865, 528.0272, 170.6172>>			RETURN TRUE BREAK		//-95.4016		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_READING		vCreateCoords = <<-17.2672, -1441.1541, 30.1015>>		RETURN TRUE BREAK		//fCreateHead =-16.0627+180	tRoom = "v_franklins"		RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_READING		vCreateCoords = <<1.7600, 525.9200, 173.63>>			RETURN TRUE BREAK		//129.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a	vCreateCoords = <<-17.4073, -1439.4010, 31.1023>>		RETURN TRUE BREAK		//-86.6130		tRoom = "v_franklins"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_b	vCreateCoords = <<-13.8167, -1423.6730, 30.7231 >>		RETURN TRUE BREAK		//76.1360			RETURN TRUE BREAK
		CASE PR_SCENE_F1_SH_PUSHUP		vCreateCoords = <<15.3608, 523.6475, 169.2282+1 >>		RETURN TRUE BREAK		//111.6880			RETURN TRUE BREAK
		CASE PR_SCENE_F1_POOLSIDE_a		vCreateCoords = <<-11.5281, 512.3040, 174.5978>>		RETURN TRUE BREAK		//143.7974			RETURN TRUE BREAK
		CASE PR_SCENE_F1_POOLSIDE_b		vCreateCoords = <<20.9569, 521.8147, 170.1977>>			RETURN TRUE BREAK		//143.7920			RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANINGAPT	vCreateCoords = <<-1.5790, 535.2489, 175.3424>>			RETURN TRUE BREAK		//4.6834		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONCELL			vCreateCoords = <<3.8524, 525.7295, 174.6234>>			RETURN TRUE BREAK		//-108.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_SNACKING		vCreateCoords = <<-8.8600, 515.8400, 174.6280>>			RETURN TRUE BREAK		//69.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_ONLAPTOP		vCreateCoords = <<-6.9799, 524.9367, 174.9997>>			RETURN TRUE BREAK		//-172.2207		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_IRONING		vCreateCoords = <<1.4483, 527.5843, 170.0596>>			RETURN TRUE BREAK		//0.0000		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
//		CASE PR_SCENE_F0_WATCHINGTV		vCreateCoords = <<-9.6372, -1439.4460, 31.1015>>		RETURN TRUE BREAK		//179.0318		tRoom = "v_franklins"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_WATCHINGTV		vCreateCoords = <<1.8291, 526.7450, 174.6267>>			RETURN TRUE BREAK		//-12.5158		tRoom = "v_franklinshouse"	RETURN TRUE BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT	vCreateCoords = <<-14.8689, -1441.1821, 31.1932>>		RETURN TRUE BREAK		//-151.5000		tRoom = "v_franklins"	RETURN TRUE BREAK
		CASE PR_SCENE_F1_NEWHOUSE		vCreateCoords = <<3.6410, 530.1489, 175.6695>>			RETURN TRUE BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC		vCreateCoords = <<-1174.4580, -1573.6320, 4.7619>>		RETURN TRUE BREAK		//105.9810			RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_a		vCreateCoords = <<-1175.2980, -1573.6920, 4.3599>>		RETURN TRUE BREAK		//172.9187			RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_b		vCreateCoords = <<-1153.5110, -1371.6520, 4.0730>>		RETURN TRUE BREAK		//-67.6080			RETURN TRUE BREAK
		CASE PR_SCENE_F_KUSH_DOC_c		vCreateCoords = <<-1162.9871, -1427.2640, 3.6370>>		RETURN TRUE BREAK		//74.1158			RETURN TRUE BREAK
		CASE PR_SCENE_F0_GARBAGE		vCreateCoords = <<-13.7603, -1451.2000, 29.6322>>		RETURN TRUE BREAK		//1.0411			RETURN TRUE BREAK
		CASE PR_SCENE_F1_GARBAGE		vCreateCoords = <<14.3790, 544.1280, 175.0021>>			RETURN TRUE BREAK		//-152.2030			RETURN TRUE BREAK
		CASE PR_SCENE_F_THROW_CUP		vCreateCoords = << 2.8895, -1607.2864, 29.2949 >>		RETURN TRUE BREAK		//310.879-180 tRoom = ""	RETURN TRUE BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND	vCreateCoords = <<2.8895, -1607.2864, 29.2866>>			RETURN TRUE BREAK		//130.8790			RETURN TRUE BREAK
		CASE PR_SCENE_F_GYM				vCreateCoords = <<-1244.8879, -1613.6560, 4.1295>>		RETURN TRUE BREAK		//35.6040			RETURN TRUE BREAK
		CASE PR_SCENE_F0_WALKCHOP		vCreateCoords = <<-16.4273, -1452.2660, 30.5424>>		RETURN TRUE BREAK
		CASE PR_SCENE_F0_PLAYCHOP		vCreateCoords = <<-15.0259, -1422.9363, 30.6908>>		RETURN TRUE BREAK		//-119.3944			RETURN TRUE BREAK
//		CASE PR_SCENE_F1_WALKCHOP		vCreateCoords = <<15.3388, 523.7823, 170.1958>>			RETURN TRUE BREAK		//120.7268			RETURN TRUE BREAK
		CASE PR_SCENE_F1_PLAYCHOP		vCreateCoords = <<15.3678, 523.7120, 170.2095>>			RETURN TRUE BREAK		//121.9322			RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_a		vCreateCoords = <<154.0731, 765.5721, 209.6720>>		RETURN TRUE BREAK		//-36.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F_WALKCHOP_b		vCreateCoords = <<-268.1390, 415.2881, 109.7258>>		RETURN TRUE BREAK		//-95.5880			RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_a		vCreateCoords = <<-464.2200, -1592.9800, 38.7300>>		RETURN TRUE BREAK		//168.0000 			RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_b		vCreateCoords = <<31.90, -1483.30, 29.26>>				RETURN TRUE BREAK		//230.78			RETURN TRUE BREAK
		CASE PR_SCENE_F_TRAFFIC_c		vCreateCoords = <<208.9683, 222.0408, 104.6000>>		RETURN TRUE BREAK		//165.7751			RETURN TRUE BREAK
		CASE PR_SCENE_F0_BIKE			vCreateCoords = <<-24.5203, -1436.2000, 30.1544>>		RETURN TRUE BREAK		//-179.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F0_CLEANCAR
			vCreateCoords = <<-23.8762, -1444.5953, 30.6345>>
			//1.2709
			RETURN TRUE BREAK
			
		CASE PR_SCENE_F1_BIKE			vCreateCoords = <<14.0000, 546.1900, 175.4851>>			RETURN TRUE BREAK		//75.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F1_CLEANCAR		vCreateCoords = <<8.8185, 545.0300, 175.6051>>			RETURN TRUE BREAK
		CASE PR_SCENE_F1_BYETAXI		vCreateCoords = <<10.9694, 551.7596, 176.1069>>			RETURN TRUE BREAK		//-49.0324			RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_c			vCreateCoords = <<-1150.8199, 943.2700, 198.2370>>		RETURN TRUE BREAK		//102.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F_BIKE_d			vCreateCoords = <<-1689.0000, -946.1600, 7.1768>>		RETURN TRUE BREAK
//		CASE PR_SCENE_F_LAMGRAFF 		vCreateCoords = <<-86.0010, -1456.8710, 33.0587>>		RETURN TRUE BREAK		//90.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F_CLUB
			vCreateCoords = <<-521.1300, -28.5400, 45.2617>>		//vCreateCoords = <<-257.2960, 242.5970, 92.0869>>		RETURN TRUE BREAK		//3.9600			RETURN TRUE BREAK
			RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE	vCreateCoords = <<480.9113, -1316.3550, 29.1966>>		RETURN TRUE BREAK		//-59.3848		tRoom = "v_chopshop"	RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPEHANDS	vCreateCoords = <<473.3613, -1309.9950, 29.2326>>		RETURN TRUE BREAK		//43.8200		tRoom = "v_chopshop"	RETURN TRUE BREAK
		CASE PR_SCENE_F_CS_WIPERIGHT	RETURN initialise_player_ped_position_for_scene(PR_SCENE_F_CS_CHECKSHOE, vCreateCoords) BREAK
		CASE PR_SCENE_F_BAR_a_01		vCreateCoords = <<28.9860, -1351.4120, 29.3437>>		RETURN TRUE BREAK		//160.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_b_01		vCreateCoords = <<-379.1773, 220.9259, 84.1440>>		RETURN TRUE BREAK		//-14.7490			RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_c_02		vCreateCoords = <<131.5816, -1303.5580, 29.1592>>		RETURN TRUE BREAK		//-150.0000			RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_d_02		vCreateCoords = <<792.1553, -735.5871, 27.5721>>		RETURN TRUE BREAK		//96.0116			RETURN TRUE BREAK
		CASE PR_SCENE_F_BAR_e_01		vCreateCoords = <<-297.4081, -1332.3430, 31.3057>>		RETURN TRUE BREAK		//-43.6661			RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1								//alley behind franklin's house
			vCreateCoords =	<<-9.4, -1415.3, 28.32>>			RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3								//rec center next to basketball courts
			vCreateCoords = <<-242.0927, -1538.1809, 30.5334>>	RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5								//street corner gang area
			vCreateCoords = <<-18.8892, -1823.9120, 25.8711>>	RETURN TRUE BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT							//alley in gang area
			vCreateCoords = <<192.751, -1672.653, 28.8033>>		RETURN TRUE BREAK
			
		CASE PR_SCENE_F_S_EXILE2
			vCreateCoords = <<-2654.6338, 2625.1609, 15.6744>>	// <<-2646.31, 2677.32, 16.6427>>	//<<-2689.2244, 2368.0752, 15.7681>>
			//350.3382
			RETURN TRUE BREAK
			
		CASE PR_SCENE_F_S_AGENCY_2A_a
			vCreateCoords = << -78.4023, -1019.2347, 28.5449 >>
			RETURN TRUE BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_b
			vCreateCoords = << -78.4023, -1019.2347, 28.5449 >>
			RETURN TRUE BREAK
//		CASE PR_SCENE_F_S_FBI1end
//			vCreateCoords = <<1601.751,-1944.011,100.732>>
//			//RAD_TO_DEG(1.25 * CONST_PI)-180
//			RETURN TRUE BREAK
		
//		CASE PR_SCENE_F_S_AGENCY_2B
//			vCreateCoords = <<144.4447, -933.0371, 28.7486>>
//			//-17.2431
//			RETURN TRUE BREAK
//		CASE PR_SCENE_F_S_AGENCY_2C
//			vCreateCoords = << 217.4798, 363.9874, 105.1759 >>
//			//198.0893
//			RETURN TRUE BREAK
		
//		CASE PR_SCENE_T_STRIPCLUB_a		vCreateCoords = <<110.1095, -1291.8535, 27.2609>>		RETURN TRUE BREAK		//30.0			tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE	vCreateCoords = <<116.9369, -1287.7040, 28.2979>>		RETURN TRUE BREAK		//-112.0000		tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_BAR			vCreateCoords = <<126.8211, -1283.7660, 29.2740>>		RETURN TRUE BREAK		//114.0000		tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_CHASE		vCreateCoords = <<127.9570, -1298.5129, 29.4270>>		RETURN TRUE BREAK		//30.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_STRIPCLUB_out	vCreateCoords = <<130.2769, -1300.8740, 29.1559>>		RETURN TRUE BREAK		//-164.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_ESCORTED_OUT	vCreateCoords = <<-55.8087, 358.255, 113.0610>>			RETURN TRUE BREAK		//-122.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_b	vCreateCoords = <<1534.0430, 3613.1221, 34.3670>>		RETURN TRUE BREAK		//-37.1240			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHATEAU_c	vCreateCoords = <<-175.4296, 6428.7500, 29.6226>>		RETURN TRUE BREAK		//108.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHATEAU_d	vCreateCoords = <<-1654.9370, -147.5126, 57.4610>>		RETURN TRUE BREAK		//13.7207			RETURN TRUE BREAK
		CASE PR_SCENE_T_SMOKEMETH		vCreateCoords = <<1972.8101, 3818.2729, 32.0050>>		RETURN TRUE BREAK		//27.7460		tRoom = "v_trailer"	RETURN TRUE BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD	vCreateCoords = <<433.8850, -1462.4780, 28.2735>>		RETURN TRUE BREAK		//18.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_THROW_FOOD		vCreateCoords = <<433.8850, -1462.4780, 28.2804>>		RETURN TRUE BREAK		//-51.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		vCreateCoords = <<-1199.5500, -1569.6880, 4.6120>>		RETURN TRUE BREAK		//-165.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS	vCreateCoords = <<-1325.8800, -1667.4900, 1.5744>>		RETURN TRUE BREAK		//133.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	vCreateCoords = <<285.9300, 182.1800, 103.3496>>		RETURN TRUE BREAK		//10.7700			RETURN TRUE BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN	vCreateCoords = <<292.1700, 191.0900, 103.3496+1>>		RETURN TRUE BREAK		//138.0000-180		RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_a			vCreateCoords = <<288.0774, -3201.8811, 5.8080>>		RETURN TRUE BREAK		//87.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_b			vCreateCoords = <<-871.2493, 67.3477, 52.1137>>			RETURN TRUE BREAK		//-96.8529			RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_c			vCreateCoords = <<-46.1798, -1474.1639, 32.0083>>		RETURN TRUE BREAK		//2.6497			RETURN TRUE BREAK
		CASE PR_SCENE_T_DOCKS_d			vCreateCoords = <<1876.0250, 2620.8269, 45.6722>>		RETURN TRUE BREAK		//135.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_LINGERIE		vCreateCoords = <<154.7300, -219.2100, 54.3030>>		RETURN TRUE BREAK		//-40.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_FUNERAL		vCreateCoords = <<411.6250, -1488.9890, 30.1244>>		RETURN TRUE BREAK		//30.2400			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_DUMPSTER		vCreateCoords = <<488.0162, -1342.3940, 29.4108>>		RETURN TRUE BREAK		//-90.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRASH_b	vCreateCoords = <<-438.0249, 1595.8950, 356.5938>>		RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEBEACH	vCreateCoords = <<-3067.8679, 130.6339, 9.9056>>		RETURN TRUE BREAK		//fCreateHead =356.8227-270		RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEBARN		vCreateCoords = <<2209.6990, 4914.9141, 39.6760>>		RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKETRAIN	vCreateCoords = <<1800.0305, 6293.4619, 48.6294>>		RETURN TRUE BREAK		//-110.4244+180		RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_WAKEROOFTOP	vCreateCoords = <<418.6078, -788.4689, 43.5311>>		RETURN TRUE BREAK		//-106.6605			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN	vCreateCoords = <<2949.5669, 5755.3389, 317.8481>>		RETURN TRUE BREAK		//-23.2800			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK	vCreateCoords = <<-1267.3890, -1098.8990, 6.8082>>		RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_ALLEYDRUNK	vCreateCoords = <<107.0137, -1316.0350, 28.2084>>		RETURN TRUE BREAK		//-83.3175			RETURN TRUE BREAK
		CASE PR_SCENE_T_PUKEINTOFOUNT	vCreateCoords = <<-118.1968, -442.9148, 35.2820>>		RETURN TRUE BREAK		//-153.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PARK_b		vCreateCoords = <<-1858.9570, 2071.2300, 140.3656>>		RETURN TRUE BREAK		//9.0000			RETURN TRUE BREAK
//		CASE PR_SCENE_T_CN_PARK_c		vCreateCoords = <<-1874.2850, 2033.0222, 138.6292>>		RETURN TRUE BREAK		//-98.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_POLICE_a
			#IF NOT USE_TU_CHANGES
			vCreateCoords = << 937.3,-1195.9,49.4>>
			#ENDIF
			#IF USE_TU_CHANGES
			vCreateCoords = (<<937.3,-1195.9,49.4>> + << 895.4,-1200.3,48.8>>) / 2.0
			#ENDIF
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_POLICE_b
			#IF NOT USE_TU_CHANGES
			vCreateCoords = <<1893.4,2303.4,54.5>>
			#ENDIF
			#IF USE_TU_CHANGES
			CONST_FLOAT fOrigPoliceBMult	1.0
			CONST_FLOAT fNewPoliceBMult		2.0
			vCreateCoords = ((<<1893.4,2303.4,54.5>>*fOrigPoliceBMult)+(<<1885.6,2251.8,54.5>>*fNewPoliceBMult))/(fOrigPoliceBMult+fNewPoliceBMult)
			#ENDIF
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_POLICE_c		vCreateCoords = <<1076.9883, 2685.1528, 37.9730>>		RETURN TRUE BREAK		//-98.5600			RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE	vCreateCoords = <<642.6800, -1001.2700, 36.8997>>		RETURN TRUE BREAK		//-33.7700			RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_GARDEN	vCreateCoords = <<-145.8739, 868.3813, 231.6979>>		RETURN TRUE BREAK		//155.6800			RETURN TRUE BREAK
		CASE PR_SCENE_T_NAKED_ISLAND	vCreateCoords = <<2789.8450, -1453.7310, 0.5519>>		RETURN TRUE BREAK		//-49.5600			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASECAR_a	vCreateCoords = <<612.6,-932.6,10.6>>					RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b	vCreateCoords = <<-975.9,2891.0,15.7>>					RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE
			#IF NOT USE_TU_CHANGES
			vCreateCoords = <<-3015.0432, 318.9735, 13.7263>>
			#ENDIF
			#IF USE_TU_CHANGES
			CONST_FLOAT fOrigMult	1.0
			CONST_FLOAT fNewMult	2.0
			vCreateCoords = ((fOrigMult*<<-3019.98, 303.17, 15.66>>)+(fNewMult*<<-3031.2,285.2,15.7>>))/(fOrigMult+fNewMult)
			#ENDIF
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER
			#IF NOT USE_TU_CHANGES
			vCreateCoords = <<1307.9390, -1153.0182, 50.5614>>
			#ENDIF
			#IF USE_TU_CHANGES
			vCreateCoords = (<<1356.03225, -1128.68655, 51.5964>>+<<1316.7,-1149.0,51.5>>)/2.0
			#ENDIF
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	vCreateCoords = <<48.1743, -2057.1294, 18.3524>>		RETURN TRUE BREAK		//47.0540			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_a		vCreateCoords = << -1242.68, -1105.15, 7.10 >>			RETURN TRUE BREAK		//120.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_b		vCreateCoords = <<-1667.1479, -974.7168, 6.4790>>		RETURN TRUE BREAK		//171.2530			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTBAR_c		vCreateCoords = <<-301.4778, 6250.8999, 30.5054>>		RETURN TRUE BREAK		//10.2470			RETURN TRUE BREAK
		CASE PR_SCENE_T_YELLATDOORMAN	vCreateCoords = <<-724.2600, -1307.0500, 5.0602>>		RETURN TRUE BREAK		//-32.4880			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b	vCreateCoords = <<-1280.0540, 303.9235, 63.9553>>		RETURN TRUE BREAK		//-29.0930			RETURN TRUE BREAK
		CASE PR_SCENE_T_FIGHTCASINO		vCreateCoords = <<924.1288, 48.0048, 79.7644>>			RETURN TRUE BREAK		//229.6085			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE	vCreateCoords = <<-1273.6899, -1195.0100, 5.0372>>		RETURN TRUE BREAK		//-150.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE	vCreateCoords = <<-888.4500, -853.1100, 19.5602>>		RETURN TRUE BREAK		//-81.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_UNDERPIER		vCreateCoords = <<-1696.1400, -1073.2000, 0.6898>>		RETURN TRUE BREAK		//12.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_KONEIGHBOUR		vCreateCoords = <<-1155.9570, -1521.6860, 4.3519>>		RETURN TRUE BREAK		//-90.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_SCARETRAMP		vCreateCoords = <<-565.3700, -1258.0200, 13.8618>>		RETURN TRUE BREAK		//-171.0000			RETURN TRUE BREAK
		CASE PR_SCENE_T_DRUNKHOWLING	vCreateCoords = <<440.6737, -228.7473, 55.9725>>		RETURN TRUE BREAK		//-11.5018			RETURN TRUE BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING	vCreateCoords = <<118.4869, -1286.4139, 28.2610>>		RETURN TRUE BREAK		//-129.0000		tRoom = "v_strip3"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	vCreateCoords = <<-1159.2729, -1522.5040, 9.6340>>		RETURN TRUE BREAK		//64.6610		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A	vCreateCoords = <<-1145.3680, -1515.5900, 9.5847>>		RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B	vCreateCoords = <<-1145.4370, -1515.6490, 9.5894>>		RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B2	RETURN initialise_player_ped_position_for_scene(PR_SCENE_T_FLOYDSPOON_B, vCreateCoords) BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A2	RETURN initialise_player_ped_position_for_scene(PR_SCENE_T_FLOYDSPOON_A, vCreateCoords) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A	vCreateCoords = <<-1158.1331, -1521.3940, 9.6327>>		RETURN TRUE BREAK		//34.6610		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0	RETURN initialise_player_ped_position_for_scene(PR_SCENE_T_FLOYDCRYING_E3, vCreateCoords) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E1	RETURN initialise_player_ped_position_for_scene(PR_SCENE_T_FLOYDCRYING_E3, vCreateCoords) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E2	RETURN initialise_player_ped_position_for_scene(PR_SCENE_T_FLOYDCRYING_E3, vCreateCoords) BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E3	vCreateCoords = <<-1157.8030, -1521.3340, 9.6327>>		RETURN TRUE BREAK		//39.0000		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_BEAR		vCreateCoords = <<-1146.1270, -1515.5250, 9.6346>>		RETURN TRUE BREAK		//116.7420		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYD_DOLL		vCreateCoords = <<-1153.5150, -1518.4351, 9.6346>>		RETURN TRUE BREAK		//100.4600		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE	vCreateCoords = <<-1156.4220, -1519.5610, 10.6327>>		RETURN TRUE BREAK		//102.0000		tRoom = "v_Trevors"	RETURN TRUE BREAK
		CASE PR_SCENE_T6_SMOKECRYSTAL	vCreateCoords = <<1972.4320, 3814.3799, 32.9320>>		RETURN TRUE BREAK
		
//		CASE PR_SCENE_T6_BLOWSHITUP
//			
//			
//			VECTOR vTBlowShitUpOffset
//			FLOAT fTBlowShitUpHead
//			
//			IF PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_blowing_shit_up,
//					vTBlowShitUpOffset, fTBlowShitUpHead)
//				vCreateCoords = vTBlowShitUpOffset + << 1974.6129, 3819.1438, 32.4374 >>//+ <<0,0,1>>
//				//fTBlowShitUpHead + 92.6017
//				tRoom = ""
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_T6_EVENING
//			RETURN initialise_player_ped_position_for_scene(PR_SCENE_M6_EVENING, vCreateCoords) BREAK
		CASE PR_SCENE_T6_METHLAB		vCreateCoords = << 1394.2081, 3602.2839, 37.9419 >>		RETURN TRUE BREAK		//122.5269	tRoom = "v_methlab"	RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING1
			VECTOR vHunt1Offset
			vHunt1Offset = <<0.0055, 7.4990, -1.2748>>	//<<-7.4998, 7.4995, -0.5258>>
		
			vCreateCoords = <<-557.5234, 5619.9341, 42.2955>> + vHunt1Offset
			//GET_HEADING_FROM_VECTOR_2D(-vHunt1Offset.x, -vHunt1Offset.y)
			
			
			/*
			#IF IS_DEBUG_BUILD
			vCreateCoords = <<-557.5179, 5627.4331, 41.0207>>
			VECTOR vHunt1Coord
			vHunt1Coord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING1)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_HUNTING1 vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vHunt1Coord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vHunt1Coord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING2
//			VECTOR vHunt2Offset 
//			vHunt2Offset = <<10.6345, 0.7246, 1.2508>>
//		
//			vCreateCoords = << -1585.1000, 4692.7, 46.4 >> + vHunt2Offset
//			//GET_HEADING_FROM_VECTOR_2D(-vHunt2Offset.x, -vHunt2Offset.y)
			
			vCreateCoords = <<-1574.4655, 4693.4248, 47.6226>>
			/*
			#IF IS_DEBUG_BUILD
			vCreateCoords = <<-1574.4655, 4693.4248, 47.6508>>
			VECTOR vHunt2Coord
			vHunt2Coord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING2)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_HUNTING2 vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vHunt2Coord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vHunt2Coord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_HUNTING3
//			VECTOR vHunt3Offset 
//			vHunt3Offset = <<-3.4271, -13.6787, -1.4107>>
//		
//			vCreateCoords = << -1553.9672, 4603.7944, 20.0372 >> + vHunt3Offset
//			//GET_HEADING_FROM_VECTOR_2D(-vHunt3Offset.x, -vHunt3Offset.y)
			
			vCreateCoords = <<-1557.3943, 4590.1157, 18.6076>>
			/*
			#IF IS_DEBUG_BUILD
			vCreateCoords = <<-1557.3943, 4590.1157, 18.6265>>
			VECTOR vHunt3Coord
			vHunt3Coord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_HUNTING3)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_HUNTING3 vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vHunt3Coord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vHunt3Coord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_TRAF_AIR
			//VECTOR vTrafAirOffset
			//vTrafAirOffset = <<-19.6582, 7.8960, 0.1334>>
			//
			//vCreateCoords = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TRAF_AIR) + vTrafAirOffset
			//GET_HEADING_FROM_VECTOR_2D(-vTrafAirOffset.x, -vTrafAirOffset.y)
			
			vCreateCoords = <<2113.6938, 4792.3525, 40.2104>>
			
			/*
			#IF IS_DEBUG_BUILD
			vCreateCoords = <<2113.6943, 4792.3535, 40.1971>>
			VECTOR vTrafAirCoord
			vTrafAirCoord = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TRAF_AIR)
			
			SAVE_STRING_TO_DEBUG_FILE("PR_SCENE_T6_TRAF_AIR vCreateCoords = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
			SAVE_STRING_TO_DEBUG_FILE(" - ")
			SAVE_VECTOR_TO_DEBUG_FILE(vTrafAirCoord)
			SAVE_STRING_TO_DEBUG_FILE(" = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords - vTrafAirCoord)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
			*/
			
			RETURN TRUE BREAK
		
//		CASE PR_SCENE_T6_DISPOSEBODY_A
//			vCreateCoords = <<787.5558, -2614.8931, 51.8703>>
//			//11.6025
//			RETURN TRUE BREAK
		CASE PR_SCENE_T6_DIGGING
			vCreateCoords = <<2020.2517, 3401.1542, 42.7215>>
			//-87.7215
			RETURN TRUE BREAK
		
		CASE PR_SCENE_T6_FLUSHESFOOT
			vCreateCoords = <<1972.2371, 3817.8601, 33.4287>>
			//-145.0000	tRoom = "v_trailer"
			RETURN TRUE BREAK
		CASE PR_SCENE_T_CN_PIER
			vCreateCoords = <<-275.7407, 6629.9688, 6.4377>>
			RETURN TRUE BREAK
		CASE PR_SCENE_T6_LAKE
			vCreateCoords = <<1838.1, 4381.3, 19.6+5>>
			//GET_RANDOM_FLOAT_IN_RANGE(-180, 180)
			RETURN TRUE BREAK
		CASE PR_SCENE_T_FLYING_PLANE
			vCreateCoords = << 1126.4146, 2035.1354, 243.9449 >>
			//327.7746
			RETURN TRUE BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for player scene position: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	vCreateCoords	= <<0,0,0>>
	fCreateHead	= 0
	tRoom = tRoom
	RETURN FALSE
ENDFUNC


PROC Initialise_Player_Scene_Global_Variables_On_SP_Startup()
	
	PED_REQUEST_SCENE_ENUM eScene
	VECTOR vCreateCoords
//	FLOAT fCreateHead
//	TEXT_LABEL_31 tRoom
	
	REPEAT NUM_OF_PED_REQUEST_SCENES eScene
		IF initialise_player_ped_position_for_scene(eScene, vCreateCoords)
			g_sPedSceneData[eScene].vCreateCoords = vCreateCoords
		ENDIF
	ENDREPEAT
	
	INT iPed
	REPEAT NUM_OF_PLAYABLE_PEDS iPed
		g_ePlayerLastVehGen[iPed] = VEHGEN_NONE
	ENDREPEAT
	
	
	
	
	enumFamilyMember eFamilyMember
	IF NOT g_savedGlobals.sFamilyData.bInitialisedPreviousEvents
		REPEAT MAX_FAMILY_MEMBER eFamilyMember
			g_savedGlobals.sFamilyData.ePreviousFamilyEvent[eFamilyMember] = MAX_FAMILY_EVENTS
		ENDREPEAT
		g_savedGlobals.sFamilyData.bInitialisedPreviousEvents = TRUE
	ENDIF
	
	REPEAT MAX_FAMILY_MEMBER eFamilyMember
		g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
	ENDREPEAT
	
ENDPROC
