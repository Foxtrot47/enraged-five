// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 04/10/2010		 		 ___
//-	commands headers	-//

//- script headers	-//

//-	public headers	-//
USING "cutscene_public.sch"
USING "rc_helper_functions.sch"
USING "friendActivity_public.sch"

//-	private headers	-//
USING "friendActivity_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
#ENDIF

///private cutscene header for friend activity scripts
///    sam.hackett@rockstarleeds.com
///

//-------------------------------------------------------------------------------------------------------------------------------------------
//	VARIABLES
//-------------------------------------------------------------------------------------------------------------------------------------------

// Constants
//CONST_INT		CONST_iCutsceneStartPanDuration		5000//5000//DEFAULT_GOD_TEXT_TIME
CONST_INT		CONST_iCutsceneTimeshiftDuration	2500
CONST_INT		CONST_iCutsceneEndPanDuration		5000//5000//DEFAULT_GOD_TEXT_TIME
CONST_INT		CONST_iCutsceneSkipDuration			500

// Vars
CAMERA_INDEX	hBarCams[2]
BOOL bAddedDrunkTime = FALSE

structTimelapse sTimelapse

INT iTOD_DestHour
INT iTOD_DestMin
INT iTOD_SkipTotalHours

//-------------------------------------------------------------------------------------------------------------------------------------------
//	UTILS
//-------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL RC_IS_CUTSCENE_OK_TO_START_NO_DIALOGUE_WAIT(FLOAT stopping_distance = 10.0)

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), stopping_distance, 1)
			RETURN FALSE
		ENDIF
		IF NOT IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			RETURN FALSE
		ENDIF
	ELSE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	ENDIF

	RETURN TRUE
ENDFUNC

PROC Private_GetActivityScenePedPosBoundingBox(structFActivityScene& scene, VECTOR& vMin, VECTOR& vMax)
	
	vMin = scene.vPedPPos[0]
	vMax = scene.vPedPPos[0]
	
	INT i
	REPEAT 9 i
		VECTOR vPos
		IF i = 0	vPos = scene.vPedPPos[0]
		ELIF i = 1	vPos = scene.vPedPPos[1]
		ELIF i = 2	vPos = scene.vPedPPos[2]
		ELIF i = 3	vPos = scene.vPedAPos[0]
		ELIF i = 4	vPos = scene.vPedAPos[1]
		ELIF i = 5	vPos = scene.vPedAPos[2]
		ELIF i = 6	vPos = scene.vPedBPos[0]
		ELIF i = 7	vPos = scene.vPedBPos[1]
		ELIF i = 8	vPos = scene.vPedBPos[2]
		ENDIF

		IF scene.bIsCinema = FALSE OR ((i % 3) <> 2)
			IF vPos.x < vMin.x			vMin.x = vPos.x			ENDIF
			IF vPos.y < vMin.y			vMin.y = vPos.y			ENDIF
			IF vPos.z < vMin.z			vMin.z = vPos.z			ENDIF

			IF vPos.x > vMax.x			vMax.x = vPos.x			ENDIF
			IF vPos.y > vMax.y			vMax.y = vPos.y			ENDIF
			IF vPos.z > vMax.z			vMax.z = vPos.z			ENDIF
		ENDIF
	ENDREPEAT
	
	vMin -= << 0.5, 0.5, 2.5 >>
	vMax += << 0.5, 0.5, 2.5 >>

ENDPROC

PROC Private_SetCinemaCatchupCam(enumActivityLocation eLoc)
	
	// Set timeshift cam
	CAMERA_INDEX hNewCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	IF DOES_CAM_EXIST(hNewCam)
		SWITCH eLoc
			CASE ALOC_cinema_downtown
				SET_CAM_COORD(hNewCam,	<<397.2, -716.2, 29.9>>)
				SET_CAM_ROT(hNewCam,	<<-4.0, 0.0, -9.3>>)
				SET_CAM_FOV(hNewCam,	50.0)
			BREAK
			CASE ALOC_cinema_morningwood
				SET_CAM_COORD(hNewCam,	<<-1411.5, -200.6, 47.8>>)
				SET_CAM_ROT(hNewCam,	<<-3.8, 0.0, 19.7>>)
				SET_CAM_FOV(hNewCam,	50.0)
			BREAK
			CASE ALOC_cinema_vinewood
				SET_CAM_COORD(hNewCam,	<<298.9, 200.8, 105.0>>)
				SET_CAM_ROT(hNewCam,	<<-7.6, 0.0, 161.6>>)
				SET_CAM_FOV(hNewCam,	50.0)
			BREAK
			DEFAULT
				EXIT
			BREAK
		ENDSWITCH

		IF DOES_CAM_EXIST(hBarCams[0])
			DESTROY_CAM(hBarCams[0])
		ENDIF
		IF DOES_CAM_EXIST(hBarCams[1])
			DESTROY_CAM(hBarCams[1])
		ENDIF

		SET_CAM_ACTIVE(hNewCam, TRUE)
		hBarCams[0] = hNewCam
	ENDIF
ENDPROC

PROC Private_ChargePedForBar(PED_INDEX hPed, enumActivityLocation eLoc, INT iDebitAmount)

	BANK_ACCOUNT_ACTION_SOURCE_BAAC eDebitor = BAAC_UNLOGGED_SMALL_ACTION

	SWITCH eLoc
		CASE ALOC_bar_bahamas		eDebitor = BAAC_BAHAMAMAMAS			BREAK
		CASE ALOC_bar_baybar		eDebitor = BAAC_BAY_BAR				BREAK
		CASE ALOC_bar_biker			eDebitor = BAAC_BIKER_BAR			BREAK
		CASE ALOC_bar_downtown		eDebitor = BAAC_SHENANIGANS_BAR		BREAK
		CASE ALOC_bar_himen			eDebitor = BAAC_HIMEN_BAR			BREAK
		CASE ALOC_bar_mojitos		eDebitor = BAAC_MOJITOS_BAR			BREAK
		CASE ALOC_bar_singletons	eDebitor = BAAC_SINGLETONS_BAR		BREAK
	ENDSWITCH
	
	enumCharacterList eChar = Private_GetCharFromPed(hPed)
	
	IF eChar = CHAR_MICHAEL
	OR eChar = CHAR_FRANKLIN
	OR eChar = CHAR_TREVOR

		INT iCash = GET_TOTAL_CASH(eChar)
		IF iCash > iDebitAmount
			DEBIT_BANK_ACCOUNT(eChar, eDebitor, iDebitAmount)
		ELIF iCash > 0
			DEBIT_BANK_ACCOUNT(eChar, eDebitor, iCash)
		ENDIF

	ENDIF

ENDPROC

PROC Private_UpdateStockExchange(enumActivityLocation eLoc)
	IF eLoc = ALOC_bar_bahamas
	OR eLoc = ALOC_bar_himen
	OR eLoc = ALOC_bar_singletons
		BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_PUBCLUB) //call on taking friends to a club
	ELSE
		BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_FRNPUB) //call on taking friends to a pub
	ENDIF
ENDPROC


//-------------------------------------------------------------------------------------------------------------------------------------------
//	BAR SCENE
//-------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL Private_ProcessActivityScene(enumActivityLocation eLoc, structFActivityScene& scene, INT& iStage, PED_INDEX hFriendA, PED_INDEX hFriendB, structPedsForConversation& convPeds)
	
	//-- Fade out if skip is pressed --
	CPRINTLN(DEBUG_FRIENDS, "Private_ProcessActivityScene() A - ", iStage)
	
	IF scene.bIsCinema = FALSE

		IF iStage > 0
		AND iStage < 4
			IF IS_SCREEN_FADED_OUT()
				ADD_TO_CLOCK_TIME(iTOD_SkipTotalHours, 0, 0)

				iStage = 98
				RETURN FALSE
			
			ELIF NOT IS_SCREEN_FADING_IN()
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					DO_SCREEN_FADE_OUT(CONST_iCutsceneSkipDuration)
				ENDIF
			ENDIF
		ENDIF
		
		IF iStage < 3
			REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
		ENDIF
	
	ENDIF
	
//	IF iStage >= 0
//	AND iStage < 100
//		SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
//	ENDIF

	
	CPRINTLN(DEBUG_FRIENDS, "Private_ProcessActivityScene() B - ", iStage)
	
	// Disable player pressing PS4 touchpad during cutscene (for some reason that can cause problem during this scene)
	IF iStage != -1
		DISABLE_CAMERA_VIEW_MODE_CYCLE(PLAYER_ID())
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF
	
	//------------------------------------------------------------------------
	IF iStage = 0
		IF RC_IS_CUTSCENE_OK_TO_START_NO_DIALOGUE_WAIT(5.0)
			RC_START_CUTSCENE_MODE(scene.vPedPPos[0], TRUE, TRUE, FALSE, FALSE, FALSE)
			bAddedDrunkTime = FALSE
			
			// Clear larger area of vehicles (for camera)
			CLEAR_ANGLED_AREA_OF_VEHICLES(scene.vClearA, scene.vClearB, scene.fClearW, TRUE)
			SET_ROADS_IN_ANGLED_AREA(scene.vClearA, scene.vClearB, scene.fClearW, FALSE, FALSE)

			// Clear smaller area of peds (for walking)
			VECTOR vPedPosMin, vPedPosMax
			Private_GetActivityScenePedPosBoundingBox(scene, vPedPosMin, vPedPosMax)
			SET_PED_PATHS_IN_AREA(vPedPosMin, vPedPosMax, FALSE)
			CLEAR_AREA((vPedPosMin+vPedPosMax)*0.5, VDIST(vPedPosMax, vPedPosMin)*0.5, TRUE)

			SEQUENCE_INDEX seq
			SETTIMERA(0)

			INT iDebitAmount = GET_RANDOM_INT_IN_RANGE(25, 35)		
			iTOD_SkipTotalHours = GET_RANDOM_INT_IN_RANGE(2, 4)
				
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// Set car position
				Private_RespotVehicleForActivity(GET_PLAYERS_LAST_VEHICLE(), eLoc)
				
				//-- Set ped positions + tasks
				IF IS_PED_UNINJURED(hFriendA)
					// Set player pos + tasks
					SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.vPedPPos[0])
					SET_ENTITY_FACING(PLAYER_PED_ID(), scene.vPedPPos[1])
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)

					OPEN_SEQUENCE_TASK(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, scene.vPedPPos[1], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_ACCURATE_WALKRUN_START)
						IF scene.bIsCinema AND NOT IS_PED_DRUNK(PLAYER_PED_ID())
							TASK_TURN_PED_TO_FACE_ENTITY(null, hFriendA)
						ENDIF
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
					CLEAR_SEQUENCE_TASK(seq)

					// Set friend A pos + tasks
					SET_ENTITY_COORDS(hFriendA, scene.vPedAPos[0])
					SET_ENTITY_FACING(hFriendA, scene.vPedAPos[1])
					CLEAR_PED_TASKS_IMMEDIATELY(hFriendA)
					SET_PED_STEALTH_MOVEMENT(hFriendA, FALSE)
					SET_CURRENT_PED_WEAPON(hFriendA, WEAPONTYPE_UNARMED, TRUE)
					STOP_PED_SPEAKING(hFriendA, TRUE)

					OPEN_SEQUENCE_TASK(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, scene.vPedAPos[1], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						IF scene.bIsCinema AND NOT IS_PED_DRUNK(hFriendA)
							TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
						ENDIF
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(hFriendA, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				
				IF IS_PED_UNINJURED(hFriendB)
					SET_ENTITY_COORDS(hFriendB, scene.vPedBPos[0])
					SET_ENTITY_FACING(hFriendB, scene.vPedBPos[1])
					CLEAR_PED_TASKS_IMMEDIATELY(hFriendB)
					SET_PED_STEALTH_MOVEMENT(hFriendB, FALSE)
					SET_CURRENT_PED_WEAPON(hFriendB, WEAPONTYPE_UNARMED, TRUE)
					STOP_PED_SPEAKING(hFriendB, TRUE)
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, scene.vPedBPos[1], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						IF scene.bIsCinema AND NOT IS_PED_DRUNK(hFriendB)
							TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
						ENDIF
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(hFriendB, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				
				// Charge player/friends
				IF scene.bIsCinema = FALSE
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						Private_ChargePedForBar(PLAYER_PED_ID(), eLoc, iDebitAmount)
					ENDIF
					
					IF NOT IS_PED_INJURED(hFriendA)
						Private_ChargePedForBar(hFriendA, eLoc, iDebitAmount)
					ENDIF
					
					IF NOT IS_PED_INJURED(hFriendB)
						Private_ChargePedForBar(hFriendB, eLoc, iDebitAmount)
					ENDIF
					Private_UpdateStockExchange(eLoc)
				ENDIF

				//-- Start first cam pan
				hBarCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				SET_CAM_COORD(hBarCams[0], scene.mCamPanA[0].vPos)
				SET_CAM_ROT(hBarCams[0], scene.mCamPanA[0].vRot)
				SET_CAM_FOV(hBarCams[0], scene.fCamPanAFov)

				hBarCams[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				SET_CAM_COORD(hBarCams[1], scene.mCamPanA[1].vPos)
				SET_CAM_ROT(hBarCams[1], scene.mCamPanA[1].vRot)
				SET_CAM_FOV(hBarCams[1], scene.fCamPanAFov)
				
				// Start camera pan
				FLOAT fPanADuration = scene.fPanADurationNormal
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_DRUNK(PLAYER_PED_ID())
					fPanADuration = scene.fPanADurationDrunk
				ENDIF
				
				SET_CAM_ACTIVE_WITH_INTERP(hBarCams[1], hBarCams[0], FLOOR(fPanADuration * 1000.0))
//				SHAKE_CAM(hBarCams[0], "HAND_SHAKE", 1.0)
//				SHAKE_CAM(hBarCams[1], "HAND_SHAKE", 1.0)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF NOT IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF

			iStage++
		ENDIF
	
	ELIF iStage = 1
		
		IF scene.bIsCinema
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(hFriendA)

			IF TIMERA() > 1000
				TEXT_LABEL tBlock, tRoot
				enumCharacterList ePlayerChar, eFriendChar
				ePlayerChar = GET_CURRENT_PLAYER_PED_ENUM()
				eFriendChar = Private_GetCharFromPed(hFriendA)
		
				// Try to do special Michael/Franklin Meltdown dialogue
				IF g_ePreviousActivityResult = AR_playerDraw
				AND ePlayerChar = CHAR_FRANKLIN
				AND eFriendChar = CHAR_MICHAEL
				AND NOT Is_Ped_Drunk(PLAYER_PED_ID())
				AND Private_GetFriendActivityPhrase(ePlayerChar, eFriendChar, FAP_COMMENT_MELTDOWN, tBlock, tRoot)
					
					IF TIMERA() > FLOOR(scene.fPanADurationNormal * 1000.0)
					OR CREATE_CONVERSATION(convPeds, tBlock, tRoot, CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
						iStage++
					ENDIF
						
				// Otherwise do normal dialogue
				ELSE
					IF g_ePreviousActivityResult = AR_playerDraw
						g_ePreviousActivityResult = AR_playerWon
					ENDIF
					IF NOT Private_GetFriendActivityResult(ePlayerChar, eFriendChar, ALOC_cinema_downtown, g_ePreviousActivityResult, Is_Ped_Drunk(PLAYER_PED_ID()), tBlock, tRoot)
					OR CREATE_CONVERSATION(convPeds, tBlock, tRoot, CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
					OR TIMERA() > FLOOR(scene.fPanADurationNormal * 1000.0)
						iStage++
					ENDIF
				ENDIF
			ENDIF

		ELSE
			iStage++
		ENDIF
	
	ELIF iStage = 2
		// Wait for a few seconds (or for skip button)
		FLOAT fPanADuration = scene.fPanADurationNormal
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_DRUNK(PLAYER_PED_ID())
			fPanADuration = scene.fPanADurationDrunk
		ENDIF

		IF TIMERA() > FLOOR(fPanADuration * 1000.0)
			IF scene.bIsCinema
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					iStage = 50
				ELSE
					Private_SetCinemaCatchupCam(eLoc)
					iStage = 100
				ENDIF
			ELSE
				IF DOES_CAM_EXIST(hBarCams[0])
					DESTROY_CAM(hBarCams[0])
				ENDIF
				
				// Set timeshift cam
				hBarCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				SET_CAM_COORD(hBarCams[0], scene.mCamTime.vPos)
				SET_CAM_ROT(hBarCams[0], scene.mCamTime.vRot)
				SET_CAM_FOV(hBarCams[0], scene.fCamTimeFov)
	//			SHAKE_CAM(hBarCams[0], "HAND_SHAKE", 1.0)
				SET_CAM_ACTIVE(hBarCams[0], TRUE)
				
				// Clear dialogue/prints
				STOP_SCRIPTED_CONVERSATION(FALSE)
				CLEAR_PRINTS()

				// Make peds drunk
				INT iDrunkTime = 55
				IF (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND Private_GetCharFromPed(PLAYER_PED_ID()) = CHAR_TREVOR)
				OR (NOT IS_PED_INJURED(hFriendA) AND Private_GetCharFromPed(hFriendA) = CHAR_TREVOR)
				OR (NOT IS_PED_INJURED(hFriendB) AND Private_GetCharFromPed(hFriendB) = CHAR_TREVOR)
					iDrunkTime = 75
				ENDIF
				iDrunkTime += (CONST_iCutsceneTimeshiftDuration / 1000)

				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					IF NOT IS_PED_DRUNK(PLAYER_PED_ID())
						MAKE_PED_DRUNK(PLAYER_PED_ID(), iDrunkTime*1000)
					ELSE
						EXTEND_OVERALL_DRUNK_TIME(PLAYER_PED_ID(), iDrunkTime*1000)
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(hFriendA)
					CLEAR_PED_TASKS_IMMEDIATELY(hFriendA)
					IF NOT IS_PED_DRUNK(hFriendA)
						MAKE_PED_DRUNK(hFriendA, iDrunkTime*1000)
					ELSE
						EXTEND_OVERALL_DRUNK_TIME(hFriendA, iDrunkTime*1000)
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(hFriendB)
					CLEAR_PED_TASKS_IMMEDIATELY(hFriendB)
					IF NOT IS_PED_DRUNK(hFriendB)
						MAKE_PED_DRUNK(hFriendB, iDrunkTime*1000)
					ELSE
						EXTEND_OVERALL_DRUNK_TIME(hFriendB, iDrunkTime*1000)
					ENDIF
				ENDIF
				bAddedDrunkTime = TRUE

				SETTIMERA(0)
			
				iStage++
			ENDIF
		ENDIF
	
	ELIF iStage = 3
		IF REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
		    sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
		    
		    ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, scene.mCamTime.vPos, scene.mCamTime.vRot, CONST_iCutsceneTimeshiftDuration)
		    ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, scene.mCamTime.vPos, scene.mCamTime.vRot, CONST_iCutsceneTimeshiftDuration)
		    SET_CAM_FOV(sTimelapse.splineCamera, scene.fCamTimeFov)
		    SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
		    
			sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
			sTimelapse.currentTimeOfDay = GET_CURRENT_TIMEOFDAY()
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
			
			iTOD_DestHour = (GET_CLOCK_HOURS() + iTOD_SkipTotalHours) % 24
			iTOD_DestMin = GET_CLOCK_MINUTES()

			iStage++
		ENDIF

	ELIF iStage = 4                
		IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iTOD_DestHour, iTOD_DestMin, "", "", sTimelapse)
        	iStage++
       	ENDIF
		
	ELIF iStage = 5
		SEQUENCE_INDEX seq
		SETTIMERA(0)

		//-- Set ped positions + tasks
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			// Set player pos + tasks
			SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.vPedPPos[1])
			SET_ENTITY_FACING(PLAYER_PED_ID(), scene.vPedPPos[2])
			
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null, scene.vPedPPos[2], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
//				TASK_TURN_PED_TO_FACE_ENTITY(null, hFriendA)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
			CLEAR_SEQUENCE_TASK(seq)

			// Walk out of bar
			IF NOT IS_PED_INJURED(hFriendA)
				SET_ENTITY_COORDS(hFriendA, scene.vPedAPos[1])
				SET_ENTITY_FACING(hFriendA, scene.vPedAPos[2])

				IF IS_PED_GROUP_MEMBER(hFriendA, PLAYER_GROUP_ID())
					REMOVE_PED_FROM_GROUP(hFriendA)
				ENDIF

				OPEN_SEQUENCE_TASK(seq)
					TASK_PAUSE(null, 250)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, scene.vPedAPos[2], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
//					TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(hFriendA, seq)
				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
			
			IF IS_PED_UNINJURED(hFriendB)
				SET_ENTITY_COORDS(hFriendB, scene.vPedBPos[1])
				SET_ENTITY_FACING(hFriendB, scene.vPedBPos[2])
				
				IF IS_PED_GROUP_MEMBER(hFriendB, PLAYER_GROUP_ID())
					REMOVE_PED_FROM_GROUP(hFriendB)
				ENDIF

				OPEN_SEQUENCE_TASK(seq)
					TASK_PAUSE(null, 1200)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, scene.vPedBPos[2], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
//					TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(hFriendB, seq)
				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
			
			//-- Start end cam pan
			IF DOES_CAM_EXIST(hBarCams[0])
				DESTROY_CAM(hBarCams[0])
			ENDIF
			IF DOES_CAM_EXIST(hBarCams[1])
				DESTROY_CAM(hBarCams[1])
			ENDIF
			
//			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, DEFAULT, TRUE, FALSE)

			hBarCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			SET_CAM_COORD(hBarCams[0], scene.mCamPanB[0].vPos)
			SET_CAM_ROT(hBarCams[0], scene.mCamPanB[0].vRot)
			SET_CAM_FOV(hBarCams[0], scene.fCamPanBFov)

			hBarCams[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			SET_CAM_COORD(hBarCams[1], scene.mCamPanB[1].vPos)
			SET_CAM_ROT(hBarCams[1], scene.mCamPanB[1].vRot)
			SET_CAM_FOV(hBarCams[1], scene.fCamPanBFov)
			
			// Start camera pan
//			SHAKE_CAM(hBarCams[0], "HAND_SHAKE", 1.0)
//			SHAKE_CAM(hBarCams[1], "HAND_SHAKE", 1.0)
			SET_CAM_ACTIVE_WITH_INTERP(hBarCams[1], hBarCams[0], CONST_iCutsceneEndPanDuration)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		
		IF eLoc = ALOC_bar_biker
			CLEAR_AREA_OF_PEDS(scene.vPedPPos[0], 300.0)
			CLEAR_AREA_OF_PROJECTILES(scene.vPedPPos[0], 300.0)
		ENDIF
		
		iStage++
	
	ELIF iStage = 6
		// Wait for a few seconds (or for skip button)
		IF TIMERA() > CONST_iCutsceneEndPanDuration

			// Set drunk cam
			INT iDrunkTime = 55
			IF (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND Private_GetCharFromPed(PLAYER_PED_ID()) = CHAR_TREVOR)
			OR (NOT IS_PED_INJURED(hFriendA) AND Private_GetCharFromPed(hFriendA) = CHAR_TREVOR)
			OR (NOT IS_PED_INJURED(hFriendB) AND Private_GetCharFromPed(hFriendB) = CHAR_TREVOR)
				iDrunkTime = 75
			ENDIF
			Activate_Drunk_Camera(iDrunkTime*1000)
			g_drunkCameraTimeCycleModifier = 0.25
			g_drunkCameraTimeCycleName = "DRUNK"

			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				iStage = 50
			ELSE
				// Execute extra catchup cam if included
				IF scene.bExtraCatchupCam
					IF DOES_CAM_EXIST(hBarCams[1])
						DESTROY_CAM(hBarCams[1])
					ENDIF
					hBarCams[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)

					SET_CAM_COORD(hBarCams[1], scene.mCamCatch.vPos)
					SET_CAM_ROT(hBarCams[1], scene.mCamCatch.vRot)
					SET_CAM_FOV(hBarCams[1], scene.fCamCatchFov)
					SET_CAM_ACTIVE(hBarCams[1], TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF

				iStage = 100
			ENDIF

		ENDIF
		
	
	//-- 1st person end transition
	ELIF iStage = 50
		ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
		PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
		SETTIMERA(0)
		iStage++
	
	ELIF iStage = 51
		IF TIMERA() > 300
			iStage = 100
		ENDIF	
	
	//-- Skip scene
	
	ELIF iStage = 98
	
		// Set camera
		IF DOES_CAM_EXIST(hBarCams[0])
			DESTROY_CAM(hBarCams[0])
		ENDIF
		IF DOES_CAM_EXIST(hBarCams[1])
			DESTROY_CAM(hBarCams[1])
		ENDIF

		hBarCams[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		IF scene.bExtraCatchupCam = FALSE
			SET_CAM_COORD(hBarCams[1], scene.mCamPanB[1].vPos)
			SET_CAM_ROT(hBarCams[1], scene.mCamPanB[1].vRot)
			SET_CAM_FOV(hBarCams[1], scene.fCamPanBFov)
		ELSE
			SET_CAM_COORD(hBarCams[1], scene.mCamCatch.vPos)
			SET_CAM_ROT(hBarCams[1], scene.mCamCatch.vRot)
			SET_CAM_FOV(hBarCams[1], scene.fCamCatchFov)
		ENDIF
		
		SET_CAM_ACTIVE(hBarCams[1], TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)

		//-- Set ped positions to end of scene
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.vPedPPos[2])
			SET_ENTITY_FACING(PLAYER_PED_ID(), scene.vPedPPos[2] + (scene.vPedPPos[2] - scene.vPedPPos[1]))
		ENDIF
			
		// Set friend A pos
		IF NOT IS_PED_INJURED(hFriendA)
			SET_ENTITY_COORDS(hFriendA, scene.vPedAPos[2])
			SET_ENTITY_FACING(hFriendA, scene.vPedAPos[2] + (scene.vPedAPos[2] - scene.vPedAPos[1]))

			IF IS_PED_GROUP_MEMBER(hFriendA, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(hFriendA)
			ENDIF
		ENDIF
		
		// Set friend B pos
		IF IS_PED_UNINJURED(hFriendB)
			SET_ENTITY_COORDS(hFriendB, scene.vPedBPos[2])
			SET_ENTITY_FACING(hFriendB, scene.vPedBPos[2] + (scene.vPedBPos[2] - scene.vPedBPos[1]))
			
			IF IS_PED_GROUP_MEMBER(hFriendB, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(hFriendB)
			ENDIF
		ENDIF
		
		// Make peds drunk
		INT iDrunkTime = 55
		IF (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND Private_GetCharFromPed(PLAYER_PED_ID()) = CHAR_TREVOR)
		OR (NOT IS_PED_INJURED(hFriendA) AND Private_GetCharFromPed(hFriendA) = CHAR_TREVOR)
		OR (NOT IS_PED_INJURED(hFriendB) AND Private_GetCharFromPed(hFriendB) = CHAR_TREVOR)
			iDrunkTime = 75
		ENDIF
		Activate_Drunk_Camera(iDrunkTime*1000)
		g_drunkCameraTimeCycleModifier = 0.25
		g_drunkCameraTimeCycleName = "DRUNK"
		iDrunkTime += 1

		IF bAddedDrunkTime = FALSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				IF NOT IS_PED_DRUNK(PLAYER_PED_ID())
					MAKE_PED_DRUNK(PLAYER_PED_ID(), iDrunkTime*1000)
				ELSE
					EXTEND_OVERALL_DRUNK_TIME(PLAYER_PED_ID(), iDrunkTime*1000)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(hFriendA)
				CLEAR_PED_TASKS_IMMEDIATELY(hFriendA)
				IF NOT IS_PED_DRUNK(hFriendA)
					MAKE_PED_DRUNK(hFriendA, iDrunkTime*1000)
				ELSE
					EXTEND_OVERALL_DRUNK_TIME(hFriendA, iDrunkTime*1000)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(hFriendB)
				CLEAR_PED_TASKS_IMMEDIATELY(hFriendB)
				IF NOT IS_PED_DRUNK(hFriendA)
					MAKE_PED_DRUNK(hFriendB, iDrunkTime*1000)
				ELSE
					EXTEND_OVERALL_DRUNK_TIME(hFriendB, iDrunkTime*1000)
				ENDIF
			ENDIF
			bAddedDrunkTime = TRUE
		ENDIF
		
		SETTIMERA(0)
		iStage++
	
	ELIF iStage = 99
	
		IF TIMERA() > 2000
			// Fade in
			DO_SCREEN_FADE_IN(1000)
			iStage++
		ENDIF	
	

	//-- Fade out and re-spot
	
	ELIF iStage = 100
		// Destroy cameras (if in first person, just cut back to gameplay cam)
		IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ELSE
			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
		ENDIF
		
		INT iCamera
		REPEAT COUNT_OF(hBarCams) iCamera
			IF DOES_CAM_EXIST(hBarCams[iCamera])
				DESTROY_CAM(hBarCams[iCamera])
			ENDIF
		ENDREPEAT

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(hFriendA)
			STOP_PED_SPEAKING(hFriendA, FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(hFriendB)
			STOP_PED_SPEAKING(hFriendB, FALSE)
		ENDIF

		// Unblock ped and road paths
		VECTOR vPedPosMin, vPedPosMax
		Private_GetActivityScenePedPosBoundingBox(scene, vPedPosMin, vPedPosMax)
		SET_PED_PATHS_BACK_TO_ORIGINAL(vPedPosMin, vPedPosMax)
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(scene.vClearA, scene.vClearB, scene.fClearW)

		RC_END_CUTSCENE_MODE(TRUE, FALSE)
		iStage = -1
	ENDIF
	
	IF iStage = -1
		// If peds are ok, put buddy back in players group
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(hFriendA) AND NOT IS_PED_INJURED(hFriendA)
				IF NOT IS_PED_GROUP_MEMBER(hFriendA, PLAYER_GROUP_ID())
					SET_PED_AS_GROUP_MEMBER(hFriendA, PLAYER_GROUP_ID())
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(hFriendA, VS_FRONT_RIGHT)
				ENDIF
			ENDIF

			IF DOES_ENTITY_EXIST(hFriendB) AND NOT IS_PED_INJURED(hFriendB)
				IF NOT IS_PED_GROUP_MEMBER(hFriendB, PLAYER_GROUP_ID())
					SET_PED_AS_GROUP_MEMBER(hFriendB, PLAYER_GROUP_ID())
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(hFriendB, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		
		ADVANCE_FRIEND_TIMERS(TO_FLOAT(iTOD_SkipTotalHours))
		DO_SCREEN_FADE_IN(1000)

		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC


//-------------------------------------------------------------------------------------------------------------------------------------------
//	INTERFACE - BAR
//-------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL PROCESS_BAR_STATE()
	
	structFActivityScene scene
	Private_ALOC_GetActivityScene(g_eCurrentActivityLoc, scene)
	
	IF Private_ProcessActivityScene(g_eCurrentActivityLoc, scene, gActivity.iStateProgress, gActivity.mFriendA.hPed, gActivity.mFriendB.hPed, gActivity.convPedsDefault)
		finishActivity(g_eCurrentActivityLoc, AR_playerWon)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//-------------------------------------------------------------------------------------------------------------------------------------------
//	INTERFACE - CINEMA
//-------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL PROCESS_CINEMA_STATE()
	
	structFActivityScene scene
	Private_ALOC_GetActivityScene(g_ePreviousActivityLoc, scene)
	
	IF Private_ProcessActivityScene(g_ePreviousActivityLoc, scene, gActivity.iStateProgress, gActivity.mFriendA.hPed, gActivity.mFriendB.hPed, gActivity.convPedsDefault)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC END_CINEMA_STATE()

	//-- Return to journey state --
	Private_SetActivityState(ACTIVITY_STATE_Journey)
	
ENDPROC

