//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	respawn_location_private.sch								//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Functionality to setup the respawn locations for hospitals,	//
//							police stations, and savehouses.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_zone.sch"
USING "blip_control_public.sch"
USING "building_control_public.sch"
USING "savegame_public.sch"


/// PURPOSE: Debug procs used for widgets
#IF IS_DEBUG_BUILD
	#if USE_CLF_DLC
	FUNC STRING Get_Savehouse_Respawn_NameCLF(SAVEHOUSE_NAME_ENUM eName)
		SWITCH eName
			CASE SAVEHOUSEclf_MICHAEL_BH		RETURN "SAVEHOUSEclf_MICHAEL_BH"		BREAK
			CASE SAVEHOUSEclf_MICHAEL_CS		RETURN "SAVEHOUSEclf_MICHAEL_CS"		BREAK
			CASE SAVEHOUSEclf_MICHAEL_PRO		RETURN "SAVEHOUSEclf_MICHAEL_PRO"		BREAK
			CASE SAVEHOUSEclf_TREVOR_CS			RETURN "SAVEHOUSEclf_TREVOR_CS"			BREAK
			CASE SAVEHOUSEclf_TREVOR_VB			RETURN "SAVEHOUSEclf_TREVOR_VB"			BREAK
			CASE SAVEHOUSEclf_TREVOR_SC			RETURN "SAVEHOUSEclf_TREVOR_SC"			BREAK
			CASE SAVEHOUSEclf_TREVOR_PRO		RETURN "SAVEHOUSEclf_TREVOR_PRO"		BREAK
			CASE SAVEHOUSEclf_FRANKLIN_SC		RETURN "SAVEHOUSEclf_FRANKLIN_SC"		BREAK
			CASE SAVEHOUSEclf_FRANKLIN_VH		RETURN "SAVEHOUSEclf_FRANKLIN_VH"		BREAK
			CASE SAVEHOUSEclf_FRANKLIN_PRO		RETURN "SAVEHOUSEclf_FRANKLIN_PRO"		BREAK
		ENDSWITCH
		
		RETURN "SAVEHOUSEclf_MISSING!"
	ENDFUNC
	#endif
	
	#if USE_NRM_DLC
	FUNC STRING Get_Savehouse_Respawn_NameNRM(SAVEHOUSE_NAME_ENUM eName)
		SWITCH eName
			CASE SAVEHOUSENRM_BH		RETURN "SAVEHOUSENRM_BH"		BREAK
			CASE SAVEHOUSENRM_CHATEAU	RETURN "SAVEHOUSENRM_CHATEAU"	BREAK
		ENDSWITCH
		
		RETURN "SAVEHOUSENRM_MISSING!"
	ENDFUNC
	#endif
	
	FUNC STRING Get_Savehouse_Respawn_Name(SAVEHOUSE_NAME_ENUM eName)
		
		#if USE_CLF_DLC
			return Get_Savehouse_Respawn_NameCLF(eName)
		#endif
		
		#if USE_NRM_DLC
			return Get_Savehouse_Respawn_NameNRM(eName)
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		
			SWITCH eName
				CASE SAVEHOUSE_MICHAEL_BH		RETURN "SAVEHOUSE_MICHAEL_BH"		BREAK
				CASE SAVEHOUSE_MICHAEL_CS		RETURN "SAVEHOUSE_MICHAEL_CS"		BREAK
				CASE SAVEHOUSE_MICHAEL_PRO		RETURN "SAVEHOUSE_MICHAEL_PRO"		BREAK
				CASE SAVEHOUSE_TREVOR_CS		RETURN "SAVEHOUSE_TREVOR_CS"		BREAK
				CASE SAVEHOUSE_TREVOR_VB		RETURN "SAVEHOUSE_TREVOR_VB"		BREAK
				CASE SAVEHOUSE_TREVOR_SC		RETURN "SAVEHOUSE_TREVOR_SC"		BREAK
				CASE SAVEHOUSE_TREVOR_PRO		RETURN "SAVEHOUSE_TREVOR_PRO"		BREAK
				CASE SAVEHOUSE_FRANKLIN_SC		RETURN "SAVEHOUSE_FRANKLIN_SC"		BREAK
				CASE SAVEHOUSE_FRANKLIN_VH		RETURN "SAVEHOUSE_FRANKLIN_VH"		BREAK
				CASE SAVEHOUSE_FRANKLIN_PRO		RETURN "SAVEHOUSE_FRANKLIN_PRO"		BREAK
			ENDSWITCH
		
		RETURN "SAVEHOUSE_MISSING!"
		#endif
		#endif
		
	ENDFUNC
	
	FUNC STRING Get_Police_Respawn_Name(POLICE_STATION_NAME_ENUM eName)
		SWITCH eName
			CASE POLICE_STATION_RH
				RETURN "Rockford Hills"
			BREAK
			CASE POLICE_STATION_VB
				RETURN "Vespucci Beach"
			BREAK
			CASE POLICE_STATION_DT
				RETURN "Downtown"
			BREAK
			CASE POLICE_STATION_SC
				RETURN "South Central"
			BREAK
			CASE POLICE_STATION_SS
				RETURN "Sandy Shores"
			BREAK
			CASE POLICE_STATION_PB
				RETURN "Paleto Bay"
			BREAK
			CASE POLICE_STATION_HW
				RETURN "Vineood"
			BREAK
		ENDSWITCH
		
		RETURN "Invalid police station name"
	ENDFUNC
	
	FUNC STRING Get_Hospital_Respawn_Name(HOSPITAL_NAME_ENUM eName)
		SWITCH eName
			CASE HOSPITAL_RH
				RETURN "Rockford Hills"
			BREAK
			CASE HOSPITAL_DT
				RETURN "Downtown"
			BREAK
			CASE HOSPITAL_SC
				RETURN "South Central"
			BREAK
			CASE HOSPITAL_SS
				RETURN "Sandy Shores"
			BREAK
			CASE HOSPITAL_PB
				RETURN "Paleto Bay"
			BREAK
		ENDSWITCH
		
		RETURN "Invalid hospital name"
	ENDFUNC
#ENDIF

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SAVEHOUSES
///    

/// PURPOSE: Returns TRUE if the default savehouse respawns have been initialised
FUNC BOOL Has_Savehouse_Respawns_Been_Initialised()
#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sRespawnData.bDefaultSavehouseDataSet
#endif
#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sRespawnData.bDefaultSavehouseDataSet
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	RETURN g_savedGlobals.sRespawnData.bDefaultSavehouseDataSet
#endif	
#endif
ENDFUNC

/// PURPOSE: Returns TRUE if the savehouse is available
FUNC BOOL Is_Savehouse_Respawn_Available(SAVEHOUSE_NAME_ENUM eName)
#if USE_CLF_DLC
	RETURN IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
#endif
#if USE_NRM_DLC
	RETURN IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	RETURN IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
#endif
#endif
ENDFUNC


/// PURPOSE: Reset all the savehouse respawn data back to the defaults
#if USE_CLF_DLC
PROC Clear_Savehouse_Respawn_LocationsCLF()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)-1
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vBlipCoords 	= << 0.0, 0.0, 0.0 >>
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords 	= << 0.0, 0.0, 0.0 >>
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].fSpawnHeading 	= 0.0
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].eBlip 			= STATIC_BLIP_NAME_DUMMY_FINAL
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].index			= NULL
	ENDFOR
ENDPROC  
#endif
#if USE_NRM_DLC
PROC Clear_Savehouse_Respawn_LocationsNRM()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vBlipCoords 	= << 0.0, 0.0, 0.0 >>
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords 	= << 0.0, 0.0, 0.0 >>
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].fSpawnHeading 	= 0.0
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].eBlip 			= STATIC_BLIP_NAME_DUMMY_FINAL
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].index			= NULL
	ENDFOR
ENDPROC
#endif
PROC Clear_Savehouse_Respawn_Locations()
#if USE_CLF_DLC
	Clear_Savehouse_Respawn_LocationsCLF()
	EXIT
#endif
#if USE_NRM_DLC
	Clear_Savehouse_Respawn_LocationsNRM()
	EXIT
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vBlipCoords 	= << 0.0, 0.0, 0.0 >>
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords 	= << 0.0, 0.0, 0.0 >>
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].fSpawnHeading 	= 0.0
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].eBlip 			= STATIC_BLIP_NAME_DUMMY_FINAL
		g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].index			= NULL
	ENDFOR
#endif
#endif	
	
ENDPROC

/// PURPOSE: Make sure all the savehouses have been set up
#if USE_CLF_DLC
PROC Check_Savehouse_Respawn_LocationsCLF()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)-1
		IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.x = 0.0
		AND g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.y = 0.0
		AND g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.z = 0.0
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("\nCheck_Savehouse_Respawn_Locations() - Not all savehouses have been set up")
				PRINTSTRING("\nCheck_Savehouse_Respawn_Locations() - Not all savehouses have been set up")PRINTNL()
			#ENDIF
		ENDIF
	ENDFOR
ENDPROC
#endif
#if USE_NRM_DLC
PROC Check_Savehouse_Respawn_LocationsNRM()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1
		IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.x = 0.0
		AND g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.y = 0.0
		AND g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.z = 0.0
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("\nCheck_Savehouse_Respawn_Locations() - Not all savehouses have been set up")
				PRINTSTRING("\nCheck_Savehouse_Respawn_Locations() - Not all savehouses have been set up")PRINTNL()
			#ENDIF
		ENDIF
	ENDFOR
ENDPROC
#endif

PROC Check_Savehouse_Respawn_Locations()
#if USE_CLF_DLC
	Check_Savehouse_Respawn_LocationsCLF()
	EXIT
#endif
#if USE_NRM_DLC
	Check_Savehouse_Respawn_LocationsNRM()
	EXIT
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1
		IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.x = 0.0
		AND g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.y = 0.0
		AND g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords.z = 0.0
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("\nCheck_Savehouse_Respawn_Locations() - Not all savehouses have been set up")
				PRINTSTRING("\nCheck_Savehouse_Respawn_Locations() - Not all savehouses have been set up")PRINTNL()
			#ENDIF
		ENDIF
	ENDFOR
#endif
#endif
	
ENDPROC

/// PURPOSE: Registers a new savehouse with code and stores a copy of the data for script.
///    
/// PARAMS:
///    vCoords - Location of the savehouse
///    fHeading - Heading when spawning the player at the savehouse
///    sRoomName - The room that the savehouse occupies
///    eName - The enum name that we setup in respawn_location_globals.sch
///    eBlip - The enum blip we use for the savehouse
///    eChar - Which character the savehouse belongs to.
#if USE_CLF_DLC
PROC Setup_Savehouse_Respawn_LocationCLF(VECTOR vBlipCoords, VECTOR vSpawnCoords, FLOAT fSpawnHeading, SAVEHOUSE_NAME_ENUM eName, STATIC_BLIP_NAME_ENUM eBlip, STRING strRoomName, enumCharacterList eChar)

	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("\nSetup_Savehouse_Respawn_Location() - Invalid savehouse enum")
			PRINTSTRING("\nSetup_Savehouse_Respawn_Location() - Invalid savehouse enum")PRINTNL()
		#ENDIF
		EXIT
	ENDIF
	
	MODEL_NAMES ePedModelName = GET_PLAYER_PED_MODELCLF(eChar)
	#IF IS_DEBUG_BUILD
		IF ePedModelName = DUMMY_MODEL_FOR_SCRIPT
			SCRIPT_ASSERT("Setup_Savehouse_Respawn_Location - GET_PLAYER_PED_MODEL() failed to find a ped model. Make sure that Initialise_CharSheet_Global_Variables_On_Startup() is called before Setup_Savehouse_Respawn_Location()")
		ENDIF
	#ENDIF
	
	// Kenneth R.
	// The map areas are not set up for V yet - see the g_eMapAreas enum in commands_zone.sch.
	// I spoke to Graeme about it and he cant seem to find where it gets used. 
	// Advised to just use 0 for now.
	g_sSavehouses[eName].index = REGISTER_SAVE_HOUSE(vSpawnCoords, fSpawnHeading, strRoomName, 0, ePedModelName)
	SET_SAVE_HOUSE(g_sSavehouses[eName].index, TRUE, TRUE)
	
	g_sSavehouses[eName].vBlipCoords 	 = vBlipCoords
	g_sSavehouses[eName].vSpawnCoords 	 = vSpawnCoords
	g_sSavehouses[eName].fSpawnHeading 	 = fSpawnHeading
	g_sSavehouses[eName].eBlip 			 = eBlip
	g_sSavehouses[eName].iChar 			 = ENUM_TO_INT(eChar)
	
	IF eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(eBlip, FALSE)							/* Active 		*/
		//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, FALSE)						/* Flash		*/
		SET_STATIC_BLIP_SHORT_RANGE(eBlip)
		SET_STATIC_BLIP_HIDDEN_IN_MISSION(eBlip, FALSE)						/* Mission		*/
		//SET_STATIC_BLIP_IS_DISCOVERABLE(eBlip, FALSE)						/* Discoverable */
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(eBlip, TRUE, eChar)			/* Character	*/
		SET_STATIC_BLIP_POSITION(eBlip, g_sSavehouses[eName].vBlipCoords)	/* Position		*/
		SET_STATIC_BLIP_CATEGORY(eBlip, STATIC_BLIP_CATEGORY_SAVEHOUSE)		/* Category		*/
		SET_STATIC_BLIP_ICON(eBlip, RADAR_TRACE_SAFEHOUSE)					/* Sprite		*/
	ENDIF
ENDPROC
#endif
#if USE_NRM_DLC
PROC Setup_Savehouse_Respawn_LocationNRM(VECTOR vBlipCoords, VECTOR vSpawnCoords, FLOAT fSpawnHeading, SAVEHOUSE_NAME_ENUM eName, STATIC_BLIP_NAME_ENUM eBlip, STRING strRoomName, enumCharacterList eChar)

	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("\nSetup_Savehouse_Respawn_Location() - Invalid savehouse enum")
			PRINTSTRING("\nSetup_Savehouse_Respawn_Location() - Invalid savehouse enum")PRINTNL()
		#ENDIF
		EXIT
	ENDIF
	
	MODEL_NAMES ePedModelName = GET_PLAYER_PED_MODELNRM(eChar)
	#IF IS_DEBUG_BUILD
		IF ePedModelName = DUMMY_MODEL_FOR_SCRIPT
			SCRIPT_ASSERT("Setup_Savehouse_Respawn_Location - GET_PLAYER_PED_MODEL() failed to find a ped model. Make sure that Initialise_CharSheet_Global_Variables_On_Startup() is called before Setup_Savehouse_Respawn_Location()")
		ENDIF
	#ENDIF
	
	// Kenneth R.
	// The map areas are not set up for V yet - see the g_eMapAreas enum in commands_zone.sch.
	// I spoke to Graeme about it and he cant seem to find where it gets used. 
	// Advised to just use 0 for now.
	g_sSavehouses[eName].index = REGISTER_SAVE_HOUSE(vSpawnCoords, fSpawnHeading, strRoomName, 0, ePedModelName)
	SET_SAVE_HOUSE(g_sSavehouses[eName].index, TRUE, TRUE)
	
	g_sSavehouses[eName].vBlipCoords 	 = vBlipCoords
	g_sSavehouses[eName].vSpawnCoords 	 = vSpawnCoords
	g_sSavehouses[eName].fSpawnHeading 	 = fSpawnHeading
	g_sSavehouses[eName].eBlip 			 = eBlip
	g_sSavehouses[eName].iChar 			 = ENUM_TO_INT(eChar)
	
	IF eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(eBlip, FALSE)							/* Active 		*/
		//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, FALSE)						/* Flash		*/
		SET_STATIC_BLIP_SHORT_RANGE(eBlip)
		SET_STATIC_BLIP_HIDDEN_IN_MISSION(eBlip, FALSE)						/* Mission		*/
		//SET_STATIC_BLIP_IS_DISCOVERABLE(eBlip, FALSE)						/* Discoverable */
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(eBlip, TRUE, eChar)			/* Character	*/
		SET_STATIC_BLIP_POSITION(eBlip, g_sSavehouses[eName].vBlipCoords)	/* Position		*/
		SET_STATIC_BLIP_CATEGORY(eBlip, STATIC_BLIP_CATEGORY_SAVEHOUSE)		/* Category		*/
		SET_STATIC_BLIP_ICON(eBlip, RADAR_TRACE_SAFEHOUSE)					/* Sprite		*/
	ENDIF
ENDPROC
#endif
PROC Setup_Savehouse_Respawn_Location(VECTOR vBlipCoords, VECTOR vSpawnCoords, FLOAT fSpawnHeading, SAVEHOUSE_NAME_ENUM eName, STATIC_BLIP_NAME_ENUM eBlip, STRING strRoomName, enumCharacterList eChar)
#if USE_CLF_DLC
	Setup_Savehouse_Respawn_LocationCLF(vBlipCoords,vSpawnCoords,fSpawnHeading,eName,eBlip,strRoomName,eChar)
	exit
#endif
#if USE_NRM_DLC
	Setup_Savehouse_Respawn_LocationNRM(vBlipCoords,vSpawnCoords,fSpawnHeading,eName,eBlip,strRoomName,eChar)	
	exit
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("\nSetup_Savehouse_Respawn_Location() - Invalid savehouse enum")
			PRINTSTRING("\nSetup_Savehouse_Respawn_Location() - Invalid savehouse enum")PRINTNL()
		#ENDIF
		EXIT
	ENDIF

	MODEL_NAMES ePedModelName = GET_PLAYER_PED_MODEL(eChar)
	#IF IS_DEBUG_BUILD
		IF ePedModelName = DUMMY_MODEL_FOR_SCRIPT
			SCRIPT_ASSERT("Setup_Savehouse_Respawn_Location - GET_PLAYER_PED_MODEL() failed to find a ped model. Make sure that Initialise_CharSheet_Global_Variables_On_Startup() is called before Setup_Savehouse_Respawn_Location()")
		ENDIF
	#ENDIF
	
	// Kenneth R.
	// The map areas are not set up for V yet - see the g_eMapAreas enum in commands_zone.sch.
	// I spoke to Graeme about it and he cant seem to find where it gets used. 
	// Advised to just use 0 for now.
	g_sSavehouses[eName].index = REGISTER_SAVE_HOUSE(vSpawnCoords, fSpawnHeading, strRoomName, 0, ePedModelName)
	SET_SAVE_HOUSE(g_sSavehouses[eName].index, TRUE, TRUE)
	
	g_sSavehouses[eName].vBlipCoords 	 = vBlipCoords
	g_sSavehouses[eName].vSpawnCoords 	 = vSpawnCoords
	g_sSavehouses[eName].fSpawnHeading 	 = fSpawnHeading
	g_sSavehouses[eName].eBlip 			 = eBlip
	g_sSavehouses[eName].iChar 			 = ENUM_TO_INT(eChar)
	
	IF eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(eBlip, FALSE)							/* Active 		*/
		//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, FALSE)						/* Flash		*/
		SET_STATIC_BLIP_SHORT_RANGE(eBlip)
		SET_STATIC_BLIP_HIDDEN_IN_MISSION(eBlip, FALSE)						/* Mission		*/
		//SET_STATIC_BLIP_IS_DISCOVERABLE(eBlip, FALSE)						/* Discoverable */
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(eBlip, TRUE, eChar)			/* Character	*/
		SET_STATIC_BLIP_POSITION(eBlip, g_sSavehouses[eName].vBlipCoords)	/* Position		*/
		SET_STATIC_BLIP_CATEGORY(eBlip, STATIC_BLIP_CATEGORY_SAVEHOUSE)		/* Category		*/
		SET_STATIC_BLIP_ICON(eBlip, RADAR_TRACE_SAFEHOUSE)					/* Sprite		*/
	ENDIF
#endif
#endif	
ENDPROC


/// PURPOSE: Adds/removes the savehouse blip depending on current state
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
#if USE_CLF_DLC
PROC Update_Savehouse_Respawn_BlipCLF(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Invalid savehouse enum.")
		EXIT
	ENDIF
	
	IF g_sSavehouses[eName].eBlip = STATIC_BLIP_NAME_DUMMY_FINAL
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Invalid savehouse static blip.")
		EXIT
	ENDIF
	
	BOOL bDisplayBlip = FALSE
	IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Savehouse available.")
		IF g_sSavehouses[eName].iChar != GET_CURRENT_PLAYER_PED_INT()
			CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Playing as another character. Setting blip active.")
			bDisplayBlip = TRUE
		ELIF NOT IS_PLAYER_IN_SAVEHOUSE(eName)
			CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Not inside the safehouse. Setting blip active.")
			bDisplayBlip = TRUE
#IF IS_DEBUG_BUILD
		ELSE
			CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Valid character and in safehouse. Setting blip inactive.")
#ENDIF
		ENDIF
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Savehouse not available. Setting blip inactive.")
#ENDIF
	ENDIF

	SET_STATIC_BLIP_ACTIVE_STATE(g_sSavehouses[eName].eBlip, bDisplayBlip)
ENDPROC
#endif
#if USE_NRM_DLC
PROC Update_Savehouse_Respawn_BlipNRM(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Invalid savehouse enum.")
		EXIT
	ENDIF
	
	IF g_sSavehouses[eName].eBlip = STATIC_BLIP_NAME_DUMMY_FINAL
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Invalid savehouse static blip.")
		EXIT
	ENDIF
	
	BOOL bDisplayBlip = FALSE

	IF IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Savehouse available.")
		IF g_sSavehouses[eName].iChar != GET_CURRENT_PLAYER_PED_INT()
			CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Playing as another character. Setting blip active.")
			bDisplayBlip = TRUE
		ELIF NOT IS_PLAYER_IN_SAVEHOUSE(eName)
			CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Not inside the safehouse. Setting blip active.")
			bDisplayBlip = TRUE
#IF IS_DEBUG_BUILD
		ELSE
			CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Valid character and in safehouse. Setting blip inactive.")
#ENDIF
		ENDIF
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Savehouse not available. Setting blip inactive.")
#ENDIF
	ENDIF
	
	SET_STATIC_BLIP_ACTIVE_STATE(g_sSavehouses[eName].eBlip, bDisplayBlip)
ENDPROC
#endif


PROC Update_Savehouse_Respawn_Blip(SAVEHOUSE_NAME_ENUM eName)
#if USE_CLF_DLC
	Update_Savehouse_Respawn_BlipCLF( eName)
	exit
#endif
#if USE_NRM_DLC
	Update_Savehouse_Respawn_BlipNRM( eName)
	exit
#endif
	
#if not USE_SP_DLC

	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Invalid savehouse enum.")
		EXIT
	ENDIF
	
	IF g_sSavehouses[eName].eBlip = STATIC_BLIP_NAME_DUMMY_FINAL
		CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Invalid savehouse static blip.")
		EXIT
	ENDIF
	
	BOOL bDisplayBlip = FALSE
	IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
		CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Savehouse available.")
		IF g_sSavehouses[eName].iChar != GET_CURRENT_PLAYER_PED_INT()
			CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Playing as another character. Setting blip active.")
			bDisplayBlip = TRUE
		ELIF NOT IS_PLAYER_IN_SAVEHOUSE(eName)
			CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Not inside the safehouse. Setting blip active.")
			bDisplayBlip = TRUE
#IF IS_DEBUG_BUILD
		ELSE
			CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Valid character and in safehouse. Setting blip inactive.")
#ENDIF
		ENDIF
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Savehouse not available. Setting blip inactive.")
#ENDIF
	ENDIF
	
	// Fix for bug #1234093
	IF eName = SAVEHOUSE_FRANKLIN_SC
		IF Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH)
			CDEBUG2LN(DEBUG_BLIP, "<", Get_Savehouse_Respawn_Name(eName), "> Setting SAVEHOUSE_FRANKLIN_SC inactive as SAVEHOUSE_FRANKLIN_VH is available.")
			bDisplayBlip = FALSE
		ENDIF
	ENDIF
	
	//B* 2185792: Savehouse blip unavailable in Director mode
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
		bDisplayBlip = FALSE
	ENDIF
	
	SET_STATIC_BLIP_ACTIVE_STATE(g_sSavehouses[eName].eBlip, bDisplayBlip)	
#endif
	

ENDPROC

/// PURPOSE: Enables/disabled the savehouse garage depending on current state
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
#if USE_CLF_DLC
PROC Update_Savehouse_Respawn_GarageCLF(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		PRINTSTRING("\nUpdate_Savehouse_Respawn_Garage() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF	
	INT iPed, iGarage
	SAVEHOUSE_NAME_ENUM eGarageSavehouse
	TEXT_LABEL_31 tlGarageName
	BOOL bGarageAvailable
	
	bGarageAvailable = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	REPEAT NUM_OF_PLAYABLE_PEDS iPed
		iGarage = 0
		tlGarageName = ""
		eGarageSavehouse = NUMBER_OF_CLF_SAVEHOUSE
		
		WHILE GET_PLAYER_GARAGE_DATA(INT_TO_ENUM(enumCharacterList, iPed), iGarage, tlGarageName, eGarageSavehouse)
			IF eGarageSavehouse = eName
				IF bGarageAvailable
					PRINTLN("SP GARAGE: Enabling garage, ", tlGarageName)
				ELSE
					PRINTLN("SP GARAGE: Disabling garage, ", tlGarageName)
				ENDIF
				ENABLE_SAVING_IN_GARAGE(GET_HASH_KEY(tlGarageName), bGarageAvailable)
			ENDIF
			
			iGarage++
		ENDWHILE
	ENDREPEAT
ENDPROC
#endif


#if USE_NRM_DLC
PROC Update_Savehouse_Respawn_GarageNRM(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		PRINTSTRING("\nUpdate_Savehouse_Respawn_Garage() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF	
	INT iPed, iGarage
	SAVEHOUSE_NAME_ENUM eGarageSavehouse
	TEXT_LABEL_31 tlGarageName
	BOOL bGarageAvailable
	

	bGarageAvailable = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	REPEAT NUM_OF_PLAYABLE_PEDS iPed
		iGarage = 0
		tlGarageName = ""
		eGarageSavehouse = NUMBER_OF_NRM_SAVEHOUSE
		
		WHILE GET_PLAYER_GARAGE_DATA(INT_TO_ENUM(enumCharacterList, iPed), iGarage, tlGarageName, eGarageSavehouse)
			IF eGarageSavehouse = eName
				IF bGarageAvailable
					PRINTLN("SP GARAGE: Enabling garage, ", tlGarageName)
				ELSE
					PRINTLN("SP GARAGE: Disabling garage, ", tlGarageName)
				ENDIF
				ENABLE_SAVING_IN_GARAGE(GET_HASH_KEY(tlGarageName), bGarageAvailable)
			ENDIF
			
			iGarage++
		ENDWHILE
	ENDREPEAT

	
ENDPROC
#endif


PROC Update_Savehouse_Respawn_Garage(SAVEHOUSE_NAME_ENUM eName, BOOL bCleanGarageBeforeEnabling = FALSE)
#if USE_CLF_DLC
	Update_Savehouse_Respawn_GarageCLF( eName)
	exit
#endif
#if USE_NRM_DLC
	Update_Savehouse_Respawn_GarageNRM( eName)
	exit
#endif
	
#if not USE_CLF_DLC
#if not USE_NRM_DLC

	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		PRINTSTRING("\nUpdate_Savehouse_Respawn_Garage() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF	
	INT iPed, iGarage
	SAVEHOUSE_NAME_ENUM eGarageSavehouse
	TEXT_LABEL_31 tlGarageName
	BOOL bGarageAvailable
	
	bGarageAvailable = IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	
	// Fix for bug #1669328 - If Franklin has moved, allow the south central savehouse garage to work.
	IF eName = SAVEHOUSE_FRANKLIN_SC
	AND IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_FRANKLIN_VH], REPAWN_FLAG_AVAILABLE_BIT)
		PRINTSTRING("\nUpdate_Savehouse_Respawn_Garage() - Forcing Franklins aunts garage to work")PRINTNL()
		bGarageAvailable = TRUE
	ENDIF
	
	REPEAT NUM_OF_PLAYABLE_PEDS iPed
		iGarage = 0
		tlGarageName = ""
		eGarageSavehouse = NUMBER_OF_SAVEHOUSE_LOCATIONS
		
		WHILE GET_PLAYER_GARAGE_DATA(INT_TO_ENUM(enumCharacterList, iPed), iGarage, tlGarageName, eGarageSavehouse)
			IF eGarageSavehouse = eName
				IF bGarageAvailable
					PRINTLN("SP GARAGE: Enabling garage, ", tlGarageName)
					
					IF bCleanGarageBeforeEnabling
						PRINTLN("SP GARAGE: bCleanGarageBeforeEnabling = TRUE, deleting whatever is inside")
						CLEAR_GARAGE(GET_HASH_KEY(tlGarageName))
					ENDIF
				ELSE
					PRINTLN("SP GARAGE: Disabling garage, ", tlGarageName)
				ENDIF
				ENABLE_SAVING_IN_GARAGE(GET_HASH_KEY(tlGarageName), bGarageAvailable)
			ENDIF
			
			iGarage++
		ENDWHILE
	ENDREPEAT
#endif		
#endif
	
ENDPROC


/// PURPOSE: Enables/disabled the savehouse autosave depending on current state of savehouses
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
#if USE_CLF_DLC
PROC Update_Savehouse_Autosave_StateCLF(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		PRINTSTRING("\nUpdate_Savehouse_Autosave_State() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	
	
	SWITCH eName
		// Michaels
		CASE SAVEHOUSEclf_MICHAEL_BH
		CASE SAVEHOUSEclf_MICHAEL_CS
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_MICHAEL_CS)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_MICHAEL_CS].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_MICHAEL_BH].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_MICHAEL_BH), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSEclf_MICHAEL_CS")
				
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_MICHAEL_BH)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_MICHAEL_BH].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_MICHAEL_CS].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_MICHAEL_CS), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSEclf_MICHAEL_BH")
			ENDIF
		BREAK
		
		// Trevors
		CASE SAVEHOUSEclf_TREVOR_CS
		CASE SAVEHOUSEclf_TREVOR_VB
		CASE SAVEHOUSEclf_TREVOR_SC
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_VB)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_VB].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_SC].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_SC), FALSE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_CS].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_CS), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_TREVOR_VB")
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_SC)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_SC].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_VB].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_VB), FALSE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_CS].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_CS), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_TREVOR_SC")
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_CS)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_CS].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_VB].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_VB), FALSE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_TREVOR_SC].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_TREVOR_SC), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_TREVOR_CS")
			ENDIF
		BREAK
		
		// Franklins
		CASE SAVEHOUSEclf_FRANKLIN_SC
		CASE SAVEHOUSEclf_FRANKLIN_VH
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_FRANKLIN_VH)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_FRANKLIN_VH].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_FRANKLIN_SC].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_FRANKLIN_SC), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_FRANKLIN_VH")
				
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSEclf_FRANKLIN_SC)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_FRANKLIN_SC].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSEclf_FRANKLIN_VH].index, Is_Savehouse_Respawn_Available(SAVEHOUSEclf_FRANKLIN_VH), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_FRANKLIN_SC")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#endif


#if USE_NRM_DLC
PROC Update_Savehouse_Autosave_StateNRM(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		PRINTSTRING("\nUpdate_Savehouse_Autosave_State() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
		
	SWITCH eName
		// Michaels
		CASE SAVEHOUSENRM_BH
		CASE SAVEHOUSENRM_CHATEAU
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSENRM_BH)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSENRM_BH].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSENRM_CHATEAU].index, Is_Savehouse_Respawn_Available(SAVEHOUSENRM_CHATEAU), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSENRM_CHATEAU")
				
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSENRM_CHATEAU)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSENRM_CHATEAU].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSENRM_BH].index, Is_Savehouse_Respawn_Available(SAVEHOUSENRM_BH), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSENRM_BH")
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC
#endif


PROC Update_Savehouse_Autosave_State(SAVEHOUSE_NAME_ENUM eName)
#if USE_CLF_DLC
	Update_Savehouse_Autosave_StateCLF(eName)
	exit
#endif
#if USE_NRM_DLC
	Update_Savehouse_Autosave_StateNRM(eName)
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC


	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		PRINTSTRING("\nUpdate_Savehouse_Autosave_State() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	
	
	SWITCH eName
		// Michaels
		CASE SAVEHOUSE_MICHAEL_BH
		CASE SAVEHOUSE_MICHAEL_CS
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_CS)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_MICHAEL_CS].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_MICHAEL_BH].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_BH), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_MICHAEL_CS")
				
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_BH)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_MICHAEL_BH].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_MICHAEL_CS].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_CS), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_MICHAEL_BH")
			ENDIF
		BREAK
		
		// Trevors
		CASE SAVEHOUSE_TREVOR_CS
		CASE SAVEHOUSE_TREVOR_VB
		CASE SAVEHOUSE_TREVOR_SC
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_VB)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_VB].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_SC].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_SC), FALSE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_CS].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_TREVOR_VB")
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_SC)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_SC].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_VB].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_VB), FALSE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_CS].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_TREVOR_SC")
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_CS].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_VB].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_VB), FALSE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_TREVOR_SC].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_SC), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_TREVOR_CS")
			ENDIF
		BREAK
		
		// Franklins
		CASE SAVEHOUSE_FRANKLIN_SC
		CASE SAVEHOUSE_FRANKLIN_VH
			// Set autosave in main savehouse
			IF Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_FRANKLIN_VH].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_FRANKLIN_SC].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_FRANKLIN_VH")
				
			ELIF Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_FRANKLIN_SC].index, TRUE, TRUE)
				SET_SAVE_HOUSE(g_sSavehouses[SAVEHOUSE_FRANKLIN_VH].index, Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH), FALSE)
				PRINTLN("Update_Savehouse_Autosave_State() - Setting default autosave savehouse to SAVEHOUSE_FRANKLIN_SC")
			ENDIF
		BREAK
	ENDSWITCH
#endif
#endif	
ENDPROC


#if USE_CLF_DLC
PROC Update_Savehouse_Door_StatesCLF(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		PRINTSTRING("\nUpdate_Savehouse_Door_States() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF	
	
	SWITCH eName
		// Michaels
		CASE SAVEHOUSEclf_MICHAEL_BH
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_F_L)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_F_L)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_F_L, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_L1)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_L1)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_L2)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_L2)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_R1)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_R1)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_R2)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_R2)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_UNLOCKED)
			
		BREAK
		CASE SAVEHOUSEclf_MICHAEL_CS
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_TRAILER_CS)/32], (ENUM_TO_INT(DOORNAME_T_TRAILER_CS)%32))
			SET_DOOR_STATE(DOORNAME_T_TRAILER_CS, DOORSTATE_UNLOCKED)
		BREAK
		
		// Trevors
		CASE SAVEHOUSEclf_TREVOR_CS
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_TRAILER_CS)/32], (ENUM_TO_INT(DOORNAME_T_TRAILER_CS)%32))
			SET_DOOR_STATE(DOORNAME_T_TRAILER_CS, DOORSTATE_UNLOCKED)
		BREAK
		CASE SAVEHOUSEclf_TREVOR_VB
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_APARTMENT_VB)/32], (ENUM_TO_INT(DOORNAME_T_APARTMENT_VB)%32))
			SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB, DOORSTATE_UNLOCKED)
		BREAK
		CASE SAVEHOUSEclf_TREVOR_SC
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_STRIPCLUB_F)/32], (ENUM_TO_INT(DOORNAME_STRIPCLUB_F)%32))
			SET_DOOR_STATE(DOORNAME_STRIPCLUB_F, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_STRIPCLUB_R)/32], (ENUM_TO_INT(DOORNAME_STRIPCLUB_R)%32))
			SET_DOOR_STATE(DOORNAME_STRIPCLUB_R, DOORSTATE_UNLOCKED)
		BREAK
		
		// Franklins
		CASE SAVEHOUSEclf_FRANKLIN_SC
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_F_HOUSE_SC_F)/32], (ENUM_TO_INT(DOORNAME_F_HOUSE_SC_F)%32))
			SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)/32], (ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)%32))
			SET_DOOR_STATE(DOORNAME_F_HOUSE_VH_F, DOORSTATE_UNLOCKED)
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_VH
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)/32], (ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)%32))
			SET_DOOR_STATE(DOORNAME_F_HOUSE_VH_F, DOORSTATE_UNLOCKED)
		BREAK
	ENDSWITCH
ENDPROC
#endif


#if USE_NRM_DLC
PROC Update_Savehouse_Door_StatesNRM(SAVEHOUSE_NAME_ENUM eName)

	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		PRINTSTRING("\nUpdate_Savehouse_Door_StatesNRM() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF	
	
	SWITCH eName
		// Michaels
		CASE SAVEHOUSENRM_BH
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_F_L)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_F_L)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_F_L, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_L1)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_L1)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_L2)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_L2)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_R1)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_R1)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_R2)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_R2)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_UNLOCKED)
			
		BREAK
		CASE SAVEHOUSENRM_CHATEAU
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_TRAILER_CS)/32], (ENUM_TO_INT(DOORNAME_T_TRAILER_CS)%32))
			SET_DOOR_STATE(DOORNAME_T_TRAILER_CS, DOORSTATE_UNLOCKED)
		BREAK		
	ENDSWITCH
ENDPROC
#endif


PROC Update_Savehouse_Door_States(SAVEHOUSE_NAME_ENUM eName)

#if USE_CLF_DLC
	Update_Savehouse_Door_StatesCLF( eName)
	exit
#endif
#if USE_NRM_DLC
	Update_Savehouse_Door_StatesNRM( eName)
	exit
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC

	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		PRINTSTRING("\nUpdate_Savehouse_Door_States() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF	
	
	SWITCH eName
		// Michaels
		CASE SAVEHOUSE_MICHAEL_BH
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_F_L)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_F_L)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_F_L, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_L1)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_L1)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_L2)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_L2)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_R1)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_R1)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_M_MANSION_R_R2)/32], (ENUM_TO_INT(DOORNAME_M_MANSION_R_R2)%32))
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_UNLOCKED)
			
		BREAK
		CASE SAVEHOUSE_MICHAEL_CS
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_TRAILER_CS)/32], (ENUM_TO_INT(DOORNAME_T_TRAILER_CS)%32))
			SET_DOOR_STATE(DOORNAME_T_TRAILER_CS, DOORSTATE_UNLOCKED)
		BREAK
		
		// Trevors
		CASE SAVEHOUSE_TREVOR_CS
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_TRAILER_CS)/32], (ENUM_TO_INT(DOORNAME_T_TRAILER_CS)%32))
			SET_DOOR_STATE(DOORNAME_T_TRAILER_CS, DOORSTATE_UNLOCKED)
		BREAK
		CASE SAVEHOUSE_TREVOR_VB
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_T_APARTMENT_VB)/32], (ENUM_TO_INT(DOORNAME_T_APARTMENT_VB)%32))
			SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB, DOORSTATE_UNLOCKED)
		BREAK
		CASE SAVEHOUSE_TREVOR_SC
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_STRIPCLUB_F)/32], (ENUM_TO_INT(DOORNAME_STRIPCLUB_F)%32))
			SET_DOOR_STATE(DOORNAME_STRIPCLUB_F, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_STRIPCLUB_R)/32], (ENUM_TO_INT(DOORNAME_STRIPCLUB_R)%32))
			SET_DOOR_STATE(DOORNAME_STRIPCLUB_R, DOORSTATE_UNLOCKED)
		BREAK
		
		// Franklins
		CASE SAVEHOUSE_FRANKLIN_SC
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_F_HOUSE_SC_F)/32], (ENUM_TO_INT(DOORNAME_F_HOUSE_SC_F)%32))
			SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_UNLOCKED)
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)/32], (ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)%32))
			SET_DOOR_STATE(DOORNAME_F_HOUSE_VH_F, DOORSTATE_UNLOCKED)
		BREAK
		CASE SAVEHOUSE_FRANKLIN_VH
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)/32], (ENUM_TO_INT(DOORNAME_F_HOUSE_VH_F)%32))
			SET_DOOR_STATE(DOORNAME_F_HOUSE_VH_F, DOORSTATE_UNLOCKED)
		BREAK
	ENDSWITCH
#endif
#endif
ENDPROC


/// PURPOSE: Sets the savehouse to be active/inactive
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
///    bAvailable - If the savehouse is available
PROC Set_Savehouse_Respawn_Available(SAVEHOUSE_NAME_ENUM eName, BOOL bAvailable)

	
#if USE_CLF_DLC
	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		PRINTSTRING("\nSet_Savehouse_Respawn_Available() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	IF bAvailable
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
	SET_SAVE_HOUSE(g_sSavehouses[eName].index, bAvailable, TRUE)	
#endif
#if USE_NRM_DLC
	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		PRINTSTRING("\nSet_Savehouse_Respawn_Available() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	IF bAvailable
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
	SET_SAVE_HOUSE(g_sSavehouses[eName].index, bAvailable, TRUE)	
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		PRINTSTRING("\nSet_Savehouse_Respawn_Available() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	IF bAvailable
		SET_BIT(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
	SET_SAVE_HOUSE(g_sSavehouses[eName].index, bAvailable, TRUE)
#endif
#endif	
	
	Update_Savehouse_Respawn_Blip(eName)
	Update_Savehouse_Respawn_Garage(eName)
	Update_Savehouse_Autosave_State(eName)
	Update_Savehouse_Door_StateS(eName)
ENDPROC


/// PURPOSE: Sets the savehouse blip to be short or long range
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
///    bLongRange - If the savehouse blip is to be long range
PROC Set_Savehouse_Respawn_Blip_Long_Range(SAVEHOUSE_NAME_ENUM eName, BOOL bLongRange)

#if USE_CLF_DLC
	IF eName = NUMBER_OF_CLF_SAVEHOUSE
		PRINTSTRING("\nSet_Savehouse_Blip_Long_Range() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	IF bLongRange
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF
	
	IF g_sSavehouses[eName].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sSavehouses[eName].eBlip, IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
	ENDIF
#endif
#if USE_NRM_DLC
	IF eName = NUMBER_OF_NRM_SAVEHOUSE
		PRINTSTRING("\nSet_Savehouse_Blip_Long_Range() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	IF bLongRange
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF
	
	IF g_sSavehouses[eName].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sSavehouses[eName].eBlip, IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
	ENDIF
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF eName = NUMBER_OF_SAVEHOUSE_LOCATIONS
		PRINTSTRING("\nSet_Savehouse_Blip_Long_Range() - Invalid savehouse enum")PRINTNL()
		EXIT
	ENDIF
	IF bLongRange
		SET_BIT(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF
	
	IF g_sSavehouses[eName].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sSavehouses[eName].eBlip, IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
	ENDIF
#endif
#endif

ENDPROC


PROC SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(enumCharacterList ePed, SAVEHOUSE_NAME_ENUM eSavehouse, BOOL bAllowEntry)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		IF bAllowEntry
			PRINTLN("SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(", ENUM_TO_INT(ePed), ", ", ENUM_TO_INT(eSavehouse), ", TRUE) called by ", GET_THIS_SCRIPT_NAME())
			SET_BIT(g_iSavehouseOpenForPedsBitset[eSavehouse], ENUM_TO_INT(ePed))
		ELSE
			PRINTLN("SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(", ENUM_TO_INT(ePed), ", ", ENUM_TO_INT(eSavehouse), ", TRUE) called by ", GET_THIS_SCRIPT_NAME())
			CLEAR_BIT(g_iSavehouseOpenForPedsBitset[eSavehouse], ENUM_TO_INT(ePed))
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
/// Returns the savehouse closest to the specified coords and character
///    NOTE: Set eCharacter to NO_CHARACTER to ignore the character specific check.
/// PARAMS:
///    vCoords - the position from which you are running this check
///    eCharacter - the character whose savehouse you want to use (Set to NO_CHARACTER to ignore the character specific check.)
///    bAvailableOnly - only check available savehouses?
/// RETURNS:
///    SAVEHOUSE_NAME_ENUM of the nearest savehouse
#if USE_CLF_DLC
FUNC SAVEHOUSE_NAME_ENUM GET_CLOSEST_SAVEHOUSECLF(VECTOR vCoords, enumCharacterList eCharacter = NO_CHARACTER, BOOL bAvailableOnly = FALSE)

	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 999999.99
	SAVEHOUSE_NAME_ENUM eSavehouse = NUMBER_OF_CLF_SAVEHOUSE
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)-1
		
		// Do not include temp savehouses (these dont have blips)
		IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].iChar = ENUM_TO_INT(eCharacter)
			OR eCharacter = NO_CHARACTER
			
				// available check
				IF Is_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount))
				OR bAvailableOnly = FALSE
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						eSavehouse = INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN eSavehouse
ENDFUNC
#endif


#if USE_NRM_DLC
FUNC SAVEHOUSE_NAME_ENUM GET_CLOSEST_SAVEHOUSENRM(VECTOR vCoords, enumCharacterList eCharacter = NO_CHARACTER, BOOL bAvailableOnly = FALSE)

	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 999999.99
	SAVEHOUSE_NAME_ENUM eSavehouse = NUMBER_OF_NRM_SAVEHOUSE
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1
		
		// Do not include temp savehouses (these dont have blips)
		IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].iChar = ENUM_TO_INT(eCharacter)
			OR eCharacter = NO_CHARACTER
			
				// available check
				IF Is_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount))
				OR bAvailableOnly = FALSE
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						eSavehouse = INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN eSavehouse
ENDFUNC
#endif


FUNC SAVEHOUSE_NAME_ENUM GET_CLOSEST_SAVEHOUSE(VECTOR vCoords, enumCharacterList eCharacter = NO_CHARACTER, BOOL bAvailableOnly = FALSE)
#if USE_CLF_DLC
return GET_CLOSEST_SAVEHOUSECLF(vCoords, eCharacter, bAvailableOnly)
#endif
#if USE_NRM_DLC
return GET_CLOSEST_SAVEHOUSENRM(vCoords, eCharacter, bAvailableOnly)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 999999.99
	SAVEHOUSE_NAME_ENUM eSavehouse = NUMBER_OF_SAVEHOUSE_LOCATIONS
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1
		
		// Do not include temp savehouses (these dont have blips)
		IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].iChar = ENUM_TO_INT(eCharacter)
			OR eCharacter = NO_CHARACTER
			
				// available check
				IF Is_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount))
				OR bAvailableOnly = FALSE
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						eSavehouse = INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN eSavehouse
#endif
#endif	
ENDFUNC


/// PURPOSE:
///    Gets vector and heading of suitable position for repsotting a vehicle at the nearest savehouse  
/// PARAMS:
///    vCoords - the position from which you are running this check
///    eCharacter - the character whose savehouse you want to use
///    vReturnPos - the position you should use to respot the car
///    fReturnHeading - the heading you should use to respot the car
/// RETURNS:
///    TRUE if a position was found, FALSE otherwise
#if USE_CLF_DLC
FUNC BOOL GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSECLF(VECTOR vCoords, enumCharacterList eCharacter, VECTOR &vReturnPos, FLOAT &fReturnHeading)

	SAVEHOUSE_NAME_ENUM eSaveHouse = GET_CLOSEST_SAVEHOUSE(vCoords, eCharacter, TRUE)

	SWITCH eSaveHouse
		CASE SAVEHOUSEclf_MICHAEL_BH
			vReturnPos = <<-827.3510, 157.7850, 68.2143>>
			fReturnHeading = 85.1509
			RETURN TRUE
		BREAK
		CASE SAVEHOUSEclf_MICHAEL_CS // these 2 are the same
		CASE SAVEHOUSEclf_TREVOR_CS
			vReturnPos = <<1992.5234, 3813.9158, 31.1008>>
			fReturnHeading = 122.1498
			RETURN TRUE
		BREAK
		CASE SAVEHOUSEclf_TREVOR_VB
			vReturnPos = <<-1184.2582, -1496.5555, 3.3895>>
			fReturnHeading = 303.2098
			RETURN TRUE
		BREAK
		CASE SAVEHOUSEclf_TREVOR_SC
			vReturnPos = <<118.1067, -1325.9058, 28.3706>>
			fReturnHeading = 123.5016
			RETURN TRUE
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_SC
			vReturnPos = <<-18.1180, -1455.1265, 29.5004>>
			fReturnHeading = 273.2822
			RETURN TRUE
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_VH
			//vReturnPos = <<5.1878, 545.5834, 174.0986>> 
			vReturnPos = <<1.5947, 543.4017, 173.46439>>
			fReturnHeading = 310.7556
			RETURN TRUE
		BREAK
		DEFAULT //other savehouses aren't valid
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
#endif


#if USE_NRM_DLC
FUNC BOOL GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSENRM(VECTOR vCoords, enumCharacterList eCharacter, VECTOR &vReturnPos, FLOAT &fReturnHeading)

	SAVEHOUSE_NAME_ENUM eSaveHouse = GET_CLOSEST_SAVEHOUSE(vCoords, eCharacter, TRUE)

	SWITCH eSaveHouse
		CASE SAVEHOUSENRM_BH
			vReturnPos = <<-827.3510, 157.7850, 68.2143>>
			fReturnHeading = 85.1509
			RETURN TRUE
		BREAK
		CASE SAVEHOUSENRM_CHATEAU
			vReturnPos = <<-827.3510, 157.7850, 68.2143>>
			fReturnHeading = 85.1509
			RETURN TRUE
		BREAK
		DEFAULT //other savehouses aren't valid
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
#endif
FUNC BOOL GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSE(VECTOR vCoords, enumCharacterList eCharacter, VECTOR &vReturnPos, FLOAT &fReturnHeading)
#if USE_CLF_DLC
	return GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSECLF( vCoords,  eCharacter,  vReturnPos, fReturnHeading)
#endif
#if USE_NRM_DLC
	return GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSENRM( vCoords,  eCharacter,  vReturnPos, fReturnHeading)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC


	SAVEHOUSE_NAME_ENUM eSaveHouse = GET_CLOSEST_SAVEHOUSE(vCoords, eCharacter, TRUE)

	SWITCH eSaveHouse
		CASE SAVEHOUSE_MICHAEL_BH
			vReturnPos = <<-827.3510, 157.7850, 68.2143>>
			fReturnHeading = 85.1509
			RETURN TRUE
		BREAK
		CASE SAVEHOUSE_MICHAEL_CS // these 2 are the same
		CASE SAVEHOUSE_TREVOR_CS
			vReturnPos = <<1992.5234, 3813.9158, 31.1008>>
			fReturnHeading = 122.1498
			RETURN TRUE
		BREAK
		CASE SAVEHOUSE_TREVOR_VB
			vReturnPos = <<-1184.2582, -1496.5555, 3.3895>>
			fReturnHeading = 303.2098
			RETURN TRUE
		BREAK
		CASE SAVEHOUSE_TREVOR_SC
			vReturnPos = <<118.1067, -1325.9058, 28.3706>>
			fReturnHeading = 123.5016
			RETURN TRUE
		BREAK
		CASE SAVEHOUSE_FRANKLIN_SC
			vReturnPos = <<-18.1180, -1455.1265, 29.5004>>
			fReturnHeading = 273.2822
			RETURN TRUE
		BREAK
		CASE SAVEHOUSE_FRANKLIN_VH
			//vReturnPos = <<5.1878, 545.5834, 174.0986>> 
			vReturnPos = <<1.5947, 543.4017, 173.46439>>
			fReturnHeading = 310.7556
			RETURN TRUE
		BREAK
		DEFAULT //other savehouses aren't valid
		BREAK
	ENDSWITCH
	RETURN FALSE
#endif
#endif
ENDFUNC


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    POLICE STATIONS
///    

/// PURPOSE: Reset all the police respawn data back to the defaults
///    
PROC Clear_Police_Respawn_Locations()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vBlipCoords 		= << 0.0, 0.0, 0.0 >>
		g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords 		= << 0.0, 0.0, 0.0 >>
		g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].fSpawnHeading 		= 0.0
		g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].eBlip 				= STATIC_BLIP_NAME_DUMMY_FINAL
		g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].index				= NULL
	ENDFOR
ENDPROC


/// PURPOSE: Make sure all the police respawn locations have been set up
///    
PROC Check_Police_Respawn_Locations()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		IF g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords.x = 0.0
		AND g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords.y = 0.0
		AND g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords.z = 0.0
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("\nCheck_Police_Respawn_Locations() - Not all police stations been set up")
				PRINTSTRING("\nCheck_Police_Respawn_Locations() - Not all police stations have been set up")PRINTNL()
			#ENDIF
		ENDIF
	ENDFOR
ENDPROC


/// PURPOSE: Registers a new police respawn location with code and stores a copy of the data for script.
///    
/// PARAMS:
///    vCoords - Location of the police station
///    fHeading - Heading used when creating the player
///    eName - The enum name that we setup in respawn_location_globals.sch
///    eBlip - The enum blip we use for the respawn location
PROC Setup_Police_Respawn_Location(VECTOR vBlipCoords, VECTOR vSpawnCoords, FLOAT fSpawnHeading, POLICE_STATION_NAME_ENUM eName, STATIC_BLIP_NAME_ENUM eBlip)

	IF eName = NUMBER_OF_POLICE_STATION_LOCATIONS
		PRINTSTRING("\nSetup_Police_Respawn_Location() - Invalid police station enum")PRINTNL()
		EXIT
	ENDIF
	
	g_sPoliceStations[eName].index = ADD_POLICE_RESTART(vSpawnCoords, fSpawnHeading, 0)    
	DISABLE_POLICE_RESTART(g_sPoliceStations[eName].index, FALSE)
	
	g_sPoliceStations[eName].vBlipCoords 	= vBlipCoords
	g_sPoliceStations[eName].vSpawnCoords 	= vSpawnCoords
	g_sPoliceStations[eName].fSpawnHeading 	= fSpawnHeading
	g_sPoliceStations[eName].eBlip 			= eBlip
	
	SET_STATIC_BLIP_ACTIVE_STATE(eBlip, FALSE)								/* Active 		*/
	//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, FALSE)							/* Flash		*/
	SET_STATIC_BLIP_APPEAR_ON_MAP(eBlip, FALSE)								/* Map			*/
	SET_STATIC_BLIP_APPEAR_IN_RADAR(eBlip, TRUE)							/* Radar		*/
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(eBlip, FALSE)							/* Rader edge	*/
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(eBlip, TRUE)							/* Mission		*/
	//SET_STATIC_BLIP_IS_DISCOVERABLE(eBlip, FALSE)							/* Discoverable */
	SET_STATIC_BLIP_POSITION(eBlip, g_sPoliceStations[eName].vBlipCoords)	/* Position		*/
	SET_STATIC_BLIP_CATEGORY(eBlip, STATIC_BLIP_CATEGORY_POLICE_STATION)	/* Category		*/
	SET_STATIC_BLIP_ICON(eBlip, RADAR_TRACE_POLICE_STATION)					/* Sprite		*/
ENDPROC


/// PURPOSE: Sets the police station to be active/inactive
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
///    bAvailable - If the police station is available
PROC Set_Police_Respawn_Available(POLICE_STATION_NAME_ENUM eName, BOOL bAvailable)

	IF eName = NUMBER_OF_POLICE_STATION_LOCATIONS
		PRINTSTRING("\nSet_Police_Respawn_Available() - Invalid police station enum")PRINTNL()
		EXIT
	ENDIF
#if USE_CLF_DLC	
	IF bAvailable
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
#endif	
#if USE_NRM_DLC
	IF bAvailable
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
#endif	

#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	IF bAvailable
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
#endif	
#endif
	#IF IS_DEBUG_BUILD
		PRINTLN("Set_Police_Respawn_Available(", Get_Police_Respawn_Name(eName), ", ", bAvailable, ")")
	#ENDIF
	DISABLE_POLICE_RESTART(g_sPoliceStations[eName].index, !bAvailable)
	
	// Bug #32774 - Removing blips for now
	SET_STATIC_BLIP_ACTIVE_STATE(g_sPoliceStations[eName].eBlip, FALSE)
	
ENDPROC


/// PURPOSE: Sets the police station blip to be short or long range
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
///    bLongRange - If the police station blip is to be long range
PROC Set_Police_Respawn_Blip_Long_Range(POLICE_STATION_NAME_ENUM eName, BOOL bLongRange)

	IF eName = NUMBER_OF_POLICE_STATION_LOCATIONS
		PRINTSTRING("\nSet_Police_Station_Blip_Long_Range() - Invalid police station enum")PRINTNL()
		EXIT
	ENDIF
#if USE_CLF_DLC		
	IF bLongRange
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sPoliceStations[eName].eBlip, IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
#endif
#if USE_NRM_DLC	
	IF bLongRange
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sPoliceStations[eName].eBlip, IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
#endif

#if not USE_CLF_DLC		
#if not USE_NRM_DLC
	IF bLongRange
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sPoliceStations[eName].eBlip, IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
#endif
#endif
ENDPROC


/// PURPOSE: Returns TRUE if the default police respawns have been initialised
FUNC BOOL Has_Police_Respawns_Been_Initialised()
#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sRespawnData.bDefaultPoliceStationDataSet
#endif
#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sRespawnData.bDefaultPoliceStationDataSet
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	RETURN g_savedGlobals.sRespawnData.bDefaultPoliceStationDataSet	
#endif
#endif
ENDFUNC


/// PURPOSE: Returns the POLICE_STATION closest to the specified coords and character
///    NOTE: Set eCharacter to NO_CHARACTER to ignore the character specific check.
FUNC POLICE_STATION_NAME_ENUM GET_CLOSEST_POLICE_STATION(VECTOR vCoords, BOOL bIgnoreBlocked = FALSE)

	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 999999.99
	POLICE_STATION_NAME_ENUM ePoliceStation = NUMBER_OF_POLICE_STATION_LOCATIONS
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		
		// Do not include temp POLICE_STATIONs (these dont have blips)
		IF g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			#if USE_CLF_DLC
				IF NOT bIgnoreBlocked
				OR IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)], REPAWN_FLAG_AVAILABLE_BIT)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						ePoliceStation = INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			#endif
			#if USE_NRM_DLC
				IF NOT bIgnoreBlocked
				OR IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)], REPAWN_FLAG_AVAILABLE_BIT)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						ePoliceStation = INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF NOT bIgnoreBlocked
				OR IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)], REPAWN_FLAG_AVAILABLE_BIT)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						ePoliceStation = INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			#endif
			#endif
		ENDIF
	ENDFOR
	
	RETURN ePoliceStation
ENDFUNC


/// PURPOSE: Returns the distance to the police station closest to the given coords
///    NOTE: Set eCharacter to NO_CHARACTER to ignore the character specific check.
FUNC FLOAT GET_DISTANCE_TO_CLOSEST_POLICE_STATION(VECTOR vPlayerCoords)

	POLICE_STATION_NAME_ENUM ePoliceStation = GET_CLOSEST_POLICE_STATION(vPlayerCoords)
	
	IF ePoliceStation = NUMBER_OF_POLICE_STATION_LOCATIONS
		RETURN -1.0
	ENDIF
	
	RETURN GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, g_sPoliceStations[ePoliceStation].vSpawnCoords)
ENDFUNC


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    HOSPITALS
///    

/// PURPOSE: Reset all the hospital data back to the defaults
///    
PROC Clear_Hospital_Respawn_Locations()
	INT iCount	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vBlipCoords 	 	= << 0.0, 0.0, 0.0 >>
		g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords 	 	= << 0.0, 0.0, 0.0 >>
		g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].fSpawnHeading 	= 0.0
		g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].eBlip 	 		= STATIC_BLIP_NAME_DUMMY_FINAL
		g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].index				= NULL
	ENDFOR
ENDPROC


/// PURPOSE: Make sure all the hospitals have been set up
///    
PROC Check_Hospital_Respawn_Locations()
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		IF g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords.x = 0.0
		AND g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords.y = 0.0
		AND g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords.z = 0.0
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("\nCheck_Hospital_Respawn_Locations() - Not all hospitals have been set up")
				PRINTSTRING("\nCheck_Hospital_Respawn_Locations() - Not all hospitals have been set up")PRINTNL()
			#ENDIF
		ENDIF
	ENDFOR
ENDPROC


/// PURPOSE: Registers a new hospital repsawn location with code and stores a copy of the data for script.
///    
/// PARAMS:
///    vCoords - Location of the hospital
///    fHeading - Heading used when creating the player
///    bAvailable - If the hospital is available to the player
///    eName - The enum name that we setup in respawn_location_globals.sch
///    eBlip - The enum blip we use for the hospital
PROC Setup_Hospital_Respawn_Location(VECTOR vBlipCoords, VECTOR vSpawnCoords, FLOAT fSpawnHeading, HOSPITAL_NAME_ENUM eName, STATIC_BLIP_NAME_ENUM eBlip)

	IF eName = NUMBER_OF_HOSPITAL_LOCATIONS
		PRINTSTRING("\nSetup_Hospital_Respawn_Location() - Invalid hospital enum")PRINTNL()
		EXIT
	ENDIF
	
	g_sHospitals[eName].index = ADD_HOSPITAL_RESTART(vSpawnCoords, fSpawnHeading, 0)
	DISABLE_HOSPITAL_RESTART(g_sHospitals[eName].index, FALSE)
	
	g_sHospitals[eName].vBlipCoords 	= vBlipCoords
	g_sHospitals[eName].vSpawnCoords 	= vSpawnCoords
	g_sHospitals[eName].fSpawnHeading 	= fSpawnHeading
	g_sHospitals[eName].eBlip			= eBlip
	
	SET_STATIC_BLIP_ACTIVE_STATE(eBlip, FALSE)							/* Active 		*/
	//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, FALSE)						/* Flash		*/
	SET_STATIC_BLIP_APPEAR_ON_MAP(eBlip, FALSE)							/* Map			*/
	SET_STATIC_BLIP_APPEAR_IN_RADAR(eBlip, TRUE)						/* Radar		*/
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(eBlip, FALSE)						/* Rader edge	*/
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(eBlip, TRUE)						/* Mission		*/
	//SET_STATIC_BLIP_IS_DISCOVERABLE(eBlip, FALSE)						/* Discoverable */
	SET_STATIC_BLIP_POSITION(eBlip, g_sHospitals[eName].vBlipCoords)	/* Position		*/
	SET_STATIC_BLIP_CATEGORY(eBlip, STATIC_BLIP_CATEGORY_HOSPITAL)		/* Category		*/
	SET_STATIC_BLIP_ICON(eBlip, RADAR_TRACE_HOSPITAL)					/* Sprite		*/
ENDPROC


/// PURPOSE: Sets the hospital to be active/inactive
///    
/// PARAMS:
///    eName - The enum name that we setup in locations_globals.sch
///    bAvailable - If the hospital is available
PROC Set_Hospital_Respawn_Available(HOSPITAL_NAME_ENUM eName, BOOL bAvailable)

	IF eName = NUMBER_OF_HOSPITAL_LOCATIONS
		PRINTSTRING("\nSet_Hospital_Respawn_Available() - Invalid hospital enum")PRINTNL()
		EXIT
	ENDIF
	
#if USE_CLF_DLC	
	IF bAvailable
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
#endif
#if USE_NRM_DLC
	IF bAvailable
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
#endif

#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	IF bAvailable
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_AVAILABLE_BIT)
	ENDIF
#endif
#endif

	#IF IS_DEBUG_BUILD
		PRINTLN("Set_Hospital_Respawn_Available(", Get_Hospital_Respawn_Name(eName), ", ", bAvailable, ")")
	#ENDIF
	DISABLE_HOSPITAL_RESTART(g_sHospitals[eName].index, !bAvailable)
	
	// Bug #32774 - Removing blips for now
	SET_STATIC_BLIP_ACTIVE_STATE(g_sHospitals[eName].eBlip, FALSE)
	
ENDPROC


/// PURPOSE: Sets the hospital blip to be short or long range
///    
/// PARAMS:
///    eName - The enum name that we setup in respawn_location_globals.sch
///    bLongRange - If the hospital blip is to be long range
PROC Set_Hospital_Respawn_Blip_Long_Range(HOSPITAL_NAME_ENUM eName, BOOL bLongRange)

	IF eName = NUMBER_OF_HOSPITAL_LOCATIONS
		PRINTSTRING("\nSet_Hospital_Blip_Long_Range() - Invalid hospital enum")PRINTNL()
		EXIT
	ENDIF
#if USE_CLF_DLC		
	IF bLongRange
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF	
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sHospitals[eName].eBlip, IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
#endif
#if USE_NRM_DLC	
	IF bLongRange
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF	
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sHospitals[eName].eBlip, IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
#endif

#if not USE_CLF_DLC		
#if not USE_NRM_DLC
	IF bLongRange
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ELSE
		CLEAR_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	ENDIF	
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(g_sHospitals[eName].eBlip, IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[eName], REPAWN_FLAG_LONG_RANGE_BLIP_BIT))
#endif
#endif
ENDPROC


/// PURPOSE: Returns TRUE if the default hospital respawns have been initialised
FUNC BOOL Has_Hospital_Respawns_Been_Initialised()
#if USE_CLF_DLC	
	RETURN g_savedGlobalsClifford.sRespawnData.bDefaultHospitalDataSet
#endif
#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sRespawnData.bDefaultHospitalDataSet
#endif

#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	RETURN g_savedGlobals.sRespawnData.bDefaultHospitalDataSet
#endif	
#endif
ENDFUNC


/// PURPOSE: Returns the HOSPITAL closest to the specified coords and character
///    NOTE: Set eCharacter to NO_CHARACTER to ignore the character specific check.
FUNC HOSPITAL_NAME_ENUM GET_CLOSEST_HOSPITAL(VECTOR vCoords, BOOL bIgnoreBlocked = FALSE)

	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 9999999.99
	HOSPITAL_NAME_ENUM eHospital = NUMBER_OF_HOSPITAL_LOCATIONS
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		
		// Do not include temp hospitals (these dont have blips)
		IF g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			#if USE_CLF_DLC	
				IF NOT bIgnoreBlocked
				OR IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)], REPAWN_FLAG_AVAILABLE_BIT)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						eHospital = INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			#endif
			#if USE_NRM_DLC
				IF NOT bIgnoreBlocked
				OR IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)], REPAWN_FLAG_AVAILABLE_BIT)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						eHospital = INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			#endif
			
			#if not USE_CLF_DLC	
			#if not USE_NRM_DLC
				IF NOT bIgnoreBlocked
				OR IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)], REPAWN_FLAG_AVAILABLE_BIT)
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)].vSpawnCoords)
					IF fCurrentDist < fShortDist
						fShortDist = fCurrentDist
						eHospital = INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)
					ENDIF
				ENDIF
			#endif
			#endif
		ENDIF
	ENDFOR
	
	RETURN eHospital
ENDFUNC


FUNC BOOL GetSelectedHospital(HOSPITAL_NAME_ENUM &eSelectedHospital)
#if USE_CLF_DLC	
	enumCharacterList	eLastKnownPed		= g_savedGlobalsClifford.sPlayerData.sInfo.eLastKnownPed
	VECTOR				vLastKnownCoords	= g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[eLastKnownPed]
#endif	
#if USE_NRM_DLC
	enumCharacterList	eLastKnownPed		= g_savedGlobalsnorman.sPlayerData.sInfo.eLastKnownPed
	VECTOR				vLastKnownCoords	= g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[eLastKnownPed]
#endif	

#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	enumCharacterList	eLastKnownPed		= g_savedGlobals.sPlayerData.sInfo.eLastKnownPed
	VECTOR				vLastKnownCoords	= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[eLastKnownPed]
#endif
#endif

	IF ARE_VECTORS_EQUAL(vLastKnownCoords, <<0,0,0>>)
		vLastKnownCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	eSelectedHospital = NUMBER_OF_HOSPITAL_LOCATIONS
	FLOAT fMinHospitalDistance = 9999999.0
	
	HOSPITAL_NAME_ENUM eHospital
	REPEAT NUMBER_OF_HOSPITAL_LOCATIONS eHospital
		#if  USE_CLF_DLC		
			IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[eHospital], REPAWN_FLAG_AVAILABLE_BIT)
				VECTOR vHospitalCoords = g_sHospitals[eHospital].vSpawnCoords
				FLOAT fDistToHospital = VDIST(vLastKnownCoords, vHospitalCoords)
				
				IF fDistToHospital > 150.0
					IF fDistToHospital < fMinHospitalDistance
						eSelectedHospital		= eHospital
						fMinHospitalDistance	= fDistToHospital
					ENDIF
				ENDIF
			ENDIF
		#endif	
		#if  USE_NRM_DLC		
			IF IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[eHospital], REPAWN_FLAG_AVAILABLE_BIT)
				VECTOR vHospitalCoords = g_sHospitals[eHospital].vSpawnCoords
				FLOAT fDistToHospital = VDIST(vLastKnownCoords, vHospitalCoords)
				
				IF fDistToHospital > 150.0
					IF fDistToHospital < fMinHospitalDistance
						eSelectedHospital		= eHospital
						fMinHospitalDistance	= fDistToHospital
					ENDIF
				ENDIF
			ENDIF
		#endif
		
		#if not USE_CLF_DLC		
		#if not USE_NRM_DLC
			IF IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[eHospital], REPAWN_FLAG_AVAILABLE_BIT)
				VECTOR vHospitalCoords = g_sHospitals[eHospital].vSpawnCoords
				FLOAT fDistToHospital = VDIST(vLastKnownCoords, vHospitalCoords)
				
				IF fDistToHospital > 150.0
					IF fDistToHospital < fMinHospitalDistance
						eSelectedHospital		= eHospital
						fMinHospitalDistance	= fDistToHospital
					ENDIF
				ENDIF
			ENDIF
		#endif	
		#endif
	ENDREPEAT
	
	RETURN (eSelectedHospital <> NUMBER_OF_HOSPITAL_LOCATIONS)
ENDFUNC
