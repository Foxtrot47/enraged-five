//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	respawn_location_data.sch									//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Where we setup the respawn locations for savehouses,		//
//							hospitals, and 	police stations.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "respawn_location_private.sch"
USING "mission_stat_public.sch"

/// PURPOSE: Registers all the savehouse respawn locations
#if USE_CLF_DLC
PROC Register_Savehouse_Respawn_LocationsCLF()
	
	Clear_Savehouse_Respawn_LocationsCLF()
	/////////////////////////////////////////////////////////
	///     PROLOGUE -> ARMENIAN1
	Setup_Savehouse_Respawn_LocationCLF(<< -1909.5134, -573.3823, 18.0970 >>, << -1909.5134, -573.3823, 18.0970 >>, 144.3309, SAVEHOUSEclf_MICHAEL_PRO, STATIC_BLIP_NAME_DUMMY_FINAL, "V_65_Main", CHAR_MICHAEL)
	Setup_Savehouse_Respawn_LocationCLF(<< -1909.5134, -573.3823, 18.0970 >>, << -1909.5134, -573.3823, 18.0970 >>, 144.3309, SAVEHOUSEclf_FRANKLIN_PRO, STATIC_BLIP_NAME_DUMMY_FINAL, "V_65_Main", CHAR_FRANKLIN)
	Setup_Savehouse_Respawn_LocationCLF(<< -1909.5134, -573.3823, 18.0970 >>, << -1909.5134, -573.3823, 18.0970 >>, 144.3309, SAVEHOUSEclf_TREVOR_PRO, STATIC_BLIP_NAME_DUMMY_FINAL, "V_65_Main", CHAR_TREVOR)
	
	/////////////////////////////////////////////////////////
	///     MICHAEL
	Setup_Savehouse_Respawn_LocationCLF(<< -813.6030, 179.4738, 71.1531 >>, << -813.6030, 179.4738, 71.1531 >>, 111.1168, SAVEHOUSEclf_MICHAEL_BH, STATIC_BLIP_RESPAWN_SAVEHOUSE_MICHAEL_BH, "V_Michael_G_Hall", CHAR_MICHAEL)
	Setup_Savehouse_Respawn_LocationCLF(<< 1972.6061, 3817.0444, 32.4297 >>, << 1972.6061, 3817.0444, 32.4297 >>, 210.6042, SAVEHOUSEclf_MICHAEL_CS, STATIC_BLIP_RESPAWN_SAVEHOUSE_MICHAEL_CS, "V_TrailerRm", CHAR_MICHAEL)
	
	/////////////////////////////////////////////////////////
	///     TREVOR
	Setup_Savehouse_Respawn_LocationCLF(<< 1972.6061, 3817.0444, 32.4297 >>, << 1972.6061, 3817.0444, 32.4297 >>, 210.6042, SAVEHOUSEclf_TREVOR_CS, STATIC_BLIP_RESPAWN_SAVEHOUSE_TREVOR_CS, "V_TrailerRm", CHAR_TREVOR)
	Setup_Savehouse_Respawn_LocationCLF(<< -1151.7458, -1518.1357, 9.6346 >>, << -1151.7458, -1518.1357, 9.6346 >>, 187.2844, SAVEHOUSEclf_TREVOR_VB, STATIC_BLIP_RESPAWN_SAVEHOUSE_TREVOR_VB, "rm_Lounge", CHAR_TREVOR)
	Setup_Savehouse_Respawn_LocationCLF(<< 94.5723, -1290.0817, 28.7355 >>, << 96.1503, -1290.7251, 28.2688 >>, 301.5321, SAVEHOUSEclf_TREVOR_SC, STATIC_BLIP_RESPAWN_SAVEHOUSE_TREVOR_SC, "strp3off", CHAR_TREVOR)
	
	/////////////////////////////////////////////////////////
	///     FRANKLIN
	Setup_Savehouse_Respawn_LocationCLF(<< -14.3803, -1438.5143, 31.2000 >>, << -14.3803, -1438.5143, 31.2000 >>, 182.1964, SAVEHOUSEclf_FRANKLIN_SC, STATIC_BLIP_RESPAWN_SAVEHOUSE_FRANKLIN_SC, "V_57_HallRM", CHAR_FRANKLIN)
	Setup_Savehouse_Respawn_LocationCLF(<< 7.1190, 536.6152, 175.0280 >>, << 7.1190, 536.6152, 175.0280 >>, 336.5715, SAVEHOUSEclf_FRANKLIN_VH, STATIC_BLIP_RESPAWN_SAVEHOUSE_FRANKLIN_VH, "Lounge", CHAR_FRANKLIN)
	Check_Savehouse_Respawn_LocationsCLF()
ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Savehouse_Respawn_LocationsNRM()	
	Clear_Savehouse_Respawn_LocationsNRM()	
	Setup_Savehouse_Respawn_LocationNRM(<< -813.6030, 179.4738, 71.1531 >>, << -813.6030, 179.4738, 71.1531 >>, 111.1168, SAVEHOUSENRM_BH, STATIC_BLIP_RESPAWN_SAVEHOUSE_MICHAEL_BH, "V_Michael_G_Hall", CHAR_MICHAEL)
	Setup_Savehouse_Respawn_LocationNRM(<<-68.51072, 354.86896, 111.44838>>, << -56.06457, 351.33057, 111.44754 >>, 0, SAVEHOUSENRM_CHATEAU, STATIC_BLIP_RESPAWN_SAVEHOUSE_MICHAEL_CS, "V_TrailerRm", CHAR_MICHAEL)
	Check_Savehouse_Respawn_LocationsNRM()
ENDPROC
#endif

PROC Register_Savehouse_Respawn_Locations()
	
	#if USE_CLF_DLC
		Register_Savehouse_Respawn_LocationsCLF()
		EXIT
	#endif
	#if USE_NRM_DLC
		Register_Savehouse_Respawn_LocationsNRM()
		EXIT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	Clear_Savehouse_Respawn_Locations()
	
	/////////////////////////////////////////////////////////
	///     PROLOGUE -> ARMENIAN1
	Setup_Savehouse_Respawn_Location(<< -1909.5134, -573.3823, 18.0970 >>, << -1909.5134, -573.3823, 18.0970 >>, 144.3309, SAVEHOUSE_MICHAEL_PRO, STATIC_BLIP_NAME_DUMMY_FINAL, "V_65_Main", CHAR_MICHAEL)
	Setup_Savehouse_Respawn_Location(<< -1909.5134, -573.3823, 18.0970 >>, << -1909.5134, -573.3823, 18.0970 >>, 144.3309, SAVEHOUSE_FRANKLIN_PRO, STATIC_BLIP_NAME_DUMMY_FINAL, "V_65_Main", CHAR_FRANKLIN)
	Setup_Savehouse_Respawn_Location(<< -1909.5134, -573.3823, 18.0970 >>, << -1909.5134, -573.3823, 18.0970 >>, 144.3309, SAVEHOUSE_TREVOR_PRO, STATIC_BLIP_NAME_DUMMY_FINAL, "V_65_Main", CHAR_TREVOR)
	
	/////////////////////////////////////////////////////////
	///     MICHAEL
	Setup_Savehouse_Respawn_Location(<< -813.6030, 179.4738, 71.1531 >>, << -813.6030, 179.4738, 71.1531 >>, 111.1168, SAVEHOUSE_MICHAEL_BH, STATIC_BLIP_RESPAWN_SAVEHOUSE_MICHAEL_BH, "V_Michael_G_Hall", CHAR_MICHAEL)
	Setup_Savehouse_Respawn_Location(<< 1972.6061, 3817.0444, 32.4297 >>, << 1972.6061, 3817.0444, 32.4297 >>, 210.6042, SAVEHOUSE_MICHAEL_CS, STATIC_BLIP_RESPAWN_SAVEHOUSE_MICHAEL_CS, "V_TrailerRm", CHAR_MICHAEL)
	
	/////////////////////////////////////////////////////////
	///     TREVOR
	Setup_Savehouse_Respawn_Location(<< 1972.6061, 3817.0444, 32.4297 >>, << 1972.6061, 3817.0444, 32.4297 >>, 210.6042, SAVEHOUSE_TREVOR_CS, STATIC_BLIP_RESPAWN_SAVEHOUSE_TREVOR_CS, "V_TrailerRm", CHAR_TREVOR)
	Setup_Savehouse_Respawn_Location(<< -1151.7458, -1518.1357, 9.6346 >>, << -1151.7458, -1518.1357, 9.6346 >>, 187.2844, SAVEHOUSE_TREVOR_VB, STATIC_BLIP_RESPAWN_SAVEHOUSE_TREVOR_VB, "rm_Lounge", CHAR_TREVOR)
	Setup_Savehouse_Respawn_Location(<< 94.5723, -1290.0817, 28.7355 >>, << 96.1503, -1290.7251, 28.2688 >>, 301.5321, SAVEHOUSE_TREVOR_SC, STATIC_BLIP_RESPAWN_SAVEHOUSE_TREVOR_SC, "strp3off", CHAR_TREVOR)
	
	/////////////////////////////////////////////////////////
	///     FRANKLIN
	Setup_Savehouse_Respawn_Location(<< -14.3803, -1438.5143, 31.2000 >>, << -14.3803, -1438.5143, 31.2000 >>, 182.1964, SAVEHOUSE_FRANKLIN_SC, STATIC_BLIP_RESPAWN_SAVEHOUSE_FRANKLIN_SC, "V_57_HallRM", CHAR_FRANKLIN)
	Setup_Savehouse_Respawn_Location(<< 7.1190, 536.6152, 175.0280 >>, << 7.1190, 536.6152, 175.0280 >>, 336.5715, SAVEHOUSE_FRANKLIN_VH, STATIC_BLIP_RESPAWN_SAVEHOUSE_FRANKLIN_VH, "Lounge", CHAR_FRANKLIN)
	Check_Savehouse_Respawn_Locations()
	#endif
	#endif
ENDPROC


/// PURPOSE: Registers all the hospital respawn locations
///    
PROC Register_Hospital_Respawn_Locations()
	Clear_Hospital_Respawn_Locations()
	Setup_Hospital_Respawn_Location(<< -449.7836, -341.3995, 33.5024 >>, << -449.7836, -341.3995, 33.5024 >>, 83.1352, HOSPITAL_RH, STATIC_BLIP_RESPAWN_HOSPITAL_RH)
	Setup_Hospital_Respawn_Location(<< 341.4144, -1396.2910, 31.5098 >>, << 341.4144, -1396.2910, 31.5098 >>, 47.5300, HOSPITAL_SC, STATIC_BLIP_RESPAWN_HOSPITAL_SC)
	Setup_Hospital_Respawn_Location(<< 360.7675, -583.4315, 27.8269 >>, << 360.7675, -583.4315, 27.8269 >>, 276.9074, HOSPITAL_DT, STATIC_BLIP_RESPAWN_HOSPITAL_DT)
	Setup_Hospital_Respawn_Location(<< 1838.4948, 3672.2222, 33.2783 >>, << 1838.4948, 3672.2222, 33.2783 >>, 206.5448, HOSPITAL_SS, STATIC_BLIP_RESPAWN_HOSPITAL_SS)
	Setup_Hospital_Respawn_Location(<< -242.2981, 6325.2334, 31.4271 >>, << -242.2981, 6325.2334, 31.4271 >>, 335.2387, HOSPITAL_PB, STATIC_BLIP_RESPAWN_HOSPITAL_PB)
	Check_Hospital_Respawn_Locations()
ENDPROC


/// PURPOSE: Registers all the police station respawn locations
///    
PROC Register_Police_Respawn_Locations()
	Clear_Police_Respawn_Locations()
	Setup_Police_Respawn_Location(<< -1093.8900, -807.0834, 18.2612 >>, << -1093.8900, -807.0834, 18.2612 >>, 42.2400, POLICE_STATION_VB, STATIC_BLIP_RESPAWN_POLICE_VB)
	Setup_Police_Respawn_Location(<< 360.8818, -1581.6075, 28.2931 >>, << 360.8818, -1581.6075, 28.2931 >>, 23.6148, POLICE_STATION_SC, STATIC_BLIP_RESPAWN_POLICE_SC)
	Setup_Police_Respawn_Location(<< -560.7550, -133.9789, 37.0586 >>, << -560.5718, -132.7783, 37.0586 >>, 210.6082, POLICE_STATION_RH, STATIC_BLIP_RESPAWN_POLICE_RH)
	Setup_Police_Respawn_Location(<< 1856.3516, 3682.0608, 33.2672 >>, << 1856.3516, 3682.0608, 33.2672 >>, 210.2006, POLICE_STATION_SS, STATIC_BLIP_RESPAWN_POLICE_SS)
	Setup_Police_Respawn_Location(<< -440.7429, 6019.8892, 30.4903 >>, << -440.7429, 6019.8892, 30.4903 >>, 314.9315, POLICE_STATION_PB, STATIC_BLIP_RESPAWN_POLICE_PB)
	Setup_Police_Respawn_Location(<< 639.1819, 1.7650, 81.7865 >>, <<639.1819, 1.7650, 81.7865 >>, 238.0365, POLICE_STATION_HW, STATIC_BLIP_RESPAWN_POLICE_HW)
	Setup_Police_Respawn_Location(<<479.6391, -976.6794, 26.9839>>, <<479.6391, -976.6794, 26.9839>>, 331.7253, POLICE_STATION_DT, STATIC_BLIP_RESPAWN_POLICE_DT)
	
	Check_Police_Respawn_Locations()
ENDPROC

/// PURPOSE: Sets the active state of all the savehouse respawn locations
///    		 This must be called after the saved data has been loaded
#if USE_CLF_DLC
PROC Initialise_Savehouse_Respawn_LocationsCLF()

	PRINTSTRING("\nInitialising savehouse respawn locations AGT...")PRINTNL()
	// Set the default data if we haven't already
	IF NOT g_savedGlobalsClifford.sRespawnData.bDefaultSavehouseDataSet
		
		PRINTSTRING("<clf>	Setting default savehouse respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_clf_SAVEHOUSE)-1
			g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[i] = 0
			
			// If we're in debug mode, make them all available
			IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive AND NOT IS_REPEAT_PLAY_ACTIVE()
				SET_BIT(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[i], REPAWN_FLAG_AVAILABLE_BIT)
			ENDIF
			
			// They all have long range blips to start with
			SET_BIT(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[i], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		ENDFOR
		
		g_savedGlobalsClifford.sRespawnData.bDefaultSavehouseDataSet = TRUE
	ELSE
		PRINTSTRING("<clf>	Default savehouse respawn info already set")PRINTNL()
	ENDIF	
	// Set the active state of each savehouse
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_clf_SAVEHOUSE)-1
		Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	
ENDPROC
#endif
#if USE_NRM_DLC
/// PURPOSE: Sets the active state of all the savehouse respawn locations
///    		 This must be called after the saved data has been loaded
PROC Initialise_Savehouse_Respawn_LocationsNRM()

	PRINTSTRING("\nInitialising savehouse respawn locations NRM...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobalsnorman.sRespawnData.bDefaultSavehouseDataSet
		
		PRINTSTRING("<NRM>	Setting default savehouse respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1
			g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[i] = 0
			
			// If we're in debug mode, make them all available
			IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive AND NOT IS_REPEAT_PLAY_ACTIVE()
				SET_BIT(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[i], REPAWN_FLAG_AVAILABLE_BIT)
			ENDIF
			
			// They all have long range blips to start with
			SET_BIT(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[i], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		ENDFOR
		
		g_savedGlobalsnorman.sRespawnData.bDefaultSavehouseDataSet = TRUE
	ELSE
		PRINTSTRING("<NRM>	Default savehouse respawn info already set")PRINTNL()
	ENDIF	
	// Set the active state of each savehouse
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1
		Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	
ENDPROC
#endif
/// PURPOSE: Sets the active state of all the savehouse respawn locations
///    		 This must be called after the saved data has been loaded
PROC Initialise_Savehouse_Respawn_Locations()

	
	#if USE_CLF_DLC
		if g_bLoadedClifford
			Initialise_Savehouse_Respawn_LocationsCLF()
			exit
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			Initialise_Savehouse_Respawn_LocationsNRM()
			exit
		endif
	#endif
	#if not USE_CLF_DLC	
	#if not USE_NRM_DLC
	PRINTSTRING("\nInitialising savehouse respawn locations...")PRINTNL()	
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobals.sRespawnData.bDefaultSavehouseDataSet
		
		PRINTSTRING("	Setting default savehouse respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1
			g_savedGlobals.sRespawnData.iSavehouseProperties[i] = 0
			
			// If we're in debug mode, make them all available
			IF NOT g_savedGlobals.sFlow.isGameflowActive AND NOT IS_REPEAT_PLAY_ACTIVE()
				SET_BIT(g_savedGlobals.sRespawnData.iSavehouseProperties[i], REPAWN_FLAG_AVAILABLE_BIT)
			ENDIF
			
			// They all have long range blips to start with
			SET_BIT(g_savedGlobals.sRespawnData.iSavehouseProperties[i], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		ENDFOR
		
		g_savedGlobals.sRespawnData.bDefaultSavehouseDataSet = TRUE
	ELSE
		PRINTSTRING("	Default savehouse respawn info already set")PRINTNL()
	ENDIF	
	// Set the active state of each savehouse
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1
		Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	#endif
	#endif
ENDPROC

#if USE_CLF_DLC
/// PURPOSE: Sets the active state of all the police station respawn locations
///    		 This must be called after the saved data has been loaded
PROC Initialise_Police_Respawn_LocationsCLF()
	
	PRINTSTRING("\nInitialising police station respawn locations AGT...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobalsClifford.sRespawnData.bDefaultPoliceStationDataSet
		
		PRINTSTRING("	Setting default police station respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
			g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[i] = 0
		ENDFOR
		
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_RH], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_SC], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_VB], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_DT], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_SS], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_PB], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_HW], REPAWN_FLAG_AVAILABLE_BIT)
		
		// They all have long range blips to start with
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_RH], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_SC], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_DT], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_VB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_SS], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_PB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[POLICE_STATION_HW], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		
		g_savedGlobalsClifford.sRespawnData.bDefaultPoliceStationDataSet = TRUE
	ELSE
		PRINTSTRING("	Default police station respawn info already set")PRINTNL()
	ENDIF
	
	// Set the active state of each police station
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		Set_Police_Respawn_Available(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
ENDPROC
#endif
#if USE_NRM_DLC
PROC Initialise_Police_Respawn_LocationsNRM()
	
	PRINTSTRING("\nInitialising police station respawn locations...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobalsnorman.sRespawnData.bDefaultPoliceStationDataSet
		
		PRINTSTRING("	Setting default police station respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
			g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[i] = 0
		ENDFOR
		
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_RH], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_SC], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_VB], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_DT], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_SS], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_PB], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_HW], REPAWN_FLAG_AVAILABLE_BIT)
		
		// They all have long range blips to start with
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_RH], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_SC], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_DT], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_VB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_SS], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_PB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[POLICE_STATION_HW], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		
		g_savedGlobalsnorman.sRespawnData.bDefaultPoliceStationDataSet = TRUE
	ELSE
		PRINTSTRING("	Default police station respawn info already set")PRINTNL()
	ENDIF
	
	// Set the active state of each police station
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		Set_Police_Respawn_Available(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
ENDPROC
#endif

PROC Initialise_Police_Respawn_Locations()
	
	#if USE_CLF_DLC
		if g_bLoadedClifford	
			Initialise_Police_Respawn_LocationsCLF()
			exit
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			Initialise_Police_Respawn_LocationsNRM()
			exit
		endif
	#endif
	#if not USE_CLF_DLC		
	#if not USE_NRM_DLC
	PRINTSTRING("\nInitialising police station respawn locations...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobals.sRespawnData.bDefaultPoliceStationDataSet
		
		PRINTSTRING("	Setting default police station respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
			g_savedGlobals.sRespawnData.iPoliceStationProperties[i] = 0
		ENDFOR
		
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_RH], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_SC], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_VB], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_DT], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_SS], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_PB], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_HW], REPAWN_FLAG_AVAILABLE_BIT)
		
		// They all have long range blips to start with
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_RH], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_SC], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_DT], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_VB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_SS], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_PB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iPoliceStationProperties[POLICE_STATION_HW], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		
		g_savedGlobals.sRespawnData.bDefaultPoliceStationDataSet = TRUE
	ELSE
		PRINTSTRING("	Default police station respawn info already set")PRINTNL()
	ENDIF
	
	// Set the active state of each police station
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		Set_Police_Respawn_Available(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	#endif
	#endif
ENDPROC

PROC Update_Stored_Drown_Stats()
	
	//Initialise the number of times drowned, for respawn help text
	g_iNumberOfTimesDieDrowning = 0
	g_iNumberOfTimesDieDrowning += RETRIEVE_INT_STAT(SP0_DIED_IN_DROWNING)
	g_iNumberOfTimesDieDrowning += RETRIEVE_INT_STAT(SP1_DIED_IN_DROWNING)
	g_iNumberOfTimesDieDrowning += RETRIEVE_INT_STAT(SP2_DIED_IN_DROWNING)
	
	g_iNumberOfTimesDieDrowning += RETRIEVE_INT_STAT(SP0_DIED_IN_DROWNINGINVEHICLE)
	g_iNumberOfTimesDieDrowning += RETRIEVE_INT_STAT(SP1_DIED_IN_DROWNINGINVEHICLE)
	g_iNumberOfTimesDieDrowning += RETRIEVE_INT_STAT(SP2_DIED_IN_DROWNINGINVEHICLE)
	
	//Initialise the number of times died in explosion
	g_iNumberOfTimesDieExplosion = 0
	g_iNumberOfTimesDieExplosion += RETRIEVE_INT_STAT(SP0_DIED_IN_EXPLOSION)
	g_iNumberOfTimesDieExplosion += RETRIEVE_INT_STAT(SP0_DIED_IN_EXPLOSION)
	g_iNumberOfTimesDieExplosion += RETRIEVE_INT_STAT(SP0_DIED_IN_EXPLOSION)
	
	//Initialise the number of times died in a fall
	g_iNumberOfTimesDieFall = 0
	g_iNumberOfTimesDieFall += RETRIEVE_INT_STAT(SP0_DIED_IN_FALL)
	g_iNumberOfTimesDieFall += RETRIEVE_INT_STAT(SP0_DIED_IN_FALL)
	g_iNumberOfTimesDieFall += RETRIEVE_INT_STAT(SP0_DIED_IN_FALL)
	
	//Initialise the number of times died in fire
	g_iNumberOfTimesDieFire = 0
	g_iNumberOfTimesDieFire += RETRIEVE_INT_STAT(SP0_DIED_IN_FIRE)
	g_iNumberOfTimesDieFire += RETRIEVE_INT_STAT(SP0_DIED_IN_FIRE)
	g_iNumberOfTimesDieFire += RETRIEVE_INT_STAT(SP0_DIED_IN_FIRE)
	
	//Initialise the number of times died in road
	g_iNumberOfTimesDieRoad = 0
	g_iNumberOfTimesDieRoad += RETRIEVE_INT_STAT(SP0_DIED_IN_ROAD)
	g_iNumberOfTimesDieRoad += RETRIEVE_INT_STAT(SP0_DIED_IN_ROAD)
	g_iNumberOfTimesDieRoad += RETRIEVE_INT_STAT(SP0_DIED_IN_ROAD)
	
	//Initialise the number of times died in mission
	g_iNumberOfTimesDieMission = 0
	g_iNumberOfTimesDieMission += RETRIEVE_INT_STAT(SP0_DIED_IN_MISSION)
	g_iNumberOfTimesDieMission += RETRIEVE_INT_STAT(SP0_DIED_IN_MISSION)
	g_iNumberOfTimesDieMission += RETRIEVE_INT_STAT(SP0_DIED_IN_MISSION)
	
ENDPROC

#if USE_CLF_DLC
/// PURPOSE: Sets the active state of all the hospital respawn locations
///    		 This must be called after the saved data has been loaded
PROC Initialise_Hospital_Respawn_LocationsCLF()
	
	PRINTSTRING("\nInitialising hospital respawn locations...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobalsClifford.sRespawnData.bDefaultHospitalDataSet
		
		PRINTSTRING("	Setting default hospital respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
			g_savedGlobalsClifford.sRespawnData.iHospitalProperties[i] = 0
		ENDFOR
		
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_RH], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_DT], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_SC], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_SS], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_PB], REPAWN_FLAG_AVAILABLE_BIT)
		
		// They all have long range blips to start with
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_RH], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_DT], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_SC], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_SS], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[HOSPITAL_PB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		
		g_savedGlobalsClifford.sRespawnData.bDefaultHospitalDataSet = TRUE
	ELSE
		PRINTSTRING("	Default hospital respawn info already set")PRINTNL()
	ENDIF
	
	// Set the active state of each hospital
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		Set_Hospital_Respawn_Available(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	
	Update_Stored_Drown_Stats()
	
ENDPROC
#endif
#if USE_NRM_DLC
PROC Initialise_Hospital_Respawn_LocationsNRM()
	
	PRINTSTRING("\nInitialising hospital respawn locations...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobalsnorman.sRespawnData.bDefaultHospitalDataSet
		
		PRINTSTRING("	Setting default hospital respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
			g_savedGlobalsnorman.sRespawnData.iHospitalProperties[i] = 0
		ENDFOR
		
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_RH], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_DT], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_SC], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_SS], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_PB], REPAWN_FLAG_AVAILABLE_BIT)
		
		// They all have long range blips to start with
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_RH], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_DT], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_SC], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_SS], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[HOSPITAL_PB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		
		g_savedGlobalsnorman.sRespawnData.bDefaultHospitalDataSet = TRUE
	ELSE
		PRINTSTRING("	Default hospital respawn info already set")PRINTNL()
	ENDIF
	
	// Set the active state of each hospital
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		Set_Hospital_Respawn_Available(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	
	Update_Stored_Drown_Stats()
	
ENDPROC
#endif

PROC Initialise_Hospital_Respawn_Locations()
	
	#IF USE_CLF_DLC
		IF g_bLoadedClifford	
			Initialise_Hospital_Respawn_LocationsCLF()
			EXIT
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF g_bLoadedNorman
			Initialise_Hospital_Respawn_LocationsNRM()
			EXIT
		ENDIF
	#ENDIF
	
	#IF NOT USE_SP_DLC
	PRINTSTRING("\nInitialising hospital respawn locations...")PRINTNL()
	
	// Set the default data if we haven't already
	IF NOT g_savedGlobals.sRespawnData.bDefaultHospitalDataSet
		
		PRINTSTRING("	Setting default hospital respawn info")PRINTNL()
		
		INT i
		FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
			g_savedGlobals.sRespawnData.iHospitalProperties[i] = 0
		ENDFOR
		
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_RH], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_DT], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_SC], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_SS], REPAWN_FLAG_AVAILABLE_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_PB], REPAWN_FLAG_AVAILABLE_BIT)
		
		// They all have long range blips to start with
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_RH], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_DT], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_SC], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_SS], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		SET_BIT(g_savedGlobals.sRespawnData.iHospitalProperties[HOSPITAL_PB], REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
		
		g_savedGlobals.sRespawnData.bDefaultHospitalDataSet = TRUE
	ELSE
		PRINTSTRING("	Default hospital respawn info already set")PRINTNL()
	ENDIF
	
	// Set the active state of each hospital
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		Set_Hospital_Respawn_Available(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount), IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT))
	ENDFOR
	
	Update_Stored_Drown_Stats()
	#ENDIF
ENDPROC
