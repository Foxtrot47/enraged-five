//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	respawnCutscene_private.sch									//
//		AUTHOR			:	Alwyn Roberts												//
//		DESCRIPTION		:																//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "commands_graphics.sch"

USING "mission_control_private.sch"
USING "replay_public.sch"

USING "push_in_public.sch"

ENUM PLAYER_DAMAGE_ENUM
	PD_NONE					= 0,

	/*	# 1178145
	Hospital 0 - No damage
	Hospital 1 - Bruise left forehead
	Hospital 2 - Bruise on right eye
	Hospital 3 - Bruise on right eye, Bruise on left cheek, Redness at right lower lip 
	Hospital 4 - Bruise on left eye, Cut on left cheek, Bruise on right jawbone
	Hospital 5 - Small cut on right cheek, Cuts on right brow, Bruise on left eye, Bloody part on left eye tearduct.
	Hospital 6 - Bruise on right eye, Bruise on left eye, Small cut on right cheek, Bloody Nose
	Hospital 7 - Bruise on  left eye, Redness and scuffs on left brow, Cuts on Chin
	Hospital 8 - Cuts on chin, Cuts on right cheek, Bruise on left eye
	Hospital 9 - Bruise left cheek, Bruise right eye, Bruise left eye, Bloody nose, Scars on lower lip
	*/
	
	PD_0_chinBruise			= BIT0,
	PD_1_plasterForehead	= BIT1,
	PD_2_foreheadBruise		= BIT2,
	PD_3_noseBruise			= BIT3,
	PD_4_lefteyeBruise		= BIT4,
	PD_5_splatterLeft		= BIT5,
	PD_6_splatterLeft		= BIT6,
	PD_7_splatterLeft		= BIT7,
	PD_8_splatterLeft		= BIT8,
	PD_9_cutNose			= BIT9
ENDENUM


FUNC INT iPOWER(INT iThisNumber, INT iThisPower)
	INT iLoopControl
	INT iInitialValue = iThisNumber
	
	INT iResult = 0
	
	IF iThisPower = 0
		iResult = 1 //n^0 = 1
	ELIF iThisPower = 1
		iResult = iThisNumber
	ELIF iThisPower < 0
		iLoopControl = -iThisPower + 1
		WHILE iLoopControl > 0
			iThisNumber *= iInitialValue
			iLoopControl--
		ENDWHILE
		iResult = 1 / iThisNumber
	ELIF iThisPower > 1
		iLoopControl = iThisPower - 1
		WHILE iLoopControl > 0
			iThisNumber *= iInitialValue
			iLoopControl--
		ENDWHILE
		iResult = iThisNumber
	ENDIF
	
	RETURN iResult
ENDFUNC

PROC APPLY_DEATHARREST_DAMAGE_DECALS(PED_INDEX PedIndex, PLAYER_DAMAGE_ENUM ePlayerDamage)
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_0_chinBruise)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_0", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_0\")	//PD_0_chinBruise")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_1_plasterForehead)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_1", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_1\")	//PD_1_plasterForehead")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_2_foreheadBruise)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_2", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_2\")	//PD_2_foreheadBruise")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_3_noseBruise)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_3", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_3\")	//PD_3_noseBruise")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_4_lefteyeBruise)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_4", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_4\")	//PD_4_lefteyeBruise")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_5_splatterLeft)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_5", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_5\")	//PD_5_splatterLeft")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_6_splatterLeft)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_6", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_6\")	//PD_6_splatterLeft")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_7_splatterLeft)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_7", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_7\")	//PD_7_splatterLeft")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_8_splatterLeft)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_8", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_8\")	//PD_8_splatterLeft")
	ENDIF
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_9_cutNose)
		APPLY_PED_DAMAGE_PACK(pedIndex, "HOSPITAL_9", 0.0, 1.0)
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL(\"HOSPITAL_9\")	//PD_9_cutNose")
	ENDIF
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(ePlayerDamage, PD_NONE)
		//
		CPRINTLN(DEBUG_RESPAWN, "<respawn> APPLY_PED_DAMAGE_DECAL()	//PD_NONE")
	ENDIF
ENDPROC

PROC DisableControlAction(CONTROL_TYPE control, CONTROL_ACTION action, INT &iOffset_y)
	DISABLE_CONTROL_ACTION(control, action)
	iOffset_y++
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = ""
	HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE
	
	SWITCH control
		CASE PLAYER_CONTROL
			eColour = HUD_COLOUR_REDLIGHT
		BREAK
		CASE CAMERA_CONTROL
			eColour = HUD_COLOUR_GREENLIGHT
		BREAK
		CASE FRONTEND_CONTROL
			eColour = HUD_COLOUR_BLUELIGHT
		BREAK
	ENDSWITCH
	
	SWITCH action
		CASE INPUT_LOOK_BEHIND				str += ("INPUT_LOOK_BEHIND") BREAK
		CASE INPUT_VEH_LOOK_BEHIND			str += ("INPUT_VEH_LOOK_BEHIND") BREAK
		CASE INPUT_LOOK_LR					str += ("INPUT_LOOK_LR") BREAK
		CASE INPUT_LOOK_UD					str += ("INPUT_LOOK_UD") BREAK
		
		CASE INPUT_SPRINT					str += ("INPUT_SPRINT") BREAK
		CASE INPUT_JUMP						str += ("INPUT_JUMP") BREAK
		CASE INPUT_DUCK						str += ("INPUT_DUCK") BREAK
		
		CASE INPUT_MELEE_ATTACK_ALTERNATE	str += ("INPUT_MELEE_ATTACK_ALTERNATE") BREAK
		CASE INPUT_MELEE_ATTACK_HEAVY		str += ("INPUT_MELEE_ATTACK_HEAVY") BREAK
		CASE INPUT_MELEE_ATTACK_LIGHT		str += ("INPUT_MELEE_ATTACK_LIGHT") BREAK
		CASE INPUT_MELEE_ATTACK1			str += ("INPUT_MELEE_ATTACK1") BREAK
		CASE INPUT_MELEE_ATTACK2			str += ("INPUT_MELEE_ATTACK2") BREAK
		
		CASE INPUT_MOVE_LR					str += ("INPUT_MOVE_LR") BREAK
		CASE INPUT_MOVE_UD					str += ("INPUT_MOVE_UD") BREAK
		
		DEFAULT
			str += ("INPUT_")
			str += ENUM_TO_INT(action)
			str += ("_UNKNOWN")
		BREAK
		
	ENDSWITCH
	
	IF IS_DISABLED_CONTROL_PRESSED(control, action)
		DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(iOffset_y), eColour)
	ELSE
		DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(iOffset_y), eColour, 0.25)
	ENDIF
	#ENDIF
ENDPROC

STRUCT RESPAWN_CUTSCENE_STRUCT
	STRING pAnimDictName
	STRING pAnimClipName
	STRING pAnimCamName

	FLOAT fRollingBlendoutPhase = 0.95
	PED_MOTION_STATE ePedMotionState

	VECTOR vAnimPos
	VECTOR vAnimRot

	TEXT_LABEL DialogueTextLabel
	FLOAT fDialoguePhase

	STRING pFacialDictName
	STRING pFacialClipName

	PLAYER_DAMAGE_ENUM ePlayerDamage
	VECTOR vTrafficOffset, vTrafficBounds

	BOOL bSeenFirstTimeHelp

	STRING effectName

	STRING soundSetName
	STRING soundNameA, soundNameB

	#IF IS_DEBUG_BUILD 
	WIDGET_GROUP_ID respawn_widgetId
	#ENDIF

	FLOAT gpCamPitch = 999.0
	FLOAT gpCamHead = 999.0

	BOOL bStartedPushin = FALSE
	BOOL bShortCircuitedPushin = FALSE
	
	PUSH_IN_DATA sPushInData
	FLOAT fFirstPersonPunchPhase = 999.0
	VECTOR vPushInDataDirectionMod
	FLOAT gp3rdCamPitch
	FLOAT gp3rdCamHead
	
ENDSTRUCT

PROC RUN_RESPAWN_CUTSCENE(STRING pSceneDebugName, RESPAWN_CUTSCENE_STRUCT &sRespawnCutscene, STRING HelpTextLabel)
	CONST_FLOAT fRESPAWN_CUTSCENE_RADIUS		15.0	//5.0
	FLOAT fAnimPhase = 0.0
	
	#IF IS_DEBUG_BUILD
	FLOAT fAnimPos_heightDiff = 0.0
	
	INT iDamage
	TEXT_LABEL_63 str
	BOOL bUpdateRespawnScene, bUpdateDamageDecal[10], bTestRespawnCamPichHeading
	WIDGET_GROUP_ID first_time_deatharrest_widget_group_id
	SET_CURRENT_WIDGET_GROUP(sRespawnCutscene.respawn_widgetId)
	str  = "Respawn Cutscene ["
	str += pSceneDebugName
	str += "]"
	
	CPRINTLN(DEBUG_RESPAWN, "RUN_RESPAWN_CUTSCENE(\"", pSceneDebugName, "\")")
	first_time_deatharrest_widget_group_id = START_WIDGET_GROUP(str)
		str  = "clip: \""
		str += sRespawnCutscene.pAnimClipName
		str += "\", cam:  \""
		str += sRespawnCutscene.pAnimCamName
		str += "\""
		ADD_WIDGET_FLOAT_SLIDER(str, fAnimPhase, 0.0, 1.0, 0.01)
		
		CONST_FLOAT SLIDER_RANGE 10.0
		CONST_FLOAT SLIDER_STEP 0.01
		ADD_WIDGET_STRING("synch scene origin")
		ADD_WIDGET_FLOAT_SLIDER("vAnimPos x", sRespawnCutscene.vAnimPos.x, sRespawnCutscene.vAnimPos.x-SLIDER_RANGE, sRespawnCutscene.vAnimPos.x+SLIDER_RANGE, SLIDER_STEP)
		ADD_WIDGET_FLOAT_SLIDER("vAnimPos y", sRespawnCutscene.vAnimPos.y, sRespawnCutscene.vAnimPos.y-SLIDER_RANGE, sRespawnCutscene.vAnimPos.y+SLIDER_RANGE, SLIDER_STEP)
		ADD_WIDGET_FLOAT_SLIDER("vAnimPos z", sRespawnCutscene.vAnimPos.z, sRespawnCutscene.vAnimPos.z-SLIDER_RANGE, sRespawnCutscene.vAnimPos.z+SLIDER_RANGE, SLIDER_STEP*0.1)
		
		ADD_WIDGET_FLOAT_READ_ONLY("fAnimPos_heightDiff", fAnimPos_heightDiff)
		
		IF (sRespawnCutscene.vAnimRot.z > 180)	sRespawnCutscene.vAnimRot.z -=360 ENDIF
		IF (sRespawnCutscene.vAnimRot.z < -180)	sRespawnCutscene.vAnimRot.z +=360 ENDIF
		
		ADD_WIDGET_VECTOR_SLIDER("vAnimRot", sRespawnCutscene.vAnimRot, -180, 180, 1.0)
		str  = "help: \""
		str += HelpTextLabel
		str += "\""
		ADD_WIDGET_STRING(str)
		
		ADD_WIDGET_BOOL("bUpdateRespawnScene", bUpdateRespawnScene)
		
		START_WIDGET_GROUP("damage decals")
			REPEAT COUNT_OF(bUpdateDamageDecal) iDamage
				bUpdateDamageDecal[iDamage] = IS_BITMASK_ENUM_AS_ENUM_SET(sRespawnCutscene.ePlayerDamage, INT_TO_ENUM(PLAYER_DAMAGE_ENUM, iPOWER(2, iDamage)))
			ENDREPEAT
			
			ADD_WIDGET_BOOL("PD_0_chinBruise",		bUpdateDamageDecal[0])
			ADD_WIDGET_BOOL("PD_1_plasterForehead",	bUpdateDamageDecal[1])
			ADD_WIDGET_BOOL("PD_2_foreheadBruise",	bUpdateDamageDecal[2])
			ADD_WIDGET_BOOL("PD_3_noseBruise",		bUpdateDamageDecal[3])
			ADD_WIDGET_BOOL("PD_4_lefteyeBruise",	bUpdateDamageDecal[4])
			ADD_WIDGET_BOOL("PD_5_splatterLeft",	bUpdateDamageDecal[5])
			ADD_WIDGET_BOOL("PD_6_splatterLeft",	bUpdateDamageDecal[6])
			ADD_WIDGET_BOOL("PD_7_splatterLeft",	bUpdateDamageDecal[7])
			ADD_WIDGET_BOOL("PD_8_splatterLeft",	bUpdateDamageDecal[8])
			ADD_WIDGET_BOOL("PD_9_cutNose",			bUpdateDamageDecal[9])
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("traffic")
			ADD_WIDGET_VECTOR_SLIDER("vTrafficOffset", sRespawnCutscene.vTrafficOffset, -50, 50, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vTrafficBounds", sRespawnCutscene.vTrafficBounds, 0, 50, 1.0)
		STOP_WIDGET_GROUP()
		
		
		IF ((sRespawnCutscene.gpCamPitch != 999.0) AND (sRespawnCutscene.gpCamHead != 999.0))
		OR ((sRespawnCutscene.gp3rdCamPitch != 999.0) AND (sRespawnCutscene.gp3rdCamHead != 999.0))
			START_WIDGET_GROUP("gpCamPitch and Head")
				IF NOT (sRespawnCutscene.gpCamPitch = 999.0)
				AND NOT (sRespawnCutscene.gpCamHead = 999.0)
					ADD_WIDGET_FLOAT_SLIDER("gpCamPitch", sRespawnCutscene.gpCamPitch, -360, 360, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("gpCamHead", sRespawnCutscene.gpCamHead, -360, 360, 1.0)
				ENDIF
				IF NOT (sRespawnCutscene.gp3rdCamPitch = 999.0)
				AND NOT (sRespawnCutscene.gp3rdCamHead = 999.0)
					ADD_WIDGET_FLOAT_SLIDER("gp3rdCamPitch", sRespawnCutscene.gp3rdCamPitch, -360, 360, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("gp3rdCamHead", sRespawnCutscene.gp3rdCamHead, -360, 360, 1.0)
				ENDIF
				
				ADD_WIDGET_BOOL("bTestRespawnCamPichHeading", bTestRespawnCamPichHeading)
			STOP_WIDGET_GROUP()
		ENDIF
		
		START_WIDGET_GROUP("1st person")
			IF sRespawnCutscene.fFirstPersonPunchPhase != 999.0
				ADD_WIDGET_FLOAT_SLIDER("fFirstPersonPunchPhase", sRespawnCutscene.fFirstPersonPunchPhase, 0.0, 1.0, 0.01)
			ELSE
				ADD_WIDGET_FLOAT_READ_ONLY("fFirstPersonPunchPhase", sRespawnCutscene.fFirstPersonPunchPhase)
			ENDIF
			ADD_WIDGET_VECTOR_SLIDER("vPushInDataDirectionMod", sRespawnCutscene.vPushInDataDirectionMod, -90, 90, 0.01)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(sRespawnCutscene.respawn_widgetId)
	#ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	REMOVE_PARTICLE_FX_IN_RANGE(sRespawnCutscene.vAnimPos, REPLAY_LOAD_SCENE_SIZE)
	REMOVE_DECALS_IN_RANGE(sRespawnCutscene.vAnimPos, REPLAY_LOAD_SCENE_SIZE)
	
	CLEAR_AREA(sRespawnCutscene.vAnimPos, 5.0, TRUE)
	SET_ENTITY_COORDS(PLAYER_PED_ID(), sRespawnCutscene.vAnimPos)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
	
	WAIT(0) 
	SET_GAME_PAUSED(TRUE)
	
	// load the stream vol
	
	
	REQUEST_ANIM_DICT(sRespawnCutscene.pAnimDictName)
	IF IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.pFacialDictName)
		CPRINTLN(DEBUG_RESPAWN, "	REQUEST_ANIM_DICT(\"", sRespawnCutscene.pAnimDictName, "\")")
	ELSE
		CPRINTLN(DEBUG_RESPAWN, "	REQUEST_ANIM_DICT(\"", sRespawnCutscene.pAnimDictName, "\" and \"", sRespawnCutscene.pFacialDictName, "\")")
		
		REQUEST_ANIM_DICT(sRespawnCutscene.pFacialDictName)
	ENDIF
	
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	Replay_TeleportPlayer(sRespawnCutscene.vAnimPos, sRespawnCutscene.vAnimRot.z)
	
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
		
	INT iRespawnTimeoutGametime = GET_GAME_TIMER() + REPLAY_LOAD_SCENE_TIMEOUT
	BOOL bRespawnAssetsLoading = FALSE
	sRespawnCutscene.bShortCircuitedPushin = FALSE
	
	WHILE NOT bRespawnAssetsLoading
	AND iRespawnTimeoutGametime > GET_GAME_TIMER()
		bRespawnAssetsLoading = TRUE
		
		REQUEST_ANIM_DICT(sRespawnCutscene.pAnimDictName)
		IF NOT HAS_ANIM_DICT_LOADED(sRespawnCutscene.pAnimDictName)
			bRespawnAssetsLoading = FALSE
			
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_RESPAWN, "	HAS_ANIM_DICT_LOADED(\"", sRespawnCutscene.pAnimDictName, "\")")
			#ENDIF
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.pFacialDictName)
			REQUEST_ANIM_DICT(sRespawnCutscene.pFacialDictName)
			IF NOT HAS_ANIM_DICT_LOADED(sRespawnCutscene.pFacialDictName)
				bRespawnAssetsLoading = FALSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_RESPAWN, "	HAS_ANIM_DICT_LOADED(\"", sRespawnCutscene.pFacialDictName, "\")")
				#ENDIF
				
			ENDIF
		ENDIF
		
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(0)
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_RESPAWN, "<RESPAWN> loadscene and assets took ", iRespawnTimeoutGametime - GET_GAME_TIMER(), "ms")
	#ENDIF
	
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	
	IF NOT ARE_VECTORS_EQUAL(sRespawnCutscene.vTrafficBounds, <<0,0,0>>)
		SET_ROADS_IN_AREA((sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset)-sRespawnCutscene.vTrafficBounds, (sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset)+sRespawnCutscene.vTrafficBounds, FALSE)
		CLEAR_AREA_OF_VEHICLES((sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset), VMAG(sRespawnCutscene.vTrafficBounds)+25.0)
		CLEAR_AREA((sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset), VMAG(sRespawnCutscene.vTrafficBounds)+25.0, TRUE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	SET_GAME_PAUSED(FALSE)
	
	CLEAR_AREA(sRespawnCutscene.vAnimPos, fRESPAWN_CUTSCENE_RADIUS, TRUE)
	INSTANTLY_FILL_PED_POPULATION()
	INSTANTLY_FILL_VEHICLE_POPULATION()
	
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	// wait for replay processing to finish before fading in for cut-scene
	WHILE IS_REPLAY_BEING_PROCESSED()
		WAIT(0)
		CPRINTLN(DEBUG_RESPAWN, "RUN_RESPAWN_CUTSCENE waiting for replay processing to complete")
	ENDWHILE
	
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
	ENDIF
	
	SET_FADE_IN_AFTER_DEATH_ARREST(TRUE)
	
	BOOL bPedForcedToUseScripCamHeadingFlag
	INT iSceneId
	CAMERA_INDEX respawnCamIndex
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		iSceneId = CREATE_SYNCHRONIZED_SCENE(sRespawnCutscene.vAnimPos, sRespawnCutscene.vAnimRot)
		SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneId, FALSE)
		
		respawnCamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
		
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_DONT_INTERRUPT
		IF (sRespawnCutscene.ePedMotionState <> MS_ON_FOOT_IDLE)
			flags |= SYNCED_SCENE_TAG_SYNC_OUT
		ENDIF
		
		APPLY_DEATHARREST_DAMAGE_DECALS(PLAYER_PED_ID(), sRespawnCutscene.ePlayerDamage)
		TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId,
				sRespawnCutscene.pAnimDictName, sRespawnCutscene.pAnimClipName,
				INSTANT_BLEND_IN, WALK_BLEND_OUT, flags)
		SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
		SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
		
		bPedForcedToUseScripCamHeadingFlag = GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToUseScripCamHeading)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToUseScripCamHeading, TRUE)
		
		PLAY_SYNCHRONIZED_CAM_ANIM(respawnCamIndex,
				iSceneId, sRespawnCutscene.pAnimCamName, sRespawnCutscene.pAnimDictName)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.pFacialDictName)
		AND NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.pFacialClipName)
			CPRINTLN(DEBUG_RESPAWN, "RUN_RESPAWN_CUTSCENE play facial \"", sRespawnCutscene.pFacialDictName, "\",  \"", sRespawnCutscene.pFacialClipName, "\"")
			
			TASK_PLAY_ANIM(PLAYER_PED_ID(), sRespawnCutscene.pFacialDictName, sRespawnCutscene.pFacialClipName,
					INSTANT_BLEND_IN, WALK_BLEND_OUT, 10000,
					AF_SECONDARY | AF_LOOPING)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("player is dead at respawn??")
		#ENDIF
		iSceneId = -1
	ENDIF
	
	IF NOT sRespawnCutscene.bSeenFirstTimeHelp
		IF NOT IS_STRING_NULL_OR_EMPTY(HelpTextLabel)
			PRINT_HELP(HelpTextLabel)
			sRespawnCutscene.bSeenFirstTimeHelp = TRUE
		ENDIF
	ENDIF
	
	BOOL bPlayedsRespawnCutsceneDialogueTextLabel = FALSE, bEffectOrSoundPlayed = FALSE
	structPedsForConversation MyLocalPedStruct
	IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.DialogueTextLabel)
		
		enumCharacterList ePlayerChar	= GET_CURRENT_PLAYER_PED_ENUM()
		
		INT iPlayerVoiceNum = -1
		STRING tPlayerVoiceID = ""
		
		SWITCH ePlayerChar
			CASE CHAR_MICHAEL
				iPlayerVoiceNum = 0
				tPlayerVoiceID = "MICHAEL"
			BREAK
			CASE CHAR_FRANKLIN
				iPlayerVoiceNum = 1
				tPlayerVoiceID = "FRANKLIN"
			BREAK
			CASE CHAR_TREVOR
				iPlayerVoiceNum = 2
				tPlayerVoiceID = "TREVOR"
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("invalid ePlayerChar")
			BREAK
		ENDSWITCH
		
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPlayerVoiceNum, PLAYER_PED_ID(), tPlayerVoiceID)
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RESPAWN, "SYNCHRONIZED_SCENE_RUNNING(", iSceneId, ")")
		#ENDIF
		
		STRING sWalkInterruptible = "WalkInterruptible"
		STRING sForceBlendout = "ForceBlendout"
		
		WHILE IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
			IF NOT bEffectOrSoundPlayed
				IF NOT IS_SCREEN_FADED_OUT()
					IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.effectName)
						CONST_INT durationMs 0		//3000
						ANIMPOSTFX_PLAY(sRespawnCutscene.effectName, durationMs, FALSE)
						
						CPRINTLN(DEBUG_RESPAWN, "RUN_RESPAWN_CUTSCENE animpostfx play \"", sRespawnCutscene.effectName, "\", ", durationMs)
						bEffectOrSoundPlayed = TRUE
					ENDIF
					IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.soundSetName)
						IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.soundNameA)
							PLAY_SOUND_FRONTEND(-1, sRespawnCutscene.soundNameA, sRespawnCutscene.soundSetName, FALSE)
							
							CPRINTLN(DEBUG_RESPAWN, "RUN_RESPAWN_CUTSCENE play sound frontend \"", sRespawnCutscene.soundNameA, "\", \"", sRespawnCutscene.soundSetName, "\"")
							bEffectOrSoundPlayed = TRUE
						ENDIF
						IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.soundNameB)
							PLAY_SOUND_FRONTEND(-1, sRespawnCutscene.soundNameB, sRespawnCutscene.soundSetName, FALSE)
							
							CPRINTLN(DEBUG_RESPAWN, "RUN_RESPAWN_CUTSCENE play sound frontend \"", sRespawnCutscene.soundNameB, "\", \"", sRespawnCutscene.soundSetName, "\"")
							bEffectOrSoundPlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			// Hide the HUD
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			CLEAR_REMINDER_MESSAGE()
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			//Is the global wait timer at 0?
			INT iGameTime = GET_GAME_TIMER()
			IF iGameTime >= g_iGlobalWaitTime - 0500
				ADD_GLOBAL_COMMUNICATION_DELAY(CC_DELAY_POST_AMBIENT_SWITCH / 2)
			ENDIF
			
			FLOAT ReturnStartPhase = -1, ReturnEndPhase = -1
			
			#IF IS_DEBUG_BUILD	IF NOT bUpdateRespawnScene	#ENDIF
			fAnimPhase = GET_SYNCHRONIZED_SCENE_PHASE(iSceneId)
			#IF IS_DEBUG_BUILD	ENDIF						#ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bUpdateRespawnScene
				IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iSceneId)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, TRUE)
					
					REPEAT COUNT_OF(bUpdateDamageDecal) iDamage
						bUpdateDamageDecal[iDamage] = IS_BITMASK_ENUM_AS_ENUM_SET(sRespawnCutscene.ePlayerDamage, INT_TO_ENUM(PLAYER_DAMAGE_ENUM, iPOWER(2, iDamage)))
					ENDREPEAT
					
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				ENDIF
				SET_SYNCHRONIZED_SCENE_ORIGIN(iSceneId, sRespawnCutscene.vAnimPos, sRespawnCutscene.vAnimRot)
				SET_SYNCHRONIZED_SCENE_PHASE(iSceneId, fAnimPhase)
				
				
				IF bTestRespawnCamPichHeading
					IF IS_CAM_RENDERING(respawnCamIndex)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
					
					IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(sRespawnCutscene.gpCamPitch)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(sRespawnCutscene.gpCamHead)
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(sRespawnCutscene.gp3rdCamPitch)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(sRespawnCutscene.gp3rdCamHead)
						
						SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(sRespawnCutscene.gp3rdCamPitch)
						SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(sRespawnCutscene.gp3rdCamHead)
					ENDIF
				ELSE
					IF NOT IS_CAM_RENDERING(respawnCamIndex)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ENDIF
				
				FLOAT fPlayerCoord_groundZ = 0.0
				VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				IF GET_GROUND_Z_FOR_3D_COORD(vPlayerCoord, fPlayerCoord_groundZ)
					fAnimPos_heightDiff = (vPlayerCoord.z - 1) - fPlayerCoord_groundZ
				ELSE
					fAnimPos_heightDiff = 0.0
				ENDIF
				
				BOOL bUpdatedDamageDecals = FALSE
				REPEAT COUNT_OF(bUpdateDamageDecal) iDamage
					
					PLAYER_DAMAGE_ENUM eDamageBit = INT_TO_ENUM(PLAYER_DAMAGE_ENUM, iPOWER(2, iDamage))
					
					IF bUpdateDamageDecal[iDamage] <> IS_BITMASK_ENUM_AS_ENUM_SET(sRespawnCutscene.ePlayerDamage, eDamageBit)
						IF bUpdateDamageDecal[iDamage]
							SET_BITMASK_ENUM_AS_ENUM(sRespawnCutscene.ePlayerDamage, eDamageBit)
						ELSE
							CLEAR_BITMASK_ENUM_AS_ENUM(sRespawnCutscene.ePlayerDamage, eDamageBit)
						ENDIF
						
						bUpdatedDamageDecals = TRUE
					ENDIF
					
				ENDREPEAT
				
				IF bUpdatedDamageDecals
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
						RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
						
						APPLY_DEATHARREST_DAMAGE_DECALS(PLAYER_PED_ID(), sRespawnCutscene.ePlayerDamage)
					ENDIF
				ENDIF
				
				IF g_bDrawLiteralSceneString
					DRAW_DEBUG_BOX((sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset)-sRespawnCutscene.vTrafficBounds,
							(sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset)+sRespawnCutscene.vTrafficBounds, 000,128,000,064)
							
					IF DOES_CAM_EXIST(respawnCamIndex)
						VECTOR vRenderingCamPos = GET_CAM_COORD(respawnCamIndex)
//						VECTOR vRenderingCamOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), vRenderingCamPos)
						VECTOR vRenderingCamRot = GET_CAM_ROT(respawnCamIndex)
						VECTOR vModifiedRenderingCamRot = vRenderingCamRot+ sRespawnCutscene.vPushInDataDirectionMod
//						VECTOR vRenderingCamOffsetRot = vModifiedRenderingCamRot - GET_ENTITY_ROTATION(PLAYER_PED_ID()) 
						VECTOR vRenderingCamDirection = <<-SIN(vModifiedRenderingCamRot.z) * COS(vModifiedRenderingCamRot.x), COS(vModifiedRenderingCamRot.z) * COS(vModifiedRenderingCamRot.x), SIN(vModifiedRenderingCamRot.x)>> 
//						VECTOR vRenderingCamAttachedDirection = <<-SIN(vRenderingCamOffsetRot.z) * COS(vRenderingCamOffsetRot.x), COS(vRenderingCamOffsetRot.z) * COS(vRenderingCamOffsetRot.x), SIN(vRenderingCamOffsetRot.x)>>
						
						DRAW_DEBUG_LINE(vRenderingCamPos + (vRenderingCamDirection * VDIST(vRenderingCamPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))), vRenderingCamPos)
						DRAW_DEBUG_SPHERE(vRenderingCamPos + (vRenderingCamDirection * VDIST(vRenderingCamPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))), 0.1)
					ENDIF
					
				ENDIF
				
				IF IS_KEYBOARD_KEY_PRESSED(KEY_LEFT)
					fAnimPhase -= 0.01
					
					IF (fAnimPhase < 0) fAnimPhase = 0 ENDIF
				ENDIF
				IF IS_KEYBOARD_KEY_PRESSED(KEY_RIGHT)
					fAnimPhase += 0.01
					
					IF (fAnimPhase > 1) fAnimPhase = 1 ENDIF
				ENDIF
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("CASE ")SAVE_STRING_TO_DEBUG_FILE(pSceneDebugName)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.pAnimDictName	= \"")SAVE_STRING_TO_DEBUG_FILE(sRespawnCutscene.pAnimDictName)SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.pAnimClipName	= \"")SAVE_STRING_TO_DEBUG_FILE(sRespawnCutscene.pAnimClipName)SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.pAnimCamName	= \"")SAVE_STRING_TO_DEBUG_FILE(sRespawnCutscene.pAnimCamName)SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.vAnimPos = ")SAVE_VECTOR_TO_DEBUG_FILE(sRespawnCutscene.vAnimPos)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.vAnimRot = ")SAVE_VECTOR_TO_DEBUG_FILE(sRespawnCutscene.vAnimRot)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.ePedMotionState = MS_")
					SWITCH sRespawnCutscene.ePedMotionState
						CASE MS_ON_FOOT_IDLE	SAVE_STRING_TO_DEBUG_FILE("ON_FOOT_IDLE")						BREAK
						CASE MS_ON_FOOT_WALK	SAVE_STRING_TO_DEBUG_FILE("ON_FOOT_WALK")						BREAK
						
						DEFAULT					SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(sRespawnCutscene.ePedMotionState))						BREAK
					ENDSWITCH
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("	")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					IF NOT ARE_VECTORS_EQUAL(sRespawnCutscene.vTrafficBounds, <<0,0,0>>)
						SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.vTrafficOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(sRespawnCutscene.vTrafficOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("	sRespawnCutscene.vTrafficBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(sRespawnCutscene.vTrafficBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
					
					SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					bUpdateRespawnScene = FALSE
				ENDIF
			ELSE
				
				IF IS_SYNCHRONIZED_SCENE_LOOPED(iSceneId)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
				ENDIF
			ENDIF
			#ENDIF
			
			
			#IF IS_DEBUG_BUILD
			str  = ("\"")
			str += (sRespawnCutscene.pAnimDictName)
			str += (", ")
			str += (sRespawnCutscene.pAnimClipName)
			str += ("\"")
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0, HUD_COLOUR_RED)
			
			str  = ("synch scene[")
			str += (iSceneId)
			str += ("] phase: ")
			str += (GET_STRING_FROM_FLOAT(fAnimPhase))
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 1, HUD_COLOUR_ORANGE)
			
			
			REPEAT COUNT_OF(bUpdateDamageDecal) iDamage
				PLAYER_DAMAGE_ENUM eDamageBit = INT_TO_ENUM(PLAYER_DAMAGE_ENUM, iPOWER(2, iDamage))
				
				str  = ("  PD_")
				str += (iDamage)
				IF IS_BITMASK_ENUM_AS_ENUM_SET(sRespawnCutscene.ePlayerDamage, eDamageBit)
					str += (" set")
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(2+iDamage), HUD_COLOUR_REDLIGHT)
				ELSE
					str += (" NOT set")
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(2+iDamage), HUD_COLOUR_GREENLIGHT)
				ENDIF
			ENDREPEAT
			
			#ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.DialogueTextLabel)
				IF NOT bPlayedsRespawnCutsceneDialogueTextLabel
					CPRINTLN(DEBUG_RESPAWN, "wait for ", sRespawnCutscene.fDialoguePhase, " to play \"", sRespawnCutscene.DialogueTextLabel, "\"")
					
					IF (fAnimPhase >= sRespawnCutscene.fDialoguePhase)
						IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", sRespawnCutscene.DialogueTextLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
							bPlayedsRespawnCutsceneDialogueTextLabel = TRUE
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_RESPAWN, "played \"", sRespawnCutscene.DialogueTextLabel, "\"")
				ENDIF
			ENDIF
			
			BOOL bAnimEventHit
			bAnimEventHit = FALSE
			IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY(sWalkInterruptible))
				
				#IF IS_DEBUG_BUILD
				str  = ("\"")
				str += (sWalkInterruptible)
				str += ("\" ")
				str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
				str += (" to ")
				str += GET_STRING_FROM_FLOAT(ReturnEndPhase)
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(2+COUNT_OF(bUpdateDamageDecal)), HUD_COLOUR_ORANGE)
				#ENDIF
				
//				IF (fAnimPhase >= ReturnStartPhase)
//				AND (fAnimPhase <= ReturnEndPhase)
					
					CONST_INT iLEFT_STICK_THRESHOLD		64
	
					INT ReturnLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_X) -128
					INT ReturnLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_Y) -128
					
					IF ReturnLeftX < iLEFT_STICK_THRESHOLD
					AND ReturnLeftX > -iLEFT_STICK_THRESHOLD
					AND ReturnLeftY < iLEFT_STICK_THRESHOLD
					AND ReturnLeftY > -iLEFT_STICK_THRESHOLD
						//
					ELSE
						bAnimEventHit = TRUE
					ENDIF
//				ENDIF
			ELSE
				ReturnStartPhase = -1
				ReturnEndPhase = -1
				bAnimEventHit = FALSE
				
				sRespawnCutscene.fRollingBlendoutPhase = sRespawnCutscene.fRollingBlendoutPhase
				ReturnStartPhase = ReturnStartPhase
				ReturnEndPhase = ReturnEndPhase
			ENDIF
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			AND sRespawnCutscene.fFirstPersonPunchPhase > 0
				
				#IF IS_DEBUG_BUILD
				str  = ("1st person phase: ")
				str += GET_STRING_FROM_FLOAT(fAnimPhase)
				str += (" >= ")
				str += GET_STRING_FROM_FLOAT(sRespawnCutscene.fFirstPersonPunchPhase)
				HUD_COLOURS ePushin_hud_colour = HUD_COLOUR_PINK
				#ENDIF
				
				
				IF (fAnimPhase >= sRespawnCutscene.fFirstPersonPunchPhase)
				
					IF NOT sRespawnCutscene.bStartedPushin
						FLOAT fPushinDist = PUSH_IN_DISTANCE
						VECTOR vCamCoord = GET_FINAL_RENDERED_CAM_COORD()
						VECTOR vPlayerCoord = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0,0,0>>)
						FLOAT fCoordDist3d = GET_DISTANCE_BETWEEN_COORDS(vCamCoord, vPlayerCoord)
						
						CONST_FLOAT fPUSHIN_DIST_BUFFER	0.75
						fPushinDist = CLAMP(fPushinDist, 0.001, fCoordDist3d-fPUSHIN_DIST_BUFFER)
						IF fPushinDist < 0
							CWARNINGLN(DEBUG_RESPAWN, "fPushinDist is less than zero!! ", fPushinDist)
							fPushinDist  = 0.001
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF fPushinDist = PUSH_IN_DISTANCE
							CPRINTLN(DEBUG_RESPAWN, "PUSH_IN fCoordDist3d: ", fCoordDist3d, ", fPushinDist: PUSH_IN_DISTANCE	//", pSceneDebugName)
						ELSE
							CPRINTLN(DEBUG_RESPAWN, "PUSH_IN fCoordDist3d: ", fCoordDist3d, ", fPushinDist: ", fPushinDist, "	//", pSceneDebugName)
						ENDIF
						#ENDIF
						
						FILL_PUSH_IN_DATA(sRespawnCutscene.sPushInData, PLAYER_PED_ID(), GET_CURRENT_PLAYER_PED_ENUM(),
								0.000001,			//FLOAT fDistance = PUSH_IN_DISTANCE
								300,					//INT iInterpTime = PUSH_IN_INTERP_TIME
								300,					//INT iCutTime = PUSH_IN_CUT_TIME
								000,					//INT iPostFXTime = PUSH_IN_POSTFX_TIME
								000,					//INT iSpeedUpTime = 0
								DEFAULT)				//FLOAT fSpeedUpProportion = PUSH_IN_SPEED_UP_PROPORTION)
						SET_PUSH_IN_DIRECTION_MODIFIER(sRespawnCutscene.sPushInData, sRespawnCutscene.vPushInDataDirectionMod)
						
						// rob - short-circuit the creation of push-in cams to keep the original cam moving round till the cut
						IF NOT sRespawnCutscene.bShortCircuitedPushin
							sRespawnCutscene.sPushInData.state = PUSH_IN_RUNNING
							sRespawnCutscene.sPushInData.iCreateTime = GET_GAME_TIMER()
							sRespawnCutscene.sPushInData.bDonePostFX = FALSE	
							sRespawnCutscene.bShortCircuitedPushin = TRUE
						ENDIF
					
						CPRINTLN(DEBUG_RESPAWN, "PUSH_IN vDirectionMod: ", sRespawnCutscene.sPushInData.vDirectionMod)
				
						// // // // // // // // // //
						FLOAT pitch, heading
						IF (sRespawnCutscene.gp3rdCamHead = 999.0)
							sRespawnCutscene.gp3rdCamPitch = 0.0
						ENDIF
						
						IF (sRespawnCutscene.gp3rdCamHead = 999.0)
							VECTOR vecCamEndRot
							FLOAT fCreateHead
							vecCamEndRot = GET_FINAL_RENDERED_CAM_ROT()
							fCreateHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
							
							FLOAT finalSplineCameraHeading, playerHeading
							finalSplineCameraHeading = vecCamEndRot.z
							playerHeading = fCreateHead
							sRespawnCutscene.gp3rdCamHead = finalSplineCameraHeading - playerHeading
							
							IF (sRespawnCutscene.gp3rdCamHead < 360) sRespawnCutscene.gp3rdCamHead += 360 ENDIF
							IF (sRespawnCutscene.gp3rdCamHead > 360) sRespawnCutscene.gp3rdCamHead -= 360 ENDIF
						ENDIF
						pitch = sRespawnCutscene.gp3rdCamPitch
						heading = sRespawnCutscene.gp3rdCamHead
						
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(pitch)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(heading)
						
						SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(pitch)
						SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(heading)
						
						VECTOR camIndexCoord, gamcamIndexCoord
						FLOAT fCamInterpDist
						INT iCamInterpTime
						camIndexCoord = GET_FINAL_RENDERED_CAM_COORD()
						gamcamIndexCoord = GET_GAMEPLAY_CAM_COORD()
						fCamInterpDist = GET_DISTANCE_BETWEEN_COORDS(camIndexCoord, gamcamIndexCoord) 
						iCamInterpTime = ROUND(fCamInterpDist*1000)
						
						CPRINTLN(DEBUG_RESPAWN, "SET_GAMEPLAY_CAM first person(pitch: ", pitch, ", heading:", heading, ", duration:", iCamInterpTime, ")")
						// // // // // // // // // //
						
						sRespawnCutscene.bStartedPushin = TRUE
					ENDIF
					
					IF sRespawnCutscene.bStartedPushin
						DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
						IF HANDLE_PUSH_IN(sRespawnCutscene.sPushInData,
								TRUE,		// BOOL bCamsAttached = FALSE
								TRUE,		// BOOL bDestroyCamsAtEnd = TRUE
								DEFAULT,	// BOOL bDoPostFX = TRUE
								DEFAULT,	// BOOL bOnlyAttachStartIfCamsAttached = FALSE
								FALSE)	// BOOL bDoColouredFlash = TRUE)
							#IF IS_DEBUG_BUILD
							ePushin_hud_colour = HUD_COLOUR_REDLIGHT
							str += (" [pushed in!]")
							CPRINTLN(DEBUG_RESPAWN, str)
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							ePushin_hud_colour = HUD_COLOUR_REDDARK
							str += (" [pushing in...]")
							CPRINTLN(DEBUG_RESPAWN, str)
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(3+COUNT_OF(bUpdateDamageDecal)), ePushin_hud_colour)
				#ENDIF
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY(sForceBlendout))
			OR bAnimEventHit
				#IF IS_DEBUG_BUILD	IF NOT bUpdateRespawnScene	#ENDIF
				
//				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//				AND sRespawnCutscene.fFirstPersonPunchPhase > 0
//					IF NOT sRespawnCutscene.bStartedPushin
//						CASSERTLN(DEBUG_RESPAWN, "respawn pushin phase[", sRespawnCutscene.fFirstPersonPunchPhase, "] is greater than force blend out phase [", fAnimPhase, "] so it'd never hit - consider changing!")
//					ENDIF
//				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SWITCH sRespawnCutscene.ePedMotionState
						CASE MS_ON_FOOT_IDLE
							CPRINTLN(DEBUG_RESPAWN, "FORCE_PED_MOTION_STATE(MS_ON_FOOT_IDLE) 2: ", fAnimPhase)
							
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
//							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 0500)
							
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
						BREAK
						CASE MS_ON_FOOT_WALK
							CPRINTLN(DEBUG_RESPAWN, "FORCE_PED_MOTION_STATE(MS_ON_FOOT_WALK) 2: ", fAnimPhase)
							
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE)
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 0500)
							
							// // // // // // // // // //
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								FLOAT pitch, heading
								IF (sRespawnCutscene.gpCamHead = 999.0)
									sRespawnCutscene.gpCamPitch= 0.0
								ENDIF
								
								IF (sRespawnCutscene.gpCamHead = 999.0)
									VECTOR vecCamEndRot
									FLOAT fCreateHead
									vecCamEndRot = GET_FINAL_RENDERED_CAM_ROT()
									fCreateHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
									
									FLOAT finalSplineCameraHeading, playerHeading
									finalSplineCameraHeading = vecCamEndRot.z
									playerHeading = fCreateHead
									sRespawnCutscene.gpCamHead = finalSplineCameraHeading - playerHeading
									
									IF (sRespawnCutscene.gpCamHead < 360) sRespawnCutscene.gpCamHead += 360 ENDIF
									IF (sRespawnCutscene.gpCamHead > 360) sRespawnCutscene.gpCamHead -= 360 ENDIF
								ENDIF
								pitch = sRespawnCutscene.gpCamPitch
								heading = sRespawnCutscene.gpCamHead
								
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(pitch)
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(heading)
								
								VECTOR camIndexCoord, gamcamIndexCoord
								FLOAT fCamInterpDist
								INT iCamInterpTime
								camIndexCoord = GET_FINAL_RENDERED_CAM_COORD()
								gamcamIndexCoord = GET_GAMEPLAY_CAM_COORD()
								fCamInterpDist = GET_DISTANCE_BETWEEN_COORDS(camIndexCoord, gamcamIndexCoord) 
								iCamInterpTime = ROUND(fCamInterpDist*1000)
								
								CPRINTLN(DEBUG_RESPAWN, "SET_GAMEPLAY_CAM(pitch: ", pitch, ", heading:", heading, ", duration:", iCamInterpTime, ")")
								// // // // // // // // // //
								
								
								RENDER_SCRIPT_CAMS(FALSE, TRUE, iCamInterpTime, FALSE)
							ELSE
//								// // // // // // // // // //
//								IF (sRespawnCutscene.gp3rdCamHead = 999.0)
//									sRespawnCutscene.gp3rdCamPitch = 0.0
//								ENDIF
//								
//								IF (sRespawnCutscene.gp3rdCamHead = 999.0)
//									VECTOR vecCamEndRot
//									FLOAT fCreateHead
//									vecCamEndRot = GET_FINAL_RENDERED_CAM_ROT()
//									fCreateHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
//									
//									FLOAT finalSplineCameraHeading, playerHeading
//									finalSplineCameraHeading = vecCamEndRot.z
//									playerHeading = fCreateHead
//									sRespawnCutscene.gp3rdCamHead = finalSplineCameraHeading - playerHeading
//									
//									IF (sRespawnCutscene.gp3rdCamHead < 360) sRespawnCutscene.gp3rdCamHead += 360 ENDIF
//									IF (sRespawnCutscene.gp3rdCamHead > 360) sRespawnCutscene.gp3rdCamHead -= 360 ENDIF
//								ENDIF
//								pitch = sRespawnCutscene.gp3rdCamPitch
//								heading = sRespawnCutscene.gp3rdCamHead
//								
//								SET_GAMEPLAY_CAM_RELATIVE_PITCH(pitch)
//								SET_GAMEPLAY_CAM_RELATIVE_HEADING(heading)
//								
//								VECTOR camIndexCoord, gamcamIndexCoord
//								FLOAT fCamInterpDist
//								INT iCamInterpTime
//								camIndexCoord = GET_FINAL_RENDERED_CAM_COORD()
//								gamcamIndexCoord = GET_GAMEPLAY_CAM_COORD()
//								fCamInterpDist = GET_DISTANCE_BETWEEN_COORDS(camIndexCoord, gamcamIndexCoord) 
//								iCamInterpTime = ROUND(fCamInterpDist*1000)
//								
//								CPRINTLN(DEBUG_RESPAWN, "SET_GAMEPLAY_CAM first person(pitch: ", pitch, ", heading:", heading, ", duration:", iCamInterpTime, ")")
//								// // // // // // // // // //
								
//								RENDER_SCRIPT_CAMS(FALSE, TRUE, iCamInterpTime, FALSE)
							ENDIF
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_RESPAWN, "FORCE_PED_MOTION_STATE(MS_unknown_", ENUM_TO_INT(sRespawnCutscene.ePedMotionState), ") 2: ", fAnimPhase)
							
							SCRIPT_ASSERT("invalid sRespawnCutscene.ePedMotionState")
						BREAK
					ENDSWITCH
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
						DETACH_SYNCHRONIZED_SCENE(iSceneId)
						iSceneId = -1
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD	ENDIF						#ENDIF
				
			ENDIF
			
			WAIT(0)
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RESPAWN, "SYNCHRONIZED_SCENE_NOT_RUNNING(", iSceneId, ")")
		#ENDIF
	ENDIF
	
	
	
	IF NOT ARE_VECTORS_EQUAL(sRespawnCutscene.vTrafficBounds, <<0,0,0>>)
		SET_ROADS_BACK_TO_ORIGINAL((sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset)-sRespawnCutscene.vTrafficBounds, (sRespawnCutscene.vAnimPos+sRespawnCutscene.vTrafficOffset)+sRespawnCutscene.vTrafficBounds)
	ENDIF
	
//	REMOVE_SCENARIO_BLOCKING_AREA(respawnScenarioBlockID)
//	CLEAR_PED_NON_CREATION_AREA()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	
	BOOL bIS_INTERPOLATING_FROM_SCRIPT_CAMS = IS_INTERPOLATING_FROM_SCRIPT_CAMS()
	BOOL bHANDLE_PUSH_IN = FALSE
	IF sRespawnCutscene.bStartedPushin
		DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
		IF NOT HANDLE_PUSH_IN(sRespawnCutscene.sPushInData,
				TRUE,		// BOOL bCamsAttached = FALSE
				TRUE,		// BOOL bDestroyCamsAtEnd = TRUE
				DEFAULT,	// BOOL bDoPostFX = TRUE
				DEFAULT,	// BOOL bOnlyAttachStartIfCamsAttached = FALSE
				DEFAULT)	// BOOL bDoColouredFlash = TRUE)
			bHANDLE_PUSH_IN = TRUE
		ENDIF
	ENDIF
	
	IF bIS_INTERPOLATING_FROM_SCRIPT_CAMS
	OR bHANDLE_PUSH_IN
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RESPAWN, "IS_INTERPOLATING_FROM_SCRIPT_CAMS(", ")")
		#ENDIF
		
		WHILE (bIS_INTERPOLATING_FROM_SCRIPT_CAMS
		OR bHANDLE_PUSH_IN)
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS() 	// Fix for 2399685. 
												// Guard against getting stuck in this while when starting a switch scene during the interp.
			IF bIS_INTERPOLATING_FROM_SCRIPT_CAMS
				#IF IS_DEBUG_BUILD
				str  = ("IS_INTERPOLATING_FROM_SCRIPT_CAMS...")
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0.0, HUD_COLOUR_PURE_WHITE)
				CPRINTLN(DEBUG_RESPAWN, str)
				#ENDIF
			ENDIF
			IF bHANDLE_PUSH_IN
				#IF IS_DEBUG_BUILD
				str  = ("bHANDLE_PUSH_IN...")
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 1.0, HUD_COLOUR_PURE_WHITE)
				
				CPRINTLN(DEBUG_RESPAWN, str)
				#ENDIF
			ENDIF
				
			INT iOffset_y = 2
			DisableControlAction(CAMERA_CONTROL, INPUT_LOOK_BEHIND,				iOffset_y)
			DisableControlAction(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND,			iOffset_y)
			DisableControlAction(CAMERA_CONTROL, INPUT_LOOK_LR,					iOffset_y)
			DisableControlAction(CAMERA_CONTROL, INPUT_LOOK_UD,					iOffset_y)
			
//			DisableControlAction(PLAYER_CONTROL, INPUT_SPRINT,					iOffset_y)
			DisableControlAction(PLAYER_CONTROL, INPUT_JUMP,					iOffset_y)
			DisableControlAction(PLAYER_CONTROL, INPUT_DUCK,					iOffset_y)
			
			DisableControlAction(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE,	iOffset_y)
			DisableControlAction(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY,		iOffset_y)
			DisableControlAction(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT,		iOffset_y)
			DisableControlAction(PLAYER_CONTROL, INPUT_MELEE_ATTACK1,			iOffset_y)
			DisableControlAction(PLAYER_CONTROL, INPUT_MELEE_ATTACK2,			iOffset_y)
			
			WAIT(0)
			
			bIS_INTERPOLATING_FROM_SCRIPT_CAMS = IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			bHANDLE_PUSH_IN = FALSE
			IF sRespawnCutscene.bStartedPushin
				DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				IF NOT HANDLE_PUSH_IN(sRespawnCutscene.sPushInData,
						TRUE,		// BOOL bCamsAttached = FALSE
						TRUE,		// BOOL bDestroyCamsAtEnd = TRUE
						DEFAULT,	// BOOL bDoPostFX = TRUE
						DEFAULT,	// BOOL bOnlyAttachStartIfCamsAttached = FALSE
						DEFAULT)	// BOOL bDoColouredFlash = TRUE)
					bHANDLE_PUSH_IN = TRUE
				ENDIF
			ENDIF
		ENDWHILE
	ELSE
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RESPAWN, "WASNT_INTERPOLATING_FROM_SCRIPT_CAMS(", ")")
		#ENDIF
		
	ENDIF
	

	
	#IF IS_DEBUG_BUILD
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS() // Fix for 2399685. 
		PRINTLN("[RESPAWN] Bailed early from a respawn interp-out cam due to a switch scene starting. (2399685)")
	ENDIF
	
	IF DOES_WIDGET_GROUP_EXIST(first_time_deatharrest_widget_group_id)
		DELETE_WIDGET_GROUP(first_time_deatharrest_widget_group_id)
	ENDIF
	#ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sRespawnCutscene.pFacialDictName)
		REMOVE_ANIM_DICT(sRespawnCutscene.pFacialDictName)
	ENDIF
	
	REMOVE_ANIM_DICT(sRespawnCutscene.pAnimDictName)
	DESTROY_CAM(respawnCamIndex)
	RESET_PUSH_IN(sRespawnCutscene.sPushInData)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToUseScripCamHeading, bPedForcedToUseScripCamHeadingFlag)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	pSceneDebugName = pSceneDebugName
ENDPROC
