//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 30/05/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					 		Respawn Cleanup Data Header							│
//│																				│
//│			Contains definitions for custom cleanup blocks that can be			│
//│			set to run on respawn. Useful for missions that need to queue		│
//│			up cleanup events for after the screen has faded on death/arrest.	│
//│																				│
//│			NB. This header should only ever be included once by the respawn	│
//│			controller as it may contain variable declarations.					│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_streaming.sch"
USING "blip_control_public.sch"
USING "building_control_public.sch"


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ Debug ╞═══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

FUNC STRING GET_DEBUG_STRING_FROM_RESPAWN_CLEANUP_ID(RESPAWN_CLEANUP_ID paramRCI)

	SWITCH paramRCI
		CASE RCI_NONE			RETURN "RCI_NONE"			BREAK
		CASE RCI_ARMENIAN2		RETURN "RCI_ARMENIAN2"		BREAK
		CASE RCI_BIGSCORE2B		RETURN "RCI_BIGSCORE2B"		BREAK
		CASE RCI_FAMILY2		RETURN "RCI_FAMILY2"		BREAK
		CASE RCI_FBI3			RETURN "RCI_FBI3"			BREAK
		CASE RCI_MICHAEL1		RETURN "RCI_MICHAEL1"		BREAK
		CASE RCI_PROLOGUE		RETURN "RCI_PROLOGUE"		BREAK
		CASE RCI_TREVOR2		RETURN "RCI_TREVOR2"		BREAK
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ Custom cleanup routines  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// RCI_ARMENIAN2 - Cleans up the opened doors in the shootout.
FUNC BOOL RUN_RCI_ARMENIAN2()
	SET_DOOR_STATE(DOORNAME_ARM2_HENCH_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1104.6572,-1638.4814,4.6754>>, 5.0, PROP_MAP_DOOR_01)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_MAP_DOOR_01, <<-1104.6572,-1638.4814,4.6754>>, TRUE, 0.0)
	ENDIF
	
	RETURN TRUE
ENDFUNC

// RCI_BIGSCORE2B - Cleans up the drilled tunnel rayfire.
FUNC BOOL RUN_RCI_BIGSCORE2B()
	
	RAYFIRE_INDEX rayfire_tunnel = 	GET_RAYFIRE_MAP_OBJECT(<<7.25, -656.98, 17.14>>, 50.0, "des_finale_tunnel")
	RAYFIRE_INDEX rayfire_vault = 	GET_RAYFIRE_MAP_OBJECT(<<7.25, -656.98, 17.14>>, 50.0, "des_finale_vault")
	
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfire_tunnel) 
    	SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfire_tunnel, RFMO_STATE_RESET) 
	ENDIF 
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfire_vault) 
		SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfire_vault, RFMO_STATE_RESET) 
	ENDIF
	
	RETURN TRUE
ENDFUNC


// RCI_FAMILY2 - Cleans up the cruise ship IPL after the death/arrest fade.
FUNC BOOL RUN_RCI_FAMILY2()
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_RESPAWN, "Respawn cleanup ID waiting for load scene before swapping IPLs.")
		RETURN FALSE
	ENDIF

	REMOVE_IPL("smboat")
	
	RETURN TRUE
ENDFUNC


// RCI_FBI3 - Cleans up the party music after the death/arrest fade
FUNC BOOL RUN_RCI_FBI3()
	IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TORTURE_ZOOM")
		STOP_AUDIO_SCENE("FBI_3_TORTURE_ZOOM") 
	ENDIF
	STOP_STREAM()
	
	RETURN TRUE
ENDFUNC


//RCI_MICHAEL1 - Cleans up the proglogue map after death/arrest fade.
FUNC BOOL RUN_RCI_MICHAEL1()
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_RESPAWN, "Respawn cleanup ID waiting for load scene before swapping IPLs.")
		RETURN FALSE
	ENDIF
	
	REMOVE_IPL("prologue01")
	REMOVE_IPL("prologue01c")
	REMOVE_IPL("prologue01d")
	REMOVE_IPL("prologue01e")
	REMOVE_IPL("prologue01f")
	REMOVE_IPL("prologue01g")
	REMOVE_IPL("prologue01h")
	REMOVE_IPL("prologue01i")
	REMOVE_IPL("prologue01j")
	REMOVE_IPL("prologue01k")
	REMOVE_IPL("prologue01z")
	
	REMOVE_IPL("prologue02")
	
	REMOVE_IPL("prologue03")
	REMOVE_IPL("prologue03b")
	
	REMOVE_IPL("prologue04")
	REMOVE_IPL("prologue04b")
	
	REMOVE_IPL("prologue05")
	REMOVE_IPL("prologue05b")
	
	REMOVE_IPL("prologue06")
	REMOVE_IPL("prologue06b")
	
	REMOVE_IPL("prologuerd")
	REMOVE_IPL("prologuerdb")
	
	REMOVE_IPL("prologue_occl")
	
	REMOVE_IPL("prologue_m2_door")		//cash depot door
	
	REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_LODLights")
	REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_DistantLights")
	
	//remove grave IPLs
	REMOVE_IPL("prologue03_grv_cov")	//grave covered up
	REMOVE_IPL("prologue03_grv_dug")	//grave dug up
	REMOVE_IPL("prologue_grv_torch")	//grave torch

	//call this to enable/disable prologue 2D map, from blip_control_public.sch
	SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)
	
	//clear the weather type from snow which was in the prologue area
	CLEAR_TIMECYCLE_MODIFIER()
	DISABLE_MOON_CYCLE_OVERRIDE()
	
	IF NOT NETWORK_IS_SESSION_ACTIVE()
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		CLEAR_WEATHER_TYPE_PERSIST()
	ELSE
		SET_OVERRIDE_WEATHER("EXTRASUNNY")
	ENDIF
	
	//Road nodes.
	SET_ALLOW_STREAM_PROLOGUE_NODES(FALSE)
	
	//clear clouds setting
	UNLOAD_ALL_CLOUD_HATS()
	
	//delete all trains from prologue map
	DELETE_ALL_TRAINS()
	
	SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(TRUE)
	
	RETURN TRUE
ENDFUNC


//RCI_PROLOGUE- Cleans up the proglogue map after death/arrest fade.
FUNC BOOL RUN_RCI_PROLOGUE()
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_RESPAWN, "Respawn cleanup ID waiting for load scene before swapping IPLs.")
		RETURN FALSE
	ENDIF

	//IPL Groups
	REMOVE_IPL("prologue01")
	REMOVE_IPL("prologue02")
	REMOVE_IPL("prologue03")
	REMOVE_IPL("prologue04")
	REMOVE_IPL("prologue05")
	REMOVE_IPL("prologue06")
	REMOVE_IPL("prologuerd")
	REMOVE_IPL("Prologue01c")
	REMOVE_IPL("Prologue01d")
	REMOVE_IPL("Prologue01e")
	REMOVE_IPL("Prologue01f")
	REMOVE_IPL("Prologue01g")
	REMOVE_IPL("prologue01h")
	REMOVE_IPL("prologue01i")
	REMOVE_IPL("prologue01j")
	REMOVE_IPL("prologue01k")
	REMOVE_IPL("prologue01z")
	REMOVE_IPL("prologue03b")
	REMOVE_IPL("prologue04b")
	REMOVE_IPL("prologue05b")
	REMOVE_IPL("prologue06b")
	REMOVE_IPL("prologuerdb")
	REMOVE_IPL("prologue_occl")
	REMOVE_IPL("prologue06_int")
	REMOVE_IPL("prologue04_cover")
	REMOVE_IPL("prologue03_grv_dug")
	REMOVE_IPL("prologue03_grv_cov")
	REMOVE_IPL("prologue03_grv_fun")
	REMOVE_IPL("prologue_grv_torch")
	REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_DistantLights")
	REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_LODLights")
	REMOVE_IPL("DES_ProTree_start")
	REMOVE_IPL("DES_ProTree_start_lod")
	
	//Cullbox
	SET_MAPDATACULLBOX_ENABLED("Prologue_Main", FALSE)
	
	//Road nodes.
	SET_ALLOW_STREAM_PROLOGUE_NODES(FALSE)
	
	//Weather
	UNLOAD_ALL_CLOUD_HATS()
	
	IF NOT NETWORK_IS_SESSION_ACTIVE()
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		CLEAR_WEATHER_TYPE_PERSIST()
	ELSE
		SET_OVERRIDE_WEATHER("EXTRASUNNY")
	ENDIF
	
	//Minimap
	SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)
	
	SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(TRUE)

	RETURN TRUE
ENDFUNC


//RCI_TREVOR2- Cleans up the airfield IPL group after death/arrest.
FUNC BOOL RUN_RCI_TREVOR2()
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_RESPAWN, "Respawn cleanup ID waiting for load scene before swapping IPLs.")
		RETURN FALSE
	ENDIF

	REMOVE_IPL("airfield")
	
	RETURN TRUE
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Respawn Controller routines ╞═══════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL RUN_CUSTOM_CLEANUP_ON_RESPAWN()
	IF g_eTriggerRCI != RCI_NONE
		CPRINTLN(DEBUG_RESPAWN, "Running respawn cleanup ID ", GET_DEBUG_STRING_FROM_RESPAWN_CLEANUP_ID(g_eTriggerRCI), ".")
		
		BOOL bComplete = FALSE
		SWITCH g_eTriggerRCI
			//Link RCI enums to custom routines here.
			CASE RCI_ARMENIAN2		bComplete = RUN_RCI_ARMENIAN2()		BREAK
			CASE RCI_BIGSCORE2B		bComplete = RUN_RCI_BIGSCORE2B()	BREAK
			CASE RCI_FAMILY2		bComplete = RUN_RCI_FAMILY2()		BREAK	
			CASE RCI_FBI3			bComplete = RUN_RCI_FBI3()			BREAK
			CASE RCI_MICHAEL1		bComplete = RUN_RCI_MICHAEL1()		BREAK	
			CASE RCI_PROLOGUE		bComplete = RUN_RCI_PROLOGUE()		BREAK
			CASE RCI_TREVOR2		bComplete = RUN_RCI_TREVOR2()		BREAK
			
			DEFAULT	
				bComplete = TRUE
				SCRIPT_ASSERT("RUN_CUSTOM_CLEANUP_ON_RESPAWN: A Respawn Cleanup ID was passed that did not have an assigned cleanup procedure. Bug BenR")
			BREAK
		ENDSWITCH
		
		IF bComplete
			CPRINTLN(DEBUG_RESPAWN, "Respawn cleanup ID ", GET_DEBUG_STRING_FROM_RESPAWN_CLEANUP_ID(g_eTriggerRCI), " completed.")
			g_eTriggerRCI = RCI_NONE
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC
