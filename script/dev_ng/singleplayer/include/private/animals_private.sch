//╒═════════════════════════════════════════════════════════════════════════════╕
//│               Author:  Ben Rollinson + Luke Austin    Date: 12/12/14          │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│                                                                               │
//│                           Animals Private Header                              │
//│                                                                               │
//│       Useful general functionality for managing playing as an animal.         │
//│                                                                               │
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "globals.sch"
USING "ambient_globals.sch"
USING "rage_builtins.sch"

USING "commands_pad.sch"
USING "commands_player.sch"
USING "commands_graphics.sch"
USING "commands_misc.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "commands_audio.sch"
USING "commands_event.sch"
USING "commands_stats.sch"

USING "animals_private.sch"
USING "candidate_public.sch"
USING "player_ped_public.sch"
USING "shop_public.sch"
USING "snapshot_private.sch"
USING "randomChar_public.sch"
USING "cheat_controller_public.sch"
USING "vehicle_gen_public.sch"
USING "taxi_functions.sch"
USING "common_packages.sch"
USING "scrap_common.sch"
USING "clothes_shop_private.sch"

STRUCT AnimalSoundData
    TEXT_LABEL_63 strBank
    TEXT_LABEL_31 strSound
    TEXT_LABEL_15 strBarkBank
    TEXT_LABEL_63 strBarkSound
ENDSTRUCT

STRUCT AnimalAnimationData
    STRING strDict
    STRING strAnim
    STRING strCamAnim
    STRING strIdleDict
    STRING strIdleAnim
    TEXT_LABEL_63 strBarkDict
    TEXT_LABEL_15 strBarkAnim
ENDSTRUCT

FUNC BOOL Is_Model_A_Playable_Animal(MODEL_NAMES eModel)
    SWITCH eModel
        CASE A_C_BOAR           
        CASE A_C_CAT_01     
        CASE A_C_COW            
        CASE A_C_COYOTE     
        CASE A_C_DEER   
        CASE A_C_HUSKY
        CASE A_C_MTLION     
        CASE A_C_PIG        
        CASE A_C_POODLE         
        CASE A_C_PUG            
        CASE A_C_RABBIT_01  
        CASE A_C_RETRIEVER  
        CASE A_C_ROTTWEILER 
        CASE A_C_SHEPHERD   
        CASE A_C_WESTY      
        CASE A_C_CHICKENHAWK    
        CASE A_C_CORMORANT  
        CASE A_C_CROW           
        CASE A_C_HEN            
        CASE A_C_PIGEON     
        CASE A_C_SEAGULL        
        CASE A_C_DOLPHIN        
        CASE A_C_FISH           
        CASE A_C_KILLERWHALE    
        CASE A_C_SHARKHAMMER    
        CASE A_C_SHARKTIGER 
        CASE A_C_STINGRAY
        CASE IG_ORLEANS
            RETURN TRUE
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC


FUNC BOOL Is_Model_A_Flying_Animal(MODEL_NAMES eModel)
    SWITCH eModel
        CASE A_C_CHICKENHAWK    
        CASE A_C_CORMORANT  
        CASE A_C_CROW           
        CASE A_C_HEN            
        CASE A_C_PIGEON     
        CASE A_C_SEAGULL 
            RETURN TRUE
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC


FUNC BOOL Can_Player_Model_Enter_Water(BOOL bOrleans = TRUE)
    SWITCH GET_PLAYER_MODEL()
        CASE A_C_DOLPHIN                
        CASE A_C_FISH                                   
        CASE A_C_KILLERWHALE       
        CASE A_C_SHARKHAMMER       
        CASE A_C_SHARKTIGER              
        CASE A_C_STINGRAY 
            RETURN TRUE
        CASE IG_ORLEANS 
            RETURN bOrleans
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC


FUNC BOOL Is_Player_Model_Out_Of_Water()
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
            IF Can_Player_Model_Enter_Water(FALSE)
                //(fish) ped not in water               
                IF g_iAnimalWaterTimer = -1 
                    g_iAnimalWaterTimer = GET_GAME_TIMER() + 3000
                ELIF GET_GAME_TIMER() > g_iAnimalWaterTimer
                    RETURN TRUE
                ENDIF
            ENDIF
        ELSE
            g_iAnimalWaterTimer = -1 
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC


FUNC BOOL Is_Player_Model_Too_Deep_In_Water(INT &crushDepthTimer)
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
            IF NOT Can_Player_Model_Enter_Water()
                FLOAT fWaterHeight
                VECTOR vHeadCoords = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0,0,0>>)
                
                //Probe from a little above the head height so that if the animal is under water we still get valid results.
                vHeadCoords.z += 0.75
                
                IF TEST_VERTICAL_PROBE_AGAINST_ALL_WATER(vHeadCoords, SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_RIVER, fWaterHeight) = SCRIPT_WATER_TEST_RESULT_WATER
                    CDEBUG2LN(DEBUG_ANIMAL_MODE, "Relative water height to head: ", vHeadCoords.z - fWaterHeight - 0.75, ".")
                
                    FLOAT limit = -0.32
                    
                    //B* - 2044883
                    IF GET_PLAYER_MODEL() = A_C_HEN
                        CDEBUG2LN(DEBUG_ANIMAL_MODE, "We're a hen")
                        limit = -0.24
                    ENDIF
                
                    IF (vHeadCoords.z - fWaterHeight - 0.75) < limit
                        RETURN TRUE
                    ENDIF
                ENDIF
            ENDIF
            //B*-2185923, kill sasquatch if too deep
            VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
            IF vPlayerPos.z <= -150.0
                IF GET_PLAYER_MODEL() = IG_ORLEANS
                    INT health = GET_ENTITY_HEALTH(PLAYER_PED_ID())
                    IF health < 500
                        RETURN TRUE
                    ELSE
                        IF GET_GAME_TIMER() > crushDepthTimer+2000
                            SET_ENTITY_HEALTH(PLAYER_PED_ID(),health-500)
                            crushDepthTimer = GET_GAME_TIMER()
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
        //B*-2186997, killboxes for small animals in waterfalls
        IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<-1031.0200, -399.0387, 37.4325>>) < 40.0
            IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1027.6340, -397.8321, 38.1158>>,<<-1026.0916, -397.0680, 37.5389>>,1.5)
                RETURN TRUE
            ENDIF
            IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1033.1865, -400.7195, 38.1248>>,<<-1034.7971, -401.5790, 37.6024>>,1.5)
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC


FUNC BOOL Should_Animal_Mode_Exit(INT &crushDepthTimer)
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())

        //Is the player too deep in water when they can't swim?
        IF Is_Player_Model_Too_Deep_In_Water(crushDepthTimer)
            CPRINTLN(DEBUG_ANIMAL_MODE, "Exit condition fired. The player is in water and the model has no swim animations.")
            RETURN TRUE
        ENDIF
        
        //Is the player on land when they can only swim?
        IF Is_Player_Model_Out_Of_Water()
            CPRINTLN(DEBUG_ANIMAL_MODE, "Exit condition fired. The player is out of water and the model has no walk animations.")
            RETURN TRUE
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC


PROC Activate_Animal_Restricted_State(MODEL_NAMES eAnimalModel, BOOL bHideHud = TRUE, BOOL shockingEvent = TRUE)
    CPRINTLN(DEBUG_ANIMAL_MODE, "Activating animal restricted mode.")
    
    IF NOT Is_Model_A_Playable_Animal(eAnimalModel)
        CPRINTLN(DEBUG_ANIMAL_MODE, "Tried to activate restricted animal state for an invalid model.")
        SCRIPT_ASSERT("Tried to activate restricted animal state for an invalid model. Exiting.")
        EXIT
    ENDIF
    
    IF bHideHud
        DISPLAY_RADAR(FALSE) 
        
        IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
            THEFEED_PAUSE()
        ENDIF
    ENDIF

    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
    
        IF eAnimalModel != IG_ORLEANS
            SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
            REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
            SET_ENTITY_MAX_HEALTH(PLAYER_PED_ID(), 200)
            SET_ENTITY_HEALTH(PLAYER_PED_ID(), 200)
        ELSE
            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, TRUE)
            SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 100.0)
            SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 5000)
            SET_ENTITY_HEALTH(PLAYER_PED_ID(), 5000)
        ENDIF
        
        SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanBeArrested, FALSE)

        IF shockingEvent
            // Shocking animals.
            SWITCH eAnimalModel
                CASE A_C_MTLION
                CASE A_C_BOAR
                CASE A_C_COW
                CASE A_C_COYOTE
                CASE IG_ORLEANS
                    CPRINTLN(DEBUG_ANIMAL_MODE, "Adding shocking event for animal.")
                    g_iAnimalShockingEvent = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DANGEROUS_ANIMAL, PLAYER_PED_ID(),0)
                    IF g_iAnimalShockingEvent = 0
                        CPRINTLN(DEBUG_ANIMAL_MODE, "Failed to add shocking event.")
                    ENDIF
                BREAK
            ENDSWITCH   
        ENDIF
    ENDIF

    SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
    g_bBlockShopRoberies = TRUE
    g_bForceNoCableCar = TRUE
    DISABLE_TAXI_HAILING(TRUE)

    //stop stunt jumps
    SET_STUNT_JUMPS_CAN_TRIGGER(FALSE)
    
    //calm water
    WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(0) 
    WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(0)  
    WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(0)  
    WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(0) 
    WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(0)  
    WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(0)  
    WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(0)  
    WATER_OVERRIDE_SET_RIPPLEBUMPINESS(0)  
    WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(0)  
    WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(0)  
    WATER_OVERRIDE_SET_RIPPLEDISTURB(0)  
    WATER_OVERRIDE_SET_STRENGTH(0.1)        
    
    //Disable all non-humanoid cheats
    IF eAnimalModel != IG_ORLEANS
        DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_SLIDEY_CARS, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_FAST_SWIM, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_GIVE_PARACHUTE, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_BANG_BANG, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_FLAMING_BULLETS, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_EXPLOSIVE_MELEE, TRUE) 
        DISABLE_CHEAT(CHEAT_TYPE_0_GRAVITY, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_INVINCIBILITY, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_SLOWMO, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_SKYFALL, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_DRUNK, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_AIM_SLOWMO, TRUE)
        DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
    ENDIF
    
    ALLOW_DIALOGUE_IN_WATER(TRUE)


    //Added by Steve T. for TRC bug 2463721 - We can't accept invites whilst an animal as we have no phone access.
    CPRINTLN(DEBUG_ANIMAL_MODE, "Blocked network invites after transitioning into animal form.")
    NETWORK_BLOCK_INVITES(TRUE)


    
ENDPROC

PROC Manage_Wanted_Level(int &wantedTimer)
    IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 1
        IF wantedTimer = -1
            wantedTimer = GET_GAME_TIMER()
        ELIF GET_GAME_TIMER() > wantedTimer + 20000
            SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
            SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
            wantedTimer = -1
        ENDIF
    ELSE
        wantedTimer = -1
    ENDIF
ENDPROC

PROC Update_Animal_Restricted_State(MODEL_NAMES eAnimalModel, INT &wantedTimer, BOOL bHideHud = TRUE)

    Manage_Wanted_Level(wantedTimer)
    
    IF NOT Is_Model_A_Playable_Animal(eAnimalModel)
        CPRINTLN(DEBUG_ANIMAL_MODE, "Tried to update restricted animal state for an invalid model.")
        SCRIPT_ASSERT("Tried to update restricted animal state for an invalid model. Exiting.")
        EXIT
    ENDIF
    
    IF bHideHud
        IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
            THEFEED_HIDE_THIS_FRAME()
        ENDIF
        
        HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
    ENDIF
    
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE)
        SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
        SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableDropDowns, TRUE)
    ENDIF
    
    //Flying animals use the jump input to land.
    IF (NOT Is_Model_A_Flying_Animal(eAnimalModel) OR eAnimalModel = A_C_HEN)
    AND eAnimalModel != IG_ORLEANS
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
    ENDIF
    
    IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
    ENDIF   
        
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ARREST)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)

    //Sasquatch ignores these blocks.
    IF eAnimalModel != IG_ORLEANS
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CINEMATIC_SLOWMO)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_HUD_SPECIAL)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_WEAPON)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)
    ELSE
        DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
        SET_PLAYER_CAN_USE_COVER(player_id(),FALSE)
    ENDIF
    
    //Don't allow first person or 3rd person near view modes while 
    //in animal form. There is too much camera clipping.
    IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
    OR GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_THIRD_PERSON_NEAR
        SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
    ENDIF

    IF IS_PLAYER_PLAYING(PLAYER_ID())
        RESET_PLAYER_STAMINA(PLAYER_ID())
    ENDIF
    
    //Don't allow autosaving once animal mode is active.
    g_sAutosaveData.bFlushAutosaves = TRUE
ENDPROC


PROC Deactivate_Animal_Restricted_State()
    CPRINTLN(DEBUG_ANIMAL_MODE, "Deactivating animal restricted mode.")
    
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanBeArrested, TRUE)
        SET_ENTITY_HEALTH(PLAYER_PED_ID(), 200)
    ENDIF
    
    SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 200)
    
    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, FALSE)
    SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
    SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
    SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 1.0)
    
    SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
    g_bBlockShopRoberies = FALSE
    g_bForceNoCableCar = FALSE
    DISABLE_TAXI_HAILING(FALSE)

    DISPLAY_RADAR(TRUE)
    
    IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
        THEFEED_RESUME()
    ENDIF
    
    SET_PLAYER_CAN_USE_COVER(player_id(),TRUE)
    
    //Enable stunt jumps
    SET_STUNT_JUMPS_CAN_TRIGGER(TRUE)
    
    //Reset water
    WATER_OVERRIDE_SET_STRENGTH(0)
    
    DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_SLIDEY_CARS, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_FAST_SWIM, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_GIVE_PARACHUTE, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_BANG_BANG, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_FLAMING_BULLETS, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_EXPLOSIVE_MELEE, FALSE)    
    DISABLE_CHEAT(CHEAT_TYPE_0_GRAVITY, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_INVINCIBILITY, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_SLOWMO, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_SKYFALL, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_DRUNK, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_AIM_SLOWMO, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, FALSE)
    
    //allow autosaving once animal mode is complete.
    g_sAutosaveData.bFlushAutosaves = FALSE
    
    IF g_iAnimalShockingEvent != 0
        REMOVE_SHOCKING_EVENT(g_iAnimalShockingEvent)
    ENDIF
    
    ALLOW_DIALOGUE_IN_WATER(FALSE)

    
    //Added by Steve T. for TRC bug 2463721 - We can't accept invites whilst an animal as we have no phone access. After deactivation of animal state, re-enable network invites.
    CPRINTLN(DEBUG_ANIMAL_MODE, "Re-enabled network invites after deactivating restricted animal state.")
    NETWORK_BLOCK_INVITES(FALSE) 


ENDPROC

PROC Get_Pickup_Type_Bark_Anim_Data(TEXT_LABEL_63 &strBarkDict, TEXT_LABEL_15 &strBarkAnim, MODEL_NAMES animalModel)
    
    SWITCH animalModel
        CASE A_C_BOAR           strBarkDict = ""        BREAK
        CASE A_C_CAT_01         strBarkDict = ""        BREAK
        CASE A_C_COW            strBarkDict = ""        BREAK
        CASE A_C_COYOTE         strBarkDict = ""        BREAK
        CASE A_C_DEER           strBarkDict = ""        BREAK
        CASE A_C_HUSKY          strBarkDict = "facials@creatures@retriever@bark"    BREAK
        CASE A_C_MTLION         strBarkDict = ""        BREAK
        CASE A_C_PIG            strBarkDict = ""        BREAK
        CASE A_C_POODLE         strBarkDict = "facials@creatures@pug@bark"          BREAK
        CASE A_C_PUG            strBarkDict = "facials@creatures@pug@bark"          BREAK
        CASE A_C_RABBIT_01      strBarkDict = ""        BREAK
        CASE A_C_RETRIEVER      strBarkDict = "facials@creatures@retriever@bark"    BREAK
        CASE A_C_ROTTWEILER     strBarkDict = "facials@creatures@rottweile@bark"    BREAK
        CASE A_C_SHEPHERD       strBarkDict = "facials@creatures@retriever@bark"    BREAK
        CASE A_C_WESTY          strBarkDict = "facials@creatures@pug@bark"          BREAK
        
        CASE A_C_CHICKENHAWK    strBarkDict = ""        BREAK
        CASE A_C_CORMORANT      strBarkDict = ""        BREAK
        CASE A_C_CROW           strBarkDict = ""        BREAK
        CASE A_C_HEN            strBarkDict = ""        BREAK
        CASE A_C_PIGEON         strBarkDict = ""        BREAK
        CASE A_C_SEAGULL        strBarkDict = ""        BREAK
        
        CASE A_C_DOLPHIN        strBarkDict = ""        BREAK
        CASE A_C_FISH           strBarkDict = ""        BREAK
        CASE A_C_KILLERWHALE    strBarkDict = ""        BREAK
        CASE A_C_SHARKHAMMER    strBarkDict = ""        BREAK
        CASE A_C_SHARKTIGER     strBarkDict = ""        BREAK
        CASE A_C_STINGRAY       strBarkDict = ""        BREAK
        
        CASE IG_ORLEANS         strBarkDict = ""        BREAK
        
        DEFAULT SCRIPT_ASSERT("Get_Pickup_Type_Bark_Anim_Data: Invalid animal enum passed.")    BREAK
    ENDSWITCH
    
    IF NOT IS_STRING_NULL_OR_EMPTY(strBarkDict)
        strBarkAnim = "bark_facial"
    ENDIF
    
    CPRINTLN(DEBUG_ANIMAL_MODE,"Selected Bark_Anim animation data. Dict:", strBarkDict, " Anim:", strBarkAnim, ".")
ENDPROC

PROC Get_Pickup_Sound_Data(AnimalSoundData &sData, MODEL_NAMES animalModel)
    SWITCH animalModel
        CASE A_C_BOAR           sData.strBank = "PEYOTE_ATTRACT_BOAR"           sData.strSound = "BOAR"         sData.strBarkSound = "EXCITED"          BREAK
        CASE A_C_CAT_01         sData.strBank = "PEYOTE_ATTRACT_CAT"            sData.strSound = "CAT"          sData.strBarkSound = "CAT_MEOW"         BREAK
        CASE A_C_COW            sData.strBank = "PEYOTE_ATTRACT_COW"            sData.strSound = "COW"          sData.strBarkSound = ""                 BREAK //Cow moo's with attack, disable context bark
        CASE A_C_COYOTE         sData.strBank = "PEYOTE_ATTRACT_COYOTE"         sData.strSound = "COYOTE"       sData.strBarkSound = "BARK"             BREAK
        CASE A_C_DEER           sData.strBank = "PEYOTE_ATTRACT_DEER"           sData.strSound = "DEER"         sData.strBarkSound = "CALL"             BREAK
        CASE A_C_HUSKY          sData.strBank = "PEYOTE_ATTRACT_HUSKY"          sData.strSound = "HUSKY"        sData.strBarkSound = "BARK"             BREAK
        CASE A_C_MTLION         sData.strBank = "PEYOTE_ATTRACT_MTLION"         sData.strSound = "MTLION"       sData.strBarkSound = "ROAR"             BREAK
        CASE A_C_PIG            sData.strBank = "PEYOTE_ATTRACT_PIG"            sData.strSound = "PIG"          sData.strBarkSound = "EXCITED"          BREAK
        CASE A_C_POODLE         sData.strBank = "PEYOTE_ATTRACT_SMALL_DOG"      sData.strSound = "SMALL_DOG"    sData.strBarkSound = "BARK"             BREAK
        CASE A_C_PUG            sData.strBank = "PEYOTE_ATTRACT_SMALL_DOG"      sData.strSound = "SMALL_DOG"    sData.strBarkSound = "BARK"             BREAK
        CASE A_C_RABBIT_01      sData.strBank = "PEYOTE_ATTRACT_RABBIT"         sData.strSound = "RABBIT"       sData.strBarkSound = "RABBIT_SCREAM"    BREAK
        CASE A_C_RETRIEVER      sData.strBank = "PEYOTE_ATTRACT_RETRIEVER"      sData.strSound = "RETRIEVER"    sData.strBarkSound = "BARK"         BREAK
        CASE A_C_ROTTWEILER     sData.strBank = "PEYOTE_ATTRACT_ROTTWEILER"     sData.strSound = "ROTTWEILER"   sData.strBarkSound = "BARK"         BREAK
        CASE A_C_SHEPHERD       sData.strBank = "PEYOTE_ATTRACT_SHEPHERD"       sData.strSound = "SHEPHERD"     sData.strBarkSound = "BARK"         BREAK
        CASE A_C_WESTY          sData.strBank = "PEYOTE_ATTRACT_SMALL_DOG"      sData.strSound = "SMALL_DOG"    sData.strBarkSound = "BARK"         BREAK
        
        CASE A_C_CHICKENHAWK    sData.strBank = "PEYOTE_ATTRACT_CHICKENHAWK"    sData.strSound = "CHICKENHAWK"  sData.strBarkSound = "SCREECH"      BREAK
        CASE A_C_CORMORANT      sData.strBank = "PEYOTE_ATTRACT_CORMORANT"      sData.strSound = "CORMORANT"    sData.strBarkSound = "CALL"         BREAK
        CASE A_C_CROW           sData.strBank = "PEYOTE_ATTRACT_CROW"           sData.strSound = "CROW"         sData.strBarkSound = "FLIGHT"       BREAK
        CASE A_C_HEN            sData.strBank = "PEYOTE_ATTRACT_HEN"            sData.strSound = "HEN"          sData.strBarkSound = "SQUAWK"       BREAK
        CASE A_C_PIGEON         sData.strBank = "PEYOTE_ATTRACT_PIGEON"         sData.strSound = "PIGEON"       sData.strBarkSound = "COO"          BREAK
        CASE A_C_SEAGULL        sData.strBank = "PEYOTE_ATTRACT_SEAGULL"        sData.strSound = "SEAGULL"      sData.strBarkSound = "CALL"         BREAK
        
        CASE A_C_DOLPHIN        sData.strBank = "PEYOTE_ATTRACT_DOLPHIN"        sData.strSound = "DOLPHIN"      sData.strBarkSound = "DOLPHIN_CALL"                 BREAK
        CASE A_C_FISH           sData.strBank = "PEYOTE_ATTRACT_SEA_CREATURE"   sData.strSound = "SEA_CREATURE" BREAK
        CASE A_C_KILLERWHALE    sData.strBank = "PEYOTE_ATTRACT_SEA_CREATURE"   sData.strSound = "SEA_CREATURE" sData.strBarkSound = "WHALE_CALL"                   BREAK
        CASE A_C_SHARKHAMMER    sData.strBank = "PEYOTE_ATTRACT_SEA_CREATURE"   sData.strSound = "SEA_CREATURE" BREAK
        CASE A_C_SHARKTIGER     sData.strBank = "PEYOTE_ATTRACT_SEA_CREATURE"   sData.strSound = "SEA_CREATURE" BREAK
        CASE A_C_STINGRAY       sData.strBank = "PEYOTE_ATTRACT_SEA_CREATURE"   sData.strSound = "SEA_CREATURE" BREAK
        
        CASE IG_ORLEANS         sData.strBank = "PEYOTE_ATTRACT_SASQUATCH"      sData.strSound = "SASQUATCH"    sData.strBarkBank = "SAS_BANK_01"   sData.strBarkSound = "PEYOTE_PLAYER_SECRET_VOCAL_MASTER" BREAK
        
        DEFAULT SCRIPT_ASSERT("Get_Pickup_Sound_Data: Invalid pickup enum passed.")                                     BREAK
    ENDSWITCH
    
    CPRINTLN(DEBUG_ANIMAL_MODE,"Selected pickup sound data. Bank:", sData.strBank, " Sound:", sData.strSound, ".")
ENDPROC

PROC Play_Animal_Sound(STRING strBarkDict, STRING strBarkAnim, INT &barkTimer, enumCharacterList eLastPlayerCharacter, INT &iBarkSoundID, STRING strBarkSound)
    IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
    AND NOT IS_ANIMAL_VOCALIZATION_PLAYING(PLAYER_PED_ID())
    AND (IS_STRING_NULL_OR_EMPTY(strBarkDict) OR NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),strBarkDict, strBarkAnim))
    AND GET_GAME_TIMER() > barkTimer
    AND NOT IS_PED_INJURED(PLAYER_PED_ID())
        
        STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)           
        SWITCH GET_ENTITY_MODEL(player_ped_ID())            
            CASE A_C_FISH
            CASE A_C_SHARKHAMMER
            CASE A_C_SHARKTIGER
            CASE A_C_STINGRAY
                CPRINTLN(DEBUG_ANIMAL_MODE, "Play Fish Bark")
                SWITCH eLastPlayerCharacter
                    CASE CHAR_MICHAEL
                        PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE("DROWNING", "WAVELOAD_PAIN_MICHAEL", <<0.0, 0.0, 0.0>>, "SPEECH_PARAMS_FORCE_FRONTEND")
                    BREAK
                    CASE CHAR_FRANKLIN
                        PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE("DROWNING", "WAVELOAD_PAIN_FRANKLIN", <<0.0, 0.0, 0.0>>, "SPEECH_PARAMS_FORCE_FRONTEND")
                    BREAK
                    CASE CHAR_TREVOR 
                        PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE("DROWNING", "WAVELOAD_PAIN_TREVOR",  <<0.0, 0.0, 0.0>>, "SPEECH_PARAMS_FORCE_FRONTEND")
                    BREAK
                    DEFAULT
                        PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE("DROWNING", "WAVELOAD_PAIN_MICHAEL", <<0.0, 0.0, 0.0>>, "SPEECH_PARAMS_FORCE_FRONTEND")
                    BREAK
                ENDSWITCH
            BREAK
            
            CASE IG_ORLEANS
                CPRINTLN(DEBUG_ANIMAL_MODE, "Play SAS Bark")
                IF iBarkSoundID = -1
                    iBarkSoundID = GET_SOUND_ID()
                ENDIF
                PLAY_SOUND_FROM_ENTITY(iBarkSoundID, strBarkSound, PLAYER_PED_ID())
            BREAK
            
            DEFAULT
                CPRINTLN(DEBUG_ANIMAL_MODE, "Play Default Bark")
                IF NOT IS_STRING_NULL_OR_EMPTY(strBarkSound)
                    PLAY_ANIMAL_VOCALIZATION(PLAYER_PED_ID(), AUD_ANIMAL_NONE, strBarkSound)
                ENDIF
            BREAK
        ENDSWITCH           
        STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
        
        IF NOT IS_STRING_NULL_OR_EMPTY(strBarkDict)
            TASK_PLAY_ANIM(PLAYER_PED_ID(), strBarkDict, strBarkAnim,DEFAULT,SLOW_BLEND_OUT,DEFAULT,AF_SECONDARY)
            barkTimer = GET_GAME_TIMER() + 500
        ENDIF           
    ENDIF
ENDPROC
