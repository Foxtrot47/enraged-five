//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	tattoo_shop_private.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains all the functionality for the 	//
//							tattoo shops.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_pad.sch"
USING "script_player.sch"
USING "commands_interiors.sch"
USING "shop_private.sch"
USING "cellphone_public.sch"
USING "finance_control_public.sch"
USING "LineActivation.sch"
USING "net_spawn.sch"
USING "cheat_handler.sch"

CONST_INT TATTOO_LIMIT_BIT		0

ENUM TATTOO_DIALOGUE_ENUM

	TATT_DLG_NONE = -1,
	
	TATT_DLG_GREET,
	TATT_DLG_ASK,
	TATT_DLG_WORK,
	TATT_DLG_EXIT
ENDENUM

ENUM TORSO_DECORATION_SUBZONE
    TDS_TORSO_BACK = 0,	
	TDS_TORSO_BACK_FULL,
    TDS_TORSO_CHEST,
	TDS_TORSO_STOMACH,
	TDS_INVALID = -1
ENDENUM


/// PURPOSE: The main struct that we will use for tattoo parlours
STRUCT TATTOO_SHOP_STRUCT
	
	SHOP_INFO_STRUCT			sShopInfo
	SHOP_BROWSE_INFO_STRUCT		sBrowseInfo
	SHOP_ENTRY_INFO_STRUCT 		sEntryInfo
	SHOP_LOCATE_STRUCT  		sLocateInfo
	SHOP_DIALOGUE_STRUCT 		sDialogueInfo
	SHOP_INPUT_DATA_STRUCT 		sInputData
	
	INT 						iAvailableTattoos[MAX_NUMBER_OF_TATTOO_BITSETS]
	INT 						iAvailableDLCTattoos[MAX_NUMBER_OF_DLC_TATTOO_BITSETS]
	
	TATTOO_NAME_ENUM 			eCurrentTattoo
	PED_VARIATION_STRUCT 		sCurrentVariations
	BOOL 						bCurrentVariationsStored
	
	TATTOO_NAME_ENUM 			eCrewLogoTattoo
	
	VECTOR						vSceneOrigin
	
	BOOL bDoorTimerSet
	TIME_DATATYPE tdDoorTimer
ENDSTRUCT

FUNC BOOL IS_DLC_TATTOO_SHOP(SHOP_NAME_ENUM eShop)
	SWITCH eShop
		CASE TATTOO_PARLOUR_01_HW
		CASE TATTOO_PARLOUR_02_SS
		CASE TATTOO_PARLOUR_03_PB
		CASE TATTOO_PARLOUR_04_VC
		CASE TATTOO_PARLOUR_05_ELS
		CASE TATTOO_PARLOUR_06_GOH
			RETURN FALSE
			
		#IF FEATURE_TUNER
		CASE TATTOO_PARLOUR_07_CCT	
			RETURN TRUE
		#ENDIF
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC RESET_TATTOO_SHOP_DATA(TATTOO_SHOP_STRUCT &sData)
	
	RESET_SHOP_BROWSE_INFO_STRUCT(sData.sBrowseInfo)
	RESET_SHOP_INFO_STRUCT(sData.sShopInfo)
	RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
	RESET_SHOP_LOCATE_STRUCT(sData.sLocateInfo)
	
	INT i
	REPEAT COUNT_OF(sData.iAvailableTattoos) i
		sData.iAvailableTattoos[i] = 0
	ENDREPEAT
	REPEAT COUNT_OF(sData.iAvailableDLCTattoos) i
		sData.iAvailableDLCTattoos[i] = 0
	ENDREPEAT
ENDPROC

/// PURPOSE: Helper function to fill the shop info struct
PROC FILL_TATTOO_SHOP_DATA(SHOP_INFO_STRUCT &sShopInfo, SHOP_NAME_ENUM paramShop, INT paramRoomKey, INT paramLocateCount, MODEL_NAMES paramSKModel, VECTOR paramSKPos, FLOAT paramSKHead, VECTOR paramSKPosCounter, FLOAT paramSKHeadCounter, MODEL_NAMES paramICModel, VECTOR paramICSpawn, FLOAT paramICHead, BOOL paramDataSet)
	sShopInfo.eShop = paramShop
	sShopInfo.iRoomKey = paramRoomKey
	sShopInfo.iLocateCount = paramLocateCount
	sShopInfo.sShopKeeper.model = paramSKModel
	sShopInfo.sShopKeeper.vPosIdle = paramSKPos
	sShopInfo.sShopKeeper.fHeadIdle = paramSKHead
	sShopInfo.sShopKeeper.vPosCounter = paramSKPosCounter
	sShopInfo.sShopKeeper.fHeadCounter = paramSKHeadCounter
	
	sShopInfo.sShopCustomer.model = paramICModel
	sShopInfo.sShopCustomer.vPosIdle = paramICSpawn
	sShopInfo.sShopCustomer.fHeadIdle = paramICHead
	sShopInfo.bDataSet = paramDataSet
ENDPROC

/// PURPOSE: Fills the specified struct will the shop information and returns TRUE if successful
FUNC BOOL GET_TATTOO_SHOP_DATA(SHOP_INFO_STRUCT &sShopInfo, SHOP_NAME_ENUM eShop)
	
	sShopInfo.bDataSet = FALSE

	// Reset
	FILL_TATTOO_SHOP_DATA(sShopInfo, EMPTY_SHOP, 0, 0, DUMMY_MODEL_FOR_SCRIPT, <<0.0, 0.0, 0.0>>, 0.0, <<0.0, 0.0, 0.0>>, 0.0, DUMMY_MODEL_FOR_SCRIPT, <<0.0, 0.0, 0.0>>, 0.0, FALSE)
	
	INT iInterior1 = GET_HASH_KEY("v_tattoo")
	INT iInterior2 = GET_HASH_KEY("v_tattoo2")
	#IF FEATURE_TUNER
	INT iInterior3 = GET_HASH_KEY("tr_tuner_car_meet")
	#ENDIF
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	IF (iInteriorCheck = iInterior1)
		FILL_TATTOO_SHOP_DATA(sShopInfo, eShop, GET_HASH_KEY("MainTatRm"), 1,
		
		U_M_Y_Tattoo_01, 
		<<322.5702, 182.0852, 102.5865>>, 196.6765,	 	// Shop keeper intro pos    //<< 323.50, 182.08, 102.5865>>, 201.2717
		<< 319.6180, 181.5184, 102.5865 >>, 246.6867, 	// Shop Keeper counter pos
		
		G_M_Y_MEXGOON_03,
		<<324.1968, 181.3282, 103.2660>>, 78.9718, 		// Intro Customer pos 
		TRUE)
		fCameraPointHeading[TATTOO_FRONT] = 52.17
	ELIF (iInteriorCheck = iInterior2)
		FILL_TATTOO_SHOP_DATA(sShopInfo, eShop, GET_HASH_KEY("V_49_Tat2_Room"), 1,
		
		U_M_Y_Tattoo_01, 
		<<1862.8435, 3748.1587, 33.031849>>, 60.9120, 	// Shop keeper intro pos   //<<1862.701050,3748.444824,33.031849>> //35.4570 //<< 1862.5107, 3748.3438, 32.0319 >>
		<<1862.8435, 3748.1587, 33.031849>>, 60.9120,   // Shop Keeper counter pos  //<<1862.701050,3748.444824,33.031849>> //35.4570 //<<1862.4863, 3748.4702, 240.1709>>
		
		G_M_Y_MEXGOON_03,
		<<1862.4863, 3748.4702, 32.0319>>, 35.4570,	// Intro Customer pos 
		TRUE)
		fCameraPointHeading[TATTOO_FRONT] = -13.17
	#IF FEATURE_TUNER
	ELIF (iInteriorCheck = iInterior3)
		FILL_TATTOO_SHOP_DATA(sShopInfo, eShop, GET_HASH_KEY("Meet_rm"), 1,
		
		INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Tattoo_01")),
		<<-2163.2468, 1073.8733, -25.3537>>, 339.0,	// Shop keeper intro pos
		<<-2163.2468, 1073.8733, -25.3537>>, 339.0,	// Shop Keeper counter pos
		
		G_M_Y_MEXGOON_03,
		<<-2160.2385, 1075.0209, -24.3617>>, 0.0,	// Intro Customer pos
		TRUE)
		fCameraPointHeading[TATTOO_FRONT] = -13.17
	#ENDIF
	ENDIF
	
	fCameraPointHeading[TATTOO_BACK] = fCameraPointHeading[TATTOO_FRONT] + 180.0
	fCameraPointHeading[TATTOO_LEFT] = fCameraPointHeading[TATTOO_FRONT] - 90.0
	fCameraPointHeading[TATTOO_RIGHT] =  fCameraPointHeading[TATTOO_FRONT] + 90.0
	fCameraPointHeading[TATTOO_FRONT_LEFT] = fCameraPointHeading[TATTOO_FRONT] - 45.0
	fCameraPointHeading[TATTOO_BACK_LEFT] = fCameraPointHeading[TATTOO_FRONT] - 90.0 - 45.0
	fCameraPointHeading[TATTOO_FRONT_RIGHT] = fCameraPointHeading[TATTOO_FRONT] + 45.0
	fCameraPointHeading[TATTOO_BACK_RIGHT] = fCameraPointHeading[TATTOO_FRONT] + 90.0 + 45.0
	
	#IF IS_DEBUG_BUILD
		IF sShopInfo.bDataSet
			PRINTSTRING("\nUsing data set for ")PRINTSTRING(GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(sShopInfo.eShop)))PRINTSTRING(" - ")PRINTSTRING(GET_SHOP_NAME(sShopInfo.eShop))PRINTNL()
		ELSE
			//SCRIPT_ASSERT("Unable to find suitable tattoo shop data.")
			PRINTSTRING("\nUnable to find suitable tattoo shop data.")PRINTNL()
		ENDIF
	#ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = TATTOO_PARLOUR_01_HW
	ELIF (iInteriorCheck = iInterior2)
		eBaseShop = TATTOO_PARLOUR_02_SS
	#IF FEATURE_TUNER
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = TATTOO_PARLOUR_07_CCT
	#ENDIF
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sShopInfo.sShopKeeper.vPosIdle)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sShopInfo.sShopKeeper.fHeadIdle)
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sShopInfo.sShopCustomer.vPosIdle)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sShopInfo.sShopCustomer.fHeadIdle)
	
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_FRONT])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_BACK])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_LEFT])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_RIGHT])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_FRONT_LEFT])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_BACK_LEFT])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_FRONT_RIGHT])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fCameraPointHeading[TATTOO_BACK_RIGHT])
	
	RETURN sShopInfo.bDataSet
ENDFUNC

/// PURPOSE: Returns the locate info for the specified locate
PROC GET_TATTOO_SHOP_LOCATE_DATA(SHOP_NAME_ENUM eShop, SHOP_LOCATE_STRUCT &sLocate)

	// Reset
	sLocate.vLocatePos		  = <<0.0,0.0,0.0>>
	//sLocate.iEntryPoints	  = 0
	sLocate.vPlayerBrowsePos  = <<0.0,0.0,0.0>>
	sLocate.fPlayerBrowseHead = 0.0
	sLocate.vPlayerEndPos 	  = <<0.0,0.0,0.0>>
	sLocate.fPlayerEndHead 	  = 0.0
			
	// Work out current tattoo interior
	INT iInterior1 = GET_HASH_KEY("v_tattoo")
	INT iInterior2 = GET_HASH_KEY("v_tattoo2")
	#IF FEATURE_TUNER
	INT iInterior3 = GET_HASH_KEY("tr_tuner_car_meet")
	#ENDIF
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	// Initial tattoo shop
	IF (iInteriorCheck = iInterior1)
		
		sLocate.eType = LOCATE_TYPE_TATTOOS
		//sLocate.vEntryPos[0]      = <<324.814423,179.332596,102.486542>>
		//sLocate.vEntryPos[1]      = <<322.634796,180.156189,104.086540>> 
		//sLocate.fEntrySize[0]     = 1.875000
		sLocate.vEntryPos[0]      = <<325.289093,182.556055,102.592110>>
		sLocate.vEntryPos[1]      = <<323.910919,177.915439,104.586533>>
		sLocate.fEntrySize[0]     = 4.550000
		
		sLocate.vPlayerBrowsePos  = <<323.5805, 179.8582, 103.5915>>
		sLocate.fPlayerBrowseHead = 78.9718
		sLocate.vPlayerEndPos 	  = << 323.0614, 180.4159, 103.5915 >>
		sLocate.fPlayerEndHead 	  = 42.4545 
		sLocate.vPlayerGoToPos 	  = << 319.7213, 176.2593, 103.5915 >>
		
	ELIF (iInteriorCheck = iInterior2)
		sLocate.eType = LOCATE_TYPE_TATTOOS
		//sLocate.vEntryPos[0]      = <<1864.994995,3748.143066,31.931868>>
		//sLocate.vEntryPos[1]      = <<1862.753906,3747.039795,33.531868>> 
		//sLocate.fEntrySize[0]     = 2.125000
		sLocate.vEntryPos[0]      = <<1866.537354,3747.878906,32.031864>>
		sLocate.vEntryPos[1]      = <<1862.889893,3745.789795,34.061180>>
		sLocate.fEntrySize[0]     = 4.550000
		
		sLocate.vPlayerBrowsePos  = <<1864.3986, 3747.7668, 33.0364>>//<< 1863.9592, 3747.6, 33.0364 >>
		sLocate.fPlayerBrowseHead = 27.5523 //12.3803
		sLocate.vPlayerEndPos 	  = << 1863.9720, 3748.1902, 33.0364 >>
		sLocate.fPlayerEndHead 	  = 350.0413
		sLocate.vPlayerGoToPos 	  = << 1857.3545, 3747.6350, 33.0364 >>
		
	#IF FEATURE_TUNER
	ELIF (iInteriorCheck = iInterior3)
		sLocate.eType = LOCATE_TYPE_TATTOOS
		sLocate.vEntryPos[0]      = <<-2159.900879,1075.212524,-25.361736>>
		sLocate.vEntryPos[1]      = <<-2164.270020,1075.127808,-23.361595>>
		sLocate.fEntrySize[0]     = 3.5
		
		sLocate.vPlayerBrowsePos  = <<-2160.2385, 1075.0209, -24.3617>>
		sLocate.fPlayerBrowseHead = 0.0
		sLocate.vPlayerEndPos 	  = <<-2160.2385, 1075.0209, -24.3617>>
		sLocate.fPlayerEndHead 	  = 0.0
		sLocate.vPlayerGoToPos 	  = <<-2160.2385, 1075.0209, -24.3617>>
	#ENDIF
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = TATTOO_PARLOUR_01_HW
	ELIF (iInteriorCheck = iInterior2)
		eBaseShop = TATTOO_PARLOUR_02_SS
	#IF FEATURE_TUNER
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = TATTOO_PARLOUR_07_CCT
	#ENDIF
	ENDIF
		
	ALIGN_SHOP_COORD(eBaseShop, eShop, sLocate.vEntryPos[0])
	ALIGN_SHOP_COORD(eBaseShop, eShop, sLocate.vEntryPos[1])
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sLocate.vPlayerBrowsePos)
	ALIGN_SHOP_COORD(eBaseShop, eShop, sLocate.vPlayerEndPos)
	ALIGN_SHOP_COORD(eBaseShop, eShop, sLocate.vPlayerGoToPos)
	
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sLocate.fPlayerBrowseHead)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sLocate.fPlayerEndHead)
ENDPROC

/// PURPOSE: Returns the layout type for this tattoo parlour
FUNC SHOP_NAME_ENUM GET_BASE_SHOP_TYPE(SHOP_NAME_ENUM eShop)	

	// Work out which tattoo parlour is used for this location 
	INT iInteriorType1 = GET_HASH_KEY("v_tattoo")
	#IF FEATURE_TUNER
	INT iInteriorType3 = GET_HASH_KEY("tr_tuner_car_meet")
	#ENDIF
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	IF (iInteriorCheck = iInteriorType1)
		RETURN TATTOO_PARLOUR_01_HW
	#IF FEATURE_TUNER
	ELIF (iInteriorCheck = iInteriorType3)
		RETURN TATTOO_PARLOUR_07_CCT
	#ENDIF
	ELSE
		RETURN TATTOO_PARLOUR_02_SS
	ENDIF
ENDFUNC

/// PURPOSE: Gets synchronised scene origin
FUNC VECTOR GET_SCENE_ORIGIN(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)	
	
	VECTOR vSceneOrigin
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		vSceneOrigin = << 323.67, 182.06, 102.59 >>
	ELSE
		vSceneOrigin = << 1864.89, 3746.32, 32.04 >>
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vSceneOrigin)
	RETURN vSceneOrigin
ENDFUNC

/// PURPOSE: Returns location of tattoo shop main entrance
FUNC VECTOR GET_TATTOO_DOOR_POS(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	
	VECTOR vDoorPos
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		vDoorPos = <<321.81, 178.36, 103.68>>
	ELSE
		vDoorPos = <<1859.89, 3749.79, 33.18>>
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vDoorPos)
	RETURN vDoorPos
ENDFUNC

/// PURPOSE: Returns door model for current tattoo shop
FUNC MODEL_NAMES GET_TATTOO_DOOR_MODEL(SHOP_NAME_ENUM eBaseShop)

	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN V_ILEV_TA_DOOR
	ELSE
		RETURN V_ILEV_ML_DOOR1
	ENDIF
ENDFUNC

/// PURPOSE: Returns location of tattoo gun
FUNC VECTOR GET_TATTOO_GUN_POS(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	
	VECTOR vGunPos
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		vGunPos = <<324.17, 182.28, 103.40>>
	ELSE
		vGunPos = <<324.17, 182.28, 103.40>>
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vGunPos)
	RETURN vGunPos
ENDFUNC

FUNC VECTOR GET_PLAYER_END_POS(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	
	VECTOR vPlayerPos
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		vPlayerPos = <<322.7368, 179.8585, 102.5865>>
	ELSE
		vPlayerPos = <<1863.9399, 3749.830, 32.0319>>
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vPlayerPos)
	RETURN vPlayerPos
ENDFUNC

FUNC FLOAT GET_PLAYER_END_HEADING(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	
	FLOAT fPlayerHeading
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		fPlayerHeading = 68.7827
	ELSE
		fPlayerHeading = 169.99
	ENDIF
	
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fPlayerHeading)
	RETURN fPlayerHeading
ENDFUNC

FUNC VECTOR GET_TATTOOIST_END_POS(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	
	VECTOR vTattooPos
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		vTattooPos = <<319.62, 181.52, 103.59>>
	ELSE
		vTattooPos = <<1863.19, 3747.52, 33.03>>
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vTattooPos)
	GET_GROUND_Z_FOR_3D_COORD(vTattooPos, vTattooPos.z)
	
	RETURN vTattooPos
ENDFUNC

FUNC FLOAT GET_TATTOOIST_END_HEADING(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	
	FLOAT fTatooHeading
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		fTatooHeading = -113.31
	ELSE
		fTatooHeading = -17.02
	ENDIF
	
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fTatooHeading)
	RETURN fTatooHeading
ENDFUNC

FUNC STRING GET_INTRO_ANIM_CUSTOMER(SHOP_NAME_ENUM eBaseShop)
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN "shop_ig_4_customer"
	ELSE
		RETURN "artist_finishes_up_his_tattoo_player"
	ENDIF
ENDFUNC

FUNC STRING GET_INTRO_ANIM_CUSTOMER_DOOR(SHOP_NAME_ENUM eBaseShop)
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN "shop_ig_4_door"
	ELSE
		RETURN "artist_finishes_up_his_tattoo_door"
	ENDIF
ENDFUNC

FUNC STRING GET_INTRO_ANIM_PLAYER(SHOP_NAME_ENUM eBaseShop)
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN "shop_ig_4_b_player"
	ELSE
		RETURN "shop_ig_5_b_player"
	ENDIF
ENDFUNC

FUNC STRING GET_INTRO_ANIM_PLAYER_DOOR(SHOP_NAME_ENUM eBaseShop)
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN "shop_ig_4_b_door"
	ELSE
		RETURN "shop_ig_5_b_door"
	ENDIF
ENDFUNC

FUNC STRING GET_INTRO_ANIM_TATTOOIST(SHOP_NAME_ENUM eBaseShop)
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN "shop_ig_4_tattooist"
	ELSE
		RETURN "artist_finishes_up_his_tattoo_artist"
	ENDIF
ENDFUNC

/// PURPOSE: Returns string name for player animation based on location
FUNC STRING GET_INTRO_ANIM_TATTOO_GUN(SHOP_NAME_ENUM eBaseShop)
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		RETURN "shop_ig_4_tattoogun"
	ELSE
		// Don't have anim for this yet
		RETURN "shop_ig_4_tattoogun"		
	ENDIF
ENDFUNC

/// PURPOSE: Sets up all the entry intro data for the current tattoo parlour
PROC GET_TATTOO_SHOP_ENTRY_INTRO_DATA(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop, SHOP_ENTRY_INFO_STRUCT &sEntryInfo)
	
	IF eBaseShop = TATTOO_PARLOUR_01_HW
			
		// Cutscene camera angles
		SWITCH sEntryInfo.iStage
			
			// Overview of shop
			CASE 0
				
				sEntryInfo.vCamCoord[0] = <<320.0082, 184.6191, 106.4468>>  // << 320.2584, 183.9999, 105.8749 >>
				sEntryInfo.vCamRot[0] 	= <<-35.1591, 0.2702, -156.5826>>   // << -18.2403, -0.0000, -148.3427 >>
				sEntryInfo.fCamFov[0] 	= 42.0858							// 50.0000
				
				sEntryInfo.vCamCoord[1] = <<320.0082, 184.6191, 106.4468>>  // << 320.1350, 184.1259, 105.3562 >>
				sEntryInfo.vCamRot[1] 	= <<-35.1591, 0.2702, -156.5826>>   // << -18.2403, -0.0000, -148.3427 >>
				sEntryInfo.fCamFov[1] 	= 42.0858							// 50.0000
			BREAK
			
			// Close up of seat
			CASE 1
				
				sEntryInfo.vCamCoord[0] = <<323.3365, 178.0721, 104.0448>>
				sEntryInfo.vCamRot[0] 	= <<-11.8398, -0.0000, -12.8859>>
				sEntryInfo.fCamFov[0] 	= 41.6715
				
				sEntryInfo.vCamCoord[1] = <<323.4999, 178.7863, 103.8912>>
				sEntryInfo.vCamRot[1] 	= <<-11.8398, -0.0000, -12.8859>>
				sEntryInfo.fCamFov[1] 	= 41.6715
			BREAK
			
			// Close up of keeper
			CASE 2
				
				sEntryInfo.vCamCoord[0] = <<323.8286, 182.0626, 104.6501>>
				sEntryInfo.vCamRot[0] 	= <<-18.0641, 0.2702, 102.4422>>
				sEntryInfo.fCamFov[0] 	= 42.0858
				
				sEntryInfo.vCamCoord[1] = <<322.5048, 181.7705, 104.3379>>
				sEntryInfo.vCamRot[1] 	= <<-11.1765, 0.2702, 102.4422>>
				sEntryInfo.fCamFov[1] 	= 42.0858
			BREAK
		ENDSWITCH
	ELSE
		// Camera stage
		SWITCH sEntryInfo.iStage
			
			// Overview of shop
			CASE 0
				sEntryInfo.vCamCoord[0] = <<1864.0313, 3752.0933, 34.7299>>//<<1863.8679, 3751.6853, 33.2819>>//<<1864.0313, 3752.0933, 34.7299>>  // << 1863.7668, 3752.1746, 34.7330 >>
				sEntryInfo.vCamRot[0] 	= <<-19.1933, -0.0000, 157.3466>>//<<-1.3391, -0.0000, 154.2719>>//<<-19.1933, -0.0000, 157.3466>>    // << -13.1203, -0.0000, 177.3853 >>
				sEntryInfo.fCamFov[0] 	= 51.8344//56.5999//51.8344
				
				sEntryInfo.vCamCoord[1] = <<1864.0313, 3752.0933, 34.7299>>  // << 1863.5735, 3752.2297, 34.5156 >>
				sEntryInfo.vCamRot[1] 	= <<-19.1933, -0.0000, 157.3466>>    // << -13.1203, -0.0000, 177.3853 >>
				sEntryInfo.fCamFov[1] 	= 51.8344
			BREAK
			
			// Close up of seat
			CASE 1
				sEntryInfo.vCamCoord[0] = << 1865.2209, 3748.1377, 33.4306 >>
				sEntryInfo.vCamRot[0] 	= << -17.1882, 0.0000, 154.0068 >>
				sEntryInfo.fCamFov[0] 	= 39.4671
				
				sEntryInfo.vCamCoord[1] = << 1863.5363, 3748.3088, 33.3994 >>
				sEntryInfo.vCamRot[1] 	= << -17.1882, -0.0000, -168.6809 >>
				sEntryInfo.fCamFov[1] 	= 39.4671
			BREAK
			
			// Close up of keeper
			CASE 2
				sEntryInfo.vCamCoord[0] = <<1861.521851, 3750.145264, 33.340031>>
				sEntryInfo.vCamRot[0] 	= <<-0.962345, -0.000000, -140.008926>>
				sEntryInfo.fCamFov[0] 	= 39.467098
				
				sEntryInfo.vCamCoord[1] = <<1861.313110, 3749.975830, 33.340031>>
				sEntryInfo.vCamRot[1] 	= <<-0.962345, -0.000000, -140.008926>>
				sEntryInfo.fCamFov[1] 	= 39.467098
			BREAK
		ENDSWITCH
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sEntryInfo.vCamCoord[0])
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, sEntryInfo.vCamRot[0])
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sEntryInfo.vCamCoord[1])
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, sEntryInfo.vCamRot[1])
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////
///    
///    HELPER PROCS USED IN TATTOO SHOP AND LOCKER ROOM

/// PURPOSE: Returns TRUE if the specified tattoo has been unlocked
FUNC BOOL IS_TATTOO_UNLOCKED(TATTOO_NAME_ENUM eTattoo)

	#IF IS_DEBUG_BUILD
		IF lw_bUnlockAllShopItems OR lw_bUnlockAllTattoos
			RETURN TRUE
		ENDIF
	#ENDIF
	
	#IF USE_TU_CHANGES
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND eTattoo >= TATTOO_MP_FM_DLC
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		RETURN (IS_MP_TATTOO_UNLOCKED(eTattoo))
	ELSE
		RETURN (IS_PLAYER_PED_TATTOO_UNLOCKED(GET_CURRENT_PLAYER_PED_ENUM(), eTattoo))
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_THIS_LABEL_A_CREW_TATTOO(STRING paramLabel)
	/*
	[TAT_LCKCREW:TAT_MNU]
	Complete the tutorial and join a Crew to unlock this emblem.
	*/
	
	IF NOT IS_STRING_NULL_OR_EMPTY(paramLabel)
		IF ARE_STRINGS_EQUAL(paramLabel, "TAT_BUS_012")		//"MP_Male_Crew_Tat_000"
		OR ARE_STRINGS_EQUAL(paramLabel, "TAT_BUS_013")		//"MP_Male_Crew_Tat_001"
		OR ARE_STRINGS_EQUAL(paramLabel, "TAT_BUS_F_014")	//"MP_Female_Crew_Tat_000"
		OR ARE_STRINGS_EQUAL(paramLabel, "TAT_BUS_F_015")	//"MP_Female_Crew_Tat_001"
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_TATTOO_UNLOCKED(TATTOO_NAME_ENUM eTattoo, STRING paramLabel)

	TEXT_LABEL_15 tlLabel = paramLabel

	IF IS_STRING_NULL_OR_EMPTY(tlLabel)
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID()), PLAYER_PED_ID())
			tlLabel = sTattooData.sLabel
		ENDIF
	ENDIF
	
	IF IS_THIS_LABEL_A_CREW_TATTOO(tlLabel)
		IF NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
		OR NOT g_bPlayerHasValidCrewEmblem
			CPRINTLN(DEBUG_SHOPS, "IS_THIS_TATTOO_UNLOCKED false for \"", tlLabel, "\" - NOT in active clan")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN IS_TATTOO_UNLOCKED(eTattoo)
ENDFUNC

/// PURPOSE: Returns TRUE if the player has unlocked at least one tattoo/patch
FUNC BOOL HAS_PLAYER_UNLOCKED_ANY_TATTOOS()

	INT iTattoo
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, iTattoo), eFaction, PLAYER_PED_ID())
			IF IS_TATTOO_UNLOCKED(INT_TO_ENUM(TATTOO_NAME_ENUM, iTattoo))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the player ped has the specified tattoo in the zone
FUNC BOOL HAS_PLAYER_PED_GOT_TATTOO(TATTOO_NAME_ENUM eTattoo)
	IF g_bInMultiplayer
		RETURN (IS_MP_TATTOO_CURRENT(eTattoo))
	ELSE
		RETURN (IS_PLAYER_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), eTattoo))
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Returns TRUE if the player ped has already viewed the tattoo
FUNC BOOL HAS_PLAYER_PED_VIEWED_TATTOO(TATTOO_NAME_ENUM eTattoo)
	
	IF g_bInMultiplayer
		RETURN (IS_MP_TATTOO_VIEWED(eTattoo))
	ELSE
		RETURN (HAS_PLAYER_PED_TATTOO_BEEN_VIEWED(GET_CURRENT_PLAYER_PED_ENUM(), eTattoo))
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Returns TRUE if the player ped has already viewed the tattoo
PROC SET_PLAYER_PED_VIEWED_TATTOO(TATTOO_NAME_ENUM eTattoo, BOOL bViewed)
	
	IF g_bInMultiplayer
		SET_MP_TATTOO_VIEWED(eTattoo, bViewed)
	ELSE
		SET_PLAYER_PED_TATTOO_AS_VIEWED(GET_CURRENT_PLAYER_PED_ENUM(), eTattoo, bViewed)
	ENDIF
ENDPROC

/// PURPOSE: Clears all ped decorations and then restores any saved ones
PROC REMOVE_TATTOOS_NOT_PURCHASED(PED_INDEX pedID)
	IF NOT IS_PED_INJURED(pedID)
		IF g_bInMultiplayer
			SET_MP_CHARACTER_TATTOOS_AND_PATCHES(pedID)
		ELSE
			RESTORE_PLAYER_PED_TATTOOS(pedID)
		ENDIF
	ENDIF
ENDPROC

PROC ADD_TATTOOS_NO_OVERLAP(PED_INDEX pedID, TATTOO_NAME_ENUM eTattoo, BOOL bPurchase = FALSE, INT iSlot = -1,BOOL bBlockTorsoDecals = FALSE)

	IF iSlot = -1
		iSlot = GET_ACTIVE_CHARACTER_SLOT()
	ENDIF

	CLEAR_PED_DECORATIONS_LEAVE_SCARS(pedID)
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID)
	
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(pedID)	
	
	GET_TATTOO_DATA(sTattooData, eTattoo, eFaction, pedID)
	
	TATTOO_GROUP_ENUM newTattoo = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sTattooData.sLabel, sTattooData.iUpgradeGroup)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 sNewLabel = sTattooData.sLabel
	STRING sUpgradeGroup = DEBUG_get_string_from_namehash(sTattooData.iUpgradeGroup)
	#ENDIF
	
	INT i
	BOOL bTatsOverlap = FALSE
	BOOL bBlockCrewEmblems = SHOULD_CREW_EMBLEMS_BE_BLOCKED_FOR_OUTFIT(pedID)
	BOOL bBlockBackTattoos = SHOULD_BACK_TATTOOS_BE_BLOCKED_FOR_OUTFIT(pedID)
	REPEAT MAX_NUMBER_OF_TATTOOS i
		IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, i), eFaction, pedID)
			IF IS_MP_TATTOO_CURRENT(sTattooData.eEnum, iSlot)
				TATTOO_GROUP_ENUM currentTattoo = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sTattooData.sLabel, sTattooData.iUpgradeGroup)
				
				bTatsOverlap = FALSE
				
				IF newTattoo != TG_INVALID
				AND currentTattoo != TG_INVALID
					IF sTattooData.eEnum != eTattoo
						IF DO_TATTOO_GROUPS_OVERLAP(currentTattoo, newTattoo)
							
							CWARNINGLN(DEBUG_SHOPS, "ADD_TATTOOS_NO_OVERLAP:DISC(", sTattooData.sLabel, ", ", sNewLabel, ") - true [", sUpgradeGroup, "]")
							
							bTatsOverlap = TRUE	
							IF bPurchase
								SET_MP_TATTOO_CURRENT(sTattooData.eEnum, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bTatsOverlap
				AND IS_TATTOO_SAFE_TO_APPLY(pedID, sTattooData.sLabel, sTattooData.eEnum, sTattooData.iCollection, sTattooData.iUpgradeGroup, FALSE, bBlockCrewEmblems, bBlockTorsoDecals, bBlockBackTattoos)
					ADD_PED_DECORATION_FROM_HASHES(pedID, sTattooData.iCollection, sTattooData.iPreset)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		// Apply fuzz overlay that was added for Beach Bum
		IF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_HO_FUZZ)
			IF GET_PED_DECORATION_ZONE_FROM_HASHES(HASH("mpBeach_overlays"), HASH("FM_Hair_Fuzz")) != PDZ_INVALID
				ADD_PED_DECORATION_FROM_HASHES(pedID, HASH("mpBeach_overlays"), HASH("FM_Hair_Fuzz"))
			ENDIF
		ENDIF
			
		//info for the exsiting tattoo
		sTattooShopItemValues 	sDLCTattooData		
		TATTOO_NAME_ENUM 		eDLCTattoo
				
		INT iDLCIndex		
		INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)		
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
					eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
				
					IF IS_MP_TATTOO_CURRENT(eDLCTattoo, iSlot)			
						//get the tat positions of the new DLC tat
						TATTOO_GROUP_ENUM currentDLCTattoo = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sDLCTattooData.Label, sDLCTattooData.UpdateGroup)
						
						bTatsOverlap = FALSE
						
						// Dont allow overlap in the same upgrade group or tat positions as the one we are viewing.
						IF newTattoo != TG_INVALID
						AND currentDLCTattoo != TG_INVALID
							IF eDLCTattoo != eTattoo
								IF sDLCTattooData.UpdateGroup != 0
									IF DO_TATTOO_GROUPS_OVERLAP(currentDLCTattoo, newTattoo)
										CWARNINGLN(DEBUG_SHOPS, "ADD_TATTOOS_NO_OVERLAP:DLC(", sDLCTattooData.Label, ", ", sNewLabel, ") - true [", sUpgradeGroup, "]")
										
										bTatsOverlap = TRUE
										IF bPurchase
											SET_MP_TATTOO_CURRENT(eDLCTattoo, FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bTatsOverlap
						AND IS_TATTOO_SAFE_TO_APPLY(pedID, sDLCTattooData.Label, eDLCTattoo, sDLCTattooData.Collection, sDLCTattooData.UpdateGroup, FALSE, bBlockCrewEmblems, bBlockTorsoDecals, bBlockBackTattoos)
							ADD_PED_DECORATION_FROM_HASHES(pedID, sDLCTattooData.Collection, sDLCTattooData.Preset)
							ADD_SECONDARY_PED_DECORATION_FROM_HASHES(pedID, sDLCTattooData.Collection, sDLCTattooData.Preset)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL IS_TATTOO_BLOCKED_IN_SHOP(TATTOO_NAME_ENUM eTattoo, TATTOO_FACTION_ENUM eFaction)

	#IF USE_TU_CHANGES
		SWITCH eFaction
			CASE TATTOO_MP_FM
			CASE TATTOO_MP_FM_F
				SWITCH eTattoo
					CASE TATTOO_MP_FM_CREW_A
					CASE TATTOO_MP_FM_CREW_B
					CASE TATTOO_MP_FM_CREW_C
					CASE TATTOO_MP_FM_CREW_D
					CASE TATTOO_MP_FM_CREW_E
					CASE TATTOO_MP_FM_CREW_F
						RETURN TRUE
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	#ENDIF
	
	#IF NOT USE_TU_CHANGES
		SWITCH eFaction
			CASE TATTOO_MP_FM
				SWITCH eTattoo
					CASE TATTOO_MP_FM_CREW_A
					CASE TATTOO_MP_FM_CREW_B
					CASE TATTOO_MP_FM_CREW_C
					CASE TATTOO_MP_FM_CREW_D
						RETURN TRUE
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(TATTOO_NAME_ENUM eTattoo, TATTOO_FACTION_ENUM eFaction)
	#IF USE_TU_CHANGES
		RETURN ((eFaction = TATTOO_MP_FM OR eFaction = TATTOO_MP_FM_F) AND eTattoo >= TATTOO_MP_FM_DLC)
	#ENDIF
	
	IF (eFaction = eFaction AND eTattoo = eTattoo)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Temp proc to give the player ped a tattoo
///    NOTE: This does not add it to the saved globals
PROC GIVE_PED_TEMP_TATTOO(PED_INDEX pedID, TATTOO_NAME_ENUM eTattoo)
	IF NOT IS_PED_INJURED(pedID)
		IF eTattoo != INVALID_TATTOO
			
			TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(pedID)
			
			TATTOO_DATA_STRUCT sTattooData
			IF GET_TATTOO_DATA(sTattooData, eTattoo, eFaction, pedID)
			
				// Do not give temp tattoo if we already have it otherwise it will assert!
				IF NOT HAS_PLAYER_PED_GOT_TATTOO(eTattoo)
				
					PRINTLN("GIVE_PED_TEMP_TATTOO")
					PRINTLN("...enum = ", ENUM_TO_INT(eTattoo))
					PRINTLN("...collection = ", sTattooData.iCollection)
					PRINTLN("...preset = ", sTattooData.iPreset)
					PRINTLN("...label = ", sTattooData.sLabel)
					PRINTLN("...name = ", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel))
					
					ADD_PED_DECORATION_FROM_HASHES(pedID, sTattooData.iCollection, sTattooData.iPreset)
					ADD_SECONDARY_PED_DECORATION_FROM_HASHES(pedID, sTattooData.iCollection, sTattooData.iPreset)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets tattoos in same upgrade group as not current
PROC REMOVE_TATTOOS_IN_SAME_UPGRADE_GROUP(TATTOO_NAME_ENUM eTattoo)
	// If this tattoo is part of an upgrade group then we need to remove the other tats.
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
	
	IF GET_TATTOO_DATA(sTattooData, eTattoo, eFaction, PLAYER_PED_ID())
		IF sTattooData.iUpgradeGroup != 0
			TATTOO_GROUP_ENUM eTattooGroup = TG_INVALID
			IF NETWORK_IS_GAME_IN_PROGRESS()
				eTattooGroup = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sTattooData.sLabel, sTattooData.iUpgradeGroup)
			ENDIF
			
			INT iUpgradeGroup = sTattooData.iUpgradeGroup
			INT iBitset, iBit
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_15 sNewLabel = sTattooData.sLabel
			STRING sUpgradeGroup = DEBUG_get_string_from_namehash(sTattooData.iUpgradeGroup)
			#ENDIF
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				INT iDLCIndex
				INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
				sTattooShopItemValues sDLCTattooData
				TATTOO_NAME_ENUM eDLCTattoo
				TATTOO_GROUP_ENUM eDLCTattooGroup
				
				REPEAT iDLCCount iDLCIndex
					IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
						IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
							eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
							eDLCTattooGroup = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sDLCTattooData.Label, sDLCTattooData.UpdateGroup)
							
							IF DO_TATTOO_GROUPS_OVERLAP(eDLCTattooGroup, eTattooGroup)
							AND eDLCTattoo != eTattoo
								
								CWARNINGLN(DEBUG_SHOPS, "REMOVE_TATTOOS_IN_SAME_UPGRADE_GROUP:DLC(", sDLCTattooData.Label, ", ", sNewLabel, ") - true [", sUpgradeGroup, "]")
								
								SET_MP_TATTOO_CURRENT(eDLCTattoo, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitset
				REPEAT 32 iBit
					IF NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
						IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitset, iBit), eFaction, PLAYER_PED_ID())
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
								
								TATTOO_GROUP_ENUM eBitsetTattooGroup
								eBitsetTattooGroup = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sTattooData.sLabel, sTattooData.iUpgradeGroup)
								
								IF DO_TATTOO_GROUPS_OVERLAP(eBitsetTattooGroup, eTattooGroup)
								AND sTattooData.eEnum != eTattoo
									
									CWARNINGLN(DEBUG_SHOPS, "REMOVE_TATTOOS_IN_SAME_UPGRADE_GROUP:BITSET(", sTattooData.sLabel, ", ", sNewLabel, ") - true [", sUpgradeGroup, "]")
									
									IF g_bInMultiplayer
										SET_MP_TATTOO_CURRENT(sTattooData.eEnum, FALSE)
									ELSE
										SET_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), sTattooData.eEnum, FALSE)
									ENDIF
								ENDIF
							ELSE
								IF sTattooData.iUpgradeGroup = iUpgradeGroup
								AND sTattooData.eEnum != eTattoo
									
									CWARNINGLN(DEBUG_SHOPS, "REMOVE_TATTOOS_IN_SAME_UPGRADE_GROUP:BITSET(", sTattooData.sLabel, ", ", sNewLabel, ") - true [", sUpgradeGroup, "]")
									
									IF g_bInMultiplayer
										SET_MP_TATTOO_CURRENT(sTattooData.eEnum, FALSE)
									ELSE
										SET_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), sTattooData.eEnum, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_PED_DECORATION_ZONE_FROM_INT(INT iCurrentGroup, PED_DECORATION_ZONE &eZone, TORSO_DECORATION_SUBZONE &eSubzone, BOOL paramSuppressAssert = FALSE, BOOL bSPFailCondition = FALSE)

	CPRINTLN(DEBUG_SHOPS, "GET_PED_DECORATION_ZONE_FROM_INT - iCurrentGroup: ", iCurrentGroup, "...")

	eZone = PDZ_INVALID
	eSubzone = TDS_INVALID
	SWITCH iCurrentGroup
		CASE 0
			eZone = PDZ_TORSO
			eSubzone = TDS_TORSO_BACK
			RETURN TRUE
		BREAK
		CASE 1
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				RETURN bSPFailCondition
			ENDIF
			
			eZone = PDZ_TORSO
			eSubzone = TDS_TORSO_BACK_FULL
			RETURN TRUE
		BREAK
		CASE 2
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				RETURN bSPFailCondition
			ENDIF
			
			eZone = PDZ_TORSO
			eSubzone = TDS_TORSO_CHEST
			RETURN TRUE
		BREAK
		CASE 3
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				RETURN bSPFailCondition
			ENDIF
			
			eZone = PDZ_TORSO
			eSubzone = TDS_TORSO_STOMACH
			RETURN TRUE
		BREAK
		CASE 4	eZone = PDZ_HEAD		RETURN TRUE BREAK
		CASE 5	eZone = PDZ_LEFT_ARM	RETURN TRUE BREAK
		CASE 6	eZone = PDZ_RIGHT_ARM	RETURN TRUE BREAK
		CASE 7	eZone = PDZ_LEFT_LEG	RETURN TRUE BREAK
		CASE 8	eZone = PDZ_RIGHT_LEG	RETURN TRUE BREAK
	ENDSWITCH
	
	IF NOT paramSuppressAssert
		CASSERTLN(DEBUG_SHOPS, "GET_PED_DECORATION_ZONE_FROM_INT - invalid iCurrentGroup: ", iCurrentGroup)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_TORSO_TATTOO_BELONG(MODEL_NAMES ePedModel, STRING sLabel, INT iupdateGroup, PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone)
	IF eZone = PDZ_INVALID
		RETURN FALSE
	ENDIF
	
	IF eZone = PDZ_TORSO
		IF eSubzone = TDS_INVALID
			CASSERTLN(DEBUG_SHOPS, "DOES_TORSO_TATTOO_BELONG \"", sLabel, "\" - invalid torso group and submenu combo!!!")
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			IF eSubzone = TDS_TORSO_BACK
				RETURN TRUE
			ENDIF
			RETURN FALSE
		ENDIF
		
		#IF USE_TU_CHANGES
		TATTOO_GROUP_ENUM eTattooGroup = GET_TATTOO_GROUP_FROM_LABEL(ePedModel, sLabel, iupdateGroup)
		SWITCH eTattooGroup
			CASE TG_INVALID
				RETURN FALSE
			BREAK
			CASE TG_BACK_FULL
			CASE TG_BACK_FULL_ARMS_FULL_BACK
			
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF eSubzone = TDS_TORSO_BACK_FULL
						RETURN TRUE
					ENDIF
					RETURN FALSE
				ELSE
					IF eSubzone = TDS_TORSO_BACK
						RETURN TRUE
					ENDIF
					RETURN FALSE
				ENDIF
			BREAK
			CASE TG_BACK_FULL_SHORT
			CASE TG_BACK_MID
			CASE TG_BACK_UPPER
			CASE TG_BACK_UPPER_LEFT
			CASE TG_BACK_UPPER_RIGHT
			CASE TG_BACK_LOWER
			CASE TG_BACK_LOWER_LEFT
			CASE TG_BACK_LOWER_MID
			CASE TG_BACK_LOWER_RIGHT
				IF eSubzone = TDS_TORSO_BACK
					RETURN TRUE
				ENDIF
				RETURN FALSE
			BREAK
			CASE TG_CHEST_FULL
			CASE TG_CHEST_STOM
			CASE TG_CHEST_STOM_FULL
			CASE TG_CHEST_LEFT
			CASE TG_CHEST_UPPER_LEFT
			CASE TG_CHEST_RIGHT
			CASE TG_CHEST_UPPER_RIGHT
			CASE TG_CHEST_MID
				IF eSubzone = TDS_TORSO_CHEST
					RETURN TRUE
				ENDIF
				RETURN FALSE
			BREAK
			CASE TG_STOMACH_FULL
			CASE TG_STOMACH_UPPER
			CASE TG_STOMACH_UPPER_LEFT
			CASE TG_STOMACH_UPPER_RIGHT
			CASE TG_STOMACH_LOWER
			CASE TG_STOMACH_LOWER_LEFT
			CASE TG_STOMACH_LOWER_RIGHT
			CASE TG_STOMACH_LEFT
			CASE TG_STOMACH_RIGHT
			CASE TG_TORSO_SIDE_RIGHT
			CASE TG_TORSO_SIDE_LEFT
				IF eSubzone = TDS_TORSO_STOMACH
					RETURN TRUE
				ENDIF
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		CASSERTLN(DEBUG_SHOPS, "DOES_TORSO_TATTOO_BELONG \"", sLabel, "\" - unknown group \"", GET_STRING_FROM_TATTOO_GROUP(eTattooGroup), "\" and torso submenu combo!!!")
		RETURN FALSE
		#ENDIF
		
		#IF NOT USE_TU_CHANGES
		UNUSED_PARAMETER(ePedModel)
		UNUSED_PARAMETER(sLabel)
		UNUSED_PARAMETER(iupdateGroup)
		RETURN TRUE
		#ENDIF
	ELSE
		IF eSubzone != TDS_INVALID
			CASSERTLN(DEBUG_SHOPS, "DOES_TORSO_TATTOO_BELONG \"", sLabel, "\" - invalid non-torso group and submenu combo!!!")
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
ENDFUNC


PROC BUILD_AVAILABLE_TATTOO_ITEMS(TATTOO_FACTION_ENUM eFaction, INT &iAvailableItemsBitset[], INT &iAvailableDLCItemsBitset[], PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID)
	INT iBitsetIndex, iBitIndex
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_NAME_ENUM eTattoo
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iDLCIndex
		INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
		sTattooShopItemValues sDLCTattooData
		
		REPEAT COUNT_OF(iAvailableDLCItemsBitset) iDLCIndex
			iAvailableDLCItemsBitset[iDLCIndex] = 0
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		IF eZone = PDZ_TORSO
			IF eSubzone = TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "BUILD_AVAILABLE_TATTOO_ITEMS - zone is torso but subzone is invalid, needs valid subzone")
			ENDIF
		ELSE
			IF eSubzone != TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "BUILD_AVAILABLE_TATTOO_ITEMS - zone isn't torso but subzone isn't invalid, needs invalid subzone")
			ENDIF
		ENDIF
		#ENDIF
		
		IF iDLCCount > MAX_DLC_TATTOOS_IN_SHOP
			IF (eFaction = TATTOO_MP_FM)
				CASSERTLN(DEBUG_SHOPS, "BUILD_AVAILABLE_TATTOO_ITEMS - Too many TATTOO_MP_FM DLC tattoos. Having to cap tattoos! iDLCCount=", iDLCCount, ", iDLCCount=", GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction), ", MAX_DLC_TATTOOS_IN_SHOP=", MAX_DLC_TATTOOS_IN_SHOP, ", MAX_NUMBER_OF_DLC_TATTOO_BITSETS=", MAX_NUMBER_OF_DLC_TATTOO_BITSETS)
			ELIF (eFaction = TATTOO_MP_FM_F)
				CASSERTLN(DEBUG_SHOPS, "BUILD_AVAILABLE_TATTOO_ITEMS - Too many TATTOO_MP_FM_F DLC tattoos. Having to cap tattoos! iDLCCount=", iDLCCount, ", iDLCCount=", GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction), ", MAX_DLC_TATTOOS_IN_SHOP=", MAX_DLC_TATTOOS_IN_SHOP, ", MAX_NUMBER_OF_DLC_TATTOO_BITSETS=", MAX_NUMBER_OF_DLC_TATTOO_BITSETS)
			ELSE
				CASSERTLN(DEBUG_SHOPS, "BUILD_AVAILABLE_TATTOO_ITEMS - Too many faction:", eFaction, " DLC tattoos. Having to cap tattoos! iDLCCount=", iDLCCount, ", MAX_DLC_TATTOOS_IN_SHOP=", MAX_DLC_TATTOOS_IN_SHOP, ", MAX_NUMBER_OF_DLC_TATTOO_BITSETS=", MAX_NUMBER_OF_DLC_TATTOO_BITSETS)
			ENDIF
			iDLCCount = MAX_DLC_TATTOOS_IN_SHOP
		ENDIF
		
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
				AND NOT IS_DLC_OVERLAY_LOCKED_BY_SCRIPT(sDLCTattooData.Collection, sDLCTattooData.Preset, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex))
				AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
				AND sDLCTattooData.UpdateGroup != HASH("torsoDecal")
					IF GET_PED_DECORATION_ZONE_FROM_HASHES(sDLCTattooData.Collection, sDLCTattooData.Preset) = eZone
					AND DOES_TORSO_TATTOO_BELONG(GET_ENTITY_MODEL(PLAYER_PED_ID()), sDLCTattooData.Label, sDLCTattooData.UpdateGroup, eZone, eSubzone)
						PRINTLN("BUILD_AVAILABLE_TATTOO_ITEMS() Adding DLC tattoo '", sDLCTattooData.Label, "'")
						PRINTLN("...UpgradeGroup = ", sDLCTattooData.UpdateGroup)
						PRINTLN("...Facing = ", sDLCTattooData.Facing)
						SET_BIT(iAvailableDLCItemsBitset[iDLCIndex/32], iDLCIndex%32)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
		
		iAvailableItemsBitset[iBitsetIndex] = 0
		
		REPEAT 32 iBitIndex
			eTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
			IF GET_TATTOO_DATA(sTattooData, eTattoo, eFaction, PLAYER_PED_ID())
				IF NOT IS_TATTOO_BLOCKED_IN_SHOP(eTattoo, eFaction)
				AND NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
				AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
				AND sTattooData.iUpgradeGroup != HASH("torsoDecal")
					IF IS_TATTOO_UNLOCKED(sTattooData.eEnum)
					OR (NETWORK_IS_GAME_IN_PROGRESS() AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
						IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = eZone
						AND DOES_TORSO_TATTOO_BELONG(GET_ENTITY_MODEL(PLAYER_PED_ID()), sTattooData.sLabel, sTattooData.iUpgradeGroup, eZone, eSubzone)
							PRINTLN("BUILD_AVAILABLE_TATTOO_ITEMS() Adding tattoo '", sTattooData.sLabel, "'")
							SET_BIT(iAvailableItemsBitset[iBitsetIndex], iBitIndex)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

FUNC BOOL IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iBitsetIndex, iBitIndex
		TATTOO_DATA_STRUCT sTattooData
		MODEL_NAMES eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		TATTOO_NAME_ENUM eTattoo
		
		#IF IS_DEBUG_BUILD
		IF eZone = PDZ_TORSO
			IF eSubzone = TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE - zone is torso but subzone is invalid, needs valid subzone")
			ENDIF
		ELSE
			IF eSubzone != TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE - zone isn't torso but subzone isn't invalid, needs invalid subzone")
			ENDIF
		ENDIF
		#ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			INT iDLCIndex
			INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
			sTattooShopItemValues sDLCTattooData
			
			REPEAT iDLCCount iDLCIndex
				IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
					IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
					AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
					AND sDLCTattooData.UpdateGroup != HASH("torsoDecal")
						IF GET_PED_DECORATION_ZONE_FROM_HASHES(sDLCTattooData.Collection, sDLCTattooData.Preset) = eZone
						AND DOES_TORSO_TATTOO_BELONG(eModel, sDLCTattooData.Label, sDLCTattooData.UpdateGroup, eZone, eSubzone)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF

		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
			REPEAT 32 iBitIndex
				eTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
				IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
					IF NOT IS_TATTOO_BLOCKED_IN_SHOP(eTattoo, eFaction)
					AND NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
					AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
					AND sTattooData.iUpgradeGroup != HASH("torsoDecal")
						IF IS_THIS_TATTOO_UNLOCKED(sTattooData.eEnum, sTattooData.sLabel)
						OR (NETWORK_IS_GAME_IN_PROGRESS() AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sTattooData.sLabel, sTattooData.iUpgradeGroup, eZone, eSubzone)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID)
	INT iCount = 0
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iBitsetIndex, iBitIndex
		TATTOO_DATA_STRUCT sTattooData
		MODEL_NAMES eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		TATTOO_NAME_ENUM eTattoo
		
		#IF IS_DEBUG_BUILD
		IF eZone = PDZ_TORSO
			IF eSubzone = TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "COUNT_UNVIEWED_TATTOOS_FOR_ZONE - zone is torso but subzone is invalid, needs valid subzone")
			ENDIF
		ELSE
			IF eSubzone != TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "COUNT_UNVIEWED_TATTOOS_FOR_ZONE - zone isn't torso but subzone isn't invalid, needs invalid subzone")
			ENDIF
		ENDIF
		#ENDIF
		
		#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
				INT iDLCIndex
				INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
				sTattooShopItemValues sDLCTattooData
				TATTOO_NAME_ENUM eDLCTattoo
				
				REPEAT iDLCCount iDLCIndex
					IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
						IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
						AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
						AND sDLCTattooData.UpdateGroup != HASH("torsoDecal")
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sDLCTattooData.Collection, sDLCTattooData.Preset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sDLCTattooData.Label, sDLCTattooData.UpdateGroup, eZone, eSubzone)
								eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
								IF NOT HAS_PLAYER_PED_VIEWED_TATTOO(eDLCTattoo)
									iCount ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		#ENDIF
		
		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
			REPEAT 32 iBitIndex
				eTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
				IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
					IF NOT IS_TATTOO_BLOCKED_IN_SHOP(eTattoo, eFaction)
					AND NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
					AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
					AND sTattooData.iUpgradeGroup != HASH("torsoDecal")
						IF IS_TATTOO_UNLOCKED(sTattooData.eEnum)
						AND NOT HAS_PLAYER_PED_VIEWED_TATTOO(sTattooData.eEnum)
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sTattooData.sLabel, sTattooData.iUpgradeGroup, eZone, eSubzone)
								iCount ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	RETURN iCount
ENDFUNC

FUNC BOOL HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iBitsetIndex, iBitIndex
		TATTOO_DATA_STRUCT sTattooData
		MODEL_NAMES eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		TATTOO_NAME_ENUM eTattoo
		
		#IF IS_DEBUG_BUILD
		IF eZone = PDZ_TORSO
			IF eSubzone = TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE - zone is torso but subzone is invalid, needs valid subzone")
			ENDIF
		ELSE
			IF eSubzone != TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE - zone isn't torso but subzone isn't invalid, needs invalid subzone")
			ENDIF
		ENDIF
		#ENDIF
		
		#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
				INT iDLCIndex
				INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
				sTattooShopItemValues sDLCTattooData
				TATTOO_NAME_ENUM eDLCTattoo
				
				REPEAT iDLCCount iDLCIndex
					IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
					AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
					AND sDLCTattooData.UpdateGroup != HASH("torsoDecal")
						IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sDLCTattooData.Collection, sDLCTattooData.Preset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sDLCTattooData.Label, sDLCTattooData.UpdateGroup, eZone, eSubzone)
								eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
								IF NOT IS_DLC_OVERLAY_LOCKED_BY_SCRIPT(sDLCTattooData.Collection, sDLCTattooData.Preset, eDLCTattoo)
								AND NOT IS_TATTOO_BLOCKED_IN_SHOP(eDLCTattoo, eFaction)
								AND SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eDLCTattoo, eFaction)
								AND NOT HAS_PLAYER_PED_VIEWED_TATTOO(eDLCTattoo)
									CDEBUG1LN(DEBUG_SHOPS, "HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE ", sDLCTattooData.Label, " \"", GET_STRING_FROM_TEXT_FILE(sDLCTattooData.Label), "\" DLC not viewed zone:", eZone, ", eSubzone:", eSubzone)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		#ENDIF
		
		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
			REPEAT 32 iBitIndex
				eTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
				IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
					IF NOT IS_TATTOO_BLOCKED_IN_SHOP(eTattoo, eFaction)
					AND NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
					AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
					AND sTattooData.iUpgradeGroup != HASH("torsoDecal")
						IF IS_TATTOO_UNLOCKED(sTattooData.eEnum)
						AND NOT HAS_PLAYER_PED_VIEWED_TATTOO(sTattooData.eEnum)
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sTattooData.sLabel, sTattooData.iUpgradeGroup, eZone, eSubzone)
								CDEBUG1LN(DEBUG_SHOPS, "HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE ", sTattooData.sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\" not viewed zone:", eZone, ", eSubzone:", eSubzone)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(TATTOO_NAME_ENUM eTattoo, STRING paramLabel, PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF HAS_PLAYER_PED_GOT_TATTOO(eTattoo)
		CDEBUG1LN(DEBUG_SHOPS, "should_discount_be_displayed_for_tattoo player has tattoo ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
		RETURN FALSE
	ENDIF

	IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_ALL_TATTOOS
		CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_ALL ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
		RETURN TRUE
	ENDIF
	
	IF eZone = PDZ_TORSO
		SWITCH eSubzone
			CASE TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO - zone is torso but subzone is invalid, needs valid subzone")
				RETURN FALSE
			BREAK
			
			CASE TDS_TORSO_BACK
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_BACK
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_TORSO_BACK ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE TDS_TORSO_BACK_FULL
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_BACK_FULL
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_TORSO_BACK_FULL ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE TDS_TORSO_CHEST
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_CHEST
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_TORSO_BACK ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE TDS_TORSO_STOMACH
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_STOMACH
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_TORSO_BACK ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF eSubzone != TDS_INVALID
			CASSERTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO - zone isn't torso but subzone isn't invalid, needs invalid subzone")
		ENDIF
		
		SWITCH eZone
			CASE PDZ_HEAD
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HEAD
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_HEAD ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE PDZ_LEFT_ARM
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_LEFT_ARM
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_LEFT_ARM ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE PDZ_RIGHT_ARM
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_RIGHT_ARM
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_RIGHT_ARM ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE PDZ_LEFT_LEG
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_LEFT_LEG
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_LEFT_LEG ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
			CASE PDZ_RIGHT_LEG
				IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_RIGHT_LEG
					CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_RIGHT_LEG ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	INT i, iLabelHash = GET_HASH_KEY(paramLabel)
	REPEAT COUNT_OF(g_sMPTunables.iINDIVIDUAL_TATTOO_SALE_HASH_LABELS) i
		IF (g_sMPTunables.iINDIVIDUAL_TATTOO_SALE_HASH_LABELS[i] != 0)
			IF (iLabelHash = g_sMPTunables.iINDIVIDUAL_TATTOO_SALE_HASH_LABELS[i])
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO_HASH_", i, " ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\" ", iLabelHash)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SHOPS, "should_discount_be_displayed_for_tattoo ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\" ", iLabelHash)
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PED_DECORATION_ZONE eZone, TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iBitsetIndex, iBitIndex
		TATTOO_DATA_STRUCT sTattooData
		MODEL_NAMES eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		TATTOO_NAME_ENUM eTattoo
		
		#IF IS_DEBUG_BUILD
		IF eZone = PDZ_TORSO
			IF eSubzone = TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE - zone is torso but subzone is invalid, needs valid subzone")
			ENDIF
		ELSE
			IF eSubzone != TDS_INVALID
				CASSERTLN(DEBUG_SHOPS, "DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE - zone isn't torso but subzone isn't invalid, needs invalid subzone")
			ENDIF
		ENDIF
		#ENDIF
		
		#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
				INT iDLCIndex
				INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
				sTattooShopItemValues sDLCTattooData
				TATTOO_NAME_ENUM eDLCTattoo
				
				REPEAT iDLCCount iDLCIndex
					IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
					AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
					AND sDLCTattooData.UpdateGroup != HASH("torsoDecal")
						IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sDLCTattooData.Collection, sDLCTattooData.Preset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sDLCTattooData.Label, sDLCTattooData.UpdateGroup, eZone, eSubzone)
								eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
								IF NOT IS_DLC_OVERLAY_LOCKED_BY_SCRIPT(sDLCTattooData.Collection, sDLCTattooData.Preset, eDLCTattoo)
								AND NOT IS_TATTOO_BLOCKED_IN_SHOP(eDLCTattoo, eFaction)
								AND SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eDLCTattoo, eFaction)
								AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(eDLCTattoo, sDLCTattooData.Label, eZone, eSubzone)
									CPRINTLN(DEBUG_SHOPS, "DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE ", sDLCTattooData.Label, " \"", GET_STRING_FROM_TEXT_FILE(sDLCTattooData.Label), "\" DLC discount zone:", eZone, ", eSubzone:", eSubzone)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		#ENDIF
		
		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
			REPEAT 32 iBitIndex
				eTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
				IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
					IF NOT IS_TATTOO_BLOCKED_IN_SHOP(eTattoo, eFaction)
					AND NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
					AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
					AND sTattooData.iUpgradeGroup != HASH("torsoDecal")
						IF IS_TATTOO_UNLOCKED(sTattooData.eEnum)
						AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(sTattooData.eEnum, sTattooData.sLabel, eZone, eSubzone)
							IF GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset) = eZone
							AND DOES_TORSO_TATTOO_BELONG(eModel, sTattooData.sLabel, sTattooData.iUpgradeGroup, eZone, eSubzone)
								CPRINTLN(DEBUG_SHOPS, "DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE ", sTattooData.sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\" discount zone:", eZone, ", eSubzone:", eSubzone)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	
	CDEBUG1LN(DEBUG_SHOPS, "does_player_have_discounted_options_in_zone has not discount zone:", eZone, ", eSubzone:", eSubzone)
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks to see if the player has tattoos in the 5 main zones
FUNC BOOL HAS_PLAYER_GOT_TATTOOS_FOR_HUMAN_CANVAS_AWARD()

	INT iZonesToCheck = 0
	SET_BIT(iZonesToCheck, ENUM_TO_INT(PDZ_TORSO))
	SET_BIT(iZonesToCheck, ENUM_TO_INT(PDZ_LEFT_ARM))
	SET_BIT(iZonesToCheck, ENUM_TO_INT(PDZ_LEFT_LEG))
	SET_BIT(iZonesToCheck, ENUM_TO_INT(PDZ_RIGHT_ARM))
	SET_BIT(iZonesToCheck, ENUM_TO_INT(PDZ_RIGHT_LEG))
	SET_BIT(iZonesToCheck, ENUM_TO_INT(PDZ_HEAD))

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iBitsetIndex, iBitIndex
		TATTOO_DATA_STRUCT sTattooData
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		TATTOO_NAME_ENUM eTattoo
		
		#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
				INT iDLCIndex
				INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
				sTattooShopItemValues sDLCTattooData
				TATTOO_NAME_ENUM eDLCTattoo
				
				REPEAT iDLCCount iDLCIndex
					IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
					AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
					AND sDLCTattooData.UpdateGroup != HASH("torsoDecal")
						eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
						IF (IS_MP_TATTOO_CURRENT(eDLCTattoo))
							CLEAR_BIT(iZonesToCheck, ENUM_TO_INT(GET_PED_DECORATION_ZONE_FROM_HASHES(sDLCTattooData.Collection, sDLCTattooData.Preset)))
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		#ENDIF

		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
			REPEAT 32 iBitIndex
				eTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
				IF NOT IS_TATTOO_BLOCKED_IN_SHOP(eTattoo, eFaction)
				AND NOT SHOULD_TATTOO_ENUM_USE_DLC_SPECIFIC_CHECKS(eTattoo, eFaction)
					IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
					AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
					AND sTattooData.iUpgradeGroup != HASH("torsoDecal")
						IF g_bInMultiplayer
							IF (IS_MP_TATTOO_CURRENT(sTattooData.eEnum))
								CLEAR_BIT(iZonesToCheck, ENUM_TO_INT(GET_PED_DECORATION_ZONE_FROM_HASHES(sTattooData.iCollection, sTattooData.iPreset)))
							ENDIF
						ELSE
							IF (IS_PLAYER_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), sTattooData.eEnum))
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	
	RETURN (iZonesToCheck = 0)
ENDFUNC

FUNC INT GET_TATTOO_DISCOUNT()
	INT iDiscount = 0
	
	// MP only discount
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_GIVE_SHOP_DISCOUNT()
			iDiscount += g_sMPTunables.iShopDiscountPercentValue
		ENDIF
	ENDIF
	
//	// 20% discount for SE
//	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
//		IF IS_SPECIAL_EDITION_GAME() OR IS_COLLECTORS_EDITION_GAME()
//			iDiscount += 20
//		ENDIF
//	ENDIF
	
	RETURN iDiscount
ENDFUNC

FUNC CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT GET_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FROM_LABEL(TEXT_LABEL_15 tlLabel)
	IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()
		SWITCH GET_HASH_KEY(tlLabel)
			CASE HASH("TAT_BI_000") /* "Demon Rider				*/		RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DEMON_RIDER
			CASE HASH("TAT_BI_001") /* "Both Barrels			*/      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BOTH_BARRELS
			CASE HASH("TAT_BI_002") /* "Rose Tribute            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_ROSE_TRIBUTE
			CASE HASH("TAT_BI_003") /* "Web Rider               */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WEB_RIDER
			CASE HASH("TAT_BI_004") /* "Dragon's Fury           */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DRAGONS_FURY
			CASE HASH("TAT_BI_005") /* "Made In America         */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MADE_IN_AMERICA
			CASE HASH("TAT_BI_006") /* "Chopper Freedom         */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CHOPPER_FREEDOM
			CASE HASH("TAT_BI_007") /* "Raptor Pounce           */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SWOOPING_EAGLE
			CASE HASH("TAT_BI_008") /* "Freedom Wheels          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FREEMOM_WHEELS
			CASE HASH("TAT_BI_009") /* "Morbid Arachnid         */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MORBID_ARACHNID
			CASE HASH("TAT_BI_010") /* "Skull of Taurus         */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SKULL_OF_TAURUS
			CASE HASH("TAT_BI_011") /* "R.I.P. My Brothers      */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIP_MY_BROTHERS
			CASE HASH("TAT_BI_012") /* "Urban Stunter           */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_URBAN_STUNTER
			CASE HASH("TAT_BI_013") /* "Demon Crossbones        */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DEMON_CROSSBONES
			CASE HASH("TAT_BI_014") /* "Lady Mortality          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_LADY_MORTALITY
			CASE HASH("TAT_BI_015") /* "Ride or Die             */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_OR_DIE
			CASE HASH("TAT_BI_016") /* "Macabre Tree            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MACABRE_TREE
			CASE HASH("TAT_BI_017") /* "Clawed Beast            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CLAWED_BEAST
			CASE HASH("TAT_BI_018") /* "Skeletal Chopper        */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SKELETAL_CHOPPER
			CASE HASH("TAT_BI_019") /* "Gruesome Talons         */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GRUESOME_TALONS
			CASE HASH("TAT_BI_020") /* "Cranial Rose            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CRANIAL_ROSE
			CASE HASH("TAT_BI_021") /* "Flaming Reaper          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FLAMING_REAPER
			CASE HASH("TAT_BI_022") /* "Western Insignia        */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_INSIGNIA
			CASE HASH("TAT_BI_023") /* "Western MC              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_MC
			CASE HASH("TAT_BI_024") /* "Live to Ride            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_LIVE_TO_RIDE
			CASE HASH("TAT_BI_025") /* "Good Luck               */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GOOD_LUCK
			CASE HASH("TAT_BI_026") /* "American Dream          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_AMERICAN_DREAM
			CASE HASH("TAT_BI_027") /* "Bad Luck                */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BAD_LUCK
			CASE HASH("TAT_BI_028") /* "Dusk Rider              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DUSK_RIDER
			CASE HASH("TAT_BI_029") /* "Bone Wrench             */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BONE_WRENCH
			CASE HASH("TAT_BI_030") /* "Brothers For Life       */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BROTHERS_FOR_LIFE
			CASE HASH("TAT_BI_031") /* "Gear Head               */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GEAR_HEAD
			CASE HASH("TAT_BI_032") /* "Western Eagle           */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_EAGLE
			CASE HASH("TAT_BI_033") /* "Eagle Emblem            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_EAGLE_EMBLEM
			CASE HASH("TAT_BI_034") /* "Brotherhood of Bikes    */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BROTHERHOOD_OF_BIKES
			CASE HASH("TAT_BI_035") /* "Chain Fist              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CHAIN_FIST
			CASE HASH("TAT_BI_036") /* "Engulfed Skull          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_ENGULFED_SKULL
			CASE HASH("TAT_BI_037") /* "Scorched Soul           */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SCORCHED_SOUL
			CASE HASH("TAT_BI_038") /* "FTW                     */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FTW
			CASE HASH("TAT_BI_039") /* "Gas Guzzler             */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GAS_GUZZLER
			CASE HASH("TAT_BI_040") /* "American Made           */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_AMERICAN_MADE
			CASE HASH("TAT_BI_041") /* "No Regrets              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_NO_REGRETS
			CASE HASH("TAT_BI_042") /* "Grim Rider              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GRIM_RIDER
			CASE HASH("TAT_BI_043") /* "Ride Forever            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_FOREVER
			CASE HASH("TAT_BI_044") /* "Ride Free               */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_FREE
			CASE HASH("TAT_BI_045") /* "Ride Hard Die Fast      */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_HARD_DIE_FAST
			CASE HASH("TAT_BI_046") /* "Skull Chain             */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SKULL_CHAIN
			CASE HASH("TAT_BI_047") /* "Snake Bike              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SNAKE_BIKE
			CASE HASH("TAT_BI_048") /* "STFU                    */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_STFU
			CASE HASH("TAT_BI_049") /* "These Colors Don't Run  */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_THESE_COLOURS_DONT_RUN
			CASE HASH("TAT_BI_050") /* "Unforgiven              */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_UNFORGIVEN
			CASE HASH("TAT_BI_051") /* "Western Stylized        */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_STYLIZED
			CASE HASH("TAT_BI_052") /* "Biker Mount             */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BIKER_MOUNT
			CASE HASH("TAT_BI_053") /* "Muffler Helmet          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MUFFLER_HELMET
			CASE HASH("TAT_BI_054") /* "Mum                     */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MUM
			CASE HASH("TAT_BI_055") /* "Poison Scorpion         */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_POISON_SCORPION
			CASE HASH("TAT_BI_056") /* "Bone Cruiser            */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BONE_CRUISER
			CASE HASH("TAT_BI_057") /* "Laughing Skull          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_LAUGHING_SKULL
			CASE HASH("TAT_BI_058") /* "Reaper Vulture          */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_REAPER_VULTURE
			CASE HASH("TAT_BI_059") /* "Faggio                  */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FAGGIO
			CASE HASH("TAT_BI_060") /* "We Are The Mods!        */      RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WE_ARE_THE_MODS
		ENDSWITCH
	ENDIF
	RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END
ENDFUNC
FUNC BOOL SHOULD_TATTOO_BE_FREE_FOR_PLAYER(TEXT_LABEL_15 tlLabel)
	RETURN GET_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FROM_LABEL(tlLabel) != CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END
ENDFUNC

FUNC INT GET_TATTOO_DISPLAY_PRICE(INT iCost, TEXT_LABEL_15 tlLabel)

	// Price override for basket system
	IF SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
		BOOL bStarterPackItem = FALSE
		IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(tlLabel)
			bStarterPackItem = TRUE
		ENDIF
		
		TEXT_LABEL_63 tlCatalogueKey
		GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, tlLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
		IF NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
			RETURN NET_GAMESERVER_GET_PRICE(GET_HASH_KEY(tlCatalogueKey), CATEGORY_TATTOO, 1)
		ELSE
			RETURN -1
		ENDIF
	ENDIF
	
	IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(tlLabel)
		RETURN 0
	ENDIF

	// apply tuneables discount
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iLabelHash = GET_HASH_KEY(tlLabel)
		SWITCH iLabelHash
			CASE HASH("TAT_BB_000")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Los_Santos_Wreath               BREAK //8500                  BREAK //"DLC_FEMALE_TATTOOS_LOS_SANTOS_WREATH"            
			CASE HASH("TAT_BB_001")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Hibiscus_Flower_Duo             BREAK //6900                  BREAK //"DLC_FEMALE_TATTOOS_HIBISCUS_FLOWER_DUO"          
			CASE HASH("TAT_BB_002")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Tribal_Flower                   BREAK //3500                  BREAK //"DLC_FEMALE_TATTOOS_TRIBAL_FLOWER"                
			CASE HASH("TAT_BB_003")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Rock_Solid                      BREAK //5500                  BREAK //"DLC_FEMALE_TATTOOS_ROCK_SOLID"                   
			CASE HASH("TAT_BB_004")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Catfish                         BREAK //5250                  BREAK //"DLC_FEMALE_TATTOOS_CATFISH"                      
			CASE HASH("TAT_BB_005")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Shrimp                          BREAK //2500                  BREAK //"DLC_FEMALE_TATTOOS_SHRIMP"                       
			CASE HASH("TAT_BB_006")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Love_Dagger                     BREAK //6850                  BREAK //"DLC_FEMALE_TATTOOS_LOVE_DAGGER"                  
			CASE HASH("TAT_BB_007")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_School_of_Fish                  BREAK //2950                  BREAK //"DLC_FEMALE_TATTOOS_SCHOOL_OF_FISH"               
			CASE HASH("TAT_BB_008")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Tribal_Butterfly                BREAK //1700                  BREAK //"DLC_FEMALE_TATTOOS_TRIBAL_BUTTERFLY"             
			CASE HASH("TAT_BB_009")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Hibiscus_Flower                 BREAK //2650                  BREAK //"DLC_FEMALE_TATTOOS_HIBISCUS_FLOWER"              
			CASE HASH("TAT_BB_010")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Dolphin                         BREAK //1900                  BREAK //"DLC_FEMALE_TATTOOS_DOLPHIN"                      
			CASE HASH("TAT_BB_011")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Sea_Horses                      BREAK //5100                  BREAK //"DLC_FEMALE_TATTOOS_SEA_HORSES"                   
			CASE HASH("TAT_BB_012")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Anchor                          BREAK //2500                  BREAK //"DLC_FEMALE_TATTOOS_ANCHOR"                       
			CASE HASH("TAT_BB_013")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Anchor                          BREAK //2500                  BREAK //"DLC_FEMALE_TATTOOS_ANCHOR"                       
			CASE HASH("TAT_BB_014")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Swallow                         BREAK //2100                  BREAK //"DLC_FEMALE_TATTOOS_SWALLOW"                      
			CASE HASH("TAT_BB_015")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Tribal_Fish                     BREAK //3700                  BREAK //"DLC_FEMALE_TATTOOS_TRIBAL_FISH"                  
			CASE HASH("TAT_BB_016")				iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Parrot                          BREAK //5250                  BREAK //"DLC_FEMALE_TATTOOS_PARROT"                       
			CASE HASH("TAT_BUS_F_002")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_High_Roller                     BREAK //7000                  BREAK //"DLC_FEMALE_TATTOOS_HIGH_ROLLER"                  
			CASE HASH("TAT_BUS_F_000")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Respect                         BREAK //4200                  BREAK //"DLC_FEMALE_TATTOOS_RESPECT"                      
			CASE HASH("TAT_BUS_F_006")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Single                          BREAK //4850                  BREAK //"DLC_FEMALE_TATTOOS_SINGLE"                       
			CASE HASH("TAT_BUS_F_007")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_ValdeGrace_Logo                 BREAK //1900                  BREAK //"DLC_FEMALE_TATTOOS_VALDEGRACE_LOGO"              
			CASE HASH("TAT_BUS_F_008")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Money_Rose                      BREAK //2500                  BREAK //"DLC_FEMALE_TATTOOS_MONEY_ROSE"                   
			CASE HASH("TAT_BUS_F_010")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Diamond_Crown                   BREAK //4500                  BREAK //"DLC_FEMALE_TATTOOS_DIAMOND_CROWN"                
			CASE HASH("TAT_BUS_F_009")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Dollar_Sign                     BREAK //4900                  BREAK //"DLC_FEMALE_TATTOOS_DOLLAR_SIGN"                  
			CASE HASH("TAT_BUS_F_011")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Diamond_Jack                    BREAK //6800                  BREAK //"DLC_FEMALE_TATTOOS_DIAMOND_JACK"                 
			CASE HASH("TAT_BUS_F_003")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Makin_Money                     BREAK //7200                  BREAK //"DLC_FEMALE_TATTOOS_MAKIN_MONEY"                  
			CASE HASH("TAT_BUS_F_001")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Gold_Digger                     BREAK //2700                  BREAK //"DLC_FEMALE_TATTOOS_GOLD_DIGGER"                  
			CASE HASH("TAT_BUS_F_005")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Greed_is_Good                   BREAK //5500                  BREAK //"DLC_FEMALE_TATTOOS_GREED_IS_GOOD"                
			CASE HASH("TAT_BUS_F_004")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Love_Money                      BREAK //1600                  BREAK //"DLC_FEMALE_TATTOOS_LOVE_MONEY"                   
			CASE HASH("TAT_BUS_F_012")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Santo_Capra_Logo                BREAK //1800                  BREAK //"DLC_FEMALE_TATTOOS_SANTO_CAPRA_LOGO"             
			CASE HASH("TAT_BUS_F_013")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Money_Bag                       BREAK //1500                  BREAK //"DLC_FEMALE_TATTOOS_MONEY_BAG"                    
			CASE HASH("TAT_BUS_F_014")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Crew_Emblem_Chest               BREAK //20000                 BREAK //"DLC_FEMALE_TATTOOS_CREW_EMBLEM_CHEST"            
			CASE HASH("TAT_BUS_F_015")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Crew_Emblem_Arm                 BREAK //19500                 BREAK //"DLC_FEMALE_TATTOOS_CREW_EMBLEM_ARM"              
			CASE HASH("TAT_BB_017")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Mermaid_LS                         BREAK //6600                    BREAK //"DLC_MALE_TATTOO_MERMAID_LS"                      
			CASE HASH("TAT_BB_018")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Ship_Arms                          BREAK //7250                  BREAK //"DLC_MALE_TATTOO_SHIP_ARMS"                       
			CASE HASH("TAT_BB_019")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Hammerhead                  BREAK //5800                  BREAK //"DLC_MALE_TATTOO_TRIBAL_HAMMERHEAD"               
			CASE HASH("TAT_BB_020")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Shark                       BREAK //5900                  BREAK //"DLC_MALE_TATTOO_TRIBAL_SHARK"                    
			CASE HASH("TAT_BB_021")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Pirate_Skull                       BREAK //12000                 BREAK //"DLC_MALE_TATTOO_PIRATE_SKULL"                    
			CASE HASH("TAT_BB_022")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Surf_LS                            BREAK //1450                  BREAK //"DLC_MALE_TATTOO_SURF_LS"                         
			CASE HASH("TAT_BB_023")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Swordfish                          BREAK //3100                  BREAK //"DLC_MALE_TATTOO_SWORDFISH"                       
			CASE HASH("TAT_BB_024")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tiki_Tower                         BREAK //4800                  BREAK //"DLC_MALE_TATTOO_TIKI_TOWER"                      
			CASE HASH("TAT_BB_025")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Tiki_Tower                  BREAK //6500                  BREAK //"DLC_MALE_TATTOO_TRIBAL_TIKI_TOWER"               
			CASE HASH("TAT_BB_026")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Sun                         BREAK //6200                  BREAK //"DLC_MALE_TATTOO_TRIBAL_SUN"                      
			CASE HASH("TAT_BB_027")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Star                        BREAK //4450                  BREAK //"DLC_MALE_TATTOO_TRIBAL_STAR"                     
			CASE HASH("TAT_BB_028")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Little_Fish                        BREAK //1650                  BREAK //"DLC_MALE_TATTOO_LITTLE_FISH"                     
			CASE HASH("TAT_BB_029")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Surfs_Up                           BREAK //2250                  BREAK //"DLC_MALE_TATTOO_SURFS_UP"                        
			CASE HASH("TAT_BB_030")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Vespucci_Beauty                    BREAK //7000                  BREAK //"DLC_MALE_TATTOO_VESPUCCI_BEAUTY"                 
			CASE HASH("TAT_BB_031")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Shark                              BREAK //1850                  BREAK //"DLC_MALE_TATTOO_SHARK"                           
			CASE HASH("TAT_BB_032")				iCost = g_sMPTunables.iDLC_MALE_TATTOO_Wheel                              BREAK //5500                  BREAK //"DLC_MALE_TATTOO_WHEEL"                           
			CASE HASH("TAT_BUS_005")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Cash_King                          BREAK //5000                  BREAK //"DLC_MALE_TATTOO_CASH_KING"                       
			CASE HASH("TAT_BUS_003")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_100_Bill                           BREAK //3500                  BREAK //"DLC_MALE_TATTOO_100_BILL"                        
			CASE HASH("TAT_BUS_011")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Hustler                            BREAK //6400                  BREAK //"DLC_MALE_TATTOO_HUSTLER"                         
			CASE HASH("TAT_BUS_000")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Makin_Paper                        BREAK //5500                  BREAK //"DLC_MALE_TATTOO_MAKIN_PAPER"                     
			CASE HASH("TAT_BUS_006")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Bold_Dollar_Sign                   BREAK //1600                  BREAK //"DLC_MALE_TATTOO_BOLD_DOLLAR_SIGN"                
			CASE HASH("TAT_BUS_007")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Script_Dollar_Sign                 BREAK //1750                  BREAK //"DLC_MALE_TATTOO_SCRIPT_DOLLAR_SIGN"              
			CASE HASH("TAT_BUS_008")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_100                                BREAK //6900                  BREAK //"DLC_MALE_TATTOO_100"                             
			CASE HASH("TAT_BUS_009")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Dollar_Skull                       BREAK //4800                  BREAK //"DLC_MALE_TATTOO_DOLLAR_SKULL"                    
			CASE HASH("TAT_BUS_001")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Rich                               BREAK //3250                  BREAK //"DLC_MALE_TATTOO_RICH"                            
			CASE HASH("TAT_BUS_002")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_DOLLAR_SIGNS                       BREAK //3500                  BREAK //"DLC_MALE_TATTOO_DOLLAR_SIGNS"                    
			CASE HASH("TAT_BUS_004")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_AllSeeing_Eye                      BREAK //7300                  BREAK //"DLC_MALE_TATTOO_ALLSEEING_EYE"                   
			CASE HASH("TAT_BUS_010")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Green                              BREAK //1500                  BREAK //"DLC_MALE_TATTOO_GREEN"                           
			CASE HASH("TAT_BUS_012")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Crew_Emblem_Chest                  BREAK //20000                 BREAK //"DLC_MALE_TATTOO_CREW_EMBLEM_CHEST"               
			CASE HASH("TAT_BUS_013")			iCost = g_sMPTunables.iDLC_MALE_TATTOO_Crew_Emblem_Arm                    BREAK //19500                 BREAK //"DLC_MALE_TATTOO_CREW_EMBLEM_ARM"
			
			CASE HASH("TAT_HP_000")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[0]			  BREAK // "Arrows"
			CASE HASH("TAT_HP_001")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[1]			  BREAK // "Axe"
			CASE HASH("TAT_HP_002")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[2]			  BREAK // "Dog"
			CASE HASH("TAT_HP_003")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[3]			  BREAK // "Bomb"
			CASE HASH("TAT_HP_004")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[4]			  BREAK // "Bone"
			CASE HASH("TAT_HP_005")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[5]			  BREAK // "The End"
			CASE HASH("TAT_HP_006")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[6]			  BREAK // "Blade"
			CASE HASH("TAT_HP_007")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[7]			  BREAK // "Bricks"
			CASE HASH("TAT_HP_008")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[8]			  BREAK // "Cube"
			CASE HASH("TAT_HP_009")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[9]			  BREAK // "Squares"
			CASE HASH("TAT_HP_010")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[10]			  BREAK // "Rock"
			CASE HASH("TAT_HP_011")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[11]			  BREAK // "Flower"
			CASE HASH("TAT_HP_012")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[12]			  BREAK // "Cash Heart"
			CASE HASH("TAT_HP_013")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[13]			  BREAK // "Heartbreak"
			CASE HASH("TAT_HP_014")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[14]			  BREAK // "Angry Heart"
			CASE HASH("TAT_HP_015")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[15]			  BREAK // "Hope"
			CASE HASH("TAT_HP_016")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[16]			  BREAK // "Lighting Bolt"
			CASE HASH("TAT_HP_017")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[17]			  BREAK // "Padlock"
			CASE HASH("TAT_HP_018")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[18]			  BREAK // "Primal"
			CASE HASH("TAT_HP_019")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[19]			  BREAK // "Charm"
			CASE HASH("TAT_HP_020")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[20]			  BREAK // "Noose"
			CASE HASH("TAT_HP_021")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[21]			  BREAK // "Okay"
			CASE HASH("TAT_HP_022")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[22]			  BREAK // "Pencil"
			CASE HASH("TAT_HP_023")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[23]			  BREAK // "Smiley"
			CASE HASH("TAT_HP_024")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[24]			  BREAK // "Pyramid"
			CASE HASH("TAT_HP_025")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[25]			  BREAK // "Watch Your Step"
			CASE HASH("TAT_HP_026")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[26]			  BREAK // "Razor"
			CASE HASH("TAT_HP_027")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[27]			  BREAK // "Rocket"
			CASE HASH("TAT_HP_028")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[28]			  BREAK // "Thorny Rose"
			CASE HASH("TAT_HP_029")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[29]			  BREAK // "Sad"
			CASE HASH("TAT_HP_030")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[30]			  BREAK // "Shark Fin"
			CASE HASH("TAT_HP_031")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[31]			  BREAK // "Skateboard"
			CASE HASH("TAT_HP_032")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[32]			  BREAK // "Cartoon Skull"
			CASE HASH("TAT_HP_033")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[33]			  BREAK // "Stag"
			CASE HASH("TAT_HP_034")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[34]			  BREAK // "Stop"
			CASE HASH("TAT_HP_035")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[35]			  BREAK // "Shades"
			CASE HASH("TAT_HP_036")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[36]			  BREAK // "Shapes"
			CASE HASH("TAT_HP_037")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[37]			  BREAK // "Sunrise"
			CASE HASH("TAT_HP_038")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[38]			  BREAK // "Grub"
			CASE HASH("TAT_HP_039")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[39]			  BREAK // "Mask"
			CASE HASH("TAT_HP_040")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[40]			  BREAK // "Gravestone"
			CASE HASH("TAT_HP_041")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[41]			  BREAK // "Tooth"
			CASE HASH("TAT_HP_042")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[42]			  BREAK // "Watching You"
			CASE HASH("TAT_HP_043")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[43]			  BREAK // "Triangle White"
			CASE HASH("TAT_HP_044")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[44]			  BREAK // "Triangle Black"
			CASE HASH("TAT_HP_045")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[45]			  BREAK // "Mesh Band"
			CASE HASH("TAT_HP_046")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[46]			  BREAK // "Triangles"
			CASE HASH("TAT_HP_047")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[47]			  BREAK // "Unlucky 13"
			CASE HASH("TAT_HP_048")				iCost = g_sMPTunables.iDLC_hipster_tattoo_price_modifiers[48]			  BREAK // "Peace"
			
			#IF IS_NEXTGEN_BUILD
			CASE HASH("TAT_LX_000")				iCost = g_sMPTunables.iluxe1_both_genders_serpent_of_death				  BREAK // "Serpent of Death"
			CASE HASH("TAT_LX_001")				iCost = g_sMPTunables.iluxe1_both_genders_elaborate_los_muertos			  BREAK // "Elaborate Los Muertos"
			CASE HASH("TAT_LX_003")				iCost = g_sMPTunables.iluxe1_both_genders_abstract_skull				  BREAK // "Abstract Skull"
			CASE HASH("TAT_LX_004")				iCost = g_sMPTunables.iluxe1_both_genders_floral_raven					  BREAK // "Floral Raven"
			CASE HASH("TAT_LX_006")				iCost = g_sMPTunables.iluxe1_both_genders_adorned_wolf					  BREAK // "Adorned Wolf"
			CASE HASH("TAT_LX_007")				iCost = g_sMPTunables.iluxe1_both_genders_eye_of_the_griffin			  BREAK // "Eye of the Griffin"
			CASE HASH("TAT_LX_008")				iCost = g_sMPTunables.iluxe1_both_genders_flying_eye					  BREAK // "Flying Eye"
			CASE HASH("TAT_LX_009")				iCost = g_sMPTunables.iluxe1_both_genders_floral_symmetry				  BREAK // "Floral Symmetry"
			CASE HASH("TAT_LX_013")				iCost = g_sMPTunables.iluxe1_both_genders_mermaid_harpist				  BREAK // "Mermaid Harpist"
			CASE HASH("TAT_LX_014")				iCost = g_sMPTunables.iluxe1_both_genders_ancient_queen					  BREAK // "Ancient Queen"
			CASE HASH("TAT_LX_015")				iCost = g_sMPTunables.iluxe1_both_genders_smoking_sisters				  BREAK // "Smoking Sisters"
			CASE HASH("TAT_LX_019")				iCost = g_sMPTunables.iluxe1_both_genders_geisha_bloom					  BREAK // "Geisha Bloom"
			CASE HASH("TAT_LX_020")				iCost = g_sMPTunables.iluxe1_both_genders_archangel_and_mary			  BREAK // "Archangel & Mary"
			CASE HASH("TAT_LX_021")				iCost = g_sMPTunables.iluxe1_both_genders_gabriel_						  BREAK // "Gabriel"
			CASE HASH("TAT_LX_024")				iCost = g_sMPTunables.iluxe1_both_genders_feather_mural					  BREAK // "Feather Mural"
			CASE HASH("TAT_L2_002")				iCost = g_sMPTunables.iLuxe2_The_Howler									  BREAK // "The Howler"
			CASE HASH("TAT_L2_005")				iCost = g_sMPTunables.iLuxe2_FATAL_DAGGER								  BREAK // "Fatal Dagger"
			CASE HASH("TAT_L2_010")				iCost = g_sMPTunables.iLuxe2_INTROMETRIC								  BREAK // "Intrometric"
			CASE HASH("TAT_L2_011")				iCost = g_sMPTunables.iLuxe2_CROSS_OF_ROSES								  BREAK // "Cross of Roses"
			CASE HASH("TAT_L2_012")				iCost = g_sMPTunables.iLuxe2_GEOMETRIC_GALAXY							  BREAK // "Geometric Galaxy"
			CASE HASH("TAT_L2_016")				iCost = g_sMPTunables.iLuxe2_EGYPTIAN_MURAL								  BREAK // "Egyptian Mural"
			CASE HASH("TAT_L2_017")				iCost = g_sMPTunables.iLuxe2_HEAVENLY_DEITY            					  BREAK // "Heavenly Deity"
			CASE HASH("TAT_L2_018")				iCost = g_sMPTunables.iLuxe2_DIVINE_GODDESS								  BREAK // "Divine Goddess"
			CASE HASH("TAT_L2_022")				iCost = g_sMPTunables.iLuxe2_CLOAKED_ANGEL								  BREAK // "Cloaked Angel"
			CASE HASH("TAT_L2_023")				iCost = g_sMPTunables.iLuxe2_STARMETRIC									  BREAK // "Starmetric"
			CASE HASH("TAT_L2_025")				iCost = g_sMPTunables.iLuxe2_REAPER_SWAY								  BREAK // "Reaper Sway"
			CASE HASH("TAT_L2_026")				iCost = g_sMPTunables.iLuxe2_FLORAL_PRINT								  BREAK // "Floral Print"
			CASE HASH("TAT_L2_027")				iCost = g_sMPTunables.iLuxe2_COBRA_DAWN									  BREAK // "Cobra Dawn"
			CASE HASH("TAT_L2_028")				iCost = g_sMPTunables.iLuxe2_PYTHON_SKULL								  BREAK // "Python Skull"
			CASE HASH("TAT_L2_029")				iCost = g_sMPTunables.iLuxe2_GEOMETRIC_DESIGN							  BREAK // "Geometric Design"
			CASE HASH("TAT_L2_030")				iCost = g_sMPTunables.iLuxe2_GEOMETRIC_DESIGN							  BREAK // "Geometric Design"
			CASE HASH("TAT_L2_031")				iCost = g_sMPTunables.iLuxe2_GEOMETRIC_DESIGN							  BREAK // "Geometric Design"
			#ENDIF

			CASE HASH("TAT_X2_000")				iCost = g_sMPTunables.iTattoos_xmas14_Skull_Rider						  BREAK // "Skull Rider
			CASE HASH("TAT_X2_001")				iCost = g_sMPTunables.iTattoos_xmas14_Spider_Outline					  BREAK // "Spider Outline
			CASE HASH("TAT_X2_002")				iCost = g_sMPTunables.iTattoos_xmas14_Spider_Color						  BREAK // "Spider Color
			CASE HASH("TAT_X2_003")				iCost = g_sMPTunables.iTattoos_xmas14_Snake_Outline		 				  BREAK // "Snake Outline
			CASE HASH("TAT_X2_004")				iCost = g_sMPTunables.iTattoos_xmas14_Snake_Shaded  					  BREAK // "Snake Shaded
			CASE HASH("TAT_X2_005")				iCost = g_sMPTunables.iTattoos_xmas14_Carp_Outline  					  BREAK // "Carp Outline
			CASE HASH("TAT_X2_006")				iCost = g_sMPTunables.iTattoos_xmas14_Carp_Shaded						  BREAK // "Carp Shaded
			CASE HASH("TAT_X2_007")				iCost = g_sMPTunables.iTattoos_xmas14_Los_Muertos						  BREAK // "Los Muertos
			CASE HASH("TAT_X2_008")				iCost = g_sMPTunables.iTattoos_xmas14_Death_Before_Dishonor        		  BREAK // "Death Before Dishonor
			CASE HASH("TAT_X2_009")				iCost = g_sMPTunables.iTattoos_xmas14_Time_To_Die						  BREAK // "Time To Die
			CASE HASH("TAT_X2_010")				iCost = g_sMPTunables.iTattoos_xmas14_Electric_Snake					  BREAK // "Electric Snake
			CASE HASH("TAT_X2_011")				iCost = g_sMPTunables.iTattoos_xmas14_Roaring_Tiger	 					  BREAK // "Roaring Tiger
			CASE HASH("TAT_X2_012")				iCost = g_sMPTunables.iTattoos_xmas14_8_Ball_Skull  					  BREAK // "8 Ball Skull
			CASE HASH("TAT_X2_013")				iCost = g_sMPTunables.iTattoos_xmas14_Lizard    			 			  BREAK // "Lizard
			CASE HASH("TAT_X2_014")				iCost = g_sMPTunables.iTattoos_xmas14_Floral_Dagger 					  BREAK // "Floral Dagger
			CASE HASH("TAT_X2_015")				iCost = g_sMPTunables.iTattoos_xmas14_Japanese_Warrior  				  BREAK // "Japanese Warrior
			CASE HASH("TAT_X2_016")				iCost = g_sMPTunables.iTattoos_xmas14_Loose_Lips_Outline				  BREAK // "Loose Lips Outline
			CASE HASH("TAT_X2_017")				iCost = g_sMPTunables.iTattoos_xmas14_Loose_Lips_Color  				  BREAK // "Loose Lips Color
			CASE HASH("TAT_X2_018")				iCost = g_sMPTunables.iTattoos_xmas14_Royal_Dagger_Outline				  BREAK // "Royal Dagger Outline
			CASE HASH("TAT_X2_019")				iCost = g_sMPTunables.iTattoos_xmas14_Royal_Dagger_Color				  BREAK // "Royal Dagger Color
			CASE HASH("TAT_X2_020")				iCost = g_sMPTunables.iTattoos_xmas14_Times_Up_Outline  				  BREAK // "Time's Up Outline
			CASE HASH("TAT_X2_021")				iCost = g_sMPTunables.iTattoos_xmas14_Times_Up_Color					  BREAK // "Time's Up Color
			CASE HASH("TAT_X2_022")				iCost = g_sMPTunables.iTattoos_xmas14_Youre_Next_Outline				  BREAK // "You're Next Outline
			CASE HASH("TAT_X2_023")				iCost = g_sMPTunables.iTattoos_xmas14_Youre_Next_Color 		 			  BREAK // "You're Next Color
			CASE HASH("TAT_X2_024")				iCost = g_sMPTunables.iTattoos_xmas14_Snake_Head_Outline				  BREAK // "Snake Head Outline
			CASE HASH("TAT_X2_025")				iCost = g_sMPTunables.iTattoos_xmas14_Snake_Head_Color  				  BREAK // "Snake Head Color
			CASE HASH("TAT_X2_026")				iCost = g_sMPTunables.iTattoos_xmas14_Fuck_Luck_Outline 				  BREAK // "Fuck Luck Outline
			CASE HASH("TAT_X2_027")				iCost = g_sMPTunables.iTattoos_xmas14_Fuck_Luck_Color   				  BREAK // "Fuck Luck Color
			CASE HASH("TAT_X2_028")				iCost = g_sMPTunables.iTattoos_xmas14_Executioner						  BREAK // "Executioner
			CASE HASH("TAT_X2_029")				iCost = g_sMPTunables.iTattoos_xmas14_Beautiful_Death					  BREAK // "Beautiful Death

		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_S1_001") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_King_Fight) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)       BREAK// King Fight
			CASE HASH("TAT_S1_002") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Holy_Mary) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)        BREAK// Holy Mary
			CASE HASH("TAT_S1_004") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Gun_Mic) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)          BREAK// Gun Mic
			CASE HASH("TAT_S1_005") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_No_Evil) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)          BREAK// No Evil
			CASE HASH("TAT_S1_007") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_LS_Serpent) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)       BREAK// LS Serpent
			CASE HASH("TAT_S1_009") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Amazon) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)           BREAK// Amazon
			CASE HASH("TAT_S1_010") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Bad_Angel) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)        BREAK// Bad Angel
			CASE HASH("TAT_S1_013") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Love_Gamble) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)      BREAK// Love Gamble
			CASE HASH("TAT_S1_014") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Love_is_Blind) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)    BREAK// Love is Blind
			CASE HASH("TAT_S1_015") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Seductress) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)       BREAK// Seductress
			CASE HASH("TAT_S1_017") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Ink_Me) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)           BREAK// Ink Me
			CASE HASH("TAT_S1_020") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Presidents) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)       BREAK// Presidents
			CASE HASH("TAT_S1_021") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Sad_Angel) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)        BREAK// Sad Angel
			CASE HASH("TAT_S1_023") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Dance_of_Hearts) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)  BREAK// Dance of Hearts
			CASE HASH("TAT_S1_026") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Royal_Takeover) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)   BREAK// Royal Takeover
			CASE HASH("TAT_S1_027") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_Los_Santos_Life) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)  BREAK// Los Santos Life
			CASE HASH("TAT_S1_033") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBoth_Genders_City_Sorrow) * g_sMPTunables.fLowrider_Male_and_Female_Tattoos_All_Tattoos)      BREAK// City Sorrow
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_S2_000") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_SA_Assault) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// SA Assault
			CASE HASH("TAT_S2_003") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Lady_Vamp) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)        BREAK// Lady Vamp
			CASE HASH("TAT_S2_006") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Love_Hustle) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)          BREAK// Love Hustle
			CASE HASH("TAT_S2_008") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Love_the_Game) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)          BREAK// Love the Game
			CASE HASH("TAT_S2_011") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Lady_Liberty) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Lady Liberty
			CASE HASH("TAT_S2_012") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Royal_Kiss) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)           BREAK// Royal Kiss
			CASE HASH("TAT_S2_016") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Two_Face) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)        BREAK// Two Face
			CASE HASH("TAT_S2_018") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Skeleton_Party) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)      BREAK// Skeleton Party
			CASE HASH("TAT_S2_019") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Death_Behind) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)    BREAK// Death Behind
			CASE HASH("TAT_S2_022") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_My_Crazy_Life) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// My Crazy Life
			CASE HASH("TAT_S2_028") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Loving_Los_Muertos) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)           BREAK// Loving Los Muertos
			CASE HASH("TAT_S2_029") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Death_Us_Do_Part) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Death Us Do Part
			CASE HASH("TAT_S2_030") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_San_Andreas_Prayer) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)        BREAK// San Andreas Prayer
			CASE HASH("TAT_S2_031") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Dead_Pretty) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)  BREAK// Dead Pretty
			CASE HASH("TAT_S2_032") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Reign_Over) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)   BREAK// Reign Over
			CASE HASH("TAT_S2_035") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Black_Tears) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)  BREAK// Black Tears
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_ST_000") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Stunt_Skull) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Stunt Skull
			CASE HASH("TAT_ST_001") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_8_Eyed_Skull) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "8 Eyed Skull
			CASE HASH("TAT_ST_002") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Big_Cat) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Big Cat
			CASE HASH("TAT_ST_003") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Poison_Wrench) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Poison Wrench
			CASE HASH("TAT_ST_004") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Scorpion_) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Scorpion
			CASE HASH("TAT_ST_005") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Demon_Spark_Plug) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Demon Spark Plug
			CASE HASH("TAT_ST_006") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Toxic_Spider) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Toxic Spider
			CASE HASH("TAT_ST_007") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStuntDagger_Devil) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Dagger Devil
			CASE HASH("TAT_ST_008") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Moonlight_Ride) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Moonlight Ride
			CASE HASH("TAT_ST_009") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Arachnid_of_Death) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Arachnid of Death
			CASE HASH("TAT_ST_010") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Grave_Vulture) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Grave Vulture
			CASE HASH("TAT_ST_011") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Wheels_of_Death) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Wheels of Death
			CASE HASH("TAT_ST_012") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Punk_Biker_) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Punk Biker
			CASE HASH("TAT_ST_013") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Dirt_Track_Hero) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Dirt Track Hero
			CASE HASH("TAT_ST_014") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Bat_Cat_of_Spades) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Bat Cat of Spades
			CASE HASH("TAT_ST_015") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Praying_Gloves) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Praying Gloves
			CASE HASH("TAT_ST_016") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Coffin_Racer) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Coffin Racer
			CASE HASH("TAT_ST_017") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Bat_Wheel) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Bat Wheel
			CASE HASH("TAT_ST_018") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Vintage_Bully) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Vintage Bully
			CASE HASH("TAT_ST_019") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Engine_Heart) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Engine Heart
			CASE HASH("TAT_ST_020") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Piston_Angel) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Piston Angel
			CASE HASH("TAT_ST_021") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Golden_Cobra) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Golden Cobra
			CASE HASH("TAT_ST_022") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Piston_Head) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Piston Head
			CASE HASH("TAT_ST_023") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Tanked_) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Tanked
			CASE HASH("TAT_ST_024") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Road_Kill) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Road Kill
			CASE HASH("TAT_ST_025") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Speed_Freak) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Speed Freak
			CASE HASH("TAT_ST_026") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Winged_Wheel) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Winged Wheel
			CASE HASH("TAT_ST_027") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Punk_Road_Hog) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Punk Road Hog
			CASE HASH("TAT_ST_028") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Quad_Goblin) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Quad Goblin
			CASE HASH("TAT_ST_029") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Majestic_Finish) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Majestic Finish
			CASE HASH("TAT_ST_030") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Mans_Ruin_) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Man's Ruin
			CASE HASH("TAT_ST_031") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Stunt_Jesus) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Stunt Jesus
			CASE HASH("TAT_ST_032") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Wheelie_Mouse) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Wheelie Mouse
			CASE HASH("TAT_ST_033") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Sugar_Skull_Trucker) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Sugar Skull Trucker
			CASE HASH("TAT_ST_034") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Feather_Road_Kill) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Feather Road Kill
			CASE HASH("TAT_ST_035") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Stuntmans_End) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Stuntman's End
			CASE HASH("TAT_ST_036") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Biker_Stallion) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Biker Stallion
			CASE HASH("TAT_ST_037") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Big_Grills) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Big Grills
			CASE HASH("TAT_ST_038") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_One_Down_Five_Up) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "One Down Five Up
			CASE HASH("TAT_ST_039") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Kaboom) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Kaboom
			CASE HASH("TAT_ST_040") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Monkey_Chopper) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Monkey Chopper
			CASE HASH("TAT_ST_041") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Brapp) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Brapp
			CASE HASH("TAT_ST_042") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Flaming_Quad) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Flaming Quad
			CASE HASH("TAT_ST_043") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Engine_Arm) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Engine Arm
			CASE HASH("TAT_ST_044") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Ram_Skull) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Ram Skull
			CASE HASH("TAT_ST_045") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Severed_Hand) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Severed Hand
			CASE HASH("TAT_ST_046") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Full_Throtle) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Full Throttle
			CASE HASH("TAT_ST_047") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Brake_Knife) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Brake Knife
			CASE HASH("TAT_ST_048") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Racing_Doll) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Racing Doll
			CASE HASH("TAT_ST_049") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iStunt_Seductive_Mechanic) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Seductive Mechanic
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_BI_000") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Demon_Rider) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Demon Rider
			CASE HASH("TAT_BI_001") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Both_Barrels) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Both Barrels
			CASE HASH("TAT_BI_002") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Rose_Tribute) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Rose Tribute
			CASE HASH("TAT_BI_003") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Web_Rider) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Web Rider
			CASE HASH("TAT_BI_004") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Dragons_Fury) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Dragon's Fury
			CASE HASH("TAT_BI_005") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Made_In_America) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Made In America
			CASE HASH("TAT_BI_006") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Chopper_Freedom) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Chopper Freedom
			CASE HASH("TAT_BI_007") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Swooping_Eagle) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Raptor Pounce
			CASE HASH("TAT_BI_008") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Freedom_Wheels) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Freedom Wheels
			CASE HASH("TAT_BI_009") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Morbid_Arachnid) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Morbid Arachnid
			CASE HASH("TAT_BI_010") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Skull_of_Taurus) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Skull of Taurus
			CASE HASH("TAT_BI_011") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_RIP_My_Brothers) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "R.I.P. My Brothers
			CASE HASH("TAT_BI_012") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Urban_Stunter) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Urban Stunter
			CASE HASH("TAT_BI_013") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Demon_Crossbones) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Demon Crossbones
			CASE HASH("TAT_BI_014") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Lady_Mortality) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Lady Mortality
			CASE HASH("TAT_BI_015") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Ride_or_Die) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Ride or Die
			CASE HASH("TAT_BI_016") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Macabre_Tree) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Macabre Tree
			CASE HASH("TAT_BI_017") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Clawed_Beast) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Clawed Beast
			CASE HASH("TAT_BI_018") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Skeletal_Chopper) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Skeletal Chopper
			CASE HASH("TAT_BI_019") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Gruesome_Talons) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Gruesome Talons
			CASE HASH("TAT_BI_020") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Cranial_Rose) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Cranial Rose
			CASE HASH("TAT_BI_021") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Flaming_Reaper) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Flaming Reaper
			CASE HASH("TAT_BI_022") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Western_Insignia) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Western Insignia
			CASE HASH("TAT_BI_023") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Western_MC) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Western MC
			CASE HASH("TAT_BI_024") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iBiker_Both_Genders_Live_to_Ride) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Live to Ride
			CASE HASH("TAT_BI_025") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_GOOD_LUCK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Good Luck
			CASE HASH("TAT_BI_026") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_AMERICAN_DREAM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "American Dream
			CASE HASH("TAT_BI_027") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_BAD_LUCK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Bad Luck
			CASE HASH("TAT_BI_028") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_DUSK_RIDER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Dusk Rider
			CASE HASH("TAT_BI_029") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_BONE_WRENCH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Bone Wrench
			CASE HASH("TAT_BI_030") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_BROTHERS_FOR_LIFE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Brothers For Life
			CASE HASH("TAT_BI_031") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_GEAR_HEAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Gear Head
			CASE HASH("TAT_BI_032") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_WESTERN_EAGLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Western Eagle
			CASE HASH("TAT_BI_033") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_EAGLE_EMBLEM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Eagle Emblem
			CASE HASH("TAT_BI_034") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_BROTHERHOOD_OF_BIKES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Brotherhood of Bikes
			CASE HASH("TAT_BI_035") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_CHAIN_FIST) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Chain Fist
			CASE HASH("TAT_BI_036") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_SKULL_AND_SWORD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Engulfed Skull
			CASE HASH("TAT_BI_037") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_SCORCHED_SOUL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Scorched Soul
			CASE HASH("TAT_BI_038") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_FTW) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)								BREAK // "FTW
			CASE HASH("TAT_BI_039") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_GAS_GUZZLER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Gas Guzzler
			CASE HASH("TAT_BI_040") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_AMERICAN_MADE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "American Made
			CASE HASH("TAT_BI_041") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_NO_REGRETS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "No Regrets
			CASE HASH("TAT_BI_042") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_GRIM_RIDER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Grim Rider
			CASE HASH("TAT_BI_043") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_RIDE_FOREVER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Ride Forever
			CASE HASH("TAT_BI_044") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_RIDE_FREE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Ride Free
			CASE HASH("TAT_BI_045") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_RIDE_HARD_DIE_FAST) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Ride Hard Die Fast
			CASE HASH("TAT_BI_046") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_SKULL_CHAIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Skull Chain
			CASE HASH("TAT_BI_047") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_SNAKE_BIKE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Snake Bike
			CASE HASH("TAT_BI_048") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_STFU) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)							BREAK // "STFU
			CASE HASH("TAT_BI_049") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_THESE_COLORS_DONT_RUN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "These Colors Don't Run
			CASE HASH("TAT_BI_050") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_UNFORGIVEN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Unforgiven
			CASE HASH("TAT_BI_051") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iWESTERN_STYLIZED) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Western Stylized
			CASE HASH("TAT_BI_052") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_BIKER_MOUNT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Biker Mount
			CASE HASH("TAT_BI_053") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_MUFFLER_HELMET) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Muffler Helmet
			CASE HASH("TAT_BI_054") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_MUM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)								BREAK // "Mum
			CASE HASH("TAT_BI_055") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_POISON_SCORPION) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Poison Scorpion
			CASE HASH("TAT_BI_056") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_BONE_CRUISER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Bone Cruiser
			CASE HASH("TAT_BI_057") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_LAUGHING_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Laughing Skull
			CASE HASH("TAT_BI_058") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTATTOO_REAPER_VULTURE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Reaper Vulture
			CASE HASH("TAT_BI_059") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_Faggio) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)							BREAK // "Faggio
			CASE HASH("TAT_BI_060") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iTattoo_We_Are_The_Mods) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "We Are The Mods!
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_IE_000") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_BLOCK_BACK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Block Back
			CASE HASH("TAT_IE_001") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_POWER_PLANT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Power Plant
			CASE HASH("TAT_IE_002") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_TUNED_TO_DEATH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Tuned to Death
			CASE HASH("TAT_IE_003") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_MECHANICAL_SLEEVE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Mechanical Sleeve
			CASE HASH("TAT_IE_004") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_PISTON_SLEEVE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Piston Sleeve
			CASE HASH("TAT_IE_005") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_DIALLED_IN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Dialed In
			CASE HASH("TAT_IE_006") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_ENGULFED_BLOCK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Engulfed Block
			CASE HASH("TAT_IE_007") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_DRIVE_FOREVER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Drive Forever
			CASE HASH("TAT_IE_008") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_SCARLETT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Scarlett
			CASE HASH("TAT_IE_009") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_SERPENTS_OF_DESTRUCTION) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Serpents of Destruction
			CASE HASH("TAT_IE_010") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_TAKE_THE_WHEEL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Take the Wheel
			CASE HASH("TAT_IE_011") 			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIMPEXP_BOTH_GENDERS_TALK_SHIT_GET_HIT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Talk Shit Get Hit
			
			CASE HASH("TAT_GR_000")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_BULLET_PROOF) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Bullet Proof
			CASE HASH("TAT_GR_001")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_CROSSED_WEAPONS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Crossed Weapons
			CASE HASH("TAT_GR_002")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_GRENADE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Grenade
			CASE HASH("TAT_GR_003")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_LOCK_AND_LOAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Lock & Load
			CASE HASH("TAT_GR_004")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_SIDEARM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Sidearm
			CASE HASH("TAT_GR_005")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_PATRIOT_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Patriot Skull
			CASE HASH("TAT_GR_006")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_COMBAT_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Combat Skull
			CASE HASH("TAT_GR_007")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_STYLIZED_TIGER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Stylized Tiger
			CASE HASH("TAT_GR_008")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_BANDOLIER		) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Bandolier
			CASE HASH("TAT_GR_009")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_BUTTERFLY_KNIFE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Butterfly Knife
			CASE HASH("TAT_GR_010")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_CASH_MONEY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Cash Money
			CASE HASH("TAT_GR_011")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_DEATH_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Death Skull
			CASE HASH("TAT_GR_012")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_DOLLAR_DAGGERS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Dollar Daggers
			CASE HASH("TAT_GR_013")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_WOLF_INSIGNIA) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Wolf Insignia
			CASE HASH("TAT_GR_014")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_BACKSTABBER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Backstabber
			CASE HASH("TAT_GR_015")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_SPIKED_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Spiked Skull
			CASE HASH("TAT_GR_016")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_BLOOD_MONEY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Blood Money
			CASE HASH("TAT_GR_017")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_DOG_TAGS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Dog Tags
			CASE HASH("TAT_GR_018")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_DUAL_WIELD_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Dual Wield Skull
			CASE HASH("TAT_GR_019")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_PISTOL_WINGS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Pistol Wings
			CASE HASH("TAT_GR_020")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_CROWNED_WEAPONS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Crowned Weapons
			CASE HASH("TAT_GR_021")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_HAVE_A_NICE_DAY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Have a Nice Day
			CASE HASH("TAT_GR_022")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_EXPLOSIVE_HEART) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Explosive Heart
			CASE HASH("TAT_GR_023")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_ROSE_REVOLVER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Rose Revolver
			CASE HASH("TAT_GR_024")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_COMBAT_REAPER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Combat Reaper
			CASE HASH("TAT_GR_025")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_PRAYING_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Praying Skull
			CASE HASH("TAT_GR_026")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_RESTLESS_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Restless Skull
			CASE HASH("TAT_GR_027")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_SERPENT_REVOLVER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Serpent Revolver
			CASE HASH("TAT_GR_028")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_MICRO_SMG_CHAIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Micro SMG Chain
			CASE HASH("TAT_GR_029")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_WIN_SOME_LOSE_SOME) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Win Some Lose Some
			CASE HASH("TAT_GR_030")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iGR_BOTH_GENDERS_PISTOL_ACE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Pistol Ace
			
			CASE HASH("TAT_AR_000")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TURBULENCE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Turbulence
			CASE HASH("TAT_AR_001")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_PILOT_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Pilot Skull
			CASE HASH("TAT_AR_002")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_WINGED_BOMBSHELL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Winged Bombshell
			CASE HASH("TAT_AR_003")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TOXIC_TRAILS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Toxic Trails
			CASE HASH("TAT_AR_004")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_BALLOON_PIONEER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Balloon Pioneer
			CASE HASH("TAT_AR_005")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_PARACHUTE_BELLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Parachute Belle
			CASE HASH("TAT_AR_006")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_BOMBS_AWAY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Bombs Away
			CASE HASH("TAT_AR_007")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_EAGLE_EYES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Eagle Eyes
			
			CASE HASH("TAT_SM_000")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_BLESS_THE_DEAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Bless The Dead
			CASE HASH("TAT_SM_001")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_CRACKSHOT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Crackshot
			CASE HASH("TAT_SM_002")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_DEAD_LIES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Dead Lies
			CASE HASH("TAT_SM_003")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_GIVE_NOTHING_BACK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Give Nothing Back
			CASE HASH("TAT_SM_004")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_HONOR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Honor
			CASE HASH("TAT_SM_005")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_MUTINY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Mutiny
			CASE HASH("TAT_SM_006")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_NEVER_SURRENDER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Never Surrender
			CASE HASH("TAT_SM_007")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_NO_HONOR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "No Honor
			CASE HASH("TAT_SM_008")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_HORRORS_OF_THE_DEEP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Horrors Of The Deep
			CASE HASH("TAT_SM_009")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TALL_SHIP_CONFLICT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Tall Ship Conflict
			CASE HASH("TAT_SM_010")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_SEE_YOU_IN_HELL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "See You In Hell
			CASE HASH("TAT_SM_011")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_SINNER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Sinner
			CASE HASH("TAT_SM_012")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_THIEF) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Thief
			CASE HASH("TAT_SM_013")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TORN_WINGS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Torn Wings
			CASE HASH("TAT_SM_014")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_MERMAIDS_CURSE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Mermaid's Curse
			CASE HASH("TAT_SM_015")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_JOLLY_ROGER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Jolly Roger
			CASE HASH("TAT_SM_016")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_SKULL_COMPASS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Skull Compass
			CASE HASH("TAT_SM_017")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_FRAMED_TALL_SHIP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Framed Tall Ship
			CASE HASH("TAT_SM_018")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_FINDERS_KEEPERS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Finders Keepers
			CASE HASH("TAT_SM_019")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_LOST_AT_SEA) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Lost At Sea
			CASE HASH("TAT_SM_020")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_HOMEWARD_BOUND) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Homeward Bound
			CASE HASH("TAT_SM_021")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_DEAD_TALES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Dead Tales
			CASE HASH("TAT_SM_022")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_X_MARKS_THE_SPOT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "X Marks The Spot
			CASE HASH("TAT_SM_023")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_STYLIZED_KRAKEN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Stylized Kraken
			CASE HASH("TAT_SM_024")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_PIRATE_CAPTAIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // "Pirate Captain
			CASE HASH("TAT_SM_025")				iCost = ROUND(TO_FLOAT(g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_CLAIMED_BY_THE_BEAST) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // "Claimed By The Beast
			
			CASE HASH("TAT_H27_000")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_THOR_AND_GOBLIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Thor & Goblin
			CASE HASH("TAT_H27_001")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_VIKING_WARRIOR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Viking Warrior
			CASE HASH("TAT_H27_002")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_KABUTO) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Kabuto
			CASE HASH("TAT_H27_003")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_NATIVE_WARRIOR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Native Warrior
			CASE HASH("TAT_H27_004")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATS_TIG_AND_MASK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Tiger & Mask
			CASE HASH("TAT_H27_005")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_GHOST_DRAGON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Ghost Dragon
			CASE HASH("TAT_H27_006")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MEDUSA) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Medusa
			CASE HASH("TAT_H27_007")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_COMBAT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Spartan Combat
			CASE HASH("TAT_H27_008")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_WARRIOR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Spartan Warrior
			CASE HASH("TAT_H27_009")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_NORSE_RUNE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Norse Rune
			CASE HASH("TAT_H27_010")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_SHIELD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Spartan Shield
			CASE HASH("TAT_H27_011")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_WEATHERED_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Weathered Skull
			CASE HASH("TAT_H27_012")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_TIGER_HEADDRESS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Tiger Headdress
			CASE HASH("TAT_H27_013")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_KATANA) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // "Katana
			CASE HASH("TAT_H27_014")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_CELTIC_BAND) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Celtic Band
			CASE HASH("TAT_H27_015")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SAMURAI_COMBAT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Samurai Combat
			CASE HASH("TAT_H27_016")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_ODIN_AND_RAVEN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Odin & Raven
			CASE HASH("TAT_H27_017")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_FEATHER_SLEEVE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Feather Sleeve
			CASE HASH("TAT_H27_018")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MUSCLE_TEAR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Muscle Tear
			CASE HASH("TAT_H27_019")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_STRIKE_FORCE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Strike Force
			CASE HASH("TAT_H27_020")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MEDUSAS_GAZE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Medusa's Gaze
			CASE HASH("TAT_H27_021")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_AND_LION) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Spartan & Lion
			CASE HASH("TAT_H27_022")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_AND_HORSE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Spartan & Horse
			CASE HASH("TAT_H27_023")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SAMURAI_TALLSHIP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // "Samurai Tallship
			CASE HASH("TAT_H27_024")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_DRAGON_SLAYER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Dragon Slayer
			CASE HASH("TAT_H27_025")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_WINGED_SERPENT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Winged Serpent
			CASE HASH("TAT_H27_026")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Spartan Skull
			CASE HASH("TAT_H27_027")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MOLON_LABE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Molon Labe
			CASE HASH("TAT_H27_028")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_MURAL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // "Spartan Mural
			CASE HASH("TAT_H27_029")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_CERBERUS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // "Cerberus
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_VW_000") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_IN_THE_POCKET) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// In the Pocket
			CASE HASH("TAT_VW_001") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_JACKPOT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Jackpot
			CASE HASH("TAT_VW_002") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_SUITS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Suits
			CASE HASH("TAT_VW_003") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_ROYAL_FLUSH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Royal Flush
			CASE HASH("TAT_VW_004") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_LADY_LUCK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Lady Luck
			CASE HASH("TAT_VW_005") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_GET_LUCKY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Get Lucky
			CASE HASH("TAT_VW_006") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_WHEEL_OF_SUITS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Wheel of Suits
			CASE HASH("TAT_VW_007") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_777) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// 777
			CASE HASH("TAT_VW_008") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_SNAKE_EYES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Snake Eyes
			CASE HASH("TAT_VW_009") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_TILL_DEATH_DO_US_PART) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Till Death Do Us Part
			CASE HASH("TAT_VW_010") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_PHOTO_FINISH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Photo Finish
			CASE HASH("TAT_VW_011") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_LIFES_A_GAMBLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Life's a Gamble
			CASE HASH("TAT_VW_012") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_SKULL_OF_SUITS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Skull of Suits
			CASE HASH("TAT_VW_013") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_ONEARMED_BANDIT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// One-armed Bandit
			CASE HASH("TAT_VW_014") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_GAMBLERS_RUIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Gambler's Ruin
			CASE HASH("TAT_VW_015") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_THE_JOLLY_JOKER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// The Jolly Joker
			CASE HASH("TAT_VW_016") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_ROSE_ACES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Rose & Aces
			CASE HASH("TAT_VW_017") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_ROLL_THE_DICE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Roll the Dice
			CASE HASH("TAT_VW_018") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_THE_GAMBLERS_LIFE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// The Gambler's Life
			CASE HASH("TAT_VW_019") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_CANT_WIN_THEM_ALL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Can't Win Them All
			CASE HASH("TAT_VW_020") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_CASH_IS_KING) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Cash is King
			CASE HASH("TAT_VW_021") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_SHOW_YOUR_HAND) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Show Your Hand
			CASE HASH("TAT_VW_022") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_BLOOD_MONEY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Blood Money
			CASE HASH("TAT_VW_023") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_LUCKY_7S) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Lucky 7s
			CASE HASH("TAT_VW_024") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_CASH_MOUTH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Cash Mouth
			CASE HASH("TAT_VW_025") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_QUEEN_OF_ROSES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Queen of Roses
			CASE HASH("TAT_VW_026") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_BANKNOTE_ROSE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Banknote Rose
			CASE HASH("TAT_VW_027") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_8BALL_ROSE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// 8-Ball Rose
			CASE HASH("TAT_VW_028") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_SKULL_ACES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Skull & Aces
			CASE HASH("TAT_VW_029") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_THE_TABLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// The Table
			CASE HASH("TAT_VW_030") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_THE_ROYALS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// The Royals
			CASE HASH("TAT_VW_031") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_GAMBLING_ROYALTY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Gambling Royalty
			CASE HASH("TAT_VW_032") iCost = ROUND(TO_FLOAT(g_sMPTunables.iVC_TATTOO_PLAY_YOUR_ACE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)       BREAK// Play Your Ace
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_H3_000")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_FIVE_STARS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_001")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_ACE_OF_SPADES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_002")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_ANIMAL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_003")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_ASSAULT_RIFLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_004")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_BAND_AID) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_005")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_SPADES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_006")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_CROWNED) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_007")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_TWO_HORNS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_008")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_ICE_CREAM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_009")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_KNIFED) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_010")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_GREEN_LEAF) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_011")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LIPSTICK_KISS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_012")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_RAZOR_POP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_013")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LS_STAR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_014")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LS_WINGS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_015")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_ON_OFF) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_016")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_SLEEPY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_017")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_SPACE_MONKEY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_018")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_STITCHES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_019")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_TEDDY_BEAR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_020")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_UFO) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_021")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_WANTED) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_022")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_THOGS_SWORD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_023")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_BIGFOOT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_024")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_MOUNT_CHILIAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_025")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_DAVIS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_026")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_DIGNITY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_027")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_EPSILON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_028")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_BANANAS_GONE_BAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_029")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_FATAL_INCURSION) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_030")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_HOWITZER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_031")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_KIFFLOM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_032")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LOVE_FIST) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_033")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LS_CITY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_034")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LS_MONOGRAM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_035")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LS_PANIC) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_036")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LS_SHIELD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_037")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_LADYBUG) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_038")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_ROBOT_BUBBLEGUM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_039")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_SPACE_RANGERS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_040")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_TIGER_HEART) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_041")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_MIGHTY_THOG) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_042")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_HEARTS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_043")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_DIAMONDS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
			CASE HASH("TAT_H3_044")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iCH_TATTOO_CLUBS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK
		ENDSWITCH
		
		SWITCH iLabelHash
			CASE HASH("TAT_H4_000")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_HEADPHONE_SPLAT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Headphone Splat
			CASE HASH("TAT_H4_001")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_TROPICAL_DUDE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Tropical Dude
			CASE HASH("TAT_H4_002")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_JELLYFISH_SHADES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Jellyfish Shades
			CASE HASH("TAT_H4_003")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_LIGHTHOUSE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Lighthouse
			CASE HASH("TAT_H4_004")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SKELETON_BREEZE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Skeleton Breeze
			CASE HASH("TAT_H4_005")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_LSUR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // LSUR
			CASE HASH("TAT_H4_006")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_MUSIC_LOCKER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Music Locker
			CASE HASH("TAT_H4_007")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SKELETON_DJ) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Skeleton DJ
			CASE HASH("TAT_H4_008")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SMILEY_GLITCH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Smiley Glitch
			CASE HASH("TAT_H4_009")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SCRATCH_PANTHER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Scratch Panther
			CASE HASH("TAT_H4_010")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_TROPICAL_SERPENT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Tropical Serpent
			CASE HASH("TAT_H4_011")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SOULWAX) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Soulwax
			CASE HASH("TAT_H4_012")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_STILL_SLIPPIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Still Slippin'
			CASE HASH("TAT_H4_013")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_WILD_DANCERS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Wild Dancers
			CASE HASH("TAT_H4_014")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_PARADISE_NAP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Paradise Nap
			CASE HASH("TAT_H4_015")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_PARADISE_UKULELE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Paradise Ukulele 
			CASE HASH("TAT_H4_016")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_ROSE_PANTHER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Rose Panther
			CASE HASH("TAT_H4_017")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_TROPICAL_SORCERER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Tropical Sorcerer
			CASE HASH("TAT_H4_018")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_RECORD_HEAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Record Head
			CASE HASH("TAT_H4_019")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_RECORD_SHOT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Record Shot
			CASE HASH("TAT_H4_020")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SPEAKER_TOWER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Speaker Tower
			CASE HASH("TAT_H4_021")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SKULL_SURFER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Skull Surfer
			CASE HASH("TAT_H4_022")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_PARADISE_SIRENS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Paradise Sirens
			CASE HASH("TAT_H4_023")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_TECHNO_GLITCH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Techno Glitch
			CASE HASH("TAT_H4_024")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_PINEAPPLE_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Pineapple Skull
			CASE HASH("TAT_H4_025")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_GLOW_PRINCESS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Glow Princess
			CASE HASH("TAT_H4_026")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SHARK_WATER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Shark Water
			CASE HASH("TAT_H4_027")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SKULLPHONES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Skullphones
			CASE HASH("TAT_H4_028")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SKULL_WATERS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Skull Waters
			CASE HASH("TAT_H4_029")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_SOUNDWAVES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Soundwaves
			CASE HASH("TAT_H4_030")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_RADIO_TAPE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Radio Tape
			CASE HASH("TAT_H4_031")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_OCTOPUS_SHADES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Octopus Shades
			CASE HASH("TAT_H4_032")			iCost = ROUND(TO_FLOAT(g_sMPTunables.iIH_TATTOO_KULT_991_FM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // K.U.L.T. 99.1 FM
		ENDSWITCH
		
		#IF FEATURE_FIXER
		SWITCH iLabelHash
			CASE HASH("TAT_FX_000") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_HOOD_SKELETON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Hood Skeleton
			CASE HASH("TAT_FX_001") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_BRIGHT_DIAMOND) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Bright Diamond
			CASE HASH("TAT_FX_002") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_HUSTLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Hustle
			CASE HASH("TAT_FX_003") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_BANDANA_KNIFE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Bandana Knife
			CASE HASH("TAT_FX_004") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_HOOD_HEART) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Hood Heart
			CASE HASH("TAT_FX_005") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_PEACOCK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Peacock
			CASE HASH("TAT_FX_006") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_SKELETON_SHOT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Skeleton Shot
			CASE HASH("TAT_FX_007") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_BALLAS_4_LIFE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Ballas 4 Life
			CASE HASH("TAT_FX_008") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_LOS_SANTOS_TAG) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Los Santos Tag
			CASE HASH("TAT_FX_009") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_ASCENSION) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Ascension
			CASE HASH("TAT_FX_010") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_MUSIC_IS_THE_REMEDY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // Music Is The Remedy
			CASE HASH("TAT_FX_011") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_SERPENT_MIC) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Serpent Mic
			CASE HASH("TAT_FX_012") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_ZOMBIE_RHYMES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Zombie Rhymes
			CASE HASH("TAT_FX_013") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_BLESSED_BOOMBOX) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Blessed Boombox
			CASE HASH("TAT_FX_014") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_CHAMBERLAIN_HILLS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Chamberlain Hills
			CASE HASH("TAT_FX_015") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_SMOKING_BARRELS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Smoking Barrels
			CASE HASH("TAT_FX_016") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_ALL_FROM_THE_SAME_TREE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)	BREAK // All From The Same Tree
			CASE HASH("TAT_FX_017") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_KING_OF_THE_JUNGLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // King of the Jungle
			CASE HASH("TAT_FX_018") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_NIGHT_OWL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Night Owl
			CASE HASH("TAT_FX_019") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_WEED_KNUCKLES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Weed Knuckles
			CASE HASH("TAT_FX_020") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_DOG_FIST) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)					BREAK // Dog Fist
			CASE HASH("TAT_FX_021") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_GRAFFITI_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Graffiti Skull
			CASE HASH("TAT_FX_022") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_LS_SMOKING_CARTRIDGES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // LS Smoking Cartridges
			CASE HASH("TAT_FX_023") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_TRUST) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)						BREAK // Trust
			CASE HASH("TAT_FX_024") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_BEATBOX_SILHOUETTE	) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // Beatbox Silhouette
			CASE HASH("TAT_FX_025") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_DAVIS_FLAMES) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Davis Flames
			CASE HASH("TAT_FX_026") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_DOLLAR_GUNS_CROSSED) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)		BREAK // Dollar Guns Crossed
			CASE HASH("TAT_FX_027") 		iCost = ROUND(TO_FLOAT(g_sMPTunables.iFIXER_TATTOO_BLACK_WIDOW) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)				BREAK // Black Widow
		ENDSWITCH
		#ENDIF
		
		#IF FEATURE_DLC_1_2022
		SWITCH iLabelHash
			CASE HASH("TAT_SB_000")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_LIVE_FAST_MONO) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Live Fast Mono
			CASE HASH("TAT_SB_001")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_LIVE_FAST_COLOR) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Live Fast Color
			CASE HASH("TAT_SB_002")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_COBRA_BIKER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Cobra Biker
			CASE HASH("TAT_SB_003")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BULLET_MOUTH) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Bullet Mouth
			CASE HASH("TAT_SB_004")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SMOKING_BARREL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Smoking Barrel
			CASE HASH("TAT_SB_005")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_CONCEALED) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Concealed
			CASE HASH("TAT_SB_006")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_PAINTED_MICRO_SMG) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Painted Micro SMG
			CASE HASH("TAT_SB_007")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_WEAPON_KING) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Weapon King
			CASE HASH("TAT_SB_008")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BIGNESS_CHIMP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Bigness Chimp
			CASE HASH("TAT_SB_009")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_UPNATOMIZER_DESIGN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Up-n-Atomizer Design
			CASE HASH("TAT_SB_010")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_ROCKET_LAUNCHER_GIRL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Rocket Launcher Girl
			CASE HASH("TAT_SB_011")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_MINIGUN_GUY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Minigun Guy
			CASE HASH("TAT_SB_012")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SNAKE_REVOLVER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Snake Revolver
			CASE HASH("TAT_SB_013")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_WEAPON_SLEEVE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Weapon Sleeve
			CASE HASH("TAT_SB_014")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_MINIMAL_SMG) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Minimal SMG
			CASE HASH("TAT_SB_015")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_MINIMAL_ADVANCED_RIFLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Minimal Advanced Rifle
			CASE HASH("TAT_SB_016")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_MINIMAL_SNIPER_RIFLE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Minimal Sniper Rifle
			CASE HASH("TAT_SB_017")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SKULL_GRENADE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Skull Grenade
			CASE HASH("TAT_SB_018")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BRANCHED_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Branched Skull
			CASE HASH("TAT_SB_019")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SCYTHED_CORPSE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Scythed Corpse
			CASE HASH("TAT_SB_020")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SCYTHED_CORPSE_REAPER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Scythed Corpse & Reaper
			CASE HASH("TAT_SB_021")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_THIRD_EYE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Third Eye
			CASE HASH("TAT_SB_022")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_PIERCED_THIRD_EYE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Pierced Third Eye
			CASE HASH("TAT_SB_023")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_LIP_DRIP) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Lip Drip
			CASE HASH("TAT_SB_024")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SKIN_MASK) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Skin Mask
			CASE HASH("TAT_SB_025")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_WEBBED_SCYTHE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Webbed Scythe
			CASE HASH("TAT_SB_026")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_ONI_DEMON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Oni Demon
			CASE HASH("TAT_SB_027")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BAT_WINGS) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Bat Wings
			CASE HASH("TAT_SB_028")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_LASER_EYES_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Laser Eyes Skull
			CASE HASH("TAT_SB_029")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_CLASSIC_VAMPIRE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Classic Vampire
			CASE HASH("TAT_SB_030")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_CENTIPEDE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Centipede
			CASE HASH("TAT_SB_031")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_FLESHY_EYE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Fleshy Eye
			CASE HASH("TAT_SB_032")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_MANYEYED_GOAT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Many-eyed Goat
			CASE HASH("TAT_SB_033")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_THREEEYED_DEMON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Three-eyed Demon
			CASE HASH("TAT_SB_034")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SMOULDERING_REAPER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Smouldering Reaper
			CASE HASH("TAT_SB_035")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SNIFF_SNIFF) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Sniff Sniff
			CASE HASH("TAT_SB_036")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_CHARM_PATTERN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Charm Pattern
			CASE HASH("TAT_SB_037")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_WITCH_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Witch & Skull
			CASE HASH("TAT_SB_038")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_PUMPKIN_BUG) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Pumpkin Bug
			CASE HASH("TAT_SB_039")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SINNER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Sinner
			CASE HASH("TAT_SB_040")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_CARVED_PUMPKIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Carved Pumpkin
			CASE HASH("TAT_SB_041")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BRANCHED_WEREWOLF) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Branched Werewolf
			CASE HASH("TAT_SB_042")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_WINGED_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Winged Skull
			CASE HASH("TAT_SB_043")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_CURSED_SAKI) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Cursed Saki
			CASE HASH("TAT_SB_044")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SMOULDERING_BAT_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Smouldering Bat & Skull
			CASE HASH("TAT_SB_045")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_ARMORED_ARM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Armored Arm
			CASE HASH("TAT_SB_046")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_DEMON_SMILE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Demon Smile
			CASE HASH("TAT_SB_047")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_ANGEL_DEVIL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Angel & Devil
			CASE HASH("TAT_SB_048")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_DEATH_IS_CERTAIN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Death Is Certain
			CASE HASH("TAT_SB_049")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_DEMON_DRUMMER) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Demon Drummer
			CASE HASH("TAT_SB_050")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_GOLD_GUN) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Gold Gun
			CASE HASH("TAT_SB_051")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BLUE_SERPENT) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Blue Serpent
			CASE HASH("TAT_SB_052")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_NIGHT_DEMON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Night Demon
			CASE HASH("TAT_SB_053")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_MOBSTER_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Mobster Skull
			CASE HASH("TAT_SB_054")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_WOUNDED_HEAD) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Wounded Head
			CASE HASH("TAT_SB_055")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_STABBED_SKULL) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Stabbed Skull
			CASE HASH("TAT_SB_056")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_TIGER_BLADE) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Tiger Blade
			CASE HASH("TAT_SB_057")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_GRAY_DEMON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Gray Demon
			CASE HASH("TAT_SB_058")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SHRIEKING_DRAGON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Shrieking Dragon
			CASE HASH("TAT_SB_059")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_SWORDS_CITY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Swords & City
			CASE HASH("TAT_SB_060")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_BLAINE_COUNTY) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Blaine County
			CASE HASH("TAT_SB_061")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_ANGRY_POSSUM) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Angry Possum
			CASE HASH("TAT_SB_062")		iCost = ROUND(TO_FLOAT(g_sMPTunables.iSU22_TATTOO_FLORAL_DEMON) * g_sMPTunables.fMale_and_Female_Tattoos_All_Tattoos)			BREAK // Floral Demon
		ENDSWITCH
		#ENDIF
		
		#IF NOT IS_NEXTGEN_BUILD
		INT iLabelHash = GET_HASH_KEY(GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlLabel, 0, 6))
		IF (iLabelHash = HASH("TAT_LX"))
			iCost = ROUND((TO_FLOAT(iCost) * g_sMPtunables.fluxe1_all_multiplier_male_and_female_tattoos))
		ELIF (iLabelHash = HASH("TAT_L2"))
			iCost = ROUND((TO_FLOAT(iCost) * g_sMPtunables.fLuxe2_All_Tattoos))
		ENDIF
		#ENDIF
		
		RETURN FLOOR((TO_FLOAT(iCost) * g_sMPTunables.fTattooShopMultiplier))
	ELSE
		RETURN iCost
	ENDIF
ENDFUNC

PROC APPLY_TATTOO_DISCOUNT(INT &iCost, TEXT_LABEL_15 tlLabel)
	//apply tuneables discount
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iLabelHash = GET_HASH_KEY(tlLabel)
		SWITCH iLabelHash
			CASE HASH("TAT_BB_000") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Los_Santos_Wreath		BREAK //8500            	BREAK //"DLC_FEMALE_TATTOOS_LOS_SANTOS_WREATH"            
			CASE HASH("TAT_BB_001") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Hibiscus_Flower_Duo	BREAK //6900            	BREAK //"DLC_FEMALE_TATTOOS_HIBISCUS_FLOWER_DUO"          
			CASE HASH("TAT_BB_002") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Tribal_Flower    		BREAK //3500            	BREAK //"DLC_FEMALE_TATTOOS_TRIBAL_FLOWER"                
			CASE HASH("TAT_BB_003") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Rock_Solid       		BREAK //5500            	BREAK //"DLC_FEMALE_TATTOOS_ROCK_SOLID"                   
			CASE HASH("TAT_BB_004") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Catfish          		BREAK //5250            	BREAK //"DLC_FEMALE_TATTOOS_CATFISH"                      
			CASE HASH("TAT_BB_005") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Shrimp           		BREAK //2500            	BREAK //"DLC_FEMALE_TATTOOS_SHRIMP"                       
			CASE HASH("TAT_BB_006") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Love_Dagger      		BREAK //6850            	BREAK //"DLC_FEMALE_TATTOOS_LOVE_DAGGER"                  
			CASE HASH("TAT_BB_007") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_School_of_Fish   		BREAK //2950            	BREAK //"DLC_FEMALE_TATTOOS_SCHOOL_OF_FISH"               
			CASE HASH("TAT_BB_008") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Tribal_Butterfly 		BREAK //1700            	BREAK //"DLC_FEMALE_TATTOOS_TRIBAL_BUTTERFLY"             
			CASE HASH("TAT_BB_009") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Hibiscus_Flower  		BREAK //2650            	BREAK //"DLC_FEMALE_TATTOOS_HIBISCUS_FLOWER"              
			CASE HASH("TAT_BB_010") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Dolphin          		BREAK //1900            	BREAK //"DLC_FEMALE_TATTOOS_DOLPHIN"                      
			CASE HASH("TAT_BB_011") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Sea_Horses       		BREAK //5100            	BREAK //"DLC_FEMALE_TATTOOS_SEA_HORSES"                   
			CASE HASH("TAT_BB_012")			iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Anchor           		BREAK //2500            	BREAK //"DLC_FEMALE_TATTOOS_ANCHOR"                       
			CASE HASH("TAT_BB_013") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Anchor           		BREAK //2500            	BREAK //"DLC_FEMALE_TATTOOS_ANCHOR"                       
			CASE HASH("TAT_BB_014") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Swallow          		BREAK //2100            	BREAK //"DLC_FEMALE_TATTOOS_SWALLOW"                      
			CASE HASH("TAT_BB_015") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Tribal_Fish      		BREAK //3700            	BREAK //"DLC_FEMALE_TATTOOS_TRIBAL_FISH"                  
			CASE HASH("TAT_BB_016") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Parrot           		BREAK //5250            	BREAK //"DLC_FEMALE_TATTOOS_PARROT"                       
			CASE HASH("TAT_BUS_F_002") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_High_Roller      		BREAK //7000            	BREAK //"DLC_FEMALE_TATTOOS_HIGH_ROLLER"                  
			CASE HASH("TAT_BUS_F_000") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Respect          		BREAK //4200            	BREAK //"DLC_FEMALE_TATTOOS_RESPECT"                      
			CASE HASH("TAT_BUS_F_006") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Single           		BREAK //4850            	BREAK //"DLC_FEMALE_TATTOOS_SINGLE"                       
			CASE HASH("TAT_BUS_F_007") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_ValdeGrace_Logo  		BREAK //1900            	BREAK //"DLC_FEMALE_TATTOOS_VALDEGRACE_LOGO"              
			CASE HASH("TAT_BUS_F_008") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Money_Rose       		BREAK //2500            	BREAK //"DLC_FEMALE_TATTOOS_MONEY_ROSE"                   
			CASE HASH("TAT_BUS_F_010") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Diamond_Crown    		BREAK //4500            	BREAK //"DLC_FEMALE_TATTOOS_DIAMOND_CROWN"                
			CASE HASH("TAT_BUS_F_009") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Dollar_Sign      		BREAK //4900            	BREAK //"DLC_FEMALE_TATTOOS_DOLLAR_SIGN"                  
			CASE HASH("TAT_BUS_F_011") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Diamond_Jack     		BREAK //6800            	BREAK //"DLC_FEMALE_TATTOOS_DIAMOND_JACK"                 
			CASE HASH("TAT_BUS_F_003") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Makin_Money      		BREAK //7200            	BREAK //"DLC_FEMALE_TATTOOS_MAKIN_MONEY"                  
			CASE HASH("TAT_BUS_F_001") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Gold_Digger      		BREAK //2700            	BREAK //"DLC_FEMALE_TATTOOS_GOLD_DIGGER"                  
			CASE HASH("TAT_BUS_F_005") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Greed_is_Good    		BREAK //5500            	BREAK //"DLC_FEMALE_TATTOOS_GREED_IS_GOOD"                
			CASE HASH("TAT_BUS_F_004") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Love_Money       		BREAK //1600            	BREAK //"DLC_FEMALE_TATTOOS_LOVE_MONEY"                   
			CASE HASH("TAT_BUS_F_012") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Santo_Capra_Logo 		BREAK //1800            	BREAK //"DLC_FEMALE_TATTOOS_SANTO_CAPRA_LOGO"             
			CASE HASH("TAT_BUS_F_013") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Money_Bag        		BREAK //1500            	BREAK //"DLC_FEMALE_TATTOOS_MONEY_BAG"                    
			CASE HASH("TAT_BUS_F_014") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Crew_Emblem_Chest		BREAK //20000           	BREAK //"DLC_FEMALE_TATTOOS_CREW_EMBLEM_CHEST"            
			CASE HASH("TAT_BUS_F_015") 		iCost = g_sMPTunables.iDLC_FEMALE_TATTOOS_Crew_Emblem_Arm  		BREAK //19500           	BREAK //"DLC_FEMALE_TATTOOS_CREW_EMBLEM_ARM"              
			CASE HASH("TAT_BB_017") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Mermaid_LS          		BREAK //6600            	BREAK //"DLC_MALE_TATTOO_MERMAID_LS"                      
			CASE HASH("TAT_BB_018") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Ship_Arms           		BREAK //7250            	BREAK //"DLC_MALE_TATTOO_SHIP_ARMS"                       
			CASE HASH("TAT_BB_019") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Hammerhead   		BREAK //5800            	BREAK //"DLC_MALE_TATTOO_TRIBAL_HAMMERHEAD"               
			CASE HASH("TAT_BB_020") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Shark        		BREAK //5900            	BREAK //"DLC_MALE_TATTOO_TRIBAL_SHARK"                    
			CASE HASH("TAT_BB_021") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Pirate_Skull        		BREAK //12000           	BREAK //"DLC_MALE_TATTOO_PIRATE_SKULL"                    
			CASE HASH("TAT_BB_022") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Surf_LS             		BREAK //1450            	BREAK //"DLC_MALE_TATTOO_SURF_LS"                         
			CASE HASH("TAT_BB_023") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Swordfish           		BREAK //3100            	BREAK //"DLC_MALE_TATTOO_SWORDFISH"                       
			CASE HASH("TAT_BB_024") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tiki_Tower          		BREAK //4800            	BREAK //"DLC_MALE_TATTOO_TIKI_TOWER"                      
			CASE HASH("TAT_BB_025") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Tiki_Tower   		BREAK //6500            	BREAK //"DLC_MALE_TATTOO_TRIBAL_TIKI_TOWER"               
			CASE HASH("TAT_BB_026") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Sun          		BREAK //6200            	BREAK //"DLC_MALE_TATTOO_TRIBAL_SUN"                      
			CASE HASH("TAT_BB_027") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Tribal_Star         		BREAK //4450            	BREAK //"DLC_MALE_TATTOO_TRIBAL_STAR"                     
			CASE HASH("TAT_BB_028") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Little_Fish         		BREAK //1650            	BREAK //"DLC_MALE_TATTOO_LITTLE_FISH"                     
			CASE HASH("TAT_BB_029") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Surfs_Up            		BREAK //2250            	BREAK //"DLC_MALE_TATTOO_SURFS_UP"                        
			CASE HASH("TAT_BB_030") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Vespucci_Beauty     		BREAK //7000            	BREAK //"DLC_MALE_TATTOO_VESPUCCI_BEAUTY"                 
			CASE HASH("TAT_BB_031") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Shark               		BREAK //1850            	BREAK //"DLC_MALE_TATTOO_SHARK"                           
			CASE HASH("TAT_BB_032") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Wheel               		BREAK //5500            	BREAK //"DLC_MALE_TATTOO_WHEEL"                           
			CASE HASH("TAT_BUS_005") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Cash_King           		BREAK //5000            	BREAK //"DLC_MALE_TATTOO_CASH_KING"                       
			CASE HASH("TAT_BUS_003") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_100_Bill            		BREAK //3500            	BREAK //"DLC_MALE_TATTOO_100_BILL"                        
			CASE HASH("TAT_BUS_011") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Hustler             		BREAK //6400            	BREAK //"DLC_MALE_TATTOO_HUSTLER"                         
			CASE HASH("TAT_BUS_000") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Makin_Paper         		BREAK //5500            	BREAK //"DLC_MALE_TATTOO_MAKIN_PAPER"                     
			CASE HASH("TAT_BUS_006") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Bold_Dollar_Sign    		BREAK //1600            	BREAK //"DLC_MALE_TATTOO_BOLD_DOLLAR_SIGN"                
			CASE HASH("TAT_BUS_007") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Script_Dollar_Sign  		BREAK //1750            	BREAK //"DLC_MALE_TATTOO_SCRIPT_DOLLAR_SIGN"              
			CASE HASH("TAT_BUS_008") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_100                 		BREAK //6900            	BREAK //"DLC_MALE_TATTOO_100"                             
			CASE HASH("TAT_BUS_009") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Dollar_Skull        		BREAK //4800            	BREAK //"DLC_MALE_TATTOO_DOLLAR_SKULL"                    
			CASE HASH("TAT_BUS_001") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Rich                		BREAK //3250            	BREAK //"DLC_MALE_TATTOO_RICH"                            
			CASE HASH("TAT_BUS_002") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_DOLLAR_SIGNS        		BREAK //3500            	BREAK //"DLC_MALE_TATTOO_DOLLAR_SIGNS"                    
			CASE HASH("TAT_BUS_004") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_AllSeeing_Eye       		BREAK //7300            	BREAK //"DLC_MALE_TATTOO_ALLSEEING_EYE"                   
			CASE HASH("TAT_BUS_010") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Green               		BREAK //1500            	BREAK //"DLC_MALE_TATTOO_GREEN"                           
			CASE HASH("TAT_BUS_012") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Crew_Emblem_Chest   		BREAK //20000           	BREAK //"DLC_MALE_TATTOO_CREW_EMBLEM_CHEST"               
			CASE HASH("TAT_BUS_013") 		iCost = g_sMPTunables.iDLC_MALE_TATTOO_Crew_Emblem_Arm     		BREAK //19500                 // "DLC_MALE_TATTOO_CREW_EMBLEM_ARM"
			CASE HASH("TAT_X2_000") 		iCost = g_sMPTunables.iTattoos_xmas14_Skull_Rider				BREAK // "Skull Rider
			CASE HASH("TAT_X2_001") 		iCost = g_sMPTunables.iTattoos_xmas14_Spider_Outline			BREAK // "Spider Outline
			CASE HASH("TAT_X2_002") 		iCost = g_sMPTunables.iTattoos_xmas14_Spider_Color				BREAK // "Spider Color
			CASE HASH("TAT_X2_003") 		iCost = g_sMPTunables.iTattoos_xmas14_Snake_Outline		 		BREAK // "Snake Outline
			CASE HASH("TAT_X2_004") 		iCost = g_sMPTunables.iTattoos_xmas14_Snake_Shaded  			BREAK // "Snake Shaded
			CASE HASH("TAT_X2_005") 		iCost = g_sMPTunables.iTattoos_xmas14_Carp_Outline  			BREAK // "Carp Outline
			CASE HASH("TAT_X2_006") 		iCost = g_sMPTunables.iTattoos_xmas14_Carp_Shaded				BREAK // "Carp Shaded
			CASE HASH("TAT_X2_007") 		iCost = g_sMPTunables.iTattoos_xmas14_Los_Muertos				BREAK // "Los Muertos
			CASE HASH("TAT_X2_008") 		iCost = g_sMPTunables.iTattoos_xmas14_Death_Before_Dishonor		BREAK // "Death Before Dishonor
			CASE HASH("TAT_X2_009") 		iCost = g_sMPTunables.iTattoos_xmas14_Time_To_Die				BREAK // "Time To Die
			CASE HASH("TAT_X2_010") 		iCost = g_sMPTunables.iTattoos_xmas14_Electric_Snake			BREAK // "Electric Snake
			CASE HASH("TAT_X2_011") 		iCost = g_sMPTunables.iTattoos_xmas14_Roaring_Tiger	 			BREAK // "Roaring Tiger
			CASE HASH("TAT_X2_012") 		iCost = g_sMPTunables.iTattoos_xmas14_8_Ball_Skull  			BREAK // "8 Ball Skull
			CASE HASH("TAT_X2_013") 		iCost = g_sMPTunables.iTattoos_xmas14_Lizard    			 	BREAK // "Lizard
			CASE HASH("TAT_X2_014") 		iCost = g_sMPTunables.iTattoos_xmas14_Floral_Dagger 			BREAK // "Floral Dagger
			CASE HASH("TAT_X2_015") 		iCost = g_sMPTunables.iTattoos_xmas14_Japanese_Warrior  		BREAK // "Japanese Warrior
			CASE HASH("TAT_X2_016") 		iCost = g_sMPTunables.iTattoos_xmas14_Loose_Lips_Outline		BREAK // "Loose Lips Outline
			CASE HASH("TAT_X2_017") 		iCost = g_sMPTunables.iTattoos_xmas14_Loose_Lips_Color  		BREAK // "Loose Lips Color
			CASE HASH("TAT_X2_018") 		iCost = g_sMPTunables.iTattoos_xmas14_Royal_Dagger_Outline		BREAK // "Royal Dagger Outline
			CASE HASH("TAT_X2_019") 		iCost = g_sMPTunables.iTattoos_xmas14_Royal_Dagger_Color		BREAK // "Royal Dagger Color
			CASE HASH("TAT_X2_020") 		iCost = g_sMPTunables.iTattoos_xmas14_Times_Up_Outline  		BREAK // "Time's Up Outline
			CASE HASH("TAT_X2_021") 		iCost = g_sMPTunables.iTattoos_xmas14_Times_Up_Color			BREAK // "Time's Up Color
			CASE HASH("TAT_X2_022") 		iCost = g_sMPTunables.iTattoos_xmas14_Youre_Next_Outline		BREAK // "You're Next Outline
			CASE HASH("TAT_X2_023") 		iCost = g_sMPTunables.iTattoos_xmas14_Youre_Next_Color 		 	BREAK // "You're Next Color
			CASE HASH("TAT_X2_024") 		iCost = g_sMPTunables.iTattoos_xmas14_Snake_Head_Outline		BREAK // "Snake Head Outline
			CASE HASH("TAT_X2_025") 		iCost = g_sMPTunables.iTattoos_xmas14_Snake_Head_Color  		BREAK // "Snake Head Color
			CASE HASH("TAT_X2_026") 		iCost = g_sMPTunables.iTattoos_xmas14_Fuck_Luck_Outline 		BREAK // "Fuck Luck Outline
			CASE HASH("TAT_X2_027") 		iCost = g_sMPTunables.iTattoos_xmas14_Fuck_Luck_Color   		BREAK // "Fuck Luck Color
			CASE HASH("TAT_X2_028") 		iCost = g_sMPTunables.iTattoos_xmas14_Executioner				BREAK // "Executioner
			CASE HASH("TAT_X2_029") 		iCost = g_sMPTunables.iTattoos_xmas14_Beautiful_Death			BREAK // "Beautiful Death
		ENDSWITCH
		SWITCH iLabelHash
			CASE HASH("TAT_ST_000") 		iCost = g_sMPTunables.iStunt_Stunt_Skull						BREAK // "Stunt Skull
			CASE HASH("TAT_ST_001") 		iCost = g_sMPTunables.iStunt_8_Eyed_Skull  						BREAK // "8 Eyed Skull
			CASE HASH("TAT_ST_002") 		iCost = g_sMPTunables.iStunt_Big_Cat       						BREAK // "Big Cat
			CASE HASH("TAT_ST_003") 		iCost = g_sMPTunables.iStunt_Poison_Wrench 						BREAK // "Poison Wrench
			CASE HASH("TAT_ST_004") 		iCost = g_sMPTunables.iStunt_Scorpion_     						BREAK // "Scorpion
			CASE HASH("TAT_ST_005") 		iCost = g_sMPTunables.iStunt_Demon_Spark_Plug   				BREAK // "Demon Spark Plug
			CASE HASH("TAT_ST_006") 		iCost = g_sMPTunables.iStunt_Toxic_Spider  						BREAK // "Toxic Spider
			CASE HASH("TAT_ST_007") 		iCost = g_sMPTunables.iStuntDagger_Devil   						BREAK // "Dagger Devil
			CASE HASH("TAT_ST_008") 		iCost = g_sMPTunables.iStunt_Moonlight_Ride						BREAK // "Moonlight Ride
			CASE HASH("TAT_ST_009") 		iCost = g_sMPTunables.iStunt_Arachnid_of_Death  				BREAK // "Arachnid of Death
			CASE HASH("TAT_ST_010") 		iCost = g_sMPTunables.iStunt_Grave_Vulture 						BREAK // "Grave Vulture
			CASE HASH("TAT_ST_011") 		iCost = g_sMPTunables.iStunt_Wheels_of_Death					BREAK // "Wheels of Death
			CASE HASH("TAT_ST_012") 		iCost = g_sMPTunables.iStunt_Punk_Biker_   						BREAK // "Punk Biker
			CASE HASH("TAT_ST_013") 		iCost = g_sMPTunables.iStunt_Dirt_Track_Hero					BREAK // "Dirt Track Hero
			CASE HASH("TAT_ST_014") 		iCost = g_sMPTunables.iStunt_Bat_Cat_of_Spades  				BREAK // "Bat Cat of Spades
			CASE HASH("TAT_ST_015") 		iCost = g_sMPTunables.iStunt_Praying_Gloves						BREAK // "Praying Gloves
			CASE HASH("TAT_ST_016") 		iCost = g_sMPTunables.iStunt_Coffin_Racer  						BREAK // "Coffin Racer
			CASE HASH("TAT_ST_017") 		iCost = g_sMPTunables.iStunt_Bat_Wheel     						BREAK // "Bat Wheel
			CASE HASH("TAT_ST_018") 		iCost = g_sMPTunables.iStunt_Vintage_Bully 						BREAK // "Vintage Bully
			CASE HASH("TAT_ST_019") 		iCost = g_sMPTunables.iStunt_Engine_Heart  						BREAK // "Engine Heart
			CASE HASH("TAT_ST_020") 		iCost = g_sMPTunables.iStunt_Piston_Angel  						BREAK // "Piston Angel
			CASE HASH("TAT_ST_021") 		iCost = g_sMPTunables.iStunt_Golden_Cobra  						BREAK // "Golden Cobra
			CASE HASH("TAT_ST_022") 		iCost = g_sMPTunables.iStunt_Piston_Head   						BREAK // "Piston Head
			CASE HASH("TAT_ST_023") 		iCost = g_sMPTunables.iStunt_Tanked_       						BREAK // "Tanked
			CASE HASH("TAT_ST_024") 		iCost = g_sMPTunables.iStunt_Road_Kill     						BREAK // "Road Kill
			CASE HASH("TAT_ST_025") 		iCost = g_sMPTunables.iStunt_Speed_Freak   						BREAK // "Speed Freak
			CASE HASH("TAT_ST_026") 		iCost = g_sMPTunables.iStunt_Winged_Wheel  						BREAK // "Winged Wheel
			CASE HASH("TAT_ST_027") 		iCost = g_sMPTunables.iStunt_Punk_Road_Hog 						BREAK // "Punk Road Hog
			CASE HASH("TAT_ST_028") 		iCost = g_sMPTunables.iStunt_Quad_Goblin   						BREAK // "Quad Goblin
			CASE HASH("TAT_ST_029") 		iCost = g_sMPTunables.iStunt_Majestic_Finish					BREAK // "Majestic Finish
			CASE HASH("TAT_ST_030") 		iCost = g_sMPTunables.iStunt_Mans_Ruin_    						BREAK // "Man's Ruin
			CASE HASH("TAT_ST_031") 		iCost = g_sMPTunables.iStunt_Stunt_Jesus   						BREAK // "Stunt Jesus
			CASE HASH("TAT_ST_032") 		iCost = g_sMPTunables.iStunt_Wheelie_Mouse 						BREAK // "Wheelie Mouse
			CASE HASH("TAT_ST_033") 		iCost = g_sMPTunables.iStunt_Sugar_Skull_Trucker				BREAK // "Sugar Skull Trucker
			CASE HASH("TAT_ST_034") 		iCost = g_sMPTunables.iStunt_Feather_Road_Kill  				BREAK // "Feather Road Kill
			CASE HASH("TAT_ST_035") 		iCost = g_sMPTunables.iStunt_Stuntmans_End 						BREAK // "Stuntman's End
			CASE HASH("TAT_ST_036") 		iCost = g_sMPTunables.iStunt_Biker_Stallion						BREAK // "Biker Stallion
			CASE HASH("TAT_ST_037") 		iCost = g_sMPTunables.iStunt_Big_Grills    						BREAK // "Big Grills
			CASE HASH("TAT_ST_038") 		iCost = g_sMPTunables.iStunt_One_Down_Five_Up   				BREAK // "One Down Five Up
			CASE HASH("TAT_ST_039") 		iCost = g_sMPTunables.iStunt_Kaboom        						BREAK // "Kaboom
			CASE HASH("TAT_ST_040") 		iCost = g_sMPTunables.iStunt_Monkey_Chopper						BREAK // "Monkey Chopper
			CASE HASH("TAT_ST_041") 		iCost = g_sMPTunables.iStunt_Brapp         						BREAK // "Brapp
			CASE HASH("TAT_ST_042") 		iCost = g_sMPTunables.iStunt_Flaming_Quad  						BREAK // "Flaming Quad
			CASE HASH("TAT_ST_043") 		iCost = g_sMPTunables.iStunt_Engine_Arm    						BREAK // "Engine Arm
			CASE HASH("TAT_ST_044") 		iCost = g_sMPTunables.iStunt_Ram_Skull     						BREAK // "Ram Skull
			CASE HASH("TAT_ST_045") 		iCost = g_sMPTunables.iStunt_Severed_Hand  						BREAK // "Severed Hand
			CASE HASH("TAT_ST_046") 		iCost = g_sMPTunables.iStunt_Full_Throtle  						BREAK // "Full Throttle
			CASE HASH("TAT_ST_047") 		iCost = g_sMPTunables.iStunt_Brake_Knife   						BREAK // "Brake Knife
			CASE HASH("TAT_ST_048") 		iCost = g_sMPTunables.iStunt_Racing_Doll   						BREAK // "Racing Doll
			CASE HASH("TAT_ST_049") 		iCost = g_sMPTunables.iStunt_Seductive_Mechanic   				BREAK // "Seductive Mechanic
		ENDSWITCH
		SWITCH iLabelHash
			CASE HASH("TAT_BI_000") 		iCost = g_sMPTunables.iBiker_Both_Genders_Demon_Rider     	 	BREAK // "Demon Rider
			CASE HASH("TAT_BI_001") 		iCost = g_sMPTunables.iBiker_Both_Genders_Both_Barrels    	 	BREAK // "Both Barrels
			CASE HASH("TAT_BI_002") 		iCost = g_sMPTunables.iBiker_Both_Genders_Rose_Tribute    	 	BREAK // "Rose Tribute
			CASE HASH("TAT_BI_003") 		iCost = g_sMPTunables.iBiker_Both_Genders_Web_Rider       	 	BREAK // "Web Rider
			CASE HASH("TAT_BI_004") 		iCost = g_sMPTunables.iBiker_Both_Genders_Dragons_Fury    	 	BREAK // "Dragon's Fury
			CASE HASH("TAT_BI_005") 		iCost = g_sMPTunables.iBiker_Both_Genders_Made_In_America 	 	BREAK // "Made In America
			CASE HASH("TAT_BI_006") 		iCost = g_sMPTunables.iBiker_Both_Genders_Chopper_Freedom 	 	BREAK // "Chopper Freedom
			CASE HASH("TAT_BI_007") 		iCost = g_sMPTunables.iBiker_Both_Genders_Swooping_Eagle  	 	BREAK // "Raptor Pounce
			CASE HASH("TAT_BI_008") 		iCost = g_sMPTunables.iBiker_Both_Genders_Freedom_Wheels  	 	BREAK // "Freedom Wheels
			CASE HASH("TAT_BI_009") 		iCost = g_sMPTunables.iBiker_Both_Genders_Morbid_Arachnid 	 	BREAK // "Morbid Arachnid
			CASE HASH("TAT_BI_010") 		iCost = g_sMPTunables.iBiker_Both_Genders_Skull_of_Taurus 	 	BREAK // "Skull of Taurus
			CASE HASH("TAT_BI_011") 		iCost = g_sMPTunables.iBiker_Both_Genders_RIP_My_Brothers 	 	BREAK // "R.I.P. My Brothers
			CASE HASH("TAT_BI_012") 		iCost = g_sMPTunables.iBiker_Both_Genders_Urban_Stunter   	 	BREAK // "Urban Stunter
			CASE HASH("TAT_BI_013") 		iCost = g_sMPTunables.iBiker_Both_Genders_Demon_Crossbones	 	BREAK // "Demon Crossbones
			CASE HASH("TAT_BI_014") 		iCost = g_sMPTunables.iBiker_Both_Genders_Lady_Mortality  	 	BREAK // "Lady Mortality
			CASE HASH("TAT_BI_015") 		iCost = g_sMPTunables.iBiker_Both_Genders_Ride_or_Die     	 	BREAK // "Ride or Die
			CASE HASH("TAT_BI_016") 		iCost = g_sMPTunables.iBiker_Both_Genders_Macabre_Tree    	 	BREAK // "Macabre Tree
			CASE HASH("TAT_BI_017") 		iCost = g_sMPTunables.iBiker_Both_Genders_Clawed_Beast    	 	BREAK // "Clawed Beast
			CASE HASH("TAT_BI_018") 		iCost = g_sMPTunables.iBiker_Both_Genders_Skeletal_Chopper	 	BREAK // "Skeletal Chopper
			CASE HASH("TAT_BI_019") 		iCost = g_sMPTunables.iBiker_Both_Genders_Gruesome_Talons 	 	BREAK // "Gruesome Talons
			CASE HASH("TAT_BI_020") 		iCost = g_sMPTunables.iBiker_Both_Genders_Cranial_Rose    	 	BREAK // "Cranial Rose
			CASE HASH("TAT_BI_021") 		iCost = g_sMPTunables.iBiker_Both_Genders_Flaming_Reaper  	 	BREAK // "Flaming Reaper
			CASE HASH("TAT_BI_022") 		iCost = g_sMPTunables.iBiker_Both_Genders_Western_Insignia	 	BREAK // "Western Insignia
			CASE HASH("TAT_BI_023") 		iCost = g_sMPTunables.iBiker_Both_Genders_Western_MC      	 	BREAK // "Western MC
			CASE HASH("TAT_BI_024") 		iCost = g_sMPTunables.iBiker_Both_Genders_Live_to_Ride    	 	BREAK // "Live to Ride
			CASE HASH("TAT_BI_025") 		iCost = g_sMPTunables.iTATTOO_GOOD_LUCK							BREAK // "Good Luck
			CASE HASH("TAT_BI_026") 		iCost = g_sMPTunables.iTATTOO_AMERICAN_DREAM					BREAK // "American Dream
			CASE HASH("TAT_BI_027") 		iCost = g_sMPTunables.iTATTOO_BAD_LUCK							BREAK // "Bad Luck
			CASE HASH("TAT_BI_028") 		iCost = g_sMPTunables.iTATTOO_DUSK_RIDER						BREAK // "Dusk Rider
			CASE HASH("TAT_BI_029") 		iCost = g_sMPTunables.iTATTOO_BONE_WRENCH						BREAK // "Bone Wrench
			CASE HASH("TAT_BI_030") 		iCost = g_sMPTunables.iTATTOO_BROTHERS_FOR_LIFE					BREAK // "Brothers For Life
			CASE HASH("TAT_BI_031") 		iCost = g_sMPTunables.iTATTOO_GEAR_HEAD							BREAK // "Gear Head
			CASE HASH("TAT_BI_032") 		iCost = g_sMPTunables.iTATTOO_WESTERN_EAGLE						BREAK // "Western Eagle
			CASE HASH("TAT_BI_033") 		iCost = g_sMPTunables.iTATTOO_EAGLE_EMBLEM						BREAK // "Eagle Emblem
			CASE HASH("TAT_BI_034") 		iCost = g_sMPTunables.iTATTOO_BROTHERHOOD_OF_BIKES				BREAK // "Brotherhood of Bikes
			CASE HASH("TAT_BI_035") 		iCost = g_sMPTunables.iTATTOO_CHAIN_FIST						BREAK // "Chain Fist
			CASE HASH("TAT_BI_036") 		iCost = g_sMPTunables.iTATTOO_SKULL_AND_SWORD					BREAK // "Engulfed Skull
			CASE HASH("TAT_BI_037") 		iCost = g_sMPTunables.iTATTOO_SCORCHED_SOUL						BREAK // "Scorched Soul
			CASE HASH("TAT_BI_038") 		iCost = g_sMPTunables.iTATTOO_FTW								BREAK // "FTW
			CASE HASH("TAT_BI_039") 		iCost = g_sMPTunables.iTATTOO_GAS_GUZZLER						BREAK // "Gas Guzzler
			CASE HASH("TAT_BI_040") 		iCost = g_sMPTunables.iTATTOO_AMERICAN_MADE						BREAK // "American Made
			CASE HASH("TAT_BI_041") 		iCost = g_sMPTunables.iTATTOO_NO_REGRETS						BREAK // "No Regrets
			CASE HASH("TAT_BI_042") 		iCost = g_sMPTunables.iTATTOO_GRIM_RIDER						BREAK // "Grim Rider
			CASE HASH("TAT_BI_043") 		iCost = g_sMPTunables.iTATTOO_RIDE_FOREVER						BREAK // "Ride Forever
			CASE HASH("TAT_BI_044") 		iCost = g_sMPTunables.iTATTOO_RIDE_FREE							BREAK // "Ride Free
			CASE HASH("TAT_BI_045") 		iCost = g_sMPTunables.iTATTOO_RIDE_HARD_DIE_FAST				BREAK // "Ride Hard Die Fast
			CASE HASH("TAT_BI_046") 		iCost = g_sMPTunables.iTATTOO_SKULL_CHAIN						BREAK // "Skull Chain
			CASE HASH("TAT_BI_047") 		iCost = g_sMPTunables.iTATTOO_SNAKE_BIKE						BREAK // "Snake Bike
			CASE HASH("TAT_BI_048") 		iCost = g_sMPTunables.iTATTOO_STFU								BREAK // "STFU
			CASE HASH("TAT_BI_049") 		iCost = g_sMPTunables.iTATTOO_THESE_COLORS_DONT_RUN				BREAK // "These Colors Don't Run
			CASE HASH("TAT_BI_050") 		iCost = g_sMPTunables.iTATTOO_UNFORGIVEN						BREAK // "Unforgiven
			CASE HASH("TAT_BI_051") 		iCost = g_sMPTunables.iWESTERN_STYLIZED							BREAK // "Western Stylized
			CASE HASH("TAT_BI_052") 		iCost = g_sMPTunables.iTATTOO_BIKER_MOUNT						BREAK // "Biker Mount
			CASE HASH("TAT_BI_053") 		iCost = g_sMPTunables.iTATTOO_MUFFLER_HELMET					BREAK // "Muffler Helmet
			CASE HASH("TAT_BI_054") 		iCost = g_sMPTunables.iTATTOO_MUM								BREAK // "Mum
			CASE HASH("TAT_BI_055") 		iCost = g_sMPTunables.iTATTOO_POISON_SCORPION					BREAK // "Poison Scorpion
			CASE HASH("TAT_BI_056") 		iCost = g_sMPTunables.iTATTOO_BONE_CRUISER						BREAK // "Bone Cruiser
			CASE HASH("TAT_BI_057") 		iCost = g_sMPTunables.iTATTOO_LAUGHING_SKULL					BREAK // "Laughing Skull
			CASE HASH("TAT_BI_058") 		iCost = g_sMPTunables.iTATTOO_REAPER_VULTURE					BREAK // "Reaper Vulture
			CASE HASH("TAT_BI_059") 		iCost = g_sMPTunables.iTattoo_Faggio							BREAK // "Faggio
			CASE HASH("TAT_BI_060") 		iCost = g_sMPTunables.iTattoo_We_Are_The_Mods					BREAK // "We Are The Mods!
		ENDSWITCH
		SWITCH iLabelHash
			CASE HASH("TAT_IE_000") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_BLOCK_BACK				BREAK // "Block Back
			CASE HASH("TAT_IE_001") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_POWER_PLANT              BREAK // "Power Plant
			CASE HASH("TAT_IE_002") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_TUNED_TO_DEATH           BREAK // "Tuned to Death
			CASE HASH("TAT_IE_003") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_MECHANICAL_SLEEVE        BREAK // "Mechanical Sleeve
			CASE HASH("TAT_IE_004") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_PISTON_SLEEVE            BREAK // "Piston Sleeve
			CASE HASH("TAT_IE_005") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_DIALLED_IN               BREAK // "Dialed In
			CASE HASH("TAT_IE_006") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_ENGULFED_BLOCK           BREAK // "Engulfed Block
			CASE HASH("TAT_IE_007") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_DRIVE_FOREVER            BREAK // "Drive Forever
			CASE HASH("TAT_IE_008") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_SCARLETT                 BREAK // "Scarlett
			CASE HASH("TAT_IE_009") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_SERPENTS_OF_DESTRUCTION	BREAK // "Serpents of Destruction
			CASE HASH("TAT_IE_010") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_TAKE_THE_WHEEL           BREAK // "Take the Wheel
			CASE HASH("TAT_IE_011") 		iCost = g_sMPTunables.iIMPEXP_BOTH_GENDERS_TALK_SHIT_GET_HIT        BREAK // "Talk Shit Get Hit
			
			CASE HASH("TAT_GR_000")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_BULLET_PROOF					BREAK // "Bullet Proof
			CASE HASH("TAT_GR_001")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_CROSSED_WEAPONS				BREAK // "Crossed Weapons
			CASE HASH("TAT_GR_002")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_GRENADE						BREAK // "Grenade
			CASE HASH("TAT_GR_003")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_LOCK_AND_LOAD				BREAK // "Lock & Load
			CASE HASH("TAT_GR_004")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_SIDEARM						BREAK // "Sidearm
			CASE HASH("TAT_GR_005")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_PATRIOT_SKULL				BREAK // "Patriot Skull
			CASE HASH("TAT_GR_006")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_COMBAT_SKULL					BREAK // "Combat Skull
			CASE HASH("TAT_GR_007")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_STYLIZED_TIGER				BREAK // "Stylized Tiger
			CASE HASH("TAT_GR_008")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_BANDOLIER					BREAK // "Bandolier
			CASE HASH("TAT_GR_009")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_BUTTERFLY_KNIFE    			BREAK // "Butterfly Knife
			CASE HASH("TAT_GR_010")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_CASH_MONEY         			BREAK // "Cash Money
			CASE HASH("TAT_GR_011")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_DEATH_SKULL        			BREAK // "Death Skull
			CASE HASH("TAT_GR_012")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_DOLLAR_DAGGERS     			BREAK // "Dollar Daggers
			CASE HASH("TAT_GR_013")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_WOLF_INSIGNIA      			BREAK // "Wolf Insignia
			CASE HASH("TAT_GR_014")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_BACKSTABBER        			BREAK // "Backstabber
			CASE HASH("TAT_GR_015")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_SPIKED_SKULL       			BREAK // "Spiked Skull
			CASE HASH("TAT_GR_016")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_BLOOD_MONEY        			BREAK // "Blood Money
			CASE HASH("TAT_GR_017")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_DOG_TAGS           			BREAK // "Dog Tags
			CASE HASH("TAT_GR_018")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_DUAL_WIELD_SKULL   			BREAK // "Dual Wield Skull
			CASE HASH("TAT_GR_019")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_PISTOL_WINGS       			BREAK // "Pistol Wings
			CASE HASH("TAT_GR_020")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_CROWNED_WEAPONS    			BREAK // "Crowned Weapons
			CASE HASH("TAT_GR_021")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_HAVE_A_NICE_DAY    			BREAK // "Have a Nice Day
			CASE HASH("TAT_GR_022")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_EXPLOSIVE_HEART    			BREAK // "Explosive Heart
			CASE HASH("TAT_GR_023")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_ROSE_REVOLVER      			BREAK // "Rose Revolver
			CASE HASH("TAT_GR_024")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_COMBAT_REAPER      			BREAK // "Combat Reaper
			CASE HASH("TAT_GR_025")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_PRAYING_SKULL      			BREAK // "Praying Skull
			CASE HASH("TAT_GR_026")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_RESTLESS_SKULL     			BREAK // "Restless Skull
			CASE HASH("TAT_GR_027")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_SERPENT_REVOLVER   			BREAK // "Serpent Revolver
			CASE HASH("TAT_GR_028")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_MICRO_SMG_CHAIN    			BREAK // "Micro SMG Chain
			CASE HASH("TAT_GR_029")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_WIN_SOME_LOSE_SOME 			BREAK // "Win Some Lose Some
			CASE HASH("TAT_GR_030")			iCost = g_sMPTunables.iGR_BOTH_GENDERS_PISTOL_ACE         			BREAK // "Pistol Ace
			
			CASE HASH("TAT_AR_000")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TURBULENCE         		BREAK // "Turbulence
			CASE HASH("TAT_AR_001")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_PILOT_SKULL         	BREAK // "Pilot Skull
			CASE HASH("TAT_AR_002")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_WINGED_BOMBSHELL        BREAK // "Winged Bombshell
			CASE HASH("TAT_AR_003")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TOXIC_TRAILS         	BREAK // "Toxic Trails
			CASE HASH("TAT_AR_004")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_BALLOON_PIONEER         BREAK // "Balloon Pioneer
			CASE HASH("TAT_AR_005")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_PARACHUTE_BELLE         BREAK // "Parachute Belle
			CASE HASH("TAT_AR_006")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_BOMBS_AWAY         		BREAK // "Bombs Away
			CASE HASH("TAT_AR_007")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_EAGLE_EYES         		BREAK // "Eagle Eyes
			
			CASE HASH("TAT_SM_000")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_BLESS_THE_DEAD         	BREAK // "Bless The Dead
			CASE HASH("TAT_SM_001")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_CRACKSHOT         		BREAK // "Crackshot
			CASE HASH("TAT_SM_002")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_DEAD_LIES         		BREAK // "Dead Lies
			CASE HASH("TAT_SM_003")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_GIVE_NOTHING_BACK       BREAK // "Give Nothing Back
			CASE HASH("TAT_SM_004")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_HONOR         			BREAK // "Honor
			CASE HASH("TAT_SM_005")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_MUTINY         			BREAK // "Mutiny
			CASE HASH("TAT_SM_006")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_NEVER_SURRENDER         BREAK // "Never Surrender
			CASE HASH("TAT_SM_007")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_NO_HONOR         		BREAK // "No Honor
			CASE HASH("TAT_SM_008")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_HORRORS_OF_THE_DEEP     BREAK // "Horrors Of The Deep
			CASE HASH("TAT_SM_009")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TALL_SHIP_CONFLICT      BREAK // "Tall Ship Conflict
			CASE HASH("TAT_SM_010")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_SEE_YOU_IN_HELL         BREAK // "See You In Hell
			CASE HASH("TAT_SM_011")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_SINNER         			BREAK // "Sinner
			CASE HASH("TAT_SM_012")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_THIEF         			BREAK // "Thief
			CASE HASH("TAT_SM_013")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_TORN_WINGS         		BREAK // "Torn Wings
			CASE HASH("TAT_SM_014")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_MERMAIDS_CURSE         	BREAK // "Mermaid's Curse
			CASE HASH("TAT_SM_015")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_JOLLY_ROGER         	BREAK // "Jolly Roger
			CASE HASH("TAT_SM_016")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_SKULL_COMPASS         	BREAK // "Skull Compass
			CASE HASH("TAT_SM_017")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_FRAMED_TALL_SHIP        BREAK // "Framed Tall Ship
			CASE HASH("TAT_SM_018")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_FINDERS_KEEPERS         BREAK // "Finders Keepers
			CASE HASH("TAT_SM_019")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_LOST_AT_SEA         	BREAK // "Lost At Sea
			CASE HASH("TAT_SM_020")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_HOMEWARD_BOUND         	BREAK // "Homeward Bound
			CASE HASH("TAT_SM_021")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_DEAD_TALES         		BREAK // "Dead Tales
			CASE HASH("TAT_SM_022")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_X_MARKS_THE_SPOT        BREAK // "X Marks The Spot
			CASE HASH("TAT_SM_023")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_STYLIZED_KRAKEN         BREAK // "Stylized Kraken
			CASE HASH("TAT_SM_024")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_PIRATE_CAPTAIN         	BREAK // "Pirate Captain
			CASE HASH("TAT_SM_025")			iCost = g_sMPTunables.iSMUGGLER_TATTOOS_MALE_AND_FEMALE_CLAIMED_BY_THE_BEAST	BREAK // "Claimed By The Beast
			
			CASE HASH("TAT_H27_000") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_THOR_AND_GOBLIN  				BREAK // "Thor & Goblin
			CASE HASH("TAT_H27_001") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_VIKING_WARRIOR   				BREAK // "Viking Warrior
			CASE HASH("TAT_H27_002") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_KABUTO           				BREAK // "Kabuto
			CASE HASH("TAT_H27_003") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_NATIVE_WARRIOR   				BREAK // "Native Warrior
			CASE HASH("TAT_H27_004") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATS_TIG_AND_MASK   				BREAK // "Tiger & Mask
			CASE HASH("TAT_H27_005") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_GHOST_DRAGON     				BREAK // "Ghost Dragon
			CASE HASH("TAT_H27_006") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MEDUSA           				BREAK // "Medusa
			CASE HASH("TAT_H27_007") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_COMBAT   				BREAK // "Spartan Combat
			CASE HASH("TAT_H27_008") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_WARRIOR  				BREAK // "Spartan Warrior
			CASE HASH("TAT_H27_009") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_NORSE_RUNE       				BREAK // "Norse Rune
			CASE HASH("TAT_H27_010") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_SHIELD   				BREAK // "Spartan Shield
			CASE HASH("TAT_H27_011") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_WEATHERED_SKULL  				BREAK // "Weathered Skull
			CASE HASH("TAT_H27_012") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_TIGER_HEADDRESS  				BREAK // "Tiger Headdress
			CASE HASH("TAT_H27_013") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_KATANA           				BREAK // "Katana
			CASE HASH("TAT_H27_014") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_CELTIC_BAND      				BREAK // "Celtic Band
			CASE HASH("TAT_H27_015") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SAMURAI_COMBAT   				BREAK // "Samurai Combat
			CASE HASH("TAT_H27_016") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_ODIN_AND_RAVEN   				BREAK // "Odin & Raven
			CASE HASH("TAT_H27_017") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_FEATHER_SLEEVE   				BREAK // "Feather Sleeve
			CASE HASH("TAT_H27_018") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MUSCLE_TEAR      				BREAK // "Muscle Tear
			CASE HASH("TAT_H27_019") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_STRIKE_FORCE     				BREAK // "Strike Force
			CASE HASH("TAT_H27_020") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MEDUSAS_GAZE     				BREAK // "Medusa's Gaze
			CASE HASH("TAT_H27_021") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_AND_LION 				BREAK // "Spartan & Lion
			CASE HASH("TAT_H27_022") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_AND_HORSE				BREAK // "Spartan & Horse
			CASE HASH("TAT_H27_023") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SAMURAI_TALLSHIP 				BREAK // "Samurai Tallship
			CASE HASH("TAT_H27_024") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_DRAGON_SLAYER    				BREAK // "Dragon Slayer
			CASE HASH("TAT_H27_025") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_WINGED_SERPENT   				BREAK // "Winged Serpent
			CASE HASH("TAT_H27_026") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_SKULL    				BREAK // "Spartan Skull
			CASE HASH("TAT_H27_027") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_MOLON_LABE       				BREAK // "Molon Labe
			CASE HASH("TAT_H27_028") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_SPARTAN_MURAL    				BREAK // "Spartan Mural
			CASE HASH("TAT_H27_029") 		iCost = g_sMPTunables.iH2_MALE_AND_FEMALE_TATTOOS_CERBERUS         				BREAK // "Cerberus
		
			CASE HASH("TAT_VW_000")			iCost = g_sMPTunables.iVC_TATTOO_IN_THE_POCKET         							BREAK // "In the Pocket
			CASE HASH("TAT_VW_001")			iCost = g_sMPTunables.iVC_TATTOO_JACKPOT         								BREAK // "Jackpot
			CASE HASH("TAT_VW_002")			iCost = g_sMPTunables.iVC_TATTOO_SUITS         									BREAK // "Suits
			CASE HASH("TAT_VW_003")			iCost = g_sMPTunables.iVC_TATTOO_ROYAL_FLUSH         							BREAK // "Royal Flush
			CASE HASH("TAT_VW_004")			iCost = g_sMPTunables.iVC_TATTOO_LADY_LUCK         								BREAK // "Lady Luck
			CASE HASH("TAT_VW_005")			iCost = g_sMPTunables.iVC_TATTOO_GET_LUCKY         								BREAK // "Get Lucky
			CASE HASH("TAT_VW_006")			iCost = g_sMPTunables.iVC_TATTOO_WHEEL_OF_SUITS         						BREAK // "Wheel of Suits
			CASE HASH("TAT_VW_007")			iCost = g_sMPTunables.iVC_TATTOO_777         									BREAK // "777
			CASE HASH("TAT_VW_008")			iCost = g_sMPTunables.iVC_TATTOO_SNAKE_EYES         							BREAK // "Snake Eyes
			CASE HASH("TAT_VW_009")			iCost = g_sMPTunables.iVC_TATTOO_TILL_DEATH_DO_US_PART         					BREAK // "Till Death Do Us Part
			CASE HASH("TAT_VW_010")			iCost = g_sMPTunables.iVC_TATTOO_PHOTO_FINISH         							BREAK // "Photo Finish
			CASE HASH("TAT_VW_011")			iCost = g_sMPTunables.iVC_TATTOO_LIFES_A_GAMBLE         						BREAK // "Life's a Gamble
			CASE HASH("TAT_VW_012")			iCost = g_sMPTunables.iVC_TATTOO_SKULL_OF_SUITS         						BREAK // "Skull of Suits
			CASE HASH("TAT_VW_013")			iCost = g_sMPTunables.iVC_TATTOO_ONEARMED_BANDIT         						BREAK // "One-armed Bandit
			CASE HASH("TAT_VW_014")			iCost = g_sMPTunables.iVC_TATTOO_GAMBLERS_RUIN         							BREAK // "Gambler's Ruin
			CASE HASH("TAT_VW_015")			iCost = g_sMPTunables.iVC_TATTOO_THE_JOLLY_JOKER         						BREAK // "The Jolly Joker
			CASE HASH("TAT_VW_016")			iCost = g_sMPTunables.iVC_TATTOO_ROSE_ACES         								BREAK // "Rose & Aces
			CASE HASH("TAT_VW_017")			iCost = g_sMPTunables.iVC_TATTOO_ROLL_THE_DICE         							BREAK // "Roll the Dice
			CASE HASH("TAT_VW_018")			iCost = g_sMPTunables.iVC_TATTOO_THE_GAMBLERS_LIFE         						BREAK // "The Gambler's Life
			CASE HASH("TAT_VW_019")			iCost = g_sMPTunables.iVC_TATTOO_CANT_WIN_THEM_ALL         						BREAK // "Can't Win Them All
			CASE HASH("TAT_VW_020")			iCost = g_sMPTunables.iVC_TATTOO_CASH_IS_KING         							BREAK // "Cash is King
			CASE HASH("TAT_VW_021")			iCost = g_sMPTunables.iVC_TATTOO_SHOW_YOUR_HAND         						BREAK // "Show Your Hand
			CASE HASH("TAT_VW_022")			iCost = g_sMPTunables.iVC_TATTOO_BLOOD_MONEY         							BREAK // "Blood Money
			CASE HASH("TAT_VW_023")			iCost = g_sMPTunables.iVC_TATTOO_LUCKY_7S         								BREAK // "Lucky 7s
			CASE HASH("TAT_VW_024")			iCost = g_sMPTunables.iVC_TATTOO_CASH_MOUTH         							BREAK // "Cash Mouth
			CASE HASH("TAT_VW_025")			iCost = g_sMPTunables.iVC_TATTOO_QUEEN_OF_ROSES         						BREAK // "Queen of Roses
			CASE HASH("TAT_VW_026")			iCost = g_sMPTunables.iVC_TATTOO_BANKNOTE_ROSE         							BREAK // "Banknote Rose
			CASE HASH("TAT_VW_027")			iCost = g_sMPTunables.iVC_TATTOO_8BALL_ROSE         							BREAK // "8-Ball Rose
			CASE HASH("TAT_VW_028")			iCost = g_sMPTunables.iVC_TATTOO_SKULL_ACES         							BREAK // "Skull & Aces
			CASE HASH("TAT_VW_029")			iCost = g_sMPTunables.iVC_TATTOO_THE_TABLE         								BREAK // "The Table
			CASE HASH("TAT_VW_030")			iCost = g_sMPTunables.iVC_TATTOO_THE_ROYALS         							BREAK // "The Royals
			CASE HASH("TAT_VW_031")			iCost = g_sMPTunables.iVC_TATTOO_GAMBLING_ROYALTY         						BREAK // "Gambling Royalty
			CASE HASH("TAT_VW_032")			iCost = g_sMPTunables.iVC_TATTOO_PLAY_YOUR_ACE         							BREAK // "Play Your Ace
		ENDSWITCH
		SWITCH iLabelHash
			CASE HASH("TAT_H3_000")			iCost = g_sMPTunables.iCH_TATTOO_FIVE_STARS				BREAK
			CASE HASH("TAT_H3_001")			iCost = g_sMPTunables.iCH_TATTOO_ACE_OF_SPADES			BREAK
			CASE HASH("TAT_H3_002")			iCost = g_sMPTunables.iCH_TATTOO_ANIMAL					BREAK
			CASE HASH("TAT_H3_003")			iCost = g_sMPTunables.iCH_TATTOO_ASSAULT_RIFLE			BREAK
			CASE HASH("TAT_H3_004")			iCost = g_sMPTunables.iCH_TATTOO_BAND_AID				BREAK
			CASE HASH("TAT_H3_005")			iCost = g_sMPTunables.iCH_TATTOO_SPADES					BREAK
			CASE HASH("TAT_H3_006")			iCost = g_sMPTunables.iCH_TATTOO_CROWNED				BREAK
			CASE HASH("TAT_H3_007")			iCost = g_sMPTunables.iCH_TATTOO_TWO_HORNS				BREAK
			CASE HASH("TAT_H3_008")			iCost = g_sMPTunables.iCH_TATTOO_ICE_CREAM				BREAK
			CASE HASH("TAT_H3_009")			iCost = g_sMPTunables.iCH_TATTOO_KNIFED					BREAK
			CASE HASH("TAT_H3_010")			iCost = g_sMPTunables.iCH_TATTOO_GREEN_LEAF				BREAK
			CASE HASH("TAT_H3_011")			iCost = g_sMPTunables.iCH_TATTOO_LIPSTICK_KISS			BREAK
			CASE HASH("TAT_H3_012")			iCost = g_sMPTunables.iCH_TATTOO_RAZOR_POP				BREAK
			CASE HASH("TAT_H3_013")			iCost = g_sMPTunables.iCH_TATTOO_LS_STAR				BREAK
			CASE HASH("TAT_H3_014")			iCost = g_sMPTunables.iCH_TATTOO_LS_WINGS				BREAK
			CASE HASH("TAT_H3_015")			iCost = g_sMPTunables.iCH_TATTOO_ON_OFF					BREAK
			CASE HASH("TAT_H3_016")			iCost = g_sMPTunables.iCH_TATTOO_SLEEPY					BREAK
			CASE HASH("TAT_H3_017")			iCost = g_sMPTunables.iCH_TATTOO_SPACE_MONKEY			BREAK
			CASE HASH("TAT_H3_018")			iCost = g_sMPTunables.iCH_TATTOO_STITCHES				BREAK
			CASE HASH("TAT_H3_019")			iCost = g_sMPTunables.iCH_TATTOO_TEDDY_BEAR				BREAK
			CASE HASH("TAT_H3_020")			iCost = g_sMPTunables.iCH_TATTOO_UFO					BREAK
			CASE HASH("TAT_H3_021")			iCost = g_sMPTunables.iCH_TATTOO_WANTED					BREAK
			CASE HASH("TAT_H3_022")			iCost = g_sMPTunables.iCH_TATTOO_THOGS_SWORD			BREAK
			CASE HASH("TAT_H3_023")			iCost = g_sMPTunables.iCH_TATTOO_BIGFOOT				BREAK
			CASE HASH("TAT_H3_024")			iCost = g_sMPTunables.iCH_TATTOO_MOUNT_CHILIAD			BREAK
			CASE HASH("TAT_H3_025")			iCost = g_sMPTunables.iCH_TATTOO_DAVIS					BREAK
			CASE HASH("TAT_H3_026")			iCost = g_sMPTunables.iCH_TATTOO_DIGNITY				BREAK
			CASE HASH("TAT_H3_027")			iCost = g_sMPTunables.iCH_TATTOO_EPSILON				BREAK
			CASE HASH("TAT_H3_028")			iCost = g_sMPTunables.iCH_TATTOO_BANANAS_GONE_BAD		BREAK
			CASE HASH("TAT_H3_029")			iCost = g_sMPTunables.iCH_TATTOO_FATAL_INCURSION		BREAK
			CASE HASH("TAT_H3_030")			iCost = g_sMPTunables.iCH_TATTOO_HOWITZER				BREAK
			CASE HASH("TAT_H3_031")			iCost = g_sMPTunables.iCH_TATTOO_KIFFLOM				BREAK
			CASE HASH("TAT_H3_032")			iCost = g_sMPTunables.iCH_TATTOO_LOVE_FIST				BREAK
			CASE HASH("TAT_H3_033")			iCost = g_sMPTunables.iCH_TATTOO_LS_CITY				BREAK
			CASE HASH("TAT_H3_034")			iCost = g_sMPTunables.iCH_TATTOO_LS_MONOGRAM			BREAK
			CASE HASH("TAT_H3_035")			iCost = g_sMPTunables.iCH_TATTOO_LS_PANIC				BREAK
			CASE HASH("TAT_H3_036")			iCost = g_sMPTunables.iCH_TATTOO_LS_SHIELD				BREAK
			CASE HASH("TAT_H3_037")			iCost = g_sMPTunables.iCH_TATTOO_LADYBUG				BREAK
			CASE HASH("TAT_H3_038")			iCost = g_sMPTunables.iCH_TATTOO_ROBOT_BUBBLEGUM		BREAK
			CASE HASH("TAT_H3_039")			iCost = g_sMPTunables.iCH_TATTOO_SPACE_RANGERS			BREAK
			CASE HASH("TAT_H3_040")			iCost = g_sMPTunables.iCH_TATTOO_TIGER_HEART			BREAK
			CASE HASH("TAT_H3_041")			iCost = g_sMPTunables.iCH_TATTOO_MIGHTY_THOG			BREAK
			CASE HASH("TAT_H3_042")			iCost = g_sMPTunables.iCH_TATTOO_HEARTS					BREAK
			CASE HASH("TAT_H3_043")			iCost = g_sMPTunables.iCH_TATTOO_DIAMONDS				BREAK
			CASE HASH("TAT_H3_044")			iCost = g_sMPTunables.iCH_TATTOO_CLUBS					BREAK
		ENDSWITCH
		SWITCH iLabelHash
			CASE HASH("TAT_H4_000")			iCost = g_sMPTunables.iIH_TATTOO_HEADPHONE_SPLAT		BREAK // Headphone Splat
			CASE HASH("TAT_H4_001")			iCost = g_sMPTunables.iIH_TATTOO_TROPICAL_DUDE			BREAK // Tropical Dude
			CASE HASH("TAT_H4_002")			iCost = g_sMPTunables.iIH_TATTOO_JELLYFISH_SHADES		BREAK // Jellyfish Shades
			CASE HASH("TAT_H4_003")			iCost = g_sMPTunables.iIH_TATTOO_LIGHTHOUSE				BREAK // Lighthouse
			CASE HASH("TAT_H4_004")			iCost = g_sMPTunables.iIH_TATTOO_SKELETON_BREEZE		BREAK // Skeleton Breeze
			CASE HASH("TAT_H4_005")			iCost = g_sMPTunables.iIH_TATTOO_LSUR					BREAK // LSUR
			CASE HASH("TAT_H4_006")			iCost = g_sMPTunables.iIH_TATTOO_MUSIC_LOCKER			BREAK // Music Locker
			CASE HASH("TAT_H4_007")			iCost = g_sMPTunables.iIH_TATTOO_SKELETON_DJ			BREAK // Skeleton DJ
			CASE HASH("TAT_H4_008")			iCost = g_sMPTunables.iIH_TATTOO_SMILEY_GLITCH			BREAK // Smiley Glitch
			CASE HASH("TAT_H4_009")			iCost = g_sMPTunables.iIH_TATTOO_SCRATCH_PANTHER		BREAK // Scratch Panther
			CASE HASH("TAT_H4_010")			iCost = g_sMPTunables.iIH_TATTOO_TROPICAL_SERPENT		BREAK // Tropical Serpent
			CASE HASH("TAT_H4_011")			iCost = g_sMPTunables.iIH_TATTOO_SOULWAX				BREAK // Soulwax
			CASE HASH("TAT_H4_012")			iCost = g_sMPTunables.iIH_TATTOO_STILL_SLIPPIN			BREAK // Still Slippin'
			CASE HASH("TAT_H4_013")			iCost = g_sMPTunables.iIH_TATTOO_WILD_DANCERS			BREAK // Wild Dancers
			CASE HASH("TAT_H4_014")			iCost = g_sMPTunables.iIH_TATTOO_PARADISE_NAP			BREAK // Paradise Nap
			CASE HASH("TAT_H4_015")			iCost = g_sMPTunables.iIH_TATTOO_PARADISE_UKULELE		BREAK // Paradise Ukulele 
			CASE HASH("TAT_H4_016")			iCost = g_sMPTunables.iIH_TATTOO_ROSE_PANTHER			BREAK // Rose Panther
			CASE HASH("TAT_H4_017")			iCost = g_sMPTunables.iIH_TATTOO_TROPICAL_SORCERER		BREAK // Tropical Sorcerer
			CASE HASH("TAT_H4_018")			iCost = g_sMPTunables.iIH_TATTOO_RECORD_HEAD			BREAK // Record Head
			CASE HASH("TAT_H4_019")			iCost = g_sMPTunables.iIH_TATTOO_RECORD_SHOT			BREAK // Record Shot
			CASE HASH("TAT_H4_020")			iCost = g_sMPTunables.iIH_TATTOO_SPEAKER_TOWER			BREAK // Speaker Tower
			CASE HASH("TAT_H4_021")			iCost = g_sMPTunables.iIH_TATTOO_SKULL_SURFER			BREAK // Skull Surfer
			CASE HASH("TAT_H4_022")			iCost = g_sMPTunables.iIH_TATTOO_PARADISE_SIRENS		BREAK // Paradise Sirens
			CASE HASH("TAT_H4_023")			iCost = g_sMPTunables.iIH_TATTOO_TECHNO_GLITCH			BREAK // Techno Glitch
			CASE HASH("TAT_H4_024")			iCost = g_sMPTunables.iIH_TATTOO_PINEAPPLE_SKULL		BREAK // Pineapple Skull
			CASE HASH("TAT_H4_025")			iCost = g_sMPTunables.iIH_TATTOO_GLOW_PRINCESS			BREAK // Glow Princess
			CASE HASH("TAT_H4_026")			iCost = g_sMPTunables.iIH_TATTOO_SHARK_WATER			BREAK // Shark Water
			CASE HASH("TAT_H4_027")			iCost = g_sMPTunables.iIH_TATTOO_SKULLPHONES			BREAK // Skullphones
			CASE HASH("TAT_H4_028")			iCost = g_sMPTunables.iIH_TATTOO_SKULL_WATERS			BREAK // Skull Waters
			CASE HASH("TAT_H4_029")			iCost = g_sMPTunables.iIH_TATTOO_SOUNDWAVES				BREAK // Soundwaves
			CASE HASH("TAT_H4_030")			iCost = g_sMPTunables.iIH_TATTOO_RADIO_TAPE				BREAK // Radio Tape
			CASE HASH("TAT_H4_031")			iCost = g_sMPTunables.iIH_TATTOO_OCTOPUS_SHADES			BREAK // Octopus Shades
			CASE HASH("TAT_H4_032")			iCost = g_sMPTunables.iIH_TATTOO_KULT_991_FM			BREAK // K.U.L.T. 99.1 FM
		ENDSWITCH
		#IF FEATURE_FIXER
		SWITCH iLabelHash
			CASE HASH("TAT_FX_000") iCost = g_sMPTunables.iFIXER_TATTOO_HOOD_SKELETON	BREAK // Hood Skeleton
			CASE HASH("TAT_FX_001") iCost = g_sMPTunables.iFIXER_TATTOO_BRIGHT_DIAMOND	BREAK // Bright Diamond
			CASE HASH("TAT_FX_002") iCost = g_sMPTunables.iFIXER_TATTOO_HUSTLE	BREAK // Hustle
			CASE HASH("TAT_FX_003") iCost = g_sMPTunables.iFIXER_TATTOO_BANDANA_KNIFE	BREAK // Bandana Knife
			CASE HASH("TAT_FX_004") iCost = g_sMPTunables.iFIXER_TATTOO_HOOD_HEART	BREAK // Hood Heart
			CASE HASH("TAT_FX_005") iCost = g_sMPTunables.iFIXER_TATTOO_PEACOCK	BREAK // Peacock
			CASE HASH("TAT_FX_006") iCost = g_sMPTunables.iFIXER_TATTOO_SKELETON_SHOT	BREAK // Skeleton Shot
			CASE HASH("TAT_FX_007") iCost = g_sMPTunables.iFIXER_TATTOO_BALLAS_4_LIFE	BREAK // Ballas 4 Life
			CASE HASH("TAT_FX_008") iCost = g_sMPTunables.iFIXER_TATTOO_LOS_SANTOS_TAG	BREAK // Los Santos Tag
			CASE HASH("TAT_FX_009") iCost = g_sMPTunables.iFIXER_TATTOO_ASCENSION	BREAK // Ascension
			CASE HASH("TAT_FX_010") iCost = g_sMPTunables.iFIXER_TATTOO_MUSIC_IS_THE_REMEDY	BREAK // Music Is The Remedy
			CASE HASH("TAT_FX_011") iCost = g_sMPTunables.iFIXER_TATTOO_SERPENT_MIC	BREAK // Serpent Mic
			CASE HASH("TAT_FX_012") iCost = g_sMPTunables.iFIXER_TATTOO_ZOMBIE_RHYMES	BREAK // Zombie Rhymes
			CASE HASH("TAT_FX_013") iCost = g_sMPTunables.iFIXER_TATTOO_BLESSED_BOOMBOX	BREAK // Blessed Boombox
			CASE HASH("TAT_FX_014") iCost = g_sMPTunables.iFIXER_TATTOO_CHAMBERLAIN_HILLS	BREAK // Chamberlain Hills
			CASE HASH("TAT_FX_015") iCost = g_sMPTunables.iFIXER_TATTOO_SMOKING_BARRELS	BREAK // Smoking Barrels
			CASE HASH("TAT_FX_016") iCost = g_sMPTunables.iFIXER_TATTOO_ALL_FROM_THE_SAME_TREE	BREAK // All From The Same Tree
			CASE HASH("TAT_FX_017") iCost = g_sMPTunables.iFIXER_TATTOO_KING_OF_THE_JUNGLE	BREAK // King of the Jungle
			CASE HASH("TAT_FX_018") iCost = g_sMPTunables.iFIXER_TATTOO_NIGHT_OWL	BREAK // Night Owl
			CASE HASH("TAT_FX_019") iCost = g_sMPTunables.iFIXER_TATTOO_WEED_KNUCKLES	BREAK // Weed Knuckles
			CASE HASH("TAT_FX_020") iCost = g_sMPTunables.iFIXER_TATTOO_DOG_FIST	BREAK // Dog Fist
			CASE HASH("TAT_FX_021") iCost = g_sMPTunables.iFIXER_TATTOO_GRAFFITI_SKULL	BREAK // Graffiti Skull
			CASE HASH("TAT_FX_022") iCost = g_sMPTunables.iFIXER_TATTOO_LS_SMOKING_CARTRIDGES	BREAK // LS Smoking Cartridges
			CASE HASH("TAT_FX_023") iCost = g_sMPTunables.iFIXER_TATTOO_TRUST	BREAK // Trust
			CASE HASH("TAT_FX_024") iCost = g_sMPTunables.iFIXER_TATTOO_BEATBOX_SILHOUETTE	BREAK // Beatbox Silhouette
			CASE HASH("TAT_FX_025") iCost = g_sMPTunables.iFIXER_TATTOO_DAVIS_FLAMES	BREAK // Davis Flames
			CASE HASH("TAT_FX_026") iCost = g_sMPTunables.iFIXER_TATTOO_DOLLAR_GUNS_CROSSED	BREAK // Dollar Guns Crossed
			CASE HASH("TAT_FX_027") iCost = g_sMPTunables.iFIXER_TATTOO_BLACK_WIDOW	BREAK // Black Widow
		ENDSWITCH
		#ENDIF
		#IF FEATURE_DLC_1_2022
		SWITCH iLabelHash
			CASE HASH("TAT_SB_000")		iCost = g_sMPTunables.iSU22_TATTOO_LIVE_FAST_MONO			BREAK // Live Fast Mono
			CASE HASH("TAT_SB_001")		iCost = g_sMPTunables.iSU22_TATTOO_LIVE_FAST_COLOR			BREAK // Live Fast Color
			CASE HASH("TAT_SB_002")		iCost = g_sMPTunables.iSU22_TATTOO_COBRA_BIKER			BREAK // Cobra Biker
			CASE HASH("TAT_SB_003")		iCost = g_sMPTunables.iSU22_TATTOO_BULLET_MOUTH			BREAK // Bullet Mouth
			CASE HASH("TAT_SB_004")		iCost = g_sMPTunables.iSU22_TATTOO_SMOKING_BARREL			BREAK // Smoking Barrel
			CASE HASH("TAT_SB_005")		iCost = g_sMPTunables.iSU22_TATTOO_CONCEALED			BREAK // Concealed
			CASE HASH("TAT_SB_006")		iCost = g_sMPTunables.iSU22_TATTOO_PAINTED_MICRO_SMG			BREAK // Painted Micro SMG
			CASE HASH("TAT_SB_007")		iCost = g_sMPTunables.iSU22_TATTOO_WEAPON_KING			BREAK // Weapon King
			CASE HASH("TAT_SB_008")		iCost = g_sMPTunables.iSU22_TATTOO_BIGNESS_CHIMP			BREAK // Bigness Chimp
			CASE HASH("TAT_SB_009")		iCost = g_sMPTunables.iSU22_TATTOO_UPNATOMIZER_DESIGN			BREAK // Up-n-Atomizer Design
			CASE HASH("TAT_SB_010")		iCost = g_sMPTunables.iSU22_TATTOO_ROCKET_LAUNCHER_GIRL			BREAK // Rocket Launcher Girl
			CASE HASH("TAT_SB_011")		iCost = g_sMPTunables.iSU22_TATTOO_MINIGUN_GUY			BREAK // Minigun Guy
			CASE HASH("TAT_SB_012")		iCost = g_sMPTunables.iSU22_TATTOO_SNAKE_REVOLVER			BREAK // Snake Revolver
			CASE HASH("TAT_SB_013")		iCost = g_sMPTunables.iSU22_TATTOO_WEAPON_SLEEVE			BREAK // Weapon Sleeve
			CASE HASH("TAT_SB_014")		iCost = g_sMPTunables.iSU22_TATTOO_MINIMAL_SMG			BREAK // Minimal SMG
			CASE HASH("TAT_SB_015")		iCost = g_sMPTunables.iSU22_TATTOO_MINIMAL_ADVANCED_RIFLE			BREAK // Minimal Advanced Rifle
			CASE HASH("TAT_SB_016")		iCost = g_sMPTunables.iSU22_TATTOO_MINIMAL_SNIPER_RIFLE			BREAK // Minimal Sniper Rifle
			CASE HASH("TAT_SB_017")		iCost = g_sMPTunables.iSU22_TATTOO_SKULL_GRENADE			BREAK // Skull Grenade
			CASE HASH("TAT_SB_018")		iCost = g_sMPTunables.iSU22_TATTOO_BRANCHED_SKULL			BREAK // Branched Skull
			CASE HASH("TAT_SB_019")		iCost = g_sMPTunables.iSU22_TATTOO_SCYTHED_CORPSE			BREAK // Scythed Corpse
			CASE HASH("TAT_SB_020")		iCost = g_sMPTunables.iSU22_TATTOO_SCYTHED_CORPSE_REAPER			BREAK // Scythed Corpse & Reaper
			CASE HASH("TAT_SB_021")		iCost = g_sMPTunables.iSU22_TATTOO_THIRD_EYE			BREAK // Third Eye
			CASE HASH("TAT_SB_022")		iCost = g_sMPTunables.iSU22_TATTOO_PIERCED_THIRD_EYE			BREAK // Pierced Third Eye
			CASE HASH("TAT_SB_023")		iCost = g_sMPTunables.iSU22_TATTOO_LIP_DRIP			BREAK // Lip Drip
			CASE HASH("TAT_SB_024")		iCost = g_sMPTunables.iSU22_TATTOO_SKIN_MASK			BREAK // Skin Mask
			CASE HASH("TAT_SB_025")		iCost = g_sMPTunables.iSU22_TATTOO_WEBBED_SCYTHE			BREAK // Webbed Scythe
			CASE HASH("TAT_SB_026")		iCost = g_sMPTunables.iSU22_TATTOO_ONI_DEMON			BREAK // Oni Demon
			CASE HASH("TAT_SB_027")		iCost = g_sMPTunables.iSU22_TATTOO_BAT_WINGS			BREAK // Bat Wings
			CASE HASH("TAT_SB_028")		iCost = g_sMPTunables.iSU22_TATTOO_LASER_EYES_SKULL			BREAK // Laser Eyes Skull
			CASE HASH("TAT_SB_029")		iCost = g_sMPTunables.iSU22_TATTOO_CLASSIC_VAMPIRE			BREAK // Classic Vampire
			CASE HASH("TAT_SB_030")		iCost = g_sMPTunables.iSU22_TATTOO_CENTIPEDE			BREAK // Centipede
			CASE HASH("TAT_SB_031")		iCost = g_sMPTunables.iSU22_TATTOO_FLESHY_EYE			BREAK // Fleshy Eye
			CASE HASH("TAT_SB_032")		iCost = g_sMPTunables.iSU22_TATTOO_MANYEYED_GOAT			BREAK // Many-eyed Goat
			CASE HASH("TAT_SB_033")		iCost = g_sMPTunables.iSU22_TATTOO_THREEEYED_DEMON			BREAK // Three-eyed Demon
			CASE HASH("TAT_SB_034")		iCost = g_sMPTunables.iSU22_TATTOO_SMOULDERING_REAPER			BREAK // Smouldering Reaper
			CASE HASH("TAT_SB_035")		iCost = g_sMPTunables.iSU22_TATTOO_SNIFF_SNIFF			BREAK // Sniff Sniff
			CASE HASH("TAT_SB_036")		iCost = g_sMPTunables.iSU22_TATTOO_CHARM_PATTERN			BREAK // Charm Pattern
			CASE HASH("TAT_SB_037")		iCost = g_sMPTunables.iSU22_TATTOO_WITCH_SKULL			BREAK // Witch & Skull
			CASE HASH("TAT_SB_038")		iCost = g_sMPTunables.iSU22_TATTOO_PUMPKIN_BUG			BREAK // Pumpkin Bug
			CASE HASH("TAT_SB_039")		iCost = g_sMPTunables.iSU22_TATTOO_SINNER			BREAK // Sinner
			CASE HASH("TAT_SB_040")		iCost = g_sMPTunables.iSU22_TATTOO_CARVED_PUMPKIN			BREAK // Carved Pumpkin
			CASE HASH("TAT_SB_041")		iCost = g_sMPTunables.iSU22_TATTOO_BRANCHED_WEREWOLF			BREAK // Branched Werewolf
			CASE HASH("TAT_SB_042")		iCost = g_sMPTunables.iSU22_TATTOO_WINGED_SKULL			BREAK // Winged Skull
			CASE HASH("TAT_SB_043")		iCost = g_sMPTunables.iSU22_TATTOO_CURSED_SAKI			BREAK // Cursed Saki
			CASE HASH("TAT_SB_044")		iCost = g_sMPTunables.iSU22_TATTOO_SMOULDERING_BAT_SKULL			BREAK // Smouldering Bat & Skull
			CASE HASH("TAT_SB_045")		iCost = g_sMPTunables.iSU22_TATTOO_ARMORED_ARM			BREAK // Armored Arm
			CASE HASH("TAT_SB_046")		iCost = g_sMPTunables.iSU22_TATTOO_DEMON_SMILE			BREAK // Demon Smile
			CASE HASH("TAT_SB_047")		iCost = g_sMPTunables.iSU22_TATTOO_ANGEL_DEVIL			BREAK // Angel & Devil
			CASE HASH("TAT_SB_048")		iCost = g_sMPTunables.iSU22_TATTOO_DEATH_IS_CERTAIN			BREAK // Death Is Certain
			CASE HASH("TAT_SB_049")		iCost = g_sMPTunables.iSU22_TATTOO_DEMON_DRUMMER			BREAK // Demon Drummer
			CASE HASH("TAT_SB_050")		iCost = g_sMPTunables.iSU22_TATTOO_GOLD_GUN			BREAK // Gold Gun
			CASE HASH("TAT_SB_051")		iCost = g_sMPTunables.iSU22_TATTOO_BLUE_SERPENT			BREAK // Blue Serpent
			CASE HASH("TAT_SB_052")		iCost = g_sMPTunables.iSU22_TATTOO_NIGHT_DEMON			BREAK // Night Demon
			CASE HASH("TAT_SB_053")		iCost = g_sMPTunables.iSU22_TATTOO_MOBSTER_SKULL			BREAK // Mobster Skull
			CASE HASH("TAT_SB_054")		iCost = g_sMPTunables.iSU22_TATTOO_WOUNDED_HEAD			BREAK // Wounded Head
			CASE HASH("TAT_SB_055")		iCost = g_sMPTunables.iSU22_TATTOO_STABBED_SKULL			BREAK // Stabbed Skull
			CASE HASH("TAT_SB_056")		iCost = g_sMPTunables.iSU22_TATTOO_TIGER_BLADE			BREAK // Tiger Blade
			CASE HASH("TAT_SB_057")		iCost = g_sMPTunables.iSU22_TATTOO_GRAY_DEMON			BREAK // Gray Demon
			CASE HASH("TAT_SB_058")		iCost = g_sMPTunables.iSU22_TATTOO_SHRIEKING_DRAGON			BREAK // Shrieking Dragon
			CASE HASH("TAT_SB_059")		iCost = g_sMPTunables.iSU22_TATTOO_SWORDS_CITY			BREAK // Swords & City
			CASE HASH("TAT_SB_060")		iCost = g_sMPTunables.iSU22_TATTOO_BLAINE_COUNTY			BREAK // Blaine County
			CASE HASH("TAT_SB_061")		iCost = g_sMPTunables.iSU22_TATTOO_ANGRY_POSSUM			BREAK // Angry Possum
			CASE HASH("TAT_SB_062")		iCost = g_sMPTunables.iSU22_TATTOO_FLORAL_DEMON			BREAK // Floral Demon
		ENDSWITCH
		#ENDIF
		
		iCost = FLOOR((TO_FLOAT(iCost) * g_sMPTunables.fTattooShopMultiplier))
	ENDIF
	
	INT iDiscount = GET_TATTOO_DISCOUNT()
	iCost = FLOOR((TO_FLOAT(iCost) * (1.0 - TO_FLOAT(iDiscount)/100)))
	
	IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(tlLabel)
		iCost = 0
	ENDIF
	
	// Price override for basket system
	IF SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
		BOOL bStarterPackItem = FALSE
		IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(tlLabel)
			bStarterPackItem = TRUE
		ENDIF
		
		TEXT_LABEL_63 tlCatalogueKey
		GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, tlLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
		IF NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
			iCost = NET_GAMESERVER_GET_PRICE(GET_HASH_KEY(tlCatalogueKey), CATEGORY_TATTOO, 1)
		ELSE
			iCost = -1
		ENDIF
	ENDIF
ENDPROC

