USING "rage_builtins.sch"
USING "script_misc.sch"

/////////////////////////////////////////////////////////////////
///    Function helpers for gen9 exclusive vehicle mods    //////
/////////////////////////////////////////////////////////////////
 
 
FUNC BOOL IS_GEN9_EXCLUSIVE_VEHICLE_MODS_UNLOCKED()
	#IF FEATURE_GEN9_EXCLUSIVE
		RETURN TRUE
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_COLOUR_INDEX_GEN9_EXCLUSIVE(INT iColourIndex)
	SWITCH iColourIndex
		CASE 161 	// Anodized Red Pearl
		CASE 164 	// Anodized Blue Pearl
		CASE 170	// Anodized Gold Pearl
		CASE 171	// Green/Blue Flip
		CASE 183 	// purple/Blue Flip
		CASE 191 	// orange/Blue Flip
		CASE 199 	// Dark purple pearl
		CASE 209 	// Baby blue pearl
		CASE 216 	// Red prismatic pearl
		CASE 218 	// Black prismatic pearl
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_COLOUR_TYPE_GEN9_EXCLUSIVE(MOD_COLOR_TYPE eType)
	SWITCH eType
		CASE MCT_CHAMELEON
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_MOD_LOCKED(VEHICLE_INDEX playerVehicle, MOD_TYPE modType, INT iModIndex)
	
	IF NOT IS_GEN9_EXCLUSIVE_VEHICLE_MODS_UNLOCKED()
	AND IS_VEHICLE_MOD_GEN9_EXCLUSIVE(playerVehicle, modType, iModIndex)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_COLOUR_INDEX_LOCKED(INT iColourIndex)
	
	IF NOT IS_GEN9_EXCLUSIVE_VEHICLE_MODS_UNLOCKED()
	AND IS_THIS_COLOUR_INDEX_GEN9_EXCLUSIVE(iColourIndex)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_COLOUR_TYPE_LOCKED(MOD_COLOR_TYPE eType)
	
	IF NOT IS_GEN9_EXCLUSIVE_VEHICLE_MODS_UNLOCKED()
	AND IS_THIS_COLOUR_TYPE_GEN9_EXCLUSIVE(eType)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

