//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	hairdo_shop_private.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains all the functionality for the 	//
//							hairdo shops.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "shop_private.sch"
USING "clothes_shop_private.sch"
USING "reward_unlocks.sch"

CONST_INT USE_NEW_HAIRDOS_SHOP_MENUS 1

CONST_FLOAT fMIN_OPACITY_BLEND		0.15
CONST_FLOAT fMAX_OPACITY_BLEND		1.0

ENUM HAIRDO_GROUP_ENUM
	HAIR_GROUP_NONE = -2,
	HAIR_GROUP_ALL = -1,
	
	#IF FEATURE_DLC_1_2022
	HAIR_GROUP_M_DLC_SUMMER2022,
	HAIR_GROUP_F_DLC_SUMMER2022,
	#ENDIF
	
	#IF FEATURE_FIXER
	HAIR_GROUP_M_DLC_FIXER,
	HAIR_GROUP_F_DLC_FIXER,
	#ENDIF
	
	HAIR_GROUP_M_DLC_AFRO,
	HAIR_GROUP_F_DLC_AFRO,
	
	HAIR_GROUP_M_DLC_VINEWOOD,
	HAIR_GROUP_F_DLC_VINEWOOD,
	
	HAIR_GROUP_M_DLC_GUNRUNNING,
	HAIR_GROUP_F_DLC_GUNRUNNING,
	
	HAIR_GROUP_M_DLC_BIKER, // BIKER
	HAIR_GROUP_F_DLC_BIKER, // BIKER
	
	HAIR_GROUP_M_DLC_LOWRIDER, // Lowrider
	HAIR_GROUP_F_DLC_LOWRIDER, // Lowrider
	
	HAIR_GROUP_MULLET, // Independence
	
	HAIR_GROUP_LONG_SLICKED, // Hipster
	HAIR_GROUP_HIPSTER_YOUTH, // Hipster
	HAIR_GROUP_BIG_BANGS, // Hipster
	HAIR_GROUP_BRAIDED_TOP_KNOT, // Hipster
	
	HAIR_GROUP_SHORT_SIDE_PART, // Business
	HAIR_GROUP_HIGH_SLICKED_SIDES, // Business
	HAIR_GROUP_TIGHT_BUN, // Business
	HAIR_GROUP_TWISTED_BOB, // Business
	
	HAIR_GROUP_FLAPPER_BOB, // Valentines
	
	HAIR_GROUP_PIN_UP_GIRL, // Beach
	HAIR_GROUP_MESSY_BUN, // Beach
	HAIR_GROUP_SHAGGY, // Beach
	HAIR_GROUP_SURFER, // Beach
	
	// Male hairdoos
	HAIR_GROUP_M_SHAVEN,
	HAIR_GROUP_M_BUZZCUT,
	HAIR_GROUP_M_FAUX_HAWK,
	HAIR_GROUP_M_HIPSTER_SHAVED,
	HAIR_GROUP_M_SIDE_PARTING_SPIKED,
	HAIR_GROUP_M_SHORTER_CUT,
	HAIR_GROUP_M_BIKER,
	HAIR_GROUP_M_PONYTAIL,
	HAIR_GROUP_M_CORNROWS,
	HAIR_GROUP_M_SLICKED,
	HAIR_GROUP_M_SHORT_BRUSHED,
	HAIR_GROUP_M_SPIKEY,
	HAIR_GROUP_M_CAESER,
	HAIR_GROUP_M_CHOPPED,
	HAIR_GROUP_M_DREADS,
	HAIR_GROUP_M_LONG_HAIR,
	
	// Female hairdos
	HAIR_GROUP_F_SHAVEN,
	HAIR_GROUP_F_SHORT,
	HAIR_GROUP_F_LAYERED_BOB,
	HAIR_GROUP_F_PIGTAILS,
	HAIR_GROUP_F_PONYTAIL,
	HAIR_GROUP_F_BRAIDED_MOHAWK,
	HAIR_GROUP_F_BRAIDS,
	HAIR_GROUP_F_BOB,
	HAIR_GROUP_F_FAUX_HAWK,
	HAIR_GROUP_F_FRENCH_TWIST,
	HAIR_GROUP_F_LONG_BOB,
	HAIR_GROUP_F_LOOSE_TIED,
	HAIR_GROUP_F_PIXIE,
	HAIR_GROUP_F_SHAVED_BANGS,
	HAIR_GROUP_F_TOP_KNOT,
	HAIR_GROUP_F_WAVY_BOB,
	
	HAIR_GROUP_TOTAL
	
ENDENUM

CONST_INT NUMBER_OF_HAIRDO_GROUPS (HAIR_GROUP_TOTAL)

/*
Hairdo shops animations list:

Salon:
	"misshair_shop@hair_dressers"
	
	assistant_base
	assistant_idle_a
	
	cam_base
	cam_enterchair
	cam_exitchair
	cam_hair_cut_a
	cam_hair_cut_b
	cam_intro_-_customer_-keeper_-player_-assistant
	
	customer_intro

	keeper_base
	keeper_enterchair
	keeper_exitchair
	keeper_hair_cut_a
	keeper_hair_cut_b
	keeper_idle_a
	keeper_idle_b
	keeper_idle_c
	keeper_intro
	
	left_door_cust_intro
	left_door_playerintro 	// played on the door as player enters
	
	player_base
	player_enterchair
	player_exitchair
	player_idle_a
	player_idle_b
	player_idle_c
	player_idle_d
	player_intro			// played on player as he enters (with door anims)
	
	right_door_cust_intro
	right_door_playerintro	// played on the door as player enters

	scissors_base
	scissors_enterchair
	scissor_exitchair
	scissors_hair_cut_a
	scissors_hair_cut_b
	scissors_idle_a
	scissors_idle_b
	scissors_idle_c
	scissors_intro

barbers:
 	"misshair_shop@barbers"
	
	cam_base
	enterchair_cam
	exitchair_cam
	idle_a_cam
	idle_b_cam
	tutorial_cam
	
	customer_intro
	customer_tutorial
	customer_tutorial_base
	
	door_intro
	door_tutorial
	
	keeper_base
	keeper_enterchair
	keeper_exitchair
	keeper_idle_a
	keeper_idle_b
	keeper_tutorial
	keeper_tutorial_base
	
	player_base
	player_enterchair
	player_exitchair
	player_idle_a
	player_idle_b
	player_idle_c
	player_idle_d
	player_intro
	
	scissors_base
	scissors_enterchair
	scissors_exitchair
	scissors_idle_a
	scissors_idle_b
	scissors_tutorial
	scissors_tutorial_base
*/

ENUM HAIRDO_SHOP_CAMERAS
	HSC_ENTER_CHAIR,
	HSC_EXIT_CHAIR,
	HSC_TUTORIAL,
	HSC_CUT
ENDENUM

ENUM HAIRDO_DIALOGUE_ENUM
	HAIR_DLG_NONE = -1,
	HAIR_DLG_GREET,
	HAIR_DLG_ASK,
	HAIR_DLG_CUT,
	HAIR_DLG_EXIT
ENDENUM

FUNC BOOL IS_NGMP_MENU(HAIRDO_MENU_ENUM eMenu)
	RETURN (eMenu >= HME_NGMP_MAIN)
ENDFUNC

FUNC INT GET_MENU_ITEM_FOR_NGMP_MENU(HAIRDO_MENU_ENUM eMenu)
	SWITCH eMenu
		CASE HME_NGMP_MAIN					RETURN 0 BREAK
		CASE HME_NGMP_HAIR					RETURN 0 BREAK
		CASE HME_NGMP_BEARD					RETURN 1 BREAK
		CASE HME_NGMP_EYEBROWS				RETURN 2 BREAK
		CASE HME_NGMP_CHEST					RETURN 3 BREAK
		CASE HME_NGMP_CONTACTS				RETURN 4 BREAK
		CASE HME_NGMP_FACEPAINT				RETURN 5 BREAK
		CASE HME_NGMP_MAKEUP				RETURN 6 BREAK
		CASE HME_NGMP_MAKEUP_BLUSHER		RETURN 0 BREAK
		CASE HME_NGMP_MAKEUP_EYE			RETURN 1 BREAK
		CASE HME_NGMP_MAKEUP_LIPSTICK		RETURN 2 BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, SHOP_NAME_ENUM eShop)
	IF (eShop = HAIRDO_SHOP_CASINO_APT)
		SWITCH eMenu
			CASE HME_NGMP_CHEST					RETURN FALSE BREAK
			CASE HME_NGMP_CONTACTS				RETURN FALSE BREAK
			CASE HME_NGMP_FACEPAINT				RETURN FALSE BREAK
		ENDSWITCH
	ENDIF
	
	IF ePedModel = MP_M_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_MAIN					RETURN TRUE BREAK
			CASE HME_NGMP_HAIR					RETURN TRUE BREAK
			CASE HME_NGMP_BEARD					RETURN TRUE BREAK
			CASE HME_NGMP_EYEBROWS				RETURN TRUE BREAK
			CASE HME_NGMP_CHEST					RETURN TRUE BREAK
			CASE HME_NGMP_CONTACTS				RETURN TRUE BREAK
			CASE HME_NGMP_FACEPAINT				RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP				RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP_BLUSHER		RETURN FALSE BREAK // No
			CASE HME_NGMP_MAKEUP_EYE			RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK		RETURN TRUE BREAK
		ENDSWITCH
	ELIF ePedModel = MP_F_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_MAIN					RETURN TRUE BREAK
			CASE HME_NGMP_HAIR					RETURN TRUE BREAK
			CASE HME_NGMP_BEARD					RETURN FALSE BREAK // No
			CASE HME_NGMP_EYEBROWS				RETURN TRUE BREAK
			CASE HME_NGMP_CHEST					RETURN FALSE BREAK // No
			CASE HME_NGMP_CONTACTS				RETURN TRUE BREAK
			CASE HME_NGMP_FACEPAINT				RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP				RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP_BLUSHER		RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP_EYE			RETURN TRUE BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK		RETURN TRUE BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC

CONST_INT NUMBER_OF_NGMP_MAIN_OPTIONS 7
FUNC HAIRDO_MENU_ENUM GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(INT iOption)
	SWITCH iOption
		CASE 0			RETURN HME_NGMP_HAIR		BREAK
		CASE 1			RETURN HME_NGMP_BEARD	 	BREAK
		CASE 2			RETURN HME_NGMP_EYEBROWS	BREAK
		CASE 3			RETURN HME_NGMP_CHEST		BREAK
		CASE 4			RETURN HME_NGMP_CONTACTS	BREAK
		CASE 5			RETURN HME_NGMP_FACEPAINT	BREAK
		CASE 6			RETURN HME_NGMP_MAKEUP		BREAK
	ENDSWITCH
	RETURN INT_TO_ENUM(HAIRDO_MENU_ENUM, -1)
ENDFUNC

CONST_INT NUMBER_OF_NGMP_MAKEUP_OPTIONS 3
FUNC HAIRDO_MENU_ENUM GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(INT iOption)
	SWITCH iOption
		CASE 0			RETURN HME_NGMP_MAKEUP_BLUSHER	BREAK
		CASE 1			RETURN HME_NGMP_MAKEUP_EYE	 	BREAK
		CASE 2			RETURN HME_NGMP_MAKEUP_LIPSTICK	BREAK
	ENDSWITCH
	RETURN INT_TO_ENUM(HAIRDO_MENU_ENUM, -1)
ENDFUNC

FUNC STRING GET_LABEL_FOR_NGMP_MENU(HAIRDO_MENU_ENUM eMenu, BOOL bTitle = FALSE)
	SWITCH eMenu
		CASE HME_NGMP_MAIN					IF bTitle RETURN "HAIR_NG_T0" 	ELSE RETURN "HAIR_NG_O0" 	ENDIF BREAK
		CASE HME_NGMP_HAIR					IF bTitle RETURN "HAIR_NG_T1" 	ELSE RETURN "HAIR_NG_O1" 	ENDIF BREAK
		CASE HME_NGMP_BEARD					IF bTitle RETURN "HAIR_NG_T2" 	ELSE RETURN "HAIR_NG_O2" 	ENDIF BREAK
		CASE HME_NGMP_EYEBROWS				IF bTitle RETURN "HAIR_NG_T3" 	ELSE RETURN "HAIR_NG_O3" 	ENDIF BREAK
		CASE HME_NGMP_CHEST					IF bTitle RETURN "HAIR_NG_T4" 	ELSE RETURN "HAIR_NG_O4" 	ENDIF BREAK
		CASE HME_NGMP_CONTACTS				IF bTitle RETURN "HAIR_NG_T5" 	ELSE RETURN "HAIR_NG_O5" 	ENDIF BREAK
		CASE HME_NGMP_FACEPAINT				IF bTitle RETURN "HAIR_NG_T6" 	ELSE RETURN "HAIR_NG_O6" 	ENDIF BREAK
		CASE HME_NGMP_MAKEUP				IF bTitle RETURN "HAIR_NG_T7" 	ELSE RETURN "HAIR_NG_O7" 	ENDIF BREAK
		CASE HME_NGMP_MAKEUP_BLUSHER		IF bTitle RETURN "HAIR_NG_T8" 	ELSE RETURN "HAIR_NG_O8" 	ENDIF BREAK
		CASE HME_NGMP_MAKEUP_EYE			IF bTitle RETURN "HAIR_NG_T9" 	ELSE RETURN "HAIR_NG_O9" 	ENDIF BREAK
		CASE HME_NGMP_MAKEUP_LIPSTICK		IF bTitle RETURN "HAIR_NG_T10" 	ELSE RETURN "HAIR_NG_O10" 	ENDIF BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

STRUCT NGMP_MENU_OPTION_DATA
	INT iCost
	TEXT_LABEL_15 tlLabel
	HEAD_OVERLAY_SLOT eHeadSlot
	INT iTexture
	FLOAT fBlend
	PED_COMP_NAME_ENUM eHairItem
	INT iType
	CLOTHES_UNLOCK_ITEMS eUnlockItem
	INT iUniqueHash
ENDSTRUCT
PROC FILL_NGMP_MENU_OPTION_DATA_PED_COMP(NGMP_MENU_OPTION_DATA &sOptionData, INT iCost, STRING tlLabel, PED_COMP_NAME_ENUM eHairItem, INT iUniqueHash)
	sOptionData.iType = 0
	
	sOptionData.iCost = iCost
	sOptionData.tlLabel = tlLabel
	sOptionData.eHairItem = eHairItem
	
	sOptionData.iTexture = ENUM_TO_INT(eHairItem)
	
	sOptionData.eUnlockItem = DUMMY_CLOTHES_UNLOCK
	sOptionData.iUniqueHash = iUniqueHash
ENDPROC
PROC FILL_NGMP_MENU_OPTION_DATA_OVERLAY(NGMP_MENU_OPTION_DATA &sOptionData, INT iCost, STRING tlLabel, HEAD_OVERLAY_SLOT eHeadSlot, INT iTexture, FLOAT fBlend, CLOTHES_UNLOCK_ITEMS eUnlockItem, INT iUniqueHash)
	
	sOptionData.iType = 1
	IF (eHeadSlot = HOS_BASE_DETAIL)
		sOptionData.iType = 2
	ENDIF
	
	sOptionData.iCost = iCost
	sOptionData.tlLabel = tlLabel
	sOptionData.eHeadSlot = eHeadSlot
	sOptionData.iTexture = iTexture
	sOptionData.fBlend = fBlend
	
	sOptionData.eUnlockItem = eUnlockItem
	sOptionData.iUniqueHash = iUniqueHash
ENDPROC


CONST_INT MAX_M_NGMP_HAIR_COUNT				43
CONST_INT MAX_M_NGMP_BEARD_COUNT			30
CONST_INT MAX_M_NGMP_EYEBROWS_COUNT			35
CONST_INT MAX_M_NGMP_CHEST_COUNT			18
CONST_INT MAX_M_NGMP_CONTACTS_COUNT			32
CONST_INT MAX_M_NGMP_FACEPAINT_COUNT		93
CONST_INT MAX_M_NGMP_MAKEUP_EYE_COUNT		26
CONST_INT MAX_M_NGMP_MAKEUP_LIPSTICK_COUNT	11

CONST_INT MAX_F_NGMP_HAIR_COUNT				45
CONST_INT MAX_F_NGMP_EYEBROWS_COUNT			35
CONST_INT MAX_F_NGMP_CONTACTS_COUNT			32
CONST_INT MAX_F_NGMP_FACEPAINT_COUNT		93
CONST_INT MAX_F_NGMP_MAKEUP_BLUSHER_COUNT	8
CONST_INT MAX_F_NGMP_MAKEUP_EYE_COUNT		26
CONST_INT MAX_F_NGMP_MAKEUP_LIPSTICK_COUNT	11

FUNC INT GET_NUMBER_OF_NGMP_MENU_OPTIONS(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu)
	IF ePedModel = MP_M_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR				RETURN MAX_M_NGMP_HAIR_COUNT BREAK
			CASE HME_NGMP_BEARD				RETURN MAX_M_NGMP_BEARD_COUNT BREAK
			CASE HME_NGMP_EYEBROWS			RETURN MAX_M_NGMP_EYEBROWS_COUNT BREAK
			CASE HME_NGMP_CHEST				RETURN MAX_M_NGMP_CHEST_COUNT BREAK
			CASE HME_NGMP_CONTACTS 			RETURN MAX_M_NGMP_CONTACTS_COUNT BREAK
			CASE HME_NGMP_FACEPAINT			RETURN MAX_M_NGMP_FACEPAINT_COUNT BREAK
			CASE HME_NGMP_MAKEUP_EYE		RETURN MAX_M_NGMP_MAKEUP_EYE_COUNT BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK	RETURN MAX_M_NGMP_MAKEUP_LIPSTICK_COUNT BREAK
		ENDSWITCH
	ELIF ePedModel = MP_F_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR				RETURN MAX_F_NGMP_HAIR_COUNT BREAK
			CASE HME_NGMP_EYEBROWS			RETURN MAX_F_NGMP_EYEBROWS_COUNT BREAK
			CASE HME_NGMP_CONTACTS 			RETURN MAX_F_NGMP_CONTACTS_COUNT BREAK
			CASE HME_NGMP_FACEPAINT			RETURN MAX_F_NGMP_FACEPAINT_COUNT BREAK
			CASE HME_NGMP_MAKEUP_BLUSHER	RETURN MAX_F_NGMP_MAKEUP_BLUSHER_COUNT BREAK
			CASE HME_NGMP_MAKEUP_EYE		RETURN MAX_F_NGMP_MAKEUP_EYE_COUNT BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK	RETURN MAX_F_NGMP_MAKEUP_LIPSTICK_COUNT BREAK
		ENDSWITCH
	ENDIF
	RETURN 0
ENDFUNC

FUNC STRING GET_HAIR_LABEL_FROM_LABEL(STRING tlLabel)
	SWITCH GET_HASH_KEY(tlLabel)
		CASE HASH("CLO_BBM_H_00")		RETURN "CC_M_HS_16"		// Shaggy Curls
		CASE HASH("CLO_BBM_H_05")		RETURN "CC_M_HS_17"		// Surfer Dude
		CASE HASH("CLO_BUS_H_0_0")		RETURN "CC_M_HS_18"		// Short Side Part
		CASE HASH("CLO_BUS_H_1_0")		RETURN "CC_M_HS_19"		// High Slicked Sides
		CASE HASH("CLO_HP_HR_0_0")		RETURN "CC_M_HS_20"		// Long Slicked
		CASE HASH("CLO_HP_HR_1_0")		RETURN "CC_M_HS_21"		// Hipster Youth
		CASE HASH("CLO_IND_H_0_0")		RETURN "CC_M_HS_22"		// Mullet
		
		CASE HASH("CLO_BBF_H_05")		RETURN "CC_F_HS_17"		// Messy Bun
		CASE HASH("CLO_BBF_H_00")		RETURN "CC_F_HS_16"		// Pin Up Girl
		CASE HASH("CLO_BUS_F_H_0_0")	RETURN "CC_F_HS_18"		// Tight Bun
		CASE HASH("CLO_BUS_F_H_1_0")	RETURN "CC_F_HS_19"		// Twisted Bob
		CASE HASH("CLO_VALF_H_0_0" )	RETURN "CC_F_HS_23"		// Flapper Bob
		CASE HASH("CLO_HP_F_HR_0_0")	RETURN "CC_F_HS_20"		// Big Bangs
		CASE HASH("CLO_HP_F_HR_1_0")	RETURN "CC_F_HS_21"		// Braided Top Knot
		CASE HASH("CLO_INDF_H_0_0")		RETURN "CC_F_HS_22"		// Mullet
	ENDSWITCH
	
	RETURN tlLabel
ENDFUNC

FUNC BOOL GET_NGMP_MENU_OPTION_DATA(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iOption, NGMP_MENU_OPTION_DATA &sOptionData, INT iVariant = 0)

	// Reset the data
	sOptionData.iCost = 0
	sOptionData.tlLabel = ""
	sOptionData.eHeadSlot = INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)
	sOptionData.iTexture = 0
	sOptionData.fBlend = 0.0

	IF ePedModel = MP_M_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR
				SWITCH iOption
					CASE 0  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(500)*g_sMPTunables.fhaircut_male_expenditure_tunable[0]),									"CC_M_HS_0", GET_MALE_HAIR(HAIR_FMM_0_0),																						HASH("NGMP_HAIR_M_0")) BREAK // Close Shave
					CASE 1  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(170)*g_sMPTunables.fhaircut_male_expenditure_tunable[5]),									"CC_M_HS_1", GET_MALE_HAIR(HAIR_FMM_1_0),																						HASH("NGMP_HAIR_M_1")) BREAK // Buzzcut
					CASE 2  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(560)*g_sMPTunables.fhaircut_male_expenditure_tunable[10]),									"CC_M_HS_2", GET_MALE_HAIR(HAIR_FMM_2_0),																						HASH("NGMP_HAIR_M_2")) BREAK // Faux Hawk
					CASE 3  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1080)*g_sMPTunables.fhaircut_male_expenditure_tunable[15]),									"CC_M_HS_3", GET_MALE_HAIR(HAIR_FMM_3_0),																						HASH("NGMP_HAIR_M_3")) BREAK // Hipster
					CASE 4  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(500)*g_sMPTunables.fhaircut_male_expenditure_tunable[20]),									"CC_M_HS_4", GET_MALE_HAIR(HAIR_FMM_4_0),																						HASH("NGMP_HAIR_M_4")) BREAK // Side Parting
					CASE 5  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(275)*g_sMPTunables.fhaircut_male_expenditure_tunable[25]),									"CC_M_HS_5", GET_MALE_HAIR(HAIR_FMM_5_0),																						HASH("NGMP_HAIR_M_5")) BREAK // Shorter Cut
					CASE 6  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(930)*g_sMPTunables.fhaircut_male_expenditure_tunable[30]),									"CC_M_HS_6", GET_MALE_HAIR(HAIR_FMM_6_0),																						HASH("NGMP_HAIR_M_6")) BREAK // Biker
					CASE 7  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(420)*g_sMPTunables.fhaircut_male_expenditure_tunable[35]),									"CC_M_HS_7", GET_MALE_HAIR(HAIR_FMM_7_0),																						HASH("NGMP_HAIR_M_7")) BREAK // Ponytail
					CASE 8  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1130)*g_sMPTunables.fhaircut_male_expenditure_tunable[40]),									"CC_M_HS_8", GET_MALE_HAIR(HAIR_FMM_8_0),																						HASH("NGMP_HAIR_M_8")) BREAK // Cornrows
					CASE 9  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(375)*g_sMPTunables.fhaircut_male_expenditure_tunable[45]),									"CC_M_HS_9", GET_MALE_HAIR(HAIR_FMM_9_0),																						HASH("NGMP_HAIR_M_9")) BREAK // Slicked
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(480)*g_sMPTunables.fhaircut_male_expenditure_tunable[50]),									"CC_M_HS_10", GET_MALE_HAIR(HAIR_FMM_10_0),																						HASH("NGMP_HAIR_M_10")) BREAK // Short Brushed
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1030)*g_sMPTunables.fhaircut_male_expenditure_tunable[55]),									"CC_M_HS_11", GET_MALE_HAIR(HAIR_FMM_11_0),																						HASH("NGMP_HAIR_M_11")) BREAK // Spikey
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(530)*g_sMPTunables.fhaircut_male_expenditure_tunable[60]),									"CC_M_HS_12", GET_MALE_HAIR(HAIR_FMM_12_0),																						HASH("NGMP_HAIR_M_12")) BREAK // Caesar
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1180)*g_sMPTunables.fhaircut_male_expenditure_tunable[65]),									"CC_M_HS_13", GET_MALE_HAIR(HAIR_FMM_13_0),																						HASH("NGMP_HAIR_M_13")) BREAK // Chopped
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1980)*g_sMPTunables.fhaircut_male_expenditure_tunable[70]),									"CC_M_HS_14", GET_MALE_HAIR(HAIR_FMM_14_0),																						HASH("NGMP_HAIR_M_14")) BREAK // Dreads
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(455)*g_sMPTunables.fhaircut_male_expenditure_tunable[75]),									"CC_M_HS_15", GET_MALE_HAIR(HAIR_FMM_15_0),																						HASH("NGMP_HAIR_M_15")) BREAK // Long Hair
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_MALE_HAIR_Shaggy_Curls_Black),															"CC_M_HS_16", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BEACH_M_HAIR00"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_16")) BREAK // Shaggy Curls
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_MALE_HAIR_Surfer_Dude_Black),															"CC_M_HS_17", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BEACH_M_HAIR05"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_17")) BREAK // Surfer Dude
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_MALE_HAIR_Short_Side_Part_Black),														"CC_M_HS_18", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BUSI_M_HAIR0_0"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_18")) BREAK // Short Side Part
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_MALE_HAIR_High_Slicked_Sides_Black),													"CC_M_HS_19", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BUSI_M_HAIR1_0"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_19")) BREAK // High Slicked Sides
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[14]),												"CC_M_HS_20", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_HIPS_M_HAIR0_0"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_20")) BREAK // Long Slicked
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[19]),												"CC_M_HS_21", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_HIPS_M_HAIR1_0"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_21")) BREAK // Hipster Youth
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(4960)*g_sMPTunables.fHair_Makeup_IndependenceDay_Group),									"CC_M_HS_22", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_IND_M_HAIR0_0"), COMP_TYPE_HAIR, 3)),		HASH("NGMP_HAIR_M_22")) BREAK // Mullet
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Classic_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),	"CLO_S1M_H_0_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_M_HAIR0_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_23")) BREAK // Classic Cornrows
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Palm_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),		"CLO_S1M_H_1_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_M_HAIR1_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_24")) BREAK // Palm Cornrows
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Lightning_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),	"CLO_S1M_H_2_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_M_HAIR2_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_25")) BREAK // Lightning Cornrows
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Whipped_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),	"CLO_S1M_H_3_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_M_HAIR3_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_26")) BREAK // Whipped Cornrows
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Zig_Zag_Cornrows*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_S2M_H_0_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW2_M_HAIR0_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_27")) BREAK // Zig Zag Cornrows
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Snail_Cornrows*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_S2M_H_1_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW2_M_HAIR1_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_28")) BREAK // Snail Cornrows
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iMale_Hair_Hightop*g_sMPTunables.fMale_and_Female_Hair_All_Hair),						"CLO_S2M_H_2_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW2_M_HAIR2_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_29")) BREAK // Hightop
					CASE 30 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Loose_Swept_Back*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_BIM_H_0_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_M_HAIR_0_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_30")) BREAK
					CASE 31 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Undercut_Swept_Back*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_BIM_H_1_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_M_HAIR_1_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_31")) BREAK
					CASE 32 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Undercut_Swept_Side*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_BIM_H_2_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_M_HAIR_2_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_32")) BREAK
					CASE 33 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Spiked_Mohawk*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_BIM_H_3_0", GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_M_HAIR_3_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_33")) BREAK
					CASE 34 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iHair_Mod*g_sMPTunables.fMale_and_Female_Hair_All_Hair),								"CLO_BIM_H_4_0",  GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_M_HAIR_4_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_34")) BREAK
					CASE 35 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iHair_Layered_Mod*g_sMPTunables.fMale_and_Female_Hair_All_Hair),						"CLO_BIM_H_5_0",  GET_MALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_M_HAIR_5_0"), COMP_TYPE_HAIR, 3)),	HASH("NGMP_HAIR_M_35")) BREAK
					CASE 36 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iGR_MALE_HAIR_FLATTOP*g_sMPTunables.fMale_and_Female_Hair_All_Hair),					"CLO_GRM_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_GR_M_HAIR_35_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_36")) BREAK	// Flattop
					CASE 37 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iGR_MALE_HAIR_MILITARY_BUZZCUT*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_GRM_H_1_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_GR_M_HAIR_36_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_37")) BREAK	// Military Buzzcut
					CASE 38 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(0*g_sMPTunables.fMale_and_Female_Hair_All_Hair),														"CLO_VWM_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_VWD_M_HAIR_0_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_38")) BREAK	// Impotent Rage Hair
					CASE 39 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iTUNER_HAIR_AFRO_FADED*g_sMPTunables.fMale_and_Female_Hair_All_Hair),					"CLO_TRM_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_TUNER_M_HAIR_0_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_39")) BREAK	// Afro Faded
					#IF FEATURE_FIXER
					CASE 40 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFIXER_HAIRSTYLE_TOP_KNOT_FADE*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_FXM_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_FIXER_M_HAIR_0_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_40")) BREAK	// Top Knot
					#ENDIF
					#IF FEATURE_DLC_1_2022
					CASE 41 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iSU22_HAIRSTYLE_TWO_BLOCK_M*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_SBM_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_SUM2_M_HAIR_0_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_41")) BREAK	// Two Block
					CASE 42 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iSU22_HAIRSTYLE_SHAGGY_MULLET_M*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_SBM_H_1_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_SUM2_M_HAIR_1_0"), COMP_TYPE_HAIR, 3),	HASH("NGMP_HAIR_M_42")) BREAK	// Shaggy Mullet
					#ENDIF
					
					CASE MAX_M_NGMP_HAIR_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_BEARD
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(500)*g_sMPTunables.fbeard_unlocks_cleanshaven_expenditure_tunable),						"HAIR_BEARD0",	HOS_FACIAL_HAIR,	-1,	1.0,	BEARD_UNLOCKS_CLEANSHAVEN,						HASH("NGMP_BEARD_M_0")) BREAK // Clean Shaven
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(150)*g_sMPTunables.fbeard_unlocks_lightstubble_expenditure_tunable),						"HAIR_BEARD1",	HOS_FACIAL_HAIR,	0,	1.0,	BEARD_UNLOCKS_LIGHTSTUBBLE,						HASH("NGMP_BEARD_M_1")) BREAK // Light Stubble
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(430)*g_sMPTunables.fbeard_unlocks_balbo_expenditure_tunable),						 	"HAIR_BEARD2",	HOS_FACIAL_HAIR,	1,	1.0,	BEARD_UNLOCKS_BALBO,							HASH("NGMP_BEARD_M_2")) BREAK // Balbo
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(325)*g_sMPTunables.fbeard_unlocks_circlebeard_expenditure_tunable),						"HAIR_BEARD3",	HOS_FACIAL_HAIR,	2,	1.0,	BEARD_UNLOCKS_CIRCLEBEARD,						HASH("NGMP_BEARD_M_3")) BREAK // Circle Beard
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(465)*g_sMPTunables.fbeard_unlocks_goatee_expenditure_tunable),						 	"HAIR_BEARD4",	HOS_FACIAL_HAIR,	3,	1.0,	BEARD_UNLOCKS_GOATEE,							HASH("NGMP_BEARD_M_4")) BREAK // Goatee
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(500)*g_sMPTunables.fbeard_unlocks_chin_expenditure_tunable),								"HAIR_BEARD5",	HOS_FACIAL_HAIR,	4,	1.0,	BEARD_UNLOCKS_CHIN,								HASH("NGMP_BEARD_M_5")) BREAK // Chin
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(350)*g_sMPTunables.fbeard_unlocks_soulpatch_expenditure_tunable),					 	"HAIR_BEARD6",	HOS_FACIAL_HAIR,	5,	1.0,	BEARD_UNLOCKS_SOULPATCH,						HASH("NGMP_BEARD_M_6")) BREAK // Chin Fuzz
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(600)*g_sMPTunables.fbeard_unlocks_pencilchinstrap_expenditure_tunable),					"HAIR_BEARD7",	HOS_FACIAL_HAIR,	6,	1.0,	BEARD_UNLOCKS_PENCILCHINSTRAP,					HASH("NGMP_BEARD_M_7")) BREAK // Pencil Chin Strap
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(220)*g_sMPTunables.fbeard_unlocks_lightbeard_expenditure_tunable),						"HAIR_BEARD8",	HOS_FACIAL_HAIR,	7,	1.0,	BEARD_UNLOCKS_LIGHTBEARD,						HASH("NGMP_BEARD_M_8")) BREAK // Scruffy
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1425)*g_sMPTunables.fbeard_unlocks_musketeer_expenditure_tunable),						"HAIR_BEARD9",	HOS_FACIAL_HAIR,	8,	1.0,	BEARD_UNLOCKS_MUSKETEER,						HASH("NGMP_BEARD_M_9")) BREAK // Musketeer
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1700)*g_sMPTunables.fbeard_unlocks_moustache_expenditure_tunable),						"HAIR_BEARD10",	HOS_FACIAL_HAIR,	9,	1.0,	BEARD_UNLOCKS_MOUSTACHE,						HASH("NGMP_BEARD_M_10")) BREAK // Mustache
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(290)*g_sMPTunables.fbeard_unlocks_heavybeard_expenditure_tunable),						"HAIR_BEARD11",	HOS_FACIAL_HAIR,	10,	1.0,	BEARD_UNLOCKS_HEAVYBEARD,						HASH("NGMP_BEARD_M_11")) BREAK // Trimmed Beard
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(185)*g_sMPTunables.fbeard_unlocks_stubble_expenditure_tunable),						 	"HAIR_BEARD12",	HOS_FACIAL_HAIR,	11,	1.0,	BEARD_UNLOCKS_STUBBLE,							HASH("NGMP_BEARD_M_12")) BREAK // Stubble
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(255)*g_sMPTunables.fbeard_unlocks_circlebeard2_expenditure_tunable),					 	"HAIR_BEARD13",	HOS_FACIAL_HAIR,	12,	1.0,	BEARD_UNLOCKS_CIRCLEBEARD2,						HASH("NGMP_BEARD_M_13")) BREAK // Thin Circle Beard
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1150)*g_sMPTunables.fbeard_unlocks_horseshoeandsideburns_expenditure_tunable),	 		"HAIR_BEARD14",	HOS_FACIAL_HAIR,	13,	1.0,	BEARD_UNLOCKS_HORSESHOEANDSIDEBURNS,		 	HASH("NGMP_BEARD_M_14")) BREAK // Horseshoe
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1975)*g_sMPTunables.fbeard_unlocks_pencilmoustacheandmuttonchops_expenditure_tunable),	"HAIR_BEARD15",	HOS_FACIAL_HAIR,	14,	1.0,	BEARD_UNLOCKS_PENCILMOUSTACHEANDMUTTONCHOPS,	HASH("NGMP_BEARD_M_15")) BREAK // Pencil and 'Chops
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(875)*g_sMPTunables.fbeard_unlocks_pencilmoustacheandchinstrap_expenditure_tunable),	 	"HAIR_BEARD16",	HOS_FACIAL_HAIR,	15,	1.0,	BEARD_UNLOCKS_PENCILMOUSTACHEANDCHINSTRAP,		HASH("NGMP_BEARD_M_16")) BREAK // Chin Strap Beard
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(2000)*g_sMPTunables.fbeard_unlocks_balboanddesignsideburns_expenditure_tunable),	 		"HAIR_BEARD17",	HOS_FACIAL_HAIR,	16,	1.0,	BEARD_UNLOCKS_BALBOANDDESIGNSIDEBURNS,			HASH("NGMP_BEARD_M_17")) BREAK // Balbo and Sideburns
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(725)*g_sMPTunables.fbeard_unlocks_muttonchops_expenditure_tunable),	 					"HAIR_BEARD18",	HOS_FACIAL_HAIR,	17,	1.0,	BEARD_UNLOCKS_MUTTONCHOPS,						HASH("NGMP_BEARD_M_18")) BREAK // Trimmed Sideburns
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(395)*g_sMPTunables.fbeard_unlocks_fullbeard_expenditure_tunable),					 	"HAIR_BEARD19",	HOS_FACIAL_HAIR,	18,	1.0,	BEARD_UNLOCKS_FULLBEARD,						HASH("NGMP_BEARD_M_19")) BREAK // Scruffy Beard
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_Curly),																	"BRD_HP_0",		HOS_FACIAL_HAIR,	19,	1.0,	BEARD_UNLOCKS_CURLY,							HASH("NGMP_BEARD_M_20")) BREAK // Curly
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_CurlyDeepStranger),														"BRD_HP_1",		HOS_FACIAL_HAIR,	20,	1.0,	BEARD_UNLOCKS_CURLY_AND_DEEP_STRANGER,	 		HASH("NGMP_BEARD_M_21")) BREAK // Curly & Deep Stranger
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_Handlebar),																"BRD_HP_2",		HOS_FACIAL_HAIR,	21,	1.0,	BEARD_UNLOCKS_HANDLEBAR,						HASH("NGMP_BEARD_M_22")) BREAK // Handlebar
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_Faustic),																"BRD_HP_3",		HOS_FACIAL_HAIR,	22,	1.0,	BEARD_UNLOCKS_FAUSTIC,							HASH("NGMP_BEARD_M_23")) BREAK // Faustic
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_OttoPatch),																"BRD_HP_4",		HOS_FACIAL_HAIR,	23,	1.0,	BEARD_UNLOCKS_OTTO_AND_PATCH,					HASH("NGMP_BEARD_M_24")) BREAK // Otto & Patch
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_OttoFullStranger),														"BRD_HP_5",		HOS_FACIAL_HAIR,	24,	1.0,	BEARD_UNLOCKS_OTTO_AND_FULL_STRANGER,			HASH("NGMP_BEARD_M_25")) BREAK // Otto & Full Stranger
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_LightFranz),																"BRD_HP_6",		HOS_FACIAL_HAIR,	25,	1.0,	BEARD_UNLOCKS_LIGHT_FRANZ,						HASH("NGMP_BEARD_M_26")) BREAK // Light Franz
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_Hampstead),																"BRD_HP_7",		HOS_FACIAL_HAIR,	26,	1.0,	BEARD_UNLOCKS_THE_HAMPSTEAD,					HASH("NGMP_BEARD_M_27")) BREAK // The Hampstead
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_Ambrose),																"BRD_HP_8",		HOS_FACIAL_HAIR,	27,	1.0,	BEARD_UNLOCKS_THE_AMBROSE,						HASH("NGMP_BEARD_M_28")) BREAK // The Ambrose
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iDlc_BeardPrice_LincolnCurtain),															"BRD_HP_9",		HOS_FACIAL_HAIR,	28,	1.0,	BEARD_UNLOCKS_LINCOLN_CURTAIN,					HASH("NGMP_BEARD_M_29")) BREAK // Lincoln Curtain
					
					CASE MAX_M_NGMP_BEARD_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_EYEBROWS
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_None),	 																		"NONE",	 		HOS_EYEBROW,	 	-1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_0")) BREAK // None
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Balanced),																		"CC_EYEBRW_0", 	HOS_EYEBROW,	 	0,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_1")) BREAK // Balanced
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Fashion),																		"CC_EYEBRW_1", 	HOS_EYEBROW,	 	1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_2")) BREAK // Fashion
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Cleopatra),																	"CC_EYEBRW_2", 	HOS_EYEBROW,	 	2,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_3")) BREAK // Cleopatra
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Quizzical),																	"CC_EYEBRW_3", 	HOS_EYEBROW,	 	3,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_4")) BREAK // Quizzical
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Femme),																		"CC_EYEBRW_4", 	HOS_EYEBROW,	 	4,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_5")) BREAK // Femme
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Seductive),																	"CC_EYEBRW_5", 	HOS_EYEBROW,	 	5,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_6")) BREAK // Seductive
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Pinched),																		"CC_EYEBRW_6", 	HOS_EYEBROW,	 	6,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_7")) BREAK // Pinched
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Chola),																		"CC_EYEBRW_7", 	HOS_EYEBROW,	 	7,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_8")) BREAK // Chola
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Triomphe),																		"CC_EYEBRW_8", 	HOS_EYEBROW,	 	8,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_9")) BREAK // Triomphe
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Carefree),																		"CC_EYEBRW_9", 	HOS_EYEBROW,	 	9,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_10")) BREAK // Carefree
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Curvaceous),																	"CC_EYEBRW_10", HOS_EYEBROW,	 	10,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_11")) BREAK // Curvaceous
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Rodent),																		"CC_EYEBRW_11", HOS_EYEBROW,	 	11,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_12")) BREAK // Rodent
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Double_Tram),																	"CC_EYEBRW_12", HOS_EYEBROW,	 	12,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_13")) BREAK // Double Tram
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Thin),																			"CC_EYEBRW_13", HOS_EYEBROW,	 	13,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_14")) BREAK // Thin
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Penciled),																		"CC_EYEBRW_14", HOS_EYEBROW,	 	14,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_15")) BREAK // Penciled
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Mother_Plucker),																"CC_EYEBRW_15", HOS_EYEBROW,	 	15,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_16")) BREAK // Mother Plucker
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Straight_and_Narrow),															"CC_EYEBRW_16", HOS_EYEBROW,	 	16,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_17")) BREAK // Straight and Narrow
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Natural),																		"CC_EYEBRW_17", HOS_EYEBROW,	 	17,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_18")) BREAK // Natural
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Fuzzy),																		"CC_EYEBRW_18", HOS_EYEBROW,	 	18,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_19")) BREAK // Fuzzy
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Unkempt),																		"CC_EYEBRW_19", HOS_EYEBROW,	 	19,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_20")) BREAK // Unkempt
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Caterpillar),																	"CC_EYEBRW_20", HOS_EYEBROW,	 	20,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_21")) BREAK // Caterpillar
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Regular),																		"CC_EYEBRW_21", HOS_EYEBROW,	 	21,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_22")) BREAK // Regular
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Mediterranean),																"CC_EYEBRW_22", HOS_EYEBROW,	 	22,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_23")) BREAK // Mediterranean
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Groomed),																		"CC_EYEBRW_23", HOS_EYEBROW,	 	23,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_24")) BREAK // Groomed
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Bushels),																		"CC_EYEBRW_24", HOS_EYEBROW,	 	24,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_25")) BREAK // Bushels
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Feathered),																	"CC_EYEBRW_25", HOS_EYEBROW,	 	25,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_26")) BREAK // Feathered
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Prickly),																		"CC_EYEBRW_26", HOS_EYEBROW,	 	26,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_27")) BREAK // Prickly
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Monobrow),																		"CC_EYEBRW_27", HOS_EYEBROW,	 	27,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_28")) BREAK // Monobrow
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Winged),																		"CC_EYEBRW_28", HOS_EYEBROW,	 	28,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_29")) BREAK // Winged
					CASE 30 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Triple_Tram),																	"CC_EYEBRW_29", HOS_EYEBROW,	 	29,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_30")) BREAK // Triple Tram
					CASE 31 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Arched_Tram),																	"CC_EYEBRW_30", HOS_EYEBROW,	 	30,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_31")) BREAK // Arched Tram
					CASE 32 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Cutouts),																		"CC_EYEBRW_31", HOS_EYEBROW,	 	31,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_32")) BREAK // Cutouts
					CASE 33 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Fade_Away),																	"CC_EYEBRW_32", HOS_EYEBROW,	 	32,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_33")) BREAK // Fade Away
					CASE 34 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Solo_Tram),																	"CC_EYEBRW_33", HOS_EYEBROW,	 	33,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_M_34")) BREAK // Solo Tram
					
					CASE MAX_M_NGMP_EYEBROWS_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_CHEST
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Shaved),															"CC_BODY_1_0",	HOS_BODY_1,			-1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_0")) BREAK // Shaved
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Natural),															"CC_BODY_1_1",	HOS_BODY_1,			0,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_1")) BREAK // Natural
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_The_Strip),														"CC_BODY_1_2",	HOS_BODY_1,			1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_2")) BREAK // The Strip
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_The_Tree),														"CC_BODY_1_3",	HOS_BODY_1,			2,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_3")) BREAK // The Tree
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Hairy),															"CC_BODY_1_4",	HOS_BODY_1,			3,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_4")) BREAK // Hairy
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Grisly),															"CC_BODY_1_5",	HOS_BODY_1,			4,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_5")) BREAK // Grisly
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Ape),																"CC_BODY_1_6",	HOS_BODY_1,			5,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_6")) BREAK // Ape
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Groomed_Ape),														"CC_BODY_1_7",	HOS_BODY_1,			6,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_7")) BREAK // Groomed Ape
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Bikini),															"CC_BODY_1_8",	HOS_BODY_1,			7,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_8")) BREAK // Bikini
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Lightning_Bolt),													"CC_BODY_1_9",	HOS_BODY_1,			8,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_9")) BREAK // Lightning Bolt
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Reverse_Lightning),												"CC_BODY_1_10",	HOS_BODY_1,			9,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_10")) BREAK // Reverse Lightning
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Love_Heart),														"CC_BODY_1_11",	HOS_BODY_1,			10,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_11")) BREAK // Love Heart
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Chestache),														"CC_BODY_1_12",	HOS_BODY_1,			11,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_12")) BREAK // Chestache
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Happy_Face),														"CC_BODY_1_13",	HOS_BODY_1,			12,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_13")) BREAK // Happy Face
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Skull),															"CC_BODY_1_14",	HOS_BODY_1,			13,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_14")) BREAK // Skull
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Snail_Trail),														"CC_BODY_1_15",	HOS_BODY_1,			14,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_15")) BREAK // Snail Trail
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iChest_Hair_Chest_Hair_Slug_and_Nips),													"CC_BODY_1_16",	HOS_BODY_1,			15,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_16")) BREAK // Slug and Nips
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(1100),																									"CC_BODY_1_17",	HOS_BODY_1,			16,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CHEST_M_17")) BREAK // Hairy Arms
					
					CASE MAX_M_NGMP_CHEST_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_CONTACTS
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Green),																		"FACE_E_C_0",	HOS_BASE_DETAIL,	0,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_0")) BREAK // Green
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Emerald),																		"FACE_E_C_1",	HOS_BASE_DETAIL,	1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_1")) BREAK // Emerald
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Light_Blue),																	"FACE_E_C_2",	HOS_BASE_DETAIL,	2,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_2")) BREAK // Light Blue 
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Ocean_Blue),																	"FACE_E_C_3",	HOS_BASE_DETAIL,	3,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_3")) BREAK // Ocean Blue
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Light_Brown),																	"FACE_E_C_4",	HOS_BASE_DETAIL,	4,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_4")) BREAK // Light Brown
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Dark_Brown),																	"FACE_E_C_5",	HOS_BASE_DETAIL,	5,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_5")) BREAK // Dark Brown 
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Hazel),																		"FACE_E_C_6",	HOS_BASE_DETAIL,	6,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_6")) BREAK // Hazel
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Dark_Grey),																	"FACE_E_C_7",	HOS_BASE_DETAIL,	7,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_7")) BREAK // Dark Grey
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Light_Grey),																	"FACE_E_C_8",	HOS_BASE_DETAIL,	8,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_8")) BREAK // Light Grey
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Pink),																			"FACE_E_C_9",	HOS_BASE_DETAIL,	9,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_9")) BREAK // Pink
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Yellow),																		"FACE_E_C_10",	HOS_BASE_DETAIL,	10,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_10")) BREAK // Yellow
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Purple),																		"FACE_E_C_11",	HOS_BASE_DETAIL,	11,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_11")) BREAK // Purple
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Blackout),																		"FACE_E_C_12",	HOS_BASE_DETAIL,	12,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_12")) BREAK // Blackout
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Shades_of_Gray),																"FACE_E_C_13",	HOS_BASE_DETAIL,	13,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_13")) BREAK // Shades of Gray
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Tequila_Sunrise),																"FACE_E_C_14",	HOS_BASE_DETAIL,	14,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_14")) BREAK // Tequila Sunrise
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Atomic),																		"FACE_E_C_15",	HOS_BASE_DETAIL,	15,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_15")) BREAK // Atomic
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Warp),																			"FACE_E_C_16",	HOS_BASE_DETAIL,	16,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_16")) BREAK // Warp
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Ecola),																		"FACE_E_C_17",	HOS_BASE_DETAIL,	17,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_17")) BREAK // Ecola
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Space_Ranger),																	"FACE_E_C_18",	HOS_BASE_DETAIL,	18,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_18")) BREAK // Space Ranger
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Ying_Yang),																	"FACE_E_C_19",	HOS_BASE_DETAIL,	19,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_19")) BREAK // Ying Yang
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Bullseye),																		"FACE_E_C_20",	HOS_BASE_DETAIL,	20,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_20")) BREAK // Bullseye
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Lizard),																		"FACE_E_C_21",	HOS_BASE_DETAIL,	21,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_21")) BREAK // Lizard
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Dragon),																		"FACE_E_C_22",	HOS_BASE_DETAIL,	22,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_22")) BREAK // Dragon
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Extra_Terrestrial),															"FACE_E_C_23",	HOS_BASE_DETAIL,	23,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_23")) BREAK // Extra Terrestrial 
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Goat),																			"FACE_E_C_24",	HOS_BASE_DETAIL,	24,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_24")) BREAK // Goat
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Smiley),																		"FACE_E_C_25",	HOS_BASE_DETAIL,	25,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_25")) BREAK // Smiley
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Possessed),																	"FACE_E_C_26",	HOS_BASE_DETAIL,	26,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_26")) BREAK // Possessed
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Demon),																		"FACE_E_C_27",	HOS_BASE_DETAIL,	27,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_27")) BREAK // Demon
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Infected),																		"FACE_E_C_28",	HOS_BASE_DETAIL,	28,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_28")) BREAK // Infected 
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Alien),																		"FACE_E_C_29",	HOS_BASE_DETAIL,	29,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_29")) BREAK // Alien
					CASE 30 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Undead),																		"FACE_E_C_30",	HOS_BASE_DETAIL,	30,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_30")) BREAK // Undead
					CASE 31 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Zombie),																		"FACE_E_C_31",	HOS_BASE_DETAIL,	31,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_M_31")) BREAK // Zombie
					
					CASE MAX_M_NGMP_CONTACTS_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_FACEPAINT
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(100)*g_sMPTunables.fmakeup_unlocks_basic_expenditure_tunable),							"NONE",			HOS_MAKEUP,			-1,	1.0,	MAKEUP_UNLOCKS_BASIC,				HASH("NGMP_FACEPAINT_M_0")) BREAK // "None"
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(500)*g_sMPTunables.fmakeup_unlocks_kissmyaxe_expenditure_tunable),						"CC_MKUP_16",	HOS_MAKEUP,			16,	1.0,	MAKEUP_UNLOCKS_KISSMYAXE,			HASH("NGMP_FACEPAINT_M_1")) BREAK // Kiss My Axe
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1000)*g_sMPTunables.fmakeup_unlocks_pandapussy_expenditure_tunable),						"CC_MKUP_17",	HOS_MAKEUP,			17,	1.0,	MAKEUP_UNLOCKS_PANDAPUSSY,			HASH("NGMP_FACEPAINT_M_2")) BREAK // Panda Pussy
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(750)*g_sMPTunables.fmakeup_unlocks_thebat_expenditure_tunable),							"CC_MKUP_18",	HOS_MAKEUP,			18,	1.0,	MAKEUP_UNLOCKS_THEBAT,				HASH("NGMP_FACEPAINT_M_3")) BREAK // The Bat
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(5750)*g_sMPTunables.fmakeup_unlocks_skullinscarlet_expenditure_tunable),					"CC_MKUP_19",	HOS_MAKEUP,			19,	1.0,	MAKEUP_UNLOCKS_SKULLINSCARLET,		HASH("NGMP_FACEPAINT_M_4")) BREAK // Skull in Scarlet
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(10000)*g_sMPTunables.fmakeup_unlocks_serpentine_expenditure_tunable),					"CC_MKUP_20",	HOS_MAKEUP,			20,	1.0,	MAKEUP_UNLOCKS_SERPENTINE,			HASH("NGMP_FACEPAINT_M_5")) BREAK // Serpentine
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(6000)*g_sMPTunables.fmakeup_unlocks_theveldt_expenditure_tunable),						"CC_MKUP_21",	HOS_MAKEUP,			21,	1.0,	MAKEUP_UNLOCKS_THEVELDT,			HASH("NGMP_FACEPAINT_M_6")) BREAK // The Veldt
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(5000)*g_sMPTunables.fmakeup_unlocks_triballines_expenditure_tunable),					"CC_MKUP_26",	HOS_MAKEUP,			26,	1.0,	MAKEUP_UNLOCKS_TRIBALLINES,			HASH("NGMP_FACEPAINT_M_7")) BREAK // Tribal Lines
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(5500)*g_sMPTunables.fmakeup_unlocks_tribalswirls_expenditure_tunable),					"CC_MKUP_27",	HOS_MAKEUP,			27,	1.0,	MAKEUP_UNLOCKS_TRIBALSWIRLS,		HASH("NGMP_FACEPAINT_M_8")) BREAK // Tribal Swirls
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1250)*g_sMPTunables.fmakeup_unlocks_tribalorange_expenditure_tunable),					"CC_MKUP_28",	HOS_MAKEUP,			28,	1.0,	MAKEUP_UNLOCKS_TRIBALORANGE,		HASH("NGMP_FACEPAINT_M_9")) BREAK // Tribal Orange
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(2000)*g_sMPTunables.fmakeup_unlocks_tribalred_expenditure_tunable),						"CC_MKUP_29",	HOS_MAKEUP,			29,	1.0,	MAKEUP_UNLOCKS_TRIBALRED,			HASH("NGMP_FACEPAINT_M_10")) BREAK // Tribal Red
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(15000)*g_sMPTunables.fmakeup_unlocks_trappedinabox_expenditure_tunable),					"CC_MKUP_30",	HOS_MAKEUP,			30,	1.0,	MAKEUP_UNLOCKS_TRAPPEDINABOX,		HASH("NGMP_FACEPAINT_M_11")) BREAK // Trapped in a Box
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(20000)*g_sMPTunables.fmakeup_unlocks_clowning_expenditure_tunable),						"CC_MKUP_31",	HOS_MAKEUP,			31,	1.0,	MAKEUP_UNLOCKS_CLOWNING,			HASH("NGMP_FACEPAINT_M_12")) BREAK // Clowning
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(25000)*g_sMPTunables.fHair_Makeup_IndependenceDay_Group),								"CC_MKUP_33",	HOS_MAKEUP,			33,	1.0,	MAKEUP_UNLOCKS_STARS_N_STRIPES,		HASH("NGMP_FACEPAINT_M_13")) BREAK // Stars n Stripes Face Paint
					CASE 14	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Shadow_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_42",	HOS_MAKEUP,			42, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_00,		HASH("NGMP_FACEPAINT_M_14")) BREAK // Shadow Demon
					CASE 15	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Fleshy_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_43",	HOS_MAKEUP,			43, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_01,		HASH("NGMP_FACEPAINT_M_15")) BREAK // Fleshy Demon
					CASE 16	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Flayed_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_44",	HOS_MAKEUP,			44, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_02,		HASH("NGMP_FACEPAINT_M_16")) BREAK // Flayed Demon
					CASE 17	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Sorrow_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_45",	HOS_MAKEUP,			45, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_03,		HASH("NGMP_FACEPAINT_M_17")) BREAK // Sorrow Demon
					CASE 18	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Smiler_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_46",	HOS_MAKEUP,			46, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_04,		HASH("NGMP_FACEPAINT_M_18")) BREAK // Smiler Demon
					CASE 19	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Cracked_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_47",	HOS_MAKEUP,			47, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_05,		HASH("NGMP_FACEPAINT_M_19")) BREAK // Cracked Demon
					CASE 20	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Danger_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_48",	HOS_MAKEUP,			48, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_06,		HASH("NGMP_FACEPAINT_M_20")) BREAK // Danger Skull
					CASE 21	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Wicked_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_49",	HOS_MAKEUP,			49, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_07,		HASH("NGMP_FACEPAINT_M_21")) BREAK // Wicked Skull
					CASE 22	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Menace_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_50",	HOS_MAKEUP,			50, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_08,		HASH("NGMP_FACEPAINT_M_22")) BREAK // Menace Skull
					CASE 23	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Bone_Jaw_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_51",	HOS_MAKEUP,			51, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_09,		HASH("NGMP_FACEPAINT_M_23")) BREAK // Bone Jaw Skull
					CASE 24	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Flesh_Jaw_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_52",	HOS_MAKEUP,			52, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_10,		HASH("NGMP_FACEPAINT_M_24")) BREAK // Flesh Jaw Skull
					CASE 25	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Spirit_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_53",	HOS_MAKEUP,			53, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_11,		HASH("NGMP_FACEPAINT_M_25")) BREAK // Spirit Skull
					CASE 26	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Ghoul_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_54",	HOS_MAKEUP,			54, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_12,		HASH("NGMP_FACEPAINT_M_26")) BREAK // Ghoul Skull
					CASE 27	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Phantom_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_55",	HOS_MAKEUP,			55, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_13,		HASH("NGMP_FACEPAINT_M_27")) BREAK // Phantom Skull
					CASE 28	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Gnasher_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_56",	HOS_MAKEUP,			56, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_14,		HASH("NGMP_FACEPAINT_M_28")) BREAK // Gnasher Skull
					CASE 29	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Exposed_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_57",	HOS_MAKEUP,			57, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_15,		HASH("NGMP_FACEPAINT_M_29")) BREAK // Exposed Skull
					CASE 30	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Ghostly_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_58",	HOS_MAKEUP,			58, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_16,		HASH("NGMP_FACEPAINT_M_30")) BREAK // Ghostly Skull
					CASE 31	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Fury_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_59",	HOS_MAKEUP,			59, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_17,		HASH("NGMP_FACEPAINT_M_31")) BREAK // Fury Skull
					CASE 32	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Demi_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_60",	HOS_MAKEUP,			60, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_18,		HASH("NGMP_FACEPAINT_M_32")) BREAK // Demi Skull
					CASE 33	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Inbred_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_61",	HOS_MAKEUP,			61, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_19,		HASH("NGMP_FACEPAINT_M_33")) BREAK // Inbred Skull
					CASE 34	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Spooky_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_62",	HOS_MAKEUP,			62, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_20,		HASH("NGMP_FACEPAINT_M_34")) BREAK // Spooky Skull
					CASE 35	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Slashed_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),				"CC_MKUP_63",	HOS_MAKEUP,			63, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_21,		HASH("NGMP_FACEPAINT_M_35")) BREAK // Slashed Skull
					CASE 36	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Web_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_64",	HOS_MAKEUP,			64, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_22,		HASH("NGMP_FACEPAINT_M_36")) BREAK // Web Sugar Skull
					CASE 37	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_SeNor_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_65",	HOS_MAKEUP,			65, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_23,		HASH("NGMP_FACEPAINT_M_37")) BREAK // Senor Sugar Skull
					CASE 38	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Swirl_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_66",	HOS_MAKEUP,			66, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_24,		HASH("NGMP_FACEPAINT_M_38")) BREAK // Swirl Sugar Skull
					CASE 39	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Floral_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_67",	HOS_MAKEUP,			67, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_25,		HASH("NGMP_FACEPAINT_M_39")) BREAK // Floral Sugar Skull
					CASE 40	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Mono_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_68",	HOS_MAKEUP,			68, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_26,		HASH("NGMP_FACEPAINT_M_40")) BREAK // Mono Sugar Skull
					CASE 41	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Femme_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_69",	HOS_MAKEUP,			69, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_27,		HASH("NGMP_FACEPAINT_M_41")) BREAK // Femme Sugar Skull
					CASE 42	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Demi_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_70",	HOS_MAKEUP,			70, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_28,		HASH("NGMP_FACEPAINT_M_42")) BREAK // Demi Sugar Skull
					CASE 43	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Scarred_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_71",	HOS_MAKEUP,			71, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_29,		HASH("NGMP_FACEPAINT_M_43")) BREAK // Scarred Sugar Skull		
					CASE 44 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_WAVES_LEFT),																				"CC_BLUSH_7",	HOS_BLUSHER,		7,	1.0,	MAKEUP_UNLOCKS_GANGOPS_00,			HASH("NGMP_MAKEUP_BLUSHER_M_7")) BREAK	//	Waves Left
					CASE 45 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_WAVES_RIGHT),																				"CC_BLUSH_8",	HOS_BLUSHER,		8,	1.0,	MAKEUP_UNLOCKS_GANGOPS_01,			HASH("NGMP_MAKEUP_BLUSHER_M_8")) BREAK  //	Waves Right
					CASE 46 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_TOTEMIC),																					"CC_BLUSH_9",	HOS_BLUSHER,		9,	1.0,	MAKEUP_UNLOCKS_GANGOPS_02,			HASH("NGMP_MAKEUP_BLUSHER_M_9")) BREAK  //	Totemic
					CASE 47 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_STREAKS_LEFT),																				"CC_BLUSH_10",	HOS_BLUSHER,		10,	1.0,	MAKEUP_UNLOCKS_GANGOPS_03,			HASH("NGMP_MAKEUP_BLUSHER_M_10")) BREAK //	Streaks Left
					CASE 48 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_STREAKS_RIGHT),																			"CC_BLUSH_11",	HOS_BLUSHER,		11,	1.0,	MAKEUP_UNLOCKS_GANGOPS_04,			HASH("NGMP_MAKEUP_BLUSHER_M_11")) BREAK //	Streaks Right
					CASE 49 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BREAKUP_LEFT),																				"CC_BLUSH_12",	HOS_BLUSHER,		12,	1.0,	MAKEUP_UNLOCKS_GANGOPS_05,			HASH("NGMP_MAKEUP_BLUSHER_M_12")) BREAK //	Breakup Left
					CASE 50 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BREAKUP_RIGHT),																			"CC_BLUSH_13",	HOS_BLUSHER,		13,	1.0,	MAKEUP_UNLOCKS_GANGOPS_06,			HASH("NGMP_MAKEUP_BLUSHER_M_13")) BREAK //	Breakup Right
					CASE 51 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BLOTCH_LEFT),																				"CC_BLUSH_14",	HOS_BLUSHER,		14,	1.0,	MAKEUP_UNLOCKS_GANGOPS_07,			HASH("NGMP_MAKEUP_BLUSHER_M_14")) BREAK //	Blotch Left
					CASE 52 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BLOTCH_RIGHT),																				"CC_BLUSH_15",	HOS_BLUSHER,		15,	1.0,	MAKEUP_UNLOCKS_GANGOPS_08,			HASH("NGMP_MAKEUP_BLUSHER_M_15")) BREAK //	Blotch Right
					CASE 53 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_STRIPES),																					"CC_BLUSH_16",	HOS_BLUSHER,		16,	1.0,	MAKEUP_UNLOCKS_GANGOPS_09,			HASH("NGMP_MAKEUP_BLUSHER_M_16")) BREAK //	Stripes
					CASE 54 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_ELDER),																				"CC_BLUSH_17",	HOS_BLUSHER,		17,	1.0,	MAKEUP_UNLOCKS_GANGOPS_10,			HASH("NGMP_MAKEUP_BLUSHER_M_17")) BREAK //	The Elder
					CASE 55 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_VERTICAL_STRIPE),																			"CC_BLUSH_18",	HOS_BLUSHER,		18,	1.0,	MAKEUP_UNLOCKS_GANGOPS_11,			HASH("NGMP_MAKEUP_BLUSHER_M_18")) BREAK //	Vertical Stripe
					CASE 56 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_CLAN),																						"CC_BLUSH_19",	HOS_BLUSHER,		19,	1.0,	MAKEUP_UNLOCKS_GANGOPS_12,			HASH("NGMP_MAKEUP_BLUSHER_M_19")) BREAK //	Clan
					CASE 57 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_SPLATS_RIGHT),																				"CC_BLUSH_20",	HOS_BLUSHER,		20,	1.0,	MAKEUP_UNLOCKS_GANGOPS_13,			HASH("NGMP_MAKEUP_BLUSHER_M_20")) BREAK //	Splats Right
					CASE 58 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_SPLATS_LEFT),																				"CC_BLUSH_21",	HOS_BLUSHER,		21,	1.0,	MAKEUP_UNLOCKS_GANGOPS_14,			HASH("NGMP_MAKEUP_BLUSHER_M_21")) BREAK //	Splats Left
					CASE 59 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_PHOENIX),																				"CC_BLUSH_22",	HOS_BLUSHER,		22,	1.0,	MAKEUP_UNLOCKS_GANGOPS_15,			HASH("NGMP_MAKEUP_BLUSHER_M_22")) BREAK //	The Phoenix
					CASE 60 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_ANCESTRAL),																				"CC_BLUSH_23",	HOS_BLUSHER,		23,	1.0,	MAKEUP_UNLOCKS_GANGOPS_16,			HASH("NGMP_MAKEUP_BLUSHER_M_23")) BREAK //	Ancestral
					CASE 61 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_COVERAGE),																					"CC_BLUSH_24",	HOS_BLUSHER,		24,	1.0,	MAKEUP_UNLOCKS_GANGOPS_17,			HASH("NGMP_MAKEUP_BLUSHER_M_24")) BREAK //	Coverage
					CASE 62 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_SYMMETRY),																					"CC_BLUSH_25",	HOS_BLUSHER,		25,	1.0,	MAKEUP_UNLOCKS_GANGOPS_18,			HASH("NGMP_MAKEUP_BLUSHER_M_25")) BREAK //	Symmetry
					CASE 63 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_FINGERPRINTS),																				"CC_BLUSH_26",	HOS_BLUSHER,		26,	1.0,	MAKEUP_UNLOCKS_GANGOPS_19,			HASH("NGMP_MAKEUP_BLUSHER_M_26")) BREAK //	Fingerprints
					CASE 64 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BLOTCH_LOWER),																				"CC_BLUSH_27",	HOS_BLUSHER,		27,	1.0,	MAKEUP_UNLOCKS_GANGOPS_20,			HASH("NGMP_MAKEUP_BLUSHER_M_27")) BREAK //	Blotch Lower
					CASE 65 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_SPIRIT),																				"CC_BLUSH_28",	HOS_BLUSHER,		28,	1.0,	MAKEUP_UNLOCKS_GANGOPS_21,			HASH("NGMP_MAKEUP_BLUSHER_M_28")) BREAK //	The Spirit
					CASE 66 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_CHEEK_STRIPES),																			"CC_BLUSH_29",	HOS_BLUSHER,		29,	1.0,	MAKEUP_UNLOCKS_GANGOPS_22,			HASH("NGMP_MAKEUP_BLUSHER_M_29")) BREAK //	Cheek Stripes
					CASE 67 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_DEMON),																				"CC_BLUSH_30",	HOS_BLUSHER,		30,	1.0,	MAKEUP_UNLOCKS_GANGOPS_23,			HASH("NGMP_MAKEUP_BLUSHER_M_30")) BREAK //	The Demon
					CASE 68 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_MANDIBLE),																					"CC_BLUSH_31",	HOS_BLUSHER,		31,	1.0,	MAKEUP_UNLOCKS_GANGOPS_24,			HASH("NGMP_MAKEUP_BLUSHER_M_31")) BREAK //	Mandible
	  				CASE 69 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_FRONTAL),																					"CC_BLUSH_32",	HOS_BLUSHER,		32,	1.0,	MAKEUP_UNLOCKS_GANGOPS_25,			HASH("NGMP_MAKEUP_BLUSHER_M_32")) BREAK //	Frontal
					CASE 70 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_STRIPED_CAMO),																								"CC_MKUP_72",	HOS_MAKEUP,			72,	1.0,	MAKEUP_UNLOCKS_HEIST4_00,			HASH("NGMP_FACEPAINT_M_44")) BREAK //	Camo 1
					CASE 71 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_WOODLAND_CAMO),																							"CC_MKUP_73",	HOS_MAKEUP,			73,	1.0,	MAKEUP_UNLOCKS_HEIST4_01,			HASH("NGMP_FACEPAINT_M_45")) BREAK //	Camo 2
					CASE 72 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_HUNTER_CAMO),																								"CC_MKUP_74",	HOS_MAKEUP,			74,	1.0,	MAKEUP_UNLOCKS_HEIST4_02,			HASH("NGMP_FACEPAINT_M_46")) BREAK //	Camo 3
					CASE 73 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_HALF_CAMO),																								"CC_MKUP_75",	HOS_MAKEUP,			75,	1.0,	MAKEUP_UNLOCKS_HEIST4_03,			HASH("NGMP_FACEPAINT_M_47")) BREAK //	Camo 4
					CASE 74 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_BLOTCHY_CAMO),																								"CC_MKUP_76",	HOS_MAKEUP,			76,	1.0,	MAKEUP_UNLOCKS_HEIST4_04,			HASH("NGMP_FACEPAINT_M_48")) BREAK //	Camo 5
					CASE 75 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_STARFISH_CAMO),																							"CC_MKUP_77",	HOS_MAKEUP,			77,	1.0,	MAKEUP_UNLOCKS_HEIST4_05,			HASH("NGMP_FACEPAINT_M_49")) BREAK //	Camo 6
					CASE 76 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_GHOST_CAMO),																								"CC_MKUP_78",	HOS_MAKEUP,			78,	1.0,	MAKEUP_UNLOCKS_HEIST4_06,			HASH("NGMP_FACEPAINT_M_50")) BREAK //	Camo 7
					CASE 77 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_BLENDED_CAMO),																								"CC_MKUP_79",	HOS_MAKEUP,			79,	1.0,	MAKEUP_UNLOCKS_HEIST4_07,			HASH("NGMP_FACEPAINT_M_51")) BREAK //	Camo 8
					CASE 78 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_JUNGLE_CAMO),																								"CC_MKUP_80",	HOS_MAKEUP,			80,	1.0,	MAKEUP_UNLOCKS_HEIST4_08,			HASH("NGMP_FACEPAINT_M_52")) BREAK //	Camo 9
					#IF FEATURE_FIXER
					CASE 79 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_RED_CHEEK_LINES),																						"CC_MKUP_81",	HOS_MAKEUP,			81,	1.0,	MAKEUP_UNLOCKS_FIXER_00,			HASH("NGMP_FACEPAINT_M_53")) BREAK //	Red Cheek Lines
					CASE 80 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_GREEN_CHEEK_LINES),																						"CC_MKUP_82",	HOS_MAKEUP,			82,	1.0,	MAKEUP_UNLOCKS_FIXER_01,			HASH("NGMP_FACEPAINT_M_54")) BREAK //	Green Cheek Lines
					CASE 81 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_COOL_EYE_FLICKS),																						"CC_MKUP_83",	HOS_MAKEUP,			83,	1.0,	MAKEUP_UNLOCKS_FIXER_02,			HASH("NGMP_FACEPAINT_M_55")) BREAK //	Cool Eye Flicks
					CASE 82 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_HOT_EYE_FLICKS),																						"CC_MKUP_84",	HOS_MAKEUP,			84,	1.0,	MAKEUP_UNLOCKS_FIXER_03,			HASH("NGMP_FACEPAINT_M_56")) BREAK //	Hot Eye Flicks
					CASE 83 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_BLUE_EYE_PLAID),																						"CC_MKUP_85",	HOS_MAKEUP,			85,	1.0,	MAKEUP_UNLOCKS_FIXER_04,			HASH("NGMP_FACEPAINT_M_57")) BREAK //	Blue Eye Plaid
					CASE 84 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_GREEN_EYE_PLAID),																						"CC_MKUP_86",	HOS_MAKEUP,			86,	1.0,	MAKEUP_UNLOCKS_FIXER_05,			HASH("NGMP_FACEPAINT_M_58")) BREAK //	Green Eye Plaid
					CASE 85 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_ORANGE_SUN),																							"CC_MKUP_87",	HOS_MAKEUP,			87,	1.0,	MAKEUP_UNLOCKS_FIXER_06,			HASH("NGMP_FACEPAINT_M_59")) BREAK //	Orange Sun
					CASE 86 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_RED_SUN),																								"CC_MKUP_88",	HOS_MAKEUP,			88,	1.0,	MAKEUP_UNLOCKS_FIXER_07,			HASH("NGMP_FACEPAINT_M_60")) BREAK //	Red Sun
					CASE 87 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_WHITE_LINES_DOTS),																						"CC_MKUP_89",	HOS_MAKEUP,			89,	1.0,	MAKEUP_UNLOCKS_FIXER_08,			HASH("NGMP_FACEPAINT_M_61")) BREAK //	White Lines & Dots
					CASE 88 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_COOL_LINES_DOTS),																						"CC_MKUP_90",	HOS_MAKEUP,			90,	1.0,	MAKEUP_UNLOCKS_FIXER_09,			HASH("NGMP_FACEPAINT_M_62")) BREAK //	Cool Lines & Dots
					CASE 89 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_PURPLE_EYEMASK),																						"CC_MKUP_91",	HOS_MAKEUP,			91,	1.0,	MAKEUP_UNLOCKS_FIXER_10,			HASH("NGMP_FACEPAINT_M_63")) BREAK //	Purple Eyemask
					CASE 90 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_PINK_EYEMASK),																							"CC_MKUP_92",	HOS_MAKEUP,			92,	1.0,	MAKEUP_UNLOCKS_FIXER_11,			HASH("NGMP_FACEPAINT_M_64")) BREAK //	Pink Eyemask
					CASE 91 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_YELLOW_TRIBAL),																							"CC_MKUP_93",	HOS_MAKEUP,			93,	1.0,	MAKEUP_UNLOCKS_FIXER_12,			HASH("NGMP_FACEPAINT_M_65")) BREAK //	Yellow Tribal
					CASE 92 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_GREEN_TRIBAL),																							"CC_MKUP_94",	HOS_MAKEUP,			94,	1.0,	MAKEUP_UNLOCKS_FIXER_13,			HASH("NGMP_FACEPAINT_M_66")) BREAK //	Green Tribal
					#ENDIF
					
					CASE MAX_M_NGMP_FACEPAINT_COUNT RETURN FALSE BREAK
				ENDSWITCH																																																											
			BREAK
			CASE HME_NGMP_MAKEUP_EYE
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(100)*g_sMPTunables.fmakeup_unlocks_basic_expenditure_tunable),							"NONE",			HOS_MAKEUP,			-1,	1.0,	MAKEUP_UNLOCKS_BASIC,				HASH("NGMP_MAKEUP_EYE_M_0")) BREAK // "None"
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(225)*g_sMPTunables.fmakeup_unlocks_nude_expenditure_tunable),							"CC_MKUP_0",	HOS_MAKEUP,			0,	1.0,	MAKEUP_UNLOCKS_NUDE,				HASH("NGMP_MAKEUP_EYE_M_1")) BREAK // "Smoky Black"
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(250)*g_sMPTunables.fmakeup_unlocks_smoky_expenditure_tunable),							"CC_MKUP_1",	HOS_MAKEUP,			1,	1.0,	MAKEUP_UNLOCKS_SMOKY,				HASH("NGMP_MAKEUP_EYE_M_2")) BREAK // "Bronze"
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(175)*g_sMPTunables.fmakeup_unlocks_gothic_expenditure_tunable),							"CC_MKUP_2",	HOS_MAKEUP,			2,	1.0,	MAKEUP_UNLOCKS_GOTHIC,				HASH("NGMP_MAKEUP_EYE_M_3")) BREAK // "Soft Gray"
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(610)*g_sMPTunables.fmakeup_unlocks_rocker_expenditure_tunable),							"CC_MKUP_3",	HOS_MAKEUP,			3,	1.0,	MAKEUP_UNLOCKS_ROCKER,				HASH("NGMP_MAKEUP_EYE_M_4")) BREAK // "Retro Glam"
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(150)*g_sMPTunables.fmakeup_unlocks_partygirl_expenditure_tunable),						"CC_MKUP_4",	HOS_MAKEUP,			4,	1.0,	MAKEUP_UNLOCKS_PARTYGIRL,			HASH("NGMP_MAKEUP_EYE_M_5")) BREAK // "Natural Look"
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(200)*g_sMPTunables.fmakeup_unlocks_artsy_expenditure_tunable),							"CC_MKUP_5",	HOS_MAKEUP,			5,	1.0,	MAKEUP_UNLOCKS_ARTSY,				HASH("NGMP_MAKEUP_EYE_M_6")) BREAK // "Cat Eyes"
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(870)*g_sMPTunables.fmakeup_unlocks_trailerparkprincess_expenditure_tunable),				"CC_MKUP_6",	HOS_MAKEUP,			6,	1.0,	MAKEUP_UNLOCKS_TRAILERPARKPRINCESS,	HASH("NGMP_MAKEUP_EYE_M_7")) BREAK // "Chola"
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(275)*g_sMPTunables.fmakeup_unlocks_soccermom_expenditure_tunable),						"CC_MKUP_7",	HOS_MAKEUP,			7,	1.0,	MAKEUP_UNLOCKS_SOCCERMOM,			HASH("NGMP_MAKEUP_EYE_M_8")) BREAK // "Vamp"
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(480)*g_sMPTunables.fmakeup_unlocks_femmefatale_expenditure_tunable),						"CC_MKUP_8",	HOS_MAKEUP,			8,	1.0,	MAKEUP_UNLOCKS_FEMMEFATALE,			HASH("NGMP_MAKEUP_EYE_M_9")) BREAK // "Vinewood Glamour"
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(2000)*g_sMPTunables.fmakeup_unlocks_seriouslycerise_expenditure_tunable),				"CC_MKUP_9",	HOS_MAKEUP,			9,	1.0,	MAKEUP_UNLOCKS_SERIOUSLYCERISE,		HASH("NGMP_MAKEUP_EYE_M_10")) BREAK // "Bubblegum"
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(740)*g_sMPTunables.fmakeup_unlocks_discotequewreck_expenditure_tunable),					"CC_MKUP_10",	HOS_MAKEUP,			10,	1.0,	MAKEUP_UNLOCKS_DISCOTEQUEWRECK,		HASH("NGMP_MAKEUP_EYE_M_11")) BREAK // "Aqua Dream"
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(935)*g_sMPTunables.fmakeup_unlocks_beautyspot_expenditure_tunable),						"CC_MKUP_11",	HOS_MAKEUP,			11,	1.0,	MAKEUP_UNLOCKS_BEAUTYSPOT,			HASH("NGMP_MAKEUP_EYE_M_12")) BREAK // "Pin Up"
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(675)*g_sMPTunables.fmakeup_unlocks_toneddown_expenditure_tunable),						"CC_MKUP_12",	HOS_MAKEUP,			12,	1.0,	MAKEUP_UNLOCKS_TONEDDOWN,			HASH("NGMP_MAKEUP_EYE_M_13")) BREAK // "Purple Passion"
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1000)*g_sMPTunables.fmakeup_unlocks_cyanswiped_expenditure_tunable),						"CC_MKUP_13",	HOS_MAKEUP,			13,	1.0,	MAKEUP_UNLOCKS_CYANSWIPED,			HASH("NGMP_MAKEUP_EYE_M_14")) BREAK // "Smoky Cat Eye"
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(805)*g_sMPTunables.fmakeup_unlocks_morningafter_expenditure_tunable),					"CC_MKUP_14",	HOS_MAKEUP,			14,	1.0,	MAKEUP_UNLOCKS_MORNINGAFTER,		HASH("NGMP_MAKEUP_EYE_M_15")) BREAK // "Smoldering Ruby"
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(545)*g_sMPTunables.fmakeup_unlocks_covergirl_expenditure_tunable),						"CC_MKUP_15",	HOS_MAKEUP,			15,	1.0,	MAKEUP_UNLOCKS_COVERGIRL,			HASH("NGMP_MAKEUP_EYE_M_16")) BREAK // "Pop Princess"
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(300)*g_sMPTunables.fmakeup_unlocks_guyliner_expenditure_tunable),						"CC_MKUP_32",	HOS_MAKEUP,			32,	1.0,	MAKEUP_UNLOCKS_GUYLINER,			HASH("NGMP_MAKEUP_EYE_M_17")) BREAK // "Guyliner"
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Blood_Tears),																"CC_MKUP_34",	HOS_MAKEUP,			34,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_18")) BREAK // "Blood Tears"
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Heavy_Metal),																"CC_MKUP_35",	HOS_MAKEUP,			35,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_19")) BREAK // "Heavy Metal"
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Sorrow),																	"CC_MKUP_36",	HOS_MAKEUP,			36,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_20")) BREAK // "Sorrow"
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Prince_of_Darkness),														"CC_MKUP_37",	HOS_MAKEUP,			37,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_21")) BREAK // "Prince of Darkness"
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Rocker),																	"CC_MKUP_38",	HOS_MAKEUP,			38,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_22")) BREAK // "Rocker"
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Goth),																	"CC_MKUP_39",	HOS_MAKEUP,			39,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_23")) BREAK // "Goth"
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Punk),																	"CC_MKUP_40",	HOS_MAKEUP,			40,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_24")) BREAK // "Punk"
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Devastated),																"CC_MKUP_41",	HOS_MAKEUP,			41,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_M_25")) BREAK // "Devastated"
					
					CASE MAX_M_NGMP_MAKEUP_EYE_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_None),															"NONE",				HOS_SKIN_DETAIL_1,	-1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_0")) BREAK // None
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Color_Gloss),													"CC_LIPSTICK_0",	HOS_SKIN_DETAIL_1,	0,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_1")) BREAK // Color Gloss
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Color_Matt),													"CC_LIPSTICK_1",	HOS_SKIN_DETAIL_1,	1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_2")) BREAK // Color Matt
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Gloss),													"CC_LIPSTICK_2",	HOS_SKIN_DETAIL_1,	2,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_3")) BREAK // Lined Gloss
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Matt),													"CC_LIPSTICK_3",	HOS_SKIN_DETAIL_1,	3,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_4")) BREAK // Lined Matt
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Heavy_Lined_Gloss),												"CC_LIPSTICK_4",	HOS_SKIN_DETAIL_1,	4,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_5")) BREAK // Heavy Lined Gloss
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Heavy_Lined_Matt),												"CC_LIPSTICK_5",	HOS_SKIN_DETAIL_1,	5,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_6")) BREAK // Heavy Lined Matt
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Nude_Gloss),												"CC_LIPSTICK_6",	HOS_SKIN_DETAIL_1,	6,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_7")) BREAK // Lined Nude Gloss
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Nude_Matt),												"CC_LIPSTICK_7",	HOS_SKIN_DETAIL_1,	7,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_8")) BREAK // Lined Nude Matt
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Smudged),														"CC_LIPSTICK_8",	HOS_SKIN_DETAIL_1,	8,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_9")) BREAK // Smudged
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Geisha),														"CC_LIPSTICK_9",	HOS_SKIN_DETAIL_1,	9,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_M_10")) BREAK // Geisha
					
					CASE MAX_M_NGMP_MAKEUP_LIPSTICK_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELIF ePedModel = MP_F_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR
				SWITCH iOption
					CASE 0  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(500)*g_sMPTunables.fhaircut_female_expenditure_tunable[0]),									"CC_F_HS_0", GET_FEMALE_HAIR(HAIR_FMF_0_0),																							HASH("NGMP_HAIR_F_0")) BREAK // Close Shave
					CASE 1  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(485)*g_sMPTunables.fhaircut_female_expenditure_tunable[4]),									"CC_F_HS_1", GET_FEMALE_HAIR(HAIR_FMF_1_0),																							HASH("NGMP_HAIR_F_1")) BREAK // Short
					CASE 2  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(425)*g_sMPTunables.fhaircut_female_expenditure_tunable[9]),									"CC_F_HS_2", GET_FEMALE_HAIR(HAIR_FMF_2_0),																							HASH("NGMP_HAIR_F_2")) BREAK // Layered Bob
					CASE 3  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(175)*g_sMPTunables.fhaircut_female_expenditure_tunable[14]),								"CC_F_HS_3", GET_FEMALE_HAIR(HAIR_FMF_3_0),																							HASH("NGMP_HAIR_F_3")) BREAK // Pigtails 
					CASE 4  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(280)*g_sMPTunables.fhaircut_female_expenditure_tunable[19]),								"CC_F_HS_4", GET_FEMALE_HAIR(HAIR_FMF_4_0),																							HASH("NGMP_HAIR_F_4")) BREAK // Ponytail
					CASE 5  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1985)*g_sMPTunables.fhaircut_female_expenditure_tunable[24]),								"CC_F_HS_5", GET_FEMALE_HAIR(HAIR_FMF_5_0),																							HASH("NGMP_HAIR_F_5")) BREAK // Braided Mohawk
					CASE 6  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1135)*g_sMPTunables.fhaircut_female_expenditure_tunable[29]),								"CC_F_HS_6", GET_FEMALE_HAIR(HAIR_FMF_6_0),																							HASH("NGMP_HAIR_F_6")) BREAK // Braids
					CASE 7  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(535)*g_sMPTunables.fhaircut_female_expenditure_tunable[34]),								"CC_F_HS_7", GET_FEMALE_HAIR(HAIR_FMF_7_0),																							HASH("NGMP_HAIR_F_7")) BREAK // Bob
					CASE 8  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(565)*g_sMPTunables.fhaircut_female_expenditure_tunable[39]),								"CC_F_HS_8", GET_FEMALE_HAIR(HAIR_FMF_8_0),																							HASH("NGMP_HAIR_F_8")) BREAK // Faux Hawk
					CASE 9  FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1085)*g_sMPTunables.fhaircut_female_expenditure_tunable[44]),								"CC_F_HS_9", GET_FEMALE_HAIR(HAIR_FMF_9_0),																							HASH("NGMP_HAIR_F_9")) BREAK // French Twist
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(505)*g_sMPTunables.fhaircut_female_expenditure_tunable[49]),								"CC_F_HS_10", GET_FEMALE_HAIR(HAIR_FMF_10_0),																						HASH("NGMP_HAIR_F_10")) BREAK // Long Bob
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(380)*g_sMPTunables.fhaircut_female_expenditure_tunable[54]),								"CC_F_HS_11", GET_FEMALE_HAIR(HAIR_FMF_11_0),																						HASH("NGMP_HAIR_F_11")) BREAK // Loose Tied
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1035)*g_sMPTunables.fhaircut_female_expenditure_tunable[59]),								"CC_F_HS_12", GET_FEMALE_HAIR(HAIR_FMF_12_0),																						HASH("NGMP_HAIR_F_12")) BREAK // Pixie
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(1185)*g_sMPTunables.fhaircut_female_expenditure_tunable[64]),								"CC_F_HS_13", GET_FEMALE_HAIR(HAIR_FMF_13_0),																						HASH("NGMP_HAIR_F_13")) BREAK // Shaved Bangs
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(460)*g_sMPTunables.fhaircut_female_expenditure_tunable[69]),								"CC_F_HS_14", GET_FEMALE_HAIR(HAIR_FMF_14_0),																						HASH("NGMP_HAIR_F_14")) BREAK // Top Knot
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(935)*g_sMPTunables.fhaircut_female_expenditure_tunable[74]),								"CC_F_HS_15", GET_FEMALE_HAIR(HAIR_FMF_15_0),																						HASH("NGMP_HAIR_F_15")) BREAK // Wavy Bob
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_FEMALE_HAIR_Messy_Bun_Black),															"CC_F_HS_17", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BEACH_F_HAIR05"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_16")) BREAK // Messy Bun
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_FEMALE_HAIR_Pin_Up_Girl_Black),														"CC_F_HS_16", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BEACH_F_HAIR00"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_17")) BREAK // Pin Up Girl
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_FEMALE_HAIR_Tight_Bun_Black),															"CC_F_HS_18", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BUSI_F_HAIR0_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_18")) BREAK // Tight Bun
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_FEMALE_HAIR_Twisted_Bob_Black),														"CC_F_HS_19", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BUSI_F_HAIR1_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_19")) BREAK // Twisted Bob
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(980)*g_sMPTunables.fValentine_Haircuts_Multiplier),											"CC_F_HS_23", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_VAL_F_HAIR0_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_20")) BREAK // Flapper Bob
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[3]),												"CC_F_HS_20", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_HIPS_F_HAIR0_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_21")) BREAK // Big Bangs
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, (g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[8]),												"CC_F_HS_21", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_HIPS_F_HAIR1_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_22")) BREAK // Braided Top Knot
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(TO_FLOAT(4965)*g_sMPTunables.fHair_Makeup_IndependenceDay_Group),									"CC_F_HS_22", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_IND_F_HAIR0_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_23")) BREAK // Mullet
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Pinched_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),	"CLO_S1F_H_0_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_F_HAIR0_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_24")) BREAK // Pinched Cornrows
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Leaf_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),		"CLO_S1F_H_1_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_F_HAIR1_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_25")) BREAK // Leaf Cornrows
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Zig_Zag_Cornrows*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),	"CLO_S1F_H_2_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_F_HAIR2_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_26")) BREAK // Zig Zag Cornrows
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Pigtail_Bangs*g_sMPTunables.fLowrider_Male_and_Female_Hair_All_Hair),		"CLO_S1F_H_3_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW_F_HAIR3_0"), COMP_TYPE_HAIR, 4)),		HASH("NGMP_HAIR_F_27")) BREAK // Pigtail Bangs
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Wave_Braids*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_S2F_H_0_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW2_F_HAIR0_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_28")) BREAK // Wave Braids
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Coil_Braids*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_S2F_H_1_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW2_F_HAIR1_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_29")) BREAK // Coil Braids
					CASE 30 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFemale_Hair_Rolled_Quiff*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_S2F_H_2_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_LOW2_F_HAIR2_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_30")) BREAK // Low Bandana Bun
					CASE 31 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Loose_Swept_Back*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_BIF_H_0_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_0_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_31")) BREAK
					CASE 32 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Undercut_Swept_Back*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_BIF_H_1_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_1_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_32")) BREAK
					CASE 33 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Undercut_Swept_Side*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_BIF_H_2_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_2_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_33")) BREAK
					CASE 34 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Hair_Spiked_Mohawk*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_BIF_H_3_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_3_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_34")) BREAK
					CASE 35 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iBiker_Female_Hair_Bandana_and_Braid*g_sMPTunables.fMale_and_Female_Hair_All_Hair),	"CLO_BIF_H_4_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_4_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_35")) BREAK
					CASE 36 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iHair_Layered_Mod*g_sMPTunables.fMale_and_Female_Hair_All_Hair),						"CLO_BIF_H_5_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_6_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_36")) BREAK
					CASE 37 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iHair_Skinbyrd*g_sMPTunables.fMale_and_Female_Hair_All_Hair),							"CLO_BIF_H_6_0", GET_FEMALE_HAIR(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_BIKER_F_HAIR_5_0"), COMP_TYPE_HAIR, 4)),	HASH("NGMP_HAIR_F_37")) BREAK
					CASE 38 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iGR_FEMALE_HAIR_NEAT_BUN*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_GRF_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_GR_F_HAIR_37_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_38")) BREAK	// Neat Bun
					CASE 39 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iGR_FEMALE_HAIR_SHORT_BOB*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_GRF_H_1_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_GR_F_HAIR_38_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_39")) BREAK	// Short Bob
					CASE 40 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(0*g_sMPTunables.fMale_and_Female_Hair_All_Hair),														"CLO_VWF_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_VWD_F_HAIR_0_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_40")) BREAK	// Impotent Rage Hair
					CASE 41 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iTUNER_HAIR_AFRO_FULL*g_sMPTunables.fMale_and_Female_Hair_All_Hair),					"CLO_TRF_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_TUNER_F_HAIR_0_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_41")) BREAK	// Afro
					#IF FEATURE_FIXER
					CASE 42 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iFIXER_HAIRSTYLE_PIXIE_WAVY*g_sMPTunables.fMale_and_Female_Hair_All_Hair),				"CLO_FXF_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_FIXER_F_HAIR_0_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_42")) BREAK	// Pixie
					#ENDIF
					#IF FEATURE_DLC_1_2022
					CASE 43 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iSU22_HAIRSTYLE_SHORT_TUCKED_BOB_F*g_sMPTunables.fMale_and_Female_Hair_All_Hair),		"CLO_SBF_H_0_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_SUM2_F_HAIR_0_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_43")) BREAK	// Short Bob
					CASE 44 FILL_NGMP_MENU_OPTION_DATA_PED_COMP(sOptionData, ROUND(g_sMPTunables.iSU22_HAIRSTYLE_SHAGGY_MULLET_F*g_sMPTunables.fMale_and_Female_Hair_All_Hair),			"CLO_SBF_H_1_0", GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_SUM2_F_HAIR_1_0"), COMP_TYPE_HAIR, 4),	HASH("NGMP_HAIR_F_44")) BREAK	// Shaggy Mullet
					#ENDIF
					
					CASE MAX_F_NGMP_HAIR_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_EYEBROWS
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_None),					"NONE",			HOS_EYEBROW,	-1,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_0")) BREAK // None
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Balanced),				"CC_EYEBRW_0",	HOS_EYEBROW,	0,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_1")) BREAK // Balanced
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Fashion),				"CC_EYEBRW_1",	HOS_EYEBROW,	1,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_2")) BREAK // Fashion
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Cleopatra),			"CC_EYEBRW_2",	HOS_EYEBROW,	2,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_3")) BREAK // Cleopatra
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Quizzical),			"CC_EYEBRW_3",	HOS_EYEBROW,	3,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_4")) BREAK // Quizzical
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Femme),				"CC_EYEBRW_4",	HOS_EYEBROW,	4,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_5")) BREAK // Femme
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Seductive),			"CC_EYEBRW_5",	HOS_EYEBROW,	5,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_6")) BREAK // Seductive
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Pinched),				"CC_EYEBRW_6",	HOS_EYEBROW,	6,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_7")) BREAK // Pinched
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Chola),				"CC_EYEBRW_7",	HOS_EYEBROW,	7,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_8")) BREAK // Chola
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Triomphe),				"CC_EYEBRW_8",	HOS_EYEBROW,	8,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_9")) BREAK // Triomphe
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Carefree),				"CC_EYEBRW_9",	HOS_EYEBROW,	9,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_10")) BREAK // Carefree
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Curvaceous),			"CC_EYEBRW_10",	HOS_EYEBROW,	10,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_11")) BREAK // Curvaceous
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Rodent),				"CC_EYEBRW_11",	HOS_EYEBROW,	11,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_12")) BREAK // Rodent
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Double_Tram),			"CC_EYEBRW_12",	HOS_EYEBROW,	12,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_13")) BREAK // Double Tram
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Thin),					"CC_EYEBRW_13",	HOS_EYEBROW,	13,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_14")) BREAK // Thin
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Penciled),				"CC_EYEBRW_14",	HOS_EYEBROW,	14,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_15")) BREAK // Penciled
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Mother_Plucker),		"CC_EYEBRW_15",	HOS_EYEBROW,	15,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_16")) BREAK // Mother Plucker
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Straight_and_Narrow),	"CC_EYEBRW_16",	HOS_EYEBROW,	16,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_17")) BREAK // Straight and Narrow
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Natural),				"CC_EYEBRW_17",	HOS_EYEBROW,	17,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_18")) BREAK // Natural
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Fuzzy),				"CC_EYEBRW_18",	HOS_EYEBROW,	18,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_19")) BREAK // Fuzzy
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Unkempt),				"CC_EYEBRW_19",	HOS_EYEBROW,	19,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_20")) BREAK // Unkempt
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Caterpillar),			"CC_EYEBRW_20",	HOS_EYEBROW,	20,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_21")) BREAK // Caterpillar
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Regular),				"CC_EYEBRW_21",	HOS_EYEBROW,	21,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_22")) BREAK // Regular
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Mediterranean),		"CC_EYEBRW_22",	HOS_EYEBROW,	22,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_23")) BREAK // Mediterranean
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Groomed),				"CC_EYEBRW_23",	HOS_EYEBROW,	23,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_24")) BREAK // Groomed
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Bushels),				"CC_EYEBRW_24",	HOS_EYEBROW,	24,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_25")) BREAK // Bushels
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Feathered),			"CC_EYEBRW_25",	HOS_EYEBROW,	25,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_26")) BREAK // Feathered
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Prickly),				"CC_EYEBRW_26",	HOS_EYEBROW,	26,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_27")) BREAK // Prickly
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Monobrow),				"CC_EYEBRW_27",	HOS_EYEBROW,	27,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_28")) BREAK // Monobrow
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Winged),				"CC_EYEBRW_28",	HOS_EYEBROW,	28,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_29")) BREAK // Winged
					CASE 30 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Triple_Tram),			"CC_EYEBRW_29",	HOS_EYEBROW,	29,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_30")) BREAK // Triple Tram
					CASE 31 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Arched_Tram),			"CC_EYEBRW_30",	HOS_EYEBROW,	30,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_31")) BREAK // Arched Tram
					CASE 32 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Cutouts),				"CC_EYEBRW_31",	HOS_EYEBROW,	31,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_32")) BREAK // Cutouts
					CASE 33 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Fade_Away),			"CC_EYEBRW_32",	HOS_EYEBROW,	32,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_33")) BREAK // Fade Away
					CASE 34 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iEyebrows_Solo_Tram),			"CC_EYEBRW_33",	HOS_EYEBROW,	33,		1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_EYEBROWS_F_34")) BREAK // Solo Tram
					
					CASE MAX_F_NGMP_EYEBROWS_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_CONTACTS
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Green),				"FACE_E_C_0",	HOS_BASE_DETAIL,	0,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_0")) BREAK // Green
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Emerald),				"FACE_E_C_1",	HOS_BASE_DETAIL,	1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_1")) BREAK // Emerald
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Light_Blue),			"FACE_E_C_2",	HOS_BASE_DETAIL,	2,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_2")) BREAK // Light Blue 
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Ocean_Blue),			"FACE_E_C_3",	HOS_BASE_DETAIL,	3,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_3")) BREAK // Ocean Blue
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Light_Brown),			"FACE_E_C_4",	HOS_BASE_DETAIL,	4,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_4")) BREAK // Light Brown
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Dark_Brown),			"FACE_E_C_5",	HOS_BASE_DETAIL,	5,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_5")) BREAK // Dark Brown 
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Hazel),				"FACE_E_C_6",	HOS_BASE_DETAIL,	6,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_6")) BREAK // Hazel
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Dark_Grey),			"FACE_E_C_7",	HOS_BASE_DETAIL,	7,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_7")) BREAK // Dark Grey
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Light_Grey),			"FACE_E_C_8",	HOS_BASE_DETAIL,	8,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_8")) BREAK // Light Grey
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Pink),					"FACE_E_C_9",	HOS_BASE_DETAIL,	9,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_9")) BREAK // Pink
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Yellow),				"FACE_E_C_10",	HOS_BASE_DETAIL,	10,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_10")) BREAK // Yellow
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Purple),				"FACE_E_C_11",	HOS_BASE_DETAIL,	11,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_11")) BREAK // Purple
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Blackout),				"FACE_E_C_12",	HOS_BASE_DETAIL,	12,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_12")) BREAK // Blackout
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Shades_of_Gray),		"FACE_E_C_13",	HOS_BASE_DETAIL,	13,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_13")) BREAK // Shades of Gray
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Tequila_Sunrise),		"FACE_E_C_14",	HOS_BASE_DETAIL,	14,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_14")) BREAK // Tequila Sunrise
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Atomic),				"FACE_E_C_15",	HOS_BASE_DETAIL,	15,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_15")) BREAK // Atomic
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Warp),					"FACE_E_C_16",	HOS_BASE_DETAIL,	16,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_16")) BREAK // Warp
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Ecola),				"FACE_E_C_17",	HOS_BASE_DETAIL,	17,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_17")) BREAK // Ecola
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Space_Ranger),			"FACE_E_C_18",	HOS_BASE_DETAIL,	18,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_18")) BREAK // Space Ranger
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Ying_Yang),			"FACE_E_C_19",	HOS_BASE_DETAIL,	19,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_19")) BREAK // Ying Yang
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Bullseye),				"FACE_E_C_20",	HOS_BASE_DETAIL,	20,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_20")) BREAK // Bullseye
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Lizard),				"FACE_E_C_21",	HOS_BASE_DETAIL,	21,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_21")) BREAK // Lizard
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Dragon),				"FACE_E_C_22",	HOS_BASE_DETAIL,	22,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_22")) BREAK // Dragon
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Extra_Terrestrial),	"FACE_E_C_23",	HOS_BASE_DETAIL,	23,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_23")) BREAK // Extra Terrestrial 
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Goat),					"FACE_E_C_24",	HOS_BASE_DETAIL,	24,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_24")) BREAK // Goat
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Smiley),				"FACE_E_C_25",	HOS_BASE_DETAIL,	25,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_25")) BREAK // Smiley
					CASE 26 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Possessed),			"FACE_E_C_26",	HOS_BASE_DETAIL,	26,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_26")) BREAK // Possessed
					CASE 27 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Demon),				"FACE_E_C_27",	HOS_BASE_DETAIL,	27,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_27")) BREAK // Demon
					CASE 28 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Infected),				"FACE_E_C_28",	HOS_BASE_DETAIL,	28,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_28")) BREAK // Infected 
					CASE 29 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Alien),				"FACE_E_C_29",	HOS_BASE_DETAIL,	29,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_29")) BREAK // Alien
					CASE 30 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Undead),				"FACE_E_C_30",	HOS_BASE_DETAIL,	30,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_30")) BREAK // Undead
					CASE 31 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iContacts_Zombie),				"FACE_E_C_31",	HOS_BASE_DETAIL,	31,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_CONTACTS_F_31")) BREAK // Zombie
					
					CASE MAX_F_NGMP_CONTACTS_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_FACEPAINT
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(100)*g_sMPTunables.fmakeup_unlocks_basic_expenditure_tunable),				"NONE",			HOS_MAKEUP,		-1,	1.0,	MAKEUP_UNLOCKS_BASIC,				HASH("NGMP_FACEPAINT_F_0")) BREAK // "None"
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(500)*g_sMPTunables.fmakeup_unlocks_kissmyaxe_expenditure_tunable),			"CC_MKUP_16",	HOS_MAKEUP,		16,	1.0,	MAKEUP_UNLOCKS_KISSMYAXE,			HASH("NGMP_FACEPAINT_F_1")) BREAK // Kiss My Axe
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1000)*g_sMPTunables.fmakeup_unlocks_pandapussy_expenditure_tunable),			"CC_MKUP_17",	HOS_MAKEUP,		17,	1.0,	MAKEUP_UNLOCKS_PANDAPUSSY,			HASH("NGMP_FACEPAINT_F_2")) BREAK // Panda Pussy
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(750)*g_sMPTunables.fmakeup_unlocks_thebat_expenditure_tunable),				"CC_MKUP_18",	HOS_MAKEUP,		18,	1.0,	MAKEUP_UNLOCKS_THEBAT,				HASH("NGMP_FACEPAINT_F_3")) BREAK // The Bat
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(5750)*g_sMPTunables.fmakeup_unlocks_skullinscarlet_expenditure_tunable),		"CC_MKUP_19",	HOS_MAKEUP,		19,	1.0,	MAKEUP_UNLOCKS_SKULLINSCARLET,		HASH("NGMP_FACEPAINT_F_4")) BREAK // Skull in Scarlet
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(10000)*g_sMPTunables.fmakeup_unlocks_serpentine_expenditure_tunable),		"CC_MKUP_20",	HOS_MAKEUP,		20,	1.0,	MAKEUP_UNLOCKS_SERPENTINE,			HASH("NGMP_FACEPAINT_F_5")) BREAK // Serpentine
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(6000)*g_sMPTunables.fmakeup_unlocks_theveldt_expenditure_tunable),			"CC_MKUP_21",	HOS_MAKEUP,		21,	1.0,	MAKEUP_UNLOCKS_THEVELDT,			HASH("NGMP_FACEPAINT_F_6")) BREAK // The Veldt
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(5000)*g_sMPTunables.fmakeup_unlocks_triballines_expenditure_tunable),		"CC_MKUP_26",	HOS_MAKEUP,		26,	1.0,	MAKEUP_UNLOCKS_TRIBALLINES,			HASH("NGMP_FACEPAINT_F_7")) BREAK // Tribal Lines
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(5500)*g_sMPTunables.fmakeup_unlocks_tribalswirls_expenditure_tunable),		"CC_MKUP_27",	HOS_MAKEUP,		27,	1.0,	MAKEUP_UNLOCKS_TRIBALSWIRLS,		HASH("NGMP_FACEPAINT_F_8")) BREAK // Tribal Swirls
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1250)*g_sMPTunables.fmakeup_unlocks_tribalorange_expenditure_tunable),		"CC_MKUP_28",	HOS_MAKEUP,		28,	1.0,	MAKEUP_UNLOCKS_TRIBALORANGE,		HASH("NGMP_FACEPAINT_F_9")) BREAK // Tribal Orange
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(2000)*g_sMPTunables.fmakeup_unlocks_tribalred_expenditure_tunable),			"CC_MKUP_29",	HOS_MAKEUP,		29,	1.0,	MAKEUP_UNLOCKS_TRIBALRED,			HASH("NGMP_FACEPAINT_F_10")) BREAK // Tribal Red
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(15000)*g_sMPTunables.fmakeup_unlocks_trappedinabox_expenditure_tunable),		"CC_MKUP_30",	HOS_MAKEUP,		30,	1.0,	MAKEUP_UNLOCKS_TRAPPEDINABOX,		HASH("NGMP_FACEPAINT_F_11")) BREAK // Trapped in a Box
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(20000)*g_sMPTunables.fmakeup_unlocks_clowning_expenditure_tunable),			"CC_MKUP_31",	HOS_MAKEUP,		31,	1.0,	MAKEUP_UNLOCKS_CLOWNING,			HASH("NGMP_FACEPAINT_F_12")) BREAK // Clowning
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(25000)*g_sMPTunables.fHair_Makeup_IndependenceDay_Group),					"CC_MKUP_33",	HOS_MAKEUP,		33,	1.0,	MAKEUP_UNLOCKS_STARS_N_STRIPES,		HASH("NGMP_FACEPAINT_F_13")) BREAK // Stars n Stripes Face Paint
					CASE 14	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Shadow_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_42",	HOS_MAKEUP,		42, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_00,		HASH("NGMP_FACEPAINT_F_14")) BREAK // Shadow Demon
					CASE 15	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Fleshy_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_43",	HOS_MAKEUP,		43, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_01,		HASH("NGMP_FACEPAINT_F_15")) BREAK // Fleshy Demon
					CASE 16	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Flayed_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_44",	HOS_MAKEUP,		44, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_02,		HASH("NGMP_FACEPAINT_F_16")) BREAK // Flayed Demon
					CASE 17	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Sorrow_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_45",	HOS_MAKEUP,		45, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_03,		HASH("NGMP_FACEPAINT_F_17")) BREAK // Sorrow Demon
					CASE 18	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Smiler_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_46",	HOS_MAKEUP,		46, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_04,		HASH("NGMP_FACEPAINT_F_18")) BREAK // Smiler Demon
					CASE 19	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Cracked_Demon) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_47",	HOS_MAKEUP,		47, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_05,		HASH("NGMP_FACEPAINT_F_19")) BREAK // Cracked Demon
					CASE 20	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Danger_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_48",	HOS_MAKEUP,		48, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_06,		HASH("NGMP_FACEPAINT_F_20")) BREAK // Danger Skull
					CASE 21	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Wicked_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_49",	HOS_MAKEUP,		49, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_07,		HASH("NGMP_FACEPAINT_F_21")) BREAK // Wicked Skull
					CASE 22	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Menace_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_50",	HOS_MAKEUP,		50, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_08,		HASH("NGMP_FACEPAINT_F_22")) BREAK // Menace Skull
					CASE 23	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Bone_Jaw_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_51",	HOS_MAKEUP,		51, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_09,		HASH("NGMP_FACEPAINT_F_23")) BREAK // Bone Jaw Skull
					CASE 24	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Flesh_Jaw_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_52",	HOS_MAKEUP,		52, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_10,		HASH("NGMP_FACEPAINT_F_24")) BREAK // Flesh Jaw Skull
					CASE 25	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Spirit_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_53",	HOS_MAKEUP,		53, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_11,		HASH("NGMP_FACEPAINT_F_25")) BREAK // Spirit Skull
					CASE 26	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Ghoul_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_54",	HOS_MAKEUP,		54, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_12,		HASH("NGMP_FACEPAINT_F_26")) BREAK // Ghoul Skull
					CASE 27	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Phantom_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_55",	HOS_MAKEUP,		55, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_13,		HASH("NGMP_FACEPAINT_F_27")) BREAK // Phantom Skull
					CASE 28	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Gnasher_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_56",	HOS_MAKEUP,		56, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_14,		HASH("NGMP_FACEPAINT_F_28")) BREAK // Gnasher Skull
					CASE 29	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Exposed_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_57",	HOS_MAKEUP,		57, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_15,		HASH("NGMP_FACEPAINT_F_29")) BREAK // Exposed Skull
					CASE 30	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Ghostly_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_58",	HOS_MAKEUP,		58, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_16,		HASH("NGMP_FACEPAINT_F_30")) BREAK // Ghostly Skull
					CASE 31	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Fury_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_59",	HOS_MAKEUP,		59, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_17,		HASH("NGMP_FACEPAINT_F_31")) BREAK // Fury Skull
					CASE 32	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Demi_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_60",	HOS_MAKEUP,		60, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_18,		HASH("NGMP_FACEPAINT_F_32")) BREAK // Demi Skull
					CASE 33	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Inbred_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_61",	HOS_MAKEUP,		61, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_19,		HASH("NGMP_FACEPAINT_F_33")) BREAK // Inbred Skull
					CASE 34	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Spooky_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_62",	HOS_MAKEUP,		62, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_20,		HASH("NGMP_FACEPAINT_F_34")) BREAK // Spooky Skull
					CASE 35	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Slashed_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),			"CC_MKUP_63",	HOS_MAKEUP,		63, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_21,		HASH("NGMP_FACEPAINT_F_35")) BREAK // Slashed Skull
					CASE 36	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Web_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_64",	HOS_MAKEUP,		64, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_22,		HASH("NGMP_FACEPAINT_F_36")) BREAK // Web Sugar Skull
					CASE 37	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_SeNor_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_65",	HOS_MAKEUP,		65, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_23,		HASH("NGMP_FACEPAINT_F_37")) BREAK // Senor Sugar Skull
					CASE 38	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Swirl_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_66",	HOS_MAKEUP,		66, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_24,		HASH("NGMP_FACEPAINT_F_38")) BREAK // Swirl Sugar Skull
					CASE 39	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Floral_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),	"CC_MKUP_67",	HOS_MAKEUP,		67, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_25,		HASH("NGMP_FACEPAINT_F_39")) BREAK // Floral Sugar Skull
					CASE 40	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Mono_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_68",	HOS_MAKEUP,		68, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_26,		HASH("NGMP_FACEPAINT_F_40")) BREAK // Mono Sugar Skull
					CASE 41	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Femme_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_69",	HOS_MAKEUP,		69, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_27,		HASH("NGMP_FACEPAINT_F_41")) BREAK // Femme Sugar Skull
					CASE 42	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Demi_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),		"CC_MKUP_70",	HOS_MAKEUP,		70, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_28,		HASH("NGMP_FACEPAINT_F_42")) BREAK // Demi Sugar Skull
					CASE 43	FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(g_sMPTunables.iHalloween_2015_Scarred_Sugar_Skull) * g_sMPTunables.fHalloween_2015_Halloween_2015_All_Face_Paint),	"CC_MKUP_71",	HOS_MAKEUP,		71, 1.0,	MAKEUP_UNLOCKS_HALLOWEEN_29,		HASH("NGMP_FACEPAINT_F_43")) BREAK // Scarred Sugar Skull
					CASE 44 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_WAVES_LEFT),																			"CC_BLUSH_7",	HOS_BLUSHER,	7,	1.0,	MAKEUP_UNLOCKS_GANGOPS_00,			HASH("NGMP_MAKEUP_BLUSHER_F_7")) BREAK	//	Waves Left
					CASE 45 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_WAVES_RIGHT),																			"CC_BLUSH_8",	HOS_BLUSHER,	8,	1.0,	MAKEUP_UNLOCKS_GANGOPS_01,			HASH("NGMP_MAKEUP_BLUSHER_F_8")) BREAK  //	Waves Right
					CASE 46 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_TOTEMIC),																				"CC_BLUSH_9",	HOS_BLUSHER,	9,	1.0,	MAKEUP_UNLOCKS_GANGOPS_02,			HASH("NGMP_MAKEUP_BLUSHER_F_9")) BREAK  //	Totemic
					CASE 47 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_STREAKS_LEFT),																			"CC_BLUSH_10",	HOS_BLUSHER,	10,	1.0,	MAKEUP_UNLOCKS_GANGOPS_03,			HASH("NGMP_MAKEUP_BLUSHER_F_10")) BREAK //	Streaks Left
					CASE 48 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_STREAKS_RIGHT),																		"CC_BLUSH_11",	HOS_BLUSHER,	11,	1.0,	MAKEUP_UNLOCKS_GANGOPS_04,			HASH("NGMP_MAKEUP_BLUSHER_F_11")) BREAK //	Streaks Right
					CASE 49 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BREAKUP_LEFT),																			"CC_BLUSH_12",	HOS_BLUSHER,	12,	1.0,	MAKEUP_UNLOCKS_GANGOPS_05,			HASH("NGMP_MAKEUP_BLUSHER_F_12")) BREAK //	Breakup Left
					CASE 50 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BREAKUP_RIGHT),																		"CC_BLUSH_13",	HOS_BLUSHER,	13,	1.0,	MAKEUP_UNLOCKS_GANGOPS_06,			HASH("NGMP_MAKEUP_BLUSHER_F_13")) BREAK //	Breakup Right
					CASE 51 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BLOTCH_LEFT),																			"CC_BLUSH_14",	HOS_BLUSHER,	14,	1.0,	MAKEUP_UNLOCKS_GANGOPS_07,			HASH("NGMP_MAKEUP_BLUSHER_F_14")) BREAK //	Blotch Left
					CASE 52 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BLOTCH_RIGHT),																			"CC_BLUSH_15",	HOS_BLUSHER,	15,	1.0,	MAKEUP_UNLOCKS_GANGOPS_08,			HASH("NGMP_MAKEUP_BLUSHER_F_15")) BREAK //	Blotch Right
					CASE 53 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_STRIPES),																				"CC_BLUSH_16",	HOS_BLUSHER,	16,	1.0,	MAKEUP_UNLOCKS_GANGOPS_09,			HASH("NGMP_MAKEUP_BLUSHER_F_16")) BREAK //	Stripes
					CASE 54 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_ELDER),																			"CC_BLUSH_17",	HOS_BLUSHER,	17,	1.0,	MAKEUP_UNLOCKS_GANGOPS_10,			HASH("NGMP_MAKEUP_BLUSHER_F_17")) BREAK //	The Elder
					CASE 55 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_VERTICAL_STRIPE),																		"CC_BLUSH_18",	HOS_BLUSHER,	18,	1.0,	MAKEUP_UNLOCKS_GANGOPS_11,			HASH("NGMP_MAKEUP_BLUSHER_F_18")) BREAK //	Vertical Stripe
					CASE 56 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_CLAN),																					"CC_BLUSH_19",	HOS_BLUSHER,	19,	1.0,	MAKEUP_UNLOCKS_GANGOPS_12,			HASH("NGMP_MAKEUP_BLUSHER_F_19")) BREAK //	Clan
					CASE 57 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_SPLATS_RIGHT),																			"CC_BLUSH_20",	HOS_BLUSHER,	20,	1.0,	MAKEUP_UNLOCKS_GANGOPS_13,			HASH("NGMP_MAKEUP_BLUSHER_F_20")) BREAK //	Splats Right
					CASE 58 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_SPLATS_LEFT),																			"CC_BLUSH_21",	HOS_BLUSHER,	21,	1.0,	MAKEUP_UNLOCKS_GANGOPS_14,			HASH("NGMP_MAKEUP_BLUSHER_F_21")) BREAK //	Splats Left
					CASE 59 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_PHOENIX),																			"CC_BLUSH_22",	HOS_BLUSHER,	22,	1.0,	MAKEUP_UNLOCKS_GANGOPS_15,			HASH("NGMP_MAKEUP_BLUSHER_F_22")) BREAK //	The Phoenix
					CASE 60 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_ANCESTRAL),																			"CC_BLUSH_23",	HOS_BLUSHER,	23,	1.0,	MAKEUP_UNLOCKS_GANGOPS_16,			HASH("NGMP_MAKEUP_BLUSHER_F_23")) BREAK //	Ancestral
					CASE 61 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_COVERAGE),																				"CC_BLUSH_24",	HOS_BLUSHER,	24,	1.0,	MAKEUP_UNLOCKS_GANGOPS_17,			HASH("NGMP_MAKEUP_BLUSHER_F_24")) BREAK //	Coverage
					CASE 62 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_SYMMETRY),																				"CC_BLUSH_25",	HOS_BLUSHER,	25,	1.0,	MAKEUP_UNLOCKS_GANGOPS_18,			HASH("NGMP_MAKEUP_BLUSHER_F_25")) BREAK //	Symmetry
					CASE 63 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_FINGERPRINTS),																			"CC_BLUSH_26",	HOS_BLUSHER,	26,	1.0,	MAKEUP_UNLOCKS_GANGOPS_19,			HASH("NGMP_MAKEUP_BLUSHER_F_26")) BREAK //	Fingerprints
					CASE 64 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_BLOTCH_LOWER),																			"CC_BLUSH_27",	HOS_BLUSHER,	27,	1.0,	MAKEUP_UNLOCKS_GANGOPS_20,			HASH("NGMP_MAKEUP_BLUSHER_F_27")) BREAK //	Blotch Lower
					CASE 65 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_SPIRIT),																			"CC_BLUSH_28",	HOS_BLUSHER,	28,	1.0,	MAKEUP_UNLOCKS_GANGOPS_21,			HASH("NGMP_MAKEUP_BLUSHER_F_28")) BREAK //	The Spirit
					CASE 66 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_CHEEK_STRIPES),																		"CC_BLUSH_29",	HOS_BLUSHER,	29,	1.0,	MAKEUP_UNLOCKS_GANGOPS_22,			HASH("NGMP_MAKEUP_BLUSHER_F_29")) BREAK //	Cheek Stripes
					CASE 67 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_THE_DEMON),																			"CC_BLUSH_30",	HOS_BLUSHER,	30,	1.0,	MAKEUP_UNLOCKS_GANGOPS_23,			HASH("NGMP_MAKEUP_BLUSHER_F_30")) BREAK //	The Demon
					CASE 68 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_MANDIBLE),																				"CC_BLUSH_31",	HOS_BLUSHER,	31,	1.0,	MAKEUP_UNLOCKS_GANGOPS_24,			HASH("NGMP_MAKEUP_BLUSHER_F_31")) BREAK //	Mandible
	  				CASE 69 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iH2_MALE_AND_FEMALE_FACEPAINT_FRONTAL),																				"CC_BLUSH_32",	HOS_BLUSHER,	32,	1.0,	MAKEUP_UNLOCKS_GANGOPS_25,			HASH("NGMP_MAKEUP_BLUSHER_F_32")) BREAK //	Frontal
					CASE 70 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_STRIPED_CAMO),																							"CC_MKUP_72",	HOS_MAKEUP,		72,	1.0,	MAKEUP_UNLOCKS_HEIST4_00,			HASH("NGMP_FACEPAINT_F_44")) BREAK //	Camo 1
					CASE 71 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_WOODLAND_CAMO),																						"CC_MKUP_73",	HOS_MAKEUP,		73,	1.0,	MAKEUP_UNLOCKS_HEIST4_01,			HASH("NGMP_FACEPAINT_F_45")) BREAK //	Camo 2
					CASE 72 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_HUNTER_CAMO),																							"CC_MKUP_74",	HOS_MAKEUP,		74,	1.0,	MAKEUP_UNLOCKS_HEIST4_02,			HASH("NGMP_FACEPAINT_F_46")) BREAK //	Camo 3
					CASE 73 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_HALF_CAMO),																							"CC_MKUP_75",	HOS_MAKEUP,		75,	1.0,	MAKEUP_UNLOCKS_HEIST4_03,			HASH("NGMP_FACEPAINT_F_47")) BREAK //	Camo 4
					CASE 74 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_BLOTCHY_CAMO),																							"CC_MKUP_76",	HOS_MAKEUP,		76,	1.0,	MAKEUP_UNLOCKS_HEIST4_04,			HASH("NGMP_FACEPAINT_F_48")) BREAK //	Camo 5
					CASE 75 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_STARFISH_CAMO),																						"CC_MKUP_77",	HOS_MAKEUP,		77,	1.0,	MAKEUP_UNLOCKS_HEIST4_05,			HASH("NGMP_FACEPAINT_F_49")) BREAK //	Camo 6
					CASE 76 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_GHOST_CAMO),																							"CC_MKUP_78",	HOS_MAKEUP,		78,	1.0,	MAKEUP_UNLOCKS_HEIST4_06,			HASH("NGMP_FACEPAINT_F_50")) BREAK //	Camo 7
					CASE 77 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_BLENDED_CAMO),																							"CC_MKUP_79",	HOS_MAKEUP,		79,	1.0,	MAKEUP_UNLOCKS_HEIST4_07,			HASH("NGMP_FACEPAINT_F_51")) BREAK //	Camo 8
					CASE 78 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iIH_FACEPAINT_JUNGLE_CAMO),																							"CC_MKUP_80",	HOS_MAKEUP,		80,	1.0,	MAKEUP_UNLOCKS_HEIST4_08,			HASH("NGMP_FACEPAINT_F_52")) BREAK //	Camo 9
					#IF FEATURE_FIXER
					CASE 79 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_RED_CHEEK_LINES),																					"CC_MKUP_81",	HOS_MAKEUP,		81,	1.0,	MAKEUP_UNLOCKS_FIXER_00,			HASH("NGMP_FACEPAINT_F_53")) BREAK //	Red Cheek Lines
					CASE 80 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_GREEN_CHEEK_LINES),																					"CC_MKUP_82",	HOS_MAKEUP,		82,	1.0,	MAKEUP_UNLOCKS_FIXER_01,			HASH("NGMP_FACEPAINT_F_54")) BREAK //	Green Cheek Lines
					CASE 81 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_COOL_EYE_FLICKS),																					"CC_MKUP_83",	HOS_MAKEUP,		83,	1.0,	MAKEUP_UNLOCKS_FIXER_02,			HASH("NGMP_FACEPAINT_F_55")) BREAK //	Cool Eye Flicks
					CASE 82 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_HOT_EYE_FLICKS),																					"CC_MKUP_84",	HOS_MAKEUP,		84,	1.0,	MAKEUP_UNLOCKS_FIXER_03,			HASH("NGMP_FACEPAINT_F_56")) BREAK //	Hot Eye Flicks
					CASE 83 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_BLUE_EYE_PLAID),																					"CC_MKUP_85",	HOS_MAKEUP,		85,	1.0,	MAKEUP_UNLOCKS_FIXER_04,			HASH("NGMP_FACEPAINT_F_57")) BREAK //	Blue Eye Plaid
					CASE 84 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_GREEN_EYE_PLAID),																					"CC_MKUP_86",	HOS_MAKEUP,		86,	1.0,	MAKEUP_UNLOCKS_FIXER_05,			HASH("NGMP_FACEPAINT_F_58")) BREAK //	Green Eye Plaid
					CASE 85 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_ORANGE_SUN),																						"CC_MKUP_87",	HOS_MAKEUP,		87,	1.0,	MAKEUP_UNLOCKS_FIXER_06,			HASH("NGMP_FACEPAINT_F_59")) BREAK //	Orange Sun
					CASE 86 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_RED_SUN),																							"CC_MKUP_88",	HOS_MAKEUP,		88,	1.0,	MAKEUP_UNLOCKS_FIXER_07,			HASH("NGMP_FACEPAINT_F_60")) BREAK //	Red Sun
					CASE 87 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_WHITE_LINES_DOTS),																					"CC_MKUP_89",	HOS_MAKEUP,		89,	1.0,	MAKEUP_UNLOCKS_FIXER_08,			HASH("NGMP_FACEPAINT_F_61")) BREAK //	White Lines & Dots
					CASE 88 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_COOL_LINES_DOTS),																					"CC_MKUP_90",	HOS_MAKEUP,		90,	1.0,	MAKEUP_UNLOCKS_FIXER_09,			HASH("NGMP_FACEPAINT_F_62")) BREAK //	Cool Lines & Dots
					CASE 89 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_PURPLE_EYEMASK),																					"CC_MKUP_91",	HOS_MAKEUP,		91,	1.0,	MAKEUP_UNLOCKS_FIXER_10,			HASH("NGMP_FACEPAINT_F_63")) BREAK //	Purple Eyemask
					CASE 90 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_PINK_EYEMASK),																						"CC_MKUP_92",	HOS_MAKEUP,		92,	1.0,	MAKEUP_UNLOCKS_FIXER_11,			HASH("NGMP_FACEPAINT_F_64")) BREAK //	Pink Eyemask
					CASE 91 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_YELLOW_TRIBAL),																						"CC_MKUP_93",	HOS_MAKEUP,		93,	1.0,	MAKEUP_UNLOCKS_FIXER_12,			HASH("NGMP_FACEPAINT_F_65")) BREAK //	Yellow Tribal
					CASE 92 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iFIXER_FACEPAINT_GREEN_TRIBAL),																						"CC_MKUP_94",	HOS_MAKEUP,		94,	1.0,	MAKEUP_UNLOCKS_FIXER_13,			HASH("NGMP_FACEPAINT_F_66")) BREAK //	Green Tribal
					#ENDIF
					
					CASE MAX_F_NGMP_FACEPAINT_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_MAKEUP_EYE
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(100)*g_sMPTunables.fmakeup_unlocks_basic_expenditure_tunable),				"NONE",			HOS_MAKEUP,		-1,	1.0,	MAKEUP_UNLOCKS_BASIC,				HASH("NGMP_MAKEUP_EYE_F_0")) BREAK // "None"
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(225)*g_sMPTunables.fmakeup_unlocks_nude_expenditure_tunable),				"CC_MKUP_0",	HOS_MAKEUP,		0,	1.0,	MAKEUP_UNLOCKS_NUDE,				HASH("NGMP_MAKEUP_EYE_F_1")) BREAK // "Smoky Black"
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(250)*g_sMPTunables.fmakeup_unlocks_smoky_expenditure_tunable),				"CC_MKUP_1",	HOS_MAKEUP,		1,	1.0,	MAKEUP_UNLOCKS_SMOKY,				HASH("NGMP_MAKEUP_EYE_F_2")) BREAK // "Bronze"
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(175)*g_sMPTunables.fmakeup_unlocks_gothic_expenditure_tunable),				"CC_MKUP_2",	HOS_MAKEUP,		2,	1.0,	MAKEUP_UNLOCKS_GOTHIC,				HASH("NGMP_MAKEUP_EYE_F_3")) BREAK // "Soft Gray"
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(610)*g_sMPTunables.fmakeup_unlocks_rocker_expenditure_tunable),				"CC_MKUP_3",	HOS_MAKEUP,		3,	1.0,	MAKEUP_UNLOCKS_ROCKER,				HASH("NGMP_MAKEUP_EYE_F_4")) BREAK // "Retro Glam"
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(150)*g_sMPTunables.fmakeup_unlocks_partygirl_expenditure_tunable),			"CC_MKUP_4",	HOS_MAKEUP,		4,	1.0,	MAKEUP_UNLOCKS_PARTYGIRL,			HASH("NGMP_MAKEUP_EYE_F_5")) BREAK // "Natural Look"
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(200)*g_sMPTunables.fmakeup_unlocks_artsy_expenditure_tunable),				"CC_MKUP_5",	HOS_MAKEUP,		5,	1.0,	MAKEUP_UNLOCKS_ARTSY,				HASH("NGMP_MAKEUP_EYE_F_6")) BREAK // "Cat Eyes"
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(870)*g_sMPTunables.fmakeup_unlocks_trailerparkprincess_expenditure_tunable),	"CC_MKUP_6",	HOS_MAKEUP,		6,	1.0,	MAKEUP_UNLOCKS_TRAILERPARKPRINCESS,	HASH("NGMP_MAKEUP_EYE_F_7")) BREAK // "Chola"
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(275)*g_sMPTunables.fmakeup_unlocks_soccermom_expenditure_tunable),			"CC_MKUP_7",	HOS_MAKEUP,		7,	1.0,	MAKEUP_UNLOCKS_SOCCERMOM,			HASH("NGMP_MAKEUP_EYE_F_8")) BREAK // "Vamp"
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(480)*g_sMPTunables.fmakeup_unlocks_femmefatale_expenditure_tunable),			"CC_MKUP_8",	HOS_MAKEUP,		8,	1.0,	MAKEUP_UNLOCKS_FEMMEFATALE,			HASH("NGMP_MAKEUP_EYE_F_9")) BREAK // "Vinewood Glamour"
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(2000)*g_sMPTunables.fmakeup_unlocks_seriouslycerise_expenditure_tunable),	"CC_MKUP_9",	HOS_MAKEUP,		9,	1.0,	MAKEUP_UNLOCKS_SERIOUSLYCERISE,		HASH("NGMP_MAKEUP_EYE_F_10")) BREAK // "Bubblegum"
					CASE 11 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(740)*g_sMPTunables.fmakeup_unlocks_discotequewreck_expenditure_tunable),		"CC_MKUP_10",	HOS_MAKEUP,		10,	1.0,	MAKEUP_UNLOCKS_DISCOTEQUEWRECK,		HASH("NGMP_MAKEUP_EYE_F_11")) BREAK // "Aqua Dream"
					CASE 12 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(935)*g_sMPTunables.fmakeup_unlocks_beautyspot_expenditure_tunable),			"CC_MKUP_11",	HOS_MAKEUP,		11,	1.0,	MAKEUP_UNLOCKS_BEAUTYSPOT,			HASH("NGMP_MAKEUP_EYE_F_12")) BREAK // "Pin Up"
					CASE 13 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(675)*g_sMPTunables.fmakeup_unlocks_toneddown_expenditure_tunable),			"CC_MKUP_12",	HOS_MAKEUP,		12,	1.0,	MAKEUP_UNLOCKS_TONEDDOWN,			HASH("NGMP_MAKEUP_EYE_F_13")) BREAK // "Purple Passion"
					CASE 14 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(1000)*g_sMPTunables.fmakeup_unlocks_cyanswiped_expenditure_tunable),			"CC_MKUP_13",	HOS_MAKEUP,		13,	1.0,	MAKEUP_UNLOCKS_CYANSWIPED,			HASH("NGMP_MAKEUP_EYE_F_14")) BREAK // "Smoky Cat Eye"
					CASE 15 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(805)*g_sMPTunables.fmakeup_unlocks_morningafter_expenditure_tunable),		"CC_MKUP_14",	HOS_MAKEUP,		14,	1.0,	MAKEUP_UNLOCKS_MORNINGAFTER,		HASH("NGMP_MAKEUP_EYE_F_15")) BREAK // "Smoldering Ruby"
					CASE 16 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(545)*g_sMPTunables.fmakeup_unlocks_covergirl_expenditure_tunable),			"CC_MKUP_15",	HOS_MAKEUP,		15,	1.0,	MAKEUP_UNLOCKS_COVERGIRL,			HASH("NGMP_MAKEUP_EYE_F_16")) BREAK // "Pop Princess"
					CASE 17 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	ROUND(TO_FLOAT(300)*g_sMPTunables.fmakeup_unlocks_guyliner_expenditure_tunable),			"CC_MKUP_32",	HOS_MAKEUP,		32,	1.0,	MAKEUP_UNLOCKS_GUYLINER,			HASH("NGMP_MAKEUP_EYE_F_17")) BREAK // "Guyliner"
					CASE 18 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Blood_Tears),													"CC_MKUP_34",	HOS_MAKEUP,		34,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_18")) BREAK // "Blood Tears"
					CASE 19 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Heavy_Metal),													"CC_MKUP_35",	HOS_MAKEUP,		35,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_19")) BREAK // "Heavy Metal"
					CASE 20 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Sorrow),														"CC_MKUP_36",	HOS_MAKEUP,		36,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_20")) BREAK // "Sorrow"
					CASE 21 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Prince_of_Darkness),											"CC_MKUP_37",	HOS_MAKEUP,		37,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_21")) BREAK // "Prince of Darkness"
					CASE 22 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Rocker),														"CC_MKUP_38",	HOS_MAKEUP,		38,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_22")) BREAK // "Rocker"
					CASE 23 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Goth),														"CC_MKUP_39",	HOS_MAKEUP,		39,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_23")) BREAK // "Goth"
					CASE 24 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Punk),														"CC_MKUP_40",	HOS_MAKEUP,		40,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_24")) BREAK // "Punk"
					CASE 25 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Makeup_Devastated),													"CC_MKUP_41",	HOS_MAKEUP,		41,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_EYE_F_25")) BREAK // "Devastated"
					
					CASE MAX_F_NGMP_MAKEUP_EYE_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_MAKEUP_BLUSHER
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_None),														"NONE",			HOS_BLUSHER,	-1,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_0")) BREAK // None
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_Full),														"CC_BLUSH_0",	HOS_BLUSHER,	0,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_1")) BREAK // Full
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_Angled),														"CC_BLUSH_1",	HOS_BLUSHER,	1,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_2")) BREAK // Angled
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_Round),														"CC_BLUSH_2",	HOS_BLUSHER,	2,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_3")) BREAK // Round
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_Horizontal),													"CC_BLUSH_3",	HOS_BLUSHER,	3,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_4")) BREAK // Horizontal
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_High),														"CC_BLUSH_4",	HOS_BLUSHER,	4,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_5")) BREAK // High
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_Sweetheart),													"CC_BLUSH_5",	HOS_BLUSHER,	5,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_6")) BREAK // Sweetheart
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Blusher_Eighties),													"CC_BLUSH_6",	HOS_BLUSHER,	6,	1.0,	DUMMY_CLOTHES_UNLOCK,				HASH("NGMP_MAKEUP_BLUSHER_F_7")) BREAK // Eighties
					
					CASE MAX_F_NGMP_MAKEUP_BLUSHER_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK
				SWITCH iOption
					CASE 0 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_None),															"NONE",				HOS_SKIN_DETAIL_1,	-1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_0")) BREAK // None
					CASE 1 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Color_Gloss),													"CC_LIPSTICK_0",	HOS_SKIN_DETAIL_1,	0,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_1")) BREAK // Color Gloss
					CASE 2 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Color_Matt),													"CC_LIPSTICK_1",	HOS_SKIN_DETAIL_1,	1,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_2")) BREAK // Color Matt
					CASE 3 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Gloss),													"CC_LIPSTICK_2",	HOS_SKIN_DETAIL_1,	2,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_3")) BREAK // Lined Gloss
					CASE 4 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Matt),													"CC_LIPSTICK_3",	HOS_SKIN_DETAIL_1,	3,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_4")) BREAK // Lined Matt
					CASE 5 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Heavy_Lined_Gloss),												"CC_LIPSTICK_4",	HOS_SKIN_DETAIL_1,	4,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_5")) BREAK // Heavy Lined Gloss
					CASE 6 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Heavy_Lined_Matt),												"CC_LIPSTICK_5",	HOS_SKIN_DETAIL_1,	5,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_6")) BREAK // Heavy Lined Matt
					CASE 7 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Nude_Gloss),												"CC_LIPSTICK_6",	HOS_SKIN_DETAIL_1,	6,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_7")) BREAK // Lined Nude Gloss
					CASE 8 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Lined_Nude_Matt),												"CC_LIPSTICK_7",	HOS_SKIN_DETAIL_1,	7,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_8")) BREAK // Lined Nude Matt
					CASE 9 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Smudged),														"CC_LIPSTICK_8",	HOS_SKIN_DETAIL_1,	8,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_9")) BREAK // Smudged
					CASE 10 FILL_NGMP_MENU_OPTION_DATA_OVERLAY(sOptionData,	(g_sMPTunables.iMakeup_Lips_Geisha),														"CC_LIPSTICK_9",	HOS_SKIN_DETAIL_1,	9,	1.0,	DUMMY_CLOTHES_UNLOCK,	HASH("NGMP_MAKEUP_LIPSTICK_F_10")) BREAK // Geisha
					
					CASE MAX_F_NGMP_MAKEUP_LIPSTICK_COUNT RETURN FALSE BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	IF (iVariant = 1)
	AND (sOptionData.iCost = 0)
	AND IS_PLAYER_IN_CASINO_APARTMENT_THEY_OWN(PLAYER_ID())
		IF NOT IS_STRING_NULL_OR_EMPTY(sOptionData.tlLabel)
			PRINTLN(DEBUG_SHOPS, "GET_NGMP_MENU_OPTION_DATA: player in casino, set \"", sOptionData.tlLabel, "\" ", GET_LABEL_FOR_NGMP_MENU(eMenu), " ", GET_MODEL_NAME_FOR_DEBUG(ePedModel), " variant to 0.")
		ENDIF
		iVariant = 0
	ELIF (iVariant = 0)
	AND (sOptionData.iCost != 0)
	AND IS_PLAYER_IN_CASINO_APARTMENT_THEY_OWN(PLAYER_ID())
		PRINTLN(DEBUG_SHOPS, "GET_NGMP_MENU_OPTION_DATA: player in casino, set \"", sOptionData.tlLabel, "\" ", GET_LABEL_FOR_NGMP_MENU(eMenu), " ", GET_MODEL_NAME_FOR_DEBUG(ePedModel), " variant to 1.")
		iVariant = 1
	ENDIF
	
	IF (iVariant = 1)
		sOptionData.iCost = 0	//FREE
	ENDIF
	
	// Price override for basket system
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
	AND NOT IS_STRING_NULL_OR_EMPTY(sOptionData.tlLabel)
	
		TEXT_LABEL_63 tlCategoryKey
		GENERATE_HAIRDO_KEY_FOR_CATALOGUE(tlCategoryKey, eMenu, sOptionData.tlLabel, DEFAULT, iVariant)
		
		IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCategoryKey)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tlCategoryKey)
				CERRORLN(DEBUG_SHOPS, "GET_NGMP_MENU_OPTION_DATA: \"", tlCategoryKey, "\" (keyhash:", GET_HASH_KEY(tlCategoryKey), ") has no valid catalog item, add price:$", sOptionData.iCost, " for \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\".")
			ENDIF
			
			RETURN (NOT IS_STRING_NULL_OR_EMPTY(sOptionData.tlLabel))
		ENDIF
		
		INT iCatalogCost = NET_GAMESERVER_GET_PRICE(GET_HASH_KEY(tlCategoryKey), GET_CATALOGUE_CATEGORY_FOR_HAIRDO_MENU(eMenu, sOptionData.eHeadSlot), 1)
		
		IF (sOptionData.iCost = iCatalogCost)
			CDEBUG1LN(DEBUG_SHOPS, "GET_NGMP_MENU_OPTION_DATA: \"", tlCategoryKey, "\" has valid catalog item, use cost $", iCatalogCost, " for \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\".")
		ELSE
			CWARNINGLN(DEBUG_SHOPS, "GET_NGMP_MENU_OPTION_DATA: \"", tlCategoryKey, "\" has valid catalog item, use cost $", iCatalogCost, " instead of $", sOptionData.iCost, " for \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\".")
		ENDIF
		sOptionData.iCost = iCatalogCost
	ENDIF
	
	RETURN (NOT IS_STRING_NULL_OR_EMPTY(sOptionData.tlLabel))
ENDFUNC
FUNC INT GET_NUMBER_OF_NGMP_TINT_OPTIONS(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu)
	IF ePedModel = MP_M_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR				RETURN GET_NUM_PED_HAIR_TINTS() BREAK
			CASE HME_NGMP_BEARD				RETURN GET_NUM_PED_HAIR_TINTS()	BREAK
			CASE HME_NGMP_EYEBROWS			RETURN GET_NUM_PED_HAIR_TINTS()	BREAK
			CASE HME_NGMP_CHEST				RETURN GET_NUM_PED_HAIR_TINTS()	BREAK
			CASE HME_NGMP_CONTACTS 			RETURN 0 BREAK
			CASE HME_NGMP_FACEPAINT			RETURN GET_NUM_PED_MAKEUP_TINTS() BREAK
			CASE HME_NGMP_MAKEUP_EYE		RETURN 0 BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK	RETURN GET_NUM_PED_MAKEUP_TINTS() BREAK
		ENDSWITCH
	ELIF ePedModel = MP_F_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR				RETURN GET_NUM_PED_HAIR_TINTS() BREAK
			CASE HME_NGMP_EYEBROWS			RETURN GET_NUM_PED_HAIR_TINTS() BREAK
			CASE HME_NGMP_CONTACTS 			RETURN 0 BREAK
			CASE HME_NGMP_FACEPAINT			RETURN GET_NUM_PED_MAKEUP_TINTS() BREAK
			CASE HME_NGMP_MAKEUP_BLUSHER	RETURN GET_NUM_PED_MAKEUP_TINTS() BREAK
			CASE HME_NGMP_MAKEUP_EYE		RETURN 0 BREAK
			CASE HME_NGMP_MAKEUP_LIPSTICK	RETURN GET_NUM_PED_MAKEUP_TINTS() BREAK
		ENDSWITCH
	ENDIF
	RETURN 0
ENDFUNC


FUNC BOOL IS_NGMP_ITEM_DLC(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, NGMP_MENU_OPTION_DATA &sOptionData)
	
	IF ePedModel = MP_M_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR
			//	IF sOptionData.eHairItem >= HAIR_FMM_DLC
				IF NOT ARE_STRINGS_EQUAL("CC_M_HS", GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(sOptionData.tlLabel, GET_LENGTH_OF_LITERAL_STRING("CC_M_HS")))
					RETURN TRUE
				ENDIF
			BREAK
			CASE HME_NGMP_BEARD
				IF sOptionData.eUnlockItem >= BEARD_UNLOCKS_CURLY
				AND sOptionData.eUnlockItem <= BEARD_UNLOCKS_THE_AMBROSE
					RETURN TRUE
				ENDIF
			BREAK
			CASE HME_NGMP_FACEPAINT
				IF sOptionData.eUnlockItem = MAKEUP_UNLOCKS_STARS_N_STRIPES
					RETURN TRUE
				ENDIF
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_HALLOWEEN_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_HALLOWEEN_29
					RETURN TRUE
				ENDIF
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_GANGOPS_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_GANGOPS_25
					RETURN TRUE
				ENDIF
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_HEIST4_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_HEIST4_08
					RETURN TRUE
				ENDIF
				#IF FEATURE_FIXER
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_FIXER_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_FIXER_13
					RETURN TRUE
				ENDIF
				#ENDIF
			BREAK
		ENDSWITCH
	ELIF ePedModel = MP_F_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR
			//	IF sOptionData.eHairItem >= HAIR_FMF_DLC
				IF NOT ARE_STRINGS_EQUAL("CC_F_HS", GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(sOptionData.tlLabel, GET_LENGTH_OF_LITERAL_STRING("CC_F_HS")))
					RETURN TRUE
				ENDIF
			BREAK
			CASE HME_NGMP_FACEPAINT
				IF sOptionData.eUnlockItem = MAKEUP_UNLOCKS_STARS_N_STRIPES
					RETURN TRUE
				ENDIF
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_HALLOWEEN_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_HALLOWEEN_29
					RETURN TRUE
				ENDIF
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_GANGOPS_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_GANGOPS_25
					RETURN TRUE
				ENDIF
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_HEIST4_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_HEIST4_08
					RETURN TRUE
				ENDIF
				#IF FEATURE_FIXER
				IF sOptionData.eUnlockItem >= MAKEUP_UNLOCKS_FIXER_00
				AND sOptionData.eUnlockItem <= MAKEUP_UNLOCKS_FIXER_13
					RETURN TRUE
				ENDIF
				#ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	CDEBUG3LN(DEBUG_SHOPS, "IS_NGMP_ITEM_DLC ", sOptionData.tlLabel, " \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\" content: \"", GET_SHOP_CONTENT_FOR_MENU(sOptionData.tlLabel), "\"")
	RETURN NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sOptionData.tlLabel))
ENDFUNC

FUNC INT GET_NGMP_HAIR_OPTION(MODEL_NAMES ePedModel, INT iOption)
	
	IF ePedModel = MP_M_FREEMODE_01
		IF iOption = 0 	// Close Shave
			RETURN -1
		ENDIF
		RETURN 0
	ELIF ePedModel = MP_F_FREEMODE_01
		IF iOption = 0 	// Close Shave
			RETURN -1
		ENDIF
		RETURN 0
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_TINT_AVAILABLE_FOR_NGMP_ITEM(HAIRDO_MENU_ENUM eMenu, INT tint, SHOP_NAME_ENUM eShop)
	IF eMenu = HME_NGMP_HAIR
	OR eMenu = HME_NGMP_BEARD
	OR eMenu = HME_NGMP_CHEST
	OR eMenu = HME_NGMP_EYEBROWS
		IF (eShop = HAIRDO_SHOP_CASINO_APT)
			IF NOT IS_PED_HAIR_TINT_FOR_CREATOR(tint)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_PED_HAIR_TINT_FOR_BARBER(tint)
			RETURN FALSE
		ENDIF
	ELIF eMenu = HME_NGMP_MAKEUP_LIPSTICK
		IF (eShop = HAIRDO_SHOP_CASINO_APT)
			IF NOT IS_PED_LIPSTICK_TINT_FOR_CREATOR(tint)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_PED_LIPSTICK_TINT_FOR_BARBER(tint)
			RETURN FALSE
		ENDIF
	ELIF eMenu = HME_NGMP_MAKEUP_BLUSHER
		IF (eShop = HAIRDO_SHOP_CASINO_APT)
			IF NOT IS_PED_BLUSH_TINT_FOR_CREATOR(tint)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_PED_BLUSH_TINT_FOR_BARBER(tint)
			RETURN FALSE
		ENDIF
	ELIF eMenu = HME_NGMP_FACEPAINT
		IF NOT IS_PED_BLUSH_FACEPAINT_TINT_FOR_BARBER(tint)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC CHECK_RAMP_IS_VALID(HEAD_OVERLAY_SLOT eHeadSlot, RAMP_TYPE &rampType)
	SWITCH eHeadSlot
		CASE HOS_FACIAL_HAIR
		CASE HOS_EYEBROW
		CASE HOS_BODY_1			// chest hair
			IF (rampType != RT_HAIR)
				CASSERTLN(DEBUG_SHOPS, "CHECK_RAMP_IS_VALID: invalid ramp type[", GET_RAMP_TYPE_TEXT(rampType), "] for ", GET_CHARACTER_OVERLAY_TEXT(eHeadSlot), ", force to RT_HAIR")
				rampType = RT_HAIR
			ENDIF
		BREAK
//		CASE HOS_MAKEUP
		CASE HOS_BLUSHER
		CASE HOS_SKIN_DETAIL_1	// makeup - lipstick
			IF (rampType != RT_MAKEUP)
				CASSERTLN(DEBUG_SHOPS, "CHECK_RAMP_IS_VALID: invalid ramp type[", GET_RAMP_TYPE_TEXT(rampType), "] for ", GET_CHARACTER_OVERLAY_TEXT(eHeadSlot), ", force to RT_MAKEUP")
				rampType = RT_MAKEUP
			ENDIF
		BREAK
		DEFAULT
			IF (rampType != RT_NONE)
				CASSERTLN(DEBUG_SHOPS, "CHECK_RAMP_IS_VALID: invalid ramp type[", GET_RAMP_TYPE_TEXT(rampType), "] for ", GET_CHARACTER_OVERLAY_TEXT(eHeadSlot), ", force to RT_NONE")
				rampType = RT_NONE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(HEAD_OVERLAY_SLOT headOverlay, RAMP_TYPE &rtType, INT iSlot = -1)
	MP_INT_STATS aIntStat = GET_CHARACTER_OVERLAY_COLOUR_STAT(headOverlay)
	IF aIntStat = INT_TO_ENUM(MP_INT_STATS, -1)
		CPRINTLN(DEBUG_SHOPS, "unknown GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(", GET_CHARACTER_OVERLAY_TEXT(headOverlay), ") return -1")
		rtType = RT_NONE
		RETURN -1
	ENDIF
	
	INT iColourSaved = GET_MP_INT_CHARACTER_STAT(aIntStat, iSlot)
	INT iColour
	UNPACK_CHARACTER_OVERLAY_TINT_VALUES(iColourSaved, iColour, rtType)
	
	RETURN iColour
ENDFUNC

PROC SET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(HEAD_OVERLAY_SLOT headOverlay, INT iValue, RAMP_TYPE rtType, INT iSlot = -1, BOOL CoderReset = TRUE)
	MP_INT_STATS aIntStat = GET_CHARACTER_OVERLAY_COLOUR_STAT(headOverlay)
	IF aIntStat != INT_TO_ENUM(MP_INT_STATS, -1)
		CHECK_RAMP_IS_VALID(headOverlay, rtType)
		
		CPRINTLN(DEBUG_SHOPS, "		- [CHARDATA] ", GET_CHARACTER_OVERLAY_TEXT(headOverlay), " Colour:...........................", iValue)
		INT iColourSaveValue = PACK_CHARACTER_OVERLAY_TINT_VALUES(iValue, rtType)
		
		CPRINTLN(DEBUG_SHOPS, "		- [CHARDATA] ", GET_CHARACTER_OVERLAY_TEXT(headOverlay), " Save Value (colour & ramp_type):..", iColourSaveValue, ", ", GET_RAMP_TYPE_TEXT(rtType))
		SET_MP_INT_CHARACTER_STAT(aIntStat, iColourSaveValue, iSlot, CoderReset)
	ELSE
		CASSERTLN(DEBUG_SHOPS, "bad SET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(", GET_CHARACTER_OVERLAY_TEXT(headOverlay), ")")
	ENDIF
ENDPROC



FUNC INT GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HEAD_OVERLAY_SLOT headOverlay, INT iSlot = -1)
	MP_INT_STATS aIntStat = GET_CHARACTER_OVERLAY_SECONDARY_COLOUR_STAT(headOverlay)
	IF aIntStat = INT_TO_ENUM(MP_INT_STATS, -1)
		CPRINTLN(DEBUG_SHOPS, "unknown GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(", GET_CHARACTER_OVERLAY_TEXT(headOverlay), ") return -1")
		RETURN -1
	ENDIF
	
	RETURN GET_MP_INT_CHARACTER_STAT(aIntStat, iSlot)
ENDFUNC



FUNC INT SAFE_GET_MP_INT_CHARACTER_STAT(MP_INT_STATS anIntStat, INT iSlot = -1 #IF USE_TU_CHANGES, BOOL bIgnoreAssert = FALSE #ENDIF )
	IF anIntStat = INT_TO_ENUM(MP_INT_STATS, -1)
		CPRINTLN(DEBUG_SHOPS, "SAFE_GET_MP_INT_CHARACTER_STAT passed negative anIntStat")
		RETURN -1
	ENDIF
	
	RETURN GET_MP_INT_CHARACTER_STAT(anIntStat, iSlot #IF USE_TU_CHANGES, bIgnoreAssert #ENDIF )
ENDFUNC

STRUCT HAIRDO_CUTSCENE_ASSETS_STRUCT
	BOOL bModelRequested
	MODEL_NAMES eModel
	PED_INDEX pedID
	PED_VARIATION_STRUCT sVariations
	GB_MAGNATE_OUTFIT_BOSS_STYLE sGBStyle
ENDSTRUCT

/// PURPOSE: The main struct that we will use for hairdo shops
STRUCT HAIRDO_SHOP_STRUCT
	SHOP_INFO_STRUCT			sShopInfo
	SHOP_BROWSE_INFO_STRUCT		sBrowseInfo
	SHOP_ENTRY_INFO_STRUCT 		sEntryInfo
	SHOP_LOCATE_STRUCT   		sLocateInfo
	//SHOP_COMPONENT_ITEMS_STRUCT	sHairdoInfo
	SHOP_INPUT_DATA_STRUCT 		sInputData
	INT iCurrentHairdo
	INT iCurrentBeard

	INT iOriginalBeard
	FLOAT fCurrentBeardFade
	SHOP_DIALOGUE_STRUCT		sDialogueInfo
	SHOP_KEEPER_STRUCT			sShopReceptionist
	
	IsPedCompItemCurrent fpIsPedCompItemCurrent
	GetPedCompItemCurrent fpGetPedCompItemCurrent
	SetPedCompItemCurrent fpSetPedCompItemCurrent
	IsPedCompItemAvailable fpIsPedCompItemAvailable
	IsPedCompItemAcquired fpIsPedCompItemAcquired
	SetPedCompItemAcquired fpSetPedCompItemAcquired
	IsPedCompItemNew fpIsPedCompItemNew
	GetPedCompDataForItem fpGetPedCompDataForItem
	SetupClothingItemForShop fpSetupClothingItemForShop
	
	// Anims
	STRING sHairdoShopAnimDict				// contains all of the anims for shop except some cam anims
	STRING sCamAnimDict						// contains all of the cam anims except the base on
	STRING sCurrentKeeperAnim 				// used to track which anims are being played by shopkeeper
	STRING sCurrentCustomerAnim				// used to track which anims are being played by player / customer

	// Bools (see if we can get some of these into a bitset instead)
	BOOL bIntroSkipped = FALSE				// used to handle the player skipping the entry intro
	BOOL bInitialEntryComplete				// used to ensure we only increment the visits counter once per visit
	BOOL bSelectedItemIsLocked				// used to display rank unlock text (check if this needs to be declared here)
	BOOL bSelectedItemIsNew					// 
	BOOL bSelectedItemIsDLC					// 
	BOOL bRelGroupSetup						// used to set up the relationship group for shop workers once
	BOOL bLoadPTFX							// have we requested the particle FX?
	BOOL bAudioBankLoaded					// have we requested the audio bank?
	BOOL bTriggerSave						// checks if we need to trigger an autosave once player exits
	BOOL bPlayingSound						// have we triggered the scissors / clippers sound?
	BOOL bUnlockMessageDisplayed			// 
	BOOL bPurchasedHairOrMakeup				// to track if the player has bought a hairdo or makeup this session (#1942210)
	BOOL bLeaveRemotePlayerBehindDisabled	// to track whether the command NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND has been set

	// Other variables
	BUDDY_HIDE_STRUCT sBuddyHideData		// used to hide any buddies during the cutscenes
	SHOP_NAME_ENUM eInShop					// which of the hairdo shops we are in
	REL_GROUP_HASH hairdoGroup				// sets shopkeeper and assistant as friendly to the player
	HAIRDO_MENU_ENUM eCurrentMenu			// used to track which menu the player is in. (main, hair, beard, makeup)	

	OBJECT_INDEX mScissors					// index of the scissors used by the shopkeeper
	OBJECT_INDEX mBrush						// index of the makeup brush used by the shopkeeper

	// Synced scenes
	INT iSyncScenePlayer
	INT iSyncSceneShopkeeper
	INT iNETSyncScenePlayer
	INT iNETSyncSceneShopkeeper
	BOOL bNETSyncSceneLooped
	
	// Camera
	FLOAT fBrowseCamHeightOffset
	FLOAT fBrowseCamHeading
	VECTOR vBrowseCamLookAtCoord
	VECTOR vBrowseCamOffset	
	FLOAT fLowerCamLimit
	FLOAT fUpperCamLimit
	FLOAT fCamZoomAlpha
	
	// Stuff for storing / restoring props
	PED_COMP_NAME_ENUM eHeadProp
	PED_COMP_NAME_ENUM eEyeProp
	PED_COMP_NAME_ENUM eSpecialMask
	PED_COMP_NAME_ENUM eSpecial2Mask
	PED_COMP_NAME_ENUM eHairMask
	PED_COMP_NAME_ENUM eBerdMask
	PED_COMP_NAME_ENUM eTeethMask
	INT iHandDrawable
	INT iHandTexture
	HOODED_JACKET_STATE_ENUM eJacketState
	
	// Still to sort:
	BOOL bHairdresserLookingAtPlayer
	BOOL bReceptionistLookingAtPlayer
	PTFX_ID ptfxHair
	INT iHairCutSoundID
	INT iClippersSoundID
	INT iMakeupSoundID
	TEXT_LABEL_23 sAudioBank
	
	TEXT_LABEL_15 sCurrentItemLabel

	INT iDlgChk_Reset
	TIME_DATATYPE tdDlgChk_Reset
	BOOL bDlgChk_Reset_TimerSet
	INT iDlgChk_Greet
	TIME_DATATYPE tdDlgChk_Greet
	BOOL bDlgChk_Greet_Triggered
	BOOL bDlgChk_Greet_TimerSet
	INT iDlgChk_Ask
	TIME_DATATYPE tdDlgChk_Ask
	BOOL bDlgChk_Ask_Triggered
	BOOL bDlgChk_Ask_TimerSet
	INT iDlgChk_Cut
	TIME_DATATYPE tdDlgChk_Cut
	BOOL bDlgChk_Cut_Triggered
	BOOL bDlgChk_Cut_TimerSet
	INT iDlgChk_Exit
	TIME_DATATYPE tdDlgChk_Exit
	BOOL bDlgChk_Exit_TimerSet
	BOOL bDlgChk_Exit_Trigger
	BOOL bMenuInitialised
	BOOL bRebuildMenu
	BOOL bDoFinaliseHeadBlend
	BOOL bDoInstantLocateTrigger
	BOOL bDontBlockCutsceneSkipButton = FALSE
	BOOL bBlockAutoItemSelect
	INT iTopMenuItem
	
	//Sofa
	VECTOR vSofaLocate1
	VECTOR vSofaLocate2
	FLOAT fSofaLocateWidth
	VECTOR vSofaSeat1
	VECTOR vSofaSeat2
	FLOAT fSofaHeading
	
	// Make up
	#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
	MAKEUP_DATA_STRUCT sMakeup[44]	// Hard code this limit for the moment.
	#ENDIF
	INT iOriginalMakeup				// 
	INT	iPlayerMakeup 				// The makeup the player enters the store with.
	INT iNumMakeupItems				// Total number of makeup items
	MODEL_NAMES eCurrentPedModel
	
	//tints
	SCALEFORM_DATA_STRUCT colourTint_movieID_01
	SCALEFORM_DATA_STRUCT colourTint_movieID_02
	INT iTint1, iTint2, iSavedTint2
	RAMP_TYPE eRampType
	BOOL bToggleChangedItem = FALSE
	FLOAT fBlend
	HAIRDO_CUTSCENE_ASSETS_STRUCT sCutsceneData
	BOOL bRebuildTintMenu
	BOOL bAllowZoomControl
//	INT iValidTints[64]
//	INT iValidTintCount
	INT iMAX_TINT_COUNT
	
	BOOL bDoorTimerSet
	TIME_DATATYPE tdDoorTimer
	
	INT iHeadBlendTimer
	
	HAIRDO_GROUP_ENUM eCurrentHairGroup
	INT iHairdoGroupsAvailable[(NUMBER_OF_HAIRDO_GROUPS/32)+1]
	INT iHairdoGroupsWithNewItems[(NUMBER_OF_HAIRDO_GROUPS/32)+1]

	TIME_DATATYPE tdEntryTimer
	INT iEntryTimer
	
	TATTOO_NAME_ENUM eCrewLogoTattoo = INVALID_TATTOO
	
	#IF IS_DEBUG_BUILD
	BOOL bDrawDebugStuff = FALSE
	#ENDIF
	
	INT iTopItem
ENDSTRUCT

FUNC BOOL LOAD_NGMP_MENU_ASSETS(HAIRDO_SHOP_STRUCT &sData)
	BOOL bCOLOUR_SWITCHER_01Loaded, bCOLOUR_SWITCHER_02Loaded
	
	sData.colourTint_movieID_01.filename = "COLOUR_SWITCHER_01"	//"HAIR_COLOUR_CARD"
	bCOLOUR_SWITCHER_01Loaded = SETUP_SCALEFORM_MOVIE(sData.colourTint_movieID_01)
	
	sData.colourTint_movieID_02.filename = "COLOUR_SWITCHER_02"
	bCOLOUR_SWITCHER_02Loaded = SETUP_SCALEFORM_MOVIE(sData.colourTint_movieID_02)
	
	RETURN bCOLOUR_SWITCHER_01Loaded AND bCOLOUR_SWITCHER_02Loaded
ENDFUNC
PROC CLEANUP_NGMP_MENU_ASSETS(HAIRDO_SHOP_STRUCT &sData)
	CLEANUP_SCALEFORM_MOVIE(sData.colourTint_movieID_01)
	CLEANUP_SCALEFORM_MOVIE(sData.colourTint_movieID_02)
	
ENDPROC

/// PURPOSE:
///    Checks if the hairdo shop we are running is the salon
/// RETURNS:
///    TRUE if we are running the salon, FALSE otherwise
FUNC BOOL IS_SALON_ACTIVE(HAIRDO_SHOP_STRUCT &sData)
	IF sData.sShopInfo.eShop = HAIRDO_SHOP_01_BH
		RETURN TRUE // we are using the salon
	ENDIF	
	
	RETURN FALSE // normal barber shop
ENDFUNC



// ---------------------Hair----------------------------------------------------

/// PURPOSE:
///    SP hairdo prices vary greatly from barbers to salon so need to be in this lookup function
/// PARAMS:
///    ePedModel - model of ped getting haircut / beard
///    eItem - the selected haircut / beard
///    bHair- If TRUE gets haricut price. If FALSE gets beard price
/// RETURNS:
///    price of the haircut
FUNC INT GET_SP_HAIRDO_PRICE(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel, PED_COMP_NAME_ENUM eItem, BOOL bHair)

	INT iCost =1
	
	IF IS_SALON_ACTIVE(sData)
		// ---------------Salon prices---------------------------------
		SWITCH ePedModel
			CASE PLAYER_ZERO
			
				IF bHair = TRUE // hair
					SWITCH eItem 
						CASE HAIR_P0_0_0 				iCost = 180 BREAK
						CASE HAIR_P0_1_0 				iCost = 0 BREAK // social club = free
						CASE HAIR_P0_2_0 				iCost = 200 BREAK
						CASE HAIR_P0_3_0 				iCost = 290 BREAK
						CASE HAIR_P0_4_0 				iCost = 350 BREAK
					ENDSWITCH
				ELSE // beards
					SWITCH eItem
						CASE BERD_P0_NONE 				iCost = 50 BREAK
						CASE BERD_P0_1_0 				iCost = 80 BREAK
						CASE BERD_P0_3_0 				iCost = 100 BREAK
						CASE BERD_P0_4_0 				iCost = 0 BREAK // social club = free
					ENDSWITCH
				ENDIF
			BREAK

			CASE PLAYER_ONE
				IF bHair = TRUE // hair
					SWITCH eItem
						CASE HAIR_P1_SHORT_3 			iCost = 200 BREAK
						CASE HAIR_P1_SHORT_7 			iCost = 400 BREAK
						CASE HAIR_P1_SHORT_9 			iCost = 250 BREAK
						CASE HAIR_P1_SHORT_10 			iCost = 450 BREAK
						CASE HAIR_P1_SHORT_15 			iCost = 300 BREAK
						CASE HAIR_P1_SHORT_16 			iCost = 350 BREAK
						CASE HAIR_P1_2_0 				iCost = 0 BREAK // social club = free
						CASE HAIR_P1_VERY_SHORT 		iCost = 180 BREAK
					ENDSWITCH
				ELSE // beards
					SWITCH eItem
						CASE BERD_P1_NONE 				iCost = 50 BREAK
						CASE BERD_P1_1_0 				iCost = 0 BREAK // social club = free
						CASE BERD_P1_2_0 				iCost = 100 BREAK
					ENDSWITCH
				ENDIF
			BREAK
		
			CASE PLAYER_TWO
				IF bHair = TRUE // hair
					SWITCH eItem
						CASE HAIR_P2_MESSY 				iCost = 200 BREAK
						CASE HAIR_P2_SHAVED 			iCost = 200 BREAK  //180
						CASE HAIR_P2_CURLS 				iCost = 0 BREAK // social club = free
						CASE HAIR_P2_MESSY2 			iCost = 300 BREAK
					ENDSWITCH
				ELSE // beards
					SWITCH eItem
						CASE BERD_P2_NONE 				iCost = 50 BREAK
						CASE BERD_P2_2_0 				iCost = 60 BREAK
						CASE BERD_P2_3_0 				iCost = 80 BREAK
						CASE BERD_P2_4_0 				iCost = 0 BREAK // social club = free
					ENDSWITCH
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		// ----------------Barber shop prices----------------
		SWITCH ePedModel
			CASE PLAYER_ZERO
				IF bHair = TRUE // hair
					SWITCH eItem
						CASE HAIR_P0_0_0 				iCost = 40 BREAK
						CASE HAIR_P0_1_0 				iCost = 0 BREAK // social club = free
						CASE HAIR_P0_2_0 				iCost = 50 BREAK
						CASE HAIR_P0_3_0 				iCost = 60 BREAK
					ENDSWITCH
				ELSE // beards
					SWITCH eItem
						CASE BERD_P0_NONE 				iCost = 20 BREAK
						CASE BERD_P0_2_0 				iCost = 30 BREAK
						CASE BERD_P0_4_0 				iCost = 0 BREAK // social club = free
					ENDSWITCH
				ENDIF
			BREAK
		
			CASE PLAYER_ONE
				IF bHair = TRUE // hair
					SWITCH eItem
						CASE HAIR_P1_SHORT_1 			iCost = 25 BREAK
						CASE HAIR_P1_SHORT_2 			iCost = 28 BREAK
						CASE HAIR_P1_SHORT_4 			iCost = 30 BREAK
						CASE HAIR_P1_SHORT_5 			iCost = 28 BREAK
						CASE HAIR_P1_SHORT_6 			iCost = 25 BREAK
						CASE HAIR_P1_SHORT_8 			iCost = 35 BREAK
						CASE HAIR_P1_SHORT_11 			iCost = 38 BREAK
						CASE HAIR_P1_SHORT_12 			iCost = 40 BREAK
						CASE HAIR_P1_SHORT_13 			iCost = 45 BREAK
						CASE HAIR_P1_SHORT_14 			iCost = 50 BREAK
						CASE HAIR_P1_1_0 				iCost = 95 BREAK
						CASE HAIR_P1_2_0 				iCost = 0 BREAK // social club = free
						CASE HAIR_P1_3_0 				iCost = 150 BREAK
						CASE HAIR_P1_VERY_SHORT 		iCost = 20 BREAK
					ENDSWITCH
				ELSE // beards
					SWITCH eItem
						CASE BERD_P1_NONE 				iCost = 20 BREAK
						CASE BERD_P1_1_0 				iCost = 0 BREAK // social club = free
						CASE BERD_P1_3_0 				iCost = 65 BREAK
						CASE BERD_P1_4_0 				iCost = 30 BREAK
					ENDSWITCH
				ENDIF
			BREAK
		
			CASE PLAYER_TWO
				IF bHair = TRUE // hair
					SWITCH eItem
						CASE HAIR_P2_MESSY 				iCost = 20 BREAK
						CASE HAIR_P2_CURLS 				iCost = 0 BREAK // social club = free
						CASE HAIR_P2_MESSY2 			iCost = 30 BREAK
						CASE HAIR_P2_MESSY3 			iCost = 60 BREAK
					ENDSWITCH
				ELSE // beards
					SWITCH eItem
						CASE BERD_P2_NONE 				iCost = 20 BREAK
						CASE BERD_P2_4_0 				iCost = 0 BREAK // social club = free
						CASE BERD_P2_5_0 				iCost = 65 BREAK
					ENDSWITCH
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	RETURN iCost
ENDFUNC

FUNC BOOL IS_HAIRDO_GROUP_SAFE_TO_USE(HAIRDO_GROUP_ENUM eMainGroup, HAIRDO_GROUP_ENUM eGroupForCurrentItem)
	IF eMainGroup = HAIR_GROUP_ALL
		RETURN TRUE
	ENDIF
	
	IF eGroupForCurrentItem = eMainGroup
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_HAIRDO_GROUP_LABEL(HAIRDO_GROUP_ENUM eHairGroup, TEXT_LABEL_15 &tlLabel)

	tlLabel = "HAIR_GROUP" // missing
	
	SWITCH eHairGroup
		#IF FEATURE_DLC_1_2022
		CASE HAIR_GROUP_M_DLC_SUMMER2022		tlLabel = "HAIR_GROUP_SB_MO" BREAK
		CASE HAIR_GROUP_F_DLC_SUMMER2022		tlLabel = "HAIR_GROUP_SB_FO" BREAK
		#ENDIF
		#IF FEATURE_FIXER
		CASE HAIR_GROUP_M_DLC_FIXER				tlLabel = "HAIR_GROUP_FX_MO" BREAK
		CASE HAIR_GROUP_F_DLC_FIXER				tlLabel = "HAIR_GROUP_FX_FO" BREAK
		#ENDIF
		CASE HAIR_GROUP_M_DLC_AFRO				tlLabel = "HAIR_GROUP_AF_MO" BREAK
		CASE HAIR_GROUP_F_DLC_AFRO				tlLabel = "HAIR_GROUP_AF_FO" BREAK
		CASE HAIR_GROUP_M_DLC_VINEWOOD			tlLabel = "HAIR_GROUP_VW_M0" BREAK
		CASE HAIR_GROUP_F_DLC_VINEWOOD			tlLabel = "HAIR_GROUP_VW_F0" BREAK
		CASE HAIR_GROUP_M_DLC_GUNRUNNING		tlLabel = "HAIR_GROUP_GR_M0" BREAK
		CASE HAIR_GROUP_F_DLC_GUNRUNNING		tlLabel = "HAIR_GROUP_GR_F0" BREAK
		CASE HAIR_GROUP_M_DLC_BIKER				tlLabel = "HAIR_GROUP_BIK_M0" BREAK
		CASE HAIR_GROUP_F_DLC_BIKER				tlLabel = "HAIR_GROUP_BIK_F0" BREAK
		CASE HAIR_GROUP_M_DLC_LOWRIDER			tlLabel = "HAIR_GROUP_LOW_M0" BREAK
		CASE HAIR_GROUP_F_DLC_LOWRIDER			tlLabel = "HAIR_GROUP_LOW_F0" BREAK
		CASE HAIR_GROUP_MULLET					tlLabel = "HAIR_GROUP_IND1" BREAK
		CASE HAIR_GROUP_LONG_SLICKED			tlLabel = "HAIR_GROUP_HIP1" BREAK
		CASE HAIR_GROUP_HIPSTER_YOUTH			tlLabel = "HAIR_GROUP_HIP2" BREAK
		CASE HAIR_GROUP_BIG_BANGS				tlLabel = "HAIR_GROUP_HIP3" BREAK
		CASE HAIR_GROUP_BRAIDED_TOP_KNOT		tlLabel = "HAIR_GROUP_HIP4" BREAK
		CASE HAIR_GROUP_SHORT_SIDE_PART			tlLabel = "HAIR_GROUP_BUS1" BREAK
		CASE HAIR_GROUP_HIGH_SLICKED_SIDES		tlLabel = "HAIR_GROUP_BUS2" BREAK
		CASE HAIR_GROUP_TIGHT_BUN				tlLabel = "HAIR_GROUP_BUS3" BREAK
		CASE HAIR_GROUP_TWISTED_BOB				tlLabel = "HAIR_GROUP_BUS4" BREAK
		CASE HAIR_GROUP_FLAPPER_BOB				tlLabel = "HAIR_GROUP_VAL1" BREAK
		CASE HAIR_GROUP_PIN_UP_GIRL				tlLabel = "HAIR_GROUP_BCH1" BREAK
		CASE HAIR_GROUP_MESSY_BUN				tlLabel = "HAIR_GROUP_BCH2" BREAK
		CASE HAIR_GROUP_SHAGGY					tlLabel = "HAIR_GROUP_BCH3" BREAK
		CASE HAIR_GROUP_SURFER					tlLabel = "HAIR_GROUP_BCH4" BREAK
		CASE HAIR_GROUP_M_SHAVEN				tlLabel = "HAIR_GROUP_M0" BREAK
		CASE HAIR_GROUP_M_BUZZCUT				tlLabel = "HAIR_GROUP_M1" BREAK
		CASE HAIR_GROUP_M_FAUX_HAWK				tlLabel = "HAIR_GROUP_M2" BREAK
		CASE HAIR_GROUP_M_HIPSTER_SHAVED		tlLabel = "HAIR_GROUP_M3" BREAK
		CASE HAIR_GROUP_M_SIDE_PARTING_SPIKED	tlLabel = "HAIR_GROUP_M4" BREAK
		CASE HAIR_GROUP_M_SHORTER_CUT			tlLabel = "HAIR_GROUP_M5" BREAK
		CASE HAIR_GROUP_M_BIKER					tlLabel = "HAIR_GROUP_M6" BREAK
		CASE HAIR_GROUP_M_PONYTAIL				tlLabel = "HAIR_GROUP_M7" BREAK
		CASE HAIR_GROUP_M_CORNROWS				tlLabel = "HAIR_GROUP_M8" BREAK
		CASE HAIR_GROUP_M_SLICKED				tlLabel = "HAIR_GROUP_M9" BREAK
		CASE HAIR_GROUP_M_SHORT_BRUSHED			tlLabel = "HAIR_GROUP_M10" BREAK
		CASE HAIR_GROUP_M_SPIKEY				tlLabel = "HAIR_GROUP_M11" BREAK
		CASE HAIR_GROUP_M_CAESER				tlLabel = "HAIR_GROUP_M12" BREAK
		CASE HAIR_GROUP_M_CHOPPED				tlLabel = "HAIR_GROUP_M13" BREAK
		CASE HAIR_GROUP_M_DREADS				tlLabel = "HAIR_GROUP_M14" BREAK
		CASE HAIR_GROUP_M_LONG_HAIR				tlLabel = "HAIR_GROUP_M15" BREAK
		CASE HAIR_GROUP_F_SHAVEN				tlLabel = "HAIR_GROUP_F0" BREAK
		CASE HAIR_GROUP_F_SHORT					tlLabel = "HAIR_GROUP_F1" BREAK
		CASE HAIR_GROUP_F_LAYERED_BOB			tlLabel = "HAIR_GROUP_F2" BREAK
		CASE HAIR_GROUP_F_PIGTAILS				tlLabel = "HAIR_GROUP_F3" BREAK
		CASE HAIR_GROUP_F_PONYTAIL				tlLabel = "HAIR_GROUP_F4" BREAK
		CASE HAIR_GROUP_F_BRAIDED_MOHAWK		tlLabel = "HAIR_GROUP_F5" BREAK
		CASE HAIR_GROUP_F_BRAIDS				tlLabel = "HAIR_GROUP_F6" BREAK
		CASE HAIR_GROUP_F_BOB					tlLabel = "HAIR_GROUP_F7" BREAK
		CASE HAIR_GROUP_F_FAUX_HAWK				tlLabel = "HAIR_GROUP_F8" BREAK
		CASE HAIR_GROUP_F_FRENCH_TWIST			tlLabel = "HAIR_GROUP_F9" BREAK
		CASE HAIR_GROUP_F_LONG_BOB				tlLabel = "HAIR_GROUP_F10" BREAK
		CASE HAIR_GROUP_F_LOOSE_TIED			tlLabel = "HAIR_GROUP_F11" BREAK
		CASE HAIR_GROUP_F_PIXIE					tlLabel = "HAIR_GROUP_F12" BREAK
		CASE HAIR_GROUP_F_SHAVED_BANGS			tlLabel = "HAIR_GROUP_F13" BREAK
		CASE HAIR_GROUP_F_TOP_KNOT				tlLabel = "HAIR_GROUP_F14" BREAK
		CASE HAIR_GROUP_F_WAVY_BOB				tlLabel = "HAIR_GROUP_F15" BREAK
	ENDSWITCH
ENDPROC

FUNC HAIRDO_GROUP_ENUM GET_HAIRDO_GROUP_FROM_LABEL(STRING sLabel)
	
	IF IS_STRING_NULL_OR_EMPTY(sLabel)
		#IF IS_DEBUG_BUILD
		CASSERTLN(DEBUG_SHOPS, "GET_HAIRDO_GROUP_FROM_LABEL - Null label lookup.")
		#ENDIF
		RETURN HAIR_GROUP_NONE
	ENDIF
	
	INT iLabelHash = GET_HASH_KEY(sLabel)
	SWITCH iLabelHash
		#IF FEATURE_DLC_1_2022
		CASE HASH("CLO_SBM_H_0_0")
		CASE HASH("CLO_SBM_H_1_0")
			RETURN HAIR_GROUP_M_DLC_SUMMER2022
		BREAK
		CASE HASH("CLO_SBF_H_0_0")
		CASE HASH("CLO_SBF_H_1_0")
			RETURN HAIR_GROUP_F_DLC_SUMMER2022
		BREAK
		#ENDIF
		#IF FEATURE_FIXER
		CASE HASH("CLO_FXM_H_0_0")
			RETURN HAIR_GROUP_M_DLC_FIXER
		BREAK
		CASE HASH("CLO_FXF_H_0_0")
			RETURN HAIR_GROUP_F_DLC_FIXER
		BREAK
		#ENDIF
		CASE HASH("CLO_TRM_H_0_0")
			RETURN HAIR_GROUP_M_DLC_AFRO
		BREAK
		CASE HASH("CLO_TRF_H_0_0")
			RETURN HAIR_GROUP_F_DLC_AFRO
		BREAK
		
		CASE HASH("CLO_VWM_H_0_0") //
			RETURN HAIR_GROUP_M_DLC_VINEWOOD
		BREAK
		CASE HASH("CLO_VWF_H_0_0") //
			RETURN HAIR_GROUP_F_DLC_VINEWOOD
		BREAK
		
		CASE HASH("CLO_GRM_H_0_0") //
		CASE HASH("CLO_GRM_H_1_0") //
			RETURN HAIR_GROUP_M_DLC_GUNRUNNING
		BREAK
		CASE HASH("CLO_GRF_H_0_0") //
		CASE HASH("CLO_GRF_H_1_0") //
			RETURN HAIR_GROUP_F_DLC_GUNRUNNING
		BREAK
		
		CASE HASH("CLO_BIM_H_0_0") //
		CASE HASH("CLO_BIM_H_1_0") //
		CASE HASH("CLO_BIM_H_2_0") //
		CASE HASH("CLO_BIM_H_3_0") //
		CASE HASH("CLO_BIM_H_4_0") //
		CASE HASH("CLO_BIM_H_5_0") //
			RETURN HAIR_GROUP_M_DLC_BIKER
		BREAK
		CASE HASH("CLO_BIF_H_0_0") //
		CASE HASH("CLO_BIF_H_1_0") //
		CASE HASH("CLO_BIF_H_2_0") //
		CASE HASH("CLO_BIF_H_3_0") //
		CASE HASH("CLO_BIF_H_4_0") //
		CASE HASH("CLO_BIF_H_5_0") //
		CASE HASH("CLO_BIF_H_6_0") //
			RETURN HAIR_GROUP_F_DLC_BIKER
		BREAK
		
		CASE HASH("CLO_S1M_H_0_0") //
		CASE HASH("CLO_S1M_H_1_0") //
		CASE HASH("CLO_S1M_H_2_0") //
		CASE HASH("CLO_S1M_H_3_0") //
		CASE HASH("CLO_S2M_H_0_0") //
		CASE HASH("CLO_S2M_H_1_0") //
		CASE HASH("CLO_S2M_H_2_0") //
			RETURN HAIR_GROUP_M_DLC_LOWRIDER
		BREAK
		CASE HASH("CLO_S1F_H_0_0") //
		CASE HASH("CLO_S1F_H_1_0") //
		CASE HASH("CLO_S1F_H_2_0") //
		CASE HASH("CLO_S1F_H_3_0") //
		CASE HASH("CLO_S2F_H_0_0") //
		CASE HASH("CLO_S2F_H_1_0") //
		CASE HASH("CLO_S2F_H_2_0") //
			RETURN HAIR_GROUP_F_DLC_LOWRIDER
		BREAK
		
		CASE HASH("CLO_IND_H_0_0") //Mullet Dark Brown
		CASE HASH("CLO_IND_H_0_1") //Mullet Light Brown
		CASE HASH("CLO_IND_H_0_2") //Mullet Auburn
		CASE HASH("CLO_IND_H_0_3") //Mullet Blonde
		CASE HASH("CLO_IND_H_0_4") //Mullet Black
		CASE HASH("CLO_INDF_H_0_0") //Mullet Chestnut
		CASE HASH("CLO_INDF_H_0_1") //Mullet Blonde
		CASE HASH("CLO_INDF_H_0_2") //Mullet Auburn
		CASE HASH("CLO_INDF_H_0_3") //Mullet Black
		CASE HASH("CLO_INDF_H_0_4") //Mullet Brown
			RETURN HAIR_GROUP_MULLET
		BREAK
		CASE HASH("CLO_HP_HR_0_0") //Long Slicked Dark Brown 
		CASE HASH("CLO_HP_HR_0_1") //Long Slicked Light Brown
		CASE HASH("CLO_HP_HR_0_2") //Long Slicked Auburn
		CASE HASH("CLO_HP_HR_0_3") //Long Slicked Blonde
		CASE HASH("CLO_HP_HR_0_4") //Long Slicked Black
			RETURN HAIR_GROUP_LONG_SLICKED
		BREAK
		CASE HASH("CLO_HP_HR_1_0") //Hipster Youth Dark Brown
		CASE HASH("CLO_HP_HR_1_1") //Hipster Youth Blonde
		CASE HASH("CLO_HP_HR_1_2") //Hipster Youth Auburn
		CASE HASH("CLO_HP_HR_1_3") //Hipster Youth Light Brown
		CASE HASH("CLO_HP_HR_1_4") //Hipster Youth Black
			RETURN HAIR_GROUP_HIPSTER_YOUTH
		BREAK
		CASE HASH("CLO_HP_F_HR_0_0") //Big Bangs Chestnut
		CASE HASH("CLO_HP_F_HR_0_1") //Big Bangs Blonde
		CASE HASH("CLO_HP_F_HR_0_2") //Big Bangs Auburn
		CASE HASH("CLO_HP_F_HR_0_3") //Big Bangs Black
		CASE HASH("CLO_HP_F_HR_0_4") //Big Bangs Brown
			RETURN HAIR_GROUP_BIG_BANGS
		BREAK
		CASE HASH("CLO_HP_F_HR_1_0") //Braided Top Knot Chestnut
		CASE HASH("CLO_HP_F_HR_1_1") //Braided Top Knot Blonde
		CASE HASH("CLO_HP_F_HR_1_2") //Braided Top Knot Auburn
		CASE HASH("CLO_HP_F_HR_1_3") //Braided Top Knot Black
		CASE HASH("CLO_HP_F_HR_1_4") //Braided Top Knot Brown
			RETURN HAIR_GROUP_BRAIDED_TOP_KNOT
		BREAK
		CASE HASH("CLO_BUS_H_0_0") //Short Side Part Dark Brown
		CASE HASH("CLO_BUS_H_0_1") //Short Side Part Light Brown
		CASE HASH("CLO_BUS_H_0_2") //Short Side Part Auburn
		CASE HASH("CLO_BUS_H_0_3") //Short Side Part Blonde
		CASE HASH("CLO_BUS_H_0_4") //Short Side Part Black
			RETURN HAIR_GROUP_SHORT_SIDE_PART
		BREAK
		CASE HASH("CLO_BUS_H_1_0") //High Slicked Sides Dark Brown
		CASE HASH("CLO_BUS_H_1_1") //High Slicked Sides Light Brown
		CASE HASH("CLO_BUS_H_1_2") //High Slicked Sides Auburn
		CASE HASH("CLO_BUS_H_1_3") //High Slicked Sides Blonde
		CASE HASH("CLO_BUS_H_1_4") //High Slicked Sides Black
			RETURN HAIR_GROUP_HIGH_SLICKED_SIDES
		BREAK
		CASE HASH("CLO_BUS_F_H_0_0") //Tight Bun Black
		CASE HASH("CLO_BUS_F_H_0_1") //Tight Bun Brown
		CASE HASH("CLO_BUS_F_H_0_2") //Tight Bun Auburn
		CASE HASH("CLO_BUS_F_H_0_3") //Tight Bun Chestnut
		CASE HASH("CLO_BUS_F_H_0_4") //Tight Bun Blonde
			RETURN HAIR_GROUP_TIGHT_BUN
		BREAK
		CASE HASH("CLO_BUS_F_H_1_0") //Twisted Bob Chestnut
		CASE HASH("CLO_BUS_F_H_1_1") //Twisted Bob Black
		CASE HASH("CLO_BUS_F_H_1_2") //Twisted Bob Auburn
		CASE HASH("CLO_BUS_F_H_1_3") //Twisted Bob Brown
		CASE HASH("CLO_BUS_F_H_1_4") //Twisted Bob Blonde
			RETURN HAIR_GROUP_TWISTED_BOB
		BREAK
		CASE HASH("CLO_VALF_H_0_0") //Flapper Bob Chestnut
		CASE HASH("CLO_VALF_H_0_1") //Flapper Bob Blonde
		CASE HASH("CLO_VALF_H_0_2") //Flapper Bob Auburn
		CASE HASH("CLO_VALF_H_0_3") //Flapper Bob Black
		CASE HASH("CLO_VALF_H_0_4") //Flapper Bob Brown
		CASE HASH("CLO_VALF_H_0_5") //Flapper Bob Blue
			RETURN HAIR_GROUP_FLAPPER_BOB
		BREAK
		CASE HASH("CLO_BBF_H_00") //Pin Up Girl Chestnut
		CASE HASH("CLO_BBF_H_01") //Pin Up Girl Blonde
		CASE HASH("CLO_BBF_H_02") //Pin Up Girl Auburn
		CASE HASH("CLO_BBF_H_03") //Pin Up Girl Black
		CASE HASH("CLO_BBF_H_04") //Pin Up Girl Brown
			RETURN HAIR_GROUP_PIN_UP_GIRL
		BREAK
		CASE HASH("CLO_BBF_H_05") //Messy Bun Chestnut
		CASE HASH("CLO_BBF_H_06") //Messy Bun Blonde
		CASE HASH("CLO_BBF_H_07") //Messy Bun Auburn
		CASE HASH("CLO_BBF_H_08") //Messy Bun Black
		CASE HASH("CLO_BBF_H_09") //Messy Bun Brown
			RETURN HAIR_GROUP_MESSY_BUN
		BREAK
		CASE HASH("CLO_BBM_H_00") //Shaggy Curls Dark Brown
		CASE HASH("CLO_BBM_H_01") //Shaggy Curls Light Brown
		CASE HASH("CLO_BBM_H_02") //Shaggy Curls Auburn
		CASE HASH("CLO_BBM_H_03") //Shaggy Curls Blonde
		CASE HASH("CLO_BBM_H_04") //Shaggy Curls Black
			RETURN HAIR_GROUP_SHAGGY
		BREAK
		CASE HASH("CLO_BBM_H_05") //Surfer Dude Dark Brown
		CASE HASH("CLO_BBM_H_06") //Surfer Dude Light Brown
		CASE HASH("CLO_BBM_H_07") //Surfer Dude Auburn
		CASE HASH("CLO_BBM_H_08") //Surfer Dude Blonde
		CASE HASH("CLO_BBM_H_09") //Surfer Dude Black
			RETURN HAIR_GROUP_SURFER
		BREAK
		
		CASE HASH("CC_M_HS_0")	RETURN HAIR_GROUP_M_SHAVEN
		CASE HASH("CC_M_HS_1")	RETURN HAIR_GROUP_M_BUZZCUT
		CASE HASH("CC_M_HS_2")	RETURN HAIR_GROUP_M_FAUX_HAWK
		CASE HASH("CC_M_HS_3")	RETURN HAIR_GROUP_M_HIPSTER_SHAVED
		CASE HASH("CC_M_HS_4")	RETURN HAIR_GROUP_M_SIDE_PARTING_SPIKED
		CASE HASH("CC_M_HS_5")	RETURN HAIR_GROUP_M_SHORTER_CUT
		CASE HASH("CC_M_HS_6")	RETURN HAIR_GROUP_M_BIKER
		CASE HASH("CC_M_HS_7")	RETURN HAIR_GROUP_M_PONYTAIL
		CASE HASH("CC_M_HS_8")	RETURN HAIR_GROUP_M_CORNROWS
		CASE HASH("CC_M_HS_9")	RETURN HAIR_GROUP_M_SLICKED
		CASE HASH("CC_M_HS_10")	RETURN HAIR_GROUP_M_SHORT_BRUSHED
		CASE HASH("CC_M_HS_11")	RETURN HAIR_GROUP_M_SPIKEY
		CASE HASH("CC_M_HS_12")	RETURN HAIR_GROUP_M_CAESER
		CASE HASH("CC_M_HS_13")	RETURN HAIR_GROUP_M_CHOPPED
		CASE HASH("CC_M_HS_14")	RETURN HAIR_GROUP_M_DREADS
		CASE HASH("CC_M_HS_15")	RETURN HAIR_GROUP_M_LONG_HAIR
		
		CASE HASH("CC_F_HS_0")	RETURN HAIR_GROUP_F_SHAVEN
		CASE HASH("CC_F_HS_1")	RETURN HAIR_GROUP_F_SHORT
		CASE HASH("CC_F_HS_2")	RETURN HAIR_GROUP_F_LAYERED_BOB
		CASE HASH("CC_F_HS_3")	RETURN HAIR_GROUP_F_PIGTAILS
		CASE HASH("CC_F_HS_4")	RETURN HAIR_GROUP_F_PONYTAIL
		CASE HASH("CC_F_HS_5")	RETURN HAIR_GROUP_F_BRAIDED_MOHAWK
		CASE HASH("CC_F_HS_6")	RETURN HAIR_GROUP_F_BRAIDS
		CASE HASH("CC_F_HS_7")	RETURN HAIR_GROUP_F_BOB
		CASE HASH("CC_F_HS_8")	RETURN HAIR_GROUP_F_FAUX_HAWK
		CASE HASH("CC_F_HS_9")	RETURN HAIR_GROUP_F_FRENCH_TWIST
		CASE HASH("CC_F_HS_10")	RETURN HAIR_GROUP_F_LONG_BOB
		CASE HASH("CC_F_HS_11")	RETURN HAIR_GROUP_F_LOOSE_TIED
		CASE HASH("CC_F_HS_12")	RETURN HAIR_GROUP_F_PIXIE
		CASE HASH("CC_F_HS_13")	RETURN HAIR_GROUP_F_SHAVED_BANGS
		CASE HASH("CC_F_HS_14")	RETURN HAIR_GROUP_F_TOP_KNOT
		CASE HASH("CC_F_HS_15")	RETURN HAIR_GROUP_F_WAVY_BOB
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF DOES_TEXT_LABEL_EXIST(sLabel)
		CASSERTLN(DEBUG_SHOPS, "GET_HAIRDO_GROUP_FROM_LABEL - Missing label lookup, label = ", sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sLabel), "\"")
	ELSE
		CASSERTLN(DEBUG_SHOPS, "GET_HAIRDO_GROUP_FROM_LABEL - Missing label lookup, unknown label = ", sLabel)
	ENDIF
	#ENDIF
	
	RETURN HAIR_GROUP_NONE
ENDFUNC

/// PURPOSE:
///    Adds all of the SP hairdos to this shop.
/// PARAMS:
///    fpSetupClothingItemForShop - SP version of setup clothing item for shop
///    sTempHairdoInfo - struct we are populating
///    iCurrentPed - ped we want to get haircuts for
///    eCurrentSubMenu - the menu we are populating
///     bSalon - if TRUE gets the haircuts for the salon, else gets haircuts for barbers
PROC ADD_SP_HAIRDOS_TO_LOCATE(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempHairdoInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu, BOOL bSalon)
	scrShopPedComponent componentItem
	INIT_SHOP_PED_COMPONENT(componentItem)
	
	INT iDLCItem
	INT iDLCItemCount
	
	PED_COMP_NAME_ENUM eDLCHair
	PED_COMP_NAME_ENUM eDLCItem
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			eDLCHair = HAIR_P0_DLC
		BREAK
		CASE CHAR_FRANKLIN
			eDLCHair = HAIR_P1_DLC
		BREAK
		CASE CHAR_TREVOR
			eDLCHair = HAIR_P2_DLC
		BREAK
	ENDSWITCH
	
	// DLC HAIRDOS
	iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_COMPONENT), 0, ENUM_TO_INT(PED_COMP_HAIR))
	REPEAT iDLCItemCount iDLCItem
		GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
		IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
			eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCHair)+iDLCItem)
			CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_NONE, eDLCItem, COMP_TYPE_HAIR)
		ENDIF
	ENDREPEAT
	
	// Michael
	IF bSalon = TRUE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_0_0, COMP_TYPE_HAIR)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_1_0, COMP_TYPE_HAIR) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_2_0, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_3_0, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_4_0, COMP_TYPE_HAIR)
	ELSE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_0_0, COMP_TYPE_HAIR)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_1_0, COMP_TYPE_HAIR) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_2_0, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P0_3_0, COMP_TYPE_HAIR)
	ENDIF
	
	// Franklin
	IF bSalon = TRUE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_3, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_7, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_9, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_10, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_15, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_16, COMP_TYPE_HAIR)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_2_0, COMP_TYPE_HAIR) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_VERY_SHORT, COMP_TYPE_HAIR)
	ELSE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_1, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_2, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_4, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_5, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_6, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_8, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_11, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_12, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_13, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_SHORT_14, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_1_0, COMP_TYPE_HAIR)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_2_0, COMP_TYPE_HAIR) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_3_0, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P1_VERY_SHORT, COMP_TYPE_HAIR)
	ENDIF

	// Trevor
	IF bSalon = TRUE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_MESSY, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_SHAVED, COMP_TYPE_HAIR)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_CURLS, COMP_TYPE_HAIR) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_MESSY2, COMP_TYPE_HAIR)
	ELSE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_MESSY, COMP_TYPE_HAIR)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_CURLS, COMP_TYPE_HAIR) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_MESSY2, COMP_TYPE_HAIR)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, HAIR_P2_MESSY3, COMP_TYPE_HAIR)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds all of the SP beards to this shop.
/// PARAMS:
///    fpSetupClothingItemForShop - SP version of setup clothing item for shop
///    sTempHairdoInfo - struct we are populating
///    iCurrentPed - ped we want to get beards for
///    eCurrentSubMenu - the menu we are populating
///    bSalon - if TRUE gets the beards for the salon, else gets beards for barbers
PROC ADD_SP_BEARDS_TO_LOCATE(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempHairdoInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu, BOOL bSalon)
	// Michael
	IF bSalon = TRUE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_NONE, COMP_TYPE_BERD)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_1_0, COMP_TYPE_BERD)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_3_0, COMP_TYPE_BERD)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_4_0, COMP_TYPE_BERD) ENDIF
	ELSE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_NONE, COMP_TYPE_BERD)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_2_0, COMP_TYPE_BERD)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), eCurrentSubMenu, CLO_MENU_NONE, BERD_P0_4_0, COMP_TYPE_BERD) ENDIF
	ENDIF

	// Franklin
	IF bSalon = TRUE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_NONE, COMP_TYPE_BERD)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_1_0, COMP_TYPE_BERD) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_2_0, COMP_TYPE_BERD)
	ELSE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_NONE, COMP_TYPE_BERD)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_1_0, COMP_TYPE_BERD) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_3_0, COMP_TYPE_BERD)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), eCurrentSubMenu, CLO_MENU_NONE, BERD_P1_4_0, COMP_TYPE_BERD)
	ENDIF

	// Trevor
	IF bSalon = TRUE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_NONE, COMP_TYPE_BERD)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_2_0, COMP_TYPE_BERD)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_3_0, COMP_TYPE_BERD)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_4_0, COMP_TYPE_BERD) ENDIF
	ELSE
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_NONE, COMP_TYPE_BERD)
		IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE) CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_4_0, COMP_TYPE_BERD) ENDIF
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_PLAYER_PED_MODEL(CHAR_TREVOR), eCurrentSubMenu, CLO_MENU_NONE, BERD_P2_5_0, COMP_TYPE_BERD)
	ENDIF
ENDPROC

PROC ADD_MP_HAIRDOS_TO_LOCATE(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sTempHairdoInfo, INT iCurrentPed, CLOTHES_MENU_ENUM eCurrentSubMenu, HAIRDO_GROUP_ENUM eHairGroup, INT &iHairdoGroupsAvailable[], INT &iHairdoGroupsWithNewItems[])
	
	scrShopPedComponent componentItem
	INIT_SHOP_PED_COMPONENT(componentItem)
	
	INT iDLCItem
	INT iDLCItemCount
	
	PED_COMP_NAME_ENUM eDLCHair
	PED_COMP_NAME_ENUM eDLCItem
	
	HAIRDO_GROUP_ENUM eCurrentHairGroup
	
	SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
		CASE MP_M_FREEMODE_01
			eDLCHair = HAIR_FMM_DLC
		BREAK
		CASE MP_F_FREEMODE_01
			eDLCHair = HAIR_FMF_DLC
		BREAK
	ENDSWITCH
	
	BOOL bUnlocked = TRUE
	
	INT i
	REPEAT COUNT_OF(iHairdoGroupsAvailable) i
		iHairdoGroupsAvailable[i] = 0
		iHairdoGroupsWithNewItems[i] = 0
	ENDREPEAT
	
	// In order for this to work with the ped component system we need to set the global locate
	g_iCurrentLocateForDLC = ENUM_TO_INT(eCurrentSubMenu)
	
	// So we don't increase the item count.
	g_bBuildingMainhairGroupMenu = (eHairGroup = HAIR_GROUP_ALL)
	
	// DLC HAIRDOS
	iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_COMPONENT), 0, ENUM_TO_INT(PED_COMP_HAIR))
	REPEAT iDLCItemCount iDLCItem
		GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
		
		PRINTLN("DLC HAIRDO ITEM ", iDLCItem)
		PRINTLN("...label = \"", componentItem.m_textLabel, "\"")
		PRINTLN("...comp type = ", GET_PED_COMP_TYPE_STRING(INT_TO_ENUM(PED_COMP_TYPE_ENUM, componentItem.m_eCompType)))
		PRINTLN("...drawable = ", componentItem.m_drawableIndex)
		PRINTLN("...texture = ", componentItem.m_textureIndex)
		PRINTLN("...locked = ", GET_STRING_FROM_BOOL(IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)))
		PRINTLN("...namehash = ", componentItem.m_nameHash)
		
		IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
			IF NOT IS_CONTENT_ITEM_LOCKED_BY_SCRIPT(componentItem.m_lockHash, componentItem.m_nameHash, SHOP_PED_COMPONENT)
			AND NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(componentItem.m_nameHash, DLC_RESTRICTION_TAG_NIGHT_VISION, ENUM_TO_INT(SHOP_PED_COMPONENT))
				eCurrentHairGroup = GET_HAIRDO_GROUP_FROM_LABEL(componentItem.m_textLabel)
				IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
					SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
					IF NOT IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(componentItem.m_nameHash, PED_COMPONENT_USED_SLOT)
						SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
					ENDIF
					eDLCItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCHair)+iDLCItem)
					CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSubMenu, CLO_MENU_NONE, eDLCItem, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Freemode - Male
	eCurrentHairGroup = HAIR_GROUP_M_SHAVEN
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_0_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_BUZZCUT
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_1_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_1_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_1_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_1_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_1_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_1_5, COMP_TYPE_HAIR) // Grey for creator menu
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_FAUX_HAWK
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_2_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_2_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_2_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_2_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_2_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_2_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)  // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_HIPSTER_SHAVED
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_3_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_3_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_3_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_3_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_3_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_3_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)  // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_SIDE_PARTING_SPIKED
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_5, COMP_TYPE_HAIR) // Grey for creator menu
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_4_6, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_SHORTER_CUT
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_5_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_5_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_5_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_5_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_5_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_5_5, COMP_TYPE_HAI, FALSE, CLO_LBL_NONE, bUnlockedR) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_BIKER
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_6_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_6_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_6_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_6_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_6_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_6_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_PONYTAIL
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_5, COMP_TYPE_HAIR) // Grey for creator menu
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_7_6, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_CORNROWS
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_8_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_8_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_8_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_8_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_8_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_SLICKED
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_5, COMP_TYPE_HAIR) // Grey for creator menu
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_9_6, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_SHORT_BRUSHED
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_10_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_10_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_10_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_10_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_10_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_10_5, COMP_TYPE_HAIR) // Grey for creator menu
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_SPIKEY
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_11_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_11_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_11_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_11_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_11_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_11_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_CAESER
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_12_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_12_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_12_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_12_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_12_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_CHOPPED
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_13_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_13_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_13_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_13_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_13_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_13_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_DREADS
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_14_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_14_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_14_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_14_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_14_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_M_LONG_HAIR
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_15_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_15_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_15_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_15_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_15_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_M_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMM_15_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	
	// Freemode - Female
	eCurrentHairGroup = HAIR_GROUP_F_SHAVEN
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_0_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_SHORT
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_1_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_1_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_1_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_1_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_1_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_1_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_LAYERED_BOB
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_2_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_2_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_2_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_2_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_2_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_2_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_PIGTAILS
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_3_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_3_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_3_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_3_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_3_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_PONYTAIL
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_4_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_4_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_4_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_4_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_4_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_4_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_BRAIDED_MOHAWK
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_5_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_5_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_5_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_5_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_5_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_5_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_BRAIDS
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_6_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_6_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_6_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_6_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_6_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_BOB
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_7_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_7_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_7_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_7_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_7_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_7_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_FAUX_HAWK
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_8_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_8_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_8_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_8_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_8_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_8_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_FRENCH_TWIST
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_9_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_9_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_9_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_9_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_9_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_9_5, COMP_TYPE_HAIR) // Grey for creator menu
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_LONG_BOB
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_5, COMP_TYPE_HAIR) // Grey for creator menu
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_10_6, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_LOOSE_TIED
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_5, COMP_TYPE_HAIR) // Grey for creator menu
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_11_6, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_PIXIE
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_12_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_12_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_12_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_12_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_12_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_12_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_SHAVED_BANGS
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_13_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_13_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_13_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_13_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_13_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_13_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_TOP_KNOT
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_14_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_14_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_14_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_14_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_14_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_14_5, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // Grey for creator menu
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
	eCurrentHairGroup = HAIR_GROUP_F_WAVY_BOB
	g_bHairdoItemAddedIsNew = FALSE
	g_bHairdoItemAdded = FALSE
	IF IS_HAIRDO_GROUP_SAFE_TO_USE(eHairGroup, eCurrentHairGroup)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_0, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_1, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_2, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_3, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_4, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked)
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_5, COMP_TYPE_HAIR) // Grey for creator menu
		//CALL fpSetupClothingItemForShop(sTempHairdoInfo, iCurrentPed, MP_F_FREEMODE_01, eCurrentSubMenu, CLO_MENU_NONE, HAIR_FMF_15_6, COMP_TYPE_HAIR, FALSE, CLO_LBL_NONE, bUnlocked) // DLC - Wrap in IS_BLAH_DLC_INSTALLED when we get one 
		IF g_bHairdoItemAdded
			SET_BIT(iHairdoGroupsAvailable[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			IF g_bHairdoItemAddedIsNew
				SET_BIT(iHairdoGroupsWithNewItems[ENUM_TO_INT(eCurrentHairGroup)/32], ENUM_TO_INT(eCurrentHairGroup)%32)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the current hairdo shop items menu and repopulates with hair or beard options
/// PARAMS:
///    fpSetupClothingItemForShop - SP version of command
///    sHairdos - struct we are populating
///    bHair - TRUE for hair. FALSE for beards
PROC REPOPULATE_HAIRDO_MENU(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_COMPONENT_ITEMS_STRUCT &sHairdos, BOOL bHair,  BOOL bSalon, HAIRDO_GROUP_ENUM eHairGroup, INT &iHairdoGroupsAvailable[], INT &iHairdoGroupsWithNewItems[])

	SHOP_COMPONENT_ITEMS_STRUCT sTempHairdoInfo
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		ADD_MP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sTempHairdoInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, eHairGroup, iHairdoGroupsAvailable, iHairdoGroupsWithNewItems)
	ELSE
		IF bHair = TRUE
			ADD_SP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sTempHairdoInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, bSalon)
		ELSE
			ADD_SP_BEARDS_TO_LOCATE(fpSetupClothingItemForShop, sTempHairdoInfo, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, bSalon)
		ENDIF
	ENDIF
	sHairdos = sTempHairdoInfo
ENDPROC



// --------------------------private functions ----------------------------------------------------

/// PURPOSE:
///    Checks whether we will be playing the intro for this hairdo shop
/// PARAMS:
///    sData - main hairdo shop struct
/// RETURNS:
///    TRUE if intro will be playing. FALSE otherwise
FUNC BOOL IS_HAIRDO_SHOP_INTRO_NEEDED(HAIRDO_SHOP_STRUCT &sData)
	RETURN sData.sShopInfo.bRunEntryIntro
ENDFUNC


PROC RESET_HAIRDO_SHOP_DATA(HAIRDO_SHOP_STRUCT &sData)
	
	CPRINTLN(DEBUG_SHOPS, "RESET_HAIRDO_SHOP_DATA has been called")
	RESET_SHOP_BROWSE_INFO_STRUCT(sData.sBrowseInfo)
	RESET_SHOP_INFO_STRUCT(sData.sShopInfo)
	RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
	RESET_SHOP_LOCATE_STRUCT(sData.sLocateInfo)
	
//	INT i
//	REPEAT MAX_CLOTHES_PER_MENU i
//		sData.sHairdoInfo.eItems[i] = DUMMY_PED_COMP
//		sData.sHairdoInfo.eTypes[i] = COMP_TYPE_HAIR
//	ENDREPEAT
//	sData.sHairdoInfo.iItemCount = 0
//	REPEAT 3 i
//		sData.sHairdoInfo.iSubMenusWithItems[i] = 0
//	ENDREPEAT
		
	
	sData.iCurrentHairdo = 0
	//sData.sShopReceptionist.pedID = NULL
	sData.sShopReceptionist.model = DUMMY_MODEL_FOR_SCRIPT
	sData.sShopReceptionist.vPosIdle 	= << 0.0, 0.0, 0.0 >>
	sData.sShopReceptionist.vPosBrowse 	= << 0.0, 0.0, 0.0 >>
	sData.sShopReceptionist.fHeadIdle 	= 0.0
	sData.sShopReceptionist.fHeadBrowse = 0.0
	sData.sShopReceptionist.bSetup = FALSE
ENDPROC

/// PURPOSE: Helper function to fill the shop info struct
PROC FILL_HAIRDO_SHOP_DATA(SHOP_INFO_STRUCT &sShopInfo, SHOP_NAME_ENUM paramShop, INT paramRoomKey, INT paramLocateCount, BOOL paramDataSet)
	sShopInfo.eShop = paramShop
	sShopInfo.iRoomKey = paramRoomKey
	sShopInfo.iLocateCount = paramLocateCount
	sShopInfo.bDataSet = paramDataSet
ENDPROC

/// PURPOSE: Helper function to fill the shop keeper struct
PROC FILL_SHOP_PED_DATA(SHOP_KEEPER_STRUCT &sShopPed, MODEL_NAMES paramModel, VECTOR paramIdlePos, FLOAT paramIdleHead, VECTOR paramBrowsePos, FLOAT paramBrowseHead)
	sShopPed.model = paramModel
	sShopPed.vPosIdle = paramIdlePos
	sShopPed.fHeadIdle = paramIdleHead
	sShopPed.vPosBrowse = paramBrowsePos
	sShopPed.fHeadBrowse = paramBrowseHead
ENDPROC

/// PURPOSE: Fills the specified struct will the shop information and returns TRUE if successful
FUNC BOOL GET_HAIRDO_SHOP_DATA(SHOP_INFO_STRUCT &sShopInfo, SHOP_KEEPER_STRUCT &sReceptionist, SHOP_NAME_ENUM eShop)
	
	SHOP_NAME_ENUM eBaseShop

	sShopInfo.bDataSet = FALSE
	
	// Reset
	FILL_HAIRDO_SHOP_DATA(sShopInfo, EMPTY_SHOP, 0, 0, FALSE)
	FILL_SHOP_PED_DATA(sShopInfo.sShopKeeper, DUMMY_MODEL_FOR_SCRIPT,	<<0.0, 0.0, 0.0>>, 0.0, <<0.0, 0.0, 0.0>>, 0.0)
	FILL_SHOP_PED_DATA(sReceptionist, DUMMY_MODEL_FOR_SCRIPT, 			<<0.0, 0.0, 0.0>>, 0.0, <<0.0, 0.0, 0.0>>, 0.0)
	
	INT iInterior1 = GET_HASH_KEY("v_hairdresser")
	INT iInterior2 = GET_HASH_KEY("v_barbers")
	INT iInterior3 = GET_HASH_KEY("vw_dlc_casino_apart")
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	// SALON
	IF (iInteriorCheck = iInterior1)
		eBaseShop = HAIRDO_SHOP_01_BH
		FILL_HAIRDO_SHOP_DATA(sShopInfo, eShop, GET_HASH_KEY("Hair_room"), 1, TRUE)
		FILL_SHOP_PED_DATA(sShopInfo.sShopKeeper, s_m_m_hairdress_01, << -815.86, -183.75, 36.5689 >>, 132.08, << -815.86, -183.75, 36.5689 >>, 132.08)//<< -816.7231, -183.7184, 36.5689 >>, 157.9029, << -816.7231, -183.7184, 36.5689 >>, 157.9029)
		FILL_SHOP_PED_DATA(sReceptionist, A_F_Y_BUSINESS_01, <<-823.77, -183.76, 36.5689>>, 209.4933, <<0.0, 0.0 ,0.0>>, 0.0)
		 
		sShopInfo.sShopCustomer.model = A_M_M_BUSINESS_01
		sShopInfo.sShopCustomer.vPosIdle = <<-817.7860, -184.7719, 36.5689>>
		sShopInfo.sShopCustomer.fHeadIdle = 122.6571
		
	// BARBERS
	ELIF (iInteriorCheck = iInterior2) 
		eBaseShop = HAIRDO_SHOP_02_SC
		FILL_HAIRDO_SHOP_DATA(sShopInfo, eShop, GET_HASH_KEY("V_38_BarberRM"), 1, TRUE)
		FILL_SHOP_PED_DATA(sShopInfo.sShopKeeper, S_F_M_FEMBARBER, <<135.0426, -1707.7325, 28.2916>>, 150.8105, <<137.6816, -1708.4681, 28.2916>>, 134.1280 )  //<<135.1728, -1708.9612, 28.2916>>, 140.0702
		
		sShopInfo.sShopCustomer.model =  A_M_Y_StBla_02//A_M_Y_StLat_01//A_M_Y_GENSTREET_01
		sShopInfo.sShopCustomer.vPosIdle = << 138.353, -1709.266, 28.305 >>
		sShopInfo.sShopCustomer.fHeadIdle = -40.150
		
	// CASINO PENTHOUSE
	ELIF (iInteriorCheck = iInterior3) 
		eBaseShop = HAIRDO_SHOP_01_BH
		FILL_HAIRDO_SHOP_DATA(sShopInfo, eShop, GET_HASH_KEY("Apart_Bedroom_Room"), 1, TRUE)
		FILL_SHOP_PED_DATA(sShopInfo.sShopKeeper, s_m_m_hairdress_01, << -815.86, -183.75, 36.5689 >>, 132.08, << -815.86, -183.75, 36.5689 >>, 132.08)//<< -816.7231, -183.7184, 36.5689 >>, 157.9029, << -816.7231, -183.7184, 36.5689 >>, 157.9029)
//		FILL_SHOP_PED_DATA(sReceptionist, A_F_Y_BUSINESS_01, <<-823.77, -183.76, 36.5689>>, 209.4933, <<0.0, 0.0 ,0.0>>, 0.0)
		 
//		sShopInfo.sShopCustomer.model = A_M_M_BUSINESS_01
//		sShopInfo.sShopCustomer.vPosIdle = <<-817.7860, -184.7719, 36.5689>>
//		sShopInfo.sShopCustomer.fHeadIdle = 122.6571
	ENDIF

	IF sShopInfo.bDataSet
		CPRINTLN(DEBUG_SHOPS,"\nUsing data set for ", GET_SHOP_NAME(sShopInfo.eShop), " - ", GET_SHOP_NAME(sShopInfo.eShop))
	ELSE
		CPRINTLN(DEBUG_SHOPS, "GET_HAIRDO_SHOP_DATA Unable to find suitable hairdo shop data")
	ENDIF

	// Align vec data to shop
	ALIGN_SHOP_COORD(eBaseShop, eShop, sShopInfo.sShopKeeper.vPosIdle)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sShopInfo.sShopKeeper.fHeadIdle)
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sShopInfo.sShopKeeper.vPosBrowse)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sShopInfo.sShopKeeper.fHeadBrowse)
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sReceptionist.vPosIdle)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sReceptionist.fHeadIdle)
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sShopInfo.sShopCustomer.vPosIdle)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sShopInfo.sShopCustomer.fHeadIdle)
	
	RETURN sShopInfo.bDataSet
ENDFUNC

FUNC VECTOR GET_HAIRDO_SHOP_DOOR_POS(SHOP_NAME_ENUM eBaseShop, SHOP_NAME_ENUM eShop)
	VECTOR vDoorPos
	IF eBaseShop = HAIRDO_SHOP_01_BH
		// salon
		vDoorPos = <<-822.44, -188.39, 37.82>>
	ELSE
		// barbers
		vDoorPos = <<132.63, -1710.87, 29.59>>
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vDoorPos)
	RETURN vDoorPos
ENDFUNC

PROC GET_HAIRDO_SHOP_SOFA_LOCATE_DATA(SHOP_NAME_ENUM eShop, VECTOR &vSofaLocate1, VECTOR &vSofaLocate2, FLOAT &fSofaLocateWidth)
	
	SHOP_NAME_ENUM eBaseShop
	
	eBaseShop = HAIRDO_SHOP_02_SC
	
	vSofaLocate1 = <<136.878571,-1711.796387,28.291611>>
	vSofaLocate2 = <<135.717346,-1713.230591,30.291611>>
	fSofaLocateWidth = 1.4
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vSofaLocate1)
	ALIGN_SHOP_COORD(eBaseShop, eShop, vSofaLocate2)

ENDPROC

PROC GET_HAIRDO_SHOP_SOFA_ANIM_DATA(SHOP_NAME_ENUM eShop, VECTOR &vSofaSeat1, VECTOR &vSofaSeat2, FLOAT &fSofaHeading)
	
	SHOP_NAME_ENUM eBaseShop
	
	eBaseShop = HAIRDO_SHOP_02_SC
	
	vSofaSeat1 = <<136.83, -1712.38, 28.7754>> //<<136.8461, -1712.4105, 28.7754>>//28.7164
	vSofaSeat2 = <<136.27, -1713.03, 28.7754>> //<<136.2995, -1713.0546, 28.7754>>
	fSofaHeading = 55.0
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vSofaSeat1)
	ALIGN_SHOP_COORD(eBaseShop, eShop, vSofaSeat2)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, fSofaHeading)

ENDPROC

FUNC BOOL GET_HAIRDO_SHOP_ANIM_POSITION(SHOP_NAME_ENUM eShop, VECTOR &vAnimCoords, VECTOR &vAnimRot)
	
	vAnimCoords = <<0,0,0>>
	vAnimRot = <<0,0,0>>
	
	INT iInterior1 = GET_HASH_KEY("v_hairdresser")
	INT iInterior2 = GET_HASH_KEY("v_barbers")
	INT iInterior3 = GET_HASH_KEY("vw_dlc_casino_apart")
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	SHOP_NAME_ENUM eBaseShop
	
	// ----SALON-----
	IF (iInteriorCheck = iInterior1)
		eBaseShop = HAIRDO_SHOP_01_BH
		vAnimCoords = << -816.22, -182.97, 36.57 >>	vAnimRot = <<0, 0, RAD_TO_DEG(-2.60)-90 >>
		
	// ----BARBERS------
	ELIF (iInteriorCheck = iInterior2) 
		eBaseShop = HAIRDO_SHOP_02_SC
		vAnimCoords = << 138.3646, -1709.2522, 28.3182>> vAnimRot = <<0, 0, RAD_TO_DEG(-5.41)-90>>
		
	// ----CASINO PENTHOUSE------
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = HAIRDO_SHOP_01_BH
		vAnimCoords = << -816.22, -182.97, 36.57 >>	vAnimRot = <<0, 0, RAD_TO_DEG(-2.60)-90 >>
		
	ENDIF
	
	// Align vec data to shop
	IF NOT ARE_VECTORS_EQUAL(vAnimCoords, <<0,0,0>>)
	AND NOT ARE_VECTORS_EQUAL(vAnimRot, <<0,0,0>>)
		ALIGN_SHOP_COORD(eBaseShop, eShop, vAnimCoords)
		ALIGN_SHOP_ROTATION(eBaseShop, eShop, vAnimRot)
		RETURN TRUE
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vAnimCoords, <<0,0,0>>)
		CPRINTLN(DEBUG_SHOPS, "GET_HAIRDO_SHOP_ANIM_POSITION vAnimCoords = 0")
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vAnimRot, <<0,0,0>>)
		CPRINTLN(DEBUG_SHOPS, "GET_HAIRDO_SHOP_ANIM_POSITION vAnimRot = 0")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the go to position for the player. Used to have the player walk out of shop after haircut.
///    Also used by the other customer in the intro.
/// PARAMS:
///    eShop - the active shop
///    vPlayerGoToPos - the return vector
PROC GET_HAIRDO_SHOP_PLAYER_GO_TO_POS(SHOP_NAME_ENUM eShop, VECTOR &vPlayerGoToPos)
	
	SHOP_NAME_ENUM eBaseShop
	INT iInterior1 = GET_HASH_KEY("v_hairdresser")
	INT iInterior2 = GET_HASH_KEY("v_barbers")
	INT iInterior3 = GET_HASH_KEY("vw_dlc_casino_apart")
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	IF (iInteriorCheck = iInterior1)
		// salon
		eBaseShop = HAIRDO_SHOP_01_BH
		vPlayerGoToPos 		= << -825.9313, -189.4604, 36.6014 >>
		
	ELIF (iInteriorCheck = iInterior2)
		// barbers
		eBaseShop = HAIRDO_SHOP_02_SC
		vPlayerGoToPos 		=  <<130.3482, -1713.9735, 28.2503>> //<<130.6592, -1714.5642, 28.2456>> //<<129.3686, -1713.4192, 28.2463>>
		
	ELIF (iInteriorCheck = iInterior3)
		// casino penthouse
		eBaseShop = HAIRDO_SHOP_01_BH
		vPlayerGoToPos 		= << -825.9313, -189.4604, 36.6014 >>
		
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vPlayerGoToPos)
ENDPROC

/// PURPOSE:
///    Grabs all the relevant data for this hairdo shop.
///    Locate positions and menu data
/// PARAMS:
///    fpSetupClothingItemForShop - SP / MP version of command
///    eShop - the shop we are in
///    sLocate - locate struct we are populating
///    sHairdos - the shop item struct we are populating
///    bBeards - If TRUE grabs the beard info instead of the hairdo info
/// RETURNS:
///    TRUE if there are options in the menu
FUNC BOOL GET_HAIRDO_SHOP_LOCATE_DATA(SetupClothingItemForShop &fpSetupClothingItemForShop, SHOP_NAME_ENUM eShop, SHOP_LOCATE_STRUCT &sLocate,  SHOP_COMPONENT_ITEMS_STRUCT &sHairdos, INT &iHairdoGroupsAvailable[], INT &iHairdoGroupsWithNewItems[])

	// Temp so all data is cleared
	SHOP_LOCATE_STRUCT sTempLocateInfo
	SHOP_NAME_ENUM eBaseShop
	
	// Reset the ped comp struct.
	INT i
	REPEAT COUNT_OF(sHairdos.iItemLabelOverride) i
		sHairdos.iItemLabelOverride[i] = 0
	ENDREPEAT
	REPEAT COUNT_OF(sHairdos.iSubMenusWithItems) i
		sHairdos.iSubMenusWithItems[i] = 0
		sHairdos.iSubMenusWithNEWItems[i] = 0
	ENDREPEAT
	sHairdos.iItemCount = 0
	
	INT iInterior1 = GET_HASH_KEY("v_hairdresser")
	INT iInterior2 = GET_HASH_KEY("v_barbers")
	INT iInterior3 = GET_HASH_KEY("vw_dlc_casino_apart")
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	IF (iInteriorCheck = iInterior1)
		// Salon
		eBaseShop = HAIRDO_SHOP_01_BH
	
		sTempLocateInfo.eType 				= LOCATE_TYPE_HAIRDOS
		sTempLocateInfo.vLocatePos			= << -816.09, -183.22, 38.0 >>
		
		sTempLocateInfo.vEntryPos[0] 		= <<-817.970520,-184.580215,36.568916>>
		sTempLocateInfo.vEntryPos[1] 		= <<-812.012878,-181.253891,38.568916>>
		sTempLocateInfo.fEntrySize[0] 		= 5.8
		
		sTempLocateInfo.vPlayerBrowsePos 	= << -816.279, -182.900, 36.588 >>
		sTempLocateInfo.vTempPos 			= << -816.279, -182.900, 36.588 >>
		sTempLocateInfo.vTempRot 			= << 0.000, 0.000, 120.000 >>
		
		sTempLocateInfo.vCoronaPos			= << -815.91992, -183.44055, 36.56892 >> // Auto trigger point
		
		// grab hairdo / beards info
		IF NETWORK_IS_GAME_IN_PROGRESS()
			ADD_MP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sHairdos, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, HAIR_GROUP_ALL, iHairdoGroupsAvailable, iHairdoGroupsWithNewItems)
		ELSE
			ADD_SP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sHairdos, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, TRUE)
		ENDIF
		
	ELIF (iInteriorCheck = iInterior2)
		// Barbers
		eBaseShop = HAIRDO_SHOP_02_SC
	
		sTempLocateInfo.eType		   	  	= LOCATE_TYPE_HAIRDOS
		sTempLocateInfo.vLocatePos  		= << 138.450, -1709.320, 30.00>>
		  
		sTempLocateInfo.vEntryPos[0] 		= << 139.691071,-1706.982178,28.291590>>
		sTempLocateInfo.vEntryPos[1] 		= << 136.352570,-1710.824829,30.301617>>
		sTempLocateInfo.fEntrySize[0] 		= 5.4
		
		sTempLocateInfo.vPlayerBrowsePos 	= << 136.6735, -1709.5129, 29.2964 >>
		sTempLocateInfo.vTempPos 			= << 138.353, -1709.266, 28.305 >>
		sTempLocateInfo.vTempRot 			= << 0.000, -0.000, -40.000 >>
		
		sTempLocateInfo.vCoronaPos			= << 138.00740, -1709.34302, 28.46070 >> // Auto trigger point
		
		// grab hairdo / beards info
		IF NETWORK_IS_GAME_IN_PROGRESS()
			ADD_MP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sHairdos, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, HAIR_GROUP_ALL, iHairdoGroupsAvailable, iHairdoGroupsWithNewItems)
		ELSE
			ADD_SP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sHairdos, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, FALSE)
		ENDIF
	ELIF (iInteriorCheck = iInterior3)
		// casino penthouse
		eBaseShop = HAIRDO_SHOP_01_BH
	
		sTempLocateInfo.eType 				= LOCATE_TYPE_HAIRDOS
		sTempLocateInfo.vLocatePos			= << -816.09, -183.22, 38.0 >>
		
		sTempLocateInfo.vEntryPos[0] 		= <<-816.799520,-184.675215,36.569916>>
		sTempLocateInfo.vEntryPos[1] 		= <<-815.539878,-180.635891,38.569916>>
		sTempLocateInfo.fEntrySize[0] 		= 5.8
		
		sTempLocateInfo.vPlayerBrowsePos 	= << -816.279, -182.900, 36.588 >>
		sTempLocateInfo.vTempPos 			= << -816.279, -182.900, 36.588 >>
		sTempLocateInfo.vTempRot 			= << 0.000, 0.000, 120.000 >>
		
		sTempLocateInfo.vCoronaPos			= << -815.91992, -183.44055, 36.56892 >> // Auto trigger point
		
		// grab hairdo / beards info
		IF NETWORK_IS_GAME_IN_PROGRESS()
			ADD_MP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sHairdos, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, HAIR_GROUP_ALL, iHairdoGroupsAvailable, iHairdoGroupsWithNewItems)
		ELSE
			ADD_SP_HAIRDOS_TO_LOCATE(fpSetupClothingItemForShop, sHairdos, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_NONE, TRUE)
		ENDIF
		
	ENDIF
	
	// Align vec data to shop
	ALIGN_SHOP_COORD(eBaseShop, eShop, sTempLocateInfo.vLocatePos)
	ALIGN_SHOP_COORD(eBaseShop, eShop, sTempLocateInfo.vEntryPos[0])
	ALIGN_SHOP_COORD(eBaseShop, eShop, sTempLocateInfo.vEntryPos[1])
	ALIGN_SHOP_COORD(eBaseShop, eShop, sTempLocateInfo.vPlayerBrowsePos)
	ALIGN_SHOP_COORD(eBaseShop, eShop, sTempLocateInfo.vCoronaPos)
	ALIGN_SHOP_COORD(eBaseShop, eShop, sTempLocateInfo.vTempPos)
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, sTempLocateInfo.vTempRot)
	
	// go to pos done separately as it is also used in the shop entry intros for the other customer
	GET_HAIRDO_SHOP_PLAYER_GO_TO_POS(eShop, sTempLocateInfo.vPlayerGoToPos)
	
	sLocate = sTempLocateInfo
	
	RETURN (sHairdos.iSubMenusWithItems[0] != 0 OR sHairdos.iSubMenusWithItems[1] != 0)
ENDFUNC

/// PURPOSE:
///    Sets up entry intro data for hairdo shop
///    
///    NOTE: uses sEntryInfo.iStage as follows
///    	0 = overview of shop
///	 	1 = close up of chair
///	 	2 = close up of hairdresser
///    
/// PARAMS:
///    eShop - the shop we are setting up
///    sEntryInfo - struct to store data
PROC GET_HAIRDO_SHOP_ENTRY_INTRO_DATA(SHOP_NAME_ENUM eShop, SHOP_ENTRY_INFO_STRUCT &sEntryInfo)

	INT iInterior1 = GET_HASH_KEY("v_hairdresser")
	INT iInterior2 = GET_HASH_KEY("v_barbers")
	INT iInterior3 = GET_HASH_KEY("vw_dlc_casino_apart")
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	SHOP_NAME_ENUM eBaseShop
	
	IF (iInteriorCheck = iInterior1)
		// ---SALON-------
		eBaseShop = HAIRDO_SHOP_01_BH
		
		// Start position
		sEntryInfo.vPlayerCoords[0] = << -820.49, -186.87, 36.5792 >>
		sEntryInfo.fPlayerHead[0] 	= 303.2416
		
		// Go to
		sEntryInfo.vPlayerCoords[1] = <<-820.4874, -186.8682, 36.5689>>
		sEntryInfo.fPlayerHead[1] 	= 297.7427
		
		SWITCH sEntryInfo.iStage
			// Overview of shop
			CASE 0
				sEntryInfo.vCamCoord[0] = <<-821.3704, -188.6373, 38.1144>>
				sEntryInfo.vCamRot[0] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[0] 	= 45.0
				
				sEntryInfo.vCamCoord[1] = <<-821.3514, -188.5915, 38.1065>>
				sEntryInfo.vCamRot[1] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[1] 	= 45.0
			BREAK
			
			// Close up of chair
			CASE 1
				sEntryInfo.vCamCoord[0] = <<-821.3514, -188.5915, 38.1065>>
				sEntryInfo.vCamRot[0] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[0] 	= 45.0
				
				sEntryInfo.vCamCoord[1] = <<-821.3514, -188.5915, 38.1065>>
				sEntryInfo.vCamRot[1] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[1] 	= 45.0
			BREAK
			
			// Close up of hairdresser
			CASE 2
				sEntryInfo.vCamCoord[0] = <<-816.8626, -187.4645, 38.2802>>
				sEntryInfo.vCamRot[0] 	= <<-12.0164, 0.0000, -0.8625>>
				sEntryInfo.fCamFov[0] 	= 35.0
				
				sEntryInfo.vCamCoord[1] = <<-816.8530, -187.3602, 38.2580>>
				sEntryInfo.vCamRot[1] 	= <<-12.0164, 0.0000, -0.8625>>
				sEntryInfo.fCamFov[1] 	= 35.0
			BREAK
		ENDSWITCH
	
	
	ELIF (iInteriorCheck = iInterior2)
		// ----BARBERS-----------
		eBaseShop = HAIRDO_SHOP_02_SC
		
		// Start position
		sEntryInfo.vPlayerCoords[0] = << 133.3136, -1711.1904, 28.2916 >>
		sEntryInfo.fPlayerHead[0] 	= 316.6123 
		
		// Go to
		sEntryInfo.vPlayerCoords[1] = <<134.7426, -1710.8428, 28.2916>>//<<134.7746, -1711.5789, 28.2916>>
		sEntryInfo.fPlayerHead[1] 	= 310.9816//316.6123
		
		SWITCH sEntryInfo.iStage
		
			// Shot of customer getting hair-cut
			CASE 0
				sEntryInfo.vCamCoord[0] = <<139.5276, -1708.9738, 29.7782>> 
				sEntryInfo.vCamRot[0] 	= <<-4.7210, 0.2142, 119.2654>>
				sEntryInfo.fCamFov[0] 	= 40.0
				
				sEntryInfo.vCamCoord[1]  = <<139.3900, -1709.0494, 29.7652>>
				sEntryInfo.vCamRot[1] 	= <<-4.7210, 0.2142, 119.2654>>
				sEntryInfo.fCamFov[1] 	= 40.0
			BREAK
			
			// Close up of barber / customer
			CASE 1
				sEntryInfo.vCamCoord[0]  = <<139.3900, -1709.0494, 29.7652>>
				sEntryInfo.vCamRot[0] 	= <<-4.7210, 0.2142, 119.2654>>
				sEntryInfo.fCamFov[0] 	= 40.0
				
				sEntryInfo.vCamCoord[1]  = <<139.3498, -1709.0717, 29.7614>>
				sEntryInfo.vCamRot[1] 	= <<-4.7210, 0.2142, 119.2654>>
				sEntryInfo.fCamFov[1] 	= 40.0
			BREAK
			
			// Behind barber, customer checks hair
			CASE 2
			
				sEntryInfo.vCamCoord[0] = <<138.1713, -1707.7611, 29.7857>>
				sEntryInfo.vCamRot[0] 	= <<-6.5899, 0.2142, -160.3469>>
				sEntryInfo.fCamFov[0] 	= 40.0
				
				sEntryInfo.vCamCoord[1]  = <<138.0825, -1707.5123, 29.8162>>
				sEntryInfo.vCamRot[1] 	= <<-6.5899, 0.2142, -160.3469>>
				sEntryInfo.fCamFov[1] 	= 40.0
			BREAK
			
			CASE 3
			BREAK
			
			// Last shot (gameplay cam blends from this)
			CASE 4
			
				sEntryInfo.vCamCoord[0] = <<134.0933, -1712.4443, 30.0197>>
				sEntryInfo.vCamRot[0] 	= <<-9.3416, 0.0000, -25.3724>>
				sEntryInfo.fCamFov[0] 	= 45.0
				
				sEntryInfo.vCamCoord[1]  = <<134.0933, -1712.4443, 30.0197>>
				sEntryInfo.vCamRot[1] 	= <<-9.3416, 0.0000, -25.3724>>
				sEntryInfo.fCamFov[1] 	= 45.0
			BREAK
			
			CASE 5
			BREAK
			
			// Last shot (gameplay cam blends from this) Here for skipping
			CASE 6
			
				sEntryInfo.vCamCoord[0] = <<134.0933, -1712.4443, 30.0197>>
				sEntryInfo.vCamRot[0] 	= <<-9.3416, 0.0000, -25.3724>>
				sEntryInfo.fCamFov[0] 	= 45.0
				
				sEntryInfo.vCamCoord[1]  = <<134.0933, -1712.4443, 30.0197>>
				sEntryInfo.vCamRot[1] 	= <<-9.3416, 0.0000, -25.3724>>
				sEntryInfo.fCamFov[1] 	= 45.0
			BREAK
			
		ENDSWITCH
	
	
	ELIF (iInteriorCheck = iInterior3)
		// ---CASINO PENTHOUSE-------
		eBaseShop = HAIRDO_SHOP_01_BH
		
		// Start position
		sEntryInfo.vPlayerCoords[0] = << -820.49, -186.87, 36.5792 >>
		sEntryInfo.fPlayerHead[0] 	= 303.2416
		
		// Go to
		sEntryInfo.vPlayerCoords[1] = <<-820.4874, -186.8682, 36.5689>>
		sEntryInfo.fPlayerHead[1] 	= 297.7427
		
		SWITCH sEntryInfo.iStage
			// Overview of shop
			CASE 0
				sEntryInfo.vCamCoord[0] = <<-821.3704, -188.6373, 38.1144>>
				sEntryInfo.vCamRot[0] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[0] 	= 45.0
				
				sEntryInfo.vCamCoord[1] = <<-821.3514, -188.5915, 38.1065>>
				sEntryInfo.vCamRot[1] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[1] 	= 45.0
			BREAK
			
			// Close up of chair
			CASE 1
				sEntryInfo.vCamCoord[0] = <<-821.3514, -188.5915, 38.1065>>
				sEntryInfo.vCamRot[0] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[0] 	= 45.0
				
				sEntryInfo.vCamCoord[1] = <<-821.3514, -188.5915, 38.1065>>
				sEntryInfo.vCamRot[1] 	= <<-9.1377, -0.0000, -26.8098>>
				sEntryInfo.fCamFov[1] 	= 45.0
			BREAK
			
			// Close up of hairdresser
			CASE 2
				sEntryInfo.vCamCoord[0] = <<-816.8626, -187.4645, 38.2802>>
				sEntryInfo.vCamRot[0] 	= <<-12.0164, 0.0000, -0.8625>>
				sEntryInfo.fCamFov[0] 	= 35.0
				
				sEntryInfo.vCamCoord[1] = <<-816.8530, -187.3602, 38.2580>>
				sEntryInfo.vCamRot[1] 	= <<-12.0164, 0.0000, -0.8625>>
				sEntryInfo.fCamFov[1] 	= 35.0
			BREAK
		ENDSWITCH
	
	
	ENDIF
	
	// Align vec data to shop
	ALIGN_SHOP_COORD(eBaseShop, eShop, sEntryInfo.vPlayerCoords[0])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sEntryInfo.fPlayerHead[0])
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sEntryInfo.vPlayerCoords[1])
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sEntryInfo.fPlayerHead[1])
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sEntryInfo.vCamCoord[0])
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, sEntryInfo.vCamRot[0])
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sEntryInfo.vCamCoord[1])
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, sEntryInfo.vCamRot[1])
ENDPROC


// --------------------------Beards-----------------------------------------------

FUNC INT GET_BEARD_INDEX_FROM_MENU_INDEX(INT iMenuIndex)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN iMenuIndex
	ENDIF
	
	IF NOT IS_MP_HIPSTER_PACK_PRESENT()
		RETURN iMenuIndex
	ENDIF
	
	SWITCH iMenuIndex
		// Clean Shaven
		CASE 0 	RETURN 0
		
		// Hipster x10
		CASE 1 	RETURN 27
		CASE 2 	RETURN 28
		CASE 3 	RETURN 29
		CASE 4 	RETURN 30
		CASE 5 	RETURN 31
		CASE 6 	RETURN 32
		CASE 7 	RETURN 33
		CASE 8 	RETURN 34
		CASE 9 	RETURN 35
		CASE 10 RETURN 36
		
		// Standard x19
		CASE 11 RETURN 1
		CASE 12 RETURN 2
		CASE 13 RETURN 3
		CASE 14 RETURN 4
		CASE 15 RETURN 5
		CASE 16 RETURN 6
		CASE 17 RETURN 7
		CASE 18 RETURN 8
		CASE 19 RETURN 9
		CASE 20 RETURN 10
		CASE 21 RETURN 11
		CASE 22 RETURN 12
		CASE 23 RETURN 13
		CASE 24 RETURN 14
		CASE 25 RETURN 15
		CASE 26 RETURN 16
		CASE 27 RETURN 17
		CASE 28 RETURN 18
		CASE 29 RETURN 19
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_MENU_INDEX_FROM_BEARD_INDEX(INT iBeardIndex)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN iBeardIndex
	ENDIF
	
	IF NOT IS_MP_HIPSTER_PACK_PRESENT()
		RETURN iBeardIndex
	ENDIF
	
	SWITCH iBeardIndex
		// Clean Shaven
		CASE 0 	RETURN 0
		
		// Hipster x10
		CASE 27	RETURN 1
		CASE 28	RETURN 2
		CASE 29	RETURN 3
		CASE 30	RETURN 4
		CASE 31	RETURN 5
		CASE 32	RETURN 6
		CASE 33	RETURN 7
		CASE 34	RETURN 8
		CASE 35	RETURN 9
		CASE 36 RETURN 10
		
		// Standard x18
		CASE 1 	RETURN 11
		CASE 2 	RETURN 12
		CASE 3 	RETURN 13
		CASE 4 	RETURN 14
		CASE 5 	RETURN 15
		CASE 6 	RETURN 16
		CASE 7 	RETURN 17
		CASE 8 	RETURN 18
		CASE 9 	RETURN 19
		CASE 10 RETURN 20
		CASE 11 RETURN 21
		CASE 12 RETURN 22
		CASE 13 RETURN 23
		CASE 14 RETURN 24
		CASE 15 RETURN 25
		CASE 16 RETURN 26
		CASE 17 RETURN 27
		CASE 18 RETURN 28
		CASE 19 RETURN 29
	ENDSWITCH
	RETURN -1
ENDFUNC


FUNC CLOTHES_UNLOCK_ITEMS GET_BEARD_UNLOCK_ENUM_FOR_SHOP(INT iBeardIndex)
	// Info comes from bug 969692
	SWITCH iBeardIndex		
		CASE 0 RETURN (BEARD_UNLOCKS_CLEANSHAVEN) // Clean Shaven - Rank 0
		CASE 1 RETURN (BEARD_UNLOCKS_LIGHTBEARD) // Light Stubble  - Rank 0
		CASE 2 RETURN (BEARD_UNLOCKS_BALBO) // Balbo - Rank 10
		CASE 3 RETURN (BEARD_UNLOCKS_CIRCLEBEARD) // Circle Beard - Rank 10
		CASE 4 RETURN (BEARD_UNLOCKS_GOATEE) // Goatee - Rank 0
		CASE 5 RETURN (BEARD_UNLOCKS_CHIN) // Chin - Rank 10
		CASE 6 RETURN (BEARD_UNLOCKS_SOULPATCH) // Soul Patch - Rank 0
		CASE 7 RETURN (BEARD_UNLOCKS_PENCILCHINSTRAP) // Pencil Chin Strap - Rank 10
		CASE 8 RETURN (BEARD_UNLOCKS_LIGHTBEARD) // Light Beard
		CASE 9 	RETURN (BEARD_UNLOCKS_MUSKETEER) // Musketeer - Rank 10
		CASE 10 RETURN (BEARD_UNLOCKS_MOUSTACHE) // Moustache - Rank 0
		CASE 11 RETURN (BEARD_UNLOCKS_HEAVYBEARD) // Heavy Beard
		CASE 12 RETURN (BEARD_UNLOCKS_STUBBLE) // Stubble - Rank 10
		CASE 13 RETURN (BEARD_UNLOCKS_CIRCLEBEARD2) // Circle Beard 2
		CASE 14 RETURN (BEARD_UNLOCKS_HORSESHOEANDSIDEBURNS) // Horseshoe and Sideburns
		CASE 15 RETURN (BEARD_UNLOCKS_PENCILMOUSTACHEANDMUTTONCHOPS) // Pencil Moustache and Mutton Chops
		CASE 16 RETURN (BEARD_UNLOCKS_PENCILMOUSTACHEANDCHINSTRAP) // Pencil Moustache and Chin Strap
		CASE 17 RETURN (BEARD_UNLOCKS_BALBOANDDESIGNSIDEBURNS) // Balbo and Design Sideburns
		CASE 18 RETURN (BEARD_UNLOCKS_MUTTONCHOPS) // Mutton Chops
		CASE 19 RETURN (BEARD_UNLOCKS_FULLBEARD) // Full Beard - Rank 0
		CASE 20 RETURN (BEARD_UNLOCKS_FULLBEARD) // Beard 20
		CASE 27 RETURN (BEARD_UNLOCKS_CURLY) // 
		CASE 28 RETURN (BEARD_UNLOCKS_CURLY_AND_DEEP_STRANGER) // 
		CASE 29 RETURN (BEARD_UNLOCKS_HANDLEBAR) // 
		CASE 30 RETURN (BEARD_UNLOCKS_FAUSTIC) // 
		CASE 31 RETURN (BEARD_UNLOCKS_OTTO_AND_PATCH) // 
		CASE 32 RETURN (BEARD_UNLOCKS_OTTO_AND_FULL_STRANGER) // 
		CASE 33 RETURN (BEARD_UNLOCKS_LIGHT_FRANZ) // 
		CASE 36 RETURN (BEARD_UNLOCKS_LINCOLN_CURTAIN) // 
		CASE 34 RETURN (BEARD_UNLOCKS_THE_HAMPSTEAD) // 
		CASE 35 RETURN (BEARD_UNLOCKS_THE_AMBROSE) // 
	ENDSWITCH
	
	RETURN BEARD_UNLOCKS_CLEANSHAVEN
ENDFUNC


/// PURPOSE:
///    Checks if the beard specified is unlocked
/// PARAMS:
///    iBeardIndex - the beard we are checking
/// RETURNS:
///    TRUE if unlocked. FALSE otherwise
FUNC BOOL IS_BEARD_UNLOCKED_FOR_SHOP(INT iBeardIndex)
	
	IF iBeardIndex >= ENUM_TO_INT(MP_BEARDS_MAX)+1 // +1 for clean shaven
		//Hipster Beards
		IF iBeardIndex >= 27 AND iBeardIndex <= 36
			RETURN TRUE
		ENDIF
		
		//non-hipster beards
		RETURN FALSE
	ENDIF
	
	// Info comes from bug 969692
	RETURN IS_MP_CLOTHES_UNLOCKED(GET_BEARD_UNLOCK_ENUM_FOR_SHOP(iBeardIndex))
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_BEARD_BEEN_VIEWED_IN_SHOP(INT iBeardIndex)
	RETURN (HAS_CLOTHES_UNLOCK_BEEN_VIEWED_IN_SHOP(GET_BEARD_UNLOCK_ENUM_FOR_SHOP(iBeardIndex)))
ENDFUNC

PROC SET_BEARD_BEEN_VIEWED_IN_SHOP(INT iBeardIndex)
	SET_CLOTHES_UNLOCK_HAS_BEEN_VIEWED_IN_SHOP(GET_BEARD_UNLOCK_ENUM_FOR_SHOP(iBeardIndex))
ENDPROC

/// PURPOSE:
///    Checks if current player has beard options available
/// RETURNS:
///    TRUE if current player has available beard options. FALSE otherwise
FUNC BOOL DOES_PLAYER_HAVE_BEARD_OPTIONS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE // all SP characters have beard option
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Determines the current beard in the menu list
PROC GET_CURRENT_BEARD_ITEM(HAIRDO_SHOP_STRUCT &sData, BOOL bSetCurrentItem)

	IF IS_SHOP_PED_OK()
		IF sData.iCurrentBeard = -1
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.iCurrentBeard = GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BEARD) + 1
				
				// coming in with no beard is setting this value to 256 for some strange reason
				IF (sData.iCurrentBeard = 256)
					sData.iCurrentBeard = 0
				ENDIF
				
				sData.fCurrentBeardFade = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BEARD_PC)
			ELSE
				sData.iCurrentBeard = ENUM_TO_INT(CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD))
				sData.fCurrentBeardFade = 1.0
			ENDIF
			
			sData.iOriginalBeard = sData.iCurrentBeard
		ENDIF
		
		IF bSetCurrentItem
			sData.sBrowseInfo.iCurrentItem = GET_MENU_INDEX_FROM_BEARD_INDEX(sData.iCurrentBeard)
		ENDIF
	ENDIF
ENDPROC

// ----------------------------Make up ----------------------------------------------------


FUNC BOOL IS_SPORTS_FANATIC_DLC_INSTALLED_FOR_MAKEUP()
	//TODO Fill this in when dlc is setup and we get a command to check it
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_INDEPENDENCE_DLC_INSTALLED_FOR_MAKEUP()
	IF IS_MP_INDEPENDENCE_PACK_PRESENT()
	AND (g_sMPTunables.btoggleactivateIndependencepack #IF IS_DEBUG_BUILD OR g_bDebugAllowIndependenceItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_FACEPAINT))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_ARMY
FUNC BOOL IS_ARMY_DLC_INSTALLED_FOR_MAKEUP()
	IF IS_MP_ARMY_PACK_PRESENT()
/*	AND (g_sMPTunables.btoggleactivatearmypendencepack #IF IS_DEBUG_BUILD OR g_bDebugAllowarmypendenceItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_FACEPAINT))	*/
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC CLOTHES_UNLOCK_ITEMS GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(INT iMakeupIndex)
	SWITCH iMakeupIndex		
		CASE 0 RETURN MAKEUP_UNLOCKS_NUDE
		CASE 1 RETURN MAKEUP_UNLOCKS_BASIC
		CASE 2 RETURN MAKEUP_UNLOCKS_SMOKY
		CASE 3 RETURN MAKEUP_UNLOCKS_GOTHIC
		CASE 4 RETURN MAKEUP_UNLOCKS_ROCKER
		CASE 5 RETURN MAKEUP_UNLOCKS_PARTYGIRL
		CASE 6 RETURN MAKEUP_UNLOCKS_ARTSY
		CASE 7 RETURN MAKEUP_UNLOCKS_TRAILERPARKPRINCESS
		CASE 8 RETURN MAKEUP_UNLOCKS_SOCCERMOM
		CASE 9 RETURN MAKEUP_UNLOCKS_FEMMEFATALE
		CASE 10 RETURN MAKEUP_UNLOCKS_SERIOUSLYCERISE
		CASE 11 RETURN MAKEUP_UNLOCKS_DISCOTEQUEWRECK
		CASE 12 RETURN MAKEUP_UNLOCKS_BEAUTYSPOT
		CASE 13 RETURN MAKEUP_UNLOCKS_TONEDDOWN
		CASE 14 RETURN MAKEUP_UNLOCKS_CYANSWIPED
		CASE 15 RETURN MAKEUP_UNLOCKS_MORNINGAFTER
		CASE 16 RETURN MAKEUP_UNLOCKS_COVERGIRL
		CASE 17 RETURN MAKEUP_UNLOCKS_KISSMYAXE
		CASE 18 RETURN MAKEUP_UNLOCKS_PANDAPUSSY
		CASE 19 RETURN MAKEUP_UNLOCKS_THEBAT
		CASE 20 RETURN MAKEUP_UNLOCKS_SKULLINSCARLET
		CASE 21 RETURN MAKEUP_UNLOCKS_SERPENTINE		
		CASE 22 RETURN MAKEUP_UNLOCKS_THEVELDT		
		CASE 23 RETURN MAKEUP_UNLOCKS_THEJOCK		
		CASE 24 RETURN MAKEUP_UNLOCKS_LOSSANTOSCORKERS		
		CASE 25 RETURN MAKEUP_UNLOCKS_LOSSANTOSPANIC		
		CASE 26 RETURN MAKEUP_UNLOCKS_LIBERTYCITYSWINGERS		
		CASE 27 RETURN MAKEUP_UNLOCKS_TRIBALLINES		
		CASE 28 RETURN MAKEUP_UNLOCKS_TRIBALSWIRLS		
		CASE 29 RETURN MAKEUP_UNLOCKS_TRIBALORANGE		
		CASE 30 RETURN MAKEUP_UNLOCKS_TRIBALRED		
		CASE 31 RETURN MAKEUP_UNLOCKS_TRAPPEDINABOX		
		CASE 32 RETURN MAKEUP_UNLOCKS_CLOWNING		
		CASE 33 RETURN MAKEUP_UNLOCKS_GUYLINER
		CASE 34 RETURN MAKEUP_UNLOCKS_STARS_N_STRIPES
		
		CASE 35 RETURN MAKEUP_UNLOCKS_HALLOWEEN_00
		CASE 36 RETURN MAKEUP_UNLOCKS_HALLOWEEN_01
		CASE 37 RETURN MAKEUP_UNLOCKS_HALLOWEEN_02
		CASE 38 RETURN MAKEUP_UNLOCKS_HALLOWEEN_03
		CASE 39 RETURN MAKEUP_UNLOCKS_HALLOWEEN_04
		CASE 40 RETURN MAKEUP_UNLOCKS_HALLOWEEN_05
		CASE 41 RETURN MAKEUP_UNLOCKS_HALLOWEEN_06
		CASE 42 RETURN MAKEUP_UNLOCKS_HALLOWEEN_07
		CASE 43 RETURN MAKEUP_UNLOCKS_HALLOWEEN_08
		CASE 44 RETURN MAKEUP_UNLOCKS_HALLOWEEN_09
		CASE 45 RETURN MAKEUP_UNLOCKS_HALLOWEEN_10
		CASE 46 RETURN MAKEUP_UNLOCKS_HALLOWEEN_11
		CASE 47 RETURN MAKEUP_UNLOCKS_HALLOWEEN_12
		CASE 48 RETURN MAKEUP_UNLOCKS_HALLOWEEN_13
		CASE 49 RETURN MAKEUP_UNLOCKS_HALLOWEEN_14
		CASE 50 RETURN MAKEUP_UNLOCKS_HALLOWEEN_15
		CASE 51 RETURN MAKEUP_UNLOCKS_HALLOWEEN_16
		CASE 52 RETURN MAKEUP_UNLOCKS_HALLOWEEN_17
		CASE 53 RETURN MAKEUP_UNLOCKS_HALLOWEEN_18
		CASE 54 RETURN MAKEUP_UNLOCKS_HALLOWEEN_19
		CASE 55 RETURN MAKEUP_UNLOCKS_HALLOWEEN_20
		CASE 56 RETURN MAKEUP_UNLOCKS_HALLOWEEN_21
		CASE 57 RETURN MAKEUP_UNLOCKS_HALLOWEEN_22
		CASE 58 RETURN MAKEUP_UNLOCKS_HALLOWEEN_23
		CASE 59 RETURN MAKEUP_UNLOCKS_HALLOWEEN_24
		CASE 60 RETURN MAKEUP_UNLOCKS_HALLOWEEN_25
		CASE 61 RETURN MAKEUP_UNLOCKS_HALLOWEEN_26
		CASE 62 RETURN MAKEUP_UNLOCKS_HALLOWEEN_27
		CASE 63 RETURN MAKEUP_UNLOCKS_HALLOWEEN_28
		CASE 64 RETURN MAKEUP_UNLOCKS_HALLOWEEN_29
		
		CASE 65 RETURN MAKEUP_UNLOCKS_HEIST4_00
		CASE 66 RETURN MAKEUP_UNLOCKS_HEIST4_01
		CASE 67 RETURN MAKEUP_UNLOCKS_HEIST4_02
		CASE 68 RETURN MAKEUP_UNLOCKS_HEIST4_03
		CASE 69 RETURN MAKEUP_UNLOCKS_HEIST4_04
		CASE 70 RETURN MAKEUP_UNLOCKS_HEIST4_05
		CASE 71 RETURN MAKEUP_UNLOCKS_HEIST4_06
		CASE 72 RETURN MAKEUP_UNLOCKS_HEIST4_07
		CASE 73 RETURN MAKEUP_UNLOCKS_HEIST4_08
		
		#IF FEATURE_FIXER
		CASE 74 RETURN MAKEUP_UNLOCKS_FIXER_00
		CASE 75 RETURN MAKEUP_UNLOCKS_FIXER_01
		CASE 76 RETURN MAKEUP_UNLOCKS_FIXER_02
		CASE 77 RETURN MAKEUP_UNLOCKS_FIXER_03
		CASE 78 RETURN MAKEUP_UNLOCKS_FIXER_04
		CASE 79 RETURN MAKEUP_UNLOCKS_FIXER_05
		CASE 80 RETURN MAKEUP_UNLOCKS_FIXER_06
		CASE 81 RETURN MAKEUP_UNLOCKS_FIXER_07
		CASE 82 RETURN MAKEUP_UNLOCKS_FIXER_08
		CASE 83 RETURN MAKEUP_UNLOCKS_FIXER_09
		CASE 84 RETURN MAKEUP_UNLOCKS_FIXER_10
		CASE 85 RETURN MAKEUP_UNLOCKS_FIXER_11
		CASE 86 RETURN MAKEUP_UNLOCKS_FIXER_12
		CASE 87 RETURN MAKEUP_UNLOCKS_FIXER_13
		#ENDIF
	ENDSWITCH									
												
	RETURN MAKEUP_UNLOCKS_NUDE					
ENDFUNC



/// PURPOSE:
///    Checks if the makeup specified is unlocked
/// PARAMS:
///    iMakeupIndex - the makeup we are checking
/// RETURNS:
///    TRUE if unlocked. FALSE otherwise
FUNC BOOL IS_MAKEUP_UNLOCKED_FOR_SHOP(INT iMakeupIndex)
	RETURN IS_MP_CLOTHES_UNLOCKED(GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(iMakeupIndex))

	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_MAKEUP_BEEN_VIEWED_IN_SHOP(INT iMakeupIndex)
	RETURN (HAS_CLOTHES_UNLOCK_BEEN_VIEWED_IN_SHOP(GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(iMakeupIndex)))
ENDFUNC

PROC SET_MAKEUP_BEEN_VIEWED_IN_SHOP(INT iMakeupIndex)
	SET_CLOTHES_UNLOCK_HAS_BEEN_VIEWED_IN_SHOP(GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(iMakeupIndex))
ENDPROC

#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
/// PURPOSE:
///    Set makeup options and prices
PROC FILL_MAKEUP_DATA(MAKEUP_DATA_STRUCT &sMakeup[])
	
	sMakeup[0].sLabel = "HAIR_MKUP0"
	sMakeup[0].iCost = 100  sMakeup[0].iCost = ROUND((TO_FLOAT(sMakeup[0].iCost) * g_sMPTunables.fmakeup_unlocks_nude_expenditure_tunable)) 
	
	sMakeup[1].sLabel = "HAIR_MKUP1"
	sMakeup[1].iCost = 225  sMakeup[1].iCost = ROUND((TO_FLOAT(sMakeup[1].iCost) * g_sMPTunables.fmakeup_unlocks_basic_expenditure_tunable)) 
	
	sMakeup[2].sLabel = "HAIR_MKUP2"
	sMakeup[2].iCost = 250  sMakeup[2].iCost = ROUND((TO_FLOAT(sMakeup[2].iCost) * g_sMPtunables.fmakeup_unlocks_smoky_expenditure_tunable)) 
	
	sMakeup[3].sLabel = "HAIR_MKUP3"
	sMakeup[3].iCost = 175  sMakeup[3].iCost = ROUND((TO_FLOAT(sMakeup[3].iCost) * g_sMPTunables.fmakeup_unlocks_gothic_expenditure_tunable)) 
	
	sMakeup[4].sLabel = "HAIR_MKUP4"
	sMakeup[4].iCost = 610  sMakeup[4].iCost = ROUND((TO_FLOAT(sMakeup[4].iCost) * g_sMPTunables.fmakeup_unlocks_rocker_expenditure_tunable)) 
	
	sMakeup[5].sLabel = "HAIR_MKUP5"
	sMakeup[5].iCost = 150  sMakeup[5].iCost = ROUND((TO_FLOAT(sMakeup[5].iCost) * g_sMPTunables.fmakeup_unlocks_partygirl_expenditure_tunable)) 
	
	sMakeup[6].sLabel = "HAIR_MKUP6"
	sMakeup[6].iCost = 200  sMakeup[6].iCost = ROUND((TO_FLOAT(sMakeup[6].iCost) * g_sMPTunables.fmakeup_unlocks_artsy_expenditure_tunable)) 
	
	sMakeup[7].sLabel = "HAIR_MKUP7"
	sMakeup[7].iCost = 870  sMakeup[7].iCost = ROUND((TO_FLOAT(sMakeup[7].iCost) * g_sMPTunables.fmakeup_unlocks_trailerparkprincess_expenditure_tunable)) 
	
	sMakeup[8].sLabel = "HAIR_MKUP8"
	sMakeup[8].iCost = 275  sMakeup[8].iCost = ROUND((TO_FLOAT(sMakeup[8].iCost) * g_sMPTunables.fmakeup_unlocks_soccermom_expenditure_tunable)) 
	
	sMakeup[9].sLabel = "HAIR_MKUP9"
	sMakeup[9].iCost = 480  sMakeup[9].iCost = ROUND((TO_FLOAT(sMakeup[9].iCost) * g_sMPTunables.fmakeup_unlocks_femmefatale_expenditure_tunable)) 
	
	sMakeup[10].sLabel = "HAIR_MKUP10"
	sMakeup[10].iCost = 2000  sMakeup[10].iCost = ROUND((TO_FLOAT(sMakeup[10].iCost) * g_sMPTunables.fmakeup_unlocks_seriouslycerise_expenditure_tunable)) 
	
	sMakeup[11].sLabel = "HAIR_MKUP11"
	sMakeup[11].iCost = 740  sMakeup[11].iCost = ROUND((TO_FLOAT(sMakeup[11].iCost) * g_sMPTunables.fmakeup_unlocks_discotequewreck_expenditure_tunable)) 
	
	sMakeup[12].sLabel = "HAIR_MKUP12"
	sMakeup[12].iCost = 935  sMakeup[12].iCost = ROUND((TO_FLOAT(sMakeup[12].iCost) * g_sMPTunables.fmakeup_unlocks_beautyspot_expenditure_tunable)) 
	
	sMakeup[13].sLabel = "HAIR_MKUP13"
	sMakeup[13].iCost = 675  sMakeup[13].iCost = ROUND((TO_FLOAT(sMakeup[13].iCost) * g_sMPTunables.fmakeup_unlocks_toneddown_expenditure_tunable)) 
	
	sMakeup[14].sLabel = "HAIR_MKUP14"
	sMakeup[14].iCost = 1000  sMakeup[14].iCost = ROUND((TO_FLOAT(sMakeup[14].iCost) * g_sMPTunables.fmakeup_unlocks_cyanswiped_expenditure_tunable)) 
	
	sMakeup[15].sLabel = "HAIR_MKUP15"
	sMakeup[15].iCost = 805  sMakeup[15].iCost = ROUND((TO_FLOAT(sMakeup[15].iCost) * g_sMPTunables.fmakeup_unlocks_morningafter_expenditure_tunable)) 
	
	sMakeup[16].sLabel = "HAIR_MKUP16"
	sMakeup[16].iCost = 545  sMakeup[16].iCost = ROUND((TO_FLOAT(sMakeup[16].iCost) * g_sMPTunables.fmakeup_unlocks_covergirl_expenditure_tunable)) 
	
	sMakeup[17].sLabel = "HAIR_MKUP17"
	sMakeup[17].iCost = 500  sMakeup[17].iCost = ROUND((TO_FLOAT(sMakeup[17].iCost) * g_sMPTunables.fmakeup_unlocks_kissmyaxe_expenditure_tunable)) 
	
	sMakeup[18].sLabel = "HAIR_MKUP18"
	sMakeup[18].iCost = 1000  sMakeup[18].iCost = ROUND((TO_FLOAT(sMakeup[18].iCost) * g_sMPTunables.fmakeup_unlocks_pandapussy_expenditure_tunable)) 
	
	sMakeup[19].sLabel = "HAIR_MKUP19"
	sMakeup[19].iCost = 750  sMakeup[19].iCost = ROUND((TO_FLOAT(sMakeup[19].iCost) * g_sMPTunables.fmakeup_unlocks_thebat_expenditure_tunable)) 
	
	sMakeup[20].sLabel = "HAIR_MKUP20"
	sMakeup[20].iCost = 5750  sMakeup[20].iCost = ROUND((TO_FLOAT(sMakeup[20].iCost) * g_sMPTunables.fmakeup_unlocks_skullinscarlet_expenditure_tunable)) 
	
	sMakeup[21].sLabel = "HAIR_MKUP21"
	sMakeup[21].iCost = 10000  sMakeup[21].iCost = ROUND((TO_FLOAT(sMakeup[21].iCost) * g_sMPTunables.fmakeup_unlocks_serpentine_expenditure_tunable)) 
	
	sMakeup[22].sLabel = "HAIR_MKUP22"
	sMakeup[22].iCost = 6000  sMakeup[22].iCost = ROUND((TO_FLOAT(sMakeup[22].iCost) * g_sMPTunables.fmakeup_unlocks_theveldt_expenditure_tunable)) 
	
	sMakeup[23].sLabel = "HAIR_MKUP23"
	sMakeup[23].iCost = 0  sMakeup[23].iCost = ROUND((TO_FLOAT(sMakeup[23].iCost) * g_sMPTunables.fmakeup_unlocks_thejock_expenditure_tunable)) 
	
	sMakeup[24].sLabel = "HAIR_MKUP24"
	sMakeup[24].iCost = 0  sMakeup[24].iCost = ROUND((TO_FLOAT(sMakeup[24].iCost) * g_sMPTunables.fmakeup_unlocks_lossantoscorkers_expenditure_tunable)) 
	
	sMakeup[25].sLabel = "HAIR_MKUP25"
	sMakeup[25].iCost = 0  sMakeup[25].iCost = ROUND((TO_FLOAT(sMakeup[25].iCost) * g_sMPTunables.fmakeup_unlocks_lossantospanic_expenditure_tunable)) 
	
	sMakeup[26].sLabel = "HAIR_MKUP26"
	sMakeup[26].iCost = 0  sMakeup[26].iCost = ROUND((TO_FLOAT(sMakeup[26].iCost) * g_sMPTunables.fmakeup_unlocks_libertycityswingers_expenditure_tunable)) 
	
	sMakeup[27].sLabel = "HAIR_MKUP27"
	sMakeup[27].iCost = 5000  sMakeup[27].iCost = ROUND((TO_FLOAT(sMakeup[27].iCost) * g_sMPTunables.fmakeup_unlocks_triballines_expenditure_tunable)) 
	
	sMakeup[28].sLabel = "HAIR_MKUP28"
	sMakeup[28].iCost = 5500  sMakeup[28].iCost = ROUND((TO_FLOAT(sMakeup[28].iCost) * g_sMPTunables.fmakeup_unlocks_tribalswirls_expenditure_tunable)) 
	
	sMakeup[29].sLabel = "HAIR_MKUP29"
	sMakeup[29].iCost = 1250  sMakeup[29].iCost = ROUND((TO_FLOAT(sMakeup[29].iCost) * g_sMPTunables.fmakeup_unlocks_tribalorange_expenditure_tunable)) 
	
	sMakeup[30].sLabel = "HAIR_MKUP30"
	sMakeup[30].iCost = 2000  sMakeup[30].iCost = ROUND((TO_FLOAT(sMakeup[30].iCost) * g_sMPTunables.fmakeup_unlocks_tribalred_expenditure_tunable)) 
	
	sMakeup[31].sLabel = "HAIR_MKUP31"
	sMakeup[31].iCost = 15000  sMakeup[31].iCost = ROUND((TO_FLOAT(sMakeup[31].iCost) * g_sMPTunables.fmakeup_unlocks_trappedinabox_expenditure_tunable)) 
	
	sMakeup[32].sLabel = "HAIR_MKUP32"
	sMakeup[32].iCost = 20000  sMakeup[32].iCost = ROUND((TO_FLOAT(sMakeup[32].iCost) * g_sMPTunables.fmakeup_unlocks_clowning_expenditure_tunable)) 
	
	sMakeup[33].sLabel = "HAIR_MKUP33"
	sMakeup[33].iCost = 300  sMakeup[33].iCost = ROUND((TO_FLOAT(sMakeup[33].iCost) * g_sMPTunables.fmakeup_unlocks_guyliner_expenditure_tunable)) 
	
	IF IS_INDEPENDENCE_DLC_INSTALLED_FOR_MAKEUP()
		sMakeup[34].sLabel = "MKUP_IND_0"	//Stars n Stripes Facepaint
		sMakeup[34].iCost = 25000
	ELSE
		sMakeup[34].sLabel = ""		sMakeup[34].iCost = -1
	ENDIF
		
	sMakeup[35].sLabel = ""		sMakeup[35].iCost = -1
	sMakeup[36].sLabel = ""		sMakeup[36].iCost = -1
	sMakeup[37].sLabel = ""		sMakeup[37].iCost = -1
	sMakeup[38].sLabel = ""		sMakeup[38].iCost = -1
	sMakeup[39].sLabel = ""		sMakeup[39].iCost = -1
	sMakeup[40].sLabel = ""		sMakeup[40].iCost = -1
	sMakeup[41].sLabel = ""		sMakeup[41].iCost = -1
	sMakeup[42].sLabel = ""		sMakeup[42].iCost = -1
	sMakeup[43].sLabel = ""		sMakeup[43].iCost = -1
		
	#IF FEATURE_ARMY
	IF IS_ARMY_DLC_INSTALLED_FOR_MAKEUP()
		sMakeup[35].sLabel = "MKUP_ARM_0"	//Army Face Paint A
		sMakeup[35].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[36].sLabel = "MKUP_ARM_1"	//Army Face Paint B
		sMakeup[36].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[37].sLabel = "MKUP_ARM_2"	//Army Face Paint C
		sMakeup[37].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[38].sLabel = "MKUP_ARM_3"	//Army Face Paint D
		sMakeup[38].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[39].sLabel = "MKUP_ARM_4"	//Army Face Paint E
		sMakeup[39].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[40].sLabel = "MKUP_ARM_5"	//Army Face Paint F
		sMakeup[40].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[41].sLabel = "MKUP_ARM_6"	//Army Face Paint G
		sMakeup[41].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[42].sLabel = "MKUP_ARM_7"	//Army Face Paint H
		sMakeup[42].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
		
		sMakeup[43].sLabel = "MKUP_ARM_8"	//Army Face Paint I
		sMakeup[43].iCost = GET_RANDOM_INT_IN_RANGE(100, 1000)*10
	ENDIF
	#ENDIF
ENDPROC
#ENDIF

/// PURPOSE: Determines the current beard in the menu list
PROC GET_CURRENT_MAKEUP_ITEM(HAIRDO_SHOP_STRUCT &sData, BOOL bSetCurrentItem)

	IF IS_SHOP_PED_OK()
		IF sData.iPlayerMakeup = -1
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.iPlayerMakeup = GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP) + 1
				
				// coming in with no beard is setting this value to 256 for some strange reason
				IF (sData.iPlayerMakeup = 256)
					sData.iPlayerMakeup = 0
				ENDIF
			ENDIF
			
			sData.iOriginalMakeup = sData.iPlayerMakeup
		ENDIF
		
		IF bSetCurrentItem
			sData.sBrowseInfo.iCurrentItem = sData.iPlayerMakeup 
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if current player has makeup options available 
/// RETURNS:
///    TRUE if current player has available makeup options. FALSE otherwise
FUNC BOOL DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
	
	// Assuming both male and female freemode characters can have makeup for now.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
			OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_HAIRDO_SHOP_KEY_VARIANT(SHOP_NAME_ENUM eShop)
	IF (eShop = HAIRDO_SHOP_CASINO_APT)
		RETURN 1
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL IS_DLC_INSTALLED_FOR_NGMP_ITEM(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iOption, SHOP_NAME_ENUM eShop)
	IF ePedModel = MP_M_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR
				IF (eShop = HAIRDO_SHOP_CASINO_APT)
					IF iOption = 22
					OR iOption = 23
					OR iOption = 24
					OR iOption = 25
					OR iOption = 26
					OR iOption = 27
					OR iOption = 28
					OR iOption = 29
					OR iOption = 30
					OR iOption = 31
					OR iOption = 32
					OR iOption = 33
					OR iOption = 34
					OR iOption = 35
						RETURN FALSE
					ENDIF
				ENDIF
				
				SWITCH iOption
					CASE 22				// Mullet
						IF IS_MP_INDEPENDENCE_PACK_PRESENT()
						AND (g_sMPTunables.btoggleactivateIndependencepack #IF IS_DEBUG_BUILD OR g_bDebugAllowIndependenceItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_MULLET))
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male mullet not installed")
						RETURN FALSE
					BREAK
					CASE 23 FALLTHRU	// Classic Cornrows
					CASE 24 FALLTHRU	// Palm Cornrows
					CASE 25 FALLTHRU	// Lightning Cornrows
					CASE 26				// Whipped Cornrows
						IF IS_MP_LOWRIDER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male lowrider pack not installed")
						RETURN FALSE
					BREAK
					CASE 27 FALLTHRU	// Zig Zag Cornrows
					CASE 28 FALLTHRU	// Snail Cornrows
					CASE 29				// Hightop
						IF IS_MP_LOWRIDER2_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male lowrider2 pack not installed")
						RETURN FALSE
					BREAK
					CASE 30 FALLTHRU	//
					CASE 31 FALLTHRU	//
					CASE 32 FALLTHRU	//
					CASE 33	FALLTHRU	//
					CASE 34	FALLTHRU	//
					CASE 35				//
						IF IS_MP_BIKER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male biker pack not installed")
						RETURN FALSE
					BREAK
					CASE 38
						IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_COLLECTED_IMPOTANT_RAGE_OUTFIT)
						#IF IS_DEBUG_BUILD
						OR g_bDebugUnlockCasinoRewardItems
						#ENDIF
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male casino pack not installed")
						RETURN FALSE
					BREAK
					CASE 39
						IF IS_MP_TUNER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male tuner pack not installed")
						RETURN FALSE
					BREAK
					#IF FEATURE_FIXER
					CASE 40
						IF IS_MP_FIXER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male fixer pack not installed")
						RETURN FALSE
					BREAK
					#ENDIF
					#IF FEATURE_DLC_1_2022
					CASE 41
					CASE 42
						IF IS_MP_SUMMER_2022_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male summer 2022 pack not installed")
						RETURN FALSE
					BREAK
					#ENDIF
				ENDSWITCH
			BREAK
//			CASE HME_NGMP_CHEST
//				IF iOption = 17    //Blank
//					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male stars n stripes face paint not installed")
//					RETURN FALSE
//				ENDIF
//				
//			BREAK
			CASE HME_NGMP_FACEPAINT
				IF iOption = 13    // Stars n Stripes Face Paint
					IF IS_MP_INDEPENDENCE_PACK_PRESENT()
					AND (g_sMPTunables.btoggleactivateIndependencepack #IF IS_DEBUG_BUILD OR g_bDebugAllowIndependenceItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_FACEPAINT))
						RETURN TRUE
					ENDIF
					
					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male stars n stripes face paint not installed")
					RETURN FALSE
					
				ELIF iOption >= 14
				AND iOption <= 43
					IF IS_MP_HALLOWEEN_PACK_PRESENT()
						IF (g_sMPTunables.bTURN_ON_HALLOWEEN_FACEPAINT)
							RETURN TRUE
						ENDIF
						#IF IS_DEBUG_BUILD
						IF (g_bDebugUnlockHalloweenItems)
							RETURN TRUE
						ENDIF
						#ENDIF
						
						NGMP_MENU_OPTION_DATA sOptionData
						IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iOption, sOptionData)
						AND IS_MP_CLOTHES_OWNED(sOptionData.eUnlockItem)
							RETURN TRUE
						ENDIF
						
						IF (iOption = 14 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SHADOW_DEMON))
							RETURN TRUE
						ELIF (iOption = 15 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLESHY_DEMON))
							RETURN TRUE
						ELIF (iOption = 16 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLAYED_DEMON))
							RETURN TRUE
						ELIF (iOption = 17 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SORROW_DEMON))
							RETURN TRUE
						ELIF (iOption = 18 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SMILER_DEMON))
							RETURN TRUE
						ELIF (iOption = 19 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_CRACKED_DEMON))
							RETURN TRUE
						ELIF (iOption = 20 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DANGER_SKULL))
							RETURN TRUE
						ELIF (iOption = 21 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_WICKED_SKULL))
							RETURN TRUE
						ELIF (iOption = 22 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_MENACE_SKULL))
							RETURN TRUE
						ELIF (iOption = 23 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_BONE_JAW_SKULL))
							RETURN TRUE
						ELIF (iOption = 24 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLESH_JAW_SKULL))
							RETURN TRUE
						ELIF (iOption = 25 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SPIRIT_SKULL))
							RETURN TRUE
						ELIF (iOption = 26 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GHOUL_SKULL))
							RETURN TRUE
						ELIF (iOption = 27 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_PHANTOM_SKULL))
							RETURN TRUE
						ELIF (iOption = 28 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GNASHER_SKULL))
							RETURN TRUE
						ELIF (iOption = 29 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_EXPOSED_SKULL))
							RETURN TRUE
						ELIF (iOption = 30 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GHOSTLY_SKULL))
							RETURN TRUE
						ELIF (iOption = 31 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FURY_SKULL))
							RETURN TRUE
						ELIF (iOption = 32 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DEMI_SKULL))
							RETURN TRUE
						ELIF (iOption = 33 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_INBRED_SKULL))
							RETURN TRUE
						ELIF (iOption = 34 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SPOOKY_SKULL))
							RETURN TRUE
						ELIF (iOption = 35 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SLASHED_SKULL))
							RETURN TRUE
						ELIF (iOption = 36 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_WEB_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 37 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SENOR_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 38 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SWIRL_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 39 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLORAL_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 40 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_MONO_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 41 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FEMME_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 42 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DEMI_SUGAR_SKULL))
							RETURN TRUE
						ELIF (iOption = 43 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SCARRED_SUGAR_SKULL))
							RETURN TRUE
						ENDIF
					ENDIF
					
					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male halloween facepaint pack not installed for ", iOption)
					RETURN FALSE
					
				ELIF iOption >= 44
				AND iOption <= 69
					IF IS_MP_CHRISTMAS_2017_PACK_PRESENT()
						IF (g_sMPTunables.bTURN_ON_GANGOPS_FACEPAINT)
							RETURN TRUE
						ENDIF
						#IF IS_DEBUG_BUILD
						IF (g_bDebugUnlockGangOpsRewardItems)
							RETURN TRUE
						ENDIF
						#ENDIF
						
						NGMP_MENU_OPTION_DATA sOptionData
						IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iOption, sOptionData)
						AND IS_MP_CLOTHES_OWNED(sOptionData.eUnlockItem)
							RETURN TRUE
						ENDIF
					ENDIF
					
					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - male gangops facepaint pack not installed for ", iOption)
					RETURN FALSE
				
				ENDIF
			BREAK
		ENDSWITCH
	ELIF ePedModel = MP_F_FREEMODE_01
		SWITCH eMenu
			CASE HME_NGMP_HAIR
				IF (eShop = HAIRDO_SHOP_CASINO_APT)
					IF iOption = 20
					OR iOption = 23
					OR iOption = 24
					OR iOption = 25
					OR iOption = 26
					OR iOption = 27
					OR iOption = 28
					OR iOption = 29
					OR iOption = 30
					OR iOption = 31
					OR iOption = 32
					OR iOption = 33
					OR iOption = 34
					OR iOption = 35
					OR iOption = 36
					OR iOption = 37
						RETURN FALSE
					ENDIF
				ENDIF
				
				SWITCH iOption
					CASE 20				// Flapper Bob
						IF IS_MP_VALENTINES_PACK_PRESENT()
						AND (g_sMPTunables.bturnonvalentinesevent OR g_sMPTunables.bTURN_ON_VALENTINE_HAIR  #IF IS_DEBUG_BUILD OR g_bDebugAllowValentinesItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STATS_PURCHASED_FLAPPER_BOB))
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female flapper bob not installed")
						RETURN FALSE
					BREAK
					CASE 23				// Mullet
						IF IS_MP_INDEPENDENCE_PACK_PRESENT()
						AND (g_sMPTunables.btoggleactivateIndependencepack #IF IS_DEBUG_BUILD OR g_bDebugAllowIndependenceItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_MULLET))
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female mullet not installed")
						RETURN FALSE
					BREAK
					CASE 24 FALLTHRU	// Pinched Cornrows
					CASE 25 FALLTHRU	// Leaf Cornrows
					CASE 26 FALLTHRU	// Zig Zag Cornrows
					CASE 27				// Pigtail Bangs
						IF IS_MP_LOWRIDER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female lowrider pack not installed")
						RETURN FALSE
					BREAK
					CASE 28 FALLTHRU	// Wave Braids
					CASE 29 FALLTHRU	// Coil Braids
					CASE 30				// Low Bandana Bun
						IF IS_MP_LOWRIDER2_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female lowrider2 pack not installed")
						RETURN FALSE
					BREAK
					CASE 31 FALLTHRU	// 
					CASE 32 FALLTHRU	// 
					CASE 33 FALLTHRU	// 
					CASE 34 FALLTHRU	// 
					CASE 35 FALLTHRU 	//
					CASE 36 FALLTHRU 	//
					CASE 37 			//
						IF IS_MP_BIKER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female biker pack not installed")
						RETURN FALSE
					BREAK
					CASE 40
						IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_COLLECTED_IMPOTANT_RAGE_OUTFIT)
						#IF IS_DEBUG_BUILD
						OR g_bDebugUnlockCasinoRewardItems
						#ENDIF
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female casino pack not installed")
						RETURN FALSE
					BREAK
					CASE 41
						IF IS_MP_TUNER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female tuner pack not installed")
						RETURN FALSE
					BREAK
					#IF FEATURE_FIXER
					CASE 42
						IF IS_MP_FIXER_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female fixer pack not installed")
						RETURN FALSE
					BREAK
					#ENDIF
					#IF FEATURE_DLC_1_2022
					CASE 43
					CASE 44
						IF IS_MP_SUMMER_2022_PACK_PRESENT()
							RETURN TRUE
						ENDIF
						
						CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female summer 2022 pack not installed")
						RETURN FALSE
					BREAK
					#ENDIF
				ENDSWITCH
			BREAK
			CASE HME_NGMP_FACEPAINT
				IF iOption = 13    // Stars n Stripes Face Paint
					IF IS_MP_INDEPENDENCE_PACK_PRESENT()
					AND (g_sMPTunables.btoggleactivateIndependencepack #IF IS_DEBUG_BUILD OR g_bDebugAllowIndependenceItems #ENDIF OR GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_FACEPAINT))
						RETURN TRUE
					ENDIF
					
					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female stars n stripes face paint not installed")
					RETURN FALSE
					
				ELIF iOption >= 14
				AND iOption <= 43
					IF IS_MP_HALLOWEEN_PACK_PRESENT()
						IF (g_sMPTunables.bTURN_ON_HALLOWEEN_FACEPAINT)
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with bTURN_ON_HALLOWEEN_FACEPAINT.")
							RETURN TRUE
						ENDIF
						#IF IS_DEBUG_BUILD
						IF (g_bDebugUnlockHalloweenItems)
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with g_bDebugUnlockHalloweenItems.")
							RETURN TRUE
						ENDIF
						#ENDIF
						
						NGMP_MENU_OPTION_DATA sOptionData
						IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iOption, sOptionData)
						AND IS_MP_CLOTHES_OWNED(sOptionData.eUnlockItem)
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with IS_MP_CLOTHES_OWNED(", sOptionData.eUnlockItem, ").")
							RETURN TRUE
						ENDIF
						
						IF (iOption = 14 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SHADOW_DEMON))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 15 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLESHY_DEMON))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 16 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLAYED_DEMON))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 17 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SORROW_DEMON))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 18 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SMILER_DEMON))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 19 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_CRACKED_DEMON))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 20 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DANGER_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 21 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_WICKED_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 22 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_MENACE_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 23 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_BONE_JAW_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 24 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLESH_JAW_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 25 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SPIRIT_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 26 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GHOUL_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 27 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_PHANTOM_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 28 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GNASHER_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 29 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_EXPOSED_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 30 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GHOSTLY_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 31 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FURY_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 32 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DEMI_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 33 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_INBRED_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 34 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SPOOKY_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 35 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SLASHED_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 36 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_WEB_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 37 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SENOR_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 38 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SWIRL_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 39 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLORAL_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 40 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_MONO_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 41 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FEMME_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 42 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DEMI_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ELIF (iOption = 43 AND GET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SCARRED_SUGAR_SKULL))
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint ", iOption, " installed with PACKED_STAT_FACEPAINT_HALLOWEEN_2015.")
							RETURN TRUE
						ENDIF
					ENDIF
					
					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female halloween facepaint pack not installed for ", iOption)
					RETURN FALSE
					
				ELIF iOption >= 44
				AND iOption <= 69
					IF IS_MP_CHRISTMAS_2017_PACK_PRESENT()
						IF (g_sMPTunables.bTURN_ON_GANGOPS_FACEPAINT)
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female gangops facepaint ", iOption, " installed with bTURN_ON_GANGOPS_FACEPAINT.")
							RETURN TRUE
						ENDIF
						#IF IS_DEBUG_BUILD
						IF (g_bDebugUnlockGangOpsRewardItems)
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female gangops facepaint ", iOption, " installed with g_bDebugUnlockGangOpsRewardItems.")
							RETURN TRUE
						ENDIF
						#ENDIF
						
						NGMP_MENU_OPTION_DATA sOptionData
						IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iOption, sOptionData)
						AND IS_MP_CLOTHES_OWNED(sOptionData.eUnlockItem)
							CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female gangops facepaint ", iOption, " installed with IS_MP_CLOTHES_OWNED(", sOptionData.eUnlockItem, ").")
							RETURN TRUE
						ENDIF
					ENDIF
					
					CDEBUG3LN(DEBUG_SHOPS, "IS_DLC_INSTALLED_FOR_NGMP_ITEM - female gangops facepaint pack not installed for ", iOption)
					RETURN FALSE
				
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel)
	SWITCH sData.eCurrentMenu
		CASE HME_NGMP_HAIR
			SWITCH GET_NGMP_HAIR_OPTION(ePedModel, sData.sBrowseInfo.iCurrentItem)
				CASE 0		//hair highlights
//				CASE 1		//hair accessories
					//
				BREAK
				CASE -1		//none
					RETURN FALSE
				BREAK
				
				DEFAULT
					CASSERTLN(DEBUG_SHOPS, "BUILD_NGMP_TINT_MENU - unknown GET_NGMP_HAIR_OPTION ", GET_NGMP_HAIR_OPTION(ePedModel, sData.sBrowseInfo.iCurrentItem), " for item ", sData.sBrowseInfo.iCurrentItem)
				BREAK
			ENDSWITCH
		BREAK
		CASE HME_NGMP_CONTACTS
			RETURN FALSE
		BREAK
		CASE HME_NGMP_MAKEUP_EYE
		CASE HME_NGMP_FACEPAINT
			IF sData.sBrowseInfo.iCurrentItem = 0
				RETURN FALSE
			ENDIF
			
			//cant set colour, but function used to query opacity
		BREAK
		CASE HME_NGMP_MAKEUP_BLUSHER
		CASE HME_NGMP_MAKEUP_LIPSTICK
		CASE HME_NGMP_BEARD
		CASE HME_NGMP_EYEBROWS
		CASE HME_NGMP_CHEST
			IF sData.sBrowseInfo.iCurrentItem = 0
				RETURN FALSE
			ENDIF
		BREAK
		
		DEFAULT
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_NGMP_ITEM_AND_TINTS_CURRENT(MODEL_NAMES ePedModel, NGMP_MENU_OPTION_DATA &sOptionData, HAIRDO_SHOP_STRUCT &sData)
	
	SWITCH sOptionData.iType
		// Ped comp
		CASE 0
			PED_COMP_NAME_ENUM eCurrentHaircut
			eCurrentHaircut = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT))
			
			IF NOT g_bValidatedPlayerPurchasedHair
			OR ((ePedModel = MP_M_FREEMODE_01) AND NOT (eCurrentHaircut >= HAIR_FMM_DLC))
			OR ((ePedModel = MP_F_FREEMODE_01) AND NOT (eCurrentHaircut >= HAIR_FMF_DLC))
				IF (eCurrentHaircut != DUMMY_PED_COMP)
					PED_COMP_NAME_ENUM eGRHairItem
					eGRHairItem = DUMMY_PED_COMP
					IF (ePedModel = MP_M_FREEMODE_01)
						eGRHairItem = GET_MALE_HAIR(eCurrentHaircut)
					ELIF (ePedModel = MP_F_FREEMODE_01)
						eGRHairItem = GET_FEMALE_HAIR(eCurrentHaircut)
					ENDIF
					
					IF (eGRHairItem != DUMMY_PED_COMP)
					AND (eCurrentHaircut != eGRHairItem)
						CPRINTLN(DEBUG_SHOPS,"[ALREADY_HAS_NGMP_OVERLAY_ITEM] gr_hair: replacing hair enum ", eCurrentHaircut, " with gunrunning hair enum ", eGRHairItem)
						eCurrentHaircut = eGRHairItem
					ENDIF
				ENDIF
			ENDIF
			
			IF (sOptionData.eHairItem = eCurrentHaircut)
				
				IF NOT CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same hair texture, can't edit tints")
					RETURN TRUE
				ENDIF
				
				IF sData.iTint1 = GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
				AND sData.iTint2 = GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same hair texture and tints")
					RETURN TRUE
				ELSE
					CPRINTLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same hair texture, different tints")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - different hair texture \"", sOptionData.tlLabel, "\" (", sOptionData.eHairItem, " != ", eCurrentHaircut, ")")
			ENDIF
		BREAK
		// Overlay
		CASE 1
			INT sOptionData_iTexture
			sOptionData_iTexture = sOptionData.iTexture
			IF sOptionData_iTexture = -1
				CDEBUG3LN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - update sOptionData_iTexture to 255 instead of -1")
				sOptionData_iTexture = 255
			ENDIF
			
			STATS_PACKED sp_headslot
			sp_headslot = GET_CHARACTER_OVERLAY_STAT(sOptionData.eHeadSlot)
			
			INT iOverriden_ChestTexture
			iOverriden_ChestTexture = -99
			IF sOptionData.eHeadSlot = HOS_BODY_1	//GET_HEAD_OVERLAY_SLOT_FOR_NGMP_MENU(HME_NGMP_CHEST)
				IF ((sp_headslot != INT_TO_ENUM(STATS_PACKED, -1))
				AND (GET_PACKED_STAT_INT(sp_headslot) = 17))
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - update chest hair texture as its currently max for some reason")
					SET_PACKED_STAT_INT(sp_headslot, 16)
					iOverriden_ChestTexture = 16
				ENDIF
			ENDIF
			
			IF ((sp_headslot != INT_TO_ENUM(STATS_PACKED, -1))
			AND (sOptionData_iTexture = GET_PACKED_STAT_INT(sp_headslot) OR sOptionData_iTexture = iOverriden_ChestTexture))
				
				IF sOptionData.eHeadSlot = HOS_MAKEUP
				AND sOptionData_iTexture = 255
					STATS_PACKED sp_blusher_headslot
					sp_blusher_headslot = GET_CHARACTER_OVERLAY_STAT(HOS_BLUSHER)
					
					IF (sp_blusher_headslot != INT_TO_ENUM(STATS_PACKED, -1))
						IF (GET_PACKED_STAT_INT(sp_blusher_headslot) >= 7)
						AND (GET_PACKED_STAT_INT(sp_blusher_headslot) < 33)
							CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - ignore no makeup as player has blusher facepaint ", GET_PACKED_STAT_INT(sp_blusher_headslot), " that could be current")
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot), " texture, can't edit tints")
					RETURN TRUE
				ENDIF
				
				INT iTint1
				iTint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((sOptionData.eHeadSlot), sData.eRampType)
				
				MP_INT_STATS mpi_tint2
				MP_FLOAT_STATS mpf_blend 
				mpi_tint2 = GET_CHARACTER_OVERLAY_SECONDARY_COLOUR_STAT(sOptionData.eHeadSlot)
				mpf_blend = GET_CHARACTER_OVERLAY_BLEND_STAT(sOptionData.eHeadSlot)
				
				IF (mpf_blend != INT_TO_ENUM(MP_FLOAT_STATS, -1))
					IF (sData.fBlend != GET_MP_FLOAT_CHARACTER_STAT(mpf_blend))
					AND (sData.fBlend = 0.0)
						sData.fBlend = GET_MP_FLOAT_CHARACTER_STAT(mpf_blend)
					ENDIF
				ENDIF
				
				IF ((iTint1  =-1) OR (sData.iTint1 = iTint1))
				AND ((mpi_tint2 = INT_TO_ENUM(MP_INT_STATS, -1)) OR (sData.iTint2 = GET_MP_INT_CHARACTER_STAT(mpi_tint2)))
				AND ((mpf_blend = INT_TO_ENUM(MP_FLOAT_STATS, -1)) OR (sData.fBlend = GET_MP_FLOAT_CHARACTER_STAT(mpf_blend)))
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot), " texture and tints/blend")
					RETURN TRUE
				ELIF (sOptionData_iTexture = 255)
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot), " texture, ignore tints/blend")
					RETURN TRUE
				ELSE
					BOOL bWarned
					bWarned = FALSE
					
					CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot), " texture, different iTint1 (", sData.iTint1, " != ", GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((sOptionData.eHeadSlot), sData.eRampType), ")")
					IF (mpi_tint2 != INT_TO_ENUM(MP_INT_STATS, -1))
						CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same texture, different iTint2 (", sData.iTint2, " != ", GET_MP_INT_CHARACTER_STAT(mpi_tint2), ")")
						bWarned = TRUE
					ELSE
						CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same texture, different iTint2 (", sData.iTint2, ")")
						bWarned = TRUE
					ENDIF
					IF (mpf_blend != INT_TO_ENUM(MP_FLOAT_STATS, -1))
						CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same texture, fBlend (", sData.fBlend, " != ", GET_MP_FLOAT_CHARACTER_STAT(mpf_blend), ")")
						bWarned = TRUE
					ELSE
						CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same texture, fBlend (", sData.fBlend, ")")
						bWarned = TRUE
					ENDIF
					IF NOT bWarned
						CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - same texture, different iTint1, iTint2 or fBlend")
						bWarned = TRUE
					ENDIF
					
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_OVERLAY_ITEM - different ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot), " texture (", sOptionData_iTexture, " != ", GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(sOptionData.eHeadSlot)), ")")
			ENDIF
		BREAK
		// Eyes
		CASE 2
			IF sOptionData.iTexture = ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20))
				CWARNINGLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_EYES_ITEM - same contacts")
				RETURN TRUE
			ELSE
				CPRINTLN(DEBUG_SHOPS, "ALREADY_HAS_NGMP_EYES_ITEM - different contacts (", sOptionData.iTexture, " != ", ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20)), ")")
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NGMP_TINT_FROM_TINTINDEX(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iTintIndex, SHOP_NAME_ENUM eShop)
	IF iTintIndex = -1
		RETURN -1
	ENDIF
	
	INT iTint, iTintIndexCount = -1
	REPEAT GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, eMenu) iTint
		IF IS_TINT_AVAILABLE_FOR_NGMP_ITEM(eMenu, iTint, eShop)
			iTintIndexCount++
			IF iTintIndexCount = iTintIndex
				CDEBUG3LN(DEBUG_SHOPS, "GET_NGMP_TINT_FROM_TINTINDEX(	", iTintIndex, "	):	", iTint)
				
				RETURN iTint
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (eMenu = HME_NGMP_MAKEUP_BLUSHER)
		CERRORLN(DEBUG_SHOPS, "GET_NGMP_TINT_FROM_TINTINDEX - menu \"", GET_LABEL_FOR_NGMP_MENU(eMenu), "\" has unknown iTintIndex = \"", iTintIndex, "\"")
		RETURN 0
	ELSE
		CASSERTLN(DEBUG_SHOPS, "GET_NGMP_TINT_FROM_TINTINDEX - menu \"", GET_LABEL_FOR_NGMP_MENU(eMenu), "\" has unknown iTintIndex = \"", iTintIndex, "\"")
		RETURN -1
	ENDIF
ENDFUNC
FUNC INT GET_NGMP_TINTINDEX_FROM_TINT(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iTint, SHOP_NAME_ENUM eShop)
	IF iTint = -1
		RETURN -1
	ENDIF
	
	INT i, iTintCount = -1
	REPEAT GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, eMenu) i
		IF IS_TINT_AVAILABLE_FOR_NGMP_ITEM(eMenu, i, eShop)
			iTintCount++
			IF (i = iTint)
				CDEBUG3LN(DEBUG_SHOPS, "	FOUND GET_NGMP_TINTINDEX_FROM_TINT(	", iTint, "	):	", iTintCount)
				
				RETURN iTintCount
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (eShop = HAIRDO_SHOP_CASINO_APT)
		RETURN GET_NGMP_TINTINDEX_FROM_TINT(ePedModel, eMenu, iTint, HAIRDO_SHOP_01_BH)
	ELSE
		IF (eMenu = HME_NGMP_MAKEUP_BLUSHER)
			CERRORLN(DEBUG_SHOPS, "GET_NGMP_TINTINDEX_FROM_TINT - menu \"", GET_LABEL_FOR_NGMP_MENU(eMenu), "\" has unknown iTint = \"", iTint, "\"")
			RETURN 0
		ELSE
			CASSERTLN(DEBUG_SHOPS, "GET_NGMP_TINTINDEX_FROM_TINT - menu \"", GET_LABEL_FOR_NGMP_MENU(eMenu), "\" has unknown iTint = \"", iTint, "\"")
			RETURN -1
		ENDIF
	ENDIF
ENDFUNC



FUNC INT GET_HAIRDO_NGMP_ITEM_INDEX(INT nameHash)
	SWITCH nameHash
		//Male hair
		CASE HASH("NGMP_HAIR_M_0")				RETURN 0 BREAK
		CASE HASH("NGMP_HAIR_M_1")				RETURN 1 BREAK
		CASE HASH("NGMP_HAIR_M_2")				RETURN 2 BREAK
		CASE HASH("NGMP_HAIR_M_3")				RETURN 3 BREAK
		CASE HASH("NGMP_HAIR_M_4")				RETURN 4 BREAK
		CASE HASH("NGMP_HAIR_M_5")				RETURN 5 BREAK
		CASE HASH("NGMP_HAIR_M_6")				RETURN 6 BREAK
		CASE HASH("NGMP_HAIR_M_7")				RETURN 7 BREAK
		CASE HASH("NGMP_HAIR_M_8")				RETURN 8 BREAK
		CASE HASH("NGMP_HAIR_M_9")				RETURN 9 BREAK
		CASE HASH("NGMP_HAIR_M_10")				RETURN 10 BREAK
		CASE HASH("NGMP_HAIR_M_11")				RETURN 11 BREAK
		CASE HASH("NGMP_HAIR_M_12")				RETURN 12 BREAK
		CASE HASH("NGMP_HAIR_M_13")				RETURN 13 BREAK
		CASE HASH("NGMP_HAIR_M_14")				RETURN 14 BREAK
		CASE HASH("NGMP_HAIR_M_15")				RETURN 15 BREAK
		CASE HASH("NGMP_HAIR_M_16")				RETURN 16 BREAK
		CASE HASH("NGMP_HAIR_M_17")				RETURN 17 BREAK
		CASE HASH("NGMP_HAIR_M_18")				RETURN 18 BREAK
		CASE HASH("NGMP_HAIR_M_19")				RETURN 19 BREAK
		CASE HASH("NGMP_HAIR_M_20")				RETURN 20 BREAK
		CASE HASH("NGMP_HAIR_M_21")				RETURN 21 BREAK
		CASE HASH("NGMP_HAIR_M_22")				RETURN 22 BREAK
		//Male Beard
		CASE HASH("NGMP_BEARD_M_0")				RETURN 23 BREAK
		CASE HASH("NGMP_BEARD_M_1")				RETURN 24 BREAK
		CASE HASH("NGMP_BEARD_M_2")				RETURN 25 BREAK
		CASE HASH("NGMP_BEARD_M_3")				RETURN 26 BREAK
		CASE HASH("NGMP_BEARD_M_4")				RETURN 27 BREAK
		CASE HASH("NGMP_BEARD_M_5")				RETURN 28 BREAK
		CASE HASH("NGMP_BEARD_M_6")				RETURN 29 BREAK
		CASE HASH("NGMP_BEARD_M_7")				RETURN 30 BREAK
		CASE HASH("NGMP_BEARD_M_8")				RETURN 31 BREAK
		CASE HASH("NGMP_BEARD_M_9")				RETURN 32 BREAK
		CASE HASH("NGMP_BEARD_M_10")			RETURN 33 BREAK
		CASE HASH("NGMP_BEARD_M_11")			RETURN 34 BREAK
		CASE HASH("NGMP_BEARD_M_12")			RETURN 35 BREAK
		CASE HASH("NGMP_BEARD_M_13")			RETURN 36 BREAK
		CASE HASH("NGMP_BEARD_M_14")			RETURN 37 BREAK
		CASE HASH("NGMP_BEARD_M_15")			RETURN 38 BREAK
		CASE HASH("NGMP_BEARD_M_16")			RETURN 39 BREAK
		CASE HASH("NGMP_BEARD_M_17")			RETURN 40 BREAK
		CASE HASH("NGMP_BEARD_M_18")			RETURN 41 BREAK
		CASE HASH("NGMP_BEARD_M_19")			RETURN 42 BREAK
		CASE HASH("NGMP_BEARD_M_20")			RETURN 43 BREAK
		CASE HASH("NGMP_BEARD_M_21")			RETURN 44 BREAK
		CASE HASH("NGMP_BEARD_M_22")			RETURN 45 BREAK
		CASE HASH("NGMP_BEARD_M_23")			RETURN 46 BREAK
		CASE HASH("NGMP_BEARD_M_24")			RETURN 47 BREAK
		CASE HASH("NGMP_BEARD_M_25")			RETURN 48 BREAK
		CASE HASH("NGMP_BEARD_M_26")			RETURN 49 BREAK
		CASE HASH("NGMP_BEARD_M_27")			RETURN 50 BREAK
		CASE HASH("NGMP_BEARD_M_28")			RETURN 51 BREAK
		CASE HASH("NGMP_BEARD_M_29")			RETURN 52 BREAK
		//Male eyebrows
		CASE HASH("NGMP_EYEBROWS_M_0")			RETURN 53 BREAK
		CASE HASH("NGMP_EYEBROWS_M_1")			RETURN 54 BREAK
		CASE HASH("NGMP_EYEBROWS_M_2")			RETURN 55 BREAK
		CASE HASH("NGMP_EYEBROWS_M_3")			RETURN 56 BREAK
		CASE HASH("NGMP_EYEBROWS_M_4")			RETURN 57 BREAK
		CASE HASH("NGMP_EYEBROWS_M_5")			RETURN 58 BREAK
		CASE HASH("NGMP_EYEBROWS_M_6")			RETURN 59 BREAK
		CASE HASH("NGMP_EYEBROWS_M_7")			RETURN 60 BREAK
		CASE HASH("NGMP_EYEBROWS_M_8")			RETURN 61 BREAK
		CASE HASH("NGMP_EYEBROWS_M_9")			RETURN 62 BREAK
		CASE HASH("NGMP_EYEBROWS_M_10")			RETURN 63 BREAK
		CASE HASH("NGMP_EYEBROWS_M_11")			RETURN 64 BREAK
		CASE HASH("NGMP_EYEBROWS_M_12")			RETURN 65 BREAK
		CASE HASH("NGMP_EYEBROWS_M_13")			RETURN 66 BREAK
		CASE HASH("NGMP_EYEBROWS_M_14")			RETURN 67 BREAK
		CASE HASH("NGMP_EYEBROWS_M_15")			RETURN 68 BREAK
		CASE HASH("NGMP_EYEBROWS_M_16")			RETURN 69 BREAK
		CASE HASH("NGMP_EYEBROWS_M_17")			RETURN 70 BREAK
		CASE HASH("NGMP_EYEBROWS_M_18")			RETURN 71 BREAK
		CASE HASH("NGMP_EYEBROWS_M_19")			RETURN 72 BREAK
		CASE HASH("NGMP_EYEBROWS_M_20")			RETURN 73 BREAK
		CASE HASH("NGMP_EYEBROWS_M_21")			RETURN 74 BREAK
		CASE HASH("NGMP_EYEBROWS_M_22")			RETURN 75 BREAK
		CASE HASH("NGMP_EYEBROWS_M_23")			RETURN 76 BREAK
		CASE HASH("NGMP_EYEBROWS_M_24")			RETURN 77 BREAK
		CASE HASH("NGMP_EYEBROWS_M_25")			RETURN 78 BREAK
		CASE HASH("NGMP_EYEBROWS_M_26")			RETURN 79 BREAK
		CASE HASH("NGMP_EYEBROWS_M_27")			RETURN 80 BREAK
		CASE HASH("NGMP_EYEBROWS_M_28")			RETURN 81 BREAK
		CASE HASH("NGMP_EYEBROWS_M_29")			RETURN 82 BREAK
		CASE HASH("NGMP_EYEBROWS_M_30")			RETURN 83 BREAK
		CASE HASH("NGMP_EYEBROWS_M_31")			RETURN 84 BREAK
		CASE HASH("NGMP_EYEBROWS_M_32")			RETURN 85 BREAK
		CASE HASH("NGMP_EYEBROWS_M_33")			RETURN 86 BREAK
		CASE HASH("NGMP_EYEBROWS_M_34")			RETURN 87 BREAK
		//Male chest
		CASE HASH("NGMP_CHEST_M_0")				RETURN 88 BREAK
		CASE HASH("NGMP_CHEST_M_1")				RETURN 89 BREAK
		CASE HASH("NGMP_CHEST_M_2")				RETURN 90 BREAK
		CASE HASH("NGMP_CHEST_M_3")				RETURN 91 BREAK
		CASE HASH("NGMP_CHEST_M_4")				RETURN 92 BREAK
		CASE HASH("NGMP_CHEST_M_5")				RETURN 93 BREAK
		CASE HASH("NGMP_CHEST_M_6")				RETURN 94 BREAK
		CASE HASH("NGMP_CHEST_M_7")				RETURN 95 BREAK
		CASE HASH("NGMP_CHEST_M_8")				RETURN 96 BREAK
		CASE HASH("NGMP_CHEST_M_9")				RETURN 97 BREAK
		CASE HASH("NGMP_CHEST_M_10")			RETURN 98 BREAK
		CASE HASH("NGMP_CHEST_M_11")			RETURN 99 BREAK
		CASE HASH("NGMP_CHEST_M_12")			RETURN 100 BREAK
		CASE HASH("NGMP_CHEST_M_13")			RETURN 101 BREAK
		CASE HASH("NGMP_CHEST_M_14")			RETURN 102 BREAK
		CASE HASH("NGMP_CHEST_M_15")			RETURN 103 BREAK
		CASE HASH("NGMP_CHEST_M_16")			RETURN 104 BREAK
		CASE HASH("NGMP_CHEST_M_17")			RETURN 105 BREAK
		//Male contacts			
		CASE HASH("NGMP_CONTACTS_M_0")			RETURN 106 BREAK
		CASE HASH("NGMP_CONTACTS_M_1")			RETURN 107 BREAK
		CASE HASH("NGMP_CONTACTS_M_2")			RETURN 108 BREAK
		CASE HASH("NGMP_CONTACTS_M_3")			RETURN 109 BREAK
		CASE HASH("NGMP_CONTACTS_M_4")			RETURN 110 BREAK
		CASE HASH("NGMP_CONTACTS_M_5")			RETURN 111 BREAK
		CASE HASH("NGMP_CONTACTS_M_6")			RETURN 112 BREAK
		CASE HASH("NGMP_CONTACTS_M_7")			RETURN 113 BREAK
		CASE HASH("NGMP_CONTACTS_M_8")			RETURN 114 BREAK
		CASE HASH("NGMP_CONTACTS_M_9")			RETURN 115 BREAK
		CASE HASH("NGMP_CONTACTS_M_10")			RETURN 116 BREAK
		CASE HASH("NGMP_CONTACTS_M_11")			RETURN 117 BREAK
		CASE HASH("NGMP_CONTACTS_M_12")			RETURN 118 BREAK
		CASE HASH("NGMP_CONTACTS_M_13")			RETURN 119 BREAK
		CASE HASH("NGMP_CONTACTS_M_14")			RETURN 120 BREAK
		CASE HASH("NGMP_CONTACTS_M_15")			RETURN 121 BREAK
		CASE HASH("NGMP_CONTACTS_M_16")			RETURN 122 BREAK
		CASE HASH("NGMP_CONTACTS_M_17")			RETURN 123 BREAK
		CASE HASH("NGMP_CONTACTS_M_18")			RETURN 124 BREAK
		CASE HASH("NGMP_CONTACTS_M_19")			RETURN 125 BREAK
		CASE HASH("NGMP_CONTACTS_M_20")			RETURN 126 BREAK
		CASE HASH("NGMP_CONTACTS_M_21")			RETURN 127 BREAK
		CASE HASH("NGMP_CONTACTS_M_22")			RETURN 128 BREAK
		CASE HASH("NGMP_CONTACTS_M_23")			RETURN 129 BREAK
		CASE HASH("NGMP_CONTACTS_M_24")			RETURN 130 BREAK
		CASE HASH("NGMP_CONTACTS_M_25")			RETURN 131 BREAK
		CASE HASH("NGMP_CONTACTS_M_26")			RETURN 132 BREAK
		CASE HASH("NGMP_CONTACTS_M_27")			RETURN 133 BREAK
		CASE HASH("NGMP_CONTACTS_M_28")			RETURN 134 BREAK
		CASE HASH("NGMP_CONTACTS_M_29")			RETURN 135 BREAK
		CASE HASH("NGMP_CONTACTS_M_30")			RETURN 136 BREAK
		CASE HASH("NGMP_CONTACTS_M_31")			RETURN 137 BREAK
		//Male facepaints
		CASE HASH("NGMP_FACEPAINT_M_0")			RETURN 138 BREAK
		CASE HASH("NGMP_FACEPAINT_M_1")			RETURN 139 BREAK
		CASE HASH("NGMP_FACEPAINT_M_2")			RETURN 140 BREAK
		CASE HASH("NGMP_FACEPAINT_M_3")			RETURN 141 BREAK
		CASE HASH("NGMP_FACEPAINT_M_4")			RETURN 142 BREAK
		CASE HASH("NGMP_FACEPAINT_M_5")			RETURN 143 BREAK
		CASE HASH("NGMP_FACEPAINT_M_6")			RETURN 144 BREAK
		CASE HASH("NGMP_FACEPAINT_M_7")			RETURN 145 BREAK
		CASE HASH("NGMP_FACEPAINT_M_8")			RETURN 146 BREAK
		CASE HASH("NGMP_FACEPAINT_M_9")			RETURN 147 BREAK
		CASE HASH("NGMP_FACEPAINT_M_10")		RETURN 148 BREAK
		CASE HASH("NGMP_FACEPAINT_M_11")		RETURN 149 BREAK
		CASE HASH("NGMP_FACEPAINT_M_12")		RETURN 150 BREAK
		CASE HASH("NGMP_FACEPAINT_M_13")		RETURN 151 BREAK
		//Male eye makeup
		CASE HASH("NGMP_MAKEUP_EYE_M_0")		RETURN 152 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_1")		RETURN 153 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_2")		RETURN 154 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_3")		RETURN 155 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_4")		RETURN 156 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_5")		RETURN 157 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_6")		RETURN 158 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_7")		RETURN 159 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_8")		RETURN 160 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_9")		RETURN 161 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_10")		RETURN 162 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_11")		RETURN 163 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_12")		RETURN 164 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_13")		RETURN 165 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_14")		RETURN 166 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_15")		RETURN 167 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_16")		RETURN 168 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_17")		RETURN 169 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_18")		RETURN 170 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_19")		RETURN 171 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_20")		RETURN 172 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_21")		RETURN 173 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_22")		RETURN 174 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_23")		RETURN 175 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_24")		RETURN 176 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_M_25")		RETURN 177 BREAK
		//Male lipstick
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_0")	RETURN 178 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_1")	RETURN 179 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_2")	RETURN 180 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_3")	RETURN 181 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_4")	RETURN 182 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_5")	RETURN 183 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_6")	RETURN 184 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_7")	RETURN 185 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_8")	RETURN 186 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_9")	RETURN 187 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_M_10")	RETURN 188 BREAK
	ENDSWITCH
	SWITCH nameHash
		//Female hair
		CASE HASH("NGMP_HAIR_F_0")				RETURN 189 BREAK
		CASE HASH("NGMP_HAIR_F_1")				RETURN 190 BREAK
		CASE HASH("NGMP_HAIR_F_2")				RETURN 191 BREAK
		CASE HASH("NGMP_HAIR_F_3")				RETURN 192 BREAK
		CASE HASH("NGMP_HAIR_F_4")				RETURN 193 BREAK
		CASE HASH("NGMP_HAIR_F_5")				RETURN 194 BREAK
		CASE HASH("NGMP_HAIR_F_6")				RETURN 195 BREAK
		CASE HASH("NGMP_HAIR_F_7")				RETURN 196 BREAK
		CASE HASH("NGMP_HAIR_F_8")				RETURN 197 BREAK
		CASE HASH("NGMP_HAIR_F_9")				RETURN 198 BREAK
		CASE HASH("NGMP_HAIR_F_10")				RETURN 199 BREAK
		CASE HASH("NGMP_HAIR_F_11")				RETURN 200 BREAK
		CASE HASH("NGMP_HAIR_F_12")				RETURN 201 BREAK
		CASE HASH("NGMP_HAIR_F_13")				RETURN 202 BREAK
		CASE HASH("NGMP_HAIR_F_14")				RETURN 203 BREAK
		CASE HASH("NGMP_HAIR_F_15")				RETURN 204 BREAK
		CASE HASH("NGMP_HAIR_F_16")				RETURN 205 BREAK
		CASE HASH("NGMP_HAIR_F_17")				RETURN 206 BREAK
		CASE HASH("NGMP_HAIR_F_18")				RETURN 207 BREAK
		CASE HASH("NGMP_HAIR_F_19")				RETURN 208 BREAK
		CASE HASH("NGMP_HAIR_F_20")				RETURN 209 BREAK
		CASE HASH("NGMP_HAIR_F_21")				RETURN 210 BREAK
		CASE HASH("NGMP_HAIR_F_22")				RETURN 211 BREAK
		CASE HASH("NGMP_HAIR_F_23")				RETURN 212 BREAK
		//Female eyebrows
		CASE HASH("NGMP_EYEBROWS_F_0")			RETURN 213 BREAK
		CASE HASH("NGMP_EYEBROWS_F_1")			RETURN 214 BREAK
		CASE HASH("NGMP_EYEBROWS_F_2")			RETURN 215 BREAK
		CASE HASH("NGMP_EYEBROWS_F_3")			RETURN 216 BREAK
		CASE HASH("NGMP_EYEBROWS_F_4")			RETURN 217 BREAK
		CASE HASH("NGMP_EYEBROWS_F_5")			RETURN 218 BREAK
		CASE HASH("NGMP_EYEBROWS_F_6")			RETURN 219 BREAK
		CASE HASH("NGMP_EYEBROWS_F_7")			RETURN 220 BREAK
		CASE HASH("NGMP_EYEBROWS_F_8")			RETURN 221 BREAK
		CASE HASH("NGMP_EYEBROWS_F_9")			RETURN 222 BREAK
		CASE HASH("NGMP_EYEBROWS_F_10")			RETURN 223 BREAK
		CASE HASH("NGMP_EYEBROWS_F_11")			RETURN 224 BREAK
		CASE HASH("NGMP_EYEBROWS_F_12")			RETURN 225 BREAK
		CASE HASH("NGMP_EYEBROWS_F_13")			RETURN 226 BREAK
		CASE HASH("NGMP_EYEBROWS_F_14")			RETURN 227 BREAK
		CASE HASH("NGMP_EYEBROWS_F_15")			RETURN 228 BREAK
		CASE HASH("NGMP_EYEBROWS_F_16")			RETURN 229 BREAK
		CASE HASH("NGMP_EYEBROWS_F_17")			RETURN 230 BREAK
		CASE HASH("NGMP_EYEBROWS_F_18")			RETURN 231 BREAK
		CASE HASH("NGMP_EYEBROWS_F_19")			RETURN 232 BREAK
		CASE HASH("NGMP_EYEBROWS_F_20")			RETURN 233 BREAK
		CASE HASH("NGMP_EYEBROWS_F_21")			RETURN 234 BREAK
		CASE HASH("NGMP_EYEBROWS_F_22")			RETURN 235 BREAK
		CASE HASH("NGMP_EYEBROWS_F_23")			RETURN 236 BREAK
		CASE HASH("NGMP_EYEBROWS_F_24")			RETURN 237 BREAK
		CASE HASH("NGMP_EYEBROWS_F_25")			RETURN 238 BREAK
		CASE HASH("NGMP_EYEBROWS_F_26")			RETURN 239 BREAK
		CASE HASH("NGMP_EYEBROWS_F_27")			RETURN 240 BREAK
		CASE HASH("NGMP_EYEBROWS_F_28")			RETURN 241 BREAK
		CASE HASH("NGMP_EYEBROWS_F_29")			RETURN 242 BREAK
		CASE HASH("NGMP_EYEBROWS_F_30")			RETURN 243 BREAK
		CASE HASH("NGMP_EYEBROWS_F_31")			RETURN 244 BREAK
		CASE HASH("NGMP_EYEBROWS_F_32")			RETURN 245 BREAK
		CASE HASH("NGMP_EYEBROWS_F_33")			RETURN 246 BREAK
		CASE HASH("NGMP_EYEBROWS_F_34")			RETURN 247 BREAK
		//Female contacts
		CASE HASH("NGMP_CONTACTS_F_0")			RETURN 248 BREAK
		CASE HASH("NGMP_CONTACTS_F_1")			RETURN 249 BREAK
		CASE HASH("NGMP_CONTACTS_F_2")			RETURN 250 BREAK
		CASE HASH("NGMP_CONTACTS_F_3")			RETURN 251 BREAK
		CASE HASH("NGMP_CONTACTS_F_4")			RETURN 252 BREAK
		CASE HASH("NGMP_CONTACTS_F_5")			RETURN 253 BREAK
		CASE HASH("NGMP_CONTACTS_F_6")			RETURN 254 BREAK
		CASE HASH("NGMP_CONTACTS_F_7")			RETURN 255 BREAK
		CASE HASH("NGMP_CONTACTS_F_8")			RETURN 256 BREAK
		CASE HASH("NGMP_CONTACTS_F_9")			RETURN 257 BREAK
		CASE HASH("NGMP_CONTACTS_F_10")			RETURN 258 BREAK
		CASE HASH("NGMP_CONTACTS_F_11")			RETURN 259 BREAK
		CASE HASH("NGMP_CONTACTS_F_12")			RETURN 260 BREAK
		CASE HASH("NGMP_CONTACTS_F_13")			RETURN 261 BREAK
		CASE HASH("NGMP_CONTACTS_F_14")			RETURN 262 BREAK
		CASE HASH("NGMP_CONTACTS_F_15")			RETURN 263 BREAK
		CASE HASH("NGMP_CONTACTS_F_16")			RETURN 264 BREAK
		CASE HASH("NGMP_CONTACTS_F_17")			RETURN 265 BREAK
		CASE HASH("NGMP_CONTACTS_F_18")			RETURN 266 BREAK
		CASE HASH("NGMP_CONTACTS_F_19")			RETURN 267 BREAK
		CASE HASH("NGMP_CONTACTS_F_20")			RETURN 268 BREAK
		CASE HASH("NGMP_CONTACTS_F_21")			RETURN 269 BREAK
		CASE HASH("NGMP_CONTACTS_F_22")			RETURN 270 BREAK
		CASE HASH("NGMP_CONTACTS_F_23")			RETURN 271 BREAK
		CASE HASH("NGMP_CONTACTS_F_24")			RETURN 272 BREAK
		CASE HASH("NGMP_CONTACTS_F_25")			RETURN 273 BREAK
		CASE HASH("NGMP_CONTACTS_F_26")			RETURN 274 BREAK
		CASE HASH("NGMP_CONTACTS_F_27")			RETURN 275 BREAK
		CASE HASH("NGMP_CONTACTS_F_28")			RETURN 276 BREAK
		CASE HASH("NGMP_CONTACTS_F_29")			RETURN 277 BREAK
		CASE HASH("NGMP_CONTACTS_F_30")			RETURN 278 BREAK
		CASE HASH("NGMP_CONTACTS_F_31")			RETURN 279 BREAK
		//Female facepaint
		CASE HASH("NGMP_FACEPAINT_F_0")			RETURN 280 BREAK
		CASE HASH("NGMP_FACEPAINT_F_1")			RETURN 281 BREAK
		CASE HASH("NGMP_FACEPAINT_F_2")			RETURN 282 BREAK
		CASE HASH("NGMP_FACEPAINT_F_3")			RETURN 283 BREAK
		CASE HASH("NGMP_FACEPAINT_F_4")			RETURN 284 BREAK
		CASE HASH("NGMP_FACEPAINT_F_5")			RETURN 285 BREAK
		CASE HASH("NGMP_FACEPAINT_F_6")			RETURN 286 BREAK
		CASE HASH("NGMP_FACEPAINT_F_7")			RETURN 287 BREAK
		CASE HASH("NGMP_FACEPAINT_F_8")			RETURN 288 BREAK
		CASE HASH("NGMP_FACEPAINT_F_9")			RETURN 289 BREAK
		CASE HASH("NGMP_FACEPAINT_F_10")		RETURN 290 BREAK
		CASE HASH("NGMP_FACEPAINT_F_11")		RETURN 291 BREAK
		CASE HASH("NGMP_FACEPAINT_F_12")		RETURN 292 BREAK
		CASE HASH("NGMP_FACEPAINT_F_13")		RETURN 293 BREAK
		//Female eye makeup
		CASE HASH("NGMP_MAKEUP_EYE_F_0")		RETURN 294 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_1")		RETURN 295 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_2")		RETURN 296 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_3")		RETURN 297 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_4")		RETURN 298 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_5")		RETURN 299 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_6")		RETURN 300 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_7")		RETURN 301 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_8")		RETURN 302 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_9")		RETURN 303 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_10")		RETURN 304 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_11")		RETURN 305 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_12")		RETURN 306 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_13")		RETURN 307 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_14")		RETURN 308 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_15")		RETURN 309 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_16")		RETURN 310 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_17")		RETURN 311 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_18")		RETURN 312 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_19")		RETURN 313 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_20")		RETURN 314 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_21")		RETURN 315 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_22")		RETURN 316 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_23")		RETURN 317 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_24")		RETURN 318 BREAK
		CASE HASH("NGMP_MAKEUP_EYE_F_25")		RETURN 319 BREAK
		//Female blusher
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_0")	RETURN 320 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_1")	RETURN 321 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_2")	RETURN 322 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_3")	RETURN 323 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_4")	RETURN 324 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_5")	RETURN 325 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_6")	RETURN 326 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_7")	RETURN 327 BREAK
		//Female lipstick
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_0")	RETURN 328 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_1")	RETURN 329 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_2")	RETURN 330 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_3")	RETURN 331 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_4")	RETURN 332 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_5")	RETURN 333 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_6")	RETURN 334 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_7")	RETURN 335 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_8")	RETURN 336 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_9")	RETURN 337 BREAK
		CASE HASH("NGMP_MAKEUP_LIPSTICK_F_10")	RETURN 338 BREAK
	ENDSWITCH
	SWITCH nameHash
		//Male lowrider hair
		CASE HASH("NGMP_HAIR_M_23")				RETURN 339 BREAK
		CASE HASH("NGMP_HAIR_M_24")				RETURN 340 BREAK
		CASE HASH("NGMP_HAIR_M_25")				RETURN 341 BREAK
		CASE HASH("NGMP_HAIR_M_26")				RETURN 342 BREAK
		//Female lowrider hair
		CASE HASH("NGMP_HAIR_F_24")				RETURN 343 BREAK
		CASE HASH("NGMP_HAIR_F_25")				RETURN 344 BREAK
		CASE HASH("NGMP_HAIR_F_26")				RETURN 345 BREAK
		CASE HASH("NGMP_HAIR_F_27")				RETURN 346 BREAK
		
		//Female halloween facepaint
		CASE HASH("NGMP_FACEPAINT_M_14")		RETURN 347 BREAK // Demonic 1
		CASE HASH("NGMP_FACEPAINT_M_15")		RETURN 348 BREAK // Demonic 2
		CASE HASH("NGMP_FACEPAINT_M_16")		RETURN 349 BREAK // Demonic 3
		CASE HASH("NGMP_FACEPAINT_M_17")		RETURN 350 BREAK // Demonic 4
		CASE HASH("NGMP_FACEPAINT_M_18")		RETURN 351 BREAK // Demonic 5
		CASE HASH("NGMP_FACEPAINT_M_19")		RETURN 352 BREAK // Demonic 6
		CASE HASH("NGMP_FACEPAINT_M_20")		RETURN 353 BREAK // Skull 1
		CASE HASH("NGMP_FACEPAINT_M_21")		RETURN 354 BREAK // Skull 2
		CASE HASH("NGMP_FACEPAINT_M_22")		RETURN 355 BREAK // Skull 3
		CASE HASH("NGMP_FACEPAINT_M_23")		RETURN 356 BREAK // Skull 4
		CASE HASH("NGMP_FACEPAINT_M_24")		RETURN 357 BREAK // Skull 5
		CASE HASH("NGMP_FACEPAINT_M_25")		RETURN 358 BREAK // Skull 6
		CASE HASH("NGMP_FACEPAINT_M_26")		RETURN 359 BREAK // Skull 7
		CASE HASH("NGMP_FACEPAINT_M_27")		RETURN 360 BREAK // Skull 8
		CASE HASH("NGMP_FACEPAINT_M_28")		RETURN 361 BREAK // Skull 9
		CASE HASH("NGMP_FACEPAINT_M_29")		RETURN 362 BREAK // Skull 10
		CASE HASH("NGMP_FACEPAINT_M_30")		RETURN 363 BREAK // Skull 11
		CASE HASH("NGMP_FACEPAINT_M_31")		RETURN 364 BREAK // Skull 12
		CASE HASH("NGMP_FACEPAINT_M_32")		RETURN 365 BREAK // Skull 13
		CASE HASH("NGMP_FACEPAINT_M_33")		RETURN 366 BREAK // Skull 14
		CASE HASH("NGMP_FACEPAINT_M_34")		RETURN 367 BREAK // Skull 15
		CASE HASH("NGMP_FACEPAINT_M_35")		RETURN 368 BREAK // Skull 16
		CASE HASH("NGMP_FACEPAINT_M_36")		RETURN 369 BREAK // Sugar Skull 1
		CASE HASH("NGMP_FACEPAINT_M_37")		RETURN 370 BREAK // Sugar Skull 2
		CASE HASH("NGMP_FACEPAINT_M_38")		RETURN 371 BREAK // Sugar Skull 3
		CASE HASH("NGMP_FACEPAINT_M_39")		RETURN 372 BREAK // Sugar Skull 4
		CASE HASH("NGMP_FACEPAINT_M_40")		RETURN 373 BREAK // Sugar Skull 5
		CASE HASH("NGMP_FACEPAINT_M_41")		RETURN 374 BREAK // Sugar Skull 6
		CASE HASH("NGMP_FACEPAINT_M_42")		RETURN 375 BREAK // Sugar Skull 7
		CASE HASH("NGMP_FACEPAINT_M_43")		RETURN 376 BREAK // Sugar Skull 8
		//Female halloween facepaint
		CASE HASH("NGMP_FACEPAINT_F_14")		RETURN 377 BREAK // Demonic 1
		CASE HASH("NGMP_FACEPAINT_F_15")		RETURN 378 BREAK // Demonic 2
		CASE HASH("NGMP_FACEPAINT_F_16")		RETURN 379 BREAK // Demonic 3
		CASE HASH("NGMP_FACEPAINT_F_17")		RETURN 380 BREAK // Demonic 4
		CASE HASH("NGMP_FACEPAINT_F_18")		RETURN 381 BREAK // Demonic 5
		CASE HASH("NGMP_FACEPAINT_F_19")		RETURN 382 BREAK // Demonic 6
		CASE HASH("NGMP_FACEPAINT_F_20")		RETURN 383 BREAK // Skull 1
		CASE HASH("NGMP_FACEPAINT_F_21")		RETURN 384 BREAK // Skull 2
		CASE HASH("NGMP_FACEPAINT_F_22")		RETURN 385 BREAK // Skull 3
		CASE HASH("NGMP_FACEPAINT_F_23")		RETURN 386 BREAK // Skull 4
		CASE HASH("NGMP_FACEPAINT_F_24")		RETURN 387 BREAK // Skull 5
		CASE HASH("NGMP_FACEPAINT_F_25")		RETURN 388 BREAK // Skull 6
		CASE HASH("NGMP_FACEPAINT_F_26")		RETURN 389 BREAK // Skull 7
		CASE HASH("NGMP_FACEPAINT_F_27")		RETURN 390 BREAK // Skull 8
		CASE HASH("NGMP_FACEPAINT_F_28")		RETURN 391 BREAK // Skull 9
		CASE HASH("NGMP_FACEPAINT_F_29")		RETURN 392 BREAK // Skull 10
		CASE HASH("NGMP_FACEPAINT_F_30")		RETURN 393 BREAK // Skull 11
		CASE HASH("NGMP_FACEPAINT_F_31")		RETURN 394 BREAK // Skull 12
		CASE HASH("NGMP_FACEPAINT_F_32")		RETURN 395 BREAK // Skull 13
		CASE HASH("NGMP_FACEPAINT_F_33")		RETURN 396 BREAK // Skull 14
		CASE HASH("NGMP_FACEPAINT_F_34")		RETURN 397 BREAK // Skull 15
		CASE HASH("NGMP_FACEPAINT_F_35")		RETURN 398 BREAK // Skull 16
		CASE HASH("NGMP_FACEPAINT_F_36")		RETURN 399 BREAK // Sugar Skull 1
		CASE HASH("NGMP_FACEPAINT_F_37")		RETURN 400 BREAK // Sugar Skull 2
		CASE HASH("NGMP_FACEPAINT_F_38")		RETURN 401 BREAK // Sugar Skull 3
		CASE HASH("NGMP_FACEPAINT_F_39")		RETURN 402 BREAK // Sugar Skull 4
		CASE HASH("NGMP_FACEPAINT_F_40")		RETURN 403 BREAK // Sugar Skull 5
		CASE HASH("NGMP_FACEPAINT_F_41")		RETURN 404 BREAK // Sugar Skull 6
		CASE HASH("NGMP_FACEPAINT_F_42")		RETURN 405 BREAK // Sugar Skull 7
		CASE HASH("NGMP_FACEPAINT_F_43")		RETURN 406 BREAK // Sugar Skull 8
		
		//Male lowrider2 hair
		CASE HASH("NGMP_HAIR_M_27")				RETURN 407 BREAK
		CASE HASH("NGMP_HAIR_M_28")				RETURN 408 BREAK
		CASE HASH("NGMP_HAIR_M_29")				RETURN 409 BREAK
		//Female lowrider2 hair
		CASE HASH("NGMP_HAIR_F_28")				RETURN 410 BREAK
		CASE HASH("NGMP_HAIR_F_29")				RETURN 411 BREAK
		CASE HASH("NGMP_HAIR_F_30")				RETURN 412 BREAK
		
		//Male biker hair
		CASE HASH("NGMP_HAIR_M_30")				RETURN 413 BREAK
		CASE HASH("NGMP_HAIR_M_31")				RETURN 414 BREAK
		CASE HASH("NGMP_HAIR_M_32")				RETURN 415 BREAK
		CASE HASH("NGMP_HAIR_M_33")				RETURN 416 BREAK
		CASE HASH("NGMP_HAIR_M_34")				RETURN 417 BREAK
		CASE HASH("NGMP_HAIR_M_35")				RETURN 418 BREAK
		//Female biker hair
		CASE HASH("NGMP_HAIR_F_31")				RETURN 419 BREAK
		CASE HASH("NGMP_HAIR_F_32")				RETURN 420 BREAK
		CASE HASH("NGMP_HAIR_F_33")				RETURN 421 BREAK
		CASE HASH("NGMP_HAIR_F_34")				RETURN 422 BREAK
		CASE HASH("NGMP_HAIR_F_35")				RETURN 423 BREAK
		CASE HASH("NGMP_HAIR_F_36")				RETURN 423 BREAK
		CASE HASH("NGMP_HAIR_F_37")				RETURN 423 BREAK
		
		//Male gunrunning hair
		CASE HASH("NGMP_HAIR_M_36")				RETURN 424 BREAK
		CASE HASH("NGMP_HAIR_M_37")				RETURN 425 BREAK
		//Female gunrunning hair
		CASE HASH("NGMP_HAIR_F_38")				RETURN 426 BREAK
		CASE HASH("NGMP_HAIR_F_39")				RETURN 427 BREAK
		
		//Male christmas facepaint
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_7") 	RETURN 428 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_8") 	RETURN 429 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_9")	RETURN 430 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_10")	RETURN 431 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_11")	RETURN 432 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_12")	RETURN 433 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_13")	RETURN 434 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_14")	RETURN 435 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_15")	RETURN 436 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_16")	RETURN 437 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_17")	RETURN 438 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_18")	RETURN 439 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_19")	RETURN 440 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_20")	RETURN 441 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_21")	RETURN 442 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_22")	RETURN 443 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_23")	RETURN 444 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_24")	RETURN 445 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_25")	RETURN 446 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_26")	RETURN 447 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_27")	RETURN 448 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_28")	RETURN 449 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_29")	RETURN 450 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_30")	RETURN 451 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_31")	RETURN 452 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_M_32")	RETURN 453 BREAK
		//Female christmas facepaint
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_7") 	RETURN 455 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_8") 	RETURN 456 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_9")	RETURN 457 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_10")	RETURN 458 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_11")	RETURN 459 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_12")	RETURN 460 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_13")	RETURN 461 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_14")	RETURN 462 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_15")	RETURN 463 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_16")	RETURN 464 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_17")	RETURN 465 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_18")	RETURN 466 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_19")	RETURN 467 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_20")	RETURN 468 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_21")	RETURN 469 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_22")	RETURN 470 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_23")	RETURN 471 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_24")	RETURN 472 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_25")	RETURN 473 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_26")	RETURN 474 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_27")	RETURN 475 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_28")	RETURN 476 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_29")	RETURN 477 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_30")	RETURN 478 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_31")	RETURN 479 BREAK
		CASE HASH("NGMP_MAKEUP_BLUSHER_F_32")	RETURN 480 BREAK
		
		//Male gunrunning hair
		CASE HASH("NGMP_HAIR_M_38")				RETURN 481 BREAK
		//Female gunrunning hair
		CASE HASH("NGMP_HAIR_F_40")				RETURN 482 BREAK
		
		//Male Heist4 facepaints
		CASE HASH("NGMP_FACEPAINT_M_44")	RETURN 483 BREAK
		CASE HASH("NGMP_FACEPAINT_M_45")	RETURN 484 BREAK
		CASE HASH("NGMP_FACEPAINT_M_46")	RETURN 485 BREAK
		CASE HASH("NGMP_FACEPAINT_M_47")	RETURN 486 BREAK
		CASE HASH("NGMP_FACEPAINT_M_48")	RETURN 487 BREAK
		CASE HASH("NGMP_FACEPAINT_M_49")	RETURN 488 BREAK
		CASE HASH("NGMP_FACEPAINT_M_50")	RETURN 489 BREAK
		CASE HASH("NGMP_FACEPAINT_M_51")	RETURN 490 BREAK
		CASE HASH("NGMP_FACEPAINT_M_52")	RETURN 491 BREAK
		
		//Female Heist4 facepaints
		CASE HASH("NGMP_FACEPAINT_F_44")	RETURN 492 BREAK
		CASE HASH("NGMP_FACEPAINT_F_45")	RETURN 493 BREAK
		CASE HASH("NGMP_FACEPAINT_F_46")	RETURN 494 BREAK
		CASE HASH("NGMP_FACEPAINT_F_47")	RETURN 495 BREAK
		CASE HASH("NGMP_FACEPAINT_F_48")	RETURN 496 BREAK
		CASE HASH("NGMP_FACEPAINT_F_49")	RETURN 497 BREAK
		CASE HASH("NGMP_FACEPAINT_F_50")	RETURN 498 BREAK
		CASE HASH("NGMP_FACEPAINT_F_51")	RETURN 499 BREAK
		CASE HASH("NGMP_FACEPAINT_F_52")	RETURN 500 BREAK
		
		//Male tuner hair
		CASE HASH("NGMP_HAIR_M_39")				RETURN 501 BREAK
		//Female tuner hair
		CASE HASH("NGMP_HAIR_F_41")				RETURN 502 BREAK
	ENDSWITCH
	
	#IF FEATURE_FIXER
	SWITCH nameHash
		//Male fixer facepaints
		CASE HASH("NGMP_FACEPAINT_M_53")	RETURN 503 BREAK
		CASE HASH("NGMP_FACEPAINT_M_54")	RETURN 504 BREAK
		CASE HASH("NGMP_FACEPAINT_M_55")	RETURN 505 BREAK
		CASE HASH("NGMP_FACEPAINT_M_56")	RETURN 506 BREAK
		CASE HASH("NGMP_FACEPAINT_M_57")	RETURN 507 BREAK
		CASE HASH("NGMP_FACEPAINT_M_58")	RETURN 508 BREAK
		CASE HASH("NGMP_FACEPAINT_M_59")	RETURN 509 BREAK
		CASE HASH("NGMP_FACEPAINT_M_60")	RETURN 510 BREAK
		CASE HASH("NGMP_FACEPAINT_M_61")	RETURN 511 BREAK
		CASE HASH("NGMP_FACEPAINT_M_62")	RETURN 512 BREAK
		CASE HASH("NGMP_FACEPAINT_M_63")	RETURN 513 BREAK
		CASE HASH("NGMP_FACEPAINT_M_64")	RETURN 514 BREAK
		CASE HASH("NGMP_FACEPAINT_M_65")	RETURN 515 BREAK
		CASE HASH("NGMP_FACEPAINT_M_66")	RETURN 516 BREAK
		
		// Female fixer facepaints
		CASE HASH("NGMP_FACEPAINT_F_53")	RETURN 517 BREAK
		CASE HASH("NGMP_FACEPAINT_F_54")	RETURN 518 BREAK
		CASE HASH("NGMP_FACEPAINT_F_55")	RETURN 519 BREAK
		CASE HASH("NGMP_FACEPAINT_F_56")	RETURN 520 BREAK
		CASE HASH("NGMP_FACEPAINT_F_57")	RETURN 521 BREAK
		CASE HASH("NGMP_FACEPAINT_F_58")	RETURN 522 BREAK
		CASE HASH("NGMP_FACEPAINT_F_59")	RETURN 523 BREAK
		CASE HASH("NGMP_FACEPAINT_F_60")	RETURN 524 BREAK
		CASE HASH("NGMP_FACEPAINT_F_61")	RETURN 525 BREAK
		CASE HASH("NGMP_FACEPAINT_F_62")	RETURN 526 BREAK
		CASE HASH("NGMP_FACEPAINT_F_63")	RETURN 527 BREAK
		CASE HASH("NGMP_FACEPAINT_F_64")	RETURN 528 BREAK
		CASE HASH("NGMP_FACEPAINT_F_65")	RETURN 529 BREAK
		CASE HASH("NGMP_FACEPAINT_F_66")	RETURN 530 BREAK
		
		//Male fixer hair
		CASE HASH("NGMP_HAIR_M_40")			RETURN 531 BREAK
		
		//Female fixer hair
		CASE HASH("NGMP_HAIR_F_42")			RETURN 532 BREAK
	ENDSWITCH
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	SWITCH nameHash
		//Male summer 2022 hair
		CASE HASH("NGMP_HAIR_M_41")				RETURN 533 BREAK
		CASE HASH("NGMP_HAIR_M_42")				RETURN 534 BREAK
		//Female summer 2022 hair
		CASE HASH("NGMP_HAIR_F_43")				RETURN 535 BREAK
		CASE HASH("NGMP_HAIR_F_44")				RETURN 536 BREAK
	ENDSWITCh
	#ENDIF
	
	// Items are saved in the CHAR_HAIR_UNLCK* stats.
	
	RETURN -1
ENDFUNC

FUNC BOOL GET_HAIRDO_NGMP_SAVE_DATA(INT nameHash, INT bitFlag, MP_INT_STATS &eStat, INT &iBit)

	#IF USE_SP_DLC
		UNUSED_PARAMETER(nameHash)
		UNUSED_PARAMETER(bitFlag)
		UNUSED_PARAMETER(eStat)
		UNUSED_PARAMETER(iBit)
		RETURN FALSE
	#ENDIF
	
	#IF NOT USE_SP_DLC

		eStat = MAX_NUM_MP_INT_STATS

		// Grab the index for each item
		INT iDLCIndex = GET_HAIRDO_NGMP_ITEM_INDEX(nameHash)
		
		IF iDLCIndex = -1
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_PED_COMP, "GET_HAIRDO_NGMP_SAVE_DATA - Missing name hash ", nameHash, ".")
			#ENDIF
			RETURN FALSE
		ENDIF
		
		// Determine which stat to use
		SWITCH bitFlag
			CASE PED_COMPONENT_ACQUIRED_SLOT
				SWITCH (FLOOR(TO_FLOAT(iDLCIndex)/32))
					CASE 0	eStat = MP_STAT_CHAR_HAIR_UNLCK1 BREAK
					CASE 1	eStat = MP_STAT_CHAR_HAIR_UNLCK2 BREAK
					CASE 2	eStat = MP_STAT_CHAR_HAIR_UNLCK3 BREAK
					CASE 3	eStat = MP_STAT_CHAR_HAIR_UNLCK4 BREAK
					CASE 4	eStat = MP_STAT_CHAR_HAIR_UNLCK5 BREAK
					CASE 5	eStat = MP_STAT_CHAR_HAIR_UNLCK6 BREAK
					CASE 6	eStat = MP_STAT_CHAR_HAIR_UNLCK7 BREAK
					CASE 7	eStat = MP_STAT_CHAR_HAIR_UNLCK8 BREAK
					CASE 8	eStat = MP_STAT_CHAR_HAIR_UNLCK9 BREAK
					CASE 9	eStat = MP_STAT_CHAR_HAIR_UNLCK10 BREAK
					CASE 10	eStat = MP_STAT_CHAR_HAIR_UNLCK11 BREAK
					CASE 11	eStat = MP_STAT_CHAR_HAIR_UNLCK12 BREAK
					CASE 12	eStat = MP_STAT_CHAR_HAIR_UNLCK13 BREAK
					CASE 13	eStat = MP_STAT_CHAR_HAIR_UNLCK14 BREAK
					CASE 14	eStat = MP_STAT_CHAR_HAIR_UNLCK15 BREAK
					CASE 15	eStat = MP_STAT_CHAR_HAIR_UNLCK16 BREAK
					CASE 16	eStat = MP_STAT_CHAR_HAIR_UNLCK17 BREAK
					DEFAULT
						#IF IS_DEBUG_BUILD
							CASSERTLN(DEBUG_PED_COMP, "GET_HAIRDO_NGMP_SAVE_DATA - Missing unlock iBitset ", (FLOOR(TO_FLOAT(iDLCIndex)/32)), ".")
						#ENDIF
						RETURN FALSE
					BREAK
				ENDSWITCH
			BREAK
			CASE PED_COMPONENT_USED_SLOT
				SWITCH (FLOOR(TO_FLOAT(iDLCIndex)/32))
					CASE 0	eStat = MP_STAT_CHAR_HAIR_VIEWED1 BREAK
					CASE 1	eStat = MP_STAT_CHAR_HAIR_VIEWED2 BREAK
					CASE 2	eStat = MP_STAT_CHAR_HAIR_VIEWED3 BREAK
					CASE 3	eStat = MP_STAT_CHAR_HAIR_VIEWED4 BREAK
					CASE 4	eStat = MP_STAT_CHAR_HAIR_VIEWED5 BREAK
					CASE 5	eStat = MP_STAT_CHAR_HAIR_VIEWED6 BREAK
					CASE 6	eStat = MP_STAT_CHAR_HAIR_VIEWED7 BREAK
					CASE 7	eStat = MP_STAT_CHAR_HAIR_VIEWED8 BREAK
					CASE 8	eStat = MP_STAT_CHAR_HAIR_VIEWED9 BREAK
					CASE 9	eStat = MP_STAT_CHAR_HAIR_VIEWED10 BREAK
					CASE 10	eStat = MP_STAT_CHAR_HAIR_VIEWED11 BREAK
					CASE 11	eStat = MP_STAT_CHAR_HAIR_VIEWED12 BREAK
					CASE 12	eStat = MP_STAT_CHAR_HAIR_VIEWED13 BREAK
					CASE 13	eStat = MP_STAT_CHAR_HAIR_VIEWED14 BREAK
					CASE 14	eStat = MP_STAT_CHAR_HAIR_VIEWED15 BREAK
					CASE 15	eStat = MP_STAT_CHAR_HAIR_VIEWED16 BREAK
					CASE 16	eStat = MP_STAT_CHAR_HAIR_VIEWED17 BREAK
					DEFAULT
						#IF IS_DEBUG_BUILD
							CASSERTLN(DEBUG_PED_COMP, "GET_HAIRDO_NGMP_SAVE_DATA - Missing viewed iBitset ", (FLOOR(TO_FLOAT(iDLCIndex)/32)), ".")
						#ENDIF
						RETURN FALSE
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		iBit = iDLCIndex%32
		
		RETURN (eStat != MAX_NUM_MP_INT_STATS)
	#ENDIF
ENDFUNC

PROC SET_BIT_HAIRDO_NGMP_SCRIPT(INT nameHash, INT bitFlag, INT iSlot = -1)
	INT iStatSlot = g_iPedComponentSlot
	IF iSlot != -1
		iStatSlot = iSlot
	ENDIF
	INT iBit
	MP_INT_STATS eStat
	IF GET_HAIRDO_NGMP_SAVE_DATA(nameHash, bitFlag, eStat, iBit)
		INT iStatValue = GET_MP_INT_CHARACTER_STAT(eStat, iStatSlot)
		SET_BIT(iStatValue, iBit)
		SET_MP_INT_CHARACTER_STAT(eStat, iStatValue, iStatSlot)
	ENDIF
ENDPROC

PROC CLEAR_BIT_HAIRDO_NGMP_SCRIPT(INT nameHash, INT bitFlag, INT iSlot = -1)
	INT iStatSlot = g_iPedComponentSlot
	IF iSlot != -1
		iStatSlot = iSlot
	ENDIF
	INT iBit
	MP_INT_STATS eStat
	IF GET_HAIRDO_NGMP_SAVE_DATA(nameHash, bitFlag, eStat, iBit)
		INT iStatValue = GET_MP_INT_CHARACTER_STAT(eStat, iStatSlot)
		CLEAR_BIT(iStatValue, iBit)
		SET_MP_INT_CHARACTER_STAT(eStat, iStatValue, iStatSlot)
	ENDIF
ENDPROC

FUNC BOOL IS_BIT_SET_HAIRDO_NGMP_SCRIPT(INT nameHash, INT bitFlag, INT iSlot = -1)
	INT iStatSlot = g_iPedComponentSlot
	IF iSlot != -1
		iStatSlot = iSlot
	ENDIF
	INT iBit
	MP_INT_STATS eStat
	IF GET_HAIRDO_NGMP_SAVE_DATA(nameHash, bitFlag, eStat, iBit)
		INT iStatValue = GET_MP_INT_CHARACTER_STAT(eStat, iStatSlot)
		RETURN (IS_BIT_SET(iStatValue, iBit))
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_HAIRDO_KEY_VARIANT(HAIRDO_MENU_ENUM eMenu, INT iOption, SHOP_NAME_ENUM eShop, MODEL_NAMES eCharacterModel = DUMMY_MODEL_FOR_SCRIPT)
	IF (eCharacterModel = DUMMY_MODEL_FOR_SCRIPT)
		eCharacterModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
	ENDIF
	
	NGMP_MENU_OPTION_DATA sOptionData
	IF (GET_HAIRDO_SHOP_KEY_VARIANT(eShop) = 1)
	AND GET_NGMP_MENU_OPTION_DATA(eCharacterModel, eMenu, iOption, sOptionData)
		TEXT_LABEL_63 tlCategoryKey
		GENERATE_HAIRDO_KEY_FOR_CATALOGUE(tlCategoryKey, eMenu, sOptionData.tlLabel, eCharacterModel, 1)
		
		IF NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCategoryKey)
			RETURN 1
		ENDIF
	ENDIF
	
	IF (eMenu = HME_NGMP_HAIR)
		IF GET_NGMP_MENU_OPTION_DATA(eCharacterModel, eMenu, iOption, sOptionData)
		
			PED_COMP_NAME_ENUM eGREnum = sOptionData.eHairItem
			IF eCharacterModel = MP_M_FREEMODE_01
				eGREnum = GET_MALE_HAIR(sOptionData.eHairItem)
			ELIF eCharacterModel = MP_F_FREEMODE_01
				eGREnum = GET_FEMALE_HAIR(sOptionData.eHairItem)
			ENDIF
			IF (eGREnum = sOptionData.eHairItem)
				PRINTLN("GET_HAIRDO_KEY_VARIANT - eGREnum:", eGREnum, " sOptionData.eHairItem:", sOptionData.eHairItem)
				RETURN 2
			ENDIF
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC
