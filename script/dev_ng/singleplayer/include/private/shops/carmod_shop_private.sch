//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	carmod_shop_private.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains all the functionality for the 	//
//							car mod shops.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "shop_private.sch"
USING "net_ambience.sch"
USING "net_include.sch"

CONST_INT MAX_MENU_DEPTHS 7

CONST_INT NUMBER_OF_CAR_WHEEL_TYPES		13

CONST_INT NUMBER_OF_CAR_CLASSIC_PAINTS			75
CONST_INT NUMBER_OF_CAR_METALLIC_PAINTS			75
CONST_INT NUMBER_OF_CAR_PEARLESCENT_PAINTS		75
CONST_INT NUMBER_OF_CAR_MATTE_PAINTS			20
CONST_INT NUMBER_OF_CAR_METALS_PAINTS			3
CONST_INT NUMBER_OF_CAR_CHROME_PAINTS			1
CONST_INT NUMBER_OF_CAR_CREW_PAINTS				1
#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT NUMBER_OF_CAR_CHAMELEON_PAINTS		2
#ENDIF

#IF NOT FEATURE_GEN9_EXCLUSIVE
CONST_INT TOTAL_NUMBER_OF_CAR_PAINTS			251
#ENDIF

#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT TOTAL_NUMBER_OF_CAR_PAINTS			253
#ENDIF

CONST_INT WING_R_FIRST_MENU_ITEM				21			// This is the wing r menu index after stock item

ENUM CARMOD_DIALOGUE_ENUM

	CMOD_DLG_NONE = -1,
	
	CMOD_DLG_GREET,
	CMOD_DLG_GREET_FAM1,
	CMOD_DLG_ASK,
	CMOD_DLG_MOD,
	CMOD_DLG_EXIT,
	CMOD_DLG_NEW_CAR,
	CMOD_DLG_NO_SERVICE,
	CMOD_DLG_TIME_WASTE,
	CMOD_DLG_HEAVY_LOAD,
	CMOD_DLG_GTAO_TUT,
	CMOD_DLG_GTAO_BUNKER_MOD_SHOP_INTRO,
	CMOD_DLG_GTAO_TRUCK_VEH_MOD_SHOP_INTRO
ENDENUM

ENUM MOD_SHOP_CAM_ENUM
	MOD_CAM_DUMMY = 0,
	MOD_CAM_FRONT,
	MOD_CAM_REAR,
	MOD_CAM_SIDE_L,	
	MOD_CAM_SIDE_R,
	MOD_CAM_REAR_L,	
	MOD_CAM_REAR_R,
	MOD_CAM_FULL,
	MAX_MOD_CAMS
ENDENUM

ENUM AWARD_LIVERY_NAME
	AWARD_LIVERY_NONE = 0,
	AWARD_LIVERY_BANSH_LIV11,		// 1
	AWARD_LIVERY_BANSH_LIV12,		// 2
	AWARD_LIVERY_BANSH_LIV13,		// 3
	AWARD_LIVERY_BANSH_LIV14,		// 4
	AWARD_LIVERY_BANSH_LIV15,		// 5
	AWARD_LIVERY_CLIQ_LIV11,		// 6
	AWARD_LIVERY_COMET6_LIV11,		// 7
	AWARD_LIVERY_COMET6_LIV12,		// 8
	AWARD_LIVERY_COMET6_LIV13,		// 9
	AWARD_LIVERY_COMET6_LIV14,		// 10
	AWARD_LIVERY_COMET6_LIV15,		// 11
	AWARD_LIVERY_CYPH_LIVERY11,		// 12
	AWARD_LIVERY_CYPH_LIVERY12,		// 13
	AWARD_LIVERY_CYPH_LIVERY13,		// 14
	AWARD_LIVERY_CYPH_LIVERY14,		// 15
	AWARD_LIVERY_CYPH_LIVERY15,		// 16
	AWARD_LIVERY_DOM7_LIVERY11,		// 17
	AWARD_LIVERY_DOM7_LIVERY12,		// 18
	AWARD_LIVERY_DOM7_LIVERY13,		// 19
	AWARD_LIVERY_DOM7_LIVERY14,		// 20
	AWARD_LIVERY_DOM7_LIVERY15,		// 21
	AWARD_LIVERY_GROWLER_LIV11,		// 22
	AWARD_LIVERY_GROWLER_LIV12,		// 23
	AWARD_LIVERY_GROWLER_LIV13,		// 24
	AWARD_LIVERY_GROWLER_LIV14,		// 25
	AWARD_LIVERY_GROWLER_LIV15,		// 26
	AWARD_LIVERY_HERMES_LIV11,		// 27
	AWARD_LIVERY_HOTRING_LIV31,		// 28
	AWARD_LIVERY_KANJO_LIV13,		// 29
	AWARD_LIVERY_KURUMA_LIV11,		// 30
	AWARD_LIVERY_KURUMA_LIV12,		// 31
	AWARD_LIVERY_KURUMA_LIV13,		// 32
	AWARD_LIVERY_KURUMA_LIV14,		// 33
	AWARD_LIVERY_KURUMA_LIV15,		// 34
	AWARD_LIVERY_LANDST2_LIV_10,	// 35
	AWARD_LIVERY_PARAG_LIV2,		// 36
	AWARD_LIVERY_PREV_LIV11,		// 37
	AWARD_LIVERY_PREV_LIV12,		// 38
	AWARD_LIVERY_PREV_LIV13,		// 39
	AWARD_LIVERY_PREV_LIV14,		// 40
	AWARD_LIVERY_PREV_LIV15,		// 41
	AWARD_LIVERY_RSX_LIVERY11,		// 42
	AWARD_LIVERY_S80_LIV_10,		// 43
	AWARD_LIVERY_SCCAMO_LIV1,		// 44
	AWARD_LIVERY_SCCAMO_LIV10,		// 45
	AWARD_LIVERY_SCCAMO_LIV2,		// 46
	AWARD_LIVERY_SCCAMO_LIV3,		// 47
	AWARD_LIVERY_SCCAMO_LIV4,		// 48
	AWARD_LIVERY_SCCAMO_LIV5,		// 49
	AWARD_LIVERY_SCCAMO_LIV6,		// 50
	AWARD_LIVERY_SCCAMO_LIV7,		// 51
	AWARD_LIVERY_SCCAMO_LIV8,		// 52
	AWARD_LIVERY_SCCAMO_LIV9,		// 53
	AWARD_LIVERY_SEM2_LIVERY9,		// 54
	AWARD_LIVERY_STAF_LIV8,			// 55
	AWARD_LIVERY_STAF_LIV9,			// 56
	AWARD_LIVERY_SULTAN3_LIV11,		// 57
	AWARD_LIVERY_SULTAN3_LIV12,		// 58
	AWARD_LIVERY_SULTAN3_LIV13,		// 59
	AWARD_LIVERY_SULTAN3_LIV14,		// 60
	AWARD_LIVERY_SULTAN3_LIV15,		// 61
	AWARD_LIVERY_SWGR_LIV9,			// 62
	AWARD_LIVERY_THX_LIV_10,		// 63
	AWARD_LIVERY_TORD_LIV11,		// 64
	AWARD_LIVERY_TORD_LIV12,		// 65
	AWARD_LIVERY_VTC_LIV11,			// 66
	AWARD_LIVERY_VTC_LIV12,			// 67
	AWARD_LIVERY_VTC_LIV13,			// 68
	AWARD_LIVERY_VTC_LIV14,			// 69
	AWARD_LIVERY_VTC_LIV15,			// 70
	AWARD_LIVERY_XMAS_CAMO01,		// 71
	AWARD_LIVERY_XMAS_CAMO02,		// 72
	AWARD_LIVERY_XMAS_CAMO03,		// 73
	AWARD_LIVERY_XMAS_CAMO04,		// 74
	AWARD_LIVERY_XMAS_CAMO05,		// 75
	AWARD_LIVERY_XMAS_CAMO06,		// 76
	AWARD_LIVERY_XMAS_CAMO07,		// 77	
	AWARD_LIVERY_XMAS_CAMO08,		// 78
	AWARD_LIVERY_XMAS_CAMO09,		// 79
	AWARD_LIVERY_XMAS_CAMO10,		// 80
	AWARD_LIVERY_COMET7_LIV11,		// 81
	AWARD_LIVERY_DEITY_LIVERY12,	// 82
	AWARD_LIVERY_BALL7_LIV_13,		// 83
	AWARD_LIVERY_CORSITA_LIV10,		// 84
	AWARD_LIVERY_VIGR2_LIV_10		// 85
ENDENUM 

/// PURPOSE: The main struct that we will use for the car mod shop
STRUCT CARMOD_SHOP_STRUCT
	SHOP_INFO_STRUCT			sShopInfo
	SHOP_BROWSE_INFO_STRUCT		sBrowseInfo
	SHOP_LOCATE_STRUCT  		sLocateInfo
	SHOP_DIALOGUE_STRUCT 		sDialogueInfo
	SHOP_ENTRY_INFO_STRUCT 		sEntryInfo
	VEHICLE_INDEX				vehMod
	SHOP_INPUT_DATA_STRUCT 		sInputData
	CARMOD_MENU_ENUM			eMenu
	MOD_TYPE					eModSlot
	SUPERMOD_TYPE				eSupermodType
	
	INT iMenuDepth
	INT iMenuOptions[(MAX_MENU_OPTIONS/32) + 1]
	INT iMenuOption[MAX_MENU_DEPTHS]
	INT iTopOption[MAX_MENU_DEPTHS]
	INT iOptionCost[MAX_MENU_OPTIONS]
	BOOL bMenuInitialised
	BOOL bRestoreCurrentItem
	INT iNumberPlateKey[8]
	
	INT iWheelTypeControl
	INT iWheelSlotControl
	INT iWheelRimControl
	
	INT iColourGroupControl
	INT iColourSlotControl
	
	// Pay n Spray setup
	INT iPayNSprayDoorHash
	FLOAT fPayNSprayDoorRatio
	MODEL_NAMES ePayNSprayDoor
	VECTOR vPayNSprayDoor
	BOOL bPayNSprayRequired
	BOOL bPayNSprayLeave
	BOOL bPayNSprayBail
	BOOL bPayNSprayControlRemoved
	INT iPayNSprayStage
	INT iPayNSprayTimer
	VECTOR vPayNSprayLoc1, vPayNSprayLoc2
	FLOAT fPayNSprayWidth1
	VECTOR vPayNSprayLoc3, vPayNSprayLoc4
	FLOAT fPayNSprayWidth2
	BLIP_INDEX blipPayNSpray
	
	INT iCarAppSlot
	 
	#IF USE_TU_CHANGES
	INT iCarWheelsViewedBitset[NUMBER_OF_CAR_WHEEL_TYPES]
	INT iCarWheelsColorViewedBitset[2]			// 41 colors here

	/// NOTE:
	/// The last bit of iPrimary is used for crew colow
	/// The last bit - 1 of iPrimary is used for crew emblem
	/// The last bit of iSecondary is used for crew colow
	/// The last bit - 1 of iSecondary is used for crew smoke  
	INT iPrimaryCarPaintsViewedBitset[(TOTAL_NUMBER_OF_CAR_PAINTS/32) + 2]
	INT iSecondaryCarPaintsViewedBitset[(TOTAL_NUMBER_OF_CAR_PAINTS/32) + 2]
	#ENDIF
	
	BOOL bDoorTimerSet
	TIME_DATATYPE tdDoorTimer
	
	INT iPersonalCarModVar, iCarmodSlot
	PERSONAL_CAR_MOD_STAGES ePersonalCarModStage
	VEHICLE_INDEX personalCarModCloneVehicle
	PED_INDEX personalCarModClonePed
	VECTOR vBikerSlotCoords
	FLOAT fBikerSlotHeading
	CAMERA_INDEX personalCarModIntroCam[2]
	VEHICLE_INDEX	remoteVehMod[NUM_NETWORK_PLAYERS]
	#IF IS_DEBUG_BUILD
	BOOL bDrawDebugStuff
	#ENDIF
ENDSTRUCT

#IF IS_DEBUG_BUILD
	FUNC STRING debug_GET_CARMOD_MENU_NAME(CARMOD_MENU_ENUM eMenu)
		SWITCH eMenu
			CASE CMM_MAIN 	RETURN "CMM_MAIN " BREAK
			
			CASE CMM_BUY	RETURN "CMM_BUY" BREAK
			
			CASE CMM_MOD	RETURN "CMM_MOD" BREAK
			CASE CMM_MAINTAIN	RETURN "CMM_MAINTAIN" BREAK
			CASE CMM_ARMOUR	RETURN "CMM_ARMOUR" BREAK
			CASE CMM_BRAKES	RETURN "CMM_BRAKES" BREAK
			CASE CMM_BODYWORK	RETURN "CMM_BODYWORK" BREAK
			CASE CMM_BULLBARS	RETURN "CMM_BULLBARS" BREAK
			CASE CMM_BUMPERS	RETURN "CMM_BUMPERS" BREAK
			CASE CMM_CHASSIS	RETURN "CMM_CHASSIS" BREAK
			CASE CMM_ENGINE	RETURN "CMM_ENGINE" BREAK
			CASE CMM_ENGINEBAY	RETURN "CMM_ENGINEBAY" BREAK
			CASE CMM_EXHAUST	RETURN "CMM_EXHAUST" BREAK
			CASE CMM_EXPLOSIVES	RETURN "CMM_EXPLOSIVES" BREAK
			CASE CMM_FAIRING	RETURN "CMM_FAIRING" BREAK
			CASE CMM_FENDERS	RETURN "CMM_FENDERS" BREAK
			CASE CMM_FRAME	RETURN "CMM_FRAME" BREAK
			CASE CMM_FRONTFORKS	RETURN "CMM_FRONTFORKS" BREAK
			CASE CMM_FRONTMUDGUARD	RETURN "CMM_FRONTMUDGUARD" BREAK
			CASE CMM_FRONTSEAT	RETURN "CMM_FRONTSEAT" BREAK
			CASE CMM_FUELTANK	RETURN "CMM_FUELTANK" BREAK
			CASE CMM_GOLD_PREP	RETURN "CMM_GOLD_PREP" BREAK
			CASE CMM_GOLD_PREP2	RETURN "CMM_GOLD_PREP2" BREAK
			CASE CMM_GRILL	RETURN "CMM_GRILL" BREAK
			CASE CMM_HANDLEBARS	RETURN "CMM_HANDLEBARS" BREAK
			CASE CMM_HEADLIGHTS	RETURN "CMM_HEADLIGHTS" BREAK
			CASE CMM_HOOD	RETURN "CMM_HOOD" BREAK
			CASE CMM_HORN	RETURN "CMM_HORN" BREAK
			CASE CMM_LIGHTS	RETURN "CMM_LIGHTS" BREAK
			CASE CMM_LIGHTS_HEAD	RETURN "CMM_LIGHTS_HEAD" BREAK
			CASE CMM_LIGHTS_NEON	RETURN "CMM_LIGHTS_NEON" BREAK
			CASE CMM_INSURANCE RETURN "CMM_INSURANCE" BREAK	// Now called LOSS/THEFT PREVENTION
			CASE CMM_MIRRORS	RETURN "CMM_MIRRORS" BREAK
			CASE CMM_PLATES	RETURN "CMM_PLATES" BREAK
			CASE CMM_PLATEHOLDER	RETURN "CMM_PLATEHOLDER" BREAK
			CASE CMM_PUSHBAR	RETURN "CMM_PUSHBAR" BREAK
			CASE CMM_REARMUDGUARD	RETURN "CMM_REARMUDGUARD" BREAK
			CASE CMM_REARSEAT	RETURN "CMM_REARSEAT" BREAK
			CASE CMM_PAINT_JOB	RETURN "CMM_PAINT_JOB" BREAK	// Now called RESPRAY
			CASE CMM_ROLLCAGE	RETURN "CMM_ROLLCAGE" BREAK
			CASE CMM_ROOF	RETURN "CMM_ROOF" BREAK
			CASE CMM_SADDLEBAGS	RETURN "CMM_SADDLEBAGS" BREAK
			CASE CMM_SELL	RETURN "CMM_SELL" BREAK
			CASE CMM_SIDESTEP	RETURN "CMM_SIDESTEP" BREAK
			CASE CMM_SKIRTS	RETURN "CMM_SKIRTS" BREAK
			CASE CMM_SPAREWHEEL	RETURN "CMM_SPAREWHEEL" BREAK
			CASE CMM_SPOILER	RETURN "CMM_SPOILER" BREAK
			CASE CMM_SUSPENSION	RETURN "CMM_SUSPENSION" BREAK
			CASE CMM_TAILGATE	RETURN "CMM_TAILGATE" BREAK
			CASE CMM_TRACKER	RETURN "CMM_TRACKER" BREAK
			CASE CMM_TRANSMISSION	RETURN "CMM_TRANSMISSION" BREAK
			CASE CMM_TRUCKBED	RETURN "CMM_TRUCKBED" BREAK
			CASE CMM_TRUNK	RETURN "CMM_TRUNK" BREAK
			CASE CMM_TURBO	RETURN "CMM_TURBO" BREAK
			CASE CMM_VIN	RETURN "CMM_VIN" BREAK
			CASE CMM_WHEELS_MAIN	RETURN "CMM_WHEELS_MAIN" BREAK
			CASE CMM_WHEELS	RETURN "CMM_WHEELS" BREAK
			CASE CMM_WHEEL_COL	RETURN "CMM_WHEEL_COL" BREAK
			CASE CMM_WHEEL_ACCS	RETURN "CMM_WHEEL_ACCS" BREAK
			CASE CMM_WHEELIEBAR	RETURN "CMM_WHEELIEBAR" BREAK
			CASE CMM_WINDOWS	RETURN "CMM_WINDOWS" BREAK
			
			CASE CMM_SUPERMOD	RETURN "CMM_SUPERMOD" BREAK
			
			CASE CMM_SUPERMOD_PLTHOLDER	RETURN "CMM_SUPERMOD_PLTHOLDER" BREAK
			CASE CMM_SUPERMOD_PLTVANITY	RETURN "CMM_SUPERMOD_PLTVANITY" BREAK
			CASE CMM_SUPERMOD_INTERIOR1	RETURN "CMM_SUPERMOD_INTERIOR1" BREAK
			CASE CMM_SUPERMOD_INTERIOR2	RETURN "CMM_SUPERMOD_INTERIOR2" BREAK
			CASE CMM_SUPERMOD_INTERIOR3	RETURN "CMM_SUPERMOD_INTERIOR3" BREAK
			CASE CMM_SUPERMOD_INTERIOR4	RETURN "CMM_SUPERMOD_INTERIOR4" BREAK
			CASE CMM_SUPERMOD_INTERIOR5	RETURN "CMM_SUPERMOD_INTERIOR5" BREAK
			CASE CMM_SUPERMOD_SEATS	RETURN "CMM_SUPERMOD_SEATS" BREAK
			CASE CMM_SUPERMOD_STEERING	RETURN "CMM_SUPERMOD_STEERING" BREAK
			CASE CMM_SUPERMOD_KNOB	RETURN "CMM_SUPERMOD_KNOB" BREAK
			CASE CMM_SUPERMOD_PLAQUE	RETURN "CMM_SUPERMOD_PLAQUE" BREAK
			CASE CMM_SUPERMOD_ICE	RETURN "CMM_SUPERMOD_ICE" BREAK
			CASE CMM_SUPERMOD_TRUNK	RETURN "CMM_SUPERMOD_TRUNK" BREAK
			CASE CMM_SUPERMOD_HYDRO	RETURN "CMM_SUPERMOD_HYDRO" BREAK
			CASE CMM_SUPERMOD_ENGINEBAY1	RETURN "CMM_SUPERMOD_ENGINEBAY1" BREAK
			CASE CMM_SUPERMOD_ENGINEBAY2	RETURN "CMM_SUPERMOD_ENGINEBAY2" BREAK
			CASE CMM_SUPERMOD_ENGINEBAY3	RETURN "CMM_SUPERMOD_ENGINEBAY3" BREAK
			CASE CMM_SUPERMOD_CHASSIS2	RETURN "CMM_SUPERMOD_CHASSIS2" BREAK
			CASE CMM_SUPERMOD_CHASSIS3	RETURN "CMM_SUPERMOD_CHASSIS3" BREAK
			CASE CMM_SUPERMOD_CHASSIS4	RETURN "CMM_SUPERMOD_CHASSIS4" BREAK
			CASE CMM_SUPERMOD_CHASSIS5	RETURN "CMM_SUPERMOD_CHASSIS5" BREAK
			CASE CMM_SUPERMOD_DOOR_L	RETURN "CMM_SUPERMOD_DOOR_L" BREAK
			CASE CMM_SUPERMOD_DOOR_R	RETURN "CMM_SUPERMOD_DOOR_R" BREAK
			CASE CMM_SUPERMOD_LIVERY	RETURN "CMM_SUPERMOD_LIVERY" BREAK
			
			CASE CMM_CHASSIS_GROUP	RETURN "CMM_CHASSIS_GROUP" BREAK
			CASE CMM_INTERIOR_GROUP	RETURN "CMM_INTERIOR_GROUP" BREAK
			CASE CMM_PLATE_GROUP	RETURN "CMM_PLATE_GROUP" BREAK
			CASE CMM_ENGINE_GROUP	RETURN "CMM_ENGINE_GROUP" BREAK
			
			CASE CMM_DIALS	RETURN "CMM_DIALS" BREAK
			CASE CMM_TRIM	RETURN "CMM_TRIM" BREAK
			CASE CMM_LIGHT_COLOUR RETURN "CMM_LIGHT_COLOUR" BREAK
			CASE CMM_DEFLECTOR RETURN "CMM_DEFLECTOR" BREAK
			CASE CMM_ORNAMENTS RETURN "CMM_ORNAMENTS" BREAK
			CASE CMM_FENDERSTWO RETURN "CMM_FENDERSTWO" BREAK
			CASE CMM_WAITING_ON_PLAYERS RETURN "CMM_WAITING_ON_PLAYERS" BREAK
			CASE CMM_PRIMARY_COLOUR RETURN "CMM_PRIMARY_COLOUR" BREAK
			CASE CMM_SECONDARY_COLOUR RETURN "CMM_SECONDARY_COLOUR" BREAK
			CASE CMM_BUMPER_GROUP RETURN "CMM_BUMPER_GROUP" BREAK
			CASE CMM_BUMPER_SUBMENU_GROUP RETURN "CMM_BUMPER_SUBMENU_GROUP" BREAK
			CASE CMM_HOOD_GROUP RETURN "CMM_HOOD_GROUP" BREAK
			CASE CMM_ROOF_GROUP RETURN "CMM_ROOF_GROUP" BREAK
			CASE CMM_SUPERMOD_TWO RETURN "CMM_SUPERMOD_TWO" BREAK
		ENDSWITCH
		
		CASSERTLN(DEBUG_SHOPS, "debug_GET_CARMOD_MENU_NAME missing menu ", eMenu)
		RETURN ""
	ENDFUNC
	FUNC STRING debug_GET_MOD_SHOP_CAM_NAME(MOD_SHOP_CAM_ENUM eCam)
		SWITCH eCam
			CASE MOD_CAM_DUMMY	RETURN "MOD_CAM_DUMMY" BREAK
			CASE MOD_CAM_FRONT	RETURN "MOD_CAM_FRONT" BREAK
			CASE MOD_CAM_REAR	RETURN "MOD_CAM_REAR" BREAK
			CASE MOD_CAM_SIDE_L	RETURN "MOD_CAM_SIDE_L" BREAK
			CASE MOD_CAM_SIDE_R	RETURN "MOD_CAM_SIDE_R" BREAK
			CASE MOD_CAM_REAR_L	RETURN "MOD_CAM_REAR_L" BREAK
			CASE MOD_CAM_REAR_R	RETURN "MOD_CAM_REAR_R" BREAK
			CASE MOD_CAM_FULL	RETURN "MOD_CAM_FULL" BREAK
			CASE MAX_MOD_CAMS	RETURN "MAX_MOD_CAMS" BREAK
		ENDSWITCH
		
		RETURN ""
	ENDFUNC
#ENDIF


PROC RESET_CARMOD_SHOP_DATA(CARMOD_SHOP_STRUCT &sData)
	RESET_SHOP_BROWSE_INFO_STRUCT(sData.sBrowseInfo)
	RESET_SHOP_INFO_STRUCT(sData.sShopInfo)
	RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
	RESET_SHOP_LOCATE_STRUCT(sData.sLocateInfo)
	
	sData.eMenu = CMM_MAIN
	sData.vehMod = NULL
	sData.iMenuDepth = 0
	sData.iMenuOptions[0] = 0
	sData.iMenuOptions[1] = 0
	
	INT i
	REPEAT MAX_MENU_DEPTHS i
		sData.iMenuOption[i] = 0
		sData.iTopOption[i] = 0
	ENDREPEAT
	REPEAT 30 i
		sData.iOptionCost[i] = 0
	ENDREPEAT
	
	sData.bMenuInitialised = FALSE
	sData.bRestoreCurrentItem = FALSE
	
	REPEAT 8 i
		sData.iNumberPlateKey[i] = 0
	ENDREPEAT
	 
	sData.iPayNSprayDoorHash = 0
	sData.ePayNSprayDoor = DUMMY_MODEL_FOR_SCRIPT
	sData.vPayNSprayDoor = <<0,0,0>>
	sData.bPayNSprayRequired = FALSE
	sData.bPayNSprayBail = FALSE
	sData.bPayNSprayControlRemoved = FALSE
	sData.iPayNSprayStage = 0
	sData.iPayNSprayTimer = 0
	sData.vPayNSprayLoc1 = <<0,0,0>>
	sData.vPayNSprayLoc2 = <<0,0,0>>
	sData.vPayNSprayLoc3 = <<0,0,0>>
	sData.vPayNSprayLoc4 = <<0,0,0>>
	sData.fPayNSprayWidth1 = 0.0
	sData.fPayNSprayWidth2 = 0.0
ENDPROC


/// PURPOSE: Fills the specified struct will the shop information and returns TRUE if successful
FUNC BOOL GET_CARMOD_SHOP_DATA(CARMOD_SHOP_STRUCT &sData, SHOP_NAME_ENUM eShop)

	sData.sShopInfo.bDataSet = FALSE
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(sData.iPersonalCarModVar)) // Mobilemod
	
	IF (iInteriorCheck = iInterior1)
		sData.sShopInfo.eShop 				= eShop
		
		// Shop keeper position
		
		IF eShop = CARMOD_SHOP_01_AP
			#IF FEATURE_GEN9_EXCLUSIVE
			sData.sShopInfo.sShopKeeper.model = S_M_M_AutoShop_01
			#ENDIF
			
			#IF NOT FEATURE_GEN9_EXCLUSIVE
			sData.sShopInfo.sShopKeeper.model = IG_HAO
			#ENDIF
		ELSE
			sData.sShopInfo.sShopKeeper.model = S_M_M_AutoShop_01
		ENDIF
		
		sData.sShopInfo.sShopKeeper.vPosIdle = <<-1156.3611, -2000.5420, 12.1803>>
		sData.sShopInfo.sShopKeeper.fHeadIdle = 46.9352
		sData.sShopInfo.bDataSet = TRUE
	
	ELIF (iInteriorCheck = iInterior3)
	
		sData.sShopInfo.eShop 				= eShop
				
		// Shop keeper position
		sData.sShopInfo.sShopKeeper.model = S_M_M_AutoShop_01
		sData.sShopInfo.sShopKeeper.vPosIdle = <<107.4948, 6629.7529, 30.7872>>
		sData.sShopInfo.sShopKeeper.fHeadIdle = 51.6177
		
		sData.sShopInfo.bDataSet = TRUE
	
	ELIF (iInteriorCheck = iInterior5)
	
		sData.sShopInfo.eShop 				= eShop
		
		// Shop keeper position
		sData.sShopInfo.sShopKeeper.model = S_M_M_AutoShop_01
		sData.sShopInfo.sShopKeeper.vPosIdle = <<737.8705, -1078.1737, 21.1686>>
		sData.sShopInfo.sShopKeeper.fHeadIdle = 271.5469
		
		sData.sShopInfo.bDataSet = TRUE
	ELIF (iInteriorCheck = iInteriorSuper)
	
		sData.sShopInfo.eShop 				= eShop
		
		// Shop keeper position
		sData.sShopInfo.sShopKeeper.model = IG_BENNY
		sData.sShopInfo.sShopKeeper.vPosIdle = <<-216.1331, -1318.6567, 29.8893>>
		sData.sShopInfo.sShopKeeper.fHeadIdle = 122.6285
		
		sData.sShopInfo.bDataSet = TRUE
	ELIF (iInteriorCheck = iInteriorMobile)
	
		sData.sShopInfo.eShop 				= eShop
		
		// Shop keeper position
		SWITCH INT_TO_ENUM(PERSONAL_CAR_MOD_VARIATION, sData.iPersonalCarModVar)
			CASE PERSONAL_CAR_MOD_VARIATION_BIKER_ONE
			CASE PERSONAL_CAR_MOD_VARIATION_BIKER_TWO
				sData.sShopInfo.sShopKeeper.model = S_M_Y_XMech_02
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_ONE
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_TWO
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_THREE
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_FOUR
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_F_CarDesign_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_IE_LARGE
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WareMech_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_TRUCK
			CASE PERSONAL_CAR_MOD_VARIATION_BUNKER
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapExp_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_HANGAR
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_smugmech_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_AOC
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapExp_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_BASE
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapExp_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_BUSINESS_HUB
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapExp_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_HACKER_TRUCK
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapExp_01"))
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_ARENA_WARS
				sData.sShopInfo.sShopKeeper.model 			= INT_TO_ENUM(MODEL_NAMES, HASH("ig_sacha"))
				sData.sShopInfo.sShopKeeper.modelTwo 		= INT_TO_ENUM(MODEL_NAMES, HASH("MP_F_BennyMech_01"))
				sData.sShopInfo.sShopKeeper.modelThree 		= INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapExp_01"))
				IF GET_INTERIOR_FLOOR_INDEX() = ciARENA_GARAGE_FLOOR_WORKSHOP
					sData.sShopInfo.sShopKeeper.vPosTwoIdle 	= <<202.4, 5190.0, -88.6>>
					sData.sShopInfo.sShopKeeper.fHeadTwoIdle 	= 165.7900
					
					sData.sShopInfo.sShopKeeper.vPosThreeIdle 	= <<205.3, 5198.1, -88.6>>
					sData.sShopInfo.sShopKeeper.fHeadThreeIdle 	= 197.8665
				ELSE
					sData.sShopInfo.sShopKeeper.vPosTwoIdle 	= <<2462.6248, -3807.4131, 151.2768>>
					sData.sShopInfo.sShopKeeper.fHeadTwoIdle 	= 42.6383
					
					sData.sShopInfo.sShopKeeper.vPosThreeIdle 	= <<2461.8010, -3798.7068, 150.2137>>
					sData.sShopInfo.sShopKeeper.fHeadThreeIdle 	= 144.2312
				ENDIF
			BREAK
			#IF FEATURE_TUNER
			CASE PERSONAL_CAR_MOD_VARIATION_CAR_MEET
				#IF NOT FEATURE_GEN9_EXCLUSIVE
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Autoshop_01"))
				#ENDIF
				
				#IF FEATURE_GEN9_EXCLUSIVE
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("IG_HAO_02"))
				#ENDIF
			BREAK
			#ENDIF
			CASE PERSONAL_CAR_MOD_VARIATION_FIXER_HQ
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_STUDIOASSIST_02"))
			BREAK
			#IF FEATURE_DLC_2_2022
			CASE PERSONAL_CAR_MOD_VARIATION_JUGGALO_HIDEOUT
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("IG_AcidLabCook"))
			BREAK
			#ENDIF
			DEFAULT
				sData.sShopInfo.sShopKeeper.model = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapexp_01"))
			BREAK
			
		ENDSWITCH
		
		GET_PERSONAL_CAR_MOD_SHOP_KEEPER_COORDS_AND_HEADING(sData.iPersonalCarModVar,sData.sShopInfo.sShopKeeper.vPosIdle,sData.sShopInfo.sShopKeeper.fHeadIdle, sData.sShopInfo.sShopKeeper.iPositionVariant )
		
		sData.sShopInfo.bDataSet = TRUE
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ELIF (iInteriorCheck = iInteriorMobile)
		eBaseShop = CARMOD_SHOP_PERSONALMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, sData.sShopInfo.sShopKeeper.vPosIdle)
	ALIGN_SHOP_HEADING(eBaseShop, eShop, sData.sShopInfo.sShopKeeper.fHeadIdle)
	
	#IF IS_DEBUG_BUILD
		IF sData.sShopInfo.bDataSet
			PRINTSTRING("\nUsing data set for ")PRINTSTRING(GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(sData.sShopInfo.eShop)))PRINTSTRING(" - ")PRINTSTRING(GET_SHOP_NAME(sData.sShopInfo.eShop))PRINTNL()
		ELSE
			SCRIPT_ASSERT("Unable to find suitable carmod shop data.")
			PRINTSTRING("\ncarmod_shop : Unable to find suitable carmod shop data.")PRINTNL()
		ENDIF
	#ENDIF
	
	RETURN sData.sShopInfo.bDataSet
ENDFUNC

FUNC BOOL GET_CARMOD_SHOP_ANIM_POSITION_AND_ROTATION(SHOP_NAME_ENUM eShop, STRING sAnim, VECTOR &vPos, VECTOR &vRot , INT iPersonalCarModVar , INT &iPositionVariant)
	
	vPos = <<0.0,0.0,0.0>>
	vRot = <<0.0,0.0,0.0>>
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(iPersonalCarModVar)) // Mobilemod
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	IF (iInteriorCheck = iInterior1) // LSC
		IF GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_in")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_base")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_out")
			vPos = << -1155.943, -2000.468, 13.175 >>	vRot = << 0.000, 0.000, 52.000 >>
		ELIF GET_HASH_KEY(sAnim) = GET_HASH_KEY("stand_base")
			vPos = << -1155.943, -2000.468, 13.175 >>	vRot = << 0.000, 0.000, 180.000 >>
		ENDIF
		
	ELIF (iInteriorCheck = iInterior3)// Biker
		IF GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_in")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_base")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_out")
			vPos = << 107.463, 6629.725, 31.813 >>		vRot = << 0.000, 0.000, 48.000 >>
		ELIF GET_HASH_KEY(sAnim) = GET_HASH_KEY("stand_base")
			vPos = << 107.638, 6629.650, 31.813 >>	vRot = << 0.000, 0.000, -133.750 >>
		ENDIF
		
	ELIF (iInteriorCheck = iInterior5) // Underpass
		IF GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_in")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_base")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_out")
			vPos = << 737.885, -1078.192, 22.160 >>		vRot = << 0.000, 0.000, -80.000 >>
		ELIF GET_HASH_KEY(sAnim) = GET_HASH_KEY("stand_base")
			vPos = << 737.622, -1078.380, 22.160 >>		vRot = << 0.000, 0.000, 142.750 >>
		ENDIF
	ELIF (iInteriorCheck = iInteriorSuper)
		/*SUPERMOD_TODO*/
		IF GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_in")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_base")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_out")
			vPos = <<-216.1331, -1318.6567, 29.8893>>		vRot = << 0.000, 0.000, 122.6285 >>
		ELIF GET_HASH_KEY(sAnim) = GET_HASH_KEY("stand_base")
			vPos = <<-216.1331, -1318.6567, 29.8893>>		vRot = << 0.000, 0.000, 122.6285 >>
		ENDIF
	ELIF (iInteriorCheck = iInteriorMobile)
		IF GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_in")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_base")
		OR GET_HASH_KEY(sAnim) = GET_HASH_KEY("work2_out")
			vRot = << 0.000, 0.000, 0.0 >>
			GET_PERSONAL_CAR_MOD_SHOP_KEEPER_COORDS_AND_HEADING(iPersonalCarModVar,vPos,vRot.z, iPositionVariant)	
		ELIF GET_HASH_KEY(sAnim) = GET_HASH_KEY("stand_base")
			vRot = << 0.000, 0.000, 0.0 >>
			GET_PERSONAL_CAR_MOD_SHOP_KEEPER_COORDS_AND_HEADING(iPersonalCarModVar,vPos,vRot.z, iPositionVariant)
		ENDIF
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ELIF (iInteriorCheck = iInteriorMobile)
		eBaseShop = CARMOD_SHOP_PERSONALMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vPos)
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, vRot)
	
	RETURN (NOT ARE_VECTORS_EQUAL(vPos, <<0.0,0.0,0.0>>) OR NOT ARE_VECTORS_EQUAL(vRot, <<0.0,0.0,0.0>>))
ENDFUNC

/// PURPOSE: Helper function to fill the camera struct
PROC FILL_CAMERA_DATA_CARMOD(SHOP_CAMERA_STRUCT &sCamera, VECTOR paramCoord, VECTOR paramRot, FLOAT paramFOV, BOOL paramSetup)
	sCamera.vPos = paramCoord
	sCamera.vRot = paramRot
	sCamera.fFOV = paramFOV
	sCamera.bSetup = paramSetup
ENDPROC

/// PURPOSE: Sets up all the intro data for the current car mod shop
PROC GET_CARMOD_SHOP_INTRO_DATA(CARMOD_SHOP_STRUCT &sData, BOOL bOnFootEntry)

	FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[0], << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0.0, FALSE)
	FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[1], << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0.0, FALSE)
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(sData.iPersonalCarModVar)) // Mobilemod
	
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop))
	
	IF (iInteriorCheck = iInterior1)
		IF bOnFootEntry
			// Set player on foot browse
			sData.sLocateInfo.vPlayerBrowsePos = << -1157.7800, -2002.8582, 12.1668 >>
			sData.sLocateInfo.fPlayerBrowseHead = 157.9579
			// Set intro cams
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[0], << -1156.9397, -2004.9951, 13.9017 >>, << -8.3401, 0.0000, 48.1026 >>, 45.0, TRUE)
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[1], << -1156.4581, -2004.2922, 13.7499 >>, << -8.3401, -0.0000, 62.8555 >>, 45.0, TRUE)
		ELSE
//			// Set player vehicle browse
//			sData.sLocateInfo.vPlayerBrowsePos = <<-1157.406250,-2007.898315,12.180257>>  //<< -1155.110, -2006.355, 12.825 >>
//			sData.sLocateInfo.fPlayerBrowseHead = 338.0777
//			// Alternative
//			sData.sLocateInfo.vPlayerEndPos = <<-1157.406250,-2007.898315,12.180257>>  // << -1155.110, -2006.355, 12.825 >>
//			sData.sLocateInfo.fPlayerEndHead = 156.250
			
			// Set player vehicle browse
			sData.sLocateInfo.vPlayerBrowsePos = <<-1156.727, -2007.229, 12.180257>>  // << -1155.110, -2006.355, 12.825 >>
			sData.sLocateInfo.fPlayerBrowseHead = 156.250
			// Alternative
			sData.sLocateInfo.vPlayerEndPos = <<-1157.406250,-2007.898315,12.180257>>  //<< -1155.110, -2006.355, 12.825 >>
			sData.sLocateInfo.fPlayerEndHead = 338.0777
			
			
			// Set intro cams
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[0], << -1154.0126, -2014.0784, 13.9018 >>, << -8.2053, -0.0765, 24.0591 >>, 38.6020, TRUE)
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[1], << -1154.7230, -2013.2655, 13.6235 >>, << -8.2053, -0.0765, 24.0591 >>, 38.6020, TRUE)
		ENDIF

	ELIF (iInteriorCheck = iInterior3)
		IF bOnFootEntry
			// Set player on foot browse
			sData.sLocateInfo.vPlayerBrowsePos = <<108.6923, 6628.9463, 30.7872>>
			sData.sLocateInfo.fPlayerBrowseHead = 61.8635
			// Set intro cams
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[0], << 962.5477, -108.3911, 75.0029 >>, << -7.6879, 0.0000, -50.6592 >>, 42.1612, TRUE)
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[1], << 963.0760, -108.7499, 74.9057 >>, << -5.2523, -0.0000, -32.4065 >>, 42.1612, TRUE)
		ELSE
			// Set player vehicle browse
			sData.sLocateInfo.vPlayerBrowsePos = <<111.7790, 6625.3140, 30.7872>>
			sData.sLocateInfo.fPlayerBrowseHead = 44.3001
			// Alternative
			sData.sLocateInfo.vPlayerEndPos = <<112.0473, 6625.1709, 30.7872>>
			sData.sLocateInfo.fPlayerEndHead = 224.6659
			// Set intro cams
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[0], << 960.0109, -107.3940, 75.2301 >>, << -15.9799, 0.0000, -88.5405 >>, 45.3778, TRUE)
			FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[1], << 960.1140, -106.6346, 75.2203 >>, << -15.9799, 0.0000, -96.7096 >>, 45.3778, TRUE)
		ENDIF
			
	ELIF (iInteriorCheck = iInterior5) 
		// Set player vehicle browse
		sData.sLocateInfo.vPlayerBrowsePos = <<730.845215,-1088.338257,21.169001>> //<<731.7382, -1086.9537, 21.1689>>
		sData.sLocateInfo.fPlayerBrowseHead = 319.3350
		// Alternative
		sData.sLocateInfo.vPlayerEndPos = <<732.0129, -1086.6846, 21.1689>>
		sData.sLocateInfo.fPlayerEndHead = 132.6685
		
	ELIF (iInteriorCheck = iInteriorSuper)
		/*SUPERMOD_TODO*/
		// Set player vehicle browse
		sData.sLocateInfo.vPlayerBrowsePos = <<-211.8880, -1324.1129, 29.8893>>
		sData.sLocateInfo.fPlayerBrowseHead = 163.8972
		// Alternative
		sData.sLocateInfo.vPlayerEndPos = <<-211.8880, -1324.1129, 29.8893>>
		sData.sLocateInfo.fPlayerEndHead = 163.8972
	ELIF (iInteriorCheck = iInteriorMobile)
		GET_PERSONAL_CAR_MOD_BROWSING_DETAILS(sData.iPersonalCarModVar,sData.sLocateInfo.vPlayerBrowsePos,sData.sLocateInfo.fPlayerBrowseHead,sData.sLocateInfo.vPlayerEndPos,sData.sLocateInfo.fPlayerEndHead, sData.iCarmodSlot )
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ELIF (iInteriorCheck = iInteriorMobile)
		eBaseShop = CARMOD_SHOP_PERSONALMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.sLocateInfo.vPlayerBrowsePos)
	ALIGN_SHOP_HEADING(eBaseShop, sData.sShopInfo.eShop, sData.sLocateInfo.fPlayerBrowseHead)
	
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.sLocateInfo.vPlayerEndPos)
	ALIGN_SHOP_HEADING(eBaseShop, sData.sShopInfo.eShop, sData.sLocateInfo.fPlayerEndHead)
	
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.sShopInfo.sCameras[0].vPos)
	ALIGN_SHOP_ROTATION(eBaseShop, sData.sShopInfo.eShop, sData.sShopInfo.sCameras[0].vRot)
	
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.sShopInfo.sCameras[1].vPos)
	ALIGN_SHOP_ROTATION(eBaseShop, sData.sShopInfo.eShop, sData.sShopInfo.sCameras[1].vRot)
	
ENDPROC

FUNC BOOL SETUP_CARMOD_CAM(SHOP_NAME_ENUM eShop, MOD_SHOP_CAM_ENUM eCam, MOD_SHOP_CAM_ENUM &RetCam, FLOAT &fRetYOffset, FLOAT &fRetZOffset  , INT iPersonalCarModVar  , MODEL_NAMES mdl = DUMMY_MODEL_FOR_SCRIPT, BOOL overrideY = TRUE, BOOL overrideZ = TRUE)
	DEBUG_PRINTCALLSTACK()
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(iPersonalCarModVar)) // Mobilemod
	
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	
	/* cycle cam - test - ask ken about this properly later
	INT i = ENUM_TO_INT(eCam)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
		i ++
		IF (i >= ENUM_TO_INT(MAX_MOD_CAMS))
			i = 1
		ENDIF

		eCam = INT_TO_ENUM(MOD_SHOP_CAM_ENUM, i)
	ENDIF
	*/
	
	IF (iInteriorCheck = iInterior1)// LSC
		IF eCam = MOD_CAM_FULL
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 64.40
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.310
			ENDIF
		ELIF eCam = MOD_CAM_FRONT
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 15.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.350
			ENDIF
		ELIF eCam = MOD_CAM_REAR
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 15.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.350
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_REAR_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_REAR_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18-60
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ENDIF
	ELIF (iInteriorCheck = iInterior3)// Biker
		IF eCam = MOD_CAM_FULL
			eCam = MOD_CAM_FRONT 
		ENDIF
		
		IF (mdl = SANDKING) OR (mdl = SANDKING2) OR (mdl = GRANGER)
			IF (eCam = MOD_CAM_SIDE_R)
				eCam = MOD_CAM_SIDE_L
			ENDIF
		ENDIF
		
		IF eCam = MOD_CAM_FRONT
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 5.6
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = -0.291
			ENDIF
		ELIF eCam = MOD_CAM_REAR
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 24.7
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = -0.005
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 27.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.731
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 22.6
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 1.662
			ENDIF
		ELIF eCam = MOD_CAM_REAR_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 27.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.731
			ENDIF
		ELIF eCam = MOD_CAM_REAR_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 22.6-60
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 1.662
			ENDIF
		ENDIF
	ELIF (iInteriorCheck = iInterior5)// Underpass
		IF eCam = MOD_CAM_FRONT OR eCam = MOD_CAM_FULL
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 1.25
			ENDIF
		ELIF eCam = MOD_CAM_REAR
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 32.700
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = -0.263
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 42.5
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.731
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 5.026
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.026
			ENDIF
		ELIF eCam = MOD_CAM_REAR_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 42.5
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.731
			ENDIF
		ELIF eCam = MOD_CAM_REAR_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 5.026-60
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.026
			ENDIF
		ENDIF
		
	ELIF (iInteriorCheck = iInteriorSuper)
		/*SUPERMOD_TODO*/
		IF eCam = MOD_CAM_FULL
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 64.40
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.310
			ENDIF
		ELIF eCam = MOD_CAM_FRONT
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 15.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.350
			ENDIF
		ELIF eCam = MOD_CAM_REAR
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 15.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.350
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_REAR_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_REAR_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18-60
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ENDIF
	ELIF (iInteriorCheck = iInteriorMobile)
		IF eCam = MOD_CAM_FULL
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 64.40
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.310
			ENDIF
		ELIF eCam = MOD_CAM_FRONT
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 15.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.350
			ENDIF
		ELIF eCam = MOD_CAM_REAR
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 15.0
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.350
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_SIDE_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_REAR_L
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ELIF eCam = MOD_CAM_REAR_R
			RetCam = eCam
			IF (overrideY)
				fRetYOffset = 18-60
			ENDIF
			
			IF (overrideZ)
				fRetZOffset = 0.590
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG3LN(DEBUG_SHOPS, "SETUP_CARMOD_CAM(", eShop,
			", ", debug_GET_MOD_SHOP_CAM_NAME(eCam),
			PICK_STRING(RetCam = eCam, "", ", RetCam:"),
			PICK_STRING(RetCam = eCam, "", debug_GET_MOD_SHOP_CAM_NAME(RetCam)),
			", fRetYOffset:", fRetYOffset,
			", fRetZOffset:", fRetZOffset, ")")
	DEBUG_PRINTCALLSTACK()
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the coords of the front entry.
///    NOTE: We use these coords to determine if player is trying to get in garage.
FUNC BOOL GET_CARMOD_ENTRY_COORDS(SHOP_NAME_ENUM eShop, VECTOR &vLoc1, VECTOR &vLoc2, FLOAT &fLocWidth)
	vLoc1 = <<0.0,0.0,0.0>>
	vLoc2 = <<0.0,0.0,0.0>>
	fLocWidth = 0.0
	
	IF (eShop = CARMOD_SHOP_01_AP)
		vLoc1 = <<-1138.812134,-1991.065308,12.166301>>
		vLoc2 = <<-1145.783691,-1983.836426,16.160961>> 
		fLocWidth = 13.125000
	ELIF (eShop = CARMOD_SHOP_05_ID2)
		vLoc1 = <<719.727905,-1083.272827,20.440435>>
		vLoc2 = <<719.954590,-1093.445557,25.324745>> 
		fLocWidth = 9.500000
	ELIF (eShop = CARMOD_SHOP_06_BT1)
		vLoc1 = <<-357.792145,-129.713257,37.679173>>
		vLoc2 = <<-360.728882,-137.642380,41.679173>> 
		fLocWidth = 10.750000
	ELIF (eShop = CARMOD_SHOP_07_CS1)
		vLoc1 = <<108.742729,6612.335938,31.000193>>
		vLoc2 = <<119.628311,6623.331055,34.958996>>
		fLocWidth = 10.437500
	ELIF (eShop = CARMOD_SHOP_08_CS6)
		vLoc1 = <<1188.101685,2649.342285,36.022659>>
		vLoc2 = <<1169.038208,2649.437744,40.851032>>
		fLocWidth = 10.750000
	ELIF (eShop = CARMOD_SHOP_SUPERMOD)
		/*SUPERMOD_TODO*/
		vLoc1 = <<-202.156021,-1307.185303,35.571644>>
		vLoc2 = <<-208.437683,-1307.250366,29.576435>> 
		fLocWidth = 8.000000
	ENDIF
	
	RETURN (fLocWidth != 0.0)
ENDFUNC

/// PURPOSE: Returns the coords and cam info for intro
PROC GET_CARMOD_SHOP_INTRO_CUT_DATA(VEHICLE_INDEX pedVeh, 
									SHOP_NAME_ENUM eShop, 
									VECTOR &vCoords, 
									FLOAT &fHeading, 
									VECTOR &vCamPos1, 
									VECTOR &vCamRot1, 
									FLOAT &fCamFOV1, 
									VECTOR &vCamPos2, 
									VECTOR &vCamRot2, 
									FLOAT &fCamFOV2, 
									INT &Duration, 
									CAMERA_GRAPH_TYPE &GraphTypePos,
									INT iPersonalCarModVar,
									INT iCarmodSlot)
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(iPersonalCarModVar)) // Mobilemod
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))
	
	IF (iInteriorCheck = iInterior1)
		vCamPos1 = <<-1155.423828,-2018.869995,13.032037>>
		vCamRot1 = <<1.669779,0.017783,-0.949821>>
		fCamFOV1 = 42.500000
		vCamPos2 = <<-1156.687622,-2016.962036,13.089501>>
		vCamRot2 = <<2.467418,0.017783,7.845163>>
		fCamFOV2 = 42.500000
		Duration = 6250
		GraphTypePos = GRAPH_TYPE_LINEAR
	ELIF (iInteriorCheck = iInterior3)
		// dont have intro for this one
	ELIF (iInteriorCheck = iInterior5)
		vCamPos1 = <<727.993225,-1074.518066,21.942186>>
		vCamRot1 = <<2.894465,-0.056487,-172.847076>>
		fCamFOV1 = 42.500000
		vCamPos2 = <<731.116089,-1073.823486,21.920891>>
		vCamRot2 = <<4.065939,-0.056487,-172.162308>>
		fCamFOV2 = 42.500000
		Duration = 6250
		GraphTypePos = GRAPH_TYPE_DECEL
	ELIF (iInteriorCheck = iInteriorSuper)
		/*SUPERMOD_TODO*/
		vCoords = <<-205.6834, -1314.8661, 30.5925>>
		fHeading = 162.3779
		vCamPos1 = <<-197.8978, -1319.6437, 31.9083>>
		vCamRot1 = <<-3.9028, 0.0429, 80.5337>>
		fCamFOV1 = 42.5000
		vCamPos2 = <<-200.9182, -1317.0875, 31.5489>>
		vCamRot2 = <<-3.2649, 0.0429, 120.8134>>
		fCamFOV2 = 42.5000
		Duration = 5500
		GraphTypePos = GRAPH_TYPE_DECEL
	
	ELIF (iInteriorCheck = iInteriorMobile)
		MP_PROP_OFFSET_STRUCT camOffset[2]
		SWITCH INT_TO_ENUM(PERSONAL_CAR_MOD_VARIATION, iPersonalCarModVar)
			CASE PERSONAL_CAR_MOD_VARIATION_BIKER_ONE
				vCoords = <<1100.0337, -3152.1204, -38.5186>>
				fHeading = 185.0912
				vCamPos1 = <<1105.0616, -3158.7822, -38.2311>>
				vCamRot1 = <<9.5563, 0.2776, 110.2079>>
				fCamFOV1 = 37.8999
				vCamPos2 = <<1101.3624, -3156.1733, -38.2142>>
				vCamRot2 = <<11.0383, 0.2776, 169.5401>>
				fCamFOV2 = 37.8999
				Duration = 5500
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_BIKER_TWO
				vCoords = <<999.3533, -3163.8474, -39.9075>>
				fHeading = 5.2313
				vCamPos1 = <<1003.1498, -3156.2534, -39.5476>>
				vCamRot1 = <<4.9862, -0.0000, 75.3973>>
				fCamFOV1 = 40.6350
				vCamPos2 = <<1000.4309, -3156.4646, -39.4781>>
				vCamRot2 = <<5.4350, 0.0000, 8.5682>>
				fCamFOV2 = 49.6350
				Duration = 5000
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_ONE
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords = <<-1573.1259, -569.2021, 104.2001>>
				fHeading = 104.9417 
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 32.9017
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 32.9017
				Duration = 5000
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_TWO
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_2_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_2_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords = <<-1387.5962, -482.1745, 77.2001>>
				fHeading = 341.9669
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 32.9017
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 32.9017
				Duration = 5000
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_THREE
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_3_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_3_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords = <<-142.1119, -589.7615, 166.0001>>
				fHeading = 104.0751
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 32.9017
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 32.9017
				Duration = 5000
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_FOUR
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_4_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_4_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_INTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords = <<-73.8994, -813.8752, 284.0001>>
				fHeading = 145.5786
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 32.9017
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 32.9017
				Duration = 5000
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_ARENA_WARS
				IF IS_VEHICLE_MODEL(pedVeh, CERBERUS)
				OR IS_VEHICLE_MODEL(pedVeh, CERBERUS2)
				OR IS_VEHICLE_MODEL(pedVeh, CERBERUS3)
					vCoords = <<184.7756, 5180.5391, -89.5998>>
					fHeading = 267.8019
					vCamPos1 = <<216.7307, 5186.8906, -88.6595>>
					vCamRot1 = <<2.9918, -0.0622, 107.2284>>
					fCamFOV1 = 29.7775
					vCamPos2 = <<215.7855, 5186.5933, -88.3669>>
					vCamRot2 = <<1.2286, -0.0622, 110.5082>>
					fCamFOV2 = 29.7775
					Duration = 6500
					GraphTypePos = GRAPH_TYPE_DECEL
				ELIF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(pedVeh))
					vCoords = <<192.5482, 5180.1963, -89.5998>>
					fHeading = 267.8019
					vCamPos1 = <<216.3670, 5187.5630, -88.3663>>
					vCamRot1 = <<-2.3421, -0.0565, 114.5001>>
					fCamFOV1 = 29.7775
					vCamPos2 = <<215.2157, 5187.3042, -88.3894>>
					vCamRot2 = <<-1.9945, -0.0565, 122.9104>>
					fCamFOV2 = 29.7775
					Duration = 6000
					GraphTypePos = GRAPH_TYPE_DECEL
				ELSE
					vCoords = <<189.2239, 5180.1567, -89.6001>>
					fHeading = 267.8019
					vCamPos1 = <<215.6897, 5187.2837, -88.4805>>
					vCamRot1 = <<-0.5560, -0.0687, 111.9115>>
					fCamFOV1 = 29.7775
					vCamPos2 = <<215.1667, 5186.7563, -88.3407>>
					vCamRot2 = <<-3.8925, -0.0687, 120.4300>>
					fCamFOV2 = 29.7775
					Duration = 6000
					GraphTypePos = GRAPH_TYPE_DECEL
				ENDIF	
			BREAK
			#IF FEATURE_TUNER
			CASE PERSONAL_CAR_MOD_VARIATION_CAR_MEET
				SWITCH iCarmodSlot
					CASE 0
						vCoords = <<-2171.8699, 1143.1667, -25.3715>>
						vCamPos1 = <<-2174.7942, 1144.7314, -22.3553>>
						vCamRot1 = <<-17.8070, -0.0000, -32.5385>>
						fCamFOV1 = 39.0827
						vCamPos2 = <<-2173.5945, 1146.6234, -22.8766>>
						vCamRot2 = <<-14.2780, -0.0000, -29.4331>>
						fCamFOV2 = 39.0827
						Duration = 8000
						fHeading = 164.0600
					BREAK
					CASE 1
						vCoords = <<-2167.2981, 1145.5575, -25.3715>>
						vCamPos1 = <<-2167.9238, 1147.1818, -22.0505>>
						vCamRot1 = <<-24.6613, 0.0051, -34.5499>>
						fCamFOV1 = 39.5776
						vCamPos2 = <<-2166.8921, 1148.6802, -22.9220>>
						vCamRot2 = <<-17.8446, 0.0051, -34.5302>>
						fCamFOV2 = 39.5776
						Duration = 5000
						fHeading = 137.4083
					BREAK
					CASE 2
						vCoords = <<-2162.4561, 1147.9392, -25.3715>>
						vCamPos1 = <<-2162.9075, 1147.9314, -21.3519>>
						vCamRot1 = <<-26.8709, -0.0099, -43.2433>>
						fCamFOV1 = 34.6805
						vCamPos2 = <<-2162.4519, 1147.5433, -22.5754>>
						vCamRot2 = <<-18.9884, -0.0097, -31.8243>>
						fCamFOV2 = 34.6805
						Duration = 5000
						fHeading = 127.9938
					BREAK
					CASE 3
						vCoords = <<-2156.5779, 1146.2635, -25.3715>>
						vCamPos1 = <<-2155.8076, 1149.6051, -21.5993>>
						vCamRot1 = <<-30.0098, -0.0013, -59.2044>>
						fCamFOV1 = 45.000
						vCamPos2 = <<-2154.0488, 1149.1698, -22.8287>>
						vCamRot2 = <<-26.0694, 0.0294, -37.5953>>
						fCamFOV2 = 45.0
						Duration = 5500
						fHeading = 116.3129
					BREAK	
				ENDSWITCH	

				
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_TUNER_AUTO_SHOP
				IF GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()) = PLAYER_ID()
				#IF IS_DEBUG_BUILD
				AND NOT g_bDebugTestRemoteAutoShopData
				#ENDIF
					vCoords = <<-1340.2018, 147.8535, -100.1944>>
					fHeading = 244.1431
					vCamPos1 = <<-1347.2178, 146.0408, -93.0568>>
					vCamRot1 = <<-69.8684, 0.0008, 19.5052>>
					fCamFOV1 = 45.0000
					vCamPos2 = <<-1348.0131, 146.3322, -93.9206>>
					vCamRot2 = <<-36.0648, 0.0001, 23.3343>>
					fCamFOV2 = 45.0000
					Duration = 6500
				ELSE
					vCoords = <<-1357.918, 165.353, -99.421>>
					fHeading = -180.000
					vCamPos1 = <<-1361.9412, 163.2318, -98.8914>>
					vCamRot1 = <<-2.6194, 0.4921, -108.7364>>
					fCamFOV1 = 38.6037
					vCamPos2 = <<-1358.2842, 160.9097, -99.2155>>
					vCamRot2 = <<-2.5618, 0.4921, -122.5809>>
					fCamFOV2 = 38.6037
					Duration = 7500
				ENDIF
				GraphTypePos = GRAPH_TYPE_DECEL
			BREAK
			#ENDIF
			#IF FEATURE_FIXER
			CASE PERSONAL_CAR_MOD_VARIATION_FIXER_HQ
				vCoords = <<-1063.9858, -74.6544, -91.2000>>
				fHeading = 90.0
			BREAK
			#ENDIF
		ENDSWITCH
	ENDIF
	
	// Shop specific data
	IF eShop = CARMOD_SHOP_01_AP
		vCoords = <<-1148.2762, -1994.0066, 12.1803>>
		fHeading = 135.1723
	ELIF eShop = CARMOD_SHOP_05_ID2
		vCoords = <<723.4568, -1089.1243, 21.2235>>
		fHeading = 271.0299
	ELIF eShop = CARMOD_SHOP_06_BT1
		vCoords = <<-351.2211, -136.5706, 38.0096>>
		fHeading = 250.6349
	ELIF eShop = CARMOD_SHOP_SUPERMOD
		vCoords = <<-205.9822, -1315.4341, 29.8904>> //<<-205.6834, -1314.8661, 30.5925>>
		fHeading = 162.3779
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ELIF (iInteriorCheck = iInteriorMobile)
		eBaseShop = CARMOD_SHOP_PERSONALMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vCamPos1)
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, vCamRot1)
	ALIGN_SHOP_COORD(eBaseShop, eShop, vCamPos2)
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, vCamRot2)
ENDPROC

/// PURPOSE: Returns the coords and cam info for outro
PROC GET_CARMOD_SHOP_OUTRO_CUT_DATA(VEHICLE_INDEX pedVeh, 
									SHOP_NAME_ENUM eShop, 
									VECTOR &vCoords, 
									FLOAT &fHeading, 
									VECTOR &vCamPos1, 
									VECTOR &vCamRot1, 
									FLOAT &fCamFOV1, 
									VECTOR &vCamPos2, 
									VECTOR &vCamRot2, 
									FLOAT &fCamFOV2, 
									INT &Duration, 
									VECTOR &vClearArea,
									INT iPersonalCarModVar,
									INT iCarmodSlot)

	IF eShop = CARMOD_SHOP_01_AP
		vCoords = <<-1151.1986, -1996.8804, 12.1803>>
		fHeading = 315.6785
		vCamPos1 = <<-1142.144775,-1982.416382,15.142601>>
		vCamRot1 = <<4.845536,-0.000248,149.426712>>
		fCamFOV1 = 50.0000
		vCamPos2 = <<-1141.118408,-1982.483643,13.143607>>
		vCamRot2 = <<-0.574181,-0.000248,-178.205902>>
		fCamFOV2 = 50.0000
		Duration = 5500
		vClearArea = <<-1140.0028, -1985.6334, 12.1660>>
	ELIF eShop = CARMOD_SHOP_05_ID2
		vCoords = <<732.0324, -1088.1350, 21.1690>>
		fHeading = 93.8784
		vCamPos1 = <<710.606995,-1094.429688,22.697084>>
		vCamRot1 = <<11.841957,0.009556,-57.244869>>
		fCamFOV1 = 50.000000
		vCamPos2 = <<714.992493,-1092.484131,22.185455>>
		vCamRot2 = <<2.543958,0.009556,-39.357609>>
		fCamFOV2 = 50.000000
		Duration = 5500
		vClearArea = <<719.4805, -1088.4611, 21.6632>>
	ELIF eShop = CARMOD_SHOP_06_BT1
		vCoords = <<-348.0383, -137.0883, 38.0096>>
		fHeading = 71.4219
		vCamPos1 = <<-368.274811,-133.080566,39.875538>>
		vCamRot1 = <<10.126453,0.000000,-92.063141>>
		fCamFOV1 = 50.000000
		vCamPos2 = <<-367.209930,-133.499847,38.513096>>
		vCamRot2 = <<4.783289,-0.000000,-65.436745>>
		fCamFOV2 = 50.000000
		Duration = 6250
		vClearArea = <<-362.0122, -132.4046, 37.6800>>
		vClearArea = <<719.4805, -1088.4611, 21.6632>>
	ELIF eShop = CARMOD_SHOP_SUPERMOD
		vCoords = <<-205.9946, -1316.0386, 29.8904>> //<<-205.6086, -1318.0973, 29.8904>> //<<-205.5987, -1314.8097, 29.9319>> //<<-205.6416, -1309.8585, 31.0239>>
		fHeading = 358.7647
		vCamPos1 = <<-203.0339, -1297.7800, 32.6463>>
		vCamRot1 = <<0.1514, -0.0000, 165.9550>>
		fCamFOV1 = 50.0000
		vCamPos2 = <<-199.7189, -1297.5970, 32.0562>> //<<-199.7380, -1296.7200, 32.0625>>
		vCamRot2 = <<-8.6217, -0.0000, 171.0568>>
		fCamFOV2 = 50.0000
		Duration = 5500
		vClearArea = <<-197.2410, -1301.4960, 30.2960>>
	
	ELIF eShop = CARMOD_SHOP_PERSONALMOD
		MP_PROP_OFFSET_STRUCT camOffset[2]
		SWITCH INT_TO_ENUM(PERSONAL_CAR_MOD_VARIATION, iPersonalCarModVar)
			CASE PERSONAL_CAR_MOD_VARIATION_BIKER_ONE
				vCoords = <<1100.3308, -3163.6970, -38.4795>>
				fHeading = 353.7347
				vCamPos1 = <<1101.5173, -3158.3665, -38.1072>>
				vCamRot1 = <<7.9170, -0.0000, 170.3602>>
				fCamFOV1 = 45.0409
				vCamPos2 = <<1103.2483, -3157.9470, -38.2130>>
				vCamRot2 = <<8.2426, -0.0000, 124.5706>>
				fCamFOV2 = 49.0409
				Duration = 3800
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_BIKER_TWO
				vCoords = <<1000.1379, -3146.9158, -39.8879>>
				fHeading = 174.5641
				vCamPos1 = <<1001.5336, -3156.3994, -39.2787>>
				vCamRot1 = <<6.0641, -0.0000, 19.1658>>
				fCamFOV1 = 40.2156
				vCamPos2 = <<1003.3333, -3156.7019, -39.4844>>
				vCamRot2 = <<6.9524, 0.2023, 52.4298>>
				fCamFOV2 = 50.0
				Duration = 5000
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_ONE
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords =  <<-1581.0408, -579.3764, 104.2001>>
				fHeading = 356.4082 
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 21.5238
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 21.5238
				Duration = 5000
				vClearArea = <<-1581.0408, -579.3764, 104.2001>>
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_TWO
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_2_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_2_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords =  <<-1392.9937, -469.9412, 77.2001>>
				fHeading = 245.8999
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 21.5238
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 21.5238
				Duration = 5000
				vClearArea = <<-1392.9937, -469.9412, 77.2001>>
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_THREE
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_3_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_3_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords =  <<-149.7302, -599.8518, 166.0001>>
				fHeading = 5.6653
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 21.5238
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 21.5238
				Duration = 5000
				vClearArea = <<-149.7302, -599.8518, 166.0001>>
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_FOUR
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_4_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_1, camOffset[0],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(PROPERTY_OFFICE_4_GARAGE_LVL1, MP_PROP_ELEM_OFFICE_GARAGE_OUTRO_CAM_2, camOffset[1],PROPERTY_OFFICE_3_GARAGE_LVL1, TRUE)
				vCoords =  <<-74.2015, -825.1844, 284.0001>>
				fHeading = 20.8981
				vCamPos1 = camOffset[0].vLoc
				vCamRot1 = camOffset[0].vRot
				fCamFOV1 = 21.5238
				vCamPos2 = camOffset[1].vLoc
				vCamRot2 = camOffset[1].vRot
				fCamFOV2 = 21.5238
				Duration = 5000
				vClearArea = <<-74.2015, -825.1844, 284.0001>>
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_ARENA_WARS
				IF IS_VEHICLE_MODEL(pedVeh, CERBERUS)
				OR IS_VEHICLE_MODEL(pedVeh, CERBERUS2)
				OR IS_VEHICLE_MODEL(pedVeh, CERBERUS3)
					vCoords = <<203.2751, 5180.1831, -89.9973>>
					fHeading = 90.0000
					vCamPos1 = <<209.5832, 5191.9429, -83.9091>>
					vCamRot1 = <<-17.6307, -0.0000, 164.1196>>
					fCamFOV1 = 52.8822
					vCamPos2 = <<200.7296, 5192.8672, -83.9091>>
					vCamRot2 = <<-16.9160, -0.0000, -167.0705>>
					fCamFOV2 = 37.4463
					Duration = 6000
					vClearArea = <<2561.0425, -3795.0356, 150.5998>>
				ELSE
					vCoords = <<203.2751, 5180.1831, -89.9973>>
					fHeading = 90.0000
					vCamPos1 = <<209.5832, 5191.9429, -83.9091>>
					vCamRot1 = <<-17.6307, -0.0000, 164.1196>>
					fCamFOV1 = 52.8822
					vCamPos2 = <<200.7296, 5192.8672, -83.9091>>
					vCamRot2 = <<-16.9160, -0.0000, -167.0705>>
					fCamFOV2 = 37.4463
					Duration = 6000
					vClearArea = <<2561.0425, -3795.0356, 150.5998>>
				ENDIF
			BREAK
			#IF FEATURE_TUNER
			CASE PERSONAL_CAR_MOD_VARIATION_CAR_MEET
				SWITCH iCarmodSlot
					CASE 0		
						vCoords = <<-2169.6323, 1151.8933, -25.3719>>
						vCamPos1 = <<-2174.2153, 1155.0592, -24.6859>>
						vCamRot1 = <<0.5370, 0.0016, -130.3960>>
						fCamFOV1 = 32.5663
						vCamPos2 = <<-2172.7349, 1152.9116, -23.6989>>
						vCamRot2 = <<0.5370, 0.0016, -153.2426>>
						fCamFOV2 = 32.5663
						Duration = 6000
					BREAK	
					CASE 1		
						vCoords = <<-2164.5547, 1152.3569, -25.3717>>
						vCamPos1 = <<-2172.7957, 1150.1729, -24.7317>>
						vCamRot1 = <<0.7614, 0.0047, -94.2815>>
						fCamFOV1 = 27.6286
						vCamPos2 = <<-2172.5754, 1151.7056, -23.9484>>
						vCamRot2 = <<0.7614, 0.0047, -158.0785>>
						fCamFOV2 = 27.6286
						Duration = 6000
					BREAK
					CASE 2		
						vCoords = <<-2158.9351, 1152.5377, -25.3717>>	
						vCamPos1 = <<-2160.6411, 1157.9922, -24.7615>>
						vCamRot1 = <<0.1358, 0.0062, -162.5294>>
						fCamFOV1 = 29.5483
						vCamPos2 = <<-2161.9277, 1154.9069, -23.9345>>
						vCamRot2 = <<0.1358, 0.0062, -179.6634>>
						fCamFOV2 = 29.5483
						Duration = 6000
					BREAK
					CASE 3	
						vCoords = <<-2150.9626, 1152.5878, -25.3717>>	
						vCamPos1 = <<-2153.1768, 1158.1604, -24.8078>>
						vCamRot1 = <<0.5333, 0.0019, -156.2617>>
						fCamFOV1 = 31.9256
						vCamPos2 = <<-2152.3140, 1155.6364, -23.8069>>
						vCamRot2 = <<0.5333, 0.0019, 179.0399>>
						fCamFOV2 = 31.9256
						Duration = 6000
					BREAK
				ENDSWITCH
				
				fHeading = 179.500
				
				vClearArea = <<2561.0425, -3795.0356, 150.5998>>
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_TUNER_AUTO_SHOP
				IF GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()) = PLAYER_ID()
				#IF IS_DEBUG_BUILD
				AND NOT g_bDebugTestRemoteAutoShopData
				#ENDIF
					vCoords = <<-1351.6940, 154.8091, -100.1944>>
					fHeading = 180.0
					vCamPos1 = <<-1346.6462, 148.0642, -99.4512>>
					vCamRot1 = <<-1.3528, -0.0003, 38.8266>>
					fCamFOV1 = 43.2962
					vCamPos2 = <<-1348.6589, 147.3986, -99.4438>>
					vCamRot2 = <<1.5253, -0.0003, 2.0172>>
					fCamFOV2 = 43.2962
					Duration = 5000
					vClearArea = <<2561.0425, -3795.0356, 150.5998>>
				ELSE
					vCoords = <<-1351.6256, 154.1439, -100.1944>>
					fHeading = 358.9327
					vCamPos1 = <<-1353.6694, 161.9915, -99.3788>>
					vCamRot1 = <<-0.8291, -0.0389, -165.0284>>
					fCamFOV1 = 40.1593
					vCamPos2 = <<-1349.6042, 162.0746, -99.3770>>
					vCamRot2 = <<-0.8291, -0.0389, 137.8593>>
					fCamFOV2 = 40.1593
					Duration = 7000
					vClearArea = <<2561.0425, -3795.0356, 150.5998>>
				ENDIF
			BREAK
			#ENDIF
			#IF FEATURE_FIXER
			CASE PERSONAL_CAR_MOD_VARIATION_FIXER_HQ
				vCoords = <<-1063.9858, -74.6544, -91.2000>>
				fHeading = 90.0
			BREAK
			#ENDIF
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE: Returns the coords for player warp
FUNC BOOL GET_CARMOD_WARP_POSITION(SHOP_NAME_ENUM eShop, INT iSlot, VECTOR &vCoords, FLOAT &fHeading, FLOAT &fWeighting  ,INT iPersonalCarModVar = 0  )
	
	vCoords = <<0,0,0>>
	fHeading = 0.0
	fWeighting = 1.0
	
	/*IF (eShop = CARMOD_SHOP_01_AP)// Car Mod - AMB1 (v_carmod)
		IF iSlot = 0 	vCoords = <<-1136.7974, -1993.2719, 12.1678>>	fHeading = 315.2252
		ELIF iSlot = 1 	vCoords = <<-1134.4193, -1995.4301, 12.1690>>	fHeading = 314.7090
		ELIF iSlot = 2 	vCoords = <<-1133.0610, -1999.7975, 12.1728>>	fHeading = 314.8764
		ELIF iSlot = 3 	vCoords = <<-1129.3790, -2003.3844, 12.1761>>	fHeading = 316.3972
		ELIF iSlot = 4 	vCoords = <<-1122.9049, -2006.5736, 12.1793>>	fHeading = 46.8969 
		ELIF iSlot = 5 	vCoords = <<-1135.1396, -1976.5625, 12.1621>>	fHeading = 269.8207
		ELIF iSlot = 6 	vCoords = <<-1128.3204, -1976.4552, 12.1623>>	fHeading = 271.2042
		ELIF iSlot = 7 	vCoords = <<-1148.5983, -1974.8552, 12.1610>>	fHeading = 233.0304
		ELIF iSlot = 8 	vCoords = <<-1151.6425, -1977.3403, 12.1610>>	fHeading = 227.3544
		ELIF iSlot = 9 	vCoords = <<-1153.8512, -1983.8148, 12.1606>>	fHeading = 277.7804
		ENDIF
	ELIF (eShop = CARMOD_SHOP_05_ID2) // Car Mod - AMB2 (v_lockup)
		IF iSlot = 0 	vCoords = <<715.9170, -1056.8544, 21.0320>>	fHeading = 120.1917
		ELIF iSlot = 1 	vCoords = <<715.5419, -1060.7522, 21.1036>>	fHeading = 125.1627
		ELIF iSlot = 2 	vCoords = <<716.5426, -1064.2949, 21.2268>>	fHeading = 129.1433
		ELIF iSlot = 3 	vCoords = <<716.8484, -1067.8566, 21.2530>>	fHeading = 125.7484
		ELIF iSlot = 4 	vCoords = <<715.2232, -1073.4856, 21.2736>>	fHeading = 125.6881
		ELIF iSlot = 5 	vCoords = <<717.0334, -1076.5818, 21.2530>>	fHeading = 125.7507
		ELIF iSlot = 6 	vCoords = <<718.9215, -1080.6038, 21.2514>>	fHeading = 125.6956
		ELIF iSlot = 7 	vCoords = <<703.0445, -1079.1876, 21.4000>>	fHeading = 232.5002
		ELIF iSlot = 8 	vCoords = <<706.7367, -1072.9039, 21.4054>>	fHeading = 234.1397
		ELIF iSlot = 9 	vCoords = <<703.9542, -1066.5944, 21.4602>>	fHeading = 231.5333
		ELIF iSlot = 10	vCoords = <<705.4819, -1063.4484, 21.4325>>	fHeading = 234.5070
		ENDIF
	ELIF (eShop = CARMOD_SHOP_06_BT1) // Car Mod - AMB3 (v_carmod)
		IF iSlot = 0 	vCoords = <<-376.2975, -146.2927, 37.6785>>	fHeading = 298.4096
		ELIF iSlot = 1 	vCoords = <<-378.0852, -143.2059, 37.6785>>	fHeading = 298.3375
		ELIF iSlot = 2 	vCoords = <<-379.6474, -140.3793, 37.6785>>	fHeading = 300.4394
		ELIF iSlot = 3 	vCoords = <<-381.3388, -137.2407, 37.6785>>	fHeading = 296.2632
		ELIF iSlot = 4 	vCoords = <<-383.0698, -133.7630, 37.6787>>	fHeading = 298.7080
		ELIF iSlot = 5 	vCoords = <<-384.8618, -130.8699, 37.6804>>	fHeading = 299.6163
		ELIF iSlot = 6 	vCoords = <<-391.6121, -118.5516, 37.5406>>	fHeading = 296.8287
		ELIF iSlot = 7 	vCoords = <<-389.6161, -121.2788, 37.6646>>	fHeading = 297.0251
		ELIF iSlot = 8 	vCoords = <<-387.7787, -124.5911, 37.6810>>	fHeading = 298.0492
		ENDIF
	ELIF (eShop = CARMOD_SHOP_07_CS1) // Car Mod - AMB4 (v_carmod3)
		IF iSlot = 0 	vCoords = <<116.9116, 6599.6670, 31.0076>>	fHeading = 274.0656
		ELIF iSlot = 1 	vCoords = <<121.9585, 6594.8198, 31.0194>>	fHeading = 269.6931
		ELIF iSlot = 2 	vCoords = <<126.3070, 6589.6558, 30.9411>>	fHeading = 267.4287
		ELIF iSlot = 3 	vCoords = <<132.0808, 6585.1323, 30.9625>>	fHeading = 270.7679
		ELIF iSlot = 4 	vCoords = <<136.6882, 6580.0737, 31.0158>>	fHeading = 269.7758
		ELIF iSlot = 5 	vCoords = <<155.2655, 6591.7090, 30.8450>>	fHeading = 355.2730
		ELIF iSlot = 6 	vCoords = <<151.1561, 6597.2197, 30.8449>>	fHeading = 357.7174
		ELIF iSlot = 7 	vCoords = <<151.0934, 6606.4858, 30.8753>>	fHeading = 359.8299
		ELIF iSlot = 8 	vCoords = <<155.8102, 6603.2271, 30.8612>>	fHeading = 359.6158
		ELIF iSlot = 9 	vCoords = <<146.6074, 6614.6289, 30.8154>>	fHeading = 177.2282
		ELIF iSlot = 10 vCoords = <<145.8584, 6602.5356, 30.8536>>	fHeading = 176.6134
		ENDIF
	ELIF (eShop = CARMOD_SHOP_08_CS6) // Car Mod - AMB5 (v_carmod3)
		IF iSlot = 0 	vCoords = <<1162.5773, 2677.7568, 37.0406>>	fHeading = 269.2866
		ELIF iSlot = 1 	vCoords = <<1171.3484, 2677.4395, 36.9594>>	fHeading = 269.2448
		ELIF iSlot = 2 	vCoords = <<1184.1366, 2677.0405, 36.8499>>	fHeading = 268.5350
		ELIF iSlot = 3 	vCoords = <<1201.1283, 2664.6995, 36.8099>>	fHeading = 134.4073
		ELIF iSlot = 4 	vCoords = <<1203.3816, 2661.9138, 36.8100>>	fHeading = 132.0064
		ELIF iSlot = 5 	vCoords = <<1205.5745, 2659.4585, 36.8137>>	fHeading = 134.5788
		ELIF iSlot = 6 	vCoords = <<1208.1188, 2656.8833, 36.8232>>	fHeading = 135.9477
		ELIF iSlot = 7 	vCoords = <<1210.6748, 2654.4478, 36.8179>>	fHeading = 138.4631
		ELIF iSlot = 8 	vCoords = <<1215.3839, 2664.7913, 36.8094>>	fHeading = 12.5143
		ENDIF
	ENDIF*/
	
	IF (eShop = CARMOD_SHOP_01_AP)// Car Mod - AMB1 (v_carmod)
		IF iSlot = 0 	vCoords = <<-1136.4912, -1991.1274, 12.1674>>	fHeading = 312.0856 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<-1133.2526, -1993.8540, 12.1687>>	fHeading = 314.3890	fWeighting = 1.0
		ELIF iSlot = 2 	vCoords = <<-1130.8060, -1997.8751, 12.1713>>	fHeading = 317.9671 fWeighting = 0.6
		ELIF iSlot = 3 	vCoords = <<-1127.3292, -2001.5482, 12.1741>>	fHeading = 315.1484 fWeighting = 0.5
		ELIF iSlot = 4 	vCoords = <<-1123.2858, -2006.2626, 12.1791>>	fHeading = 236.5043 fWeighting = 0.4
		ELIF iSlot = 5 	vCoords = <<-1115.4692, -2012.3073, 12.1802>>	fHeading = 264.1943 fWeighting = 0.2
		ELIF iSlot = 6 	vCoords = <<-1110.5621, -2015.4457, 12.1999>>	fHeading = 302.6618 fWeighting = 0.1
		ELIF iSlot = 7 	vCoords = <<-1121.6904, -2000.2299, 12.1718>>	fHeading = 238.2144 fWeighting = 0.1
		ELIF iSlot = 8 	vCoords = <<-1111.5887, -2006.4545, 12.1692>>	fHeading = 238.3360 fWeighting = 0.3
		ELIF iSlot = 9 	vCoords = <<-1138.6481, -1979.8260, 12.1634>>	fHeading = 278.7886 fWeighting = 0.9
		ELIF iSlot = 10 vCoords = <<-1124.3206, -1978.1239, 12.1856>>	fHeading = 276.2760 fWeighting = 0.4
		ELIF iSlot = 11 vCoords = <<-1110.2635, -1975.9711, 12.1588>>	fHeading = 279.4681 fWeighting = 0.5
		ELIF iSlot = 12	vCoords = <<-1128.2931, -1984.8282, 12.1659>>	fHeading = 295.1603 fWeighting = 0.8
		ELIF iSlot = 13	vCoords = <<-1126.0660, -1991.3458, 12.1683>>	fHeading = 227.6551 fWeighting = 0.7
		ENDIF
	ELIF (eShop = CARMOD_SHOP_05_ID2) // Car Mod - AMB2 (v_lockup)
		IF iSlot = 0 	vCoords = <<717.5009, -1082.0131, 21.2916>>	fHeading = 3.6761 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<716.7202, -1069.2635, 21.2546>>	fHeading = 3.5291 fWeighting = 1.0 
		ELIF iSlot = 2 	vCoords = <<717.0090, -1058.2042, 21.0152>>	fHeading = 355.8705 fWeighting = 0.9
		ELIF iSlot = 3 	vCoords = <<712.7385, -1083.5344, 21.3647>>	fHeading = 359.9393 fWeighting = 0.9
		ELIF iSlot = 4 	vCoords = <<712.7766, -1072.9674, 21.3070>>	fHeading = 359.9453 fWeighting = 0.8
		ELIF iSlot = 5 	vCoords = <<712.8783, -1061.0941, 21.1883>>	fHeading = 357.2739	fWeighting = 0.8
		ELIF iSlot = 6 	vCoords = <<708.5093, -1081.2853, 21.3978>>	fHeading = 358.5361	fWeighting = 0.7
		ELIF iSlot = 7 	vCoords = <<708.9708, -1068.3582, 21.3519>>	fHeading = 357.9787	fWeighting = 0.7
		ELIF iSlot = 8 	vCoords = <<710.7449, -1053.6115, 21.2011>>	fHeading = 339.2883	fWeighting = 0.6
		ELIF iSlot = 9 	vCoords = <<704.6691, -1079.5365, 21.3804>>	fHeading = 359.4009	fWeighting = 0.5
		ELIF iSlot = 10	vCoords = <<704.4818, -1065.9111, 21.4447>>	fHeading = 0.9557  	fWeighting = 0.4
		ELIF iSlot = 11	vCoords = <<716.4031, -1044.6154, 20.9157>>	fHeading = 280.6607	fWeighting = 0.3
		ELIF iSlot = 12	vCoords = <<725.7982, -1044.2264, 21.0460>>	fHeading = 271.6107	fWeighting = 0.2
		ELIF iSlot = 13	vCoords = <<727.0708, -1047.9510, 21.2648>>	fHeading = 270.6534	fWeighting = 0.1
		ENDIF
	ELIF (eShop = CARMOD_SHOP_06_BT1) // Car Mod - AMB3 (v_carmod)
		IF iSlot = 0 	vCoords = <<-365.9221, -125.5196, 37.6785>>	fHeading = 65.4762 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<-362.3411, -122.0465, 37.6788>>	fHeading = 68.8025 fWeighting = 1.0
		ELIF iSlot = 2 	vCoords = <<-363.4988, -117.1944, 37.6792>>	fHeading = 78.9594 fWeighting = 0.9
		ELIF iSlot = 3 	vCoords = <<-372.3338, -115.2380, 37.6796>>	fHeading = 74.7182 fWeighting = 0.8
		ELIF iSlot = 4 	vCoords = <<-371.1733, -121.0303, 37.6797>>	fHeading = 62.1230 fWeighting = 0.5
		ELIF iSlot = 5 	vCoords = <<-369.2153, -127.0247, 37.6784>>	fHeading = 61.9032 fWeighting = 0.7
		ELIF iSlot = 6 	vCoords = <<-371.8094, -130.2364, 37.6798>>	fHeading = 52.1100 fWeighting = 0.6
		ELIF iSlot = 7 	vCoords = <<-378.4785, -130.0621, 37.6796>>	fHeading = 36.4690 fWeighting = 0.5
		ELIF iSlot = 8 	vCoords = <<-387.4156, -118.9469, 37.6829>>	fHeading = 38.3679 fWeighting = 0.4
		ELIF iSlot = 9 	vCoords = <<-382.7090, -112.8489, 37.6985>>	fHeading = 65.0749 fWeighting = 0.3
		ELIF iSlot = 10 vCoords = <<-397.1963, -107.1517, 37.6834>>	fHeading = 33.3150 fWeighting = 0.2
		ELIF iSlot = 11 vCoords = <<-404.3896, -96.5394, 39.0491>>	fHeading = 34.1156 fWeighting = 0.1
		ENDIF
	ELIF (eShop = CARMOD_SHOP_07_CS1) // Car Mod - AMB4 (v_carmod3)
		IF iSlot = 0 	vCoords = <<120.3574, 6599.5732, 31.0156>>	fHeading = 269.5703 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<123.4549, 6594.4399, 30.9958>>	fHeading = 269.5584 fWeighting = 0.9
		ELIF iSlot = 2 	vCoords = <<126.7120, 6589.7983, 30.9386>>	fHeading = 269.5731	fWeighting = 0.5
		ELIF iSlot = 3 	vCoords = <<133.9324, 6585.5552, 30.9551>>	fHeading = 269.4128 fWeighting = 0.5
		ELIF iSlot = 4 	vCoords = <<136.8265, 6580.1201, 31.0130>>	fHeading = 269.4126 fWeighting = 0.2
		ELIF iSlot = 5 	vCoords = <<141.8716, 6575.2144, 30.9522>>	fHeading = 270.5616 fWeighting = 0.2
		ELIF iSlot = 6 	vCoords = <<140.8046, 6606.3120, 30.8449>>	fHeading = 178.8423 fWeighting = 0.7
		ELIF iSlot = 7 	vCoords = <<145.8316, 6601.0991, 30.8500>>	fHeading = 180.9941 fWeighting = 0.6
		ELIF iSlot = 8 	vCoords = <<150.5503, 6596.5327, 30.8449>>	fHeading = 177.9041 fWeighting = 0.4
		ELIF iSlot = 9 	vCoords = <<155.6983, 6591.2925, 30.8449>>	fHeading = 177.9025 fWeighting = 0.2
		ELIF iSlot = 10	vCoords = <<159.1213, 6580.5444, 30.8410>>	fHeading = 208.1021 fWeighting = 0.3
		ELIF iSlot = 11	vCoords = <<153.3835, 6581.3574, 30.8430>>	fHeading = 208.9007 fWeighting = 0.3
		ELIF iSlot = 12	vCoords = <<160.6086, 6567.4976, 30.8061>>	fHeading = 210.3887 fWeighting = 0.1
		ELIF iSlot = 13	vCoords = <<166.7977, 6567.1348, 30.7544>>	fHeading = 210.3833 fWeighting = 0.2
		ENDIF
	ELIF (eShop = CARMOD_SHOP_08_CS6) // Car Mod - AMB5 (v_carmod3)
		IF iSlot = 0 	vCoords = <<1182.4977, 2653.5815, 36.8099>>	fHeading = 304.7889 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<1190.6785, 2661.1431, 36.8165>>	fHeading = 321.4830 fWeighting = 1.0
		ELIF iSlot = 2 	vCoords = <<1196.7334, 2669.6602, 36.7883>>	fHeading = 345.6812 fWeighting = 0.8
		ELIF iSlot = 3 	vCoords = <<1200.4663, 2666.4939, 36.8099>>	fHeading = 347.5349 fWeighting = 0.6
		ELIF iSlot = 4 	vCoords = <<1205.5857, 2667.1362, 36.8099>>	fHeading = 352.7466 fWeighting = 0.5
		ELIF iSlot = 5 	vCoords = <<1210.6979, 2666.5916, 36.8099>>	fHeading = 7.6557   fWeighting = 0.1
		ELIF iSlot = 6 	vCoords = <<1193.5735, 2688.7141, 36.7457>>	fHeading = 87.5179  fWeighting = 0.2
		ELIF iSlot = 7 	vCoords = <<1181.1273, 2689.2600, 36.8532>>	fHeading = 87.4959  fWeighting = 0.4
		ELIF iSlot = 8 	vCoords = <<1162.4586, 2689.2036, 37.1039>>	fHeading = 87.5173  fWeighting = 0.3
		ELIF iSlot = 9 	vCoords = <<1162.6628, 2677.4583, 37.0780>>	fHeading = 268.6483 fWeighting = 0.1
		ELIF iSlot = 10	vCoords = <<1171.8584, 2677.1252, 36.9950>>	fHeading = 267.8824 fWeighting = 0.2
		ELIF iSlot = 11	vCoords = <<1224.3582, 2677.2256, 36.6702>>	fHeading = 272.1325 fWeighting = 0.1
		ENDIF
	ELIF (eShop = CARMOD_SHOP_SUPERMOD) // Car Mod - AMB6 (lr_supermod_int)
		IF iSlot = 0 	vCoords = <<-199.5331, -1301.6898, 30.2960>> fHeading = 263.7030 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<-205.9477, -1303.8677, 30.2575>> fHeading = 1.5267   fWeighting = 0.9
		ELIF iSlot = 2 	vCoords = <<-200.7847, -1298.6356, 30.2960>> fHeading = 89.6847  fWeighting = 0.8
		ELIF iSlot = 3 	vCoords = <<-193.9668, -1305.6500, 30.3643>> fHeading = 86.5508  fWeighting = 0.7
		ELIF iSlot = 4 	vCoords = <<-213.3890, -1305.8219, 30.3533>> fHeading = 88.0277  fWeighting = 0.6
		ELIF iSlot = 5 	vCoords = <<-210.4487, -1301.4200, 30.2960>> fHeading = 50.2894  fWeighting = 0.5
		ELIF iSlot = 6 	vCoords = <<-193.1976, -1302.8916, 30.2960>> fHeading = 310.5602 fWeighting = 0.4
		ELIF iSlot = 7 	vCoords = <<-198.8820, -1304.7721, 30.3250>> fHeading = 268.7130 fWeighting = 0.3
		ELIF iSlot = 8 	vCoords = <<-230.2501, -1305.5962, 30.3533>> fHeading = 89.1508  fWeighting = 0.2
		ELIF iSlot = 9 	vCoords = <<-186.4736, -1305.7463, 30.3495>> fHeading = 270.2632 fWeighting = 0.2
		ELIF iSlot = 10 vCoords = <<-236.5289, -1302.0707, 30.2960>> fHeading = 270.2677 fWeighting = 0.1
		ELIF iSlot = 11	vCoords = <<-181.8344, -1299.3108, 30.2960>> fHeading = 88.0286  fWeighting = 0.1
		ENDIF
	ELIF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(eShop, iPersonalCarModVar)
		IF iSlot = 0 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352   fWeighting = 0.9
		ELIF iSlot = 2 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352  fWeighting = 0.8
		ELIF iSlot = 3 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352  fWeighting = 0.7
		ELIF iSlot = 4 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352  fWeighting = 0.6
		ELIF iSlot = 5 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352  fWeighting = 0.5
		ELIF iSlot = 6 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352 fWeighting = 0.4
		ELIF iSlot = 7 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352 fWeighting = 0.3
		ELIF iSlot = 8 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352  fWeighting = 0.2
		ELIF iSlot = 9 	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352 fWeighting = 0.2
		ELIF iSlot = 10 vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352 fWeighting = 0.1
		ELIF iSlot = 11	vCoords = <<-795.6738, 306.1536, 84.7005>> fHeading = 175.3352  fWeighting = 0.1
		ENDIF
	ELIF IS_THIS_PERSONAL_CAR_MOD_IN_OFFICE(eShop, iPersonalCarModVar)
		SWITCH INT_TO_ENUM(PERSONAL_CAR_MOD_VARIATION, iPersonalCarModVar)
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_ONE
				IF iSlot = 0 	vCoords = <<-1574.9066, -569.6819, 104.2001>> fHeading = 104.9486 fWeighting = 1.0
				ELIF iSlot = 1 	vCoords = <<-1573.9828, -570.0646, 104.2001>> fHeading = 104.9486   fWeighting = 0.9
				ELIF iSlot = 2 	vCoords = <<-1575.2893, -570.6058, 104.2001>> fHeading = 104.9486  fWeighting = 0.8
				ELIF iSlot = 3 	vCoords = <<-1575.8304, -569.2993, 104.2001>> fHeading = 104.9486  fWeighting = 0.7
				ELIF iSlot = 4 	vCoords = <<-1574.5239, -568.7581, 104.2001>> fHeading = 104.9486  fWeighting = 0.6
				ELIF iSlot = 5 	vCoords = <<-1574.3655, -570.9885, 104.2001>> fHeading = 104.9486  fWeighting = 0.5
				ELIF iSlot = 6 	vCoords = <<-1576.2131, -570.2231, 104.2001>> fHeading = 104.9486 fWeighting = 0.4
				ELIF iSlot = 7 	vCoords = <<-1575.4478, -568.3754, 104.2001>> fHeading = 104.9486 fWeighting = 0.3
				ELIF iSlot = 8 	vCoords = <<-1573.6001, -569.1407, 104.2001>> fHeading = 104.9486  fWeighting = 0.2
				ELIF iSlot = 9 	vCoords = <<-1573.0588, -570.4473, 104.2001>> fHeading = 104.9486 fWeighting = 0.2
				ELIF iSlot = 10 vCoords = <<-1575.6720, -571.5297, 104.2001>> fHeading = 104.9486 fWeighting = 0.1
				ELIF iSlot = 11	vCoords = <<-1576.7544, -568.9166, 104.2001>> fHeading = 104.9486  fWeighting = 0.1
				ENDIF
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_TWO
				IF iSlot = 0 	vCoords = <<-1387.5612, -481.6370, 77.2001>> fHeading = 353.0411 fWeighting = 1.0
				ELIF iSlot = 1 	vCoords = <<-1386.6373, -482.0197, 77.2001>> fHeading = 353.0411   fWeighting = 0.9
				ELIF iSlot = 2 	vCoords = <<-1387.9438, -482.5609, 77.2001>> fHeading = 353.0411  fWeighting = 0.8
				ELIF iSlot = 3 	vCoords = <<-1388.4850, -481.2543, 77.2001>> fHeading = 353.0411  fWeighting = 0.7
				ELIF iSlot = 4 	vCoords = <<-1387.1785, -480.7131, 77.2001>> fHeading = 353.0411  fWeighting = 0.6
				ELIF iSlot = 5 	vCoords = <<-1387.0200, -482.9435, 77.2001>> fHeading = 353.0411  fWeighting = 0.5
				ELIF iSlot = 6 	vCoords = <<-1388.8677, -482.1782, 77.2001>> fHeading = 353.0411 fWeighting = 0.4
				ELIF iSlot = 7 	vCoords = <<-1388.1023, -480.3304, 77.2001>> fHeading = 353.0411 fWeighting = 0.3
				ELIF iSlot = 8 	vCoords = <<-1386.2546, -481.0958, 77.2001>> fHeading = 353.0411  fWeighting = 0.2
				ELIF iSlot = 9 	vCoords = <<-1385.7134, -482.4024, 77.2001>> fHeading = 353.0411 fWeighting = 0.2
				ELIF iSlot = 10 vCoords = <<-1388.3265, -483.4847, 77.2001>> fHeading = 353.0411 fWeighting = 0.1
				ELIF iSlot = 11	vCoords = <<-1389.4089, -480.8716, 77.2001>> fHeading = 353.0411  fWeighting = 0.1
				ENDIF
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_THREE
				IF iSlot = 0 	vCoords = <<-142.5312, -590.6586, 166.0001>> fHeading = 110.9811 fWeighting = 1.0
				ELIF iSlot = 1 	vCoords = <<-141.6073, -591.0413, 166.0001>> fHeading = 110.9811   fWeighting = 0.9
				ELIF iSlot = 2 	vCoords = <<-142.9139, -591.5825, 166.0001>> fHeading = 110.9811  fWeighting = 0.8
				ELIF iSlot = 3 	vCoords = <<-143.4551, -590.2759, 166.0001>> fHeading = 110.9811  fWeighting = 0.7
				ELIF iSlot = 4 	vCoords = <<-142.1485, -589.7347, 166.0001>> fHeading = 110.9811  fWeighting = 0.6
				ELIF iSlot = 5 	vCoords = <<-141.9900, -591.9651, 166.0001>> fHeading = 110.9811  fWeighting = 0.5
				ELIF iSlot = 6 	vCoords = <<-143.8378, -591.1998, 166.0001>> fHeading = 110.9811 fWeighting = 0.4
				ELIF iSlot = 7 	vCoords = <<-143.0724, -589.3520, 166.0001>> fHeading = 110.9811 fWeighting = 0.3
				ELIF iSlot = 8 	vCoords = <<-141.2247, -590.1174, 166.0001>> fHeading = 110.9811  fWeighting = 0.2
				ELIF iSlot = 9 	vCoords = <<-141.2247, -590.1174, 166.0001>> fHeading = 110.9811 fWeighting = 0.2
				ELIF iSlot = 10 vCoords = <<-140.6835, -591.4240, 166.0001>> fHeading = 110.9811 fWeighting = 0.1
				ELIF iSlot = 11	vCoords = <<-143.2966, -592.5063, 166.0001>> fHeading = 110.9811  fWeighting = 0.1
				ENDIF
			BREAK
			CASE PERSONAL_CAR_MOD_VARIATION_OFFICE_FOUR
				IF iSlot = 0 	vCoords = <<-74.8035, -814.8599, 284.0001>> fHeading = 146.2765 fWeighting = 1.0
				ELIF iSlot = 1 	vCoords = <<-73.8796, -815.2426, 284.0001>> fHeading = 146.2765   fWeighting = 0.9
				ELIF iSlot = 2 	vCoords = <<-75.1862, -815.7838, 284.0001>> fHeading = 146.2765  fWeighting = 0.8
				ELIF iSlot = 3 	vCoords = <<-75.7274, -814.4772, 284.0001>> fHeading = 146.2765  fWeighting = 0.7
				ELIF iSlot = 4 	vCoords = <<-74.4208, -813.9360, 284.0001>>fHeading = 146.2765  fWeighting = 0.6
				ELIF iSlot = 5 	vCoords = <<-74.2623, -816.1664, 284.0001>> fHeading = 146.2765  fWeighting = 0.5
				ELIF iSlot = 6 	vCoords = <<-76.1100, -815.4011, 284.0001>> fHeading = 146.2765 fWeighting = 0.4
				ELIF iSlot = 7 	vCoords = <<-75.3447, -813.5533, 284.0001>> fHeading = 146.2765 fWeighting = 0.3
				ELIF iSlot = 8 	vCoords = <<-73.4969, -814.3187, 284.0001>> fHeading = 146.2765  fWeighting = 0.2
				ELIF iSlot = 9 	vCoords = <<-72.9557, -815.6252, 284.0001>> fHeading = 146.2765 fWeighting = 0.2
				ELIF iSlot = 10 vCoords = <<-75.5689, -816.7076, 284.0001>> fHeading = 146.2765 fWeighting = 0.1
				ELIF iSlot = 11	vCoords = <<-76.6513, -814.0945, 284.0001>> fHeading = 146.2765  fWeighting = 0.1
				ENDIF
			BREAK
		ENDSWITCH
	ELIF IS_THIS_PERSONAL_CAR_MOD_IN_TRUCK(eShop, iPersonalCarModVar)
		IF iSlot = 0 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345   fWeighting = 0.9
		ELIF iSlot = 2 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345  fWeighting = 0.8
		ELIF iSlot = 3 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345  fWeighting = 0.7
		ELIF iSlot = 4 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345  fWeighting = 0.6
		ELIF iSlot = 5 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345  fWeighting = 0.5
		ELIF iSlot = 6 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345 fWeighting = 0.4
		ELIF iSlot = 7 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345 fWeighting = 0.3
		ELIF iSlot = 8 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345  fWeighting = 0.2
		ELIF iSlot = 9 	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345 fWeighting = 0.2
		ELIF iSlot = 10 vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345 fWeighting = 0.1
		ELIF iSlot = 11	vCoords = <<-1200.8616, -1737.3851, 3.1713>> fHeading = 284.5345  fWeighting = 0.1
		ENDIF
	ELIF IS_THIS_PERSONAL_CAR_MOD_IN_ARENA(eShop, iPersonalCarModVar)
		IF iSlot = 0 	vCoords = <<2799.3127, -3931.3870, 147.0031>> fHeading = 0.0 fWeighting = 1.0
		ELIF iSlot = 1 	vCoords = <<2801.3127, -3931.3870, 147.0031>> fHeading = 0.0   fWeighting = 0.9
		ELIF iSlot = 2 	vCoords = <<2799.3127, -3929.3870, 147.0031>> fHeading = 0.0  fWeighting = 0.8
		ELIF iSlot = 3 	vCoords = <<2801.3127, -3929.3870, 147.0031>> fHeading = 0.0  fWeighting = 0.7
		ELIF iSlot = 4 	vCoords = <<2799.3127, -3927.3870, 147.0031>> fHeading = 0.0  fWeighting = 0.6
		ELIF iSlot = 5 	vCoords = <<2801.3127, -3927.3870, 147.0031>> fHeading = 0.0  fWeighting = 0.5
		ELIF iSlot = 6 	vCoords = <<2799.3127, -3925.3870, 147.0031>> fHeading = 0.0 fWeighting = 0.4
		ELIF iSlot = 7 	vCoords = <<2801.3127, -3925.3870, 147.0031>> fHeading = 0.0 fWeighting = 0.3
		ELIF iSlot = 8 	vCoords = <<2801.3127, -3929.3870, 147.0031>> fHeading = 0.0  fWeighting = 0.2
		ELIF iSlot = 9 	vCoords = <<2799.3127, -3929.3870, 147.0031>> fHeading = 0.0 fWeighting = 0.2
		ELIF iSlot = 10 vCoords = <<2801.3127, -3931.3870, 147.0031>> fHeading = 0.0 fWeighting = 0.1
		ELIF iSlot = 11	vCoords = <<2799.3127, -3931.3870, 147.0031>> fHeading = 0.0  fWeighting = 0.1
		ENDIF
	ENDIF
	RETURN (NOT ARE_VECTORS_EQUAL(vCoords, <<0,0,0>>))
ENDFUNC

/// PURPOSE: Returns the coords of the spray room for the specific carmod garage.
///    NOTE: We use these coords to position the spray particle effect.
FUNC BOOL GET_CARMOD_SPRAY_ROOM_COORDS(SHOP_NAME_ENUM eShop, VECTOR &vLoc1, VECTOR &vLoc2, FLOAT &fLocWidth  , INT iPersonalCarModVar )
	vLoc1 = <<0.0,0.0,0.0>>
	vLoc2 = <<0.0,0.0,0.0>>
	fLocWidth = 0.0
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(iPersonalCarModVar)) // Mobilemod
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))

	IF (iInteriorCheck = iInterior1)
		vLoc1 = <<-1165.950562,-2015.789429,12.236073>>
		vLoc2 = <<-1169.546387,-2012.092773,13.985141>>
		fLocWidth = 3.500000
		
	ELIF (iInteriorCheck = iInterior3)
		vLoc1 = <<101.375214,6621.337402,30.833986>>
		vLoc2 = <<105.010719,6625.065430,32.583054>> 
		fLocWidth = 3.500000
		
	ELIF (iInteriorCheck = iInterior5)
		vLoc1 = <<733.021545,-1071.859497,21.237959>>
		vLoc2 = <<738.346436,-1071.855225,22.987961>> 
		fLocWidth = 3.500000
		
	ELIF (iInteriorCheck = iInteriorSuper)
		/*SUPERMOD_TODO*/
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ELIF (iInteriorCheck = iInteriorMobile)
		eBaseShop = CARMOD_SHOP_PERSONALMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vLoc1)
	ALIGN_SHOP_COORD(eBaseShop, eShop, vLoc2)
	
	RETURN (fLocWidth != 0.0)
ENDFUNC

/// PURPOSE: Sets up all the spray room cam and player coords
PROC GET_CARMOD_SPRAY_ROOM_DATA(CARMOD_SHOP_STRUCT &sData)

	FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[3], << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0.0, FALSE)
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorMobile = GET_HASH_KEY(GET_PERSONAL_CAR_MOD_SHOP_INTERIOR_TYPE(sData.iPersonalCarModVar)) // Mobilemod
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop))
	
	IF (iInteriorCheck = iInterior1)
		FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[3], <<-1155.9224, -2008.0701, 16.0813>>, <<-19.5482, 0.0000, 105.2472>>, 45.0000, TRUE)
		sData.vPayNSprayLoc1 = <<-1163.977051,-2013.979614,12.242297>>
		sData.vPayNSprayLoc2 = <<-1167.763672,-2010.176147,14.616633>> 
		sData.fPayNSprayWidth1 = 10.937500
		
	ELIF (iInteriorCheck = iInterior3)
		FILL_CAMERA_DATA_CARMOD(sData.sShopInfo.sCameras[3], <<111.833275,6617.769043,33.470455>>,<<-16.981098,-0.000001,67.910126>>,45.000000, TRUE)
		sData.vPayNSprayLoc1 = <<103.090004,6619.515137,30.771496>>
		sData.vPayNSprayLoc2 = <<106.843491,6623.239258,34.083046>> 
		sData.fPayNSprayWidth1 = 10.000000
		
	ELIF (iInteriorCheck = iInterior5)
		// Do nothing
		
	ELIF (iInteriorCheck = iInteriorSuper)
		/*SUPERMOD_TODO*/
		
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ELIF (iInteriorCheck = iInteriorMobile)
		eBaseShop = CARMOD_SHOP_PERSONALMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.sShopInfo.sCameras[3].vPos)
	ALIGN_SHOP_ROTATION(eBaseShop, sData.sShopInfo.eShop, sData.sShopInfo.sCameras[3].vRot)
	
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.vPayNSprayLoc1)
	ALIGN_SHOP_COORD(eBaseShop, sData.sShopInfo.eShop, sData.vPayNSprayLoc2)
	
	// Grab area check separately.
	GET_CARMOD_SPRAY_ROOM_COORDS(sData.sShopInfo.eShop, sData.vPayNSprayLoc3, sData.vPayNSprayLoc4, sData.fPayNSprayWidth2 ,sData.iPersonalCarModVar )
ENDPROC

/// PURPOSE: Sets up all the entry intro data for the los santos customs garage
PROC GET_CARMOD_SHOP_ENTRY_INTRO_DATA(SHOP_NAME_ENUM eShop, SHOP_ENTRY_INFO_STRUCT &sEntryInfo)

	IF eShop = CARMOD_SHOP_01_AP
		// Start position
		sEntryInfo.vPlayerCoords[0] = <<-1105.5139, -1975.6949, 12.0761>>
		sEntryInfo.fPlayerHead[0] 	= 87.9293
		
		sEntryInfo.vPlayerCoords[1] = <<-1130.2709, -1983.4476, 12.1654>>
		sEntryInfo.fPlayerHead[1] 	= 114.2586
				
		SWITCH sEntryInfo.iStage
			// Overview of shop
			CASE 0
				sEntryInfo.vCamCoord[0] = <<-1138.801880,-1980.212769,12.727169>>
				sEntryInfo.vCamRot[0] 	= <<9.925048,-4.277269,164.131790>>
				sEntryInfo.fCamFov[0] 	= 39.161556
				
				sEntryInfo.vCamCoord[1] = <<-1138.644043,-1981.327515,12.727169>>
				sEntryInfo.vCamRot[1] 	= <<9.925048,-4.277269,164.131790>>
				sEntryInfo.fCamFov[1] 	= 39.161556
			BREAK
			
			// Close up of mechanic
			CASE 1
				sEntryInfo.vCamCoord[0] = <<-1149.803955,-2006.059326,14.513581>>
				sEntryInfo.vCamRot[0] 	= <<-13.841268,-4.277272,89.836845>>
				sEntryInfo.fCamFov[0] 	= 45.000000
				
				sEntryInfo.vCamCoord[1] = <<-1147.009521,-2006.066528,15.202101>>
				sEntryInfo.vCamRot[1] 	= <<-13.841268,-4.277272,89.836845>>
				sEntryInfo.fCamFov[1] 	= 45.000000
			BREAK
			
			// Close up of pay n spray
			CASE 2
				sEntryInfo.vCamCoord[0] = <<-1161.011475,-2010.058716,13.641221>>
				sEntryInfo.vCamRot[0] 	= <<-0.605575,0.000000,115.483505>>
				sEntryInfo.fCamFov[0] 	= 45.000000
				
				sEntryInfo.vCamCoord[1] = <<-1159.111084,-2009.153076,13.663468>>
				sEntryInfo.vCamRot[1] 	= <<-0.605575,0.000000,115.483505>>
				sEntryInfo.fCamFov[1] 	= 45.000000
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_CARMOD_SHOP_LAMP_MODEL(SHOP_NAME_ENUM eShop)
	SWITCH eShop
		CASE CARMOD_SHOP_01_AP		RETURN V_ILEV_CARMODLAMPS BREAK 		// Car Mod - AMB1 (v_carmod)
		CASE CARMOD_SHOP_05_ID2		RETURN V_ILEV_CARMODLAMPS BREAK			// Car Mod - AMB2 (v_lockup)
		CASE CARMOD_SHOP_06_BT1		RETURN V_ILEV_CARMODLAMPS BREAK			// Car Mod - AMB3 (v_carmod)
		CASE CARMOD_SHOP_07_CS1		RETURN V_ILEV_CARMOD3LAMP BREAK			// Car Mod - AMB4 (v_carmod3)
		CASE CARMOD_SHOP_08_CS6 	RETURN V_ILEV_CARMOD3LAMP BREAK			// Car Mod - AMB5 (v_carmod3)
		CASE CARMOD_SHOP_SUPERMOD	RETURN LR_PROP_SUPERMOD_LFRAME BREAK	// Car Mod - AMB6 (lr_supermod_int)
	ENDSWITCH
	
	RETURN V_ILEV_CARMODLAMPS
ENDFUNC

FUNC BOOL GET_CARMOD_SHOP_LAMP_ORIENTATION(SHOP_NAME_ENUM eShop, VECTOR &vPos, VECTOR &vRot)
	
	vPos = << 0.0, 0.0, 0.0 >>
	vRot = << 0.0, 0.0, 0.0 >>
	
	INT iInterior1 = GET_HASH_KEY("v_carmod") // LSC
	INT iInterior3 = GET_HASH_KEY("v_carmod3") // Biker
	INT iInterior5 = GET_HASH_KEY("v_lockup") // Underpass
	INT iInteriorSuper = GET_HASH_KEY("lr_supermod_int") // Supermod
	INT iInteriorCheck = GET_HASH_KEY(GET_SHOP_INTERIOR_TYPE(eShop))

	IF (iInteriorCheck = iInterior1)
		vPos = << -1158.845, -2005.970, 13.660 >>
		vRot = << 0.0, 0.0, 112.320 >>	
	ELIF (iInteriorCheck = iInterior3)
		vPos = << 110.594, 6628.001, 32.260 >>
		vRot = << 0.0, 0.0, 41.760 >>
	ELIF (iInteriorCheck = iInterior5)
		vPos = << 731.491, -1086.889, 22.629 >>
		vRot = << 0.0, 0.0, 196.560 >>
		
		//vPos = << 732.571, -1086.889, 22.629 >>
		//vRot = << 0.0, 0.0, 342.500 >>
		
	ELIF (iInteriorCheck = iInteriorSuper)
		vPos = << -211.596, -1324.139, 33.952 >>
		vRot = << 0.0, 0.0, 0.0 >>
	ENDIF
	
	// Align vec data to shop
	SHOP_NAME_ENUM eBaseShop
	IF (iInteriorCheck = iInterior1)
		eBaseShop = CARMOD_SHOP_01_AP
	ELIF (iInteriorCheck = iInterior3)
		eBaseShop = CARMOD_SHOP_07_CS1
	ELIF (iInteriorCheck = iInterior5)
		eBaseShop = CARMOD_SHOP_05_ID2
	ELIF (iInteriorCheck = iInteriorSuper)
		eBaseShop = CARMOD_SHOP_SUPERMOD
	ENDIF
	
	ALIGN_SHOP_COORD(eBaseShop, eShop, vPos)
	ALIGN_SHOP_ROTATION(eBaseShop, eShop, vRot)
	
	
//	// emergency fix for car mod country shop 2
//	IF (eShop = CARMOD_SHOP_08_CS6) AND (iInteriorCheck = iInterior3)
//		vPos = <<1174.081, 2638.714, 38.250>>
//		vRot = <<0, 0, 201.6>>
//	ENDIF
	
//	// B*1576836
//	IF (eShop = CARMOD_SHOP_06_BT1) AND (iInteriorCheck = iInterior1)
//		vPos = <<-339.113, -140.087, 39.490>>
//		vRot = <<0, 0, -132.906>>
//	ENDIF
	
	RETURN (NOT ARE_VECTORS_EQUAL(vPos, <<0.0,0.0,0.0>>))
ENDFUNC

/// PURPOSE: Checks stats to see if item is unlocked.
FUNC BOOL IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ITEMS eItem , SHOP_NAME_ENUM eShop = EMPTY_SHOP , VEHICLE_INDEX vehToMod = NULL ,INT iPersonalCarModVar = -1)

	#IF IS_DEBUG_BUILD
		IF lw_bUnlockAllShopItems OR lw_bUnlockAllCarmods
			RETURN TRUE
		ENDIF
		
		IF g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(eShop, iPersonalCarModVar)
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_TUNER
	IF IS_THIS_PERSONAL_CAR_MOD_IN_TUNER_AUTO_SHOP(eShop, iPersonalCarModVar)
	AND IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(PLAYER_ID())
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_VEHICLE_A_HSW_VEHICLE(vehToMod)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF eItem = DUMMY_CARMOD_UNLOCK_ITEM
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
	ELSE
		MODEL_NAMES vehicleModel
		
		IF DOES_ENTITY_EXIST(vehToMod)
		AND NOT IS_ENTITY_DEAD(vehToMod)
			vehicleModel = GET_ENTITY_MODEL(vehToMod)
		ENDIF
		
		IF IS_THIS_PERSONAL_CAR_MOD_IN_BUNKER(eShop, iPersonalCarModVar)
			IF vehicleModel = PHANTOM3
			OR vehicleModel = HAULER2
				RETURN TRUE
			ELIF vehicleModel = TRAILERLARGE
				IF eItem = CARMOD_UNLOCK_ROOF_L1
				OR eItem = CARMOD_UNLOCK_ROOF_L2
				OR eItem =	CARMOD_UNLOCK_ROOF_L3
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF IS_THIS_PERSONAL_CAR_MOD_IN_JUGGALO_HIDEOUT(eShop, iPersonalCarModVar)
			IF vehicleModel = BRICKADE2
			OR vehicleModel = MANCHEZ3
				RETURN TRUE
			ENDIF
		ENDIF
		#ENDIF
		
		IF IS_THIS_PERSONAL_CAR_MOD_IN_BASE(eShop, iPersonalCarModVar)
			IF vehicleModel = AVENGER
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_THIS_PERSONAL_CAR_MOD_IN_HACKER_TRUCK(eShop, iPersonalCarModVar)
			IF vehicleModel = OPPRESSOR2
				RETURN TRUE
			ENDIF
		ENDIF

		IF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(eShop, iPersonalCarModVar)
			RETURN TRUE
		ENDIF
		
		IF IS_THIS_PERSONAL_CAR_MOD_IN_TRUCK(eShop, iPersonalCarModVar)
			IF vehicleModel = VIGILANTE
				IF eItem = CARMOD_UNLOCK_ROOF_L1
				OR eItem = CARMOD_UNLOCK_ROOF_L2
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		SWITCH vehicleModel
			CASE MONSTER3 
			CASE BRUISER 
			CASE BRUISER2
			CASE BRUISER3
			CASE IMPALER2 
			CASE IMPALER3 
			CASE IMPALER4 
			CASE ISSI4
			CASE DEATHBIKE
			CASE DEATHBIKE2
			CASE DEATHBIKE3
			CASE DOMINATOR
			CASE DOMINATOR2
			CASE DOMINATOR4
			CASE DOMINATOR5
			CASE DOMINATOR6
			CASE SLAMVAN4
			CASE IMPERATOR
			CASE ZR380
			CASE CERBERUS
			CASE SCARAB
			CASE BRUTUS
			CASE IMPERATOR2
			CASE IMPERATOR3
			CASE ZR3802
			CASE ZR3803
			CASE CERBERUS2
			CASE SCARAB2
			CASE BRUTUS2
			CASE CERBERUS3
			CASE SCARAB3
			CASE BRUTUS3
			CASE SLAMVAN5
			CASE SLAMVAN6
			CASE MONSTER4 
			CASE MONSTER5 
			CASE ISSI5
			CASE ISSI6
			CASE RCBANDITO
				RETURN TRUE
			BREAK
		ENDSWITCH	
		
		IF eItem = CARMOD_UNLOCK_ROOF_L1
		OR eItem = CARMOD_UNLOCK_ROOF_L2
		OR eItem =	CARMOD_UNLOCK_ROOF_L3
		OR eItem =	CARMOD_UNLOCK_ROOF_L4
			IF IS_VEHICLE_ALLOWED_IN_ARMORY_WORKSHOP(vehToMod)
			OR IS_VEHICLE_ALLOWED_IN_BUSINESS_HUB_MOD_SHOP(vehicleModel)
				RETURN TRUE
			ENDIF
		ENDIF	
		
		IF eItem = CARMOD_UNLOCK_EXHAUST_L2
		OR eItem = CARMOD_UNLOCK_EXHAUST_L3
			IF vehicleModel = THRUSTER
				RETURN TRUE
			ENDIF
		ENDIF	
		
		IF eItem = CARMOD_UNLOCK_GRILL_L2 
		OR eItem = CARMOD_UNLOCK_GRILL_L3
		OR eItem = CARMOD_UNLOCK_GRILL_L4
			IF vehicleModel = OPPRESSOR2
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF eItem = CARMOD_UNLOCK_SPOILER_L1
		OR eItem = CARMOD_UNLOCK_SPOILER_L2
	 	OR eItem = CARMOD_UNLOCK_SPOILER_L3
		OR eItem = CARMOD_UNLOCK_SPOILER_L4
			IF vehicleModel = BARRAGE
			OR IS_VEHICLE_ALLOWED_IN_BUSINESS_HUB_MOD_SHOP(vehicleModel)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF eItem = CARMOD_UNLOCK_CHASSIS_L2
		OR eItem = CARMOD_UNLOCK_CHASSIS_L3
	 	OR eItem = CARMOD_UNLOCK_CHASSIS_L4
			IF IS_VEHICLE_ALLOWED_IN_BUSINESS_HUB_MOD_SHOP(vehicleModel)
				RETURN TRUE
			ENDIF
		
		ELIF eItem = CARMOD_UNLOCK_R_WING_L2
		OR eItem = CARMOD_UNLOCK_R_WING_L3
			IF IS_VEHICLE_ALLOWED_IN_BUSINESS_HUB_MOD_SHOP(vehicleModel)
			OR IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehicleModel)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF eItem = CARMOD_UNLOCK_F_BUMPER_L2
		OR eItem = CARMOD_UNLOCK_F_BUMPER_L3
		OR eItem = CARMOD_UNLOCK_F_BUMPER_L4
		OR eItem = CARMOD_UNLOCK_F_BUMPER_L5
		OR eItem = CARMOD_UNLOCK_R_BUMPER_L2
		OR eItem = CARMOD_UNLOCK_R_BUMPER_L3
		OR eItem = CARMOD_UNLOCK_R_BUMPER_L4
			IF IS_VEHICLE_ALLOWED_IN_BUSINESS_HUB_MOD_SHOP(vehicleModel)
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF

	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		RETURN IS_MP_CARMOD_UNLOCKED(eItem)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_ARENA_CAREER_LOCKED_MOD_MENU_HAVE_BUY_NOW_OPTION(CARMOD_MENU_ENUM eMenu)
	SWITCH eMenu
		CASE CMM_LIGHTS_HEAD
		CASE CMM_SUPERMOD_CHASSIS3
		CASE CMM_CHASSIS
		CASE CMM_FENDERS
		CASE CMM_SUPERMOD_ENGINEBAY2
		CASE CMM_SUPERMOD_ENGINEBAY3
		CASE CMM_SUPERMOD_LIVERY
		CASE CMM_BUMPERS
		CASE CMM_EXHAUST
		CASE CMM_HOOD
		CASE CMM_SUPERMOD_PLAQUE
		CASE CMM_GRILL
		CASE CMM_SKIRTS
		CASE CMM_FENDERSTWO
		CASE CMM_ROOF
		CASE CMM_ROLLCAGE
		CASE CMM_SUPERMOD_PLTVANITY
		CASE CMM_FRONTMUDGUARD
		CASE CMM_SPOILER
		CASE CMM_SPAREWHEEL
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARENA_CAREER_MOD_SHOP_ITEM_UNLOCKED(VEHICLE_INDEX vehToCheck, CARMOD_MENU_ENUM eMenu, INT iMenuIndex, INT iControl #IF IS_DEBUG_BUILD, BOOL bUnlockForDebug = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockArenaCareerMods")
	OR bUnlockForDebug
		RETURN TRUE
	ENDIF
	#ENDIF
	
	SWITCH eMenu
		CASE CMM_SPAREWHEEL
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE BRUISER2
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_SPARE_TIRE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_CRATES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_SUPERMOD_PLTVANITY
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE ISSI4
				CASE ISSI6
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_SPEAR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_LEFT_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_DOLLY_SPEARTON)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_RIGHT_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_SKULL_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_DOLLY_SPEARTON_SET)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_DUAL_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 8
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_DOLLY_SPEARTON_W_WAR_POLE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 9
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_SKULL_CROSS_W_SPEAR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 10
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_SKULL_CROSS_W_WAR_POLE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 11
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_SKULL_CROSS_W_DOLLY)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ROLLCAGE
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE SCARAB
				CASE SCARAB3
					SWITCH 	iMenuIndex 
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_CAGE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_PLATED_CAGE)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_FRONTMUDGUARD
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE SCARAB2
					SWITCH 	iMenuIndex 
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 8
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SECONDARY_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 9
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_CARBON_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 10
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_LIVERY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK	
					ENDSWITCH
				BREAK
				CASE SCARAB
				CASE SCARAB3
					SWITCH 	iMenuIndex 
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SECONDARY_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_CARBON_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 8
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_RUSTY_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ROOF
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE ZR3802
					IF 	iMenuIndex = 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_REAR_PHANTOM_COVERS)
							RETURN FALSE
						ENDIF	
					ENDIF
				BREAK
				CASE BRUTUS3
				CASE BRUTUS
					IF 	iMenuIndex = 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_ARMORED_SPARES)
							RETURN FALSE
						ENDIF
					ELIF 	iMenuIndex = 4
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_ARMORED_SUPPLIES)
							RETURN FALSE
						ENDIF	
					ENDIF
				BREAK
				CASE SCARAB
				CASE SCARAB3
					SWITCH 	iMenuIndex 
						CASE 7
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_AIR_FILTRATION_VENTS)
								RETURN FALSE
							ENDIF
						BREAK	
					ENDSWITCH
				BREAK
				CASE SCARAB2
					SWITCH 	iMenuIndex 
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_AIR_FILTRATION_VENTS)
								RETURN FALSE
							ENDIF
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_SKIRTS
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE BRUTUS3
				CASE BRUTUS
					IF 	iMenuIndex = 2
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_GRASSED_UP_BAR)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SCARAB
				CASE SCARAB3
					IF iMenuIndex = 8
					OR iMenuIndex = 9
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_MEGA_COVER)
							RETURN FALSE
						ENDIF
					ELIF iMenuIndex = 10
					OR iMenuIndex = 11
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_RUSTY_MEGA_COVER)
							RETURN FALSE
						ENDIF	
					ENDIF
				BREAK
				CASE SCARAB2
					SWITCH iMenuIndex
						CASE 11
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_PRIMARY_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 12
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_LIVERY_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 13
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SKIRT_CARBON_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 14
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_MATTE_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 15
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_FUTURISTIC_PANEL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 16
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_PLATED_LIVERY_FULL_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_FENDERSTWO
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE BRUTUS3
				CASE BRUTUS
					IF 	iMenuIndex = 2
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_HVY_ARMORED_ARCHES)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_BUMPERS
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE MONSTER3
				CASE MONSTER4
				CASE MONSTER5
					IF iControl = 1
						IF 	iMenuIndex = 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_HEAVY_ARMORED_FRONT)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF	
				BREAK
				CASE CERBERUS
				CASE CERBERUS2
				CASE CERBERUS3
					IF iControl = 2
						IF 	iMenuIndex = 23
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_BASTIONED_RAM_BARS)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF	
				BREAK
				CASE BRUISER2
					IF iControl = 1
						IF 	iMenuIndex = 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_LIGHT_COVER_SET)
								RETURN FALSE
							ENDIF
						ENDIF
					ELIF  iControl = 2
						IF 	iMenuIndex = 21
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_LIGHT_COVER_SET)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF	
				BREAK
				CASE SLAMVAN4
				CASE SLAMVAN5
				CASE SLAMVAN6
					IF iControl = 2
						IF 	iMenuIndex = 22
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_R_BUMPER_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						ELIF 	iMenuIndex = 23
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_R_BUMPER_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF	
						ENDIF
					ENDIF	
				BREAK
				CASE BRUTUS
				CASE BRUTUS3
					IF iControl = 2
						IF 	iMenuIndex = 23
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_JUNK_TRUNK)
								RETURN FALSE
							ENDIF
						ENDIF
					ELIF iControl = 1
						IF 	iMenuIndex = 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_ROADBLOCK)
								RETURN FALSE
							ENDIF
						ENDIF	
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_GRILL
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE ISSI4
				CASE ISSI5
				CASE ISSI6
					SWITCH 	iMenuIndex 
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_HEAVY_DUTY_RAM_BAR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ZR3802
					SWITCH 	iMenuIndex 
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_ARMOR_PLATING_LVL3)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPERATOR
				CASE IMPERATOR3
					SWITCH 	iMenuIndex 
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_RIDE_EM_COWBOY)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_CANNIBAL_TOTEM)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER
				CASE BRUISER2
				CASE BRUISER3
					IF 	iMenuIndex = 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_HEAVY_ARMORED_GRILLE)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUTUS
				CASE BRUTUS3
					IF 	iMenuIndex = 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_TOOTHY)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_HOOD
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE ZR3803
					IF 	iMenuIndex = 4
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_MISMATCH)
							RETURN FALSE
						ENDIF
					ENDIF	
				BREAK
				CASE ZR380
					IF 	iMenuIndex = 5
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_MISMATCH)
							RETURN FALSE
						ENDIF
					ENDIF	
				BREAK
				CASE DOMINATOR4
				CASE DOMINATOR6
					IF 	iMenuIndex = 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_SKULL_HOOD)
							RETURN FALSE
						ENDIF
					ENDIF	
				BREAK
				CASE MONSTER3
				CASE MONSTER4
				CASE MONSTER5
					IF 	iMenuIndex = 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_HEAVY_ARMORED_HOOD)
							RETURN FALSE
						ENDIF
					ENDIF	
				BREAK
				CASE CERBERUS
				CASE CERBERUS2
				CASE CERBERUS3
					IF 	iMenuIndex = 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_BOLSTERED_HOOD_CAGE)
							RETURN FALSE
						ENDIF
					ELIF iMenuIndex = 4
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_REINFORCED_RIOT_HOOD)
							RETURN FALSE
						ENDIF
					ELIF iMenuIndex = 5
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_JUGGERNAUT_HOOD)
							RETURN FALSE
						ENDIF	
					ENDIF	
				BREAK
				CASE SLAMVAN4
				CASE SLAMVAN6
					SWITCH 	iMenuIndex 
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_BATTLE_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_WAR_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_EXHAUST
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE IMPERATOR
				CASE IMPERATOR3
				CASE IMPERATOR2
					SWITCH 	iMenuIndex 
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_SHAKOTAN_EXHAUST)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_JUNK_PIPES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_MEGA_ZORST)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ZR380
				CASE ZR3803
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_SIDE_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_SPIKE_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ZR3802
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_SIDE_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_SPIKE_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_RAY_GUN_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DOMINATOR4
				CASE DOMINATOR5
				CASE DOMINATOR6
					SWITCH 	iMenuIndex 
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_TRIP_FRONT_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_HORN_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_TRIP_REAR_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE MONSTER3
				CASE MONSTER4
				CASE MONSTER5
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_MOHAWK_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_DUAL_MOHAWK_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER
				CASE BRUISER2
				CASE BRUISER3
					SWITCH 	iMenuIndex 
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_TWIN_OVAL_EXHAUST)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_LONG_TRIPLE_REAR_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_FRONT_AND_REAR_TRIP_EXHAUSTS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUTUS
				CASE BRUTUS3
					SWITCH 	iMenuIndex 
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_FIRE_SPITTERS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_HELL_CHAMBERS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_SPOILER
			SWITCH GET_ENTITY_MODEL(vehToCheck)	
				CASE ZR3802
					SWITCH 	iMenuIndex 
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_SPRINT_CAR_WING)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER
					SWITCH 	iMenuIndex 
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_BARRELS_AND_JUNK)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_SKELETON_CAGE)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER2
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_SPARE_TIRE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_CRATES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER3
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_lARGE_BURGER)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_LARGE_DOUGHNUTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_lARGE_ECOLA_CANS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_SUPERMOD_PLAQUE
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE ISSI4
				CASE ISSI6
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_LEFT_SPEAR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_RIGHT_SPEAR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_LEFT_SKULL_AXE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_RIGHT_AXE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_DUAL_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_SPEAR_AND_AXE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_AXE_AND_SPEAR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 8
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_DUAL_AXES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPERATOR
				CASE IMPERATOR3
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_WHOLE_LOTTA_POLE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_GETTING_MEDIEVAL)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_ITS_A_STICK_UP)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_BOOM_ON_A_SPEAR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_VILLAGE_JUSTICE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_WASTELAND_PEACOCK)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_SHISH_KEBAB)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPALER2
				CASE IMPALER4
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_GOT_POLE)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_GETTING_MEDIEVAL)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_WASTELAND_PEACOCK)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_SHISH_KEBAB)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_ITS_A_STICK_UP)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_THE_DARK_AGES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_DOLLY_SPEARTON)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 8
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DOMINATOR4
				CASE DOMINATOR6
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_REAR_POINTING_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_FRONT_FACING_AXES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_FRONT_FACING_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_UNHOLY_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_BRUTAL_UNHOLY_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_BUNCH_OF_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_FRONT_POINTING_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SCARAB
				CASE SCARAB3
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_REAR_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_REAR_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SKULL_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SKULL_CROSS_W_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SKULL_CROSS_W_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_LODA_WAR_POLES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_LODA_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUTUS
				CASE BRUTUS3
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_ETERNALLY_CHAINED)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_SPEARED)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SLAMVAN4
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_BASIC_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_BATTLE_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_WAR_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SLAMVAN6
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_BASIC_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_KNIFE_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_FORK_AND_KNIFE_SPEARS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE MONSTER3
				CASE MONSTER5
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_REAR_SPEARS_LEFT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_REAR_SPEARS_RIGHT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_SKULL_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_RAM_SKULL_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_BLONDE_DOLL_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_BRUNETTE_DOLL_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE CERBERUS
				CASE CERBERUS3
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_WAR_SPEARHEADS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_WAR_SPEAR_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_NADE_SPEARHEADS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_NADE_SPEAR_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_SKULL_SPEARHEADS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_SKULL_SPEAR_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 7
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_ARROW_SPEARHEADS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 8
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_ARROW_SPEAR_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 9
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_TRIDENTS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 10
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_WASTELAND_RITUAL)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH	
				BREAK
				CASE CERBERUS2
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_PANEL_DETAIL)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_CRANE_PIPES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_HEDGEHOG)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_HEDGEHOG_MK2)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER
				CASE BRUISER3
					SWITCH 	iMenuIndex 
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_DOUBLE_CROSS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_CROSS_BLADE_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_TIKI_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_MEDIEVAL_KIT)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_MEDIEVAL_MADNESS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_SUPERMOD_LIVERY
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE MONSTER3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_MONSTER_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE MONSTER4
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_MONSTER_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE MONSTER5
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_MONSTER_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE CERBERUS
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_CERBERUS_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE CERBERUS2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_CERBERUS_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE CERBERUS3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_CERBERUS_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUISER
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUISER_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUISER2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUISER_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUISER3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUISER_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SLAMVAN4
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SLAMVAN_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SLAMVAN5
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SLAMVAN_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SLAMVAN6
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SLAMVAN_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUTUS
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUTUS_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUTUS2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUTUS_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE BRUTUS3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUTUS_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SCARAB
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SCARAB_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SCARAB2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SCARAB_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE SCARAB3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SCARAB_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE DOMINATOR4
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DOMINATOR_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE DOMINATOR5
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DOMINATOR_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE DOMINATOR6
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DOMINATOR_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE IMPALER2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPALER_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE IMPALER3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPALER_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE IMPALER4
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPALER_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE IMPERATOR
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPERATOR_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE IMPERATOR2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPERATOR_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE IMPERATOR3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPERATOR_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE ZR380
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ZR380_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE ZR3802
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ZR380_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE ZR3803
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ZR380_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE ISSI4
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ISSI_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE ISSI5
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ISSI_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE ISSI6
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ISSI_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE DEATHBIKE
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DEATHBIKE_APOC_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE DEATHBIKE2
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DEATHBIKE_SCIFI_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				CASE DEATHBIKE3
					IF 	iMenuIndex > 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DEATHBIKE_NIGHT_LIVERY_SET)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_LIGHTS_HEAD
			SWITCH 	iMenuIndex
				CASE 3
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_BLUE_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 4
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_ELEC_BLUE_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 5
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_MINT_GREEN_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 6 
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_LIME_GREEN_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 7
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_YELLOW_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE  8
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_GS_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 9
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_ORANGE_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 10
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_RED_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 11
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_PONY_PINK_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 12
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_HOT_PINK_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 13
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_PURPLE_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK
				CASE 14
					IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_BLACKLIGHT_LIGHTS)
						RETURN FALSE
					ENDIF
				BREAK	
			ENDSWITCH
		BREAK
		CASE CMM_SUPERMOD_CHASSIS3
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE CERBERUS
				CASE CERBERUS2
				CASE CERBERUS3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER
				CASE BRUISER2
				CASE BRUISER3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SLAMVAN4
				CASE SLAMVAN5
				CASE SLAMVAN6
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPALER2
				CASE IMPALER3
				CASE IMPALER4
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPERATOR
				CASE IMPERATOR2
				CASE IMPERATOR3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ZR380
				CASE ZR3802
				CASE ZR3803
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ISSI4
				CASE ISSI5
				CASE ISSI6
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SCARAB
					IF iMenuIndex > 0					
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_APOC_BODY_SPIKES)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK					
				CASE SCARAB2
					IF iMenuIndex > 0					
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_SCIFI_BODY_SPIKES)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK	
				CASE SCARAB3
					IF iMenuIndex > 0					
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_NIGHT_BODY_SPIKES)
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK	
				CASE DOMINATOR4
				CASE DOMINATOR5
				CASE DOMINATOR6
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_BODY_SPIKES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_FENDERS
			IF IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(GET_ENTITY_MODEL(vehToCheck))
			AND NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehToCheck))
				IF NOT NETWORK_IS_ACTIVITY_SESSION()
					SWITCH 	iMenuIndex
						CASE 22
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_KINETIC_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 23
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SPIKE_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 24
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_EMP_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 25
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SLICK_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 26
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_TAR_MINES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_KINETIC_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SPIKE_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_EMP_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SLICK_MINES)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_TAR_MINES)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_ENGINEBAY3
			IF IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(GET_ENTITY_MODEL(vehToCheck))
				SWITCH 	iMenuIndex
					CASE 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_VEH_JUMP_UPGRADE_1)
							RETURN FALSE
						ENDIF
					BREAK
					CASE 2
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_VEH_JUMP_UPGRADE_2)
							RETURN FALSE
						ENDIF
					BREAK
					CASE 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_VEH_JUMP_UPGRADE_3)
							RETURN FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_ENGINEBAY2
			IF IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(GET_ENTITY_MODEL(vehToCheck))
				SWITCH 	iMenuIndex
					CASE 1
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_VEH_BOOST_UPGRADE_1)
							RETURN FALSE
						ENDIF
					BREAK
					CASE 2
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_VEH_BOOST_UPGRADE_2)
							RETURN FALSE
						ENDIF
					BREAK
					CASE 3
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_VEH_BOOST_UPGRADE_3)
							RETURN FALSE
						ENDIF
					BREAK
					CASE 4
						IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SHUNT_BOOST)
							RETURN FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_CHASSIS
			SWITCH GET_ENTITY_MODEL(vehToCheck)
				CASE MONSTER3
				CASE MONSTER4
				CASE MONSTER5
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MONSTER_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_MONSTER_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE CERBERUS
				CASE CERBERUS2
				CASE CERBERUS3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_CERBERUS_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_CERBERUS_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUISER
				CASE BRUISER2
				CASE BRUISER3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUISER_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUISER_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SLAMVAN4
				CASE SLAMVAN5
				CASE SLAMVAN6
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SLAMVAN_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SLAMVAN_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE BRUTUS
				CASE BRUTUS2
				CASE BRUTUS3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_BRUTUS_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_BRUTUS_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE SCARAB
				CASE SCARAB2
				CASE SCARAB3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_SCARAB_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_SCARAB_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DOMINATOR4
				CASE DOMINATOR5
				CASE DOMINATOR6
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DOMINATOR_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DOMINATOR_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPALER2
				CASE IMPALER3
				CASE IMPALER4
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPALER_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPALER_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE IMPERATOR
				CASE IMPERATOR2
				CASE IMPERATOR3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_IMPERATOR_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_IMPERATOR_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ZR380
				CASE ZR3802
				CASE ZR3803
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ZR380_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ZR380_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ISSI4
				CASE ISSI5
				CASE ISSI6
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_ISSI_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_ISSI_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DEATHBIKE2
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DEATHBIKE_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DEATHBIKE_LIGHT_ARMOR_WS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DEATHBIKE_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 4
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DEATHBIKE_REINFORCED_ARMOR_WS)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 5
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DEATHBIKE_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 6
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DEATHBIKE_HEAVY_ARMOR_WS)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DEATHBIKE
				CASE DEATHBIKE3
					SWITCH 	iMenuIndex
						CASE 1
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DEATHBIKE_LIGHT_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 2
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_DEATHBIKE_REINFORCED_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
						CASE 3
							IF !IS_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_HV_DEATHBIKE_HEAVY_ARMOR)
								RETURN FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC	

PROC SET_HEAD_LIGHT_CAREER_ITEM_UNLOCKED(INT iMenuIndex, BOOL bSet)
	SWITCH 	iMenuIndex
		CASE 3 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_BLUE_LIGHTS, bSet)			BREAK
		CASE 4 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_ELEC_BLUE_LIGHTS, bSet)	BREAK
		CASE 5 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_MINT_GREEN_LIGHTS, bSet)	BREAK
		CASE 6 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_LIME_GREEN_LIGHTS, bSet)	BREAK
		CASE 7 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_YELLOW_LIGHTS, bSet)		BREAK
		CASE 8 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_GS_LIGHTS, bSet)			BREAK
		CASE 9 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_ORANGE_LIGHTS, bSet)		BREAK
		CASE 10 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_RED_LIGHTS, bSet)			BREAK
		CASE 11 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_PONY_PINK_LIGHTS, bSet)	BREAK
		CASE 12 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_HOT_PINK_LIGHTS, bSet)	BREAK
		CASE 13 SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_PURPLE_LIGHTS, bSet)		BREAK
		CASE 14	SET_ARENA_CAREER_ITEM_UNLOCKED(CAREER_ITEM_LV_MOD_BLACKLIGHT_LIGHTS, bSet)	BREAK
	ENDSWITCH
ENDPROC

FUNC BUNKER_RESEARCH_ITEMS GET_RESEARCH_ITEM_FOR_CAR_MOD(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex)
		SWITCH eVehicleModel
		CASE APC
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // SAM Battery
					RETURN RESEARCH_ITEM_SAM_BATTERY
				ENDIF
			ENDIF
			IF eModSlot = MOD_WING_R
				IF iModIndex = 11 // Proximity Mine
					RETURN RESEARCH_ITEM_PROXIMITY_MINES
				ENDIF
			ENDIF
		BREAK
		CASE HALFTRACK
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // Quad 20mm Flak Cannons
					RETURN RESEARCH_ITEM_QUAD_HALFTRACK_FLAK_CANNONS
				ENDIF
			ENDIF
			IF eModSlot = MOD_WING_R
				IF iModIndex = 11 // Proximity Mine
					RETURN RESEARCH_ITEM_PROXIMITY_MINES
				ENDIF
			ENDIF
		BREAK
		CASE DUNE3
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // 40mm Grenade Launcher
					RETURN RESEARCH_ITEM_DUNE3_GRENADE_LAUNCHER
				ELIF iModIndex = 2 // 7.62mm Minigun
					RETURN RESEARCH_ITEM_DUNE3_MINIGUN
				ENDIF
			ENDIF
			IF eModSlot = MOD_WING_R
				IF iModIndex = 11 // Proximity Mine
					RETURN RESEARCH_ITEM_PROXIMITY_MINES
				ENDIF
			ENDIF
		BREAK
		CASE TAMPA3
			IF eModSlot = MOD_BUMPER_F
				IF iModIndex = 1 // Front Missile Launcher
					RETURN RESEARCH_ITEM_TAMPA3_FRONT_MISSILE_LAUNCHER
				ENDIF	
			ELIF eModSlot = MOD_BUMPER_R
				IF iModIndex = 1 // Rear-firing Mortar
					RETURN RESEARCH_ITEM_TAMPA3_REAR_MORTAR
				ENDIF		
			ELIF eModSlot = MOD_ROOF
				IF iModIndex = 1 // Duel Miniguns
					RETURN RESEARCH_ITEM_TAMPA3_MINIGUN
				ENDIF
			ENDIF
			IF eModSlot = MOD_WING_R
				IF iModIndex = 11 // Proximity Mine
					RETURN RESEARCH_ITEM_PROXIMITY_MINES
				ENDIF
			ENDIF
			IF eModSlot = MOD_CHASSIS
				IF iModIndex > 2
					RETURN RESEARCH_ITEM_TAMPA3_HEAVY_CHASSIS_ARMOR
				ENDIF
			ENDIF
		BREAK
		CASE INSURGENT3
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // Minigun
					RETURN RESEARCH_ITEM_INSURGENT3_CAL_MINIGUN
				ENDIF
			ENDIF
			IF eModSlot = MOD_CHASSIS
				IF iModIndex = 3 // Lvl 3 Vehicle Armor
					RETURN RESEARCH_ITEM_INSURGENT3_LVL3_VEH_ARMROR
				ENDIF
			ENDIF
			IF eModSlot = MOD_WING_R
				IF iModIndex = 11 // Proximity Mine
					RETURN RESEARCH_ITEM_PROXIMITY_MINES
				ENDIF
			ENDIF
		BREAK
		
		CASE TECHNICAL3
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // Minigun
					RETURN RESEARCH_ITEM_TECHNICAL3_MINIGUN
				ENDIF
			ENDIF
			IF eModSlot = MOD_CHASSIS
				IF iModIndex = 3 // Lvl 3 Vehicle Armor
					RETURN RESEARCH_ITEM_TECHNICAL3_HEAVY_CHASSIS_ARMOR
				ENDIF
			ENDIF
			IF eModSlot = MOD_GRILL
				IF iModIndex = 4 // Ram Bar
					RETURN RESEARCH_ITEM_TECHNICAL3_RAM_BAR_MOD
				ELIF iModIndex = 5 // Brute Bar
					RETURN RESEARCH_ITEM_TECHNICAL3_BRUTE_BAR_MOD
				ENDIF
			ENDIF
			IF eModSlot = MOD_WING_R
				IF iModIndex = 11 // Proximity Mine
					RETURN RESEARCH_ITEM_PROXIMITY_MINES
				ENDIF
			ENDIF
		BREAK
		
		CASE OPPRESSOR
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // ROCKETS
					RETURN RESEARCH_ITEM_OPPRESSOR_ROCKETS
				ENDIF
			ENDIF
		BREAK
		
		CASE TRAILERSMALL2
			IF eModSlot = MOD_ROOF
				IF iModIndex = 1 // MISSILE BARRAGE
					RETURN RESEARCH_ITEM_AA_MISSILE_BARRAGE
				ELIF iModIndex = 2 // DUEL FLAK
					RETURN RESEARCH_ITEM_AA_DUEL_FLAK
				ENDIF
			ENDIF
		BREAK
		CASE TRAILERLARGE
			IF eModSlot = MOD_ROOF
				IF iModIndex = 2
					RETURN RESEARCH_ITEM_TRAILER_LARGE_TURRET
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	

	//LIVERY
	IF eVehicleModel = APC
	OR eVehicleModel = HALFTRACK
	OR eVehicleModel = DUNE3
	OR eVehicleModel = TAMPA3
	OR eVehicleModel = INSURGENT3
	OR eVehicleModel = TECHNICAL3
	OR eVehicleModel = NIGHTSHARK
	OR eVehicleModel = ARDENT
	OR eVehicleModel = TRAILERSMALL2
	OR eVehicleModel = OPPRESSOR
	OR eVehicleModel = KHANJALI
	OR eVehicleModel = CHERNOBOG
	OR eVehicleModel = THRUSTER
	OR eVehicleModel = BARRAGE
	OR eVehicleModel = RIOT2
	OR eVehicleModel = AKULA
	OR eVehicleModel = AVENGER
	OR eVehicleModel = REVOLTER
	OR eVehicleModel = CARACARA
	OR eVehicleModel = SPEEDO4
	OR eVehicleModel = MULE4
	OR eVehicleModel = POUNDER2
	OR eVehicleModel = MENACER
	OR eVehicleModel = OPPRESSOR2
	OR eVehicleModel = PATRIOT2
	OR eVehicleModel = PATRIOT
	OR eVehicleModel = VERUS
	OR eVehicleModel = SQUADDIE
	OR eVehicleModel = MANCHEZ2
	OR eVehicleModel = WINKY
	OR eVehicleModel = ANNIHILATOR2
		IF eModSlot = MOD_LIVERY
			IF iModIndex = 10 // Nature Reserve
				RETURN RESEARCH_ITEM_LIVERY_NATURE_RESERVE
			ENDIF
			IF iModIndex = 11 // Naval 
				RETURN RESEARCH_ITEM_LIVERY_NAVAL_BATTLE
			ENDIF
			IF iModIndex >= 12 AND iModIndex <= 15 // Geometric Set
				RETURN RESEARCH_ITEM_LIVERY_SET_GEOMETRIC
			ENDIF
			IF iModIndex >= 16 AND iModIndex <= 17 // Fractal Set
				RETURN RESEARCH_ITEM_LIVERY_SET_FRACTAL
			ENDIF
			IF iModIndex >= 18 AND iModIndex <= 20 // Digital Set
				RETURN RESEARCH_ITEM_LIVERY_SET_DIGITAL
			ENDIF
		ENDIF
	ENDIF
	
	RETURN RESEARCH_ITEM_INVALID
ENDFUNC

FUNC BOOL IS_CARMOD_ITEM_A_RESEARCH_ITEM(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex)
	RETURN GET_RESEARCH_ITEM_FOR_CAR_MOD(eVehicleModel, eModSlot, iModIndex) != RESEARCH_ITEM_INVALID
ENDFUNC

FUNC BOOL IS_CARMOD_ITEM_LIMITED_AMMO(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex)

	IF IS_CARMOD_ITEM_A_RESEARCH_ITEM(eVehicleModel, eModSlot, iModIndex)
		BUNKER_RESEARCH_ITEMS eItem = GET_RESEARCH_ITEM_FOR_CAR_MOD(eVehicleModel, eModSlot, iModIndex)
		SWITCH eItem
			CASE RESEARCH_ITEM_SAM_BATTERY
			CASE RESEARCH_ITEM_DUNE3_GRENADE_LAUNCHER
			CASE RESEARCH_ITEM_TAMPA3_FRONT_MISSILE_LAUNCHER
			CASE RESEARCH_ITEM_TAMPA3_REAR_MORTAR
			CASE RESEARCH_ITEM_OPPRESSOR_ROCKETS
			CASE RESEARCH_ITEM_AA_MISSILE_BARRAGE
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CARMOD_ITEM_LOCKED_FOR_RESEARCH(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex)
	#IF IS_DEBUG_BUILD
		IF lw_bUnlockAllShopItems OR lw_bUnlockAllCarmods
			RETURN FALSE
		ENDIF
		
		IF g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF IS_CARMOD_ITEM_A_RESEARCH_ITEM(eVehicleModel, eModSlot, iModIndex)
	AND NOT IS_RESEARCH_ITEM_UNLOCKED(GET_RESEARCH_ITEM_FOR_CAR_MOD(eVehicleModel, eModSlot, iModIndex))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESEARCH_ITEM_PURCHASE_UNLOCK(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex)	
	IF IS_CARMOD_ITEM_A_RESEARCH_ITEM(eVehicleModel, eModSlot, iModIndex)
	AND IS_RESEARCH_ITEM_UNLOCKED_BY_TUNABLE(GET_RESEARCH_ITEM_FOR_CAR_MOD(eVehicleModel, eModSlot, iModIndex))
		RESEARCH_ITEM_INSTANT_UNLOCK(GET_RESEARCH_ITEM_FOR_CAR_MOD(eVehicleModel, eModSlot, iModIndex))
	ENDIF
ENDPROC

FUNC INT GET_NUM_HOTRACE_NEEDED_FOR_LIVERY_UNLOCK(INT iModIndex)
	SWITCH iModIndex
		CASE 21		RETURN 2	BREAK
		CASE 22		RETURN 4	BREAK
		CASE 23		RETURN 6	BREAK
		CASE 24		RETURN 8	BREAK
		CASE 25		RETURN 10	BREAK
		CASE 26 	RETURN 12	BREAK
		CASE 27		RETURN 14	BREAK
		CASE 28		RETURN 16	BREAK
		CASE 29		RETURN 18	BREAK
		CASE 30		RETURN 20	BREAK	
	ENDSWITCH
	RETURN -1
ENDFUNC 

/// PURPOSE: Checks to see if the mod item has a text label defined by vehicle artists
FUNC BOOL SETUP_CARMOD_ITEM_LIVERY_LABEL(VEHICLE_INDEX vehToCheck, TEXT_LABEL_15 &sRetLabel, MOD_TYPE eModType, INT iIndex)
	
	IF DOES_ENTITY_EXIST(vehToCheck)
	AND IS_VEHICLE_DRIVEABLE(vehToCheck)	
		iIndex--
		IF iIndex >= 0
			sRetLabel = GET_MOD_TEXT_LABEL(vehToCheck, eModType, iIndex)
			IF GET_HASH_KEY(sRetLabel) != 0
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HOTRING_LIVERY_LOCKED(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex #IF IS_DEBUG_BUILD, BOOL bDebugUnlock #ENDIF )
	#IF FEATURE_GEN9_EXCLUSIVE
	TEXT_LABEL_15 sItemText
	#ENDIF 
	
	SWITCH eVehicleModel
	 	CASE HOTRING
			IF eModSlot = MOD_LIVERY
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_COUNT_HOTRING_RACE) < GET_NUM_HOTRACE_NEEDED_FOR_LIVERY_UNLOCK(iModIndex) //+ 1
				#IF IS_DEBUG_BUILD
				AND NOT bDebugUnlock
				#ENDIF
					
					#IF FEATURE_DLC_1_2022
					IF iModIndex = 24 // eCola
						IF g_sMPTunables.bFREE_ECOLA_LIVERY
							RETURN FALSE
						ENDIF
					ELIF iModIndex = 28 // Sprunk
						IF g_sMPTunables.bFREE_SPRUNK_LIVERY
							RETURN FALSE
						ENDIF
					ENDIF
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF	
		BREAK	
		CASE WEEVIL
			IF eModSlot = MOD_LIVERY
				IF iModIndex = 15
					INT iStatInt
					iStatInt = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES16)
					
					IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_COURIER)
					AND NOT IS_BIT_SET(iStatInt, 12)
					#IF IS_DEBUG_BUILD
					AND NOT bDebugUnlock
					#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		DEFAULT
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
				IF eModSlot = MOD_LIVERY
				AND iModIndex > 0
					IF SETUP_CARMOD_ITEM_LIVERY_LABEL(g_ModVehicle, sItemText, eModSlot, iModIndex)
					AND NOT IS_VEHICLE_MOD_LOCKED(g_ModVehicle, eModSlot, iModIndex)
						IF NOT IS_STRING_NULL_OR_EMPTY(sItemText)
							IF GET_HASH_KEY(sItemText) = HASH("HSW_LIV2")
							OR GET_HASH_KEY(sItemText) = HASH("VIGR2_HSWLIV_1")
								IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_HSW_TIME_TRIAL_LIVERY)
								#IF IS_DEBUG_BUILD
								AND NOT bDebugUnlock
								#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF	
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CAR_CLUB_REP_LEVEL_FOR_LIVERY_UNLOCK(MODEL_NAMES eVehicleModel, INT iModIndex)
	CAR_CLUB_REP_REWARDS eRepReward = GET_CAR_MEET_VEHICLE_LIVERY_UNLOCK_FOR_MOD_INDEX(eVehicleModel, iModIndex)
	IF eRepReward != REP_REWARD_INVALID
		RETURN GET_CAR_CLUB_REP_REWARD_TIER(eRepReward)
	ENDIF
	RETURN -1
ENDFUNC 

FUNC BOOL IS_CAR_CLUB_LIVERY_LOCKED(MODEL_NAMES eVehicleModel, MOD_TYPE eModSlot, INT iModIndex #IF IS_DEBUG_BUILD, BOOL bDebugUnlock #ENDIF )

	CAR_CLUB_REP_REWARDS eRepReward

	SWITCH eVehicleModel
	 	CASE TAILGATER2
	 	CASE EUROS
	 	CASE RT3000
	 	CASE ZR350
	 	CASE WARRENER2
	 	CASE CALICO
	 	CASE REMUS
	 	CASE JESTER4
	 	CASE FUTO2
	 	CASE DOMINATOR8
			IF eModSlot = MOD_LIVERY
				eRepReward = GET_CAR_MEET_VEHICLE_LIVERY_UNLOCK_FOR_MOD_INDEX(eVehicleModel, iModIndex)
				IF eRepReward != REP_REWARD_INVALID
					IF NOT GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(eRepReward))
					#IF IS_DEBUG_BUILD AND NOT bDebugUnlock #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CAR_CLUB_REP_LEVEL_FOR_WHEELS_UNLOCK(MOD_WHEEL_TYPE eWheelType, INT iModIndex)
	CAR_CLUB_REP_REWARDS eRepReward = GET_CAR_MEET_VEHICLE_WHEELS_UNLOCK_FOR_MOD_INDEX(eWheelType, iModIndex)
	IF eRepReward != REP_REWARD_INVALID
		RETURN GET_CAR_CLUB_REP_REWARD_TIER(eRepReward)
	ENDIF
	RETURN -1
ENDFUNC

FUNC BOOL IS_CAR_CLUB_WHEELS_LOCKED(MOD_WHEEL_TYPE eWheelType, INT iModIndex, VEHICLE_INDEX vehIndex)
	
	UNUSED_PARAMETER(vehIndex)
	
	CAR_CLUB_REP_REWARDS eRepReward

	IF eWheelType = MWT_SUPERMOD5
		#IF FEATURE_GEN9_EXCLUSIVE
		IF IS_VEHICLE_A_HSW_VEHICLE(vehIndex)
			RETURN FALSE
		ENDIF
		#ENDIF

		eRepReward = GET_CAR_MEET_VEHICLE_WHEELS_UNLOCK_FOR_MOD_INDEX(eWheelType, iModIndex)
		IF eRepReward != REP_REWARD_INVALID
			IF NOT GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(eRepReward))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_DLC_CARMOD_ITEM_INDEX(INT iLabelHash)
	SWITCH iLabelHash
		// Business
		CASE HASH("CMOD_TYR_10")
		CASE HASH("CMOD_SMOKE_10")	RETURN 0 BREAK // Green Tire Smoke
		CASE HASH("CMOD_TYR_11") 	
		CASE HASH("CMOD_SMOKE_11") 	RETURN 1 BREAK // Purple Tire Smoke
		
		CASE HASH("HORN_CLAS1") 	
		CASE HASH("CMOD_SMOKE_4")	RETURN 2 BREAK // Classical Horn 1
		CASE HASH("HORN_CLAS2") 	
		CASE HASH("CMOD_SMOKE_5")	RETURN 3 BREAK // Classical Horn 2
		CASE HASH("HORN_CLAS3") 	
		CASE HASH("CMOD_SMOKE_6")	RETURN 4 BREAK // Classical Horn 3
		CASE HASH("HORN_CLAS4") 	
		CASE HASH("CMOD_SMOKE_7")	RETURN 5 BREAK // Classical Horn 4
		CASE HASH("HORN_CLAS5") 	RETURN 6 BREAK // Classical Horn 5
		
		// Business 2
		CASE HASH("HORN_CLAS6") 	
		CASE HASH("CMOD_SMOKE_3")	RETURN 7 BREAK // Classical Horn 6
		CASE HASH("HORN_CLAS7") 	
		CASE HASH("CMOD_SMOKE_8")	RETURN 8 BREAK // Classical Horn 7
		CASE HASH("HORN_CNOTE_C0") 	RETURN 9 BREAK // Scale - Do
		CASE HASH("HORN_CNOTE_D0") 	RETURN 10 BREAK // Scale - Re
		CASE HASH("HORN_CNOTE_E0") 	RETURN 11 BREAK // Scale - Mi
		CASE HASH("HORN_CNOTE_F0") 	RETURN 12 BREAK // Scale - Fa
		CASE HASH("HORN_CNOTE_G0") 	RETURN 13 BREAK // Scale - Sol
		CASE HASH("HORN_CNOTE_A0") 	RETURN 14 BREAK // Scale - La
		CASE HASH("HORN_CNOTE_B0") 	RETURN 15 BREAK // Scale - Ti
		CASE HASH("HORN_CNOTE_C1") 	RETURN 16 BREAK // Scale - Do (high)
		
		// Hipster
		CASE HASH("CMOD_WIN_5") 	RETURN 17 BREAK // Pure Black
		CASE HASH("CMOD_TYR_12") 	
		CASE HASH("CMOD_SMOKE_12") 	RETURN 18 BREAK // Pink Tire Smoke
		CASE HASH("CMOD_TYR_13") 	
		CASE HASH("CMOD_SMOKE_13") 	RETURN 19 BREAK // Brown Tire Smoke
		CASE HASH("HORN_HIPS1") 	RETURN 20 BREAK // Jazz Horn 1
		CASE HASH("HORN_HIPS2") 	RETURN 22 BREAK // Jazz Horn 2
		CASE HASH("HORN_HIPS3") 	RETURN 22 BREAK // Jazz Horn 3
		CASE HASH("HORN_HIPS4") 	RETURN 23 BREAK // Jazz Horn 4
		
		// Independence
		CASE HASH("HORN_INDI_1")	RETURN 24 BREAK // Star Spangled Banner 1
		CASE HASH("HORN_INDI_2")	RETURN 25 BREAK // Star Spangled Banner 2
		CASE HASH("HORN_INDI_3")	RETURN 26 BREAK // Star Spangled Banner 3
		CASE HASH("HORN_INDI_4")	RETURN 27 BREAK // Star Spangled Banner 4
		CASE HASH("CMOD_TYR_PAT")	RETURN 28 BREAK // Patriot Tire Smoke
		
		// Luxe
		CASE HASH("HORN_LUXE1")	RETURN 29 BREAK // Classical Horn 8
		CASE HASH("HORN_LUXE2")	RETURN 30 BREAK // Classical Horn Loop 1
		CASE HASH("HORN_LUXE3")	RETURN 31 BREAK // Classical Horn Loop 1
		
		// Lowrider
		CASE HASH("HORN_LOWRDER1")	RETURN 32 BREAK // San Andreas Loop
		CASE HASH("HORN_LOWRDER2")	RETURN 33 BREAK // Liberty City Loop
		
		// Halloween
		CASE HASH("HORN_HWEEN1")	RETURN 34 BREAK // Halloween 1 Loop
		CASE HASH("HORN_HWEEN2")	RETURN 35 BREAK // Halloween 2 Loop
		
		// Xmas 2015
		CASE HASH("HORN_XM15_1")	RETURN 36 BREAK // Festive Loop 1
		CASE HASH("HORN_XM15_2")	RETURN 37 BREAK // Festive Loop 2
		CASE HASH("HORN_XM15_3")	RETURN 38 BREAK // Festive Loop 3
		// !!!! Be sure to keep MAX_DLC_CARMOD_ITEMS up to date.
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_DLC_SUPERMOD_ITEM_INDEX(MODEL_NAMES eModel, INT iOption, INT iControl, CARMOD_MENU_ENUM eMenu)
	
	UNUSED_PARAMETER(iOption)
	UNUSED_PARAMETER(iControl)
	INT iOffset = 0
	
	IF eModel = BTYPE2
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_SUPERMOD_LIVERY  AND iOption >= 1 AND iOption <= 4) RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE
		iOffset += (46+5)
	ENDIF
	
	IF eModel = LURCHER
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_SUPERMOD_LIVERY  AND iOption >= 1 AND iOption <= 2) RETURN iOffset+iOption ENDIF iOffset += 3
	ELSE
		iOffset += (46+3)
	ENDIF
	
	IF eModel = FACTION2
		IF (eMenu = CMM_SUPERMOD_STEERING AND iOption >= 1 AND iOption <= 16) RETURN iOffset+iOption ENDIF					iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iOption >= 1 AND iOption <= 15) RETURN iOffset+iOption ENDIF						iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iOption >= 1 AND iOption <= 22) RETURN iOffset+iOption ENDIF					iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iOption >= 1 AND iOption <= 2) RETURN iOffset+iOption ENDIF						iOffset += 3
		
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 5) RETURN iOffset+iOption ENDIF				iOffset += 6
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 2) AND iOption >= 0 AND iOption <= 67) RETURN iOffset+iOption ENDIF				iOffset += 68
		IF (eMenu = CMM_DIALS AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 14) RETURN iOffset+iOption ENDIF				iOffset += 15
		IF (eMenu = CMM_LIGHT_COLOUR AND iOption >= 0 AND iOption <= 48) RETURN iOffset+iOption ENDIF				iOffset += 49
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iOption >= 2 AND iOption <= 4)	RETURN iOffset+iOption ENDIF	iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF					iOffset += 10
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF					iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF				iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF				iOffset += 6
	ELSE
		iOffset += (17+16+23+3+46+6+68+15+49+5+10+4+6)
	ENDIF
	
	IF eModel = BUCCANEER2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF	iOffset += 3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF	iOffset += 5
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iOption >= 2 AND iOption <= 4)	RETURN iOffset+iOption ENDIF		iOffset += 5
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF				iOffset += 13
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 2) AND iOption >= 1 AND iOption <= 67) RETURN iOffset+iOption ENDIF					iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF				iOffset += 15
		IF (eMenu = CMM_LIGHT_COLOUR AND iOption >= 0 AND iOption <= 48) RETURN iOffset+iOption ENDIF				iOffset += 49
		IF (eMenu = CMM_SUPERMOD_STEERING AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF		iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF	iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF		iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF	iOffset += 11
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF	iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF		iOffset += 8
	ELSE
		iOffset += (3+4+5+5+13+68+46+15+49+17+16+23+5+11+14+17+8)
	ENDIF
	IF eModel = CHINO2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF	iOffset += 3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF	iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF		iOffset += 6
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF				iOffset += 13
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 2) AND iOption >= 1 AND iOption <= 67) RETURN iOffset+iOption ENDIF					iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF				iOffset += 15
		IF (eMenu = CMM_LIGHT_COLOUR AND iOption >= 0 AND iOption <= 48) RETURN iOffset+iOption ENDIF				iOffset += 49
		IF (eMenu = CMM_SUPERMOD_STEERING AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF		iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF	iOffset += 23
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF		iOffset += 10
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF	iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF		iOffset += 9
	ELSE
		iOffset += (3+5+4+6+13+68+46+15+49+17+16+23+10+14+17+9)
	ENDIF
	IF eModel = MOONBEAM2
		IF (eMenu = CMM_BODYWORK AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF			iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF	iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF		iOffset += 5
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF				iOffset += 3
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 2) AND iOption >= 1 AND iOption <= 67) RETURN iOffset+iOption ENDIF					iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF				iOffset += 15
		IF (eMenu = CMM_LIGHT_COLOUR AND iOption >= 0 AND iOption <= 48) RETURN iOffset+iOption ENDIF				iOffset += 49
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF	iOffset += 6
		IF (eMenu = CMM_SUPERMOD_SEATS AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF		iOffset += 2
		IF (eMenu = CMM_SUPERMOD_STEERING AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF		iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF	iOffset += 23
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF		iOffset += 10
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF	iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF		iOffset += 6
	ELSE
		iOffset += (2+5+4+5+3+68+46+15+49+6+2+17+16+23+10+14+17+6)
	ENDIF
	IF eModel = PRIMO2
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF	iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF	iOffset += 7
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF	iOffset += 6
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF		iOffset += 5
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF				iOffset += 5
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 2) AND iOption >= 1 AND iOption <= 67) RETURN iOffset+iOption ENDIF					iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_LIGHT_COLOUR AND iOption >= 0 AND iOption <= 48) RETURN iOffset+iOption ENDIF				iOffset += 49
		IF (eMenu = CMM_SUPERMOD_STEERING AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF		iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF	iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF		iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF		iOffset += 10
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF	iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF		iOffset += 8
	ELSE
		iOffset += (5+4+4+7+6+5+5+68+46+49+17+16+23+5+10+14+17+8)
	ENDIF
	IF eModel = VOODOO
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF	iOffset += 9
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_BODYWORK AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF			iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF	iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF	iOffset += 4
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF		iOffset += 6
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF				iOffset += 13
		IF (eMenu = CMM_TRIM AND (iControl = 0 OR iControl = 2) AND iOption >= 1 AND iOption <= 67) RETURN iOffset+iOption ENDIF					iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF				iOffset += 9
		IF (eMenu = CMM_LIGHT_COLOUR AND iOption >= 0 AND iOption <= 48) RETURN iOffset+iOption ENDIF				iOffset += 49
		IF (eMenu = CMM_SUPERMOD_STEERING AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF		iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF	iOffset += 23
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF		iOffset += 10
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF	iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF	iOffset += 17
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF		iOffset += 6
	ELSE
		iOffset += (4+4+9+4+2+5+4+6+13+68+46+9+49+17+16+23+10+14+17+6)
	ENDIF
	
	// Supermod options
	IF eModel = FACTION
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = BUCCANEER
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = CHINO
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = MOONBEAM
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = PRIMO
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = VOODOO2
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = SULTAN
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = BANSHEE
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
	ELSE
		iOffset += (1 + 16)
	ENDIF
	
	IF eModel = BANSHEE2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0 AND iOption >= 1 AND iOption <= 11)	RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_SUPERMOD_INTERIOR2)
			IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
				IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
			ELSE
				// option 22 = BOBBLE_BRD1
				IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
			ENDIF
		ENDIF
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 8
	ELSE
		iOffset += (5+12+46+6+5+15+17+8)
	ENDIF
	
	IF eModel = SULTANRS
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 1 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF (eMenu = CMM_SUPERMOD_INTERIOR2)
			IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
				IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
			ELSE
				// option 22 = BOBBLE_BRD1
				IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
			ENDIF
		ENDIF
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += (7+3+3+4+5+9+13+2+68+46+6+7+8+14+17+9)
	ENDIF
	
	IF eModel = BTYPE3
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0 AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_DEFLECTOR AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ORNAMENTS AND iControl = 0 AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE
		iOffset += (2+2+4+3+3+2)
	ENDIF
	
	// Supermod options
	IF eModel = FACTION
		IF (eMenu = CMM_SUPERMOD AND iOption = 1) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = SABREGT
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = TORNADO
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = TORNADO2
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = TORNADO3
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = VIRGO3
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = MINIVAN
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	IF eModel = SLAMVAN
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (1)
	ENDIF
	
	IF eModel = FACTION3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF (eMenu = CMM_SUPERMOD_INTERIOR2)
			IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
				IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
			ELSE
				// option 22 = BOBBLE_BRD1
				IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
			ENDIF
		ENDIF
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0 AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0 AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 0 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
	ELSE
		iOffset += (5+6+6+68+46+15+17+16+23+3+10+8+49)
	ENDIF
	
	IF eModel = MINIVAN2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0 AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 0 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
	ELSE
		iOffset += (5+4+5+5+68+46+15+6+2+17+16+23+10+49+10)
	ENDIF
	
	IF eModel = SABREGT2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND (iControl = 0 OR iControl = 1) AND iOption >= 1 AND iOption <= 14) RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0 AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 0 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iControl = 0 AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
	ELSE
		iOffset += (8+5+5+46+15+17+16+23+3+49+14+17+10+6)
	ENDIF
	
	IF eModel = SLAMVAN3
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0 AND iOption >= 1 AND iOption <= 1)	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 11)	RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0 AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 0 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iControl = 0 AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
	ELSE
		iOffset += (3+2+5+6+23+4+68+46+3+15+12+17+16+5+10+49+14+17)
	ENDIF
	
	IF eModel = TORNADO5
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF (eMenu = CMM_TRIM AND iControl = 3 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0 AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 0 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
	ELSE
		iOffset += (7+4+9+4+6+5+7+15+68+68+46+9+15+17+16+23+10+4+49)
	ENDIF
	
	IF eModel = VIRGO2
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF g_sMPTunables.bTURN_ON_HALLOWEEN_BOBBLEHEADS
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 45) RETURN iOffset+iOption ENDIF					iOffset += 46
		ELSE
			// option 22 = BOBBLE_BRD1
			IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iOption >= 1 AND iOption <= 21) RETURN iOffset+iOption ENDIF					iOffset += 46
		ENDIF
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0 AND iOption >= 1 AND iOption <= 22)	RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 0 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
	ELSE
		iOffset += (4+3+5+6+5+5+5+68+46+15+17+16+23+6+10+7+49)
	ENDIF
	
	IF eModel = AVARUS
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = CHIMERA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = DAEMON2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = ZOMBIEA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = ZOMBIEB
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = SANCTUS
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = WOLFSBANE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 28)	RETURN iOffset+iOption ENDIF iOffset += 29
	ELSE
		iOffset += 29
	ENDIF
	
	IF  eModel = BLAZER4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	IF eModel = YOUGA2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) 												RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE
		iOffset += (19 + 1)
	ENDIF
	
	IF eModel = FCR2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 1 AND iOption <= 48)		RETURN iOffset+iOption ENDIF iOffset += 49
	ELSE
		iOffset += (19 + 40)
	ENDIF
	
	IF eModel = DIABLOUS2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 18)	RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE
		iOffset += 19
	ENDIF
	
	
	IF eModel = NIGHTBLADE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = FAGGIO3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = FAGGIO
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = DEFILER
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = ESSKEY
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = HAKUCHOU2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = MANCHEZ
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = VORTEX
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE
		iOffset += 13
	ENDIF
	
	IF eModel = APC
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = HALFTRACK
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = DUNE3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = TAMPA3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = NIGHTSHARK
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = TRAILERSMALL2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = OPPRESSOR
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = INSURGENT3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = TECHNICAL3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = ARDENT
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = SPECTER2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_INTERIOR4 AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption = 1)	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 1 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset +=9
	ELSE
		iOffset += (10 + 5 + 9 + 2 + 15 + 17 + 49 + 9)
	ENDIF
	
	IF eModel = COMET3
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=4
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF iOffset +=8
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption = 6)	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 1 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset +=13
	ELSE
		iOffset += (4 + 3 +68 +6 + 8 + 7 +15 +17 +49 +10 + 13)
	ENDIF
	
	IF eModel = ELEGY
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset +=5
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 11)	RETURN iOffset+iOption ENDIF iOffset +=12
		IF (eMenu = CMM_TRIM AND iControl = 1 AND iOption = 1 )	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_TRIM AND iControl = 2 AND iOption >= 0 AND iOption <= 67)	RETURN iOffset+iOption ENDIF iOffset += 68
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 15)	RETURN iOffset+iOption ENDIF iOffset +=16
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption = 7)	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 14)	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 1 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset +=3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset +=7
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 4)	RETURN iOffset+iOption ENDIF iOffset +=5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset +=9
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0 AND iOption >= 1 AND iOption <= 12)	RETURN iOffset+iOption ENDIF iOffset +=13
	ELSE
		iOffset += (5 + 4 + 12 + 2 + 68 + 6 + 16 + 8 + 15 + 17 + 49 + 3 + 10 + 7 + 3 + 5 + 9 + 13)
	ENDIF
	
	IF eModel = ITALIGTB2
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=4
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_INTERIOR4 AND iControl = 0 AND iOption >= 1 AND iOption <= 7)	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset +=7
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 13)	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 1 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE
		iOffset += (4 + 6 + 8 + 7 + 14 + 17 + 49 + 10 + 7)
	ENDIF
	
	IF eModel = NERO2
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=4
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_INTERIOR4 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0 AND iOption >= 1 AND iOption <= 16)	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_LIGHT_COLOUR AND iControl = 0 AND iOption >= 1 AND iOption <= 48)	RETURN iOffset+iOption ENDIF iOffset += 49
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 9)	RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0 AND iOption = 1 )	RETURN iOffset+iOption ENDIF iOffset +=2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0 AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF iOffset +=3
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0 AND iOption >= 1 AND iOption <= 3)	RETURN iOffset+iOption ENDIF iOffset +=4
	ELSE
		iOffset += (4 + 4 + 3 + 3 + 4 + 17 + 49 + 10 + 3 + 1 + 3 + 4)
	ENDIF
	
	IF eModel = STARLING
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	IF eModel = NOKOTA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	IF eModel = ROGUE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	IF eModel = MOLOTOK
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	IF eModel = BOMBUSHKA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	IF eModel = TULA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	IF eModel = HUNTER
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	IF eModel = SEABREEZE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	IF eModel = ALPHAZ1
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	IF eModel = HAVOK
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	IF eModel = RETINUE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	IF eModel = MICROLIGHT
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	
	IF eModel = VISIONE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 8)	RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE
		iOffset += 9
	ENDIF
	
	IF eModel = COMET4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 10)	RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += 11
	ENDIF
	
	IF eModel = BARRAGE
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = AVENGER
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0 AND iOption >= 1 AND iOption <= 20)	RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE
		iOffset += 21
	ENDIF
	
	IF eModel = GB200
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 4 + 5 + 4 + 2 + 2 + 6 + 4 + 11 + 2 + 2 + 11) 
	ENDIF 
	
	IF eModel = FAGALOA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 5 + 7 + 3 + 10 + 3 + 2 + 6 + 4 + 7 + 7 + 11) 
	ENDIF 	
	
	IF eModel = MICHELLI
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		ELSE 
		iOffset += ( 5 + 8 + 3 + 4 + 7 + 3 + 6 + 2 + 4 + 5 + 11) 
	ENDIF 
	
	IF eModel = ELLIE
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 3 + 8 + 5 + 11) 
	ENDIF
	
	IF eModel = ISSI3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 9 ) 			RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 10 + 6 + 9 + 10 + 7 + 10 + 14 + 13 + 7 + 7 + 11  ) 
	ENDIF
	
	IF eModel = TEZERACT
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1 AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		ELSE 
		iOffset += ( 7 + 10 + 5 + 5 + 5 + 11 ) 
	ENDIF 
	
	IF eModel = FLASHGT
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 7 + 7 + 5 + 7 + 6 + 4 + 11 ) 
	ENDIF 
	
	IF eModel = CARACARA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 21 ) 
	ENDIF 
	
	IF eModel = CHEBUREK
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 9 + 5 + 3 + 3 + 9 + 3 + 9 + 11) 
	ENDIF 
	
	IF eModel = DOMINATOR3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 				RETURN iOffset+iOption ENDIF iOffset += 18
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 19 ) 				RETURN iOffset+iOption ENDIF iOffset += 20
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
	ELSE 
		iOffset += ( 18 + 5 + 4 + 15 + 11 + 11 + 17 + 2 + 6 + 20 + 6 ) 
	ENDIF 
	
	IF eModel = TYRANT
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE 
		iOffset += ( 4 + 3 + 3 + 2 ) 
	ENDIF 
	
	IF eModel = TAIPAN
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 6 + 5 + 4 + 6 + 7 + 2 + 7 + 3 + 7 ) 
	ENDIF 
	
	IF eModel = ENTITY2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
	ELSE 
		iOffset += ( 6 + 8 + 4 + 4 + 4 ) 
	ENDIF 
	
	IF eModel = HOTRING
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 31 ) 				RETURN iOffset+iOption ENDIF iOffset += 32
	ELSE 
		iOffset += ( 32 ) 
	ENDIF 
	
	IF eModel = JESTER3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 12 ) 			RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 11 + 13 + 5 + 3 + 5 + 5 + 6 + 11 + 3 + 4 + 11 ) 
	ENDIF 
	
	IF eModel = STAFFORD
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
	ELSE 
		iOffset += ( 8 ) 
	ENDIF
	
	IF eModel = SPEEDO4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 4 + 2 + 4 + 21 ) 
	ENDIF 
	
	IF eModel = MULE4
		IF (eMenu = CMM_SPOILER AND iControl =  0   AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 3 + 4 + 2 + 21 + 2) 
	ENDIF 
	
	IF eModel = POUNDER2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 2 + 2 + 4 + 2 + 4 + 21 ) 
	ENDIF
	
	IF eModel = SWINGER
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 		RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 3 + 5 + 5 + 12 + 4 + 10 + 5 + 11) 
	ENDIF  
	
	IF eModel = PATRIOT2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 3 + 3 + 4 + 9 + 3 + 21 ) 
	ENDIF  
	
	IF eModel = OPPRESSOR2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 2 + 13 + 3 + 21) 
	ENDIF 
	
	IF eModel = MENACER
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 14 ) 			RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 21 ) 				RETURN iOffset+iOption ENDIF iOffset += 22
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 15 + 9 + 17 + 13 + 3 + 10 + 22 + 5 + 21) 
	ENDIF 		
	
	IF eModel = FREECRAWLER
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE 
		iOffset += ( 4 + 4 + 12 + 4 + 5 + 10 + 8 + 2 + 3 + 9) 
	ENDIF 				
	
	IF eModel = SCRAMJET
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 11 ) 
	ENDIF 				
	
	IF eModel = STRIKEFORCE
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 		RETURN iOffset+iOption ENDIF iOffset += 12
	ELSE 
		iOffset += ( 13 + 5 + 12) 
	ENDIF			
	
	IF eModel = PATRIOT
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 21 ) 
	ENDIF  
		
	IF eModel = PRAIRIE	
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE
		iOffset += ( 11 ) 
	ENDIF
	
	IF eModel = CLIQUE
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 2 + 5 + 5 + 2 + 8 + 3 + 3 + 4 + 11 ) 
	ENDIF 
	
	IF eModel = DEVESTE
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 5 + 6 + 9 + 3 + 4 + 11 ) 
	ENDIF
	
	IF eModel = DEVIANT
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 11 + 9 + 7 + 4 + 12 + 8 + 4 + 11 ) 
	ENDIF 
	
	IF eModel = IMPALER
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 4 + 3 + 5 + 5 + 3 + 8 + 11 ) 
	ENDIF 
	
	IF eModel = SCHLAGEN
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 9 ) 			RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 11 + 10 + 7 + 7 + 5 + 5 + 6 + 11 + 4 + 3 + 6 + 11 ) 
	ENDIF  
	
	IF eModel = TOROS
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 9 + 5 + 7 + 13 + 15 + 15 + 2 + 9 + 11 ) 
	ENDIF 
	
	IF eModel = VAMOS
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 9 + 7 + 4 + 7 + 3 + 7 + 11 + 4 + 2 + 13 ) 
	ENDIF 

	
	IF eModel = TULIP
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 7 + 8 + 2 + 5 + 12 + 11 ) 
	ENDIF 
	
	IF eModel = IMPALER2
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_WHEELIEBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 3 + 4 + 2 + 2 + 4 + 3 + 4 + 6 + 2 + 9 + 2 + 2 + 5 + 4 + 4 + 2 + 3 + 5 + 9 ) 
	ENDIF 
	
	IF eModel = ITALIGTO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 9 ) 			RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 		RETURN iOffset+iOption ENDIF iOffset += 12
	ELSE 
		iOffset += ( 9 + 10 + 5 + 7 + 6 + 4 + 11 + 4 + 9 + 12 ) 
	ENDIF 

	IF eModel = DOMINATOR4
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 2 + 2 + 6 + 4 + 7 + 4 + 6 + 5 + 8 + 5 + 4 + 4 + 2 + 3 + 5  ) 
	ENDIF  
	
	IF eModel = SCARAB
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 9 + 12 + 5 + 4 + 3 + 6 + 8 + 7 + 8 + 5 + 4 + 7 + 4 + 3 + 5  ) 
	ENDIF 
	
	IF eModel = CERBERUS
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 12 + 4 + 8 + 6 + 6 + 2 + 11 + 5 + 4 + 7 + 2 + 5 ) 
	ENDIF 
	
	IF eModel = CERBERUS2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 4 + 8 + 4 + 8 + 6 + 6 + 2 + 5 + 5 + 4 + 7 + 2 + 19 ) 
	ENDIF 
		
	IF eModel = BRUISER
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 		RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 2 + 4 + 4 + 14 + 2 + 6 + 4 + 4 + 5 + 7 + 7 + 2 + 3 + 5 ) 
	ENDIF 
 
	
	IF eModel = MONSTER3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
	ELSE 
		iOffset += ( 4 + 4 + 3 + 4 + 4 + 4 + 7 + 4 + 6 + 3 + 3) 
	ENDIF 
	
	IF eModel = SLAMVAN4
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 		RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 2 + 4 + 4 + 5 + 6 + 6 + 4 + 5 + 4 + 7 + 2 + 2 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = IMPERATOR
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE 
		iOffset += ( 2 + 7 + 3 + 8 + 8 + 8 + 4 + 11 + 5 + 2 + 2 + 4 + 4 + 3 + 6 + 2) 
	ENDIF 
	
	IF eModel = ZR380
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 7 + 2 + 2 + 3 + 4 + 5 + 4 + 6 + 4 + 5 + 4 + 5 + 2 + 2 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = ISSI4
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 		RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 	RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE 
		iOffset += ( 4 + 4 + 3 + 5 + 4 + 5 + 13 + 4 + 17 + 16 + 2 + 5 + 6 + 4 + 2 + 2 + 4 + 2 + 12 + 9) 
	ENDIF 
	
	IF eModel = DEATHBIKE
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 4 + 4 + 2 + 2 + 2 + 5 ) 
	ENDIF 
	
	IF eModel = BRUTUS
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 3 + 4 + 4 + 3 + 6 + 4 + 4 + 4 + 3 + 6 + 3 + 5 + 4 + 8 + 2 + 2 + 5 ) 
	ENDIF 
	
	IF eModel = ZR3802
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 8 + 3 + 2 + 4 + 4 + 4 + 4 + 5 + 6 + 2 + 4 + 5 + 4 + 5 + 2 + 2 + 3 + 19 ) 
	ENDIF 
	
	IF eModel = DOMINATOR5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 2 + 2 + 6 + 4 + 7 + 6 + 5 + 5 + 4 + 4 + 2 + 3 + 19 ) 
	ENDIF 

	IF eModel = DEATHBIKE2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 7 + 4 + 4 + 2 + 2 + 2 + 19 ) 
	ENDIF 
	
	IF eModel = ISSI6
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 	RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 4 + 3 + 5 + 4 + 5 + 13 + 4 + 6 + 2 + 12 + 5 + 9 + 5 + 4 + 4 + 2 + 2 + 5 ) 
	ENDIF 
		
	IF eModel = ZR3803
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 7 + 2 + 2 + 3 + 4 + 5 + 4 + 6 + 4 + 5 + 4 + 5 + 2 + 2 + 3 + 5 ) 
	ENDIF 

	IF eModel = DOMINATOR6
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 2 + 2 + 6 + 4 + 7 + 4 + 6 + 5 + 8 + 5 + 4 + 4 + 2 + 3 + 5 ) 
	ENDIF
	
	IF eModel = DEATHBIKE3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE 
		iOffset += ( 4 + 4 + 2 + 2 + 2 ) 
	ENDIF 
	
	IF eModel = RCBANDITO
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 				RETURN iOffset+iOption ENDIF iOffset += 21
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE 
		iOffset += ( 21 + 2 + 3 + 4 + 9 ) 
	ENDIF  
	
	IF eModel = MONSTER5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 		RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 3 + 3 + 4 + 4 + 6 + 7 + 5 + 4 + 7 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = SCARAB2
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 11 + 5 + 17 + 4 + 4 + 5 + 6 + 8 + 7 + 5 + 4 + 7 + 3 + 3 + 19 ) 
	ENDIF 
	
	IF eModel = IMPALER3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 4 + 4 + 2 + 4 + 4 + 4 + 4 + 2 + 3 + 19 ) 
	ENDIF 
	
	IF eModel = IMPALER4
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
	ELSE 
		iOffset += ( 3 + 4 + 3 + 2 + 4 + 3 + 4 + 7 + 2 + 9 + 2 + 5 + 4 + 4 + 2 + 3 + 5 + 3 ) 
	ENDIF 
	
	IF eModel = BRUTUS3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 		RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 4 + 3 + 6 + 4 + 4 + 4 + 3 + 6 + 3 + 5 + 5 + 4 + 7 + 2 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = MONSTER4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 4 + 3 + 3 + 4 + 4 + 6 + 5 + 4 + 7 + 3 + 19 ) 
	ENDIF 
	
	IF eModel = BRUISER2
		IF (eMenu = CMM_SPAREWHEEL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 3 + 2 + 2 + 4 + 4 + 8 + 6 + 4 + 5 + 4 + 7 + 2 + 3 + 19 ) 
	ENDIF 
	
	IF eModel = BRUISER3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 6	 ) 		RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 4 + 2 + 2 + 4 + 4 + 14 + 2 + 6 + 4 + 5 + 4 + 7 + 2 + 3 + 5 + 7 ) 
	ENDIF 
	
	IF eModel = ISSI5
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 6 + 6 + 5 + 10 + 7 + 4 + 5 + 15 + 9 + 9 + 2 + 9 + 5 + 5 + 4 + 4 + 2 + 3 + 19 ) 
	ENDIF 
	
	IF eModel = CERBERUS3
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 12 + 4 + 8 + 6 + 6 + 2 + 11 + 5 + 4 + 7 + 2 + 5 ) 
	ENDIF 
	
	IF eModel = SLAMVAN5
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 		RETURN iOffset+iOption ENDIF iOffset += 23
	ELSE 
		iOffset += ( 2 + 4 + 4 + 5 + 5 + 5 + 4 + 7 + 2 + 2 + 3 + 23 ) 
	ENDIF 
	
	IF eModel = IMPERATOR2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 5 + 4 + 8 + 6 + 2 + 3 + 5 + 4 + 5 + 2 + 2 + 3 + 19 ) 
	ENDIF	
	
	IF eModel = IMPERATOR3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 7 + 4 + 4 + 8 + 6 + 2 + 4 + 8 + 5 + 4 + 5 + 2 + 2 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = BRUTUS2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 2 + 3 + 3 + 2 + 4 + 2 + 3 + 2 + 6 + 5 + 4 + 8 + 2 + 3 + 19 ) 
	ENDIF
	
	IF eModel = SCARAB3
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 9 + 12 + 5 + 4 + 3 + 6 + 8 + 7 + 8 + 5 + 4 + 7 + 4 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = SLAMVAN6
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 		RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 	RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 		RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 2 + 4 + 4 + 5 + 6 + 6 + 4 + 5 + 4 + 7 + 2 + 2 + 3 + 5 ) 
	ENDIF 
	
	IF eModel = CARACARA2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 8 + 6 + 3 + 4 + 9 + 4 + 3 + 11 ) 
	ENDIF 
	
	IF eModel = DRAFTER
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 					RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 4 + 4 + 4 + 7 + 4 + 5 + 10 + 2 + 4 + 7 + 11 ) 
	ENDIF 
	
	IF eModel = DYNASTY
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 6 + 7 + 7 + 9 + 5 + 7 + 7 + 7 + 7 + 8 + 11 ) 
	ENDIF 
	
	IF eModel = GAUNTLET3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) 													RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE 
		iOffset += ( 9 + 5 + 4 + 8 + 6 + 7 + 6 + 16 + 2 + 3 + 9 + 11 + 1) 
	ENDIF 
	
	IF eModel = GAUNTLET4
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 11 + 7 + 2 + 3 + 6 + 7 + 7 + 11 + 7 + 10 + 2 + 13 ) 
	ENDIF 
	
	IF eModel = HELLION
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 16 ) 			RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 4 + 17 + 11 + 10 + 10 + 5 + 11 + 10 + 5 + 11 ) 
	ENDIF 		
	
	IF eModel = ISSI7
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 6 + 5 + 3 + 5 + 8 + 4 + 11 + 4 + 2 + 6 + 11 ) 
	ENDIF 		
	
	IF eModel = KRIEGER
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 13 + 2 + 6 + 5 + 11 ) 
	ENDIF 
	
	IF eModel = LOCUST
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 			RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 9 + 7 + 7 + 13 + 7 + 6 + 11 ) 
	ENDIF 

	IF eModel = NEBULA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 8 + 9 + 2 + 2 + 4 + 2 + 4 + 8 + 2 + 3 + 6 + 11 ) 
	ENDIF 
			
	IF eModel = NEO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 9 + 4 + 5 + 14 + 13 + 11 + 3 + 7 + 11 ) 
	ENDIF 
				
	
	IF eModel = NOVAK
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 6 + 5 + 5 + 5 + 9 + 3 + 11 ) 
	ENDIF 		
	
	IF eModel = S80
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 8 + 5 + 4 + 2 + 2 + 11 ) 
	ENDIF 
	
	IF eModel = THRAX
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 5 + 4 + 6 + 11 + 4 + 9 + 16 + 4 + 11 ) 
	ENDIF 
		
	IF eModel = ZION3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 8 + 4 + 11 + 6 + 4 + 3 + 9 + 4 + 3 + 6 + 11 ) 
	ENDIF 
		
	IF eModel = ZORRUSSO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 6 + 5 + 7 + 3 + 4 + 2 + 11 ) 
	ENDIF 
	
	IF eModel = EMERUS
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 23 ) 				RETURN iOffset+iOption ENDIF iOffset += 24
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 24 + 13 + 6 + 7 + 3 + 8 + 11 ) 
	ENDIF 			
	
	IF eModel = PEYOTE
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE 
		iOffset += ( 1 ) 
	ENDIF 	
	
	IF eModel = PEYOTE2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 				RETURN iOffset+iOption ENDIF iOffset += 18
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
	ELSE 
		iOffset += ( 12 + 6 + 6 + 18 + 11 + 4 ) 
	ENDIF 		
	
	IF eModel = RROCKET
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 5 + 11 ) 
	ENDIF 
	
	IF eModel = JUGULAR
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 11 + 4 + 3 + 5 + 3 + 4 + 8 + 3 + 2 + 11 ) 
	ENDIF 
	
	IF eModel = PARAGON
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 5 + 9 + 4 + 4 + 6 + 7 + 6 + 11 + 3 + 3 + 13  ) 
	ENDIF 
	
	IF eModel = PARAGON2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 5 + 9 + 4 + 4 + 6 + 7 + 6 + 11 + 3 + 3 + 13 ) 
	ENDIF 			
	
	IF eModel = REBLA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 9 + 4 + 9 + 8 + 6 + 10 + 14 + 8 + 3 + 3 + 11 ) 
	ENDIF				
						
	IF eModel = IMORGON
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 20 ) 				RETURN iOffset+iOption ENDIF iOffset += 21
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 24 ) 				RETURN iOffset+iOption ENDIF iOffset += 25
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 			RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 15 + 9 + 21 + 3 + 6 + 25 + 3 + 8 + 16 + 3 + 3 + 13) 
	ENDIF 
	
	IF eModel = ASBO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 16 + 7 + 13 + 9 + 5 + 3 + 12 + 5 + 5 + 7 + 11 ) 
	ENDIF 
	
	IF eModel = SUGOI
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 14 ) 			RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 15 + 15 + 8 + 10 + 8 + 8 + 13 + 5 + 7 + 11 ) 
	ENDIF 
	
	IF eModel = KOMODA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 24 ) 				RETURN iOffset+iOption ENDIF iOffset += 25
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 12 ) 			RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 				RETURN iOffset+iOption ENDIF iOffset += 19
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 				RETURN iOffset+iOption ENDIF iOffset += 18
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 25 + 13 + 5 + 19 + 5 + 18 + 4 + 3 + 12 + 11 ) 
	ENDIF  					
	
	IF eModel = ZHABA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 30 ) 		RETURN iOffset+iOption ENDIF iOffset += 31
	ELSE 
		iOffset += ( 7 + 4 + 4 + 7 + 10 + 4 + 7 + 3 + 3 + 7 + 31 ) 
	ENDIF  					
	
	IF eModel = VSTR
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 				RETURN iOffset+iOption ENDIF iOffset += 18
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 7 + 5 + 18 + 4 + 2 + 14 + 3 + 6 + 3 + 11 ) 
	ENDIF 
	
	IF eModel = EVERON
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 10 ) 			RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 				RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 				RETURN iOffset+iOption ENDIF iOffset += 18
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 2 + 11 + 3 + 3 + 23 + 2 + 8 + 18 + 11 ) 
	ENDIF 	
	
	IF eModel = SULTAN2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) 													RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE 
		iOffset += ( 17 + 6 + 5 + 5 + 10 + 10 + 5 + 16 + 4 + 8 + 4 + 11 + 1 ) 
	ENDIF 
	
	IF eModel = KANJO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 				RETURN iOffset+iOption ENDIF iOffset += 19
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 12 ) 			RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 		RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 		RETURN iOffset+iOption ENDIF iOffset += 14
	ELSE 
		iOffset += ( 19 + 13 + 14 + 8 + 12 + 7 + 8 + 16 + 10 + 5 + 7 + 3 + 6 + 4 + 143 ) 
	ENDIF 	

	IF eModel = OUTLAW
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 21 ) 
	ENDIF 
	
	IF eModel = FORMULA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 13 + 7 + 8 + 2 + 6 + 11 ) 
	ENDIF 						
	
	IF eModel = STRYDER
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 6 + 6 + 11 ) 
	ENDIF 							
	
	IF eModel = JB7002
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE 
		iOffset += ( 3 + 2 ) 
	ENDIF 
	
	IF eModel = VAGRANT
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 30 ) 		RETURN iOffset+iOption ENDIF iOffset += 31
	ELSE 
		iOffset += ( 9 + 3 + 3 + 3 + 4 + 4 + 3 + 31 ) 
	ENDIF 						
	
	IF eModel = RETINUE2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 7 + 7 + 2 + 6 + 5 + 9 + 8 + 3 + 6 + 11 ) 
	ENDIF 
	
	IF eModel = YOSEMITE2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 11 ) 			RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_TRUCKBED AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 			RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 12 + 2 + 12 + 2 + 4 + 5 + 6 + 2 + 5 + 11 ) 
	ENDIF 						
	
	IF eModel = FURIA
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 9 + 7 + 15 + 2 + 10 + 15 + 3 + 3 + 11 ) 
	ENDIF 
	
	IF eModel = FORMULA2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 10 + 8 + 6 + 4 + 11 ) 
	ENDIF 						
	
	IF eModel = MINITANK
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 4 + 11 ) 
	ENDIF 
	
	#IF FEATURE_COPS_N_CROOKS
	IF eModel = SURFER3
		IF (eMenu = CMM_HANDLEBARS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 21 ) 				RETURN iOffset+iOption ENDIF iOffset += 22
		IF (eMenu = CMM_SPAREWHEEL AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 			RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_HEADLIGHTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 			RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 5 + 6 + 3 + 4 + 9 + 22 + 12 + 4 + 17 + 6 + 5 + 11 ) 
	ENDIF 
	#ENDIF
	
	IF eModel = GAUNTLET5
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 								RETURN iOffset+iOption ENDIF iOffset += 18
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 13 ) 							RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 								RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 								RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 								RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 								RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= (WING_R_FIRST_MENU_ITEM + 1) AND iOption <= 23 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 									RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 								RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_WHEELIEBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 							RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 								RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0  AND iOption >= 1 AND iOption <= 8 )				 	RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 								RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 						RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 								RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 								RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_VIN AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 									RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 								RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 								RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 							RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_DIALS AND iControl = 1 AND iOption >= 1 AND iOption <= 4 ) 									RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 						RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 					RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 18 + 14 + 6 + 9 + 11 + 8 + 3 + 4 + 7 + 4 + 4 + 7 + 9 + 5 + 11 + 4 + 7 + 3 + 4 + 7 + 13 + 7 + 6 + 5 + 6 + 6 + 14 + 5) 
	ENDIF 				
	
	IF eModel = MANANA2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS2 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 		RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 		RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iControl = 0  AND iOption >= 1 AND iOption <= 21 ) 	RETURN iOffset+iOption ENDIF iOffset += 22
		//IF (eMenu = CMM_SUPERMOD_INTERIOR4 AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 	RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_DIALS AND iControl = 1  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 		RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
	ELSE 
		iOffset += ( 3 + 3 + 3 + 4 + 2 + 2 + 3 + 3 + 2 + 5 + 6 + 6 + 13 + 4 + 4 + 15 + 22 + 15 + 17 + 16 + 23 + 14 + 17 + 2)// + 15) 
	ENDIF 
	
	IF eModel = MANANA
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) 													RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE 
		iOffset += ( 1 ) 
	ENDIF 	
	
	IF eModel = YOSEMITE
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) 													RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE 
		iOffset += ( 1 ) 
	ENDIF 
	
	#IF FEATURE_COPS_N_CROOKS
	IF eModel = POLICE5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 		RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 27 + 8 + 8 + 7 + 5) 
	ENDIF 		
	
	IF eModel = POLBUFFALO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 		RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 3 + 8 + 8 + 7 +  6 + 6 + 5 + 3 + 6 + 2 + 5 + 5 + 27 ) 
	ENDIF 
	
	IF eModel = POLPANTO
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 24 ) 		RETURN iOffset+iOption ENDIF iOffset += 25
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 4 + 5 + 3 + 5 + 6 + 2 + 3 + 25 + 5 + 8 + 8 + 7 + 5) 
	ENDIF 							
	
	IF eModel = POLGAUNTLET
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 		RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 2 + 8 + 8 + 7 + 11 + 7 + 5 + 3 + 6 + 7 + 7 + 11 + 7 + 9 + 23 ) 
	ENDIF 								
	
	IF eModel = POLGRANGER
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 		RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 8 + 8 + 7 + 23 + 5) 
	ENDIF	
	
	IF eModel = ZOKU
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 23 ) 		RETURN iOffset+iOption ENDIF iOffset += 24
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_FUELTANK AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 24 + 5 + 13 + 14 + 6 + 11 ) 
	ENDIF
	
	IF eModel = POLICE6
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 		RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 8 + 8 + 7 + 3 + 3 + 3 + 3 + 2 + 3 + 27 + 5) 
	ENDIF	
	
	IF eModel = POLCARACARA
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 		RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 3 + 6 + 8 + 7 +  5 + 8 + 6 + 5 + 4 + 9 + 4 + 27 ) 
	ENDIF 	
	
	IF eModel = POLICET2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 		RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
	ELSE 
		iOffset += ( 8 + 8 + 4 + 27 + 5) 
	ENDIF 
	
	IF eModel = POLRIATA
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 		RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 3 + 16 + 7 + 7 + 4 + 5 + 4 + 5 + 9 + 27 ) 
	ENDIF 
	
	IF eModel = POLBATI
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE 
		iOffset += ( 8 + 5 + 4 + 2 + 2 + 21 + 9 ) 
	ENDIF 

	IF eModel = POLICEB2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 		RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
	ELSE 
		iOffset += ( 9 + 9 + 6 + 4 + 7 + 4 + 21 + 9 + 9) 
	ENDIF 	
	
	IF eModel = POLGREENWOOD
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 19 ) 		RETURN iOffset+iOption ENDIF iOffset += 20
		IF (eMenu = CMM_PUSHBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
	ELSE 
		iOffset += ( 9 + 7 + 2 + 3 + 20 + 8 ) 
	ENDIF 
	
	#ENDIF
	#IF FEATURE_SUMMER_2020
	IF eModel = DUKES3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 10 ) 			RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_WHEELIEBAR AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 26 ) 				RETURN iOffset+iOption ENDIF iOffset += 27
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 		RETURN iOffset+iOption ENDIF iOffset += 14
	ELSE 
		iOffset += ( 8 + 11 + 4 + 5 + 10 + 9 + 5 + 13 + 5 + 3 + 27 + 14 ) 
	ENDIF 
	
	IF eModel = CLUB
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 								RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 8 ) 							RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 								RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 								RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_VIN AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 									RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 								RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 									RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 								RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= (WING_R_FIRST_MENU_ITEM + 1) AND iOption <= 27 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 									RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 						RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 								RETURN iOffset+iOption ENDIF iOffset += 10
	ELSE 
		iOffset += ( 11 + 9 + 6 + 3 + 4 + 6 + 9 + 3 + 7 + 6 + 11 + 10) 
	ENDIF 

	IF eModel = YOUGA3
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SIDESTEP AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 					RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 		RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_HANDLEBARS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BULLBARS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_TRIM AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_INTERIOR3 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 	RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 		RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		//IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 9 + 5 + 7 + 6 + 7 + 9 + 10 + 17 + 5 + 4 + 9 + 6 + 5 + 4 + 7 + 2 + 5 + 7 + 16 + 6 + 8 + 2 + 17 + 2 + 7)//+ 16) 
	ENDIF 														
	
	IF eModel = PEYOTE3
		IF (eMenu = CMM_TRIM AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iControl = 0  AND iOption >= 1 AND iOption <= 21 ) 	RETURN iOffset+iOption ENDIF iOffset += 22
		IF (eMenu = CMM_DIALS AND iControl = 1  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 		RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 		RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 		RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SPAREWHEEL AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 			RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 	RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE 
		iOffset += ( 3 + 4 + 3 + 2 + 4 + 6 + 5 + 7 + 6 + 6 + 11 + 14 + 22 + 9 + 17 + 16 + 23 + 5 + 4 + 14 + 17 + 7 + 2) 
	ENDIF 								
	
	IF eModel = GLENDALE2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 		RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_HYDRO AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 		RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_TRIM AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iControl = 0  AND iOption >= 1 AND iOption <= 21 ) 	RETURN iOffset+iOption ENDIF iOffset += 22
		IF (eMenu = CMM_DIALS AND iControl = 1  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_PLAQUE AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 		RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_SUPERMOD_ICE AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_PLTHOLDER AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 	RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_SUPERMOD_PLTVANITY AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
	ELSE 
		iOffset += ( 2 + 2 + 9 + 11 + 4 + 6 + 6 + 11 + 3 + 13 + 22 + 15 + 17 + 16 + 23 + 5 + 14 + 17 + 4 + 7 ) 
	ENDIF 
	
 	IF eModel = YOSEMITE3
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 14 ) 			RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SIDESTEP AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BULLBARS AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 			RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 		RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_DOOR_R AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 		RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
		IF (eMenu = CMM_SUPERMOD_INTERIOR2 AND iControl = 0  AND iOption >= 1 AND iOption <= 21 ) 	RETURN iOffset+iOption ENDIF iOffset += 22
		IF (eMenu = CMM_DIALS AND iControl = 1  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SUPERMOD_SEATS AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD_STEERING AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 	RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_KNOB AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_SUPERMOD_DOOR_L AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 		RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_TRUCKBED AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_CHASSIS3 AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_CHASSIS4 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_CHASSIS5 AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 	RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY1 AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 	RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_ENGINEBAY2 AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 	RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_INTERIOR5 AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 	RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
	ELSE 
		iOffset += ( 6 + 8 + 7 + 8 + 11 + 15 + 6 + 7 + 6 + 10 + 15 + 6 + 16 + 3 + 8 + 3 + 19 + 22 + 15 + 11 + 17 + 16 + 9 +  5 + 6 + 3 + 6) 
	ENDIF 								

	IF eModel = SEMINOLE2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 9 ) 			RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SIDESTEP AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 			RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 		RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 					RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
	ELSE 
		iOffset += ( 10 + 2 + 3 + 6 + 5 + 7 + 7 + 2 + 8 + 10 + 4 + 9 + 2) 
	ENDIF 	
	
	IF eModel = PENUMBRA2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 22 ) 				RETURN iOffset+iOption ENDIF iOffset += 23
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 10 ) 			RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 				RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_FRONTSEAT AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 			RETURN iOffset+iOption ENDIF iOffset += 16
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 23 + 11 + 9 + 16 + 10 + 16 + 17 + 15 + 5 + 3 + 7 + 13 ) 
	ENDIF 
	
	IF eModel = GLENDALE
		IF (eMenu = CMM_SUPERMOD AND iOption = 0) 													RETURN iOffset+iOption ENDIF iOffset += 1
	ELSE 
		iOffset += ( 1 ) 
	ENDIF 	
	
	IF eModel = LANDSTALKER2
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 		RETURN iOffset+iOption ENDIF iOffset += 10
	ELSE 
		iOffset += ( 6 + 4 + 9 + 8 + 7 + 8 + 3 + 7 + 10 ) 
	ENDIF 				
	
//	IF eModel = PRIMO3
//		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 19 ) 				RETURN iOffset+iOption ENDIF iOffset += 20
//		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
//		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
//		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
//		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
//		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
//		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
//		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 17 ) 				RETURN iOffset+iOption ENDIF iOffset += 18
//		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
//	ELSE 
//		iOffset += ( 20 + 4 + 4 + 9 + 4 + 2 + 12 + 18 + 11 ) 
//	ENDIF 				
	
	IF eModel = COQUETTE4
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_VIN AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 					RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 9 + 6 + 5 + 4 + 7 + 4 + 4 + 7 + 3 + 11) 
	ENDIF 				
	
	IF eModel = OPENWHEEL1
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 4 ) 			RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 7 + 5 + 5 + 11 ) 
	ENDIF 				
	
	IF eModel = OPENWHEEL2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 6 + 8 + 5 + 4 + 7 + 6 + 4 + 6 + 11 ) 
	ENDIF			
	
	IF eModel = TIGON
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 12 + 7 + 6 + 6 + 6 + 3 + 11 ) 
	ENDIF
	#ENDIF	
	
	#IF FEATURE_HEIST_ISLAND
	IF eModel = BRIOSO2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_TRUNK AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 5 + 8 + 5 + 4 + 6 + 3 + 4 + 3 + 12 + 11 ) 
	ENDIF 			
	
	IF eModel = MANCHEZ2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 		RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 5 + 3 + 5 + 6 + 5 + 21) 
	ENDIF 
	
	IF eModel = SEASPARROW2
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
	ELSE 
		iOffset += ( 3 ) 
	ENDIF 
	
	IF eModel = SLAMTRUCK
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 					RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_FENDERSTWO AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 			RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 18 ) 		RETURN iOffset+iOption ENDIF iOffset += 19
	ELSE 
		iOffset += ( 8 + 4 + 4 + 10 + 5 + 9 + 19 ) 
	ENDIF 
	
	IF eModel = ITALIRSX
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 				RETURN iOffset+iOption ENDIF iOffset += 21
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 5 ) 			RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 				RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 					RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 21 + 6 + 15 + 10 + 9 + 6 + 9 + 14 + 3 + 8 + 11 ) 
	ENDIF
	
	IF eModel = TOREADOR
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 				RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 9 ) 			RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_REARMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 					RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 11 + 10 + 10 + 11 + 3 + 13 ) 
	ENDIF 
	
	IF eModel = WINKY
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 7 ) 			RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FAIRING AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_BULLBARS AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 			RETURN iOffset+iOption ENDIF iOffset += 11
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 					RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 29 ) 		RETURN iOffset+iOption ENDIF iOffset += 30
	ELSE 
		iOffset += ( 4 + 8 + 3 + 7 + 5 + 11 + 5 + 7 + 4 + 2 + 30 ) 
	ENDIF 
	
	IF eModel = WEEVIL
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 16 ) 			RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 				RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 				RETURN iOffset+iOption ENDIF iOffset += 12
		IF (eMenu = CMM_ROLLCAGE AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 1 ) 				RETURN iOffset+iOption ENDIF iOffset += 2
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 					RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= (WING_R_FIRST_MENU_ITEM + 1) AND iOption <= 27 ) 	RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
	ELSE 
		iOffset += ( 6 + 17 + 9 + 7 + 12 + 8 + 2 + 6 + 10 + 7 + 17 + 16 ) 
	ENDIF 
	
	IF eModel = SQUADDIE
		IF (eMenu = CMM_BODYWORK AND iControl = 0  AND iOption >= 1 AND iOption <= 13 ) 			RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 13 ) 			RETURN iOffset+iOption ENDIF iOffset += 14
		IF (eMenu = CMM_BUMPERS AND iControl =  2 AND iOption >= 1 AND iOption <= 8 ) 				RETURN iOffset+iOption ENDIF iOffset += 9
		IF (eMenu = CMM_SKIRTS AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 				RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 5 ) 				RETURN iOffset+iOption ENDIF iOffset += 6
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 19 ) 				RETURN iOffset+iOption ENDIF iOffset += 20
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 14 ) 				RETURN iOffset+iOption ENDIF iOffset += 15
		IF (eMenu = CMM_MIRRORS AND iControl = 0  AND iOption >= 1 AND iOption <= 7 ) 				RETURN iOffset+iOption ENDIF iOffset += 8
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 16 ) 				RETURN iOffset+iOption ENDIF iOffset += 17
		IF (eMenu = CMM_ROOF AND iControl = 0  AND iOption >= 1 AND iOption <= 9 ) 					RETURN iOffset+iOption ENDIF iOffset += 10
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 14 + 14 + 9 + 15 + 10 + 6 + 20 + 15 + 8 + 17 + 10 + 21 )
	ENDIF
	
	IF eModel = VETO
		IF (eMenu = CMM_BUMPERS AND iControl =  1   AND iOption >= 1 AND iOption <= 6 ) 			RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 		RETURN iOffset+iOption ENDIF iOffset += 13
	ELSE 
		iOffset += ( 7 + 13 + 4 + 13 + 5 + 13 ) 
	ENDIF 
			
	IF eModel = ALKONOST
		IF (eMenu = CMM_FRONTMUDGUARD AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 		RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 10 ) 		RETURN iOffset+iOption ENDIF iOffset += 11
	ELSE 
		iOffset += ( 3 + 5 + 11 ) 
	ENDIF 
	
	IF eModel = ANNIHILATOR2
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 3 + 21 ) 
	ENDIF 
	
	IF eModel = VETO2
		IF (eMenu = CMM_SPOILER AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_GRILL AND iControl = 0  AND iOption >= 1 AND iOption <= 12 ) 				RETURN iOffset+iOption ENDIF iOffset += 13
		IF (eMenu = CMM_FENDERS AND iControl = 0  AND iOption >= 1 AND iOption <= 4 ) 				RETURN iOffset+iOption ENDIF iOffset += 5
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 30 ) 		RETURN iOffset+iOption ENDIF iOffset += 31
	ELSE 
		iOffset += ( 3 + 13 + 4 + 13 + 5 + 31 ) 
	ENDIF 
	
	IF eModel = VERUS
		IF (eMenu = CMM_SADDLEBAGS AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 			RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_EXHAUST AND iControl = 0  AND iOption >= 1 AND iOption <= 3 ) 				RETURN iOffset+iOption ENDIF iOffset += 4
		IF (eMenu = CMM_CHASSIS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_BULLBARS AND iControl = 0  AND iOption >= 1 AND iOption <= 2 ) 				RETURN iOffset+iOption ENDIF iOffset += 3
		IF (eMenu = CMM_HOOD AND iControl = 0  AND iOption >= 1 AND iOption <= 6 ) 					RETURN iOffset+iOption ENDIF iOffset += 7
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 20 ) 		RETURN iOffset+iOption ENDIF iOffset += 21
	ELSE 
		iOffset += ( 4 + 4 + 3 + 3 + 7 + 21 ) 
	ENDIF 
	#ENDIF	
	
	#IF FEATURE_TUNER	
	IF eModel = KURUMA
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 15 ) 		RETURN iOffset+iOption ENDIF iOffset += 16
	ELSE
		iOffset += ( 16 )
	ENDIF
	
	IF eModel = HERMES
		IF (eMenu = CMM_SUPERMOD_LIVERY AND iControl = 0  AND iOption >= 1 AND iOption <= 11 ) 		RETURN iOffset+iOption ENDIF iOffset += 12
	ELSE 
		iOffset += ( 12 ) 
	ENDIF 
	#ENDIF					
					
	IF iOffset > MAX_DLC_SUPERMOD_ITEMS
		PRINTLN("kr_debug: max = ", iOffset)
		SCRIPT_ASSERT("MAX_DLC_SUPERMOD_ITEMS needs to be increased. Tell Kenneth R.")
	ENDIF
	
	// !!!! Be sure to keep MAX_DLC_SUPERMOD_ITEMS up to date.
	
	RETURN 0
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_ONLY_WING_R_IN_FENDERS_MENU(MODEL_NAMES vehModel)
	SWITCH vehModel
		CASE DOMINATOR3
		CASE SPEEDO4
		CASE MULE4
		CASE POUNDER2
		CASE STRIKEFORCE
		CASE RCBANDITO
		CASE JB7002
		#IF FEATURE_HEIST_ISLAND
		CASE ALKONOST
		CASE VETO
		CASE VETO2
		CASE SQUADDIE
		#ENDIF
			RETURN TRUE
		BREAK		
		DEFAULT	
			IF IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(vehModel)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DISABLE_UNLOCK_STARS_FOR_VEHICLE(MODEL_NAMES eVehicleModel)

	SWITCH eVehicleModel
		CASE CALICO
		CASE COMET6
		CASE CYPHER
		CASE DOMINATOR7
		CASE JESTER4
		CASE REMUS
		CASE VECTRE
		CASE ZR350
		CASE WARRENER2
		CASE RT3000
		CASE FUTO2
		CASE TAILGATER2
		CASE SULTAN3
		CASE DOMINATOR8
		CASE EUROS
		CASE GROWLER
		CASE PREVION
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_LIMITED_STAR_SYSTEM(MODEL_NAMES eVehicleModel)
	SWITCH eVehicleModel
		CASE GB200
		CASE ISSI3
		CASE FAGALOA
		CASE ELLIE
		CASE MICHELLI
		CASE FLASHGT
		CASE HOTRING
		CASE TEZERACT
		CASE TYRANT
		CASE DOMINATOR3
		CASE TAIPAN
		CASE ENTITY2
		CASE JESTER3
		CASE CHEBUREK
		CASE CARACARA
		CASE SEASPARROW
		CASE MULE4
		CASE SPEEDO4
		CASE POUNDER2
		CASE SWINGER
		CASE PATRIOT2
		CASE OPPRESSOR2
		CASE MENACER
		CASE FREECRAWLER
		CASE STAFFORD
		CASE SCRAMJET
		CASE TERBYTE
		CASE STRIKEFORCE
		CASE CLIQUE
		CASE DEVESTE
		CASE DEVIANT
		CASE IMPALER
		CASE SCARAB
		CASE SCHLAGEN
		CASE TOROS
		CASE VAMOS
		CASE TULIP
		CASE MONSTER3
		CASE IMPALER2
		CASE BRUISER
		CASE CERBERUS
		CASE DOMINATOR4
		CASE IMPERATOR
		CASE ISSI4
		CASE ITALIGTO
		CASE DEATHBIKE
		CASE SLAMVAN4
		CASE BRUTUS
		CASE IMPERATOR2
		CASE IMPERATOR3
		CASE DEATHBIKE2
		CASE DEATHBIKE3
		CASE IMPALER3
		CASE BRUTUS2
		CASE BRUISER2
		CASE SLAMVAN5
		CASE ISSI5
		CASE MONSTER4
		CASE SCARAB2
		CASE CERBERUS2
		CASE DOMINATOR5
		CASE ZR3802
		CASE IMPALER4
		CASE BRUTUS3
		CASE BRUISER3
		CASE SLAMVAN6
		CASE ISSI6
		CASE MONSTER5
		CASE SCARAB3
		CASE CERBERUS3
		CASE DOMINATOR6
		CASE ZR3803
		CASE ZR380
		CASE RCBANDITO
		CASE CARACARA2 
		CASE DRAFTER 
		CASE DYNASTY 
		CASE GAUNTLET3 
		CASE GAUNTLET4 
		CASE HELLION 
		CASE ISSI7 
		CASE KRIEGER 
		CASE LOCUST 
		CASE NEBULA 
		CASE NEO 
		CASE NOVAK 
		CASE S80 
		CASE THRAX 
		CASE ZION3 
		CASE ZORRUSSO
		CASE EMERUS
		CASE PEYOTE2
		CASE RROCKET
		CASE JUGULAR
		CASE PARAGON
		CASE PARAGON2
		CASE ASBO
		CASE KANJO
		CASE FORMULA
		CASE IMORGON
		CASE KOMODA
		CASE MANANA2
		#IF FEATURE_COPS_N_CROOKS
		CASE POLBATI
		CASE POLBUFFALO
		CASE POLGAUNTLET
		CASE POLGRANGER
		CASE POLICE5
		CASE POLICE6
		CASE POLICEB2
		CASE POLICET2
		CASE POLPANTO
		CASE POLCARACARA
		CASE SURFER3
		CASE ZOKU
		CASE POLRIATA
		CASE POLGREENWOOD
		#ENDIF
		CASE GAUNTLET5
		CASE YOSEMITE3
		CASE PEYOTE3
		CASE CLUB
		CASE GLENDALE2
		CASE PENUMBRA2
		CASE LANDSTALKER2
		CASE SEMINOLE2
		CASE TIGON	
		CASE OPENWHEEL1	
		CASE OPENWHEEL2
		CASE COQUETTE4
		CASE DUKES3	
//		CASE PRIMO3
		CASE REBLA
		CASE SUGOI
		CASE YOUGA3
		CASE ZHABA
		CASE VSTR
		CASE EVERON
		CASE SULTAN2
		CASE JB7002
		CASE OUTLAW
		CASE STRYDER
		CASE FIRETRUK
		CASE BURRITO2
		CASE BOXVILLE
		CASE STOCKADE
		CASE LGUARD 
		CASE BLAZER2
		CASE VAGRANT
		CASE FORMULA2
		CASE FURIA
		CASE YOSEMITE2
		CASE RETINUE2
		CASE MINITANK
		#IF FEATURE_HEIST_ISLAND
		CASE MANCHEZ2
		CASE BRIOSO2
		CASE WINKY
		CASE VERUS
		CASE ALKONOST		
		CASE AVISA			
		CASE LONGFIN		
		CASE PATROLBOAT	
		CASE SEASPARROW2	
		CASE SEASPARROW3	
		CASE SLAMTRUCK		
		CASE VETIR			
		CASE KOSATKA
		CASE ITALIRSX
		CASE TOREADOR
		CASE WEEVIL	
		CASE DINGHY5
		CASE ANNIHILATOR2
		CASE SQUADDIE
		CASE VETO
		CASE VETO2
		#ENDIF
		#IF FEATURE_TUNER
		CASE CALICO
		CASE COMET6
		CASE CYPHER
		CASE DOMINATOR7
		CASE JESTER4
		CASE REMUS
		CASE VECTRE
		CASE ZR350
		CASE WARRENER2
		CASE RT3000
		CASE FUTO2	
		CASE TAILGATER2	
		CASE SULTAN3
		CASE DOMINATOR8
		CASE EUROS		
		CASE GROWLER	
		CASE PREVION
		#ENDIF
		CASE ASTRON
		CASE BALLER7
		CASE BUFFALO4
		CASE COMET7
		CASE DEITY
		CASE GRANGER2
		CASE IGNUS
		CASE JUBILEE
		CASE PATRIOT3
		CASE CHAMPION
		CASE YOUGA4
		CASE ZENO
		CASE CINQUEMILA
		CASE REEVER
		CASE SHINOBI
		CASE IWAGEN
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	#IF FEATURE_DLC_1_2022
	SWITCH eVehicleModel
		CASE BRIOSO3
		CASE CORSITA
		CASE DRAUGUR
		CASE GREENWOOD
		CASE KANJOSJ
		CASE LM87
		CASE POSTLUDE
		CASE RHINEHART
		CASE SM722
		CASE TENF
		CASE TENF2
		CASE TORERO2
		CASE VIGERO2
		CASE WEEVIL2
		CASE RUINER4
		CASE SENTINEL4
		CASE OMNISEGT
			RETURN TRUE
		BREAK
	ENDSWITCH
	#ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SWITCH eVehicleModel
		CASE ARBITERGT
		CASE IGNUS2
		CASE ASTRON2
		CASE CYCLONE2
		CASE S95
			RETURN TRUE
		BREAK	
	ENDSWITCH 
	#ENDIF
	RETURN FALSE
ENDFUNC 

FUNC BOOL DOES_VEHICLE_HAVE_TWO_FENDERS_MENU(MODEL_NAMES eVehicleModel)
	SWITCH eVehicleModel
		CASE CLUB
		CASE GAUNTLET5
		#IF FEATURE_HEIST_ISLAND
		CASE WEEVIL
		#ENDIF
		#IF FEATURE_TUNER
		CASE ZR350
		CASE REMUS
		CASE DOMINATOR7
		CASE TAILGATER2
		CASE PREVION
		CASE RT3000
		#ENDIF
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_XMAS_17_LIVERIES_LOCKED_THREE(MODEL_NAMES eVehicleModel, INT iLabelHash, CARMOD_MENU_ENUM eMenu)

	IF eMenu = CMM_SUPERMOD_LIVERY
		
		INT iStatInt25 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES19)
		//INT iStatInt26 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES20)
		
		SWITCH iLabelHash		
			CASE HASH("PREV_LIV11")
				SWITCH eVehicleModel
					CASE PREVION
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 6)
						AND NOT IS_BIT_SET(iStatInt25, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("PREV_LIV12")
				SWITCH eVehicleModel
					CASE PREVION
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 7)
						AND NOT IS_BIT_SET(iStatInt25, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("PREV_LIV13")
				SWITCH eVehicleModel
					CASE PREVION
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 8)
						AND NOT IS_BIT_SET(iStatInt25, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("PREV_LIV14")
				SWITCH eVehicleModel
					CASE PREVION
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 9)
						AND NOT IS_BIT_SET(iStatInt25, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("PREV_LIV15")
				SWITCH eVehicleModel
					CASE PREVION
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 10)
						AND NOT IS_BIT_SET(iStatInt25, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("SULTAN3_LIV11")
				SWITCH eVehicleModel
					CASE SULTAN3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 11)
						AND NOT IS_BIT_SET(iStatInt25, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("SULTAN3_LIV12")
				SWITCH eVehicleModel
					CASE SULTAN3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 12)
						AND NOT IS_BIT_SET(iStatInt25, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("SULTAN3_LIV13")
				SWITCH eVehicleModel
					CASE SULTAN3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 13)
						AND NOT IS_BIT_SET(iStatInt25, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("SULTAN3_LIV14")
				SWITCH eVehicleModel
					CASE SULTAN3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 14)
						AND NOT IS_BIT_SET(iStatInt25, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("SULTAN3_LIV15")
				SWITCH eVehicleModel
					CASE SULTAN3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 15)
						AND NOT IS_BIT_SET(iStatInt25, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("COMET7_LIV11")
				SWITCH eVehicleModel
					CASE COMET7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 16)
						AND NOT IS_BIT_SET(iStatInt25, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK	
			CASE HASH("DEITY_LIVERY12")
				SWITCH eVehicleModel
					CASE DEITY
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 17)
						AND NOT IS_BIT_SET(iStatInt25, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK	
			CASE HASH("BALL7_LIV_13")
				SWITCH eVehicleModel
					CASE BALLER7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 18)
						AND NOT IS_BIT_SET(iStatInt25, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("CORSITA_LIV10")
				SWITCH eVehicleModel
					CASE CORSITA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 19)
						AND NOT IS_BIT_SET(iStatInt25, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("VIGR2_LIV_10")
				SWITCH eVehicleModel
					CASE VIGERO2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, 20)
						AND NOT IS_BIT_SET(iStatInt25, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_XMAS_17_LIVERIES_LOCKED_TWO(MODEL_NAMES eVehicleModel, INT iLabelHash, CARMOD_MENU_ENUM eMenu)

	IF eMenu = CMM_SUPERMOD_LIVERY
		
		INT iStatInt13 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES7)
		INT iStatInt14 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES8)
		INT iStatInt15 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES9)
		INT iStatInt16 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES10)
		INT iStatInt17 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES11)
		INT iStatInt18 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES12)
		INT iStatInt19 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES13)
		INT iStatInt20 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES14)
		INT iStatInt21 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES15)
		INT iStatInt22 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES16)
		INT iStatInt23 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES17)
		INT iStatInt24 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES18)
		
		SWITCH iLabelHash	
			CASE HASH("SCCAMO_LIV1")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 0)
						AND NOT IS_BIT_SET(iStatInt13, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 10)
						AND NOT IS_BIT_SET(iStatInt13, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 20)
						AND NOT IS_BIT_SET(iStatInt13, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 0)
						AND NOT IS_BIT_SET(iStatInt14, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 10)
						AND NOT IS_BIT_SET(iStatInt14, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 20)
						AND NOT IS_BIT_SET(iStatInt14, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 0)
						AND NOT IS_BIT_SET(iStatInt15, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 10)
						AND NOT IS_BIT_SET(iStatInt15, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 20)
						AND NOT IS_BIT_SET(iStatInt15, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 0)
						AND NOT IS_BIT_SET(iStatInt16, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 10)
						AND NOT IS_BIT_SET(iStatInt16, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 20)
						AND NOT IS_BIT_SET(iStatInt16, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 0)
						AND NOT IS_BIT_SET(iStatInt17, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 10)
						AND NOT IS_BIT_SET(iStatInt17, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 20)
						AND NOT IS_BIT_SET(iStatInt17, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 0)
						AND NOT IS_BIT_SET(iStatInt18, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 10)
						AND NOT IS_BIT_SET(iStatInt18, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 20)
						AND NOT IS_BIT_SET(iStatInt18, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 0)
						AND NOT IS_BIT_SET(iStatInt19, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 10)
						AND NOT IS_BIT_SET(iStatInt19, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 20)
						AND NOT IS_BIT_SET(iStatInt19, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 0)
						AND NOT IS_BIT_SET(iStatInt20, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 10)
						AND NOT IS_BIT_SET(iStatInt20, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 20)
						AND NOT IS_BIT_SET(iStatInt20, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 0)
						AND NOT IS_BIT_SET(iStatInt21, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 10)
						AND NOT IS_BIT_SET(iStatInt21, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 20)
						AND NOT IS_BIT_SET(iStatInt21, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 20)
						AND NOT IS_BIT_SET(iStatInt22, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV2")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 1)
						AND NOT IS_BIT_SET(iStatInt13, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 11)
						AND NOT IS_BIT_SET(iStatInt13, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 21)
						AND NOT IS_BIT_SET(iStatInt13, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 1)
						AND NOT IS_BIT_SET(iStatInt14, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 11)
						AND NOT IS_BIT_SET(iStatInt14, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 21)
						AND NOT IS_BIT_SET(iStatInt14, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 1)
						AND NOT IS_BIT_SET(iStatInt15, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 11)
						AND NOT IS_BIT_SET(iStatInt15, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 21)
						AND NOT IS_BIT_SET(iStatInt15, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 1)
						AND NOT IS_BIT_SET(iStatInt16, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 11)
						AND NOT IS_BIT_SET(iStatInt16, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 21)
						AND NOT IS_BIT_SET(iStatInt16, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 1)
						AND NOT IS_BIT_SET(iStatInt17, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 11)
						AND NOT IS_BIT_SET(iStatInt17, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 21)
						AND NOT IS_BIT_SET(iStatInt17, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 1)
						AND NOT IS_BIT_SET(iStatInt18, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 11)
						AND NOT IS_BIT_SET(iStatInt18, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 21)
						AND NOT IS_BIT_SET(iStatInt18, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 1)
						AND NOT IS_BIT_SET(iStatInt19, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 11)
						AND NOT IS_BIT_SET(iStatInt19, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 21)
						AND NOT IS_BIT_SET(iStatInt19, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 1)
						AND NOT IS_BIT_SET(iStatInt20, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 11)
						AND NOT IS_BIT_SET(iStatInt20, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 21)
						AND NOT IS_BIT_SET(iStatInt20, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 1)
						AND NOT IS_BIT_SET(iStatInt21, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 11)
						AND NOT IS_BIT_SET(iStatInt21, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 21)
						AND NOT IS_BIT_SET(iStatInt21, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 21)
						AND NOT IS_BIT_SET(iStatInt22, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV3")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 2)
						AND NOT IS_BIT_SET(iStatInt13, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 12)
						AND NOT IS_BIT_SET(iStatInt13, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 22)
						AND NOT IS_BIT_SET(iStatInt13, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 2)
						AND NOT IS_BIT_SET(iStatInt14, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 12)
						AND NOT IS_BIT_SET(iStatInt14, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 22)
						AND NOT IS_BIT_SET(iStatInt14, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 2)
						AND NOT IS_BIT_SET(iStatInt15, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 12)
						AND NOT IS_BIT_SET(iStatInt15, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 22)
						AND NOT IS_BIT_SET(iStatInt15, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 2)
						AND NOT IS_BIT_SET(iStatInt16, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 12)
						AND NOT IS_BIT_SET(iStatInt16, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 22)
						AND NOT IS_BIT_SET(iStatInt16, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 2)
						AND NOT IS_BIT_SET(iStatInt17, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 12)
						AND NOT IS_BIT_SET(iStatInt17, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 22)
						AND NOT IS_BIT_SET(iStatInt17, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 2)
						AND NOT IS_BIT_SET(iStatInt18, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 12)
						AND NOT IS_BIT_SET(iStatInt18, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 22)
						AND NOT IS_BIT_SET(iStatInt18, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 2)
						AND NOT IS_BIT_SET(iStatInt19, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 12)
						AND NOT IS_BIT_SET(iStatInt19, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 22)
						AND NOT IS_BIT_SET(iStatInt19, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 2)
						AND NOT IS_BIT_SET(iStatInt20, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 12)
						AND NOT IS_BIT_SET(iStatInt20, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 21)
						AND NOT IS_BIT_SET(iStatInt20, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 2)
						AND NOT IS_BIT_SET(iStatInt21, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 12)
						AND NOT IS_BIT_SET(iStatInt21, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 22)
						AND NOT IS_BIT_SET(iStatInt21, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 22)
						AND NOT IS_BIT_SET(iStatInt22, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV4")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 3)
						AND NOT IS_BIT_SET(iStatInt13, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 13)
						AND NOT IS_BIT_SET(iStatInt13, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 23)
						AND NOT IS_BIT_SET(iStatInt13, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 3)
						AND NOT IS_BIT_SET(iStatInt14, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 13)
						AND NOT IS_BIT_SET(iStatInt14, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 23)
						AND NOT IS_BIT_SET(iStatInt14, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 3)
						AND NOT IS_BIT_SET(iStatInt15, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 13)
						AND NOT IS_BIT_SET(iStatInt15, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 23)
						AND NOT IS_BIT_SET(iStatInt15, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 3)
						AND NOT IS_BIT_SET(iStatInt16, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 13)
						AND NOT IS_BIT_SET(iStatInt16, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 23)
						AND NOT IS_BIT_SET(iStatInt16, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 3)
						AND NOT IS_BIT_SET(iStatInt17, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 13)
						AND NOT IS_BIT_SET(iStatInt17, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 23)
						AND NOT IS_BIT_SET(iStatInt17, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 3)
						AND NOT IS_BIT_SET(iStatInt18, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 13)
						AND NOT IS_BIT_SET(iStatInt18, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 23)
						AND NOT IS_BIT_SET(iStatInt18, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 3)
						AND NOT IS_BIT_SET(iStatInt19, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 13)
						AND NOT IS_BIT_SET(iStatInt19, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 23)
						AND NOT IS_BIT_SET(iStatInt19, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 3)
						AND NOT IS_BIT_SET(iStatInt20, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 13)
						AND NOT IS_BIT_SET(iStatInt20, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 23)
						AND NOT IS_BIT_SET(iStatInt20, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 3)
						AND NOT IS_BIT_SET(iStatInt21, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 13)
						AND NOT IS_BIT_SET(iStatInt21, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 23)
						AND NOT IS_BIT_SET(iStatInt21, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 23)
						AND NOT IS_BIT_SET(iStatInt22, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV5")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 4)
						AND NOT IS_BIT_SET(iStatInt13, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 14)
						AND NOT IS_BIT_SET(iStatInt13, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 24)
						AND NOT IS_BIT_SET(iStatInt13, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 4)
						AND NOT IS_BIT_SET(iStatInt14, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 14)
						AND NOT IS_BIT_SET(iStatInt14, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 24)
						AND NOT IS_BIT_SET(iStatInt14, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 4)
						AND NOT IS_BIT_SET(iStatInt15, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 14)
						AND NOT IS_BIT_SET(iStatInt15, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 24)
						AND NOT IS_BIT_SET(iStatInt15, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 4)
						AND NOT IS_BIT_SET(iStatInt16, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 14)
						AND NOT IS_BIT_SET(iStatInt16, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 24)
						AND NOT IS_BIT_SET(iStatInt16, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 4)
						AND NOT IS_BIT_SET(iStatInt17, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 14)
						AND NOT IS_BIT_SET(iStatInt17, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 24)
						AND NOT IS_BIT_SET(iStatInt17, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 4)
						AND NOT IS_BIT_SET(iStatInt18, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 14)
						AND NOT IS_BIT_SET(iStatInt18, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 24)
						AND NOT IS_BIT_SET(iStatInt18, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 4)
						AND NOT IS_BIT_SET(iStatInt19, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 14)
						AND NOT IS_BIT_SET(iStatInt19, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 24)
						AND NOT IS_BIT_SET(iStatInt19, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 4)
						AND NOT IS_BIT_SET(iStatInt20, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 14)
						AND NOT IS_BIT_SET(iStatInt20, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 24)
						AND NOT IS_BIT_SET(iStatInt20, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 4)
						AND NOT IS_BIT_SET(iStatInt21, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 14)
						AND NOT IS_BIT_SET(iStatInt21, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 24)
						AND NOT IS_BIT_SET(iStatInt21, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 24)
						AND NOT IS_BIT_SET(iStatInt22, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK	
			CASE HASH("SCCAMO_LIV6")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 5)
						AND NOT IS_BIT_SET(iStatInt13, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 15)
						AND NOT IS_BIT_SET(iStatInt13, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 25)
						AND NOT IS_BIT_SET(iStatInt13, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 5)
						AND NOT IS_BIT_SET(iStatInt14, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 15)
						AND NOT IS_BIT_SET(iStatInt14, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 25)
						AND NOT IS_BIT_SET(iStatInt14, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 5)
						AND NOT IS_BIT_SET(iStatInt15, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 15)
						AND NOT IS_BIT_SET(iStatInt15, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 25)
						AND NOT IS_BIT_SET(iStatInt15, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 5)
						AND NOT IS_BIT_SET(iStatInt16, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 15)
						AND NOT IS_BIT_SET(iStatInt16, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 25)
						AND NOT IS_BIT_SET(iStatInt16, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 5)
						AND NOT IS_BIT_SET(iStatInt17, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 15)
						AND NOT IS_BIT_SET(iStatInt17, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 25)
						AND NOT IS_BIT_SET(iStatInt17, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 5)
						AND NOT IS_BIT_SET(iStatInt18, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 15)
						AND NOT IS_BIT_SET(iStatInt18, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 25)
						AND NOT IS_BIT_SET(iStatInt18, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 5)
						AND NOT IS_BIT_SET(iStatInt19, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 15)
						AND NOT IS_BIT_SET(iStatInt19, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 25)
						AND NOT IS_BIT_SET(iStatInt19, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 5)
						AND NOT IS_BIT_SET(iStatInt20, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 15)
						AND NOT IS_BIT_SET(iStatInt20, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 25)
						AND NOT IS_BIT_SET(iStatInt20, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 5)
						AND NOT IS_BIT_SET(iStatInt21, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 15)
						AND NOT IS_BIT_SET(iStatInt21, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 25)
						AND NOT IS_BIT_SET(iStatInt21, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 25)
						AND NOT IS_BIT_SET(iStatInt22, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV7")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 6)
						AND NOT IS_BIT_SET(iStatInt13, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 16)
						AND NOT IS_BIT_SET(iStatInt13, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 26)
						AND NOT IS_BIT_SET(iStatInt13, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 6)
						AND NOT IS_BIT_SET(iStatInt14, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 16)
						AND NOT IS_BIT_SET(iStatInt14, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 26)
						AND NOT IS_BIT_SET(iStatInt14, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 6)
						AND NOT IS_BIT_SET(iStatInt15, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 16)
						AND NOT IS_BIT_SET(iStatInt15, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 26)
						AND NOT IS_BIT_SET(iStatInt15, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 6)
						AND NOT IS_BIT_SET(iStatInt16, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 16)
						AND NOT IS_BIT_SET(iStatInt16, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 26)
						AND NOT IS_BIT_SET(iStatInt16, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 6)
						AND NOT IS_BIT_SET(iStatInt17, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 16)
						AND NOT IS_BIT_SET(iStatInt17, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 26)
						AND NOT IS_BIT_SET(iStatInt17, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 6)
						AND NOT IS_BIT_SET(iStatInt18, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 16)
						AND NOT IS_BIT_SET(iStatInt18, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 26)
						AND NOT IS_BIT_SET(iStatInt18, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 6)
						AND NOT IS_BIT_SET(iStatInt19, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 16)
						AND NOT IS_BIT_SET(iStatInt19, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 26)
						AND NOT IS_BIT_SET(iStatInt19, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 6)
						AND NOT IS_BIT_SET(iStatInt20, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 16)
						AND NOT IS_BIT_SET(iStatInt20, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 26)
						AND NOT IS_BIT_SET(iStatInt20, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 6)
						AND NOT IS_BIT_SET(iStatInt21, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 16)
						AND NOT IS_BIT_SET(iStatInt21, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 26)
						AND NOT IS_BIT_SET(iStatInt21, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 26)
						AND NOT IS_BIT_SET(iStatInt22, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV8")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 7)
						AND NOT IS_BIT_SET(iStatInt13, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 17)
						AND NOT IS_BIT_SET(iStatInt13, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 27)
						AND NOT IS_BIT_SET(iStatInt13, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 7)
						AND NOT IS_BIT_SET(iStatInt14, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 17)
						AND NOT IS_BIT_SET(iStatInt14, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 27)
						AND NOT IS_BIT_SET(iStatInt14, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 7)
						AND NOT IS_BIT_SET(iStatInt15, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 17)
						AND NOT IS_BIT_SET(iStatInt15, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 27)
						AND NOT IS_BIT_SET(iStatInt15, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 7)
						AND NOT IS_BIT_SET(iStatInt16, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 17)
						AND NOT IS_BIT_SET(iStatInt16, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 27)
						AND NOT IS_BIT_SET(iStatInt16, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 7)
						AND NOT IS_BIT_SET(iStatInt17, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 17)
						AND NOT IS_BIT_SET(iStatInt17, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 27)
						AND NOT IS_BIT_SET(iStatInt17, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 7)
						AND NOT IS_BIT_SET(iStatInt18, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 17)
						AND NOT IS_BIT_SET(iStatInt18, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 27)
						AND NOT IS_BIT_SET(iStatInt18, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 7)
						AND NOT IS_BIT_SET(iStatInt19, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 17)
						AND NOT IS_BIT_SET(iStatInt19, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 27)
						AND NOT IS_BIT_SET(iStatInt19, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 7)
						AND NOT IS_BIT_SET(iStatInt20, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 17)
						AND NOT IS_BIT_SET(iStatInt20, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 27)
						AND NOT IS_BIT_SET(iStatInt20, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 7)
						AND NOT IS_BIT_SET(iStatInt21, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 17)
						AND NOT IS_BIT_SET(iStatInt21, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 27)
						AND NOT IS_BIT_SET(iStatInt21, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 27)
						AND NOT IS_BIT_SET(iStatInt22, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV9")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 8)
						AND NOT IS_BIT_SET(iStatInt13, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 18)
						AND NOT IS_BIT_SET(iStatInt13, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 28)
						AND NOT IS_BIT_SET(iStatInt13, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 8)
						AND NOT IS_BIT_SET(iStatInt14, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES H", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 18)
						AND NOT IS_BIT_SET(iStatInt14, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 28)
						AND NOT IS_BIT_SET(iStatInt14, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 8)
						AND NOT IS_BIT_SET(iStatInt15, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 18)
						AND NOT IS_BIT_SET(iStatInt15, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 28)
						AND NOT IS_BIT_SET(iStatInt15, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 8)
						AND NOT IS_BIT_SET(iStatInt16, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 18)
						AND NOT IS_BIT_SET(iStatInt16, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 28)
						AND NOT IS_BIT_SET(iStatInt16, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 8)
						AND NOT IS_BIT_SET(iStatInt17, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 18)
						AND NOT IS_BIT_SET(iStatInt17, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 28)
						AND NOT IS_BIT_SET(iStatInt17, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 8)
						AND NOT IS_BIT_SET(iStatInt18, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 18)
						AND NOT IS_BIT_SET(iStatInt18, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 28)
						AND NOT IS_BIT_SET(iStatInt18, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 8)
						AND NOT IS_BIT_SET(iStatInt19, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 18)
						AND NOT IS_BIT_SET(iStatInt19, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 28)
						AND NOT IS_BIT_SET(iStatInt19, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 8)
						AND NOT IS_BIT_SET(iStatInt20, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 18)
						AND NOT IS_BIT_SET(iStatInt20, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 28)
						AND NOT IS_BIT_SET(iStatInt20, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 8)
						AND NOT IS_BIT_SET(iStatInt21, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 18)
						AND NOT IS_BIT_SET(iStatInt21, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 28)
						AND NOT IS_BIT_SET(iStatInt21, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 28)
						AND NOT IS_BIT_SET(iStatInt22, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("SCCAMO_LIV10")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 9)
						AND NOT IS_BIT_SET(iStatInt13, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MULE4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 19)
						AND NOT IS_BIT_SET(iStatInt13, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, 29)
						AND NOT IS_BIT_SET(iStatInt13, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 9)
						AND NOT IS_BIT_SET(iStatInt14, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 19)
						AND NOT IS_BIT_SET(iStatInt14, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, 29)
						AND NOT IS_BIT_SET(iStatInt14, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 9)
						AND NOT IS_BIT_SET(iStatInt15, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 19)
						AND NOT IS_BIT_SET(iStatInt15, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, 29)
						AND NOT IS_BIT_SET(iStatInt15, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 9)
						AND NOT IS_BIT_SET(iStatInt16, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 19)
						AND NOT IS_BIT_SET(iStatInt16, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, 29)
						AND NOT IS_BIT_SET(iStatInt16, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 9)
						AND NOT IS_BIT_SET(iStatInt17, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 19)
						AND NOT IS_BIT_SET(iStatInt17, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, 29)
						AND NOT IS_BIT_SET(iStatInt17, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 9)
						AND NOT IS_BIT_SET(iStatInt18, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 19)
						AND NOT IS_BIT_SET(iStatInt18, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, 29)
						AND NOT IS_BIT_SET(iStatInt18, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 9)
						AND NOT IS_BIT_SET(iStatInt19, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 19)
						AND NOT IS_BIT_SET(iStatInt19, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, 29)
						AND NOT IS_BIT_SET(iStatInt19, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE SPEEDO4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 9)
						AND NOT IS_BIT_SET(iStatInt20, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE PATRIOT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 19)
						AND NOT IS_BIT_SET(iStatInt20, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE PATRIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, 29)
						AND NOT IS_BIT_SET(iStatInt20, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE POUNDER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 9)
						AND NOT IS_BIT_SET(iStatInt21, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE MENACER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 19)
						AND NOT IS_BIT_SET(iStatInt21, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE OPPRESSOR2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, 29)
						AND NOT IS_BIT_SET(iStatInt21, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE REVOLTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 29)
						AND NOT IS_BIT_SET(iStatInt22, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("STAF_LIV8")
				SWITCH eVehicleModel
					
					CASE SWINGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 0)
						AND NOT IS_BIT_SET(iStatInt22, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE STAFFORD
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 10)
						AND NOT IS_BIT_SET(iStatInt22, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
				ENDSWITCH	
			BREAK	
			CASE HASH("STAF_LIV9")
				SWITCH eVehicleModel
					
					CASE SWINGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 1)
						AND NOT IS_BIT_SET(iStatInt22, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
				ENDSWITCH	
			BREAK
			CASE HASH("THX_LIV_10")
				SWITCH eVehicleModel
					CASE THRAX
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 2)
						AND NOT IS_BIT_SET(iStatInt22, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("PARAG_LIV2")
				SWITCH eVehicleModel
					CASE PARAGON2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 3)
						AND NOT IS_BIT_SET(iStatInt22, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					CASE PARAGON
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 4)
						AND NOT IS_BIT_SET(iStatInt22, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("S80_LIV_10")
				SWITCH eVehicleModel
					CASE S80
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 5)
						AND NOT IS_BIT_SET(iStatInt22, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("CLIQ_LIV11")
				SWITCH eVehicleModel
					CASE CLIQUE
						IF !GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_COUPON_CAR_WHEN_LOGGING_ON_CHRISTMAS_DAY_18) 
						AND NOT IS_BIT_SET(iStatInt22, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
				ENDSWITCH	
			BREAK
			CASE HASH("LANDST2_LIV_10")
				SWITCH eVehicleModel
					CASE LANDSTALKER2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 7)
						AND NOT IS_BIT_SET(iStatInt22, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK	
			CASE HASH("SEM2_LIVERY9")
				SWITCH eVehicleModel
					CASE SEMINOLE2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 8)
						AND NOT IS_BIT_SET(iStatInt22, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("RSX_LIVERY11")
				SWITCH eVehicleModel
					CASE ITALIRSX
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 9)
						AND NOT IS_BIT_SET(iStatInt22, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("TORD_LIV11")
				SWITCH eVehicleModel
					CASE TOREADOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 11)
						AND NOT IS_BIT_SET(iStatInt22, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("BANSH_LIV11")
				SWITCH eVehicleModel
					CASE BANSHEE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 13)
						AND NOT IS_BIT_SET(iStatInt22, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("BANSH_LIV12")
				SWITCH eVehicleModel
					CASE BANSHEE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 14)
						AND NOT IS_BIT_SET(iStatInt22, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("BANSH_LIV13")
				SWITCH eVehicleModel
					CASE BANSHEE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 15)
						AND NOT IS_BIT_SET(iStatInt22, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("BANSH_LIV14")
				SWITCH eVehicleModel
					CASE BANSHEE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 16)
						AND NOT IS_BIT_SET(iStatInt22, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("BANSH_LIV15")
				SWITCH eVehicleModel
					CASE BANSHEE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, 17)
						AND NOT IS_BIT_SET(iStatInt22, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("KURUMA_LIV11")
				SWITCH eVehicleModel
					CASE KURUMA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 0)
						AND NOT IS_BIT_SET(iStatInt23, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("KURUMA_LIV12")
				SWITCH eVehicleModel
					CASE KURUMA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 1)
						AND NOT IS_BIT_SET(iStatInt23, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("KURUMA_LIV13")
				SWITCH eVehicleModel
					CASE KURUMA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 2)
						AND NOT IS_BIT_SET(iStatInt23, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("KURUMA_LIV14")
				SWITCH eVehicleModel
					CASE KURUMA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 3)
						AND NOT IS_BIT_SET(iStatInt23, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("KURUMA_LIV15")
				SWITCH eVehicleModel
					CASE KURUMA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 4)
						AND NOT IS_BIT_SET(iStatInt23, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("HOTRING_LIV31")
				SWITCH eVehicleModel
					CASE HOTRING
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 5)
						AND NOT IS_BIT_SET(iStatInt23, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("KANJO_LIV13")
				SWITCH eVehicleModel
					CASE KANJO
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 6)
						AND NOT IS_BIT_SET(iStatInt23, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("HERMES_LIV11")
				SWITCH eVehicleModel
					CASE HERMES
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 7)
						AND NOT IS_BIT_SET(iStatInt23, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("TORD_LIV12")
				SWITCH eVehicleModel
					CASE TOREADOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 8)
						AND NOT IS_BIT_SET(iStatInt23, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("COMET6_LIV11")
				SWITCH eVehicleModel
					CASE COMET6
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 14)
						AND NOT IS_BIT_SET(iStatInt23, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("COMET6_LIV12")
				SWITCH eVehicleModel
					CASE COMET6
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 15)
						AND NOT IS_BIT_SET(iStatInt23, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("COMET6_LIV13")
				SWITCH eVehicleModel
					CASE COMET6
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 16)
						AND NOT IS_BIT_SET(iStatInt23, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("COMET6_LIV14")
				SWITCH eVehicleModel
					CASE COMET6
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 17)
						AND NOT IS_BIT_SET(iStatInt23, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("COMET6_LIV15")
				SWITCH eVehicleModel
					CASE COMET6
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 18)
						AND NOT IS_BIT_SET(iStatInt23, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("CYPH_LIVERY11")
				SWITCH eVehicleModel
					CASE CYPHER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 19)
						AND NOT IS_BIT_SET(iStatInt23, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("CYPH_LIVERY12")
				SWITCH eVehicleModel
					CASE CYPHER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 20)
						AND NOT IS_BIT_SET(iStatInt23, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("CYPH_LIVERY13")
				SWITCH eVehicleModel
					CASE CYPHER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 21)
						AND NOT IS_BIT_SET(iStatInt23, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("CYPH_LIVERY14")
				SWITCH eVehicleModel
					CASE CYPHER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 22)
						AND NOT IS_BIT_SET(iStatInt23, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("CYPH_LIVERY15")
				SWITCH eVehicleModel
					CASE CYPHER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 23)
						AND NOT IS_BIT_SET(iStatInt23, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("DOM7_LIVERY11")
				SWITCH eVehicleModel
					CASE DOMINATOR7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 24)
						AND NOT IS_BIT_SET(iStatInt23, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("DOM7_LIVERY12")
				SWITCH eVehicleModel
					CASE DOMINATOR7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 25)
						AND NOT IS_BIT_SET(iStatInt23, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("DOM7_LIVERY13")
				SWITCH eVehicleModel
					CASE DOMINATOR7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 26)
						AND NOT IS_BIT_SET(iStatInt23, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("DOM7_LIVERY14")
				SWITCH eVehicleModel
					CASE DOMINATOR7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 27)
						AND NOT IS_BIT_SET(iStatInt23, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("DOM7_LIVERY15")
				SWITCH eVehicleModel
					CASE DOMINATOR7
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, 28)
						AND NOT IS_BIT_SET(iStatInt23, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("VTC_LIV11")
				SWITCH eVehicleModel
					CASE VECTRE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 3)
						AND NOT IS_BIT_SET(iStatInt24, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("VTC_LIV12")
				SWITCH eVehicleModel
					CASE VECTRE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 4)
						AND NOT IS_BIT_SET(iStatInt24, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("VTC_LIV13")
				SWITCH eVehicleModel
					CASE VECTRE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 5)
						AND NOT IS_BIT_SET(iStatInt24, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("VTC_LIV14")
				SWITCH eVehicleModel
					CASE VECTRE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 6)
						AND NOT IS_BIT_SET(iStatInt24, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("VTC_LIV15")
				SWITCH eVehicleModel
					CASE VECTRE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 7)
						AND NOT IS_BIT_SET(iStatInt24, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("GROWLER_LIV11")
				SWITCH eVehicleModel
					CASE GROWLER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 18)
						AND NOT IS_BIT_SET(iStatInt24, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("GROWLER_LIV12")
				SWITCH eVehicleModel
					CASE GROWLER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 19)
						AND NOT IS_BIT_SET(iStatInt24, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("GROWLER_LIV13")
				SWITCH eVehicleModel
					CASE GROWLER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 20)
						AND NOT IS_BIT_SET(iStatInt24, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("GROWLER_LIV14")
				SWITCH eVehicleModel
					CASE GROWLER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 21)
						AND NOT IS_BIT_SET(iStatInt24, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
			CASE HASH("GROWLER_LIV15")
				SWITCH eVehicleModel
					CASE GROWLER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, 22)
						AND NOT IS_BIT_SET(iStatInt24, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			BREAK
		ENDSWITCH	
	ENDIF
	
	RETURN SHOULD_XMAS_17_LIVERIES_LOCKED_THREE(eVehicleModel, iLabelHash, eMenu)
ENDFUNC

FUNC BOOL SHOULD_XMAS_17_LIVERIES_LOCKED(MODEL_NAMES eVehicleModel, INT iLabelHash, CARMOD_MENU_ENUM eMenu)
	
	#IF IS_DEBUG_BUILD
	IF g_bUnlockXmasLiveries
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF eMenu = CMM_SUPERMOD_LIVERY
		
		INT iStatInt6 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES0)
		INT iStatInt7 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES1)
		INT iStatInt8 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES2)
		INT iStatInt9 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES3)
		INT iStatInt10 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES4)
		INT iStatInt11 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES5)
		INT iStatInt12 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES6)
		
		SWITCH iLabelHash
			CASE HASH("XMAS_CAMO01")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 0)
						AND NOT IS_BIT_SET(iStatInt6, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 10)
						AND NOT IS_BIT_SET(iStatInt6, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 20)
						AND NOT IS_BIT_SET(iStatInt6, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 0)
						AND NOT IS_BIT_SET(iStatInt7, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 10)
						AND NOT IS_BIT_SET(iStatInt7, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 20)
						AND NOT IS_BIT_SET(iStatInt7, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 0)
						AND NOT IS_BIT_SET(iStatInt8, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 10)
						AND NOT IS_BIT_SET(iStatInt8, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 20)
						AND NOT IS_BIT_SET(iStatInt8, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 0)
						AND NOT IS_BIT_SET(iStatInt9, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 10)
						AND NOT IS_BIT_SET(iStatInt9, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 20)
						AND NOT IS_BIT_SET(iStatInt9, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 0)
						AND NOT IS_BIT_SET(iStatInt10, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 10)
						AND NOT IS_BIT_SET(iStatInt10, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 20)
						AND NOT IS_BIT_SET(iStatInt10, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 0)
						AND NOT IS_BIT_SET(iStatInt11, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 10)
						AND NOT IS_BIT_SET(iStatInt11, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 20)
						AND NOT IS_BIT_SET(iStatInt11, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 0)
						AND NOT IS_BIT_SET(iStatInt12, 0)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 10)
						AND NOT IS_BIT_SET(iStatInt12, 10)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 20)
						AND NOT IS_BIT_SET(iStatInt12, 20)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO02")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 1)
						AND NOT IS_BIT_SET(iStatInt6, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 11)
						AND NOT IS_BIT_SET(iStatInt6, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 21)
						AND NOT IS_BIT_SET(iStatInt6, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 1)
						AND NOT IS_BIT_SET(iStatInt7, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 11)
						AND NOT IS_BIT_SET(iStatInt7, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 21)
						AND NOT IS_BIT_SET(iStatInt7, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 1)
						AND NOT IS_BIT_SET(iStatInt8, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 11)
						AND NOT IS_BIT_SET(iStatInt8, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 21)
						AND NOT IS_BIT_SET(iStatInt8, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 1)
						AND NOT IS_BIT_SET(iStatInt9, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 11)
						AND NOT IS_BIT_SET(iStatInt9, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 21)
						AND NOT IS_BIT_SET(iStatInt9, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 1)
						AND NOT IS_BIT_SET(iStatInt10, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 11)
						AND NOT IS_BIT_SET(iStatInt10, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 21)
						AND NOT IS_BIT_SET(iStatInt10, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 1)
						AND NOT IS_BIT_SET(iStatInt11, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 11)
						AND NOT IS_BIT_SET(iStatInt11, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 21)
						AND NOT IS_BIT_SET(iStatInt11, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 1)
						AND NOT IS_BIT_SET(iStatInt12, 1)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 11)
						AND NOT IS_BIT_SET(iStatInt12, 11)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 21)
						AND NOT IS_BIT_SET(iStatInt12, 21)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO03")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 2)
						AND NOT IS_BIT_SET(iStatInt6, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 12)
						AND NOT IS_BIT_SET(iStatInt6, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 22)
						AND NOT IS_BIT_SET(iStatInt6, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 2)
						AND NOT IS_BIT_SET(iStatInt7, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 12)
						AND NOT IS_BIT_SET(iStatInt7, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 22)
						AND NOT IS_BIT_SET(iStatInt7, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 2)
						AND NOT IS_BIT_SET(iStatInt8, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 12)
						AND NOT IS_BIT_SET(iStatInt8, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 22)
						AND NOT IS_BIT_SET(iStatInt8, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 2)
						AND NOT IS_BIT_SET(iStatInt9, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 12)
						AND NOT IS_BIT_SET(iStatInt9, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 22)
						AND NOT IS_BIT_SET(iStatInt9, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 2)
						AND NOT IS_BIT_SET(iStatInt10, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 12)
						AND NOT IS_BIT_SET(iStatInt10, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 22)
						AND NOT IS_BIT_SET(iStatInt10, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 2)
						AND NOT IS_BIT_SET(iStatInt11, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 12)
						AND NOT IS_BIT_SET(iStatInt11, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 22)
						AND NOT IS_BIT_SET(iStatInt11, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 2)
						AND NOT IS_BIT_SET(iStatInt12, 2)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 12)
						AND NOT IS_BIT_SET(iStatInt12, 12)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 22)
						AND NOT IS_BIT_SET(iStatInt12, 22)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO04")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 3)
						AND NOT IS_BIT_SET(iStatInt6, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 13)
						AND NOT IS_BIT_SET(iStatInt6, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 23)
						AND NOT IS_BIT_SET(iStatInt6, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 3)
						AND NOT IS_BIT_SET(iStatInt7, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 13)
						AND NOT IS_BIT_SET(iStatInt7, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 23)
						AND NOT IS_BIT_SET(iStatInt7, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 3)
						AND NOT IS_BIT_SET(iStatInt8, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 13)
						AND NOT IS_BIT_SET(iStatInt8, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 23)
						AND NOT IS_BIT_SET(iStatInt8, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 3)
						AND NOT IS_BIT_SET(iStatInt9, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 13)
						AND NOT IS_BIT_SET(iStatInt9, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 23)
						AND NOT IS_BIT_SET(iStatInt9, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 3)
						AND NOT IS_BIT_SET(iStatInt10, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 13)
						AND NOT IS_BIT_SET(iStatInt10, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 23)
						AND NOT IS_BIT_SET(iStatInt10, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 3)
						AND NOT IS_BIT_SET(iStatInt11, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 13)
						AND NOT IS_BIT_SET(iStatInt11, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 23)
						AND NOT IS_BIT_SET(iStatInt11, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 3)
						AND NOT IS_BIT_SET(iStatInt12, 3)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 13)
						AND NOT IS_BIT_SET(iStatInt12, 13)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 23)
						AND NOT IS_BIT_SET(iStatInt12, 23)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO05")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 4)
						AND NOT IS_BIT_SET(iStatInt6, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 14)
						AND NOT IS_BIT_SET(iStatInt6, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 24)
						AND NOT IS_BIT_SET(iStatInt6, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 4)
						AND NOT IS_BIT_SET(iStatInt7, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 14)
						AND NOT IS_BIT_SET(iStatInt7, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 24)
						AND NOT IS_BIT_SET(iStatInt7, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 4)
						AND NOT IS_BIT_SET(iStatInt8, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 14)
						AND NOT IS_BIT_SET(iStatInt8, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 24)
						AND NOT IS_BIT_SET(iStatInt8, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 4)
						AND NOT IS_BIT_SET(iStatInt9, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 14)
						AND NOT IS_BIT_SET(iStatInt9, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 24)
						AND NOT IS_BIT_SET(iStatInt9, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 4)
						AND NOT IS_BIT_SET(iStatInt10, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 14)
						AND NOT IS_BIT_SET(iStatInt10, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 24)
						AND NOT IS_BIT_SET(iStatInt10, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 4)
						AND NOT IS_BIT_SET(iStatInt11, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 14)
						AND NOT IS_BIT_SET(iStatInt11, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 24)
						AND NOT IS_BIT_SET(iStatInt11, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 4)
						AND NOT IS_BIT_SET(iStatInt12, 4)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 14)
						AND NOT IS_BIT_SET(iStatInt12, 14)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 24)
						AND NOT IS_BIT_SET(iStatInt12, 24)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK	
			CASE HASH("XMAS_CAMO06")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 5)
						AND NOT IS_BIT_SET(iStatInt6, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 15)
						AND NOT IS_BIT_SET(iStatInt6, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 25)
						AND NOT IS_BIT_SET(iStatInt6, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 5)
						AND NOT IS_BIT_SET(iStatInt7, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 15)
						AND NOT IS_BIT_SET(iStatInt7, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 25)
						AND NOT IS_BIT_SET(iStatInt7, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 5)
						AND NOT IS_BIT_SET(iStatInt8, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 15)
						AND NOT IS_BIT_SET(iStatInt8, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 25)
						AND NOT IS_BIT_SET(iStatInt8, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 5)
						AND NOT IS_BIT_SET(iStatInt9, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 15)
						AND NOT IS_BIT_SET(iStatInt9, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 25)
						AND NOT IS_BIT_SET(iStatInt9, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 5)
						AND NOT IS_BIT_SET(iStatInt10, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 15)
						AND NOT IS_BIT_SET(iStatInt10, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 25)
						AND NOT IS_BIT_SET(iStatInt10, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 5)
						AND NOT IS_BIT_SET(iStatInt11, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 15)
						AND NOT IS_BIT_SET(iStatInt11, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 25)
						AND NOT IS_BIT_SET(iStatInt11, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 5)
						AND NOT IS_BIT_SET(iStatInt12, 5)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 15)
						AND NOT IS_BIT_SET(iStatInt12, 15)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 25)
						AND NOT IS_BIT_SET(iStatInt12, 25)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO07")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 6)
						AND NOT IS_BIT_SET(iStatInt6, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 16)
						AND NOT IS_BIT_SET(iStatInt6, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 26)
						AND NOT IS_BIT_SET(iStatInt6, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 6)
						AND NOT IS_BIT_SET(iStatInt7, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 16)
						AND NOT IS_BIT_SET(iStatInt7, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 26)
						AND NOT IS_BIT_SET(iStatInt7, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 6)
						AND NOT IS_BIT_SET(iStatInt8, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 16)
						AND NOT IS_BIT_SET(iStatInt8, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 26)
						AND NOT IS_BIT_SET(iStatInt8, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 6)
						AND NOT IS_BIT_SET(iStatInt9, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 16)
						AND NOT IS_BIT_SET(iStatInt9, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 26)
						AND NOT IS_BIT_SET(iStatInt9, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 6)
						AND NOT IS_BIT_SET(iStatInt10, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 16)
						AND NOT IS_BIT_SET(iStatInt10, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 26)
						AND NOT IS_BIT_SET(iStatInt10, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 6)
						AND NOT IS_BIT_SET(iStatInt11, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 16)
						AND NOT IS_BIT_SET(iStatInt11, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 26)
						AND NOT IS_BIT_SET(iStatInt11, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 6)
						AND NOT IS_BIT_SET(iStatInt12, 6)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 16)
						AND NOT IS_BIT_SET(iStatInt12, 16)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 26)
						AND NOT IS_BIT_SET(iStatInt12, 26)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO08")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 7)
						AND NOT IS_BIT_SET(iStatInt6, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 17)
						AND NOT IS_BIT_SET(iStatInt6, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 27)
						AND NOT IS_BIT_SET(iStatInt6, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 7)
						AND NOT IS_BIT_SET(iStatInt7, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 17)
						AND NOT IS_BIT_SET(iStatInt7, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 27)
						AND NOT IS_BIT_SET(iStatInt7, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 7)
						AND NOT IS_BIT_SET(iStatInt8, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 17)
						AND NOT IS_BIT_SET(iStatInt8, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 27)
						AND NOT IS_BIT_SET(iStatInt8, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 7)
						AND NOT IS_BIT_SET(iStatInt9, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 17)
						AND NOT IS_BIT_SET(iStatInt9, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 27)
						AND NOT IS_BIT_SET(iStatInt9, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 7)
						AND NOT IS_BIT_SET(iStatInt10, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 17)
						AND NOT IS_BIT_SET(iStatInt10, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 27)
						AND NOT IS_BIT_SET(iStatInt10, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 7)
						AND NOT IS_BIT_SET(iStatInt11, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 17)
						AND NOT IS_BIT_SET(iStatInt11, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 27)
						AND NOT IS_BIT_SET(iStatInt11, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 7)
						AND NOT IS_BIT_SET(iStatInt12, 7)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 17)
						AND NOT IS_BIT_SET(iStatInt12, 17)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 27)
						AND NOT IS_BIT_SET(iStatInt12, 27)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO09")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 8)
						AND NOT IS_BIT_SET(iStatInt6, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 18)
						AND NOT IS_BIT_SET(iStatInt6, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 28)
						AND NOT IS_BIT_SET(iStatInt6, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 8)
						AND NOT IS_BIT_SET(iStatInt7, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES H", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 18)
						AND NOT IS_BIT_SET(iStatInt7, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 28)
						AND NOT IS_BIT_SET(iStatInt7, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 8)
						AND NOT IS_BIT_SET(iStatInt8, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 18)
						AND NOT IS_BIT_SET(iStatInt8, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 28)
						AND NOT IS_BIT_SET(iStatInt8, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 8)
						AND NOT IS_BIT_SET(iStatInt9, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 18)
						AND NOT IS_BIT_SET(iStatInt9, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 28)
						AND NOT IS_BIT_SET(iStatInt9, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 8)
						AND NOT IS_BIT_SET(iStatInt10, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 18)
						AND NOT IS_BIT_SET(iStatInt10, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 28)
						AND NOT IS_BIT_SET(iStatInt10, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 8)
						AND NOT IS_BIT_SET(iStatInt11, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 18)
						AND NOT IS_BIT_SET(iStatInt11, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 28)
						AND NOT IS_BIT_SET(iStatInt11, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 8)
						AND NOT IS_BIT_SET(iStatInt12, 8)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 18)
						AND NOT IS_BIT_SET(iStatInt12, 18)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 28)
						AND NOT IS_BIT_SET(iStatInt12, 28)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE HASH("XMAS_CAMO10")
				SWITCH eVehicleModel
					
					CASE THRUSTER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 9)
						AND NOT IS_BIT_SET(iStatInt6, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK 
					
					CASE MOLOTOK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 19)
						AND NOT IS_BIT_SET(iStatInt6, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE KHANJALI
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, 29)
						AND NOT IS_BIT_SET(iStatInt6, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE STREITER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 9)
						AND NOT IS_BIT_SET(iStatInt7, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE RIOT2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 19)
						AND NOT IS_BIT_SET(iStatInt7, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK	
					
					CASE COMET4
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, 29)
						AND NOT IS_BIT_SET(iStatInt7, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE CHERNOBOG
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 9)
						AND NOT IS_BIT_SET(iStatInt8, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE BARRAGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 19)
						AND NOT IS_BIT_SET(iStatInt8, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AVENGER
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, 29)
						AND NOT IS_BIT_SET(iStatInt8, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERSMALL2
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 9)
						AND NOT IS_BIT_SET(iStatInt9, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TECHNICAL3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 19)
						AND NOT IS_BIT_SET(iStatInt9, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TAMPA3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, 29)
						AND NOT IS_BIT_SET(iStatInt9, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE OPPRESSOR
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 9)
						AND NOT IS_BIT_SET(iStatInt10, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE NIGHTSHARK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 19)
						AND NOT IS_BIT_SET(iStatInt10, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE INSURGENT3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, 29)
						AND NOT IS_BIT_SET(iStatInt10, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE HALFTRACK
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 9)
						AND NOT IS_BIT_SET(iStatInt11, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE DUNE3
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 19)
						AND NOT IS_BIT_SET(iStatInt11, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE ARDENT
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, 29)
						AND NOT IS_BIT_SET(iStatInt11, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE APC
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 9)
						AND NOT IS_BIT_SET(iStatInt12, 9)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE AKULA
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 19)
						AND NOT IS_BIT_SET(iStatInt12, 19)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
					
					CASE TRAILERLARGE
						IF !IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, 29)
						AND NOT IS_BIT_SET(iStatInt12, 29)
							CPRINTLN(DEBUG_SHOPS, "SHOULD_AWARD_XMAS_17_LIVERIES ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", GET_STRING_FROM_HASH_KEY(iLabelHash), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu))
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH	
	ENDIF
	
	RETURN SHOULD_XMAS_17_LIVERIES_LOCKED_TWO(eVehicleModel, iLabelHash, eMenu)
ENDFUNC

FUNC BOOL HAS_CARMOD_UNLOCK_BEEN_VIEWED(MODEL_NAMES eVehicleModel, CARMOD_UNLOCK_ITEMS eUnlock #IF USE_TU_CHANGES, STRING sLabel, INT iOption = 0, INT iControl = 0, CARMOD_MENU_ENUM eMenu = CMM_MOD #ENDIF , BOOL bNewStarSystem = FALSE)

	IF DISABLE_UNLOCK_STARS_FOR_VEHICLE(eVehicleModel)
		RETURN TRUE
	ENDIF
	
	IF SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
	AND IS_CARMOD_ITEM_UNLOCKED(eUnlock)
		IF eMenu = CMM_BUMPERS
		AND iControl = 2
		AND NOT bNewStarSystem
			iOption = iOption - 20
		ENDIF 
		IF eMenu = CMM_FENDERS
		AND iOption > WING_R_FIRST_MENU_ITEM
		AND !DOES_VEHICLE_HAVE_TWO_FENDERS_MENU(eVehicleModel)
			iOption = iOption - WING_R_FIRST_MENU_ITEM
		ENDIF 
		
		IF eVehicleModel = OPPRESSOR2
			IF eMenu = CMM_GRILL
				IF iOption > 2
					iOption = iOption - 26
				ENDIF
			ENDIF
		ELIF eVehicleModel = STRIKEFORCE
			IF eMenu = CMM_FRONTMUDGUARD
				IF iOption > 2
					iOption = iOption - 26
				ENDIF
			ENDIF
		ENDIF
		
		IF eMenu = CMM_WHEEL_ACCS
		AND iControl = 2
			IF ARE_STRINGS_EQUAL(sLabel, "CMOD_TYR_LG")
				IF IS_VEHICLE_A_CAR_MEET_VEHICLE(eVehicleModel)
				AND IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
					RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_LGT_ITEM_STAR)
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	IF eUnlock != DUMMY_CARMOD_UNLOCK_ITEM
		INT iVehicle = GET_VEHICLE_INT_FOR_SAVE_DATA(eVehicleModel)
		INT iIntToCheck = (ENUM_TO_INT(eUnlock)%32)
		IF iVehicle != -1
		AND !bNewStarSystem
			INT iBitset = ((iVehicle*((NUMBER_OF_CARMOD_UNLOCKS/32)+1)) + ENUM_TO_INT(eUnlock)/32)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF ((eVehicleModel = AVARUS
				OR eVehicleModel = CHIMERA
				OR eVehicleModel = ESSKEY)
				AND (eUnlock = CARMOD_UNLOCK_GRILL_L2))
				OR ((eUnlock = CARMOD_UNLOCK_SPOILER_L1 OR eUnlock =  CARMOD_UNLOCK_BONNET_L2) AND (eVehicleModel = ESSKEY))
					iIntToCheck--
				ENDIF
				IF (eVehicleModel = SPECTER2)
				AND (eUnlock = CARMOD_UNLOCK_CHASSIS_L2 OR eUnlock = CARMOD_UNLOCK_SPOILER_L1)
					iIntToCheck--
				ENDIF
				IF (eVehicleModel = TRAILERLARGE)
				AND (eUnlock = CARMOD_UNLOCK_ROOF_L1)
					iIntToCheck++
				ENDIF
				IF (eVehicleModel = BOMBUSHKA AND eUnlock = CARMOD_UNLOCK_CHASSIS_L2)
					iIntToCheck--
				ENDIF
				
				IF iIntToCheck >= 0 
					RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarmodsViewedBitset[iBitset], iIntToCheck )
				ENDIF	
			ELSE			
				#if USE_CLF_DLC
					RETURN IS_BIT_SET(g_savedGlobalsClifford.sShopData.iCarmodsViewedBitset[iBitset], iIntToCheck)
				#endif
				#if not USE_CLF_DLC
					RETURN IS_BIT_SET(g_savedGlobals.sShopData.iCarmodsViewedBitset[iBitset], iIntToCheck)
				#endif
			ENDIF
		ELSE
			IF SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
			AND IS_CARMOD_ITEM_UNLOCKED(eUnlock)
				INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
				IF iSupermodItem > 0
					RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32)
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
	
	
	// Check DLC items.
	#IF USE_TU_CHANGES
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF eUnlock = DUMMY_CARMOD_UNLOCK_ITEM
			OR (bNewStarSystem OR IS_CARMOD_ITEM_UNLOCKED(eUnlock))
				INT iVehicle = GET_VEHICLE_INT_FOR_SAVE_DATA(eVehicleModel)
				IF iVehicle != -1
					INT iBitset = (iVehicle*((MAX_DLC_CARMOD_ITEMS/32)+1))
					INT iDLCItem = GET_DLC_CARMOD_ITEM_INDEX(GET_HASH_KEY(sLabel))
					IF iDLCItem != -1
						RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCCarmodItemsViewed[iBitset+(iDLCItem/32)], iDLCItem%32)
					ENDIF
				ENDIF
				// Supermod
				INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
				IF iSupermodItem > 0
					RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32)
				ENDIF
			ENDIF
		ENDIF
	#ENDIF			
	RETURN TRUE
ENDFUNC


FUNC BOOL SHOULD_UNLOCK_STAR_BE_DISPLAYED(MODEL_NAMES eVehicleModel, CARMOD_UNLOCK_ITEMS eUnlock #IF USE_TU_CHANGES, STRING sLabel, INT iOption = 0, INT iControl = 0, CARMOD_MENU_ENUM eMenu = CMM_MOD #ENDIF)

	IF DISABLE_UNLOCK_STARS_FOR_VEHICLE(eVehicleModel)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
	AND IS_CARMOD_ITEM_UNLOCKED(eUnlock)
		IF eMenu = CMM_BUMPERS
		AND iControl = 2
			iOption = iOption - 20
		ENDIF 

		IF eMenu = CMM_FENDERS
		AND iOption > WING_R_FIRST_MENU_ITEM
		AND !DOES_VEHICLE_HAVE_TWO_FENDERS_MENU(eVehicleModel)
			iOption = iOption - WING_R_FIRST_MENU_ITEM
		ENDIF 

		IF eVehicleModel = OPPRESSOR2
			IF eMenu = CMM_GRILL
				IF iOption > 2
					iOption = iOption - 26
				ENDIF
			ENDIF
		ELIF eVehicleModel = STRIKEFORCE
			IF eMenu = CMM_FRONTMUDGUARD
				IF iOption > 2
					iOption = iOption - 26
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eUnlock != DUMMY_CARMOD_UNLOCK_ITEM	
	AND NOT SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
		#IF NOT USE_TU_CHANGES
			IF IS_CARMOD_ITEM_UNLOCKED(eUnlock)
			AND NOT HAS_CARMOD_UNLOCK_BEEN_VIEWED(eVehicleModel, eUnlock, DEFAULT ,DEFAULT, DEFAULT, DEFAULT, SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel))
				RETURN TRUE
			ENDIF
		#ENDIF
		
		#IF USE_TU_CHANGES
			IF IS_CARMOD_ITEM_UNLOCKED(eUnlock)
			AND NOT HAS_CARMOD_UNLOCK_BEEN_VIEWED(eVehicleModel, eUnlock, "", DEFAULT, DEFAULT, DEFAULT, SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel))
				RETURN TRUE
			ENDIF
		#ENDIF
	ENDIF
	
	// Check DLC items.
	#IF USE_TU_CHANGES
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF eUnlock = DUMMY_CARMOD_UNLOCK_ITEM
			AND NOT SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
				INT iVehicle = GET_VEHICLE_INT_FOR_SAVE_DATA(eVehicleModel)
				IF iVehicle != -1
					INT iBitset = (iVehicle*((MAX_DLC_CARMOD_ITEMS/32)+1))
					INT iDLCItem = GET_DLC_CARMOD_ITEM_INDEX(GET_HASH_KEY(sLabel))
					IF iDLCItem != -1
						RETURN (NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCCarmodItemsViewed[iBitset+(iDLCItem/32)], iDLCItem%32))
					ENDIF
				ENDIF
				// Supermod
				INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
				IF iSupermodItem > 0
					RETURN (NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32))
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	IF SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
	AND (IS_CARMOD_ITEM_UNLOCKED(eUnlock) OR eUnlock = DUMMY_CARMOD_UNLOCK_ITEM)
		INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
		IF iSupermodItem > 0
			IF NOT  IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF eMenu = CMM_WHEEL_ACCS
		AND iControl = 2
			IF ARE_STRINGS_EQUAL(sLabel, "CMOD_TYR_LG")
				IF IS_VEHICLE_A_CAR_MEET_VEHICLE(eVehicleModel)
				AND IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_LGT_ITEM_STAR)
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BIKE_FOR_BIKER_PACK(VEHICLE_INDEX mVeh)
	IF IS_VEHICLE_DRIVEABLE(mVeh)
		IF (GET_ENTITY_MODEL(mVeh) = AVARUS)
		OR (GET_ENTITY_MODEL(mVeh) = CHIMERA)
		OR (GET_ENTITY_MODEL(mVeh) = DAEMON2)
		OR (GET_ENTITY_MODEL(mVeh) = DEFILER)
		OR (GET_ENTITY_MODEL(mVeh) = ESSKEY)
		OR (GET_ENTITY_MODEL(mVeh) = NIGHTBLADE)
		OR (GET_ENTITY_MODEL(mVeh) = RATBIKE)
		OR (GET_ENTITY_MODEL(mVeh) = ZOMBIEA)
		OR (GET_ENTITY_MODEL(mVeh) = ZOMBIEB)
		OR (GET_ENTITY_MODEL(mVeh) = SANCTUS)
		OR (GET_ENTITY_MODEL(mVeh) = MANCHEZ)
		OR (GET_ENTITY_MODEL(mVeh) = VORTEX)
		OR (GET_ENTITY_MODEL(mVeh) = WOLFSBANE)
		OR (GET_ENTITY_MODEL(mVeh) = FAGGIO3)
		OR (GET_ENTITY_MODEL(mVeh) = FAGGIO)
		OR (GET_ENTITY_MODEL(mVeh) = HAKUCHOU2)
		OR (GET_ENTITY_MODEL(mVeh) = BLAZER4)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAR_CARMOD_UNLOCK_VIEW_STARS()
	INT i
	INT gs
	INT countof = (VEHICLE_COUNT_FOR_STAR_ICON)*((NUMBER_OF_CARMOD_UNLOCKS/32)+1)
	 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		gs = GET_SAVE_GAME_ARRAY_SLOT()
		
		REPEAT countof i
			g_savedMPGlobalsNew.g_savedMPGlobals[gs].MpSavedGeneral.iCarmodsViewedBitset[i] = 0
		ENDREPEAT
	ELSE
		REPEAT countof i
			#if USE_CLF_DLC
				g_savedGlobalsClifford.sShopData.iCarmodsViewedBitset[i] = 0
			#endif
			#if not USE_CLF_DLC
				g_savedGlobals.sShopData.iCarmodsViewedBitset[i] = 0
			#endif
		ENDREPEAT	
	ENDIF
	
	// Check DLC items.
	#IF USE_TU_CHANGES
		IF NETWORK_IS_GAME_IN_PROGRESS()
			countof = (VEHICLE_COUNT_FOR_STAR_ICON)*((MAX_DLC_CARMOD_ITEMS/32)+1)
			gs = GET_SAVE_GAME_ARRAY_SLOT()	
			REPEAT countof i
				g_savedMPGlobalsNew.g_savedMPGlobals[gs].MpSavedGeneral.iDLCCarmodItemsViewed[i] = 0
			ENDREPEAT
			// Supermod
			countof = ((MAX_DLC_SUPERMOD_ITEMS/32)+1)
			gs = GET_SAVE_GAME_ARRAY_SLOT()	
			REPEAT countof i
				g_savedMPGlobalsNew.g_savedMPGlobals[gs].MpSavedGeneral.iDLCSupermodItemsViewed[i] = 0
			ENDREPEAT
		ENDIF
	#ENDIF
ENDPROC
		
PROC SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(MODEL_NAMES eVehicleModel, CARMOD_UNLOCK_ITEMS eUnlock #IF USE_TU_CHANGES, STRING sLabel, INT iOption = 0, INT iControl = 0, CARMOD_MENU_ENUM eMenu = CMM_MOD #ENDIF)
	
	IF SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
	AND IS_CARMOD_ITEM_UNLOCKED(eUnlock)
		IF eMenu = CMM_BUMPERS
		AND iControl = 2
			iOption = iOption - 20
		ENDIF 
		IF eMenu = CMM_FENDERS
		AND iOption > WING_R_FIRST_MENU_ITEM
		AND !DOES_VEHICLE_HAVE_TWO_FENDERS_MENU(eVehicleModel)
			iOption = iOption - WING_R_FIRST_MENU_ITEM 
		ENDIF 
		
		IF eVehicleModel = OPPRESSOR2
			IF eMenu = CMM_GRILL
				IF iOption > 2
					iOption = iOption - 26
				ENDIF
			ENDIF
		ELIF eVehicleModel = STRIKEFORCE
			IF eMenu = CMM_FRONTMUDGUARD
				IF iOption > 2
					iOption = iOption - 26
				ENDIF
			ENDIF
		ENDIF
		
		IF eMenu = CMM_WHEEL_ACCS
		AND iControl = 2
			IF ARE_STRINGS_EQUAL(sLabel, "CMOD_TYR_LG")
				IF IS_VEHICLE_A_CAR_MEET_VEHICLE(eVehicleModel)
				AND IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_LGT_ITEM_STAR)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_LGT_ITEM_STAR, TRUE)
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	IF eUnlock = DUMMY_CARMOD_UNLOCK_ITEM
		IF !SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
			// Check DLC items.
			#IF USE_TU_CHANGES
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
						INT iVehicle
						REPEAT VEHICLE_COUNT_FOR_STAR_ICON iVehicle
							INT iBitset = (iVehicle*((MAX_DLC_CARMOD_ITEMS/32)+1))
							INT iDLCItem = GET_DLC_CARMOD_ITEM_INDEX(GET_HASH_KEY(sLabel))
							IF iDLCItem != -1
								SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCCarmodItemsViewed[iBitset+(iDLCItem/32)], iDLCItem%32)
							ENDIF
						ENDREPEAT
					ELSE
						INT iVehicle = GET_VEHICLE_INT_FOR_SAVE_DATA(eVehicleModel)
						IF iVehicle != -1
							INT iBitset = (iVehicle*((MAX_DLC_CARMOD_ITEMS/32)+1))
							INT iDLCItem = GET_DLC_CARMOD_ITEM_INDEX(GET_HASH_KEY(sLabel))
							IF iDLCItem != -1

								SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCCarmodItemsViewed[iBitset+(iDLCItem/32)], iDLCItem%32)
							ENDIF
						ENDIF
						// Supermod
						INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
						IF iSupermodItem > 0
							SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32)
						ENDIF
					ENDIF	
				ENDIF
			#ENDIF
			
			EXIT
		ELSE
			IF IS_CARMOD_ITEM_UNLOCKED(eUnlock)
			OR eUnlock = DUMMY_CARMOD_UNLOCK_ITEM
				INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
				IF iSupermodItem > 0
					SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32)
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF SHOULD_USE_LIMITED_STAR_SYSTEM(eVehicleModel)
		AND (IS_CARMOD_ITEM_UNLOCKED(eUnlock))
			INT iSupermodItem = GET_DLC_SUPERMOD_ITEM_INDEX(eVehicleModel, iOption, iControl, eMenu)
			IF iSupermodItem > 0
				SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDLCSupermodItemsViewed[(iSupermodItem/32)], iSupermodItem%32)
			ENDIF
		ENDIF	
	
	ENDIF
	
	IF eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		INT iVehicle
		REPEAT VEHICLE_COUNT_FOR_STAR_ICON iVehicle
			IF IS_CARMOD_ITEM_UNLOCKED(eUnlock)
				INT iBitset = ((iVehicle*((NUMBER_OF_CARMOD_UNLOCKS/32)+1)) + ENUM_TO_INT(eUnlock)/32)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarmodsViewedBitset[iBitset], (ENUM_TO_INT(eUnlock)%32))
				ELSE
					#if USE_CLF_DLC
						SET_BIT(g_savedGlobalsClifford.sShopData.iCarmodsViewedBitset[iBitset], (ENUM_TO_INT(eUnlock)%32))
					#endif
					#if not USE_CLF_DLC
						SET_BIT(g_savedGlobals.sShopData.iCarmodsViewedBitset[iBitset], (ENUM_TO_INT(eUnlock)%32))
					#endif
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF IS_CARMOD_ITEM_UNLOCKED(eUnlock)
			INT iVehicle = GET_VEHICLE_INT_FOR_SAVE_DATA(eVehicleModel)
			IF iVehicle != -1
				INT iBitset = ((iVehicle*((NUMBER_OF_CARMOD_UNLOCKS/32)+1)) + ENUM_TO_INT(eUnlock)/32)
				INT iIntToSet = (ENUM_TO_INT(eUnlock)%32)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF ((eVehicleModel = AVARUS
					OR eVehicleModel = CHIMERA
					OR eVehicleModel = ESSKEY)
					AND (eUnlock = CARMOD_UNLOCK_GRILL_L2))
					OR ((eUnlock = CARMOD_UNLOCK_SPOILER_L1 OR eUnlock =  CARMOD_UNLOCK_BONNET_L2) AND (eVehicleModel = ESSKEY))
						iIntToSet--
					ENDIF
					IF (eVehicleModel = SPECTER2)
					AND (eUnlock = CARMOD_UNLOCK_CHASSIS_L2 OR eUnlock = CARMOD_UNLOCK_SPOILER_L1)
						iIntToSet--
					ENDIF
					IF (eVehicleModel = TRAILERLARGE)
					AND (eUnlock = CARMOD_UNLOCK_ROOF_L1)
						iIntToSet++
					ENDIF
					IF (eVehicleModel = BOMBUSHKA AND eUnlock = CARMOD_UNLOCK_CHASSIS_L2)
						iIntToSet--
					ENDIF
					IF iIntToSet >= 0 
						SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarmodsViewedBitset[iBitset], iIntToSet)
					ENDIF
				ELSE
					#if USE_CLF_DLC
						SET_BIT(g_savedGlobalsClifford.sShopData.iCarmodsViewedBitset[iBitset], iIntToSet)
					#endif
					#if not USE_CLF_DLC
						SET_BIT(g_savedGlobals.sShopData.iCarmodsViewedBitset[iBitset], iIntToSet)
					#endif
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC CARMOD_UNLOCK_ITEMS GET_CARMOD_UNLOCK_ITEM_FOR_MOD_SLOT(INT iItem, MOD_TYPE eMod, VEHICLE_INDEX vehicle)
	SWITCH eMod
		CASE MOD_SPOILER
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM	BREAK
				CASE 1	RETURN CARMOD_UNLOCK_SPOILER_L1	BREAK
				CASE 2	RETURN CARMOD_UNLOCK_SPOILER_L2	BREAK
				CASE 3	RETURN CARMOD_UNLOCK_SPOILER_L3	BREAK
				CASE 4	RETURN CARMOD_UNLOCK_SPOILER_L4	BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_SPOILER_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_BUMPER_F
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_F_BUMPER_L2	BREAK
				CASE 2	RETURN CARMOD_UNLOCK_F_BUMPER_L3	BREAK
				CASE 3	RETURN CARMOD_UNLOCK_F_BUMPER_L4	BREAK
				CASE 4	RETURN CARMOD_UNLOCK_F_BUMPER_L5	BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_F_BUMPER_L6	ELSE RETURN CARMOD_UNLOCK_F_BUMPER_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_BUMPER_R
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_R_BUMPER_L2	BREAK
				CASE 2	RETURN CARMOD_UNLOCK_R_BUMPER_L3	BREAK
				CASE 3	RETURN CARMOD_UNLOCK_R_BUMPER_L4	BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem
						RETURN CARMOD_UNLOCK_R_BUMPER_L5
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_SKIRT
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_SKIRTS_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_SKIRTS_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_SKIRTS_L4		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_SKIRTS_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_EXHAUST
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_EXHAUST_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_EXHAUST_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_EXHAUST_L4		BREAK
				CASE 4	RETURN CARMOD_UNLOCK_EXHAUST_L5		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_EXHAUST_L6	ELSE RETURN CARMOD_UNLOCK_EXHAUST_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_CHASSIS
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_CHASSIS_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_CHASSIS_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_CHASSIS_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_CHASSIS_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_GRILL
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_GRILL_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_GRILL_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_GRILL_L4		BREAK
				CASE 4	RETURN CARMOD_UNLOCK_GRILL_L5		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_GRILL_L6	ELSE RETURN CARMOD_UNLOCK_GRILL_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_BONNET
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_BONNET_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_BONNET_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_BONNET_L4		BREAK
				CASE 4	RETURN CARMOD_UNLOCK_BONNET_L5		BREAK
				CASE 5	RETURN CARMOD_UNLOCK_BONNET_L6		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_BONNET_L7 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_ROOF
			IF GET_ENTITY_MODEL(vehicle) = DUKES
				CDEBUG1LN(DEBUG_SHOPS, "GET_CARMOD_UNLOCK_ITEM_FOR_MOD_SLOT(DUKES, MOD_ROOF, ", iItem, ")")
				
				SWITCH iItem
					CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
					CASE 1	RETURN CARMOD_UNLOCK_ROOF_L1		BREAK
					DEFAULT
						RETURN DUMMY_CARMOD_UNLOCK_ITEM
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iItem
					CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
					CASE 1	RETURN CARMOD_UNLOCK_ROOF_L1		BREAK
					CASE 2	RETURN CARMOD_UNLOCK_ROOF_L2		BREAK
					CASE 3	RETURN CARMOD_UNLOCK_ROOF_L3		BREAK
					CASE 4	RETURN CARMOD_UNLOCK_ROOF_L4		BREAK
					DEFAULT
						IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem
							IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_ROOF_L5	ELSE RETURN CARMOD_UNLOCK_ROOF_L4 ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE MOD_ENGINE
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_ENGINE_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_ENGINE_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_ENGINE_L4		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_ENGINE_L4 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_BRAKES
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM		BREAK
				CASE 1	RETURN CARMOD_UNLOCK_BRAKES_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_BRAKES_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_BRAKES_L4		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_BRAKES_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_GEARBOX
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM			BREAK
				CASE 1	RETURN CARMOD_UNLOCK_GEAR_BOX_L2		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_GEAR_BOX_L3		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_GEAR_BOX_L4		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem 	RETURN CARMOD_UNLOCK_GEAR_BOX_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_SUSPENSION
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM			BREAK
				CASE 1	RETURN CARMOD_UNLOCK_SUSPENSION_L1		BREAK
				CASE 2	RETURN CARMOD_UNLOCK_SUSPENSION_L2		BREAK
				CASE 3	RETURN CARMOD_UNLOCK_SUSPENSION_L3		BREAK
				CASE 4	RETURN CARMOD_UNLOCK_SUSPENSION_L4		BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, eMod) >= iItem
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_SUSPENSION_L5	ELSE RETURN CARMOD_UNLOCK_SUSPENSION_L4 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_WING_L
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_L_WING_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_L_WING_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_L_WING_L4 BREAK
				DEFAULT
					RETURN CARMOD_UNLOCK_L_WING_L5
				BREAK
			ENDSWITCH
		BREAK
		CASE MOD_WING_R
			SWITCH iItem
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_R_WING_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_R_WING_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_R_WING_L4 BREAK
				DEFAULT
					RETURN CARMOD_UNLOCK_R_WING_L5 
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN DUMMY_CARMOD_UNLOCK_ITEM
ENDFUNC




FUNC CARMOD_UNLOCK_ITEMS GET_CARMOD_UNLOCK_ITEM_FOR_MENU_ITEM(CARMOD_MENU_ENUM eMenu, MOD_TYPE eModSlot, INT iOption, VEHICLE_INDEX vehicle)
	
	IF DOES_ENTITY_EXIST(vehicle)
	AND NOT IS_ENTITY_DEAD(vehicle)
		SWITCH GET_ENTITY_MODEL(vehicle)
			CASE POUNDER2
			CASE MULE4
			CASE SPEEDO4
			CASE TERBYTE
				RETURN DUMMY_CARMOD_UNLOCK_ITEM
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH eMenu
		CASE CMM_ARMOUR
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_BODY_ARMOUR_20 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_BODY_ARMOUR_40 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_BODY_ARMOUR_60 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_BODY_ARMOUR_80 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_ARMOUR) >= iOption 	RETURN CARMOD_UNLOCK_BODY_ARMOUR_100 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_BRAKES
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_BRAKES_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_BRAKES_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_BRAKES_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_BRAKES) >= iOption 	RETURN CARMOD_UNLOCK_BRAKES_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_HOOD
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_BONNET_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_BONNET_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_BONNET_L4 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_BONNET_L5 BREAK
				CASE 5	RETURN CARMOD_UNLOCK_BONNET_L6 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_BONNET) >= iOption 	RETURN CARMOD_UNLOCK_BONNET_L7 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_BUMPERS
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_F_BUMPER_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_F_BUMPER_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_F_BUMPER_L4 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_F_BUMPER_L5 BREAK
				
				CASE 20	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 21	RETURN CARMOD_UNLOCK_R_BUMPER_L2 BREAK
				CASE 22	RETURN CARMOD_UNLOCK_R_BUMPER_L3 BREAK
				CASE 23	RETURN CARMOD_UNLOCK_R_BUMPER_L4 BREAK
			ENDSWITCH
			
			IF iOption > 4 AND iOption < 20
				IF GET_NUM_VEHICLE_MODS(vehicle, MOD_BUMPER_F) >= iOption
					IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_F_BUMPER_L6	ELSE RETURN CARMOD_UNLOCK_F_BUMPER_L5 ENDIF
				ENDIF
			ELIF iOption > 23
				IF GET_NUM_VEHICLE_MODS(vehicle, MOD_BUMPER_R) >= (iOption-20)
					RETURN CARMOD_UNLOCK_R_BUMPER_L5
				ENDIF
			ENDIF
				
		BREAK
		CASE CMM_CHASSIS
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_CHASSIS_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_CHASSIS_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_CHASSIS_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_CHASSIS) >= iOption 	RETURN CARMOD_UNLOCK_CHASSIS_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ENGINE
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_ENGINE_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_ENGINE_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_ENGINE_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_ENGINE) >= iOption 	RETURN CARMOD_UNLOCK_ENGINE_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_EXHAUST
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_EXHAUST_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_EXHAUST_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_EXHAUST_L4 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_EXHAUST_L5 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_EXHAUST) >= iOption
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_EXHAUST_L6	ELSE RETURN CARMOD_UNLOCK_EXHAUST_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_EXPLOSIVES
			IF NETWORK_IS_GAME_IN_PROGRESS() AND (INT_TO_ENUM(FIXER_PAYPHONE_VARIATION, GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID())) = FPV_MOTEL)
				RETURN DUMMY_CARMOD_UNLOCK_ITEM
			ELSE	
				SWITCH iOption
					CASE 0	RETURN CARMOD_UNLOCK_IGNITION_BOMB BREAK
					CASE 1	RETURN CARMOD_UNLOCK_TIMED_BOMB BREAK
				ENDSWITCH
			ENDIF	
		BREAK
		CASE CMM_TRANSMISSION
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_GEAR_BOX_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_GEAR_BOX_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_GEAR_BOX_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_GEARBOX) >= iOption 	RETURN CARMOD_UNLOCK_GEAR_BOX_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_GRILL
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_GRILL_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_GRILL_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_GRILL_L4 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_GRILL_L5 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_GRILL) >= iOption
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_GRILL_L6	ELSE RETURN CARMOD_UNLOCK_GRILL_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_HORN
			CARMOD_HORN_ENUM eHorn
			eHorn = INT_TO_ENUM(CARMOD_HORN_ENUM, iOption)
			SWITCH eHorn
				CASE HORN_STOCK							RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE HORN_TRUCK							RETURN CARMOD_UNLOCK_HORN_L2 BREAK
				CASE HORN_COP							RETURN CARMOD_UNLOCK_HORN_L3 BREAK
				CASE HORN_CLOWN							RETURN CARMOD_UNLOCK_HORN_L4 BREAK
				CASE HORN_MUSICAL_1						RETURN CARMOD_UNLOCK_HORN_L5 BREAK
				CASE HORN_MUSICAL_2						RETURN CARMOD_UNLOCK_HORN_L6 BREAK
				CASE HORN_MUSICAL_3						RETURN CARMOD_UNLOCK_HORN_L7 BREAK
				CASE HORN_MUSICAL_4						RETURN CARMOD_UNLOCK_HORN_L8 BREAK
				CASE HORN_MUSICAL_5						RETURN CARMOD_UNLOCK_HORN_L9 BREAK
				CASE HORN_SAD_TROMBONE					RETURN CARMOD_UNLOCK_HORN_L9 BREAK
			ENDSWITCH
		BREAK
		CASE CMM_LIGHTS_HEAD
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_XENON_LIGHTS BREAK
			ENDSWITCH
		BREAK
		CASE CMM_PLATES
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_PLATES_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_PLATES_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_PLATES_L4 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_PLATES_L5 BREAK
				CASE 5	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ROOF
			IF GET_ENTITY_MODEL(vehicle) = DUKES
				CDEBUG1LN(DEBUG_SHOPS, "GET_CARMOD_UNLOCK_ITEM_FOR_MENU_ITEM(DUKES, CMM_ROOF, ", iOption, ")")
				
				SWITCH iOption
					CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
					CASE 1	RETURN CARMOD_UNLOCK_ROOF_L1 BREAK
					DEFAULT
						RETURN DUMMY_CARMOD_UNLOCK_ITEM
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
					CASE 1	RETURN CARMOD_UNLOCK_ROOF_L1 BREAK
					CASE 2	RETURN CARMOD_UNLOCK_ROOF_L2 BREAK
					CASE 3	RETURN CARMOD_UNLOCK_ROOF_L3 BREAK
					CASE 4	RETURN CARMOD_UNLOCK_ROOF_L4 BREAK
					DEFAULT
						IF GET_NUM_VEHICLE_MODS(vehicle, MOD_ROOF) >= iOption
							IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_ROOF_L5	ELSE RETURN CARMOD_UNLOCK_ROOF_L4 ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_SKIRTS
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_SKIRTS_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_SKIRTS_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_SKIRTS_L4 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_SKIRTS_L5 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_SKIRT) > 4
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_SKIRTS_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_SPOILER
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_SPOILER_L1 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_SPOILER_L2 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_SPOILER_L3 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_SPOILER_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_SPOILER) >= iOption 	RETURN CARMOD_UNLOCK_SPOILER_L5 ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_SUSPENSION
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_SUSPENSION_L1 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_SUSPENSION_L2 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_SUSPENSION_L3 BREAK
				CASE 4	RETURN CARMOD_UNLOCK_SUSPENSION_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_SKIRT) >= iOption
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_SUSPENSION_L5	ELSE RETURN CARMOD_UNLOCK_SUSPENSION_L4 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CMM_TURBO
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_TURBO BREAK
			ENDSWITCH
		BREAK
		CASE CMM_WHEEL_ACCS
			SWITCH iOption
				CASE WHEEL_ACCS_DESIGN_STOCK	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_1
					IF NETWORK_IS_GAME_IN_PROGRESS()
						RETURN CARMOD_UNLOCK_CUSTOM_TYRES
					ELSE
						RETURN DUMMY_CARMOD_UNLOCK_ITEM
					ENDIF
				BREAK
				
				// Bennys:
				CASE WHEEL_ACCS_DESIGN_CUSTOM_2
				CASE WHEEL_ACCS_DESIGN_CUSTOM_3
				CASE WHEEL_ACCS_DESIGN_CUSTOM_4
				CASE WHEEL_ACCS_DESIGN_CUSTOM_5
				CASE WHEEL_ACCS_DESIGN_CUSTOM_6
				CASE WHEEL_ACCS_DESIGN_CUSTOM_7
				CASE WHEEL_ACCS_DESIGN_CUSTOM_8
				CASE WHEEL_ACCS_DESIGN_CUSTOM_9
					RETURN DUMMY_CARMOD_UNLOCK_ITEM
				BREAK
				
				CASE WHEEL_ACCS_OPTION_BPT		RETURN CARMOD_UNLOCK_BULLET_PROOF_TYRES BREAK
				CASE WHEEL_ACCS_OPTION_LGT		RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				
				CASE WHEEL_ACCS_SMOKE_WHITE		RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE WHEEL_ACCS_SMOKE_BLACK		RETURN CARMOD_UNLOCK_TYRE_SMOKE_BLACK BREAK
				CASE WHEEL_ACCS_SMOKE_BLUE		RETURN CARMOD_UNLOCK_TYRE_SMOKE_BLUE BREAK
				CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN CARMOD_UNLOCK_TYRE_SMOKE_YELLOW BREAK
				CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK // Business 1 - Purple tyre smoke
				CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN CARMOD_UNLOCK_TYRE_SMOKE_ORANGE BREAK
				CASE WHEEL_ACCS_SMOKE_GREEN		RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK // Business 1 - Green tyre smoke
				CASE WHEEL_ACCS_SMOKE_RED		RETURN CARMOD_UNLOCK_TYRE_SMOKE_RED BREAK
				CASE WHEEL_ACCS_SMOKE_PINK		RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK // Hipster 1 - Pink tyre smoke
				CASE WHEEL_ACCS_SMOKE_BROWN		RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK // Hipster 2 - Brown tyre smoke
				CASE WHEEL_ACCS_SMOKE_INDI		RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK // Indi - red, white, and blue
			ENDSWITCH
		BREAK
		CASE CMM_WINDOWS
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_LIGHT_SMOKE_WINDOWS BREAK
				CASE 2	RETURN CARMOD_UNLOCK_DARK_SMOKE_WINDOWS BREAK
				CASE 3	RETURN CARMOD_UNLOCK_LIMO_WINDOWS BREAK
			ENDSWITCH
		BREAK
		CASE CMM_FENDERSTWO
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_L_WING_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_L_WING_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_L_WING_L4 BREAK
				DEFAULT
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_WING_L) >= iOption
						IF NETWORK_IS_GAME_IN_PROGRESS() RETURN CARMOD_UNLOCK_L_WING_L5 ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CMM_FENDERS
			IF IS_BIKE_FOR_BIKER_PACK(vehicle)
				iOption = iOption - 20
			ENDIF
			SWITCH iOption
				CASE 0	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 1	RETURN CARMOD_UNLOCK_L_WING_L2 BREAK
				CASE 2	RETURN CARMOD_UNLOCK_L_WING_L3 BREAK
				CASE 3	RETURN CARMOD_UNLOCK_L_WING_L4 BREAK
				
				CASE 20	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 21	RETURN DUMMY_CARMOD_UNLOCK_ITEM BREAK
				CASE 22	RETURN CARMOD_UNLOCK_R_WING_L2 BREAK
				CASE 23	RETURN CARMOD_UNLOCK_R_WING_L3 BREAK
				CASE 24	RETURN CARMOD_UNLOCK_R_WING_L4 BREAK
			ENDSWITCH
			
			IF !IS_BIKE_FOR_BIKER_PACK(vehicle)
				IF iOption > 3 AND iOption < 21
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_WING_L) >= iOption
						RETURN CARMOD_UNLOCK_L_WING_L5
					ENDIF
				ELIF iOption > 23
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_WING_R) >= (iOption-20)
						RETURN CARMOD_UNLOCK_R_WING_L5
					ENDIF
				ENDIF
			ELSE
				IF iOption > 23
					IF GET_NUM_VEHICLE_MODS(vehicle, MOD_WING_R) >= (iOption-20)
						RETURN CARMOD_UNLOCK_R_WING_L5
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CMM_MAIN
		CASE CMM_BUY
		CASE CMM_MOD
		CASE CMM_MAINTAIN
		CASE CMM_GOLD_PREP
		CASE CMM_GOLD_PREP2
		CASE CMM_INSURANCE
		CASE CMM_PAINT_JOB
		CASE CMM_SELL
		CASE CMM_VIN
		CASE CMM_WHEELS_MAIN
		CASE CMM_WHEELS
		CASE CMM_WHEEL_COL
		CASE CMM_SUPERMOD
		CASE CMM_SUPERMOD_TWO
		CASE CMM_SUPERMOD_PLTHOLDER
		CASE CMM_SUPERMOD_PLTVANITY
		CASE CMM_SUPERMOD_INTERIOR1
		CASE CMM_SUPERMOD_INTERIOR2
		CASE CMM_SUPERMOD_INTERIOR3
		CASE CMM_SUPERMOD_INTERIOR4
		CASE CMM_SUPERMOD_INTERIOR5
		CASE CMM_SUPERMOD_SEATS
		CASE CMM_SUPERMOD_STEERING
		CASE CMM_SUPERMOD_KNOB
		CASE CMM_SUPERMOD_PLAQUE
		CASE CMM_SUPERMOD_ICE
		CASE CMM_SUPERMOD_TRUNK
		CASE CMM_SUPERMOD_HYDRO
		CASE CMM_SUPERMOD_ENGINEBAY1
		CASE CMM_SUPERMOD_ENGINEBAY2
		CASE CMM_SUPERMOD_ENGINEBAY3
		CASE CMM_SUPERMOD_CHASSIS2
		CASE CMM_SUPERMOD_CHASSIS3
		CASE CMM_SUPERMOD_CHASSIS4
		CASE CMM_SUPERMOD_CHASSIS5
		CASE CMM_SUPERMOD_DOOR_L
		CASE CMM_SUPERMOD_DOOR_R
		CASE CMM_SUPERMOD_LIVERY
		CASE CMM_CHASSIS_GROUP
		CASE CMM_INTERIOR_GROUP
		CASE CMM_PLATE_GROUP
		CASE CMM_ENGINE_GROUP
		CASE CMM_DIALS
		CASE CMM_TRIM
		CASE CMM_LIGHT_COLOUR
			RETURN DUMMY_CARMOD_UNLOCK_ITEM
		BREAK
	ENDSWITCH
	
	// Try the mod slot instead.
	RETURN GET_CARMOD_UNLOCK_ITEM_FOR_MOD_SLOT(iOption, eModSlot, vehicle)
ENDFUNC

FUNC BOOL SHOULD_DISCOUNT_BE_DISPLAYED_FOR_CARMOD(VEHICLE_INDEX vehicle, STRING sLabel, CARMOD_MENU_ENUM eMenu, MOD_TYPE eModSlot, INT iControl, STRING sDebug, INT iCurrentMod, SHOP_NAME_ENUM eShop, INT iPersonalCarModVar)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(vehicle)
	
	//custom menu option overrides
	IF (eMenu = CMM_PAINT_JOB) AND ARE_STRINGS_EQUAL(sLabel, "CMOD_MOD_TRIM3") AND (eVehicleModel = PROTOTIPO)
		eMenu = CMM_LIGHT_COLOUR
	ENDIF
	
	IF NOT IS_CARMOD_ITEM_UNLOCKED(GET_CARMOD_UNLOCK_ITEM_FOR_MENU_ITEM(eMenu, eModSlot, iCurrentMod, vehicle), eShop, vehicle, iPersonalCarModVar)
	OR IS_CARMOD_ITEM_LOCKED_FOR_RESEARCH(GET_ENTITY_MODEL(vehicle), eModSlot, iCurrentMod)
		IF iControl != -1
			CDEBUG1LN(DEBUG_SHOPS, "should_discount_be_displayed_for_carmod ITEM NOT UNOCKED RETURNING FALSE. UNLOCK ITEM: ", GET_CARMOD_NAME(GET_CARMOD_UNLOCK_ITEM_FOR_MENU_ITEM(eMenu, eModSlot, iCurrentMod, vehicle)), " ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sLabel), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu), " ", iControl, " - ", sDebug)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bALL_VEHICLE_MODS_FOR_SALE
	AND NOT (eMenu = CMM_SELL OR eMenu = CMM_MAINTAIN)
		CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_CARMOD ALL ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sLabel), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu), " ", iControl, " - ", sDebug)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_sMPTunables.iSALE_CARMOD_MENU_BITSET[ENUM_TO_INT(eMenu)/32], ENUM_TO_INT(eMenu)%32)
		CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_CARMOD BITSET [", ENUM_TO_INT(eMenu)/32, ", ", ENUM_TO_INT(eMenu)%32, "] ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sLabel), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu), " - ", sDebug)
		RETURN TRUE
	ENDIF
	
	INT i, iLabelHash = GET_HASH_KEY(sLabel)
	REPEAT COUNT_OF(g_sMPTunables.iINDIVIDUAL_VEHICLE_SALE_HASH_LABELS) i
		IF (iLabelHash = g_sMPTunables.iINDIVIDUAL_VEHICLE_SALE_HASH_LABELS[i])
			CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_CARMOD HASHLABEL[", i, "] ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sLabel), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu), " ", iControl, " - ", sDebug)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SHOPS, "should_discount_be_displayed_for_carmod ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), ", ", sLabel, " \"", GET_STRING_FROM_TEXT_FILE(sLabel), "\", ", debug_GET_CARMOD_MENU_NAME(eMenu), " ", iControl, " - ", sDebug)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_XMAS_LIVERIES_FREE(VEHICLE_INDEX vehIndex, INT iLabelHash, CARMOD_MENU_ENUM eMenu #IF FEATURE_DLC_1_2022 , INT iModIndex #ENDIF)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(vehIndex)
	
	SWITCH eMenu
		CASE CMM_SUPERMOD_LIVERY
			SWITCH iLabelHash
				CASE HASH("XMAS_CAMO01")
				CASE HASH("XMAS_CAMO02")
				CASE HASH("XMAS_CAMO03")
				CASE HASH("XMAS_CAMO04")
				CASE HASH("XMAS_CAMO05")
				CASE HASH("XMAS_CAMO06")
				CASE HASH("XMAS_CAMO07")
				CASE HASH("XMAS_CAMO08")
				CASE HASH("XMAS_CAMO09")
				CASE HASH("XMAS_CAMO10")
					SWITCH eVehicleModel
						CASE THRUSTER
						CASE MOLOTOK
						CASE KHANJALI
						CASE STREITER
						CASE RIOT2
						CASE COMET4
						CASE CHERNOBOG		
						CASE BARRAGE
						CASE AVENGER
						CASE TRAILERSMALL2
						CASE TECHNICAL3
						CASE TAMPA3
						CASE OPPRESSOR
						CASE NIGHTSHARK
						CASE INSURGENT3
						CASE HALFTRACK
						CASE DUNE3
						CASE ARDENT
						CASE APC
						CASE AKULA
						CASE TRAILERLARGE
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("SCCAMO_LIV1")
				CASE HASH("SCCAMO_LIV2")
				CASE HASH("SCCAMO_LIV3")
				CASE HASH("SCCAMO_LIV4")
				CASE HASH("SCCAMO_LIV5")
				CASE HASH("SCCAMO_LIV6")
				CASE HASH("SCCAMO_LIV7")
				CASE HASH("SCCAMO_LIV8")
				CASE HASH("SCCAMO_LIV9")
				CASE HASH("SCCAMO_LIV10")
					SWITCH eVehicleModel
						CASE THRUSTER
						CASE MOLOTOK
						CASE KHANJALI
						CASE STREITER
						CASE RIOT2
						CASE COMET4
						CASE CHERNOBOG		
						CASE BARRAGE
						CASE AVENGER
						CASE TRAILERSMALL2
						CASE TECHNICAL3
						CASE TAMPA3
						CASE OPPRESSOR
						CASE NIGHTSHARK
						CASE INSURGENT3
						CASE HALFTRACK
						CASE DUNE3
						CASE ARDENT
						CASE APC
						CASE AKULA
						CASE TRAILERLARGE
						CASE SPEEDO4
						CASE PATRIOT
						CASE PATRIOT2
						CASE MULE4
						CASE POUNDER2
						CASE OPPRESSOR2
						CASE MENACER
						CASE REVOLTER
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("STAF_LIV8")
					SWITCH eVehicleModel
						CASE SWINGER
						CASE STAFFORD
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK	
				CASE HASH("STAF_LIV9")
					SWITCH eVehicleModel
						CASE SWINGER
							RETURN TRUE
						BREAK
					ENDSWITCH		
				BREAK
				CASE HASH("CLIQ_LIV11")
					SWITCH eVehicleModel
						CASE CLIQUE
							RETURN TRUE
						BREAK
					ENDSWITCH		
				BREAK
				CASE HASH("THX_LIV_10")
					SWITCH eVehicleModel
						CASE THRAX
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				CASE HASH("PARAG_LIV2")
					SWITCH eVehicleModel
						CASE PARAGON2
						CASE PARAGON
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				CASE HASH("S80_LIV_10")
					SWITCH eVehicleModel
						CASE S80
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				CASE HASH("LANDST2_LIV_10")
					SWITCH eVehicleModel
						CASE LANDSTALKER2
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				CASE HASH("SEM2_LIVERY9")
					SWITCH eVehicleModel
						CASE SEMINOLE2
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				#IF FEATURE_HEIST_ISLAND
				CASE HASH("RSX_LIVERY11")
					SWITCH eVehicleModel
						CASE ITALIRSX
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				CASE HASH("TORD_LIV11")
					SWITCH eVehicleModel
						CASE TOREADOR
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				#ENDIF
				CASE HASH("WEEVIL_LIV13")
					SWITCH eVehicleModel
						CASE WEEVIL
							RETURN TRUE
						BREAK
					ENDSWITCH	
				BREAK
				CASE HASH("BANSH_LIV11")
				CASE HASH("BANSH_LIV12")
				CASE HASH("BANSH_LIV13")
				CASE HASH("BANSH_LIV14")
				CASE HASH("BANSH_LIV15")
					SWITCH eVehicleModel
						CASE BANSHEE
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("KURUMA_LIV11")
				CASE HASH("KURUMA_LIV12")
				CASE HASH("KURUMA_LIV13")
				CASE HASH("KURUMA_LIV14")
				CASE HASH("KURUMA_LIV15")
					SWITCH eVehicleModel
						CASE KURUMA
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("HOTRING_LIV31")
					SWITCH eVehicleModel
						CASE HOTRING
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("KANJO_LIV13")
					SWITCH eVehicleModel
						CASE KANJO
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("HERMES_LIV11")
					SWITCH eVehicleModel
						CASE HERMES
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("TORD_LIV12")
					SWITCH eVehicleModel
						CASE TOREADOR
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("COMET6_LIV11")
				CASE HASH("COMET6_LIV12")
				CASE HASH("COMET6_LIV13")
				CASE HASH("COMET6_LIV14")
				CASE HASH("COMET6_LIV15")
					SWITCH eVehicleModel
						CASE COMET6
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("CYPH_LIVERY11")
				CASE HASH("CYPH_LIVERY12")
				CASE HASH("CYPH_LIVERY13")
				CASE HASH("CYPH_LIVERY14")
				CASE HASH("CYPH_LIVERY15")
					SWITCH eVehicleModel
						CASE CYPHER
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("DOM7_LIVERY11")
				CASE HASH("DOM7_LIVERY12")
				CASE HASH("DOM7_LIVERY13")
				CASE HASH("DOM7_LIVERY14")
				CASE HASH("DOM7_LIVERY15")
					SWITCH eVehicleModel
						CASE DOMINATOR7
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("VTC_LIV11")
				CASE HASH("VTC_LIV12")
				CASE HASH("VTC_LIV13")
				CASE HASH("VTC_LIV14")
				CASE HASH("VTC_LIV15")
					SWITCH eVehicleModel
						CASE VECTRE
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("GROWLER_LIV11")
				CASE HASH("GROWLER_LIV12")
				CASE HASH("GROWLER_LIV13")
				CASE HASH("GROWLER_LIV14")
				CASE HASH("GROWLER_LIV15")
					SWITCH eVehicleModel
						CASE GROWLER
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("PREV_LIV11")
				CASE HASH("PREV_LIV12")
				CASE HASH("PREV_LIV13")
				CASE HASH("PREV_LIV14")
				CASE HASH("PREV_LIV15")
					SWITCH eVehicleModel
						CASE PREVION
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("SULTAN3_LIV11")
				CASE HASH("SULTAN3_LIV12")
				CASE HASH("SULTAN3_LIV13")
				CASE HASH("SULTAN3_LIV14")
				CASE HASH("SULTAN3_LIV15")
					SWITCH eVehicleModel
						CASE SULTAN3
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
				CASE HASH("HSW_LIV3")
				CASE HASH("HSW_LIV4")
				CASE HASH("HSW_LIV3BRIO")
				CASE HASH("HSW_LIV4BRIO")
					IF IS_GEN9_EXCLUSIVE_VEHICLE_MODS_UNLOCKED()
						RETURN TRUE
					ENDIF	
				BREAK
				CASE HASH("COMET7_LIV11")
				CASE HASH("DEITY_LIVERY12")
				CASE HASH("BALL7_LIV_13")
					RETURN TRUE
				BREAK
				#IF FEATURE_DLC_1_2022
				CASE HASH("STNT_lv3")
				CASE HASH("FAC3_LIV_6")
				CASE HASH("BF_LIV_2")
				CASE HASH("BRIO_LIV_2")
				CASE HASH("NERO2_LIV_7")
				CASE HASH("STAR_LIV8")
				CASE HASH("ALPHAZ1_LIVERY6")
				CASE HASH("HAVOK_LIVERY3")
				CASE HASH("MICLI_LIVERY5")
				CASE HASH("HOTRING_LIV28")
				CASE HASH("GB_LIV10")
				CASE HASH("TEZ_LIV4")
				CASE HASH("BRUT3_LIV_4")
				CASE HASH("CERB3_LIV_1")
				CASE HASH("BRUIS3_LIV4")
				CASE HASH("ZR3803_LIV3")
				CASE HASH("DOM6_LIV1")
				CASE HASH("PARAG_LIV7")
				CASE HASH("ISSI7_LIV_7")
				CASE HASH("ISSI7_LIV_8")
				CASE HASH("THX_LIV_9")
				CASE HASH("NOVA_LIV7")
				CASE HASH("FORMULA_LIV2")
				CASE HASH("ASBO_LIV5")
				CASE HASH("IMGN_LIV5")
				CASE HASH("VET2_LIV1")
				CASE HASH("ZR350_LIV7")
				CASE HASH("EUROS_LIV14")
				CASE HASH("BUFF4_LIV_7")
				CASE HASH("REEVER_LIV10")
				CASE HASH("CHAMPION_LIV10")
				CASE HASH("JUG_LIV8")
				CASE HASH("OPENW1_LIV8")
					// sprunk
					IF g_sMPTunables.bFREE_SPRUNK_LIVERY
						RETURN TRUE
					ENDIF
				BREAK
				CASE HASH("ALPHAZ1_LIVERY4")
				CASE HASH("HAVOK_LIVERY8")
				CASE HASH("MICLI_LIVERY4")
				CASE HASH("HOTRING_LIV24")
				CASE HASH("MICHEL_LIV4")
				CASE HASH("BRUIS3_CANS")
				CASE HASH("DOM6_LIV3")
				CASE HASH("NOVA_LIV9")
				CASE HASH("OPENW1_LIV7")
				CASE HASH("SLT_LIV14")
				CASE HASH("VET2_LIV2")
					// ecola
					IF g_sMPTunables.bFREE_ECOLA_LIVERY
						RETURN TRUE
					ENDIF
				BREAK
				#ENDIF
				CASE HASH("CORSITA_LIV10")
					IF eVehicleModel = CORSITA
						RETURN TRUE
					ENDIF
				BREAK
				CASE HASH("VIGR2_LIV_10")
					IF eVehicleModel = VIGERO2
						RETURN TRUE
					ENDIF
				BREAK	
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE HASH("CORSITA_LIV11")
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CORSITA_MEMBER_LIVERY1)
					OR g_sMPTunables.bENABLE_CORSITA_MEMBER_LIVERY1
						RETURN TRUE
					ENDIF		
				BREAK
				CASE HASH("VIGR2_LIV_11")
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_VIGERO2_MEMBER_LIVERY1)
					OR g_sMPTunables.bENABLE_VIGERO2_MEMBER_LIVERY1
						RETURN TRUE
					ENDIF	
				BREAK
				#ENDIF
			ENDSWITCH	
		BREAK
		#IF FEATURE_DLC_1_2022
		CASE CMM_PAINT_JOB
			SWITCH iLabelHash
				CASE HASH("CMOD_COL6_0")
					IF eVehicleModel = STUNT
					AND iModIndex = 2
						// sprunk
						IF g_sMPTunables.bFREE_SPRUNK_LIVERY
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE HASH("BATI_lV4")
				CASE HASH("SANC_lV1")
					// sprunk
					IF g_sMPTunables.bFREE_SPRUNK_LIVERY
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE CMM_SPOILER
			SWITCH iLabelHash
				CASE HASH("BRUIS3_CANS")
					// sprunk
					IF g_sMPTunables.bFREE_ECOLA_LIVERY
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC 		
	
FUNC STRING GET_AWARD_LIVERY_NAMES(AWARD_LIVERY_NAME eAwardLiveryName)
	SWITCH eAwardLiveryName
		CASE AWARD_LIVERY_BANSH_LIV11 			RETURN "BANSH_LIV11"
		CASE AWARD_LIVERY_BANSH_LIV12 			RETURN "BANSH_LIV12" 			
		CASE AWARD_LIVERY_BANSH_LIV13 			RETURN "BANSH_LIV13" 
		CASE AWARD_LIVERY_BANSH_LIV14  			RETURN "BANSH_LIV14"
		CASE AWARD_LIVERY_BANSH_LIV15  			RETURN "BANSH_LIV15"
		CASE AWARD_LIVERY_CLIQ_LIV11  			RETURN "CLIQ_LIV11"
		CASE AWARD_LIVERY_COMET6_LIV11 			RETURN "COMET6_LIV11" 
		CASE AWARD_LIVERY_COMET6_LIV12  		RETURN "COMET6_LIV12"
		CASE AWARD_LIVERY_COMET6_LIV13  		RETURN "COMET6_LIV13"
		CASE AWARD_LIVERY_COMET6_LIV14  		RETURN "COMET6_LIV14"
		CASE AWARD_LIVERY_COMET6_LIV15  		RETURN "COMET6_LIV15"
		CASE AWARD_LIVERY_CYPH_LIVERY11  		RETURN "CYPH_LIVERY11"
		CASE AWARD_LIVERY_CYPH_LIVERY12  		RETURN "CYPH_LIVERY12"
		CASE AWARD_LIVERY_CYPH_LIVERY13  		RETURN "CYPH_LIVERY13"
		CASE AWARD_LIVERY_CYPH_LIVERY14  		RETURN "CYPH_LIVERY14"
		CASE AWARD_LIVERY_CYPH_LIVERY15  		RETURN "CYPH_LIVERY15"
		CASE AWARD_LIVERY_DOM7_LIVERY11  		RETURN "DOM7_LIVERY11"
		CASE AWARD_LIVERY_DOM7_LIVERY12  		RETURN "DOM7_LIVERY12"
		CASE AWARD_LIVERY_DOM7_LIVERY13  		RETURN "DOM7_LIVERY13"
		CASE AWARD_LIVERY_DOM7_LIVERY14  		RETURN "DOM7_LIVERY14"
		CASE AWARD_LIVERY_DOM7_LIVERY15  		RETURN "DOM7_LIVERY15"
		CASE AWARD_LIVERY_GROWLER_LIV11  		RETURN "GROWLER_LIV11"
		CASE AWARD_LIVERY_GROWLER_LIV12  		RETURN "GROWLER_LIV12"
		CASE AWARD_LIVERY_GROWLER_LIV13  		RETURN "GROWLER_LIV13"
		CASE AWARD_LIVERY_GROWLER_LIV14  		RETURN "GROWLER_LIV14"
		CASE AWARD_LIVERY_GROWLER_LIV15  		RETURN "GROWLER_LIV15"
		CASE AWARD_LIVERY_HERMES_LIV11 	 		RETURN "HERMES_LIV11"
		CASE AWARD_LIVERY_HOTRING_LIV31 		RETURN "HOTRING_LIV31"	
		CASE AWARD_LIVERY_KANJO_LIV13 	 		RETURN "KANJO_LIV13"
		CASE AWARD_LIVERY_KURUMA_LIV11  		RETURN "KURUMA_LIV11"
		CASE AWARD_LIVERY_KURUMA_LIV12  		RETURN "KURUMA_LIV12"
		CASE AWARD_LIVERY_KURUMA_LIV13  		RETURN "KURUMA_LIV13"
		CASE AWARD_LIVERY_KURUMA_LIV14  		RETURN "KURUMA_LIV14"
		CASE AWARD_LIVERY_KURUMA_LIV15  		RETURN "KURUMA_LIV15"
		CASE AWARD_LIVERY_LANDST2_LIV_10		RETURN "LANDST2_LIV_10"	
		CASE AWARD_LIVERY_PARAG_LIV2  			RETURN "PARAG_LIV2"
		CASE AWARD_LIVERY_PREV_LIV11  			RETURN "PREV_LIV11"
		CASE AWARD_LIVERY_PREV_LIV12  			RETURN "PREV_LIV12"
		CASE AWARD_LIVERY_PREV_LIV13  			RETURN "PREV_LIV13"
		CASE AWARD_LIVERY_PREV_LIV14  			RETURN "PREV_LIV14"
		CASE AWARD_LIVERY_PREV_LIV15  			RETURN "PREV_LIV15"
		CASE AWARD_LIVERY_RSX_LIVERY11  		RETURN "RSX_LIVERY11"
		CASE AWARD_LIVERY_S80_LIV_10  			RETURN "S80_LIV_10"
		CASE AWARD_LIVERY_SCCAMO_LIV1  			RETURN "SCCAMO_LIV1"
		CASE AWARD_LIVERY_SCCAMO_LIV10  		RETURN "SCCAMO_LIV10"
		CASE AWARD_LIVERY_SCCAMO_LIV2  			RETURN "SCCAMO_LIV2"
		CASE AWARD_LIVERY_SCCAMO_LIV3  			RETURN "SCCAMO_LIV3"
		CASE AWARD_LIVERY_SCCAMO_LIV4  			RETURN "SCCAMO_LIV4"
		CASE AWARD_LIVERY_SCCAMO_LIV5  			RETURN "SCCAMO_LIV5"
		CASE AWARD_LIVERY_SCCAMO_LIV6  			RETURN "SCCAMO_LIV6"
		CASE AWARD_LIVERY_SCCAMO_LIV7  			RETURN "SCCAMO_LIV7"
		CASE AWARD_LIVERY_SCCAMO_LIV8 	 		RETURN "SCCAMO_LIV8"
		CASE AWARD_LIVERY_SCCAMO_LIV9  			RETURN "SCCAMO_LIV9"
		CASE AWARD_LIVERY_SEM2_LIVERY9  		RETURN "SEM2_LIVERY9"
		CASE AWARD_LIVERY_STAF_LIV8  			RETURN "STAF_LIV8"
		CASE AWARD_LIVERY_STAF_LIV9 			RETURN "STAF_LIV9"
		CASE AWARD_LIVERY_SULTAN3_LIV11  		RETURN "SULTAN3_LIV11"
		CASE AWARD_LIVERY_SULTAN3_LIV12  		RETURN "SULTAN3_LIV12"
		CASE AWARD_LIVERY_SULTAN3_LIV13  		RETURN "SULTAN3_LIV13"
		CASE AWARD_LIVERY_SULTAN3_LIV14  		RETURN "SULTAN3_LIV14"
		CASE AWARD_LIVERY_SULTAN3_LIV15  		RETURN "SULTAN3_LIV15"
		CASE AWARD_LIVERY_SWGR_LIV9 	 		RETURN "SWGR_LIV9"
		CASE AWARD_LIVERY_THX_LIV_10  			RETURN "THX_LIV_10"
		CASE AWARD_LIVERY_TORD_LIV11 	 		RETURN "TORD_LIV11"
		CASE AWARD_LIVERY_TORD_LIV12 	 		RETURN "TORD_LIV12"
		CASE AWARD_LIVERY_VTC_LIV11  			RETURN "VTC_LIV11"
		CASE AWARD_LIVERY_VTC_LIV12  			RETURN "VTC_LIV12"
		CASE AWARD_LIVERY_VTC_LIV13  			RETURN "VTC_LIV13"
		CASE AWARD_LIVERY_VTC_LIV14  			RETURN "VTC_LIV14"
		CASE AWARD_LIVERY_VTC_LIV15 			RETURN "VTC_LIV15"
		CASE AWARD_LIVERY_XMAS_CAMO01 	 		RETURN "XMAS_CAMO01"
		CASE AWARD_LIVERY_XMAS_CAMO02 	 		RETURN "XMAS_CAMO02"
		CASE AWARD_LIVERY_XMAS_CAMO03 	 		RETURN "XMAS_CAMO03"
		CASE AWARD_LIVERY_XMAS_CAMO04 	 		RETURN "XMAS_CAMO04"
		CASE AWARD_LIVERY_XMAS_CAMO05 	 		RETURN "XMAS_CAMO05"
		CASE AWARD_LIVERY_XMAS_CAMO06 	 		RETURN "XMAS_CAMO06"
		CASE AWARD_LIVERY_XMAS_CAMO07  			RETURN "XMAS_CAMO07"
		CASE AWARD_LIVERY_XMAS_CAMO08 		 	RETURN "XMAS_CAMO08"										
		CASE AWARD_LIVERY_XMAS_CAMO09 		 	RETURN "XMAS_CAMO09"											
		CASE AWARD_LIVERY_XMAS_CAMO10 		 	RETURN "XMAS_CAMO10"	
		CASE AWARD_LIVERY_COMET7_LIV11			RETURN "COMET7_LIV11"
		CASE AWARD_LIVERY_DEITY_LIVERY12		RETURN "DEITY_LIVERY12"
		CASE AWARD_LIVERY_BALL7_LIV_13			RETURN "BALL7_LIV_13"
		CASE AWARD_LIVERY_CORSITA_LIV10			RETURN "CORSITA_LIV10"
		CASE AWARD_LIVERY_VIGR2_LIV_10			RETURN "VIGR2_LIV_10"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC
	
PROC SET_XMAS_17_VEHCILE_LIVERIES_STATS()
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF

	INT iStatInt6 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES0)
	INT iStatInt7 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES1)
	INT iStatInt8 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES2)
	INT iStatInt9 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES3)
	INT iStatInt10 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES4)
	INT iStatInt11 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES5)
	INT iStatInt12 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES6)
	
	INT iStatInt13 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES7)
	INT iStatInt14 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES8)
	INT iStatInt15 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES9)
	INT iStatInt16 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES10)
	INT iStatInt17 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES11)
	INT iStatInt18 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES12)
	INT iStatInt19 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES13)
	INT iStatInt20 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES14)
	INT iStatInt21 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES15)
	INT iStatInt22 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES16)
	INT iStatInt23 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES17)
	INT iStatInt24 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES18)
	INT iStatInt25 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES19)
	INT iStatInt26 = GET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES20)
	
	INT iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo6, iLoop)
			SET_BIT(iStatInt6, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt6:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo6 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES0, iStatInt6)
	ENDIF	
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo7, iLoop)
			SET_BIT(iStatInt7, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt7:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo7 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES1, iStatInt7)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo8, iLoop)
			SET_BIT(iStatInt8, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt8:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo8 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES2, iStatInt8)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo9, iLoop)
			SET_BIT(iStatInt9, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt9:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo9 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES3, iStatInt9)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo10, iLoop)
			SET_BIT(iStatInt10, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt10:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo10 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES4, iStatInt10)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo11, iLoop)
			SET_BIT(iStatInt11, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt11:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo11 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES5, iStatInt11)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo12, iLoop)
			SET_BIT(iStatInt12, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt12:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo12 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES6, iStatInt12)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo13, iLoop)
			SET_BIT(iStatInt13, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt13:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo13 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES7, iStatInt13)
	ENDIF	
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo14, iLoop)
			SET_BIT(iStatInt14, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt14:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo14 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES8, iStatInt14)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo15, iLoop)
			SET_BIT(iStatInt15, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt15:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo15 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES9, iStatInt15)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo16, iLoop)
			SET_BIT(iStatInt16, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt16:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo16 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES10, iStatInt16)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo17, iLoop)
			SET_BIT(iStatInt17, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt17:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo17 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES11, iStatInt17)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo18, iLoop)
			SET_BIT(iStatInt18, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt18:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo18 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES12, iStatInt18)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo19, iLoop)
			SET_BIT(iStatInt19, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt19:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo19 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES13, iStatInt19)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo20, iLoop)
			SET_BIT(iStatInt20, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt20:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo20 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES14, iStatInt20)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo21, iLoop)
			SET_BIT(iStatInt21, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt21:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo21 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES15, iStatInt21)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo22, iLoop)
			SET_BIT(iStatInt22, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt22:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo22 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES16, iStatInt22)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo23, iLoop)
			SET_BIT(iStatInt23, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt23:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo23 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES17, iStatInt23)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo24, iLoop)
			SET_BIT(iStatInt24, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt24:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo24 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES18, iStatInt24)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo25, iLoop)
			SET_BIT(iStatInt25, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt25:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo25 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES19, iStatInt25)
	ENDIF
	
	iLoop = 0
	FOR iLoop = 0 TO 30
		IF IS_BIT_SET(g_sMPTunables.ivehxmas_camo26, iLoop)
			SET_BIT(iStatInt26, iLoop)
			PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS SET iStatInt26:", iLoop)
		ENDIF
	ENDFOR	
	
	IF g_sMPTunables.ivehxmas_camo26 != 0
		SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES20, iStatInt26)
	ENDIF
	
	PRINTLN("SET_XMAS_17_VEHCILE_LIVERIES_STATS...")
ENDPROC		
		
PROC CLEAR_ALL_XMAS_17_VEHCILE_LIVERIES_STATS()
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF

	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES0, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES1, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES2, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES3, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES4, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES5, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES6, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES7, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES8, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES9, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES10, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES11, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES12, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES13, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES14, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES15, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES16, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES17, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES18, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES19, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_XMASLIVERIES20, 0)
	
	
	PRINTLN("CLEAR_ALL_XMAS_17_VEHCILE_LIVERIES_STATS...")
ENDPROC

#if USE_CLF_DLC
PROC SET_SP_CARMOD_ITEM_UNLOCK_STATECLF(CARMOD_UNLOCK_ITEMS eItem, INT iCurrentLevel, INT iUnlockLevel)
	IF iCurrentLevel >= iUnlockLevel
		IF NOT IS_BIT_SET(g_savedGlobalsClifford.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
		
			// Reflect this on the app.
			INT iSlot
			REPEAT COUNT_OF(g_savedGlobals.sSocialData.sCarAppData) iSlot
				g_savedGlobals.sSocialData.sCarAppData[iSlot].bUpdateMods = TRUE
			ENDREPEAT
			
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
			
			
			IF iUnlockLevel = 0
				#IF NOT USE_TU_CHANGES
					SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(DUMMY_MODEL_FOR_SCRIPT, eItem)
				#ENDIF
				#IF USE_TU_CHANGES
					SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(DUMMY_MODEL_FOR_SCRIPT, eItem, "")
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#endif
#if USE_NRM_DLC
PROC SET_SP_CARMOD_ITEM_UNLOCK_STATENRM(CARMOD_UNLOCK_ITEMS eItem, INT iCurrentLevel, INT iUnlockLevel)
	IF iCurrentLevel >= iUnlockLevel
		IF NOT IS_BIT_SET(g_savedGlobalsnorman.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
		
			// Reflect this on the app.
			INT iSlot
			REPEAT COUNT_OF(g_savedGlobals.sSocialData.sCarAppData) iSlot
				g_savedGlobals.sSocialData.sCarAppData[iSlot].bUpdateMods = TRUE
			ENDREPEAT
			
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
						
			IF iUnlockLevel = 0
				#IF NOT USE_TU_CHANGES
					SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(DUMMY_MODEL_FOR_SCRIPT, eItem)
				#ENDIF
				#IF USE_TU_CHANGES
					SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(DUMMY_MODEL_FOR_SCRIPT, eItem, "")
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#endif
PROC SET_SP_CARMOD_ITEM_UNLOCK_STATE(CARMOD_UNLOCK_ITEMS eItem, INT iCurrentLevel, INT iUnlockLevel)
#if USE_CLF_DLC
	SET_SP_CARMOD_ITEM_UNLOCK_STATECLF(eItem,iCurrentLevel,iUnlockLevel)
	exit
#endif
#if USE_NRM_DLC
	SET_SP_CARMOD_ITEM_UNLOCK_STATENRM(eItem,iCurrentLevel,iUnlockLevel)
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF iCurrentLevel >= iUnlockLevel
		IF NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
		
			// Reflect this on the app.
			INT iSlot
			REPEAT COUNT_OF(g_savedGlobals.sSocialData.sCarAppData) iSlot
				g_savedGlobals.sSocialData.sCarAppData[iSlot].bUpdateMods = TRUE
			ENDREPEAT
			
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[ENUM_TO_INT(eItem)/32], ENUM_TO_INT(eItem)%32)
			
			
			IF iUnlockLevel = 0
				#IF NOT USE_TU_CHANGES
					SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(DUMMY_MODEL_FOR_SCRIPT, eItem)
				#ENDIF
				#IF USE_TU_CHANGES
					SET_CARMOD_UNLOCK_HAS_BEEN_VIEWED(DUMMY_MODEL_FOR_SCRIPT, eItem, "")
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF
#endif
#endif
ENDPROC


#IF USE_TU_CHANGES

PROC CLEAR_CARWHEEL_VIEWED_BIT_SET(MOD_WHEEL_TYPE modType, CARMOD_SHOP_STRUCT &carmodShopStruct)
	INT iBitsetNumber = ENUM_TO_INT(modType)
	
	IF (iBitSetNumber < 0) OR (iBitSetNumber >= NUMBER_OF_CAR_WHEEL_TYPES)
		EXIT
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		carmodShopStruct.iCarWheelsViewedBitset[iBitSetNumber] = 0
	ENDIF
ENDPROC

PROC CLEAR_ALL_CARWHEEL_UNLOCK_VIEW_STARS(CARMOD_SHOP_STRUCT &carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_SPORT, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_MUSCLE, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_LOWRIDER, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_BIKE, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_SUV, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_OFFROAD, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_TUNER, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_HIEND, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_SUPERMOD4, carmodShopStruct)
	CLEAR_CARWHEEL_VIEWED_BIT_SET(MWT_SUPERMOD5, carmodShopStruct)
ENDPROC
	
FUNC BOOL HAS_CARWHEEL_BEEN_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, MOD_WHEEL_TYPE modWheelType, INT iIndex)

	IF DISABLE_UNLOCK_STARS_FOR_VEHICLE(GET_ENTITY_MODEL(carmodShopStruct.vehMod))
		RETURN TRUE
	ENDIF
	
	INT iBitsetNumber = ENUM_TO_INT(modWheelType)
	
	IF (iBitSetNumber < 0) OR (iBitSetNumber >= NUMBER_OF_CAR_WHEEL_TYPES)
		RETURN TRUE
	ENDIF
	
	IF (iIndex < 0) OR ((iIndex > 31)
	AND modWheelType != MWT_BIKE)
		RETURN TRUE
	ENDIF
	
	IF modWheelType = MWT_BIKE
		IF iIndex > 31 AND iIndex < 64
			iIndex = iIndex - 32
			iBitsetNumber = 8
		ELIF iIndex >= 64 AND iIndex < 75
			iIndex = iIndex - 64
			iBitsetNumber = 9
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		RETURN IS_BIT_SET(carmodShopStruct.iCarWheelsViewedBitset[iBitSetNumber], iIndex)
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

PROC SET_CARWHEEL_AS_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, MOD_WHEEL_TYPE modWheelType, INT iIndex)
	INT iBitsetNumber = ENUM_TO_INT(modWheelType)
	
	IF (iBitSetNumber < 0) OR (iBitSetNumber >= NUMBER_OF_CAR_WHEEL_TYPES)
		EXIT
	ENDIF
	
	IF ((iIndex < 0) OR (iIndex > 31))
	AND modWheelType != MWT_BIKE
		EXIT
	ENDIF
	
	IF modWheelType = MWT_BIKE
		IF iIndex > 31 AND iIndex < 64
			iIndex = iIndex - 32
			iBitsetNumber = 8
		ELIF iIndex >= 64 AND iIndex < 75
			iIndex = iIndex - 64
			iBitsetNumber = 9
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_BIT(carmodShopStruct.iCarWheelsViewedBitset[iBitSetNumber], iIndex)
	ENDIF	
ENDPROC

PROC COPY_CARWHEEL_VIEWED_STATS_TO_CACHE(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_SHOPS, "COPYING CARWHEEL VIEWED STATS TO CACHE")
	carmodShopStruct.iCarWheelsViewedBitset[0] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_0)
	carmodShopStruct.iCarWheelsViewedBitset[1] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_1)
	carmodShopStruct.iCarWheelsViewedBitset[2] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_2)
	carmodShopStruct.iCarWheelsViewedBitset[3] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_3)
	carmodShopStruct.iCarWheelsViewedBitset[4] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_4)
	carmodShopStruct.iCarWheelsViewedBitset[5] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_5)
	carmodShopStruct.iCarWheelsViewedBitset[6] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_6)
	carmodShopStruct.iCarWheelsViewedBitset[7] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_7)
	carmodShopStruct.iCarWheelsViewedBitset[8] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_8)
	carmodShopStruct.iCarWheelsViewedBitset[9] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_9)
	carmodShopStruct.iCarWheelsViewedBitset[11] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_10)
	carmodShopStruct.iCarWheelsViewedBitset[12] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_11)
	
	CPRINTLN(DEBUG_SHOPS, "COPYING CARWHEEL VIEWED COLOR STATS TO CACHE")
	carmodShopStruct.iCarWheelsColorViewedBitset[0] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHCOL_VIEWED_0)
	carmodShopStruct.iCarWheelsColorViewedBitset[1] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHCOL_VIEWED_1)
	
	CPRINTLN(DEBUG_SHOPS, "COPYING PRIMARY CAR COLOR VIEWED STATS TO CACHE")
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[0] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_0)	
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[1] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_1)
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[2] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_2)
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[3] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_3)
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[4] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_4)
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[5] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_5)
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[6] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_6)
	carmodShopStruct.iPrimaryCarPaintsViewedBitset[7] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_7)
	
	CPRINTLN(DEBUG_SHOPS, "COPYING SECONDARY CAR COLOR VIEWED STATS TO CACHE")
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[0] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_0)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[1] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_1)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[2] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_2)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[3] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_3)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[4] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_4)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[5] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_5)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[6] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_6)	
	carmodShopStruct.iSecondaryCarPaintsViewedBitset[7] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_7)	
		
ENDPROC

PROC COPY_CACHE_TO_CARWHEEL_VIEWED_STATS(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_SHOPS, "COPYING CARWHEEL VIEWED CACHE TO STATS")
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_0, carmodShopStruct.iCarWheelsViewedBitset[0])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_1, carmodShopStruct.iCarWheelsViewedBitset[1])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_2, carmodShopStruct.iCarWheelsViewedBitset[2])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_3, carmodShopStruct.iCarWheelsViewedBitset[3])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_4, carmodShopStruct.iCarWheelsViewedBitset[4])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_5, carmodShopStruct.iCarWheelsViewedBitset[5])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_6, carmodShopStruct.iCarWheelsViewedBitset[6])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_7, carmodShopStruct.iCarWheelsViewedBitset[7])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_8, carmodShopStruct.iCarWheelsViewedBitset[8])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_9, carmodShopStruct.iCarWheelsViewedBitset[9])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_10, carmodShopStruct.iCarWheelsViewedBitset[11])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHEELS_VIEWED_11, carmodShopStruct.iCarWheelsViewedBitset[12])
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHCOL_VIEWED_0, carmodShopStruct.iCarWheelsColorViewedBitset[0])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARMODWHCOL_VIEWED_1, carmodShopStruct.iCarWheelsColorViewedBitset[1])
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_0, carmodShopStruct.iPrimaryCarPaintsViewedBitset[0])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_1, carmodShopStruct.iPrimaryCarPaintsViewedBitset[1])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_2, carmodShopStruct.iPrimaryCarPaintsViewedBitset[2])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_3, carmodShopStruct.iPrimaryCarPaintsViewedBitset[3])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_4, carmodShopStruct.iPrimaryCarPaintsViewedBitset[4])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_5, carmodShopStruct.iPrimaryCarPaintsViewedBitset[5])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_6, carmodShopStruct.iPrimaryCarPaintsViewedBitset[6])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTPRIME_VIEW_7, carmodShopStruct.iPrimaryCarPaintsViewedBitset[7])
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_0, carmodShopStruct.iSecondaryCarPaintsViewedBitset[0])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_1, carmodShopStruct.iSecondaryCarPaintsViewedBitset[1])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_2, carmodShopStruct.iSecondaryCarPaintsViewedBitset[2])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_3, carmodShopStruct.iSecondaryCarPaintsViewedBitset[3])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_4, carmodShopStruct.iSecondaryCarPaintsViewedBitset[4])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_5, carmodShopStruct.iSecondaryCarPaintsViewedBitset[5])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_6, carmodShopStruct.iSecondaryCarPaintsViewedBitset[6])
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CARPAINTSEC_VIEW_7, carmodShopStruct.iSecondaryCarPaintsViewedBitset[7])

	
ENDPROC

PROC CLEAR_ALL_CARWHEEL_COLOR_UNLOCK_VIEW_STARS(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	carmodShopStruct.iCarWheelsColorViewedBitset[0] = 0
	carmodShopStruct.iCarWheelsColorViewedBitset[1] = 0
ENDPROC

FUNC BOOL HAS_CARWHEEL_COLOR_BEEN_VIEWED(INT ind, CARMOD_SHOP_STRUCT &carmodShopStruct)

	IF (ind < 0) OR (ind > 63) OR NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	INT iIndex = ind % 32
	INT iBitset = ind / 32
	
	RETURN IS_BIT_SET(carmodShopStruct.iCarWheelsColorViewedBitset[iBitset], iIndex)
ENDFUNC

PROC SET_CARWHEEL_COLOR_AS_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, INT ind, BOOL bOk = TRUE)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT iIndex = ind % 32
	INT iBitset = ind / 32
	
	IF (bOk)
		SET_BIT(carmodShopStruct.iCarWheelsColorViewedBitset[iBitset], iIndex)
	ELSE
		CLEAR_BIT(carmodShopStruct.iCarWheelsColorViewedBitset[iBitset], iIndex)
	ENDIF
ENDPROC

PROC SET_CARWHEEL_DEFAULTS_AS_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct)
	INT i
	
	REPEAT 5 i
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_SPORT, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_MUSCLE, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_LOWRIDER, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_BIKE, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_SUV, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_OFFROAD, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_TUNER, i)
		SET_CARWHEEL_AS_VIEWED(carmodShopStruct, MWT_HIEND, i)
	ENDREPEAT
	
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 0, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 1, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 4, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 6, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 7, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 9, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 12, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 15, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 18, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 21, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 25, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 29, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 30, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 34, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 35, DEFAULT)
	SET_CARWHEEL_COLOR_AS_VIEWED(carmodShopStruct, 39, DEFAULT)

ENDPROC

FUNC INT GET_TOTAL_NUMBER_OF_CAR_PAINTS(MOD_COLOR_TYPE colorType)
	IF colorType = MCT_CLASSIC
		RETURN NUMBER_OF_CAR_CLASSIC_PAINTS			
	ENDIF

	IF colorType = MCT_METALLIC
		RETURN NUMBER_OF_CAR_METALLIC_PAINTS			
	ENDIF
	
	IF colorType = MCT_PEARLESCENT
		RETURN NUMBER_OF_CAR_PEARLESCENT_PAINTS			
	ENDIF
	
	IF colorType = MCT_MATTE		RETURN NUMBER_OF_CAR_MATTE_PAINTS			ENDIF
	IF colorType = MCT_CHROME		RETURN NUMBER_OF_CAR_CHROME_PAINTS			ENDIF
	IF colorType = MCT_METALS		RETURN NUMBER_OF_CAR_METALS_PAINTS			ENDIF
	#IF FEATURE_GEN9_EXCLUSIVE
	IF colorType = MCT_CHAMELEON	RETURN NUMBER_OF_CAR_CHAMELEON_PAINTS		ENDIF
	#ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL IS_TU_CARMOD_COLOUR(MOD_COLOR_TYPE colorType, INT iIndex)
	IF colorType = MCT_METALS
		IF iIndex = 3 // Gold
		OR iIndex = 4 // Brushed Gold
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT _GET_CARPAINT_BITFIELD_INDEX(MOD_COLOR_TYPE colorType, INT iIndex)
	INT iArrayIndex = 0
	
	BOOL bTUModColour = IS_TU_CARMOD_COLOUR(colorType, iIndex)

	IF colorType > MCT_METALLIC
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_METALLIC_PAINTS
	ENDIF
	
	IF colorType > MCT_CLASSIC
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_CLASSIC_PAINTS		
	ENDIF
	
	IF colorType > MCT_PEARLESCENT
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_PEARLESCENT_PAINTS
	ENDIF

	IF colorType > MCT_MATTE
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_MATTE_PAINTS
	ENDIF

	IF colorType > MCT_METALS
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_METALS_PAINTS
	ENDIF
	
	IF colorType > MCT_CHROME
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_CHROME_PAINTS
	ENDIF
	
	// New mod colours that get added to the end
	IF bTUModColour
		IF colorType = MCT_METALS
			IF iIndex > 3	iArrayIndex++	ENDIF
			IF iIndex > 4	iArrayIndex++	ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF colorType > MCT_CHAMELEON
	OR bTUModColour
		iArrayIndex += NUMBER_OF_CAR_CHAMELEON_PAINTS
	ENDIF
	#ENDIF
	
	RETURN iArrayIndex + iIndex
ENDFUNC

PROC SET_CARPAINT_AS_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, MOD_COLOR_TYPE colorType, INT iIndex, BOOL bPrimary = TRUE, BOOL bViewed = TRUE)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT iArrayIndex = _GET_CARPAINT_BITFIELD_INDEX(colorType, iIndex)
	INT iBitSet = iArrayIndex / 32
	INT iBit = iArrayIndex % 32
		
	IF (bPrimary)
		IF (bViewed)
			SET_BIT(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iBitSet], iBit)
		ELSE
			CLEAR_BIT(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iBitSet], iBit)
		ENDIF		
	ELSE
		IF (bViewed)
			SET_BIT(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iBitSet], iBit)
		ELSE
			CLEAR_BIT(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iBitSet], iBit)
		ENDIF	
	ENDIF
	
ENDPROC

FUNC BOOL HAS_CARPAINT_BEEN_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, MOD_COLOR_TYPE colorType, INT iIndex, BOOL bPrimary = TRUE)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	INT iArrayIndex = _GET_CARPAINT_BITFIELD_INDEX(colorType, iIndex)
	INT iBitSet = iArrayIndex / 32
	INT iBit = iArrayIndex % 32
	
	IF (bPrimary)
		RETURN IS_BIT_SET(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iBitSet], iBit)
	ELSE
		RETURN IS_BIT_SET(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iBitSet], iBit)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_CARPAINT_CREW_COLOR_AS_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, BOOL bPrimary = TRUE)
	INT iIndex
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF (bPrimary)
		iIndex = COUNT_OF(carmodShopStruct.iPrimaryCarPaintsViewedBitset) - 1
		SET_BIT(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iIndex], 31)
	ELSE
		iIndex = COUNT_OF(carmodShopStruct.iSecondaryCarPaintsViewedBitset) - 1
		SET_BIT(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iIndex], 31)
	ENDIF
ENDPROC

FUNC BOOL HAS_CARPAINT_CREW_COLOR_BEEN_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct, BOOL bPrimary = TRUE)
	INT iIndex
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	IF (bPrimary)
		iIndex = COUNT_OF(carmodShopStruct.iPrimaryCarPaintsViewedBitset) - 1
		RETURN IS_BIT_SET(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iIndex], 31)
	ELSE
		iIndex = COUNT_OF(carmodShopStruct.iSecondaryCarPaintsViewedBitset) - 1
		RETURN IS_BIT_SET(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iIndex], 31)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CARPAINT_CREW_EMBLEM_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT iIndex = COUNT_OF(carmodShopStruct.iPrimaryCarPaintsViewedBitset) - 1
	SET_BIT(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iIndex], 30)
ENDPROC

FUNC BOOL HAS_CARPAINT_CREW_EMBLEM_BEEN_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	INT iIndex = COUNT_OF(carmodShopStruct.iPrimaryCarPaintsViewedBitset) - 1
	RETURN IS_BIT_SET(carmodShopStruct.iPrimaryCarPaintsViewedBitset[iIndex], 30)
ENDFUNC

PROC SET_CREW_SMOKE_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT iIndex = COUNT_OF(carmodShopStruct.iSecondaryCarPaintsViewedBitset) - 1
	SET_BIT(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iIndex], 30)
ENDPROC

FUNC BOOL HAS_CREW_SMOKE_BEEN_VIEWED(CARMOD_SHOP_STRUCT &carmodShopStruct)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	INT iIndex = COUNT_OF(carmodShopStruct.iSecondaryCarPaintsViewedBitset) - 1
	RETURN IS_BIT_SET(carmodShopStruct.iSecondaryCarPaintsViewedBitset[iIndex], 30)
ENDFUNC

PROC RESET_VIEWED_STARS_FOR_CARPAINTS(CARMOD_SHOP_STRUCT &carmodShopStruct)
	INT i
	
	REPEAT COUNT_OF(carmodShopStruct.iPrimaryCarPaintsViewedBitset) i
		carmodShopStruct.iPrimaryCarPaintsViewedBitset[i] = 0
		carmodShopStruct.iSecondaryCarPaintsViewedBitset[i] = 0
	ENDREPEAT
ENDPROC

#ENDIF	//	USE_TU_CHANGES




FUNC INT GET_MP_CLASSIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel)

	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN 0
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(strLabel)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_MP_CLASSIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - label doesn't exist: ", strLabel)
			SCRIPT_ASSERT("GET_MP_CLASSIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - invalid text label, pass logs to Aaron G (not Garbut).")
		#ENDIF
		RETURN 0
	ENDIF
	
	INT iHash = GET_HASH_KEY(strLabel)
	
	SWITCH iHash
		CASE  HASH("BLACK")				RETURN 840		BREAK
		CASE  HASH("BLACK_GRAPHITE")	RETURN 810		BREAK
		CASE  HASH("GRAPHITE")			RETURN 790		BREAK
		CASE  HASH("ANTHR_BLACK")		RETURN 830		BREAK
		CASE  HASH("BLACK_STEEL")		RETURN 860		BREAK
		CASE  HASH("DARK_SILVER")		RETURN 780		BREAK

		
		CASE  HASH("SILVER")			RETURN 820		BREAK
		CASE  HASH("BLUE_SILVER")		RETURN 800		BREAK
		CASE  HASH("ROLLED_STEEL")		RETURN 780		BREAK
		CASE  HASH("SHADOW_SILVER")		RETURN 860		BREAK
		CASE  HASH("STONE_SILVER")		RETURN 870		BREAK
		CASE  HASH("MIDNIGHT_SILVER")	RETURN 900		BREAK
		CASE  HASH("CAST_IRON_SIL")		RETURN 880		BREAK
		CASE  HASH("RED")				RETURN 920		BREAK
		CASE  HASH("TORINO_RED")		RETURN 5600		BREAK
		CASE  HASH("FORMULA_RED")		RETURN 5200		BREAK
		CASE  HASH("LAVA_RED")			RETURN 11600	BREAK
		CASE  HASH("BLAZE_RED")			RETURN 3800		BREAK
		CASE  HASH("GRACE_RED")			RETURN 950		BREAK
		CASE  HASH("GARNET_RED")		RETURN 890		BREAK
		CASE  HASH("SUNSET_RED")		RETURN 10600	BREAK
		CASE  HASH("CABERNET_RED")		RETURN 9600		BREAK
		CASE  HASH("WINE_RED")			RETURN 10400	BREAK

		CASE  HASH("CANDY_RED")			RETURN 12150	BREAK
		CASE  HASH("HOT PINK")			RETURN 12500	BREAK
		CASE  HASH("PINK")				RETURN 11000	BREAK
		CASE  HASH("SALMON_PINK")		RETURN 10400	BREAK
		CASE  HASH("SUNRISE_ORANGE")	RETURN 8800		BREAK
		CASE  HASH("ORANGE")			RETURN 4500		BREAK
		CASE  HASH("BRIGHT_ORANGE")		RETURN 5400		BREAK
		CASE  HASH("GOLD")				RETURN 990		BREAK
		CASE  HASH("BRONZE")			RETURN 930		BREAK
		CASE  HASH("YELLOW")			RETURN 5100		BREAK
		CASE  HASH("RACE_YELLOW")		RETURN 9200		BREAK
		CASE  HASH("FLUR_YELLOW")		RETURN 9600		BREAK

		CASE  HASH("DARK_GREEN")		RETURN 870		BREAK
		CASE  HASH("RACING_GREEN")		RETURN 8400		BREAK
		CASE  HASH("SEA_GREEN")			RETURN 950		BREAK
		CASE  HASH("OLIVE_GREEN")		RETURN 850		BREAK
		CASE  HASH("BRIGHT_GREEN")		RETURN 9600		BREAK
		CASE  HASH("PETROL_GREEN")		RETURN 10400	BREAK
		CASE  HASH("LIME_GREEN")		RETURN 12500	BREAK
		CASE  HASH("MIDNIGHT_BLUE")		RETURN 9800		BREAK
		CASE  HASH("GALAXY_BLUE")		RETURN 1010		BREAK
		CASE  HASH("DARK_BLUE")			RETURN 930		BREAK
		CASE  HASH("SAXON_BLUE")		RETURN 900		BREAK
		CASE  HASH("BLUE")				RETURN 860		BREAK
		CASE  HASH("MARINER_BLUE")		RETURN 870		BREAK
		CASE  HASH("HARBOR_BLUE")		RETURN 840		BREAK
		CASE  HASH("DIAMOND_BLUE")		RETURN 8650		BREAK
		CASE  HASH("SURF_BLUE")			RETURN 920		BREAK
		CASE  HASH("NAUTICAL_BLUE")		RETURN 890		BREAK
		CASE  HASH("RACING_BLUE")		RETURN 4500		BREAK
		CASE  HASH("ULTRA_BLUE")		RETURN 11000	BREAK
		CASE  HASH("LIGHT_BLUE")		RETURN 810		BREAK
		CASE  HASH("CHOCOLATE_BROWN")	RETURN 9050		BREAK
		CASE  HASH("BISON_BROWN")		RETURN 8000		BREAK
		CASE  HASH("CREEK_BROWN")		RETURN 780		BREAK
		CASE  HASH("UMBER_BROWN")		RETURN 810		BREAK
		CASE  HASH("MAPLE_BROWN")		RETURN 850		BREAK
		CASE  HASH("BEECHWOOD_BROWN")	RETURN 900		BREAK
		CASE  HASH("SIENNA_BROWN")		RETURN 920		BREAK
		CASE  HASH("SADDLE_BROWN")		RETURN 830		BREAK
		CASE  HASH("MOSS_BROWN")		RETURN 860		BREAK
		CASE  HASH("WOODBEECH_BROWN")	RETURN 900		BREAK
		CASE  HASH("STRAW_BROWN")		RETURN 880		BREAK
		CASE  HASH("SANDY_BROWN")		RETURN 870		BREAK
		CASE  HASH("BLEECHED_BROWN")	RETURN 780		BREAK
		CASE  HASH("PURPLE")			RETURN 4500		BREAK
		CASE  HASH("SPIN_PURPLE")		RETURN 11400	BREAK
		CASE  HASH("MIGHT_PURPLE")		RETURN 11000	BREAK
		CASE  HASH("BRIGHT_PURPLE")		RETURN 12500	BREAK
		CASE  HASH("CREAM")				RETURN 780		BREAK
		CASE  HASH("WHITE")				RETURN 810		BREAK
		CASE  HASH("FROST_WHITE")		RETURN 870		BREAK

	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_MP_METALLIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel)

	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN 0
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(strLabel)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_MP_METALLIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - label doesn't exist: ", strLabel)
			SCRIPT_ASSERT("GET_MP_METALLIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - invalid text label, pass logs to Aaron G (not Garbut).")
		#ENDIF
		RETURN 0
	ENDIF
	
	INT iHash = GET_HASH_KEY(strLabel)
	
	SWITCH iHash
		CASE  HASH("BLACK")				RETURN 1400		BREAK
		CASE  HASH("BLACK_GRAPHITE")	RETURN 1350		BREAK
		CASE  HASH("GRAPHITE")			RETURN 1320		BREAK
		CASE  HASH("ANTHR_BLACK")		RETURN 1380		BREAK
		CASE  HASH("BLACK_STEEL")		RETURN 1440		BREAK
		CASE  HASH("DARK_SILVER")		RETURN 1300		BREAK
		
		CASE  HASH("SILVER")			RETURN 1370		BREAK
		CASE  HASH("BLUE_SILVER")		RETURN 1340		BREAK
		CASE  HASH("ROLLED_STEEL")		RETURN 1300		BREAK
		CASE  HASH("SHADOW_SILVER")		RETURN 1430		BREAK
		CASE  HASH("STONE_SILVER")		RETURN 1450		BREAK
		CASE  HASH("MIDNIGHT_SILVER")	RETURN 1500		BREAK
		CASE  HASH("CAST_IRON_SIL")		RETURN 1470		BREAK
		CASE  HASH("RED")				RETURN 1530		BREAK
		CASE  HASH("TORINO_RED")		RETURN 9260		BREAK
		CASE  HASH("FORMULA_RED")		RETURN 8890		BREAK
		CASE  HASH("LAVA_RED")			RETURN 14500	BREAK
		CASE  HASH("BLAZE_RED")			RETURN 5000		BREAK
		CASE  HASH("GRACE_RED")			RETURN 1590		BREAK
		CASE  HASH("GARNET_RED")		RETURN 1480		BREAK
		CASE  HASH("SUNSET_RED")		RETURN 13250	BREAK
		CASE  HASH("CABERNET_RED")		RETURN 12000	BREAK
		CASE  HASH("WINE_RED")			RETURN 13000	BREAK

		CASE  HASH("CANDY_RED")			RETURN 14750	BREAK
		CASE  HASH("HOT PINK")			RETURN 15000	BREAK
		CASE  HASH("PINK")				RETURN 13750	BREAK
		CASE  HASH("SALMON_PINK")		RETURN 13000	BREAK
		CASE  HASH("SUNRISE_ORANGE")	RETURN 11000	BREAK
		CASE  HASH("ORANGE")			RETURN 7500		BREAK
		CASE  HASH("BRIGHT_ORANGE")		RETURN 9000		BREAK
		CASE  HASH("GOLD")				RETURN 1650		BREAK
		CASE  HASH("BRONZE")			RETURN 1550		BREAK
		CASE  HASH("YELLOW")			RETURN 8500		BREAK
		CASE  HASH("RACE_YELLOW")		RETURN 11500	BREAK
		CASE  HASH("FLUR_YELLOW")		RETURN 12000	BREAK
		
		CASE  HASH("DARK_GREEN")		RETURN 1450		BREAK
		CASE  HASH("RACING_GREEN")		RETURN 10500	BREAK
		CASE  HASH("SEA_GREEN")			RETURN 1580		BREAK
		CASE  HASH("OLIVE_GREEN")		RETURN 1420		BREAK
		CASE  HASH("BRIGHT_GREEN")		RETURN 12000	BREAK
		CASE  HASH("PETROL_GREEN")		RETURN 13000	BREAK
		CASE  HASH("LIME_GREEN")		RETURN 15000		BREAK
		CASE  HASH("MIDNIGHT_BLUE")		RETURN 12250		BREAK
		CASE  HASH("GALAXY_BLUE")		RETURN 1690		BREAK
		CASE  HASH("DARK_BLUE")			RETURN 1550		BREAK
		CASE  HASH("SAXON_BLUE")		RETURN 1500		BREAK
		CASE  HASH("BLUE")				RETURN 1430		BREAK
		CASE  HASH("MARINER_BLUE")		RETURN 1450		BREAK
		CASE  HASH("HARBOR_BLUE")		RETURN 1400		BREAK
		CASE  HASH("DIAMOND_BLUE")		RETURN 10890		BREAK
		CASE  HASH("SURF_BLUE")			RETURN 1530		BREAK
		CASE  HASH("NAUTICAL_BLUE")		RETURN 1480		BREAK
		CASE  HASH("RACING_BLUE")		RETURN 7500		BREAK
		CASE  HASH("ULTRA_BLUE")		RETURN 13750		BREAK
		CASE  HASH("LIGHT_BLUE")		RETURN 1350		BREAK
		CASE  HASH("CHOCOLATE_BROWN")		RETURN 11250		BREAK
		CASE  HASH("BISON_BROWN")		RETURN 10000		BREAK
		CASE  HASH("CREEK_BROWN")		RETURN 1300		BREAK
		CASE  HASH("UMBER_BROWN")		RETURN 1350		BREAK
		CASE  HASH("MAPLE_BROWN")		RETURN 1420		BREAK
		CASE  HASH("BEECHWOOD_BROWN")		RETURN 1500		BREAK
		CASE  HASH("SIENNA_BROWN")		RETURN 1530		BREAK
		CASE  HASH("SADDLE_BROWN")		RETURN 1380		BREAK
		CASE  HASH("MOSS_BROWN")		RETURN 1430		BREAK
		CASE  HASH("WOODBEECH_BROWN")		RETURN 1500		BREAK
		CASE  HASH("STRAW_BROWN")		RETURN 1470		BREAK
		CASE  HASH("SANDY_BROWN")		RETURN 1450		BREAK
		CASE  HASH("BLEECHED_BROWN")		RETURN 1300		BREAK
		CASE  HASH("PURPLE")			RETURN 7500		BREAK
		CASE  HASH("SPIN_PURPLE")		RETURN 14250		BREAK
		CASE  HASH("MIGHT_PURPLE")		RETURN 13750		BREAK
		CASE  HASH("BRIGHT_PURPLE")		RETURN 15000		BREAK
		CASE  HASH("CREAM")			RETURN 1300		BREAK
		CASE  HASH("WHITE")			RETURN 1350		BREAK
		CASE  HASH("FROST_WHITE")		RETURN 1450		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_MP_PEARLESCENT_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel)

	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN 0
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(strLabel)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_MP_PEARLESCENT_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - label doesn't exist: ", strLabel)
			SCRIPT_ASSERT("GET_MP_PEARLESCENT_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - invalid text label, pass logs to Aaron G (not Garbut).")
		#ENDIF
		RETURN 0
	ENDIF
	
	INT iHash = GET_HASH_KEY(strLabel)
	
	SWITCH iHash
		CASE  HASH("BLACK")				RETURN 2520		BREAK
		CASE  HASH("BLACK_GRAPHITE")	RETURN 2430		BREAK
		CASE  HASH("GRAPHITE")			RETURN 2370		BREAK
		CASE  HASH("ANTHR_BLACK")		RETURN 2490		BREAK
		CASE  HASH("BLACK_STEEL")		RETURN 2580		BREAK
		CASE  HASH("DARK_SILVER")		RETURN 2340		BREAK
		CASE  HASH("SILVER")			RETURN 2460		BREAK
		CASE  HASH("BLUE_SILVER")		RETURN 3200		BREAK
		CASE  HASH("ROLLED_STEEL")		RETURN 2340		BREAK
		CASE  HASH("SHADOW_SILVER")		RETURN 2580		BREAK
		CASE  HASH("STONE_SILVER")		RETURN 2610		BREAK
		CASE  HASH("MIDNIGHT_SILVER")		RETURN 2700		BREAK
		CASE  HASH("CAST_IRON_SIL")		RETURN 2640		BREAK
		CASE  HASH("RED")			RETURN 4200		BREAK
		CASE  HASH("TORINO_RED")		RETURN 15250		BREAK
		CASE  HASH("FORMULA_RED")		RETURN 15000		BREAK
		CASE  HASH("LAVA_RED")			RETURN 19550		BREAK
		CASE  HASH("BLAZE_RED")			RETURN 5400		BREAK
		CASE  HASH("GRACE_RED")			RETURN 2850		BREAK
		CASE  HASH("GARNET_RED")		RETURN 2670		BREAK
		CASE  HASH("SUNSET_RED")		RETURN 18100		BREAK
		CASE  HASH("CABERNET_RED")		RETURN 16240		BREAK
		CASE  HASH("WINE_RED")			RETURN 8200		BREAK

		CASE  HASH("CANDY_RED")			RETURN 19800		BREAK
		CASE  HASH("HOT PINK")			RETURN 20000		BREAK
		CASE  HASH("PINK")			RETURN 19200		BREAK
		CASE  HASH("SALMON_PINK")		RETURN 17600		BREAK
		CASE  HASH("SUNRISE_ORANGE")		RETURN 15800		BREAK
		CASE  HASH("ORANGE")			RETURN 15480		BREAK
		CASE  HASH("BRIGHT_ORANGE")		RETURN 15620		BREAK
		CASE  HASH("GOLD")			RETURN 3500		BREAK
		CASE  HASH("BRONZE")			RETURN 3450		BREAK
		CASE  HASH("YELLOW")			RETURN 15590		BREAK
		CASE  HASH("RACE_YELLOW")		RETURN 15860		BREAK
		CASE  HASH("FLUR_YELLOW")		RETURN 16000		BREAK
		
		CASE  HASH("DARK_GREEN")		RETURN 2610		BREAK
		CASE  HASH("RACING_GREEN")		RETURN 15760		BREAK
		CASE  HASH("SEA_GREEN")			RETURN 2850		BREAK
		CASE  HASH("OLIVE_GREEN")		RETURN 2550		BREAK
		CASE  HASH("BRIGHT_GREEN")		RETURN 15970		BREAK
		CASE  HASH("PETROL_GREEN")		RETURN 16850		BREAK
		CASE  HASH("LIME_GREEN")		RETURN 20000		BREAK
		CASE  HASH("MIDNIGHT_BLUE")		RETURN 7500		BREAK
		CASE  HASH("GALAXY_BLUE")		RETURN 3030		BREAK
		CASE  HASH("DARK_BLUE")			RETURN 2790		BREAK
		CASE  HASH("SAXON_BLUE")		RETURN 2700		BREAK
		CASE  HASH("BLUE")			RETURN 3840		BREAK
		CASE  HASH("MARINER_BLUE")		RETURN 2610		BREAK
		CASE  HASH("HARBOR_BLUE")		RETURN 2520		BREAK
		CASE  HASH("DIAMOND_BLUE")		RETURN 15700		BREAK
		CASE  HASH("SURF_BLUE")			RETURN 3900		BREAK
		CASE  HASH("NAUTICAL_BLUE")		RETURN 3610		BREAK
		CASE  HASH("RACING_BLUE")		RETURN 15340		BREAK
		CASE  HASH("ULTRA_BLUE")		RETURN 18750		BREAK
		CASE  HASH("LIGHT_BLUE")		RETURN 3450		BREAK
		CASE  HASH("CHOCOLATE_BROWN")		RETURN 6300		BREAK
		CASE  HASH("BISON_BROWN")		RETURN 5800		BREAK
		CASE  HASH("CREEK_BROWN")		RETURN 2340		BREAK
		CASE  HASH("UMBER_BROWN")		RETURN 2430		BREAK
		CASE  HASH("MAPLE_BROWN")		RETURN 2550		BREAK
		CASE  HASH("BEECHWOOD_BROWN")		RETURN 2700		BREAK
		CASE  HASH("SIENNA_BROWN")		RETURN 2760		BREAK
		CASE  HASH("SADDLE_BROWN")		RETURN 2490		BREAK
		CASE  HASH("MOSS_BROWN")		RETURN 2580		BREAK
		CASE  HASH("WOODBEECH_BROWN")		RETURN 2700		BREAK
		CASE  HASH("STRAW_BROWN")		RETURN 2640		BREAK
		CASE  HASH("SANDY_BROWN")		RETURN 2610		BREAK
		CASE  HASH("BLEECHED_BROWN")		RETURN 2340		BREAK
		CASE  HASH("PURPLE")			RETURN 5650		BREAK
		CASE  HASH("SPIN_PURPLE")		RETURN 10000		BREAK
		CASE  HASH("MIGHT_PURPLE")		RETURN 9400		BREAK
		CASE  HASH("BRIGHT_PURPLE")		RETURN 20000		BREAK
		CASE  HASH("CREAM")			RETURN 2340		BREAK
		CASE  HASH("WHITE")			RETURN 2430		BREAK
		CASE  HASH("FROST_WHITE")		RETURN 2610		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC INT GET_MP_CHAMELEON_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel)

	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN 0
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(strLabel)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_MP_CHAMELEON_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - label doesn't exist: ", strLabel)
			SCRIPT_ASSERT("GET_MP_CHAMELEON_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - invalid text label")
		#ENDIF
		RETURN 0
	ENDIF
	
	INT iHash = GET_HASH_KEY(strLabel)
	
	SWITCH iHash
		CASE HASH("ANOD_RED")           	RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_ANOD_RED_PEARL        BREAK
		CASE HASH("ANOD_BLUE")           	RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_ANOD_BLUE_PEARL        BREAK
		CASE HASH("ANOD_GOLD")           	RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_ANOD_GOLD_PEARL        BREAK
		CASE HASH("GREEN_BLUE_FLIP")        RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_GREENBLUE_FLIP        BREAK
		CASE HASH("PURP_GREEN_FLIP")        RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_PURPLEGREEN_FLIP        BREAK
		CASE HASH("ORANG_PURP_FLIP")        RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_ORANGEPURPLE_FLIP        BREAK
		CASE HASH("DARKPURPLEPEARL")        
			IF IS_GEN_8_PLAYER()
											RETURN 0
			ELSE	
											RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_DARKPURPLE_FLIP       
			ENDIF	
		BREAK
		CASE HASH("BLUE_PEARL")           	RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_BABYBLUE_PEARL        BREAK
		CASE HASH("RED_PRISMA")           	
			IF IS_GEN_8_PLAYER()
											RETURN 0
			ELSE
											RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_RED_PRISM_PEARL    
			ENDIF
		BREAK
		CASE HASH("BLACK_PRISMA")           RETURN                   g_sMPTunables.iGEN9_MODS_CPAINT_BLACK_PRISM_PEARL        BREAK
	ENDSWITCH	
	
	RETURN 0
ENDFUNC
#ENDIF

FUNC INT GET_MP_METALS_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel)

	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN 0
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(strLabel)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SHOPS, "GET_MP_METALS_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - label doesn't exist: ", strLabel)
			SCRIPT_ASSERT("GET_MP_METALS_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - invalid text label, pass logs to Aaron G (not Garbut).")
		#ENDIF
		RETURN 0
	ENDIF
	
	INT iHash = GET_HASH_KEY(strLabel)
	
	SWITCH iHash
		CASE  HASH("BR_STEEL")			RETURN 12500		BREAK
		CASE  HASH("BR BLACK_STEEL")	RETURN 11500		BREAK
		CASE  HASH("BR_ALUMINIUM")		RETURN 10250		BREAK
		
		CASE  HASH("GOLD_P")		RETURN g_sMPTunables.iHipster_vehicle_Pure_Gold_Metal_Respray		BREAK
		CASE  HASH("GOLD_S")	RETURN g_sMPTunables.iHipster_vehicle_Brushed_Gold_Metal_Respray		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_MP_MATTE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel)

	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN 0
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(strLabel)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_MP_MATTEC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - label doesn't exist: ", strLabel)
			SCRIPT_ASSERT("GET_MP_MATTE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL - invalid text label, pass logs to Aaron G (not Garbut).")
		#ENDIF
		RETURN 0
	ENDIF
	
	INT iHash = GET_HASH_KEY(strLabel)
	
	SWITCH iHash
		CASE  HASH("BLACK")		RETURN 2200		BREAK	
		CASE  HASH("GREY")		RETURN 2050		BREAK
		CASE  HASH("LIGHT_GREY")	RETURN 2000		BREAK
		CASE  HASH("WHITE")		RETURN 2120		BREAK
		CASE  HASH("BLUE")		RETURN 14250		BREAK	
		CASE  HASH("DARK_BLUE")		RETURN 2150		BREAK
		CASE  HASH("MIDNIGHT_BLUE")	RETURN 10000		BREAK
		CASE  HASH("MIGHT_PURPLE")	RETURN 13750		BREAK		
		CASE  HASH("PURPLE")		RETURN 17000		BREAK		
		CASE  HASH("RED")		RETURN 12500		BREAK
		CASE  HASH("DARK_RED")		RETURN 15000		BREAK
		CASE  HASH("ORANGE")		RETURN 14000		BREAK
		CASE  HASH("YELLOW")		RETURN 11600		BREAK
		CASE  HASH("LIME_GREEN")	RETURN 17500		BREAK
		CASE  HASH("GREEN")		RETURN 2130		BREAK
		
		CASE  HASH("MATTE_FOR")		RETURN 2150		BREAK
		CASE  HASH("MATTE_FOIL")	RETURN 2100		BREAK
		CASE  HASH("MATTE_OD")		RETURN 2250		BREAK
		CASE  HASH("MATTE_DIRT")	RETURN 2240		BREAK		
		CASE  HASH("MATTE_DESERT")	RETURN 2170		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_BASE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(STRING strLabel, MOD_COLOR_TYPE colType, BOOL primary, BOOL bProcessPrimaryFlag, BOOL bProcessingTrimFlag, BOOL bProcessingLightFlag, BOOL bProcessingAccentFlag)

	INT iCost = 0
	
	SWITCH colType
		CASE MCT_CLASSIC
			iCost = (GET_MP_CLASSIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel))
			#IF FEATURE_TUNER
			IF SHOULD_DISCOUNT_TUNER_AUTO_SHOP_MODS()
				IF bProcessingTrimFlag
				OR bProcessingLightFlag
				OR bProcessingAccentFlag
					// skip
				ELSE
					iCost = 0
				ENDIF	
			ENDIF
			#ENDIF
		BREAK
		CASE MCT_METALS
			iCost = (GET_MP_METALS_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel))
		BREAK	
		CASE MCT_METALLIC
			iCost = (GET_MP_METALLIC_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel))
		BREAK			
		CASE MCT_MATTE
			iCost = (GET_MP_MATTE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel))
		BREAK
		CASE MCT_CHROME
			iCost = 25000
		BREAK
		CASE MCT_PEARLESCENT
			iCost = (GET_MP_PEARLESCENT_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel))
		BREAK
		#IF FEATURE_GEN9_EXCLUSIVE	
		CASE MCT_CHAMELEON
			iCost = (GET_MP_CHAMELEON_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel))
		BREAK
		#ENDIF
	ENDSWITCH
	
	IF bProcessPrimaryFlag
	AND NOT primary
		iCost = FLOOR(TO_FLOAT(iCost) / 2.0)
	ENDIF
	
	IF bProcessingTrimFlag
		iCost = FLOOR(TO_FLOAT(iCost) * g_sMPTunables.ftrimcolorsupermod)
	ENDIF
	
	IF bProcessingLightFlag
		iCost = FLOOR(TO_FLOAT(iCost) * g_sMPTunables.fLIGHTCOLORSUPERMOD)
	ENDIF
	
	IF bProcessingAccentFlag
		iCost = FLOOR(TO_FLOAT(iCost) * g_sMPTunables.faccentcolorsupermod)
	ENDIF
	
	RETURN iCost
ENDFUNC

PROC SAVE_MISSION_PERSONAL_CAR_MOD_VEH_DATA(VEHICLE_SETUP_STRUCT_MP sVehSetupStruct , INT iSaveSlot)
	IF iSaveSlot >= 0 AND iSaveSlot < MAX_NUMBER_OF_IE_PLAYERS
		g_sIEVehicleSetupStruct[iSaveSlot]  = sVehSetupStruct
		PRINTLN("SAVE_MISSION_PERSONAL_CAR_MOD_VEH_DATA - player: ", GET_PLAYER_NAME(PLAYER_ID()), " Slot: ",  iSaveSlot)
	ENDIF	
ENDPROC 

PROC UPDATE_PERSONAL_CAR_MOD_GARAGE_VEHICLE()
	IF g_iCurrentPVSlotInOfficeGarage != -1
		IF NOT IS_BIT_SET(g_MpSavedVehicles[g_iCurrentPVSlotInOfficeGarage].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
			SET_BIT(g_MpSavedVehicles[g_iCurrentPVSlotInOfficeGarage].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
			PRINTLN("UPDATE_PERSONAL_CAR_MOD_GARAGE_VEHICLE - updating g_iCurrentPVSlotInOfficeGarage: ", g_iCurrentPVSlotInOfficeGarage)
		ENDIF
	ELSE
		PRINTLN("UPDATE_PERSONAL_CAR_MOD_GARAGE_VEHICLE - g_iCurrentPVSlotInOfficeGarage: ", g_iCurrentPVSlotInOfficeGarage)
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_ALLOW_LIVERIES_FOR_OLD_VEHICLES(VEHICLE_INDEX vehID)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SWITCH GET_ENTITY_MODEL(vehID)	
			CASE FUTO
				IF  g_sMPTunables.bENABLE_FUTO_MODS	
					RETURN TRUE
				ENDIF
			BREAK	
			CASE PENUMBRA
				IF g_sMPTunables.bENABLE_PENUMBRA_MODS
					RETURN TRUE
				ENDIF
			BREAK	
			CASE COQUETTE2
				IF g_sMPTunables.bENABLE_COQUETTE_MODS 
					RETURN TRUE
				ENDIF
			BREAK
			CASE MAMBA
				IF g_sMPTunables.bENABLE_MAMBA_MODS 
					RETURN TRUE
				ENDIF	
			BREAK
			CASE RUINER
				IF g_sMPTunables.bENABLE_RUINER_MODS
					RETURN TRUE
				ENDIF
			BREAK
			CASE PRAIRIE
				IF g_sMPTunables.bENABLE_PRAIRIE_MODS  
					RETURN TRUE
				ENDIF
			BREAK
			CASE JESTER3
				IF g_sMPTunables.bENABLE_JESTER_MODS 
					RETURN TRUE
				ENDIF
			BREAK
			CASE ROMERO
				IF g_sMPTunables.bENABLE_HEARSE_MODS 
					RETURN TRUE
				ENDIF
			BREAK
			CASE PATRIOT
				IF g_sMPTunables.bENABLE_PATRIOT_MODS
					RETURN TRUE	
				ENDIF
			BREAK
		ENDSWITCH	
	ENDIF
	
	RETURN FALSE
ENDFUNC  

FUNC STRING GET_SUPERMODE_PREVIEW_TEXTURE(MODEL_NAMES vehModel)
	SWITCH vehModel
		CASE BRUISER		RETURN "bruiser_APOC" 		BREAK
		CASE BRUISER2		RETURN "bruiser_SCIFI" 		BREAK
		CASE BRUISER3		RETURN "bruiser_CONS" 		BREAK
		CASE DEATHBIKE		RETURN "deathbike_APOC" 	BREAK
		CASE DEATHBIKE2		RETURN "deathbike_SCIFI" 	BREAK
		CASE DEATHBIKE3		RETURN "deathbike_CONS" 	BREAK
		CASE ISSI4			RETURN "issi_APOC" 			BREAK
		CASE ISSI5			RETURN "issi_SCIFI" 		BREAK
		CASE ISSI6			RETURN "issi_CONS" 			BREAK
		CASE MONSTER3		RETURN "sasquatch_APOC" 	BREAK
		CASE MONSTER4		RETURN "sasquatch_SCIFI" 	BREAK
		CASE MONSTER5		RETURN "sasquatch_CONS" 	BREAK
		CASE SLAMVAN4		RETURN "slamvan_APOC" 		BREAK
		CASE SLAMVAN5		RETURN "slamvan_SCIFI" 		BREAK
		CASE SLAMVAN6		RETURN "slamvan_CONS" 		BREAK
		CASE DOMINATOR4		RETURN "dominator_APOC" 	BREAK
		CASE DOMINATOR5		RETURN "dominator_SCIFI" 	BREAK
		CASE DOMINATOR6		RETURN "dominator_CONS" 	BREAK
		CASE IMPALER2		RETURN "Impaler_APOC" 		BREAK
		CASE IMPALER3		RETURN "Impaler_SCIFI" 		BREAK
		CASE IMPALER4		RETURN "Impaler_CONS" 		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_SUPERMOD_PREVIEW_TEXTURE_DICT(MODEL_NAMES vehModel, INT iVariation)
	UNUSED_PARAMETER(vehModel)
	UNUSED_PARAMETER(iVariation)
	RETURN "AW_UPG_VEHS"
ENDFUNC 

FUNC BOOL IS_VEHICLE_MODEL_SCI_FI(VEHICLE_INDEX vehToCheck)
	IF DOES_ENTITY_EXIST(vehToCheck)
	AND IS_VEHICLE_DRIVEABLE(vehToCheck)
		SWITCH GET_ENTITY_MODEL(vehToCheck)
			CASE IMPERATOR2
			CASE DEATHBIKE2
			CASE IMPALER3
			CASE BRUTUS2
			CASE BRUISER2
			CASE SLAMVAN5
			CASE ISSI5
			CASE MONSTER4
			CASE SCARAB2
			CASE CERBERUS2
			CASE DOMINATOR5
			CASE ZR3802
				RETURN TRUE
			BREAK	
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_VEHICLE_MODEL_CONSUMER(VEHICLE_INDEX vehToCheck)
	IF DOES_ENTITY_EXIST(vehToCheck)
	AND IS_VEHICLE_DRIVEABLE(vehToCheck)
		SWITCH GET_ENTITY_MODEL(vehToCheck)
			CASE IMPERATOR3
			CASE DEATHBIKE3
			CASE IMPALER4
			CASE BRUTUS3
			CASE BRUISER3
			CASE SLAMVAN6
			CASE ISSI6
			CASE MONSTER5
			CASE SCARAB3
			CASE CERBERUS3
			CASE DOMINATOR6
			CASE ZR3803
				RETURN TRUE
			BREAK	
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_VEHICLE_ALLOWED_IN_SUPERMOD_TYPE(MODEL_NAMES eModel, SUPERMOD_TYPE supermodType)
	SWITCH eModel
		CASE TECHNICAL3
		CASE INSURGENT3
			IF supermodType = SUPERMOD_TYPE_WEAPONIZED
				RETURN TRUE
			ENDIF	
		BREAK	
		CASE MONSTER3 
		CASE BRUISER
		CASE BRUISER2
		CASE BRUISER3
		CASE GLENDALE
		CASE IMPALER2 
		CASE IMPALER
		CASE IMPALER3 
		CASE IMPALER4 
		CASE VAMOS
		CASE ISSI4
		CASE DEATHBIKE
		CASE DEATHBIKE2
		CASE DEATHBIKE3
		CASE DOMINATOR
		CASE DOMINATOR2
		CASE DOMINATOR4
		CASE DOMINATOR5
		CASE DOMINATOR6
		CASE SLAMVAN4
		CASE IMPERATOR
		CASE ZR380
		CASE CERBERUS
		CASE SCARAB
		CASE BRUTUS
		CASE IMPERATOR2
		CASE IMPERATOR3
		CASE ZR3802
		CASE ZR3803
		CASE CERBERUS2
		CASE SCARAB2
		CASE BRUTUS2
		CASE CERBERUS3
		CASE SCARAB3
		CASE BRUTUS3
		CASE SLAMVAN5
		CASE SLAMVAN6
		CASE MONSTER4 
		CASE MONSTER5 
		CASE ISSI5
		CASE ISSI6
			IF supermodType =  SUPERMOD_TYPE_ARENA_WAR
				RETURN TRUE
			ENDIF	
		BREAK
		DEFAULT 
			IF supermodType =  SUPERMOD_TYPE_BENNY
				RETURN TRUE
			ENDIF	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_SUPERMOD_MENU_NAME(SUPERMOD_TYPE supermodType)
	SWITCH supermodType
		CASE SUPERMOD_TYPE_BENNY
			RETURN "CMM_MOD_BOM"
		BREAK
		CASE SUPERMOD_TYPE_WEAPONIZED
			RETURN "CMM_MOD_UWE"
		BREAK
		CASE SUPERMOD_TYPE_ARENA_WAR
			RETURN "CMM_MOD_ARW"
		BREAK
	ENDSWITCH
	
	RETURN "CMM_MOD_BOM"
ENDFUNC

FUNC STRING GET_SUPERMOD_MENU_TITLE(SUPERMOD_TYPE supermodType)
	SWITCH supermodType
		CASE SUPERMOD_TYPE_BENNY
			RETURN "CMM_MOD_BOMT"
		BREAK
		CASE SUPERMOD_TYPE_WEAPONIZED
			RETURN "CMM_MOD_UWET"
		BREAK
		CASE SUPERMOD_TYPE_ARENA_WAR
			RETURN "CMM_MOD_ARWT"
		BREAK
	ENDSWITCH
	
	RETURN "CMM_MOD_BOMT"
ENDFUNC

FUNC INT GET_NUM_SUPERMOD_TYPE(MODEL_NAMES eModel)
	
	SWITCH eModel
		CASE SLAMVAN
		CASE GLENDALE
			RETURN 2
		BREAK	
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC SUPERMOD_TYPE GET_SUPERMOD_TYPE(MODEL_NAMES eModel, INT iVariation)
	
	SWITCH eModel
		CASE SLAMVAN
		CASE GLENDALE
			IF iVariation = 0 
				RETURN SUPERMOD_TYPE_BENNY
			ELSE
				RETURN SUPERMOD_TYPE_ARENA_WAR
			ENDIF
		BREAK	
		CASE TECHNICAL
		CASE INSURGENT
			RETURN SUPERMOD_TYPE_WEAPONIZED
		BREAK
		CASE RATLOADER
		CASE RATLOADER2
		CASE DEVESTE
		CASE ISSI3
		CASE GARGOYLE	
		CASE MONSTER3 
		CASE BRUISER
		CASE BRUISER2
		CASE BRUISER3
		CASE IMPALER2 
		CASE IMPALER
		CASE IMPALER3 
		CASE IMPALER4 
		CASE VAMOS
		CASE ISSI4
		CASE DEATHBIKE
		CASE DEATHBIKE2
		CASE DEATHBIKE3
		CASE DOMINATOR
		CASE DOMINATOR2
		CASE DOMINATOR4
		CASE DOMINATOR5
		CASE DOMINATOR6
		CASE SLAMVAN4
		CASE IMPERATOR
		CASE ZR380
		CASE CERBERUS
		CASE SCARAB
		CASE BRUTUS
		CASE IMPERATOR2
		CASE IMPERATOR3
		CASE ZR3802
		CASE ZR3803
		CASE CERBERUS2
		CASE SCARAB2
		CASE BRUTUS2
		CASE CERBERUS3
		CASE SCARAB3
		CASE BRUTUS3
		CASE SLAMVAN5
		CASE SLAMVAN6
		CASE MONSTER4 
		CASE MONSTER5 
		CASE ISSI5
		CASE ISSI6
			RETURN SUPERMOD_TYPE_ARENA_WAR
		BREAK
	ENDSWITCH
	
	RETURN SUPERMOD_TYPE_BENNY
ENDFUNC 

