USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_pad.sch"
USING "menu_public.sch"
USING "shop_public.sch"
USING "socialclub_leaderboard.sch"

STRUCT COMMERCE_STORE_TRIGGER_DATA
	INT iCommerceStoreStage
	BOOL bOpenCommerceStore
	BOOL bPlayerSignedIn
	BOOL bProcessSignInAlert
	BOOL bPlayerHasConnection
	BOOL bProcessConnectionAlert
	SHOP_NAME_ENUM eShop
	INT iShopVariation
ENDSTRUCT

FUNC BOOL IS_PLAYER_CONNECTED_FOR_STORE()
	RETURN NETWORK_IS_CABLE_CONNECTED()
ENDFUNC

PROC INITIALISE_COMMERCE_STORE_TRIGGER_DATA(COMMERCE_STORE_TRIGGER_DATA &sData, SHOP_NAME_ENUM eShop, INT iShopVariation = 0)
	sData.iCommerceStoreStage = 0
	sData.bOpenCommerceStore = FALSE
	sData.bPlayerSignedIn = FALSE
	sData.bProcessSignInAlert = FALSE
	sData.bPlayerHasConnection = FALSE
	sData.bProcessConnectionAlert = FALSE
	sData.eShop = eShop
	sData.iShopVariation = iShopVariation
ENDPROC

FUNC BOOL CHECK_COMMERCE_STORE_TRIGGER(COMMERCE_STORE_TRIGGER_DATA &sData)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	
		// Only allow the trigger to work when it's in the help menu.
		INT i
		REPEAT MAX_STORED_HELP_KEYS i
			IF GET_HASH_KEY(g_sMenuData.tl15HelpText[i]) = HASH("ITEM_PSSTORE")
			OR GET_HASH_KEY(g_sMenuData.tl15HelpText[i]) = HASH("ITEM_MARKET")
			OR GET_HASH_KEY(g_sMenuData.tl15HelpText[i]) = HASH("ITEM_STORE")
				PRINTLN("[STORE] Opening store for script '", GET_THIS_SCRIPT_NAME(), "'")
				sData.bOpenCommerceStore = TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN sData.bOpenCommerceStore
ENDFUNC
PROC ADD_COMMERCE_STORE_TRIGGER(COMMERCE_STORE_TRIGGER_DATA &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT g_sShopSettings.bBlockCommerceStore
	
		BOOL bTriggerBlocked = FALSE
		
		SWITCH GET_SHOP_TYPE_ENUM(sData.eShop)
			CASE SHOP_TYPE_CARMOD
			CASE SHOP_TYPE_PERSONAL_CARMOD
				IF g_sMPTunables.bDISABLE_STORE_PROMPT_CARMOD
				OR IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(sData.eShop, sData.iShopVariation)
					bTriggerBlocked = TRUE
				ENDIF
			BREAK
			CASE SHOP_TYPE_HAIRDO
				bTriggerBlocked = g_sMPTunables.bDISABLE_STORE_PROMPT_HAIRDO
			BREAK
			CASE SHOP_TYPE_TATTOO
				bTriggerBlocked = g_sMPTunables.bDISABLE_STORE_PROMPT_TATTOO
			BREAK
			CASE SHOP_TYPE_GUN
				bTriggerBlocked = g_sMPTunables.bDISABLE_STORE_PROMPT_GUNSHOP
			BREAK
			CASE SHOP_TYPE_CLOTHES
				bTriggerBlocked = g_sMPTunables.bDISABLE_STORE_PROMPT_CLOTHES
			BREAK
		ENDSWITCH
		
		IF NOT bTriggerBlocked
			IF IS_PLAYSTATION_PLATFORM()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y, "ITEM_PSSTORE")
			ELIF IS_XBOX_PLATFORM()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y, "ITEM_MARKET")
			ELIF IS_PC_VERSION()
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_STORE")
			ENDIF
		ENDIF
		
		sData.bPlayerSignedIn = NETWORK_IS_SIGNED_ONLINE()
		sData.bPlayerHasConnection = IS_PLAYER_CONNECTED_FOR_STORE()
	ENDIF
ENDPROC
FUNC BOOL PROCESS_COMMERCE_STORE_TRIGGER(COMMERCE_STORE_TRIGGER_DATA &sData)
	IF sData.bOpenCommerceStore
		IF sData.iCommerceStoreStage = 0
			sData.iCommerceStoreStage++
			
		ELIF sData.iCommerceStoreStage = 1
			BOOL bLaunchStore = FALSE
			BOOL bCancelStore = FALSE
			BOOL bSignIn = FALSE
			BOOL bConnectCable = FALSE
			
			IF IS_PLAYER_CONNECTED_FOR_STORE()
				IF NETWORK_IS_SIGNED_ONLINE()
					
					IF NETWORK_IS_ACTIVITY_SESSION()
						FE_WARNING_FLAGS eFlags = FE_WARNING_OK
						SET_WARNING_MESSAGE_WITH_HEADER("GLOBAL_ALERT_DEFAULT", "STORE_UNAVAIL", eFlags)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							bCancelStore = TRUE
						ENDIF
						PRINTLN("[STORE_TRIGGER] Blocked : NETWORK_IS_ACTIVITY_SESSION")
						
					ELIF NOT IS_USER_OLD_ENOUGH_TO_ACCESS_STORE()
					
						FE_WARNING_FLAGS eFlags = FE_WARNING_OK
						
						IF IS_XBOX_PLATFORM()
							NETWORK_CHECK_PRIVILEGES(0, ENUM_TO_INT(PRIVILEGE_TYPE_DURANGO_PURCHASE_CONTENT), TRUE)
							NETWORK_SET_PRIVILEGE_CHECK_RESULT_NOT_NEEDED() // Forget the result once were done.
							bCancelStore = TRUE
							//SET_WARNING_MESSAGE_WITH_HEADER("GLOBAL_ALERT_DEFAULT", "HUD_STORE_PERMISSIONS", eFlags)
						ELSE
							SET_WARNING_MESSAGE_WITH_HEADER("GLOBAL_ALERT_DEFAULT", "HUD_PERM", eFlags)
						ENDIF
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							bCancelStore = TRUE
						ENDIF
						PRINTLN("[STORE_TRIGGER] Blocked : IS_USER_OLD_ENOUGH_TO_ACCESS_STORE")
						
					ELIF NOT IS_STORE_AVAILABLE_TO_USER()
						FE_WARNING_FLAGS eFlags = FE_WARNING_OK
						SET_WARNING_MESSAGE_WITH_HEADER("GLOBAL_ALERT_DEFAULT", "STORE_UNAVAIL", eFlags)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							bCancelStore = TRUE
						ENDIF
						PRINTLN("[STORE_TRIGGER] Blocked : IS_STORE_AVAILABLE_TO_USER")
						
					ELSE
						bLaunchStore = TRUE
						PRINTLN("[STORE_TRIGGER] bLaunchStore = TRUE")
					ENDIF
				ELSE
					bSignIn = TRUE
					PRINTLN("[STORE_TRIGGER] bSignIn = TRUE")
				ENDIF
			ELSE
				bConnectCable = TRUE
				PRINTLN("[STORE_TRIGGER] bConnectCable = TRUE")
			ENDIF
			
			IF bConnectCable
				sData.iCommerceStoreStage = 0
				sData.bOpenCommerceStore = FALSE
				sData.bProcessConnectionAlert = TRUE
			ELIF bSignIn
				sData.iCommerceStoreStage = 3
			ELIF bLaunchStore
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					g_bStoreTransitionForCash = TRUE
					g_bStoreTransitionForCashRenderPaused = TRUE
					PRINTLN("PROCESS_STORE_ALERT - pausing render phases")
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1,"NAV","HUD_AMMO_SHOP_SOUNDSET")
				
				PRINTLN("Opening commerce store.")
				SET_LAST_VIEWED_SHOP_ITEM(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.eShop)))
				OPEN_COMMERCE_STORE("", "", GET_SHOP_PURCHASE_LOCATION_FOR_STORE(sData.eShop))
				sData.iCommerceStoreStage++
				PRINTLN("[STORE_TRIGGER] Opening store [1]")
			ELIF bCancelStore
				sData.iCommerceStoreStage++
				PRINTLN("[STORE_TRIGGER] Cancel store opening")
			ENDIF
			
		ELIF sData.iCommerceStoreStage = 2
			IF NOT IS_COMMERCE_STORE_OPEN()
				sData.iCommerceStoreStage = 0
				sData.bOpenCommerceStore = FALSE
				PRINTLN("[STORE_TRIGGER] Store now closed")
			ENDIF
			
		ELIF sData.iCommerceStoreStage = 3
			INT iSignInBS
			IF DO_SIGNED_OUT_WARNING(iSignInBS, TRUE)
				IF NETWORK_IS_SIGNED_ONLINE()
					sData.iCommerceStoreStage = 4
					PRINTLN("[STORE_TRIGGER] Signed in, wait for data fetch")
				ELSE
					sData.iCommerceStoreStage = 2
					PRINTLN("[STORE_TRIGGER] Sign in failed")
				ENDIF
			ELSE
				PRINTLN("[STORE_TRIGGER] Signing in...")
			ENDIF
		ELIF sData.iCommerceStoreStage = 4
			IF NETWORK_IS_SIGNED_ONLINE()
				IF IS_COMMERCE_DATA_FETCH_IN_PROGRESS()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						g_bStoreTransitionForCash = TRUE
						g_bStoreTransitionForCashRenderPaused = TRUE
						PRINTLN("PROCESS_STORE_ALERT - pausing render phases")
					ENDIF
					
					IF IS_STORE_AVAILABLE_TO_USER()
						SET_LAST_VIEWED_SHOP_ITEM(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.eShop)))
						OPEN_COMMERCE_STORE("", "", GET_SHOP_PURCHASE_LOCATION_FOR_STORE(sData.eShop))
						PRINTLN("[STORE_TRIGGER] Opening store [2]")
					ELSE
						PRINTLN("[STORE_TRIGGER] Blocked : IS_STORE_AVAILABLE_TO_USER")
					ENDIF
					sData.iCommerceStoreStage = 2
				ELSE
					PRINTLN("[STORE_TRIGGER] Waiting for data fetch...")
				ENDIF
			ELSE
				sData.iCommerceStoreStage = 2
				PRINTLN("[STORE_TRIGGER] No longer signed in")
			ENDIF
		ENDIF
		
		// Still processing...
		RETURN TRUE
		
	ELIF (sData.bPlayerHasConnection AND NOT IS_PLAYER_CONNECTED_FOR_STORE() AND NOT NETWORK_IS_GAME_IN_PROGRESS())
	OR sData.bProcessConnectionAlert
		sData.bProcessConnectionAlert = TRUE
		FE_WARNING_FLAGS iButtonBS = FE_WARNING_OK
		SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "STORE_CON_ONL", iButtonBS)
		
		IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			sData.bPlayerHasConnection = IS_PLAYER_CONNECTED_FOR_STORE()
			sData.bProcessConnectionAlert = FALSE
		ENDIF
		
		PRINTLN("[STORE_TRIGGER] Connection check...")
		
		// Still processing...
		RETURN TRUE
		
	ELIF (sData.bPlayerSignedIn AND NOT NETWORK_IS_SIGNED_ONLINE() AND NOT NETWORK_IS_GAME_IN_PROGRESS())
	OR sData.bProcessSignInAlert
		sData.bProcessSignInAlert = TRUE
		FE_WARNING_FLAGS iButtonBS = FE_WARNING_OK
		SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "STORE_SGN_ONL2", iButtonBS)
		
		IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			sData.bPlayerSignedIn = NETWORK_IS_SIGNED_ONLINE()
			sData.bProcessSignInAlert = FALSE
		ENDIF
		
		PRINTLN("[STORE_TRIGGER] Sign in check...")
		
		// Still processing...
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
