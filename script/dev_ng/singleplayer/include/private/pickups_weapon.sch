// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "script_debug.sch"

CONST_INT NUMBER_OF_WEAPON_PICKUPS(102)
CONST_INT NUMBER_OF_EXILE1_PICKUPS(9)
CONST_INT NUMBER_OF_EXILE1_CGTONG_PICKUPS(1)
CONST_INT NUMBER_OF_TREVOR1_CGTONG_PICKUPS(1)

#IF IS_DEBUG_BUILD

	/// PURPOSE: 
	///     Returns name of pickup type at Exile 1 plane wreckage
	FUNC STRING GET_EXILE1_PICKUP_NAME(INT iIndex)
		SWITCH iIndex
			CASE 0  RETURN "Grenade Launcher" BREAK
			CASE 1  RETURN "Rocket Launcher"  BREAK
			CASE 2  RETURN "Combat MG"        BREAK
			CASE 3  RETURN "Body Armour"      BREAK
			CASE 4  RETURN "AP Pistol"        BREAK
			CASE 5  RETURN "Shotgun"          BREAK
			CASE 6  RETURN "Sniper Rifle"     BREAK
			CASE 7  RETURN "Sticky Bomb"      BREAK
			CASE 8  RETURN "Sticky Bomb"      BREAK
		ENDSWITCH
		
		RETURN "Invalid Location Index" 
	ENDFUNC
	
	/// PURPOSE: Returns name of weapon type and location
	FUNC STRING GET_WEAPON_LOCATION_NAME(INT iIndex)
		SWITCH iIndex
			CASE 0  RETURN "01: Assault Rifle: Large Mansion Balcony in Richman"	BREAK
			CASE 1  RETURN "02: Assault Rifle: Inside Army Base Security Booth" 	BREAK
			CASE 2  RETURN "03: Assault Rifle: Road Fork by Park" 					BREAK
			CASE 3  RETURN "04: Assault Rifle: Cement Works"						BREAK
			CASE 4  RETURN "05: Assault Rifle: Forest Clearing" 					BREAK
			CASE 5  RETURN "06: Assault Rifle: Beach Viewpoint" 					BREAK
			CASE 6  RETURN "07: Grenade: Arthur's Stool" 							BREAK
			CASE 7  RETURN "08: Grenade: Chumash Beach House" 						BREAK
			CASE 8  RETURN "09: Grenade: End of Jetty Blast Fishing" 				BREAK
			CASE 9  RETURN "10: Grenade: Gas Station" 								BREAK

			CASE 10 RETURN "11: Grenade Launcher: Airport Hanger Walkway" 			BREAK
			CASE 11 RETURN "12: Grenade launcher: Underwater in cave" 				BREAK
			CASE 12 RETURN "13: Grenade Launcher: Top of Bridge (Bike needed)"		BREAK
			CASE 13 RETURN "14: Minigun: Army Control Tower (Office floor)" 		BREAK
			CASE 14 RETURN "15: Machine Gun: Bollingbroke Penitentiary"				BREAK
			CASE 15 RETURN "16: Machine Gun: Freeway under Rail Bridge" 	 		BREAK
			CASE 16 RETURN "17: Machine Gun: Under Freeway" 						BREAK
			CASE 17 RETURN "18: Machine Gun: Vagos Ganghouse" 						BREAK
			CASE 18 RETURN "19: Pistol: Alleyway" 									BREAK
			CASE 19 RETURN "20: Pistol: Bleachers in University Track and Field"	BREAK
			
			CASE 20 RETURN "21: Pistol: Burned Down House" 							BREAK
			CASE 21 RETURN "22: Pistol: Drain under Road (Paleto)" 					BREAK
			CASE 22 RETURN "23: Pistol: Gas Station" 								BREAK
			CASE 23 RETURN "24: Pistol: Old Bus in Scrapyard" 						BREAK
			CASE 24 RETURN "25: Pistol: Open Grave" 								BREAK
			CASE 25 RETURN "26: Pistol: Power Station Alleyway" 					BREAK
			CASE 26 RETURN "27: Pistol: Radio Station Dumpster" 					BREAK
			CASE 27 RETURN "28: Pistol: Train yard Storage Tank" 					BREAK
			CASE 28 RETURN "29: RPG: Altruist Camp Church (Ammo Crates)" 			BREAK
			CASE 29 RETURN "30: RPG: Government Facility Roof (use ladders)" 		BREAK
			
			CASE 30 RETURN "31: RPG: Top of Bridge" 								BREAK
			CASE 31 RETURN "32: Sawn-Off: Lost Clubhouse exterior green box"		BREAK
			CASE 32 RETURN "33: Sawn-Off: Vespucci Police Car Park" 				BREAK
			CASE 33 RETURN "34: Sawn-Off: Crop Tunnel" 								BREAK
			CASE 34 RETURN "35: Sawn-Off: House Porch" 								BREAK
			CASE 35 RETURN "36: Sawn-Off: Lost Trailer Park Shack" 					BREAK
			CASE 36 RETURN "37: Sawn-Off: Rail Cabin" 								BREAK
			CASE 37 RETURN "38: SMG: Back yard in Grove Street" 					BREAK
			CASE 38 RETURN "39: SMG: El Cafe Rojo de Madera" 						BREAK
			CASE 39 RETURN "40: SMG: Quarry" 										BREAK
			
			CASE 40 RETURN "41: SMG: Rehab Center in Vinewood Hills" 				BREAK
			CASE 41 RETURN "42: SMG: Under Del Perro Beach Pier" 					BREAK
			CASE 42 RETURN "43: SMG: Under Rockford Plaza" 							BREAK
			CASE 43 RETURN "44: Sniper Rifle: Airport Radar Tower" 					BREAK
			CASE 44 RETURN "45: Sniper Rifle: Observatory Viewpoint" 				BREAK
			CASE 45 RETURN "46: Sniper Rifle: Power Plant" 							BREAK
			CASE 46 RETURN "47: Sniper Rifle: Water Tower" 							BREAK
			CASE 47 RETURN "48: Sniper Rifle: Wind Farm (Overlooking Prison)" 		BREAK
			CASE 48 RETURN "49: Sticky Bomb: Vespucci Canals House Construction"	BREAK
			CASE 49 RETURN "50: Sticky Bomb: Fancy House" 							BREAK
			
			CASE 50 RETURN "51: Rocket Launcher: Airport" 							BREAK
			CASE 51 RETURN "52: Pistol: Hills - Drain Entrance" 		 			BREAK
			CASE 52 RETURN "53: Pistol: Hills - Cabin Rooftop" 		 				BREAK
			CASE 53 RETURN "54: Sniper Rifle: Hills - Forest" 				   		BREAK
			CASE 54 RETURN "55: Shotgun: Hills - Rocks in Park" 					BREAK
			CASE 55 RETURN "56: Bat: On roof in Vinewood"		 					BREAK
			CASE 56 RETURN "57: Bat: Skip enclosure on Elysian Island" 				BREAK
			CASE 57 RETURN "58: Bat: near stairs" 									BREAK
			CASE 58 RETURN "59: Bat: soccer pitch" 									BREAK
			CASE 59 RETURN "60: Bat: baseball field" 								BREAK
			
			CASE 60 RETURN "61: Bat: house porch" 									BREAK
			CASE 61 RETURN "62: Bat: computer room" 								BREAK
			CASE 62 RETURN "63: Bat: near dumpster" 								BREAK
			CASE 63 RETURN "64: Bat: graffiti" 				 						BREAK
			CASE 64 RETURN "65: Bat: storage room" 									BREAK
			CASE 65 RETURN "66: Crowbar: Bristols Coke Storage" 					BREAK
			CASE 66 RETURN "67: Crowbar: Parking lot booth" 						BREAK
			CASE 67 RETURN "68: Crowbar: house" 									BREAK
			CASE 68 RETURN "69: Crowbar: Auto repairs" 								BREAK
			CASE 69 RETURN "70: Crowbar: seats" 									BREAK
			
			CASE 70 RETURN "71: Crowbar: Dusty portaloos" 							BREAK
			CASE 71 RETURN "72: Crowbar: Near dumpster" 							BREAK
			CASE 72 RETURN "73: Crowbar: small building" 							BREAK
			CASE 73 RETURN "74: Crowbar: Zancudo River/Alamo Sea shack"				BREAK
			CASE 74 RETURN "75: Crowbar: work yard" 								BREAK
			CASE 75 RETURN "76: Molotov: No surfing"								BREAK
			CASE 76 RETURN "77: Molotov: Cardboard boxes"							BREAK
			CASE 77 RETURN "78: Molotov: Dumpster corner"							BREAK
			CASE 78 RETURN "79: Molotov: Jetsam"									BREAK
			CASE 79 RETURN "80: Molotov: Behind Brouge Iron Works"					BREAK
			
			CASE 80 RETURN "81: Molotov: silos" 									BREAK
			CASE 81 RETURN "82: Molotov: junk near water" 							BREAK
			CASE 82 RETURN "83: Molotov: water container" 							BREAK
			CASE 83 RETURN "84: Molotov: Under bridge" 			   					BREAK
			CASE 84 RETURN "85: Molotov: Behind Suburban, Chumash"				  	BREAK
			CASE 85 RETURN "86: Crowbar: Tyres"										BREAK
			CASE 86 RETURN "87: Bat: Winch" 			 							BREAK
			CASE 87 RETURN "88: Crowbar: Unfinished house" 							BREAK
			CASE 88 RETURN "89: Crowbar: By cable spools" 							BREAK
			CASE 89 RETURN "90: Bat: Another unfinished house"	 	 				BREAK
			
			CASE 90 RETURN "91: Crowbar: YOU TOOL entrance"							BREAK
			CASE 91 RETURN "92: Bat: Farmhouse" 			 						BREAK
			CASE 92 RETURN "93: Crowbar: Construction parts" 						BREAK
			CASE 93 RETURN "94: Bat: Altruist camp"	 								BREAK
			CASE 94 RETURN "96: Crowbar: Junk cars"								 	BREAK
			CASE 95 RETURN "98: Carbine rifle: Trailer storage area" 				BREAK
			CASE 96 RETURN "99: Assault shotgun: in hole under shootout"			BREAK
			CASE 97 RETURN "100: Assault shotgun: Altruist Camp Church"				BREAK
			CASE 98 RETURN "101: Parachute: Cable Car"	 							BREAK
			CASE 99 RETURN "102: Shotgun: Galilee boat house"						BREAK

			CASE 100 RETURN "103: Parachute: The tallest crane"						BREAK
			CASE 101 RETURN "104: Sawn-Off: O'Neill's farm - barn"					BREAK
		ENDSWITCH
		
		RETURN "Invalid Location Index" 
	ENDFUNC
#ENDIF

/// PURPOSE: 
///    Returns coords of weapon pickup at Exile 1 plane wreckage
FUNC VECTOR GET_EXILE1_PICKUP_COORDS(INT iIndex)

	SWITCH iIndex
		CASE 0 RETURN << 346.89, 3999.51, -6.4 >>   BREAK
		CASE 1 RETURN << 303.80, 3949.80, -5.19 >>  BREAK
		CASE 2 RETURN << 308.5, 3972.7, -8.16 >>    BREAK
		CASE 3 RETURN << 295.2, 3961.0, -7.74 >>    BREAK
		CASE 4 RETURN << 293.0, 3969.6, -9.21 >>    BREAK
		CASE 5 RETURN << 289.23, 3955.22, -6.54 >>  BREAK
		CASE 6 RETURN << 302.19, 3983.12, -11.02 >> BREAK
		CASE 7 RETURN << 282.4, 3966.9, -7.63 >>    BREAK
		CASE 8 RETURN << 317.80, 3968.13, -10.11 >> BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid index passed to GET_EXILE1_PICKUP_COORDS")
	RETURN <<0,0,0>>
	
ENDFUNC

/// PURPOSE: 
///    Returns coords of weapon pickup at Exile 1 plane wreckage
FUNC VECTOR GET_EXILE1_CGTONG_PICKUP_COORDS(INT iIndex)

	SWITCH iIndex
		CASE 0 RETURN << 299.41788, 3942.44336, -3.18848 >>   BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid index passed to GET_EXILE1_CGTONG_PICKUP_COORDS")
	RETURN <<0,0,0>>
	
ENDFUNC

/// PURPOSE: 
///    Returns coords of weapon pickup at Trevor 1 pickup
FUNC VECTOR GET_TREVOR1_CGTONG_PICKUP_COORDS(INT iIndex)

	SWITCH iIndex
		CASE 0 RETURN << -35.64201, 2871.74414, 58.61521 >>   BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid index passed to GET_TREVOR1_CGTONG_PICKUP_COORDS")
	RETURN <<0,0,0>>
	
ENDFUNC

/// PURPOSE: 
///    Returns coords of weapon pickup
FUNC VECTOR GET_WEAPON_PICKUP_COORDS(INT iIndex, BOOL bForChop=FALSE)
		
	// If any of these have a bForChop vector set, Chop will lead the player to the bForChop vector then orientate himself towards the actual pickup vector.
	// Small alterations are fine but if you move a pickup more than 1-2 metres away from its current position you must update the bForChop vector too or else we'll have Chop leading the player to nothing.
	// The bForChop vector must be on a section of navmesh that is connected to the main navmesh. See Kev E if unsure.
	SWITCH iIndex
	
		CASE 0
			IF bForChop = TRUE
				RETURN << -1760.36, 428.60, 126.31 >>
			ELSE
				RETURN << -1756.974, 427.531, 126.70 >>
			ENDIF
		BREAK
		CASE 1  RETURN << -1595.7422, 2799.3706, 16.0205 >>  BREAK
		CASE 2
			IF bForChop = TRUE
				RETURN << -197.14859, 1482.68457, 288.25299 >>
			ELSE
				RETURN << -200.0114, 1481.6971, 289.4303 >>
			ENDIF
		BREAK
		CASE 3  RETURN <<256.7700, 2841.0466, 42.6599>> 	 BREAK
		CASE 4  RETURN << -1360.2231, 4435.7515, 27.7754 >>	 BREAK
		CASE 5  RETURN << 64.6300, 7051.2534, 15.8272 >> 	 BREAK
		CASE 6  RETURN << -456.2838, 1593.7988, 358.0119 >>  BREAK
		CASE 7
			IF bForChop = TRUE
				RETURN << -3114.61694, 749.84454, 11.59352 >>
			ELSE
				RETURN << -3111.6143, 749.2816, 12.6285 >>
			ENDIF
		BREAK
		CASE 8  RETURN << 713.6797, 4095.9688, 33.765 >>		BREAK
		CASE 9  RETURN << 255.2671, 2586.2815, 43.9619 >> 	BREAK
		CASE 10 RETURN << -1292.64, -3345.82, 23.27 >>		BREAK
		CASE 11 RETURN << 3111.5310, 2214.8481, -8.6457 >> 	BREAK
		CASE 12
			IF bForChop = TRUE
				RETURN << 555.82, -839.05, 39.84 >>
			ELSE
				RETURN << 555, -837.810, 55.080 >>
			ENDIF
		BREAK
		CASE 13 RETURN << -2360, 3245, 91.9 >> 				 BREAK
		CASE 14 RETURN << 1691.1517, 2640.8589, 54.0693 >> 	 BREAK
		CASE 15 RETURN << 1982.370, 6203.640, 41.150 >> 	 BREAK
		CASE 16 RETURN << -2979, 1588, 23.3 >>				 BREAK
		CASE 17 RETURN << 963.5308, -1824.0399, 30.0708 >> 	 BREAK
		CASE 18 RETURN << -295.41867, 6188.78564, 30.48932 >> 	 BREAK
		CASE 19 RETURN << -1778, 124, 67.9 >> 				 BREAK
		CASE 20
			IF bForChop = TRUE
				RETURN << 157.57587, 3133.99414, 42.45733 >>
			ELSE
				RETURN << 161.2925, 3131.8538, 42.5892 >>
			ENDIF
		BREAK
		CASE 21 RETURN << -888, 5406, 30.5 >> 				 BREAK
		CASE 22 RETURN << -1840, 793, 138.7 >> 				 BREAK
		CASE 23
			IF bForChop = TRUE
				RETURN << -495.80392, -1750.20667, 17.42509 >>
			ELSE
				RETURN << -495.9968, -1747.2310, 18.5194 >>
			ENDIF
		BREAK
		CASE 24
			IF bForChop = TRUE
				RETURN << -1764.69495, -265.58093, 47.61870 >>
			ELSE
				RETURN << -1763.3588, -263.2062, 47.1481 >>
			ENDIF
		BREAK
		CASE 25
			IF bForChop = TRUE
				RETURN << 548.0715, -1642.4506, 27.2033 >>
			ELSE
				RETURN << 548.5943, -1642.6030, 27.4425 >>
			ENDIF
		BREAK
		CASE 26 RETURN << 736.0563, 2595.5559, 72.9165 >>		BREAK
		CASE 27
			IF bForChop = TRUE
				RETURN << 701.00092, -1179.70154, 23.28737 >>
			ELSE
				RETURN << 699.2748, -1181.6705, 33.2608 >>
			ENDIF
		BREAK
		CASE 28
			IF bForChop = TRUE
				RETURN << -1146.74, 4938.93, 221.27 >>
			ELSE
				RETURN << -1149.4370, 4940.3618, 221.3282 >> // Altruist RPG updated for B*1346216
			ENDIF
		BREAK
		CASE 29
			IF bForChop = TRUE
				RETURN << 2543.91, -432.76, 93.12 >>
			ELSE
				RETURN << 2513.5347, -442.8484, 113.0888 >>
			ENDIF
		BREAK
		CASE 30 RETURN << -553.1764, -2239.8767, 121.3704 >> BREAK
		CASE 31
			IF bForChop = TRUE
				RETURN << 987.16, -108.02, 73.30 >>
			ELSE
				RETURN << 987.15, -105.88, 74.13 >>
			ENDIF
		BREAK
		CASE 32 RETURN << -1048, -836, 10.5 >>				BREAK
		CASE 33 RETURN << 312.6026, 6606.8965, 27.8835 >>	BREAK
		CASE 34 RETURN << -37.21, 2871.26, 58.61 >>			BREAK
		CASE 35
			IF bForChop = TRUE
				RETURN << 61.91, 3690.51, 38.83 >>
			ELSE
				RETURN << 59, 3692, 38.92 >>
			ENDIF
		BREAK
		CASE 36
			IF bForChop = TRUE
				RETURN << 2933.33, 4617.07, 47.72 >>
			ELSE
				RETURN << 2934.499, 4617.067, 47.736 >>
			ENDIF
		BREAK
		CASE 37 RETURN << 57.8219, -1932.1208, 20.4939 >> 	 BREAK
		CASE 38 RETURN << 378.26, -334.30, 45.63 >> 		 BREAK
		CASE 39
			IF bForChop = TRUE
				RETURN << 2673.66235, 2793.70215, 31.81240 >>
			ELSE
				RETURN << 2676.0454, 2793.2493, 39.5934 >>
			ENDIF
		BREAK
		CASE 40 RETURN << -1582.2780, 770.3486, 188.1942 >>  BREAK
		CASE 41
			IF bForChop = TRUE
				RETURN << -1641.13904, -1032.81519, 4.63165 >>
			ELSE
				RETURN << -1640.5464, -1034.1287, 4.6074 >>
			ENDIF
		BREAK
		CASE 42
			IF bForChop = TRUE
				RETURN << -151.09, -214.36, 44.73 >>
			ELSE
				RETURN << -159.7137, -209.1721, 48.9380 >>
			ENDIF
		BREAK
		CASE 43 RETURN << -1276.9862, -2451.8862, 72.0481 >> BREAK
		CASE 44
			IF bForChop = TRUE
				RETURN << -403.58267, 1056.21167, 322.84143 >>
			ELSE
				RETURN << -402, 1053, 323 >>
			ENDIF
		BREAK
		CASE 45 RETURN << 2728.9866, 1576.3821, 65.5818 >>	 BREAK
		CASE 46
			IF bForChop = TRUE
				RETURN << -221.27, 6133.62, 30.48 >>
			ELSE
				RETURN << -215.9002, 6125.2700, 56.2780 >>
			ENDIF
		BREAK
		CASE 47 RETURN << 2092.2253, 2492.3015, 89.4046 >> 	 BREAK
		CASE 48 RETURN << -1124.6660, -967.1614, 5.6319 >> 	 BREAK
		CASE 49
			IF bForChop = TRUE
				RETURN << -405.52991, 323.40634, 107.72269 >>
			ELSE
				RETURN << -405, 325, 108.7 >>
			ENDIF
		BREAK
		CASE 50 RETURN << -1087.310, -2429.300, 13.000 >> BREAK
		CASE 51 RETURN << -1042.6445, 879.7763, 160.2144 >>	 BREAK
		CASE 52
			IF bForChop = TRUE
				RETURN << -1081.22888, 678.94543, 141.80563 >>
			ELSE
				RETURN << -1078.3499, 682.2944, 144.904 >>
			ENDIF
		BREAK
		CASE 53 RETURN << -402.0679, 1355.0619, 329.8384 >>	 BREAK
		CASE 54 RETURN << 153.3876, 713.6584, 207.2551 >>	 BREAK
		CASE 55 RETURN << 186.9000, 243.0269, 140.5500 >>	 BREAK
		
		//bats
		CASE 56
			IF bForChop = TRUE
				RETURN <<302.01, -3254.45, 4.80>>
			ELSE
				RETURN <<304, -3259, 5.0>>
			ENDIF
		BREAK
		CASE 57
			IF bForChop = TRUE
				RETURN <<-398.57, -1883.96, 20.54>>
			ELSE
				RETURN <<-397, -1885, 21.5>>
			ENDIF
		BREAK
		CASE 58 RETURN <<780.7208, -211.0616, 65.1143>>		 	BREAK
		CASE 59 RETURN <<-1755.0115, 184.3929, 63.3711>>		BREAK
		CASE 60
			IF bForChop = TRUE
				RETURN <<788.84, 2164.06, 52.10>>
			ELSE
				RETURN <<786.0139, 2164.9507, 52.0981>>
			ENDIF
		BREAK
		CASE 61 RETURN <<1707.2278, 4920.1558, 41.0637>>		BREAK
		CASE 62 RETURN <<-684.9417, 5793.0449, 16.3310>>		BREAK
		CASE 63 RETURN <<-2188, 4251, 48>>		 				BREAK
		CASE 64
			IF bForChop = TRUE
				RETURN <<-1099.08, 2711.35, 18.11>>
			ELSE
				RETURN <<-1103, 2715, 19>>
			ENDIF
		BREAK
		//crowbar
		CASE 65 RETURN <<581, -2805, 5.5>>		 				 BREAK
		CASE 66
			IF bForChop = TRUE
				RETURN <<-663.95, -1998.85, 6.10>>
			ELSE
				RETURN <<-665.4316, -2002.0398, 7.5968>>
			ENDIF
		BREAK
		CASE 67 RETURN <<1442.9049, -1479.7588, 62.2245>>		 BREAK
		CASE 68
			IF bForChop = TRUE
				RETURN <<-361.92, -141.76, 37.58>>
			ELSE
				RETURN <<-356.5324, -145.0359, 41.7413>>
			ENDIF
		BREAK
		CASE 69 RETURN <<624.0116, 558.6531, 129.2455>>		 	 BREAK
		CASE 70 RETURN <<2833, 2807, 56.3785>>		 			 BREAK
		CASE 71
			IF bForChop = TRUE
				RETURN <<92.14449, 6360.18164, 30.37612>>
			ELSE
				RETURN <<94.0769, 6356.9609, 30.3761>>
			ENDIF
		BREAK
		CASE 72 RETURN <<-2231.5142, 3477.7915, 29.3291>>		 BREAK
		CASE 73 RETURN <<377, 3582, 32.5>>		 BREAK
		CASE 74
			IF bForChop = TRUE
				RETURN <<-2947.47, 445.60, 14.29>>
			ELSE
				RETURN <<-2948, 441, 15>>
			ENDIF
		BREAK
		//molotov
		CASE 75
			IF bForChop = TRUE
				RETURN <<1292.88, -3341.78, 4.90>>
			ELSE
				RETURN <<1296, -3348, 5.2>>
			ENDIF
		BREAK
		CASE 76
			IF bForChop = TRUE
				RETURN <<-1165.46, -2054.09, 13.20>>
			ELSE
				RETURN <<-1165.8832, -2051.0264, 13.2268>>
			ENDIF
		BREAK
		CASE 77
			IF bForChop = TRUE
				RETURN <<1235.57, -452.01, 65.70>>
			ELSE
				RETURN <<1235, -450, 66.5>>
			ENDIF
		BREAK
		CASE 78 RETURN <<-467.0458, -934.8019, 46.9844>>		 BREAK
		CASE 79 RETURN <<226.7, -1761, 28.5 >>		 	BREAK
		CASE 80
			IF bForChop = TRUE
				RETURN <<1218.07, 1898.69, 76.87>>
			ELSE
				RETURN <<1221, 1898, 77.4>>
			ENDIF
		BREAK
		CASE 81
			IF bForChop = TRUE
				RETURN <<1545.99, 3802.70, 32.42>>
			ELSE
				RETURN <<1535.8291, 3798.2166, 33.4563>>
			ENDIF
		BREAK
		CASE 82 RETURN <<756.8601, 6462.3291, 30.4229>>		 BREAK
		CASE 83 RETURN <<-2319, 4124, 26>>		 BREAK
		CASE 84 RETURN <<-3185, 1053, 20.2>>		 BREAK
		//hammer
		CASE 85 RETURN <<818.9215, -3188.1667, 4.9007>>		 BREAK
		CASE 86
			IF bForChop = TRUE
				RETURN <<-451.05, -2264.24, 6.61>>
			ELSE
				RETURN <<-457.3656, -2262.6460, 7.5212>>
			ENDIF
		BREAK
		CASE 87
			IF bForChop = TRUE
				RETURN <<1409.86, -757.22, 65.80>>
			ELSE
				RETURN <<1410.0909, -752.1061, 66.2362>>
			ENDIF
		BREAK
		CASE 88	RETURN <<8.4875, -441.2103, 44.5639>>	BREAK
		CASE 89
			IF bForChop = TRUE
				RETURN <<-935.04, 387.39, 76.62>>
			ELSE
				RETURN <<-937.0176, 391.5440, 80.2377>>
			ENDIF
		BREAK
		CASE 90
			IF bForChop = TRUE
				RETURN <<2748.49, 3463.02, 54.77>>
			ELSE
				RETURN <<2746.5537, 3463.5254, 54.7144>>
			ENDIF
		BREAK
		CASE 91 RETURN <<1898.8965, 4921.9746, 47.8088>>	 BREAK
		CASE 92
			IF bForChop = TRUE
				RETURN <<-315.51, 6310.89, 31.37>>
			ELSE
				RETURN <<-315.9772, 6307.3853, 31.4906>>
			ENDIF
		BREAK
		CASE 93 RETURN <<-1171, 4927, 224>>					 BREAK
		CASE 94 RETURN <<-281.1712, 2528.0125, 73.6376>>	 BREAK
		CASE 95
			IF bForChop = TRUE
				RETURN <<68.45, 3756.27, 38.74>>
			ELSE
				RETURN <<68.4530, 3758.9668, 38.7395>>
			ENDIF
		BREAK
		CASE 96 RETURN <<25.2888, -634.2637, 6.5084>> 		 BREAK
		
		// Altruist shotgun - B*1346216
		CASE 97
			IF bForChop = TRUE
				RETURN <<-1146.67, 4939.11, 221.27>>
			ELSE
				RETURN <<-1145.2826, 4940.2856, 221.3038>> 				
			ENDIF
		BREAK

		// Parachute - B*1400028
		CASE 98 RETURN << 441.64, 5570.38, 780.45 >> 		 BREAK
		
		CASE 99
			IF bForChop
				RETURN << 1353.1516, 4377.2227, 43.3416 >>
			ELSE
				RETURN << 1357.2092, 4388.4448, 43.8720 >>
			ENDIF
		BREAK
		
		// Parachute - B*1556857
		CASE 100 RETURN << -102.0671, -968.1538, 295.5635 >> BREAK
		
		// Sawn-off - B*1563245
		CASE 101 RETURN << 2492.6902, 4963.0142, 43.6478 >> BREAK
		
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid index passed to GET_WEAPON_PICKUP_COORDS")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE: 
///    Returns the correct room name depending on the pickup index given in the parameter
FUNC STRING GET_WEAPON_PICKUP_ROOM_NAME(INT iIndex)
	
	SWITCH iIndex
		CASE 13  RETURN "V_25_ObsvDckRm" BREAK
		CASE 42  RETURN "GtaMloRoom002"	 BREAK
		CASE 31  RETURN "V_70_GamesRm"	 BREAK
		CASE 61  RETURN "V_68_BackRm"	 BREAK
		CASE 64  RETURN "V_51_StoreRm"	 BREAK
		CASE 96  RETURN "GtaMloRoomTun2" BREAK
		DEFAULT 
			RETURN ""
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns true if Chop can reach the weapon pickup
FUNC BOOL CAN_CHOP_REACH_WEAPON(INT index)
	
	SWITCH (index)
		CASE 1	FALLTHRU	// In army base booth, player gets 3 star wanted rating
		CASE 10	FALLTHRU	// In airport, player gets 3 star wanted rating
		CASE 11	FALLTHRU	// Underwater
		CASE 13	FALLTHRU	// Top of the army control tower, loads of stairs, player gets 4 star wanted rating
		CASE 14	FALLTHRU	// Middle of prison, loads of stairs, player gets 4 star wanted rating
		CASE 30	FALLTHRU	// On bridge
		CASE 34	FALLTHRU	// Surrounded by fence
		CASE 43	FALLTHRU	// Top of the airport radar tower, loads of ladders, player gets 3 star wanted rating
		CASE 45	FALLTHRU	// On roof
		CASE 50	FALLTHRU	// Airport, player gets 3 star wanted rating
		CASE 55	FALLTHRU	// On a roof
		CASE 88	FALLTHRU	// Raised construction site up ladders
		CASE 96	FALLTHRU	// Bottom of construction site
		CASE 98 FALLTHRU	// Cable Car
		CASE 100 FALLTHRU	// Tallest crane ever
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
