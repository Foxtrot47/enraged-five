							//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	building_control_private.sch								//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Header file that contains all the data for each door.	 	//
//							This deals with both single and multi-player data.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_camera.sch"
USING "player_ped_public.sch"
USING "commands_path.sch"
USING "trains_control_public.sch"

/// PURPOSE: Checks wether the current game mode is SP
FUNC BOOL USE_SP_BUILDING_CONTROLLER_DATA()
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_SP OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY)
	AND NOT (GET_JOINING_GAMEMODE() = GAMEMODE_FM)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC QUEUE_DOOR_UPDATE_FOR_NEXT_FRAME(DOOR_NAME_ENUM eName)
	IF NOT IS_BIT_SET(g_sForceDoorUpdateData.iDoorsQueued[ENUM_TO_INT(eName)/32], ENUM_TO_INT(eName)%23)
		SET_BIT(g_sForceDoorUpdateData.iDoorsQueued[ENUM_TO_INT(eName)/32], ENUM_TO_INT(eName)%23)
		g_sForceDoorUpdateData.eDoorToForceUpdate[g_sForceDoorUpdateData.iForceDoorUpdateCount] = eName
		g_sForceDoorUpdateData.iForceDoorUpdateCount++
	ENDIF
ENDPROC

/// PURPOSE: Returns a struct that contains all the data for the specified door enum
FUNC DOOR_DATA_STRUCT GET_DOOR_DATA(DOOR_NAME_ENUM eName)
	
	DOOR_DATA_STRUCT sData
	
	SWITCH eName
		CASE DOORNAME_BARBER_SHOP_SC
			sData.model 	= V_ILev_BS_DOOR
			sData.coords 	= << 133.0, -1711.00, 29.0 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BARBER_SHOP_SC)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BARBER_SHOP_SC"
			#ENDIF
		BREAK
		CASE DOORNAME_BARBER_SHOP_V
			sData.model 	= V_ILev_BS_DOOR
			sData.coords 	= << -1287.8568, -1115.7416, 7.1401 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BARBER_SHOP_V)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BARBER_SHOP_V"
			#ENDIF
		BREAK
		CASE DOORNAME_BARBER_SHOP_SS
			sData.model 	= V_ILev_BS_DOOR
			sData.coords 	= << 1932.9518, 3725.1536, 32.9944 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BARBER_SHOP_SS)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BARBER_SHOP_SS"
			#ENDIF
		BREAK
		CASE DOORNAME_BARBER_SHOP_MP
			sData.model 	= V_ILev_BS_DOOR
			sData.coords 	= << 1207.8732, -470.063, 66.3580 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BARBER_SHOP_MP)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BARBER_SHOP_MP"
			#ENDIF
		BREAK
		CASE DOORNAME_BARBER_SHOP_HW
			sData.model 	= V_ILev_BS_DOOR
			sData.coords 	= << -29.8692, -148.1571, 57.2265 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BARBER_SHOP_HW)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BARBER_SHOP_HW"
			#ENDIF
		BREAK
		CASE DOORNAME_BARBER_SHOP_PB
			sData.model 	= V_ILev_BS_DOOR
			sData.coords 	= << -280.7851, 6232.7817, 31.8455 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BARBER_SHOP_PB)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BARBER_SHOP_PB"
			#ENDIF
		BREAK
		CASE DOORNAME_HAIR_SALON_L
			sData.model 	= V_ILEV_HD_DOOR_L
			sData.coords 	= << -824.0, -187.00, 38.0 >>
			#IF USE_TU_CHANGES
				sData.coords 	= << -823.2001 ,-187.0831, 37.8190 >>
			#ENDIF
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HAIR_SALON_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HAIR_SALON_L"
			#ENDIF
		BREAK
		CASE DOORNAME_HAIR_SALON_R
			sData.model 	= V_ILEV_HD_DOOR_R
			sData.coords 	= << -823.0, -188.0, 38.0 >>
			#IF USE_TU_CHANGES
				sData.coords 	= << -822.4442, -188.3924, 37.8190 >>
			#ENDIF
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HAIR_SALON_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HAIR_SALON_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_CLOTHES_SHOP_L_01_SC_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << 82.3186, -1392.7518, 29.5261 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_01_SC_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_01_SC_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_01_SC_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	= << 82.3186, -1390.4758, 29.5261 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_01_SC_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_01_SC_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_02_GS_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << 1686.9832, 4821.7407, 42.2131 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_02_GS_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_02_GS_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_02_GS_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	= << 1687.2817, 4819.4844, 42.2131 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_02_GS_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_02_GS_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_03_DT_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << 418.6370, -806.4570, 29.6396 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_03_DT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_03_DT_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_03_DT_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	= << 418.6370, -808.7330, 29.6396 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_03_DT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_03_DT_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_04_CS_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << -1096.6613, 2705.4458, 19.2578 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_04_CS_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_04_CS_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_04_CS_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	= << -1094.9652, 2706.9636, 19.2578 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_04_CS_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_04_CS_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_05_GSD_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << 1196.8250, 2703.2209, 38.3726 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_05_GSD_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_05_GSD_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_05_GSD_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	=  << 1199.1010, 2703.2209, 38.3726 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_05_GSD_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_05_GSD_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_06_VC_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << -818.7642, -1079.5444, 11.4781 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_06_VC_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_06_VC_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_06_VC_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	= << -816.7932, -1078.4065, 11.4781 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_06_VC_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_06_VC_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_07_PB_L
			sData.model 	= V_ILEV_CS_DOOR01
			sData.coords 	= << -0.0564, 6517.4609, 32.0278 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_07_PB_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_07_PB_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_L_07_PB_R
			sData.model 	= V_ILEV_CS_DOOR01_R
			sData.coords 	= << -1.7253, 6515.9136, 32.0278 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_L_07_PB_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_L_07_PB_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_CLOTHES_SHOP_M_01_SM
			sData.model 	= V_ILEV_CLOTHMIDDOOR
			sData.coords 	= << -1201.4349, -776.8566, 17.9918 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_M_01_SM)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_M_01_SM"
			#ENDIF
		BREAK		
		CASE DOORNAME_CLOTHES_SHOP_M_03_H
			sData.model 	= V_ILEV_CLOTHMIDDOOR
			sData.coords 	= << 617.2458, 2751.0222, 42.7578 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_M_03_H)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_M_03_H"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_M_04_HW
			sData.model 	= V_ILEV_CLOTHMIDDOOR
			sData.coords 	= << 127.8201, -211.8274, 55.2275 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_M_04_HW)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_M_04_HW"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_M_05_GOH
			sData.model 	= V_ILEV_CLOTHMIDDOOR
			sData.coords 	= << -3167.7500, 1055.5358, 21.5329 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_M_05_GOH)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_M_05_GOH"
			#ENDIF
		BREAK
		
		CASE DOORNAME_CLOTHES_SHOP_H_01_BH_L
			sData.model 	= V_ILEV_CH_GLASSDOOR
			sData.coords 	= << -716.6754, -155.4200, 37.6749 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_H_01_BH_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_H_01_BH_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_H_01_BH_R
			sData.model 	= V_ILEV_CH_GLASSDOOR
			sData.coords 	= << -715.6154, -157.2561, 37.6749 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_H_01_BH_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_H_01_BH_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_H_02_B_L
			sData.model 	= V_ILEV_CH_GLASSDOOR
			sData.coords 	= << -157.0924, -306.4413, 39.994 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_H_02_B_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_H_02_B_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_H_02_B_R
			sData.model 	= V_ILEV_CH_GLASSDOOR
			sData.coords 	= << -156.4022, -304.4366, 39.994 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_H_02_B_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_H_02_B_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_H_03_MW_L
			sData.model 	= V_ILEV_CH_GLASSDOOR
			sData.coords 	= << -1454.7819, -231.7927, 50.0565 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_H_03_MW_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_H_03_MW_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CLOTHES_SHOP_H_03_MW_R
			sData.model 	= V_ILEV_CH_GLASSDOOR
			sData.coords 	= << -1456.2007, -233.3682, 50.0565 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLOTHES_SHOP_H_03_MW_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLOTHES_SHOP_H_03_MW_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_TATTOO_VW
			sData.model 	= V_ILEV_TA_DOOR
			sData.coords 	= << 321.81, 178.36, 103.68 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TATTOO_VW)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TATTOO_VW"
			#ENDIF
		BREAK
		CASE DOORNAME_TATTOO_SS
			sData.model 	= V_ILEV_ML_DOOR1
			sData.coords 	= << 1859.89, 3749.79, 33.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TATTOO_SS)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TATTOO_SS"
			#ENDIF
		BREAK
		CASE DOORNAME_TATTOO_PB
			sData.model 	= V_ILEV_ML_DOOR1
			sData.coords 	= << -289.1752, 6199.1123, 31.6370 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TATTOO_PB)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TATTOO_PB"
			#ENDIF
		BREAK
		CASE DOORNAME_TATTOO_VC
			sData.model 	= V_ILEV_TA_DOOR
			sData.coords 	= << -1155.4541, -1424.0079, 5.0461 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TATTOO_VC)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TATTOO_VC"
			#ENDIF
		BREAK
		CASE DOORNAME_TATTOO_ELS
			sData.model 	= V_ILEV_TA_DOOR
			sData.coords 	= << 1321.2856, -1650.5967, 52.3663 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TATTOO_ELS)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TATTOO_ELS"
			#ENDIF
		BREAK
		CASE DOORNAME_TATTOO_GOH
			sData.model 	= V_ILEV_TA_DOOR
			sData.coords 	= << -3167.7888, 1074.7669, 20.9209 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TATTOO_GOH)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TATTOO_GOH"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_F_L
			sData.model 	= V_ILEV_MM_DOORM_L
			sData.coords 	= << -817.0, 179.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_F_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_F_L"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_F_R
			sData.model 	= V_ILEV_MM_DOORM_R
			sData.coords 	= << -816.0, 178.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_F_R"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_G1
			sData.model 	= PROP_LD_GARAGED_01
			sData.coords 	= << -815.0, 186.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_G1)
			sData.automaticRate = 6.5
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_G1"
			#ENDIF
		BREAK
		
		CASE DOORNAME_M_MANSION_R_L1
			sData.model 	= PROP_BH1_48_BACKDOOR_L
			sData.coords 	= << -797.0, 177.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_R_L1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_R_L1"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_R_R1
			sData.model 	= PROP_BH1_48_BACKDOOR_R
			sData.coords 	= << -795.0, 178.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_R_R1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_R_R1"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_R_L2
			sData.model 	= PROP_BH1_48_BACKDOOR_L
			sData.coords 	= << -793.0, 181.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_R_L2)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_R_L2"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_R_R2
			sData.model 	= PROP_BH1_48_BACKDOOR_R
			sData.coords 	= << -794.0, 183.0, 73.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_R_R2)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_R_R2"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_GA_SM
			sData.model 	= PROP_BH1_48_GATE_1
			sData.coords 	= << -849.0, 179.0, 70.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_GA_SM)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_GA_SM"
			#ENDIF
		BREAK
		CASE DOORNAME_M_MANSION_BW
			sData.model 	= V_ILEV_MM_WINDOWWC
			sData.coords 	= << -802.7333, 167.5041, 77.5824 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_M_MANSION_BW)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "M_MANSION_BW"
			#ENDIF
		BREAK
		
		CASE DOORNAME_F_HOUSE_SC_F
			sData.model 	= V_ILEV_FA_FRONTDOOR
			sData.coords 	= << -14.0, -1441.0, 31.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_F_HOUSE_SC_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "F_HOUSE_SC_F"
			#ENDIF
		BREAK
		CASE DOORNAME_F_HOUSE_SC_B
			sData.model 	= V_ILEV_FH_FRNTDOOR
			sData.coords 	= << -15.0, -1427.0, 31.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_F_HOUSE_SC_B)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "F_HOUSE_SC_B"
			#ENDIF
		BREAK
		CASE DOORNAME_F_HOUSE_SC_G
			sData.model 	= PROP_SC1_21_G_DOOR_01
			sData.coords 	= << -25.28, -1431.06, 30.84 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "F_HOUSE_SC_G"
			#ENDIF
		BREAK
		CASE DOORNAME_F_HOUSE_VH_F
			sData.model 	= V_ILEV_FH_FRONTDOOR
			sData.coords 	= << 7.52, 539.53, 176.18 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_F_HOUSE_VH_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "F_HOUSE_VH_F"
			#ENDIF
		BREAK
//		CASE DOORNAME_F_HOUSE_VH_G
//			sData.model 	= PROP_CH_025C_G_DOOR_01
//			sData.coords 	= << 18.65, 546.34, 176.34 >>
//			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
//			sData.doorHash  = ENUM_TO_INT(DOORHASH_F_HOUSE_VH_G)
//			sData.automaticRate = 4.0
//			#IF IS_DEBUG_BUILD
//				sData.dbg_name = "F_HOUSE_VH_G"
//			#ENDIF
//		BREAK
		CASE DOORNAME_T_TRAILER_CS
			sData.model 	= V_ILEV_TREVTRAILDR
			sData.coords 	= << 1973.0, 3815.0, 34.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_T_TRAILER_CS)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "T_TRAILER_CS"
			#ENDIF
		BREAK
		CASE DOORNAME_T_TRAILER_CS_G
			sData.model 	= PROP_CS4_10_TR_GD_01
			sData.coords 	= << 1972.7874, 3824.5537, 32.5831 >>
			//SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_T_TRAILER_CS_G)
			sData.automaticRate = 12.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "T_TRAILER_CS_G"
			#ENDIF
		BREAK
		CASE DOORNAME_T_APARTMENT_VB
			sData.model 	= V_ILEV_TREV_DOORFRONT
			sData.coords 	= << -1150.0, -1521.0, 11.0 >>
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			sData.doorHash  = ENUM_TO_INT(DOORHASH_T_APARTMENT_VB)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "T_APARTMENT_VB"
			#ENDIF
		BREAK
	ENDSWITCH
	
	// too many cases in switch statement
	SWITCH eName
		CASE DOORNAME_CARMOD_GARAGE_01
			sData.model 	= PROP_COM_LS_DOOR_01
			sData.coords 	= << -1145.90, -1991.14, 14.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_01)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_01"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_05
			sData.model 	= PROP_ID2_11_GDOOR
			sData.coords 	= <<723.12,-1088.83,23.28>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_05)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_05"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_06
			sData.model 	= PROP_COM_LS_DOOR_01
			sData.coords 	= << -356.09, -134.77, 40.01 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_06)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_06"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_07_L
			sData.model 	= V_ILEV_CARMOD3DOOR
			sData.coords 	= << 108.8502, 6617.8765, 32.6730 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_07_L)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_07_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_07_R
			sData.model 	= V_ILEV_CARMOD3DOOR
			sData.coords 	= << 114.3206, 6623.2261, 32.7161 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_07_R)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_07_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_08_L
			sData.model 	= V_ILEV_CARMOD3DOOR
			sData.coords 	= << 1182.3054, 2645.2422, 38.807 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_08_L)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_08_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_08_R
			sData.model 	= V_ILEV_CARMOD3DOOR
			sData.coords 	= << 1174.6542, 2645.2422, 38.6826 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_08_R)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_08_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CARMOD_GARAGE_09
			sData.model 	= LR_PROP_SUPERMOD_DOOR_01
			sData.coords 	= <<-205.7007, -1310.6917, 30.2957>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARMOD_GARAGE_09)
			sData.automaticRate = 25.0
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARMOD_GARAGE_09"
			#ENDIF
		BREAK
		CASE DOORNAME_JANITORS_APARTMENT
			sData.model 	= V_ILEV_JANITOR_FRONTDOOR
			sData.coords 	= << -107.5401, -9.0258, 70.6696 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "JANITORS_APARTMENT"
			#ENDIF
		BREAK
		CASE DOORNAME_SWEATSHOP_L
			sData.model 	= V_ILEV_SS_DOOR8
			sData.coords 	= << 717, -975, 25 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SWEATSHOP_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SWEATSHOP_L"
			#ENDIF
		BREAK
		CASE DOORNAME_SWEATSHOP_R
			sData.model 	= V_ILEV_SS_DOOR7
			sData.coords 	= << 719, -975, 25 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SWEATSHOP_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SWEATSHOP_R"
			#ENDIF
		BREAK
		CASE DOORNAME_SWEATSHOP_OFFICE
			sData.model 	= V_ILEV_SS_DOOR02
			sData.coords 	= << 709.9813, -963.5311, 30.5453 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SWEATSHOP_OFFICE"
			#ENDIF
		BREAK
		CASE DOORNAME_SWEATSHOP_STORE
			sData.model 	= V_ILEV_SS_DOOR03
			sData.coords 	= << 709.9894, -960.6675, 30.5453 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SWEATSHOP_STORE"
			#ENDIF
		BREAK
		CASE DOORNAME_SWEATSHOP_OFFICE_STORE
			sData.model 	= V_ILEV_STORE_DOOR
			sData.coords 	= << 707.8046, -962.4564, 30.5453 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SWEATSHOP_OFFICE_STORE"
			#ENDIF
		BREAK
		CASE DOORNAME_METHLAB_F_L
			sData.model 	= V_ILEV_ML_DOOR1
			sData.coords 	= << 1393, 3599, 35 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_METHLAB_F_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "METHLAB_F_L"
			#ENDIF
		BREAK
		CASE DOORNAME_METHLAB_F_R
			sData.model 	= V_ILEV_ML_DOOR1
			sData.coords 	= << 1395, 3600, 35 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_METHLAB_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "METHLAB_F_R"
			#ENDIF
		BREAK
		CASE DOORNAME_METHLAB_R
			sData.model 	= V_ILEV_SS_DOOR04
			sData.coords 	= << 1387, 3614, 39 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_METHLAB_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "METHLAB_R"
			#ENDIF
		BREAK
		CASE DOORNAME_FOUNDRY_B_01
			sData.model 	= PROP_RON_DOOR_01
			sData.coords 	= << 1083.5471, -1975.4354, 31.6222 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FOUNDRY_B_01)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FOUNDRY_B_01"
			#ENDIF
		BREAK
		CASE DOORNAME_FOUNDRY_B_02
			sData.model 	= PROP_RON_DOOR_01
			sData.coords 	= << 1065.2371, -2006.0791, 32.2329 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FOUNDRY_B_02)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FOUNDRY_B_02"
			#ENDIF
		BREAK
		CASE DOORNAME_FOUNDRY_T_01
			sData.model 	= PROP_RON_DOOR_01
			sData.coords 	= << 1085.3069, -2018.5613, 41.6289 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FOUNDRY_T_01)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FOUNDRY_T_01"
			#ENDIF
		BREAK
		CASE DOORNAME_RURAL_BANK_F_L
			sData.model 	= V_ILEV_BANK4DOOR02
			sData.coords 	= << -111, 6464, 32 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RURAL_BANK_F_L"
			#ENDIF
		BREAK
		CASE DOORNAME_RURAL_BANK_F_R
			sData.model 	= V_ILEV_BANK4DOOR01
			sData.coords 	= << -110, 6462, 32 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RURAL_BANK_F_R"
			#ENDIF
		BREAK
		CASE DOORNAME_LESTER_F
			sData.model 	= V_ILEV_LESTER_DOORFRONT
			sData.coords 	= <<1274, -1721, 55>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LESTER_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LESTER_F"
			#ENDIF
		BREAK
		CASE DOORNAME_LESTER_R_L
			sData.model 	= V_ILEV_LESTER_DOORVERANDA
			sData.coords 	= << 1271.89, -1707.57, 53.79 >> 
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LESTER_R_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LESTER_R_L"
			#ENDIF
		BREAK
		CASE DOORNAME_LESTER_R_R
			sData.model 	= V_ILEV_LESTER_DOORVERANDA
			sData.coords 	= << 1270.77, -1708.10, 53.75 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LESTER_R_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LESTER_R_R"
			#ENDIF
		BREAK
		CASE DOORNAME_CARSTEAL_MOTEL
			sData.model 	= v_ilev_deviantfrontdoor
			sData.coords 	= << -127.50, -1456.18, 37.94 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARSTEAL_MOTEL)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARSTEAL_MOTEL"
			#ENDIF
		BREAK
		CASE DOORNAME_CARSTEAL_GARAGE_F
			sData.model 	= prop_com_gar_door_01
			sData.coords 	= << 483.56, -1316.08, 32.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARSTEAL_GARAGE_F"
			#ENDIF
		BREAK
		CASE DOORNAME_CARSTEAL_GARAGE_S
			sData.model 	= V_ILEV_CS_DOOR
			sData.coords 	= << 483, -1312, 29 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_S)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CARSTEAL_GARAGE_S"
			#ENDIF
		BREAK
		CASE DOORNAME_STRIPCLUB_F
			sData.model 	= PROP_STRIP_DOOR_01
			sData.coords 	= << 128.0, -1299.0, 29.0 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_STRIPCLUB_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "STRIPCLUB_F"
			#ENDIF
		BREAK
		CASE DOORNAME_STRIPCLUB_R
			sData.model 	= PROP_MAGENTA_DOOR
			sData.coords 	= << 96.0, -1285.0, 29.0 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_STRIPCLUB_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "STRIPCLUB_R"
			#ENDIF
		BREAK
		CASE DOORNAME_JOSH_HOTEL
			sData.model 	= PROP_MOTEL_DOOR_09
			sData.coords 	= << 549.0, -1773.0, 34.0 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_JOSH_HOTEL)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "JOSH_HOTEL"
			#ENDIF
		BREAK
		CASE DOORNAME_MEX_GANG_SAFE
			sData.model 	= V_ILEV_GANGSAFEDOOR
			sData.coords 	= << 974, -1839, 36 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_MEX_GANG_SAFE)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IGNORE_SAFETY_CHECKS))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MEX_GANG_SAFE"
			#ENDIF
		BREAK
		CASE DOORNAME_LOST_GANG_SAFE
			sData.model 	= V_ILEV_GANGSAFEDOOR
			sData.coords 	= <<977, -105, 75>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LOST_GANG_SAFE)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IGNORE_SAFETY_CHECKS))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LOST_GANG_SAFE"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_F_L1
			sData.model 	= V_ILEV_RA_DOOR1_L
			sData.coords 	= << 1391, 1163, 114 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_F_L1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_F_L1"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_F_R1
			sData.model 	= V_ILEV_RA_DOOR1_R
			sData.coords 	= << 1391, 1161, 114 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_F_R1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_F_R1"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_F_L2
			sData.model 	= PROP_CS6_03_DOOR_L
			sData.coords 	= << 1396, 1143, 115 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_F_L2)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_F_L2"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_F_R2
			sData.model 	= PROP_CS6_03_DOOR_R
			sData.coords 	= << 1396, 1141, 115 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_F_R2)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_F_R2"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_R_L1
			sData.model 	= V_ILEV_RA_DOOR1_L
			sData.coords 	= << 1409, 1146, 114 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_R_L1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_R_L1"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_R_R1
			sData.model 	= V_ILEV_RA_DOOR1_R
			sData.coords 	= << 1409, 1148, 114 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_R_R1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_R_R1"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_R_L2
			sData.model 	= V_ILEV_RA_DOOR1_L
			sData.coords 	= << 1408, 1159, 114 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_R_L2)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_R_L2"
			#ENDIF
		BREAK
		CASE DOORNAME_RANCH_R_R2
			sData.model 	= V_ILEV_RA_DOOR1_R
			sData.coords 	= << 1408, 1161, 114 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RANCH_R_R2)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RANCH_R_R2"
			#ENDIF
		BREAK
		CASE DOORNAME_ARM2_GARAGE_01
			sData.model 	= PROP_GAR_DOOR_01
			sData.coords 	= << -1067, -1666, 5 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ARM2_GARAGE_01)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ARM2_GARAGE_01"
			#ENDIF
		BREAK
		CASE DOORNAME_ARM2_GARAGE_02
			sData.model 	= PROP_GAR_DOOR_02
			sData.coords 	= << -1065, -1669, 5 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ARM2_GARAGE_02)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ARM2_GARAGE_02"
			#ENDIF
		BREAK
		CASE DOORNAME_ARM2_HENCH_01
			sData.model 	= PROP_MAP_DOOR_01
			sData.coords 	= << -1104.66, -1638.48, 4.68 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ARM2_HENCH_01)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ARM2_HENCH_01"
			#ENDIF
		BREAK
		CASE DOORNAME_ARM2_SIMEON_OFFICE
			sData.model 	= V_ILEV_FIB_DOOR1
			sData.coords 	= << -31.72,-1101.85,26.57 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ARM2_SIMEON_01"
			#ENDIF
		BREAK
		CASE DOORNAME_TORTURE
			sData.model 	= v_ilev_tort_door
			sData.coords 	= << 134.40, -2204.10, 7.52 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TORTURE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TORTURE"
			#ENDIF
		BREAK
		CASE DOORNAME_CHEMICAL_FACTORY_L
			sData.model 	= V_ILEV_BL_SHUTTER2
			sData.coords 	= << 3628, 3747, 28 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CHEMICAL_FACTORY_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CHEMICAL_FACTORY_L"
			#ENDIF
		BREAK
		CASE DOORNAME_CHEMICAL_FACTORY_R
			sData.model 	= V_ILEV_BL_SHUTTER2
			sData.coords 	= << 3621, 3752, 28 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CHEMICAL_FACTORY_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CHEMICAL_FACTORY_R"
			#ENDIF
		BREAK
		CASE DOORNAME_RECYCLING_PLANT_F_L
			sData.model 	= v_ilev_rc_door3_l
			sData.coords 	= << -608.73, -1610.32, 27.16 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RECYCLING_PLANT_F_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RECYCLING_PLANT_F_L"
			#ENDIF
		BREAK
		CASE DOORNAME_RECYCLING_PLANT_F_R
			sData.model 	= v_ilev_rc_door3_r
			sData.coords 	= << -611.32, -1610.09, 27.16 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RECYCLING_PLANT_F_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RECYCLING_PLANT_F_R"
			#ENDIF
		BREAK
		CASE DOORNAME_RECYCLING_PLANT_R_L
			sData.model 	= v_ilev_rc_door3_l
			sData.coords 	= << -592.94, -1631.58, 27.16 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RECYCLING_PLANT_R_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RECYCLING_PLANT_R_L"
			#ENDIF
		BREAK
		CASE DOORNAME_RECYCLING_PLANT_R_R
			sData.model 	= v_ilev_rc_door3_r
			sData.coords 	= << -592.71, -1628.99, 27.16 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RECYCLING_PLANT_R_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RECYCLING_PLANT_R_R"
			#ENDIF
		BREAK
		CASE DOORNAME_HICK_BAR_F
			sData.model 	= V_ILEV_SS_DOOR04
			sData.coords 	= << 1991, 3053, 47 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HICK_BAR_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HICK_BAR_F"
			#ENDIF
		BREAK
		CASE DOORNAME_HICK_BAR_F_INT
			sData.model 	= V_ILEV_FH_DOOR4
			sData.coords 	= <<1988.3529, 3054.4109, 47.3204>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HICK_BAR_F_INT"
			#ENDIF
		BREAK
		CASE DOORNAME_EPSILON_MANSION_F_L
			sData.model 	= PROP_EPSILON_DOOR_L
			sData.coords 	= << -700.17, 47.31, 44.30 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON_MANSION_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON_MANSION_F_L"
			#ENDIF
		BREAK
		CASE DOORNAME_EPSILON_MANSION_F_R
			sData.model 	= PROP_EPSILON_DOOR_R
			sData.coords 	= << -697.94, 48.35, 44.30 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON_MANSION_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON_MANSION_F_R"
			#ENDIF
		BREAK
		CASE DOORNAME_EPSILON2_STORAGE_ROOM
			sData.model 	= V_ILev_EpsStoreDoor
			sData.coords 	= << 241.3574, 361.0488, 105.8963 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON2_STORAGE_ROOM)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON2_STORAGE_ROOM"
			#ENDIF
		BREAK
		CASE DOORNAME_EPSILON3_GARAGE
			sData.model 	= PROP_CH2_09C_GARAGE_DOOR
			sData.coords 	= << -689.11, 506.97, 110.64 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON3_GARAGE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON3_GARAGE"
			#ENDIF
		BREAK
		CASE DOORNAME_LINVADER_OFFICE_UP
			sData.model 	= V_ILEV_DOOR_ORANGESOLID
			sData.coords 	= << -1055.96, -236.43, 44.17 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LINVADER_OFFICE_UP)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LINVADER_OFFICE_UP"
			#ENDIF
		BREAK
		
		// DUMMY DOORS THAT GET USED FOR CUTSCENES ONLY
		CASE DOORNAME_HENCHMAN_TRAILER_L
			sData.model 	= PROP_MAGENTA_DOOR
			sData.coords 	= << 29, 3661, 41 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HENCHMAN_TRAILER_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HENCHMAN_TRAILER_L"
			#ENDIF
		BREAK
		CASE DOORNAME_HENCHMAN_TRAILER_R
			sData.model 	= PROP_CS4_05_TDOOR
			sData.coords 	= << 32, 3667, 41 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HENCHMAN_TRAILER_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HENCHMAN_TRAILER_R"
			#ENDIF
		BREAK
		CASE DOORNAME_FRANK1_DEALER_DOOR
			sData.model 	= V_ILEV_HOUSEDOOR1
			sData.coords 	= << 87, -1959, 21 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FRANK1_DEALER_DOOR)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FRANK1_DEALER_DOOR"
			#ENDIF
		BREAK
		CASE DOORNAME_FRANK1_SHOP_DOOR
			sData.model 	= V_ILEV_FH_FRNTDOOR
			sData.coords 	= << 0, -1823, 30 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FRANK1_SHOP_DOOR)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FRANK1_SHOP_DOOR"
			#ENDIF
		BREAK
		CASE DOORNAME_FRANK1_HOUSE_DOOR 
			sData.model     = P_CUT_DOOR_03 
			sData.coords    = << 23.34, -1897.60, 23.05 >> 
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FRANK1_HOUSE_DOOR) 
			#IF IS_DEBUG_BUILD 
				sData.dbg_name = "FRANK1_HOUSE_DOOR" 
			#ENDIF 
		BREAK

		CASE DOORNAME_EPSILON_7_TRAILER
			sData.model 	= P_CUT_DOOR_02
			sData.coords 	= << 524.20, 3081.14, 41.16 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON_7_TRAILER)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON_7_TRAILER"
			#ENDIF
		BREAK
		CASE DOORNAME_SHRINK_OFFICE
			sData.model 	= V_ILEV_PO_DOOR
			sData.coords 	= << -1910.58, -576.01, 19.25 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SHRINK_OFFICE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SHRINK_OFFICE"
			#ENDIF
		BREAK
		CASE DOORNAME_LES_BIANCO_RESTAURANT_L
			sData.model 	= PROP_SS1_10_DOOR_L
			sData.coords 	= << -720.39, 256.86, 80.29 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LES_BIANCO_RESTAURANT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LES_BIANCO_RESTAURANT_L"
			#ENDIF
		BREAK
		CASE DOORNAME_LES_BIANCO_RESTAURANT_R
			sData.model 	= PROP_SS1_10_DOOR_R
			sData.coords 	= << -718.42, 257.79, 80.29 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LES_BIANCO_RESTAURANT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LES_BIANCO_RESTAURANT_R"
			#ENDIF
		BREAK
		CASE DOORNAME_FIB_F_L
			sData.model 	= V_ILEV_FIBL_DOOR02
			sData.coords 	= << 106.38, -742.70, 46.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FIB_F_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FIB_F_L"
			#ENDIF
		BREAK
		CASE DOORNAME_FIB_F_R
			sData.model 	= V_ILEV_FIBL_DOOR01
			sData.coords 	= << 105.76, -746.65, 46.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FIB_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FIB_F_R"
			#ENDIF
		BREAK
		CASE DOORNAME_AIRPORT_TOWER_1
			sData.model 	= V_ILEV_CT_DOOR01
			sData.coords 	= << -2343.53, 3265.37, 32.96 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_AIRPORT_TOWER_1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "AIRPORT_TOWER_1"
			#ENDIF
		BREAK
		CASE DOORNAME_AIRPORT_TOWER_2
			sData.model 	= V_ILEV_CT_DOOR01
			sData.coords 	= << -2342.23, 3267.62, 32.96 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_AIRPORT_TOWER_1)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "AIRPORT_TOWER_2"
			#ENDIF
		BREAK
		CASE DOORNAME_AIRPORT_L
			sData.model 	= AP1_02_DOOR_L
			sData.coords 	= << -1041.9326, -2748.1675, 22.0308 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_AIRPORT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "AIRPORT_L"
			#ENDIF
		BREAK
		CASE DOORNAME_AIRPORT_R
			sData.model 	= AP1_02_DOOR_R
			sData.coords 	= << -1044.8408, -2746.4888, 22.0308 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_AIRPORT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "AIRPORT_R"
			#ENDIF
		BREAK
		CASE DOORNAME_LIFE_INVADER_FRONT_L
			sData.model 	= V_ILEV_FB_DOORSHORTL
			sData.coords 	= << -1045.1199, -232.0040, 39.4379 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LIFE_INVADER_FRONT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LIFE_INVADER_FRONT_L"
			#ENDIF
		BREAK
		CASE DOORNAME_LIFE_INVADER_FRONT_R
			sData.model 	= V_ILEV_FB_DOORSHORTR
			sData.coords 	= << -1046.5161, -229.3581, 39.4379 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LIFE_INVADER_FRONT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LIFE_INVADER_FRONT_R"
			#ENDIF
		BREAK
		CASE DOORNAME_LIFE_INVADER_REAR_L
			sData.model 	= V_ILEV_FB_DOOR01
			sData.coords 	= << -1083.6201, -260.4167, 38.1867 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LIFE_INVADER_REAR_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LIFE_INVADER_REAR_L"
			#ENDIF
		BREAK
		CASE DOORNAME_LIFE_INVADER_REAR_R
			sData.model 	= V_ILEV_FB_DOOR02
			sData.coords 	= << -1080.9744, -259.0204, 38.1867 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LIFE_INVADER_REAR_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LIFE_INVADER_REAR_R"
			#ENDIF
		BREAK
		CASE DOORNAME_LIFE_INVADER_SIDE_01
			sData.model 	= V_ILEV_GTDOOR
			sData.coords 	= << -1042.57, -240.60, 38.11 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_LIFE_INVADER_SIDE_01)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "LIFE_INVADER_SIDE_01"
			#ENDIF
		BREAK
		CASE DOORNAME_FBI2_MEET_BUILDING
			sData.model 	= PROP_DAMDOOR_01
			sData.coords 	= << 1385.2578, -2079.9495, 52.7638 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_FBI2_MEET_BUILDING)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "FBI2_MEET_BUILDING"
			#ENDIF
		BREAK
		CASE DOORNAME_BANK_GRAPESEED_L
			sData.model 	= v_ilev_genbankdoor2
			sData.coords 	= << 1656.57, 4849.66, 42.35 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_GRAPESEED_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_GRAPESEED_L"
			#ENDIF
		BREAK
		CASE DOORNAME_BANK_GRAPESEED_R
			sData.model 	= v_ilev_genbankdoor1
			sData.coords 	= << 1656.25, 4852.24, 42.35 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_GRAPESEED_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_GRAPESEED_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_STUDIO_NORTH_GATE_IN
			sData.model 	= PROP_SEC_BARRIER_LD_01A
			sData.coords 	= << -1051.4019, -474.6847, 36.6199 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_STUDIO_GATE))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "STUDIO_NORTH_GATE_IN"
			#ENDIF
		BREAK
		CASE DOORNAME_STUDIO_NORTH_GATE_OUT
			sData.model 	= PROP_SEC_BARRIER_LD_01A
			sData.coords 	= << -1049.2853, -476.6376, 36.7584 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_STUDIO_GATE))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "STUDIO_NORTH_GATE_OUT"
			#ENDIF
		BREAK
		CASE DOORNAME_STUDIO_SOUTH_GATE_IN
			sData.model 	= PROP_SEC_BARRIER_LD_02A
			sData.coords 	= << -1210.9567, -580.8765, 27.2373 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_STUDIO_SOUTH_GATE_IN)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_STUDIO_GATE))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "STUDIO_SOUTH_GATE_IN"
			#ENDIF
		BREAK
		CASE DOORNAME_STUDIO_SOUTH_GATE_OUT
			sData.model 	= PROP_SEC_BARRIER_LD_02A
			sData.coords 	= << -1212.4453, -578.4401, 27.2373 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_STUDIO_SOUTH_GATE_OUT)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_STUDIO_GATE))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "STUDIO_SOUTH_GATE_OUT"
			#ENDIF
		BREAK
		
		CASE DOORNAME_TEQUILA_CLUB_DOOR_F
			sData.model 	= V_ILEV_ROC_DOOR4
			sData.coords 	= << -565.1712, 276.6259, 83.2863 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TEQUILA_CLUB_DOOR_F)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TEQUILA_CLUB_DOOR_F"
			#ENDIF
		BREAK
		CASE DOORNAME_TEQUILA_CLUB_DOOR_R
			sData.model 	= V_ILEV_ROC_DOOR4
			sData.coords 	= << -561.2863, 293.5043, 87.7771 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_TEQUILA_CLUB_DOOR_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "TEQUILA_CLUB_DOOR_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_HEIST_JEWELERS_L
			sData.model 	= p_jewel_door_l
			sData.coords 	= << -631.96, -236.33, 38.21 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HEIST_JEWELERS_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HEIST_JEWELERS_L"
			#ENDIF
		BREAK
		CASE DOORNAME_HEIST_JEWELERS_R
			sData.model 	= p_jewel_door_r1
			sData.coords 	= << -630.43, -238.44, 38.21 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_HEIST_JEWELERS_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "HEIST_JEWELERS_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_VINEWOOD_CORNER_L
			sData.model 	= prop_ld_bankdoors_01
			sData.coords 	= << 231.62, 216.23, 106.40 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_VINEWOOD_CORNER_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_VINEWOOD_CORNER_L"
			#ENDIF
		BREAK
		CASE DOORNAME_BANK_VINEWOOD_CORNER_R
			sData.model 	= prop_ld_bankdoors_01
			sData.coords 	= << 232.72, 213.88, 106.40 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_VINEWOOD_CORNER_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_VINEWOOD_CORNER_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_VINEWOOD_FRONT_L
			sData.model 	= hei_prop_hei_bankdoor_new 
			sData.coords 	= << 258.32, 203.84, 106.43 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_VINEWOOD_FRONT_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_VINEWOOD_FRONT_L"
			#ENDIF
		BREAK
		CASE DOORNAME_BANK_VINEWOOD_FRONT_R
			sData.model 	= hei_prop_hei_bankdoor_new
			sData.coords 	= << 260.76, 202.95, 106.43 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_VINEWOOD_FRONT_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_VINEWOOD_FRONT_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_VINEWOOD_TELLER
		
				sData.model 	= HEI_V_ILEV_BK_GATE_PRIS
			sData.coords 	= << 256.31, 220.66, 106.43 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_VINEWOOD_TELLER)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_VINEWOOD_TELLER"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_VINEWOOD_OFFICE
			sData.model 	= v_ilev_bk_door
			sData.coords 	= << 266.36, 217.57, 110.43 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_VINEWOOD_OFFICE)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_VINEWOOD_OFFICE"
			#ENDIF
		BREAK
		
		CASE DOORNAME_SHERIFF_FRONT_L
			sData.model 	= V_ILEV_SHRF2DOOR
			sData.coords 	= <<-442.6600,6015.2217,31.8663>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SHERIFF_FRONT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SHERIFF_FRONT_L"
			#ENDIF
		BREAK
		CASE DOORNAME_SHERIFF_FRONT_R
			sData.model 	= V_ILEV_SHRF2DOOR
			sData.coords 	= <<-444.4985,6017.0601,31.8663>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SHERIFF_FRONT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SHERIFF_FRONT_R"
			#ENDIF
		BREAK
		CASE DOORNAME_SHERIFF_CS4
			sData.model 	= V_ILEV_SHRFDOOR
			sData.coords 	=<<1855.6848,3683.9302,34.5928>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_SHERIFF_CS4)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SHERIFF_CS4"
			#ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_1_L
			sData.model 	= prop_bhhotel_door_l
			sData.coords 	=<<-1223.35, -172.41, 39.98>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_VONCRAST_1_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VONCRAST_1_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_1_R
			sData.model 	= prop_bhhotel_door_r
			sData.coords 	=<<-1220.93, -173.68, 39.98>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_VONCRAST_1_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VONCRAST_1_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_2_L
			sData.model 	= prop_bhhotel_door_l
			sData.coords 	=<<-1211.99, -190.57, 39.98>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_VONCRAST_2_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VONCRAST_2_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_2_R
			sData.model 	= prop_bhhotel_door_r
			sData.coords 	=<<-1213.26, -192.98, 39.98>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_VONCRAST_2_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VONCRAST_2_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_3_L
			sData.model 	= prop_bhhotel_door_l
			sData.coords 	=<<-1217.77, -201.54, 39.98>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_VONCRAST_3_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VONCRAST_3_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_3_R
			sData.model 	= prop_bhhotel_door_r
			sData.coords 	=<<-1219.04, -203.95, 39.98>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_VONCRAST_3_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "VONCRAST_3_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_NOSE_REAR_L
			sData.model 	= prop_ch3_04_door_01l
			sData.coords 	=<<2514.32, -317.34, 93.32>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_NOSE_REAR_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "NOSE_REAR_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_NOSE_REAR_R
			sData.model 	= prop_ch3_04_door_01r
			sData.coords 	=<<2512.42, -319.26, 93.32>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_NOSE_REAR_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "NOSE_REAR_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_OMEGA_SHED_L
			sData.model 	= prop_ch3_01_trlrdoor_l
			sData.coords 	=<<2333.23, 2574.97, 47.03>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_OMEGA_SHED_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "OMEGA_SHED_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_OMEGA_SHED_R
			sData.model 	= prop_ch3_01_trlrdoor_r
			sData.coords 	=<<2329.65, 2576.64, 47.03>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_OMEGA_SHED_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "OMEGA_SHED_R"
			#ENDIF
		BREAK
		 
		CASE DOORNAME_GUN_SHOP_01_DT_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << 16.1279, -1114.6055, 29.9469 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_01_DT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_01_DT_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_01_DT_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << 18.5720, -1115.4951, 29.9469 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_01_DT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_01_DT_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_02_SS_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << 1698.1763, 3751.5056, 34.8553 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_02_SS_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_02_SS_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_02_SS_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << 1699.9371, 3753.4202, 34.8553 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_02_SS_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_02_SS_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_03_HW_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << 244.7274, -44.0791, 70.910 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_03_HW_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_03_HW_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_03_HW_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << 243.8379, -46.5232, 70.910 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_03_HW_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_03_HW_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_04_ELS_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << 845.3624, -1024.5391, 28.3448 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_04_ELS_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_04_ELS_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_04_ELS_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << 842.7684, -1024.5391, 23.3448 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_04_ELS_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_04_ELS_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_05_PB_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << -326.1122, 6075.2695, 31.6047 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_05_PB_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_05_PB_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_05_PB_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << -324.2730, 6077.1089, 31.6047 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_05_PB_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_05_PB_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_06_LS_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << -665.2424, -944.3256, 21.9792 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_06_LS_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_06_LS_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_06_LS_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << -662.6414, -944.3256, 21.9792 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_06_LS_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_06_LS_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_07_MW_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << -1313.8259, -389.1259, 36.8457 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_07_MW_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_07_MW_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_07_MW_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << -1314.4650, -391.6472, 36.8457 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_07_MW_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_07_MW_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_08_CS_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << -1114.0089, 2689.7700, 18.7041 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_08_CS_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_08_CS_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_08_CS_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << -1112.0708, 2691.5046, 18.7041 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_08_CS_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_08_CS_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_09_GOH_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << -3164.8452, 1081.3917, 20.9887 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_09_GOH_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_09_GOH_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_09_GOH_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << -3163.8115, 1083.7784, 20.9887 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_09_GOH_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_09_GOH_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_10_VWH_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << 2570.9045, 303.3556, 108.8848 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_10_VWH_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_10_VWH_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_10_VWH_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << 2568.3037, 303.3556, 108.8848 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_10_VWH_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_10_VWH_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_11_ID1_L
			sData.model 	= V_ILEV_GC_DOOR04
			sData.coords 	= << 813.1779, -2148.2695, 29.7689 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_01_DT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_11_ID1_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_11_ID1_R
			sData.model 	= V_ILEV_GC_DOOR03
			sData.coords 	= << 810.5769, -2148.2695, 29.7689 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_01_DT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_11_ID2_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_01_DT_SR
			sData.model 	= V_ILEV_GC_DOOR01
			sData.coords 	= << 6.8179, -1098.2095, 29.9469 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_01_DT_SR)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IGNORE_SAFETY_CHECKS))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_01_DT_SR"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GUN_SHOP_11_ID1_SR
			sData.model 	= V_ILEV_GC_DOOR01
			sData.coords 	= << 827.5342, -2160.4927, 29.7688 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GUN_SHOP_11_ID1_SR)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IGNORE_SAFETY_CHECKS))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GUN_SHOP_11_ID1_SR"
			#ENDIF
		BREAK
		
		CASE DOORNAME_JOSH_GATE_F_L
			sData.model 	= prop_lrggate_01c_l
			sData.coords 	= << -1107.01, 289.38, 64.76 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "JOSH_GATE_F_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_JOSH_GATE_F_R
			sData.model 	= prop_lrggate_01c_r
			sData.coords 	= << -1101.62, 290.36, 64.76 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_JOSH_GATE_F_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "JOSH_GATE_F_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_JOSH_GATE_R_L
			sData.model 	= prop_lrggate_01c_l
			sData.coords 	= << -1138.64, 300.82, 67.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_JOSH_GATE_R_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "JOSH_GATE_R_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_JOSH_GATE_R_R
			sData.model 	= prop_lrggate_01c_r
			sData.coords 	= << -1137.05, 295.59, 67.18 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_JOSH_GATE_R_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "JOSH_GATE_R_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_ARMYBASE_LIFT_L
			sData.model 	= v_ilev_bl_doorel_l
			sData.coords 	= << -2053.16, 3239.49, 30.50 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ARMYBASE_LIFT_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ARMYBASE_LIFT_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_ARMYBASE_LIFT_R
			sData.model 	= v_ilev_bl_doorel_r
			sData.coords 	= << -2054.39, 3237.23, 30.50 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ARMYBASE_LIFT_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ARMYBASE_LIFT_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_PALETO_BANK_TELLER
			sData.model 	= v_ilev_cbankcountdoor01
			sData.coords 	= << -108.91, 6469.11, 31.91 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_PALETO_BANK_TELLER)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "PALETO_BANK_TELLER"
			#ENDIF
		BREAK
		
		CASE DOORNAME_CHICKENFACTORY_EXT_GATE
			sData.model 	= prop_fnclink_03gate5
			sData.coords 	= << -182.91, 6168.37, 32.14 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CHICKENFACTORY_EXT_GATE)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CHICKENFACTORY_EXT_GATE"
			#ENDIF
		BREAK
		
		// Now we've split this up into multiple switches, this will always fire at once per frame, so let's turn it off
//		DEFAULT
//			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("GET_DOOR_DATA() - Door enum missing. Tell Kenneth R.")
//				CPRINTLN(DEBUG_DOOR, "GET_DOOR_DATA() - Door enum missing. Tell Kenneth R.")
//			#ENDIF
//		BREAK
	ENDSWITCH
	
	// The above SWITCH is now full and the game won't compile with any more CASEs in it!
	// So, uh, let's start a second one I guess 
	SWITCH eName
		CASE DOORNAME_DEALERSHIP_FRONT_L
			sData.model 	= v_ilev_csr_door_l
			sData.coords 	= << -59.89, -1092.95, 26.88 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_DEALERSHIP_FRONT_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "DEALERSHIP_FRONT_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_DEALERSHIP_FRONT_R
			sData.model 	= v_ilev_csr_door_r
			sData.coords 	= << -60.55, -1094.75, 26.89 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_DEALERSHIP_FRONT_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "DEALERSHIP_FRONT_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_DEALERSHIP_SIDE_L
			sData.model 	= v_ilev_csr_door_l
			sData.coords 	= << -39.13, -1108.22, 26.72 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_DEALERSHIP_SIDE_L)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "DEALERSHIP_SIDE_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_DEALERSHIP_SIDE_R
			sData.model 	= v_ilev_csr_door_r
			sData.coords 	= << -37.33, -1108.87, 26.72 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_DEALERSHIP_SIDE_R)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "DEALERSHIP_SIDE_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_RON_FRONTDOOR
			sData.model 	= prop_ron_door_01
			sData.coords 	= << 1943.73, 3803.63, 32.31 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_RON_FRONTDOOR)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RON_FRONTDOOR"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_FLEECA_VW_L
			sData.model 	= v_ilev_genbankdoor2
			sData.coords 	= << 316.39, -276.49, 54.52 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_FLEECA_VW_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_FLEECA_VW_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_FLEECA_VW_R
			sData.model 	= v_ilev_genbankdoor1
			sData.coords 	= << 313.96, -275.60, 54.52 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_FLEECA_VW_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_FLEECA_VW_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_FLEECA_HWAY_L
			sData.model 	= v_ilev_genbankdoor2
			sData.coords 	= << -2965.71, 484.22, 16.05 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_FLEECA_HWAY_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_FLEECA_HWAY_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_FLEECA_HWAY_R
			sData.model 	= v_ilev_genbankdoor1
			sData.coords 	= << -2965.82, 481.63, 16.05 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_FLEECA_HWAY_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_FLEECA_HWAY_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_ABBATOIR_FRONT_L
			sData.model 	= v_ilev_abbmaindoor
			sData.coords 	= << 962.10, -2183.83, 31.06 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ABBATOIR_FRONT_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ABBATOIR_FRONT_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_ABBATOIR_FRONT_R
			sData.model 	= v_ilev_abbmaindoor2
			sData.coords 	= << 961.79, -2187.08, 31.06 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ABBATOIR_FRONT_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ABBATOIR_FRONT_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_NOSE_HELIPAD
			sData.model 	= prop_ch3_04_door_02
			sData.coords 	= << 2508.43, -336.63, 115.76 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_NOSE_HELIPAD)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "NOSE_HELIPAD"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_QING_L
			sData.model 	= prop_ch1_07_door_01l
			sData.coords 	= << -2255.19, 322.26, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_QING_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_QING_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_QING_R
			sData.model 	= prop_ch1_07_door_01r
			sData.coords 	= << -2254.06, 319.70, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_QING_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_QING_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_QINGALT_L
			sData.model 	= prop_ch1_07_door_01l
			sData.coords 	= << -2301.13, 336.91, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_QINGALT_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_QINGALT_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_QINGALT_R
			sData.model 	= prop_ch1_07_door_01r
			sData.coords 	= << -2298.57, 338.05, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_QINGALT_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_QINGALT_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_TALES_L
			sData.model 	= prop_ch1_07_door_01l
			sData.coords 	= << -2222.32, 305.86, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_TALES_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_TALES_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_TALES_R
			sData.model 	= prop_ch1_07_door_01r
			sData.coords 	= << -2221.19, 303.30, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_TALES_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_TALES_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_RICHES_L
			sData.model 	= prop_ch1_07_door_01l
			sData.coords 	= << -2280.60, 265.43, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_RICHES_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_RICHES_R
			sData.model 	= prop_ch1_07_door_01r
			sData.coords 	= << -2278.04, 266.57, 184.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_RICHES_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_RICHES_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_GARAGE_JETSTEAL
			sData.model 	= prop_gar_door_04
			sData.coords 	= << 778.31, -1867.49, 30.66 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_GARAGE_JETSTEAL)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "GARAGE_JETSTEAL"
			#ENDIF
		BREAK
		
		CASE DOORNAME_EPSILON_GATE_L
			sData.model 	= prop_gate_tep_01_l
			sData.coords 	= << -721.35, 91.01, 56.68 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON_GATE_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON_GATE_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_EPSILON_GATE_R
			sData.model 	= prop_gate_tep_01_r
			sData.coords 	= << -728.84, 88.64, 56.68 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_EPSILON_GATE_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "EPSILON_GATE_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_FRONTL_L
			sData.model 	= prop_artgallery_02_dr
			sData.coords 	= << -2287.62, 363.90, 174.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_FRONTL_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_FRONTL_R
			sData.model 	= prop_artgallery_02_dl
			sData.coords 	= << -2289.78, 362.91, 174.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_FRONTL_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_FRONTR_L
			sData.model 	= prop_artgallery_02_dr
			sData.coords 	= << -2289.86, 362.88, 174.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_FRONTR_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_FRONTR_R
			sData.model 	= prop_artgallery_02_dl
			sData.coords 	= << -2292.01, 361.89, 174.93 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "KORTZ_FRONTR_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_CLETUS_GATE
			sData.model 	= PROP_FNCLINK_07GATE1
			sData.coords 	= <<1803.94, 3929.01, 33.72 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_CLETUS_GATE)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "CLETUS_GATE"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_FLEECA_BURTON_L
			sData.model 	= v_ilev_genbankdoor2
			sData.coords 	= <<-348.81, -47.26, 49.39 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_FLEECA_BURTON_L)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_FLEECA_BURTON_L"
			#ENDIF
		BREAK
		
		CASE DOORNAME_BANK_FLEECA_BURTON_R
			sData.model 	= v_ilev_genbankdoor1
			sData.coords 	= <<-351.26, -46.41, 49.39 >>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_BANK_FLEECA_BURTON_R)
			SET_BIT(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BANK_FLEECA_BURTON_R"
			#ENDIF
		BREAK
		
		CASE DOORNAME_ABATTOIR_EXIT
			sData.model 	= PROP_ABAT_SLIDE
			sData.coords 	= <<962.9084, -2105.8137, 34.6432>>
			sData.doorHash  = ENUM_TO_INT(DOORHASH_ABATTOIR_EXIT)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ABATTOIR_EXIT"
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF IS_VECTOR_ZERO(sData.coords)
			SCRIPT_ASSERT("\nGET_DOOR_DATA() - door passed through here has a coord of <<0,0,0>>, is it legit? See SCRIPT_DOORS output, tell Kenneth R/Ian G")
			CPRINTLN(DEBUG_DOOR, "GET_DOOR_DATA() - something wrong with this door: ", INT_TO_ENUM(DOOR_NAME_ENUM, eName))
		ENDIF
	#ENDIF
	
	RETURN sData
ENDFUNC

/// PURPOSE: Returns a struct that contains all the data for the specified building enum
FUNC BOOL GET_BUILDING_DATA(BUILDING_DATA_STRUCT &sData, BUILDING_NAME_ENUM eName)

	// Reset data
	INT i
	REPEAT NUMBER_OF_BUILDING_STATES i
		sData.model[i] = DUMMY_MODEL_FOR_SCRIPT
		sData.name[i] = ""
		sData.road_node_active[i] = FALSE
		sData.nav_mesh_block_active[i] = FALSE
		sData.code_vehgen_block_active[i] = FALSE
	ENDREPEAT
	
	sData.coords = <<0,0,0>>
	sData.type = BUILDINGTYPE_STATIC
	
	sData.auto_cleanup = FALSE
	
	sData.ipl_preload = ""
	sData.es_interior = ""
	sData.es_force_remove = ""
	
	// Road nodes to toggle
	sData.road_node_min_coords = <<0,0,0>>
	sData.road_node_max_coords = <<0,0,0>>
	
	// Navmesh to block
	sData.nav_mesh_block_postion = <<0,0,0>>
	sData.nav_mesh_block_sizeXYZ = <<0,0,0>>
	sData.nav_mesh_block_heading = 0.0
	
	// Scenario zone to block
	sData.scenario_block_minXYZ = <<0,0,0>>
	sData.scenario_block_maxXYZ = <<0,0,0>>
	
	// Code vehicle gens to block
	sData.code_vehgen_min_coords = <<0,0,0>>
	sData.code_vehgen_max_coords = <<0,0,0>>
	
	#IF IS_DEBUG_BUILD
		sData.dbg_name = ""
	#ENDIF	
	
	// BUILDINGTYPE_STATIC 		- Requires normal model, destroyed model, and coords
	// BUILDINGTYPE_IPL 		- Requires normal name and destroyed name
	// BUILDINGTYPE_ENTITY_SET 	- Requires normal name and destroyed name
	// BUILDINGTYPE_RAYFIRE 	- Requires normal name and coords
	
	SWITCH eName
		CASE BUILDINGNAME_IPL_ORTEGA_TRAILER
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "TRV1_Trail_start"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "TRV1_Trail_end"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "TRV1_Trail_Finish"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << -24.685, 3032.920, 40.331>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_ORTEGA_TRAILER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "CS3_05_water_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS3_05_water_grp2"
			sData.coords 							= << -24.685, 3032.920, 40.331 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_ORTEGA_TRAILER_WATER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_RF_GASSTATION01
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "gasstation_ipl_group1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "gasstation_ipl_group2"
			sData.coords 							= << -93.4, 6410.9, 36.8 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RF_GASSTATION01"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_RF_HEAT_WALL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "DES_Smash2_startimap"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DES_Smash2_endimap"
			sData.coords							= <<890.3647, -2367.289, 28.10582>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RF_HEAT_WALL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_RF_STILT_HOUSE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "DES_StiltHouse_imapstart"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DES_StiltHouse_imapend"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "des_stilthouse_rebuild"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<-1020.50, 663.41, 154.75>>
			sData.road_node_min_coords = <<-1018.913452, 603.290405, 105.661087>>
			sData.road_node_max_coords = <<-1038.913452, 639.290405, 135.661087>>
			sData.road_node_active[BUILDINGSTATE_NORMAL] = TRUE
			sData.road_node_active[BUILDINGSTATE_DESTROYED] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "RF_STILT_HOUSE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BNKHEIST_APT
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "bnkheist_apt_norm"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "bnkheist_apt_dest"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BNKHEIST_APT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BNKHEIST_APT_VFX
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "bnkheist_apt_dest_vfx"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "REMOVE_ALL_STATES"
			sData.auto_cleanup						= TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BNKHEIST_APT_VFX"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARGOPLANE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "crashed_cargoplane"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARGOPLANE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARSTEAL_JB700
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "prop_jb700_covered"
			sData.coords							= <<490.8999, -1334.0680, 28.3298>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARSTEAL_JB700"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARSTEAL_ENTITYXF
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "prop_entityXF_covered"
			sData.coords							= <<490.8999, -1334.0680, 28.3298>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARSTEAL_ENTITYXF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARSTEAL_CHEETAH
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "prop_cheetah_covered"
			sData.coords							= <<490.8999, -1334.0680, 28.3298>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARSTEAL_CHEETAH"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARSTEAL_ZTYPE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "prop_ztype_covered"
			sData.coords							= <<490.8999, -1334.0680, 28.3298>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARSTEAL_ZTYPE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_KILLED_MICHAEL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "BH1_48_Killed_Michael"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_KILLED_MICHAEL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARGOSHIP
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "cargoship"
			sData.name[BUILDINGSTATE_DESTROYED]		= "sunkcargoship"
			sData.nav_mesh_block_postion = <<-162.891815,-2365.769287,0>>
			sData.nav_mesh_block_sizeXYZ = <<190.750000,31.250000,21.000000>>
			sData.nav_mesh_block_heading = 0.0
			sData.nav_mesh_block_active[BUILDINGSTATE_NORMAL] = FALSE
			sData.nav_mesh_block_active[BUILDINGSTATE_DESTROYED] = TRUE	
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARGOSHIP"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARGOSHIP_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "ship_occ_grp1"
			sData.name[BUILDINGSTATE_DESTROYED]		= "ship_occ_grp2"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARGOSHIP_OCCLUSION"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_CRUISESHIP
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "smboat"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CRUISESHIP"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PALETO_GAS_STATION_EFFECTS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "gasparticle_grp2"
			sData.coords 							= << -95.2, 6411.3, 31.5 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PALETO_GAS_STATION_EFFECTS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_1
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "CS1_02_cf_offmission"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS1_02_cf_onmission1"
			sData.coords 							= <<-146.3837, 6161.5000, 30.2062>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PALETO_CHICKEN_FACTORY_INTERIOR_1"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_2
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS1_02_cf_onmission2"
			sData.coords 							= <<-146.3837, 6161.5000, 30.2062>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PALETO_CHICKEN_FACTORY_INTERIOR_2"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_3
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS1_02_cf_onmission3"
			sData.coords 							= <<-146.3837, 6161.5000, 30.2062>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PALETO_CHICKEN_FACTORY_INTERIOR_3"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_4
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS1_02_cf_onmission4"
			sData.coords 							= <<-146.3837, 6161.5000, 30.2062>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PALETO_CHICKEN_FACTORY_INTERIOR_4"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JETSTEAL_TUNNEL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "jetstealtunnel"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 801.7, -1810.8, 23.3 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JETSTEAL_TUNNEL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JETSTEAL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "Jetsteal_ipl_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "Jetsteal_ipl_grp2"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "REMOVE_ALL_STATES"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << 787.3967, -1808.8584, 29.8532 >>
			sData.road_node_min_coords = << 814, -1750, 20 >>
			sData.road_node_max_coords = << 790, -1899, 35 >>
			sData.road_node_active[BUILDINGSTATE_NORMAL] = TRUE
			sData.road_node_active[BUILDINGSTATE_DESTROYED] = FALSE
			sData.road_node_active[BUILDINGSTATE_CLEANUP] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JETSTEAL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JOSHHOUSE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "BH1_47_JoshHse_UnBurnt"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "BH1_47_JoshHse_Burnt"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JOSHHOUSE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JOSHHOUSE_FIRE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "bh1_47_joshhse_firevfx"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JOSHHOUSE_FIRE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JOSHHOTEL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "SC1_30_Keep_Closed"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JOSHHOTEL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_VB_PROPS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "triathlon2_VBprops"
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_VB_PROPS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FIB_C4
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DT1_05_REQUEST"
			sData.coords 							= << 163.4, -745.7, 251.0 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB_C4"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FIB_HOLE_PLUG
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "FBI_colPLUG"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 74.29, -736.05, 46.76 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB_HOLE_PLUG"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FIB_REPAIR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "FBI_repair"
			sData.coords 							= << 74.29, -736.05, 46.76 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB_REPAIR"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_MS_FIB_DAMAGE_HLOD
			sData.type 								= BUILDINGTYPE_MODEL_SWAP
			sData.model[BUILDINGSTATE_NORMAL] 		= INT_TO_ENUM(MODEL_NAMES, HASH("DT1_05_Build1_H"))
			sData.model[BUILDINGSTATE_DESTROYED] 	= INT_TO_ENUM(MODEL_NAMES, HASH("DT1_05_Build1_Damage"))
			sData.coords 							= << 136.004,-749.287,153.302 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MS_FIB_DAMAGE_HLOD"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_MS_FIB_DAMAGE_LOD
			sData.type 								= BUILDINGTYPE_MODEL_SWAP
			sData.model[BUILDINGSTATE_NORMAL] 		= INT_TO_ENUM(MODEL_NAMES, HASH("DT1_05_Build1_LOD"))
			sData.model[BUILDINGSTATE_DESTROYED] 	= INT_TO_ENUM(MODEL_NAMES, HASH("DT1_05_Build1_Damage_LOD"))
			sData.coords 							= << 136.004,-749.287,153.302 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MS_FIB_DAMAGE_LOD"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_MS_FIB_DAMAGE_SLOD
			sData.type 								= BUILDINGTYPE_MODEL_SWAP
			sData.model[BUILDINGSTATE_NORMAL] 		= INT_TO_ENUM(MODEL_NAMES, HASH("DT1_05_SLOD"))
			sData.model[BUILDINGSTATE_DESTROYED] 	= INT_TO_ENUM(MODEL_NAMES, HASH("DT1_05_Damage_SLOD"))
			sData.coords 							= << 178.534,-668.835,37.2113 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MS_FIB_DAMAGE_SLOD"
			#ENDIF
		BREAK
		
		//lights for explosion cutscene
		case BUILDINGNAME_MS_FIB_HEIST_EXPLOSION_LIGHTS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "FIB_heist_lights"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 136.004,-749.287,153.302 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MS_FIB_HEIST_EXPLOSION_LIGHTS"
			#ENDIF
		break 
		
		//damage for building after explosion
		case BUILDINGNAME_MS_FIB_HEIST_EXPLOSION_DAMAGE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "FIB_heist_dmg"
			sData.coords 							= << 136.004,-749.287,153.302 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MS_FIB_HEIST_EXPLOSION_DAMAGE"
			#ENDIF
		break 
		
		CASE BUILDINGNAME_IPL_FIB_RUBBLE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DT1_05_rubble"
			sData.coords 							= << 74.29, -736.05, 46.76 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB_RUBBLE"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_FIB_LOBBY_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "FIBlobbyfake"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "FIBlobby"
			sData.coords 							= <<105.4557, -745.4835, 44.7548>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB_LOBBY_INTERIOR"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_HELIHOLE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "DT1_05_HC_REMOVE"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DT1_05_HC_REQ"
			sData.coords 							= << 169.0, -670.3, 41.9 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_HELIHOLE"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA
			sData.type 								= BUILDINGTYPE_IPL
			sData.coords 							= << 50.2, 3743.9, 40.9 >>
			sData.scenario_block_minXYZ 			= <<16.9757, 3614.3074, 30.0677>>
			sData.scenario_block_maxXYZ 			= <<145.2451,3748.9121, 49.6958>>
			sData.code_vehgen_min_coords			= <<16.9757, 3614.3074, 30.0677>>
			sData.code_vehgen_max_coords			= <<145.2451,3748.9121, 49.6958>>
			sData.code_vehgen_block_active[BUILDINGSTATE_NORMAL] 	= FALSE
			sData.code_vehgen_block_active[BUILDINGSTATE_DESTROYED] = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_BLOCKING_AREA"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_TRAILERPARK_A
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "trailerparkA_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "trailerparkA_grp2"
			sData.coords 							= << 50.2, 3743.9, 40.9 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_A"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_A_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "occl_trailerA_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 50.2, 3743.9, 40.9 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_A_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_B
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "trailerparkB_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "trailerparkB_grp2"
			sData.coords 							= << 106.7, 3732.1, 40.8 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_B"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_B_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "occl_trailerB_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 106.7, 3732.1, 40.8 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_B_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_C
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "trailerparkC_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "trailerparkC_grp2"
			sData.coords 							= << 72.7, 3695.4, 42.0 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_C"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_C_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "occl_trailerC_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 72.7, 3695.4, 42.0 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_C_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_D
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "trailerparkD_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "trailerparkD_grp2"
			sData.coords 							= << 43.8, 3699.7, 41.3 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_D"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_D_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "occl_trailerD_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 43.8, 3699.7, 41.3 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_D_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_E
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "trailerparkE_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "trailerparkE_grp2"
			sData.coords 							= << 28.5, 3668.0, 40.4 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_E"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRAILERPARK_E_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "occl_trailerE_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 28.5, 3668.0, 40.4 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRAILERPARK_E_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_METH_TRAILER
			sData.type 								= BUILDINGTYPE_IPL
			sData.ipl_preload				 		= "des_methtrailer"
			sData.name[BUILDINGSTATE_NORMAL] 		= "methtrailer_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "methtrailer_grp2"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "methtrailer_grp3"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << 29.4838, 3735.5930, 38.6880 >>
			sData.nav_mesh_block_postion = << 31.134, 3738.783, 39.062 >>
			sData.nav_mesh_block_sizeXYZ = << 13.600, 20.000, 8.900 >>
			sData.nav_mesh_block_heading = 48.000
			sData.nav_mesh_block_active[BUILDINGSTATE_NORMAL] = FALSE
			sData.nav_mesh_block_active[BUILDINGSTATE_DESTROYED] = TRUE
			sData.nav_mesh_block_active[BUILDINGSTATE_CLEANUP] = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_METH_TRAILER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_METH_TRAILER_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "occl_meth_grp1"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 29.4838, 3735.5930, 38.6880 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_METH_TRAILER_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FARM_HOUSE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "des_farmhs_startimap"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "des_farmhs_endimap"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "REMOVE_ALL_STATES"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << 2450.5955, 4959.9292, 44.2575 >>
			sData.scenario_block_minXYZ 			= <<2383.755615,4929.988281,39.524609>>
			sData.scenario_block_maxXYZ 			= <<2505.755615,5023.988281,67.524609>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FARM_HOUSE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FARM_HOUSE_OCCLUSION
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "des_farmhs_start_occl"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "des_farmhs_end_occl"
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.coords 							= << 2450.5955, 4959.9292, 44.2575 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FARM_HOUSE_OCCLUSION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "farm"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "farm"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "farm_burnt"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << 2444.8, 4976.4, 50.5 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FARM_HOUSE_CLNUP1"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "farm_props"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= "farm_burnt_props"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << 2447.9, 4973.4, 47.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FARM_HOUSE_CLNUP2"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "des_farmhouse"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "des_farmhouse"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "REMOVE_ALL_STATES"
			sData.auto_cleanup						= TRUE
			sData.coords 							= << 2447.9, 4973.4, 47.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FARM_HOUSE_CLNUP4"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "farmint_cap"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "farmint"
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.coords 							= << 2447.9, 4973.4, 47.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FARM_HOUSE_INTERIOR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_1
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "tankerexp_grp0"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "tankerexp_grp3"
					sData.coords 							=<<1676.4154, -1626.3705, 111.4848>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_KILL_TREVOR_TANKER_EXPLOSION_1"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_2
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "tankerexp_grp1"
					sData.coords 							=<<1676.4154, -1626.3705, 111.4848>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_KILL_TREVOR_TANKER_EXPLOSION_2"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_3
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "tankerexp_grp2"
					sData.coords 							=<<1676.4154, -1626.3705, 111.4848>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_KILL_TREVOR_TANKER_EXPLOSION_3"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_KILL_TREVOR_TANKER_EXPLOSION_4
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DES_tankerexp"
					sData.coords 							=<<1676.4154, -1626.3705, 111.4848>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_KILL_TREVOR_TANKER_EXPLOSION_4"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_DOCK_CRANE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "dockcrane1"
			sData.coords 							= << 889.3, -2910.9, 40.0 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_DOCK_CRANE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CANYON_RIVERBED
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "CanyonRvrShallow"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CanyonRvrDeep"
			sData.coords 							= << -1600.6194, 4443.4565, 0.7250 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CANYON_RIVERBED"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_LOST_GANG_DOORS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "Garage_door_locked"
			sData.coords 							= << 966.1, -114.8, 75.2 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_LOST_GANG_DOORS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FIB3_BEACH_HOUSE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "ch1_02_closed"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "ch1_02_open"
			sData.coords 							= << -3086.4285, 339.2523, 6.3717 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB3_BEACH_HOUSE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FERRIS_WHEEL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "ferris_finale_Anim"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << -1675.1783, -1143.6046, 12.0175 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FERRIS_WHEEL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_EXL3_TRAIN_CRASH
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "railing_start"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "railing_end"
			sData.coords 							= << -532.1309, 4526.1870, 88.7955 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_EXL3_TRAIN_CRASH"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_EXL3_TRAIN_CRASH_2
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "canyonriver01"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "canyonriver01_traincrash"
			sData.coords 							= << -532.1309, 4526.1870, 88.7955 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_EXL3_TRAIN_CRASH_2"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FIB2_TOWER
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "DT1_05_WOFFM"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DT1_05_FIB2_Mission"
			sData.name[BUILDINGSTATE_CLEANUP]		= "DT1_05_WOFFM"
			sData.coords 							= << 131.29, -631.22, 261.85 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB2_TOWER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CORONER_TRASH
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "coronertrash"
			sData.coords 							= << 233.9, -1355.0, 30.3 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CORONER_TRASH"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CORONER_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "Coroner_Int_off"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "Coroner_Int_on"
			sData.coords 							= << 234.4, -1355.6, 40.5 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CORONER_INTERIOR"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "id2_14_pre_no_int"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 716.84, -962.05, 31.59 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SWEATSHOP_NO_INTERIOR"
			#ENDIF
		BREAK

		CASE BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "id2_14_during1"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "id2_14_during2"
			sData.coords 							= << 716.84, -962.05, 31.59 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SWEATSHOP_WITH_INTERIOR"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_SWEATSHOP_BURNT
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "id2_14_on_fire"
			sData.name[BUILDINGSTATE_CLEANUP]	 	= "id2_14_post_no_int"
			sData.coords 							= << 716.84, -962.05, 31.59 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SWEATSHOP_BURNT"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "id2_14_during_door"
			sData.coords 							= << 716.84, -962.05, 31.59 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SWEATSHOP_BURNT"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "burnt_switch_off"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= << 716.84, -962.05, 31.59 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SWEATSHOP_BURNT"
			#ENDIF
		BREAK
		
		
		CASE BUILDINGNAME_IPL_PILLBOX_HILL
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "RC12B_Default"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "RC12B_Destroyed"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "RC12B_Fixed"
			sData.auto_cleanup						= FALSE
			sData.coords 							=  <<330.4596, -584.8196, 42.3174>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PILLBOX_HILL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "RC12B_HospitalInterior"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.auto_cleanup						= FALSE
			sData.coords 							=  <<330.4596, -584.8196, 42.3174>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PILLBOX_HILL_INTERIOR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CAPOLAVORO_BILLBOARD_GRAFFITI
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "SM_15_BldGRAF1"
			sData.coords 							=  <<330.4596, -584.8196, 42.3174>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CAPOLAVORO_BILLBOARD_GRAFFITI"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BISHOPS_CHICKEN_BILLBOARD_GRAFFITI
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CH3_RD2_BishopsChickenGraffiti"
			sData.coords 							=  <<1861.28,2402.11,58.53>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BISHOPS_CHICKEN_BILLBOARD_GRAFFITI"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FRUIT_BILLBOARD_GRAFFITI
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "FruitBB"
			sData.coords 							=  <<-1327.46,-274.82,54.25>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FRUIT_BILLBOARD_GRAFFITI"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MAZE_BILLBOARD_GRAFFITI
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS5_04_MazeBillboardGraffiti"
			sData.coords 							=  <<2697.32,3162.18,58.1>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MAZE_BILLBOARD_GRAFFITI"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_RON_OIL_BILLBOARD_GRAFFITI
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS5_Roads_RonOilGraffiti"
			sData.coords 							=  <<2119.12,3058.21,53.25>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_RON_OIL_BILLBOARD_GRAFFITI"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_VAPID_BILLBOARD_GRAFFITI
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "ap1_03_bbrd_dcl"
			sData.coords 							=  <<-804.25,-2276.88,23.59>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_VAPID_BILLBOARD_GRAFFITI"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MELTDOWN_POSTERS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "HW1_02_OldBill"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "HW1_02_NewBill"
			sData.coords 							=  <<296.5,173.3,100.4>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MELTDOWN_POSTERS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MELTDOWN_POSTERS_EMISSIVE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "HW1_Emissive_OldBill"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "HW1_Emissive_NewBill"
			sData.coords 							=  <<296.5,173.3,100.4>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MELTDOWN_POSTERS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARSTEAL_GARAGE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= TRUE
			sData.coords 							= <<480.9554, -1321.2096, 28.2037>>
			sData.code_vehgen_min_coords			= <<508.3, -1299.3, 39.4>>
			sData.code_vehgen_max_coords			= <<459.9, -1363.2, 21.4>>
			sData.code_vehgen_block_active[BUILDINGSTATE_NORMAL] 	= FALSE
			sData.code_vehgen_block_active[BUILDINGSTATE_DESTROYED] = TRUE
			sData.code_vehgen_block_active[BUILDINGSTATE_CLEANUP] 	= FALSE
			
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARSTEAL_GARAGE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TREVORS_TRAILER
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "TrevorsTrailer"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "TrevorsTrailerTrash"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "TrevorsTrailerTidy"
			sData.coords 							= << 1973.0, 3815.0, 34.0 >>
			sData.auto_cleanup						= FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TREVORS_TRAILER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_ARM2_SCAFFOLDING
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "scafstartimap"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "scafendimap"
			sData.coords 							= << -1088.6, -1650.6, 6.4 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_ARM2_SCAFFOLDING"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CHOP_PROPS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "chop_props"
			sData.coords 							= << -13.83, -1455.45, 31.81 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CHOP_PROPS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRIATHLON_1_INFLATABLE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "AP1_04_TriAf01"
			sData.coords 							= <<-1277.6292, -2030.9130, 1.2823>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRIATHLON_1_INFLATABLE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRIATHLON_2_INFLATABLE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS2_06_TriAf02"
			sData.coords 							= <<2384.9692, 4277.5825, 30.3790>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRIATHLON_2_INFLATABLE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_TRIATHLON_3_INFLATABLE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "CS4_04_TriAf03"
			sData.coords 							= <<1577.8813, 3836.1074, 30.7717>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_TRIATHLON_3_INFLATABLE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_AGENCY_2_LIFTS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "DT1_21_prop_lift_on"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= <<-180.5771, -1016.9276, 28.2893>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_AGENCY_2_LIFTS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JEWEL_STORE
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "jewel2fake"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "post_hiest_unload"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "bh1_16_refurb"
			sData.coords 							= <<-630.4205, -236.7843, 37.0570>>
			sData.scenario_block_minXYZ 			= <<-623.686829-11.000000, -231.935043-11.000000,40.307034-3.250000>>
			sData.scenario_block_maxXYZ 			= <<-623.686829+11.000000, -231.935043+11.000000,40.307034+3.250000>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JEWEL_STORE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "bh1_16_doors_shut"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "refit_unload"
			sData.coords 							= <<-583.1606, -282.3967, 35.3940>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_JEWEL_HEIST_BIKE_TUNNELS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "v_tunnel_hole_swap"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "v_tunnel_hole"
			sData.coords 							= <<-14.6510, -604.3639, 25.1823>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_JEWEL_HEIST_BIKE_TUNNELS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BIG_SCORE_RAIL_TANKCAR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "cs5_4_trains"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= <<2773.6099, 2835.3274, 35.1903>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BIG_SCORE_RAIL_TANKCAR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_AIRFIELD_PROPS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "airfield"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= <<1743.6821, 3286.2512, 40.0875>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_AIRFIELD_PROPS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MARTIN1_ROAD_NODES
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= "REMOVE_ALL_STATES"
			sData.auto_cleanup						= TRUE
			sData.coords 							= <<1222.9, 1877.9, 79.9>>
			sData.road_node_min_coords = <<1206.8, 1803.0, 43.9>>
			sData.road_node_max_coords = <<1329.0, 2060.4, 143.9>>
			sData.road_node_active[BUILDINGSTATE_NORMAL] = FALSE
			sData.road_node_active[BUILDINGSTATE_DESTROYED] = TRUE
			sData.road_node_active[BUILDINGSTATE_CLEANUP] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MARTIN1_ROAD_NODES"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BB_TACO_BOMB
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "SC1_01_OldBill"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "SC1_01_NewBill"
			sData.coords 							= << -351, -1324, 44.02 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BB_TACO_BOMB"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BB_CCC_KINGS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "DT1_17_OldBill"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "DT1_17_NewBill"
			sData.coords 							= << 391.81, -962.71, 41.97 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BB_CCC_KINGS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BB_MOLLIS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "SC1_14_OldBill"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "SC1_14_NewBill"
			sData.coords 							= << 424.2, -1944.31, 33.09 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BB_MOLLIS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BIG_SCORE_RAILS_1
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "ld_rail_01_track"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= <<2626.3743, 2949.8689, 39.1409>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BIG_SCORE_RAILS_1"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BIG_SCORE_RAILS_2
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "ld_rail_02_track"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.coords 							= <<2626.3743, 2949.8689, 39.1409>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BIG_SCORE_RAILS_2"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MIC_AMANDAS_STUFF
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "V_Michael_M_items"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_M_moved"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -811.2679, 179.3344, 75.7408 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MIC_AMANDAS_STUFF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MIC_TRACEYS_STUFF
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "V_Michael_D_items"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_D_Moved"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -802.0311, 172.9131, 75.7408 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MIC_TRACEYS_STUFF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MIC_JIMMYS_STUFF
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "V_Michael_S_items"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_S_items_swap"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -808.0330, 172.1309, 75.7406 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MIC_JIMMYS_STUFF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MIC_LOUNGE_STUFF
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "V_Michael_L_Items"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_L_Moved"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -808.0330, 172.1309, 75.7406 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MIC_LOUNGE_STUFF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_M_items_swap"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -808.0330, 172.1309, 75.7406 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MIC_LOUNGE_STUFF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TRACEY_BEDROOM_FAME_OR_SHAME
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_FameShame"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -802.0311, 172.9131, 75.7408 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TRACEY_BEDROOM_FAME_OR_SHAME"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MICHAEL_BEDROOM_JEWEL_HEIST_SPY_GLASSES
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_JewelHeist"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -813.3, 177.5, 75.76.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MICHAEL_BEDROOM_JEWEL_HEIST_SPY_GLASSES"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MICHAEL_UPSTAIRS_MOVIE_PREMIER
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "Michael_premier"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -813.3, 177.5, 75.76.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MICHAEL_UPSTAIRS_MOVIE_PREMIER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MICHAEL_HALL_PLANE_TICKETS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_plane_ticket"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -813.3, 177.5, 75.76.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FAMILY5_KITCHEN_BURGERSHOT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "burgershot_yoga"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -813.3, 177.5, 75.76.7 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FAMILY5_KITCHEN_BURGERSHOT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FIB5_MICHAEL_GARAGE_SCUBA
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_Scuba"
			sData.es_interior 						= "V_Michael_Garage"
			sData.coords 							= <<-810.5301, 187.7868, 71.4786>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_ES_FIB5_MICHAEL_GARAGE_SCUBA"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_MICHAEL_BED
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "V_Michael_bed_tidy"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_Michael_bed_Messy"
			sData.es_interior 						= "V_Michael"
			sData.coords 							= << -811.2679, 179.3344, 75.7408 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_MIC_AMANDAS_STUFF"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_JEWEL_HEIST_SWEATSHOP_BUGSTAR_GEAR
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "Jewel_Gasmasks"
			sData.es_interior 						= "V_Sweat"
			sData.coords 							= <<707.2563, -965.1470, 29.4179>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_JEWEL_HEIST_SWEATSHOP_BUGSTAR_GEAR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_OVERALLS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_53_Agency _Overalls"
			sData.es_interior 						= "V_Sweat"
			sData.coords 							= <<707.2563, -965.1470, 29.4179>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_AGENCY_HEIST_SWEATSHOP_OVERALLS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_BLUEPRINT
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_53_Agency_Blueprint"
			sData.es_interior 						= "V_Sweat"
			sData.coords 							= <<707.2563, -965.1470, 29.4179>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_AGENCY_HEIST_SWEATSHOP_BLUEPRINT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FIB4_SWEATSHOP_KITBAG
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_35_KitBag"
			sData.es_interior 						= "V_Sweat"
			sData.coords 							= <<707.2563, -965.1470, 29.4179>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FIB4_SWEATSHOP_KITBAG"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_PALETO_SWEATSHOP_ARMOUR
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_35_Body_Armour"
			sData.es_interior 						= "V_Sweat"
			sData.coords 							= <<707.2563, -965.1470, 29.4179>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_PALETO_SWEATSHOP_ARMOUR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_AGENCY_HEIST_SWEATSHOP_FIREMAN_GEAR
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_35_Fireman"
			sData.es_interior 						= "V_Sweat"
			sData.coords 							= <<707.2563, -965.1470, 29.4179>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_AGENCY_HEIST_SWEATSHOP_FIREMAN_GEAR"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_26_Trevor_Helmet1"
			sData.es_interior 						= "V_Trailer"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TRAILER_NORMAL_COP_UNIFORM"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_26_Trevor_Helmet3"
			sData.es_interior 						= "V_TrailerTRASH"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TRAILER_TRASH_COP_UNIFORM"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_26_Trevor_Helmet2"
			sData.es_interior 						= "V_TrailerTidy"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_ES_TRAILER_TIDY_COP_UNIFORM"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_24_Trevor_Briefcase1"
			sData.es_interior 						= "V_Trailer"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TRAILER_NORMAL_BRIEFCASE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_24_Trevor_Briefcase3"
			sData.es_interior 						= "V_TrailerTRASH"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TRAILER_TRASH_BRIEFCASE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_24_Trevor_Briefcase2"
			sData.es_interior 						= "V_TrailerTidy"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TRAILER_TIDY_BRIEFCASE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_26_Michael_Stay1"
			sData.es_interior 						= "V_Trailer"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_26_Michael_Stay3"
			sData.es_interior 						= "V_TrailerTRASH"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_26_Michael_Stay2"
			sData.es_interior 						= "V_TrailerTidy"
			sData.coords 							= <<1973.8053, 3818.5547, 32.4363>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "shutter_open"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "shutter_closed"
			sData.es_interior 						= "v_carshowroom"
			sData.coords 							= << -30.8793, -1088.3356, 25.4221 >>
			sData.nav_mesh_block_postion 			= <<-29.30, -1086.35, 25.57>>
			sData.nav_mesh_block_sizeXYZ			= <<5.5,3,2>>
			sData.nav_mesh_block_heading 			= -10
			sData.nav_mesh_block_active[BUILDINGSTATE_NORMAL] = FALSE 
			sData.nav_mesh_block_active[BUILDINGSTATE_DESTROYED] = TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_CAR_SHOWROOOM_SHUTTERS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "csr_beforeMission"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "csr_afterMissionA"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "csr_afterMissionB"
			sData.es_force_remove					= "csr_inMission"
			sData.auto_cleanup						= FALSE
			sData.es_interior 						= "v_carshowroom"
			sData.coords 							= << -59.7936, -1098.7841, 27.2612 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_CAR_SHOWROOOM_WINDOWS"
			#ENDIF
		BREAK
		// Northern wall, next to the fake door and glass cabinet
		CASE BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE1
			sData.type 								= BUILDINGTYPE_IPL			//BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""						
			sData.name[BUILDINGSTATE_CLEANUP]		= ""
			sData.es_interior 						= "v_carshowroom"	
			sData.coords 							= <<-49.21, -1090.28, 25.42>>
			sData.nav_mesh_block_postion 			= <<-49.21, -1090.28, 25.42>>
			sData.nav_mesh_block_sizeXYZ			= <<2.5,3,3>>
			sData.nav_mesh_block_heading 			= 0
			sData.nav_mesh_block_active[BUILDINGSTATE_NORMAL] = FALSE 
			sData.nav_mesh_block_active[BUILDINGSTATE_DESTROYED] = TRUE 
			sData.nav_mesh_block_active[BUILDINGSTATE_CLEANUP] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_CAR_SHOWROOOM_RUBBLE1"
			#ENDIF
		BREAK
		// Next to rubble 1, covering the fallen Voltic display stand
		CASE BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE2
			sData.type 								= BUILDINGTYPE_IPL			//BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""						
			sData.name[BUILDINGSTATE_CLEANUP]		= ""
			sData.es_interior 						= "v_carshowroom"	
			sData.coords 							= <<-49.28, -1092.66, 25.42>>
			sData.nav_mesh_block_postion 			= <<-49.28, -1092.66, 25.42>>
			sData.nav_mesh_block_sizeXYZ			= <<3,1,3>>
			sData.nav_mesh_block_heading 			= 0
			sData.nav_mesh_block_active[BUILDINGSTATE_NORMAL] = FALSE 
			sData.nav_mesh_block_active[BUILDINGSTATE_DESTROYED] = TRUE 
			sData.nav_mesh_block_active[BUILDINGSTATE_CLEANUP] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_CAR_SHOWROOOM_RUBBLE2"
			#ENDIF
		BREAK
		// Covering the tipped-over desk by the "Premium Deluxe Motorsport" signage on the wall, by the smashed window
		CASE BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE3
			sData.type 								= BUILDINGTYPE_IPL			//BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""						
			sData.name[BUILDINGSTATE_CLEANUP]		= ""
			sData.es_interior 						= "v_carshowroom"	
			sData.coords 							= <<-53.07, -1096.73, 25.50>>
			sData.nav_mesh_block_postion 			= <<-53.07, -1096.73, 25.50>>
			sData.nav_mesh_block_sizeXYZ			= <<1,3,2>>
			sData.nav_mesh_block_heading 			= -45
			sData.nav_mesh_block_active[BUILDINGSTATE_NORMAL] = FALSE 
			sData.nav_mesh_block_active[BUILDINGSTATE_DESTROYED] = TRUE 
			sData.nav_mesh_block_active[BUILDINGSTATE_CLEANUP] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_CAR_SHOWROOOM_RUBBLE3"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "carshowroom_broken"
			sData.name[BUILDINGSTATE_CLEANUP] 		= "carshowroom_boarded"
			sData.auto_cleanup						= FALSE
			sData.coords 							= << -59.7936, -1098.7841, 27.2612 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CAR_SHOWROOM_LOD_BOARD"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CAR_SHOWROOM_INTERIOR
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= "shr_int"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "fakeint"
			sData.auto_cleanup						= FALSE
			sData.coords 							= << -59.7936, -1098.7841, 27.2612 >>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CAR_SHOWROOM_INTERIOR"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS								// THIS IS NOW AN IPL
			sData.type 								= BUILDINGTYPE_IPL			//BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "DT1_03_Shutter"			//"DT1_03_carparkshutters"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""						//""
			sData.es_interior 						= ""						//"dt1_03_carpark"
			sData.coords 							= <<23.9346, -669.7552, 30.8853>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_BANK_CAR_PARK_SHUTTERS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_PILLBOX_HILL
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "Hospitaldoorsanim"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= "Hospitaldoorsfixed"
			sData.es_interior 						= "v_hospital"
			sData.auto_cleanup						= FALSE
			sData.coords 							= <<300.9423, -586.1784, 42.2919>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_PILLBOX_HILL"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "swap_clean_apt"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_mess_A"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_mess_B"
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_MESS_1"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_2
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_mess_C"
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_MESS_2"
			#ENDIF
		BREAK

		#IF NOT IS_JAPANESE_BUILD
			CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_SEX_TOYS
				sData.type 								= BUILDINGTYPE_ENTITY_SET
				sData.name[BUILDINGSTATE_NORMAL] 		= ""
				sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_sextoys_a"
				sData.name[BUILDINGSTATE_CLEANUP] 		= ""
				sData.es_interior 						= "v_trevors"
				sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
				#IF IS_DEBUG_BUILD
					sData.dbg_name = "ES_FLOYDS_APPARTMENT_SEX_TOYS"
				#ENDIF
			BREAK
		#ENDIF
		
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_wade_shit"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_SHIT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "swap_wade_sofa_A"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_SHIT_SOFA"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "layer_debra_pic"
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_PICTURE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_TORTURE_TOOLS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_torture"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_TORTURE_TOOLS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_SOFA
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "swap_sofa_A"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "swap_sofa_B"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_SOFA"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_WHISKY
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "layer_whiskey"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_WHISKY"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "swap_mrJam_A"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "swap_mrJam_B"
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_RASPBERRY_JAM"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "swap_mrJam_C"
			sData.es_interior 						= "v_trevors"
			sData.coords 							= <<-1157.1294, -1523.0276, 9.6327>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "vb_30_emissive"
			sData.name[BUILDINGSTATE_DESTROYED]		= "vb_30_murder"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<-1150.0391, -1521.7610, 9.6331>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_FLOYDS_APPARTMENT_CRIME_TAPE
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "vb_30_crimetape"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<-1150.0391, -1521.7610, 9.6331>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FLOYDS_APPARTMENT_CRIME_TAPE"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_SHERIFF_WINDOWS
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "sheriff_cap"
			sData.name[BUILDINGSTATE_DESTROYED]		= ""
			sData.auto_cleanup						= FALSE
			sData.coords							= <<1856.0288, 3682.9983, 33.2675>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SHERIFF_WINDOWS"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_SHERIFF_WINDOWS2
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "CS1_16_Sheriff_Cap"
			sData.name[BUILDINGSTATE_DESTROYED]		= ""
			sData.auto_cleanup						= FALSE
			sData.coords							= <<-440.5073, 6018.7661, 30.4900>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_SHERIFF_WINDOWS2"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_UFO
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "ufo"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<487.3100, 5588.3857, 793.0532>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_UFO"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_UFO_EYE
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "ufo_eye"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<487.3100, 5588.3857, 793.0532>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_UFO"
			#ENDIF
		BREAK
		
		
		CASE BUILDINGNAME_ES_FRANKLINS_CITY_SAVEHOUSE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "V_57_FranklinStuff"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_57_Franklin_LEFT"
			sData.es_interior 						= "v_franklins"
			sData.coords 							= <<-13.9623, -1440.6136, 30.1015>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_CITY_SAVEHOUSE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_CITY_SAVEHOUSE_BANDANA
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_57_GangBandana"
			sData.es_interior 						= "v_franklins"
			sData.coords 							= <<-13.9623, -1440.6136, 30.1015>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_CITY_SAVEHOUSE_BANDANA"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_CITY_SAVEHOUSE_KITBAG
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_57_Safari"
			sData.es_interior 						= "v_franklins"
			sData.coords 							= <<-13.9623, -1440.6136, 30.1015>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_ES_FRANKLINS_CITY_SAVEHOUSE_KITBAG"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_STRIPCLUB_TREVORS_MESS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "V_19_Trevor_Mess"
			sData.es_interior 						= "v_strip3"
			sData.coords 							= <<96.4811, -1291.2944, 28.2688>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_STRIPCLUB_TREVORS_MESS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_BUGSTAR_DOCKS_1
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= FALSE
			sData.coords 							= <<139.579529,-3092.962402,8.646310>>
			sData.scenario_block_minXYZ 			= <<139.579529,-3092.962402,8.646310>> - <<33.312500,24.000000,4.187500>>
			sData.scenario_block_maxXYZ 			= <<139.579529,-3092.962402,8.646310>> + <<33.312500,24.000000,4.187500>>
			sData.code_vehgen_min_coords			= <<139.579529,-3092.962402,8.646310>> - <<33.312500,24.000000,4.187500>>
			sData.code_vehgen_max_coords			= <<139.579529,-3092.962402,8.646310>> + <<33.312500,24.000000,4.187500>>
			sData.code_vehgen_block_active[BUILDINGSTATE_NORMAL] 	= FALSE
			sData.code_vehgen_block_active[BUILDINGSTATE_DESTROYED] = TRUE
			sData.code_vehgen_block_active[BUILDINGSTATE_CLEANUP] 	= TRUE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_BUGSTAR_DOCKS_1"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_BUGSTAR_DOCKS_2
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= FALSE
			sData.coords 							= <<203.778381,-3131.767090,7.041344>>
			sData.scenario_block_minXYZ 			= <<203.778381,-3131.767090,7.041344>> - <<4.875000,2.750000,2.562500>>
			sData.scenario_block_maxXYZ 			= <<203.778381,-3131.767090,7.041344>> + <<4.875000,2.750000,2.562500>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_BUGSTAR_DOCKS_2"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_BUGSTAR_DOCKS_3
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= FALSE
			sData.coords 							= <<144.770554,-2982.659424,7.952507>>
			sData.scenario_block_minXYZ 			= <<144.770554,-2982.659424,7.952507>> - <<5.312500,3.437500,3.125000>>
			sData.scenario_block_maxXYZ 			= <<144.770554,-2982.659424,7.952507>> + <<5.312500,3.437500,3.125000>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_BUGSTAR_DOCKS_3"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_FLOYDS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= TRUE
			sData.coords 							= <<-1154.964722,-1520.982666, 9.132731>>
			sData.scenario_block_minXYZ 			= <<-1154.964722,-1520.982666, 9.132731>>
			sData.scenario_block_maxXYZ 			= <<-1158.964722,-1524.982666,11.632731>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_FLOYDS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_KRIS_HOLTS
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= TRUE
			sData.coords 							= <<-1052.2039, 371.9537, 67.9140>>
			sData.scenario_block_minXYZ 			= <<-1052.2039, 371.9537, 67.9140>>
			sData.scenario_block_maxXYZ 			= <<-1048.0645, 368.0221, 70.9128>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_KRIS_HOLTS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_TREVORS_TRAILER
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= TRUE
			sData.coords 							= <<1954.9836, 3792.9910, 30.3086>>
			sData.scenario_block_minXYZ 			= <<1954.9836, 3792.9910, 30.3086>>
			sData.scenario_block_maxXYZ 			= <<1983.4500, 3830.7800, 36.2726>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_TREVORS_TRAILER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_SB_N1D_GOLFER
			sData.type 								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= ""
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.auto_cleanup						= TRUE
			sData.coords 							= <<-1122.2018, 48.5724, 51.4652>>
			sData.scenario_block_minXYZ 			= <<-1122.2018, 48.5724, 51.4652>>
			sData.scenario_block_maxXYZ 			= <<-1076.2333, 92.1041, 60.0617>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "SB_N1D_GOLFER_AREA"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MIC3_KORTZ_RENNOVATION
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "KorizTempWalls"
			sData.name[BUILDINGSTATE_CLEANUP]		= ""
			sData.auto_cleanup						= FALSE
			sData.coords							= <<-2199.1377, 223.4648, 181.1118>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MIC3_KORTZ_RENNOVATION"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MIC3_HELI_DEBRIS
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "mic3_chopper_debris"
			sData.name[BUILDINGSTATE_CLEANUP]		= ""
			sData.auto_cleanup						= FALSE
			sData.coords							= <<-2242.7847, 263.4779, 173.6154>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MIC3_HELI_DEBRIS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FIB5_UNDERWATER_GRATE
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "chemgrill_grp1"
			sData.name[BUILDINGSTATE_DESTROYED]		= ""
			sData.coords							= <<3832.9, 3665.5, -23.4>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB5_UNDERWATER_GRATE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_PLANE_CRASH_TRENCH
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "Plane_crash_trench"
			sData.name[BUILDINGSTATE_CLEANUP]		= ""
			sData.auto_cleanup						= FALSE
			sData.coords							= <<2814.7, 4758.5, 47.9>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_PLANE_CRASH_TRENCH"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_GOLF_FLAGS
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "golfflags"
			sData.name[BUILDINGSTATE_DESTROYED]		= ""
			sData.coords							= <<-1096.5055, 4.5754, 49.8103>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_GOLF_FLAGS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_YOGA_GAME
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "yogagame"
			sData.name[BUILDINGSTATE_DESTROYED]		= ""
			sData.coords							= <<-781.6566, 186.8937, 71.8352>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_YOGA_GAME"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARWASH_LONG
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "Carwash_with_spinners"
			sData.name[BUILDINGSTATE_DESTROYED]		= "Carwash_without_spinners"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<55.7,-1391.3,30.5>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARWASH_LONG"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARWASH_SHORT
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "KT_CarWash"
			sData.name[BUILDINGSTATE_DESTROYED]		= "KT_CarWash_NoBrush"
			sData.auto_cleanup						= FALSE
			sData.coords							= <<700.091, -933.641, 20.308>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARWASH_SHORT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_VB_0
			sData.type 								= BUILDINGTYPE_IPL
			sData.coords 							= <<-1096.3812, -836.1700, 36.6755>>
			sData.code_vehgen_min_coords			= sData.coords - <<15.0, 25.0, 25.0>>
			sData.code_vehgen_max_coords			= sData.coords + <<15.0, 25.0, 25.0>>
			sData.code_vehgen_block_active[BUILDINGSTATE_NORMAL] 	= TRUE
			sData.code_vehgen_block_active[BUILDINGSTATE_DESTROYED] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARGEN_BLOCK_POLMAV_VB_0"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_DT1_17
			sData.type 								= BUILDINGTYPE_IPL
			sData.coords 							= <<449.6558, -980.1375, 42.6918>>
			sData.code_vehgen_min_coords			= sData.coords - <<15.0, 25.0, 25.0>>
			sData.code_vehgen_max_coords			= sData.coords + <<15.0, 25.0, 25.0>>
			sData.code_vehgen_block_active[BUILDINGSTATE_NORMAL] 	= TRUE
			sData.code_vehgen_block_active[BUILDINGSTATE_DESTROYED] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARGEN_BLOCK_POLMAV_DT1_17"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_SC1_19
			sData.type 								= BUILDINGTYPE_IPL
			sData.coords 							= <<363.0175, -1598.0793, 35.9502>>
			sData.code_vehgen_min_coords			= sData.coords - <<15.0, 25.0, 25.0>>
			sData.code_vehgen_max_coords			= sData.coords + <<15.0, 25.0, 25.0>>
			sData.code_vehgen_block_active[BUILDINGSTATE_NORMAL] 	= TRUE
			sData.code_vehgen_block_active[BUILDINGSTATE_DESTROYED] = FALSE
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_CARGEN_BLOCK_POLMAV_SC1_19"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_MILBAS_MPGATES  //1361093
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "CS3_07_MPGates"
			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
			sData.coords							= <<-1601.4241, 2808.2126, 16.2598>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_MILBAS_MPGATES"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BIG_SCORE_HOLE_COVER
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "DT1_03_Gr_Closed"
			sData.name[BUILDINGSTATE_DESTROYED]		= ""
			sData.coords							= <<23.7318, -647.2123, 37.9549>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_BIG_SCORE_HOLE_COVER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_BIG_SCORE_BANK
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "FINBANK"
			sData.coords							=  <<12.9689, -648.4698, 9.7693>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "BIG_SCORE_BANK_SHUTTER"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_RCM_DREYFUSS_MAT
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "PAPER1_RCM_ALT"
			sData.name[BUILDINGSTATE_CLEANUP]		= "PAPER1_RCM"
			sData.coords							= <<-1459.1273, 486.1281, 115.2016>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_RCM_DREYFUSS_MAT"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_STADIUM_INTERIOR
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "SP1_10_fake_interior"
			sData.name[BUILDINGSTATE_DESTROYED]		= "SP1_10_real_interior"
			sData.coords							=<<-248.4916, -2010.5090, 34.5743>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_STADIUM_INTERIOR"
			#ENDIF
		BREAK
		
			
		CASE BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "facelobbyfake"
			sData.name[BUILDINGSTATE_DESTROYED]		= "facelobby"
			sData.coords							= <<-1081.3467, -263.1502, 38.7152>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_INVADER_OFFICE_INTERIOR"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_FIB_ROOF
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= "atriumglstatic"
			sData.name[BUILDINGSTATE_DESTROYED]		= "atriumglmission"
			sData.name[BUILDINGSTATE_CLEANUP]		= "atriumglcut"
			sData.coords							= <<136.1795, -750.7010, 262.0516>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FIB_ROOF"
			#ENDIF
		BREAK
		
		CASE BUILDINGNAME_IPL_COUNTRY_RACETRACK
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "racetrack01"
			sData.coords							= <<2096.0, 3168.7, 42.9>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_COUNTRY_RACETRACK"
			#ENDIF
		BREAK
		
//		CASE BUILDINGNAME_IPL_MPAPT_EXTERIOR  
//			sData.type								= BUILDINGTYPE_IPL
//			sData.name[BUILDINGSTATE_NORMAL]		= "apt_high1_sp_door"
//			sData.name[BUILDINGSTATE_DESTROYED]		= ""
//			sData.name[BUILDINGSTATE_CLEANUP] 		= ""
//			sData.coords							= <<-793.0,305.6,85.7>>
//			#IF IS_DEBUG_BUILD
//				sData.dbg_name = "BUILDINGNAME_IPL_MPAPT_EXTERIOR"
//			#ENDIF
//		BREAK
//		DEFAULT
//			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("GET_BUILDING_DATA() - Building enum missing. Tell Kenneth R.")
//				CPRINTLN(DEBUG_BUILDING, "GET_BUILDING_DATA() - Building enum missing. Tell Kenneth R.")
//			#ENDIF
//			
//			RETURN FALSE
//		BREAK
	ENDSWITCH
	
	/// We've run out of switch statements again.
	/// I've started another list, and moved Franklin's hills setup into it, as I need to keep them all together,
	/// Also as there's a Japanese only item in the above list I wanted to make sure we have enough space in the above switch statement
	/// in the Japanese build. --Steve R. 26/07/13
	
	SWITCH eName
	
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_SHOWHOME
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "showhome_only"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_SAVEHOUSE_SHOWHOME"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_UNPACKING
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "franklin_unpacking"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_SAVEHOUSE_UNPACKING"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_SETTLED
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "franklin_settled"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_SAVEHOUSE_SETTLED"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_100_PERCENT_SHIRT
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "progress_tshirt"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_100_PERCENT_SHIRT"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_BONG_AND_WINE
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "bong_and_wine"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_BONG_AND_WINE"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_CABLE_CAR_FLYER
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "progress_flyer"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_CABLE_CAR_FLYER"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_TUXEDO
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= ""
			sData.name[BUILDINGSTATE_DESTROYED] 	= "progress_tux"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_ES_FRANKLINS_HILLS_TUXEDO"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_ES_FRANKLINS_HILLS_PATIO_DOORS
			sData.type 								= BUILDINGTYPE_ENTITY_SET
			sData.name[BUILDINGSTATE_NORMAL] 		= "locked"
			sData.name[BUILDINGSTATE_DESTROYED] 	= "unlocked"
			sData.es_interior 						= "v_franklinshouse"
			sData.coords 							= <<7.0256, 537.3075, 175.0281>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "ES_FRANKLINS_HILLS_PATIO_DOORS"
			#ENDIF
		BREAK
		CASE BUILDINGNAME_IPL_FRANKLINS_HILLS_CHOP_DOGHOUSE
			sData.type								= BUILDINGTYPE_IPL
			sData.name[BUILDINGSTATE_NORMAL]		= ""
			sData.name[BUILDINGSTATE_DESTROYED]		= "chophillskennel"
			sData.coords							= <<19.0568, 536.4818, 169.6277>>
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "IPL_FRANKLIN_HILLS_CHOP_DOGHOUSE"
			#ENDIF
		BREAK
		
//		DEFAULT
//			
//			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("GET_BUILDING_DATA() - Building enum missing. Tell Kenneth R.")
//				CPRINTLN(DEBUG_BUILDING, "GET_BUILDING_DATA() - Building enum missing. Tell Kenneth R.")
//			#ENDIF
//			
//			RETURN FALSE
//		BREAK
	
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
FUNC BOOL SHOULD_DOORSTATE_LOCK_FOR_CURRENT_VEH(DOOR_NAME_ENUM eDoorName)
	if eDoorName = DOORNAME_M_MANSION_G1
	or eDoorName = DOORNAME_F_HOUSE_SC_G
	or eDoorName = DOORNAME_T_TRAILER_CS_G
		if not IS_PED_INJURED(PLAYER_PED_ID())
			if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
				MODEL_NAMES eTempModel = GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),true)))				
				SWITCH eTempModel
					case UTILLITRUCK
					case MONSTER
						RETURN TRUE
					break
					
				ENDSWITCH				
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC
/// PURPOSE: Determines if a savehouse door should be locked or unlocked
///    		 by checking the savehouse state and current player character
#if USE_CLF_DLC
FUNC DOOR_STATE_ENUM GET_DOOR_STATE_FOR_SAVEHOUSECLF(DOOR_NAME_ENUM eDoorName)
	enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
	
	//Additional door state check added if vehicle is not sutible for garage  -CV
	if SHOULD_DOORSTATE_LOCK_FOR_CURRENT_VEH(eDoorName)
		RETURN DOORSTATE_LOCKED
	ENDIF
	
	SWITCH eDoorName
		CASE DOORNAME_M_MANSION_F_L
		CASE DOORNAME_M_MANSION_F_R
		CASE DOORNAME_M_MANSION_G1
		CASE DOORNAME_M_MANSION_R_L1
		CASE DOORNAME_M_MANSION_R_R1
		CASE DOORNAME_M_MANSION_R_L2
		CASE DOORNAME_M_MANSION_R_R2
		CASE DOORNAME_M_MANSION_GA_SM
		CASE DOORNAME_M_MANSION_BW
			IF eCurrentPed = CHAR_MICHAEL
				
					IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[SAVEHOUSEclf_MICHAEL_BH], REPAWN_FLAG_AVAILABLE_BIT)
						RETURN DOORSTATE_UNLOCKED
					ENDIF				
				
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSEclf_MICHAEL_BH], ENUM_TO_INT(eCurrentPed))
					IF eDoorName != DOORNAME_M_MANSION_G1 // If the player is Franklin or Trevor, and this is Michael's garage door, don't open it (B*1382329)
						//CPRINTLN(DEBUG_DOOR, "Michael safehouse g_iSavehouseOpenForPedsBitset IS set for door ", sDoorData.dbg_name)
						RETURN DOORSTATE_UNLOCKED
					ELSE
						//CPRINTLN(DEBUG_DOOR, "Michael safehouse g_iSavehouseOpenForPedsBitset IS set but door is ", sDoorData.dbg_name)
						RETURN DOORSTATE_LOCKED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_F_HOUSE_SC_F
		CASE DOORNAME_F_HOUSE_SC_B
		CASE DOORNAME_F_HOUSE_SC_G
			IF eCurrentPed = CHAR_FRANKLIN	
				IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[SAVEHOUSEclf_FRANKLIN_SC], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF	
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSEclf_FRANKLIN_SC], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_F_HOUSE_VH_F
			IF eCurrentPed = CHAR_FRANKLIN				
				IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[SAVEHOUSEclf_FRANKLIN_VH], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF							
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSEclf_FRANKLIN_VH], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_T_TRAILER_CS
		CASE DOORNAME_T_TRAILER_CS_G
			IF eCurrentPed = CHAR_TREVOR
								
				IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[SAVEHOUSEclf_TREVOR_CS], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF				
				
				IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
					IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSEclf_TREVOR_CS], ENUM_TO_INT(eCurrentPed))
						RETURN DOORSTATE_UNLOCKED
					ENDIF
				ENDIF
			ELIF eCurrentPed = CHAR_MICHAEL
				
				
					IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[SAVEHOUSEclf_MICHAEL_CS], REPAWN_FLAG_AVAILABLE_BIT)
						RETURN DOORSTATE_UNLOCKED
					ENDIF				
				
				IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
					IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSEclf_MICHAEL_CS], ENUM_TO_INT(eCurrentPed))
						RETURN DOORSTATE_UNLOCKED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_T_APARTMENT_VB
			IF eCurrentPed = CHAR_TREVOR		
				IF IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[SAVEHOUSEclf_TREVOR_VB], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF			
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSEclf_TREVOR_VB], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				DOOR_DATA_STRUCT sDoorData
				sDoorData = GET_DOOR_DATA(eDoorName)
				SCRIPT_ASSERT("GET_DOOR_STATE_FOR_SAVEHOUSE - Missing door enum. Tell Kenneth R.")
				CPRINTLN(DEBUG_DOOR, "GET_DOOR_STATE_FOR_SAVEHOUSE - Missiing door enum: ", sDoorData.dbg_name, ".")
			#ENDIF
			
			// Return unlocked so we can progress
			RETURN DOORSTATE_UNLOCKED
		BREAK
	ENDSWITCH
	
	// Conditions failed
	RETURN DOORSTATE_LOCKED
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC DOOR_STATE_ENUM GET_DOOR_STATE_FOR_SAVEHOUSENRM(DOOR_NAME_ENUM eDoorName)

	enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
	//Additional door state check added if vehicle is not sutible for garage  -CV
	if SHOULD_DOORSTATE_LOCK_FOR_CURRENT_VEH(eDoorName)
		RETURN DOORSTATE_LOCKED
	ENDIF
	
	SWITCH eDoorName
		CASE DOORNAME_M_MANSION_F_L
		CASE DOORNAME_M_MANSION_F_R
		CASE DOORNAME_M_MANSION_G1
		CASE DOORNAME_M_MANSION_R_L1
		CASE DOORNAME_M_MANSION_R_R1
		CASE DOORNAME_M_MANSION_R_L2
		CASE DOORNAME_M_MANSION_R_R2
		CASE DOORNAME_M_MANSION_GA_SM
		CASE DOORNAME_M_MANSION_BW
			IF eCurrentPed = CHAR_MICHAEL				
				IF IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[SAVEHOUSENRM_BH], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF	
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSENRM_BH], ENUM_TO_INT(eCurrentPed))
					IF eDoorName != DOORNAME_M_MANSION_G1 // If the player is Franklin or Trevor, and this is Michael's garage door, don't open it (B*1382329)
						//CPRINTLN(DEBUG_DOOR, "Michael safehouse g_iSavehouseOpenForPedsBitset IS set for door ", sDoorData.dbg_name)
						RETURN DOORSTATE_UNLOCKED
					ELSE
						//CPRINTLN(DEBUG_DOOR, "Michael safehouse g_iSavehouseOpenForPedsBitset IS set but door is ", sDoorData.dbg_name)
						RETURN DOORSTATE_LOCKED
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		DEFAULT
			#IF IS_DEBUG_BUILD
				DOOR_DATA_STRUCT sDoorData
				sDoorData = GET_DOOR_DATA(eDoorName)
				SCRIPT_ASSERT("GET_DOOR_STATE_FOR_SAVEHOUSE - Missing door enum. Tell Kenneth R.")
				CPRINTLN(DEBUG_DOOR, "GET_DOOR_STATE_FOR_SAVEHOUSE - Missiing door enum: ", sDoorData.dbg_name, ".")
			#ENDIF
			
			// Return unlocked so we can progress
			RETURN DOORSTATE_UNLOCKED
		BREAK
	ENDSWITCH
	
	// Conditions failed
	RETURN DOORSTATE_LOCKED
ENDFUNC
#endif
FUNC DOOR_STATE_ENUM GET_DOOR_STATE_FOR_SAVEHOUSE(DOOR_NAME_ENUM eDoorName)
	#if USE_CLF_DLC
		return GET_DOOR_STATE_FOR_SAVEHOUSECLF(eDoorName)
	#endif
	#if USE_NRM_DLC
		return GET_DOOR_STATE_FOR_SAVEHOUSENRM(eDoorName)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		
	enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
	
	//Additional door state check added if vehicle is not sutible for garage  -CV
	if SHOULD_DOORSTATE_LOCK_FOR_CURRENT_VEH(eDoorName)
		RETURN DOORSTATE_LOCKED
	ENDIF
	
	#IF USE_TU_CHANGES
		IF eDoorName = DOORNAME_F_HOUSE_SC_G
			IF eCurrentPed = CHAR_FRANKLIN				
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_FRANKLIN_SC], REPAWN_FLAG_AVAILABLE_BIT)
				OR IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_FRANKLIN_VH], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_FRANKLIN_SC], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	SWITCH eDoorName
		CASE DOORNAME_M_MANSION_F_L
		CASE DOORNAME_M_MANSION_F_R
		CASE DOORNAME_M_MANSION_G1
		CASE DOORNAME_M_MANSION_R_L1
		CASE DOORNAME_M_MANSION_R_R1
		CASE DOORNAME_M_MANSION_R_L2
		CASE DOORNAME_M_MANSION_R_R2
		CASE DOORNAME_M_MANSION_GA_SM
		CASE DOORNAME_M_MANSION_BW
			IF eCurrentPed = CHAR_MICHAEL				
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_MICHAEL_BH], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_MICHAEL_BH], ENUM_TO_INT(eCurrentPed))
					IF eDoorName != DOORNAME_M_MANSION_G1 // If the player is Franklin or Trevor, and this is Michael's garage door, don't open it (B*1382329)
						RETURN DOORSTATE_UNLOCKED
					ELSE
						RETURN DOORSTATE_LOCKED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_F_HOUSE_SC_F
		CASE DOORNAME_F_HOUSE_SC_B
		CASE DOORNAME_F_HOUSE_SC_G
			IF eCurrentPed = CHAR_FRANKLIN				
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_FRANKLIN_SC], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_FRANKLIN_SC], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_F_HOUSE_VH_F
			IF eCurrentPed = CHAR_FRANKLIN				
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_FRANKLIN_VH], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF	
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_FRANKLIN_VH], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_T_TRAILER_CS
		CASE DOORNAME_T_TRAILER_CS_G
			IF eCurrentPed = CHAR_TREVOR
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_TREVOR_CS], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF
				IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
					IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_TREVOR_CS], ENUM_TO_INT(eCurrentPed))
						RETURN DOORSTATE_UNLOCKED
					ENDIF
				ENDIF
			ELIF eCurrentPed = CHAR_MICHAEL
				
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_MICHAEL_CS], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF
				IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
					IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_MICHAEL_CS], ENUM_TO_INT(eCurrentPed))
						RETURN DOORSTATE_UNLOCKED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_T_APARTMENT_VB
			IF eCurrentPed = CHAR_TREVOR				
				IF IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[SAVEHOUSE_TREVOR_VB], REPAWN_FLAG_AVAILABLE_BIT)
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				IF IS_BIT_SET(g_iSavehouseOpenForPedsBitset[SAVEHOUSE_TREVOR_VB], ENUM_TO_INT(eCurrentPed))
					RETURN DOORSTATE_UNLOCKED
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				DOOR_DATA_STRUCT sDoorData
				sDoorData = GET_DOOR_DATA(eDoorName)
				SCRIPT_ASSERT("GET_DOOR_STATE_FOR_SAVEHOUSE - Missing door enum. Tell Kenneth R.")
				CPRINTLN(DEBUG_DOOR, "GET_DOOR_STATE_FOR_SAVEHOUSE - Missiing door enum: ", sDoorData.dbg_name, ".")
			#ENDIF
			
			// Return unlocked so we can progress
			RETURN DOORSTATE_UNLOCKED
		BREAK
	ENDSWITCH
	
	// Conditions failed
	RETURN DOORSTATE_LOCKED
	#endif
	#endif
ENDFUNC
/// PURPOSE: Returns TRUE if the specified door is locked
///    NOTE: The door has to be streamed in, in order to return a valid result
FUNC BOOL IS_DOOR_LOCKED(DOOR_NAME_ENUM eName)
	DOOR_DATA_STRUCT sData
	
	sData = GET_DOOR_DATA(eName)
	DOOR_STATE_ENUM state
	state = DOOR_SYSTEM_GET_DOOR_STATE(sData.doorHash)
	RETURN state = DOORSTATE_LOCKED OR state = DOORSTATE_FORCE_LOCKED_THIS_FRAME OR state = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
ENDFUNC

FUNC BOOL IS_STUDIO_OPEN_FOR_CURRENT_PLAYER_PED()

	// If the player has a wanted level, don't open the gates for the player
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()

		CASE CHAR_MICHAEL
			#if USE_CLF_DLC
				RETURN TRUE								
			#endif
			#if USE_NRM_DLC
				RETURN TRUE			
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MOVIE_STUDIO_OPEN]
					RETURN TRUE
				ENDIF		
			#endif	
			#endif
		BREAK

		CASE CHAR_FRANKLIN			
			#if USE_CLF_DLC
				RETURN TRUE							
			#endif
			#if USE_NRM_DLC
				RETURN TRUE	
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN]
					RETURN TRUE
				ENDIF		
			#endif	
			#endif
		BREAK
		
		CASE CHAR_TREVOR			
			#if USE_CLF_DLC
				RETURN TRUE		
			#endif
			#if USE_NRM_DLC 
				RETURN TRUE			
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MOVIE_STUDIO_OPEN]
					RETURN TRUE
				ENDIF		
			#endif	
			#endif
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the door needs to have its state updated just to the current time of day (i.e. locking for being closed)
/// PARAMS:
///    eDoorName - The door we're checking
PROC UPDATE_DOORS_FOR_TOD(DOOR_NAME_ENUM eDoorName, DOOR_DATA_STRUCT &sData)

	// Passing this by reference to cut down on processing.
	//DOOR_DATA_STRUCT sData = GET_DOOR_DATA(eDoorName)	
	
	IF NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
		EXIT
	ENDIF
	
	TIMEOFDAY eCurrentTOD = GET_CURRENT_TIMEOFDAY()
	INT iCurrentHour = GET_TIMEOFDAY_HOUR(eCurrentTOD)
	
	SWITCH eDoorName
	
		CASE DOORNAME_BANK_GRAPESEED_L
			FALLTHRU
		CASE DOORNAME_BANK_GRAPESEED_R
			FALLTHRU
		CASE DOORNAME_BANK_FLEECA_HWAY_L
			FALLTHRU
		CASE DOORNAME_BANK_FLEECA_HWAY_R
			IF IS_DOOR_LOCKED(eDoorName)
				IF iCurrentHour < 19
					IF iCurrentHour >= 7	
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif	
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ELSE
					EXIT // The doors are locked and it's during closing time, do nothing
				ENDIF
			ELSE
				IF iCurrentHour >= 19
					// If the doors need to lock, wait until the player is >=12m away, to prevent them getting locked inside!
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 12
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ELIF iCurrentHour < 7
					// If the doors need to lock, wait until the player is >=12m away, to prevent them getting locked inside!
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 12
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE DOORNAME_BANK_FLEECA_VW_L
			FALLTHRU
		CASE DOORNAME_BANK_FLEECA_VW_R
			FALLTHRU
		CASE DOORNAME_BANK_FLEECA_BURTON_L
			FALLTHRU
		CASE DOORNAME_BANK_FLEECA_BURTON_R
			IF IS_DOOR_LOCKED(eDoorName)
				IF iCurrentHour < 18
					IF iCurrentHour >= 7	
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ELSE
					EXIT // The doors are locked and it's during closing time, do nothing
				ENDIF
			ELSE
				IF iCurrentHour >= 18
					// If the doors need to lock, wait until the player is >=12m away, to prevent them getting locked inside!
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 12
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ELIF iCurrentHour < 7
					// If the doors need to lock, wait until the player is >=12m away, to prevent them getting locked inside!
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 12						
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ENDIF
			ENDIF
		BREAK
			
		CASE DOORNAME_HEIST_JEWELERS_L
			FALLTHRU
		CASE DOORNAME_HEIST_JEWELERS_R
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF IS_DOOR_LOCKED(eDoorName)			
					//CPRINTLN(DEBUG_AMBIENT,  "Trying to check jewelry store door (locked) ", eDoorName)
					// Don't touch the door if any Jewelry heist missions are running!! 
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jewelry_heist")) = 0
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jewelry_setup1")) = 0
					AND NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_FINISHED_JEWEL]
						IF iCurrentHour < 21
							IF iCurrentHour >= 7	
								g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
								DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
								//CPRINTLN(DEBUG_DOOR,  "Unlocking jewelry store door ", eDoorName)
							ENDIF
						ELSE
							EXIT // The doors are locked and it's during closing time, do nothing
						ENDIF
					ELSE
						IF NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_FINISHED_JEWEL] // Definitely do not unlock the goddamn doors
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
							//CPRINTLN(DEBUG_DOOR, "Force jewelry store doors open", eDoorName)
						ENDIF
					ENDIF
				ELSE
					IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_FINISHED_JEWEL]
						g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						//CPRINTLN(DEBUG_DOOR,  "Jewelry store heist done, setting door as locked ", eDoorName)
					ELSE
						// Don't touch the door if any Jewelry heist missions are running!!
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jewelry_heist")) = 0
						AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jewelry_setup1")) = 0
							IF iCurrentHour >= 21
								// If the doors need to lock, wait until the player is >=18m away, to prevent them getting locked inside!
								IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 18
									g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
									DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
									//CPRINTLN(DEBUG_DOOR,  "Locking jewelry store door ", eDoorName)
								ENDIF
							ELIF iCurrentHour < 7
								// If the doors need to lock, wait until the player is >=18m away, to prevent them getting locked inside!
								IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 18
									g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
									DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
									//CPRINTLN(DEBUG_DOOR,  "Locking jewelry store door ", eDoorName)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			#endif
			#endif
		BREAK
		
		CASE DOORNAME_BANK_VINEWOOD_CORNER_L
			FALLTHRU
		CASE DOORNAME_BANK_VINEWOOD_CORNER_R
			FALLTHRU
		CASE DOORNAME_BANK_VINEWOOD_FRONT_L
			FALLTHRU
		CASE DOORNAME_BANK_VINEWOOD_FRONT_R
			IF IS_DOOR_LOCKED(eDoorName)
				IF iCurrentHour < 20
					IF iCurrentHour >= 9	
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not use_NRM_dlc
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ELSE
					EXIT // The doors are locked and it's during closing time, do nothing
				ENDIF
			ELSE
				IF iCurrentHour >= 20
					// If the doors need to lock, wait until the player is >=40m away, to prevent them getting locked inside!
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 40
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ELIF iCurrentHour < 9
					// If the doors need to lock, wait until the player is >=40m away, to prevent them getting locked inside!
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) >= 40	
						#if USE_CLF_DLC
							g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if USE_NRM_DLC
							g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						#endif
						#endif
					ENDIF
				ENDIF	
			ENDIF
		BREAK
		
		CASE DOORNAME_BANK_VINEWOOD_OFFICE
			FALLTHRU
		CASE DOORNAME_BANK_VINEWOOD_TELLER
			// These doors should be locked permanently in SP
			IF NOT IS_DOOR_LOCKED(eDoorName)
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if USE_NRM_DLC
					g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#endif
			ENDIF
		BREAK
		
		CASE DOORNAME_VONCRAST_1_L
			FALLTHRU
		CASE DOORNAME_VONCRAST_1_R
			FALLTHRU
		CASE DOORNAME_VONCRAST_2_L
			FALLTHRU
		CASE DOORNAME_VONCRAST_2_R
			FALLTHRU
		CASE DOORNAME_VONCRAST_3_L
			FALLTHRU
		CASE DOORNAME_VONCRAST_3_R
			IF NOT IS_DOOR_LOCKED(eDoorName)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("assassin_valet")) = 0
					#if USE_CLF_DLC
						g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					#endif
					#if USE_NRM_DLC
						g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					#endif
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					#endif
					#endif
				ENDIF
			ELSE // Make sure the doors are unlocked if the Assassinate hotel script is running
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("assassin_valet")) > 0
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_UNLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_NOSE_REAR_L
			FALLTHRU
		CASE DOORNAME_NOSE_REAR_R
			// If Agency 3B is running, don't touch this door, let the mission handle it instead
			// Otherwise, lock it permanently
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_HEIST_AGENCY_3B].scriptHash) = 0
					IF NOT IS_DOOR_LOCKED(eDoorName)
						g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					ENDIF
				ENDIF
			#endif
			#endif
		BREAK
		
		CASE DOORNAME_OMEGA_SHED_L
			FALLTHRU
		CASE DOORNAME_OMEGA_SHED_R
			// If Omega 2 is running, don't touch this door, let the mission handle it instead
			// Otherwise, lock it permanently
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("omega2")) = 0
				IF NOT IS_DOOR_LOCKED(eDoorName)
					#if USE_CLF_DLC
						g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					#endif
					#if USE_NRM_DLC
						g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					#endif
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					#endif			
					#endif
				ENDIF
			ENDIF
		BREAK
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			CASE DOORNAME_FOUNDRY_B_01
			CASE DOORNAME_FOUNDRY_B_02
			CASE DOORNAME_FOUNDRY_T_01			
				IF NOT IS_DOOR_LOCKED(eDoorName)
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_FINALE_C1].scriptHash) = 0								
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				ELSE
					EXIT
				ENDIF
			BREAK
		
		
			CASE DOORNAME_RECYCLING_PLANT_F_L
			CASE DOORNAME_RECYCLING_PLANT_F_R
			CASE DOORNAME_RECYCLING_PLANT_R_L
			CASE DOORNAME_RECYCLING_PLANT_R_R
				IF NOT IS_DOOR_LOCKED(eDoorName)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_LAMAR].scriptHash) = 0					
						g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)					
					ENDIF
				ELSE
					EXIT
				ENDIF
			BREAK
		#endif
		#endif
		
		CASE DOORNAME_ARMYBASE_LIFT_L
		CASE DOORNAME_ARMYBASE_LIFT_R
			IF NOT IS_DOOR_LOCKED(eDoorName)
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if USE_NRM_DLC
					g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if not USE_CLF_DLC
				#if not use_NRM_dlc
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif	
				#endif
			ENDIF
		BREAK
		
		#if not USE_CLF_DLC
		#if not use_NRM_dlc
			CASE DOORNAME_CHICKENFACTORY_EXT_GATE
				IF NOT IS_DOOR_LOCKED(eDoorName)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_HEIST_RURAL_2].scriptHash) > 0
						g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
					ENDIF
				ELSE
					EXIT
				ENDIF
			BREAK
		#endif
		#endif
		
		CASE DOORNAME_RON_FRONTDOOR
			IF NOT IS_DOOR_LOCKED(eDoorName)
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if USE_NRM_DLC
					g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if not USE_CLF_DLC
				#if not use_NRM_dlc
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif	
				#endif
			ENDIF
		BREAK
		
#if not USE_CLF_DLC
#if not use_NRM_dlc
		CASE DOORNAME_CARSTEAL_GARAGE_S
			IF NOT IS_DOOR_LOCKED(eDoorName)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_CARSTEAL_1].scriptHash) = 0
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_CARSTEAL_3].scriptHash) = 0
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				ENDIF
			ELSE
				EXIT
			ENDIF
		BREAK
		
		CASE DOORNAME_ABBATOIR_FRONT_L
		CASE DOORNAME_ABBATOIR_FRONT_R
			IF NOT IS_DOOR_LOCKED(eDoorName)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_MICHAEL_2].scriptHash) = 0
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
						
				ENDIF
			ELSE
				EXIT
			ENDIF
		BREAK
		
		CASE DOORNAME_NOSE_HELIPAD
			// If Agency 3B is running, don't touch this door, let the mission handle it instead
			// Otherwise, lock it permanently
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_HEIST_AGENCY_3B].scriptHash) = 0
				IF NOT IS_DOOR_LOCKED(eDoorName)
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)						
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_QING_L
		CASE DOORNAME_KORTZ_QING_R
		CASE DOORNAME_KORTZ_QINGALT_R
		CASE DOORNAME_KORTZ_QINGALT_L
		CASE DOORNAME_KORTZ_TALES_L
		CASE DOORNAME_KORTZ_TALES_R
		CASE DOORNAME_KORTZ_RICHES_L
		CASE DOORNAME_KORTZ_RICHES_R
			// If Michael 3 is running, don't touch these doors, let the mission handle it instead
			// Otherwise, lock them permanently
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_MICHAEL_3].scriptHash) = 0
				IF NOT IS_DOOR_LOCKED(eDoorName)
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE DOORNAME_CHEMICAL_FACTORY_L
		CASE DOORNAME_CHEMICAL_FACTORY_R
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sMissionStaticData[SP_MISSION_FBI_5].scriptHash) = 0
				IF NOT IS_DOOR_LOCKED(eDoorName)
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)						
				ENDIF
			ENDIF
		BREAK
#endif
#endif

		CASE DOORNAME_GARAGE_JETSTEAL
			// This garage was used for the old jet-stealing mission that got cut - locked permanently in script instead of making a late map change
			IF NOT IS_DOOR_LOCKED(eDoorName)
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if USE_NRM_dlc
					g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if not USE_CLF_DLC
				#if not use_NRM_dlc
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif	
				#endif
			ENDIF
		BREAK
		
		CASE DOORNAME_EPSILON_GATE_L
		CASE DOORNAME_EPSILON_GATE_R
			IF NOT IS_DOOR_LOCKED(eDoorName)
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if USE_NRM_DLC
					g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if not USE_CLF_DLC
				#if not use_NRM_dlc
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif	
				#endif
			ENDIF
		BREAK
		
		CASE DOORNAME_KORTZ_FRONTL_L
		CASE DOORNAME_KORTZ_FRONTL_R
		CASE DOORNAME_KORTZ_FRONTR_L
		CASE DOORNAME_KORTZ_FRONTR_R
			IF IS_DOOR_LOCKED(eDoorName)
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName] = DOORSTATE_unLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if USE_NRM_DLC
					g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName] = DOORSTATE_unLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					g_savedGlobals.sBuildingData.eDoorState[eDoorName] = DOORSTATE_unLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, g_savedGlobals.sBuildingData.eDoorState[eDoorName], DEFAULT, TRUE)
				#endif	
				#endif
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE: Sets the door locks if valid conditions are met
PROC PERFORM_DOOR_LOCK(DOOR_NAME_ENUM eDoorName)

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_PLAYING()
			CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Cutscene is playing.")
		ENDIF
	#ENDIF
	
	IF NOT USE_SP_BUILDING_CONTROLLER_DATA() // Do not run in multiplayer
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	DOOR_DATA_STRUCT sData = GET_DOOR_DATA(eDoorName)
	
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(DOORFLAG_IS_AN_UPDATE_DOOR))
		UPDATE_DOORS_FOR_TOD(eDoorName, sData)
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sData.doorHash)
		IF IS_CUTSCENE_PLAYING()
			EXIT
		ENDIF
	ENDIF
	
	BOOl bPerformUpdate = FALSE
	BOOL bUpdatePerformed = FALSE
	DOOR_STATE_ENUM eIntendedState
	
	FLOAT fDistanceToDoor = GET_DISTANCE_BETWEEN_COORDS(sData.coords, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	// Clear the override state if the player is no longer in area
	IF IS_BIT_SET(g_iOverrideDoorStateBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32)
	AND g_eOverrideDoorState[eDoorName] = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
	AND fDistanceToDoor > VEHICLE_GEN_CLEANUP_RANGE
		CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Clearing override state for door: ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
		CLEAR_BIT(g_iOverrideDoorStateBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32)
		g_iOverrideDoorStateCounter[eDoorName] = 0
	ENDIF
	
	// Force the door to unlock if player is in the interior and the initial unlock bit is set.
	// Wait for startup positioning to finish before doing this check.
	#if USE_CLF_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_pos_CLF")) = 0		
	#endif
	#if USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_pos_NRM")) = 0		
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) = 0
	#endif
	#endif	
			IF IS_BIT_SET(g_iInitialDoorOverrideBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32)
				IF fDistanceToDoor < 25.0
					INTERIOR_INSTANCE_INDEX iPlayerInterior, iDoorInterior
					
					//Shop controller possibly not running yet. Grab interior manually.
					IF g_sShopSettings.playerInterior = NULL
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							g_sShopSettings.playerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					iPlayerInterior = g_sShopSettings.playerInterior // just use the shop instance as interior commands are expensive.
					iDoorInterior = GET_INTERIOR_AT_COORDS(sData.coords)

					IF (iPlayerInterior = iDoorInterior) AND (iPlayerInterior != NULL)
						// Force the door to unlock for this frame.
						SET_BIT(g_iOverrideDoorStateBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32)
						g_eOverrideDoorState[eDoorName] = DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
						SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(eDoorName)/32], (ENUM_TO_INT(eDoorName)%32))
						CPRINTLN(DEBUG_DOOR, "Forcing door to unlock on startup - ", sData.dbg_name, ".")
					ENDIF
				ENDIF
				CLEAR_BIT(g_iInitialDoorOverrideBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32)
			ENDIF
	#if USE_CLF_DLC
		ENDIF
	#endif
	#if USE_NRM_DLC
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		ENDIF
	#endif
	#endif	
	
	// [1] Determine what state the door should be in
	IF IS_BIT_SET(g_iOverrideDoorStateBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32)
		eIntendedState = g_eOverrideDoorState[eDoorName]
	ELSE
		// Treat savehouse doors separately
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(DOORFLAG_IS_SAVEHOUSE_DOOR))
			#if USE_CLF_DLC
				IF g_savedGlobalsClifford.sFlow.isGameflowActive
					eIntendedState = GET_DOOR_STATE_FOR_SAVEHOUSE(eDoorName)
				ELSE
					eIntendedState = DOORSTATE_UNLOCKED // Always unlocked in debug
				ENDIF
			#endif
			#if USE_NRM_DLC
				IF g_savedGlobalsnorman.sFlow.isGameflowActive
					eIntendedState = GET_DOOR_STATE_FOR_SAVEHOUSE(eDoorName)
				ELSE
					eIntendedState = DOORSTATE_UNLOCKED // Always unlocked in debug
				ENDIF
			#endif
			#if NOT USE_SP_DLC
				IF g_savedGlobals.sFlow.isGameflowActive
					eIntendedState = GET_DOOR_STATE_FOR_SAVEHOUSE(eDoorName)
				ELSE
					eIntendedState = DOORSTATE_UNLOCKED // Always unlocked in debug
				ENDIF				
			#endif
			
			// Always unlock safehouses when playing as none player characters in Director Mode.
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				eIntendedState = DOORSTATE_UNLOCKED
			ENDIF
			
		ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(DOORFLAG_IS_STUDIO_GATE)) AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ambient_solomon")) = 0)
			IF IS_STUDIO_OPEN_FOR_CURRENT_PLAYER_PED()
				eIntendedState = DOORSTATE_UNLOCKED
			ELSE
				eIntendedState = DOORSTATE_LOCKED
			ENDIF
		ELSE
			#if USE_CLF_DLC
				eIntendedState = g_savedGlobalsClifford.sBuildingData.eDoorState[eDoorName]
			#endif
			#if USE_NRM_DLC
				eIntendedState = g_savedGlobalsnorman.sBuildingData.eDoorState[eDoorName]
			#endif
			#if not USE_CLF_DLC
			#if not use_NRM_dlc
				eIntendedState = g_savedGlobals.sBuildingData.eDoorState[eDoorName]
			#endif	
			#endif
		ENDIF
		
	ENDIF
	
	// [2] Update the door state if the current and intended states differ or if we have forced an update
	IF g_eCurrentDoorState[eDoorName] != eIntendedState
		#IF IS_DEBUG_BUILD
			IF eDoorName = DOORNAME_M_MANSION_G1
				CPRINTLN(DEBUG_DOOR, "Michael garage door g_eCurrentDoorState != eIntendedState")
			ENDIF
		#ENDIF
		bPerformUpdate = TRUE
	ENDIF
	IF IS_BIT_SET(g_iUpdateDoorStateBitset[ENUM_TO_INT(eDoorName)/32], (ENUM_TO_INT(eDoorName)%32))
		IF (NOT IS_BIT_SET(g_iOverrideDoorStateBitset[ENUM_TO_INT(eDoorName)/32], ENUM_TO_INT(eDoorName)%32))
		OR (g_iOverrideDoorStateCounter[eDoorName] = 0 AND g_eOverrideDoorState[eDoorName] != DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA)
			#IF IS_DEBUG_BUILD
				IF NOT bPerformUpdate
					CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Updating state for door: ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
				ENDIF
			#ENDIF
			bPerformUpdate = TRUE
		ENDIF
	ENDIF
	
	IF bPerformUpdate
	
		IF NOT g_bDoorSystemInitialised
			CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Waiting for door system to initialise...")
		ELSE
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sData.doorHash)
				ADD_DOOR_TO_SYSTEM(sData.doorHash, sData.model, sData.coords, FALSE, FALSE)
			ENDIF
			
			SWITCH eIntendedState
				
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				///    	  
				///    	  LOCKED
				CASE DOORSTATE_LOCKED
				
					CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Door should be locked : ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
					
					// Check that it is safe to lock the doors
					BOOL bSafeToProceed
					
					IF IS_BIT_SET(sData.flags, ENUM_TO_INT(DOORFLAG_IGNORE_SAFETY_CHECKS))
						bSafeToProceed = TRUE
					ELSE
						CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Door ratio for ", sData.dbg_name, " = ", DOOR_SYSTEM_GET_OPEN_RATIO(sData.doorHash), " - ", GET_THIS_SCRIPT_NAME(), ".")
						
						// Make sure the player is atleast 3 metres away from the door
						IF fDistanceToDoor > 3.0
						OR ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(sData.doorHash)) <= 0.015
							
							// Only lock the door once it is fully closed
							//IF ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(sData.doorHash)) <= 0.01
								// Make sure we dont lock the player inside
								INTERIOR_INSTANCE_INDEX iPlayerInterior, iDoorInterior
								iPlayerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
								iDoorInterior = GET_INTERIOR_AT_COORDS(sData.coords)
								IF (iPlayerInterior <> iDoorInterior) OR (iPlayerInterior = NULL)
									// Safe to proceed with locking
									bSafeToProceed = TRUE
								ENDIF
							/*ELSE
								CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Wait for door to close : ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
							ENDIF*/
						ELSE
							CDEBUG3LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Wait for player to clear area : ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
						ENDIF
					ENDIF
					
					IF bSafeToProceed
						CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Locking doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
						
						IF sData.automaticRate != 0.0
							DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
						ENDIF
						DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
						bUpdatePerformed = TRUE
					ENDIF
				BREAK
				CASE DOORSTATE_FORCE_LOCKED_THIS_FRAME
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Locking (FORCE) doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
					
					IF sData.automaticRate != 0.0
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
				CASE DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Locking (FORCE) doorID[", eDoorName, "], until out of the area ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")

					IF sData.automaticRate != 0.0
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
				
				
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				///    	  
				///    	  UNLOCKED
				CASE DOORSTATE_UNLOCKED
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Unlocking doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")

					IF sData.automaticRate != 0.0
						CPRINTLN(DEBUG_DOOR, "Door ", INT_TO_ENUM(DOOR_HASH_ENUM,sData.doorHash), " automatic rate set to ", sData.automaticRate, ".")
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, TRUE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
				CASE DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Unlocking (FORCE) doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")

					IF sData.automaticRate != 0.0
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
				
				
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				///    	  
				///    	  OPEN
				CASE DOORSTATE_FORCE_OPEN_THIS_FRAME
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Opening (FORCE) doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")

					IF sData.automaticRate != 0.0
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
				
				
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				///    	  
				///    	  CLOSED
				CASE DOORSTATE_FORCE_CLOSED_THIS_FRAME
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - Closing (FORCE) doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")
							
					IF sData.automaticRate != 0.0
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
				
				
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////
				///    	  
				///    	  MISSING STATES
				DEFAULT
					CDEBUG1LN(DEBUG_DOOR, "PERFORM_DOOR_LOCK() - DEFAULT doorID[", eDoorName, "], ", sData.dbg_name, " - ", GET_THIS_SCRIPT_NAME(), ".")

					IF sData.automaticRate != 0.0
						DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(sData.doorHash, sData.automaticRate, FALSE, FALSE)
					ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(sData.doorHash, eIntendedState, FALSE, TRUE)
					bUpdatePerformed = TRUE
				BREAK
			ENDSWITCH
		ENDIF
		
		// [4] Update the stored global if the state change was successful
		IF bUpdatePerformed
			CLEAR_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(eDoorName)/32], (ENUM_TO_INT(eDoorName)%32))
			g_eCurrentDoorState[eDoorName] = eIntendedState
		ENDIF
	ENDIF
	
	// [5] Set the update flag if we just performed a state override
	IF IS_BIT_SET(g_iOverrideDoorStateBitset[ENUM_TO_INT(eDoorName)/32], (ENUM_TO_INT(eDoorName)%32))
	AND g_eOverrideDoorState[eDoorName] != DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
		SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(eDoorName)/32], (ENUM_TO_INT(eDoorName)%32))
		QUEUE_DOOR_UPDATE_FOR_NEXT_FRAME(eDoorName)
		IF g_iOverrideDoorStateCounter[eDoorName] < 2
			g_iOverrideDoorStateCounter[eDoorName]++
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets the road nodes state for the specified building state
PROC UPDATE_ROAD_NODES_FOR_BUILDING_STATE(BUILDING_DATA_STRUCT &sData, BUILDING_NAME_ENUM eBuildingName, BUILDING_STATE_ENUM eBuildingState)

	IF NOT ARE_VECTORS_EQUAL(sData.road_node_min_coords, <<0.0,0.0,0.0>>)
	AND NOT ARE_VECTORS_EQUAL(sData.road_node_max_coords, <<0.0,0.0,0.0>>)
		IF sData.road_node_active[eBuildingState]
			SET_ROADS_IN_AREA(sData.road_node_min_coords, sData.road_node_max_coords, TRUE)
		ELSE
			SET_ROADS_IN_AREA(sData.road_node_min_coords, sData.road_node_max_coords, FALSE)
		ENDIF
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(sData.nav_mesh_block_postion, <<0.0,0.0,0.0>>)
		IF sData.nav_mesh_block_active[eBuildingState]
			IF NOT g_bBuildingNavmeshBlockSet[eBuildingName]
				g_bBuildingNavmeshBlockingID[eBuildingName] = ADD_NAVMESH_BLOCKING_OBJECT(sData.nav_mesh_block_postion, sData.nav_mesh_block_sizeXYZ, sData.nav_mesh_block_heading)
				g_bBuildingNavmeshBlockSet[eBuildingName] = TRUE
			ENDIF
		ELSE
			IF g_bBuildingNavmeshBlockSet[eBuildingName]
				REMOVE_NAVMESH_BLOCKING_OBJECT(g_bBuildingNavmeshBlockingID[eBuildingName])
				g_bBuildingNavmeshBlockSet[eBuildingName] = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(sData.code_vehgen_min_coords, <<0.0,0.0,0.0>>)
	AND NOT ARE_VECTORS_EQUAL(sData.code_vehgen_max_coords, <<0.0,0.0,0.0>>)
		IF sData.code_vehgen_block_active[eBuildingState]
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(sData.code_vehgen_min_coords, sData.code_vehgen_max_coords, FALSE)
			
			// Test fix for bug #1327182 - Still getting no scripted vehicle gen vans spawning vans during this mission even though they are meant to be disabled.
			/*IF eBuildingName = BUILDINGNAME_SB_BUGSTAR_DOCKS_1
			
				VECTOR vCoord = (sData.code_vehgen_min_coords+((sData.code_vehgen_max_coords-sData.code_vehgen_min_coords)*0.5))
				FLOAT fRadius = GET_DISTANCE_BETWEEN_COORDS(sData.code_vehgen_min_coords, sData.code_vehgen_max_coords)*0.6
				
				//CPRINTLN(DEBUG_BUILDING, "UPDATE_ROAD_NODES_FOR_BUILDING_STATE - Clearing area of vehicle at ", GET_STRING_FROM_VECTOR(vCoord), " with radius of ", fRadius)
				CLEAR_AREA_OF_VEHICLES(vCoord, fRadius)
			ENDIF*/
		ELSE
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(sData.code_vehgen_min_coords, sData.code_vehgen_max_coords, TRUE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Sets the state of any scenario blocking areas assinged to a building.
PROC UPDATE_SCENARIO_BLOCKING_FOR_BUILDING_STATE(BUILDING_DATA_STRUCT &sData, BUILDING_NAME_ENUM eBuildingName, BUILDING_STATE_ENUM eBuildingState)
	IF NOT ARE_VECTORS_EQUAL(sData.scenario_block_minXYZ, <<0.0,0.0,0.0>>)
		//In normal state disable scenario blocks.
		IF eBuildingState = BUILDINGSTATE_NORMAL
			IF g_bBuildingScenarioBlockSet[eBuildingName]
				REMOVE_SCENARIO_BLOCKING_AREA(g_scenarioBuildingBlock[eBuildingName])
				g_bBuildingScenarioBlockSet[eBuildingName] = FALSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_BUILDING, "UPDATE_SCENARIO_BLOCKING_FOR_BUILDING_STATE - Removed scenario block for building ", sData.dbg_name, ".")
				#ENDIF
			ENDIF
			
		//In destroyed or cleanup state enable scenario blocking.
		ELIF eBuildingState = BUILDINGSTATE_DESTROYED
		OR eBuildingState = BUILDINGSTATE_CLEANUP
			IF NOT g_bBuildingScenarioBlockSet[eBuildingName]
				g_scenarioBuildingBlock[eBuildingName] = ADD_SCENARIO_BLOCKING_AREA(sData.scenario_block_minXYZ, sData.scenario_block_maxXYZ)
				g_bBuildingScenarioBlockSet[eBuildingName] = TRUE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_BUILDING, "UPDATE_SCENARIO_BLOCKING_FOR_BUILDING_STATE - Enabled scenario block for building ", sData.dbg_name, ".")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Swaps the building models if valid conditions are met
///    NOTE: Returns TRUE if a swap was made
FUNC BOOL PERFORM_BUILDING_SWAP(BUILDING_NAME_ENUM eBuildingName, BOOL bRefreshInterior = TRUE, BOOL bIgnoreDistanceChecks = FALSE, BOOL bIgnorePlayerDeathCheck = FALSE)
	
	g_bPerformingBuildingSwap = TRUE
	BOOL bUpdatePerformed = FALSE
	
		BUILDING_STATE_ENUM eIntendedState, eCurrentState
		BUILDING_DATA_STRUCT sData
		GET_BUILDING_DATA(sData, eBuildingName)
		
		// [1] Determine what state the building should be in
		IF USE_SP_BUILDING_CONTROLLER_DATA()
			
			#if USE_CLF_DLC
				eIntendedState = g_savedGlobalsClifford.sBuildingData.eBuildingState[eBuildingName]
			#endif
			#if USE_NRM_DLC
				eIntendedState = g_savedGlobalsnorman.sBuildingData.eBuildingState[eBuildingName]
			#endif
			#if not USE_CLF_DLC
			#if not use_NRM_dlc
				eIntendedState = g_savedGlobals.sBuildingData.eBuildingState[eBuildingName]
			#endif			
			#endif
		ELSE
			#IF USE_SP_DLC				eIntendedState = MPGlobals_OLD.g_MPBuildingData.eBuildingState[eBuildingName]	#ENDIF
			#IF NOT USE_SP_DLC
				#IF USE_TU_CHANGES		eIntendedState = g_MPBuildingData.eBuildingState[eBuildingName]		#ENDIF
				#IF NOT USE_TU_CHANGES	eIntendedState = MPGlobals_OLD.g_MPBuildingData.eBuildingState[eBuildingName]	#ENDIF
			#ENDIF
		ENDIF
		eCurrentState = g_eCurrentBuildingState[eBuildingName]
		
	IF IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT bIgnorePlayerDeathCheck
	
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Building '", sData.dbg_name, "' cannot update state from [", ENUM_TO_INT(eCurrentState), "] to [", ENUM_TO_INT(eIntendedState), "], PLAYER_PED_ID is injured.")
		#ENDIF
		g_bPerformingBuildingSwap = TRUE
	ELSE
	
		RAYFIRE_INDEX rayfireID

		//
		// [2] Check that we are safe to set the state
		//
		BOOL bSafeToUpdate = TRUE
		
		// On startup we call INITIALISE_STORED_BUILDING_STATES_ON_STARTUP() which sets all building states.
		// Dont bother checking some states if the call come from this.
		IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) != GET_HASH_KEY("standard_global_reg")
		
		 	IF bIgnoreDistanceChecks = FALSE
		
				IF g_bBuildingStateUpdateDelay[eBuildingName]
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords) < 200
					bSafeToUpdate = FALSE
					
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Building '", sData.dbg_name, "' waiting for player to leave area before it can change to state[", ENUM_TO_INT(eIntendedState), "].")
					#ENDIF
					g_bPerformingBuildingSwap = TRUE
				ENDIF
				
				IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
				OR IS_PED_BEING_ARRESTED(PLAYER_PED_ID())
					IF NOT IS_SCREEN_FADED_OUT()
						bSafeToUpdate = FALSE
						
						#IF IS_DEBUG_BUILD
							CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Building '", sData.dbg_name, "' waiting for deatharrest to end or screen to fade out before it can change to state[", ENUM_TO_INT(eIntendedState), "].")
						#ENDIF
						g_bPerformingBuildingSwap = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
		#IF USE_TU_CHANGES
		AND (NOT IS_PLAYER_SWITCH_IN_PROGRESS() OR GET_PLAYER_SWITCH_STATE() != SWITCH_STATE_WAITFORINPUT)
		#ENDIF
			bSafeToUpdate = FALSE
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Building '", sData.dbg_name, "' waiting for new load scene to finish before it can change to state[", ENUM_TO_INT(eIntendedState), "].")
			#ENDIF
			g_bPerformingBuildingSwap = TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF GET_INDEX_OF_CURRENT_LEVEL() != LEVEL_GTA5
				RETURN FALSE
			ENDIF
		#ENDIF
		
		IF bSafeToUpdate
		
			// [3] Attempt to put the building into the intended state
			SWITCH sData.type
				CASE BUILDINGTYPE_STATIC
					IF eIntendedState = BUILDINGSTATE_CLEANUP
						// Invalid state for this type of building
						#IF IS_DEBUG_BUILD
							CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Cleanup state is an invalid state for building '", sData.dbg_name, "'.")
							SCRIPT_ASSERT("PERFORM_BUILDING_SWAP() - Cleanup state is an invalid state for this building.")
						#ENDIF
					ELSE
						IF sData.model[eIntendedState] != DUMMY_MODEL_FOR_SCRIPT
							REMOVE_MODEL_HIDE(sData.coords, 10.0, sData.model[eIntendedState])
						ENDIF
						IF sData.model[eCurrentState] != DUMMY_MODEL_FOR_SCRIPT
							CREATE_MODEL_HIDE(sData.coords, 10.0, sData.model[eCurrentState], TRUE)
						ENDIF
						//SWAP_NEAREST_BUILDING_MODEL(sData.coords, 10.0, sData.model[eCurrentState], sData.model[eIntendedState])
						//UPDATE_ROAD_NODES_FOR_BUILDING_STATE(sData, eBuildingName, eIntendedState)
						g_bBuildingRoadNodeBlockUpdate[eBuildingName] = TRUE
					ENDIF
					bUpdatePerformed = TRUE
				BREAK
				CASE BUILDINGTYPE_IPL
					IF eIntendedState = BUILDINGSTATE_NORMAL
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY(sData.name[eIntendedState])
							IF IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_DESTROYED])
								REMOVE_IPL(sData.name[BUILDINGSTATE_DESTROYED])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.name[BUILDINGSTATE_DESTROYED], "'.")
								g_bPerformingBuildingSwap = TRUE
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_DESTROYED], "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("REMOVE_ALL_STATES")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY(sData.name[eIntendedState])
							IF IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_CLEANUP])
								REMOVE_IPL(sData.name[BUILDINGSTATE_CLEANUP])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.name[BUILDINGSTATE_CLEANUP], "'.")
								g_bPerformingBuildingSwap = TRUE
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_CLEANUP], "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY("")
							IF NOT IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_NORMAL])
								REQUEST_IPL(sData.name[BUILDINGSTATE_NORMAL])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting IPL '", sData.name[BUILDINGSTATE_NORMAL], "'.")
								g_bPerformingBuildingSwap = TRUE
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_NORMAL], "' already active.")
							ENDIF
						ENDIF
						// Request the pre load state if it exists
						IF GET_HASH_KEY(sData.ipl_preload) != GET_HASH_KEY("")
							IF NOT IS_IPL_ACTIVE(sData.ipl_preload)
								REQUEST_IPL(sData.ipl_preload)
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting IPL '", sData.ipl_preload, "'.")
								g_bPerformingBuildingSwap = TRUE
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.ipl_preload, "' already active.")
							ENDIF
						ENDIF
						
					ELIF eIntendedState = BUILDINGSTATE_DESTROYED
						// Remove the pre load state if it exists
						IF GET_HASH_KEY(sData.ipl_preload) != GET_HASH_KEY("")
							IF IS_IPL_ACTIVE(sData.ipl_preload)
								REMOVE_IPL(sData.ipl_preload)
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.ipl_preload, "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.ipl_preload, "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY(sData.name[eIntendedState])
							IF IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_NORMAL])
								REMOVE_IPL(sData.name[BUILDINGSTATE_NORMAL])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.name[BUILDINGSTATE_NORMAL], "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_NORMAL], "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("REMOVE_ALL_STATES")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY(sData.name[eIntendedState])
							IF IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_CLEANUP])
								REMOVE_IPL(sData.name[BUILDINGSTATE_CLEANUP])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.name[BUILDINGSTATE_CLEANUP], "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_CLEANUP], "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY("")
							IF NOT IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_DESTROYED])
								REQUEST_IPL(sData.name[BUILDINGSTATE_DESTROYED])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting IPL '", sData.name[BUILDINGSTATE_DESTROYED], "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_DESTROYED], "' already active.")
							ENDIF
						ENDIF
					ELIF eIntendedState = BUILDINGSTATE_CLEANUP
						// Remove the pre load state if it exists
						IF GET_HASH_KEY(sData.ipl_preload) != GET_HASH_KEY("")
							IF IS_IPL_ACTIVE(sData.ipl_preload)
								REMOVE_IPL(sData.ipl_preload)
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.ipl_preload, "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.ipl_preload, "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY(sData.name[eIntendedState])
							IF IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_NORMAL])
								REMOVE_IPL(sData.name[BUILDINGSTATE_NORMAL])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.name[BUILDINGSTATE_NORMAL], "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_NORMAL], "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY(sData.name[eIntendedState])
							IF IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_DESTROYED])
								REMOVE_IPL(sData.name[BUILDINGSTATE_DESTROYED])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing IPL '", sData.name[BUILDINGSTATE_DESTROYED], "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_DESTROYED], "' not active.")
							ENDIF
						ENDIF
						IF GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("")
						AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("REMOVE_ALL_STATES")
							IF NOT IS_IPL_ACTIVE(sData.name[BUILDINGSTATE_CLEANUP])
								REQUEST_IPL(sData.name[BUILDINGSTATE_CLEANUP])
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting IPL '", sData.name[BUILDINGSTATE_CLEANUP], "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - IPL '", sData.name[BUILDINGSTATE_CLEANUP], "' already active.")
							ENDIF
						ENDIF
					ENDIF
					//UPDATE_ROAD_NODES_FOR_BUILDING_STATE(sData, eBuildingName, eIntendedState)
					g_bBuildingScenarioBlockUpdate[eBuildingName] = TRUE
					g_bBuildingRoadNodeBlockUpdate[eBuildingName] = TRUE
					bUpdatePerformed = TRUE
				BREAK
				CASE BUILDINGTYPE_ENTITY_SET
				
					INTERIOR_INSTANCE_INDEX interiorID
					interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(sData.coords, sData.es_interior)
					
					IF interiorID != NULL
						// Force remove any entity sets
						IF GET_HASH_KEY(sData.es_force_remove) != GET_HASH_KEY("")
							IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.es_force_remove)
								DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.es_force_remove)
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Force removing Entity Set '", sData.es_force_remove, "'.")
							ELSE
								CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.es_force_remove, "' not active.")
							ENDIF
						ENDIF
						
						IF eIntendedState = BUILDINGSTATE_NORMAL
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY("")
								IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_DESTROYED])
									DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_DESTROYED])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing Entity Set '", sData.name[BUILDINGSTATE_DESTROYED], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_DESTROYED], "' not active.")
								ENDIF
							ENDIF
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("")
							AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("REMOVE_ALL_STATES")
							AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY(sData.name[eIntendedState])
								IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_CLEANUP])
									DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_CLEANUP])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing Entity Set '", sData.name[BUILDINGSTATE_CLEANUP], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_CLEANUP], "' not active.")
								ENDIF
							ENDIF
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY("")
								IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_NORMAL])
									ACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_NORMAL])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting Entity Set '", sData.name[BUILDINGSTATE_NORMAL], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_NORMAL], "' already active.")
								ENDIF
							ENDIF
							
						ELIF eIntendedState = BUILDINGSTATE_DESTROYED
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY("")
								IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_NORMAL])
									DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_NORMAL])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing Entity Set '", sData.name[BUILDINGSTATE_NORMAL], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_NORMAL], "' not active.")
								ENDIF
							ENDIF
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("")
							AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("REMOVE_ALL_STATES")
							AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY(sData.name[eIntendedState])
								IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_CLEANUP])
									DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_CLEANUP])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing Entity Set '", sData.name[BUILDINGSTATE_CLEANUP], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_CLEANUP], "' not active.")
								ENDIF
							ENDIF
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY("")
								IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, (sData.name[BUILDINGSTATE_DESTROYED]))
									ACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_DESTROYED])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting Entity Set '", sData.name[BUILDINGSTATE_DESTROYED], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_DESTROYED], "' already active.")
								ENDIF
							ENDIF
						ELIF eIntendedState = BUILDINGSTATE_CLEANUP
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_NORMAL]) != GET_HASH_KEY("")
								IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_NORMAL])
									DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_NORMAL])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing Entity Set '", sData.name[BUILDINGSTATE_NORMAL], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_NORMAL], "' not active.")
								ENDIF
							ENDIF
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_DESTROYED]) != GET_HASH_KEY("")
								IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, sData.name[BUILDINGSTATE_DESTROYED])
									DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_DESTROYED])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Removing Entity Set '", sData.name[BUILDINGSTATE_DESTROYED], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_DESTROYED], "' not active.")
								ENDIF
							ENDIF
							IF GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("")
							AND GET_HASH_KEY(sData.name[BUILDINGSTATE_CLEANUP]) != GET_HASH_KEY("REMOVE_ALL_STATES")
								IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, (sData.name[BUILDINGSTATE_CLEANUP]))
									ACTIVATE_INTERIOR_ENTITY_SET(interiorID, sData.name[BUILDINGSTATE_CLEANUP])
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Requesting Entity Set '", sData.name[BUILDINGSTATE_CLEANUP], "'.")
								ELSE
									CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Entity Set '", sData.name[BUILDINGSTATE_CLEANUP], "' already active.")
								ENDIF
							ENDIF
						ENDIF
						IF bRefreshInterior
							REFRESH_INTERIOR(interiorID)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Unable to find interior for '", sData.dbg_name, "' so we are assuming it is safe continue.")
					ENDIF
					//UPDATE_ROAD_NODES_FOR_BUILDING_STATE(sData, eBuildingName, eIntendedState)
					g_bBuildingRoadNodeBlockUpdate[eBuildingName] = TRUE
					g_bBuildingScenarioBlockUpdate[eBuildingName] = TRUE
					bUpdatePerformed = TRUE
				BREAK
				CASE BUILDINGTYPE_RAYFIRE
					// Cut out unneeded processing by doing a simple distance check
					IF GET_DISTANCE_BETWEEN_COORDS(sData.coords, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 250.0
						// Check if the rayfire map object exists and that it is in a suitable state for updating
						rayfireID = GET_RAYFIRE_MAP_OBJECT(sData.coords, 25, sData.name[BUILDINGSTATE_NORMAL])
						IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireID)
							IF eIntendedState = BUILDINGSTATE_NORMAL
								SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireID, RFMO_STATE_START)
								//UPDATE_ROAD_NODES_FOR_BUILDING_STATE(sData, eBuildingName, eIntendedState)
								g_bBuildingRoadNodeBlockUpdate[eBuildingName] = TRUE
								bUpdatePerformed = TRUE
							ELIF eIntendedState = BUILDINGSTATE_DESTROYED
								// Do not set to end state if its currently animating
								IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireID) <> RFMO_STATE_START_ANIM
								AND GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireID) <> RFMO_STATE_ANIMATING
								AND GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireID) <> RFMO_STATE_SYNC_ENDING
									SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireID, RFMO_STATE_END)
									//UPDATE_ROAD_NODES_FOR_BUILDING_STATE(sData, eBuildingName, eIntendedState)
									g_bBuildingRoadNodeBlockUpdate[eBuildingName] = TRUE
									bUpdatePerformed = TRUE
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Waiting for RFMO '", sData.dbg_name, "' to stop animating.")
									#ENDIF
								ENDIF
							ELIF eIntendedState = BUILDINGSTATE_CLEANUP
								// Invalid state for this type of building
								#IF IS_DEBUG_BUILD
									CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Cleanup state is an invalid state for building '", sData.dbg_name, "'.")
									SCRIPT_ASSERT("PERFORM_BUILDING_SWAP() - Cleanup state is an invalid state for building.")
								#ENDIF
								bUpdatePerformed = TRUE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - RFMO '", sData.dbg_name, "' doesn't exist.")
							#ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE BUILDINGTYPE_MODEL_SWAP
					IF eIntendedState = BUILDINGSTATE_NORMAL
						// bug 1748503
						//IF IS_BIT_SET(g_iModelSwappedBitset[ENUM_TO_INT(eBuildingName)/32], (ENUM_TO_INT(eBuildingName)%32))
							CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - REMOVE_MODEL_SWAP '", sData.dbg_name, "' removed.")
							REMOVE_MODEL_SWAP(sData.coords, 50.0, sData.model[BUILDINGSTATE_DESTROYED], sData.model[BUILDINGSTATE_NORMAL], FALSE)
							CLEAR_BIT(g_iModelSwappedBitset[ENUM_TO_INT(eBuildingName)/32], (ENUM_TO_INT(eBuildingName)%32))
						//ENDIF
					ELIF eIntendedState = BUILDINGSTATE_DESTROYED
						// bug 1748503
						//IF NOT IS_BIT_SET(g_iModelSwappedBitset[ENUM_TO_INT(eBuildingName)/32], (ENUM_TO_INT(eBuildingName)%32))
							CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - CREATE_MODEL_SWAP '", sData.dbg_name, "' swapped.")
							CREATE_MODEL_SWAP(sData.coords, 50.0, sData.model[BUILDINGSTATE_NORMAL], sData.model[BUILDINGSTATE_DESTROYED], TRUE)
							SET_BIT(g_iModelSwappedBitset[ENUM_TO_INT(eBuildingName)/32], (ENUM_TO_INT(eBuildingName)%32))
						//ENDIF
					ENDIF
					bUpdatePerformed = TRUE
				BREAK
			ENDSWITCH
			
			// [4] Update the stored global if the state change was successful
			IF bUpdatePerformed
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_BUILDING, "PERFORM_BUILDING_SWAP() - Building '", sData.dbg_name, "' is now in state [", ENUM_TO_INT(eIntendedState), "]. ", GET_THIS_SCRIPT_NAME())
				#ENDIF
				g_bUpdateBuildingState[eBuildingName] = FALSE
				g_eCurrentBuildingState[eBuildingName] = eIntendedState
				
				IF NOT USE_SP_BUILDING_CONTROLLER_DATA()
					IF NOT g_bBuildingStateSetInMP[eBuildingName]
						g_bBuildingStateSetInMP[eBuildingName] = TRUE
						g_iBuildingStatesSetInMP++
						CDEBUG3LN(DEBUG_BUILDING, "KENNETH TEST : ", eBuildingName)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN (bUpdatePerformed)
	
ENDFUNC


PROC SETUP_AUTOMATIC_DOOR_DATA()
	AUTOMATIC_DOOR_ENUM eAutoDoor

	// The automatic door leading into Michael's Mansion driveway.
	eAutoDoor = AUTODOOR_MICHAEL_MANSION_GATE
	g_sAutoDoorData[eAutoDoor].doorID = 						HASH("AUTODOOR_MICHAEL_MANSION_GATE")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-844, 156, 66>>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_LRGGATE_02_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					6*6
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			15.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-851.815857,170.874283,61.302193>> 
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-851.570007,148.145905,71.345329>>
	g_sAutoDoorData[eAutoDoor].activationLocate2Width =			15.000000
	g_sAutoDoorData[eAutoDoor].activationLocate2PosA = 			<<-836.659668,175.336563,65.059769>>
	g_sAutoDoorData[eAutoDoor].activationLocate2PosB =			<<-836.687805,152.788971,73.663818>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.25
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_TWO_LOCATES)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_OPEN_ON_WANTED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Docks gates.
	eAutoDoor = AUTODOOR_DOCKS_FRONT_GATE_IN
	g_sAutoDoorData[eAutoDoor].doorID = 						HASH("AUTODOOR_DOCKS_FRONT_GATE_IN")
	g_sAutoDoorData[eAutoDoor].coords = 						<<19, -2530, 5>>			//<< 18.4488, -2532.6448, 5.0502 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_DOCKS_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						25*25
	g_sAutoDoorData[eAutoDoor].blockedRange =					7*7
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			7.750000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<15.188263,-2531.203125,5.050370>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<30.920921,-2542.127930,7.504300>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_DOCKS_FRONT_GATE_OUT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DOCKS_FRONT_GATE_OUT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<11, -2542, 5>>			//<< 12.9038, -2538.7798, 5.0502 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_DOCKS_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						25*25
	g_sAutoDoorData[eAutoDoor].blockedRange =					7*7
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			7.750000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<13.802609,-2540.929199,5.050376>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-5.521768,-2527.885010,7.542066>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_DOCKS_BACK_GATE_IN
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DOCKS_BACK_GATE_IN")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-193, -2515, 5>>//<< -190.6622, -2515.3093, 5.0474 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_DOCKS_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						25*25
	g_sAutoDoorData[eAutoDoor].blockedRange =					7*7
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			12
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-190.696396,-2515.287842,4.678052>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-190.791306,-2536.691650,9.000118>>

	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_DOCKS_BACK_GATE_OUT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DOCKS_BACK_GATE_OUT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-203, -2515, 5>>			//<< -199.4173, -2515.4197, 5.0474 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_DOCKS_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						25*25
	g_sAutoDoorData[eAutoDoor].blockedRange =					7*7
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			12.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-200.207962,-2515.378662,5.047561>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-200.324066,-2495.309326,7.891297>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Military docks gates.
	eAutoDoor = AUTODOOR_MIL_DOCKS_GATE_IN
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_DOCKS_GATE_IN")
	g_sAutoDoorData[eAutoDoor].coords = 						<<476, -3116, 5>>			//<< 479.0721, -3116.1152, 5.0701 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_DOCKS_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					7*7
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			40.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<484.581024,-3115.547607,4.652226>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<473.637878,-3116.050537,11.070095>> 
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_MIL_DOCKS_GATE_OUT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_DOCKS_GATE_OUT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<492, -3116, 5>>			//<< 489.1011, -3116.0469, 5.0701 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_DOCKS_LD
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					7*7
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			40.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<484.087585,-3115.800781,4.652227>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<494.890289,-3115.787354,11.068663>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Military base gates.
	eAutoDoor = AUTODOOR_MIL_BASE_GATE_IN
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_BASE_GATE_IN")
	g_sAutoDoorData[eAutoDoor].coords = 						<<1600, -2806, 16>>//<< -1596.5776, 2808.1213, 16.1039 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_MILITARY_01
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					5*5
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			40.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1599.609009,2805.895264,15.670090>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1593.815430,2811.392334,22.024572>>  
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_MIL_BASE_GATE_OUT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_BASE_GATE_OUT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<1605, -2801, 16>>//<< -1602.2544, 2803.3594, 16.0960 >>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_MILITARY_01
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					5*5
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			40.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1599.609009,2805.895264,15.670090>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1605.858154,2800.540771,22.508896>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_CULT_GATE_LEFT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_CULT_GATE_LEFT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-1041, 4906, 209>>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_CULT_01_L
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					5*5
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			40.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1050.437134,4909.196289,200>> 
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1027.921753,4921.747559,215>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_REVERSE_OPEN_RATIO) // Needs set to false when door is finally used by Tor
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_CULT_GATE_RIGHT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_CULT_GATE_RIGHT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-1045, 4915, 209>>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_GATE_CULT_01_R
	g_sAutoDoorData[eAutoDoor].checkRange =						30*30
	g_sAutoDoorData[eAutoDoor].blockedRange =					6*6
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			40.000000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1050.437134,4909.196289,200>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1027.921753,4921.747559,215>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO) // Needs set to false when door is finally used by Tor
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Garage door on Downtown Vinewood. TODO #1210468
	eAutoDoor = AUTODOOR_DTOWN_VINEWOOD_GARAGE
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DTOWN_VINEWOOD_GARAGE")
	g_sAutoDoorData[eAutoDoor].coords = 						<<523.9820, 167.7104, 100.5962>>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_HW1_03_GARDOOR_01
	g_sAutoDoorData[eAutoDoor].checkRange =						25*25
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			21.75
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<520.243652,164.893188,98.044174>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<523.080017,172.699860,102.782425>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Franklin's Hills savehouse garage.
	eAutoDoor = AUTODOOR_FRAN_HILLS_GARAGE
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_FRAN_HILLS_GARAGE")
	g_sAutoDoorData[eAutoDoor].coords = 						<<18.6504, 546.3401, 176.3448>>
	g_sAutoDoorData[eAutoDoor].model = 							PROP_CH_025C_G_DOOR_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			14.750000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<19.326839,550.176025,174.000137>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<15.769264,543.835083,178.022980>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Outer gate to cop building car park in SC1.
	eAutoDoor = AUTODOOR_SC1_COP_CARPARK
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_SC1_COP_CARPARK")
	g_sAutoDoorData[eAutoDoor].coords = 						<<397.83, -1607.34, 28.34>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_facgate_07b
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			14.50000
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<405.365051,-1612.886475,27.522816>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<396.683960,-1605.760864,32.766979>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Gate to Devin's house.
	eAutoDoor = AUTODOOR_DEVIN_GATE_L
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DEVIN_GATE_L")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-2559.19, 1910.86, 169.07>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_lrggate_01c_l
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			13.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-2551.360352,1909.907471,166.433594>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-2572.051025,1924.179932,171.394714>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Gate to Devin's house.
	eAutoDoor = AUTODOOR_DEVIN_GATE_R
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DEVIN_GATE_R")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-2556.66, 1915.72, 169.07>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_lrggate_01c_r
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			13.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-2551.360352,1909.907471,166.433594>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-2572.051025,1924.179932,171.394714>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Gate leading out of the airport area.
	eAutoDoor = AUTODOOR_AIRPORT_BARRIER_OUT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_AIRPORT_BARRIER_OUT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-961.23, -2796.28, 13.96>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_sec_barrier_ld_01a
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			7.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-958.540161,-2798.280762,12.714785>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-964.457214,-2808.593262,17.714787>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Gate leading into the airport area.
	eAutoDoor = AUTODOOR_AIRPORT_BARRIER_IN
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_AIRPORT_BARRIER_IN")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-967.01, -2802.45, 13.96>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_sec_barrier_ld_01a
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			6.5
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-970.271973,-2800.352539,12.714786>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-965.005249,-2791.101318,17.714787>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	// Garage doors to the impound lot
	eAutoDoor = AUTODOOR_IMPOUND_L
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_IMPOUND_L")
	g_sAutoDoorData[eAutoDoor].coords = 						<<431.41, -1000.16, 28.65>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_com_gar_door_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			11.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<433.782684,-1001.489685,23.797237>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<433.886230,-1017.221680,29.593578>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_IMPOUND_R
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_IMPOUND_R")
	g_sAutoDoorData[eAutoDoor].coords = 						<<436.22, -1001.17, 26.71>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_com_gar_door_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			11.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<433.782684,-1001.489685,23.797237>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<433.886230,-1017.221680,29.593578>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	//Gates leading into the military base.
	eAutoDoor = AUTODOOR_MIL_BASE_BARRIER_IN
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_BASE_BARRIER_IN")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-1588.27, 2794.21, 16.85>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_sec_barrier_ld_01a
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			6.75
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1585.348633,2796.764648,15.333453>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1573.873657,2783.339111,20.003242>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_MIL_BASE_BARRIER_OUT
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_BASE_BARRIER_OUT")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-1589.58, 2793.67, 16.86>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_sec_barrier_ld_01a
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			6.75
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1592.551758,2791.039795,15.326007>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1604.540039,2805.557617,20.174604>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_AIRPORT_ALT_GATES_L
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_AIRPORT_ALT_GATES_L")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-984.08, -2348.40, 12.94>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_facgate_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			15.25
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-979.238770,-2341.410889,11.924736>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-993.525146,-2333.146973,20.424732>>
	g_sAutoDoorData[eAutoDoor].activationLocate2Width =			15.25
	g_sAutoDoorData[eAutoDoor].activationLocate2PosA = 			<<-984.464417,-2357.319580,11.924736>>
	g_sAutoDoorData[eAutoDoor].activationLocate2PosB =			<<-1002.451172,-2346.988281,20.444717>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_AIRPORT_ALT_GATES_R
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_AIRPORT_ALT_GATES_R")
	g_sAutoDoorData[eAutoDoor].coords = 						<<-994.50, -2341.65, 12.94>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_facgate_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			15.25
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-979.238770,-2341.410889,11.924736>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-993.525146,-2333.146973,20.424732>>
	g_sAutoDoorData[eAutoDoor].activationLocate2Width =			15.25
	g_sAutoDoorData[eAutoDoor].activationLocate2PosA = 			<<-984.464417,-2357.319580,11.924736>>
	g_sAutoDoorData[eAutoDoor].activationLocate2PosB =			<<-1002.451172,-2346.988281,20.444717>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_LINKED_DOORS)
	
	
	eAutoDoor = AUTODOOR_HAYES_GARAGE
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_HAYES_GARAGE")
	g_sAutoDoorData[eAutoDoor].coords = 						<<484.56, -1315.57, 30.20>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_com_gar_door_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			6.5
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<486.405701,-1311.665405,27.751633>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<488.912354,-1316.529053,31.652016>>
	g_sAutoDoorData[eAutoDoor].activationLocate2Width =			8.5
	g_sAutoDoorData[eAutoDoor].activationLocate2PosA = 			<<482.318695,-1319.806641,26.918484>>
	g_sAutoDoorData[eAutoDoor].activationLocate2PosB =			<<478.041046,-1311.510742,31.952971>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_DRAINSERVICE_L
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DRAINSERVICE_L")
	g_sAutoDoorData[eAutoDoor].coords = 						<<451.81, -1994.17, 22.14>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_facgate_01
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			10.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<444.066925,-1993.736206,21.109921>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<459.423035,-2010.025513,26.195147>>
	g_sAutoDoorData[eAutoDoor].activationLocate2Width =			10.0
	g_sAutoDoorData[eAutoDoor].activationLocate2PosA = 			<<452.251373,-1987.255249,21.189449>>
	g_sAutoDoorData[eAutoDoor].activationLocate2PosB =			<<466.811707,-2002.877563,25.898800>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.30
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_TWO_LOCATES)
	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_LINKED_DOORS)
	
	eAutoDoor = AUTODOOR_DRAINSERVICE_R
	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_DRAINSERVICE_R")
	g_sAutoDoorData[eAutoDoor].coords = 						<<460.06, -2003.11, 22.14>>
	g_sAutoDoorData[eAutoDoor].model = 							prop_facgate_01b
	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			10.0
	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<444.066925,-1993.736206,21.109921>>
	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<459.423035,-2010.025513,26.195147>>
	g_sAutoDoorData[eAutoDoor].activationLocate2Width =			10.0
	g_sAutoDoorData[eAutoDoor].activationLocate2PosA = 			<<452.251373,-1987.255249,21.189449>>
	g_sAutoDoorData[eAutoDoor].activationLocate2PosB =			<<466.811707,-2002.877563,25.898800>>
	g_sAutoDoorData[eAutoDoor].openRate = 						0.30
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_TWO_LOCATES)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_LINKED_DOORS)
		
ENDPROC


PROC RESET_ALL_AUTOMATIC_DOOR_REGISTERED_PEDS()
	INT iDoorIndex
	INT iRegisteredPedIndex
	
	REPEAT AUTODOOR_MAX iDoorIndex
		REPEAT g_sAutoDoorData[iDoorIndex].registeredPedCount iRegisteredPedIndex
			g_sAutoDoorData[iDoorIndex].registeredPed[iRegisteredPedIndex] = INT_TO_NATIVE(PED_INDEX, 0)
		ENDREPEAT
		g_sAutoDoorData[iDoorIndex].registeredPedCount = 0
	ENDREPEAT
	
	CPRINTLN(DEBUG_DOOR, "<AUTODOOR> Unregistered all peds for all doors.")
ENDPROC

FUNC INTERIOR_DATA_STRUCT GET_INTERIOR_DATA(INTERIOR_NAME_ENUM eInterior)

	INTERIOR_DATA_STRUCT structInteriorData
	MP_PROP_OFFSET_STRUCT tempStruct
	SWITCH eInterior
		
		CASE INTERIOR_V_COMEDY
			structInteriorData.vPos = <<-447.4833, 280.3197, 77.5215>>
			structInteriorData.sInteriorName = "v_comedy"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_PSYCHEOFFICE
			structInteriorData.vPos = <<-1906.7858, -573.7576, 19.0773>>
			structInteriorData.sInteriorName = "v_psycheoffice"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_RANCH
			structInteriorData.vPos = <<1399.9730, 1148.7559, 113.3336>>
			structInteriorData.sInteriorName = "v_ranch"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_RECYCLE
			structInteriorData.vPos = <<-598.6379, -1608.3986, 26.0108>>
			structInteriorData.sInteriorName = "v_recycle"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_ROCKCLUB
			structInteriorData.vPos = <<-556.5089, 286.3181, 81.1763>>
			structInteriorData.sInteriorName = "v_rockclub"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_JANITOR
			structInteriorData.vPos = <<-111.7116, -11.9120, 69.5196>>
			structInteriorData.sInteriorName = "v_janitor"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_LESTERS
			structInteriorData.vPos = <<1274.9338, -1714.7256, 53.7715>>
			structInteriorData.sInteriorName = "v_lesters"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_TORTURE
			structInteriorData.vPos = <<147.4330, -2201.3704, 3.6880>>
			structInteriorData.sInteriorName = "v_torture"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
			
		CASE INTERIOR_V_CINEMA_VINEWOOD
			structInteriorData.vPos = <<320.9934, 265.2515, 82.1221>>
			structInteriorData.sInteriorName = "v_cinema"
			structInteriorData.sDebugName = "v_cinema (Vinewood)"
		BREAK
		
		CASE INTERIOR_V_CINEMA_MORNINGWOOD
			structInteriorData.vPos = << -1425.5645, -244.3000, 15.8053 >>
			structInteriorData.sInteriorName = "v_cinema"
			structInteriorData.sDebugName = "v_cinema (Morningwood)"
		BREAK
		
		CASE INTERIOR_V_CINEMA_DOWNTOWN
			structInteriorData.vPos = << 377.1530, -717.5670, 10.0536 >>
			structInteriorData.sInteriorName = "v_cinema"
			structInteriorData.sDebugName = "v_cinema (Downtown)"
		BREAK
		
		CASE INTERIOR_V_EPSILONISM
			structInteriorData.vPos = <<245.1564, 370.2110, 104.7382>>
			structInteriorData.sInteriorName = "v_epsilonism"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
					
		CASE INTERIOR_V_GARAGES
			structInteriorData.vPos = <<173.1176, -1003.2789, -99.9999>>
			structInteriorData.sInteriorName = "v_garages"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_GARAGEM
			structInteriorData.vPos = <<199.9715, -999.6678, -100.0000>>
			structInteriorData.sInteriorName = "v_garagem"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_GARAGEL
			structInteriorData.vPos = <<228.6058, -992.0537, -99.9999>>
			structInteriorData.sInteriorName = "v_garagel"
			structInteriorData.sInteriorName = "hei_dlc_garage_high_new"
			structInteriorData.sDebugName = structInteriorData.sInteriorName			
		BREAK
		
		CASE INTERIOR_V_SHERIFF
			structInteriorData.vPos = <<1854.2538, 3686.7385, 33.2671>>
			structInteriorData.sInteriorName = "v_sheriff"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_SHERIFF2
			structInteriorData.vPos = <<-444.8907, 6013.5869, 30.7164>>
			structInteriorData.sInteriorName = "v_sheriff2"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_LAB
			structInteriorData.vPos = <<3522.8452, 3707.9653, 19.9918>>
			structInteriorData.sInteriorName = "v_lab"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_SWEAT
			structInteriorData.vPos = <<717.2994, -974.4271, 23.9142>>
			structInteriorData.sInteriorName = "v_sweat"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_SWEATEMPTY
			structInteriorData.vPos = <<717.2990, -974.4271, 23.9142>>
			structInteriorData.sInteriorName = "v_sweatempty"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_FARMHOUSE
			structInteriorData.vPos = <<2449.7852, 4983.8247, 45.8106>>
			structInteriorData.sInteriorName = "v_farmhouse"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_FOUNDRY
			structInteriorData.vPos = <<1087.1952, -1988.4449, 28.6490>>
			structInteriorData.sInteriorName = "v_foundry"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_ABATTOIR
			structInteriorData.vPos = <<982.2330, -2160.3816, 28.4761>>
			structInteriorData.sInteriorName = "v_abattoir"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CHOPSHOP
			structInteriorData.vPos = <<479.0568, -1316.8253, 28.2038>>
			structInteriorData.sInteriorName = "v_chopshop"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_58_SOL_OFFICE
			structInteriorData.vPos = <<-1005.6632, -478.3461, 49.0265>>
			structInteriorData.sInteriorName = "v_58_sol_office"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		// MP apartments
		CASE INTERIOR_V_APARTMENT_HIGH_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>//<<-791.8,338.5, 153.8>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (1)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (1)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_2)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (2)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (2)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (3)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (3)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_4
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>//<<-764.7, 319.2, 170.6>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (4)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (4)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_5
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_5)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>//<<-260.9, -953.6,71.0>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (5)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (5)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_6
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_6)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (6)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (6)"
			#ENDIF
		BREAK

		CASE INTERIOR_V_APARTMENT_HIGH_7
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_7)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (7)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (7)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_8
			#IF USE_SP_DLC
				tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_8)
				structInteriorData.vPos =  tempStruct.vLoc +<<0,0,1>>// <<-1468,-529.9,50.7>>
			#ENDIF
			#IF NOT USE_SP_DLC
				#IF NOT USE_TU_CHANGES
					tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_8)
					structInteriorData.vPos =  tempStruct.vLoc +<<0,0,1>>// <<-1468,-529.9,50.7>>
				#ENDIF
				#IF USE_TU_CHANGES
					structInteriorData.vPos  = mpProperties[PROPERTY_HIGH_APT_8].house.buzzerEnter.vInsidePlayerPos+<<0,0,1>>
					//structInteriorData.vPos = <<-27.0,-582.5,89.9>>
				#ENDIF
			#ENDIF
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (8)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (8)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_9
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_9)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (9)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (9)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_10
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_10)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (10)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (10)"
			#ENDIF
		BREAK

		CASE INTERIOR_V_APARTMENT_HIGH_11
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_11)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (11)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (11)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_12
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_12)
			
#IF NOT USE_TU_CHANGES
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
#ENDIF	//	NOT USE_TU_CHANGES
			
#IF USE_TU_CHANGES
			structInteriorData.vPos  = <<-20.1,-580.8,91.3>>
			//structInteriorData.vPos = <<-27.0,-582.5,89.9>>
#ENDIF	//	USE_TU_CHANGES
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (12)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (12)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_13
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_13)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (13)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (13)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_14
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_14)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (14)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (14)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_15
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_15)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (15)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (15)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_16
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_16)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (16)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (16)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_17
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_HIGH_APT_17)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (17)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (17)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_18
			structInteriorData.vPos = <<-470.3754, -698.5207, 51.5276>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (18)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (18)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_19
			structInteriorData.vPos =  <<-460.6133, -691.5562, 69.9067>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (19)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (19)"
			#ENDIF
		BREAK
		
		CASE INTERIOR_V_APARTMENT_HIGH_20
			structInteriorData.vPos = <<300.6330, -997.4288, -99.9727>>
			#IF NOT USE_TU_CHANGES
				structInteriorData.sInteriorName = "v_apartment_high"
				structInteriorData.sDebugName = "v_apartment_high (20)"
			#ENDIF
			#IF USE_TU_CHANGES
				structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
				structInteriorData.sDebugName = "hei_dlc_apart_high_new (20)"
			#ENDIF
		BREAK
		#IF USE_TU_CHANGES
		
		// In case anyone ever wondered: The locations for interiors are exactly the same (i.e. taken from) as in function 
		// GET_BASE_INTERIOR_LOCATION
		// from net_realty_details.sch
		
		CASE INTERIOR_V_APARTMENT_STILT_1_BASE_B
			structInteriorData.vPos = <<-171.3969, 494.2671, 134.4935>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_b"
			structInteriorData.sDebugName = "apa_v_mp_stilts_b (1)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_2_B
			structInteriorData.vPos = <<339.4982, 434.0887, 146.2206>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_b"
			structInteriorData.sDebugName = "apa_v_mp_stilts_b (2)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_3_B
			structInteriorData.vPos = <<-761.3884, 615.7333, 140.9805>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_b"
			structInteriorData.sDebugName = "apa_v_mp_stilts_b (3)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_4_B
			structInteriorData.vPos = <<-678.1752, 591.0076, 142.2196>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_b"
			structInteriorData.sDebugName = "apa_v_mp_stilts_b (4)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_5_BASE_A
			structInteriorData.vPos = <<120.0541, 553.7930, 181.0943>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = "apa_v_mp_stilts_a (5)"
				
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_7_A
			structInteriorData.vPos = <<-571.4039, 655.2008, 142.6293>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = "apa_v_mp_stilts_a (7)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_8_A
			structInteriorData.vPos = <<-742.2565, 587.6547, 143.0577>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = "apa_v_mp_stilts_a (8)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_10_A
			structInteriorData.vPos = <<-857.2222, 685.0510, 149.6502>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = "apa_v_mp_stilts_a (10)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_12_A
			structInteriorData.vPos = <<-1287.6498, 443.2707, 94.6919>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = "apa_v_mp_stilts_a (12)"
		BREAK
		CASE INTERIOR_V_APARTMENT_STILT_13_A
			structInteriorData.vPos = <<374.2012, 416.9688, 142.5991>>
			
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = "apa_v_mp_stilts_a (13)"
		BREAK
		#ENDIF
		
		CASE INTERIOR_DT1_03_CARPARK
			structInteriorData.vPos = <<-16.295849, -684.038513, 33.508316>>
			structInteriorData.sInteriorName = "dt1_03_carpark"
			structInteriorData.sDebugName = "dt1_03_carpark"
		BREAK

		CASE INTERIOR_V_APART_MIDSPAZ
			structInteriorData.vPos = <<341.1, -1000.0, -99.2>>
			structInteriorData.sInteriorName = "v_apart_midspaz"
			structInteriorData.sDebugName = "v_apart_midspaz"
		BREAK

		CASE INTERIOR_V_GARAGEM_SP
			structInteriorData.vPos = <<199.971558, -1018.954163, -100.000000>>
			structInteriorData.sInteriorName = "v_garagem_sp"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_BAHAMA
			structInteriorData.vPos = <<-1388.0013, -618.4197, 30.8196>>
			structInteriorData.sInteriorName = "v_bahama"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		#IF NOT USE_TU_CHANGES
		DEFAULT
			CPRINTLN(DEBUG_BUILDING, "GET_INTERIOR_DATA() ERROR - INTERIOR NOT FOUND:", ENUM_TO_INT(eInterior), ".")
		BREAK
		#ENDIF
	ENDSWITCH
	#IF USE_TU_CHANGES
	SWITCH eInterior
		CASE INTERIOR_V_CUSTOM_A_1
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_2
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_3
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_03"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_4
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_04"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_5
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_05"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_6
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_06"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_7
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_07"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_A_8
			structInteriorData.vPos = <<-787.7805, 334.9232, 215.8384>>
			structInteriorData.sInteriorName = "apa_v_mp_h_08"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_1
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_2
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_3
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_03"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_4
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_04"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_5
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_05"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_6
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_06"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_7
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_07"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_B_8
			structInteriorData.vPos = <<-773.2258, 322.8252, 194.8862>>
			structInteriorData.sInteriorName = "apa_v_mp_h_08"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_1
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_2
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_3
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_03"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_4
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_04"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_5
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_05"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_6
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_06"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_7
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_07"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CUSTOM_C_8
			structInteriorData.vPos = <<-787.7805, 334.9232, 186.1134>>
			structInteriorData.sInteriorName = "apa_v_mp_h_08"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		

		//Office 1
		CASE INTERIOR_V_OFFICE_1_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_4
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_5
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_6
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_7
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_8
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_9
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		//Office 2
		CASE INTERIOR_V_OFFICE_2_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_4
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_5
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_6
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_7
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_8
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_9
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_BASE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		//Office 3
		CASE INTERIOR_V_OFFICE_3_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_4
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_5
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_6
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_7
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_8
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_9
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		//Office 4
		CASE INTERIOR_V_OFFICE_4_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_01c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_4
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_5
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_6
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_02c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_7
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03a_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_8
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03b_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_9
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "ex_int_office_03c_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CLUBHOUSE_1_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_CLUBHOUSE_1_BASE_A)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "bkr_biker_dlc_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CLUBHOUSE_7_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_CLUBHOUSE_7_BASE_B)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "bkr_biker_dlc_int_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_GAR_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1_GARAGE_LVL1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_GAR_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1_GARAGE_LVL2)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_GAR_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1_GARAGE_LVL3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_GAR_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_GARAGE_LVL1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_GAR_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_GARAGE_LVL2)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_GAR_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_GARAGE_LVL3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_GAR_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3_GARAGE_LVL1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_GAR_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3_GARAGE_LVL2)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_GAR_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3_GARAGE_LVL3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_GAR_1
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4_GARAGE_LVL1)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_GAR_2
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4_GARAGE_LVL2)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_GAR_3
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4_GARAGE_LVL3)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_impexp_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_1_MOD
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_1_GARAGE_LVL1,TRUE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_imptexp_mod_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_2_MOD
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_2_GARAGE_LVL1,TRUE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_imptexp_mod_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_3_MOD
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_3_GARAGE_LVL1,TRUE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_imptexp_mod_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OFFICE_4_MOD
			tempStruct = GET_BASE_INTERIOR_LOCATION(PROPERTY_OFFICE_4_GARAGE_LVL1,TRUE)
			structInteriorData.vPos = tempStruct.vLoc +<<0,0,1>>
			structInteriorData.sInteriorName = "imp_imptexp_mod_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_BUNKER
			structInteriorData.vPos = <<938.307678, -3196.111572, -100.000000>>
			structInteriorData.sInteriorName = "gr_grdlc_int_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SUB
			structInteriorData.vPos = <<512.5, 4852.0, -62.6>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_sub"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_IAA
			structInteriorData.vPos = <<2047.0, 2942.0, -61.9>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_facility"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_LIFEINVADER
			structInteriorData.vPos = <<-1047.5997, -232.3503, 38.0135>>
			structInteriorData.sInteriorName = "v_faceoffice"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_FOUNDRY
			structInteriorData.vPos = <<1100, -2004, 37>>
			structInteriorData.sInteriorName = "v_foundry"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SILO_1
			structInteriorData.vPos = <<361, 6306, -159>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_silo_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK		
		CASE INTERIOR_V_SILO_2
			structInteriorData.vPos = <<305, 6298, -160>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_silo_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SILO_3
			structInteriorData.vPos = <<244, 6163, -159>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_lab"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SERVER_FARM
			structInteriorData.vPos = <<2168, 2920, -84>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_facility2"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SILO_TUNNEL
			structInteriorData.vPos = <<446, 5922, -158>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_bse_tun"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SILO_LOOP
			structInteriorData.vPos = <<252, 5972, -156>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_base_loop"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SILO_ENTRANCE
			structInteriorData.vPos = <<682, 5959, -152>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_base_ent"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SILO_BASE
			structInteriorData.vPos = <<551, 5939, -158>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_base"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_OSPREY
			structInteriorData.vPos = <<520.0001, 4750.0000, -70.0000>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_HANGAR
			structInteriorData.vPos =  <<-1266.8022, -3014.8491, -49.4903>>
			structInteriorData.sInteriorName =  "sm_smugdlc_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_IMPORT_WAREHOUSE
			structInteriorData.vPos =  <<974.9203, -3000.0647, -40.6470>>
			structInteriorData.sInteriorName = "imp_impexp_intwaremed"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY
			structInteriorData.vPos = <<969.5376, -3000.4111, -48.6470>>
			structInteriorData.sInteriorName =  "imp_impexp_int_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S
			structInteriorData.vPos = <<1094.9966, -3100.0117, -39.9999>>
			structInteriorData.sInteriorName =  "ex_int_warehouse_s_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M
			structInteriorData.vPos =  <<1059.9949, -3100.0000, -39.9999>>
			structInteriorData.sInteriorName = "ex_int_warehouse_m_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L
			structInteriorData.vPos =  <<1010.0083, -3100.0000, -39.9999>>
			structInteriorData.sInteriorName = "ex_int_warehouse_l_dlc"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_STILT_APARTMENT
			structInteriorData.vPos =  <<372.6707, 405.5235, 144.5326>>
			structInteriorData.sInteriorName = "apa_v_mp_stilts_a"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_HIGH_END_APARTMENT
			structInteriorData.vPos = <<-282.0588, -955.1700, 85.3036>>
			structInteriorData.sInteriorName = "hei_dlc_apart_high_new"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_MEDIUM_END_APARTMENT
			structInteriorData.vPos =  <<342.7946, -997.4225, -99.7444>>
			structInteriorData.sInteriorName = "v_apart_midspaz"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_LOW_END_APARTMENT
			structInteriorData.vPos =  <<260.3268, -997.4298, -100.0086>>
			structInteriorData.sInteriorName = "v_studio_lo"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_FIB_OFFICE
			structInteriorData.vPos =  <<108.2369, -753.5364, 233.1523>>
			structInteriorData.sInteriorName = "v_fib01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_FIB_OFFICE_2
			structInteriorData.vPos = <<135.3226, -746.3677, 253.1523>>
			structInteriorData.sInteriorName = "v_fib03"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_FIB_LOBBY
			structInteriorData.vPos = <<108.2572, -753.5342, 44.7548>>
			structInteriorData.sInteriorName = "v_office_lobby"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_HIGH_END_GARAGE
			structInteriorData.vPos = <<228.6161, -992.0530, -99.9999>>
			structInteriorData.sInteriorName = "heist_dlc_garage_high_new"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_APART_MIDSPAZ_2
			structInteriorData.vPos =  <<575.0, 4750.0, -60.0>>
			structInteriorData.sInteriorName = "xm_v_apart_midspaz"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_STUDIO_LO
			structInteriorData.vPos =  <<600.0, 4750.0, -60.0>>
			structInteriorData.sInteriorName = "xm_v_studio_lo"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_GARAGEM_2
			structInteriorData.vPos =  <<630.0, 4750.0, -60.0>>
			structInteriorData.sInteriorName = "xm_v_garagem"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_ENTRY
			structInteriorData.vPos =  <<1257, 4796.7, -39.1>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_entry"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_STRAIGHT_0
			structInteriorData.vPos =  <<694.4, 5898.9, -152.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_straight"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_STRAIGHT_1
			structInteriorData.vPos = <<1121.8, 5516.3, -101.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_straight"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_STRAIGHT_2
			structInteriorData.vPos = <<1279.6, 5233.2, -80.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_straight"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_SLOPE_FLAT_0
			structInteriorData.vPos = <<1158.6, 5467.1, -101.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_slope_flat"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_SLOPE_FLAT_1
			structInteriorData.vPos = <<705.9, 5838.5, -152.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_slope_flat"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_SLOPE_FLAT_2
			structInteriorData.vPos = <<1316.5, 5184.0, -80.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_slope_flat"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_FLAT_SLOPE_0
			structInteriorData.vPos = <<1248.0, 5276.1, -80.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_flat_slope"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_FLAT_SLOPE_1
			structInteriorData.vPos = <<1090.2, 5559.2, -101.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_flat_slope"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_FLAT_SLOPE_2
			structInteriorData.vPos = <<1261.0, 4808.6, -39.3>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_flat_slope"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_RIGHT_0
			structInteriorData.vPos = <<721.8, 5781.4,-146.7 >>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_r"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_RIGHT_1
			structInteriorData.vPos =  <<780.8, 5703.4, -136.7>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_r"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_RIGHT_2
			structInteriorData.vPos = <<868.1, 5659.2, -126.7>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_r"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_RIGHT_3
			structInteriorData.vPos = <<1218.2, 5321.2, -85.7>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_r"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_RIGHT_4
			structInteriorData.vPos = <<1278.5, 4859.7, -44.7>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_r"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_LEFT_0
			structInteriorData.vPos = <<1187.2, 5419.8, -96.4>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_l"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_LEFT_1
			structInteriorData.vPos = <<1344.7, 5136.4, -75.4>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_l"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_LEFT_2
			structInteriorData.vPos = <<1363.4, 5039.5, -65.4>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_l"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_LEFT_3
			structInteriorData.vPos = <<1049.3, 5602.1, -107.4>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_l"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_LEFT_4
			structInteriorData.vPos = <<1337.8, 4944.3, -55.4>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_l"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_TUNNEL_30D_LEFT_5
			structInteriorData.vPos = <<961.5, 5646.9, -117.4>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_tun_30d_l"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_FACILITY
			structInteriorData.vPos = <<345.0041, 4842.0010, -59.9997>>
			structInteriorData.sInteriorName = "xm_x17dlc_int_02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_CORONER
			structInteriorData.vPos = <<279.9322, -1337.4902, 23.7419>>
			structInteriorData.sInteriorName = "v_coroner"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_NIGHTCLUB
			structInteriorData.vPos = <<-1604.6643, -3012.5828, -79.9999>>
			structInteriorData.sInteriorName = "ba_dlc_int_01_ba"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_BUSINESS_HUB
		CASE INTERIOR_V_NIGHTCLUB_BASEMENT
			structInteriorData.vPos = <<-1505.7830, -3012.5867, -79.9999>>
			structInteriorData.sInteriorName = "ba_dlc_int_02_ba"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_JEWEL_STORE
			structInteriorData.vPos = <<-630.4205, -236.7843, 37.0570>>
			structInteriorData.sInteriorName = "V_JEWEL2"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_ARENA
			structInteriorData.vPos = <<2800.0, -3800.0, 100.0>>
			
			structInteriorData.sInteriorName = "xs_x18_int_01"
			
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_ARENA_VIP_BOX
			structInteriorData.vPos = <<2800.0, -3942.0, 182.5>>
			
			structInteriorData.sInteriorName = "xs_arena_vip"
			
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_BUILDING, "GET_INTERIOR_DATA() ERROR - INTERIOR NOT FOUND:", ENUM_TO_INT(eInterior), ".")
		BREAK	
		
		CASE INTERIOR_V_WEED_FARM
			
			structInteriorData.vPos = <<1049.6, -3196.6, -38.5>>
			structInteriorData.sInteriorName = "bkr_biker_dlc_int_ware02"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_ALT_WEED_FARM
			structInteriorData.vPos = <<2920.0, 4470.0, -100.0>>
			structInteriorData.sInteriorName = "sf_dlc_warehouse_sec"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_COCAINE_FACTORY
			structInteriorData.vPos = <<1093.6, -3196.6, -38.5>>
			structInteriorData.sInteriorName = "bkr_biker_dlc_int_ware03"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_METH_FACTORY
			structInteriorData.vPos = <<1009.5, -3196.6, -38.5>>
			structInteriorData.sInteriorName = "bkr_biker_dlc_int_ware01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO
			structInteriorData.vPos = <<1100.0, 245.0, -49.0>>
			structInteriorData.sInteriorName = "vw_dlc_casino_main"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_CAR_PARK
			structInteriorData.vPos = <<1380.0, 200.0, -50.0>>
			structInteriorData.sInteriorName = "vw_dlc_casino_carpark"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_APARTMENT
			structInteriorData.vPos = <<976.636414, 70.294762, 115.164131>>
			structInteriorData.sInteriorName = "vw_dlc_casino_apart"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_GARAGE
			structInteriorData.vPos = <<1295.0, 230.0, -50.0>>
			structInteriorData.sInteriorName = "vw_dlc_casino_garage"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_MAIN
			structInteriorData.vPos = <<2479.3, -273.9, -58.0>>
			structInteriorData.sInteriorName = "ch_DLC_Casino_Heist"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_ARCADE
			structInteriorData.vPos = <<2730.0, -380.0, -49.0>>
			structInteriorData.sInteriorName = "ch_DLC_Arcade"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_HEIST_PLANNING
			structInteriorData.vPos = <<2800.0, -380.0, -48.5>>
			structInteriorData.sInteriorName = "ch_DLC_Plan"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_TUNNEL
			structInteriorData.vPos = <<2497.7, -312.8, -69.9>>
			structInteriorData.sInteriorName = "ch_DLC_Tunnel"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_BACK_AREA
			structInteriorData.vPos = <<2523.4, -270.0, -58.7>>
			structInteriorData.sInteriorName = "ch_DLC_Casino_Back"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_HOTEL_FLOOR
			structInteriorData.vPos = <<2504.4, -257.2, -39.1>>
			structInteriorData.sInteriorName = "ch_DLC_Casino_Hotel"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_LOADING_BAY
			structInteriorData.vPos = <<2554.0, -281.4, -64.7>>
			structInteriorData.sInteriorName = "ch_DLC_Casino_Loading"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_VAULT
			structInteriorData.vPos = <<2488.3, -267.4, -70.6>>
			structInteriorData.sInteriorName = "ch_DLC_Casino_Vault"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_UTILITY_LIFT
			structInteriorData.vPos = <<2519.9, -255.3, -24.1>>
			structInteriorData.sInteriorName = "ch_DLC_Casino_Utility"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CASINO_LIFT_SHAFT
			structInteriorData.vPos = <<2572.9, -253.4, -64.7>>
			structInteriorData.sInteriorName = "ch_dlc_casino_shaft"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_CUSTOM_MISSION_MOC
			structInteriorData.vPos = <<1103.562378, -3000.0, -40.0>>
			structInteriorData.sInteriorName = "gr_grdlc_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_METH_FACTORY_MURRIETA
			structInteriorData.vPos = <<1210.0, 1857.0, -50.0>>
			structInteriorData.sInteriorName = "tr_Tuner_MethLab_1"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_METH_FACTORY_EAST_VINEWOOD
			structInteriorData.vPos = <<1569.0, -2130.0, -50.0>>
			structInteriorData.sInteriorName = "tr_Tuner_MethLab_1"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_METH_FACTORY_SENORA_DESERT_1
			structInteriorData.vPos = <<839.0, 2176.0, -50.0>>
			structInteriorData.sInteriorName = "tr_Tuner_MethLab_1"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		CASE INTERIOR_V_METH_FACTORY_SENORA_DESERT_2
			structInteriorData.vPos = <<982.0, -143.0, -50.0>>
			structInteriorData.sInteriorName = "tr_Tuner_MethLab_1"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_TUNER_CAR_MEET
			structInteriorData.vPos = <<-2000.0, 1113.4, 25.7>>
			structInteriorData.sInteriorName = "tr_tuner_car_meet"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_TUNER_MOD_GARAGE
			structInteriorData.vPos = <<-1350.0, 160.0, -99.2>>
			structInteriorData.sInteriorName = "tr_tuner_mod_garage"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_UNION_DEPOSITORY
			structInteriorData.vPos = <<1.1, -705.6, 16.1>>
			structInteriorData.sInteriorName = "finbank"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_RECORDING_STUDIO
			structInteriorData.vPos = <<-1010, -70, -99.4>>
			structInteriorData.sInteriorName = "sf_dlc_studio_sec"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_MUSIC_LOCKER
			structInteriorData.vPos = <<1550.0, 250.0, -50.0>>
			structInteriorData.sInteriorName = "h4_dlc_int_02_h4"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_DEVIN_HANGAR
			structInteriorData.vPos =  <<-935.7, -2992.2, 13.9>>
			structInteriorData.sInteriorName = "v_hanger"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_OFFICE_AUTOSHOP
			structInteriorData.vPos =  <<730.0, -2990.0, -39.0>>
			structInteriorData.sInteriorName = "imp_imptexp_mod_int_01"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_MOTEL
			structInteriorData.vPos = <<152.3, -1004.4, -97.8>>
			structInteriorData.sInteriorName = "v_motel_mp"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1
			structInteriorData.vPos = <<170.0, 5190.0, 10.0>>
			structInteriorData.sInteriorName = "xs_x18_int_mod2"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_MISSION_BASEMENT
			structInteriorData.vPos = <<850.0, -3000.0, -50.0>>
			structInteriorData.sInteriorName = "reh_dlc_int_04_sum2"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_PHARMA_OFFICE
			structInteriorData.vPos = <<495, -2560, -50.0>>
			structInteriorData.sInteriorName = "xm3_DLC_INT_04_xm3"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
		
		CASE INTERIOR_V_AUTOCARE_WAREHOUSE
			structInteriorData.vPos = <<584, -2605, -50.0>>
			structInteriorData.sInteriorName = "xm3_DLC_INT_03_xm3"
			structInteriorData.sDebugName = structInteriorData.sInteriorName
		BREAK
	ENDSWITCH
	#ENDIF
	
	RETURN structInteriorData
ENDFUNC

/// PURPOSE:
///    Gets the bitset variable and bit for a particular interior enum
/// PARAMS:
///    eInterior - The interior enum
/// RETURNS:
///    A struct containing an array index to a bitset var, and a bit index.
FUNC INTERIOR_BITSET_STRUCT GET_BITSET_FOR_INTERIOR_ENUM(INTERIOR_NAME_ENUM eInterior)

	INTERIOR_BITSET_STRUCT sBitset
	sBitset.iIndex = ENUM_TO_INT(eInterior) % 32
	sBitset.iArrayIndex = ENUM_TO_INT(eInterior) / 32
	
	IF sBitset.iArrayIndex >= MAX_INTERIOR_BITSETS
		SCRIPT_ASSERT("GET_BITSET_FOR_INTERIOR_ENUM - Ran out of bitsets. Please increase MAX_INTERIOR_BITSETS")
		CPRINTLN(DEBUG_BUILDING, "GET_BITSET_FOR_INTERIOR_ENUM - MAX_INTERIOR_BITSETS = ", MAX_INTERIOR_BITSETS, ", sBitset.iArrayIndex = ", sBitset.iArrayIndex)
		sBitset.iIndex = -1
		sBitset.iArrayIndex = -1
	ENDIF
	
	RETURN sBitset

ENDFUNC

/// PURPOSE:
///    Checks if an interior has set to be capped on exit.
/// PARAMS:
///    eInterior - The interior enum to check.
/// RETURNS:
///    TRUE if the interior has been set to be capped on exit, FALSE otherwise
FUNC BOOL IS_INTERIOR_CAPPED_ON_EXIT( INTERIOR_NAME_ENUM eInterior )

	INTERIOR_BITSET_STRUCT sBitset = GET_BITSET_FOR_INTERIOR_ENUM(eInterior)
	
	IF sBitset.iArrayIndex != -1
	AND IS_BIT_SET(iInteriorCappingBitset[sBitset.iArrayIndex],sBitset.iIndex)  
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if an interior has set to be disabled on exit.
/// PARAMS:
///    eInterior - The interior enum to check.
/// RETURNS:
///    TRUE if the interior has been set to be disabled on exit, FALSE otherwise
FUNC BOOL IS_INTERIOR_DISABLED_ON_EXIT( INTERIOR_NAME_ENUM eInterior )

	INTERIOR_BITSET_STRUCT sBitset = GET_BITSET_FOR_INTERIOR_ENUM(eInterior)
	
	IF sBitset.iArrayIndex != -1
	AND IS_BIT_SET(iInteriorDisablingBitset[sBitset.iArrayIndex],sBitset.iIndex)  
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Flags an interior to be capped when the player exits - at least 250m away.
/// PARAMS:
///    eInterior - The interior to check
///    bCapped - TRUE to cap on exit, FALSE to stop capping on exit.
PROC SET_INTERIOR_CAPPED_ON_EXIT( INTERIOR_NAME_ENUM eInterior, BOOL bCapped = TRUE )

	INTERIOR_BITSET_STRUCT sBitset = GET_BITSET_FOR_INTERIOR_ENUM(eInterior)
	
	#IF IS_DEBUG_BUILD
		INTERIOR_DATA_STRUCT   sData = GET_INTERIOR_DATA(eInterior)
	#ENDIF
	
	IF sBitset.iArrayIndex = -1
		EXIT
	ENDIF
	
	IF bCapped
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED_ON_EXIT() - '",GET_THIS_SCRIPT_NAME(), "' SET this interior to be CAPPED on player exit ", sData.sDebugName )
		#ENDIF
		SET_BIT(iInteriorCappingBitset[sBitset.iArrayIndex],sBitset.iIndex)  	
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED_ON_EXIT() - '",GET_THIS_SCRIPT_NAME(), "' UNSET this interior to be CAPPED on player exit ", sData.sDebugName )
		#ENDIF
		CLEAR_BIT(iInteriorCappingBitset[sBitset.iArrayIndex],sBitset.iIndex)  
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Flags an interior to be disabled when the player exits - at least 250m away.
/// PARAMS:
///    eInterior - The interior to check
///    bCapped - TRUE to disable on exit, FALSE to stop disabling on exit.
PROC SET_INTERIOR_DISABLED_ON_EXIT( INTERIOR_NAME_ENUM eInterior, BOOL bDisabled = TRUE )

	INTERIOR_BITSET_STRUCT sBitset = GET_BITSET_FOR_INTERIOR_ENUM(eInterior)
	
	#IF IS_DEBUG_BUILD
		INTERIOR_DATA_STRUCT   sData = GET_INTERIOR_DATA(eInterior)
	#ENDIF
	
	IF sBitset.iArrayIndex = -1
		EXIT
	ENDIF
	
	IF bDisabled
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED_ON_EXIT() - '",GET_THIS_SCRIPT_NAME(), "' SET this interior to be DISABLED on player exit ", sData.sDebugName )
		#ENDIF
		SET_BIT(iInteriorDisablingBitset[sBitset.iArrayIndex],sBitset.iIndex)  	
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED_ON_EXIT() - '",GET_THIS_SCRIPT_NAME(), "' UNSET this interior to be DISABLED on player exit ", sData.sDebugName )
		#ENDIF
		CLEAR_BIT(iInteriorDisablingBitset[sBitset.iArrayIndex],sBitset.iIndex)  
	ENDIF
	

ENDPROC



/// PURPOSE:
///    Gets the interior instance index of a specified interior
/// PARAMS:
///    eInterior - 			Enum for the interior
///    &iInteriorIndex - 	Interior instance index, passed by reference
/// RETURNS:
///    A string containing the interior name.
FUNC STRING GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR( INTERIOR_NAME_ENUM eInterior, INTERIOR_INSTANCE_INDEX &iInteriorIndex )

	INTERIOR_DATA_STRUCT structInteriorData
	
	structInteriorData = GET_INTERIOR_DATA( eInterior )
		
	iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
	
	RETURN structInteriorData.sDebugName

ENDFUNC

/// PURPOSE: Wrapper for the CAP_INTERIOR command, prints debug info so we can keep track of which scripts are capping/uncapping interiors.
///    
/// PARAMS:
///    eInterior - Enum for interior
///    bIsCapped - TRUE if the interior is capped.
///    
PROC SET_INTERIOR_CAPPED( INTERIOR_NAME_ENUM eInterior, BOOL bIsCapped )

	STRING sInteriorName = "NULL"
	INTERIOR_INSTANCE_INDEX iInteriorIndex = NULL
	
	sInteriorName = GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(eInterior, iInteriorIndex)
	
	IF NOT ARE_STRINGS_EQUAL( "NONE", sInteriorName)
	AND iInteriorIndex != NULL
				
		IF bIsCapped AND NOT IS_INTERIOR_CAPPED(iInteriorIndex)			
			
			// Re-implemented this due to B* 1632138. Was originally included in CL 4746629 but removed because it caused other scripts to 
			// include it, so we used a different method which doesn't seem to work. Steve R LDS.
			#IF USE_TU_CHANGES 
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = iInteriorIndex
				CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - '", GET_THIS_SCRIPT_NAME(), "' CANNOT CAP ", sInteriorName," BECAUSE PLAYER IS INSIDE THE INTERIOR. RUNNING SET_INTERIOR_CAPPED_ON_EXIT INSTEAD.")
				SET_INTERIOR_CAPPED_ON_EXIT(eInterior, TRUE)
				EXIT
			ENDIF
			#ENDIF
			
			CAP_INTERIOR(iInteriorIndex, TRUE)
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - '", GET_THIS_SCRIPT_NAME(), "' CAPPED interior ", sInteriorName )
		
		ELIF NOT bIsCapped AND IS_INTERIOR_CAPPED(iInteriorIndex)
			
			IF IS_INTERIOR_CAPPED_ON_EXIT(eInterior)
				CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - '",GET_THIS_SCRIPT_NAME(), "' WARNING: ",sInteriorName," was set to CAP on exit, but this will be cleared - case 1" )
				SET_INTERIOR_CAPPED_ON_EXIT(eInterior, FALSE)
			ENDIF
			
			CAP_INTERIOR(iInteriorIndex, FALSE)
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - '", GET_THIS_SCRIPT_NAME(), "' UN-CAPPED interior ", sInteriorName )
		ELSE
		
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - '", GET_THIS_SCRIPT_NAME(), "' trying to set state ",bIsCapped," but we're already in that state for interior ", sInteriorName )
			
			IF NOT bIsCapped
				 IF IS_INTERIOR_CAPPED_ON_EXIT(eInterior)
					CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - '", GET_THIS_SCRIPT_NAME(), "' WARNING: ",sInteriorName," was set to CAP on exit, but this will be cleared - case 2" )
					SET_INTERIOR_CAPPED_ON_EXIT(eInterior, FALSE)
				ENDIF
			ENDIF
			
		ENDIF
							
	ELSE
		CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_CAPPED() - Can't get valid interior instance index for ", sInteriorName, " Called by script ",GET_THIS_SCRIPT_NAME())	
	ENDIF
	
ENDPROC

/// PURPOSE: Wrapper for the DISABLE_INTERIOR command, prints debug info so we can keep track of which scripts are disabling/enabling interiors.
///    
/// PARAMS:
///    eInterior - Enum for interior
///    bIsCapped - TRUE if the interior is capped.
///     
PROC SET_INTERIOR_DISABLED( INTERIOR_NAME_ENUM eInterior, BOOL bIsDisabled )

	STRING sInteriorName = "NULL"
	INTERIOR_INSTANCE_INDEX iInteriorIndex = NULL
	
	sInteriorName = GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(eInterior, iInteriorIndex)
	
	IF NOT ARE_STRINGS_EQUAL( "NONE", sInteriorName)
	AND iInteriorIndex != NULL
		IF bIsDisabled
			IF IS_INTERIOR_DISABLED(iInteriorIndex)
				CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - '", GET_THIS_SCRIPT_NAME(), "' DISABLED interior  ", sInteriorName, " was already disabled" )
				EXIT
			ENDIF
			
			// Re-implemented this due to B* 1632138. Was originally included in CL 4746629 but removed because it caused other scripts to 
			// include it, so we used a different method which doesn't seem to work. Steve R LDS.
			#IF USE_TU_CHANGES
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = iInteriorIndex
				CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - '", GET_THIS_SCRIPT_NAME(), "' CANNOT DISABLE ", sInteriorName," BECAUSE PLAYER IS INSIDE THE INTERIOR. RUNNING SET_INTERIOR_DISABLED_ON_EXIT INSTEAD.")
				SET_INTERIOR_DISABLED_ON_EXIT(eInterior, TRUE)
				EXIT
			ENDIF
			#ENDIF
			
		ELSE
			IF NOT IS_INTERIOR_DISABLED(iInteriorIndex)
				CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - '", GET_THIS_SCRIPT_NAME(), "' ENABLED interior  ", sInteriorName, " was already enabled" )
				EXIT
			ENDIF
			
			IF IS_INTERIOR_DISABLED_ON_EXIT(eInterior)
				CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - '", GET_THIS_SCRIPT_NAME(), "' WARNING: ",sInteriorName," was set to disable on exit, but this will be cleared." )
				SET_INTERIOR_DISABLED_ON_EXIT(eInterior, FALSE)
			ENDIF
			
		ENDIF
		DISABLE_INTERIOR(iInteriorIndex, bIsDisabled)
		IF bIsDisabled
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - '", GET_THIS_SCRIPT_NAME(), "' DISABLED interior ", sInteriorName )
		ELSE
			CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - '", GET_THIS_SCRIPT_NAME(), "' ENABLED interior ", sInteriorName )
		ENDIF
	ELSE
		CPRINTLN(DEBUG_BUILDING, "SET_INTERIOR_DISABLED() - Can't get valid interior instance index for ", sInteriorName, " Called by script ",GET_THIS_SCRIPT_NAME())	
	ENDIF
	
ENDPROC

/// PURPOSE: Activates special features associated with a building state, e.g. ambient audio zones.
///    
/// PARAMS:
///    eName - 		IPL swap name
///    eNewState -	Building state
PROC SET_BUILDING_STATE_ASSOCIATED_FEATURES( BUILDING_NAME_ENUM eName, BUILDING_STATE_ENUM eNewState )

	SWITCH eName
	
		CASE BUILDINGNAME_IPL_CARGOSHIP
		
			IF eNewState = BUILDINGSTATE_NORMAL
				SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_PORT_OF_LS_UNDERWATER_CREAKS", FALSE )
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - AZ_PORT_OF_LS_UNDERWATER_CREAKS is INACTIVE")
			ELSE
				SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_PORT_OF_LS_UNDERWATER_CREAKS", TRUE )
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - AZ_PORT_OF_LS_UNDERWATER_CREAKS is ACTIVE")
			ENDIF
			
		BREAK
		
		CASE BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR
			
			IF eNewState != BUILDINGSTATE_DESTROYED
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("HEIST_SWEATSHOP_ZONES", FALSE )
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - HEIST_SWEATSHOP_ZONES is INACTIVE")
			ELSE
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("HEIST_SWEATSHOP_ZONES", TRUE )
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - HEIST_SWEATSHOP_ZONES is ACTIVE")
			ENDIF
		
		BREAK
		
		CASE BUILDINGNAME_IPL_EXL3_TRAIN_CRASH
			
			IF eNewState = BUILDINGSTATE_DESTROYED
				SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_MAIN, FALSE)
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - TRAIN_LINE_MAIN is DISABLED")
			ELSE
				SCRIPT_SET_TRAIN_LINE_ACTIVE(TRAIN_LINE_MAIN, TRUE)
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - TRAIN_LINE_MAIN is ENABLED")
			ENDIF
		
		BREAK
		
		CASE BUILDINGNAME_IPL_CARGOPLANE
		
			IF eNewState = BUILDINGSTATE_DESTROYED
				SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_UNDERWATER_EXILE_01_PLANE_WRECK", TRUE )
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - AZ_UNDERWATER_EXILE_01_PLANE_WRECK is ACTIVE")
			ELSE
				SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_UNDERWATER_EXILE_01_PLANE_WRECK", FALSE )
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - AZ_UNDERWATER_EXILE_01_PLANE_WRECK is INACTIVE")
			ENDIF
			
		BREAK
		
		CASE BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS
		
			IF eNewState = BUILDINGSTATE_CLEANUP
				REMOVE_PORTAL_SETTINGS_OVERRIDE("V_CARSHOWROOM_PS_WINDOW_UNBROKEN")
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - PORTAL SETTINGS_OVERRIDE: V_CARSHOWROOM_PS_WINDOW_UNBROKEN")
			ENDIF
		
		BREAK
		
		CASE BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA
		
			IF eNewState = BUILDINGSTATE_DESTROYED
				SET_STATIC_EMITTER_ENABLED("TREVOR1_TRAILER_PARK_MAIN_STAGE_RADIO", FALSE)
				SET_STATIC_EMITTER_ENABLED("TREVOR1_TRAILER_PARK_MAIN_TRAILER_RADIO_01", FALSE)
				SET_STATIC_EMITTER_ENABLED("TREVOR1_TRAILER_PARK_MAIN_TRAILER_RADIO_02", FALSE)
				SET_STATIC_EMITTER_ENABLED("TREVOR1_TRAILER_PARK_MAIN_TRAILER_RADIO_03", FALSE)
				CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE_ASSOCIATED_FEATURES() - LOST TRAILER PARK STATIC RADIO EMITTERS DISABLED")
			ENDIF
		
		BREAK

	ENDSWITCH

ENDPROC


/// PURPOSE: Gets called on startup to ensure that the ambient audio zones and other special features associated with building states are restored correctly.
///   
PROC RESTORE_BUILDING_STATE_ASSOCIATED_FEATURES()

	CPRINTLN(DEBUG_BUILDING, "RESTORE_BUILDING_STATE_ASSOCIATED_FEATURES()")
	
	#if USE_CLF_DLC
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_CARGOSHIP, g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_CARGOSHIP] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH, g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_EXL3_TRAIN_CRASH] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_CARGOPLANE, g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_CARGOPLANE] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS])
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA])
	#endif
	#if USE_NRM_DLC
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_CARGOSHIP, g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_CARGOSHIP] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH, g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_EXL3_TRAIN_CRASH] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_CARGOPLANE, g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_CARGOPLANE] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS])
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA])
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_CARGOSHIP, g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_CARGOSHIP] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_EXL3_TRAIN_CRASH, g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_EXL3_TRAIN_CRASH] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_CARGOPLANE, g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_CARGOPLANE] )
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS])
		SET_BUILDING_STATE_ASSOCIATED_FEATURES(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA])
	#endif
	#endif
ENDPROC

/// PURPOSE:
///    Sets up all the interior states at startup.
PROC SETUP_INTERIOR_STATES()

	#IF IS_DEBUG_BUILD 
	// Enable all interiors.
	IF GET_COMMANDLINE_PARAM_EXISTS("allInteriorsOn")
		EXIT
	ENDIF
	#ENDIF

	// Init interior status bitsets

	INT i = 0
	WHILE i < MAX_INTERIOR_BITSETS
	
		iInteriorCappingBitset[i] = 0
		iInteriorDisablingBitset[i] = 0
		
	++i
	ENDWHILE
	
	// Enabling these individually when they get sorted out in the various mission scripts.

	SET_INTERIOR_CAPPED				(INTERIOR_V_RECYCLE,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_EPSILONISM,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_SHERIFF,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_SHERIFF2,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_ABATTOIR,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_FOUNDRY,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_CHOPSHOP,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_RANCH,					TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_ROCKCLUB,				TRUE)	
	SET_INTERIOR_CAPPED_ON_EXIT		(INTERIOR_DT1_03_CARPARK,			TRUE)
	
	SET_INTERIOR_CAPPED				(INTERIOR_V_SUB,					TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_APART_MIDSPAZ_2,		TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_STUDIO_LO,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_GARAGEM_2,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_OSPREY, 				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_FACILITY, 				TRUE)
	
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_ENTRY,			TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_STRAIGHT_0,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_STRAIGHT_1,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_STRAIGHT_2,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_SLOPE_FLAT_0,	TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_SLOPE_FLAT_1,	TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_SLOPE_FLAT_2,	TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_FLAT_SLOPE_0,	TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_FLAT_SLOPE_1,	TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_FLAT_SLOPE_2,	TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_RIGHT_0,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_RIGHT_1,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_RIGHT_2,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_RIGHT_3,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_RIGHT_4,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_LEFT_0,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_LEFT_1,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_LEFT_2,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_LEFT_3,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_LEFT_4,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_TUNNEL_30D_LEFT_5,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_ENTRANCE,			TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_LOOP,				TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_TUNNEL,			TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_BASE,				TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_1,					TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_2,					TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SILO_3,					TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_IAA,					TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_SERVER_FARM,			TRUE)
	
	SET_INTERIOR_DISABLED			(INTERIOR_V_COMEDY,					TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_PSYCHEOFFICE,			TRUE)
	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_CINEMA_VINEWOOD,		TRUE)
	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_CINEMA_MORNINGWOOD,		TRUE)
	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_CINEMA_DOWNTOWN,		TRUE)
	SET_INTERIOR_DISABLED			(INTERIOR_V_58_SOL_OFFICE,			TRUE)

	SET_INTERIOR_CAPPED				(INTERIOR_V_NIGHTCLUB,				TRUE)
	SET_INTERIOR_CAPPED				(INTERIOR_V_BUSINESS_HUB, 			TRUE)
	
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_MAIN, 			TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_TUNNEL, 		TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_BACK_AREA, 		TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_HOTEL_FLOOR, 	TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_LOADING_BAY, 	TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_VAULT, 			TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_UTILITY_LIFT, 	TRUE)
	SET_INTERIOR_DISABLED(INTERIOR_V_CASINO_LIFT_SHAFT,		TRUE)

	// CDM - Leave these to me I'll disabled them in Freemode script. Currently the spawning into interior fights with the building controller.
	// Moved to freemode.sc for MP
	#IF NOT USE_SP_DLC
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		AND NOT IS_TRANSITION_SESSION_LAUNCHING()
			CPRINTLN(DEBUG_BUILDING, "Building controler: disabling the MP property interiors as not net game in progress")
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_1,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_2,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_3,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_4,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_5,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_6,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_7,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_8,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_9,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_10,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_11,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_12,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_13,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_14,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_15,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_16,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_17,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_18,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_19,	TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_20,	TRUE)
		 	SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APARTMENT_HIGH_19,  TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_APART_MIDSPAZ,  	TRUE)
			
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGES,			TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGEM,			TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGEL,			TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT	(INTERIOR_V_GARAGEM_SP,			TRUE)
		ENDIF
	#ENDIF
ENDPROC

PROC REMOVE_DOOR_FROM_SYSTEM_SAFELY(INT iDoorHash)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
		CPRINTLN(DEBUG_DOOR, "REMOVE_DOOR_FROM_SYSTEM_SAFELY - removing door with hash ", iDoorHash)
		REMOVE_DOOR_FROM_SYSTEM(iDoorHash)
	ENDIF
ENDPROC

/// PURPOSE: This gets called on startup to ensure all mission specific doors are unregistered from the system
PROC REMOVE_ALL_MISSION_SPECIFIC_DOORS_FROM_SYSTEM_ON_STARTUP()

	CPRINTLN(DEBUG_DOOR, "REMOVE_ALL_MISSION_SPECIFIC_DOORS_FROM_SYSTEM_ON_STARTUP()")
	
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("DOORHASH_OPEN_DOOR"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("DOORHASH_CLOSED_DOOR1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("DOORHASH_CLOSED_DOOR2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_4"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_5"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_6"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_7"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_8"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_9"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_10"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_11"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_12"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_13"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_1_door_14"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_4"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_7"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_8"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_9"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_10"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_11"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_12"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_13"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_14"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_15"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_16"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_17"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_18"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_19"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_20"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_21"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_22"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_23"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_24"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_25"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("fbi_5_door_26"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("CUPBOARD_DOOR"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("LEFT_RECEPTION_DOOR"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("RIGHT_RECEPTION_DOOR"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("CCTV_ROOM_DOOR"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Josh2_door_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Josh2_door_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Josh2_door_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Josh2_door_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Josh2_door_4"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Josh2_door_5"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("GRAVEYARD_GATE_WEST_L"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("GRAVEYARD_GATE_WEST_R"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("GRAVEYARD_GATE_EAST_L"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("GRAVEYARD_GATE_EAST_R"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("GRAVEYARD_GATE_SOUTH_L"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("GRAVEYARD_GATE_SOUTH_R"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_4"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_5"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_6"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_7"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_8"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_9"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Gate_10"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("WAREHOUSE_LEFT"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("WAREHOUSE_RIGHT"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("DOORHASH_F_HOUSE_SC_F"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("Finale_Bank_Exit_Barrier"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1A_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1A_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1A_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1A_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1A_4"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1A_5"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1B_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1B_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1B_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1B_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1B_4"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("N1B_5"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("LeftLockup"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("RightLockup"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("LESTER1A_DOOR_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("LESTER1A_DOOR_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("LESTER1A_DOOR_2"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("LESTER1A_DOOR_3"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("WrongLocationGateA"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("WrongLocationGateB"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("AH3B_DOOR_0"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("AH3B_DOOR_1"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("AH3B_DOOR_18"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("AH3B_DOOR_19"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_2B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_3A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_3B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_4A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_4B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_1A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_1B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_5A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_GAS_5B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_LIQUOR_1A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_LIQUOR_2A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_LIQUOR_3A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_LIQUOR_4A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_LIQUOR_5A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_3A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_3B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_2A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_2B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_4A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_4B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_5A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_5B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_6A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_6B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_7A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_7B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_8A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_8B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_9A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_9B"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_10A"))
	REMOVE_DOOR_FROM_SYSTEM_SAFELY(HASH("eCRIM_HUP_SHOP247_10B"))
ENDPROC

/// PURPOSE: Returns TRUE if the default states have been set for the buildings and doors
FUNC BOOL HAS_DEFAULT_BUILDING_DATA_BEEN_SET()
	IF USE_SP_BUILDING_CONTROLLER_DATA()
		#if USE_CLF_DLC
			RETURN g_savedGlobalsClifford.sBuildingData.bDefaultDataSet
		#endif
		#if USE_NRM_DLC
			RETURN g_savedGlobalsnorman.sBuildingData.bDefaultDataSet
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			RETURN g_savedGlobals.sBuildingData.bDefaultDataSet
		#endif
		#endif
	ELSE
		RETURN g_MPBuildingData.bDefaultDataSet
	ENDIF
ENDFUNC

/// PURPOSE: This gets called on startup to ensure the map data is in sync before code warp the player to the last saved position.
PROC INITIALISE_STORED_BUILDING_STATES_ON_STARTUP( BOOL bIgnoreDistanceChecks = FALSE )

//	DEBUG_PRINTCALLSTACK()
//	SCRIPT_ASSERT("INITIALISE_STORED_BUILDING_STATES_ON_STARTUP()")

	CPRINTLN(DEBUG_BUILDING, "INITIALISE_STORED_BUILDING_STATES_ON_STARTUP() bIgnoreDistanceChecks = ", bIgnoreDistanceChecks)
	
	IF HAS_DEFAULT_BUILDING_DATA_BEEN_SET()
	
		CPRINTLN(DEBUG_BUILDING, "...default data has been set so initialise all the building states")
		INT i
		REPEAT NUMBER_OF_BUILDINGS i
			g_bBuildingStateUpdateDelay[i] = TRUE // This flag will get ignored when this proc is called from the startup script.
			PERFORM_BUILDING_SWAP(INT_TO_ENUM(BUILDING_NAME_ENUM, i), TRUE, bIgnoreDistanceChecks )
		ENDREPEAT
		
		// Initialise any ambient audio zones assoUciated with building states.
		RESTORE_BUILDING_STATE_ASSOCIATED_FEATURES()
		
		// Cap or disable specified interiors.
		SETUP_INTERIOR_STATES()
				
	ELSE
		CPRINTLN(DEBUG_BUILDING, "...default data has not been set so skip initialisation")
	ENDIF	

	g_bInitialBuildingStatesSet = TRUE
ENDPROC



/// PURPOSE: Sets all the default building states and locks all the doors
PROC SETUP_DEFAULT_BUILDING_DATA()

	IF USE_SP_BUILDING_CONTROLLER_DATA()
		CPRINTLN(DEBUG_BUILDING, "Setting default building data for single-player.")
	ELSE
		CPRINTLN(DEBUG_BUILDING, "Setting default building data for multi-player.")
	ENDIF
	
	INT i
	FOR i = 0 TO NUMBER_OF_BUILDINGS-1
		// Every building should start off in the normal state
		IF USE_SP_BUILDING_CONTROLLER_DATA()
			#if USE_CLF_DLC
				g_savedGlobalsClifford.sBuildingData.eBuildingState[i] = BUILDINGSTATE_NORMAL
			#endif
			#if USE_NRM_DLC
				g_savedGlobalsnorman.sBuildingData.eBuildingState[i] = BUILDINGSTATE_NORMAL
			#endif
			#if not USE_CLF_DLC
			#if not use_NRM_DLC
				g_savedGlobals.sBuildingData.eBuildingState[i] = BUILDINGSTATE_NORMAL
			#endif
			#endif
		ELSE
			#IF NOT USE_SP_DLC
				g_MPBuildingData.eBuildingState[i] = BUILDINGSTATE_NORMAL
			#ENDIF
		ENDIF
		// Set the stored flag so the building_controller knows to deal with it
		g_bUpdateBuildingState[i] = TRUE
		g_bBuildingStateUpdateDelay[i] = FALSE
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_DOORS-1
		IF USE_SP_BUILDING_CONTROLLER_DATA()
			#if USE_CLF_DLC
				g_savedGlobalsClifford.sBuildingData.eDoorState[i] = DOORSTATE_LOCKED
			#endif
			#if USE_NRM_DLC
				g_savedGlobalsnorman.sBuildingData.eDoorState[i] = DOORSTATE_LOCKED
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				g_savedGlobals.sBuildingData.eDoorState[i] = DOORSTATE_LOCKED
			#endif			
			#endif
		ELSE
			#IF NOT USE_SP_DLC
				g_MPBuildingData.eDoorState[i] = DOORSTATE_UNLOCKED
			#ENDIF
		ENDIF
		// Set the stored flag so the building_controller knows to deal with it
		SET_BIT(g_iUpdateDoorStateBitset[i/32], (i%32))
		g_sForceDoorUpdateData.iForceDoorUpdateCount = 0
		g_iOverrideDoorStateCounter[i] = 0
		CLEAR_BIT(g_iOverrideDoorStateBitset[i/32], i%32)
		g_eOverrideDoorState[i] = DOORSTATE_UNLOCKED
		g_eCurrentDoorState[i] = DOORSTATE_UNLOCKED
	ENDFOR
	
	IF USE_SP_BUILDING_CONTROLLER_DATA()
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_SB_FLOYDS] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_SB_KRIS_HOLTS] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_SB_TREVORS_TRAILER] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsClifford.sBuildingData.eDoorState[DOORNAME_HICK_BAR_F_INT] = DOORSTATE_UNLOCKED
			g_savedGlobalsClifford.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_OFFICE] = DOORSTATE_UNLOCKED
			g_savedGlobalsClifford.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_STORE] = DOORSTATE_UNLOCKED
			g_savedGlobalsClifford.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_OFFICE_STORE] = DOORSTATE_UNLOCKED
			g_savedGlobalsClifford.sBuildingData.eDoorState[DOORNAME_GUN_SHOP_01_DT_SR] = DOORSTATE_UNLOCKED		
			g_savedGlobalsClifford.sBuildingData.bDefaultDataSet = TRUE
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_SB_FLOYDS] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_SB_KRIS_HOLTS] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_SB_TREVORS_TRAILER] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED
			g_savedGlobalsnorman.sBuildingData.eDoorState[DOORNAME_HICK_BAR_F_INT] = DOORSTATE_UNLOCKED
			g_savedGlobalsnorman.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_OFFICE] = DOORSTATE_UNLOCKED
			g_savedGlobalsnorman.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_STORE] = DOORSTATE_UNLOCKED
			g_savedGlobalsnorman.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_OFFICE_STORE] = DOORSTATE_UNLOCKED
			g_savedGlobalsnorman.sBuildingData.eDoorState[DOORNAME_GUN_SHOP_01_DT_SR] = DOORSTATE_UNLOCKED		
			g_savedGlobalsnorman.sBuildingData.bDefaultDataSet = TRUE
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_SB_FLOYDS] = BUILDINGSTATE_DESTROYED
			g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_SB_KRIS_HOLTS] = BUILDINGSTATE_DESTROYED
			g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_SB_TREVORS_TRAILER] = BUILDINGSTATE_DESTROYED
			g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED
			g_savedGlobals.sBuildingData.eDoorState[DOORNAME_HICK_BAR_F_INT] = DOORSTATE_UNLOCKED
			g_savedGlobals.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_OFFICE] = DOORSTATE_UNLOCKED
			g_savedGlobals.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_STORE] = DOORSTATE_UNLOCKED
			g_savedGlobals.sBuildingData.eDoorState[DOORNAME_SWEATSHOP_OFFICE_STORE] = DOORSTATE_UNLOCKED
			g_savedGlobals.sBuildingData.eDoorState[DOORNAME_GUN_SHOP_01_DT_SR] = DOORSTATE_UNLOCKED		
			g_savedGlobals.sBuildingData.bDefaultDataSet = TRUE
		#endif			
		#endif
	ELSE
		#IF NOT USE_SP_DLC
			g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_LOST_GANG_DOORS] = BUILDINGSTATE_DESTROYED
			g_MPBuildingData.eBuildingState[BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS] = BUILDINGSTATE_DESTROYED
			g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_CRUISESHIP] = BUILDINGSTATE_DESTROYED
			g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED
			//g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_MILBAS_MPGATES] = BUILDINGSTATE_DESTROYED //1361093
			
			g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_CAR_SHOWROOM_INTERIOR] = BUILDINGSTATE_DESTROYED
			//g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_MPAPT_EXTERIOR] = BUILDINGSTATE_DESTROYED
			
			g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR] = BUILDINGSTATE_DESTROYED
			g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_AIRFIELD_PROPS] = BUILDINGSTATE_DESTROYED
			
			//	IF GET_COMMANDLINE_PARAM_EXISTS("sc_LaunchHeistsFromFlow")
				
				g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR] = BUILDINGSTATE_DESTROYED
				g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR] = BUILDINGSTATE_DESTROYED
				g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR] = BUILDINGSTATE_DESTROYED
				g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_BURNT] = BUILDINGSTATE_NORMAL
				g_MPBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS] = BUILDINGSTATE_NORMAL
				
				PRINTLN("[dsw] [SETUP_DEFAULT_BUILDING_DATA] Setup Lester sweatshop for heist")
				
			//	ENDIF
			
			g_MPBuildingData.bDefaultDataSet = TRUE
		#ENDIF
	ENDIF
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR] = BUILDINGSTATE_DESTROYED
		g_savedGlobalsClifford.sBuildingData.eBuildingState[BUILDINGNAME_IPL_AIRFIELD_PROPS] = BUILDINGSTATE_DESTROYED
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR] = BUILDINGSTATE_DESTROYED
		g_savedGlobalsnorman.sBuildingData.eBuildingState[BUILDINGNAME_IPL_AIRFIELD_PROPS] = BUILDINGSTATE_DESTROYED
	#endif
	#if not USE_CLF_DLC
	#if not use_NRM_DLC
		g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR] = BUILDINGSTATE_DESTROYED
		g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_AIRFIELD_PROPS] = BUILDINGSTATE_DESTROYED
	#endif
	#endif
ENDPROC

PROC INITIALISE_EXTENDED_PICKUP_PROBE_AREAS()

	// Heist Facilities.
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<1287.4130, 2846.5930, 45.0500>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<19.0220, 2611.2339, 81.6560>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<2769.0891, 3920.1890, 41.4880>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<3407.5210, 5504.7178, 21.9340>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<2.4300, 6831.9058, 11.4730>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<-2231.2839, 2417.6641, 7.8370>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<-8.5274, 3327.2117, 40.5878>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<2086.2700, 1761.5515, 102.980>>, 40.0)
	ADD_EXTENDED_PICKUP_PROBE_AREA(<<1874.6890, 284.7910, 159.9610>>, 40.0)
ENDPROC

PROC CLEANUP_EXTENDED_PICKUP_PROBE_AREAS()
	CLEAR_EXTENDED_PICKUP_PROBE_AREAS()
ENDPROC
