USING "stripclub_stage.sch"

PROC STRIPCLUB_SET_STRIPPER_FOR_VIP_CUTSCENE(INT iStripperIndex, VECTOR vStartPos, BOOL bSecondary)
	
	PED_INDEX pedStripper = namedStripper[iStripperIndex].ped
	
	TASK_CLEAR_LOOK_AT(pedStripper)		
	SET_ENTITY_COORDS(pedStripper, vStartPos)
	SET_ENTITY_VELOCITY(pedStripper, <<0,0,0>>)
	CLEAR_PED_TASKS_IMMEDIATELY(pedStripper)
	SET_ENTITY_HEADING_FACE_COORDS(pedStripper, <<117.19, -1294.81, 29.27>>)
	TASK_STAND_STILL(pedStripper, -1)
	IF bSecondary
		FORCE_PED_MOTION_STATE(pedStripper, MS_ON_FOOT_IDLE, TRUE, FAUS_DEFAULT)
	ELSE
		FORCE_PED_MOTION_STATE(pedStripper, MS_ON_FOOT_WALK, TRUE, FAUS_DEFAULT)
	ENDIF
	STRIPCLUB_TASK_WAYPOINT_RECORDING(pedStripper, SWR_VIP_INTRO)
	
ENDPROC

PROC STRIPCLUB_SET_STRIPPER_FOR_EXIT_VIP_CUTSCENE(INT iStripperIndex, VECTOR vStartPos, STRIPCLUB_WAYPOINT_RECORDINGS waypointRecording)
	
	PED_INDEX pedStripper = namedStripper[iStripperIndex].ped
	
	CLEAR_PED_TASKS_IMMEDIATELY(pedStripper)
		
	GET_GROUND_Z_FOR_3D_COORD(vStartPos+<<0,0,0.5>>, vStartPos.z)
	SET_ENTITY_COORDS(pedStripper, vStartPos)
	//WAYPOINT_RECORDING_GET_COORD("strclub1d", 1, vFaceCoord)
	SET_ENTITY_HEADING_FACE_COORDS(pedStripper, <<120.11, -1289.95, 28.27>>)
	
	STRIPCLUB_TASK_WAYPOINT_RECORDING(pedStripper, waypointRecording)
ENDPROC

PROC CHECK_STRIPPER_STUCK_IN_LAPDANCE(INT iStripperIndex)

	IF iStripperIndex >= 0
		IF NOT IS_PED_INJURED(namedStripper[iStripperIndex].ped)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(namedStripper[iStripperIndex].ped)
				IF iCurrentStripperForDance != iStripperIndex AND iSecondaryStripper != iStripperIndex
					IF GET_STRIPPER_AI_STATE(iStripperIndex) = AI_STATE_GIVE_DANCE
					AND IS_STRIPPER_AVAILABLE(iStripperIndex)
						SET_STRIPPER_AI_STATE(iStripperIndex, AI_STATE_WANDER)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_ANY_STRIPPER_APPROACHING_PLAYER()
	INT i
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF GET_STRIPPER_AI_STATE(i) = AI_STATE_APPROACH_PLAYER
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC STRIPCLUB_SM_STRIPPER_GOTO_VIP_WAIT(INT i)

	VECTOR vWalkToPos, vWorkerPos
	FLOAT fWaitHeading = 0.0
	PED_INDEX stripperPed = namedStripper[i].ped
			
	vWalkToPos = GET_STRIPCLUB_STRIPPER_VIP_WAIT()
	vWorkerPos = GET_ENTITY_COORDS(stripperPed)
	
	CPRINTLN(DEBUG_SCLUB, "Tasking stripper ", i , " to go to wait")
	
	INT iSendToSeat = iCurrentLapDanceSeat
	#IF MP_STRIPCLUB
		
		iSendToSeat = i
		IF  i = iSecondaryStripper
			iSendToSeat = iCurrentStripperForDance
		ENDIF
		
		IF iSendToSeat = -1
			iSendToSeat = i
		ENDIF
	
		vWalkToPos = GET_VIP_DANCER_WAITING_POS(iSendToSeat)
		fWaitHeading = GET_VIP_DANCER_WAITING_HEADING(iSendToSeat)
		
		IF GET_STRIPPER_WANDER_STATE(i) = WANDER_STATE_GET_HEADING
			CLEAR_PED_TASKS(namedStripper[i].ped)
			EXIT
		ENDIF
		
		IF NETWORK_IS_IN_MP_CUTSCENE() //lapdance cutscene started, don't task anymore
			EXIT
		ENDIF
		
	#ENDIF

	SET_PED_RESET_FLAG(stripperPed, PRF_UseProbeSlopeStairsDetection, TRUE)
	SET_PED_RESET_FLAG(stripperPed, PRF_SearchForClosestDoor, TRUE)
	CPRINTLN(DEBUG_SCLUB, "Strippers distance from wait pos ", VDIST(vWorkerPos, vWalkToPos+<<0,0,1>>))
	
	IF i = iSecondaryStripper //seconday stripper follow first
		#IF MP_STRIPCLUB
			IF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
			AND iCurrentStripperForDance > -1
				TASK_GO_TO_ENTITY(stripperPed, namedStripper[iCurrentStripperForDance].ped, DEFAULT_TIME_BEFORE_WARP, DEFAULT_SEEK_RADIUS, PEDMOVEBLENDRATIO_WALK)
			ENDIF
			
			EXIT
		#ENDIF
	
		#IF NOT MP_STRIPCLUB
			vWalkToPos = GET_STRIPCLUB_SECOND_STRIPPER_VIP_WAIT()
		#ENDIF

	ENDIF
	
	BOOL bIsStripperAtWaitPos = VDIST2(vWorkerPos, vWalkToPos+<<0,0,1>>) < (0.5*0.5)
	
	 //timer to see if the player is actually going to the stripper
	IF bIsStripperAtWaitPos AND i != iSecondaryStripper //leave timer is based off the original stripper
		IF NOT IS_TIMER_STARTED(tInteraction)
			START_TIMER_NOW(tInteraction)
		ELIF CAN_DO_STRIP_CLUB_SPEECH() //so anouncers speech subtitles doesn't cover up the text
//			CPRINTLN(DEBUG_SCLUB,"Stripper wait time ", GET_TIMER_IN_SECONDS(tInteraction))
//			CPRINTLN(DEBUG_SCLUB,"Secondary stripper id ", iSecondaryStripper)
			IF TIMER_DO_ONCE_WHEN_READY(tInteraction, 120.0)
				ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
				IF iSecondaryStripper < 0
					CPRINTLN(DEBUG_SCLUB,"Wait over 1, second stripper id ", iSecondaryStripper)
					PRINT_NOW("SCLUB_WAIT_OVER1", DEFAULT_GOD_TEXT_TIME, 1)
				ELSE
					CPRINTLN(DEBUG_SCLUB,"Wait over 2, second stripper id ", iSecondaryStripper)
					RESET_STRIPPER_TO_WANDER(iSecondaryStripper)
					PRINT_NOW("SCLUB_WAIT_OVER2", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				
				RESET_STRIPPER_TO_WANDER(i)
				REMOVE_ALL_STRIPPER_BLIPS()
				REMOVE_LAPDANCE_ANIMS()
				iSecondaryStripper = -1
				iCurrentStripperInteraction = -1
				iCurrentStripperForDance = -1
				stripClubStage = STAGE_WANDER_CLUB
			ELIF GET_TIMER_IN_SECONDS(tInteraction) > 45 AND GET_TIMER_IN_SECONDS(tInteraction) < 46
				SET_STRIPPER_SPEECH_TARGET(i, STRSPEECH_HURRY)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bIsStripperAtWaitPos
	AND GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
		CPRINTLN(DEBUG_SCLUB,"Tasked stripper to move to vip wait ", vWalkToPos)
		TASK_FOLLOW_NAV_MESH_TO_COORD(stripperPed, vWalkToPos, PEDMOVE_WALK, -1, 0.05, ENAV_STOP_EXACTLY | ENAV_DONT_AVOID_PEDS, fWaitHeading)

		CANCEL_TIMER(tInteraction)
	ELIF bIsStripperAtWaitPos
	AND GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK		
		IF NOT IS_STRIPCLUB_MP()
			IF NOT IS_PED_FACING_PED(stripperPed, PLAYER_PED_ID(), 10) 
				IF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
					TASK_TURN_PED_TO_FACE_ENTITY(stripperPed, PLAYER_PED_ID())
				ENDIF
			ELIF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
				TASK_PLAY_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
		ELSE
			IF ABSF(GET_ENTITY_HEADING(stripperPed) - GET_VIP_DANCER_WAITING_HEADING(iSendToSeat)) > 5.0
				IF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_ACHIEVE_HEADING) != PERFORMING_TASK
					TASK_ACHIEVE_HEADING(stripperPed, GET_VIP_DANCER_WAITING_HEADING(iSendToSeat))
				ENDIF
			ELIF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
				TASK_PLAY_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STRIPCLUB_SM_APPROACH_PLAYER(INT i)

	PED_INDEX stripperPed = namedStripper[i].ped
	PED_INDEX pedPlayer = PLAYER_PED_ID()
	BOOL bControlsStripper = TRUE
	
	IF IS_PED_INJURED(pedPlayer) OR IS_PED_INJURED(stripperPed)
		EXIT
	ENDIF
	
	#IF MP_STRIPCLUB
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(stripperPed)
			bControlsStripper = FALSE
		ENDIF
		
		IF bControlsStripper
			INT iPlayerStripperIsAproaching = GET_PLAYER_WHO_HAS_STRIPPER_IN_USE(i)
			CPRINTLN(DEBUG_SCLUB, "Have control of stripper during approach. Stripper is aproaching ", iPlayerStripperIsAproaching)
			
			IF iPlayerStripperIsAproaching >= 0
				pedPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerStripperIsAproaching)))
			ELSE
				CPRINTLN(DEBUG_SCLUB, "No one marekd the stripper in use during approach!!")
				RESET_STRIPPER_TO_WANDER(i)
				EXIT
			ENDIF
		ELSE //someone else is in control
			EXIT
		ENDIF
	#ENDIF
	
	IF IS_PED_INJURED(pedPlayer)
		CPRINTLN(DEBUG_SCLUB, "Aproaching player is injured")
	ENDIF
	
	IF bControlsStripper AND NOT IS_PED_INJURED(pedPlayer) AND NOT IS_PED_INJURED(stripperPed)
	
		IF NOT IS_STRIPPER_NEAR_PED(i, pedPlayer)
		AND GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
			TASK_GO_TO_ENTITY(stripperPed, pedPlayer, -1, 1.2,PEDMOVEBLENDRATIO_WALK)
			
		ELIF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
			IF NOT IS_PED_FACING_PED(stripperPed, pedPlayer, 5) 
			AND GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
				TASK_TURN_PED_TO_FACE_ENTITY(stripperPed, pedPlayer)	
			ELIF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
				IF NOT IS_ENTITY_PLAYING_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1))
					TASK_PLAY_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TASK_STRIPPER_FACE_PLAYER(INT i)
	BOOL bControlsStripper = TRUE
	
	#IF MP_STRIPCLUB
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(namedStripper[i].ped)
			bControlsStripper = FALSE
		ENDIF
	#ENDIF

	IF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
	AND bControlsStripper
		TASK_TURN_PED_TO_FACE_ENTITY(namedStripper[i].ped, PLAYER_PED_ID())
	ENDIF
ENDPROC

PROC PLAY_STRIPPER_SMILE_ANIM(PED_INDEX stripperPed)
	IF HAS_ANIM_DICT_LOADED("facials@gen_female@variations@happy")
		IF NOT IS_ENTITY_PLAYING_ANIM(stripperPed, "facials@gen_female@variations@happy", "mood_happy_1")
			TASK_PLAY_ANIM(stripperPed, "facials@gen_female@variations@happy", "mood_happy_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_STRIPPER_LOOK_AT(PED_INDEX stripperPed, INT index)

	#IF MP_STRIPCLUB
	
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(stripperPed)
			EXIT
		ENDIF
	
	#ENDIF
	
	//If performing for a dude at the pole, look at him
	IF DOES_ENTITY_EXIST(GET_PED_FOR_DANCE()) AND GET_STRIPPER_AI_STATE(index) = AI_STATE_POLE_DANCE
		CPRINTLN(DEBUG_SCLUB,"Look at player for on stage dance")
		TASK_LOOK_AT_ENTITY(stripperPed, GET_PED_FOR_DANCE(), -1, SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT | SLF_WHILE_NOT_IN_FOV )
		EXIT	
	ELSE
		//Stripper is dancing on poles, don't look at anyone
		IF namedStripper[index].poledancerState > POLEDANCER_STATE_DANCE_ENTER 
		AND namedStripper[index].poledancerState < POLEDANCER_STATE_DANCE_EXIT
			IF railStage = RAIL_WATCHING
				TASK_LOOK_AT_ENTITY(stripperPed, PLAYER_PED_ID(), -1)
			ENDIF
		ENDIF
	ENDIF

	
	//If player is nearby, look at him
	IF namedStripper[index].fSquareDistanceToPlayer < (STRIPPER_APPROACH_DISTANCE*STRIPPER_APPROACH_DISTANCE)
		TASK_LOOK_AT_ENTITY(stripperPed, PLAYER_PED_ID(), -1)
		EXIT
	ENDIF
	
	IF iNextUpdateLookAtTime < GET_GAME_TIMER()
		IF GET_STRIPPER_AI_STATE(index) != AI_STATE_GIVE_DANCE
			PLAY_STRIPPER_SMILE_ANIM(stripperPed)
		ENDIF
	
		IF index >= 0
			IF iCurrentStripperInteraction = index OR iSecondaryStripper = index OR iCurrentStripperForDance = index
				TASK_CLEAR_LOOK_AT(stripperPed)
				EXIT
			ENDIF
		ENDIF
		
		PED_INDEX pedCloseToStripper

		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		GET_CLOSEST_PED(GET_ENTITY_COORDS(stripperPed), 5.0, TRUE, TRUE, pedCloseToStripper, TRUE, TRUE, GET_PED_TYPE(stripperPed))
		
		IF NOT DOES_ENTITY_EXIST(pedCloseToStripper)
			TASK_CLEAR_LOOK_AT(stripperPed) //no one is near stripper, don't look at anyone
			EXIT
		ENDIF
		
		TASK_LOOK_AT_ENTITY(stripperPed, pedCloseToStripper, -1)
	ENDIF

ENDPROC


PROC STRIPCLUB_SM_GOTO_RANDOM_LOC(INT i)
	VECTOR vGetPos
	FLOAT fGetRot
	INT iStandLoc
	
	PED_INDEX stripperPed = namedStripper[i].ped
	
	IF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = PERFORMING_TASK
		EXIT
	ENDIF
	
	IF GET_STRIPPER_WANDER_STATE(i) != WANDER_STATE_MOVE
		CLEAR_BITMASK_AS_ENUM(namedStripper[i].stripperFlags, STRIPPER_FLAG_TASKED_MOVE_TO_RAND)
		#IF MP_STRIPCLUB
			SET_PLAYER_STRIPPER_TASKED_TO_RAND(clubPlayerBD[PARTICIPANT_ID_TO_INT()], i, FALSE)
		#ENDIF
	ENDIF
	
	SWITCH GET_STRIPPER_WANDER_STATE(i)

		CASE WANDER_STATE_NONE
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_NONE")
			ENDIF
		BREAK
		
		CASE WANDER_STATE_INIT
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_INIT")
			ENDIF
			//TASK_PAUSE(pedWorker, GET_RANDOM_INT_IN_RANGE(500, 3000))
			#IF MP_STRIPCLUB IF NETWORK_IS_HOST_OF_THIS_SCRIPT() #ENDIF
			
				//Hardcode movement for stripped 2 when in the Trevor sclub switch scene.
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_SWITCHED_INSIDE_TO_TREVOR)
				AND i = 2
					iStandLoc = 5
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_SWITCHED_INSIDE_TO_TREVOR)
					
					IF namedStripper[i].iCurrentStandPos != -1
						SET_STRIPCLUB_STAND_LOCATION(namedStripper[i].iCurrentStandPos, FALSE)
					ENDIF
					namedStripper[i].iCurrentStandPos = iStandLoc
					
				ELIF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_STRIPPER_JUST_CREATED)
					//Let the host find the new stad spot
					iStandLoc = GET_RANDOM_STRIPPER_STAND_LOC(vGetPos, fGetRot)	//get new stand location

					
					 CPRINTLN(DEBUG_SCLUB,"Got random location ", iStandLoc)
					IF namedStripper[i].iCurrentStandPos != -1
						SET_STRIPCLUB_STAND_LOCATION(namedStripper[i].iCurrentStandPos, FALSE) //clear old one
					ENDIF
					namedStripper[i].iCurrentStandPos = iStandLoc
				ENDIF
				
				//Fix for 2325188: If STRIP_CLUB_STRIPPER_JUST_CREATED is set it is possible
				//iCurrentStandPos is still a null value. Only step to the wander state move
				//if we've found a valid stand position.
				IF namedStripper[i].iCurrentStandPos != -1
					#IF MP_STRIPCLUB
						serverBD.iStripperStandLocations[i] = namedStripper[i].iCurrentStandPos
					#ENDIF
					
					SET_STRIPCLUB_STAND_LOCATION(namedStripper[i].iCurrentStandPos, TRUE) //set new one
					
					SET_STRIPPER_WANDER_STATE(i,  WANDER_STATE_MOVE)
				ELSE
					SET_STRIPPER_WANDER_STATE(i,  WANDER_STATE_IDLE1)
				ENDIF
				
			#IF MP_STRIPCLUB ENDIF #ENDIF
		BREAK
		
		CASE WANDER_STATE_MOVE
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_MOVE")
			ENDIF
			INT iMoveLoc 
			iMoveLoc = namedStripper[i].iCurrentStandPos
			
			#IF MP_STRIPCLUB
				iMoveLoc = serverBD.iStripperStandLocations[i]
				namedStripper[i].iCurrentStandPos = iMoveLoc
			#ENDIF
			
			#IF MP_STRIPCLUB IF NETWORK_HAS_CONTROL_OF_ENTITY(stripperPed) #ENDIF
				IF NOT IS_BITMASK_AS_ENUM_SET(namedStripper[i].stripperFlags, STRIPPER_FLAG_TASKED_MOVE_TO_RAND)
					GET_RANDOM_CLUB_STAND_LOCAND_HEADING(iMoveLoc, vGetPos, fGetRot, TRUE)
					
					CPRINTLN(DEBUG_SCLUB,"Sending stripper ", i, " to location ", namedStripper[i].iCurrentStandPos)
					TASK_CLEAR_LOOK_AT(stripperPed)
					TASK_FOLLOW_NAV_MESH_TO_COORD(stripperPed, vGetPos, PEDMOVE_WALK, -1, 0.05, ENAV_STOP_EXACTLY, fGetRot)
					SET_BITMASK_AS_ENUM(namedStripper[i].stripperFlags, STRIPPER_FLAG_TASKED_MOVE_TO_RAND)
					
					#IF MP_STRIPCLUB
						SET_PLAYER_STRIPPER_TASKED_TO_RAND(clubPlayerBD[PARTICIPANT_ID_TO_INT()], i, TRUE)
					#ENDIF
				ENDIF
			#IF MP_STRIPCLUB ENDIF #ENDIF
			
			
			#IF MP_STRIPCLUB
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF HAS_ANY_PLAYER_TASKED_STRIPPER_TO_RAND(i)
						CPRINTLN(DEBUG_SCLUB,"Someone tasked stripper to move to rand")
						SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_GET_HEADING)
					ENDIF
				ENDIF
			#ENDIF
			
			#IF NOT MP_STRIPCLUB
				SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_GET_HEADING)
			#ENDIF
			
		BREAK
		
		CASE WANDER_STATE_GET_HEADING
			
			#IF MP_STRIPCLUB IF NETWORK_IS_HOST_OF_THIS_SCRIPT() #ENDIF
			GET_RANDOM_CLUB_STAND_LOCAND_HEADING(namedStripper[i].iCurrentStandPos, vGetPos, fGetRot, TRUE)
			
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_GET_HEADING. Dist to nava spot ", VDIST(GET_ENTITY_COORDS(stripperPed), vGetPos) )
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
			OR VDIST(GET_ENTITY_COORDS(stripperPed), vGetPos) < 1.1
			OR IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE)
				
				//make sure stripper is in safe spot for vip exit cutscene
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE)
					SET_ENTITY_COORDS(stripperPed, vGetPos)
				ENDIF
				
				IF VDIST(GET_ENTITY_COORDS(stripperPed), vGetPos) > 1.5
					CPRINTLN(DEBUG_SCLUB, "stripper isn't close enough task again")
					SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_MOVE)
				ELSE
					SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_IDLE1)
				ENDIF
			ENDIF
			
			#IF MP_STRIPCLUB ENDIF #ENDIF
		BREAK
		CASE WANDER_STATE_IDLE1
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_IDLE1")
			ENDIF
			CLEAR_BITMASK_AS_ENUM(namedStripper[i].stripperFlags, STRIPPER_FLAG_SAID_APPROACH) //she has reached a new location, allow her to talk again
			IF GET_SCRIPT_TASK_STATUS(stripperPed, SCRIPT_TASK_ACHIEVE_HEADING) != PERFORMING_TASK
				IF HAVE_STRIPPER_IDLES_LOADED()
					//play idle
					#IF MP_STRIPCLUB IF NETWORK_HAS_CONTROL_OF_ENTITY(stripperPed) #ENDIF
						TASK_PLAY_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)	
						SET_STRIPPER_WANDER_STATE(i,  WANDER_STATE_IDLE2)
					#IF MP_STRIPCLUB ELSE 
					
					ENDIF #ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1))
						SET_STRIPPER_WANDER_STATE(i,  WANDER_STATE_IDLE2)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE WANDER_STATE_IDLE2
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_IDLE2")
			ENDIF
			
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_STRIPPER_JUST_CREATED)
			IF HAVE_STRIPPER_IDLES_LOADED()
				IF IS_ENTITY_PLAYING_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1))
					IF GET_ENTITY_ANIM_CURRENT_TIME(stripperPed,GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1)) >= 0.95
						//play random idle 
						#IF MP_STRIPCLUB IF NETWORK_HAS_CONTROL_OF_ENTITY(stripperPed) #ENDIF
							TASK_PLAY_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(), NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)				
							SET_STRIPPER_WANDER_STATE(i,  WANDER_STATE_IDLE3)
						#IF MP_STRIPCLUB ELSE 
						
						ENDIF #ENDIF
					ENDIF
				ELSE
					SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_IDLE3)
				ENDIF
			ENDIF
		BREAK
		CASE WANDER_STATE_IDLE3
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_IDLE3")
			ENDIF
			
			IF HAVE_STRIPPER_IDLES_LOADED()
				INT iIdlePlaying
				iIdlePlaying = GET_STRIPPER_IDLE_PLAYING(stripperPed)
				
				IF IS_ENTITY_PLAYING_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(iIdlePlaying))
				AND iIdlePlaying != 1
					IF GET_ENTITY_ANIM_CURRENT_TIME(stripperPed,GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(iIdlePlaying)) >= 0.95
						//play looping idle
						#IF MP_STRIPCLUB IF NETWORK_HAS_CONTROL_OF_ENTITY(stripperPed) #ENDIF
							TASK_PLAY_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_END)
						#IF MP_STRIPCLUB ELSE 
						
						ENDIF #ENDIF
					ENDIF
				ELSE
					SET_STRIPPER_WANDER_STATE(i,  WANDER_STATE_END)
				ENDIF
			ENDIF
			
			namedStripper[i].iNextMoveTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 25000)
		BREAK
		
		CASE WANDER_STATE_END
			IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
				CPRINTLN(DEBUG_SCLUB,"WANDER_STATE_END")
			ENDIF
			IF namedStripper[i].iNextMoveTime < GET_GAME_TIMER()
			OR NOT IS_ENTITY_PLAYING_ANIM(stripperPed, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1))
				SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_NONE)
			ENDIF
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Invalid stripper wander state")
		BREAK
	ENDSWITCH
	
ENDPROC

// handle stripper ai
PROC HANDLE_NAMED_STRIPPER_AI(INT i, BOOL &bIsStripperOfferingDance)
	
	BOOL bOwnStripper = TRUE
	
	IF NOT IS_PED_INJURED(namedStripper[i].ped)
		IF IS_STRIPCLUB_MP()
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(namedStripper[i].ped)
				bOwnStripper = FALSE
			ENDIF
			
			CHECK_STRIPPER_STUCK_IN_LAPDANCE(i)
		ENDIF
		
		MANAGE_STRIPPER_LOOK_AT(namedStripper[i].ped, i)
		IF DISPLAY_DEBUG_STRIPPER_WANDER_STATE(i)
			CPRINTLN(DEBUG_SCLUB,  "Strippers current ai state is ", GET_STRIPPER_AI_STATE(i))
		ENDIF
			
		// switch on current ai state
		SWITCH GET_STRIPPER_AI_STATE(i)
			CASE AI_STATE_WANDER
				#IF MP_STRIPCLUB
					IF iStripperForApproach = i
						CPRINTLN(DEBUG_SCLUB, "Syncing stripper ", GET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()]))
						//make sure that you arn't in the middle of syncing a new state for the stripper
						IF GET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()]) != i
							CPRINTLN(DEBUG_SCLUB, "Stripper ", i , " is in wander state, but is set for approach, remove for approach")
							iStripperForApproach = -1
						ENDIF
					ENDIF
				#ENDIF
				
				IF iCurrentStripperInteraction <> i
					IF namedStripper[i].wanderState = WANDER_STATE_NONE
						START_STRIPPER_WANDER_SM(i)
					ELSE
						// approach player if player comes close or pass next approach time
						IF IS_CAN_APPROACH_FLAG_SET()
							// stripper approach player
							IF CAN_SPEW_STRIPPER_APPROACH_DEBUG()
								CPRINTLN(DEBUG_SCLUB,  "Stripper ", i , " next approach time ", namedStripper[i].iNextApproachTime)
							ENDIF
							
							IF GET_GAME_TIMER() >= namedStripper[i].iNextApproachTime
							AND (iRefusalCount < 2 OR HAS_PLAYER_HAD_DRINK_RECENTLY())
							AND NOT IS_BITMASK_AS_ENUM_SET(namedStripper[i].stripperFlags, STRIPPER_FLAG_HAD_LAPDANCE)
							AND NOT IS_PLAYER_NEAR_RAIL() //AND NOT IS_STRIPCLUB_MP()
							AND NOT IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_SOL_HELP") AND NOT IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_SOL_TREV")
							AND NOT IS_STRIPCLUB_MESSAGE_DISPLAYED_WITH_NUMBER("SCLUB_SOL_HELP_TUNE", DANCE_COST)
							AND iStripperForApproach = -1
								IF CAN_SPEW_STRIPPER_APPROACH_DEBUG()
									CPRINTLN(DEBUG_SCLUB,"GET_GAME_TIMER() >= namedStripper[i].iNextApproachTime")
									
									IF DOES_STRIPPER_DISLIKE_PLAYER_GENERALLY(i)
										CPRINTLN(DEBUG_SCLUB,"Stripper dislikes player")
									ENDIF
									IF IS_ANY_STRIPPER_APPROACHING_PLAYER()
										CPRINTLN(DEBUG_SCLUB,"Another stripper already approaching")
									ENDIF
								ENDIF
								
								IF (iRefusalCount < 2 OR HAS_PLAYER_HAD_DRINK_RECENTLY())// will only approach if not refusing approaches or drunk
								AND NOT DOES_STRIPPER_DISLIKE_PLAYER_GENERALLY(i)// and only if they don't think you're a bellend
								AND stripClubStage = STAGE_WANDER_CLUB // and if you're wandering around
								//AND NOT IS_ANY_STRIPPER_NEAR_PLAYER()
								 	SET_STRIPPER_APPROACH_PLAYER(i)
								ELSE
									SET_NEXT_STRIPPER_APPROACH_TIME(i)
								ENDIF
							// player approach stripper
							ELIF (iCurrentStripperForDance = -1)
								IF (iCurrentStripperInteraction = -1)
									IF IS_STRIPPER_NEAR_PLAYER(i)
									AND (GET_ENTITY_SPEED(PLAYER_PED_ID()) < 3.5) //AND CAN_DO_STRIP_CLUB_SPEECH() 
									AND IS_PLAYER_FACING_STRIPPER(i) AND NOT IS_PHONE_ONSCREEN()
									AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())		
										IF GET_ENTITY_SPEED(namedStripper[i].ped) = 0.0 AND bOwnStripper
										AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
											//CPRINTLN(DEBUG_SCLUB,"Next to stripper who is in one spot")
											IF NOT IS_BITMASK_AS_ENUM_SET(namedStripper[i].stripperFlags, STRIPPER_FLAG_SAID_APPROACH)
												SET_BITMASK_AS_ENUM(namedStripper[i].stripperFlags, STRIPPER_FLAG_SAID_APPROACH)
												CPRINTLN(DEBUG_SCLUB,"setting speech to approach")
												SET_STRIPPER_SPEECH_TARGET(i, STRSPEECH_APPROACHED)
											ENDIF
											
											TASK_TURN_PED_TO_FACE_ENTITY(namedStripper[i].ped, PLAYER_PED_ID())
										ENDIF
										
										//IF GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
										IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
											IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_HELP)
											//AND CAN_DO_STRIP_CLUB_SPEECH()
												IF STRIP_CLUB_IS_PLAYER_TREVOR() AND DOES_TREVOR_OWN_CLUB()
												OR DANCE_COST = 0
													PRINT_STRIPCLUB_HELP("SCLUB_SOL_TREV")
												ELIF IS_STRIPCLUB_MP()
													PRINT_STRIPCLUB_HELP_WITH_NUMBER("SCLUB_SOL_HELP_TUNE", DANCE_COST)
												ELSE
													PRINT_STRIPCLUB_HELP("SCLUB_SOL_HELP")
												ENDIF
												SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_HELP)												
											ENDIF
										ENDIF
										
										//AND (IS_PLAYER_ACTIVATING_PED(namedStripper[i].ped, ECOMPASS_NORTH, 0.7, EACTIVESIDE_ONEWAY, EACTIVATIONEASE_MEDIUM) 
										IF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()
											
											//IF GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
											IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
												CLEAR_ALL_STRIP_CLUB_HELP()															
												IF NOT IS_STRIPCLUB_MP() //in mp stripclub there is no dialogue so there is no need for the stripper to look at you
													TASK_STRIPPER_FACE_PLAYER(i)
												ENDIF
												iCurrentStripperInteraction = i
												
												CPRINTLN(DEBUG_SCLUB,"Asked stripper to dance ", iCurrentStripperInteraction)
												iSecondaryStripper = -1
												
												CANCEL_TIMER(tInteraction) //timer checks to see if the player is going to the vip room
												IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
													CREATE_CONVERSATION(stripClubConversation,"SCAUD","SC_SOL_T",GET_STRIPCLUB_SPEECH_PRIORITY())
												ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
													CREATE_CONVERSATION(stripClubConversation,"SCAUD","SC_SOL_M",GET_STRIPCLUB_SPEECH_PRIORITY())
												ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
													CREATE_CONVERSATION(stripClubConversation,"SCAUD","SC_SOL_F",GET_STRIPCLUB_SPEECH_PRIORITY())										
												ENDIF	
												TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), namedStripper[i].ped, 5000)
												
												IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)
													SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)	
												ENDIF
											ELSE
												CPRINTLN(DEBUG_SCLUB,"Not enough cash to solicit stripper")
												SET_STRIPPER_SPEECH_TARGET(i, STRSPEECH_NO_MONEY_TALK)
												PRINT_STRIPCLUB_HELP("SCLUB_NO_MONEY")
												STRIP_CLUB_BUY_CASH_ALERT()
											ENDIF
										ENDIF
									ENDIF
								ENDIF	
							ELSE
								//DEBUG_MESSAGE("looking for 2ndary stripper")
								IF (iCurrentStripperInteraction != i)
								AND (iCurrentStripperForDance != i)
									IF (iSecondaryStripper = -1)	
										IF IS_STRIPPER_NEAR_PLAYER(i)
										AND NOT IS_PHONE_ONSCREEN()
										
											//CPRINTLN(DEBUG_SCLUB,"Player near striper ", i)
											//IF GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
											IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
												//CPRINTLN(DEBUG_SCLUB,"You have the money for a duo dance")
												IF CAN_DO_STRIP_CLUB_SPEECH() AND IS_STRIPPER_WILLING_FOR_DUO_DANCE(i)
													//CPRINTLN(DEBUG_SCLUB,"Stripper ", i, " is available for duo dance.")
													TEXT_LABEL_23 txtMultiGirlHelp
													txtMultiGirlHelp = STRIPCLUB_FIXUP_PLAYER_LABEL("SCLUB_MLT_HLP")
													
													CLEAR_STRIPCLUB_HELP(txtMultiGirlHelp)
													PRINT_STRIPCLUB_HELP("SCLUB_SOL2_HELP")
													SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT2_HELP)
												ENDIF
											ENDIF
											
											IF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()
												//IF (GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST)		
												IF (CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE))
													CLEAR_ALL_STRIP_CLUB_HELP()
													IF NOT IS_STRIPCLUB_MP() //in mp stripclub there is no dialogue so there is no need for the stripper to look at you
														TASK_STRIPPER_FACE_PLAYER(i)
													ENDIF
													
													iSecondaryStripper = i
													
													CANCEL_TIMER(tInteraction) //timer checks to see if the player is going to the vip room																														
													IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
														CREATE_CONVERSATION(stripClubConversation,"SCAUD","SC_SOL2_T",GET_STRIPCLUB_SPEECH_PRIORITY())
													ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
														CREATE_CONVERSATION(stripClubConversation,"SCAUD","SC_SOL2_M",GET_STRIPCLUB_SPEECH_PRIORITY())
													ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
														CREATE_CONVERSATION(stripClubConversation,"SCAUD","SC_SOL2_F",GET_STRIPCLUB_SPEECH_PRIORITY())										
													ENDIF	
													
													IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_DUO)
														CPRINTLN(DEBUG_SCLUB,"Set solict second stripper flag")
														SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_DUO)
													ENDIF
													
													REMOVE_LAPDANCE_ANIMS()
													CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_DANCING_INTRO_STREAMED)
													CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_LAPDANCE_STREAMED)	
												ELSE
													SET_STRIPPER_SPEECH_TARGET(i, STRSPEECH_NO_MONEY_TALK)
													STRIP_CLUB_BUY_CASH_ALERT()
													PRINT_STRIPCLUB_HELP("SCLUB_NO_MONEY")
												ENDIF
											ENDIF
										ELSE
										ENDIF	
//									ELSE
//										DEBUG_MESSAGE("iSecondaryStripper != -1")
									ENDIF								
								ENDIF
							ENDIF			
						ELSE
							IF GET_GAME_TIMER() >= namedStripper[i].iNextApproachTime
								// if currently doing other stuff
								SET_NEXT_STRIPPER_APPROACH_TIME(i)
							ENDIF							
						ENDIF
						IF i = iUpdateThisStripperSMThisFrame
							STRIPCLUB_SM_GOTO_RANDOM_LOC(i)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE AI_STATE_APPROACH_PLAYER
				bIsStripperOfferingDance = TRUE
				
				IF CAN_SPEW_STRIPPER_APPROACH_DEBUG()
					CPRINTLN(DEBUG_SCLUB,"Stripper ", i ," is in approach player state")
				ENDIF
				IF iStripperForApproach = i
				AND IS_STRIPPER_AVAILABLE(i)
					IF IS_CAN_APPROACH_FLAG_SET()
					OR iCurrentStripperInteraction = i
						IF iCurrentStripperInteraction = -1
							IF CAN_DO_STRIP_CLUB_SPEECH()
							AND NOT IS_PHONE_ONSCREEN()
								// offer dance if over approach time
								IF IS_STRIPPER_NEAR_PLAYER(i)
									IF GET_GAME_TIMER() >= namedStripper[i].iNextApproachTime
										SET_STRIPPER_SPEECH_TARGET(i, STRSPEECH_OFFER)
									ENDIF
									iCurrentStripperInteraction = i
									iSecondaryStripper = -1
									RESTART_TIMER_NOW(tInteraction)
								ENDIF
							ENDIF
						ELSE
							IF iCurrentStripperInteraction <> i
								// if the player is doing other stuff, go back to wandering
								IF CAN_SPEW_STRIPPER_APPROACH_DEBUG()
									CPRINTLN(DEBUG_SCLUB,"Interacting with stripper ",iCurrentStripperInteraction, " reset stripper ", i, " back to wander")
								ENDIF
								
								iStripperForApproach = -1
								RESET_STRIPPER_TO_WANDER(i)
							ENDIF
						ENDIF
					ELSE
						// go back to wander if player's situation means they cannot interact
						IF CAN_SPEW_STRIPPER_APPROACH_DEBUG()
							CPRINTLN(DEBUG_SCLUB,"Doing something that should cancel approach reset stripper ", i, " back to wander")
						ENDIF
						iStripperForApproach = -1
						RESET_STRIPPER_TO_WANDER(i)
					ENDIF
				ENDIF
				
				STRIPCLUB_SM_APPROACH_PLAYER(i)
			BREAK
			
			CASE AI_STATE_GIVE_DANCE
			BREAK
			CASE AI_STATE_GO_TO_VIP_ROOM
//				CPRINTLN(DEBUG_SCLUB, "Srtipper ", i, " is in go to room state")
				//IF IS_STRIPPER_AVAILABLE(i)
					//CPRINTLN(DEBUG_SCLUB,"Stripper ", i, " is available")
					#IF MP_STRIPCLUB
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							//stripper was in the middle of moving when tasked to go to vip room, 
							//waiting until some clears task then change her state
							IF GET_STRIPPER_WANDER_STATE(i) = WANDER_STATE_GET_HEADING
							AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
								SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_NONE)
							ENDIF
						ENDIF
					#ENDIF
					#IF MP_STRIPCLUB IF NETWORK_HAS_CONTROL_OF_ENTITY(namedStripper[i].ped) #ENDIF
						STRIPCLUB_SM_STRIPPER_GOTO_VIP_WAIT(i)
					#IF MP_STRIPCLUB ENDIF #ENDIF
				//ENDIF
				
				IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
				OR GET_GLOBAL_HAS_LOCAL_PLAYER_OPTED_IN_TO_BE_BEAST()
					RESET_STRIPPER_TO_WANDER(i)
					REMOVE_ALL_STRIPPER_BLIPS()
					REMOVE_LAPDANCE_ANIMS()
					CLEAR_ALL_STRIP_CLUB_HELP()
					CLEAR_ALL_STRIP_CLUB_PRINT()
					iSecondaryStripper = -1
					iCurrentStripperInteraction = -1
					iCurrentStripperForDance = -1
					stripClubStage = STAGE_WANDER_CLUB
					
					SET_STRIPPER_AI_STATE(i, AI_STATE_WANDER)
					CPRINTLN(DEBUG_SCLUB,"change state AI_STATE_GO_TO_VIP_ROOM -> AI_STATE_WANDER // on FM menu check")
				ENDIF
			BREAK
			CASE AI_STATE_POLE_DANCE
				//trying to approach stripper walking to stage
				IF (iCurrentStripperInteraction = -1) AND GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_GOTO_STAGE_INTRO
				AND NOT IS_BITMASK_AS_ENUM_SET(namedStripper[i].stripperFlags, STRIPPER_FLAG_SAID_GO_TO_STAGE)
					IF IS_STRIPPER_NEAR_PLAYER(i)
					AND IS_PLAYER_FACING_STRIPPER(i) AND NOT IS_PHONE_ONSCREEN()
						IF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()
							SET_BITMASK_AS_ENUM(namedStripper[i].stripperFlags, STRIPPER_FLAG_SAID_GO_TO_STAGE)
							SET_STRIPPER_SPEECH_TARGET(i, STRSPEECH_GOING_TO_STAGE)
						ENDIF
					ENDIF
				ENDIF
				
				HANDLE_POLEDANCER_STRIPPER_AI(i)
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAKE_STRIPPER_PED_HIGH_QUALITY(INT iStripperID)
	VECTOR vPos
	FLOAT fHeading
	
	IF iStripperID >= 0
		IF DOES_ENTITY_EXIST(namedStripper[iStripperID].ped)
			vPos = GET_ENTITY_COORDS(namedStripper[iStripperID].ped)
			fHeading = GET_ENTITY_HEADING(namedStripper[iStripperID].ped)
			DELETE_PED(namedStripper[iStripperID].ped)
		ENDIF
		
		namedStripper[iStripperID].ped = CREATE_PED(PEDTYPE_MISSION, GET_NAMED_STRIPPER_MODEL_ENUM(namedStripper[iStripperID].stripperID, TRUE), vPos, fHeading)
		SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iStripperID].ped, namedStripper[iStripperID].stripperID, FALSE)
	ENDIF
ENDPROC

PROC MAKE_STRIPPER_PED_LOW_QUALITY(INT iStripperID)
	VECTOR vPos
	FLOAT fHeading
	
	IF iStripperID >= 0
		IF HAS_MODEL_LOADED(GET_NAMED_STRIPPER_MODEL_ENUM(namedStripper[iStripperID].stripperID, FALSE))
			IF DOES_ENTITY_EXIST(namedStripper[iStripperID].ped)
				vPos = GET_ENTITY_COORDS(namedStripper[iStripperID].ped)
				fHeading = GET_ENTITY_HEADING(namedStripper[iStripperID].ped)
				DELETE_PED(namedStripper[iStripperID].ped)
			ENDIF
			
			namedStripper[iStripperID].ped = CREATE_PED(PEDTYPE_MISSION, GET_NAMED_STRIPPER_MODEL_ENUM(namedStripper[iStripperID].stripperID, FALSE), vPos, fHeading)
			SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iStripperID].ped, namedStripper[iStripperID].stripperID, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_NAMED_STRIPPERS_CREATED()
	INT i
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF NOT DOES_ENTITY_EXIST(namedStripper[i].ped)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC SET_ALL_STRIPPERS_VOICE_NAMES()
	INT i

	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF NOT IS_PED_INJURED(namedStripper[i].ped)
			IF IS_STRIPCLUB_ENTITY_CONTROLLED_BY_PLAYER(namedStripper[i].ped)
				SET_AMBIENT_VOICE_NAME(namedStripper[i].ped, GET_BOOTY_CALL_VOICE(namedStripper[i].stripperID, IS_STRIPCLUB_MP()))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// create the named strippers 
PROC CREATE_NAMED_STRIPPERS(BOOL bCreatePed = TRUE, INT iStartTime = -1)
    INT i
	VECTOR vStorePos
	FLOAT fStoreRot
	BOOL bInitDancing
	
	SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_STRIPPER_JUST_CREATED)
	
    REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
	
		// which named stripper is this?
		namedStripper[i].stripperID = CONVERT_LOCAL_STRIPPER_INDEX_TO_NAME(i, iStartTime)
		SET_STRIPPER_AVAILABLE(i, TRUE)					

		namedStripper[i].speechTarget = STRSPEECH_NONE
			
		// Prevents the ability to create a second version of a stripper you currently have on booty call
		IF (g_StripclubGlobals.eCurrentBootyCall != CONVERT_LOCAL_STRIPPER_INDEX_TO_NAME(i, iStartTime) OR NOT IS_PLAYER_ON_BOOTY_CALL())
		AND NOT DOES_ENTITY_EXIST(namedStripper[i].ped) //don't recreate a stripper if it already exist
		
			INT iStartPoint = -1
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_SWITCHED_INSIDE_TO_TREVOR)
				CPRINTLN(DEBUG_SCLUB,"Switch to inside as trevor, use specific stripper start points for switch scene")
				iStartPoint = i
			ENDIF
			
			namedStripper[i].iCurrentStandPos = GET_RANDOM_STRIPPER_STAND_LOC(vStorePos, fStoreRot, iStartPoint, TRUE)
			
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_SWITCHED_INSIDE_TO_TREVOR)
			AND i = 2
				vStorePos = <<117.2874, -1281.7410, 27.2714>>
				fStoreRot = 216.3145
			ENDIF
			
			IF bCreatePed #IF MP_STRIPCLUB AND CAN_REGISTER_MISSION_PEDS(1) #ENDIF
				namedStripper[i].ped = CREATE_PED(PEDTYPE_MISSION, GET_NAMED_STRIPPER_MODEL_ENUM(namedStripper[i].stripperID, FALSE), vStorePos, fStoreRot)
				CPRINTLN(DEBUG_SCLUB, "Create named striper ped ", i)
				
				#IF MP_STRIPCLUB
					IF NOT DECOR_EXIST_ON(namedStripper[i].ped,"XP_Blocker")
						DECOR_SET_BOOL(namedStripper[i].ped,"XP_Blocker",TRUE)
					ENDIF
				#ENDIF
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(namedStripper[i].ped, TRUE)
				SET_PED_KEEP_TASK(namedStripper[i].ped, TRUE)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(namedStripper[i].ped, FALSE)
				SET_PED_CONFIG_FLAG(namedStripper[i].ped, PCF_OpenDoorArmIK, TRUE)
				SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[i].ped, namedStripper[i].stripperID)
				RESET_STRIPPER_TO_WANDER(i)
				
				bInitDancing = SHOULD_GENERIC_STRIPPER_INIT_DANCING(i)
				
				IF bInitDancing
					IF NOT IS_STRIPCLUB_MP()
						SET_POLEDANCER_STATE(i, POLEDANCER_STATE_DANCE)
					ELSE
						SET_POLEDANCER_STATE(i, POLEDANCER_STATE_DANCE_ENTER)
					ENDIF
					SET_STRIPPER_AI_STATE(i, AI_STATE_POLE_DANCE)
				ELSE
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_WAIT_IN_DRESSING_ROOM)
					SET_STRIPPER_AI_STATE(i, AI_STATE_WANDER)
				ENDIF
				
				EXIT //only create one stripper at a time
			ENDIF
		ENDIF
    ENDREPEAT
	
ENDPROC
