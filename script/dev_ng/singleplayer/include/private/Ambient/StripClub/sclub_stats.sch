USING "net_leaderboards.sch"

FUNC BOOL IS_STRIPCLUB_LEADERBOARD_AVAILABLE_FOR_WRITE()
	IF  NETWORK_IS_SIGNED_ONLINE() AND NETWORK_IS_SIGNED_IN() AND SCRIPT_IS_CLOUD_AVAILABLE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
INT iNumLapDance
INT iCashSpentInClub
INT iNumTimesFuckedBootycall
BOOL bUpdateStripclubLeaderboard = FALSE

FUNC INT GET_SCLUB_STAT_SCORE()
	RETURN 0
ENDFUNC

FUNC INT GET_SCLUB_STAT_NUM_LAP_DANCES()
	RETURN iNumLapDance
ENDFUNC

FUNC INT GET_SCLUB_STAT_CASH_SPENT()
	RETURN iCashSpentInClub
ENDFUNC

FUNC INT GET_BOOTYCALL_STAT_NUM_TIMES_FUCKED_BOOTYCALL()
	RETURN iNumTimesFuckedBootycall
ENDFUNC

PROC STRIPCLUB_WRITE_TO_LEADERBOARD(BOOL bMP)

	
	CPRINTLN(DEBUG_SCLUB, "STRIPCLUB_WRITE_TO_LEADERBOARD")

	IF NOT IS_STRIPCLUB_LEADERBOARD_AVAILABLE_FOR_WRITE()
		EXIT //not online
	ENDIF
	
	IF NOT bUpdateStripclubLeaderboard
		CPRINTLN(DEBUG_SCLUB, "NOT bUpdateStripclubLeaderboard")
		EXIT //didn't change any of the leaderboard values
	ENDIF
	
//Inputs: 4
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_NUM_LAP_DANCES (int)
//	LB_INPUT_COL_NUM_BOOTY_CALLS (int)
//	LB_INPUT_COL_CASH (int)
//Columns: 4
//	SCORE_COLUMN ( AGG_Sum ) - InputId: LB_INPUT_COL_SCORE
//	NUM_LAP_DANCES ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_LAP_DANCES
//	NUM_BOOTY_CALLS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_BOOTY_CALLS
//	CASH_SPENT ( AGG_Sum ) - InputId: LB_INPUT_COL_CASH
//Instances: 3
//	GameType,Location,Type
//	GameType,Location,Type,Challenge
//	GameType,Location,Type,ScEvent

	TEXT_LABEL_31 categoryNames[3] 
    TEXT_LABEL_23 uniqueIdentifiers[3]
    categoryNames[0] = "GameType" 
    categoryNames[1] = "Location" 
	categoryNames[2] = "Type" 
    
	// Begin setting the leaderboard data types
	IF bMP
		uniqueIdentifiers[0] = "MP"
	ELSE
		uniqueIdentifiers[0] = "SP"	
	ENDIF
	
	uniqueIdentifiers[1] = "City" 
	uniqueIdentifiers[2] = "????"
	
	CPRINTLN(DEBUG_SCLUB, "Writing to stripclub leaderboard. Num of dances is ", iNumLapDance)
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_STRIP_CLUB, uniqueIdentifiers, categoryNames, 3)
	
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,  			GET_SCLUB_STAT_SCORE(), 		 					0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_LAP_DANCES,  GET_SCLUB_STAT_NUM_LAP_DANCES(), 					0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_BOOTY_CALLS, GET_BOOTYCALL_STAT_NUM_TIMES_FUCKED_BOOTYCALL(), 	0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CASH,  			GET_SCLUB_STAT_CASH_SPENT(), 		 				0.0)
	ENDIF
	
	iNumLapDance = 0
	iCashSpentInClub = 0
	iNumTimesFuckedBootycall = 0
	bUpdateStripclubLeaderboard = FALSE
ENDPROC

PROC INC_SCLUB_STAT_NUM_LAPDANCES()
	CPRINTLN(DEBUG_SCLUB, "Increment lapdance leaderboard stat!")
	iNumLapDance++
	bUpdateStripclubLeaderboard = TRUE
ENDPROC

PROC INC_SCLUB_STAT_CASH_SPENT(INT iValue)
	iCashSpentInClub += iValue
	bUpdateStripclubLeaderboard = TRUE
ENDPROC

PROC INC_BOOTYCALL_STAT_NUM_TIMES_FUCKED_BOOTYCALL()
	iNumTimesFuckedBootycall++
	bUpdateStripclubLeaderboard = TRUE
ENDPROC
