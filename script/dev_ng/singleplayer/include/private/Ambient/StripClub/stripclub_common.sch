
// Model Names
//S_M_Y_Barman_01
//a_m_y_breakdance_01
//s_m_y_doorman_01
//a_m_y_hipster_01

USING "stripclub.sch"
USING "net_fps_cam.sch"

CONST_INT iThrowOutCutscene 1

//Value can change
INT DANCE_COST = 40
INT SC_DRINK_COST = 10

// Script-specific entities
STRIP_CLUB_ENUM thisStripClub
STRIP_CLUB_STAGE_ENUM stripClubStage = STAGE_INIT_CLUB
STAGE_STATE_ENUM stageState
DANCE_STAGE_ENUM danceStage = DANCE_INIT
DANCE_STAGE_ENUM nextDanceStage
BAR_STAGE_ENUM barStage = BAR_NOT_ACTIVE
RAIL_STAGE_ENUM railStage = RAIL_NOT_ACTIVE

#IF NOT MP_STRIPCLUB
NAMED_STRIPPER_PED namedStripper[MAX_NUMBER_NAMED_STRIPPERS]
#ENDIF
#IF MP_STRIPCLUB 
NAMED_STRIPPER_PED namedStripper[MAX_NUMBER_NAMED_STRIPPERS_MP]
#ENDIF


RAIL_SEGMENT railSegment[NUMBER_STRIP_CLUB_RAIL_SEGMENTS]
BOOTY_CALL_CONTACT_ENUM likeHUDName[2]
STRIP_CLUB_INSIDE_STATE	stripclubInside = SC_INSIDE_INIT

STRIPCLUB_GOTO_ROOM_STATE gotoStage = STRIPCLUB_GOTO_ROOM_INIT
TOUCHING_STRIPPER_STATE TouchingStripperState

STRIPCLUB_CAM_ENUM previousLapdanceCamera
STRIPCLUB_CAM_ENUM currentLapdanceCamera
BOUNCER_STATE enumBouncerStates //only front door and vip bouncer use this, patroling bouncer uses different SM
BOUNCER_PATROL_STATE enumBouncerPatrolState
DJ_STATE djState
FRONT_CASHIER_FLAGS enumCashierFlags

// Peds
PED_INDEX clubStaffPed[MAX_NUMBER_CLUB_STAFF]
PED_INDEX sendHostilePed
PED_INDEX pedForPrivateDance
//Delayed aggression bitset
INT BS_DelayedAggro

REL_GROUP_HASH stripClubRelationshipHash

// Object
OBJECT_INDEX cashObject[MAX_NUMBER_OF_STRIPCLUB_CASH_OBJECTS]
OBJECT_INDEX  objShotGlass, objDrinkBottle

// Interiors
INTERIOR_INSTANCE_INDEX stripClubInterior

// Blips
BLIP_INDEX stripperBlip[2]
BLIP_INDEX bouncerBlip[2]

SCENARIO_BLOCKING_INDEX blockCustomerSpawn = NULL	//Blocks all customers from spawning inside the stripclub

// Cameras
CAMERA_INDEX stripclubCam
CAMERA_INDEX stripclubCutCam
CAMERA_INDEX stripclubCutCamEnd
CAMERA_INDEX stripclubInterpFromCam
CAMERA_INDEX stripclubCamStart
CAMERA_INDEX stripclubCamEnd
CAMERA_INDEX stripclubCamBouncer
CAMERA_INDEX stripCamAnim

#IF IS_DEBUG_BUILD
#IF USE_STRIPCLUB_DEBUG
VECTOR vStripDebugVector = <<117.155, -1294.798, 29.26949>>
FLOAT fDebugHeading = 1.25
FLOAT fDebugFloat = 0.90
INT iDebugInt = 0
INT iDebugDisplayStriperWanderState = -1
BOOL bDebugDisplayLeanPoints
BOOL bDebugSpewApproachPlayer
BOOL bAllowTouch

#IF MP_STRIPCLUB
BOOL bTestPostHeistScene
INT iTestPostHeistIndex
#ENDIF
#ENDIF
#ENDIF

ENUM ALLOW_STRIPPER_INTERACTION
	ALLOW_STRIPPER_NONE    = 0,
	ALLOW_STRIPPER_ONE     = 1,
	ALLOW_STRIPPER_TWO     = 2,
	ALLOW_STRIPPER_THREE   = 4,
	ALLOW_STRIPPER_FOUR    = 8
ENDENUM

ALLOW_STRIPPER_INTERACTION bAllowStripperInteraction

FIRST_PERSON_CAM_STRUCT fpStripCam
//FIRST_PERSON_CAM_STRUCT fpStripCam2

BOOL bForceLapdanceHeadInvisible
BOOL bLocalBlockStripClubPedsForCongratsScreen

// Floats
FLOAT fLapDancePhase
//FLOAT fLikeMeterPosX
//FLOAT fLikeMeterPosY
//FLOAT fLikeMeterHeight
//FLOAT fLikeMeterWidth

// Ints
INT iCurrentStripperInteraction = -1
INT iCurrentStripperForDance = -1
INT iPoleDancerAtPlayer = -1
INT iStripperForApproach = -1

INT iUpdateThisStripperSMThisFrame = 0 //used to stagger updates to the stripper wander state machine
INT iCurrentPatronCheck = 0

INT iRefusalCount

INT iBouncerWarnedTimes //amount of times the bouncer warned the player this lapdance

INT iStripclubStopRenderCamAt

INT iRailUpdate
INT iRailSegment
INT iRailFlirtTime
VECTOR vSavedRailPos
FLOAT fSavedRailAngle

CONST_FLOAT k_STRIPCLUB_RAIL_DISTANCE_TOLERANCE		0.5
ECOMPASS savedDirection


INT iNextCaughtTouchingTime
INT iNextDanceInputTime
INT iAnnouncerTime
INT iCurrentLapDanceSeat = -1
INT iSecondaryStripper = -1
INT iStripClubBits_Streaming
INT iStripClubBits_Cutscenes
INT iStripClubBits_Workers
INT iStripClubBits_General
INT iStripClubBits_General2
INT iStripClubBits_Stand_Locations
INT iNextBouncerPatrolTime
INT iNextUpdateLookAtTime
INT iNextBouncerYellTime
INT iLastTimeDrunk
INT iStartPostHeistCutsceneTime
INT iLastAdjustTime

INT iCurrentDJIdleVariation = 1, iOldDJIdleVariation = -1
INT iCurrentStripperLDVariation = 0//, iOldStripperLDVariation = -1
INT iDJScene = -1, iLapDanceScene = -1, iApproachDanceScene= -1, iDanceOfferScene = -1, iThrowOutScene = -1, iMakeItRainScene = -1
INT iWalkToStageScene = -1, iWalkToPoleScene = -1, iPoleEnterScene = -1, iPoleDanceScene = -1, iPoleExitScene = -1, iStageExitScene = -1

INT iMoneyGivenToPoleDancer
INT iNumOfPoleDances
INT iStripButtonHoldTime

HUD_COLOURS likeHudColour
INT iFlashLength
INT iLikeHUDAfter[2]
FLOAT fTalkHudValue

//INT iColor_GreenStandard = INT_COLOUR(87, 124, 88, 255)

//INT iDebugStripThrottle = 2
INT candidateID = NO_CANDIDATE_ID
INT candidateIDLapdance = NO_CANDIDATE_ID
INT iCashObjectToCreate

INT iHelmetIndex = -1
INT iHelmetTexture = -1
PED_COMP_NAME_ENUM eHeadProp = DUMMY_PED_COMP
PED_COMP_NAME_ENUM eFeetProp = DUMMY_PED_COMP
PED_COMP_NAME_ENUM eMaskProp = DUMMY_PED_COMP
PED_COMP_NAME_ENUM eTeethProp = DUMMY_PED_COMP
PED_COMP_NAME_ENUM eJacketProp = DUMMY_PED_COMP

INT iNavBlockingObject = -1
INT iInClubNavBlockingObject[4]

INT iLookAtStripperDuringDance
INT iNextUpdateDanceLookAtTime
PED_BONETAG boneLookAtBodyDuringDance

VECTOR vLapdanceCamPos
VECTOR vCreateCashPos

// Other variables
structPedsForConversation stripClubConversation

#IF NOT MP_STRIPCLUB
	STRIPCLUB_FRIEND_DATA clubFriends[2]
	structTimer	tFriendInClub
#ENDIF

structTimer	tControl
structTimer	tInteraction
structTimer	tDanceDialogue
structTimer	tPlayerInClub
structTimer tTimeSpentInClub

//structTimer	tBouncerShoved

//VECTOR debugVector = <<0,0,0.78>>

FUNC STRING GET_STRIP_CLUB_DRINKING_STRIPT_NAME()
	RETURN "stripclub_drinking"
ENDFUNC

FUNC MODEL_NAMES GET_STRIPCLUB_CASH_MODEL()
	RETURN Prop_Anim_Cash_note_b
ENDFUNC

FUNC MODEL_NAMES GET_STRIPCLUB_CASH_PILE_MODEL()
	RETURN Prop_Anim_Cash_Pile_01
ENDFUNC

FUNC MODEL_NAMES GET_STRIPCLUB_DRINK_GLASS_MODEL()
	RETURN P_CS_Shot_Glass_2_S
ENDFUNC

FUNC MODEL_NAMES GET_STRIPCLUB_DRINK_BOTTLE_MODEL()
	RETURN P_Whiskey_Bottle_S
ENDFUNC

FUNC MODEL_NAMES GET_WORKER_MODEL(STRIP_CLUB_STAFF_ENUM eWorkerType)
	SWITCH eWorkerType	
		CASE STAFF_BOUNCER_VIP
		CASE STAFF_BOUNCER_PATROL
			RETURN S_M_M_BOUNCER_01
		BREAK
		
		CASE STAFF_BARMAN
		CASE STAFF_FRONTDESK
			RETURN	S_F_Y_Bartender_01
			//RETURN S_M_M_HIGHSEC_01
		BREAK		
		
		CASE STAFF_ANNOUNCER
			RETURN A_M_Y_HIPSTER_01
		BREAK			
	ENDSWITCH
	RETURN S_M_M_BOUNCER_01
ENDFUNC


FUNC VECTOR GET_STRIPCLUB_FRIEND_RAIL_POS(INT iIndex)
	IF iIndex = 0
		RETURN << 115.354, -1289.490, 27.27 >>
	ELIF iIndex = 1
		RETURN << 111.780, -1283.656, 27.26 >>
	ENDIF
	
	RETURN << 115.354, -1289.490, 27.27 >>
ENDFUNC

//VECTOR vTriggerVIPRoom = <<117.81, -1294.39, 29.40>>
FUNC VECTOR GET_STRIPCLUB_TRIGGER_VIP_ROOM()
	RETURN <<117.81, -1294.39, 29.40>>
ENDFUNC

//VECTOR vVIPRoomMin = <<111.10, -1295.46, 25>>
FUNC VECTOR GET_STRIPCLUB_VIP_ROOM_MIN()
	RETURN <<111.10, -1295.46, 25>>
ENDFUNC

//VECTOR vVIPRoomMax = <<116.0, -1304.66, 35>>
FUNC VECTOR GET_STRIPCLUB_VIP_ROOM_MAX()
	RETURN <<116.0, -1304.66, 35>>
ENDFUNC

#IF IS_DEBUG_BUILD
//VECTOR vBehindClub = <<94.24, -1282.41, 29.28>>
FUNC VECTOR GET_STRIPCLUB_BEHIND_CLUB()
	RETURN <<94.24, -1282.41, 29.28>>
ENDFUNC
#ENDIF

//VECTOR vStripperVIPWait = <<120.25, -1293.07, 29.27>>
FUNC VECTOR GET_STRIPCLUB_STRIPPER_VIP_WAIT()
	RETURN <<120.25, -1293.07, 28.27>>
ENDFUNC

//VECTOR v2ndStripperVIPWait = <<121.46, -1292.68, 28.27>>
FUNC VECTOR GET_STRIPCLUB_SECOND_STRIPPER_VIP_WAIT()
	RETURN <<121.46, -1292.68, 28.27>>
ENDFUNC

//VECTOR vVIP2ndDancerDanceOffset = <<0.5, 0.15, 0>>
FUNC VECTOR GET_STRIPCLUB_VIP_SECOND_DANCER_DANCE_OFFSET()
	RETURN <<0.46, -0.09, 0>>
ENDFUNC

FUNC VECTOR GET_VIP_ROOM_DOOR_COORD()
	RETURN <<117.27327, -1294.71753, 28.27479>>
ENDFUNC

//VECTOR vVIPDoorLoc = <<116.13, -1294.64, 29.42>>
FUNC VECTOR GET_STRIPCLUB_VIP_DOOR_LOC()
	RETURN <<116.13, -1294.64, 29.42>>
ENDFUNC

FUNC VECTOR GET_STRIPCLUB_DRESSING_ROOM_DOOR_LOC()
	RETURN <<113.98, -1297.43, 29.42>>
ENDFUNC

FUNC VECTOR GET_STRIPCLUB_FRONT_DOOR_LOC()
	RETURN <<127.96, -1298.51, 29.42>>
ENDFUNC

FUNC VECTOR GET_STRIPCLUB_BACK_DOOR_LOC()
	RETURN <<96.0920, -1284.8536, 29.4319>>
ENDFUNC

FUNC VECTOR GET_BOUNCER_PARTOL_START_POS()
	RETURN << 116.2204, -1297.7778, 28.0192 >>
ENDFUNC

FUNC FLOAT GET_BOUNCER_PATROL_START_HEADING()
	RETURN GET_HEADING_BETWEEN_VECTORS(GET_BOUNCER_PARTOL_START_POS(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
ENDFUNC

FUNC VECTOR GET_BOUNCER_PARTOL_END_POS()
	RETURN << 118.4887, -1300.0215, 28.0194 >>
ENDFUNC

FUNC FLOAT GET_BOUNCER_PATROL_END_HEADING()
	RETURN 130.0314
ENDFUNC

FUNC VECTOR GET_STRIPPER_VIP_EXIT_START_POINT(BOOL bSecondary)
	IF bSecondary
		RETURN <<116.9946, -1300.2452, 28.0190>>
	ELSE
		RETURN <<114.5290, -1297.4677, 28.2688>> //<<115.9361, -1299.3242, 28.0189>>
	ENDIF
ENDFUNC

FUNC VECTOR GET_STRIPCLUB_SHOT_GLASS_POS()
	RETURN <<128.36, -1283.11, 29.29>>
ENDFUNC

FUNC VECTOR GET_STRIPCLUB_BOTTLE_POS()
	RETURN <<128.15, -1282.80, 29.45>>
ENDFUNC

FUNC INT COLOR_GREEN_DARK()
	RETURN INT_COLOUR(64, 94, 75, 255)
ENDFUNC

FUNC WEAPON_TYPE GET_STAFF_WEAPON_TYPE()
	RETURN WEAPONTYPE_PISTOL
ENDFUNC

FUNC BOOL IS_STRIPCLUB_ENTITY_CONTROLLED_BY_PLAYER(ENTITY_INDEX ent, INT iMP = MP_STRIPCLUB)
	
	IF iMP != 0
		RETURN NETWORK_HAS_CONTROL_OF_ENTITY(ent)
	ENDIF
	
	RETURN TRUE
ENDFUNC

// get position for dance wait
FUNC VECTOR GET_STRIP_CLUB_DANCE_WAIT_POSITION(INT i)
	SWITCH thisStripClub
		CASE STRIP_CLUB_LOW
			SWITCH i 
				CASE 0
				DEFAULT
				//CASE 0
					RETURN << 117.7688, -1300.6309, 28.0192 >>//<<114.50, -1302.59, 28.02>>
				BREAK

			ENDSWITCH
		BREAK

	ENDSWITCH	

	RETURN GET_ENTITY_COORDS(PLAYER_PED_ID())
ENDFUNC

// get heading for dance wait
FUNC FLOAT GET_STRIP_CLUB_DANCE_WAIT_HEADING(INT i)
	SWITCH thisStripClub
		CASE STRIP_CLUB_LOW
			SWITCH i 
				CASE 0
				DEFAULT
					RETURN 129.65
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	

	RETURN 0.0
ENDFUNC

// get position for dance
FUNC VECTOR GET_STRIP_CLUB_STRIPPER_DANCE_POSITION()
	SWITCH thisStripClub
		CASE STRIP_CLUB_LOW
			RETURN << 116.5444, -1303.1603, 28.7186 >>
		BREAK
		/*
		CASE STRIP_CLUB_MED
			RETURN <<-368.20, 210.09, 83.22>>
		BREAK
		CASE STRIP_CLUB_HIGH
			RETURN <<967.47, -1804.34, 30.16>>
		BREAK
		*/
	ENDSWITCH
	
	RETURN GET_ENTITY_COORDS(PLAYER_PED_ID())
ENDFUNC

// get position for dance
FUNC VECTOR GET_STRIP_CLUB_SECONDARY_STRIPPER_DANCE_POSITION()
	SWITCH thisStripClub
		CASE STRIP_CLUB_LOW
			RETURN <<116.89, -1302.44, 28.25>>
		BREAK
		/*
		CASE STRIP_CLUB_MED
			RETURN <<-368.39, 209.48, 83.11>>
		BREAK
		CASE STRIP_CLUB_HIGH
			RETURN <<966.91, -1804.27, 30.16>>
		BREAK
		*/
	ENDSWITCH
	
	RETURN GET_ENTITY_COORDS(PLAYER_PED_ID())
ENDFUNC

// get heading for dance
FUNC FLOAT GET_STRIP_CLUB_DANCE_HEADING()
	SWITCH thisStripClub
		CASE STRIP_CLUB_LOW
			RETURN 30.02
		BREAK
		/*
		CASE STRIP_CLUB_MED
			RETURN 86.94
		BREAK
		CASE STRIP_CLUB_HIGH
			RETURN -6.11
		BREAK
		*/
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

// get position for dance ready
FUNC VECTOR GET_STRIP_CLUB_DANCE_READY_POSITION(INT i)
	SWITCH thisStripClub
		CASE STRIP_CLUB_LOW
			SWITCH i 
				CASE 0
					RETURN GET_STRIPCLUB_STRIPPER_VIP_WAIT() //<<117.7688, -1300.6309, 28.0192>>//<<114.89, -1301.91, 28.02>>
				BREAK
				CASE 1
					RETURN GET_STRIPCLUB_STRIPPER_VIP_WAIT() //<<117.7688, -1300.6309, 28.0192>>//<<115.65, -1301.39, 28.02>>
				BREAK
			ENDSWITCH
		BREAK
		/*
		CASE STRIP_CLUB_MED
			SWITCH i 
				CASE 0
					RETURN <<-370.16, 209.13, 82.66>>
				BREAK
				CASE 1
					RETURN <<-370.14, 210.26, 82.66>>
				BREAK
			ENDSWITCH
		BREAK
		CASE STRIP_CLUB_HIGH
			SWITCH i 
				CASE 0
					RETURN <<966.97, -1801.90, 30.16>>
				BREAK
				CASE 1
					RETURN <<968.46, -1802.18, 30.16>>
				BREAK
			ENDSWITCH
		BREAK
		*/
	ENDSWITCH	
	
	RETURN GET_ENTITY_COORDS(PLAYER_PED_ID())
ENDFUNC

// get dressing room location for generic stripper
PROC GET_GENERIC_STRIPPER_DRESSING_ROOM_LOC(INT i, VECTOR &vStorePos, FLOAT &fStoreRot)
	i = i

	vStorePos = <<111.9364, -1297.7983, 28.2688>>
	fStoreRot = 284.1101

ENDPROC

FUNC VECTOR GET_STRIPPER_POLE_LOC(INT index)
	
	SWITCH index
		CASE 0
			RETURN << 112.84, -1287.38, 27.46 >>
		BREAK
		CASE 1
			RETURN <<104.41, -1294.67, 28.26>>
		BREAK
		CASE 2
			RETURN <<102.38, -1291.28, 28.26>>	
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

//can hard code later, need to adjust
PROC GET_STRIPPER_POLE_LOC_AND_HEADING(INT index, VECTOR &vPolePos, FLOAT &fPoleHeading)
	
	vPolePos = GET_STRIPPER_POLE_LOC(index)
	
	SWITCH index
		CASE 0
			fPoleHeading = -60.46
		BREAK
		CASE 1
			fPoleHeading = -74.57
		BREAK
		CASE 2
			fPoleHeading =-18.70	
		BREAK
	ENDSWITCH					
ENDPROC


FUNC FLOAT GET_INIT_WORKER_HEADING(STRIP_CLUB_STAFF_ENUM eWorkerType)
	SWITCH eWorkerType
		CASE STAFF_BOUNCER_VIP
			RETURN 35.2391
		BREAK
		
		CASE STAFF_BARMAN
			RETURN 116.99
		BREAK
		
		CASE STAFF_ANNOUNCER
			RETURN 129.69
		BREAK
	ENDSWITCH
	DEBUG_MESSAGE("GET_INIT_BOUNCER_HEADING: bad bouncer type")		
	RETURN 0.0
ENDFUNC

// get initial staff location
PROC GET_INIT_CLUB_STAFF_LOC(STRIP_CLUB_STAFF_ENUM getStaff, VECTOR &vStorePos, FLOAT &fStoreRot)
	SWITCH thisStripClub
		// low
		CASE STRIP_CLUB_LOW
			SWITCH getStaff
				CASE STAFF_BARMAN
					vStorePos = <<128.78, -1282.80, 28.27>>
				BREAK
				CASE STAFF_BOUNCER_VIP
					vStorePos = <<118.7842, -1295.5625, 29.2716>>	
				BREAK
				CASE STAFF_BOUNCER_PATROL
					vStorePos = GET_BOUNCER_PARTOL_START_POS()
				BREAK
				CASE STAFF_ANNOUNCER
					vStorePos = <<120.89, -1280.67, 28.48>>				
				BREAK
				CASE STAFF_FRONTDESK
					vStorePos = <<124.56, -1298.95, 28.26>>				
				BREAK
				DEFAULT
					SCRIPT_ASSERT("GET_INIT_CLUB_STAFF_LOC: no staff loc for this enum")
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	fStoreRot = GET_INIT_WORKER_HEADING(getStaff)
ENDPROC

FUNC FLOAT GET_CLUB_STAND_HEADING(INT index, VECTOR vStandPos)
	
	FLOAT fDefaultHeading
	PED_INDEX pedFindCustomer
	
	SWITCH index
		CASE 0
			fDefaultHeading = 212.9749
		BREAK
		CASE 1
			fDefaultHeading = 191.0365
		BREAK
		CASE 2
			fDefaultHeading = 103.8310
		BREAK
		CASE 3
			fDefaultHeading = 104.9055
		BREAK
		CASE 4
			fDefaultHeading = 105.2947
		BREAK
		CASE 5
			fDefaultHeading = 41.7561
		BREAK
		CASE 6
			fDefaultHeading = 310.2245
		BREAK
	ENDSWITCH
	
	SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
	IF GET_CLOSEST_PED(vStandPos, 3.0, TRUE, FALSE, pedFindCustomer, FALSE, TRUE, PEDTYPE_MISSION) //look for anyone near by
		IF NOT IS_PED_INJURED(pedFindCustomer)
			RETURN GET_HEADING_BETWEEN_VECTORS(vStandPos, GET_ENTITY_COORDS(pedFindCustomer))
		ENDIF
	ENDIF

	RETURN fDefaultHeading
ENDFUNC

// get random stripper stand location
PROC GET_RANDOM_CLUB_STAND_LOCAND_HEADING(INT index, VECTOR &vStorePos, FLOAT &fStoreHeading, BOOL bCheckValid = FALSE)	
	

	SWITCH thisStripClub
		// low
		CASE STRIP_CLUB_LOW
			SWITCH index
				CASE 0
					vStorePos = <<102.6785, -1286.3976, 27.2610>>
				BREAK
				CASE 1
					vStorePos = <<106.4051, -1284.3169, 27.2610>>
				BREAK
				CASE 2
					vStorePos = <<118.0304, -1282.9344, 27.2761>>
				BREAK
				CASE 3
					vStorePos = <<119.6163, -1284.9426, 27.2763>>
				BREAK
				CASE 4
					vStorePos = <<121.8866, -1287.7062, 27.2609>>
				BREAK
				CASE 5
					vStorePos = <<111.2377, -1293.1378, 27.2610>>
				BREAK
				CASE 6
					vStorePos = <<111.2377, -1293.1378, 27.2610>>
				BREAK
				DEFAULT
					IF bCheckValid
						PRINTLN("Stand location index ", index)
						SCRIPT_ASSERT("Invalid stripper location index")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK		
	ENDSWITCH
	
	
	fStoreHeading = GET_CLUB_STAND_HEADING(index, vStorePos)
ENDPROC


FUNC VECTOR GET_VIP_SEAT_POS(INT iSeat)

	SWITCH iSeat
		CASE 0
			RETURN <<114.29, -1299.73, 28.72 >>	
		BREAK
		CASE 1
			RETURN << 116.90, -1303.81, 28.72 >>
		BREAK
		CASE 2
			RETURN << 114.83, -1304.97, 28.72 >>
		BREAK
		CASE 3
			RETURN << 112.42, -1300.88, 28.72 >>
		BREAK
		CASE 4
			RETURN << 110.67, -1301.90, 28.72 >>
		BREAK
		CASE 5
			RETURN << 113.08, -1306.12, 28.72 >>
		BREAK
		CASE 6
			RETURN <<119.14, -1302.68, 28.72>>
		BREAK
	ENDSWITCH
		
	RETURN <<0,0,0>>
ENDFUNC

//The position of the chair the lapdance animations are offset from
FUNC VECTOR GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(INT iSeat, FLOAT &fHeading)

	SWITCH iSeat
		CASE 0
			fHeading = 30 + 180
			RETURN <<114.22, -1299.58, 28.26 >>	- <<-0.04, 0.07, -0.5>>
		BREAK
		CASE 1
			fHeading = 30
			RETURN <<117.04, -1303.99, 28.26>> - <<0.04, -0.07, -0.5>>
		BREAK
		CASE 2
			fHeading = 30
			RETURN << 114.95, -1305.22, 28.26 >> - <<0.04, -0.07, -0.5>>
		BREAK
		CASE 3
			fHeading = 30 + 180
			RETURN << 112.33, -1300.68, 28.26 >> - <<-0.04, 0.07, -0.5>>
		BREAK
		CASE 4
			fHeading = 30 + 180
			RETURN << 110.54, -1301.70, 28.26 >> - <<-0.04, 0.07, -0.5>>
		BREAK
		CASE 5
			fHeading = 30
			RETURN << 113.13, -1306.27, 28.26 >> - <<0.04, -0.07, -0.5>>
		BREAK
		CASE 6
			fHeading = 30
			RETURN <<119.17, -1302.76, 28.26>> - <<0.04, -0.07, -0.5>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_VIP_SEAT_END_CAMERA_OFFSET(INT iSeat)

	SWITCH iSeat
		CASE 0
			RETURN << -0.103294, 0.178223, 0.644266 >>
		BREAK
		CASE 1
			RETURN << 0.0763474, -0.132324, 0.659601 >> //single player seat
		BREAK
		CASE 2
			RETURN  << 0.103294, -0.178223, 0.644266 >>
		BREAK
		CASE 3
			RETURN << -0.103294, 0.178223, 0.644266 >>
		BREAK
		CASE 4
			RETURN << -0.103294, 0.178223, 0.644266 >>
		BREAK
		CASE 5
			RETURN  << 0.0763474, -0.132324, 0.659601 >>
		BREAK
		CASE 6
			RETURN << 0.0763474, -0.132324, 0.659601 >>
		BREAK
	ENDSWITCH

	RETURN <<0, 0, 0>>
ENDFUNC

PROC GET_VIP_SEAT_SIT_CAMERA(INT iSeat, VECTOR &vPos, VECTOR &vRot)

	SWITCH iSeat
		CASE 0
			vPos = << 116.5565, -1302.6790, 30.3969 >>
			vRot = << -24.2974, 0.0000, 33.8366 >>
		BREAK
		CASE 1
			vPos = << 114.5708, -1300.8545, 30.1768 >>
			vRot = << -18.3694, 0.0000, -128.5567 >>
		BREAK
		CASE 2
			vPos = << 112.6060, -1302.0500, 30.2981 >>
			vRot = << -17.0770, 0.0000, -145.0869 >>
		BREAK
		CASE 3
			vPos = << 114.0932, -1303.4563, 30.8895 >>
			vRot = << -33.4638, 0.0000, 19.5986 >>
		BREAK
		CASE 4
			vPos = << 112.2256, -1304.6364, 30.1326 >>
			vRot = << -18.5982, 0.0000, 23.9965 >>
		BREAK
		CASE 5
			vPos = << 111.4691, -1303.2705, 30.4025 >>
			vRot =  << -26.7558, -0.0000, -156.8465 >>
		BREAK
		CASE 6
			vPos = << 116.6096, -1298.6573, 30.4177 >>
			vRot = << -17.3515, -0.0000, -149.0389 >>
		BREAK
	ENDSWITCH

ENDPROC

FUNC VECTOR GET_VIP_SEAT_DANCER_POS(INT iSeat)
	SWITCH iSeat
		CASE 0
			RETURN <<114.91, -1300.73, 28.27>>
		BREAK
		CASE 1
			RETURN <<116.34, -1302.75, 28.27>>
		BREAK
		CASE 2
			RETURN <<114.20, -1303.95, 28.27>>
		BREAK
		CASE 3
			RETURN << 112.97, -1301.67, 28.27 >>
		BREAK
		CASE 4
			RETURN <<111.24, -1302.79, 28.27 >>
		BREAK
		CASE 5
			RETURN <<112.33, -1304.91, 28.27>>
		BREAK
		CASE 6
			RETURN <<118.53, -1301.46, 28.27>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_VIP_SEAT_HEADING(INT iSeat)
	SWITCH iSeat
		CASE 0
			RETURN 20.8127+180
		BREAK
		CASE 1
			RETURN 20.8127
		BREAK
		CASE 2
			RETURN 20.8127
		BREAK
		CASE 3
			RETURN 20.8127+180
		BREAK
		CASE 4
			RETURN 20.8127+180
		BREAK
		CASE 5
			RETURN 20.8127
		BREAK
		CASE 6
			RETURN 20.8127
		BREAK
	ENDSWITCH
	
	RETURN 20.8127+180
ENDFUNC

FUNC VECTOR GET_VIP_CAM_OFFSET(STRIPCLUB_CAM_ENUM camType, INT iSeat, BOOL bDuo)
	
	IF camType = STRCLUB_CAM_DANCE_ANGLED
		SWITCH iSeat
			CASE 0
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
			CASE 1
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
			CASE 2
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
			CASE 3
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
			CASE 4
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
			CASE 5
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
			CASE 6
				IF bDuo
					RETURN <<0.040, 1.580, 1.419>>
				ELSE
					RETURN <<-1.0, 1.18, -0.28>>
				ENDIF
			BREAK
		ENDSWITCH
	
	ELIF camType = STRCLUB_CAM_DANCE_FACE_PLAYER
		SWITCH iSeat
			CASE 0
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
			CASE 1
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
			CASE 2
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
			CASE 3
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
			CASE 4
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
			CASE 5
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
			CASE 6
				IF bDuo
					RETURN <<1.1, 1.82, 0.82>>
				ELSE
					RETURN <<0.72, 1.36, 0.56>>
				ENDIF
			BREAK
		ENDSWITCH
	ELIF camType = STRCLUB_CAM_DANCE_BEHIND_PLAYER
			SWITCH iSeat
			CASE 0
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
			CASE 1
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
			CASE 2
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
			CASE 3
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
			CASE 4
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
			CASE 5
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
			CASE 6
				IF bDuo
					RETURN <<-0.943, -0.91, 0.688>>
				ELSE
					RETURN <<-0.79, -0.85, 0.597>>
				ENDIF
			BREAK
		ENDSWITCH	
	ENDIF

	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_STRIPCLUB_VIP_SECOND_DANCER_GOTO_OFFSET(INT iStripper)

	IF iStripper = 0 OR iStripper = 3 OR iStripper = 4
		RETURN <<1.11, 0.65, 0>>
	ELSE
		RETURN <<0.78, -0.72, 0>>
	ENDIF
ENDFUNC

FUNC VECTOR GET_VIP_DANCER_WAITING_POS(INT iStripper)

		SWITCH iStripper
		CASE 0
			RETURN 	<< 113.81, -1300.36, 28.41 >>
		BREAK
		CASE 1
			RETURN << 117.45, -1303.20, 28.41 >>
		BREAK
		CASE 2
			RETURN << 115.36, -1304.44, 28.41 >>
		BREAK
		CASE 3
			RETURN << 111.92, -1301.46, 28.41 >>
		BREAK
		CASE 4
			RETURN 	<< 110.13, -1302.48, 28.41 >>
		BREAK
		CASE 5
			//RETURN << >>
		BREAK
		CASE 6
			//RETURN << >>
		BREAK
	ENDSWITCH

	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_VIP_DANCER_WAITING_HEADING(INT iSeat)

	SWITCH iSeat
		CASE 0
			RETURN 22.03 + 180
		BREAK
		CASE 1
			RETURN 22.03
		BREAK
		CASE 2
			RETURN 22.03
		BREAK
		CASE 3
			RETURN 22.03 + 180
		BREAK
		CASE 4
			RETURN 22.03 + 180
		BREAK
		CASE 5
			RETURN 22.03
		BREAK
		CASE 6
			RETURN 22.03
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_VIP_OUTRO_CUTSCENE_END_COORD(STRIPCLUB_WAYPOINT_RECORDINGS clubWaypointRecording)
	
	IF clubWaypointRecording = SWR_VIP_INTRO
		RETURN << 114.1263, -1296.7617, 28.2689 >>
	ELIF clubWaypointRecording = SWR_VIP_OUTRO
		RETURN <<121.3242, -1292.9474, 28.2791>>
	ELIF clubWaypointRecording = SWR_FRIEND_ENTER
		RETURN <<119.43, -1289.14, 27.26>>
	ELIF clubWaypointRecording = SWR_STRIPPER_EXIT
		RETURN <<110.6793, -1298.3816, 28.2688>>
	ENDIF

	PRINTLN("Invalid waypoint recording enum")
	RETURN <<0,0,0>>
ENDFUNC

//Why isn't this a shared function?
PROC SWAP_INTS(INT &iA, INT &iB)
	iA = iA + iB
	iB = iA - iB
	iA = iA - iB
ENDPROC

FUNC BOOL IS_STRIPCLUB_MP()

	#IF MP_STRIPCLUB
		RETURN TRUE
	#ENDIF
	
	IF IS_FREEMODE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_NAMED_STRIPPERS()

	IF IS_STRIPCLUB_MP()
		RETURN MAX_NUMBER_NAMED_STRIPPERS_MP
	ELSE
		RETURN MAX_NUMBER_NAMED_STRIPPERS
	ENDIF

ENDFUNC


/*
PROC INIT_STRIP_CLUB_VIP_SEATS()
	
	
	//eVipSeats[STRIPCLUB_VIP_SEAT_01].iVIPSeatNum = ENUM_TO_INT(STRIPCLUB_VIP_SEAT_01)
	eVipSeats[STRIPCLUB_VIP_SEAT_01].vVIPSeatPos = <<114.29, -1299.73, 28.72 >>	
	eVipSeats[STRIPCLUB_VIP_SEAT_01].vVIPSeatDancerPos = <<114.91, -1300.73, 28.27>>
	eVipSeats[STRIPCLUB_VIP_SEAT_01].fVIPSeatDancerHeading = 30.0
	//eVipSeats[STRIPCLUB_VIP_SEAT_01].bVIPSeatInUse = FALSE

	
	//eVipSeats[STRIPCLUB_VIP_SEAT_02].iVIPSeatNum = ENUM_TO_INT(STRIPCLUB_VIP_SEAT_02)
	eVipSeats[STRIPCLUB_VIP_SEAT_02].vVIPSeatPos = << 116.90, -1303.81, 28.72 >>
	eVipSeats[STRIPCLUB_VIP_SEAT_02].vVIPSeatDancerPos = <<116.34, -1302.75, 28.27>>
	eVipSeats[STRIPCLUB_VIP_SEAT_02].fVIPSeatDancerHeading = -140.0
	//eVipSeats[STRIPCLUB_VIP_SEAT_02].bVIPSeatInUse = FALSE

	
	//eVipSeats[STRIPCLUB_VIP_SEAT_03].iVIPSeatNum = ENUM_TO_INT(STRIPCLUB_VIP_SEAT_03)
	eVipSeats[STRIPCLUB_VIP_SEAT_03].vVIPSeatPos = << 114.83, -1304.97, 28.72 >>	
	eVipSeats[STRIPCLUB_VIP_SEAT_03].vVIPSeatDancerPos = <<114.20, -1303.95, 28.27>>
	eVipSeats[STRIPCLUB_VIP_SEAT_03].fVIPSeatDancerHeading = -140.0
	//eVipSeats[STRIPCLUB_VIP_SEAT_03].bVIPSeatInUse = FALSE

	
	//eVipSeats[STRIPCLUB_VIP_SEAT_04].iVIPSeatNum = ENUM_TO_INT(STRIPCLUB_VIP_SEAT_04)
	eVipSeats[STRIPCLUB_VIP_SEAT_04].vVIPSeatPos = << 112.42, -1300.88, 28.72 >>
	eVipSeats[STRIPCLUB_VIP_SEAT_04].vVIPSeatDancerPos = << 112.97, -1301.67, 28.27 >>
	eVipSeats[STRIPCLUB_VIP_SEAT_04].fVIPSeatDancerHeading = 30.0
	//eVipSeats[STRIPCLUB_VIP_SEAT_04].bVIPSeatInUse = FALSE

	
	//eVipSeats[STRIPCLUB_VIP_SEAT_05].iVIPSeatNum = ENUM_TO_INT(STRIPCLUB_VIP_SEAT_05)
	eVipSeats[STRIPCLUB_VIP_SEAT_05].vVIPSeatPos = << 110.67, -1301.90, 28.72 >>
	eVipSeats[STRIPCLUB_VIP_SEAT_05].vVIPSeatDancerPos = <<111.24, -1302.79, 28.27 >>
	eVipSeats[STRIPCLUB_VIP_SEAT_05].fVIPSeatDancerHeading = 30.0
	//eVipSeats[STRIPCLUB_VIP_SEAT_05].bVIPSeatInUse = FALSE

	
	//eVipSeats[STRIPCLUB_VIP_SEAT_06].iVIPSeatNum = ENUM_TO_INT(STRIPCLUB_VIP_SEAT_06)
	eVipSeats[STRIPCLUB_VIP_SEAT_06].vVIPSeatPos = << 113.08, -1306.12, 28.72 >>
	eVipSeats[STRIPCLUB_VIP_SEAT_06].vVIPSeatDancerPos = <<112.33, -1304.91, 28.27>>
	eVipSeats[STRIPCLUB_VIP_SEAT_06].fVIPSeatDancerHeading = 30.0
	//eVipSeats[STRIPCLUB_VIP_SEAT_06].bVIPSeatInUse = FALSE
	
	INT iVipIndex	
	REPEAT NUM_VIP_SEATS iVipIndex		
		IF NOT ARE_VECTORS_EQUAL(eVipSeats[iVipIndex].vVIPSeatPos, <<0,0,0>>)							
			VECTOR vBlockingSize = <<STRIP_CLUB_SCENARIO_RANGE, STRIP_CLUB_SCENARIO_RANGE, STRIP_CLUB_SCENARIO_RANGE>>
			eVipSeatsBlocking[iVipIndex] = ADD_SCENARIO_BLOCKING_AREA(eVipSeats[iVipIndex].vVIPSeatPos-vBlockingSize, eVipSeats[iVipIndex].vVIPSeatPos+vBlockingSize)
		ENDIF	
	ENDREPEAT
ENDPROC
//*/
