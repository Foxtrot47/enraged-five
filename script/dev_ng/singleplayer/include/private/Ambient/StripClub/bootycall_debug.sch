///    Purpose - Setup Camera Scene debug for Luke H per B*
USING "stripclub_public.sch"

#IF IS_DEBUG_BUILD


FUNC BOOL CAN_BOOTY_CALL_BE_TAKEN_HOME(BOOTY_CALL_CONTACT_ENUM eThisBooty )
	
	IF eThisBooty = BC_STRIPPER_JULIET
	OR eThisBooty = BC_STRIPPER_NIKKI
	OR eThisBooty = BC_STRIPPER_SAPPHIRE
	OR eThisBooty = BC_STRIPPER_INFERNUS
	OR eThisBooty = BC_TAXI_LIZ
	OR eThisBooty = BC_HITCHER_GIRL
		
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Booty call valid = ", eThisBooty)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_HOUSE_NAME_FOR_OUTPUT()
	STRING sHouseName
	IF iWhichBCsHouse = 1
	
		sHouseName = "--------------------------- ||||| 1. Juliet |||| | ----------------------------"
	
	ELIF iWhichBCsHouse = 2
		sHouseName = "--------------------------- ||||| 2. Nikki  |||| | ----------------------------"
		
	ELIF iWhichBCsHouse = 3
		sHouseName = "--------------------------- ||||| 3. Sapphire |||| | ----------------------------"
		
	ELIF iWhichBCsHouse = 4
		sHouseName = "--------------------------- ||||| 4. Infernus |||| | ----------------------------"
		
	ELIF iWhichBCsHouse = 5
		sHouseName = "--------------------------- ||||| 5. Liz      |||| | ----------------------------"
		
	ELIF iWhichBCsHouse = 6
		sHouseName = "--------------------------- ||||| 6. Hitcher  |||| | ----------------------------"
		
	ELSE
		sHouseName = "------HOUSE INVALID-------"
	ENDIF
	
	RETURN sHouseName
ENDFUNC
PROC CREATE_BOOTY_CALL_FINAL_SCENE_MODE(WIDGET_GROUP_ID &bcWidgetGroup)
	
	bcWidgetGroup = START_WIDGET_GROUP("Booty Call Debug Controller")
		
		ADD_WIDGET_BOOL("TERMINATE Controller" , bTerminateScript )
		ADD_WIDGET_BOOL("DELETE Camera File" , bDeleteDebugOutputFile )
		
		//Drop down list to select which droppt to do
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("Select which house...")
			ADD_TO_WIDGET_COMBO("1. Juliet")
			ADD_TO_WIDGET_COMBO("2. Nikki")
			ADD_TO_WIDGET_COMBO("3. Sapphire")
			ADD_TO_WIDGET_COMBO("4. Infernus")
			ADD_TO_WIDGET_COMBO("5. Liz")
			ADD_TO_WIDGET_COMBO("6. Hitcher")
		STOP_WIDGET_COMBO("Ladies House", iWhichBCsHouse)
		
	STOP_WIDGET_GROUP()
	
	CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] BOOTY_CALL_FINAL_SCENE_MODE engaged = ",iWhichBCsHouse )
ENDPROC
PROC BootyCallDebug_HandleTermination()
	IF bTerminateScript
		g_bTurnOnLukeHCameraDebug = FALSE
	ENDIF
ENDPROC

PROC BootyCallDebug_CreateHouseSceneWidgets(WIDGET_GROUP_ID &bcWidgetGroup)
	
	IF NOT DOES_WIDGET_GROUP_EXIST(bcWidgetGroup)
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Widget dNE for CreateHouseSceneW" )
		EXIT
	ENDIF
	
	IF DOES_WIDGET_GROUP_EXIST(wg_BCD_Scene)
		DELETE_WIDGET_GROUP(wg_BCD_Scene)
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Destroyed Scene Widget" )
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(bcWidgetGroup)
		wg_BCD_Scene = START_WIDGET_GROUP("Scene")
		
			ADD_WIDGET_BOOL("Play Original Scene",bGoToHouse )
			
			
			wg_BCD_Camera = START_WIDGET_GROUP("Camera")
			
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("1. Alpha")
					ADD_TO_WIDGET_COMBO("2. Beta")
					ADD_TO_WIDGET_COMBO("3. Gamma")
					ADD_TO_WIDGET_COMBO("4. Delta")
				STOP_WIDGET_COMBO("Current Cam", iWhichBCCam)
				
				ADD_WIDGET_BOOL("Grab Debug Cam Info",			bGrabCameraInfo)
				ADD_WIDGET_BOOL("Load info into Debug Cam",		bLoadCameraInfo)
				ADD_WIDGET_BOOL("Clear Current Cam",			bClearCam)
			
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("Export")
			ADD_WIDGET_BOOL("Save Cams To Debug File For Diaz",	bSaveCameraToText)
			ADD_WIDGET_BOOL("Save on exit", bSaveOnExit)
		STOP_WIDGET_GROUP()	
		
		//ADD_WIDGET_BOOL("Hide Player", bHidePlayer)
		//ADD_WIDGET_BOOL("Hide Booty Call", bHideBootyCall)
	
	CLEAR_CURRENT_WIDGET_GROUP(bcWidgetGroup)

	
ENDPROC
FUNC INT GET_NUM_BC_CAMS_SET()
	iNumCamsSet = 0
	IF tBCCamStruct[0].bCamSet
		iNumCamsSet++
	ENDIF
	
	IF tBCCamStruct[1].bCamSet
		iNumCamsSet++
	ENDIF
	
	IF tBCCamStruct[2].bCamSet
		iNumCamsSet++
	ENDIF
	
	IF tBCCamStruct[3].bCamSet
		iNumCamsSet++
	ENDIF
	
	RETURN iNumCamsSet
ENDFUNC

PROC BootyCallDebug_CreatePlaybackWidgets(/*WIDGET_GROUP_ID &bcWidgetGroup*/)
	
	IF NOT DOES_WIDGET_GROUP_EXIST(wg_BCD_Scene)
	OR GET_NUM_BC_CAMS_SET() < 2
		EXIT
	ENDIF
	
	IF DOES_WIDGET_GROUP_EXIST(wg_BCD_Playback)
		EXIT
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(wg_BCD_Scene)
		wg_BCD_Playback = START_WIDGET_GROUP("Playback")
		
			START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("1 -> 2")
					ADD_TO_WIDGET_COMBO("2 -> 3")
					ADD_TO_WIDGET_COMBO("3 -> 4")
				STOP_WIDGET_COMBO("Use Seq", iWhichSeq)
			
			ADD_WIDGET_BOOL("Play Custom Scene",bPlayCustomScene )
		STOP_WIDGET_GROUP()	
		
		//ADD_WIDGET_BOOL("Hide Player", bHidePlayer)
		//ADD_WIDGET_BOOL("Hide Booty Call", bHideBootyCall)
	
	CLEAR_CURRENT_WIDGET_GROUP(wg_BCD_Scene)

	CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH]Created Playback widget")
	
ENDPROC

PROC BootyCallDebug_ClearCurrentCam()
	IF bClearCam
		tBCCamStruct[iWhichBCCam].vPos = <<0.0,0.0,0.0>>
		tBCCamStruct[iWhichBCCam].vRot = <<0.0,0.0,0.0>>
		tBCCamStruct[iWhichBCCam].fFOV = DEFAULT_DEBUG_CAM_FOV
		tBCCamStruct[iWhichBCCam].iPanTime = DEFAULT_DEBUG_CAM_PAN_TIME
		tBCCamStruct[iWhichBCCam].bCamSet = FALSE
		
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH]Cleared out current cam # ", iWhichBCCam)
		bClearCam = FALSE
	ENDIF
ENDPROC


PROC BootyCallDebug_HandleCamSelection()
	
	BootyCallDebug_ClearCurrentCam()
	
	IF iWhichBCCam != iPrevBCCam
		
		
		IF DOES_WIDGET_GROUP_EXIST(wg_BCD_CameraDetails)
			DELETE_WIDGET_GROUP(wg_BCD_CameraDetails)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH]Deleted Cam Details for cam# ", iPrevBCCam)
		ENDIF
	
		
		IF DOES_WIDGET_GROUP_EXIST(wg_BCD_Camera)
			SET_CURRENT_WIDGET_GROUP(wg_BCD_Camera)
				wg_BCD_CameraDetails = START_WIDGET_GROUP("Data")
					ADD_WIDGET_VECTOR_SLIDER("Pos",		tBCCamStruct[iWhichBCCam].vPos,		-2000.0,5200.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Rot",		tBCCamStruct[iWhichBCCam].vRot,		-180.0,	180.0,	0.1)
					ADD_WIDGET_FLOAT_SLIDER("FOV", 		tBCCamStruct[iWhichBCCam].fFOV,		1.0,	130.0,	0.1)
					ADD_WIDGET_FLOAT_SLIDER("Handshake",tBCCamStruct[iWhichBCCam].fHandShake, 0.0,	1.0,	0.1)
					ADD_WIDGET_INT_SLIDER("Pan Time", tBCCamStruct[iWhichBCCam].iPanTime,	1000, 	10000,	1000)
					
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(wg_BCD_Camera)
			
			iPrevBCCam = iWhichBCCam
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Added Camera Info Widget For Cam# ",iWhichBCCam )
		ENDIF

	ENDIF
	
ENDPROC
PROC WRITE_BOOTYCALL_CAM_TO_DEBUG_FILE(STRING sTitle,INT iCamNum, STRING sPathName, STRING sFileName)
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(sTitle,sPathName, sFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Pos: ",sPathName, sFileName)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(tBCCamStruct[iCamNum].vPos,sPathName, sFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Rot: ",sPathName, sFileName)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(tBCCamStruct[iCamNum].vRot,sPathName, sFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("FOV: ",sPathName, sFileName)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tBCCamStruct[iCamNum].fFOV,sPathName, sFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Pan Time: ",sPathName, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tBCCamStruct[iCamNum].iPanTime,sPathName, sFileName)
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
	
ENDPROC

PROC BootyCallDebug_WriteCamsToDebugFile()
	IF bDeleteDebugOutputFile
		CLEAR_NAMED_DEBUG_FILE("X:/gta5/script/dev/","ForDiaz_BootyCallCams.txt")
		bDeleteDebugOutputFile = FALSE
	ENDIF
	
	IF bSaveCameraToText
		STRING sPathName = "X:/gta5/script/dev/"
		STRING sFileName = "ForDiaz_BootyCallCams.txt"
		
		
		IF OPEN_NAMED_DEBUG_FILE(sPathName, sFileName)
			
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE("Booty Call Cameras Assembled by Luke / Forrest for Booty Call at: ",sPathName, sFileName)
		//	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_TIME_AS_STRING(GET_NETWORK_TIME_ACCURATE()),sPathName, sFileName)
			
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPathName, sFileName)
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_HOUSE_NAME_FOR_OUTPUT(),sPathName, sFileName)
			
			//Write each cam to the doc
			WRITE_BOOTYCALL_CAM_TO_DEBUG_FILE("Camera # 1: Alpha------------", 0,sPathName ,sFileName)
			WRITE_BOOTYCALL_CAM_TO_DEBUG_FILE("Camera # 2: Beta------------", 1,sPathName ,sFileName)
			WRITE_BOOTYCALL_CAM_TO_DEBUG_FILE("Camera # 3: Gamma------------", 2,sPathName ,sFileName)
			WRITE_BOOTYCALL_CAM_TO_DEBUG_FILE("Camera # 4: Delta------------", 3,sPathName ,sFileName)
			
			CLOSE_DEBUG_FILE()
			
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Data saved @ ..\ build\ dev\ ForDiaz_BootyCallCams.txt")
			bSaveCameraToText = FALSE
		ELSE
			SCRIPT_ASSERT("Couldn't write to X:\ gta5\ build\ dev\ ForDiaz_BootyCallCams.txt, please send a message to John Diaz")
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_DEBUG_CAM_TO_GAMEPLAY_CAM()
	
	IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
		
		SET_DEBUG_CAM_ACTIVE(TRUE,FALSE)
		
		SET_CAM_COORD(GET_DEBUG_CAM(),GET_GAMEPLAY_CAM_COORD()) 
		SET_CAM_ROT(GET_DEBUG_CAM(),GET_GAMEPLAY_CAM_ROT()) 
		SET_CAM_FOV(GET_DEBUG_CAM(),GET_GAMEPLAY_CAM_FOV()) 
		
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Activating Debug Cam")
			
	ENDIF
	
ENDPROC


PROC BootyCallDebug_GrabDebugCamInfo()
	IF bGrabCameraInfo
		SET_DEBUG_CAM_TO_GAMEPLAY_CAM()
		
		IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
			SCRIPT_ASSERT("Debug Cam Not Rendering")
			bGrabCameraInfo = FALSE
		ENDIF
		
		tBCCamStruct[iWhichBCCam].vPos = GET_CAM_COORD(GET_DEBUG_CAM())
		tBCCamStruct[iWhichBCCam].vRot = GET_CAM_ROT(GET_DEBUG_CAM())
		tBCCamStruct[iWhichBCCam].fFOV	= GET_CAM_FOV(GET_DEBUG_CAM())
		tBCCamStruct[iWhichBCCam].bCamSet = TRUE
		
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Debug Cam Info Successfully Grabbed :) ",iWhichBCCam )
		bGrabCameraInfo = FALSE
	ENDIF
	
	BootyCallDebug_CreatePlaybackWidgets()
	
ENDPROC

PROC BootyCallDebug_SetDebugCamInfo()
	IF bLoadCameraInfo
		
		SET_DEBUG_CAM_ACTIVE(TRUE,FALSE)
		
		IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
			SCRIPT_ASSERT("Debug Cam Not Rendering")
			bLoadCameraInfo = FALSE
		ENDIF
		
		SET_CAM_COORD(GET_DEBUG_CAM(),tBCCamStruct[iWhichBCCam].vPos) 
		SET_CAM_ROT(GET_DEBUG_CAM(),tBCCamStruct[iWhichBCCam].vRot) 
		SET_CAM_FOV(GET_DEBUG_CAM(),tBCCamStruct[iWhichBCCam].fFOV) 

		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Debug Cam Info Successfully Input  :) ",iWhichBCCam )
		bLoadCameraInfo = FALSE
	ENDIF
	
	
ENDPROC



PROC BootyCallDebug_HandleHomeSelection()

	//Create Widgets once house is selected
	IF iWhichBCsHouse != iPrevBCHouse
		BootyCallDebug_CreateHouseSceneWidgets(wg_BCD_Main)
		iPrevBCHouse = iWhichBCsHouse
	ENDIF
		
	IF bGoToHouse
		SWITCH iWhichBCsHouse
		
			CASE 0	//N/A
				bGoToHouse = FALSE
				bLoadScript = FALSE
			BREAK
			
			
			CASE 1		//Juliet
				
				START_PLAYER_TELEPORT(PLAYER_ID(), <<-172.4806, -1634.7402, 32.4393>>,269.1531)
				NEW_LOAD_SCENE_START_SPHERE(<<-172.4806, -1634.7402, 32.4393>>,20.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-0.1523)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.6645)
				
				
				bGoToHouse = FALSE
				bLoadScript = TRUE
				
				IF eBootyEnum != BC_STRIPPER_JULIET
					eBootyEnum = BC_STRIPPER_JULIET
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
				ENDIF
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Warping to Juliet's House" )

			BREAK
			
			
			CASE 2		//Nikki
				START_PLAYER_TELEPORT(PLAYER_ID(), <<134.3009, -1891.3752, 22.5610>>,130.7317)
				NEW_LOAD_SCENE_START_SPHERE(<<136.0032, -1889.0509, 22.3520>>,20.0)
				
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(4.12)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.3)
				
				bGoToHouse = FALSE
				bLoadScript = TRUE
				
				IF eBootyEnum != BC_STRIPPER_NIKKI
					eBootyEnum = BC_STRIPPER_NIKKI
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
				ENDIF	
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Warping to Nikki's House" )

				
			BREAK
			
			CASE 3		//Sapphire
				START_PLAYER_TELEPORT(PLAYER_ID(), <<-206.6022, 104.5715, 68.6460>>,212.3)
				NEW_LOAD_SCENE_START_SPHERE(<<-206.6022, 104.5715, 68.6460>>,20.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-4.72)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6.134)
				
				bGoToHouse = FALSE
				bLoadScript = TRUE
				
				IF eBootyEnum != BC_STRIPPER_SAPPHIRE
					eBootyEnum = BC_STRIPPER_SAPPHIRE
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
				ENDIF	
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Warping to Sapphire's House" )

				
			BREAK
			
			CASE 4		//Infernus
				START_PLAYER_TELEPORT(PLAYER_ID(), <<-861.6264, 516.2764, 88.1727>>,271.38)
				NEW_LOAD_SCENE_START_SPHERE( <<-861.6264, 516.2764, 88.1727>>,20.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-11.434)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(3.9691)
				
				bGoToHouse = FALSE
				bLoadScript = TRUE
				
				IF eBootyEnum != BC_STRIPPER_INFERNUS
					eBootyEnum = BC_STRIPPER_INFERNUS
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
				ENDIF
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Warping to Infernus's House" )
			BREAK
			
			CASE 5		//Liz
				START_PLAYER_TELEPORT(PLAYER_ID(), <<-27.7950, -1582.1865, 28.2296>>,340.0378)
				NEW_LOAD_SCENE_START_SPHERE(<<-27.7950, -1582.1865, 28.2296>>,20.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(18.3350)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4.2863)
		
				bGoToHouse = FALSE
				bLoadScript = TRUE
				
			
				IF eBootyEnum != BC_TAXI_LIZ
					eBootyEnum = BC_TAXI_LIZ
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
				ENDIF
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Warping to Liz's House" )
				
			BREAK
			
			CASE 6		//Hitcher
				START_PLAYER_TELEPORT(PLAYER_ID(), <<3325.7290, 5152.3330, 17.3037>>,127.0454)
				NEW_LOAD_SCENE_START_SPHERE(<<3325.7290, 5152.3330, 17.3037>>,20.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-18.9183)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(6.470)
		
				bGoToHouse = FALSE
				bLoadScript = TRUE
				
				IF eBootyEnum != BC_HITCHER_GIRL
					eBootyEnum = BC_HITCHER_GIRL
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
				ENDIF
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Warping to Hitcher's House" )
			BREAK	
			
		ENDSWITCH		
	ENDIF
ENDPROC

PROC BootyCallDebug_SimBootyCallDropoff()
	//Load the script
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("stripperhome")) = 0
	
		IF iWhichBCsHouse > 0
		AND bLoadScript	
			REQUEST_SCRIPT("stripperhome")
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Waiting for StripperHome script to load" )
			
			IF HAS_SCRIPT_LOADED("stripperhome")
				
				TAKE_HOME_STRIPPER_INFO takeHomeInfo
				takeHomeInfo.stripperID[0] = eBootyEnum
				takeHomeInfo.iNumStrippers = 1
				takeHomeInfo.myCandidateID = NO_CANDIDATE_ID
				takeHomeInfo.bIsBootyCall = TRUE
				takeHomeInfo.bIsMp = FALSE
				
				g_bootyCallData.iStripperCalling = ENUM_TO_INT(eBootyEnum)
				
				START_NEW_SCRIPT_WITH_ARGS("stripperhome", takeHomeInfo, SIZE_OF(TAKE_HOME_STRIPPER_INFO), DEFAULT_STACK_SIZE)
				SET_NEXT_STRIPPER_BOOTY_CALL_AVAILABLE_TIME(g_bootyCallData.iStripperCalling)
				
				g_bootyCallData.bootyCallState = BOOTY_CALL_SCRIPT_RUNNING
				
				bLoadScript = FALSE
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] StripperHome script should be running" )
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC CREATE_DEBUG_BOOTY_CAMS()
	
	INT iCamIter
	REPEAT MAX_NUM_BOOTY_CALL_DEBUG_CAMS iCamIter
		IF tBCCamStruct[iCamIter].bCamSet
			tBCCamStruct[iCamIter].iD = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,tBCCamStruct[iCamIter].vPos,tBCCamStruct[iCamIter].vRot,tBCCamStruct[iCamIter].fFOV,FALSE)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Created Debug BC Cam # ", iCamIter)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL CREATE_CAMERA_SEQUENCE(INT iInitialCam, INT iDestinationCam)
	
	IF (NOT tBCCamStruct[iInitialCam].bCamSet OR NOT tBCCamStruct[iDestinationCam].bCamSet)
	OR (NOT DOES_CAM_EXIST(tBCCamStruct[iInitialCam].iD) OR NOT DOES_CAM_EXIST(tBCCamStruct[iDestinationCam].iD) )
		RETURN FALSE
		SCRIPT_ASSERT("Camera Sequence Invalid")
	ENDIF

	SET_DEBUG_CAM_ACTIVE(FALSE,TRUE)
	SET_CAM_ACTIVE_WITH_INTERP(tBCCamStruct[iDestinationCam].iD,tBCCamStruct[iInitialCam].iD, tBCCamStruct[iInitialCam].iPanTime)			
	
	SHAKE_CAM(tBCCamStruct[iInitialCam].iD,"HAND_SHAKE",tBCCamStruct[iInitialCam].fHandShake)
	SHAKE_CAM(tBCCamStruct[iDestinationCam].iD,"HAND_SHAKE",tBCCamStruct[iDestinationCam].fHandShake)
	
	RENDER_SCRIPT_CAMS(TRUE,FALSE)
	
	CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Setup Sequence Between Cam#",iInitialCam, "and ",  iDestinationCam)
	RETURN TRUE
ENDFUNC


PROC BootyCallDebugController_PlayCustomScene()
	//Everthing is loaded in
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
	AND bPlayCustomScene
		
		CREATE_DEBUG_BOOTY_CAMS()
		
		IF iWhichSeq = 0
			CREATE_CAMERA_SEQUENCE(0,1)
		ELIF iWhichSeq = 1
			CREATE_CAMERA_SEQUENCE(1,2)
		ELIF iWhichSeq = 2
			CREATE_CAMERA_SEQUENCE(2,3)
		ENDIF
		
		
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Custom Scene should be playing" )
		bPlayCustomScene = FALSE
		bSceneRendering = TRUE
		
		RESTART_TIMER_NOW(timerSceneTime)

		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
ENDPROC
PROC DESTROY_BC_CAMS_SAFE()
	
	RENDER_SCRIPT_CAMS(FALSE,FALSE)
	
	IF DOES_CAM_EXIST(tBCCamStruct[0].iD)
		DESTROY_CAM(tBCCamStruct[0].iD)
	ENDIF
	
	IF DOES_CAM_EXIST(tBCCamStruct[1].iD)
		DESTROY_CAM(tBCCamStruct[1].iD)
	ENDIF
	
	IF DOES_CAM_EXIST(tBCCamStruct[2].iD)
		DESTROY_CAM(tBCCamStruct[2].iD)
	ENDIF
	
	IF DOES_CAM_EXIST(tBCCamStruct[3].iD)
		DESTROY_CAM(tBCCamStruct[3].iD)
	ENDIF
	
ENDPROC

FUNC INT GET_RENDERING_SEQUENCE_PAN_TIME_IN_SECONDS()
	INT iTotalPanTimeInSeconds = 1000
	
	IF iWhichSeq = 0
		iTotalPanTimeInSeconds = (tBCCamStruct[0].iPanTime + tBCCamStruct[1].iPanTime) / 1000
		
	ELIF iWhichSeq = 1
		iTotalPanTimeInSeconds = (tBCCamStruct[1].iPanTime + tBCCamStruct[2].iPanTime) / 1000
	
	ELIF iWhichSeq = 2
		iTotalPanTimeInSeconds = (tBCCamStruct[2].iPanTime + tBCCamStruct[3].iPanTime) / 1000
	
	ENDIF
	
	RETURN iTotalPanTimeInSeconds
	
ENDFUNC

PROC DRAW_RENDERING_CAM_INFO(STRING sName, INT iWhichCam)
	TEXT_LABEL_31 sDebugInfo
				
	IF DOES_CAM_EXIST(tBCCamStruct[iWhichCam].iD)
	
		IF IS_CAM_RENDERING(tBCCamStruct[iWhichCam].iD)
			
			//Display Title
			DRAW_DEBUG_TEXT_2D(sName, <<0.1,0.14,0.0>>,0,0,0,150)
			
			//Display Pan Time---------------------------------------------
			sDebugInfo = "Pan Time = "
			sDebugInfo+= tBCCamStruct[iWhichCam].iPanTime/1000
			sDebugInfo+=" seconds "
			DRAW_DEBUG_TEXT_2D(sDebugInfo, <<0.1,0.16,0.0>>,0,0,0,150)
			
			//Display Interp------------------------------------------------
			IF IS_CAM_INTERPOLATING(tBCCamStruct[iWhichCam].iD)
				DRAW_DEBUG_TEXT_2D("Cam Is Interpolating", <<0.1,0.18,0.0>>,0,0,0,150)
			ENDIF
			
			//Display Shake -------------------------------------------------
			IF IS_CAM_SHAKING(tBCCamStruct[iWhichCam].iD)
				sDebugInfo = "HANDSHAKE = "
				sDebugInfo+= ROUND(tBCCamStruct[iWhichCam].fHandShake * 100)
				sDebugInfo+="%"
				DRAW_DEBUG_TEXT_2D(sDebugInfo, <<0.1,0.2,0.0>>,0,0,0,150)
			ENDIF
			
		ENDIF
		
	ENDIF
			
ENDPROC


PROC BootyCallDebugController_UpdateRender()
	//Everthing is loaded in
	IF bSceneRendering
		DRAW_RENDERING_CAM_INFO("Cam 1 - Alpha ON",0)
		DRAW_RENDERING_CAM_INFO("Cam 2 - Beta ON",1)
		DRAW_RENDERING_CAM_INFO("Cam 3 - Gamma ON",2)
		DRAW_RENDERING_CAM_INFO("Cam 4 - Delta ON",3)
		
		//Show scene time
		TEXT_LABEL_31 sDebugTimer
		sDebugTimer = "Scene Time: "
		sDebugTimer+= ROUND(GET_TIMER_IN_SECONDS_SAFE(timerSceneTime))
		DRAW_DEBUG_TEXT_2D(sDebugTimer, <<0.1,0.22,0.0>>,0,0,0,150)
		
		IF GET_TIMER_IN_SECONDS_SAFE(timerSceneTime) > GET_RENDERING_SEQUENCE_PAN_TIME_IN_SECONDS()
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Custom Scene is done, total pan time = ", GET_RENDERING_SEQUENCE_PAN_TIME_IN_SECONDS())
			
			bSceneRendering = FALSE
			
			DESTROY_BC_CAMS_SAFE()
		ENDIF
		
	
	ENDIF
ENDPROC

PROC BootyCallDebug_HandlePlaybackTab()
	
	//Set Use Cam #
	INT iCamIter
	REPEAT MAX_NUM_BOOTY_CALL_DEBUG_CAMS iCamIter
		IF tBCCamStruct[iCamIter].bCamSet
			IF ARE_VECTORS_EQUAL(tBCCamStruct[iCamIter].vPos , <<0.0,0.0,0.0>>)
				tBCCamStruct[iCamIter].bCamSet = FALSE
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Toggling bSet OFF on CAM#",iCamIter )
			ENDIF
		ENDIF
	ENDREPEAT
	

ENDPROC

PROC BootyCallDebugController_UpdateWidgets()
	
	BootyCallDebug_HandleTermination()
	
	BootyCallDebug_SimBootyCallDropoff()
	
	BootyCallDebug_HandleHomeSelection()
	
	BootyCallDebug_HandleCamSelection()
	
	BootyCallDebug_HandlePlaybackTab()
	
	BootyCallDebug_GrabDebugCamInfo()
	
	BootyCallDebug_SetDebugCamInfo()
	
	BootyCallDebug_WriteCamsToDebugFile()
	
	BootyCallDebugController_PlayCustomScene()
	
	BootyCallDebugController_UpdateRender()
ENDPROC
#ENDIF

//EOF
