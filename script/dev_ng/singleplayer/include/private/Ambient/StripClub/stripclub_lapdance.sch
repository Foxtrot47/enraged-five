USING "stripclub_hotspots.sch"

FUNC FLOAT GET_END_ANIM_TIME_FOR_SEAT(INT iSeat)
	FLOAT iLeaveTime = 1.0
	
	#IF MP_STRIPCLUB
		iLeaveTime = 0.95
	#ENDIF
	
	IF iSeat = 6 OR iSeat = 0 OR iSeat = 3 OR iSeat = 4
		RETURN 0.675
	ELSE
		RETURN iLeaveTime
	ENDIF

	RETURN iLeaveTime
ENDFUNC

FUNC BOOL IS_ANY_STRIPCLUB_LIKE_HELP_MESSAGE_DISPLAYED()

	IF IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_LIKE_SP1")
	OR IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_LIKE_SP2")
	OR IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_LIK2_SP1")
	OR IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_LIK2_SP2")
	OR IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_LIKE_MP")
	OR IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_LIK2_MP")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CLEAR_LAPDANCE_HELP()
	CLEAR_STRIPCLUB_HELP("SCLUB_MORE_HELP")
	CLEAR_STRIPCLUB_HELP("SCLUB_MORE_HEL2")
	CLEAR_STRIPCLUB_HELP_WITH_STRIPPER_NAME("SCLUB_MORE_HEL3")
	CLEAR_STRIPCLUB_HELP_WITH_STRIPPER_NAME("SCLUB_MORE_HEL4")
	CLEAR_STRIPCLUB_HELP("SCLUB_MOR2_HELP")
	CLEAR_STRIPCLUB_HELP_WITH_STRIPPER_NAME("SCLUB_MOR2_HEL2")
	CLEAR_STRIPCLUB_HELP("SCLUB_OFFR_HELP")
	
	CLEAR_STRIPCLUB_HELP("SCLUB_LIKE_SP1")
	CLEAR_STRIPCLUB_HELP("SCLUB_LIKE_SP2")
	CLEAR_STRIPCLUB_HELP("SCLUB_LIK2_SP1")
	CLEAR_STRIPCLUB_HELP("SCLUB_LIK2_SP2")
	CLEAR_STRIPCLUB_HELP("SCLUB_LIKE_MP")
	CLEAR_STRIPCLUB_HELP("SCLUB_LIK2_MP")
ENDPROC

FUNC BOOL HAVE_ALL_STRIP_CLUB_LAPDANCE_ANIMS_LOADED()

	//just in case
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, iSecondaryStripper >= 0))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0))
	
	IF  HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, iSecondaryStripper >= 0))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0))
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL STRIPCLUB_SHOULD_STOP_DRAWING_PLAYERS_HEAD()

	IF (danceStage = DANCE_INIT        OR danceStage = DANCE_SET_PLAYER OR danceStage = DANCE_SET_STRIPPER OR
	    danceStage = DANCE_IN_PROGRESS OR danceStage = DANCE_FINISHED   OR danceStage = DANCE_OFFER_ANOTHER OR
	    danceStage = DANCE_OFFERS      OR danceStage = DANCE_OFFER_HOME OR danceStage = DANCE_CAUGHT_TOUCHING)
		AND currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
			RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CAM_END_OF_DANCE_ROT()

	IF DOES_CAM_EXIST(fpStripCam.theCam)
		DETACH_CAM(fpStripCam.theCam)
		
		fpStripCam.vInitCamPos = vLapdanceCamPos + <<-0.1052, 0.0107, 0.0660>>
		fpStripCam.vInitCamRot.x = 15.0
		fpStripCam.vInitCamRot.z  = GET_HEADING_BETWEEN_VECTORS_2D(GET_CAM_COORD(fpStripCam.theCam), GET_ENTITY_COORDS(namedStripper[iCurrentStripperForDance].ped))
	ENDIF

ENDPROC

PROC HANDLE_CAMERA_INPUT(BOOL bFirstPersonLookAtStripper = FALSE)
	// handle looking around in 1st person
	//VECTOR vChairOffset, vSetPos, vSetRot
	
//	PRINTLN("Current seat ", iCurrentLapDanceSeat)
	
	// If select is pressed when the player is not touching the stripping
	IF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT)
	AND TouchingStripperState = TSS_NONE

		IF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
		
			IF iSecondaryStripper >=0
			AND (iCurrentLapDanceSeat = 0) 
				//face player cam doesn't work on seat with two dancers
				currentLapdanceCamera = STRCLUB_CAM_DANCE_ANGLED
			ELSE
				IF iCurrentLapDanceSeat = 0
					currentLapdanceCamera = STRCLUB_CAM_DANCE_ANGLED
				ELSE
					currentLapdanceCamera = STRCLUB_CAM_DANCE_BEHIND_PLAYER
				ENDIF
			ENDIF
			
		ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_BEHIND_PLAYER
			
			currentLapdanceCamera = STRCLUB_CAM_DANCE_ANGLED
			
		ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_ANGLED
			
			IF iSecondaryStripper >=0
			AND (iCurrentLapDanceSeat = 3 OR iCurrentLapDanceSeat = 2 OR iCurrentLapDanceSeat = 0) 
				//face player cam doesn't work on seat with two dancers
				currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
			ELSE
				currentLapdanceCamera = STRCLUB_CAM_DANCE_FACE_PLAYER
			ENDIF
			
		ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_FACE_PLAYER
		
			currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
		
		ELSE
			PRINTLN("Invlaid stripclub lapdance camera ", currentLapdanceCamera)
			SCRIPT_ASSERT("Invlaid stripclub lapdance camera")
		ENDIF
		
		IF GET_ASPECT_RATIO(TRUE) > 2.5 AND currentLapdanceCamera = STRCLUB_CAM_DANCE_BEHIND_PLAYER
			//no angled cam with multiple monitors
			currentLapdanceCamera = STRCLUB_CAM_DANCE_FACE_PLAYER
		ENDIF
		
		IF iSecondaryStripper < 0 AND IS_STRIPCLUB_PLAYER_IN_DRESS()
		AND (currentLapdanceCamera = STRCLUB_CAM_DANCE_ANGLED OR currentLapdanceCamera = STRCLUB_CAM_DANCE_BEHIND_PLAYER)
			//Players skirts can clip into dancer with the angled camera. Don't allow that cam when playing as a female
			currentLapdanceCamera = STRCLUB_CAM_DANCE_FACE_PLAYER
		ENDIF
		

		SETUP_STRIPCLUB_CAM(stripclubCam, currentLapdanceCamera, <<0,0,0>>, <<0,0,0>>, TRUE)
		IF bFirstPersonLookAtStripper
			SET_CAM_END_OF_DANCE_ROT()
		ENDIF

	ENDIF
	
	IF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
		//CPRINTLN(DEBUG_SCLUB, "HANDLE_CAMERA_INPUT: Known to be fps camera angle")
		IF NOT DOES_CAM_EXIST(fpStripCam.theCam)
			SETUP_STRIPCLUB_CAM(stripclubCam, currentLapdanceCamera, <<0,0,0>>, <<0,0,0>>, TRUE)
		ENDIF
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			//CPRINTLN(DEBUG_SCLUB, "HANDLE_CAMERA_INPUT: Updating first person camera")
			UPDATE_FIRST_PERSON_CAMERA(fpStripCam, FALSE)
		ENDIF

	ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_BEHIND_PLAYER
//		#IF IS_DEBUG_BUILD
//			#IF USE_STRIPCLUB_DEBUG
//				VECTOR vChairOffset, vSetPos, vSetRot
//				vChairOffset = vStripDebugVector
//				vStripDebugVector = vStripDebugVector
//			
//				vSetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_VIP_SEAT_POS(iCurrentLapDanceSeat), GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat), vChairOffset)
//				vSetRot = << fDebugFloat , 0, fDebugHeading+GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat)>> 
//				
//				SET_CAM_COORD(stripclubCam, vSetPos)
//				SET_CAM_ROT(stripclubCam, vSetRot)
//			#ENDIF
//		#ENDIF

//		IF NOT IS_PAUSE_MENU_ACTIVE()
//			UPDATE_FIRST_PERSON_CAMERA(fpStripCam2, FALSE)
//		ENDIF

	ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_FACE_PLAYER
//		IF NOT IS_PAUSE_MENU_ACTIVE()
//			UPDATE_FIRST_PERSON_CAMERA(fpStripCam2, FALSE)
//		ENDIF
	ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_ANGLED
//		IF NOT IS_PAUSE_MENU_ACTIVE()
//			UPDATE_FIRST_PERSON_CAMERA(fpStripCam2, FALSE)
//		ENDIF	
	ENDIF	
	
ENDPROC

PROC SETUP_TOUCHING_LAPDANCE_SCENE()
	VECTOR vSeatPos
	FLOAT fHeading

	vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
							
	STRIPCLUB_CREATE_SYNC_SCENE(iLapDanceScene, vSeatPos, <<0,0, fHeading>>, TRUE, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, iSecondaryStripper >= 0), GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(iCurrentStripperLDVariation, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, iSecondaryStripper >= 0), GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(iCurrentStripperLDVariation, FALSE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	IF iSecondaryStripper >= 0
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, iSecondaryStripper >= 0), GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(iCurrentStripperLDVariation, FALSE, iSecondaryStripper >= 0, TRUE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	ENDIF
ENDPROC

PROC PLAY_BOUNCER_WARNING()
	IF iSecondaryStripper < 0
		CREATE_CONVERSATION(stripClubConversation, "SCAUD", "SC_NOTOUCH", GET_STRIPCLUB_SPEECH_PRIORITY())
	ELSE
		CREATE_CONVERSATION(stripClubConversation, "SCAUD", "SC_NOTOUCH2", GET_STRIPCLUB_SPEECH_PRIORITY())	
	ENDIF
ENDPROC

PROC PLAY_PLAYER_IN_SEAT_ANIM(INT iCurrentDance, FLOAT fBlendIn, FLOAT fBlendOut, FLOAT fStartingPhase)

	TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_PLAYER_LAPDANCE_IDLE_DICT(iCurrentDance, iSecondaryStripper >= 0), GET_PLAYER_LAPDANCE_IDLE_ANIM(iCurrentDance, iSecondaryStripper >= 0), fBlendIn, fBlendOut, -1, AF_TURN_OFF_COLLISION | AF_LOOPING | AF_NOT_INTERRUPTABLE, fStartingPhase, FALSE, AIK_DISABLE_LEG_IK)

ENDPROC

FUNC BOOL HANDLE_DANCE_INPUT()
// dance input
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
//	IF STRIPCLUB_IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
//	AND NOT IS_STRIPCLUB_MP()
//		USE_BOUNCER_CAM()
//		RETURN FALSE
//	ELIF DOES_CAM_EXIST(stripclubCamBouncer)
//		IF IS_CAM_ACTIVE(stripclubCamBouncer)
//			SET_CAM_ACTIVE(stripclubCamBouncer, FALSE)
//			DESTROY_CAM(stripclubCamBouncer)
//		ENDIF
//	ENDIF

	IF  NOT IS_STRIPCLUB_MP()
		IF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
		AND GET_GAME_TIMER() >= iNextDanceInputTime
			//flirt

			IF (GET_TIMER_IN_SECONDS(tDanceDialogue) > TIME_BETWEEN_DANCE_DIALOGUE)
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_RESPONSE_TALK)	
					SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_RESPONSE_TALK)				
					RESTART_TIMER_NOW(tDanceDialogue)
				ENDIF

				ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_DANCE, TRUE)
				ADVANCE_NEXT_DANCE_INPUT_TIME()		
			ENDIF
		ENDIF
		
	ELSE
		IF IS_PLAYER_FLIRTING_WITH_STRIPPER_THROUGH_HEADSET()
//			CPRINTLN(DEBUG_SCLUB, "Current talk ", fTalkHudValue, " adding ", GET_FRAME_TIME())
			fTalkHudValue += GET_FRAME_TIME()
			
			IF fTalkHudValue > MAX_LAPDANCE_TALK_BAR
				fTalkHudValue = 0
				ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_MP_FLIRT, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("Dance talk time ", GET_TIMER_IN_SECONDS(tDanceDialogue))
	IF (GET_TIMER_IN_SECONDS(tDanceDialogue) > TIME_BETWEEN_TOUCH_DIALOGUE + 5)	//when not touching, she talks less often							
		SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_DNC_TALK)
		RESTART_TIMER_NOW(tDanceDialogue)
	ENDIF
	
	IF ((STRIPCLUB_IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT) 
	OR TouchingStripperState = TSS_REACHING OR TouchingStripperState = TSS_REACHING_MP) //once you start reaching there is no gonig back until you touch the stripper
	AND NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_FORCE_LAPDANCE_RETRACT))
 	AND( TouchingStripperState != TSS_RETRACT AND TouchingStripperState != TSS_RETRACTING)
		PRINTLN("Touch button pressed")
		
		SWITCH TouchingStripperState
			CASE TSS_NONE
				
				IF IS_STRIPCLUB_MP()
					IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
						PRINTLN("Playing lap dance scene")
						fLapDancePhase = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
					ELSE
						fLapDancePhase = 0.0
					ENDIF
					
					PRINTLN("Dance phase ", fLapDancePhase)
					//pre build touching scene in mp
					SETUP_TOUCHING_LAPDANCE_SCENE()
					STRIPCLUB_START_SYNC_SCENE(iLapDanceScene)
					TouchingStripperState = TSS_REACHING_MP
				ELSE
					//TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_REACH, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION | AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0, FALSE, AIK_DISABLE_LEG_IK)
					CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: State change to TSS_REACHING")
					TouchingStripperState = TSS_REACHING
				ENDIF
			BREAK
			CASE TSS_REACHING_MP
				PRINTLN("TSS_REACHING_MP")
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLapDanceScene)
					PRINTLN("Touch dance is ready set stats")
					SET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene), fLapDancePhase)
				ENDIF
				
				previousLapdanceCamera = currentLapdanceCamera
				CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: Setting camera to FIRST PERSON")
				currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON 
				TouchingStripperState = TSS_TOUCHING
			BREAK
			CASE TSS_REACHING
				previousLapdanceCamera = currentLapdanceCamera
				CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: Setting camera to FIRST PERSON")
				currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON 
				//PRINTLN("TSS_REACHING")
				//Are you done reaching?
			//	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_REACH, TRUE, iSecondaryStripper >= 0, FALSE))
		//			IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_REACH, TRUE, iSecondaryStripper >= 0, FALSE)) >= 0.5
						//PRINTLN("Touching stripper")
						ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_TOUCH, TRUE)
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
							fLapDancePhase = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
						ELSE
							fLapDancePhase = 0.0
						ENDIF
						
						//Touching stripper, play paired anim
						SETUP_TOUCHING_LAPDANCE_SCENE()
						STRIPCLUB_START_SYNC_SCENE(iLapDanceScene)
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLapDanceScene)
							SET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene), fLapDancePhase)
						ENDIF
						TouchingStripperState = TSS_TOUCHING
			//		ENDIF
			//	ENDIF
			BREAK
			CASE TSS_TOUCHING
				//PRINTLN("TSS_TOUCHING")
				ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_TOUCH, TRUE, TRUE, TRUE)
				
				// Chaught by bouncer
				IF IS_VIP_BOUNCER_PRESENT_FOR_LAPDANCE() AND GET_GAME_TIMER() > iNextCaughtTouchingTime
				AND NOT IS_STRIPCLUB_MP() AND NOT DOES_TREVOR_OWN_CLUB()
				AND NOT CAN_DEBUG_ALLOW_STRIPPER_TOUCH()
					IF CAN_DO_STRIP_CLUB_SPEECH()
						CLEAR_ALL_STRIP_CLUB_HELP()
						PRINT_STRIPCLUB_HELP("SCLUB_BSAW_HELP")
						iNextCaughtTouchingTime = GET_GAME_TIMER() + 10000
						PRINTLN("Setting bouncer to yell")
						enumBouncerPatrolState = BOUNCER_PATROL_YELL
						IF iBouncerWarnedTimes = 2
							PRINTLN("Eject for touching")
							PLAY_PED_AMBIENT_SPEECH(clubStaffPed[STAFF_BOUNCER_PATROL], "BOUNCER_EJECT_TOUCH", SPEECH_PARAMS_FORCE_NORMAL)
							//POINT_CAM_AT_ENTITY(stripclubCam, clubStaffPed[STAFF_BOUNCER_PATROL], <<0, 0, 1.7>>)
							//USE_BOUNCER_CAM()
							SET_BITMASK_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_FORCE_LAPDANCE_RETRACT)
							CLEAR_ALL_STRIP_CLUB_HELP()
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCLUB_BSAW_HELP")
								PRINTLN("HANDLE_DANCE_INPUT: Clearing help because bouncer is warning")
								CLEAR_ALL_STRIP_CLUB_HELP()
							ENDIF
							danceStage = DANCE_CAUGHT_TOUCHING
							RETURN TRUE	
						ELSE
							CLEAR_ALL_STRIP_CLUB_HELP()
							PRINT_STRIPCLUB_HELP("SCLUB_BSAW_HELP")
							iBouncerWarnedTimes++
							SET_INSIDE_CLUB_WORKERS_VOICE()
							PLAY_BOUNCER_WARNING()
						ENDIF

					ENDIF
				ELSE
					//PRINTLN("Time until next stripper speach ", TIME_BETWEEN_TOUCH_DIALOGUE - GET_TIMER_IN_SECONDS(tDanceDialogue))
					IF (GET_TIMER_IN_SECONDS(tDanceDialogue) > TIME_BETWEEN_TOUCH_DIALOGUE)
						SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_DNC_TALK)
						RESTART_TIMER_NOW(tDanceDialogue)
					ENDIF
				ENDIF
			BREAK
			
			CASE TSS_RETRACT
				CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: TSS_RETRACT")
				//got chaught by bouncer, retract hands even though the player is holding the touch button
				//TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_RETRACT, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION | AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.5, FALSE, AIK_DISABLE_LEG_IK)
				//TouchingStripperState = TSS_RETRACTING
			BREAK
			
			CASE TSS_RETRACTING
				CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: TSS_RETRACTING")
				//PRINTLN("TSS_RETRACT")
				//Player is retracting don't allow him to put his hands back until retracting animation is over
//				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_RETRACT, TRUE, iSecondaryStripper >= 0, FALSE))
//					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_RETRACT, TRUE, iSecondaryStripper >= 0, FALSE)) >= 1.0
//						//go back to idle anim when  done retracting
//						PLAY_PLAYER_IN_SEAT_ANIM(iCurrentStripperLDVariation, SLOW_BLEND_IN, SLOW_BLEND_OUT, 0)
//						TouchingStripperState = TSS_NONE
//					ENDIF
//				ELSE
					//PLAY_PLAYER_IN_SEAT_ANIM(iCurrentStripperLDVariation, SLOW_BLEND_IN, SLOW_BLEND_OUT, 0)
					//TouchingStripperState = TSS_NONE
//				ENDIF
				
			BREAK
		ENDSWITCH
	ELSE
	
		CLEAR_BITMASK_AS_ENUM(eNetworkSyncFlags, SYNC_PRE_BUILD_TOUCHING)	
		IF TouchingStripperState = TSS_TOUCHING
			//player isn't holding touch button but was touchig stripper, retract hands
			CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: Task retracting")
			PRINTLN("Task retracting")
			TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_RETRACT, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION | AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.5, FALSE, AIK_DISABLE_LEG_IK)
			CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: State change to TSS_RETRACTING")
			TouchingStripperState = TSS_RETRACTING
			
		ELIF TouchingStripperState = TSS_RETRACTING
		//	PRINTLN("Retracting after letting go")
			//go back to idle anim when  done retracting
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_RETRACT, TRUE, iSecondaryStripper >= 0, FALSE))
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),GET_STRIP_CLUB_ANIM_DICT_REACH(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_RETRACT, TRUE, iSecondaryStripper >= 0, FALSE)) >= 1.0
					IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
						fLapDancePhase = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
					ELSE
						fLapDancePhase = 0.0
					ENDIF
					
					PRINTLN("Done Retracting, current dance phase ", fLapDancePhase)
					
					PLAY_PLAYER_IN_SEAT_ANIM(iCurrentStripperLDVariation, SLOW_BLEND_IN, INSTANT_BLEND_OUT, fLapDancePhase)
					CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: Returning to previous camera: ", previousLapdanceCamera)
					currentLapdanceCamera = previousLapdanceCamera
					
					IF previousLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
						CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: Moving from touching first person cam to non-touching first person cam")
					ELSE
						CPRINTLN(DEBUG_SCLUB, "HANDLE_DANCE_INPUT: Moving from touching fp cam to non-touching cam")
						SETUP_STRIPCLUB_CAM(stripclubCam, currentLapdanceCamera, <<0,0,0>>, <<0,0,0>>, TRUE)
					ENDIF
					
					TouchingStripperState = TSS_NONE
				ENDIF
			ELSE
				TouchingStripperState = TSS_NONE
			ENDIF
		ELIF NOT STRIPCLUB_IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_FORCE_LAPDANCE_RETRACT)
		ENDIF
	ENDIF
	
	
	IF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC DECLINE_DANCE()
	// decline politely
	CLEAR_ALL_STRIP_CLUB_HELP()
	SET_STIRP_CLUB_ADD_PED_FOR_DIALOGUE()
	
	#IF NOT MP_STRIPCLUB
		PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_DANCE_DECLINE_POLITE", SPEECH_PARAMS_FORCE_NORMAL)
	#ENDIF
	
	TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), namedStripper[iCurrentStripperInteraction].ped, 2500)

	SET_BITMASK_AS_ENUM(namedStripper[iCurrentStripperInteraction].stripperFlags, STRIPPER_FLAG_SAID_APPROACH)

	SET_STRIPPER_SPEECH_TARGET(iCurrentStripperInteraction, STRSPEECH_DECLINED_POLITE)
	RESET_STRIPPER_TO_WANDER(iCurrentStripperInteraction)
	INCREMENT_REFUSAL_COUNT()
	iCurrentStripperInteraction = -1
	iStripperForApproach = -1
ENDPROC

PROC CLEAR_CHAIR_OF_PEDS()
	CLEAR_AREA_OF_PEDS(<<116.484, -1302.986, 29.0 >>, 2.0)
ENDPROC

PROC CLEAR_VIP_CHAIRS_OF_PEDS()
	PRINTLN("Clear Vip Room Of Peds")
	CLEAR_AREA_OF_PEDS(<<114.09, -1302.54, 28.02>>, 5.75)
ENDPROC


PROC CLEAR_VIP_DOOR_OF_OBJECT_AND_VEHICLES()
	CLEAR_AREA_OF_OBJECTS(<<116.626389,-1294.911499,28.270168>> , 1.5)
	CLEAR_AREA_OF_VEHICLES(<<116.626389,-1294.911499,28.270168>> , 1.5)
ENDPROC

FUNC BOOL START_LAPDANCE_MISSION()
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		//on friend activity, already on mision
		RETURN TRUE
	ENDIF

	SWITCH Request_Mission_Launch(candidateIDLapdance, MCTID_CONTACT_POINT, MISSION_TYPE_MINIGAME_FRIENDS)
		CASE MCRET_ACCEPTED
			CPRINTLN(DEBUG_SCLUB, "Allow lap dance routines to start.")
			RETURN TRUE
		BREAK
		CASE MCRET_DENIED
			CPRINTLN(DEBUG_SCLUB, "Another mission has launched first. Back out and don't do the lapdance.")
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)
			RETURN FALSE
		BREAK
		CASE MCRET_PROCESSING
			CPRINTLN(DEBUG_SCLUB, "Continue to wait for a response.")
			RETURN FALSE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_CURRENT_STRIPPER_INTERACTION()
	
	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)	
		#IF NOT MP_STRIPCLUB
			IF NOT CAN_DO_STRIP_CLUB_SPEECH()
				CPRINTLN(DEBUG_SCLUB, "Can't solicit stripper. Conversation is running.")
				RETURN FALSE
			ENDIF
		#ENDIF
	ELSE
		IF NOT IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_OFFR_HELP")
		AND GET_STRIPPER_AI_STATE(iCurrentStripperInteraction) = AI_STATE_APPROACH_PLAYER
		AND NOT IS_PHONE_ONSCREEN()
		AND NOT g_bInATM
			CLEAR_ALL_STRIP_CLUB_HELP()
			PRINT_STRIPCLUB_HELP("SCLUB_OFFR_HELP", TRUE)
		ELSE
			IF IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_OFFR_HELP")
			AND (IS_PHONE_ONSCREEN() OR g_bInATM)
				CLEAR_STRIPCLUB_HELP("SCLUB_OFFR_HELP")
				CLEAR_ALL_STRIP_CLUB_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()
		SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)
		#IF NOT MP_STRIPCLUB
			IF NOT START_LAPDANCE_MISSION()
				RETURN FALSE
			ENDIF
		#ENDIF
		
		// accept
		CLEAR_ALL_STRIP_CLUB_HELP()
		
		//IF GET_STRIP_CLUB_PLAYER_CASH(TRUE) < DANCE_COST
		IF NOT CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
			SET_STRIPPER_SPEECH_TARGET(iCurrentStripperInteraction, STRSPEECH_NO_MONEY_TALK)
			DECLINE_DANCE()
			PRINT_STRIPCLUB_HELP("SCLUB_NO_MONEY")
			STRIP_CLUB_BUY_CASH_ALERT()
			RETURN FALSE
		ENDIF
		
		#IF NOT MP_STRIPCLUB
			// response
			IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_ACCEPTS_DANCE", SPEECH_PARAMS_FORCE_NORMAL)
			ENDIF
		#ENDIF
		
		TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), namedStripper[iCurrentStripperInteraction].ped, 2500)
		SET_STRIPPER_SPEECH_TARGET(iCurrentStripperInteraction, STRSPEECH_FOLLOW)
		
		// blip and task stripper
		IF NOT IS_PED_INJURED(namedStripper[iCurrentStripperInteraction].ped)
			PRINTLN("Soliciting stripper for dance.")
			#IF NOT MP_STRIPCLUB
				STRIP_CLUB_SPEND_CASH(DANCE_COST, TRUE, LAP_DANCE)
			#ENDIF
			
			SET_STRIPPER_GO_TO_ROOM(iCurrentStripperInteraction) //., iCurrentStripperInteraction)
			TEXT_LABEL sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(namedStripper[iCurrentStripperInteraction].stripperID)
			IF NOT DOES_BLIP_EXIST(stripperBlip[0])
				stripperBlip[0] = CREATE_BLIP_FOR_PED(namedStripper[iCurrentStripperInteraction].ped)
			ENDIF
			SET_BLIP_AS_FRIENDLY(stripperBlip[0], TRUE)
			//SET_BLIP_COLOUR(stripperBlip[0], BLIP_COLOUR_BLUE)
			SET_BLIP_NAME_FROM_TEXT_FILE(stripperBlip[0], sStripperName)
			
			//This is where the stripper is set to the CurrentDanceStripper and her LIKE meter is turned on
			IF iCurrentStripperForDance = -1
				iCurrentStripperForDance = iCurrentStripperInteraction
				ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_DANCE, FALSE)
				
				PRINTLN("Current Stripper = ", "",iCurrentStripperForDance , "", "Her name is = ", "", sStripperName)
			ENDIF
		ENDIF
		CLEAR_VIP_DOOR_OF_OBJECT_AND_VEHICLES()
		
		IF CAN_GET_MULTIPLE_DANCERS_IN_CLUB(iCurrentStripperForDance)
			TEXT_LABEL_23 txtMultiGirlHelp = STRIPCLUB_FIXUP_PLAYER_LABEL("SCLUB_MLT_HLP")
			PRINT_STRIPCLUB_HELP(txtMultiGirlHelp)
		ENDIF
		
		CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT)	
		//CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_HELP)	
		
		// change stage
		iRefusalCount = 0
		iCurrentStripperInteraction = -1
		iStripperForApproach = -1
		
		stripClubStage = STAGE_GO_TO_VIP_ROOM
		gotoStage = STRIPCLUB_GOTO_ROOM_INIT
		CLEAR_CHAIR_OF_PEDS()
		
	ELIF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
		DECLINE_DANCE()
	ELSE
		// decline rudely
		
		IF NOT IS_TIMER_STARTED(tInteraction)
			START_TIMER_NOW(tInteraction)
		ENDIF
		
		IF NOT IS_STRIPPER_NEAR_PLAYER(iCurrentStripperInteraction)							
		OR IS_PED_INJURED(namedStripper[iCurrentStripperInteraction].ped) //player knocked ped down? 
		OR IS_PED_RAGDOLL(namedStripper[iCurrentStripperInteraction].ped) //player knocked ped down? 
			
			CLEAR_ALL_STRIP_CLUB_HELP()
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), namedStripper[iCurrentStripperInteraction].ped, 2500)
			ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperInteraction, STRIPPER_LIKE_VALUE_ANNOYED, FALSE)
			SET_STRIPPER_SPEECH_TARGET(iCurrentStripperInteraction, STRSPEECH_DECLINED_RUDE)
			RESET_STRIPPER_TO_WANDER(iCurrentStripperInteraction)
			INCREMENT_REFUSAL_COUNT()
			iCurrentStripperInteraction = -1
			iStripperForApproach = -1
		ELIF (GET_TIMER_IN_SECONDS(tInteraction) > 30.0)
		OR IS_MOBILE_PHONE_CALL_ONGOING()
			CLEAR_ALL_STRIP_CLUB_HELP()
			RESET_STRIPPER_TO_WANDER(iCurrentStripperInteraction)			
			iCurrentStripperInteraction = -1
			iStripperForApproach = -1
		ENDIF
			
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_SOLICITING_SECOND_STRIPPER()
	IF (iSecondaryStripper != -1)
		CPRINTLN(DEBUG_SCLUB, "HANDLE_SOLICITING_SECOND_STRIPPER: 2ndary stripper valid")
	
		IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_DUO)	
			DEBUG_MESSAGE("HANDLE_SOLICITING_SECOND_STRIPPER: bitmasks is set")		
			//IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < DANCE_COST
			IF NOT IS_STRIPPER_WILLING_FOR_DUO_DANCE(iSecondaryStripper)
				IF NOT IS_STRIPCLUB_MP()
					CLEAR_ALL_STRIP_CLUB_HELP()										
					RESET_STRIPPER_TO_WANDER(iSecondaryStripper)
				ENDIF
				
				SET_STRIPPER_SPEECH_TARGET(iSecondaryStripper, STRSPEECH_FOLLOWN)
				iSecondaryStripper = -1									
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_DUO)	
			ELIF CAN_DO_STRIP_CLUB_SPEECH()
				SET_STRIPPER_SPEECH_TARGET(iSecondaryStripper, STRSPEECH_FOLLOW2)
				
				#IF NOT MP_STRIPCLUB
					STRIP_CLUB_SPEND_CASH(DANCE_COST, TRUE, LAP_DANCE)
				#ENDIF
				
				CANCEL_TIMER(tInteraction) //timer checks to see if the player is actaully going to the vip room
				TEXT_LABEL sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(namedStripper[iSecondaryStripper].stripperID)
				IF NOT DOES_BLIP_EXIST(stripperBlip[1])
					stripperBlip[1] = CREATE_BLIP_FOR_PED(namedStripper[iSecondaryStripper].ped)
				ENDIF
				SET_BLIP_AS_FRIENDLY(stripperBlip[1], TRUE)
				//SET_BLIP_COLOUR(stripperBlip[1], BLIP_COLOUR_BLUE)
				SET_BLIP_NAME_FROM_TEXT_FILE(stripperBlip[1], sStripperName)
				SET_SHOW_GOD_TEXT(FALSE)

				SET_STRIPPER_GO_TO_ROOM(iSecondaryStripper)
				CLEAR_VIP_DOOR_OF_OBJECT_AND_VEHICLES()

				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_SECOND_STRIPPER_TASKED)
				
				IF NETWORK_IS_IN_SESSION()
					//INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_LAPDANCES, 1)
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT , 1)
				ENDIF					
				ADJUST_STRIPPER_LIKE_VALUE(iSecondaryStripper, STRIPPER_LIKE_VALUE_DANCE, FALSE)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_SOLICIT_DUO)
			ENDIF
		ELSE
	
		ENDIF			
		
		//DEBUG_MESSAGE("HANDLE_SOLICITING_SECOND_STRIPPER: bitmasks not set")
	ELSE
		//DEBUG_MESSAGE("HANDLE_SOLICITING_SECOND_STRIPPER: 2ndary stripper NOT valid")
	ENDIF
ENDPROC

PROC SET_STRIPPER_LAP_DANCE(BOOL b3Way)
	
	// primary stripper
	PRINTLN("Set stripper lap dance")
	IF NOT IS_PED_INJURED(namedStripper[iCurrentStripperForDance].ped)
		IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_INTRO_SKIPPED)
			PRINTLN("Intro wasn't skipped")
			VECTOR vSeatPos
			FLOAT fHeading
			
			vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
			STRIPCLUB_CREATE_SYNC_SCENE(iLapDanceScene, vSeatPos, <<0,0, fHeading>>, TRUE, FALSE, TRUE)
			STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, b3Way), GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(iCurrentStripperLDVariation, FALSE, b3Way, FALSE), INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
			
			IF NOT IS_ENTITY_PLAYING_ANIM(namedStripper[iCurrentStripperForDance].ped, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, b3Way), GET_STRIP_CLUB_DANCE_FACE_ANIM(iCurrentStripperLDVariation, b3Way, FALSE), ANIM_DEFAULT)
				
				IF GET_SCRIPT_TASK_STATUS(namedStripper[iCurrentStripperForDance].ped, SCRIPT_TASK_ANY) != PERFORMING_TASK
					TASK_PLAY_ANIM(namedStripper[iCurrentStripperForDance].ped, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, b3Way), GET_STRIP_CLUB_DANCE_FACE_ANIM(iCurrentStripperLDVariation, b3Way, FALSE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
				ENDIF
			ENDIF
			
			IF b3Way
				TASK_PLAY_ANIM(namedStripper[iSecondaryStripper].ped, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, b3Way), GET_STRIP_CLUB_DANCE_FACE_ANIM(iCurrentStripperLDVariation, b3Way, TRUE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
				STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, TRUE), GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(iCurrentStripperLDVariation, FALSE, TRUE, TRUE),  INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
			ENDIF
			
			IF TouchingStripperState = TSS_TOUCHING
				STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iCurrentStripperLDVariation, iSecondaryStripper >= 0), GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(iCurrentStripperLDVariation, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
			ELSE
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), GET_PLAYER_LAPDANCE_IDLE_DICT(iCurrentStripperLDVariation, iSecondaryStripper >= 0), GET_PLAYER_LAPDANCE_IDLE_ANIM(iCurrentStripperLDVariation, iSecondaryStripper >= 0), ANIM_DEFAULT)
					PRINTLN("SET_STRIPPER_LAP_DANCE: Tasking to animate player")
					PLAY_PLAYER_IN_SEAT_ANIM(iCurrentStripperLDVariation, SLOW_BLEND_IN, SLOW_BLEND_OUT, 0)
				ENDIF
			ENDIF
			STRIPCLUB_START_SYNC_SCENE(iLapDanceScene)
		ELSE
			CPRINTLN(DEBUG_SCLUB, "SET_STRIPPER_LAP_DANCE: Clearing bitmask for intro skipped")
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_INTRO_SKIPPED)
		ENDIF
	ENDIF
			
ENDPROC

FUNC BOOL HAS_LAPDANCE_ENDED(BOOL bLoopForever)
	
	INT iRemoveScene, iNextScene, iLoadScene
	INT iSceneID = -1
	IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene, IS_STRIPCLUB_MP()))
		iSceneID = GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene, IS_STRIPCLUB_MP())
		SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID, FALSE)
	ENDIF
	
	//CPRINTLN(DEBUG_SCLUB, "iSceneID = ", iSceneID)
	
	IF iSceneID >= 0
		//CPRINTLN(DEBUG_SCLUB, "Lapdance phase ", GET_SYNCHRONIZED_SCENE_PHASE(iSceneID))
		IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) >= 1.0
			iNextScene = PICK_INT(iCurrentStripperLDVariation+1 > 2, 0, iCurrentStripperLDVariation+1)
			//CPRINTLN(DEBUG_SCLUB, "iNextScene = ", iNextScene)
			REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iNextScene, iSecondaryStripper >= 0)) // just incase
			
			//CPRINTLN(DEBUG_SCLUB, "Loading anim dict ", GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iNextScene, iSecondaryStripper >= 0))
			IF HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iNextScene, iSecondaryStripper >= 0))
				//CPRINTLN(DEBUG_SCLUB, "Anim dict has loaded ")
				iRemoveScene = iCurrentStripperLDVariation
				iCurrentStripperLDVariation++
				iLoadScene = PICK_INT(iCurrentStripperLDVariation+1 > 2, 0, iCurrentStripperLDVariation+1)
				
				IF bLoopForever AND iCurrentStripperLDVariation > 2
					iCurrentStripperLDVariation = 0
				ENDIF
				
				//CPRINTLN(DEBUG_SCLUB, "Play lapdance variation ", iCurrentStripperLDVariation)
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iLoadScene, iSecondaryStripper >= 0))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iRemoveScene, iSecondaryStripper >= 0))
				
				IF iCurrentStripperLDVariation > 2
					iCurrentStripperLDVariation = 0
					RETURN TRUE
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
						SET_STRIPPER_LAP_DANCE(iSecondaryStripper >= 0)
					ENDIF
					
					RETURN FALSE
				ENDIF
			ELSE
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iNextScene, iSecondaryStripper >= 0))
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL OKAY_TO_EXIT_LAPDANCE_ANIMATION(FLOAT fEndOffset = 0.0)
	FLOAT fPhase
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))
		CPRINTLN(DEBUG_SCLUB, "Checking if safe to start exit, Dance: ", iCurrentStripperLDVariation, " Phase : ", fPhase)
		
		IF iSecondaryStripper < 0
		//one dancer
			IF iCurrentStripperLDVariation = 0
				IF (fPhase > 0.32 AND fPhase < (0.55-fEndOffset))
				OR (fPhase > 0.75 AND fPhase < (0.885-fEndOffset))
					RETURN FALSE
				ENDIF
				
			ELIF iCurrentStripperLDVariation = 1
				IF (fPhase > 0.66 AND fPhase < (0.88-fEndOffset))
					RETURN FALSE
				ENDIF			
			ELIF iCurrentStripperLDVariation = 2
				IF (fPhase > 0.12 AND fPhase < (0.37-fEndOffset))
				OR (fPhase > 0.39 AND fPhase < (0.55-fEndOffset))
				OR (fPhase > 0.76 AND fPhase < (0.95-fEndOffset))
					RETURN FALSE
				ENDIF				
			ENDIF
		ELSE
			//two dancers
			
		ENDIF

	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL PLAY_LAPDANCE_END_ANIMATION(BOOL bQuickExit = FALSE)
	
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(TRUE, FALSE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, FALSE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, TRUE))

	FLOAT fBlendInSpeed = WALK_BLEND_IN
	
	IF bQuickExit AND iSecondaryStripper < 0
		fBlendInSpeed = INSTANT_BLEND_IN
	ENDIF
	
	IF HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(TRUE, FALSE))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, FALSE))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, TRUE))
		
		VECTOR vSeatPos
		FLOAT fHeading
		vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
		
		STRIPCLUB_CREATE_SYNC_SCENE(iApproachDanceScene, vSeatPos, <<0,0, fHeading>>, TRUE, FALSE, TRUE)
		
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iApproachDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, bQuickExit), GET_LAPDANCE_END_ANIMATION(FALSE, iSecondaryStripper >= 0, FALSE, bQuickExit), fBlendInSpeed, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
		TASK_PLAY_ANIM(namedStripper[iCurrentStripperForDance].ped, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, FALSE), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_EXIT, iSecondaryStripper >= 0, FALSE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_HOLD_LAST_FRAME)
		
		IF iSecondaryStripper >= 0
			TASK_PLAY_ANIM(namedStripper[iSecondaryStripper].ped, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, FALSE), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_EXIT, iSecondaryStripper >= 0, TRUE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_HOLD_LAST_FRAME)
			STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iApproachDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, bQuickExit), GET_LAPDANCE_END_ANIMATION(FALSE, iSecondaryStripper >= 0, TRUE, bQuickExit), fBlendInSpeed, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
		ENDIF
		
		IF bQuickExit
			STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iApproachDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, bQuickExit), GET_LAPDANCE_END_ANIMATION(TRUE, iSecondaryStripper >= 0, FALSE, bQuickExit), fBlendInSpeed, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
			STRIPCLUB_SET_SYNC_SCENE_CAM(iApproachDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, bQuickExit), GET_STRIP_CLUB_LAPDANCE_END_CAM(iSecondaryStripper >= 0, bQuickExit), FALSE, TRUE)
		ELIF TouchingStripperState = TSS_TOUCHING
			STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iApproachDanceScene, GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(iSecondaryStripper >= 0, FALSE), GET_LAPDANCE_END_ANIMATION( TRUE, iSecondaryStripper >= 0, FALSE, FALSE), fBlendInSpeed, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
		ELSE
			TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, FALSE), "ld_girl_a_exit_no_touch_m", WALK_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_TURN_OFF_COLLISION, 0, FALSE, AIK_DISABLE_LEG_IK)
		ENDIF
		
		STRIPCLUB_START_SYNC_SCENE(iApproachDanceScene)
		
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_STRIP)		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LAPDANCE_END_ANIMATION_OVER(INT &iSceneID, BOOL bSetLooping, FLOAT fEndTime)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iSceneID, IS_STRIPCLUB_MP()))
	
		IF iSceneID >= 0
			IF bSetLooping
				//CPRINTLN(DEBUG_SCLUB, "IS_LAPDANCE_END_ANIMATION_OVER: Setting to loop")
				SET_SYNCHRONIZED_SCENE_LOOPED(GET_SRTIPCLUB_LOCAL_SCENE_ID(iSceneID, IS_STRIPCLUB_MP()), TRUE)
			ENDIF
		ENDIF
	
		//CPRINTLN(DEBUG_SCLUB, "IS_LAPDANCE_END_ANIMATION_OVER: Scene time ", GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iSceneID, IS_STRIPCLUB_MP())))
		IF GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iSceneID)) >= fEndTime
			RETURN TRUE
		ENDIF
	ELSE
		//CPRINTLN(DEBUG_SCLUB, "IS_LAPDANCE_END_ANIMATION_OVER: Lap dance end animation not playing")
		RETURN TRUE
	ENDIF
	
	//CPRINTLN(DEBUG_SCLUB, "IS_LAPDANCE_END_ANIMATION_OVER: returning false")
	RETURN FALSE
ENDFUNC

PROC PLAY_LAPDANCE_OFFERS_ANIMATION()
	VECTOR vSeatPos
	FLOAT fHeading
	vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
	
	STRIPCLUB_CREATE_SYNC_SCENE(iApproachDanceScene, vSeatPos, <<0,0, fHeading>>, FALSE, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iApproachDanceScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_OFFER, FALSE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iApproachDanceScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_OFFER, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	TASK_PLAY_ANIM(namedStripper[iCurrentStripperForDance].ped, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_OFFER, iSecondaryStripper >= 0, FALSE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
	
	IF iSecondaryStripper >= 0
		TASK_PLAY_ANIM(namedStripper[iSecondaryStripper].ped, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_OFFER, iSecondaryStripper >= 0, TRUE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iApproachDanceScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_OFFER, FALSE, iSecondaryStripper >= 0, TRUE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	ENDIF
	
//	STRIPCLUB_SET_SYNC_SCENE_CAM(iApproachDanceScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_CAM_ANIM(SCLDA_OFFER, iSecondaryStripper >= 0), FALSE, TRUE)
	//SET_SYNCHRONIZED_SCENE_LOOPED(iApproachDanceScen, TRUE)
	STRIPCLUB_START_SYNC_SCENE(iApproachDanceScene)
ENDPROC

PROC PLAY_LAPDANCE_ACCEPT_ANIMATION()
	VECTOR vSeatPos
	FLOAT fHeading
	vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
	
	STRIPCLUB_CREATE_SYNC_SCENE(iDanceOfferScene, vSeatPos, <<0,0, fHeading>>, TRUE, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_ACCEPT, FALSE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_ACCEPT, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	TASK_PLAY_ANIM(namedStripper[iCurrentStripperForDance].ped, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_ACCEPT, iSecondaryStripper >= 0, FALSE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
		
	IF iSecondaryStripper >= 0
		TASK_PLAY_ANIM(namedStripper[iSecondaryStripper].ped, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_ACCEPT, iSecondaryStripper >= 0, TRUE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_ACCEPT, FALSE, iSecondaryStripper >= 0, TRUE), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	ENDIF
	
	//STRIPCLUB_SET_SYNC_SCENE_CAM(iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_CAM_ANIM(SCLDA_ACCEPT, iSecondaryStripper >= 0), FALSE, TRUE)
	STRIPCLUB_START_SYNC_SCENE(iDanceOfferScene)
ENDPROC

PROC PLAY_LAPDANCE_DECLINE_ANIMATION()
	VECTOR vSeatPos
	FLOAt fHeading
	vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
	BOOL bHappy = FALSE
	
	IF DO_TAKE_HOME_TEST(iCurrentStripperForDance)
		bHappy = TRUE
	ENDIF
	
	STRIPCLUB_CREATE_SYNC_SCENE(iDanceOfferScene, vSeatPos, <<0,0, fHeading>>, TRUE, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_DECLINE, FALSE, iSecondaryStripper >= 0, FALSE, bHappy), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_DECLINE, TRUE, iSecondaryStripper >= 0, FALSE), SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_TAG_SYNC_OUT, FALSE, TRUE)
	TASK_PLAY_ANIM(namedStripper[iCurrentStripperForDance].ped, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_DECLINE, iSecondaryStripper >= 0, FALSE, bHappy), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
	
	IF iSecondaryStripper >= 0
		TASK_PLAY_ANIM(namedStripper[iSecondaryStripper].ped, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(SCLDA_DECLINE, iSecondaryStripper >= 0, TRUE, bHappy), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_DECLINE, FALSE, iSecondaryStripper >= 0, TRUE, bHappy), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, TRUE)
	ENDIF
	
//	#IF NOT MP_STRIPCLUB
		STRIPCLUB_SET_SYNC_SCENE_CAM(iDanceOfferScene, GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_CAM_ANIM(SCLDA_DECLINE, iSecondaryStripper >= 0), FALSE, TRUE)
//	#ENDIF
	#IF MP_STRIPCLUB
		IF iCurrentLapDanceSeat = 6 OR iCurrentLapDanceSeat = 0 OR iCurrentLapDanceSeat = 3 OR iCurrentLapDanceSeat = 4
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(89.7517)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-18.5786)
		ENDIF
	#ENDIF
	
	STRIPCLUB_START_SYNC_SCENE(iDanceOfferScene)
	
	#IF MP_STRIPCLUB
		SET_STRIPCLUB_HAT()
		SET_STRIPCLUB_PLAYER_MASK(TRUE)
		SET_STRIPCLUB_NIGHTVISION(TRUE)
		SET_STRIPCLUB_PARACHUTE()
		SET_STRIPCLUB_SHOES()
		SET_STRIPCLUB_JACKET()
	#ENDIF
ENDPROC

PROC LOOK_AT_STRIPPERS_BODY_PART(INT iStripper,  PED_BONETAG BodyPart)
	INT iBlend = 1500
	SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, namedStripper[iStripper].ped, ENUM_TO_INT(BodyPart), <<0.0, 0.0, 0.0>>, ITF_DEFAULT, iBlend, iBlend)
ENDPROC

FUNC PED_BONETAG GET_LOOK_AT_BODY_PART()

	//70% change the player will look at there favorite body part
	//Michael - boobs
	//Franklin - ass
	//Trevor - feet
	IF  GET_RANDOM_INT_IN_RANGE(0, 10) <= 7
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			RETURN BONETAG_L_FOOT
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			RETURN BONETAG_SPINE3
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			RETURN BONETAG_PELVIS
		ENDIF
	ENDIF

	INT iRandomChoice = GET_RANDOM_INT_IN_RANGE(0, 4)
		
	//other wise somthing random
	IF iRandomChoice = 0  
		RETURN BONETAG_HEAD
	ELIF iRandomChoice = 1
		RETURN BONETAG_PELVIS
	ELIF  iRandomChoice = 2
		RETURN BONETAG_SPINE3
	ELSE
		RETURN BONETAG_L_FOOT
	ENDIF

ENDFUNC

FUNC INT GET_STRIPPER_TO_LOOK_AT()

	IF iSecondaryStripper >= 0
		IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
			RETURN iSecondaryStripper
		ENDIF
	ENDIF
	
	RETURN iCurrentStripperForDance
ENDFUNC

PROC MANAGE_END_OF_DANCE_HELP(BOOL bCanTakeHome)
	
	IF IS_STRIPCLUB_MESSAGE_WITH_STRIPPER_NAME_DISPLAYED("SCLUB_WONT_HOME")
		EXIT
	ENDIF
	
	IF iSecondaryStripper < 0
	
		IF CAN_GET_MULTIPLE_DANCERS_IN_CLUB()
		AND NOT IS_STRIPCLUB_MP() 
		AND CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE, FALSE, 0)
		//AND GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
			IF bCanTakeHome
				PRINT_STRIPCLUB_HELP_WITH_STRIPPER_NAMES("SCLUB_MORE_HEL4", namedStripper[iCurrentStripperForDance].stripperID, TRUE, 1)
			ELSE
				PRINT_STRIPCLUB_HELP("SCLUB_MORE_HEL2", TRUE, 1)
			ENDIF
		ELSE
			IF bCanTakeHome
				PRINT_STRIPCLUB_HELP_WITH_STRIPPER_NAMES("SCLUB_MORE_HEL3", namedStripper[iCurrentStripperForDance].stripperID, TRUE, 1)
			ELSE
				PRINT_STRIPCLUB_HELP("SCLUB_MORE_HELP", TRUE, 1)
			ENDIF		
		ENDIF
	ELSE
		IF bCanTakeHome
			PRINT_STRIPCLUB_HELP_WITH_STRIPPER_NAMES("SCLUB_MOR2_HEL2", namedStripper[iCurrentStripperForDance].stripperID, TRUE, 1)
		ELSE
			PRINT_STRIPCLUB_HELP("SCLUB_MOR2_HELP", TRUE, 1)
		ENDIF
	ENDIF
ENDPROC

PROC SET_STRIPPER_ANOTHER_DANCE_SPEECH()
	IF iSecondaryStripper > 0
		SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_AGAIN2)
	ELSE
		SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_AGAIN)
	ENDIF
ENDPROC

FUNC BOOL ADD_STRIPPER_NUMBER_POST_LAPDANCE(INT iStripper, BOOL bDoSpeach)
	
	IF NOT IS_BOOTY_CALL_ID_ACTIVATED(namedStripper[iStripper].stripperID)
		PRINTLN("Stripper ", iStripper," is not unlocked as a bootycall")
		IF  IS_STRIPPER_BOOTYCALL_STRIPPER(namedStripper[iStripper].stripperID)
		OR IS_STRIPCLUB_MP() // all girls in mp are bootycall girls
			PRINTLN("Stripper ", iStripper," is a valid bootycall")
			IF DO_TAKE_HOME_TEST(iStripper)
				PRINTLN("Stripper ", iStripper," passed take home test")
				IF bDoSpeach
					SET_STRIPPER_SPEECH_TARGET(iStripper, STRSPEECH_GIVE_NUMBER_AFTER_DANCE)
				ENDIF
				DO_STRIPPER_GIVE_NUMBER(iStripper)
				SET_KEEP_HELP_AND_SPEECH_FROM_DANCE(TRUE)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

// stage: get private dance from stripper
PROC DO_STAGE_DANCE()
	TEXT_LABEL_23 sFixedup
	FLOAT fEndingPhase
					
	VECTOR vDesiredCamPos
	FLOAT fDesiredHeading
				
	IF danceStage >= DANCE_IN_PROGRESS
		MANAGE_VIP_BOUNCER_ROTATION()
	ENDIF
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_SELECTOR_THIS_FRAME()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	#IF MP_STRIPCLUB
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
	#ENDIF
	
	#IF NOT MP_STRIPCLUB
		IF candidateIDLapdance = NO_CANDIDATE_ID
		AND candidateID = NO_CANDIDATE_ID
		AND danceStage != DANCE_OFFERS //during dance offers we switch mission types
			START_LAPDANCE_MISSION() //no mission is running, start one
		ENDIF
	#ENDIF
	
	IF danceStage = DANCE_IN_PROGRESS
		IF iSecondaryStripper >= 0
			IF IS_PED_INJURED(namedStripper[iSecondaryStripper].ped)
				STRIPCLUB_REGAIN_PLAYER()
			ENDIF
		ENDIF
		
		IF iCurrentStripperForDance >= 0
			IF IS_PED_INJURED(namedStripper[iCurrentStripperForDance].ped)
				STRIPCLUB_REGAIN_PLAYER()
				EXIT
			ENDIF
		ELSE
			PRINTLN("Trying to do dance stage witout a stripper")
			EXIT
		ENDIF
	ENDIF
//
//	IF danceStage = DANCE_IN_PROGRESS
//		CPRINTLN(DEBUG_SCLUB, "MAIN LOOP: In DANCE_IN_PROGRESS")
//	ELSE
//		CPRINTLN(DEBUG_SCLUB, "MAIN LOOP: Current state: ", ENUM_TO_INT(danceStage))
//	ENDIF
	SPEECH_PARAMS flirtSpeechParams = SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL
	
	//B* 2463715: Prevent replay recording throughout the private dance minigame
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	SWITCH danceStage
		
		CASE DANCE_INIT	
		PRINTLN("DANCE_INIT")
			fTalkHudValue = 0
			ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, 1, TRUE, FALSE)
			
			#IF NOT MP_STRIPCLUB
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_BOUNCER_YELL)
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BOUNCER_YELL())
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_BOUNCER_YELL)	
				ENDIF
			#ENDIF
			
			#IF MP_STRIPCLUB
				DISABLE_ALL_MP_HUD()
				SET_DPADDOWN_ACTIVE(FALSE)
			#ENDIF
			
			HANG_UP_AND_PUT_AWAY_PHONE()
			DISABLE_KILL_YOURSELF_OPTION()
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
			#IF MP_STRIPCLUB
				CLEAR_TEXT_MESSAGE_SIGNIFIER_FOR_MULTIPLAYER()
			#ENDIF
			
			IF NOT IS_STRIPCLUB_MP()			
				IF NOT IS_BITMASK_AS_ENUM_SET( iStripClubBits_General2, STRIP_CLUB_GENERAL2_HAS_VIEWED_PRIVATE_DANCE )
					IF iSecondaryStripper >= 0
						IF DOES_TREVOR_OWN_CLUB()
							PRINT_STRIPCLUB_HELP("SCLUB_LIK2_SP2")
						ELSE
							PRINT_STRIPCLUB_HELP("SCLUB_LIK2_SP1")
						ENDIF
					ELSE
						IF DOES_TREVOR_OWN_CLUB()
							PRINT_STRIPCLUB_HELP("SCLUB_LIKE_SP2")
						ELSE
							PRINT_STRIPCLUB_HELP("SCLUB_LIKE_SP1")
						ENDIF
					ENDIF
					SET_BITMASK_AS_ENUM( iStripClubBits_General2, STRIP_CLUB_GENERAL2_HAS_VIEWED_PRIVATE_DANCE )
				ENDIF
			ELIF NOT IS_BITMASK_AS_ENUM_SET( iStripClubBits_General2, STRIP_CLUB_GENERAL2_HAS_VIEWED_PRIVATE_DANCE )
				IF iSecondaryStripper >= 0
					PRINT_STRIPCLUB_HELP("SCLUB_LIK2_MP")
				ELSE
					PRINT_STRIPCLUB_HELP("SCLUB_LIKE_MP")
				ENDIF
				
				SET_BITMASK_AS_ENUM( iStripClubBits_General2, STRIP_CLUB_GENERAL2_HAS_VIEWED_PRIVATE_DANCE )
			ENDIF
			
			SET_BITMASK_AS_ENUM(namedStripper[iCurrentStripperForDance].stripperFlags, STRIPPER_FLAG_HAD_LAPDANCE)
			IF iSecondaryStripper >= 0
				SET_BITMASK_AS_ENUM(namedStripper[iSecondaryStripper].stripperFlags, STRIPPER_FLAG_HAD_LAPDANCE)
			ENDIF
			
			// setup cam	
			IF DOES_CAM_EXIST(stripclubInterpFromCam)
				DESTROY_CAM(stripclubInterpFromCam)
			ENDIF	
			IF DOES_CAM_EXIST(stripclubCutCam)
				DESTROY_CAM(stripclubCutCam)
			ENDIF
			
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
			IF IS_PED_DUCKING(PLAYER_PED_ID()) // forces player to not crouch
				SET_PED_DUCKING(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
			SETUP_STRIPCLUB_CAM(stripclubCam, STRCLUB_CAM_DANCE_FIRST_PERSON, <<0,0,0>>, <<0,0,0>>)
			
			DISABLE_CELLPHONE(TRUE)
			
			SET_SHOW_LIKE_HUD(TRUE)
			
			SET_STRIPCLUB_GLOBAL_GETTING_LAPDANCE(TRUE)
			danceStage = DANCE_SET_PLAYER
		BREAK
		
		CASE DANCE_SET_PLAYER
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND HAVE_ALL_STRIP_CLUB_LAPDANCE_ANIMS_LOADED()
				#IF MP_STRIPCLUB
					REMOVE_STRIPCLUB_NIGHTVISION(TRUE)
					REMOVE_STRIPCLUB_PLAYER_MASK(TRUE)
				#ENDIF
				danceStage = DANCE_SET_STRIPPER
			ENDIF
		BREAK
		
		CASE DANCE_SET_STRIPPER
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_DOUBLE_DANCE_SYNC_SCENE)
		
			STRIPCLUB_END_SYNC_SCENE(iApproachDanceScene)
			// primary stripper	
			iCurrentStripperLDVariation = 0
			SET_SHOW_LIKE_HUD(TRUE)
			IF NOT IS_PED_INJURED(namedStripper[iCurrentStripperForDance].ped)
				SET_STRIPPER_LAP_DANCE(iSecondaryStripper >= 0)
			ENDIF
			
			IF NOT IS_PED_INJURED(clubStaffPed[STAFF_BOUNCER_PATROL])
				//CLEAR_PED_TASKS_IMMEDIATELY(clubStaffPed[STAFF_BOUNCER_PATROL])
				//SET_ENTITY_COORDS(clubStaffPed[STAFF_BOUNCERSTAFF_BOUNCER_PATROL_VIP], GET_BOUNCER_PARTOL_START_POS())
				//SET_ENTITY_HEADING(clubStaffPed[STAFF_BOUNCER_PATROL], 133.96)
			ENDIF
			
			IF IS_PED_MODEL(PLAYER_PED_ID(), GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				SHRINK_ADD_LAP_DANCE_TIMESTAMP()
			ENDIF
			
			SET_SHOW_GOD_TEXT(FALSE)
			
			// do initial touch test
			//bAllowTouch = DO_ALLOW_TOUCH_TEST(iCurrentStripperForDance)
			
			DISPLAY_HUD(TRUE)
			
			#IF IS_DEBUG_BUILD
		//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			#ENDIF
			
			SET_KEEP_HELP_AND_SPEECH_FROM_DANCE(FALSE)
			
			IF NOT IS_ANY_STRIPCLUB_LIKE_HELP_MESSAGE_DISPLAYED()
				#IF MP_STRIPCLUB
					
					IF currentLapdanceCamera != STRCLUB_CAM_DANCE_FIRST_PERSON
						PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HLM2", TRUE, 1)
					ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
					AND TouchingStripperState != TSS_NONE
						PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HLM3", TRUE, 1)
					ELSE
						PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HLMP", TRUE, 1)
					ENDIF
				#ENDIF
				
				#IF NOT MP_STRIPCLUB
					// If there is not a message displayed for touching the stripper while bouncer is looking
					IF GET_GAME_TIMER() > iNextCaughtTouchingTime //NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCLUB_BSAW_HELP")
						PRINTLN("HANDLE_DANCE_INPUT: GET_GAME_TIMER() was considered higher than iNextCaughtTouchingTime: ", GET_GAME_TIMER(), " ", iNextCaughtTouchingTime)
						IF currentLapdanceCamera != STRCLUB_CAM_DANCE_FIRST_PERSON
							PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HEL2", TRUE, 1)
						ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
						AND TouchingStripperState != TSS_NONE
							IF IS_VIP_BOUNCER_PRESENT_FOR_LAPDANCE() 
							AND GET_GAME_TIMER() > iNextCaughtTouchingTime
							AND NOT IS_STRIPCLUB_MP() AND NOT DOES_TREVOR_OWN_CLUB()
							AND NOT CAN_DEBUG_ALLOW_STRIPPER_TOUCH()
							AND CAN_DO_STRIP_CLUB_SPEECH()
						
							ELSE
								PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HEL3", TRUE, 1)
							ENDIF
						ELSE
							PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HELP", TRUE, 1)
						ENDIF
					ENDIF
				#ENDIF
			ENDIF
			IF  IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_DANCING_INTRO_STREAMED)
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_APPROACH(iSecondaryStripper >= 0))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(iSecondaryStripper >= 0))
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_DANCING_INTRO_STREAMED)
			ENDIF
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_FORCE_LAPDANCE_RETRACT)
			
			RESTART_TIMER_AT(tDanceDialogue, TIME_BETWEEN_DANCE_DIALOGUE)
			
			#IF NOT MP_STRIPCLUB
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(0, TRUE))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(1, TRUE))	
			
				REMOVE_STRIPPER_IDLES()
			#ENDIF
				
			danceStage = DANCE_IN_PROGRESS
		BREAK
		
		CASE DANCE_IN_PROGRESS
			
			DISPLAY_HUD(TRUE)
			
			IF iCurrentStripperLDVariation = 0
				IF iNextUpdateDanceLookAtTime < GET_GAME_TIMER()
					iLookAtStripperDuringDance = GET_STRIPPER_TO_LOOK_AT()
					boneLookAtBodyDuringDance = GET_LOOK_AT_BODY_PART()
					iNextUpdateDanceLookAtTime = GET_GAME_TIMER() + 5000
				ENDIF
				LOOK_AT_STRIPPERS_BODY_PART(iLookAtStripperDuringDance, boneLookAtBodyDuringDance)
			ENDIF
			
			IF DOES_CAM_EXIST(fpStripCam.theCam)
				vLapdanceCamPos = GET_CAM_COORD(fpStripCam.theCam)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iApproachDanceScene))
				SET_SYNCHRONIZED_SCENE_LOOPED(GET_SRTIPCLUB_LOCAL_SCENE_ID(iApproachDanceScene), TRUE)
			ENDIF
			
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_RESPONSE_TALK)
				IF CAN_DO_STRIP_CLUB_SPEECH()
				AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
					IF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
						flirtSpeechParams = SPEECH_PARAMS_FORCE_FRONTEND
					ENDIF
					
					IF iSecondaryStripper < 0
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_COMMENT_DANCE_SINGLE", flirtSpeechParams)
					ELSE
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), PICK_STRING(GET_RANDOM_BOOL(), "STRIP_COMMENT_DANCE_SINGLE", "STRIP_COMMENT_DANCE_MULTI"), flirtSpeechParams)
					ENDIF
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_RESPONSE_TALK)
					//RESTART_TIMER_AT(tDanceDialogue, 12.0)
					RESTART_TIMER_NOW(tDanceDialogue)
				ENDIF
			ENDIF
			
			//DEBUG_MESSAGE("bouncer would be visible")
			
			IF NOT IS_ANY_STRIPCLUB_LIKE_HELP_MESSAGE_DISPLAYED()
			AND NOT IS_STRIPPER_LIKE_METER_FULL_HELP_DISPLAYING()
				#IF MP_STRIPCLUB
					IF currentLapdanceCamera != STRCLUB_CAM_DANCE_FIRST_PERSON
						PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HLM2", TRUE, 1)
					ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
					AND TouchingStripperState != TSS_NONE
						PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HLM3", TRUE, 1)
					ELSE
						PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HLMP", TRUE, 1)
					ENDIF
					
//					CPRINTLN(DEBUG_SCLUB, "LIKE ", GET_LIKE_FOR_STRIPPER_ID(namedStripper[iCurrentStripperForDance].stripperID))
					IF GET_LIKE_FOR_STRIPPER_ID(namedStripper[iCurrentStripperForDance].stripperID) >= STRIPPER_MAX_LIKE
						PRINT_STRIPPER_LIKE_METER_IS_FULL(iCurrentStripperForDance)
					ENDIF
					
					IF iSecondaryStripper >= 0
						IF GET_LIKE_FOR_STRIPPER_ID(namedStripper[iSecondaryStripper].stripperID) >= STRIPPER_MAX_LIKE
							PRINT_STRIPPER_LIKE_METER_IS_FULL(iSecondaryStripper)
						ENDIF
					ENDIF
				#ENDIF
				
				#IF NOT MP_STRIPCLUB
//					// If there is not a message displayed for touching the stripper while bouncer is looking
					IF GET_GAME_TIMER() > iNextCaughtTouchingTime  
//						PRINTLN("HANDLE_DANCE_INPUT: GET_GAME_TIMER() was considered higher than iNextCaughtTouchingTime: ", GET_GAME_TIMER(), " ", iNextCaughtTouchingTime)
						IF currentLapdanceCamera != STRCLUB_CAM_DANCE_FIRST_PERSON
							PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HEL2", TRUE, 1)
						ELIF currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON
						AND TouchingStripperState != TSS_NONE
							IF IS_VIP_BOUNCER_PRESENT_FOR_LAPDANCE() 
							AND GET_GAME_TIMER() > iNextCaughtTouchingTime
							AND NOT IS_STRIPCLUB_MP() AND NOT DOES_TREVOR_OWN_CLUB()
							AND NOT CAN_DEBUG_ALLOW_STRIPPER_TOUCH()
							AND CAN_DO_STRIP_CLUB_SPEECH()
						
							ELSE
								PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HEL3", TRUE, 1)
							ENDIF
						ELSE
							PRINT_STRIPCLUB_HELP("SCLUB_DNCE_HELP", TRUE, 1)
						ENDIF
					ENDIF
				#ENDIF
				
			ENDIF
			
			IF HANDLE_DANCE_INPUT() //player canceled dance
				IF danceStage != DANCE_CAUGHT_TOUCHING
					CLEAR_ALL_STRIP_CLUB_HELP()
					
//					ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_ANNOYED, TRUE)
					KILL_ANY_CONVERSATION()
					
					#IF NOT MP_STRIPCLUB
						//If you are not near a place that is safe to exit
						IF OKAY_TO_EXIT_LAPDANCE_ANIMATION(0.05)
							//Ask her to leave again
							SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_DELAY_ASK_TO_LEAVE)
						ENDIF
						
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_DANCE_QUIT", SPEECH_PARAMS_FORCE_NORMAL)
					#ENDIF
					
					RESTART_TIMER_NOW(tControl)
					
					IF IS_STRIPCLUB_MP() OR DO_TAKE_HOME_TEST(iCurrentStripperForDance)
						nextDanceStage = DANCE_FINISHED
						danceStage = DANCE_EXIT
					ELSE
						nextDanceStage = DANCE_CANCELED
						danceStage = DANCE_EXIT
					ENDIF
				ELSE
					
					CLEAR_ALL_STRIP_CLUB_HELP()
//					ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_ANNOYED, TRUE)		
					RESTART_TIMER_NOW(tControl)
					SET_SHOW_LIKE_HUD(FALSE)
				ENDIF
			ELSE
				//CPRINTLN(DEBUG_SCLUB, "MAIN LOOP: In DANCE_IN_PROGRESS")
				IF NOT IS_PAUSE_MENU_ACTIVE()
					HANDLE_CAMERA_INPUT()
				ENDIF
				
				//GET_SCRIPT_TASK_STATUS(namedStripper[iCurrentStripperForDance].ped, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				IF HAS_LAPDANCE_ENDED(FALSE)
					CLEAR_ALL_STRIP_CLUB_HELP()
					RESTART_TIMER_NOW(tControl)
					nextDanceStage = DANCE_FINISHED
					danceStage = DANCE_EXIT
					candidateID = NO_CANDIDATE_ID
				ELSE
					IF NOT IS_BITMASK_AS_ENUM_SET(namedStripper[iCurrentStripperForDance].stripperFlags, STRIPPER_FLAG_SAID_WANT_HOME)
					AND DO_TAKE_HOME_TEST(iCurrentStripperForDance) AND IS_STRIPPER_BOOTYCALL_STRIPPER(namedStripper[iCurrentStripperForDance].stripperID)
						IF IS_LAPDANCE_END_ANIMATION_OVER(iLapDanceScene, FALSE, 0.15) // so the don't say the line right when the dance starts
							SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_WANTS_HOME)
							SET_BITMASK_AS_ENUM(namedStripper[iCurrentStripperForDance].stripperFlags, STRIPPER_FLAG_SAID_WANT_HOME)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE DANCE_CAUGHT_TOUCHING
			
			CLEAR_ALL_STRIP_CLUB_HELP()
			HAS_LAPDANCE_ENDED(TRUE) //loop dance animation so stripper doesn't freeze
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_AMBIENT_SPEECH_PLAYING(clubStaffPed[STAFF_BOUNCER_PATROL])
				RESTART_TIMER_NOW(tControl)
				STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, 0.0)
				stripClubStage = STAGE_THROWOUT
			ENDIF
		BREAK
		
		CASE DANCE_EXIT
			IF DOES_CAM_EXIST(fpStripCam.theCam)
				vLapdanceCamPos = GET_CAM_COORD(fpStripCam.theCam)
			ENDIF
			
			//you are canceling the dance, use the end camera
			IF OKAY_TO_EXIT_LAPDANCE_ANIMATION()
			OR nextDanceStage = DANCE_CANCELED
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_DELAY_ASK_TO_LEAVE)
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_DANCE_QUIT", SPEECH_PARAMS_FORCE_NORMAL)
				ENDIF
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_DELAY_ASK_TO_LEAVE)
			
				IF PLAY_LAPDANCE_END_ANIMATION(nextDanceStage = DANCE_CANCELED)
					danceStage = nextDanceStage
				ENDIF
			ENDIF
		BREAK
		
		CASE DANCE_CANCELED
			DETACH_CAM(stripclubCam)
			CLEAR_LAPDANCE_HELP()
				
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_LAPDANCE_END_ANIMATION_OVER(iApproachDanceScene, FALSE, 0.99)
				RESTART_TIMER_NOW(tControl)
				SET_STRIPCLUB_HAT()
				danceStage = DANCE_OUTRO_CUTSCENE	
				candidateID = NO_CANDIDATE_ID
				
				#IF MP_STRIPCLUB
					STRIPCLUB_END_SYNC_SCENE(iDanceOfferScene)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				#ENDIF
			ENDIF
		BREAK
		
		CASE DANCE_FINISHED
//			IF NOT IS_PAUSE_MENU_ACTIVE()
//				HANDLE_CAMERA_INPUT()
//			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iApproachDanceScene))
				fEndingPhase = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iApproachDanceScene))
			ELSE
				fEndingPhase = 0.0
			ENDIF
			IF DOES_CAM_EXIST(fpStripCam.theCam)
				DETACH_CAM(fpStripCam.theCam)

				vDesiredCamPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VIP_SEAT_END_CAMERA_OFFSET(iCurrentLapDanceSeat)
				fDesiredHeading = GET_HEADING_BETWEEN_VECTORS_2D(fpStripCam.vInitCamPos, GET_ENTITY_COORDS(namedStripper[iCurrentStripperForDance].ped))
				
				//move root camera pos slowly to the position I want
				vLapdanceCamPos = LERP_VECTOR(vLapdanceCamPos, vDesiredCamPos, 0.1)
				
				fpStripCam.vInitCamRot += fpStripCam.vCamRotOffsetCurrent
				fpStripCam.vCamRotOffsetCurrent = <<0,0,0>>
				
				PRINTLN("heading ", fpStripCam.vInitCamRot.z, " desire ", fDesiredHeading)
				
				fpStripCam.vInitCamPos = vLapdanceCamPos + fEndingPhase*<<-0.1052, 0.0107, 0.0660>>
				fpStripCam.vInitCamRot.x = LERP_FLOAT(fpStripCam.vInitCamRot.x, 15.0 * fEndingPhase, 0.1)
				IF ABSF(fpStripCam.vInitCamRot.z - fDesiredHeading) > 180.0
					fDesiredHeading -= 360
				ENDIF
				
				fpStripCam.vInitCamRot.z  = LERP_FLOAT(fpStripCam.vInitCamRot.z, fDesiredHeading, 0.1)
				SET_CAM_PARAMS(	fpStripCam.theCam,
								fpStripCam.vInitCamPos,
								fpStripCam.vInitCamRot,
								fpStripCam.fCamFOVCurrent)
			ENDIF
			CLEAR_ALL_STRIP_CLUB_HELP()
			
			//PRINTLN("Dance finsiehd")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_LAPDANCE_END_ANIMATION_OVER(iApproachDanceScene, FALSE, 1.0)
			OR TIMER_DO_ONCE_WHEN_READY(tControl, 9.0)
				
				PLAY_LAPDANCE_OFFERS_ANIMATION()
				RESTART_TIMER_NOW(tControl)
				danceStage = DANCE_OFFERS
				candidateID = NO_CANDIDATE_ID
				
				#IF MP_STRIPCLUB
					STRIPCLUB_END_SYNC_SCENE(iDanceOfferScene)
				#ENDIF
			ENDIF
		BREAK
		
		CASE DANCE_OFFERS
			CLEAR_ALL_STRIP_CLUB_HELP()
			CLEAR_PRINTS()
			TouchingStripperState = TSS_NONE
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				HANDLE_CAMERA_INPUT(TRUE)
			ENDIF
			
			IF DOES_CAM_EXIST(stripclubCamBouncer)
				IF IS_CAM_ACTIVE(stripclubCamBouncer)
					SET_CAM_ACTIVE(stripclubCamBouncer, FALSE)
					DESTROY_CAM(stripclubCamBouncer)
				ENDIF
			ENDIF
			
			IF IS_LAPDANCE_END_ANIMATION_OVER(iApproachDanceScene, TRUE, 1.0)
				PRINTLN("Loop offer anim")
			ENDIF			
			RESTART_TIMER_NOW(tInteraction)
			
			IF IS_STRIPCLUB_MP()
				//MP don't allow the player to take the stripper hoem
				MANAGE_END_OF_DANCE_HELP(FALSE)
				
				INC_SCLUB_STAT_NUM_LAPDANCES()
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_LAPDANCES, 1)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT , 1)
				
				SET_STRIPPER_ANOTHER_DANCE_SPEECH()
				danceStage = DANCE_OFFER_ANOTHER
			ELSE
				IF DO_TAKE_HOME_TEST(iCurrentStripperForDance)
				AND (IS_STRIPPER_BOOTYCALL_STRIPPER(namedStripper[iCurrentStripperForDance].stripperID)
				OR NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_REJECTED_HOME))
				AND NOT DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
					
					IF NOT IS_STRIPPER_BOOTYCALL_STRIPPER(namedStripper[iCurrentStripperForDance].stripperID)
						//give options but you will be turned down
						INC_SCLUB_STAT_NUM_LAPDANCES()
						SET_STRIPPER_ANOTHER_DANCE_SPEECH()
						danceStage = DANCE_OFFER_HOME
					ELSE
						
						//END lapdance mission to start the take home mission
						IF candidateIDLapdance != NO_CANDIDATE_ID
							Mission_Over(candidateIDLapdance)
						ENDIF
						
						SWITCH Request_Mission_Launch(candidateID, MCTID_CONTACT_POINT, MISSION_TYPE_MINIGAME_FRIENDS)
							CASE MCRET_ACCEPTED
								INC_SCLUB_STAT_NUM_LAPDANCES()
								// offers to take you home
								SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_ASK_HOME)
								danceStage = DANCE_OFFER_HOME
							BREAK
							CASE MCRET_DENIED
								// for some reason the mission can't laucnh, don't offer take home			
								INC_SCLUB_STAT_NUM_LAPDANCES()
								danceStage = DANCE_OFFER_ANOTHER
							BREAK
							CASE MCRET_PROCESSING
								DEBUG_MESSAGE("Requesting ability to take the girl home")
							BREAK
						ENDSWITCH
					ENDIF
					
				ELSE
					// doesn't like you enough to come home
					INC_SCLUB_STAT_NUM_LAPDANCES()
					SET_STRIPPER_ANOTHER_DANCE_SPEECH()
					danceStage = DANCE_OFFER_ANOTHER
				ENDIF
						
			ENDIF
			
		BREAK
		
		CASE DANCE_OFFER_ANOTHER
			IF NOT IS_PAUSE_MENU_ACTIVE()
				HANDLE_CAMERA_INPUT(TRUE)
			ENDIF
			
			IF candidateID != NO_CANDIDATE_ID
				Mission_Over(candidateID)
			ENDIF
			
			MANAGE_END_OF_DANCE_HELP(FALSE)
			
			#IF NOT MP_STRIPCLUB
				REQUEST_MODEL(GET_STRIPPER_MODEL(0, TRUE))
				REQUEST_MODEL(GET_STRIPPER_MODEL(1, TRUE))
			#ENDIF
			
			IF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()
				//IF GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
				IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
					STRIP_CLUB_SPEND_CASH(DANCE_COST, TRUE, LAP_DANCE)
					ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_DANCE, TRUE)
					ADVANCE_NEXT_DANCE_INPUT_TIME()
					PLAY_LAPDANCE_ACCEPT_ANIMATION()
				ELSE
					CLEAR_PRINTS()
					
					PRINT_STRIPCLUB_HELP("SCLUB_NO_MONEY")
					STRIP_CLUB_BUY_CASH_ALERT()
					PLAY_LAPDANCE_DECLINE_ANIMATION()
					danceStage = DANCE_TURN_DOWN
					BREAK
				ENDIF
				
				// accept
				#IF NOT MP_STRIPCLUB
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_2nd_DANCE_ACCEPT", SPEECH_PARAMS_FORCE_NORMAL)
				#ENDIF
				
				IF NOT HAS_DISPLAYED_SELECT_HELP()
					SET_DISPLAYED_SELECT_HELP(TRUE)
				ENDIF
				
				danceStage = DANCE_GET_ANOTHER
			ELIF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
			AND CAN_GET_MULTIPLE_DANCERS_IN_CLUB()
			//AND GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
			AND CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
			AND iSecondaryStripper < 0
			AND HAS_MODEL_LOADED(CSB_STRIPPER_01) AND HAS_MODEL_LOADED(CSB_STRIPPER_02)
			
				iSecondaryStripper = GET_AVAILABLE_SECONDARY_DANCER(iCurrentStripperForDance)
				INIT_GET_SECONDARY_STRIPPER_CUTSCENE()
				danceStage = DANCE_GET_SECOND_DANCER
				
			ELIF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
			OR TIMER_DO_ONCE_WHEN_READY(tInteraction, 25.0)
				CLEAR_ALL_STRIP_CLUB_HELP()
				
				#IF NOT MP_STRIPCLUB
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_2nd_DANCE_DECLINE", SPEECH_PARAMS_FORCE_NORMAL)
				#ENDIF
				
				#IF MP_STRIPCLUB
					INT iProperty 
					iProperty = GET_OWNED_PROPERTY(0)
					PRINTLN("Check if player should get stripper number, house index is ", iProperty)
					IF iProperty <= 0
						iProperty = GET_OWNED_PROPERTY(YACHT_PROPERTY_OWNED_SLOT)
					ENDIF
					IF iProperty > 0
						BOOL bAddStripper1, bAddStripper2
					
						bAddStripper1 = FALSE
						bAddStripper2 = FALSE
						
						bAddStripper1 = ADD_STRIPPER_NUMBER_POST_LAPDANCE(iCurrentStripperForDance, TRUE)
						IF iSecondaryStripper >= 0
							bAddStripper2 = ADD_STRIPPER_NUMBER_POST_LAPDANCE(iSecondaryStripper, FALSE)
						ENDIF
						
						IF bAddStripper1 AND bAddStripper2
							PRINT_STRIPCLUB_HELP("SCLUB_PHON_HEL2")
						ENDIF
					ELIF DO_TAKE_HOME_TEST(iCurrentStripperForDance) OR DO_TAKE_HOME_TEST(iSecondaryStripper)
						PRINT_STRIPCLUB_HELP("SCLUB_NO_HOUSE")
					ENDIF
					
					bForceLapdanceHeadInvisible = TRUE
				#ENDIF
				
				danceStage = DANCE_TURN_DOWN
				PLAY_LAPDANCE_DECLINE_ANIMATION()
			ENDIF
		BREAK
		
		CASE DANCE_GET_SECOND_DANCER
			IF NOT IS_PAUSE_MENU_ACTIVE()
				HANDLE_CAMERA_INPUT()
			ENDIF
			
			IF PLAY_SECONDARY_STRIPPER_CUTSCENE()
				SETUP_STRIPCLUB_CAM(stripclubCam, currentLapdanceCamera, <<0,0,0>>, <<0,0,0>>)
				bForceLapdanceHeadInvisible = TRUE
				danceStage = DANCE_SET_STRIPPER
			ENDIF
			
		BREAK
		
		CASE DANCE_GET_ANOTHER
//			IF NOT IS_PAUSE_MENU_ACTIVE()
//				HANDLE_CAMERA_INPUT()
//			ENDIF
			PRINTLN("DANCE_GET_ANOTHER")
				
			IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDanceOfferScene))
				PRINTLN("Scene is running")
				fEndingPhase = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDanceOfferScene))
			ELSE
				fEndingPhase = 0.0
			ENDIF
			IF DOES_CAM_EXIST(fpStripCam.theCam)
				PRINTLN("fp Camera is active, ending phase is ", fEndingPhase)
				DETACH_CAM(fpStripCam.theCam)
				
				fDesiredHeading = GET_HEADING_BETWEEN_VECTORS_2D(fpStripCam.vInitCamPos, GET_ENTITY_COORDS(namedStripper[iCurrentStripperForDance].ped))
				
				fpStripCam.vInitCamRot += fpStripCam.vCamRotOffsetCurrent
				fpStripCam.vCamRotOffsetCurrent = <<0,0,0>>
				
				fpStripCam.vInitCamPos = vLapdanceCamPos + (1.0 - fEndingPhase)*<<-0.1052, 0.0107, 0.0660>>
				fpStripCam.vInitCamRot.x = LERP_FLOAT(fpStripCam.vInitCamRot.x, 15.0 * (1.0 - fEndingPhase), 0.5)
				IF ABSF(fpStripCam.vInitCamRot.z - fDesiredHeading) > 180.0
					fDesiredHeading -= 360
				ENDIF
				
				fpStripCam.vInitCamRot.z  = LERP_FLOAT(fpStripCam.vInitCamRot.z, fDesiredHeading, 0.5)
				SET_CAM_PARAMS(	fpStripCam.theCam,
								fpStripCam.vInitCamPos,
								fpStripCam.vInitCamRot,
								fpStripCam.fCamFOVCurrent)
			ENDIF
			
			REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(0, iSecondaryStripper >= 0))
			REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(1, iSecondaryStripper >= 0))
			
			REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(TRUE,  FALSE))
			REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, FALSE))
			REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, TRUE))
			
			CLEAR_ALL_STRIP_CLUB_HELP()
			IF IS_LAPDANCE_END_ANIMATION_OVER(iDanceOfferScene, FALSE, 1.0)
				IF DOES_CAM_EXIST(fpStripCam.theCam)
					ATTACH_CAM_TO_PED_BONE(fpStripCam.theCam ,PLAYER_PED_ID(),BONETAG_HEAD, <<0.0, -0.015, 0.040>>)
				ENDIF
			
				danceStage = DANCE_SET_STRIPPER
			ENDIF
		BREAK
		
		CASE DANCE_TURN_DOWN
			// decline
//			IF NOT IS_PAUSE_MENU_ACTIVE()
//				HANDLE_CAMERA_INPUT()
//			ENDIF
			CLEAR_LAPDANCE_HELP()
			#IF MP_STRIPCLUB
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				iStripclubStopRenderCamAt = GET_GAME_TIMER() + 300
			#ENDIF	
			
			IF IS_LAPDANCE_END_ANIMATION_OVER(iDanceOfferScene, FALSE, GET_END_ANIM_TIME_FOR_SEAT(iCurrentLapDanceSeat))
			#IF NOT MP_STRIPCLUB OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() #ENDIF
				CLEANUP_STRIP_CLUB_CAM(stripclubCam)
				RESTART_TIMER_NOW(tControl)
				SET_STRIPCLUB_HAT()
				danceStage = DANCE_OUTRO_CUTSCENE
				
				#IF MP_STRIPCLUB
					STRIPCLUB_PLAY_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), GET_VIP_OUTRO_CUTSCENE_END_COORD(SWR_VIP_OUTRO), PEDMOVE_WALK)
				#ENDIF
				
				#IF NOT MP_STRIPCLUB
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
					CPRINTLN(DEBUG_SCLUB, "DANCE_TURN_DOWN with cutscene pressed")
					SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_OUTRO_SKIPPED)
					danceStage = DANCE_AFTER
				ENDIF
				#ENDIF
			
			ENDIF
		BREAK
		
		CASE DANCE_OFFER_HOME
			HANDLE_CAMERA_INPUT(TRUE)
			
			PRINTLN("Dance Offer home")
			MANAGE_END_OF_DANCE_HELP(TRUE)
			
			#IF NOT MP_STRIPCLUB
				REQUEST_MODEL(GET_STRIPPER_MODEL(0, TRUE))
				REQUEST_MODEL(GET_STRIPPER_MODEL(1, TRUE))
			#ENDIF
			
			IF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
				
				IF IS_STRIPPER_BOOTYCALL_STRIPPER(namedStripper[iCurrentStripperForDance].stripperID)
					CLEAR_ALL_STRIP_CLUB_HELP()
					
					// accept
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_INVITE_HOME_ACCEPT", SPEECH_PARAMS_FORCE_NORMAL)
					
					RESTART_TIMER_NOW(tControl)
					SET_STRIPCLUB_HAT()
					danceStage = DANCE_ACCEPTED_HOME
				ELSE
					IF candidateID != NO_CANDIDATE_ID
						Mission_Over(candidateID)
					ENDIF
					SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_REJECTED_HOME)
					SET_STRIPPER_SPEECH_TARGET(iCurrentStripperForDance, STRSPEECH_WONT_DATE)
					PRINT_STRIPCLUB_HELP_WITH_STRIPPER_NAMES("SCLUB_WONT_HOME", namedStripper[iCurrentStripperForDance].stripperID, FALSE, 1)
					danceStage = DANCE_OFFER_ANOTHER
				ENDIF
			ELIF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
			AND CAN_GET_MULTIPLE_DANCERS_IN_CLUB()
			//AND GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
			AND CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
			AND iSecondaryStripper < 0
			AND HAS_MODEL_LOADED(CSB_STRIPPER_01) AND HAS_MODEL_LOADED(CSB_STRIPPER_02)
				IF candidateID != NO_CANDIDATE_ID
					Mission_Over(candidateID)
				ENDIF
				
				iSecondaryStripper = GET_AVAILABLE_SECONDARY_DANCER(iCurrentStripperForDance)
				INIT_GET_SECONDARY_STRIPPER_CUTSCENE()
				danceStage = DANCE_GET_SECOND_DANCER
				
			ELIF STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
			OR TIMER_DO_ONCE_WHEN_READY(tInteraction, 25.0)
					IF candidateID != NO_CANDIDATE_ID
						Mission_Over(candidateID)
					ENDIF
					
					CLEAR_ALL_STRIP_CLUB_HELP()
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_2nd_DANCE_DECLINE", SPEECH_PARAMS_FORCE_NORMAL)
					
//					ADD_STRIPPER_NUMBER_POST_LAPDANCE(iCurrentStripperForDance, TRUE)
					
					danceStage = DANCE_TURN_DOWN
					PLAY_LAPDANCE_DECLINE_ANIMATION()
					
			ELIF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()		
				IF candidateID != NO_CANDIDATE_ID
					Mission_Over(candidateID)
				ENDIF 
				
				//IF GET_STRIP_CLUB_PLAYER_CASH(TRUE) >= DANCE_COST
				IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(DANCE_COST, TRUE)
					STRIP_CLUB_SPEND_CASH(DANCE_COST, TRUE, LAP_DANCE)
					ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_DANCE, TRUE)
					ADVANCE_NEXT_DANCE_INPUT_TIME()
					PLAY_LAPDANCE_ACCEPT_ANIMATION()				
				ELSE
					CLEAR_PRINTS()
					STRIP_CLUB_BUY_CASH_ALERT()
					PRINT_STRIPCLUB_HELP("SCLUB_NO_MONEY")
					PLAY_LAPDANCE_DECLINE_ANIMATION()
					danceStage = DANCE_TURN_DOWN
					BREAK
				ENDIF
				
				IF IS_STRIPPER_BOOTYCALL_STRIPPER(namedStripper[iCurrentStripperForDance].stripperID)
					sFixedup = STRIPCLUB_FIXUP_PLAYER_LABEL("SC_HOMED")
					CREATE_CONVERSATION(stripClubConversation, "SCAUD", sFixedup, GET_STRIPCLUB_SPEECH_PRIORITY())
				ELSE
					#IF NOT MP_STRIPCLUB
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "STRIP_2nd_DANCE_ACCEPT", SPEECH_PARAMS_FORCE_NORMAL)
					#ENDIF
				ENDIF
				
				IF NOT HAS_DISPLAYED_SELECT_HELP()
					SET_DISPLAYED_SELECT_HELP(TRUE)
				ENDIF
				
				danceStage = DANCE_GET_ANOTHER
			ENDIF
		BREAK
		
		CASE DANCE_ACCEPTED_HOME
			CLEAR_LAPDANCE_HELP()
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CLEANUP_STRIP_CLUB_CAM(stripCamAnim)
				
				IF PLAY_OUTRO_CUTSCENE(FALSE)
					
					LAUNCH_TAKE_HOME_SCRIPT()
					RESTART_TIMER_NOW(tControl)
					danceStage = DANCE_AFTER_TAKE_HOME
				ENDIF
			ENDIF
		BREAK
		
		CASE DANCE_OUTRO_CUTSCENE
			CLEANUP_STRIP_CLUB_CAM(stripCamAnim)
			IF PLAY_OUTRO_CUTSCENE()
				RESTART_TIMER_NOW(tControl)
				danceStage = DANCE_AFTER
			ENDIF
		BREAK
		
		CASE DANCE_AFTER
		CASE DANCE_AFTER_TAKE_HOME
			
			IF candidateIDLapdance != NO_CANDIDATE_ID
				Mission_Over(candidateIDLapdance)
			ENDIF
			
			IF  IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_OUTRO_SKIPPED)
				CPRINTLN(DEBUG_SCLUB, "Making stripper low quality")
				MAKE_STRIPPER_PED_LOW_QUALITY(iCurrentStripperForDance)
				MAKE_STRIPPER_PED_LOW_QUALITY(iSecondaryStripper)
			ENDIF
			
			IF  IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_LAPDANCE_STREAMED)
				REMOVE_LAPDANCE_ANIMS()
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_LAPDANCE_STREAMED)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				#IF NOT MP_STRIPCLUB
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0) //-45
				#ENDIF
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				//CLEAR_ROOM_FOR_GAME_VIEWPORT()
				
				BOOL bAllowFlash
				bAllowFlash = TRUE
				
				IF IS_STRIPCLUB_MP() OR IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_OUTRO_SKIPPED)
					bAllowFlash = FALSE
				ENDIF
				
				RESET_STRIP_CLUB_CAMS_TO_GAMEPLAY(FALSE, 0, FALSE, bAllowFlash)
			ENDIF
			
			CLEANUP_STRIP_CLUB_CAM(stripCamAnim)
			IF DOES_CAM_EXIST(stripclubCamBouncer)
				DESTROY_CAM(stripclubCamBouncer)
			ENDIF
			
			DISABLE_CELLPHONE(FALSE)
//			SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
			
			#IF MP_STRIPCLUB
				ENABLE_ALL_MP_HUD()
				SET_DPADDOWN_ACTIVE(TRUE)
				STRIPCLUB_END_SYNC_SCENE(iLapDanceScene)
				STRIPCLUB_END_SYNC_SCENE(iApproachDanceScene)
				STRIPCLUB_END_SYNC_SCENE(iDanceOfferScene)
				END_STRIPCLUB_MP_LAPDANCE_CUTSCENE()
				
				SET_ENTITY_DYNAMIC(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				
									
				SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iCurrentStripperForDance].ped, namedStripper[iCurrentStripperForDance].stripperID)
				IF iSecondaryStripper >= 0
					SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iSecondaryStripper].ped, namedStripper[iSecondaryStripper].stripperID)
				ENDIF
			#ENDIF
			
			#IF NOT MP_STRIPCLUB
				
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_OUTRO_SKIPPED)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_VIP_OUTRO_SKIPPED)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_VIP_OUTRO_CUTSCENE_END_COORD(SWR_VIP_OUTRO))
					UNFREEZE_PLAYER_IN_CLUB() //frozen from lapdance
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_DYNAMIC(PLAYER_PED_ID(), TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					
					IF danceStage !=  DANCE_AFTER_TAKE_HOME
						CLEAR_PED_TASKS_IMMEDIATELY(namedStripper[iCurrentStripperForDance].ped)
						SET_ENTITY_COORDS(namedStripper[iCurrentStripperForDance].ped, <<119.05, -1294.07, 28.28>>)
						
						IF iSecondaryStripper >= 0
							//put second stripper at bottom of stairs
							CLEAR_PED_TASKS_IMMEDIATELY(namedStripper[iSecondaryStripper].ped)
							SET_ENTITY_COORDS(namedStripper[iSecondaryStripper].ped, GET_VIP_OUTRO_CUTSCENE_END_COORD(SWR_FRIEND_ENTER))
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
			
			CLEAR_ALL_STRIP_CLUB_HELP()
			IF NOT IS_KEEP_HELP_AND_SPEECH_FROM_DANCE_SET() AND iCurrentStripperForDance > -1
				namedStripper[iCurrentStripperForDance].speechTarget = STRSPEECH_NONE
			ENDIF
			
			// set to like more
			ADJUST_CLUB_REPUTATION(2)
			
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_FOCUS_ON_BOUNCER)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_FOCUS_ON_BOUNCER)
			ENDIF
			
			IF danceStage = DANCE_AFTER_TAKE_HOME //allow folks to wander around back there
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_DISABLED_DOOR_NAVMESH)
					//DISABLE_NAVMESH_IN_AREA(<<114.13, -1296.64, 27.42>>, <<118.13, -1292.64, 31.42>>, FALSE)
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_DISABLED_DOOR_NAVMESH)
				ENDIF
			ENDIF
			
			// reset all strippers
			INT i
			REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
				IF iCurrentStripperForDance = i
				OR iSecondaryStripper = i
					IF NOT IS_ENTITY_DEAD(namedStripper[i].ped)
//						CLEAR_PED_TASKS_IMMEDIATELY(namedStripper[i].ped)
						RESET_STRIPPER_TO_WANDER(i)
					ENDIF
				ELIF NOT IS_STRIPCLUB_MP()
					namedStripper[i].iNextApproachTime = GET_GAME_TIMER() + 30000
				ENDIF
			ENDREPEAT
			
			enumBouncerPatrolState = BOUNCER_PATROL_WALK_TO_COORD
			
			TouchingStripperState = TSS_NONE
			iCurrentLapDanceSeat = -1
			iCurrentStripperForDance = -1
			iCurrentStripperInteraction = -1
				
			// set next stage as appropriate
			IF danceStage <> DANCE_AFTER_TAKE_HOME
				stripClubStage = STAGE_WANDER_CLUB
			ELSE
				stripClubStage = STAGE_GO_HOME_WITH_STRIPPER
			ENDIF
			
			#IF NOT MP_STRIPCLUB
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(0, TRUE))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(1, TRUE))	
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(0, FALSE))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(1, FALSE))
			
				REQUEST_STRIPPER_IDLES()
			#ENDIF
			
			SET_SHOW_LIKE_HUD(FALSE)
			ENABLE_KILL_YOURSELF_OPTION()
			ENABLE_INTERACTION_MENU()
			ENABLE_SELECTOR()
			SET_STRIPCLUB_GLOBAL_GETTING_LAPDANCE(FALSE)
		BREAK
	ENDSWITCH
	
	IF NOT DOES_CAM_EXIST(GET_RENDERING_CAM())
		CPRINTLN(DEBUG_SCLUB, "Rendering cam doesn't exist?") //when switching camera rendering cam is -1 for a frame, what the fuck!
	ENDIF
	
	IF (((GET_RENDERING_CAM() = fpStripCam.theCam OR danceStage = DANCE_INIT OR danceStage = DANCE_SET_PLAYER
	OR (currentLapdanceCamera = STRCLUB_CAM_DANCE_FIRST_PERSON AND NOT DOES_CAM_EXIST(GET_RENDERING_CAM())))
	AND NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_SYNC_CAMERA_THIS_FRAME))
	AND danceStage != DANCE_TURN_DOWN AND danceStage != DANCE_OUTRO_CUTSCENE  AND danceStage != DANCE_AFTER)
	OR bForceLapdanceHeadInvisible
		bForceLapdanceHeadInvisible = FALSE
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_MakeHeadInvisble, TRUE)
	ELSE

	ENDIF
	
//	IF DOES_CAM_EXIST(fpStripCam.theCam)
//		VECTOR vasfasdf = GET_CAM_COORD(fpStripCam.theCam)
//		VECTOR adfasdf = GET_ENTITY_COORDS(PLAYER_PED_ID())
//		VECTOR diff = vasfasdf-adfasdf
//		PRINTLN("First person cam pos ", vasfasdf)
//		PRINTLN("Player coord ", adfasdf)
//		PRINTLN("diff ", diff)
//	ENDIF

//	CPRINTLN(DEBUG_SCLUB, "Dance Stage ", danceStage)
	IF (danceStage < DANCE_TURN_DOWN OR danceStage = DANCE_OFFER_HOME) AND danceStage != DANCE_CANCELED
		DRAW_LIKE_HUD()
	ENDIF
ENDPROC
