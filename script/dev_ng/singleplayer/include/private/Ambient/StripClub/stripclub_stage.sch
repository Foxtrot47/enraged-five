USING "stripclub_helpers.sch"

// is a pole dancer's current anim stage such that she can blend out without looking dodgy?
FUNC BOOL CAN_POLE_DANCER_LEAVE_ANIM()
	
	//not doing pole dance, return true
	RETURN TRUE
ENDFUNC

PROC MANAGE_STRIPPER_POLEDANCE_FLIRTING(INT iStripper, INT iNextFlirtTime = 5000)
	IF railStage = RAIL_WATCHING
		IF iStripper >= 0 AND iRailFlirtTime < GET_GAME_TIMER()
		
			iRailFlirtTime = GET_GAME_TIMER() + iNextFlirtTime
			CPRINTLN(DEBUG_SCLUB, "RAIL FLIRT ", iStripper)
			SET_STRIPPER_SPEECH_TARGET(iStripper, STRSPEECH_RAIL_FLIRT)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_ANY_STRIPPER_ENTERING_STAGE()
	INT i
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF NOT IS_PED_INJURED(namedStripper[i].ped)
			IF GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_GOTO_STAGE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_STRIPPER_EXITING_STAGE()
	INT i
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF NOT IS_PED_INJURED(namedStripper[i].ped)
			IF GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_EXIT_STAGE
			OR GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_RETURN_TO_STAGE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

//Get Ped who will get a dance from the poledancer
FUNC PED_INDEX GET_PED_FOR_DANCE()
	
	#IF MP_STRIPCLUB
		IF NATIVE_TO_INT(serverBD.serverPedForPrivateDance) < 0
			RETURN NULL
		ENDIF
	
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.serverPedForPrivateDance)
			RETURN NULL
		ENDIF
		
		RETURN GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(serverBD.serverPedForPrivateDance))
	#ENDIF
	
	RETURN pedForPrivateDance
ENDFUNC

//Set the ped who will get a pole dance
PROC SET_DANCE_FOR_PED(PED_INDEX pedForPoledance, INT iParticipantIndex = -1)
	UNUSED_PARAMETER(iParticipantIndex)

	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF iParticipantIndex > -1
				CPRINTLN(DEBUG_SCLUB, "Saving particpant for dance ", iParticipantIndex)
				serverBD.serverPedForPrivateDance = INT_TO_PARTICIPANTINDEX(iParticipantIndex)
			ENDIF
		ENDIF
	#ENDIF
	
	pedForPrivateDance = pedForPoledance
ENDPROC

PROC CLEAR_DANCE_FOR_PED()

	CPRINTLN(DEBUG_SCLUB,"Clearing dance for ped")
//	DEBUG_PRINTCALLSTACK()
	pedForPrivateDance = NULL
	
	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.bLockFindPlayerToGiveRailDance = FALSE
			serverBD.serverPedForPrivateDance = INT_TO_NATIVE(PARTICIPANT_INDEX, -1)
		ENDIF
	#ENDIF
ENDPROC

FUNC FLOAT GET_WALK_FROM_POLE_TO_PLAYER_PHASE(INT i)

	IF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0), GET_POLE_TO_RAIL_ANIM_NAME(0))
		RETURN GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0), GET_POLE_TO_RAIL_ANIM_NAME(0))
	ELIF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1), GET_POLE_TO_RAIL_ANIM_NAME(1))
		RETURN GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1), GET_POLE_TO_RAIL_ANIM_NAME(1))
	ELIF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2), GET_POLE_TO_RAIL_ANIM_NAME(2))
		RETURN GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2), GET_POLE_TO_RAIL_ANIM_NAME(2))
	ENDIF

	RETURN 0.0
ENDFUNC

PROC PLAY_WALK_TO_PLAYER_FROM_POLE_ANIM(INT i)
	INT iRailIndex
	IF GET_HEADING_FROM_ENTITIES_LA(GET_PED_FOR_DANCE(), namedStripper[i].ped) < 60.0
		iRailIndex = 0
	ELIF GET_HEADING_FROM_ENTITIES_LA(GET_PED_FOR_DANCE(), namedStripper[i].ped) > 180.0
		iRailIndex = 2
	ELSE
		iRailIndex = 1
	ENDIF
	
	TASK_PLAY_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(iRailIndex), GET_POLE_TO_RAIL_ANIM_NAME(iRailIndex), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
ENDPROC

FUNC INT GET_POLE_DANCER_ON_STAGE()
	INT i
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF NOT IS_PED_INJURED(namedStripper[i].ped)
			IF GET_POLEDANCER_STATE(i) > POLEDANCER_STATE_GOTO_STAGE_INTRO AND GET_POLEDANCER_STATE(i) < POLEDANCER_STATE_EXIT_STAGE
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

//Tells the stripper to move the the starting location for doing the stage activites.
PROC SET_GENERIC_STRIPPER_GO_STAGE_START(INT i, VECTOR vStageStartPostion)
	CPRINTLN(DEBUG_SCLUB,"Tasking stripper ", i, " To go to stage")
	TASK_FOLLOW_NAV_MESH_TO_COORD(namedStripper[i].ped, vStageStartPostion, PEDMOVE_WALK, -1, 0.15, ENAV_STOP_EXACTLY)
ENDPROC

//Tells the stripper to move the the starting location for doing the stage activites.
PROC SET_GENERIC_STRIPPER_GO_WAITING_ROOM(INT i, VECTOR vWaitingPostion, FLOAT fWaitingRot)

	IF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
	AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != WAITING_TO_START_TASK
		CPRINTLN(DEBUG_SCLUB,"Tasking stripper ", i, " To go to waiting room ", vWaitingPostion)
		TASK_FOLLOW_NAV_MESH_TO_COORD(namedStripper[i].ped, vWaitingPostion, PEDMOVE_WALK, -1, 0.15, ENAV_STOP_EXACTLY, fWaitingRot)
	ENDIF
ENDPROC

#IF MP_STRIPCLUB
PROC PRE_CREATE_POLE_DANCE_SCENE(INT iNextSceneIndex, INT iStripperIndex, INT &iSceneID, VECTOR vSceneLoc, FLOAT fSceneHeading, STRING sAnimDictName, STRING sAnimName, FLOAT fBlendIn = INSTANT_BLEND_IN, FLOAT fBlendOut = INSTANT_BLEND_OUT)

	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT //lets make it so only one machine is creating the sync scenes
	ENDIF
	
	REQUEST_ANIM_DICT(sAnimDictName)
	IF HAS_ANIM_DICT_LOADED(sAnimDictName)
		IF iPoleDancerNextNetSceneID[iNextSceneIndex] < 0
			CPRINTLN(DEBUG_SCLUB,"Pre creating sync scene ", sAnimName, " for slot ", iNextSceneIndex)
			STRIPCLUB_CREATE_SYNC_SCENE(iSceneID, vSceneLoc, <<0,0, fSceneHeading>>, TRUE, TRUE, FALSE)
			IF serverBD.iPoleDancerNetSceneID[iStripperIndex] = iSceneID
				CPRINTLN(DEBUG_SCLUB, "the newly created anim scene is the same as the active one? ", iSceneID)
				STRIPCLUB_END_SYNC_SCENE(iSceneID) //stop scene
				EXIT //try to make scene again
			ENDIF
			
			STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iStripperIndex].ped, iSceneID, sAnimDictName, sAnimName, fBlendIn, fBlendOut, SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, FALSE, FALSE)
			
			//started new poledance scene, save it server side
			CPRINTLN(DEBUG_SCLUB, "Save next net sync scene index to ", iSceneID)
			iPoleDancerNextNetSceneID[iNextSceneIndex] = iSceneID
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SCLUB,"Poledance dict not loaded for pre create", sAnimDictName)
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL PLAY_POLE_DANCE_SCENE(INT iStripperIndex, INT &iSceneID, VECTOR vSceneLoc, FLOAT fSceneHeading, STRING sAnimDictName, STRING sAnimName, BOOL bFirstCall, FLOAT fEndTime = 1.0, FLOAT fBlendIn = INSTANT_BLEND_IN, FLOAT fBlendOut = INSTANT_BLEND_OUT, INT iNextSceneIndex = 0)
	
	UNUSED_PARAMETER(iNextSceneIndex)
	UNUSED_PARAMETER(vSceneLoc)
	UNUSED_PARAMETER(fSceneHeading)
	UNUSED_PARAMETER(sAnimName)
	UNUSED_PARAMETER(fBlendIn)
	UNUSED_PARAMETER(fBlendOut)
	
	#IF MP_STRIPCLUB
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		OR iPoleDancerNextNetSceneID[iNextSceneIndex] = -1 //need to pre create next dance
			RETURN FALSE //lets make it so only one machine is calling the sync scenes
		ENDIF
	#ENDIF
	
	INT iLocalSceneID = iSceneID
	
	#IF MP_STRIPCLUB
		iLocalSceneID = serverBD.iPoleDancerNetSceneID[iStripperIndex]
	#ENDIF
	
	iLocalSceneID = GET_SRTIPCLUB_LOCAL_SCENE_ID(iLocalSceneID)
	
	REQUEST_ANIM_DICT(sAnimDictName) //just incase
		
	IF HAS_ANIM_DICT_LOADED(sAnimDictName)
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID) OR bFirstCall
			CPRINTLN(DEBUG_SCLUB,"Start scene ", sAnimName, " for stripper ", iStripperIndex)
			CPRINTLN(DEBUG_SCLUB,"Blend in duration ", fBlendIn)
			#IF NOT MP_STRIPCLUB //sync scene is created before hand in mp
				STRIPCLUB_CREATE_SYNC_SCENE(iSceneID, vSceneLoc, <<0,0, fSceneHeading>>, TRUE, FALSE, FALSE)
				STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iStripperIndex].ped, iSceneID, sAnimDictName, sAnimName, fBlendIn, fBlendOut, SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, FALSE, FALSE)
			#ENDIF
			
			#IF MP_STRIPCLUB
				CPRINTLN(DEBUG_SCLUB,"iSceneID ", iSceneID)
				CPRINTLN(DEBUG_SCLUB,"iLocalSceneID ", iLocalSceneID)
				CPRINTLN(DEBUG_SCLUB,"serverBD.iPoleDancerNetSceneID[iStripperIndex] ", serverBD.iPoleDancerNetSceneID[iStripperIndex])
				CPRINTLN(DEBUG_SCLUB,"iPoleDancerNextNetSceneID[iNextSceneIndex] ", iPoleDancerNextNetSceneID[iNextSceneIndex])
					
				//get the id of the previously created sync scene
				IF bFirstCall OR serverBD.iPoleDancerNetSceneID[iStripperIndex] = -1
					CPRINTLN(DEBUG_SCLUB,"Sever sync scene at slot ", iNextSceneIndex , " is ", iPoleDancerNextNetSceneID[iNextSceneIndex])
					iSceneID = iPoleDancerNextNetSceneID[iNextSceneIndex]
					iPoleDancerNextNetSceneID[0] = -1 //clear next pole dance sync now that they are started
					iPoleDancerNextNetSceneID[1] = -1
				ELIF iLocalSceneID = -1
					CPRINTLN(DEBUG_SCLUB, "Can' get local sync scene from saved server one! Make new one")
					STRIPCLUB_CREATE_SYNC_SCENE(iSceneID, vSceneLoc, <<0,0, fSceneHeading>>, TRUE, TRUE, FALSE)
					STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iStripperIndex].ped, iSceneID, sAnimDictName, sAnimName, fBlendIn, fBlendOut, SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, FALSE, FALSE)
				ELSE
					CPRINTLN(DEBUG_SCLUB,"Sync scene stopped in the middle of running? Why?")
					RETURN FALSE
				ENDIF
			#ENDIF
			
			STRIPCLUB_START_SYNC_SCENE(iSceneID)
			
			#IF MP_STRIPCLUB
				//started new poledance scene, save it server side
				CPRINTLN(DEBUG_SCLUB,"Saving net scene id to ", iSceneID)
				serverBD.iPoleDancerNetSceneID[iStripperIndex] = iSceneID
			#ENDIF
			
		ELSE
			CPRINTLN(DEBUG_SCLUB,sAnimName ," Scene time ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID), " for stripper ", iStripperIndex, ". iLocalSceneID ", iLocalSceneID)
			
			#IF MP_STRIPCLUB
				CPRINTLN(DEBUG_SCLUB,"serverBD.iPoleDancerNetSceneID[iStripperIndex] ", serverBD.iPoleDancerNetSceneID[iStripperIndex])
				//Server is playing scene, make sure it's saved localy
				iSceneID = serverBD.iPoleDancerNetSceneID[iStripperIndex]
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.9 AND GET_POLEDANCER_STATE(iStripperIndex) = POLEDANCER_STATE_DANCE
					CPRINTLN(DEBUG_SCLUB,"Lock find player")
					serverBD.bLockFindPlayerToGiveRailDance = TRUE
				ENDIF
			#ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fEndTime
//				STRIPCLUB_END_SYNC_SCENE(iSceneID)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SCLUB,"Poledance dict not loaded ", sAnimDictName)
	ENDIF

	RETURN FALSE
ENDFUNC 

FUNC BOOL PLAY_PRIVATE_ANIM_AT_END_OF_ANIM(PED_INDEX pedStripper, STRING sAnimDict, STRING sAnimName, 
											STRING sNextAnimDict, STRING sNextAnimName, FLOAT fSwitchTime, ANIMATION_FLAGS animFlag, BOOL bForceNextAnim = FALSE)
	
	IF bForceNextAnim
		TASK_PLAY_ANIM(pedStripper, sNextAnimDict, sNextAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, animFlag, 0.0, FALSE, AIK_DISABLE_TORSO_IK | AIK_DISABLE_TORSO_REACT_IK | AIK_DISABLE_LEG_IK)
		RETURN TRUE
	ENDIF
	
	//play stage one of private dance at end of loop
	//CPRINTLN(DEBUG_SCLUB,"Is poledancer playeing anim ", sAnimName)
	IF IS_ENTITY_PLAYING_ANIM(pedStripper, sAnimDict, sAnimName)
		//CPRINTLN(DEBUG_SCLUB,"IAnims current time is ", GET_ENTITY_ANIM_CURRENT_TIME(pedStripper, sAnimDict, sAnimName), " target time is ", fSwitchTime)
		IF GET_ENTITY_ANIM_CURRENT_TIME(pedStripper, sAnimDict, sAnimName) >= fSwitchTime
			//CPRINTLN(DEBUG_SCLUB,"Time to play anim ", sNextAnimName)
			TASK_PLAY_ANIM(pedStripper, sNextAnimDict, sNextAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, animFlag, 0.0, FALSE, AIK_DISABLE_TORSO_IK | AIK_DISABLE_TORSO_REACT_IK | AIK_DISABLE_LEG_IK)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE //returns true when first anim is not playing
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PRIVATE_DANCE_OVER(INT iStriperIndex, BOOL bForceEnd = FALSE, BOOL bCheckNotOnGround = FALSE, BOOL bAllowExitOnIdle = FALSE )

	//at end of thrid dance always end
	IF IS_ENTITY_PLAYING_ANIM(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(3), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE3))		
		FLOAT fCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(3), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE3))
			
		IF bCheckNotOnGround
			// the animation played shows the stripper down on the ground between these times
			IF fCurrentTime < 0.03
			OR fCurrentTime > 0.88
				CPRINTLN(DEBUG_SCLUB, "IS_PRIVATE_DANCE_OVER: Leaning part - Returning true with fCurrentTime of ", fCurrentTime)
				RETURN TRUE
			ENDIF
		ELSE
			IF fCurrentTime >= 0.99
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF iMoneyGivenToPoleDancer > 0 AND NOT bForceEnd
		RETURN FALSE //gave money to dancer, she will keep dancing
	ENDIF

	//Played anim and never got money, end dance
	IF IS_ENTITY_PLAYING_ANIM(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(2), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE2))
		IF GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(2), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE2)) >= 0.99
		OR bCheckNotOnGround
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_PLAYING_ANIM(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(1), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE1))
		IF GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(1), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE1)) >= 0.99
		OR bCheckNotOnGround
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF railStage != RAIL_WATCHING OR bAllowExitOnIdle
		IF IS_ENTITY_PLAYING_ANIM(namedStripper[iStriperIndex].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_IDLE))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bForceEnd
		IF GET_WALK_FROM_POLE_TO_PLAYER_PHASE(iStriperIndex) > 0.98
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_POLEDANCE_OFFSET_START_DANCE(INT i)
	
	IF i = 0
		RETURN <<-0.119, 0.407, -0.89>>
	ELIF i = 1
		RETURN <<-8.54, -6.83, -0.092>>
	ELIF i = 2 
		RETURN <<-10.480, -3.470, -0.092>>
	ENDIF
	
	CPRINTLN(DEBUG_SCLUB,"Offset index from pole ", i)
	SCRIPT_ASSERT("Invalid poledance pole index")
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_POLEDANCE_OFFSET_DANCING(INT i)

	IF i = 0
		RETURN <<0.26, 0.12, -0.89>>
	ELIF i = 1
		RETURN <<-8.160, -7.110, -0.092>>
	ELIF i = 2 
		RETURN <<-10.100, -3.750, -0.092>>
	ENDIF
	
	CPRINTLN(DEBUG_SCLUB,"Offset index from pole ", i)
	SCRIPT_ASSERT("Invalid poledance pole index")
	RETURN <<0,0,0>>
ENDFUNC

// handle the generic strippers picking up cash
PROC HANDLE_POLEDANCER_STRIPPER_AI(INT i)
	VECTOR vWaitingPos
	FLOAT fWaitingRot
	INT iLocalSceneID
	FLOAT fWalkToRailAnimTime = 0.0
	
	
//	//don't use stage dancers in mp
//	#IF MP_STRIPCLUB
//		EXIT
//	#ENDIF
	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_DONT_UPDATE_STAGE)
		EXIT
	ENDIF
	
	BOOL bControlsStripper = TRUE
		
	IF NOT IS_PED_INJURED(namedStripper[i].ped)
		
		IF IS_STRIPCLUB_MP()
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(namedStripper[i].ped)
				bControlsStripper = FALSE
			ENDIF
		ENDIF

		IF IS_PED_RAGDOLL(namedStripper[i].ped) AND GET_POLEDANCER_STATE(i) > POLEDANCER_STATE_GOTO_STAGE_INTRO 
		AND GET_POLEDANCER_STATE(i) < POLEDANCER_STATE_GOTO_DRESSING_ROOM //player caused stripper to ragdoll while they were dancing
			SET_HOSTILE_FROM_MELEE(TRUE)
		ENDIF
		
		IF NOT SHOULD_GENERIC_STRIPPER_BE_DANCING(i) AND GET_POLEDANCER_STATE(i) <= POLEDANCER_STATE_DANCE_EXIT
			//dancing when you shouldn't be? Stop that
			SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_DRESSING_ROOM)
		ENDIF
		
		//IF POLEDANCER_STATE_GOTO_STAGE
		VECTOR vPoleC = <<112.6, -1287.03, 29.35>>
		VECTOR vStageStartPosition = vPoleC + <<-9.106, -5.399, -0.093>> //offset from pole(-10.586, -0.123, 0.0316) rotated by the stages z rotation(30)
		VECTOR vEnterStageStartPos = <<102.1886, -1297.0215, 28.7704>>
		
		//DRAW_DEBUG_SPHERE(vEnterStageStartPos, 1.0)
		
		//103.4941, -1292.4288, 29.2570, 28.500
		
		INT iPole = 0//(i + GET_NUMBER_OF_POLEDANCES())%3 //get a pole between 0-2 based on what stripper it is and how many dances it's been
		INT iDanceAnimIndex = (i + GET_NUMBER_OF_POLEDANCES())%3 + 1
		SWITCH GET_POLEDANCER_STATE(i)
			CASE POLEDANCER_STATE_WAIT_IN_DRESSING_ROOM
				//just chillin in the back room
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"In back room ", i)
//				ENDIF

				IF HAVE_STRIPPER_IDLES_LOADED() AND bControlsStripper
				AND NOT IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1))
					//play looping idle
					TASK_PLAY_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
			BREAK

			CASE POLEDANCER_STATE_GOTO_STAGE_INTRO
//				IF ENUM_TO_INT(stageState) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_GOTO_STAGE_INTRO ", i)
//				ENDIF
				
				IF bControlsStripper
					SET_PED_RESET_FLAG(namedStripper[i].ped, PRF_SearchForClosestDoor, TRUE)
				ENDIF
				#IF MP_STRIPCLUB
					PRE_CREATE_POLE_DANCE_SCENE(2, i, iWalkToStageScene, vEnterStageStartPos, 28.500, GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_ENTER))
				#ENDIF
				
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE)
				AND VDIST2(GET_ENTITY_COORDS(namedStripper[i].ped),vEnterStageStartPos ) > (1.0 * 1.0)
					SET_ENTITY_COORDS(namedStripper[i].ped, vEnterStageStartPos)
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				AND VDIST2(GET_ENTITY_COORDS(namedStripper[i].ped),vEnterStageStartPos ) < (1.0 * 1.0) //are you at the starting location for walking onto the stage?
				AND NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE)
					IF NOT IS_ANY_STRIPPER_EXITING_STAGE()
						CPRINTLN(DEBUG_SCLUB,"Arrived at start spot")
						PLAY_POLE_DANCE_SCENE(i, iWalkToStageScene, vEnterStageStartPos, 28.500, GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_ENTER), TRUE, 1.0, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 2)
						SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_STAGE)
					ENDIF
				ELIF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				AND bControlsStripper
					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_GOTO_STAGE_INTRO for stripper number ", i)
					SET_GENERIC_STRIPPER_GO_STAGE_START(i, vEnterStageStartPos)
				ENDIF
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_DANCE_STREAMED)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_DANCER_PLAYER_LEAVE_STAGE)
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(1))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(2))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(3))
				
			BREAK
			
			CASE POLEDANCER_STATE_GOTO_STAGE
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_GOTO_STAGE")
//				ENDIF
//				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)				
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(0))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(0))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(1))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(1))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(2))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(2))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole))
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER())
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT())
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE())
//					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE())
//					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
//				ENDIF
				//request anims needed for this stage
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE())
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole))
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				ENDIF
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_DANCE_STREAMED)
					CPRINTLN(DEBUG_SCLUB,"Dance index ", iDanceAnimIndex, " number of dances ", GET_NUMBER_OF_POLEDANCES())
					CPRINTLN(DEBUG_SCLUB,"Loading poledance anim ", GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex))
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_DANCE_STREAMED)	
				ENDIF
			
				#IF MP_STRIPCLUB
					PRE_CREATE_POLE_DANCE_SCENE(0, i, iWalkToPoleScene, vStageStartPosition, -60.0, GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole), GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(iPole, TRUE))
				#ENDIF
				
				IF PLAY_POLE_DANCE_SCENE(i, iWalkToStageScene, vEnterStageStartPos, 28.500, GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_ENTER), FALSE, 1.0)
					PLAY_POLE_DANCE_SCENE(i, iWalkToPoleScene, vStageStartPosition, -60.0, GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole), GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(iPole, TRUE), TRUE)
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED) //going to stream new stuff for next section
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_POLE)
				ENDIF

			BREAK
			
			CASE POLEDANCER_STATE_GOTO_POLE
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_GOTO_POLE")
//				ENDIF

				//reset some call stripper variables before dance
				iMoneyGivenToPoleDancer = 0
				CLEAR_DANCE_FOR_PED()
				SET_CALLED_CURRENT_POLEDANCER(FALSE)
				#IF MP_STRIPCLUB
					SET_PLAYER_BROADCAST_CALL_STRIPPER(clubPlayerBD[PARTICIPANT_ID_TO_INT()], FALSE)
				#ENDIF
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE()) //remove unneeded anim
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER())	
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				ENDIF
				
				#IF MP_STRIPCLUB
					PRE_CREATE_POLE_DANCE_SCENE(0, i, iPoleEnterScene, vPoleC + GET_POLEDANCE_OFFSET_START_DANCE(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_ENTER))
				#ENDIF
				
				//Play walk to pole scene
				IF PLAY_POLE_DANCE_SCENE(i, iWalkToPoleScene, vStageStartPosition, -60.0, GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole), GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(iPole, TRUE), FALSE)
					PLAY_POLE_DANCE_SCENE(i, iPoleEnterScene, vPoleC + GET_POLEDANCE_OFFSET_START_DANCE(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_ENTER), TRUE)
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_DANCE_ENTER)
				ENDIF	
			BREAK
			
			CASE POLEDANCER_STATE_DANCE_ENTER
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_DANCE_ENTER")
//				ENDIF

				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(iPole))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER())
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex))
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				ENDIF
				
				MANAGE_STRIPPER_POLEDANCE_FLIRTING(i, 15000)
				
				#IF MP_STRIPCLUB
					PRE_CREATE_POLE_DANCE_SCENE(0, i, iPoleDanceScene, vPoleC + GET_POLEDANCE_OFFSET_DANCING(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex), GET_POLE_DANCE_ANIM_NAME(iDanceAnimIndex))
				#ENDIF
				
				//play pole dance enter scene
				IF PLAY_POLE_DANCE_SCENE(i, iPoleEnterScene, vPoleC + GET_POLEDANCE_OFFSET_START_DANCE(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_ENTER), FALSE, 0.999)
					PLAY_POLE_DANCE_SCENE(i, iPoleDanceScene, vPoleC + GET_POLEDANCE_OFFSET_DANCING(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex), GET_POLE_DANCE_ANIM_NAME(iDanceAnimIndex), TRUE)
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_DANCE)
				ENDIF
			BREAK
			
			CASE POLEDANCER_STATE_DANCE
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_DANCE")
//				ENDIF
//				CPRINTLN(DEBUG_SCLUB,"Heading to dancer ", GET_HEADING_FROM_ENTITIES_LA(PLAYER_PED_ID(), namedStripper[i].ped))

				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER())
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT())
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex))
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				ENDIF
				
				#IF MP_STRIPCLUB
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF serverBD.bStripperDoneWithEndScene
							serverBD.bStripperDoneWithEndScene = FALSE //for syncing end of dance
						ENDIF
					ENDIF
					
					PRE_CREATE_POLE_DANCE_SCENE(0, i, iPoleExitScene, vPoleC + GET_POLEDANCE_OFFSET_DANCING(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_EXIT), INSTANT_BLEND_IN, WALK_BLEND_OUT)
				#ENDIF
				
				MANAGE_STRIPPER_POLEDANCE_FLIRTING(i, 15000)
				
				//play pole dance scene
				IF PLAY_POLE_DANCE_SCENE(i, iPoleDanceScene, vPoleC + GET_POLEDANCE_OFFSET_DANCING(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex), GET_POLE_DANCE_ANIM_NAME(iDanceAnimIndex), FALSE,  0.999)
					PLAY_POLE_DANCE_SCENE(i, iPoleExitScene, vPoleC + GET_POLEDANCE_OFFSET_DANCING(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_EXIT), TRUE, 1.0,  INSTANT_BLEND_IN, WALK_BLEND_OUT, 0)
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_DANCE_EXIT)
				ENDIF
			BREAK
			
			CASE POLEDANCER_STATE_DANCE_EXIT
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_DANCE_EXIT")
//				ENDIF
//				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(iDanceAnimIndex))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT())
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2))
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
//				ENDIF
				
				#IF MP_STRIPCLUB
					PRE_CREATE_POLE_DANCE_SCENE(0, i, iWalkToPoleScene, vPoleC + GET_POLEDANCE_OFFSET_START_DANCE(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole), GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(iPole, FALSE))
				#ENDIF
				
				iLocalSceneID = iPoleExitScene
				#IF MP_STRIPCLUB
					iLocalSceneID = serverBD.iPoleDancerNetSceneID[i]
				#ENDIF
				iLocalSceneID = GET_SRTIPCLUB_LOCAL_SCENE_ID(iLocalSceneID) //get the scene id localy
				
				
				#IF MP_STRIPCLUB
					IF serverBD.bStripperDoneWithEndScene
						IF bControlsStripper
							CPRINTLN(DEBUG_SCLUB,"MP Ask stripper to comeover, angle to player ", GET_HEADING_FROM_ENTITIES_LA(GET_PED_FOR_DANCE(), namedStripper[i].ped))
							PLAY_WALK_TO_PLAYER_FROM_POLE_ANIM(i)
							SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_MONEY_FROM_POLE)
						ENDIF
					ENDIF
				#ENDIF
				
				//eveyone checks if sync scene has ended
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					CPRINTLN(DEBUG_SCLUB,"Exit scene is running ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID))
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 1.0
						CPRINTLN(DEBUG_SCLUB,"Exit scene is over")
						IF  DOES_ENTITY_EXIST(GET_PED_FOR_DANCE())
							CPRINTLN(DEBUG_SCLUB,"ped for dance exist")
							
							#IF MP_STRIPCLUB
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									serverBD.bStripperDoneWithEndScene = TRUE
								ENDIF
							#ENDIF
							
							#IF NOT MP_STRIPCLUB
								CPRINTLN(DEBUG_SCLUB,"Ask stripper to comeover, angle to player ", GET_HEADING_FROM_ENTITIES_LA(GET_PED_FOR_DANCE(), namedStripper[i].ped))
								PLAY_WALK_TO_PLAYER_FROM_POLE_ANIM(i)
								SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_MONEY_FROM_POLE)
							#ENDIF
						ELSE
							//no one called stripper
							#IF MP_STRIPCLUB
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							#ENDIF
								PLAY_POLE_DANCE_SCENE(i, iWalkToPoleScene, vPoleC + GET_POLEDANCE_OFFSET_START_DANCE(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole), GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(iPole, FALSE), TRUE)
								SET_POLEDANCER_STATE(i, POLEDANCER_STATE_RETURN_TO_STAGE)
								CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
							#IF MP_STRIPCLUB
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SCLUB,"Exit scene is not running")
					IF NOT IS_STRIPCLUB_MP()
						PLAY_POLE_DANCE_SCENE(i, iPoleExitScene, vPoleC + GET_POLEDANCE_OFFSET_DANCING(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_EXIT), FALSE, 1.0,  INSTANT_BLEND_IN, -0.01, 0)
					ENDIF
				ENDIF
				
				
				IF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0), GET_POLE_TO_RAIL_ANIM_NAME(0))
				OR IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1), GET_POLE_TO_RAIL_ANIM_NAME(1))
				OR IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2), GET_POLE_TO_RAIL_ANIM_NAME(2))
					//Stripper was tasked to play anim, switch state
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_MONEY_FROM_POLE, TRUE)
				ENDIF
				
			BREAK
			
			CASE POLEDANCER_STATE_GOTO_MONEY_FROM_POLE
				CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_GOTO_MONEY_FROM_POLE")
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE())
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(1))
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(2))
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(3))
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_PICK_UP_MONEY_DICT())
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(0))//  for the walk back after the private dance
				
				IF bControlsStripper

					fWalkToRailAnimTime = GET_WALK_FROM_POLE_TO_PLAYER_PHASE(i)
					
					CPRINTLN(DEBUG_SCLUB,"Walk from pole to player time ", fWalkToRailAnimTime)
					
					//Not doing any anim task
					IF fWalkToRailAnimTime = 0
					AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK
						CPRINTLN(DEBUG_SCLUB, "Not tasked to move to a player? Retask")
						PLAY_WALK_TO_PLAYER_FROM_POLE_ANIM(i)
					ENDIF
					
					IF fWalkToRailAnimTime >= 0.98
						TASK_PLAY_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_IDLE), REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_IDLE))
					//Stripper was tasked to play anim, switch state
					CPRINTLN(DEBUG_SCLUB,"Stripper is playing idle dance anim, move to next state")
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_PRIVATE_DANCE, TRUE)
				ENDIF
				
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE1_PLAYED)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE2_PLAYED)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE3_PLAYED)			
			BREAK
			
			CASE POLEDANCER_STATE_PRIVATE_DANCE
				CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_PRIVATE_DANCE")
				
				IF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_PICK_UP_MONEY_DICT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PICK_UP_MONEY))
					//Stripper was tasked to play anim by another machine, switch state
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_TAKE_MONEY, TRUE)
				ENDIF
				
				BOOL bAllowNextDance
				
				bAllowNextDance = FALSE
				
				IF GET_PED_FOR_DANCE() = PLAYER_PED_ID()// I am the one who is interacting with the stripper
					CPRINTLN(DEBUG_SCLUB, "I am the one getting a dance")
					iPoleDancerAtPlayer = i
					IF iMoneyGivenToPoleDancer > 0 //I gave the stripper money
						bAllowNextDance = TRUE
					ENDIF
					
					IF NOT bControlsStripper
						CPRINTLN(DEBUG_SCLUB, "Don't own stripper the strippper for stage dance when I should")
						NETWORK_REQUEST_CONTROL_OF_ENTITY(namedStripper[i].ped)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SCLUB, "I am not getting a dance, ped ID of owner ", NATIVE_TO_INT(GET_PED_FOR_DANCE()))
					IF bControlsStripper
						CPRINTLN(DEBUG_SCLUB, "Not the one getting a dance but im in control")
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(GET_PED_FOR_DANCE()) AND bControlsStripper
						CPRINTLN(DEBUG_SCLUB, "No one getting a poledance.")
						//no one is at pole, stop now
						IF IS_PRIVATE_DANCE_OVER(i, TRUE, TRUE, TRUE)
							TASK_PLAY_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_PICK_UP_MONEY_DICT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PICK_UP_MONEY), REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
							SET_POLEDANCER_STATE(i, POLEDANCER_STATE_TAKE_MONEY)
						ENDIF
					ENDIF
					bControlsStripper = FALSE //only player interacting with stripper should task her
					iPoleDancerAtPlayer = -1
				ENDIF
				
				IF bControlsStripper
					
					CPRINTLN(DEBUG_SCLUB,"Money given, ", iMoneyGivenToPoleDancer)
					IF IS_PRIVATE_DANCE_OVER(i)
						TASK_PLAY_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_PICK_UP_MONEY_DICT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PICK_UP_MONEY), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						SET_POLEDANCER_STATE(i, POLEDANCER_STATE_TAKE_MONEY)
					ENDIF
					
					fWalkToRailAnimTime = GET_WALK_FROM_POLE_TO_PLAYER_PHASE(i)
					
					IF fWalkToRailAnimTime = 0
					AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK
						CPRINTLN(DEBUG_SCLUB, "Stripper is not doing any animation during stage dance. End dance")
						SET_POLEDANCER_STATE(i, POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE, TRUE) //don't play money pickup anim
					ENDIF
					
					CPRINTLN(DEBUG_SCLUB, "Walk to pole phase ", fWalkToRailAnimTime)
					IF fWalkToRailAnimTime > 0
						CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE1_PLAYED)
					ENDIF
					
					//You get the first dance for calling her over
					IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE1_PLAYED)
						CPRINTLN(DEBUG_SCLUB,"Dance 1 flag not set")
						//play stage one of private dance at end of idle
						IF PLAY_PRIVATE_ANIM_AT_END_OF_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_IDLE),
															GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(1), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE1), 0.95, AF_HOLD_LAST_FRAME,
															fWalkToRailAnimTime > 0.98) //walk to rail anim is still playing, sometimes animations don't sync if the stripper changes owner.
							
							iMoneyGivenToPoleDancer = 0
							SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE1_PLAYED)
						ENDIF
						
					ELIF bAllowNextDance AND NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE2_PLAYED)
						//if stage 1 is over and player gave enough money, play stage 2
						IF PLAY_PRIVATE_ANIM_AT_END_OF_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(1), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE1),
															GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(2), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE2), 0.98, AF_HOLD_LAST_FRAME)
							iMoneyGivenToPoleDancer = 0
							SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE2_PLAYED)
						ENDIF
						
					ELIF bAllowNextDance AND NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE3_PLAYED)
						
						//if stage 2 is over and player gave enough money, play stage 3
						IF PLAY_PRIVATE_ANIM_AT_END_OF_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(2), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE2),
															GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(3), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PRIVATE_DANCE3), 0.98, AF_HOLD_LAST_FRAME)
							iMoneyGivenToPoleDancer = 0
							SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PRIVATE_DANCE3_PLAYED)
						ENDIF
						
					ELIF bAllowNextDance
						//Never mind don't loops
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SCLUB,"Don't control stripper during private dance")
				ENDIF
			BREAK
			
			CASE POLEDANCER_STATE_TAKE_MONEY
				CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_TAKE_MONEY")		
				#IF MP_STRIPCLUB
					CPRINTLN(DEBUG_SCLUB,"Stripper is done dancing for me")
					SET_PLAYER_BROADCAST_CALL_STRIPPER(clubPlayerBD[PARTICIPANT_ID_TO_INT()], FALSE) //stripper is interacting with you, stop 'calling her'
				#ENDIF
					
				iPoleDancerAtPlayer = -1
				CLEAR_DANCE_FOR_PED()
				
				IF IS_ENTITY_PLAYING_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_PICK_UP_MONEY_DICT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PICK_UP_MONEY))
					// Don't check for animation to end. Check for almost ending, for better transition into walk in next state
					IF GET_ENTITY_ANIM_CURRENT_TIME(namedStripper[i].ped, GET_STRIP_CLUB_PICK_UP_MONEY_DICT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PICK_UP_MONEY)) > 0.90
						CPRINTLN(DEBUG_SCLUB,"Done taking money")
						
						REMOVE_ALL_PRIVATE_DANCE_ANIMS()
						DELETE_CASH_OBJECTS()
								
						IF IS_STRIPCLUB_MP()
							#IF MP_STRIPCLUB
//								IF GET_PLAYER_CALLING_STRIPPER() > -1 //someone else wants to get a dance from stripper
//									SET_POLEDANCER_STATE(i, POLEDANCER_STATE_WAIT_FOR_GOTO_MONEY)
//								ELSE
									SET_POLEDANCER_STATE(i, POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE)
//								ENDIF
							#ENDIF
						ELSE
							SET_POLEDANCER_STATE(i, POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE)
						ENDIF
					ENDIF
				ELIF bControlsStripper
					TASK_PLAY_ANIM(namedStripper[i].ped, GET_STRIP_CLUB_PICK_UP_MONEY_DICT(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_PICK_UP_MONEY), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				ENDIF
			BREAK
			
			CASE POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE")
//				ENDIF
				#IF MP_STRIPCLUB

					SET_PLAYER_BROADCAST_CALL_STRIPPER(clubPlayerBD[PARTICIPANT_ID_TO_INT()], FALSE) //stripper is interacting with you, stop 'calling her'
				#ENDIF
				
				REMOVE_ALL_PRIVATE_DANCE_ANIMS()
				
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT())
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1))
				REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2))
				
				IF bControlsStripper
					FREEZE_ENTITY_POSITION(namedStripper[i].ped, FALSE)
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				AND VDIST2(GET_ENTITY_COORDS(namedStripper[i].ped),vEnterStageStartPos ) < (1.0 * 1.0)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_DRESSING_ROOM)
				ELIF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
					IF bControlsStripper
						CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE for stripper number ", i)
						SET_GENERIC_STRIPPER_GO_STAGE_START(i, vEnterStageStartPos)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE POLEDANCER_STATE_RETURN_TO_STAGE
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_RETURN_TO_STAGE")
//				ENDIF

				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT())
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE())	
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				ENDIF
				#IF MP_STRIPCLUB
					PRE_CREATE_POLE_DANCE_SCENE(0, i, iStageExitScene, vStageStartPosition, -60.0, GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_EXIT), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				#ENDIF
				
				//Play walk to pole scene
				IF PLAY_POLE_DANCE_SCENE(i, iWalkToPoleScene, vPoleC + GET_POLEDANCE_OFFSET_START_DANCE(iPole), -60.0, GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole), GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(iPole, FALSE), FALSE)
					PLAY_POLE_DANCE_SCENE(i, iStageExitScene, vStageStartPosition, -60.0, GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE(), GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_EXIT), TRUE)
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_EXIT_STAGE)
				ENDIF
			BREAK
			
			CASE POLEDANCER_STATE_EXIT_STAGE
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_EXIT_STAGE")
//				ENDIF

				IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)	
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(iPole))
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0))
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1))
					REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2))
					REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE())	
					SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
				ENDIF
				
				iLocalSceneID = iStageExitScene
				#IF MP_STRIPCLUB
					iLocalSceneID = serverBD.iPoleDancerNetSceneID[i]
				#ENDIF
				iLocalSceneID = GET_SRTIPCLUB_LOCAL_SCENE_ID(iLocalSceneID) //get the scene id localy
				BOOL bSceneDone 
				bSceneDone = FALSE
				
				#IF MP_STRIPCLUB
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT() #ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 1.0
							bSceneDone = TRUE
						ENDIF
					ELSE
						bSceneDone = TRUE
					ENDIF
				#IF MP_STRIPCLUB ENDIF #ENDIF
				
				IF bSceneDone
					INC_NUMBER_OF_POLEDANCES()
					REMOVE_ALL_POLE_ANIMS()
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_POLEDANCE_STREAMED)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_GOTO_DRESSING_ROOM)
				ENDIF
			BREAK
			
			CASE POLEDANCER_STATE_GOTO_DRESSING_ROOM
//				IF ENUM_TO_INT(GET_STAGE_STATE()) = i
//					CPRINTLN(DEBUG_SCLUB,"POLEDANCER_STATE_GOTO_DRESSING_ROOM")
//				ENDIF
				GET_GENERIC_STRIPPER_DRESSING_ROOM_LOC(i, vWaitingPos, fWaitingRot)
			
				IF GET_SCRIPT_TASK_STATUS(namedStripper[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				AND VDIST2(GET_ENTITY_COORDS(namedStripper[i].ped), vWaitingPos) < (1.5 * 1.5)
					//Stripper is back in dressing room and is done with poledancing
					CPRINTLN(DEBUG_SCLUB,"Stripclub: Arrived at waiting spot")
					SET_STRIPPER_AI_STATE(i, AI_STATE_WANDER)
					SET_POLEDANCER_STATE(i, POLEDANCER_STATE_WAIT_IN_DRESSING_ROOM)
				ELIF bControlsStripper
					SET_GENERIC_STRIPPER_GO_WAITING_ROOM(i, vWaitingPos, fWaitingRot)
				ENDIF
				
				//CPRINTLN(DEBUG_SCLUB, "Stripclub: Not passing if condition")
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC
