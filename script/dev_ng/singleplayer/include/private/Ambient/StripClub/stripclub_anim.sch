USING "load_queue_public.sch"

ENUM STRIP_CLUB_LAPDANCE_ANIM_ENUM

	SCLDA_APPROACH,
	SCLDA_SIT,
	SCLDA_IDLE_M,
	SCLDA_REACH,
	SCLDA_RETRACT,
	SCLDA_EXIT,
	SCLDA_OFFER,
	SCLDA_ACCEPT,
	SCLDA_DECLINE

ENDENUM

ENUM STRIP_CLUB_POLEDANCE_ANIM_ENUM
	SCPDA_ENTER,
	SCPDA_EXIT,
	SCPDA_STAGE_TO_POLE_A,
	SCPDA_STAGE_TO_POLE_B,
	SCPDA_STAGE_TO_POLE_C,
	SCPDA_POLE_A_TO_STAGE,
	SCPDA_POLE_B_TO_STAGE,
	SCPDA_POLE_C_TO_STAGE,
	SCPDA_POLE_ENTER,
	SCPDA_POLE_EXIT,
	SCPDA_PRIVATE_IDLE,
	SCPDA_PRIVATE_DANCE1,
	SCPDA_PRIVATE_DANCE2,
	SCPDA_PRIVATE_DANCE3,
	SCPDA_PICK_UP_MONEY
ENDENUM

ENUM STRIP_CLUB_BAR_ANIM_ENUM
	SCBA_DRUNK1,
	SCBA_DRUNK2,
	SCBA_DRUNK3,
	SCBA_DRUNK4,
	SCBA_IDLE
ENDENUM

ENUM STRIP_CLUB_LEAN_ANIM_ENUM
	SCLA_LEAN_IN,
	SCLA_LEAN_OUT,
	SCLA_LEAN_THROW,
	SCLA_LEAN_THROW_FULL,
	SCLA_LEAN_THROW_MANY,
	SCLA_LEAN_IDLE
ENDENUM

ENUM STRIP_CLUB_BOUNCER_ANIM_ENUM
	SCBA_BOUNCER_OUTSIDE_IDLE,
	SCBA_BOUNCER_VIP_IDLE,
	SCBA_BOUNCER_VIP_STOP,
	SCBA_BOUNCER_VIP_SIDE_ENTER,
	SCBA_BOUNCER_VIP_SIDE_EXIT,
	SCBA_BOUNCER_PATROL_YELL,
	SCBA_BOUNCER_THROWA,
	SCBA_BOUNCER_THROWB,
	SCBA_PLAYER_THROW
ENDENUM

ENUM NETWORK_SYNC_FLAGS
	NO_SYNC_NEEDED = 0,
	SYNC_PRE_BUILD_TOUCHING = BIT0
ENDENUM

//// get guard anim dict //THIS FUCNTION IS IN stripclub_public.sch
//FUNC STRING GET_STRIP_CLUB_ANIM_DICT_BOUNCER_IDLE()
//	RETURN "mini@strip_club@idles@bouncer@base"
//ENDFUNC

FUNC BOOL STRIPCLUB_HANDLE_DRUNK_SHAKE(CAMERA_INDEX sCam)

	IF IS_PED_DRUNK(PLAYER_PED_ID())
	AND GET_STRIPCLUB_AMOUNT_OF_ALCOHOL() > 3
		SHAKE_SCRIPT_GLOBAL("DRUNK_SHAKE", 1)
		SHAKE_CAM(sCam, "DRUNK_SHAKE", 1)
		RETURN TRUE
	ELSE
		IF IS_SCRIPT_GLOBAL_SHAKING()
			STOP_SCRIPT_GLOBAL_SHAKING()
		ENDIF
		IF IS_CAM_SHAKING(sCam)
			STOP_CAM_SHAKING(sCam)
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(INT iVariation)
	
	IF iVariation = 0
		RETURN "mini@strip_club@idles@bouncer@base"
	ELIF iVariation = 1
		RETURN "mini@strip_club@idles@bouncer@idle_a"
	ELIF iVariation = 2
		RETURN "mini@strip_club@idles@bouncer@idle_b"
	ELSE
		RETURN "mini@strip_club@idles@bouncer@idle_c"
	ENDIF

ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_BOUNCER_IDLES(INT iVariation)
	
	IF iVariation = 0
		RETURN "base"
	ELIF iVariation = 1
		RETURN "idle_a"
	ELIF iVariation = 2
		RETURN "idle_b"
	ELSE
		RETURN "idle_c"
	ENDIF

ENDFUNC

// get bouncer stop anim dict
FUNC STRING GET_STRIP_CLUB_ANIM_DICT_BOUNCER_STOP()
	RETURN "mini@strip_club@idles@bouncer@stop"
ENDFUNC

// get bouncer side enter anim dic
FUNC STRING GET_STRIP_CLUB_ANIM_DICT_BOUNCER_SIDE_ENTER()
	RETURN "mini@strip_club@idles@bouncer@side_enter"
ENDFUNC

// get bouncer side exi anim dic
FUNC STRING GET_STRIP_CLUB_ANIM_DICT_BOUNCER_SIDE_EXIT()
	RETURN "mini@strip_club@idles@bouncer@side_exit"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_BOUNCER_YELL()
	RETURN "mini@strip_club@idles@bouncer@go_away"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_THROW_OUT(INT iVariation)
	iVariation = iVariation
	
	RETURN "mini@strip_club@throwout_d@"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_CAM_ANIM_THROW_OUT(INT iVariation)
	iVariation = iVariation

	RETURN "throwout_d_cam"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_BOUNCER_THOWOUT_ANIM(STRIP_CLUB_BOUNCER_ANIM_ENUM eAnimEnum, INT iVariation)
	iVariation = iVariation
	
	SWITCH eAnimEnum
		CASE SCBA_BOUNCER_THROWA
			RETURN "throwout_d_bouncer_a"
		BREAK
		CASE SCBA_BOUNCER_THROWB
			RETURN "throwout_d_bouncer_b"
		BREAK
		CASE SCBA_PLAYER_THROW
			RETURN "throwout_d_victim"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid throwout anim")
	RETURN ""
ENDFUNC

PROC GET_THROWOUT_START_LOC(INT iVariation, VECTOR &vStartPos, FLOAT &fStartHeading)

	iVariation = iVariation
	
	vStartPos = << 138.99, -1298.27, 28.90 >>
	fStartHeading = 0

ENDPROC

PROC GET_THROWOUT_OPEN_DOOR_TIMES(INT iVariation, FLOAT &fStartTime, FLOAT &fEndTime)
	IF iVariation > 0
		fStartTime = 0.063
		fEndTime = 0.106
	ELSE
		fStartTime = 0.22
		fEndTime = 0.255
	ENDIF 
ENDPROC

FUNC STRING GET_STRIP_CLUB_BOUNCER_ANIM(STRIP_CLUB_BOUNCER_ANIM_ENUM eAnimEnum)
	SWITCH eAnimEnum
		CASE SCBA_BOUNCER_VIP_IDLE
		CASE SCBA_BOUNCER_OUTSIDE_IDLE
			RETURN "base"
		BREAK
		CASE SCBA_BOUNCER_VIP_SIDE_ENTER
			RETURN "side_enter"
		BREAK
		CASE SCBA_BOUNCER_VIP_SIDE_EXIT
			RETURN "side_exit"
		BREAK
		CASE SCBA_BOUNCER_VIP_STOP
			RETURN "stop"
		BREAK
		CASE SCBA_BOUNCER_PATROL_YELL
			RETURN "go_away"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid bouncer anim")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE()
	RETURN "mini@strip_club@idles@stripper"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_DANCE_APPROACH(BOOL bDoubleDance)
	IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_approach"
	ELSE
		RETURN "mini@strip_club@lap_dance@ld_girl_a_approach"
	ENDIF
ENDFUNC

//The player sits down
FUNC STRING GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(BOOL bDoubleDance)
	IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_intro"
	ELSE
		RETURN "mini@strip_club@lap_dance@ld_girl_a_intro"
	ENDIF
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_REACH(BOOL bDoubleDance)
		IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_reach"
	ELSE
		RETURN "mini@strip_club@lap_dance@ld_reach"
	ENDIF
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(BOOL bDoubleDance, BOOL bQuick)
	IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_exit"
	ELSE
		IF bQuick
			RETURN "mini@strip_club@lap_dance@ld_girl_a_decline_alt"
		ELSE
			RETURN "mini@strip_club@lap_dance@ld_girl_a_exit"
		ENDIF
	ENDIF

ENDFUNC

FUNC STRING GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(BOOL bDoubleDance)
	IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_wait"
	ELSE
		RETURN "mini@strip_club@lap_dance@ld_girl_a_wait"
	ENDIF
ENDFUNC

FUNC STRING GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(BOOL bDoubleDance)
	IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_accept"
	ELSE
		RETURN "mini@strip_club@lap_dance@ld_girl_a_accept"
	ENDIF
ENDFUNC

FUNC STRING GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(BOOL bDoubleDance)
	IF bDoubleDance
		RETURN "mini@strip_club@lap_dance_2g@ld_2g_decline"
	ELSE
		RETURN "mini@strip_club@lap_dance@ld_girl_a_decline"
	ENDIF
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(INT iVariation = -1)

	iVariation = PICK_INT(iVariation <= 0, GET_RANDOM_INT_IN_RANGE(2, 7), iVariation)
	iVariation = PICK_INT(iVariation = 5, 2, iVariation) //I don't like idle 5
	
	SWITCH iVariation
		CASE 1
			RETURN "stripper_idle_01"
		BREAK
		CASE 2
			RETURN "stripper_idle_02"
		BREAK
		CASE 3
			RETURN "stripper_idle_03"
		BREAK
		CASE 4
			RETURN "stripper_idle_04"
		BREAK
		CASE 5
			RETURN "stripper_idle_05"
		BREAK
		CASE 6
			RETURN "stripper_idle_06"
		BREAK
	ENDSWITCH

	PRINTLN("Invalid stripper idle variant ", iVariation)
	SCRIPT_ASSERT("Invalid variation for stripper idle")
	RETURN ""

ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(INT iVariation, BOOL bDuo = FALSE)
	SWITCH iVariation
		CASE 0
			IF bDuo
				RETURN "mini@strip_club@lap_dance_2g@ld_2g_p1"
			ELSE
				RETURN "mini@strip_club@lap_dance@ld_girl_a_song_a_p1"
			ENDIF
		BREAK
		CASE 1
			IF bDuo
				RETURN "mini@strip_club@lap_dance_2g@ld_2g_p2"
			ELSE
				RETURN "mini@strip_club@lap_dance@ld_girl_a_song_a_p2"
			ENDIF
		BREAK
		CASE 2
			IF bDuo
				RETURN "mini@strip_club@lap_dance_2g@ld_2g_p3"
			ELSE
				RETURN "mini@strip_club@lap_dance@ld_girl_a_song_a_p3"
			ENDIF
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid variation for script club lapdance dictionary variation")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_LAPDANCE_NAME(INT iVariation, BOOL bMale, BOOL bDuo = FALSE, BOOL bSecondGirl = FALSE)
	SWITCH iVariation
		CASE 0
			IF bDuo
				IF bMale
					RETURN "ld_2g_p1_m"
				ELSE
					IF bSecondGirl
						RETURN "ld_2g_p1_s1"
					ELSE
						RETURN "ld_2g_p1_s2"
					ENDIF
				ENDIF
			ELSE
				IF bMale
					RETURN "ld_girl_a_song_a_p1_m"
				ELSE
					RETURN "ld_girl_a_song_a_p1_f"
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF bDuo
				IF bMale
					RETURN "ld_2g_p2_m"
				ELSE
					IF bSecondGirl
						RETURN "ld_2g_p2_s1"
					ELSE
						RETURN "ld_2g_p2_s2"
					ENDIF
				ENDIF
			ELSE
				IF bMale
					RETURN "ld_girl_a_song_a_p2_m"
				ELSE
					RETURN "ld_girl_a_song_a_p2_f"
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF bDuo
				IF bMale
					RETURN "ld_2g_p3_m"
				ELSE
					IF bSecondGirl
						RETURN "ld_2g_p3_s1"
					ELSE
						RETURN "ld_2g_p3_s2"
					ENDIF
				ENDIF
			ELSE
				IF bMale
					RETURN "ld_girl_a_song_a_p3_m"
				ELSE
					RETURN "ld_girl_a_song_a_p3_f"
				ENDIF
			ENDIF
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid variation for script club lapdance animation variation")
		BREAK
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE()
	RETURN "mini@strip_club@pole_dance@stage_enter"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE()
	RETURN "mini@strip_club@pole_dance@stage_exit"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(INT iPole)

	SWITCH iPole
		CASE 0
			RETURN "mini@strip_club@pole_dance@stage_2_pole_c"
		BREAK
		CASE 1
			RETURN "mini@strip_club@pole_dance@stage_2_pole_b"
		BREAK
		CASE 2
			RETURN "mini@strip_club@pole_dance@stage_2_pole_a"
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid pole for stage to pole anim dict")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(INT iPole)

	SWITCH iPole
		CASE 0
			RETURN "mini@strip_club@pole_dance@pole_c_2_stage"
		BREAK
		CASE 1
			RETURN "mini@strip_club@pole_dance@pole_b_2_stage"
		BREAK
		CASE 2
			RETURN "mini@strip_club@pole_dance@pole_a_2_stage"
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid pole for stage to pole anim dict")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(INT iRailIndex)
	SWITCH iRailIndex
		CASE 0
			RETURN "mini@strip_club@pole_dance@Pole_C_2_PrvD_A"
		BREAK
		CASE 1
			RETURN "mini@strip_club@pole_dance@Pole_C_2_PrvD_B"
		BREAK
		CASE 2
			RETURN "mini@strip_club@pole_dance@Pole_C_2_PrvD_C"
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid iRailIndex for pole to rail anim dict")
	RETURN ""

ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(INT iDance)

	SWITCH iDance
		CASE 1
			RETURN "mini@strip_club@pole_dance@pole_dance1"
		BREAK
		CASE 2
			RETURN "mini@strip_club@pole_dance@pole_dance2"
		BREAK
		CASE 3
			RETURN "mini@strip_club@pole_dance@pole_dance3"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid pole dance variation anim dict")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER()
	RETURN "mini@strip_club@pole_dance@pole_enter"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT()
	RETURN "mini@strip_club@pole_dance@pole_exit"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE()
	RETURN "mini@strip_club@private_dance@idle"
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(INT iDance)
	
	SWITCH iDance
		CASE 1
			RETURN "mini@strip_club@private_dance@part1"
		BREAK
		CASE 2
			RETURN "mini@strip_club@private_dance@part2"
		BREAK
		CASE 3
			RETURN "mini@strip_club@private_dance@part3"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid private dance variation anim dict")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_PICK_UP_MONEY_DICT()
	RETURN "mini@strip_club@private_dance@exit"
ENDFUNC


FUNC STRING GET_STRIP_CLUB_LEAN_ANIM_DICT(STRIP_CLUB_LEAN_ANIM_ENUM eLeanAnim)
	SWITCH eLeanAnim
		CASE SCLA_LEAN_IN
			RETURN "mini@strip_club@leaning@enter"
		BREAK
		CASE SCLA_LEAN_OUT
			RETURN "mini@strip_club@leaning@exit"
		BREAK
		CASE SCLA_LEAN_THROW
		CASE SCLA_LEAN_THROW_FULL
			RETURN "mini@strip_club@leaning@toss"
		BREAK
		CASE SCLA_LEAN_THROW_MANY
			RETURN "mini@strip_club@leaning@toss_many"
		BREAK
		CASE SCLA_LEAN_IDLE
			RETURN "mini@strip_club@leaning@base"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid bar drunk animtion dictionary")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(STRIP_CLUB_BAR_ANIM_ENUM eDrinkAnim)
	IF NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		SWITCH eDrinkAnim
			CASE SCBA_DRUNK1
				RETURN "mini@strip_club@drink@one"
			BREAK
			CASE SCBA_DRUNK2
				RETURN "mini@strip_club@drink@two"
			BREAK
			CASE SCBA_DRUNK3
				RETURN "mini@strip_club@drink@three"
			BREAK
			CASE SCBA_DRUNK4
				RETURN "mini@strip_club@drink@four"
			BREAK
			CASE SCBA_IDLE
				RETURN"mini@strip_club@drink@idle_a"
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Invalid bar animtion dictionary")
			BREAK
		ENDSWITCH
	ELSE
		SWITCH eDrinkAnim
			CASE SCBA_DRUNK1
				RETURN "anim@mini@yacht@bar@drink@one"
			BREAK
			CASE SCBA_DRUNK2
				RETURN "anim@mini@yacht@bar@drink@two"
			BREAK
			CASE SCBA_DRUNK3
				RETURN "anim@mini@yacht@bar@drink@three"
			BREAK
			CASE SCBA_DRUNK4
				RETURN "anim@mini@yacht@bar@drink@four"
			BREAK
			CASE SCBA_IDLE
				RETURN"anim@mini@yacht@bar@drink@idle_a"
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Invalid bar animtion dictionary")
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DJ_DICT(INT iVariation)
	SWITCH iVariation
		CASE 1
			RETURN "mini@strip_club@idles@dj@idle_01"
		BREAK
		CASE 2
			RETURN "mini@strip_club@idles@dj@idle_02"
		BREAK
		CASE 3
			RETURN "mini@strip_club@idles@dj@idle_03"
		BREAK
		CASE 4
			RETURN "mini@strip_club@idles@dj@idle_04"
		BREAK
		CASE 5
			RETURN "mini@strip_club@idles@dj@idle_05"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid dj idle dictionary variation")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_DJ_IDLES(INT iVariation)
	SWITCH iVariation
		CASE 1
			RETURN "idle_01"
		BREAK
		CASE 2
			RETURN "idle_02"
		BREAK
		CASE 3
			RETURN "idle_03"
		BREAK
		CASE 4
			RETURN "idle_04"
		BREAK
		CASE 5
			RETURN "idle_05"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid dj idle name variation")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_THROW_MANY_ANIM_NAME(BOOL bPlayer, INT iMoney)

	IF bPlayer
		RETURN "toss_many_player"
	ENDIF
	
	SWITCH iMoney
		CASE 1
			RETURN "toss_many_note_01"
		BREAK
		CASE 2
			RETURN "toss_many_note_02"
		BREAK
		CASE 3
			RETURN "toss_many_note_03"
		BREAK
		CASE 4
			RETURN "toss_many_note_04"
		BREAK
		CASE 5
			RETURN "toss_many_note_05"
		BREAK
		CASE 6
			RETURN "toss_many_note_06"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_LEAN_ANIM_NAME(STRIP_CLUB_LEAN_ANIM_ENUM eLeanAnim, BOOL bFemale = FALSE)
	SWITCH eLeanAnim
		CASE SCLA_LEAN_IN
			IF bFemale
				RETURN "enter_female"
			ELSE
				RETURN "enter"
			ENDIF
		BREAK
		CASE SCLA_LEAN_OUT
			IF bFemale
				RETURN "exit_female"
			ELSE
				RETURN "exit"
			ENDIF
		BREAK
		CASE SCLA_LEAN_THROW_FULL
			RETURN "toss_full"
		BREAK
		CASE SCLA_LEAN_THROW
			IF bFemale
				RETURN "toss_female"
			ELSE
				RETURN "toss"
			ENDIF
		BREAK
		CASE SCLA_LEAN_IDLE
			IF bFemale
				RETURN "base_female"
			ELSE
				RETURN "base"
			ENDIF
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid lean animtion name")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_BAR_OBJECTS(STRIP_CLUB_BAR_ANIM_ENUM eDrunkAnim, BOOL bGlass)
	SWITCH eDrunkAnim
		CASE SCBA_DRUNK1
			IF bGlass
				RETURN "one_shot_glass"
			ELSE
				RETURN "one_whiskey"
			ENDIF
		BREAK
		CASE SCBA_DRUNK2
			IF bGlass
				RETURN "two_shot_glass"
			ELSE
				RETURN "two_whiskey"
			ENDIF
		BREAK
		CASE SCBA_DRUNK3
			IF bGlass
				RETURN "three_shot_glass"
			ELSE
				RETURN "three_whiskey"
			ENDIF
		BREAK
		CASE SCBA_DRUNK4
			IF bGlass
				RETURN "four_shot_glass"
			ELSE
				RETURN "four_whiskey"
			ENDIF
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid bar object animtion")
		BREAK
	ENDSWITCH
	
	RETURN ""

ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_BAR(STRIP_CLUB_BAR_ANIM_ENUM eDrunkAnim, BOOL bPlayer)
	
	SWITCH eDrunkAnim
			
		CASE SCBA_DRUNK1
			IF bPlayer
				RETURN "one_player"
			ELSE
				RETURN "one_bartender"
			ENDIF
		BREAK
		CASE SCBA_DRUNK2
			IF bPlayer
				RETURN "two_player"
			ELSE
				RETURN "two_bartender"
			ENDIF
		BREAK
		CASE SCBA_DRUNK3
			IF bPlayer
				RETURN "three_player"
			ELSE
				RETURN "three_bartender"
			ENDIF
		BREAK
		CASE SCBA_DRUNK4
			IF bPlayer
				RETURN "four_player"
			ELSE
				RETURN "four_bartender"
			ENDIF
		BREAK
		CASE SCBA_IDLE
			RETURN "idle_a_bartender"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid bar drunk animtion")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_BAR_CAM_ANIM(STRIP_CLUB_BAR_ANIM_ENUM eDrinkAnim)
	SWITCH eDrinkAnim
		CASE SCBA_DRUNK1
			RETURN "one_cam"
		BREAK
		CASE SCBA_DRUNK2
			RETURN "two_cam"
		BREAK
		CASE SCBA_DRUNK3
			RETURN "three_cam"
		BREAK
		CASE SCBA_DRUNK4
			RETURN "four_cam"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid bar cam animtion")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_LAPDANCE_END_CAM(BOOL bDuo, BOOL bQuick)
	IF bDuo
		RETURN "ld_2g_exit_cam"
	ELSE
		IF bQuick
			RETURN "ld_girl_a_decline_alt_cam"
		ELSE
			RETURN "ld_girl_a_exit_cam"
		ENDIF
	ENDIF
ENDFUNC


FUNC STRING GET_STRIP_CLUB_LAPDANCE_CAM_ANIM(STRIP_CLUB_LAPDANCE_ANIM_ENUM enumDanceAnim, BOOL bDuo)
	SWITCH enumDanceAnim
		CASE SCLDA_APPROACH
			IF bDuo
				RETURN "ld_2g_approach_cam"
			ELSE
				RETURN "ld_girl_a_approach_cam"
			ENDIF
		BREAK
		CASE SCLDA_SIT
			IF bDuo
				RETURN "ld_2g_intro_cam"
			ELSE
				RETURN "ld_girl_a_intro_cam"
			ENDIF
		BREAK

		CASE SCLDA_OFFER
			IF bDuo
				RETURN "ld_2g_wait_cam"
			ELSE
				RETURN "ld_girl_a_wait_cam"
			ENDIF
		BREAK
		CASE SCLDA_ACCEPT
			IF bDuo
				RETURN "ld_2g_accept_cam"
			ELSE
				RETURN "ld_girl_a_accept_cam"
			ENDIF
		BREAK
		CASE SCLDA_DECLINE
			IF bDuo
				RETURN "ld_2g_decline_cam"
			ELSE
				RETURN "ld_girl_a_decline_cam"
			ENDIF
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid stripclub lapdance camera anim")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_DANCE_FACE_ANIM(INT iVariation, BOOL bDuo, BOOL bSecondary)
	
	SWITCH iVariation
		CASE 0
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_p1_s2_face"
				ELSE
					RETURN "ld_2g_p1_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_song_a_p1_f_face"
			ENDIF
		BREAK
		CASE 1
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_p2_s2_face"
				ELSE
					RETURN "ld_2g_p2_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_song_a_p2_f_face"
			ENDIF
		BREAK
		CASE 2
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_p3_s2_face"
				ELSE
					RETURN "ld_2g_p3_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_song_a_p3_f_face"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_LAPDANCE_FACE_ANIM(STRIP_CLUB_LAPDANCE_ANIM_ENUM enumDanceAnim, BOOL bDuo, BOOL bSecondary, BOOL bHappy = FALSE)
	
	SWITCH enumDanceAnim
		CASE SCLDA_APPROACH
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_approach_s2_face"
				ELSE
					RETURN "ld_2g_approach_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_approach_f_face"
			ENDIF
		BREAK
		CASE SCLDA_SIT
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_intro_s2_face"
				ELSE
					RETURN "ld_2g_intro_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_intro_f_face"
			ENDIF
		BREAK
		
		CASE SCLDA_EXIT
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_exit_s2_face"
				ELSE
					RETURN "ld_2g_exit_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_exit_f_face"
			ENDIF
		BREAK
		CASE SCLDA_OFFER
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_wait_s2_face"
				ELSE
					RETURN "ld_2g_wait_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_wait_f_face"
			ENDIF
		BREAK
		CASE SCLDA_ACCEPT
			IF bDuo
				IF bSecondary
					RETURN "ld_2g_accept_s2_face"
				ELSE
					RETURN "ld_2g_accept_s1_face"
				ENDIF
			ELSE
				RETURN "ld_girl_a_accept_f_face"
			ENDIF
		BREAK
		CASE SCLDA_DECLINE
			IF bDuo
				IF bSecondary
					IF bHappy
						RETURN "ld_2g_decline_h_s2_face"
					ELSE
						RETURN "ld_2g_decline_s2_face"
					ENDIF
				ELSE
					IF bHappy
						RETURN "ld_2g_decline_h_s1_face"
					ELSE
						RETURN "ld_2g_decline_s1_face"
					ENDIF
				ENDIF
			ELSE
				IF bHappy
					RETURN "ld_girl_a_decline_h_f_face"
				ELSE
					RETURN "ld_girl_a_decline_f_face"
				ENDIF
			ENDIF
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid stripclub lapdance face anim enum")
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PLAYER_LAPDANCE_IDLE_DICT(INT iVariation, BOOL bDuo)
	IF bDuo
		RETURN GET_STRIP_CLUB_ANIM_DICT_REACH(TRUE)
	ELSE
		IF iVariation = 1
			RETURN GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iVariation, FALSE)
		ELIF iVariation = 2
			RETURN GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(iVariation, FALSE)
		ELSE
			RETURN GET_STRIP_CLUB_ANIM_DICT_REACH(FALSE)
		ENDIF
	ENDIF
ENDFUNC

FUNC STRING GET_PLAYER_LAPDANCE_IDLE_ANIM(INT iVariation, BOOL bDuo)
	IF bDuo
		RETURN "ld_2g_sit_idle"
	ELSE
		IF iVariation = 1
			RETURN "ld_girl_a_song_a_p2_no_touch_m"
		ELIF iVariation = 2
			RETURN "ld_girl_a_song_a_p3_no_touch_m"
		ELSE
			RETURN "ld_sit_idle"
		ENDIF
	ENDIF
ENDFUNC

FUNC STRING GET_LAPDANCE_END_ANIMATION(BOOL bMale, BOOL bDuo, BOOL bSecondStripper, BOOL bQuick)
	IF bMale
		IF bDuo
			RETURN "ld_2g_exit_m"
		ELSE
			IF bQuick
				RETURN "ld_girl_a_decline_alt_m"
			ELSE
				RETURN "ld_girl_a_exit_m"
			ENDIF
		ENDIF
	ELSE
		IF bDuo
			IF bSecondStripper
				RETURN "ld_2g_exit_s1"
			ELSE
				RETURN "ld_2g_exit_s2"
			ENDIF
		ELSE
			IF bQuick
				RETURN "ld_girl_a_decline_alt_f"
			ELSE
				RETURN "ld_girl_a_exit_f"
			ENDIF
		ENDIF
	ENDIF
ENDFUNC


FUNC STRING GET_STRIP_CLUB_LAPDANCE_ANIM(STRIP_CLUB_LAPDANCE_ANIM_ENUM enumDanceAnim, BOOL bMale, BOOL bDuo, BOOL bSecondStripper, BOOL bHappy = FALSE)
	
	SWITCH enumDanceAnim
		CASE SCLDA_APPROACH
			IF bMale
				IF bDuo
					RETURN "ld_2g_approach_m"
				ELSE
					RETURN "ld_girl_a_approach_m"
				ENDIF
			ELSE
				IF bDuo
					IF bSecondStripper
						RETURN "ld_2g_approach_s1"
					ELSE
						RETURN "ld_2g_approach_s2"
					ENDIF
				ELSE
					RETURN "ld_girl_a_approach_f"
				ENDIF
			ENDIF
		BREAK
		CASE SCLDA_SIT
			IF bMale
				IF bDuo
					RETURN "ld_2g_intro_m"
				ELSE
					RETURN "ld_girl_a_intro_m"
				ENDIF
			ELSE
				IF bDuo
					IF bSecondStripper
						RETURN "ld_2g_intro_s1"
					ELSE
						RETURN "ld_2g_intro_s2"
					ENDIF
				ELSE
					RETURN "ld_girl_a_intro_f"
				ENDIF
			ENDIF
		BREAK

		CASE SCLDA_REACH
			IF bDuo
				RETURN "ld_2g_sit_reach_enter"
			ELSE
				RETURN "ld_sit_reach_enter"
			ENDIF
		BREAK
		CASE SCLDA_RETRACT
			IF bDuo
				RETURN "ld_2g_sit_reach_exit"
			ELSE
				RETURN "ld_sit_reach_exit"
			ENDIF
		BREAK

		CASE SCLDA_OFFER
			IF bMale
				IF bDuo
					RETURN "ld_2g_wait_m"
				ELSE
					RETURN "ld_girl_a_wait_m"
				ENDIF
			ELSE
				IF bDuo
					IF bSecondStripper
						RETURN "ld_2g_wait_s1"
					ELSE
						RETURN "ld_2g_wait_s2"
					ENDIF
				ELSE
					RETURN "ld_girl_a_wait_f"
				ENDIF
			ENDIF
		BREAK
		CASE SCLDA_ACCEPT
			IF bMale
				IF bDuo
					RETURN "ld_2g_accept_m"
				ELSE
					RETURN "ld_girl_a_accept_m"
				ENDIF
			ELSE
				IF bDuo
					IF bSecondStripper
						RETURN "ld_2g_accept_s1"
					ELSE
						RETURN "ld_2g_accept_s2"
					ENDIF
				ELSE
					RETURN "ld_girl_a_accept_f"
				ENDIF
			ENDIF
		BREAK
		CASE SCLDA_DECLINE
			IF bMale
				IF bDuo
					RETURN "ld_2g_decline_m"
				ELSE
					RETURN "ld_girl_a_decline_m"
				ENDIF
			ELSE
				IF bDuo
					IF bHappy
						IF bSecondStripper
							RETURN "ld_2g_decline_h_s1"
						ELSE
							RETURN "ld_2g_decline_h_s2"
						ENDIF
					ELSE
						IF bSecondStripper
							RETURN "ld_2g_decline_s1"
						ELSE
							RETURN "ld_2g_decline_s2"
						ENDIF
					ENDIF
				ELSE
					IF bHappy
						RETURN "ld_girl_a_decline_h_f"
					ELSE
						RETURN "ld_girl_a_decline_f"
					ENDIF
				ENDIF
			ENDIF
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid stripclub lapdance anim enum")
		BREAK
	ENDSWITCH


	RETURN ""
ENDFUNC

FUNC STRING GET_POLE_DANCE_ANIM_NAME(INT iDance)
	SWITCH iDance
		CASE 1
			RETURN "pd_dance_01"
		BREAK
		CASE 2
			RETURN "pd_dance_02"
		BREAK
		CASE 3
			RETURN "pd_dance_03"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid stripclub pole dance anim index")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_POLEDANCE_ANIM(STRIP_CLUB_POLEDANCE_ANIM_ENUM enumDanceAnim)
	SWITCH enumDanceAnim
		CASE SCPDA_ENTER
			RETURN "stage_enter"
		BREAK
		CASE SCPDA_EXIT
			RETURN "stage_exit"
		BREAK
		CASE SCPDA_STAGE_TO_POLE_A
			RETURN "stage_2_pole_a"
		BREAK
		CASE SCPDA_STAGE_TO_POLE_B
			RETURN "stage_2_pole_b"
		BREAK
		CASE SCPDA_STAGE_TO_POLE_C
			RETURN "stage_2_pole_c"
		BREAK
		CASE SCPDA_POLE_A_TO_STAGE
			RETURN "pole_a_2_stage"
		BREAK
		CASE SCPDA_POLE_B_TO_STAGE
			RETURN "pole_b_2_stage"
		BREAK
		CASE SCPDA_POLE_C_TO_STAGE
			RETURN "pole_c_2_stage"
		BREAK
		CASE SCPDA_POLE_ENTER
			RETURN "pd_enter"
		BREAK
		CASE SCPDA_POLE_EXIT
			RETURN "pd_exit"
		BREAK
		CASE SCPDA_PRIVATE_IDLE
			RETURN "priv_dance_idle"
		BREAK
		CASE SCPDA_PRIVATE_DANCE1
			RETURN "priv_dance_p1"
		BREAK
		CASE SCPDA_PRIVATE_DANCE2
			RETURN "priv_dance_p2"		
		BREAK
		CASE SCPDA_PRIVATE_DANCE3
			RETURN "priv_dance_p3"
		BREAK
		CASE SCPDA_PICK_UP_MONEY
			RETURN "priv_dance_exit"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Invalid stripclub poledance anim enum")
	RETURN ""
ENDFUNC

FUNC STRING GET_STRIP_CLUB_ANIM_GOTO_POLE_FROM_POLE(INT iFromPole, INT iToPole)
	SWITCH iFromPole
		CASE 0
			SWITCH iToPole
				CASE 1
					RETURN "pole_c_2_pole_b"
				BREAK
				CASE 2
					RETURN "pole_c_2_pole_a"
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iToPole
				CASE 0
					RETURN "pole_b_2_pole_c"
				BREAK
				CASE 2
					RETURN "pole_b_2_pole_a"
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iToPole
				CASE 0
					RETURN "pole_a_2_pole_c"
				BREAK
				CASE 1
					RETURN "pole_a_2_pole_b"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid pole for pole to pole anim name")
	RETURN ""
ENDFUNC

FUNC STRIP_CLUB_BAR_ANIM_ENUM GET_BAR_ANIM_ENUM_FOR_TREVOR(INT iDrunkness)
	IF iDrunkness <= 0
		RETURN SCBA_DRUNK1
	ELIF iDrunkness = 1
		RETURN SCBA_DRUNK2
	ELIF iDrunkness = 2
		RETURN SCBA_DRUNK3
	ELSE
		IF iDrunkness % 2 = 0
			RETURN SCBA_DRUNK2
		ELSE
			RETURN SCBA_DRUNK3
		ENDIF
	ENDIF
ENDFUNC

FUNC STRIP_CLUB_BAR_ANIM_ENUM GET_BAR_ANIM_ENUM_FROM_INDEX(INT iDrunkness)
	IF iDrunkness <= 0
		RETURN SCBA_DRUNK1
	ELIF iDrunkness = 1
		RETURN SCBA_DRUNK2
	ELIF iDrunkness = 2
		RETURN SCBA_DRUNK3
	ELSE
		RETURN SCBA_DRUNK4
	ENDIF
ENDFUNC

FUNC STRING GET_POLE_TO_RAIL_ANIM_NAME(INT iRailIndex)
	SWITCH iRailIndex
		CASE 0
			RETURN "Pole_C_2_PrvD_A"
		BREAK
		CASE 1
			RETURN "Pole_C_2_PrvD_B"
		BREAK
		CASE 2
			RETURN "Pole_C_2_PrvD_C"
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid iRailIndex for pole to rail anim name")
	RETURN ""
ENDFUNC

FUNC STRING GET_POLE_DANCE_ANIM_FROM_POLE_INDEX(INT iPole, BOOL bToPole)
	
	SWITCH iPole
		CASE 0
			IF bToPole
				RETURN GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_STAGE_TO_POLE_C)
			ELSE
				RETURN GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_C_TO_STAGE)
			ENDIF
		BREAK
		CASE 1
			IF bToPole
				RETURN GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_STAGE_TO_POLE_B)
			ELSE
				RETURN GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_B_TO_STAGE)
			ENDIF
		BREAK
		CASE 2
			IF bToPole
				RETURN GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_STAGE_TO_POLE_A)
			ELSE
				RETURN GET_STRIP_CLUB_POLEDANCE_ANIM(SCPDA_POLE_A_TO_STAGE)
			ENDIF
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("Invalid pole for stage to pole anim dict")
	RETURN ""
ENDFUNC

#IF DEFINED(MP_STRIPCLUB)

PROC REQUEST_STRIPPER_IDLES()
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE())
	SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_STRIPPER_IDLES_STREAMED)
ENDPROC

PROC REMOVE_STRIPPER_IDLES()
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE())
	CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_STRIPPER_IDLES_STREAMED)
ENDPROC

FUNC BOOL HAVE_STRIPPER_IDLES_LOADED()
	
	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_STRIPPER_IDLES_STREAMED)
		RETURN HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE())
	ENDIF

//	PRINTLN("Check if stripper idles are loaded when you have not requested them")
	RETURN FALSE
ENDFUNC

FUNC INT GET_STRIPPER_IDLE_PLAYING(PED_INDEX pedIndex)

	IF NOT HAVE_STRIPPER_IDLES_LOADED()
		RETURN 0
	ENDIF

	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(1))
		RETURN 1
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(2))
		RETURN 2
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(3))
		RETURN 3
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(4))
		RETURN 4
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(5))
		RETURN 5
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_STRIPPER_IDLE(), GET_STRIP_CLUB_ANIM_STRIPPER_IDLE_NAME(6))
		RETURN 6
	ENDIF
	
	RETURN 0

ENDFUNC

#ENDIF

FUNC INT GET_BOUNCER_IDLE_PLAYING(PED_INDEX pedIndex)

	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(0), GET_STRIP_CLUB_ANIM_BOUNCER_IDLES(0))
		RETURN 0
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(1), GET_STRIP_CLUB_ANIM_BOUNCER_IDLES(1))
		RETURN 1
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(2), GET_STRIP_CLUB_ANIM_BOUNCER_IDLES(2))
		RETURN 2
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedIndex, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(3), GET_STRIP_CLUB_ANIM_BOUNCER_IDLES(3))
		RETURN 3
	ENDIF
	
	RETURN -1

ENDFUNC

FUNC BOOL HAVE_ALL_STRIP_CLUB_DANCE_INTRO_ANIMS_LOADED(BOOL bDuo)
	
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_APPROACH(bDuo))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(bDuo))
	
	IF  HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_DANCE_APPROACH(bDuo))
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(bDuo))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_BOUNCER_ANIMS_LOADED(LoadQueueLarge &sLoadQueue)
	
	RETURN HAS_LOAD_QUEUE_LARGE_LOADED(sLoadQueue)

ENDFUNC

PROC REMOVE_LAPDANCE_ANIMS()
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(0))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(1))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(2))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(0, TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(1, TRUE))
//	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(2, TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_REACH(TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_REACH(FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(TRUE,  FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE_EXIT(FALSE, TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(FALSE))
	
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_APPROACH(FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_APPROACH(TRUE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(FALSE))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(TRUE))
ENDPROC

PROC REQUEST_ALL_BOUNCER_ANIMS(LoadQueueLarge &sLoadQueue)
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(0))
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(1))
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(2))
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, GET_STRIP_CLUB_ANIM_DICT_ADD_BOUNCER_IDLES(3))
ENDPROC

PROC REQUEST_ALL_LEAN_ANIMS()

	REQUEST_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IN))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_OUT))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW_MANY))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE))

ENDPROC

PROC REMOVE_ALL_LEAN_ANIMS()

	REMOVE_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IN))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_OUT))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW_MANY))	
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE))

ENDPROC

PROC REMOVE_ALL_POLE_ANIMS()
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(1))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(2))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE(3))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(0))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(0))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(1))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(1))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_POLE(2))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GOTO_STAGE(2))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(0))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(1))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_GO_TO_RAIL(2))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_ENTER())
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_POLE_DANCE_EXIT())
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_ENTER_STAGE())
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_EXIT_STAGE())
ENDPROC

PROC  REMOVE_ALL_PRIVATE_DANCE_ANIMS()
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE_IDLE())
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(1))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(2))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_PRIVATE_DANCE(3))
	REMOVE_ANIM_DICT(GET_STRIP_CLUB_PICK_UP_MONEY_DICT())
ENDPROC

PROC REMOVE_ALL_DJ_DICTIONARIES()

	INT iVariation
	REPEAT 5 iVariation
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DJ_DICT(iVariation + 1))
	ENDREPEAT

ENDPROC

#IF DEFINED(MP_STRIPCLUB)
INT eNetworkSyncFlags

PROC STRIPCLUB_CREATE_SYNC_SCENE( INT &iSceneID, VECTOR scenePosition, VECTOR sceneOrientation, BOOL bHoldLastFrame, BOOL bIsMp, BOOL bForceSP)
	bForceSP = FALSE

	IF (IS_STRIPCLUB_MP() OR bIsMp)
	AND NOT bForceSP
		iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(scenePosition, sceneOrientation, EULER_YXZ, bHoldLastFrame)
	ELSE
		iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneOrientation)
		IF NOT IS_STRIPCLUB_MP()
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID, bHoldLastFrame)
		ENDIF
	ENDIF

ENDPROC

PROC STRIPCLUB_TASK_SYNC_SCENE(PED_INDEX ped, INT &iSceneID, STRING sAnimDic, STRING sAnimName,  FLOAT blendInDelta, FLOAT blendOutDelta, SYNCED_SCENE_PLAYBACK_FLAGS flags, BOOL bIsMp, BOOL bForceSP)
	bForceSP = FALSE
	
	IF NOT DOES_ENTITY_EXIST(ped) OR IS_ENTITY_DEAD(ped)
		EXIT
	ENDIF
	
	IF (IS_STRIPCLUB_MP() OR bIsMp)
	AND NOT bForceSP
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(ped, iSceneID, sAnimDic, sAnimName, blendInDelta, blendOutDelta, flags)
	ELSE
		TASK_SYNCHRONIZED_SCENE(ped, iSceneID, sAnimDic, sAnimName, blendInDelta, blendOutDelta, flags)
	ENDIF
	
ENDPROC

PROC STRIPCLUB_TASK_OBJECT_SYNC_SCENE(OBJECT_INDEX objIndex, INT &iSceneID, STRING sAnimDic, STRING sAnimName,  FLOAT blendInDelta, FLOAT blendOutDelta, SYNCED_SCENE_PLAYBACK_FLAGS flags, BOOL bIsMp, BOOL bForceSP)
	bForceSP = FALSE
	
	IF NOT DOES_ENTITY_EXIST(objIndex)
		EXIT
	ENDIF
	
	PRINTLN("Play animations ", sAnimName, " dict ", sAnimDic)
	
	IF (IS_STRIPCLUB_MP() OR bIsMp)
	AND NOT bForceSP
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objIndex, iSceneID, sAnimDic, sAnimName, blendInDelta, blendOutDelta, flags)
	ELSE
		PLAY_SYNCHRONIZED_ENTITY_ANIM(objIndex, iSceneID, sAnimName, sAnimDic, blendInDelta, blendOutDelta, ENUM_TO_INT(flags))
	ENDIF
ENDPROC

PROC STRIPCLUB_SET_SYNC_SCENE_CAM(INT &iSceneID, STRING sAnimDic, STRING sAnimName, BOOL bIsMp, BOOL bForceSP)
	bForceSP = FALSE

	IF (IS_STRIPCLUB_MP() OR bIsMp)
	AND NOT bForceSP
		NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(iSceneID, sAnimDic, sAnimName)
	ELSE
		PLAY_SYNCHRONIZED_CAM_ANIM(stripCamAnim, iSceneID, sAnimName, sAnimDic) 
		SET_CAM_ACTIVE(stripCamAnim, TRUE)
	ENDIF
	
	STRIPCLUB_HANDLE_DRUNK_SHAKE(stripCamAnim)
	
	SET_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_SYNC_CAMERA_THIS_FRAME)
ENDPROC

PROC STRIPCLUB_START_SYNC_SCENE(INT &iSceneID, BOOL bIsMp = FALSE)

	IF IS_STRIPCLUB_MP()
	OR bIsMp
		INT iLocalID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSceneID)
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalID)
			NETWORK_START_SYNCHRONISED_SCENE(iSceneID)
		ENDIF
	ELSE
		//single player sync scenes start as soon as you task them
	ENDIF

ENDPROC

PROC STRIPCLUB_END_SYNC_SCENE(INT &iSceneID, BOOL bIsMp = FALSE)

	IF iSceneID = -1
		EXIT //invalid scene id
	ENDIF

	IF IS_STRIPCLUB_MP()
	OR bIsMp
		NETWORK_STOP_SYNCHRONISED_SCENE(iSceneID)
	ELSE
		
	ENDIF
	
	iSceneID = -1
ENDPROC

FUNC INT GET_SRTIPCLUB_LOCAL_SCENE_ID(INT iSceneID, BOOL bIsMp = FALSE)

	IF IS_STRIPCLUB_MP()
	OR bIsMp
		RETURN NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSceneID)
	ELSE
		RETURN iSceneID
	ENDIF
ENDFUNC
#ENDIF

