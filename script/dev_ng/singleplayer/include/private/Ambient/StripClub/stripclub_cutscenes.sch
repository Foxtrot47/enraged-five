USING "stripclub_workers.sch"

#IF NOT MP_STRIPCLUB
USING "stripclub_friend.sch"
#ENDIF

PROC SET_STRIPCLUB_VIEWPORT_FOR_ROOM(STRIPCLUB_ROOMS enumRoom)
enumRoom = enumRoom
//	CLEAR_ROOM_FOR_GAME_VIEWPORT()
//	
//	IF stripClubInterior = NULL
//		PRINTLN("When setting viewport stripClubInterior is NULL?")
//		EXIT
//	ENDIF
//	
//	SWITCH enumRoom
//		CASE SCLUB_ROOM_MAIN
//			PRINTLN("Set stripclub room to main")
//			FORCE_ROOM_FOR_GAME_VIEWPORT(GET_INTERIOR_AT_COORDS_with_type(<<119.6, -1286.6, 29.3>>, "v_strip3"), HASH("strp3mainrm"))
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("strp3mainrm")
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(HASH("strp3mainrm"))
//		BREAK
//		CASE SCLUB_ROOM_VIP
//			PRINTLN("Set stripclub room to vip")
//			FORCE_ROOM_FOR_GAME_VIEWPORT(GET_INTERIOR_AT_COORDS_with_type(<<119.6, -1286.6, 29.3>>, "v_strip3"), HASH("strp3chngrm"))
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("strp3chngrm")
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(HASH("strp3chngrm"))
//		BREAK
//		CASE SCLUB_ROOM_DRESSING
//			FORCE_ROOM_FOR_GAME_VIEWPORT(GET_INTERIOR_AT_COORDS_with_type(<<119.6, -1286.6, 29.3>>, "v_strip3"), HASH("strp3chngrm"))
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("strp3chngrm")
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(HASH("strp3chngrm"))
//		BREAK
//		CASE SCLUB_ROOM_BACK
//			FORCE_ROOM_FOR_GAME_VIEWPORT(GET_INTERIOR_AT_COORDS_with_type(<<119.6, -1286.6, 29.3>>, "v_strip3"), HASH("strp3off"))
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("strp3off")
//			//SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(HASH("strp3off"))
//		BREAK	
//	ENDSWITCH
	//"strp2bkpss" 4 //back passage way
	//"strp3stge"  6 //stage

ENDPROC

// setup camera
PROC SETUP_STRIPCLUB_CAM(CAMERA_INDEX &stripCam, STRIPCLUB_CAM_ENUM camType, VECTOR vFPSCamPos, VECTOR vFPSCamRot, BOOL bDisplayHud = FALSE)	
	STRIPCLUB_CAM_PARAMS structCamParams
	VECTOR vChairOffset
	FLOAT fNearClip = -1.0
	FLOAT fFirstPersonXLimit, fFirstPersonZLimit
	BOOL bShakeCam = FALSE
	FLOAT fHeading, fTilt
	
	SWITCH camType
		CASE STRCLUB_CAM_DANCE_FIRST_PERSON
			SWITCH thisStripClub
				CASE STRIP_CLUB_LOW
					IF iCurrentLapDanceSeat > -1

						vChairOffset = <<0.0, -0.135, 0.78 >>
					
						structCamParams.vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_VIP_SEAT_POS(iCurrentLapDanceSeat), GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat), vChairOffset)
						structCamParams.vCamRot = <<0,0, GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat)>> 
						structCamParams.fCamFOV = 36.0
						fNearClip = 0.01
						fFirstPersonXLimit = FIRST_PERSON_CAM_MOVE_X_LIMIT_LAP
						fFirstPersonZLimit = FIRST_PERSON_CAM_MOVE_Z_LIMIT_LAP
						SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_VIP)
					ENDIF
				BREAK
				/*
				CASE STRIP_CLUB_MED
					vstripclubCamPos = <<-372.87, 211.95, 86.40>>
					vstripclubCamRot = <<-30.24, 0.00, -113.08>>
					fstripclubCamFOV = 50.59
				BREAK
				CASE STRIP_CLUB_HIGH
					vstripclubCamPos = <<968.52, -1799.10, 33.05>>
					vstripclubCamRot = <<-20.06, 0.00, 166.69>>
					fstripclubCamFOV = 59.60
					sstripClubCamRoom = "stripprvrooms"
				BREAK
				*/
			ENDSWITCH
		BREAK
		
		CASE STRCLUB_CAM_DANCE_FACE_PLAYER

			vChairOffset = GET_VIP_CAM_OFFSET(STRCLUB_CAM_DANCE_FACE_PLAYER, iCurrentLapDanceSeat, iSecondaryStripper >=0)
			IF iSecondaryStripper >=0
				fHeading = 140.76
				fTilt = 358.2
			ELSE
				fHeading = 155.52
				fTilt = 1.79
			ENDIF
			
			structCamParams.vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_VIP_SEAT_POS(iCurrentLapDanceSeat), GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat), vChairOffset)
			structCamParams.vCamRot = <<fTilt, 0, fHeading+GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat)>> 
			structCamParams.fCamFOV = 36.0
			SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_VIP)
			bShakeCam = TRUE
			
			fFirstPersonXLimit = 10.0
			fFirstPersonZLimit = 10.0
		BREAK
		
		CASE STRCLUB_CAM_DANCE_BEHIND_PLAYER
			vChairOffset = GET_VIP_CAM_OFFSET(STRCLUB_CAM_DANCE_BEHIND_PLAYER, iCurrentLapDanceSeat, iSecondaryStripper >=0)
			IF iSecondaryStripper >=0
				fHeading = 322.2
				fTilt = 360
			ELSE
				fHeading = 318.27
				fTilt = 2.802852
			ENDIF
			
			structCamParams.vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_VIP_SEAT_POS(iCurrentLapDanceSeat), GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat), vChairOffset)
			structCamParams.vCamRot = <<fTilt, 0, fHeading+GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat)>> 
			structCamParams.fCamFOV = 51.68
			SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_VIP)
			bShakeCam = TRUE
			
			fFirstPersonXLimit = 10.0
			fFirstPersonZLimit = 10.0
		BREAK
		
		CASE STRCLUB_CAM_DANCE_ANGLED

			vChairOffset = GET_VIP_CAM_OFFSET(STRCLUB_CAM_DANCE_ANGLED, iCurrentLapDanceSeat, iSecondaryStripper >=0)
			IF iSecondaryStripper >=0
				fHeading = 182.830
				fTilt = 338.040
			ELSE
				fHeading = 221.92
				fTilt = 32.69
			ENDIF
			
			structCamParams.vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_VIP_SEAT_POS(iCurrentLapDanceSeat), GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat), vChairOffset)
			structCamParams.vCamRot = <<fTilt, 0, fHeading+GET_VIP_SEAT_HEADING(iCurrentLapDanceSeat)>> 
			structCamParams.fCamFOV = 36.0
			SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_VIP)
			bShakeCam = TRUE
			
			fFirstPersonXLimit = 10.0
			fFirstPersonZLimit = 10.0
		BREAK
		
		CASE STRCLUB_CAM_BAR
			SWITCH thisStripClub
				CASE STRIP_CLUB_LOW
					structCamParams.vCamPos = << 129.2621, -1285.3774, 30.1676 >> // <<131.52, -1285.34, 30.39>>
					structCamParams.vCamRot = << -12.2257, 0.0000, 29.7656 >> //<<-12.72, 0.00, 59.23>>
					structCamParams.fCamFOV = 34.44
					SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_MAIN)
				BREAK
				/*
				CASE STRIP_CLUB_MED
					vstripclubCamPos = <<-378.95, 213.16, 83.54>>
					vstripclubCamRot = <<11.86, 0.00, 136.43>>
					fstripclubCamFOV = 31.14
				BREAK
				CASE STRIP_CLUB_HIGH
					vstripclubCamPos = <<973.91, -1806.03, 34.79>>
					vstripclubCamRot = <<-40.99, 0.00, -57.96>>
					fstripclubCamFOV = 51.15
				BREAK
				*/
			ENDSWITCH
		BREAK
		
		CASE STRIPCLUB_CAM_THROW_OUT
			SWITCH thisStripClub
				CASE STRIP_CLUB_LOW
					structCamParams.vCamPos = << 132.1790, -1303.4816, 30.3518 >>
					structCamParams.vCamRot = << -19.4213, -0.0000, 31.7312 >>
					structCamParams.fCamFOV = 50.0
					//CLEAR_ROOM_FOR_GAME_VIEWPORT()
				BREAK
			ENDSWITCH
		BREAK
		
		CASE STRCLUB_CAM_RAIL
			structCamParams.vCamPos = vFPSCamPos
			structCamParams.vCamRot = vFPSCamRot
			structCamParams.fCamFOV = 36.0
			fNearClip = 0.25
			fFirstPersonXLimit = FIRST_PERSON_CAM_MOVE_X_LIMIT
			fFirstPersonZLimit = 20.0
			SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_MAIN)
		BREAK
		
		CASE STRCLUB_CAM_CALL_STRIPPER
			structCamParams.vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), <<0.0, 1.45, 0.70>>)
			structCamParams.vCamRot = <<-15.0, 0, GET_ENTITY_HEADING(PLAYER_PED_ID()) +180>>
			structCamParams.fCamFOV = 51.21
			SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_MAIN)
		BREAK
	ENDSWITCH
		
	CLEAR_FIRST_PERSON_CAMERA(fpStripCam)
//	CLEAR_FIRST_PERSON_CAMERA(fpStripCam2)
	IF DOES_CAM_EXIST(stripCam)
		DESTROY_CAM(stripCam)
	ENDIF
	
	IF camType = STRCLUB_CAM_DANCE_FIRST_PERSON OR camType = STRCLUB_CAM_RAIL 
		IF NOT DOES_CAM_EXIST(fpStripCam.theCam)
			INIT_FIRST_PERSON_CAMERA(fpStripCam, structCamParams.vCamPos, structCamParams.vCamRot, structCamParams.fCamFOV, 
									CEIL(fFirstPersonZLimit), CEIL(fFirstPersonXLimit), 0, 20, FALSE, 0, fNearClip )
		ENDIF
		stripCam = fpStripCam.theCam
//	ELIF camType = STRCLUB_CAM_DANCE_FACE_PLAYER OR camType = STRCLUB_CAM_DANCE_ANGLED OR camType = STRCLUB_CAM_DANCE_BEHIND_PLAYER
//		IF NOT DOES_CAM_EXIST(fpStripCam2.theCam)
//			INIT_FIRST_PERSON_CAMERA(fpStripCam2, structCamParams.vCamPos, structCamParams.vCamRot, structCamParams.fCamFOV, 
//									CEIL(fFirstPersonZLimit), CEIL(fFirstPersonXLimit), 0, 20, FALSE, 0, fNearClip )
//		ENDIF
//		stripCam = fpStripCam2.theCam
	ENDIF
		
	IF NOT DOES_CAM_EXIST(stripCam)
		stripCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
	ENDIF
	DETACH_CAM(stripCam)
	
	IF camType = STRCLUB_CAM_DANCE_FIRST_PERSON
		ATTACH_CAM_TO_PED_BONE(stripCam ,PLAYER_PED_ID(),  BONETAG_HEAD, <<0.0, -0.015, 0.040>>)
	ELSE
		SET_CAM_COORD(stripCam, structCamParams.vCamPos)
	ENDIF
	
	SET_CAM_ROT(stripCam, structCamParams.vCamRot)
	SET_CAM_FOV(stripCam, structCamParams.fCamFOV)
	STOP_CAM_POINTING(stripCam)
	SET_CAM_ACTIVE(stripCam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	DISPLAY_HUD(bDisplayHud)
	
	IF camType > STRCLUB_CAM_DANCE_ANGLED
		DISPLAY_RADAR(FALSE)
	ENDIF
	
	IF STRIPCLUB_HANDLE_DRUNK_SHAKE(stripCam)
		PRINTLN("Setting drunk shake for scripted cam")
	ELIF bShakeCam
		SHAKE_CAM(stripCam, "HAND_SHAKE", 0.5)
	ENDIF

ENDPROC

PROC USE_BOUNCER_CAM()

	IF NOT DOES_CAM_EXIST(stripclubCamBouncer)
		PRINTLN("Create Bouncer Cam")
		stripclubCamBouncer = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		
		SET_CAM_COORD(stripclubCamBouncer, << 117.4966, -1300.7878, 30.3078 >>)
		SET_CAM_ROT(stripclubCamBouncer, << -17.6944, -0.0000, 23.1981 >> )
		SET_CAM_FOV(stripclubCamBouncer, 50.0)
		SET_CAM_ACTIVE(stripclubCamBouncer, TRUE)
		//RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
	
ENDPROC


PROC TELEPORT_STRIPPERS_FOR_VIP_CUTSCENE()
	INT idx
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() idx
		CPRINTLN(DEBUG_SCLUB, "DO_STAGE_GO_TO_ROOM: Stripper ", idx, " is in state ", ENUM_TO_INT(GET_POLEDANCER_STATE(idx)), " at position of ", GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(namedStripper[idx].ped, FALSE)))
		IF GET_POLEDANCER_STATE(idx) = POLEDANCER_STATE_GOTO_STAGE_INTRO
			//SET_ENTITY_COORDS(<<103.8784, -1300.7552, 27.7688>>,
			SET_POLEDANCER_STATE(idx, POLEDANCER_STATE_DANCE)
			IF DOES_ENTITY_EXIST(namedStripper[idx].ped)
				SET_ENTITY_COORDS(namedStripper[idx].ped, <<102.5099, -1298.7952, 28.7739>>, FALSE, TRUE)
			ENDIF
			CPRINTLN(DEBUG_SCLUB, "DO_STAGE_GO_TO_ROOM: Setting poledancer state for stripper walking to dressing room to POLEDANCER_STATE_DANCE. Should teleport")
		ELIF GET_STRIPPER_AI_STATE(idx) = AI_STATE_WANDER
			IF DOES_ENTITY_EXIST(namedStripper[idx].ped)
				VECTOR vTempStripperCoords
				vTempStripperCoords = GET_ENTITY_COORDS(namedStripper[idx].ped)
				IF VDIST(vTempStripperCoords, <<114.5, -1296.4, 29.7 >>) < 6.5
					IF vTempStripperCoords.z > 29.2
						SET_ENTITY_COORDS(namedStripper[idx].ped, <<115.8838, -1291.0198, 27.2610>>, FALSE, TRUE)
						SET_ENTITY_HEADING(namedStripper[idx].ped, 0.1092)
						CPRINTLN(DEBUG_SCLUB, "DO_STAGE_GO_TO_ROOM: Setting poledancer state for stripper walking to remain wander and warping. Should teleport")
					ENDIF
				ENDIF
			ENDIF
		ELIF GET_STRIPPER_AI_STATE(idx) = AI_STATE_POLE_DANCE
		AND GET_POLEDANCER_STATE(idx) >= POLEDANCER_STATE_DANCE_EXIT //stripper is leaving the pole dance, set her to wander
			IF DOES_ENTITY_EXIST(namedStripper[idx].ped)
				CLEAR_PED_TASKS_IMMEDIATELY(namedStripper[idx].ped)
				SET_ENTITY_COORDS(namedStripper[idx].ped, <<115.8838, -1291.0198, 27.2610>>, FALSE, TRUE)
				SET_ENTITY_HEADING(namedStripper[idx].ped, 0.1092)
				SET_STRIPPER_AI_STATE(idx, AI_STATE_WANDER)
				SET_POLEDANCER_STATE(idx, POLEDANCER_STATE_WAIT_IN_DRESSING_ROOM)
			ENDIF
		ENDIF 
	ENDREPEAT
ENDPROC


PROC SETUP_STRIPPER_DANCE_INTRO_CAM()

	SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_MAIN)
	
	IF NOT DOES_CAM_EXIST(stripclubCamStart)
		stripclubCamStart = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(stripclubCamEnd)
		stripclubCamEnd = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF

	SET_CAM_COORD(stripclubCamStart, <<120.4634, -1295.2773, 29.7474>>) 
	SET_CAM_ROT(stripclubCamStart, <<1.0747, -0.0144, 85.2721>>)
	SET_CAM_FOV(stripclubCamStart, 30)
	
	SET_CAM_COORD(stripclubCamEnd,  <<120.4634, -1295.2773, 29.7474>>) 
	SET_CAM_ROT(stripclubCamEnd, <<16.4732, -0.0144, 85.2721>>)
	SET_CAM_FOV(stripclubCamEnd, 30)
	
	SET_CAM_ACTIVE_WITH_INTERP(stripclubCamEnd, stripclubCamStart, 5000)
		
	DISPLAY_HUD(FALSE)
	DISPLAY_RADAR(FALSE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	STRIPCLUB_HANDLE_DRUNK_SHAKE(stripclubCamEnd)
ENDPROC

PROC SETUP_STRIPPER_DANCE_OUTRO_CAM(CAMERA_INDEX &stripCam)
	SET_STRIPCLUB_VIEWPORT_FOR_ROOM(SCLUB_ROOM_MAIN)
	
	IF NOT DOES_CAM_EXIST(stripCam)
		stripCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(stripclubCamStart)
		stripclubCamStart = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(stripclubCamEnd)
		stripclubCamEnd = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF
	
	SET_CAM_COORD(stripclubCamStart, <<121.0, -1291.7, 29.8>>)
	SET_CAM_ROT(stripclubCamStart, <<8.1, 0.0, 129.1>>)
	SET_CAM_FOV(stripclubCamStart, 30)
	
	SET_CAM_COORD(stripclubCamEnd, <<121.0, -1291.7, 29.8>>)
	SET_CAM_ROT(stripclubCamEnd, <<-3.7, 0, 141.0>>)
	SET_CAM_FOV(stripclubCamEnd, 30)
	
	SET_CAM_ACTIVE_WITH_INTERP(stripclubCamEnd, stripclubCamStart, 5500)
			
	DISPLAY_HUD(FALSE)
	DISPLAY_RADAR(FALSE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

FUNC BOOL PLAY_OUTRO_CUTSCENE(BOOL bTaskStripperToClub = TRUE)
	IF IS_STRIPCLUB_MP()
//		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//		FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_DEFAULT)
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
		RETURN TRUE
	ENDIF
	
	DISPLAY_HUD(FALSE)
	DISPLAY_RADAR(FALSE)
	
	
	IF IS_SHOW_LIKE_HUD_SET()
		SET_SHOW_LIKE_HUD(FALSE)
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_PLAYER_TASKED)
		
		IF (GET_TIMER_IN_SECONDS(tControl) > 5.5)
		OR (IS_CUTSCENE_SKIP_BUTTON_PRESSED() AND GET_TIMER_IN_SECONDS(tControl) > 2.0)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_SCENE)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_PLAYER_TASKED)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_BOUNCER_TASKED)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE)
			
			IF DOES_ENTITY_EXIST(clubStaffPed[STAFF_BOUNCER_VIP]) AND NOT IS_ENTITY_DEAD(clubStaffPed[STAFF_BOUNCER_VIP])
				//enumBouncerStates =  BOUNCER_MOVE_ASIDE_EXIT
			ENDIF
			
			iNextBouncerYellTime = GET_GAME_TIMER() + 15000
			RETURN TRUE
		ENDIF
		
	ELSE
		IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_SCENE)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				MAKE_STRIPPER_PED_LOW_QUALITY(iCurrentStripperForDance)
				MAKE_STRIPPER_PED_LOW_QUALITY(iSecondaryStripper)
				
				IF bTaskStripperToClub
					IF NOT IS_ENTITY_DEAD(namedStripper[iCurrentStripperForDance].ped)
						STRIPCLUB_SET_STRIPPER_FOR_EXIT_VIP_CUTSCENE(iCurrentStripperForDance, GET_STRIPPER_VIP_EXIT_START_POINT(FALSE), SWR_VIP_OUTRO)
					ENDIF
					
					IF iSecondaryStripper >= 0
						STRIPCLUB_SET_STRIPPER_FOR_EXIT_VIP_CUTSCENE(iSecondaryStripper, GET_STRIPPER_VIP_EXIT_START_POINT(TRUE), SWR_VIP_OUTRO)
					ENDIF
				ELSE
					IF NOT IS_ENTITY_DEAD(namedStripper[iCurrentStripperForDance].ped)
						STRIPCLUB_SET_STRIPPER_FOR_EXIT_VIP_CUTSCENE(iCurrentStripperForDance, <<115.9899, -1298.7009, 28.0190>>, SWR_STRIPPER_EXIT)
					ENDIF
				ENDIF
				
				SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iCurrentStripperForDance].ped, namedStripper[iCurrentStripperForDance].stripperID, FALSE)
				IF iSecondaryStripper >= 0
					SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iSecondaryStripper].ped, namedStripper[iSecondaryStripper].stripperID, FALSE)
				ENDIF
				
				SETUP_STRIPPER_DANCE_OUTRO_CAM(stripclubCutCam)
				
				TELEPORT_STRIPPERS_FOR_VIP_CUTSCENE()
				
				RESTART_TIMER_NOW(tControl)
				
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_SCENE)
			ENDIF
			
			CLEAR_ALL_STRIP_CLUB_HELP()
			SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE)
			
		ELIF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_PLAYER_TASKED)
			//IF (GET_TIMER_IN_SECONDS(tControl) > 1.5)
				
				VECTOR vTempProbe
				vTempProbe = <<114.4527, -1295.6735, 29.2690>>
				GET_GROUND_Z_FOR_3D_COORD(vTempProbe, vTempProbe.z)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vTempProbe)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 290.0659 )
				UNFREEZE_PLAYER_IN_CLUB() //frozen from lapdance
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_DYNAMIC(PLAYER_PED_ID(), TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				CLEAR_AREA_OF_PEDS( <<124.2252, -1291.2114, 28.2795>>, 5.0)
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<124.2252, -1291.2114, 28.2795>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_DONT_AVOID_OBJECTS | ENAV_DONT_AVOID_PEDS)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				
				/*
				IF DOES_ENTITY_EXIST(clubStaffPed[STAFF_BOUNCER_VIP]) AND NOT IS_ENTITY_DEAD(clubStaffPed[STAFF_BOUNCER_VIP])
					vTempProbe = <<113.80, -1296.79, 28.27>>
					GET_GROUND_Z_FOR_3D_COORD(vTempProbe, vTempProbe.z)
					SET_ENTITY_COORDS(clubStaffPed[STAFF_BOUNCER_VIP], vTempProbe)
					TASK_STAND_STILL(clubStaffPed[STAFF_BOUNCER_VIP], -1)
				ENDIF
				//*/
				
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_OUTRO_DANCE_PLAYER_TASKED)
			//ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_GET_SECONDARY_STRIPPER_CUTSCENE()
	SETUP_STRIPPER_DANCE_INTRO_CAM()
	CLEAR_ALL_STRIP_CLUB_HELP()
	
	IF GET_STRIPPER_AI_STATE(iSecondaryStripper) = AI_STATE_POLE_DANCE
		SET_POLEDANCER_STATE(iSecondaryStripper, POLEDANCER_STATE_WAIT_IN_DRESSING_ROOM)
	ENDIF

	IF DOES_ENTITY_EXIST(namedStripper[iSecondaryStripper].ped)
		MAKE_STRIPPER_PED_HIGH_QUALITY(iSecondaryStripper)
		STRIPCLUB_SET_STRIPPER_FOR_VIP_CUTSCENE(iSecondaryStripper, <<119.4184, -1293.5980, 28.2820>>, FALSE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(namedStripper[iSecondaryStripper].ped)
	ENDIF
	
	REMOVE_LAPDANCE_ANIMS()
	
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(TRUE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(0, TRUE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_LAPDANCE(1, TRUE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_REACH(TRUE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER(TRUE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_ACCEPT(TRUE))
	REQUEST_ANIM_DICT(GET_STRIP_CLUB_AIM_DICT_LAPDANCE_OFFER_DECLINE(TRUE))
	
//	IF NOT IS_PED_INJURED(clubStaffPed[STAFF_BOUNCER_VIP])
//		SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(GET_ROOM_KEY_FROM_ENTITY(clubStaffPed[STAFF_BOUNCER_VIP]))
//	ENDIF
	
	TEXT_LABEL_23 sFixedup = STRIPCLUB_FIXUP_PLAYER_LABEL("SC_GET_2ND")
	CREATE_CONVERSATION(stripClubConversation, "SCAUD", sFixedup, GET_STRIPCLUB_SPEECH_PRIORITY())
	
	ADJUST_STRIPPER_LIKE_VALUE(iCurrentStripperForDance, STRIPPER_LIKE_VALUE_DANCE, TRUE)
	SET_STRIPPER_AI_STATE(iSecondaryStripper, AI_STATE_GIVE_DANCE)
	RESTART_TIMER_NOW(tControl)	
ENDPROC

FUNC BOOL PLAY_SECONDARY_STRIPPER_CUTSCENE()

	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_DOUBLE_DANCE_SYNC_SCENE)
		STRIPCLUB_SET_SYNC_SCENE_CAM(iLapDanceScene, GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_CAM_ANIM(SCLDA_SIT, iSecondaryStripper >= 0), FALSE, FALSE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		CLEAR_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_DOUBLE_DANCE_SYNC_SCENE)
	ENDIF

	IF NOT IS_TIMER_STARTED(tControl)
	AND NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_DOUBLE_DANCE_SYNC_SCENE)
		//Stripper has walked threw door, play starting anim
		IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene))	
			IF GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iLapDanceScene)) >= 1.0
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELIF TIMER_DO_WHEN_READY(tControl, 5.0)
	AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(iSecondaryStripper >= 0))
		//stripper finished walking into room
		//CLEAR_ROOM_FOR_GAME_VIEWPORT()
		CLEAR_PED_TASKS_IMMEDIATELY(namedStripper[iSecondaryStripper].ped)
		
		SETUP_SCLUB_STRIPPER_VARIATION(namedStripper[iSecondaryStripper].ped, namedStripper[iSecondaryStripper].stripperID, TRUE) 
		
		VECTOR vSeatPos
		FLOAT fHeading
		
		vSeatPos =  GET_VIP_SEAT_ANIM_POSITION_AND_HEADING(iCurrentLapDanceSeat, fHeading)
		STRIPCLUB_CREATE_SYNC_SCENE(iLapDanceScene, vSeatPos, <<0,0, fHeading>>, TRUE, FALSE, FALSE)
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iCurrentStripperForDance].ped, iLapDanceScene,  GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_SIT, FALSE, iSecondaryStripper >= 0, FALSE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, FALSE)
		STRIPCLUB_TASK_SYNC_SCENE(namedStripper[iSecondaryStripper].ped, iLapDanceScene,  GET_STRIP_CLUB_ANIM_DICT_DANCE_SIT(iSecondaryStripper >= 0), GET_STRIP_CLUB_LAPDANCE_ANIM(SCLDA_SIT, FALSE, iSecondaryStripper >= 0, TRUE), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, FALSE, FALSE)
		
		STRIPCLUB_START_SYNC_SCENE(iLapDanceScene)

		TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_PLAYER_LAPDANCE_IDLE_DICT(0, TRUE), GET_PLAYER_LAPDANCE_IDLE_ANIM(0, TRUE), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION | AF_LOOPING | AF_NOT_INTERRUPTABLE, 0, FALSE, AIK_DISABLE_LEG_IK)

		FORCE_PED_AI_AND_ANIMATION_UPDATE(namedStripper[iCurrentStripperForDance].ped, TRUE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(namedStripper[iSecondaryStripper].ped, TRUE)

		SET_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_DOUBLE_DANCE_SYNC_SCENE)	
		CANCEL_TIMER(tControl)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_THOW_CUTSCENE()
	STRING sAnimDictName  = GET_STRIP_CLUB_ANIM_DICT_THROW_OUT(iThrowOutCutscene)
	VECTOR vStartPos
	FLOAT fStartHeading, fSceneTime, fStartDoorOpen, fEndDoorOpen
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_THROWOUT_STREAMED)
		PRINTLN("Loading throwout anim ", sAnimDictName)
		REQUEST_ANIM_DICT(sAnimDictName)
		SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_THROWOUT_STREAMED)
		//STRIPCLUB_CLOSE_FRONT_DOOR(FALSE, -1.0)
	ENDIF
	
	IF HAS_ANIM_DICT_LOADED(sAnimDictName)
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iThrowOutScene))
			PRINTLN("Create throwout scene")
			CLEAR_AREA_OF_PEDS(<<130.8747, -1302.7192, 28.2327>>, 10.0)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<127.5, -1314.6, 29.0>>)
			ENDIF
			
			IF NOT IS_PED_INJURED(clubStaffPed[STAFF_BOUNCER_PATROL])
				SET_ENTITY_COORDS(clubStaffPed[STAFF_BOUNCER_PATROL], <<127.5, -1314.6, 29.0>>)
			ENDIF
			
			IF NOT IS_PED_INJURED(clubStaffPed[STAFF_BOUNCER_VIP])
				SET_ENTITY_COORDS(clubStaffPed[STAFF_BOUNCER_VIP], <<127.5, -1314.6, 29.0>>)
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			UNFREEZE_PLAYER_IN_CLUB()
			SET_ENTITY_DYNAMIC(PLAYER_PED_ID(), TRUE)
			
			GET_THROWOUT_START_LOC( iThrowOutCutscene, vStartPos, fStartHeading)
			
			STRIPCLUB_CREATE_SYNC_SCENE(iThrowOutScene, vStartPos, <<0,0, fStartHeading>>, FALSE, FALSE, FALSE)
			STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(), iThrowOutScene, sAnimDictName, GET_STRIP_CLUB_BOUNCER_THOWOUT_ANIM(SCBA_PLAYER_THROW, iThrowOutCutscene), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT, FALSE, FALSE)
			STRIPCLUB_TASK_SYNC_SCENE(clubStaffPed[STAFF_BOUNCER_PATROL], iThrowOutScene, sAnimDictName, GET_STRIP_CLUB_BOUNCER_THOWOUT_ANIM(SCBA_BOUNCER_THROWA, iThrowOutCutscene), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT, FALSE, FALSE)
			STRIPCLUB_TASK_SYNC_SCENE(clubStaffPed[STAFF_BOUNCER_VIP], iThrowOutScene, sAnimDictName, GET_STRIP_CLUB_BOUNCER_THOWOUT_ANIM(SCBA_BOUNCER_THROWB, iThrowOutCutscene), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT, FALSE, FALSE)
			STRIPCLUB_SET_SYNC_SCENE_CAM(iThrowOutScene, sAnimDictName, GET_STRIP_CLUB_CAM_ANIM_THROW_OUT(iThrowOutCutscene), FALSE, FALSE)
			STRIPCLUB_START_SYNC_SCENE(iThrowOutScene)
	
			STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, 0.0)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			#IF NOT MP_STRIPCLUB
				TEXT_LABEL_23 sContextLabel = "SC_FRNKICK"
			
				IF NOT IS_PED_INJURED(clubFriends[0].ped)
					CLEAR_PED_TASKS(clubFriends[0].ped)
					SET_ENTITY_COORDS(clubFriends[0].ped, <<127.8327, -1287.6079, 28.2834>>)
					clubFriends[0].enumSCA_FriendState = SCA_FS_LEAVING
					STRIPCLUB_PLAY_FRIEND_CONVERSATION(0, sContextLabel)
				ENDIF
			#ENDIF
		ELSE
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2198887
			
			fSceneTime = GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iThrowOutScene))
			GET_THROWOUT_OPEN_DOOR_TIMES(iThrowOutCutscene, fStartDoorOpen, fEndDoorOpen)
			PRINTLN(sAnimDictName ," Scene time ", fSceneTime)
			//SET_SYNCHRONIZED_SCENE_LOOPED(GET_SRTIPCLUB_LOCAL_SCENE_ID(iThrowOutScene), TRUE)

			IF fSceneTime > fStartDoorOpen AND fSceneTime < fEndDoorOpen
				STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, -1.0 * ((fSceneTime - fStartDoorOpen) / (fEndDoorOpen - fStartDoorOpen))  )
			ELIF fSceneTime > fEndDoorOpen AND fSceneTime < fEndDoorOpen + 0.1
				STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, -1.0)
			ENDIF
			
			IF fSceneTime >= 0.75
				INT iLeftX, iLeftY, iRightX, iRightY
				STRIPCLUB_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY)
			
				IF ABSI(iLeftX) > 0 OR ABSI(iLeftY) > 0
					PRINTLN("Exit sync scene early")
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
				
			ENDIF
			
			IF fSceneTime >= 1.0
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		PRINTLN("Loading throw out")
			
		IF GET_RENDERING_CAM() = fpStripCam.theCam
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_MakeHeadInvisble, TRUE)
		ENDIF
	ENDIF
	//.22 .255
	//SET_ENTITY_COORDS(PLAYER_PED_ID(), <<127.5, -1314.6, 29.0>>)
	RETURN FALSE
ENDFUNC

PROC DO_STAGE_THROWOUT()
	
	IF  IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_LAPDANCE_STREAMED)
		REMOVE_LAPDANCE_ANIMS()
		CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_LAPDANCE_STREAMED)
	ENDIF
	
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
//	ENDIF
	
	IF PLAY_THOW_CUTSCENE()
		PRINTLN("Throwout over")
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_THROW_OUT(iThrowOutCutscene))
		CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_THROWOUT_STREAMED)
		
		CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_BOUNCER_YELL)
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BOUNCER_YELL())
		
		SET_STRIPCLUB_GLOBAL_THROWN_OUT(TRUE)
		SET_STRIPCLUB_GLOBAL_GETTING_LAPDANCE(FALSE)
		
		RESET_STRIP_CLUB_CAMS_TO_GAMEPLAY(FALSE, 0, TRUE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		UNFREEZE_PLAYER_IN_CLUB() //frozen from lapdance
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_DYNAMIC(PLAYER_PED_ID(), TRUE)
		DISABLE_CELLPHONE(FALSE)
		ENABLE_SELECTOR()
		
		SET_FORCE_NOT_IN_CLUB(TRUE)
		
		TouchingStripperState = TSS_NONE
		iCurrentLapDanceSeat = -1
		iCurrentStripperForDance = -1
		iCurrentStripperInteraction = -1
		STRIPCLUB_CLOSE_FRONT_DOOR(FALSE, 0)
		stripClubStage = STAGE_WANDER_CLUB
	ENDIF
	
ENDPROC


FUNC BOOL CAN_STRIPCLUB_POST_HEIST_SCENE_PLAY()
	
	
	IF NETWORK_GET_NUM_PARTICIPANTS() > 4 
		CPRINTLN(DEBUG_SCLUB, "CAN_STRIPCLUB_POST_HEIST_SCENE_PLAY - To many people in club")
		RETURN FALSE
	ENDIF
	
	IF stripClubStage != STAGE_WANDER_CLUB
		CPRINTLN(DEBUG_SCLUB, "CAN_STRIPCLUB_POST_HEIST_SCENE_PLAY - Not in wander stage")
		RETURN FALSE
	ENDIF
	
	INT iPartCount, idx, iPlayerHash
	
	REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash) idx
		
		iPlayerHash = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash[idx]
		CPRINTLN(DEBUG_SCLUB, "Player hash for index ", idx, " is ", iPlayerHash)
		
		IF iPlayerHash != 0
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartCount
				PARTICIPANT_INDEX partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPartCount)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
					PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(partId)
					
					CPRINTLN(DEBUG_SCLUB, "Checking against ",  NETWORK_HASH_FROM_PLAYER_HANDLE(playerId))
					
					IF iPlayerHash = NETWORK_HASH_FROM_PLAYER_HANDLE(playerId)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_SCLUB, "CAN_STRIPCLUB_POST_HEIST_SCENE_PLAY - Some one is in the club who is not part of the scene")
	RETURN FALSE
ENDFUNC

PROC SETUP_STRIPCLUB_END_HEIST_SCENE()
	
	INT idx, iSceneIndex = -1
	
	BOOL bInScene = FALSE
	
	CPRINTLN(DEBUG_SCLUB, "Players hash ", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
	
	REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash) idx 
		
		CPRINTLN(DEBUG_SCLUB, "Player hash in post heist cutscene ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash[idx])
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash[idx] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
			bInScene = TRUE
			iSceneIndex = idx
		ENDIF
	ENDREPEAT 
	
	IF bInScene
		IF DOES_CAM_EXIST(stripclubCutCam)
			DESTROY_CAM(stripclubCutCam)
		ENDIF
		IF DOES_CAM_EXIST(stripclubCutCamEnd)
			DESTROY_CAM(stripclubCutCamEnd)
		ENDIF
		
		stripclubCutCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		SET_CAM_COORD(stripclubCutCam, <<108.6646, -1286.0452, 29.1462>>)
		SET_CAM_ROT(stripclubCutCam, <<-5.1631, 0.0297, -108.3736>>)
		SET_CAM_FOV(stripclubCutCam, 36.0)
		SHAKE_CAM(stripclubCutCam, "HAND_SHAKE", 0.15)
		
		stripclubCutCamEnd = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		SET_CAM_COORD(stripclubCutCamEnd, <<109.2548, -1286.2391, 29.0900>>)
		SET_CAM_ROT(stripclubCutCamEnd, <<-5.1631, 0.0297, -104.5995>>)
		SET_CAM_FOV(stripclubCutCamEnd, 36.0)
		SHAKE_CAM(stripclubCutCamEnd, "HAND_SHAKE", 0.15)
		
		SET_CAM_ACTIVE_WITH_INTERP(stripclubCutCamEnd, stripclubCutCam, POST_HEIST_STRIPCLUB_SCENE_TIME)
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		iStartPostHeistCutsceneTime = GET_GAME_TIMER()
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		REQUEST_MODEL(GET_STRIPCLUB_CASH_MODEL())
		REQUEST_ALL_LEAN_ANIMS()
		
		SET_STRIPCLUB_POST_HEIST_SCENE_PLAYING(TRUE)
		
		CPRINTLN(DEBUG_SCLUB, "In stripclub end scene at index ", iSceneIndex)
		SWITCH iSceneIndex
			CASE 0  //on rail front
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<114.7723, -1285.9458, 27.2625>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 144.0703)
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_ON_RAIL_AFTER_HEIST_SCENE)
			BREAK
			
			CASE 1  //on rail left
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<115.0687, -1289.5953, 27.2610>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 40.4867)
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_ON_RAIL_AFTER_HEIST_SCENE)
			BREAK
			
			CASE 2
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<117.4653, -1287.3748, 27.2645>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 90.4942)
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_1)
				REQUEST_ANIM_DICT("anim@heists@humane_labs@finale@strip_club")
			BREAK
			
			CASE 3
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<115.9642, -1285.1058, 27.2676>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 130.9739)
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_2)
				REQUEST_ANIM_DICT("anim@heists@humane_labs@finale@strip_club")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_SCLUB, "Not in end heist stripclub scene")
	ENDIF
	
ENDPROC

PROC UPDATE_STRIPCLUB_POST_HEIST_CUTSCENE()
	
	IF iStartPostHeistCutsceneTime != 0
		FREEZE_MICROPHONE()
	
		#IF MP_STRIPCLUB
			CPRINTLN(DEBUG_SCLUB, "Update scriptclub post hiest scene")
			SET_PLAYER_STARTED_STRICLUB_HIEST_SCENE()
			IF NOT ALL_PLAYERS_STARTED_STRIPCLUB_HIEST_SCENE()
				CPRINTLN(DEBUG_SCLUB, "Not everyone is ready for stripclub cutscene")
				iStartPostHeistCutsceneTime = GET_GAME_TIMER()
				EXIT
			ENDIF
		#ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			//Cutscene is over, stop blocking the peds
			bLocalBlockStripClubPedsForCongratsScreen = FALSE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubPedsForCongratsScreen = FALSE
			
			IF iStartPostHeistCutsceneTime + 5000 < GET_GAME_TIMER()
				DO_SCREEN_FADE_IN(500)
				STOP_CUTSCENE_AUDIO()
				SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_TURN_OFF_HEIST_AUDIO_STAGE)
				
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_1)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "anim@heists@humane_labs@finale@strip_club", "ped_a_celebrate_intro", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
				ENDIF
				IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_2)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "anim@heists@humane_labs@finale@strip_club", "ped_b_celebrate_intro", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_IN()
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_TURN_OFF_HEIST_AUDIO_STAGE)
				SET_GAME_PAUSES_FOR_STREAMING(TRUE)
				PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call I.")
				SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_TURN_OFF_HEIST_AUDIO_STAGE)
			ENDIF
		ENDIF
		
		IF iStartPostHeistCutsceneTime + POST_HEIST_STRIPCLUB_SCENE_TIME < GET_GAME_TIMER()
			IF DOES_CAM_EXIST(stripclubCutCam)
				SET_CAM_ACTIVE(stripclubCutCam, FALSE)
				DESTROY_CAM(stripclubCutCam)
			ENDIF
			IF DOES_CAM_EXIST(stripclubCutCamEnd)
				SET_CAM_ACTIVE(stripclubCutCamEnd, FALSE)
				DESTROY_CAM(stripclubCutCamEnd)
			ENDIF
			IF railStage = RAIL_NOT_ACTIVE
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				UNFREEZE_PLAYER_IN_CLUB()
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_1)
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "anim@heists@humane_labs@finale@strip_club", "ped_a_celebrate_loop", SLOW_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING | AF_ABORT_ON_PED_MOVEMENT)
				REMOVE_ANIM_DICT("anim@heists@humane_labs@finale@strip_club")
			ENDIF
			IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_2)
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "anim@heists@humane_labs@finale@strip_club", "ped_b_celebrate_loop", SLOW_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING | AF_ABORT_ON_PED_MOVEMENT)
				REMOVE_ANIM_DICT("anim@heists@humane_labs@finale@strip_club")
			ENDIF
			
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_ON_RAIL_AFTER_HEIST_SCENE)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_1)
			CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_2)
			ENABLE_INTERACTION_MENU()
			iStartPostHeistCutsceneTime = 0
			SET_STRIPCLUB_POST_HEIST_SCENE_PLAYING(FALSE)
			SET_LOCAL_PLAYER_DOING_STRIP_CLUB_POST_MISSION_SCENE(FALSE)
		ENDIF
		
		IF DOES_CAM_EXIST(stripclubCutCamEnd)
			IF NOT IS_CAM_RENDERING(stripclubCutCamEnd)
				SET_CAM_ACTIVE(stripclubCutCamEnd, TRUE) //make sure this cam is always active during custscene
			ENDIF
		ENDIF
		
		//FORCE_ROOM_FOR_GAME_VIEWPORT(GET_INTERIOR_AT_COORDS_with_type(<<119.6, -1286.6, 29.3>>, "v_strip3"), HASH("strp3mainrm"))
	ENDIF
ENDPROC

PROC CLEANUP_STRIPCLUB_POST_HEIST_CUTSCENE_DATA()
	INT idx
	REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash) idx
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iStripclubPlayerNameHash[idx] = -1
	ENDREPEAT
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyForStripClubPostScene = FALSE
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
ENDPROC
