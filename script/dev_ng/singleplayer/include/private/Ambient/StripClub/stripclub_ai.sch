USING "stripclub_flags.sch"
USING "stripclub_anim.sch"

#IF MP_STRIPCLUB
USING "stripclub_mp.sch"
#ENDIF

// get time to delay before strip club ped goes hostile
FUNC INT GET_HOSTILE_REACT_DELAY_TIME(PED_INDEX ped, BOOL bShort = FALSE)
    IF ped = sendHostilePed
    OR IS_HOSTILE_FROM_SHOOTING()
	OR bShort
        // if was the one to notice the player, or going hostile from shooting, delay time is shorter
        RETURN GET_RANDOM_INT_IN_RANGE(MIN_SHORT_DELAY_TIME, MAX_SHORT_DELAY_TIME)
    ELSE
        RETURN GET_RANDOM_INT_IN_RANGE(MIN_LONG_DELAY_TIME, MAX_LONG_DELAY_TIME)
    ENDIF
ENDFUNC

PROC STRIPCLUB_TASK_WAYPOINT_RECORDING(PED_INDEX pedIndex, STRIPCLUB_WAYPOINT_RECORDINGS waypointRecording)
	PRINTLN("Stripclub waypoint recording ", waypointRecording)


	VECTOR vPos = GET_VIP_OUTRO_CUTSCENE_END_COORD(waypointRecording)

	IF VDIST2(vPos, <<0,0,0>>) != 0
		TASK_FOLLOW_NAV_MESH_TO_COORD(pedIndex, vPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_DONT_AVOID_OBJECTS | ENAV_DONT_AVOID_PEDS)
	ENDIF
ENDPROC

FUNC STRIPPER_AI_STATE_ENUM GET_STRIPPER_AI_STATE(INT i)
	#IF MP_STRIPCLUB
		RETURN serverBD.stripperAIState[i]
	#ENDIF

	RETURN namedStripper[i].aiState
ENDFUNC

PROC SET_STRIPPER_AI_STATE(INT i, STRIPPER_AI_STATE_ENUM state)
	IF i < 0
		EXIT
	ENDIF

	namedStripper[i].aiState = state
	
	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.stripperAIState[i] = state
		ENDIF
		
		clubPlayerBD[PARTICIPANT_ID_TO_INT()].stripperAIState[i] = state
		SET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()], i)
	#ENDIF
ENDPROC

FUNC STRIPPER_WANDER_STATE GET_STRIPPER_WANDER_STATE(INT i)
	#IF MP_STRIPCLUB
		RETURN serverBD.stripperWanderState[i]
	#ENDIF

	RETURN namedStripper[i].wanderState
ENDFUNC

PROC SET_STRIPPER_WANDER_STATE(INT i, STRIPPER_WANDER_STATE state)
	namedStripper[i].wanderState = state
	
	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.stripperWanderState[i] = state
		ENDIF
	#ENDIF
	
ENDPROC

PROC INC_NUMBER_OF_POLEDANCES()
	iNumOfPoleDances++
	
	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.iNumberOfPoleDances++
		ENDIF
	#ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_POLEDANCES()
	#IF MP_STRIPCLUB
		RETURN serverBD.iNumberOfPoleDances
	#ENDIF
	
	RETURN iNumOfPoleDances
ENDFUNC

PROC SET_STAGE_STATE(STAGE_STATE_ENUM newState)
	IF IS_STRIPCLUB_MP()
		IF newState = STAGE_STATE_GIRL4 //there is only three girls in mp
			newState = STAGE_STATE_GIRL1
		ENDIF
	ENDIF

	stageState = newState

	#IF MP_STRIPCLUB
		serverBD.severStageState = newState
	#ENDIF
ENDPROC

PROC STRIPCLUB_INC_STAGE_STATE()
	#IF MP_STRIPCLUB
		stageState = serverBD.severStageState
	#ENDIF
	
	INT initialStage = ENUM_TO_INT(stageState)
	INT iStage = PICK_INT(initialStage+1 < COUNT_OF(STAGE_STATE_ENUM), initialStage+1 , 0)
	
	WHILE iStage != initialStage
		IF iStage < GET_MAX_NUMBER_OF_NAMED_STRIPPERS()
			IF GET_STRIPPER_AI_STATE(iStage) = AI_STATE_WANDER
				stageState = INT_TO_ENUM(STAGE_STATE_ENUM, iStage)
				
				#IF MP_STRIPCLUB
					serverBD.severStageState = stageState
				#ENDIF
				CPRINTLN(DEBUG_SCLUB, "New stage state is ", stageState)
				EXIT
			ENDIF
		ENDIF
		iStage = PICK_INT(iStage+1 < COUNT_OF(STAGE_STATE_ENUM), iStage+1 , 0)
	ENDWHILE

	CPRINTLN(DEBUG_SCLUB, "No stripper available to go to pole")
	stageState = STAGE_STATE_NONE
	#IF MP_STRIPCLUB
		serverBD.severStageState = stageState
	#ENDIF
ENDPROC

FUNC STAGE_STATE_ENUM GET_STAGE_STATE()
	#IF MP_STRIPCLUB
		RETURN serverBD.severStageState
	#ENDIF
	
	RETURN stageState

ENDFUNC

PROC SET_POLEDANCER_STATE(INT iStripper, POLEDANCER_STATE_ENUM state, BOOL bAllowClientChange = FALSE)
	namedStripper[iStripper].poledancerState = state
	
	UNUSED_PARAMETER(bAllowClientChange)

	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.poledancerState[iStripper] = state
		
		ELIF bAllowClientChange
			clubPlayerBD[PARTICIPANT_ID_TO_INT()].poledancerState[iStripper] = state
			SET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()], iStripper)
		ENDIF
	#ENDIF
ENDPROC

FUNC POLEDANCER_STATE_ENUM GET_POLEDANCER_STATE(INT iStripper)

	#IF MP_STRIPCLUB
		RETURN serverBD.poledancerState[iStripper]
	#ENDIF

	RETURN namedStripper[iStripper].poledancerState
ENDFUNC

PROC START_STRIPPER_WANDER_SM(INT i)
	SET_STRIPPER_WANDER_STATE(i, WANDER_STATE_INIT)
ENDPROC

FUNC BOOL IS_STRIPCLUB_STAND_LOCATION_IN_USE(INT iLocation)
	
	#IF MP_STRIPCLUB
		IS_BIT_SET(serverBD.iWalkLocationsInUse, iLocation)
	#ENDIF

	RETURN IS_BIT_SET(iStripClubBits_Stand_Locations, iLocation)
ENDFUNC

PROC CLEAR_STRIPCLUB_STAND_LOCATION(INT iLocation)
	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CLEAR_BIT(serverBD.iWalkLocationsInUse, iLocation)
		ENDIF
	#ENDIF
	
	CLEAR_BIT(iStripClubBits_Stand_Locations, iLocation)
ENDPROC

PROC SET_STRIPCLUB_STAND_LOCATION_ACTIVE(INT iLocation)
	#IF MP_STRIPCLUB
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_BIT(serverBD.iWalkLocationsInUse, iLocation)
		ENDIF
	#ENDIF
	
	SET_BIT(iStripClubBits_Stand_Locations, iLocation)
ENDPROC

PROC SET_STRIPCLUB_STAND_LOCATION(INT iLocation, BOOL bActive)
	IF bActive
		SET_STRIPCLUB_STAND_LOCATION_ACTIVE(iLocation)
	ELSE
		CLEAR_STRIPCLUB_STAND_LOCATION(iLocation)
	ENDIF
ENDPROC


FUNC INT GET_FIRST_OPEN_LOCATION()

	INT iLoc
	REPEAT NUM_RAND_STAND_LOCS iLoc
		IF NOT IS_STRIPCLUB_STAND_LOCATION_IN_USE(iLoc)
			RETURN iLoc
		ENDIF
	ENDREPEAT

	RETURN 0
ENDFUNC

// get random stripper stand location
FUNC INT GET_RANDOM_STRIPPER_STAND_LOC(VECTOR &vStorePos, FLOAT &fStoreRot, INT iSpecifyPoint = -1, BOOL bSetUsed = FALSE)
	INT iRandStandLoc
	VECTOR vRetPos
	FLOAT fRetRot
	
	IF iSpecifyPoint >= 0
	AND iSpecifyPoint < NUM_RAND_STAND_LOCS
		iRandStandLoc = iSpecifyPoint
	ELIF IS_STRIPCLUB_MP()
		iRandStandLoc = GET_FIRST_OPEN_LOCATION()
	ELSE
		iRandStandLoc = GET_RANDOM_INT_IN_RANGE(0, NUM_RAND_STAND_LOCS-1)
	ENDIF
	
	IF NOT IS_STRIPCLUB_STAND_LOCATION_IN_USE(iRandStandLoc)
		GET_RANDOM_CLUB_STAND_LOCAND_HEADING(iRandStandLoc, vRetPos, fRetRot)
	ELSE
		iRandStandLoc = 0
	ENDIF
	
	WHILE (ARE_VECTORS_EQUAL(vRetPos, <<0,0,0>>) OR IS_STRIPCLUB_STAND_LOCATION_IN_USE(iRandStandLoc))
	AND iRandStandLoc < NUM_RAND_STAND_LOCS
		vRetPos = <<0,0,0>>
		fRetRot = 0
		iRandStandLoc++
		GET_RANDOM_CLUB_STAND_LOCAND_HEADING(iRandStandLoc, vRetPos, fRetRot)
		//DEBUG_MESSAGE("GET_RANDOM_STRIPPER_STAND_LOC: getting new stand loc, that was empty or was in use")
	ENDWHILE
	
	IF iRandStandLoc > 10
		PRINTLN("VECtor ", vRetPos)
		SCRIPT_ASSERT("Stand location is going to be to big")
	ENDIF
	
	vStorePos = vRetPos
	fStoreRot = fRetRot
	IF bSetUsed
		SET_STRIPCLUB_STAND_LOCATION(iRandStandLoc, TRUE)
	ENDIF
		
	RETURN iRandStandLoc
ENDFUNC

PROC STRIPCLUB_TASK_SEQUENCE(PED_INDEX pedIndex, SEQUENCE_INDEX &seqIndex)
	
	#IF MP_STRIPCLUB
		IF pedIndex != PLAYER_PED_ID()
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(NETWORK_GET_NETWORK_ID_FROM_ENTITY(pedIndex))
				PRINTLN("Trying to task entity you do not own")
				EXIT
			ENDIF
		ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("Do stripclub sequence task")
	#ENDIF
	
	TASK_PERFORM_SEQUENCE(pedIndex, seqIndex)
ENDPROC


