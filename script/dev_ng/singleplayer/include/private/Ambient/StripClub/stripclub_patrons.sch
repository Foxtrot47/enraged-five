USING "stripclub_lapdance.sch"


FUNC VECTOR GET_CUSTOMER_SPAWN_POINT(INT iSpawnIndex)

	SWITCH iSpawnIndex
		CASE 0
			RETURN <<118.51906, -1281.46216, 27.72217>>
		BREAK
		CASE 1
			RETURN <<119.51733, -1283.47253, 27.72217>>
		BREAK
		CASE 2
			RETURN <<121.30746, -1283.90930, 27.72217>>
		BREAK
		CASE 3
			RETURN <<122.12188, -1285.23987, 27.72217>>
		BREAK
		CASE 4
			RETURN <<122.81584, -1286.48706, 27.72217>>
		BREAK
		CASE 5
			RETURN <<123.58376, -1287.45093, 27.72217>>
		BREAK
		CASE 6
			RETURN <<123.82587, -1288.76892, 27.72217>>
		BREAK
		CASE 7
			RETURN <<112.90668, -1293.35681, 27.72217>>
		BREAK
		CASE 8
			RETURN <<110.34463, -1294.93396, 27.72217>>
		BREAK
		CASE 9
			RETURN <<109.23676, -1295.48083, 27.72217>>
		BREAK
		CASE 10
			RETURN <<106.67527, -1297.04932, 27.72217>>
		BREAK
		CASE 11
			RETURN <<107.08104, -1282.87268, 27.72217>>
		BREAK
		CASE 12
			RETURN <<104.80939, -1284.13660, 27.72217>>
		BREAK
		CASE 13
			RETURN <<103.31277, -1285.01794, 27.72217>>
		BREAK
		CASE 14
			RETURN <<101.16519, -1286.21143, 27.72217>>
		BREAK
		
		//peds at stage
		CASE 15
			RETURN <<113.74977, -1283.02502, 27.72217>>
		BREAK
		CASE 16
			RETURN <<115.05013, -1284.77075, 27.72217>>
		BREAK
		CASE 17
			RETURN <<116.00701, -1286.53149, 27.72217>>
		BREAK
		CASE 18
			RETURN <<116.70783, -1288.57947, 27.72217>>
		BREAK
		CASE 19
			RETURN <<103.55460, -1287.39368, 28.26099>>
		BREAK
		CASE 20
			RETURN <<104.90012, -1288.02124, 27.72693>>
		BREAK
		CASE 21
			RETURN <<106.85392, -1286.83728, 27.72693>>
		BREAK		
		CASE 22
			RETURN <<108.48473, -1285.91541, 27.72693>>
		BREAK		
		CASE 23
			RETURN <<110.53699, -1284.57886, 28.26099>>
		BREAK				
		CASE 24
			RETURN <<111.46305, -1283.95654, 28.26592>>
		BREAK	
		CASE 25
			RETURN <<113.83677, -1290.31018, 28.26099>>
		BREAK
		CASE 26
			RETURN <<111.35435, -1291.05652, 27.78339>>
		BREAK	
		CASE 27
			RETURN <<110.08369, -1291.59558, 27.72693>>
		BREAK		
		CASE 28
			RETURN <<108.99257, -1292.22119, 27.72693>>
		BREAK			
		CASE 29
			RETURN <<108.35011, -1293.68726, 28.26099>>
		BREAK	
	ENDSWITCH

	RETURN <<0,0,0>>
ENDFUNC

PROC INC_CURRENT_PARTON_CHECK()

	iCurrentPatronCheck = PICK_INT( iCurrentPatronCheck+1 < MAX_NUMBER_OF_STRIPCLUB_CUSTOMERS, iCurrentPatronCheck+1, 0)

ENDPROC

CONST_INT CLUB_PATRON_COUNT 30
PED_INDEX clubPatrons[CLUB_PATRON_COUNT]

PROC MANAGE_CLUB_PARTRONS_ANIMS()
	BOOL bOwnped
	INT iStripperIndex

	IF NOT IS_PED_INJURED(clubPatrons[iCurrentPatronCheck])
		bOwnped = TRUE
		
		IF IS_STRIPCLUB_MP()
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(clubPatrons[iCurrentPatronCheck])
				bOwnped = FALSE
			ENDIF
		ENDIF
		
		IF bOwnped
			IF iCurrentPatronCheck < 15
				SET_PED_CAN_PLAY_AMBIENT_ANIMS(clubPatrons[iCurrentPatronCheck], FALSE)
			ELSE
				//patron is at stage and should look at poledancer
				iStripperIndex = GET_POLE_DANCER_ON_STAGE()
				IF iStripperIndex > -1
					IF GET_POLEDANCER_STATE(iStripperIndex) = POLEDANCER_STATE_DANCE_ENTER
						TRIGGER_IDLE_ANIMATION_ON_PED(clubPatrons[iCurrentPatronCheck])
					ELSE
						SET_PED_CAN_PLAY_AMBIENT_ANIMS(clubPatrons[iCurrentPatronCheck], FALSE)
					ENDIF
				ELSE
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(clubPatrons[iCurrentPatronCheck], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_CLUB_PATRONS()

	//find another customer to task
	VECTOR vCustomerSpawnPoint = GET_CUSTOMER_SPAWN_POINT(iCurrentPatronCheck)
	INT iStripperIndex
	PED_INDEX pedStripper
	
	MANAGE_CLUB_PARTRONS_ANIMS()
	
	IF NOT DOES_ENTITY_EXIST(clubPatrons[iCurrentPatronCheck])
		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		GET_CLOSEST_PED(vCustomerSpawnPoint, 0.25, TRUE, FALSE, clubPatrons[iCurrentPatronCheck], FALSE, FALSE, PEDTYPE_PLAYER_NETWORK)
	ENDIF
	
	IF NOT IS_PED_INJURED(clubPatrons[iCurrentPatronCheck])
		
//		PRINTLN("Tasking ", iCurrentPatronCheck)
//		DRAW_DEBUG_SPHERE(vCustomerSpawnPoint, 0.25, 0, 0, 255)
//		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCustomerSpawnPoint, 0, 0, 255)
		IF IS_STRIPCLUB_MP()
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(clubPatrons[iCurrentPatronCheck])
				EXIT
			ENDIF
		ENDIF
		
		IF iCurrentPatronCheck < 15
			//find stripper to look at
			iStripperIndex = GET_CLOSEST_STRIPPER_INDEX_TO_POINT(vCustomerSpawnPoint)
		ELSE
			//patron is at stage and should look at poledancer
			iStripperIndex = GET_POLE_DANCER_ON_STAGE()
		ENDIF
		
		IF iStripperIndex >= 0
			pedStripper = namedStripper[iStripperIndex].ped
		ENDIF
		
		IF pedStripper != clubPatrons[iCurrentPatronCheck] AND DOES_ENTITY_EXIST(pedStripper)
//			PRINTLN("Tasked customer to look at stripper;")
//			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedStripper), 0.25, 0, 0, 255)
//			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(pedStripper), GET_ENTITY_COORDS(clubPatrons[iCurrentPatronCheck]), 0, 0, 255)
			TASK_LOOK_AT_ENTITY(clubPatrons[iCurrentPatronCheck], pedStripper, -1)
		ELSE
			//when looking for customer ped I accidently found a stripper, or no stripper
		ENDIF
	ELSE
//		PRINTLN("No ped at index ", iCurrentPatronCheck)
//		DRAW_DEBUG_SPHERE(vCustomerSpawnPoint, 0.25, 255, 0, 0)
//		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCustomerSpawnPoint, 255, 0, 0)
	ENDIF
	// lets get another customer
	INC_CURRENT_PARTON_CHECK()	
ENDPROC
