
FUNC PED_INDEX GET_STRIPCLUB_FRIEND_PED_FROM_INDEX(INT iFriendIndex)
	
	IF iFriendIndex = 0
		RETURN FRIEND_A_PED_ID()
	ELIF iFriendIndex = 1
		RETURN FRIEND_B_PED_ID()
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC TEXT_LABEL_23 GET_STRIPCLUB_FRINED_TEXT_LABEL(INT iFriendIndex)

	TEXT_LABEL_23 txtRet

	IF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_TREVOR))		
		txtRet = "BLIP_170"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))		
		txtRet = "BLIP_MICHAEL"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))		
		txtRet = "BLIP_FRANKLIN"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_LAMAR))	
		txtRet = "BLIP_172"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))		
		txtRet = "BLIP_33"
	ENDIF
	
	RETURN txtRet
ENDFUNC

PROC SETUP_STRIPCLUB_FRIEND_STRUCT()

	INT iFriendIndex
	
	REPEAT GET_STRIPCLUB_NUMBER_OF_FRIENDS() iFriendIndex
		
		clubFriends[iFriendIndex].ped = GET_STRIPCLUB_FRIEND_PED_FROM_INDEX(iFriendIndex)
		//clubFriends[iFriendIndex].blip
		//clubFriends[iFriendIndex].sequence
		//clubFriends[iFriendIndex].fRailHeading
		//clubFriends[iFriendIndex].FriendFlag
		clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_INIT
	ENDREPEAT
ENDPROC

PROC TASK_FRIEND_TO_RAIL(INT iFriendIndex)	
	ECOMPASS direction
	VECTOR vFriendRailPos
	BOOL bFemale = IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))
	
	GET_CLOSEST_RAIL_POSITION(GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex), vFriendRailPos, clubFriends[iFriendIndex].fRailHeading, direction, FALSE)
	
	CLEAR_PED_TASKS_IMMEDIATELY(clubFriends[iFriendIndex].ped)
	CLEAR_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)
	OPEN_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)
		
		VECTOR vFriendPos = GET_ENTITY_COORDS(clubFriends[iFriendIndex].ped)
		IF vFriendPos.z > 29
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_VIP_OUTRO_CUTSCENE_END_COORD(SWR_FRIEND_ENTER), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_DONT_AVOID_PEDS)
		ENDIF
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY | ENAV_DONT_AVOID_OBJECTS  | ENAV_DONT_ADJUST_TARGET_POSITION , clubFriends[iFriendIndex].fRailHeading)
		TASK_ACHIEVE_HEADING(NULL, clubFriends[iFriendIndex].fRailHeading)
		
		TASK_PLAY_ANIM(NULL,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IN), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IN, bFemale), SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME) 
	CLOSE_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)
	STRIPCLUB_TASK_SEQUENCE(clubFriends[iFriendIndex].ped, clubFriends[iFriendIndex].sequence)
	CLEAR_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)	
ENDPROC

PROC TASK_FRIEND_THROW_MONEY(INT iFriendIndex)

	IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].objFriendMoney )
		DELETE_OBJECT(clubFriends[iFriendIndex].objFriendMoney )
	ENDIF

	VECTOR vCreateCashAt = GET_PED_BONE_COORDS(clubFriends[iFriendIndex].ped, BONETAG_PH_R_HAND, (<< 0,0,0 >>))
				
	clubFriends[iFriendIndex].objFriendMoney = CREATE_OBJECT(GET_STRIPCLUB_CASH_MODEL(), vCreateCashAt, TRUE, FALSE)
	ATTACH_ENTITY_TO_ENTITY(clubFriends[iFriendIndex].objFriendMoney, clubFriends[iFriendIndex].ped, GET_PED_BONE_INDEX(clubFriends[iFriendIndex].ped, BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0,0,0>>)
	
	BOOL bFemale = IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))
	
	CLEAR_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)
	OPEN_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)
		
		TASK_PLAY_ANIM(NULL, GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_THROW, bFemale), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
		TASK_PLAY_ANIM(NULL,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IDLE, bFemale), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING) 
	CLOSE_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)
	STRIPCLUB_TASK_SEQUENCE(clubFriends[iFriendIndex].ped, clubFriends[iFriendIndex].sequence)
	CLEAR_SEQUENCE_TASK(clubFriends[iFriendIndex].sequence)	
ENDPROC

PROC STRIPCLUB_PLAY_FRIEND_CONVERSATION(INT iFriendIndex, TEXT_LABEL_23 &sContextLabel)

	IF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_TREVOR))		
		ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_FRIEND), clubFriends[iFriendIndex].ped, "TREVOR")
		sContextLabel += "_T"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))		
		ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_FRIEND), clubFriends[iFriendIndex].ped, "MICHAEL")
		sContextLabel += "_M"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))		
		ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_FRIEND), clubFriends[iFriendIndex].ped, "FRANKLIN")
		sContextLabel += "_F"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_LAMAR))		
		ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_FRIEND), clubFriends[iFriendIndex].ped, "LAMAR")
		sContextLabel += "_L"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))		
		ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_FRIEND), clubFriends[iFriendIndex].ped, "JIMMY")
		sContextLabel += "_J"
	ENDIF
	
	CREATE_CONVERSATION(stripClubConversation,"SCAUD", sContextLabel, GET_STRIPCLUB_SPEECH_PRIORITY())

ENDPROC

FUNC INT GET_STRIPPER_FOR_FRIEND_TO_LOOK_AT()

	INT i
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF NOT IS_PED_INJURED(namedStripper[i].ped)
			IF GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_GOTO_POLE
			OR GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_DANCE_ENTER OR GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_DANCE
			OR GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_DANCE_EXIT
			OR GET_POLEDANCER_STATE(i) = POLEDANCER_STATE_PRIVATE_DANCE
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

PROC MANAGE_LEAVE_CLUB_PRINTS(INT iFriendIndex)
	IF IS_PLAYER_ON_STAGE()
		EXIT
	ENDIF

	TEXT_LABEL_23 sLeaveHelpMessage 
	sLeaveHelpMessage = "SCLUB_LEAVE"
	
	IF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_LAMAR))								
		sLeaveHelpMessage += "_L"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))
		sLeaveHelpMessage += "_J"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_TREVOR))		
		sLeaveHelpMessage += "_T"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))		
		sLeaveHelpMessage += "_M"
	ELIF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))		
		sLeaveHelpMessage += "_F"
	ENDIF
	
	IF NOT IS_STRIPCLUB_MESSAGE_DISPLAYED("SCLUB_OFFR_HELP")
		PRINT_STRIPCLUB_HELP(sLeaveHelpMessage, TRUE)
	ENDIF
	
	IF IS_STRIPCLUB_SOLICIT_BUTTON_HELD()
	
		//Friend responds to being told to leave
		PLAY_PED_AMBIENT_SPEECH(clubFriends[iFriendIndex].ped, "ACTIVITY_LEAVING", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
		
		SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_JUST_ASK_FRIEND_TO_LEAVE)
		CLEAR_FRIEND_LEAVE_CLUB_MESSAGE()
		clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_ASKED_TO_LEAVE
	ENDIF
	
ENDPROC

PROC CLEAR_STRIPCLUB_FRIEND_TASK(INT iFriendIndex, BOOL bFemale)
	IF IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IDLE, bFemale))
		//Clear his uninterrupatable anim
		CLEAR_PED_TASKS_IMMEDIATELY(clubFriends[iFriendIndex].ped)
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(clubFriends[iFriendIndex].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
	OR  GET_SCRIPT_TASK_STATUS(clubFriends[iFriendIndex].ped, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
		CLEAR_PED_TASKS(clubFriends[iFriendIndex].ped)
	ENDIF
	
	IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].objFriendMoney )
		IF IS_ENTITY_ATTACHED(clubFriends[iFriendIndex].objFriendMoney)
			DETACH_ENTITY(clubFriends[iFriendIndex].objFriendMoney)
		ENDIF
		SET_OBJECT_AS_NO_LONGER_NEEDED(clubFriends[iFriendIndex].objFriendMoney)
	ENDIF
ENDPROC

PROC MANAGE_FRIEND_IN_STRIPCLUB()

//STRIP_ARRIVE_EXPECTED
//STRIP_ARRIVE_UNEXPECTED
//STRIP_DO_OWN_THING
//STRIP_ENJOYING_SELF
//STRIP_LEAVE_AGREE
//STRIP_LEAVE_DISAGREE

	BOOL bPlayerNearFriend[2]
	TEXT_LABEL_23 sContextLabel
	INT iFriendIndex, iStripperToLookAt
	FLOAT fCloseDist = 3.0
	WEAPON_TYPE friendWeapon
	
	IF DOES_ENTITY_EXIST(clubFriends[0].ped) AND NOT IS_ENTITY_DEAD(clubFriends[0].ped) 
		bPlayerNearFriend[0] = VDIST2(GET_ENTITY_COORDS(clubFriends[0].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < (fCloseDist*fCloseDist)
	ENDIF
	
	IF DOES_ENTITY_EXIST(clubFriends[1].ped) AND NOT IS_ENTITY_DEAD(clubFriends[1].ped) 
		bPlayerNearFriend[1] = VDIST2(GET_ENTITY_COORDS(clubFriends[1].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < (fCloseDist*fCloseDist)
	ENDIF
	
	IF NOT (bPlayerNearFriend[0] OR bPlayerNearFriend[1])
		CLEAR_FRIEND_LEAVE_CLUB_MESSAGE()
	ENDIF
	
	REPEAT GET_STRIPCLUB_NUMBER_OF_FRIENDS() iFriendIndex
		IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].ped)
		
			BOOL bFemale = IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))
			
			IF IS_PED_INJURED(clubFriends[iFriendIndex].ped)
				//Handle friend getting injured
				IF IS_FRIEND_ACTIVITY_ON()
					IF NOT IS_PED_IN_GROUP(clubFriends[iFriendIndex].ped)
						SET_PED_AS_GROUP_MEMBER(clubFriends[iFriendIndex].ped, PLAYER_GROUP_ID())
					ENDIF
					SET_FRIEND_ACTIVITY_ON(FALSE)
					finishActivity(ALOC_stripclub_southCentral,AR_buddy_injured)
				ENDIF
			
			ELSE
				IF NOT STRIPCLUB_IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_BIT_JUST_ASK_FRIEND_TO_LEAVE)
				ENDIF
				
				GET_CURRENT_PED_WEAPON(clubFriends[iFriendIndex].ped, friendWeapon)
				IF clubFriends[iFriendIndex].enumSCA_FriendState > SCA_FS_INIT
				AND stripClubStage != STAGE_CLUB_HOSTILE
				AND IS_FRIEND_IN_CLUB(iFriendIndex)
				AND friendWeapon != WEAPONTYPE_UNARMED
					CPRINTLN(DEBUG_SCLUB,"Remove weapons from buddy!")
					SET_CURRENT_PED_WEAPON(clubFriends[iFriendIndex].ped, WEAPONTYPE_UNARMED, TRUE)
	//				SET_PED_CAN_SWITCH_WEAPON(clubFriends[iFriendIndex].ped, FALSE)
				ENDIF
				
				SWITCH clubFriends[iFriendIndex].enumSCA_FriendState
					//Set your friend to go stand at the stage
					CASE SCA_FS_INIT
						IF IS_IN_CLUB_FLAG_SET()
						AND stripclubInside > SC_INSIDE_STREAMING
							IF stripClubStage = STAGE_CLUB_HOSTILE
								CPRINTLN(DEBUG_SCLUB,"Player entered the club hostile during friend activity")
								clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_DONE 
							ELIF IS_FRIEND_IN_CLUB(iFriendIndex)
							AND NOT IS_PED_IN_ANY_VEHICLE(clubFriends[iFriendIndex].ped)
								IF NOT IS_FRIEND_ACTIVITY_ON()
									CPRINTLN(DEBUG_SCLUB,"Starting stripclub friend activity")
									startActivity(ALOC_stripclub_southCentral)	
									SET_FRIEND_ACTIVITY_ON(TRUE)
									SET_HIGH_FUNCTIONALITY(TRUE)
									CLEAR_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_ASKED_FRIEND_TO_LEAVE)
								ELSE
									DEBUG_MESSAGE("IS_FRIEND_ACTIVITY_ON() is already TRUE")
								ENDIF	
									
								IF NOT DOES_BLIP_EXIST(clubFriends[iFriendIndex].blip)
									clubFriends[iFriendIndex].blip = CREATE_BLIP_FOR_ENTITY(clubFriends[iFriendIndex].ped)
									SET_BLIP_AS_FRIENDLY(clubFriends[iFriendIndex].blip, TRUE)
									//SET_BLIP_COLOUR(clubFriends[iFriendIndex].blip, BLIP_COLOUR_BLUE)
								ENDIF
								IF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
									SHRINK_ADD_STRIP_CLUB_VISIT_TIMESTAMP()
								ENDIF
								
								PLAY_PED_AMBIENT_SPEECH(clubFriends[iFriendIndex].ped, "STRIP_ARRIVE_EXPECTED", SPEECH_PARAMS_FORCE_NORMAL)
								
								DEBUG_MESSAGE("Starting Friend Activity ")							
								DEBUG_MESSAGE("enumSCA_FriendState = SCA_FS_SENDTOSTAGE")
								clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_SENDTOSTAGE
								
							ELSE
								DEBUG_MESSAGE("Friend is not in the club yet")
							ENDIF
						ELSE
							DEBUG_MESSAGE("Player is not in the club yet and friend exists")
						ENDIF
					BREAK
					
					//Have your friend loop on this anim, this is typically what guys do at the strip club anyways ;)
					CASE SCA_FS_SENDTOSTAGE
						IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_LEAN)				
							REQUEST_ALL_LEAN_ANIMS()
							SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_LEAN)	
						ENDIF
						
						//IF animRequestState = STRCLUB_ANIM_REQUEST_LOADED
						IF  HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IN))
						AND HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE))
							
							PLAY_PED_AMBIENT_SPEECH(clubFriends[iFriendIndex].ped, "STRIP_DO_OWN_THING", SPEECH_PARAMS_FORCE_NORMAL)
							
							IF IS_PED_IN_GROUP(clubFriends[iFriendIndex].ped)
								REMOVE_PED_FROM_GROUP(clubFriends[iFriendIndex].ped)
							ENDIF
							
							DEBUG_MESSAGE("enumSCA_FriendState = SCA_FS_GOTTOSTAGE")
							
							REMOVE_SCENARIO_BLOCKING_AREA(clubFriends[iFriendIndex].blockingIndex)
							clubFriends[iFriendIndex].blockingIndex = ADD_SCENARIO_BLOCKING_AREA(GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex) - <<1,1,1>>, GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex) + <<1,1,1>>)
							
							CLEAR_AREA_OF_PEDS(GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex), 5.0)
							TASK_FRIEND_TO_RAIL(iFriendIndex)
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_GOTTOSTAGE
						ELSE
							CPRINTLN(DEBUG_SCLUB,"Lean animations not loaded")
						ENDIF
						
					BREAK
					
					CASE SCA_FS_GOTTOSTAGE
						BOOL bDoneWithLeanIn
						bDoneWithLeanIn = FALSE
						
						IF IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IN), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IN, bFemale) )
							IF GET_ENTITY_ANIM_CURRENT_TIME(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IN), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IN, bFemale) ) >= 1.0
								bDoneWithLeanIn = TRUE
							ENDIF
						ENDIF
					
						IF GET_SCRIPT_TASK_STATUS(clubFriends[iFriendIndex].ped, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
						OR bDoneWithLeanIn
							DEBUG_MESSAGE("enumSCA_FriendState = SCA_FS_LOOP_STAGE_WATCH - Made it to the stage")
							
							sContextLabel = GET_STRIPCLUB_FRINED_TEXT_LABEL(iFriendIndex)
							
							PRINT_STRIPCLUB_HELP_WITH_STRING("SCLUB_LEAVHELP", sContextLabel, FALSE)
							PLAY_PED_AMBIENT_SPEECH(clubFriends[iFriendIndex].ped, "STRIP_ENJOYING_SELF", SPEECH_PARAMS_FORCE_NORMAL)
							
//							IF IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY))
//								clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_STAND_NEAR_STAGE
//							ELSE
								clubFriends[iFriendIndex].iNextThrowTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7000)
								TASK_PLAY_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IDLE, bFemale), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
								clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_LOOP_STAGE_WATCH
//							ENDIF
						ELSE
							CPRINTLN(DEBUG_SCLUB,"Still leaning into rail")
							IF stripClubStage = STAGE_CLUB_HOSTILE
								clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_FRIEND_ACTIVITY_ENDED
							ENDIF
						ENDIF
					BREAK
					
					CASE SCA_FS_STAND_NEAR_STAGE
						
						IF NOT IS_ENTITY_AT_COORD(clubFriends[iFriendIndex].ped, GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex), <<2.0, 2.0, 20.0>>)
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_SENDTOSTAGE
						ENDIF
						
						IF bPlayerNearFriend[iFriendIndex] AND railStage = RAIL_NOT_ACTIVE
							MANAGE_LEAVE_CLUB_PRINTS(iFriendIndex)
						ENDIF
						
					BREAK
					
					CASE SCA_FS_LOOP_STAGE_WATCH					
						IF NOT IS_BITMASK_AS_ENUM_SET(clubFriends[iFriendIndex].FriendFlag, STRIP_CLUB_FRIEND_FROZEN_ON_RAIL)
							IF IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IDLE, bFemale) )
								IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].ped)
									FREEZE_ENTITY_POSITION(clubFriends[iFriendIndex].ped, TRUE)
								ENDIF
								
								SET_BITMASK_AS_ENUM(clubFriends[iFriendIndex].FriendFlag, STRIP_CLUB_FRIEND_FROZEN_ON_RAIL)
								DEBUG_MESSAGE("Froze friend")
							ENDIF
						ENDIF
						
						iStripperToLookAt = GET_STRIPPER_FOR_FRIEND_TO_LOOK_AT()
						IF iStripperToLookAt >= 0
							TASK_LOOK_AT_ENTITY(clubFriends[iFriendIndex].ped, namedStripper[iStripperToLookAt].ped, -1)
						ELSe
							TASK_CLEAR_LOOK_AT(clubFriends[iFriendIndex].ped)
						ENDIF
						
						IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].objFriendMoney )
							IF IS_ENTITY_ATTACHED(clubFriends[iFriendIndex].objFriendMoney)
								//you are done playing throw anim, detach money
								IF IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped, GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_THROW, bFemale))
									IF GET_ENTITY_ANIM_CURRENT_TIME(clubFriends[iFriendIndex].ped, GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_THROW, bFemale)) > 0.81
										DETACH_ENTITY(clubFriends[iFriendIndex].objFriendMoney)
									ENDIF
								ELSE
									DETACH_ENTITY(clubFriends[iFriendIndex].objFriendMoney)
								ENDIF
							ENDIF
						ENDIF
						
//						CPRINTLN(DEBUG_SCLUB,"Next throw time, ",clubFriends[iFriendIndex].iNextThrowTime, " current time ", GET_GAME_TIMER())			
						IF clubFriends[iFriendIndex].iNextThrowTime < GET_GAME_TIMER()
							IF iStripperToLookAt >= 0
								TASK_FRIEND_THROW_MONEY(iFriendIndex)
							ENDIF
							clubFriends[iFriendIndex].iNextThrowTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 20000)
						ENDIF
						
						//Means he got knocked off his position on the stage. So retask
						IF NOT IS_ENTITY_AT_COORD(clubFriends[iFriendIndex].ped, GET_STRIPCLUB_FRIEND_RAIL_POS(iFriendIndex), <<2.0, 2.0, 20.0>>)
						OR (ABSF(GET_ENTITY_HEADING(clubFriends[iFriendIndex].ped)-clubFriends[iFriendIndex].fRailHeading) > 20.0)
							DEBUG_MESSAGE("enumSCA_FriendState = SCA_FS_LOOP_STAGE_WATCH - Knocked off stage, retasking")
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_SENDTOSTAGE
						ELIF NOT IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped, GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IDLE, bFemale))
						AND NOT IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped, GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_THROW), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_THROW, bFemale))
							TASK_PLAY_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_IDLE), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_IDLE, bFemale), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)		
						ELIF bPlayerNearFriend[iFriendIndex] AND railStage = RAIL_NOT_ACTIVE
							MANAGE_LEAVE_CLUB_PRINTS(iFriendIndex)
						ENDIF
						
						//Player has left or is leaving strip club
						IF NOT IS_FRIEND_IN_CLUB(iFriendIndex)
						OR NOT IS_IN_CLUB_FLAG_SET()
						OR stripClubStage = STAGE_CLUB_HOSTILE
							IF IS_BITMASK_AS_ENUM_SET(clubFriends[iFriendIndex].FriendFlag, STRIP_CLUB_FRIEND_FROZEN_ON_RAIL)							
								IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].ped)
									FREEZE_ENTITY_POSITION(clubFriends[iFriendIndex].ped, FALSE)
								ENDIF
								CLEAR_BITMASK_AS_ENUM(clubFriends[iFriendIndex].FriendFlag, STRIP_CLUB_FRIEND_FROZEN_ON_RAIL)
							ENDIF
							
							TASK_PLAY_ANIM(clubFriends[iFriendIndex].ped, GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_OUT), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_OUT, bFemale), INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE) 
							
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_FRIEND_ACTIVITY_ENDED
							DEBUG_MESSAGE("Player has left strip club")
						ENDIF
					BREAK
					
					CASE SCA_FS_ASKED_TO_LEAVE
						CPRINTLN(DEBUG_SCLUB,"SCA_FS_ASKED_TO_LEAVE")
						CLEAR_FRIEND_LEAVE_CLUB_MESSAGE()
						
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].objFriendMoney )
								IF IS_ENTITY_ATTACHED(clubFriends[iFriendIndex].objFriendMoney)
									DETACH_ENTITY(clubFriends[iFriendIndex].objFriendMoney)
								ENDIF
							ENDIF
							
							TASK_PLAY_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_OUT), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_OUT, bFemale), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE) 
							
							SET_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_ASKED_FRIEND_TO_LEAVE)
							
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_LEAVING
						ENDIF
					BREAK
					
					CASE SCA_FS_LEAVING
						CPRINTLN(DEBUG_SCLUB,"SCA_FS_LEAVING")
						CLEAR_FRIEND_LEAVE_CLUB_MESSAGE()
						CLEAR_STRIPCLUB_HELP_WITH_NAME("SCLUB_LEAVHELP", GET_STRIPCLUB_FRINED_TEXT_LABEL(iFriendIndex))
						
						IF NOT IS_ENTITY_PLAYING_ANIM(clubFriends[iFriendIndex].ped,  GET_STRIP_CLUB_LEAN_ANIM_DICT(SCLA_LEAN_OUT), GET_STRIP_CLUB_LEAN_ANIM_NAME(SCLA_LEAN_OUT, bFemale))
							
							IF IS_BITMASK_AS_ENUM_SET(clubFriends[iFriendIndex].FriendFlag, STRIP_CLUB_FRIEND_FROZEN_ON_RAIL)							
								IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].ped)
									FREEZE_ENTITY_POSITION(clubFriends[iFriendIndex].ped, FALSE)
								ENDIF
								CLEAR_BITMASK_AS_ENUM(clubFriends[iFriendIndex].FriendFlag, STRIP_CLUB_FRIEND_FROZEN_ON_RAIL)							
							ENDIF
							
							TASK_FOLLOW_NAV_MESH_TO_COORD(clubFriends[iFriendIndex].ped, <<130.4892, -1307.1581, 28.1672>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 0.1023)
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_WAITING_TO_LEAVE
						ENDIF
						
					BREAK
					
					CASE SCA_FS_WAITING_TO_LEAVE
						
						//Player has left or is leaving strip club
						IF NOT IS_IN_CLUB_FLAG_SET() AND IS_GAMEPLAY_CAM_RENDERING() // don't quit friend activity during a cutscene
						OR stripClubStage = STAGE_CLUB_HOSTILE
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_FRIEND_ACTIVITY_ENDED
							DEBUG_MESSAGE("Player and friends have left strip club")
						ENDIF
					BREAK
					
					//Player has left the club - clean up friend activity
					CASE SCA_FS_FRIEND_ACTIVITY_ENDED
						IF IS_FRIEND_ACTIVITY_ON()
							CLEAR_STRIPCLUB_FRIEND_TASK(iFriendIndex, bFemale)
							
							IF stripClubStage != STAGE_CLUB_HOSTILE
								PLAY_PED_AMBIENT_SPEECH(clubFriends[iFriendIndex].ped, "STRIP_LEAVE_AGREE", SPEECH_PARAMS_FORCE_NORMAL)
							ENDIF
							
							IF NOT IS_PED_IN_GROUP(clubFriends[iFriendIndex].ped)
								SET_PED_AS_GROUP_MEMBER(clubFriends[iFriendIndex].ped, PLAYER_GROUP_ID())
							ENDIF
							
							clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_DONE
							DEBUG_MESSAGE("Ending Strip Club Friend Activity ")
						ENDIF
					BREAK
					
					CASE SCA_FS_DONE
						IF NOT IS_FRIEND_ACTIVITY_ON()
							IF DOES_BLIP_EXIST(clubFriends[iFriendIndex].blip)
								REMOVE_BLIP(clubFriends[iFriendIndex].blip)
							ENDIF
							
							IF NOT IS_IN_CLUB_FLAG_SET() AND IS_GAMEPLAY_CAM_RENDERING()
							AND stripClubStage != STAGE_CLUB_HOSTILE
								//restart friend activity if you leave and reenter club
								clubFriends[iFriendIndex].enumSCA_FriendState = SCA_FS_INIT
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_FRIEND_ACTIVITY_ON()
		BOOL bOnFriendActivity = FALSE
		
		REPEAT GET_STRIPCLUB_NUMBER_OF_FRIENDS() iFriendIndex
			IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].ped)
				IF clubFriends[iFriendIndex].enumSCA_FriendState != SCA_FS_DONE
				OR IS_ENTITY_IN_STRIPCLUB(clubFriends[iFriendIndex].ped)
					bOnFriendActivity = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT bOnFriendActivity OR stripClubStage = STAGE_CLUB_HOSTILE
			SET_FRIEND_ACTIVITY_ON(FALSE)
			
			REPEAT GET_STRIPCLUB_NUMBER_OF_FRIENDS() iFriendIndex
				IF DOES_ENTITY_EXIST(clubFriends[iFriendIndex].ped)
					CLEAR_STRIPCLUB_FRIEND_TASK(iFriendIndex, IS_PED_MODEL(clubFriends[iFriendIndex].ped, GET_NPC_PED_MODEL(CHAR_JIMMY)))
				ENDIF
			ENDREPEAT
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General2, STRIP_CLUB_GENERAL2_ASKED_FRIEND_TO_LEAVE)
			OR stripClubStage = STAGE_CLUB_HOSTILE
				finishActivity(ALOC_stripclub_southCentral, AR_playerQuit)
			ELSE
				finishActivity(ALOC_stripclub_southCentral, AR_playerWon)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
