///    Stripclub.sch
///    Author: John R. Diaz
///    Purpose- Keep all the declarations for the strip club here.
///    
USING "stripclub_public.sch"
USING "sclub_stats.sch"
USING "net_mission.sch"

// Constants
CONST_INT DANCE_TIP_COST 1
CONST_INT MAKE_IT_RAIN_COST_MP 3
CONST_INT MAKE_IT_RAIN_COST 6
CONST_INT SC_ALCOHOL_CUTOFF 4
CONST_INT SC_ALCOHOL_EFFECT_TIME 3*60*1000 //the actual time you are effected by drinks is really short, some bonuses for being drunk will be based off this timer.

CONST_INT STRIP_CLUB_DEBUG_LIKE_FACTOR	3
CONST_INT MIN_SHORT_DELAY_TIME 50
CONST_INT MAX_SHORT_DELAY_TIME 300
CONST_INT MIN_LONG_DELAY_TIME 500
CONST_INT MAX_LONG_DELAY_TIME 1500

CONST_INT TOUCH_TEST_TIME_MIN 500
CONST_INT TOUCH_TEST_TIME_MAX 501

CONST_INT DISPLAY_LIKE_HUD_TIME 7500
CONST_INT SAFE_DANCE_TIME 2000
CONST_INT BAR_TIME 14000
CONST_INT DRUG_TIME 4000
CONST_INT POLE_DANCE_TIME 60000
CONST_INT ANNOUNCER_DELAY_TIME 10000

CONST_INT APPROACH_MOD_MULT 50
CONST_INT APPROACH_MOD_MULT_INIT 100
CONST_INT AVAILABLE_MOD_MULT 500
CONST_INT AVAILABLE_MOD_MULT_INIT 1000
CONST_INT APPROACH_RAND_TIME_MIN 15000
CONST_INT APPROACH_RAND_TIME_MAX 32000
CONST_INT APPROACH_RAND_TIME_MIN_INIT 5000
CONST_INT APPROACH_RAND_TIME_MAX_INIT 16000
CONST_INT AVAILABLE_RAND_TIME_MIN 5000
CONST_INT AVAILABLE_RAND_TIME_MAX 10000
CONST_INT PICKUP_RAND_TIME_MIN 1000
CONST_INT PICKUP_RAND_TIME_MAX 3000

CONST_INT MAX_NUMBER_OF_STRIPCLUB_CUSTOMERS 30

CONST_FLOAT STRIPPER_GENERAL_DISLIKE_THRESHOLD 0.09
CONST_FLOAT APPROACH_THRESHOLD 0.70

CONST_INT NUMBER_STRIP_CLUB_RAIL_SEGMENTS 3

CONST_FLOAT STRIP_CLUB_LOAD_INSIDE_ASSETS_DISTANCE 20.0

CONST_FLOAT	STRIP_CLUB_BAR_APPROACH_DISTANCE 1.55
CONST_FLOAT	STRIP_CLUB_RAIL_APPROACH_DISTANCE 1.05

CONST_FLOAT STRIPPER_APPROACH_DISTANCE 2.2
CONST_FLOAT STRIPPER_AVAILABLE_DISTANCE 1.4
CONST_FLOAT STRIPPER_DECLINE_RUDE_DISTANCE 2.2

CONST_FLOAT FADE_IN_LIKE_HUD_SPEED 500.0
CONST_FLOAT FADE_OUT_LIKE_HUD_SPEED 150.0

CONST_FLOAT FIRST_PERSON_CAM_MOVE_SPEED 0.011

CONST_INT FIRST_PERSON_CAM_MOVE_X_LIMIT 22
CONST_INT FIRST_PERSON_CAM_MOVE_X_LIMIT_LAP 22
CONST_INT FIRST_PERSON_CAM_MOVE_Z_LIMIT 65
CONST_INT FIRST_PERSON_CAM_MOVE_Z_LIMIT_LAP 35

CONST_INT POST_HEIST_STRIPCLUB_SCENE_TIME		10000

CONST_FLOAT LIKE_NAME_SIZE 0.55
CONST_FLOAT LIKE_NAME_Y 0.8
CONST_FLOAT LIKE_BOX_CENTERX 0.82
CONST_FLOAT LIKE_BOX_WIDTH 0.21
CONST_FLOAT LIKE_BOX_TOPX 0.8
CONST_FLOAT LIKE_BOX_TOPY 0.775
CONST_FLOAT LIKE_BOX_HEIGHT 0.07
CONST_FLOAT LIKE_BOX_BACKGROUND_TOPX 0.3485
CONST_FLOAT LIKE_BOX_BACKGROUND_TOPY 0.108
CONST_FLOAT LIKE_BOX_BACKGROUND_WIDTH 0.303
CONST_FLOAT LIKE_BOX_BACKGROUND_HEIGHT 0.038
CONST_FLOAT LIKE_CONVERT_TO_HUD_FACTOR 333.333

CONST_FLOAT MAX_LAPDANCE_TALK_BAR 60.0

CONST_FLOAT SELECT_NAME_SIZE 0.45
CONST_FLOAT SELECT_NAME_X 0.8
CONST_FLOAT SELECT_NAME_Y 0.06
CONST_FLOAT SELECT_NAME_SEPARATE_Y 0.03

CONST_FLOAT STRIP_CLUB_VIPSEAT_OFFSET_X	2.1
CONST_FLOAT STRIP_CLUB_SCENARIO_RANGE	3.0
CONST_INT 	NUM_VIP_SEATS	7

CONST_INT	NUM_RAND_STAND_LOCS		7
CONST_INT	NUM_STRIPPER_POLES		3

CONST_INT	STRIPPER_LIKE_VALUE_TOUCH			1
CONST_INT	STRIPPER_LIKE_VALUE_WATCH			1
CONST_INT	STRIPPER_LIKE_VALUE_MONEY			10
CONST_INT	STRIPPER_LIKE_VALUE_DANCE			150
CONST_INT	STRIPPER_LIKE_VALUE_MP_FLIRT		1500
CONST_INT	STRIPPER_LIKE_VALUE_ANNOYED			-150

CONST_INT	NUM_VIP_CAMS	4

CONST_FLOAT TIME_BETWEEN_DANCE_DIALOGUE		5.0
CONST_FLOAT TIME_BETWEEN_TOUCH_DIALOGUE		15.0

CONST_FLOAT CONST_fRailAdjustZ 0.5
CONST_FLOAT CONST_fRailAdjustY -0.5

CONST_INT	NUM_GENERIC_STRIPPER_LOW 3

CONST_FLOAT	CONST_fRoomCheckExpired 1.0

// Enums
ENUM STRIP_CLUB_FRIEND_STATES_ENUM
	SCA_FS_INIT = 0,
	SCA_FS_SENDTOSTAGE,
	SCA_FS_GOTTOSTAGE,
	SCA_FS_STAND_NEAR_STAGE,
	SCA_FS_LOOP_STAGE_WATCH,
	SCA_FS_ASKED_TO_LEAVE,
	SCA_FS_LEAVING,
	SCA_FS_WAITING_TO_LEAVE,
	SCA_FS_FRIEND_ACTIVITY_ENDED,
	SCA_FS_DONE,
	SCA_FS_NUM_FRIEND_STATES
	
ENDENUM

ENUM STRIP_CLUB_STAGE_ENUM
	STAGE_INIT_CLUB = 0,
    STAGE_WANDER_CLUB,
    STAGE_GO_TO_VIP_ROOM,
    STAGE_DANCE,	
	STAGE_GO_HOME_WITH_STRIPPER,
    STAGE_CLUB_HOSTILE,
	STAGE_THROWOUT,
	STAGE_RETURN_FROM_HOSTILE
ENDENUM

ENUM STRIP_CLUB_INSIDE_STATE	
	SC_INSIDE_INIT = 0,
	SC_INSIDE_STREAMING,
	//SC_INSIDE_CREATE,
	SC_INSIDE_MANAGE,
	SC_INSIDE_CLEANUP
ENDENUM

ENUM DANCE_STAGE_ENUM
	DANCE_INIT = 0,
	DANCE_SET_PLAYER,
	DANCE_SET_PLAYER_CUTSCENE,
	DANCE_SET_STRIPPER,	
	DANCE_SET_STRIPPER_CUTSCENE,
	DANCE_IN_PROGRESS,
	DANCE_EXIT,
	DANCE_CANCELED,
	DANCE_FINISHED,
	DANCE_OFFERS,
	DANCE_OFFER_ANOTHER,
	DANCE_GET_ANOTHER,
	DANCE_TURN_DOWN,	
	DANCE_GET_SECOND_DANCER,
	DANCE_OFFER_HOME,
	DANCE_ACCEPTED_HOME,
	DANCE_CAUGHT_TOUCHING,
	DANCE_OUTRO_CUTSCENE,		
	DANCE_AFTER,
	DANCE_AFTER_TAKE_HOME
ENDENUM

ENUM TOUCHING_STRIPPER_STATE
	TSS_NONE,
	TSS_REACHING_MP,
	TSS_REACHING,
	TSS_TOUCHING,
	TSS_RETRACT,
	TSS_RETRACTING
ENDENUM

ENUM BAR_STAGE_ENUM
	BAR_NOT_ACTIVE = 0,
	BAR_INIT,
	BAR_GETTING_DRINK,
	BAR_USED,
	BAR_TOO_DRUNK,
	BAR_CLEANUP
ENDENUM

ENUM RAIL_STAGE_ENUM
	RAIL_NOT_ACTIVE = 0,
	RAIL_GO_TO_LEAN,
	RAIL_WATCHING,
	RAIL_USED
ENDENUM

ENUM STRIP_CLUB_STAFF_ENUM
	STAFF_BOUNCER_VIP,		 //Order matter must be 0
	STAFF_BOUNCER_PATROL,    //For the rest order does not matter
    STAFF_BARMAN,
	STAFF_ANNOUNCER,
	STAFF_FRONTDESK
ENDENUM

ENUM STRIPPER_AI_STATE_ENUM
	AI_STATE_WANDER = 0,
	AI_STATE_APPROACH_PLAYER,
	AI_STATE_GO_TO_VIP_ROOM,
	//AI_STATE_GO_TO_DRESSING_ROOM,
	AI_STATE_GIVE_DANCE,
	AI_STATE_POLE_DANCE,
	AI_STATE_GO_HOME_WITH_PLAYER
ENDENUM

ENUM STRIPPER_WANDER_STATE
	WANDER_STATE_NONE,
	WANDER_STATE_INIT,
	WANDER_STATE_MOVE,
	WANDER_STATE_GET_HEADING,
	WANDER_STATE_IDLE1,
	WANDER_STATE_IDLE2,
	WANDER_STATE_IDLE3,
	WANDER_STATE_END
ENDENUM

ENUM STAGE_STATE_ENUM
	STAGE_STATE_GIRL1 = 0,
	STAGE_STATE_GIRL2,
	STAGE_STATE_GIRL3,
	STAGE_STATE_GIRL4,
	STAGE_STATE_NONE
ENDENUM

ENUM BOUNCER_PATROL_STATE
	BOUNCER_PATROL_WALK_TO_COORD,
	BOUNCER_PATROL_WALK_TO_START,
	BOUNCER_PATROL_WALK_TO_PLAYER,
	BOUNCER_PATROL_ACHIEVE_HEADING,
	BOUNCER_PATROL_TASK_STAND_STILL,
	BOUNCER_PATROL_STAND_STILL,
	BOUNCER_PATROL_FACE_PLAYER,
	BOUNCER_PATROL_YELL
ENDENUM

ENUM DJ_STATE
	DJ_STATE_INIT,
	DJ_STATE_PLAY_ANIM,
	DJ_STATE_NEW_ANIM
ENDENUM

ENUM POLEDANCER_STATE_ENUM
	POLEDANCER_STATE_WAIT_IN_DRESSING_ROOM = 0,
	POLEDANCER_STATE_GOTO_STAGE_INTRO,
	POLEDANCER_STATE_GOTO_STAGE,
	POLEDANCER_STATE_GOTO_POLE,
	POLEDANCER_STATE_DANCE_ENTER,
	POLEDANCER_STATE_DANCE,
	POLEDANCER_STATE_DANCE_EXIT,
	POLEDANCER_STATE_GOTO_MONEY_FROM_POLE,
	POLEDANCER_STATE_PRIVATE_DANCE,
	POLEDANCER_STATE_TAKE_MONEY,
	POLEDANCER_STATE_RETURN_FROM_PRIVATE_DANCE,
	POLEDANCER_STATE_RETURN_TO_STAGE,
	POLEDANCER_STATE_EXIT_STAGE,
	POLEDANCER_STATE_GOTO_DRESSING_ROOM

ENDENUM

ENUM STAGEDANCER_STATE_ENUM
	STAGEDANCER_STATE_WAIT_FOR_GOTO_MONEY,
	STAGEDANCER_STATE_GOTO_MONEY,
	STAGEDANCER_STATE_PRIVATE_DANCE,
	STAGEDANCER_STATE_TAKE_MONEY
ENDENUM


ENUM STRIPPER_SPEECH_TARGET_ENUM
	STRSPEECH_NONE = 0,
	STRSPEECH_OFFER,
	STRSPEECH_APPROACHED,
	STRSPEECH_FOLLOW,
	STRSPEECH_FOLLOW2,
	STRSPEECH_FOLLOWN,
	STRSPEECH_DECLINED_POLITE,
	STRSPEECH_DECLINED_RUDE,
	STRSPEECH_AGAIN,
	STRSPEECH_AGAIN2,	
	STRSPEECH_ASK_HOME,
	STRSPEECH_GIVE_NUMBER_AFTER_DANCE,	
	STRSPEECH_DNC_TALK,
	STRSPEECH_SIT_TALK,
	STRSPEECH_NO_MONEY_TALK,
	STRSPEECH_HURRY,
	STRSPEECH_WANTS_HOME,
	STRSPEECH_RAIL_FLIRT,
	STRSPEECH_ON_STAGE,
	STRSPEECH_GOING_TO_STAGE,
	STRSPEECH_WONT_DATE
ENDENUM

ENUM DIALOUGE_SPEAKERS_ENUM
	SPEAKER_PLAYER = 0,	
	SPEAKER_BOUNCER_PATROL = 2,
	SPEAKER_FRIEND = 3,
	SPEAKER_CASHIER = 4,
	SPEAKER_BOUNCER_VIP = 5,
	SPEAKER_BARMAID = 6,
	
	SPEAKER_ANNOUNCER = 8
ENDENUM

ENUM STRIPCLUB_CAM_ENUM
	STRCLUB_CAM_DANCE_FIRST_PERSON = 0,
	STRCLUB_CAM_DANCE_BEHIND_PLAYER,
	STRCLUB_CAM_DANCE_FACE_PLAYER,
	STRCLUB_CAM_DANCE_ANGLED,
	STRCLUB_CAM_BAR,
	STRCLUB_CAM_RAIL,
	STRIPCLUB_CAM_THROW_OUT, 
	STRCLUB_CAM_CALL_STRIPPER
ENDENUM

ENUM STRIPCLUB_GOTO_ROOM_STATE
	STRIPCLUB_GOTO_ROOM_INIT,
	STRIPCLUB_GOTO_ROOM_STREAMING,
	STRIPCLUB_GOTO_ROOM_TRIGGER_CUTSCENE,
	STRIPCLUB_GOTO_ROOM_CUTSCENE_TASK_STRIPPER,
	STRIPCLUB_GOTO_ROOM_CUTSCENE_TASK_PLAYER,
	STRIPCLUB_GOTO_ROOM_CUTSCENE_APPROACH_CHAIR,
	STRIPCLUB_GOTO_ROOM_CUTSCENE_COMPLETE
ENDENUM

ENUM STRIPCLUB_VIP_SEATS
	STRIPCLUB_VIP_SEAT_01 = 0,
	STRIPCLUB_VIP_SEAT_02,
	STRIPCLUB_VIP_SEAT_03,
	STRIPCLUB_VIP_SEAT_04,
	STRIPCLUB_VIP_SEAT_05,
	STRIPCLUB_VIP_SEAT_06
ENDENUM

ENUM STRIPCLUB_ROOMS
	SCLUB_ROOM_MAIN,
	SCLUB_ROOM_VIP,
	SCLUB_ROOM_DRESSING,
	SCLUB_ROOM_BACK
ENDENUM

ENUM STRIPCLUB_WAYPOINT_RECORDINGS
	SWR_VIP_INTRO,
	SWR_VIP_OUTRO,
	SWR_FRIEND_ENTER,
	SWR_STRIPPER_EXIT
ENDENUM

ENUM STRIP_CLUB_STREAMING_BIT_FLAGS		
	STRIP_CLUB_BIT_ANIM_DRINKING = BIT0,
	STRIP_CLUB_BIT_PROP_CASH_STREAMED = BIT1,	
	STRIP_CLUB_BIT_ANIM_BOUNCER_STOP = BIT2,
	STRIP_CLUB_BIT_MODEL_GENERAL = BIT3,
	STRIP_CLUB_BIT_ANIM_STRIPPER_IDLES_STREAMED = BIT4,
	STRIP_CLUB_BIT_ANIM_PICKUP_OBJECT_STREAMED = BIT5,
	
	STRIP_CLUB_BIT_ANIM_STRIPPER_IDLES = BIT7,

	STRIP_CLUB_BIT_ANIM_LEAN = BIT9,
	STRIP_CLUB_BIT_INSIDE_PROP_STREAMED = BIT10,
	
	STRIP_CLUB_BIT_DRINKING_SCRIPT_STREAMED = BIT11,
	
	STRIP_CLUB_BIT_DANCING_INTRO_STREAMED = BIT12,
	STRIP_CLUB_BIT_LAPDANCE_STREAMED = BIT13,
	STRIP_CLUB_BIT_POLEDANCE_STREAMED = BIT15,
	STRIP_CLUB_BIT_POLEDANCE_DANCE_STREAMED = BIT16,
	STRIP_CLUB_BIT_THROWOUT_STREAMED = BIT17,
	STRIP_CLUB_BIT_STAGE_DANCE_STREAMED = BIT18,
	STRIP_CLUB_BIT_ANIM_BOUNCER_YELL = BIT19,
	STRIP_CLUB_BIT_BARTENDER_NEEDS_REPOSITION = BIT20,
	STRIP_CLUB_PLAYER_CONTROLS_STREAMED = BIT21,
	STRIP_CLUB_PLAYER_IS_IN_INTERIOR = BIT22, 
	STRIP_CLUB_INSIDE_NEEDS_CLEANUP = BIT23,
	STRIP_CLUB_NEED_TO_FINALIZE_CLOTHS_CHANGE = BIT24,
	STRIP_CLUB_REMOVED_NIGHTVISION = BIT25,
	STRIP_CLUB_REMOVED_PARACHUTE = BIT26,
	STRIP_CLUB_YACHT_AUDIO_BANK_LOADED = BIT27
ENDENUM

ENUM STRIP_CLUB_CUTSCENE_BIT_FLAGS
	STRIP_CLUB_BIT_OUTRO_DANCE_SCENE = BIT0,
	
	STRIP_CLUB_BIT_OUTRO_DANCE_PLAYER_TASKED = BIT2,	
	STRIP_CLUB_BIT_OUTRO_DANCE_BOUNCER_TASKED = BIT3,
	STRIP_CLUB_BIT_SECOND_STRIPPER_TASKED = BIT4,
	STRIP_CLUB_BIT_FOLLOW_SCENE_FINISHED = BIT5,
	STRIP_CLUB_BIT_FOLLOW_SCENE_START = BIT6,
	STRIP_CLUB_BIT_VIP_OUTRO_SKIPPED = BIT7,
	STRIP_CLUB_BIT_CALL_STRIPPER = BIT8,
	STRIP_CLUB_BIT_DISABLE_LEAVE_VIP_ROOM_TEXT = BIT9,
	STRIP_CLUB_BIT_REJECTED_HOME = BIT10,
	STRIP_CLUB_BIT_MOVE_DANCERS_FOR_VIP_CUTSCENE = BIT11,
	STRIP_CLUB_BIT_JUST_ASK_FRIEND_TO_LEAVE = BIT12,
	STRIP_CLUB_BIT_VIP_INTRO_SKIPPED = BIT13,
	STRIP_CLUB_SWITCHED_INSIDE_TO_TREVOR = BIT14,
	STRIP_CLUB_DELAY_ASK_TO_LEAVE = BIT15,
	STRIP_CLUB_ON_RAIL_AFTER_HEIST_SCENE = BIT17,
	STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_1 = BIT18,
	STRIP_CLUB_STANDING_AFTER_HEIST_SCENE_2 = BIT19,
	STRIP_CLUB_TURN_OFF_HEIST_AUDIO_STAGE = BIT20
ENDENUM

ENUM STRIP_CLUB_WORKER_BIT_FLAGS

	STRIP_CLUB_BIT_WORKER_BOUNCER_VIP_STOP = BIT1,
	STRIP_CLUB_BIT_WORKER_BOUNCER_VIP_STOP2 = BIT2,
	STRIP_CLUB_BIT_WORKER_GENERAL_ARMED = BIT3,

	STRIP_CLUB_BIT_WORKER_FORCE_LAPDANCE_RETRACT = BIT4,
	STRIP_CLUB_BIT_FRONTDESK_HAS_COWERED = BIT5,
	STRIP_CLUB_BIT_STOP_NON_BOUNCERS_HOSTILE = BIT6, //if flag is set, club workers and strippers won't go hostile
	STRIP_CLUB_BIT_WORKER_BOUNCER_AGGRO = BIT7, //allow bouncers to aggro on player
	STRIP_CLUB_BIT_WORKER_FRONT_AGGRO_FROM_MELEE = BIT8,
	STRIP_CLUB_BIT_WORKER_FRONT_AGGRO_FROM_SHOOTING = BIT9,
	
	STRIP_CLUB_BIT_WORKER_DANCER_PLAYER_LEAVE_STAGE = BIT10,
	STRIP_CLUB_BIT_WORKER_DANCE_STAGE_INIT = BIT11,
	
	STRIP_CLUB_BIT_WORKER_ADD_INSIDE_BOUNCER_DIALOGUE = BIT15,
	STRIP_CLUB_BIT_WORKER_ADD_INSIDE_ANNOUNCER_DIALOGUE = BIT16,
	
	STRIP_CLUB_BIT_WORKER_BOUNCER_SAID_TREVOR_CRAZY = BIT17,
	
	STRIP_CLUB_BIT_WORKER_BOUNCER_VIP_FIGHT = BIT19,
	STRIP_CLUB_BIT_WORKER_BOUNCER_PATROL_FIGHT = BIT20,
	STRIP_CLUB_BIT_WORKER_DONT_UPDATE_STAGE = BIT21
ENDENUM

ENUM STRIP_CLUB_FRIEND_BIT_FLAGS
	STRIP_CLUB_FRIEND_FROZEN_ON_RAIL = BIT0
ENDENUM

//ENUM STRIP_CLUB_STRIPPER_BIT_FLAGS
//	STRIP_CLUB_STRIPPER_TASKED_MOVE = BIT0
//	STRIP_CLUB_GOT_STRIPPER_LOCATION = BIT1
//ENDENUM

ENUM STRIP_CLUB_GENERAL_BIT_FLAGS
	
	STRIP_CLUB_STRIPPER_JUST_CREATED = BIT0,
	STRIP_CLUB_GENERAL_INIT_RAILS = BIT1,
	STRIP_CLUB_GENERAL_RESPONSE_TALK = BIT2,
	STRIP_CLUB_GENERAL_RESPONSE_TOUCH = BIT3,
	STRIP_CLUB_GENERAL_FOCUS_ON_BOUNCER = BIT4,
	STRIP_CLUB_GENERAL_IGNORING_SHOCKING_EVENTS = BIT5,
	STRIP_CLUB_GENERAL_PLAYER_CONTROL_REMOVED = BIT6,
	STRIP_CLUB_GENERAL_HAD_TWO_WAY_DANCE = BIT7,
	STRIP_CLUB_GENERAL_DISABLED_DOOR_NAVMESH = BIT8,
	STRIP_CLUB_GENERAL_DEBUG_DRAWING_ACTIVE = BIT9,
	STRIP_CLUB_GENERAL_SOLICIT_HELP = BIT10,
	STRIP_CLUB_GENERAL_SOLICIT = BIT11,
	STRIP_CLUB_GENERAL_PLAYER_GOT_DRUNK = BIT12,
	STRIP_CLUB_GENERAL_LEAN_DISPLAYED = BIT13,
	STRIP_CLUB_GENERAL_ASSISTED_ENABLED = BIT14,
	STRIP_CLUB_GENERAL_PLAYER_HAD_DRINK = BIT15,
	STRIP_CLUB_GENERAL_PLAYER_HAD_DRINK_W_FRIEND = BIT16,
	STRIP_CLUB_GENERAL_PLAYER_VISITED = BIT17,

	STRIP_CLUB_GENERAL_SOLICIT_DUO = BIT19,
	STRIP_CLUB_GENERAL_CREATE_REL_GROUP = BIT20,
	
	
	STRIP_CLUB_GENERAL_SOLICIT2_HELP = BIT23,
	STRIP_CLUB_GENERAL_POLE_DANCERS_CREATED = BIT24,
	STRIP_CLUB_GENERAL_VIP_ROOM_CLEAR  = BIT25,
	STRIP_CLUB_GENERAL_PRIVATE_DANCE1_PLAYED = BIT26,
	STRIP_CLUB_GENERAL_PRIVATE_DANCE2_PLAYED = BIT27,
	STRIP_CLUB_GENERAL_PRIVATE_DANCE3_PLAYED = BIT28,
	STRIP_CLUB_GENERAL_VIP_ROOM_CUTSCENE_SKIPPED = BIT29
ENDENUM

ENUM STRIP_CLUB_GENERAL_BIT_FLAGS2
// Bools
	STRIP_CLUB_GENERAL2_HIGH_FUNCTION = BIT1,
	STRIP_CLUB_GENERAL2_IN_CLUB = BIT2,
	STRIP_CLUB_GENERAL2_CAN_APPROACH = BIT3,
	STRIP_CLUB_GENERAL2_BEEN_IN_CLUB = BIT4,
	STRIP_CLUB_GENERAL2_HOSTILE_FROM_SHOOTING = BIT5,
	STRIP_CLUB_GENERAL2_HOSTILE_FROM_MELEE = BIT6,
	STRIP_CLUB_GENERAL2_SHOW_GOD_TEXT = BIT7,
	STRIP_CLUB_GENERAL2_DONE_STRIPPER_SPEECH_THIS_FRAME = BIT8,
	STRIP_CLUB_GENERAL2_MOENY_ON_RAIL = BIT9,
	STRIP_CLUB_GENERAL2_KEEP_HELP_AND_SPEECH_FROM_DANCE = BIT10,
	STRIP_CLUB_GENERAL2_IS_FRIEND_ACTIVITY_ON = BIT11,
	STRIP_CLUB_GENERAL2_SHOW_LIKE_HUD = BIT12,
	STRIP_CLUB_GENERAL2_PLAYER_NEAR_RAIL = BIT13,
	STRIP_CLUB_GENERAL2_FORCE_NOT_IN_CLUB = BIT14,
	STRIP_CLUB_GENERAL2_DISPLAYED_SELECT_HELP = BIT15,
	STRIP_CLUB_GENERAL2_CALLED_CURRENT_POLEDANCER = BIT16,
	
	STRIP_CLUB_GENERAL2_SET_LAPDANCE_SYNC_SCENE = BIT17,
	STRIP_CLUB_GENERAL2_SET_SYNC_CAMERA_THIS_FRAME = BIT18,
	STRIP_CLUB_GENERAL2_HAS_VIEWED_PRIVATE_DANCE = BIT19,
	STRIP_CLUB_GENERAL2_SET_DOUBLE_DANCE_SYNC_SCENE = BIT20,
	STRIP_CLUB_GENERAL2_ASKED_FRIEND_TO_LEAVE = BIT21,
	
	STRIP_CLUB_GENERAL2_DISPLAYED_STORE_ALERT = BIT30
ENDENUM

ENUM STRIP_CLUB_STRIPPER_BIT_FLAGS
	STRIPPER_FLAG_SAID_APPROACH = BIT0,
	STRIPPER_FLAG_SAID_SIT = BIT1,
	STRIPPER_FLAG_SAID_WANT_HOME = BIT2,
	STRIPPER_FLAG_SAID_GO_TO_STAGE = BIT3,
	STRIPPER_FLAG_LOCKED_CONTROL = BIT4,
	STRIPPER_FLAG_TASKED_MOVE_TO_RAND = BIT5,
	STRIPPER_FLAG_DISPLAYEED_LIKE_METER_FULL = BIT6,
	STRIPPER_FLAG_HAD_LAPDANCE = BIT7
ENDENUM

ENUM STRIPCLUB_WORKER_TASK_ID
	ID_STRIPPER_0,
	ID_STRIPPER_1,
	ID_STRIPPER_2,
	ID_STRIPPER_3,
	ID_GEN_STRIPPER_0,
	ID_GEN_STRIPPER_1,
	ID_GEN_STRIPPER_2,
	ID_BOUNCER_FRONT,
    ID_BARMAN,
	ID_BOUNCER_VIP,
	ID_BOUNCER_BACKDOOR,
	ID_ANNOUNCER,
	ID_FRONTDESK
ENDENUM

ENUM FRONT_CASHIER_FLAGS
	CASHIER_FLAG_GREET = BIT1,
	CASHIER_FLAG_DO_NOT_GREET = BIT2
ENDENUM

// Structs
STRUCT NAMED_STRIPPER_PED
	PED_INDEX ped 
	BOOTY_CALL_CONTACT_ENUM stripperID
	STRIPPER_AI_STATE_ENUM aiState
	STRIPPER_WANDER_STATE wanderState
	POLEDANCER_STATE_ENUM poledancerState
	STRIPPER_SPEECH_TARGET_ENUM speechTarget
	INT stripperFlags //STRIP_CLUB_STRIPPER_BIT_FLAGS
	INT iNextApproachTime
	INT iNextMoveTime
	//INT iNextAvailableTime
	INT iCurrentStandPos
	FLOAT fSquareDistanceToPlayer
ENDSTRUCT

STRUCT STAGE_STRIPPER_PED
	PED_INDEX ped 
	STAGEDANCER_STATE_ENUM stagedancerState
ENDSTRUCT

STRUCT RAIL_SEGMENT
	VECTOR vPoint1
	VECTOR vPoint2
	FLOAT fRailAngle
	ECOMPASS direction 
ENDSTRUCT

STRUCT STRIPCLUB_CAM_PARAMS
	VECTOR	vCamPos			
	VECTOR 	vCamRot			
	FLOAT 	fCamFOV 		
ENDSTRUCT

STRUCT STRIPCLUB_FRIEND_DATA
	PED_INDEX ped
	BLIP_INDEX blip
	SEQUENCE_INDEX sequence
	FLOAT fRailHeading
	STRIP_CLUB_FRIEND_STATES_ENUM enumSCA_FriendState = SCA_FS_INIT
	INT FriendFlag
	INT iNextThrowTime
	OBJECT_INDEX objFriendMoney
	SCENARIO_BLOCKING_INDEX blockingIndex
ENDSTRUCT

STRUCT DRINKING_DATA
	PED_INDEX pedBartender
	OBJECT_INDEX objGlass, objBottle
	INT iBartenderID
	INT iIsMP
	BOOL bInsideYacht = FALSE
	INT iYachtInstance = -1
	NETWORK_INDEX niPedBartender
ENDSTRUCT

#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID	 		stripClubWidgets

	
#ENDIF
//EOF
