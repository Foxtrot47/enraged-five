
STRUCT STRIPCLUB_SERVER_BROADCAST_DATA
	NETWORK_INDEX clubWorkers[MAX_NUMBER_CLUB_STAFF]
	NETWORK_INDEX namedStrippers[MAX_NUMBER_NAMED_STRIPPERS_MP]
	NETWORK_INDEX netBottle, netGlass

	INT iStripperInUseBy[MAX_NUMBER_NAMED_STRIPPERS_MP] //the participant index as int of the player who owns the stripper
	STRIPPER_WANDER_STATE stripperWanderState[MAX_NUMBER_NAMED_STRIPPERS_MP] // make sure the wander state of the strippers is synced on all machines
	STRIPPER_AI_STATE_ENUM stripperAIState[MAX_NUMBER_NAMED_STRIPPERS_MP]
	INT iWalkLocationsInUse
	INT iStripperStandLocations[MAX_NUMBER_NAMED_STRIPPERS_MP]
	INT iStartTime = -1
	
	POLEDANCER_STATE_ENUM poledancerState[MAX_NUMBER_NAMED_STRIPPERS_MP]
	INT iNumberOfPoleDances
	STAGE_STATE_ENUM severStageState
	PARTICIPANT_INDEX serverPedForPrivateDance
	
	DJ_STATE syncDJState
	INT iCurrentDJAnim
	INT iPoleDancerNetSceneID[MAX_NUMBER_NAMED_STRIPPERS_MP]
	
	BOOL bIsAnyoneInClub
	BOOL bClubHostile
	BOOL bInsidePedsCreated
	BOOL bStripperDoneWithEndScene
	BOOL bLockFindPlayerToGiveRailDance

ENDSTRUCT

//Create the dance sync scene locally
INT iPoleDancerNextNetSceneID[3]

ENUM STRIPCLUB_PLAYER_BROADCAST_BITS
	
	SCLUB_PLAYER_BIT_STRIPPER_1_IN_USE = 1,
	SCLUB_PLAYER_BIT_STRIPPER_2_IN_USE = 2,
	SCLUB_PLAYER_BIT_STRIPPER_3_IN_USE = 3,
	SCLUB_PLAYER_BIT_STRIPPER_4_IN_USE = 4,
	SCLUB_PLAYER_BIT_IN_CLUB = 5,
	SCLUB_PLAYER_BIT_WENT_HOSTILE = 6,
	SCLUB_PLAYER_BIT_CALLED_STRIPPER = 7,
	SCLUB_PLAYER_BIT_TASKED_STRIPPER_1_TO_RAND = 8,
	SCLUB_PLAYER_BIT_TASKED_STRIPPER_2_TO_RAND = 9,
	SCLUB_PLAYER_BIT_TASKED_STRIPPER_3_TO_RAND = 10,
	SCLUB_PLAYER_BIT_TASKED_STRIPPER_4_TO_RAND = 11,
	SCLUB_PLAYER_BIT_ON_RAIL_1 = 12,
	SCLUB_PLAYER_BIT_ON_RAIL_2 = 13,
	SCLUB_PLAYER_BIT_ON_RAIL_3 = 14
ENDENUM

ENUM SERVER_SYNC_STATE //don't sync everything at once because it takes too long
	SYNC_CLUB_STRIPPERS = 0,
	SYNC_CLUB_POLEDANCERS,
	SYNC_CLUB_WORKERS,
	SYNC_CLUB_OBJECTS
ENDENUM

STRUCT STRIPCLUB_PLAYER_BROADCAST_DATA
	INT iPlayerStripclubBroadcast
	INT iPoledancerLikeBroadcast
	
	INT iSyncStripperState = 0
	INT iSyncPoleState = -1
	STRIPPER_AI_STATE_ENUM stripperAIState[MAX_NUMBER_NAMED_STRIPPERS_MP]
	POLEDANCER_STATE_ENUM poledancerState[MAX_NUMBER_NAMED_STRIPPERS_MP]
	
	BOOL bStartedHiestScene
ENDSTRUCT

STRIPCLUB_SERVER_BROADCAST_DATA serverBD
STRIPCLUB_PLAYER_BROADCAST_DATA clubPlayerBD[MP_STRIPCLUB_MAX_PLAYERS]
SERVER_SYNC_STATE stripServerSyncState
INT iServerSyncPlayers

BOOL bAnyoneInClub = FALSE
BOOL bAnyoneWentHostile = FALSE
BOOL bOwnsAllPeds = FALSE
BOOL bPlayerResetAfterHostile = FALSE

PROC SET_PLAYER_ON_RAIL_BIT(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iRail, BOOL bOnRail)
	IF bOnRail AND NOT IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, iRail)
		CPRINTLN(DEBUG_SCLUB, "SET_PLAYER_ON_RAIL_BIT: Setting bit for rail number = ", iRail)
		SET_BIT(playerBD.iPlayerStripclubBroadcast, iRail + ENUM_TO_INT(SCLUB_PLAYER_BIT_ON_RAIL_1))
	ELIF NOT bOnRail
		CPRINTLN(DEBUG_SCLUB, "SET_PLAYER_ON_RAIL_BIT: Clearing bit for all rails")
		CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_ON_RAIL_1))
		CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_ON_RAIL_2))
		CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_ON_RAIL_3))
	ENDIF
ENDPROC

PROC SET_PLAYER_STRIPPER_IN_USE(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iStripper, BOOL bInUse)
	
	IF bInUse AND NOT IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, iStripper+1)
		CPRINTLN(DEBUG_SCLUB, "Setting in use stripper bit ", iStripper+1)
		SET_BIT(playerBD.iPlayerStripclubBroadcast, iStripper + 1)
		
	ELIF NOT bInUse AND IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, iStripper+1)
		CPRINTLN(DEBUG_SCLUB, "Clearing in use stripper bit ", iStripper+1)
		CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, iStripper + 1)
	ENDIF
ENDPROC

//stripperInUse is one higher then the actual stripper index
FUNC INT GET_PLAYER_STRIPPER_IN_USE(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT &iRetSecondary)	
	INT index
	INT iRetStripper = -1
	iRetSecondary = -1
	
	REPEAT 4 index
		IF IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, index+1)
			IF iRetStripper = -1
				iRetStripper = index
			ELSE
				iRetSecondary = index
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iRetStripper
ENDFUNC

PROC SET_PLAYER_STRIPPER_TASKED_TO_RAND(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iStripper, BOOL bInUse)
	
	INT iBit = iStripper+ENUM_TO_INT(SCLUB_PLAYER_BIT_TASKED_STRIPPER_1_TO_RAND)
	
	IF bInUse AND NOT IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, iBit)
		PRINTLN("Setting tasked to rand stripper bit ", iBit)
		SET_BIT(playerBD.iPlayerStripclubBroadcast, iBit)
		
	ELIF NOT bInUse AND IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, iBit)
		PRINTLN("Clearing in use stripper bit ", iBit)
		CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, iBit)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_STRIPPER_TASKED_TO_RAND_SET(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iStripper)
	RETURN IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, iStripper+ENUM_TO_INT(SCLUB_PLAYER_BIT_TASKED_STRIPPER_1_TO_RAND))
ENDFUNC

FUNC BOOL IS_PLAYER_BROADCAST_IN_CLUB(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD)
	RETURN IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_IN_CLUB))
ENDFUNC

PROC SET_PLAYER_BROADCAST_IN_CLUB(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, BOOL bSetInClub)
	IF IS_PLAYER_BROADCAST_IN_CLUB(playerBD) != bSetInClub
		PRINTLN("Setting player broadcast in club")
		IF bSetInClub
			SET_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_IN_CLUB))
		ELSE
			CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_IN_CLUB))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_BROADCAST_WENT_HOSTILE(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD)
	RETURN IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_WENT_HOSTILE))
ENDFUNC

PROC SET_PLAYER_BROADCAST_WENT_HOSTILE(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, BOOL bSetHostile)
	IF IS_PLAYER_BROADCAST_WENT_HOSTILE(playerBD) != bSetHostile
		PRINTLN("Setting player broadcast hostile")
		IF bSetHostile
			SET_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_WENT_HOSTILE))
		ELSE
			CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_WENT_HOSTILE))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_BROADCAST_CALLED_STRIPPER(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD)
	RETURN IS_BIT_SET(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_CALLED_STRIPPER))
ENDFUNC

PROC SET_PLAYER_BROADCAST_CALL_STRIPPER(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, BOOL bSetCall)
	IF IS_PLAYER_BROADCAST_CALLED_STRIPPER(playerBD) != bSetCall
		PRINTLN("Setting player broadcast calling stripper")
		IF bSetCall
			SET_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_CALLED_STRIPPER))
		ELSE
			CLEAR_BIT(playerBD.iPlayerStripclubBroadcast, ENUM_TO_INT(SCLUB_PLAYER_BIT_CALLED_STRIPPER))
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD)
	INT i
	
	IF playerBD.iSyncStripperState = 0 //most common case, no strippers need sync
		RETURN -1
	ENDIF
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF IS_BIT_SET(playerBD.iSyncStripperState, i)
		 	RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

PROC SET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iStripper)
	IF iStripper >= 0
		IF NOT IS_BIT_SET(playerBD.iSyncStripperState, iStripper)
			CPRINTLN(DEBUG_SCLUB, "Set stripper need syn state ", iStripper)
			SET_BIT(playerBD.iSyncStripperState, iStripper) //set the bit of the stripper to sync
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iStripper)
	IF iStripper >= 0
		IF IS_BIT_SET(playerBD.iSyncStripperState, iStripper)
			CPRINTLN(DEBUG_SCLUB, "Removing stripper need sync state ", iStripper)
			CLEAR_BIT(playerBD.iSyncStripperState, iStripper) //clear the bit of the stripper to sync
		ENDIF
	ENDIF
ENDPROC


FUNC INT GET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD)
	RETURN playerBD.iSyncPoleState
ENDFUNC

PROC SET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iStripper)
	IF GET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(playerBD) != iStripper
		PRINTLN("Changing sync pole to ", iStripper)
		playerBD.iSyncPoleState = iStripper
	ENDIF
ENDPROC


FUNC INT GET_POLE_DANCER_LIKE_BROADCAST(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD)
	RETURN playerBD.iPoledancerLikeBroadcast
ENDFUNC

PROC SET_POLE_DANCER_LIKE_BROADCAST(STRIPCLUB_PLAYER_BROADCAST_DATA &playerBD, INT iLike)
	IF GET_POLE_DANCER_LIKE_BROADCAST(playerBD) != iLike
		PRINTLN("Chaing broadcast like to ", iLike)
		playerBD.iPoledancerLikeBroadcast = iLike
	ENDIF
ENDPROC

FUNC BOOL IS_RAIL_AVAILABLE_FOR_LEAN(INT iRailIndex)
	IF NOT IS_STRIPCLUB_MP()
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_GLOBAL_HAS_LOCAL_PLAYER_OPTED_IN_TO_BE_BEAST()
		RETURN FALSE
	ENDIF
	
	INT idx
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS idx
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(idx))
			IF IS_BIT_SET(clubPlayerBD[idx].iPlayerStripclubBroadcast, iRailIndex + ENUM_TO_INT(SCLUB_PLAYER_BIT_ON_RAIL_1))
				IF INT_TO_PARTICIPANTINDEX(idx) = PARTICIPANT_ID()
					CPRINTLN(DEBUG_SCLUB, "IS_RAIL_AVAILABLE_FOR_LEAN: iRailIndex: ", iRailIndex, " you are leaning on it and have a lower ID then anyone else trying to lean on it.")
					RETURN TRUE
				ELSE
					CPRINTLN(DEBUG_SCLUB, "IS_RAIL_AVAILABLE_FOR_LEAN: iRailIndex: ", iRailIndex, " not available for lean")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_PLAYER_WHO_HAS_STRIPPER_IN_USE(INT iStripper)

	INT index

	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
	
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(index)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF IS_BIT_SET(clubPlayerBD[index].iPlayerStripclubBroadcast, iStripper+1)
				RETURN index
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_ANY_PLAYER_TASKED_STRIPPER_TO_RAND(INT iStripper)
	INT index

	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
	
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(index)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF IS_PLAYER_STRIPPER_TASKED_TO_RAND_SET(clubPlayerBD[index], iStripper)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYER_CALLING_STRIPPER()

	INT index
	INT iHighestLikeValue = -1
	INT iPlayerWithHighestLike = -1
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
	
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(index)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF IS_PLAYER_BROADCAST_CALLED_STRIPPER(clubPlayerBD[index])
				IF iHighestLikeValue < GET_POLE_DANCER_LIKE_BROADCAST(clubPlayerBD[index])
					iHighestLikeValue = GET_POLE_DANCER_LIKE_BROADCAST(clubPlayerBD[index])
					iPlayerWithHighestLike = index
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iPlayerWithHighestLike
ENDFUNC

FUNC BOOL STRIPCLUB_PLAYER_DOES_CONTROL_PED(NETWORK_INDEX networkIndex, PED_INDEX pedIndex)

	BOOL bOwnsPed = TRUE
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedIndex)
		bOwnsPed = FALSE
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(networkIndex)		
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(networkIndex)
			bOwnsPed = FALSE
		ENDIF
	ENDIF
	
	//returns true when script has control of the peds
	RETURN bOwnsPed
ENDFUNC

PROC SYNC_STRIPPER_AI_STATES_SERVER()
	INT index
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
	
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(index)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			INT iStripperToSync = GET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[index])
			IF iStripperToSync >= 0
				IF serverBD.stripperAIState[iStripperToSync] != clubPlayerBD[index].stripperAIState[iStripperToSync]
					CPRINTLN(DEBUG_SCLUB, "Player ", index, " changed strippers ", iStripperToSync, " state to ",  clubPlayerBD[index].stripperAIState[iStripperToSync])
					serverBD.stripperAIState[iStripperToSync] = clubPlayerBD[index].stripperAIState[iStripperToSync]
				ENDIF
			ENDIF
			
			iStripperToSync = GET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(clubPlayerBD[index])
			IF iStripperToSync >= 0
				IF serverBD.poledancerState[iStripperToSync] != clubPlayerBD[index].poledancerState[iStripperToSync]
					serverBD.poledancerState[iStripperToSync] = clubPlayerBD[index].poledancerState[iStripperToSync]
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC SYNC_STRIPPER_AI_STATES_CLIENT()
	INT i
	
	REPEAT GET_MAX_NUMBER_OF_NAMED_STRIPPERS() i
		IF i = GET_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()])
			IF serverBD.stripperAIState[i] = clubPlayerBD[PARTICIPANT_ID_TO_INT()].stripperAIState[i]
				CLEAR_PLAYER_BROADCAST_STRIPPER_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()], i)
			ENDIF
		ENDIF
		
		//pole dance 'need sync' works different, it can only handle on stripper at a time.
		IF i = GET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()])
			IF serverBD.poledancerState[i] = clubPlayerBD[PARTICIPANT_ID_TO_INT()].poledancerState[i]	
				SET_PLAYER_BROADCAST_STRIPPER_POLE_STATE_TO_SYNC(clubPlayerBD[PARTICIPANT_ID_TO_INT()], -1)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_PLAYER_STARTED_STRICLUB_HIEST_SCENE()
	IF NOT clubPlayerBD[PARTICIPANT_ID_TO_INT()].bStartedHiestScene
		clubPlayerBD[PARTICIPANT_ID_TO_INT()].bStartedHiestScene = TRUE
	ENDIF
ENDPROC

FUNC BOOL ALL_PLAYERS_STARTED_STRIPCLUB_HIEST_SCENE()
	INT index
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
	
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(index)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF NOT clubPlayerBD[index].bStartedHiestScene
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC SET_STRIPPER_CAN_MIGRATE(INT iStripper, BOOL bCanMigrate)
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.namedStrippers[iStripper])		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.namedStrippers[iStripper])
			SET_NETWORK_ID_CAN_MIGRATE(serverBD.namedStrippers[iStripper], bCanMigrate)
		ENDIF
	ENDIF
ENDPROC
