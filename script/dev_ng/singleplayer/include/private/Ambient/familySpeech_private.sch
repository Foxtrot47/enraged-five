USING "dialogue_public.sch"		//"commands_speech.sch"

///private header for family task control scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FAMILY NAME VARIABLES
// *******************************************************************************************
ENUM enumFamilySpeechCheck
	FSC_checkDistance2d			= BIT0,
	FSC_checkLocateBounds		= BIT1,
	FSC_checkOnSameFloor		= BIT2,
	FSC_checkOnScreen			= BIT3,
	FSC_checkTenSecDelay		= BIT4,
	FSC_checkListenerAlive		= BIT5,
	FSC_singleRandomLine		= BIT6,
	
	#IF USE_TU_CHANGES
	FSC_setVoiceLocationForPed	= BIT7,
	#ENDIF
	
	FSC_invalid = ALLBITS
ENDENUM

#IF IS_DEBUG_BUILD
BOOL bEdit_speech_bounds = FALSE
#ENDIF

// *******************************************************************************************
//	FAMILY NAME FUNCTIONS
// *******************************************************************************************
FUNC STRING Get_VoiceID_From_FamilyMember(enumFamilyMember eFamilyMember)
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON			RETURN "JIMMY" BREAK
		CASE FM_MICHAEL_DAUGHTER	RETURN "TRACEY" BREAK
		CASE FM_MICHAEL_WIFE		RETURN "AMANDA" BREAK
		CASE FM_MICHAEL_MEXMAID		RETURN "MAID" BREAK
		CASE FM_MICHAEL_GARDENER	RETURN "GARDENER" BREAK
		
		CASE FM_FRANKLIN_AUNT		RETURN "DENISE" BREAK
		CASE FM_FRANKLIN_LAMAR		RETURN "LAMAR" BREAK
		CASE FM_FRANKLIN_STRETCH	RETURN "STRETCH" BREAK
		
		CASE FM_TREVOR_0_RON		RETURN "NERVOUSRON" BREAK
		CASE FM_TREVOR_0_MICHAEL	RETURN "MICHAEL" BREAK
		CASE FM_TREVOR_0_TREVOR		RETURN "TREVOR" BREAK
		CASE FM_TREVOR_0_WIFE		RETURN "PATRICIA" BREAK
		CASE FM_TREVOR_0_MOTHER		RETURN "TREVORMOM" BREAK
		
		CASE FM_TREVOR_1_FLOYD		RETURN "FLOYD" BREAK
		CASE FM_TREVOR_1_WADE		RETURN "WADE" BREAK
		
	ENDSWITCH
	
	SCRIPT_ASSERT("invalid eFamilyMember Get_VoiceID_From_FamilyMember()")
	RETURN "NULL"
ENDFUNC

FUNC STRING Get_SpeechLabel_From_FamilyEvent(enumFamilyEvents eFamilyEvent, INT &iRandCount, INT &iSpeechBit, INT &iPlayerBitset)
	
	// set default speech bits
	SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkDistance2d | FSC_checkOnSameFloor | FSC_singleRandomLine | FSC_checkTenSecDelay)
	
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M_FAMILY_MIC4_locked_in_room		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "MIC4_IG_1"	BREAK
		CASE FE_M7_FAMILY_finished_breakfast
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 4	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_9_0"	BREAK
		CASE FE_M7_FAMILY_finished_pizza
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 4	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_9_1"	BREAK
		CASE FE_M7_FAMILY_watching_TV
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5
			iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_9_2"	BREAK
		
		CASE FE_M2_SON_watching_TV					iRandCount = 0	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Hey Dad, fill up my car when I get back, will you?"
		CASE FE_M_SON_sleeping						iRandCount = 12	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_0"	BREAK	//"Zzzzzz..."
		CASE FE_M2_SON_gaming_loop					SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "JIM_IG_2a"	BREAK
//		CASE FE_M2_SON_gaming_exit					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
//		CASE FE_M7_SON_gaming_exit					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M_SON_rapping_in_the_shower			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 22	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_2"	BREAK	//"Hear the neighbors talk, but you know that they be losin'"
		CASE FE_M_SON_Borrows_sisters_car			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"God Dad, since you wont buy me a car I'm taking Tracys"
		CASE FE_M_SON_watching_porn					SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_checkTenSecDelay) iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"mmm... oh yeah... where did I put my tube sock?"
		CASE FE_M_SON_in_room_asks_for_munchies		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "JIM_IG_3"	BREAK
		CASE FE_M_SON_phone_calls_in_room			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 4	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_6"	BREAK	//"Yes, I know you fucked my sister dude. Hasn't everyone?"
		CASE FE_M_SON_on_ecstasy_AND_friendly		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 3 iPlayerBitset = BIT_MICHAEL	RETURN "JIM_IG_5"	BREAK
		CASE FE_M_SON_Fighting_with_sister_A		SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive | FSC_checkLocateBounds)	iRandCount = 6	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_8" BREAK
		CASE FE_M_SON_Fighting_with_sister_B		SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive | FSC_checkLocateBounds)	iRandCount = 6	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_8" BREAK
		CASE FE_M_SON_Fighting_with_sister_C		SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive | FSC_checkLocateBounds)	iRandCount = 6	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_8" BREAK
		CASE FE_M_SON_Fighting_with_sister_D		SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive | FSC_checkLocateBounds)	iRandCount = 6	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_0_8" BREAK
		CASE FE_M_SON_smoking_weed_in_a_bong		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "JIM_IG_1"	BREAK
		CASE FE_M_SON_raids_fridge_for_food			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "JIM_IG_4"	BREAK
		CASE FE_M7_SON_jumping_jacks				iRandCount = 9	iPlayerBitset = BIT_MICHAEL	RETURN "FAMR_IG_2a"	BREAK
		CASE FE_M7_SON_gaming						iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M7_SON_going_for_a_bike_ride		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_0_5"	BREAK
		CASE FE_M7_SON_watching_TV_with_tracey
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
			iRandCount = 5
			iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_0_6"
			BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing				SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_3"	BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3
			IF NOT g_bMagDemoActive
				SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)
				CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
				iRandCount = 3
				iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_5"
			ELSE
				SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)
				CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
				iRandCount = 5
				iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_MD"
			ENDIF
		BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Hey Dad, fill up my car when I get back, will you?"
		CASE FE_M_DAUGHTER_walks_to_room_music		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_1_3"	BREAK	//"...Wake up in the mornin feelin like P diddy ..."
		CASE FE_M_DAUGHTER_dancing_practice
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_checkTenSecDelay)
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkOnScreen)
			
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "" BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom	SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_7"	BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends
//			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_checkTenSecDelay)
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkOnScreen)
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)
			
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL
			RETURN "FMM_1_6"	BREAK	//"So I said, like I told you, I said..."
		CASE FE_M_DAUGHTER_on_phone_LOCKED			RETURN Get_SpeechLabel_From_FamilyEvent(FE_M_DAUGHTER_on_phone_to_friends, iRandCount, iSpeechBit, iPlayerBitset)	BREAK	//iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_2_2"	BREAK	//"Game, set and match."
		CASE FE_M_DAUGHTER_shower					SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_1_8"	BREAK	//"...7am, waking up in the morning, Gotta be fresh, gotta go downstairs..."
		CASE FE_M_DAUGHTER_watching_TV_sober		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_1_9"	BREAK	//""God, my life is JUST like this show."
		CASE FE_M_DAUGHTER_watching_TV_drunk		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_15"	BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_1_11"	BREAK	//"*snort* uh, I think he sold me the cheap crap."
		CASE FE_M_DAUGHTER_sex_sounds_from_room		iRandCount = 15	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Don't stop, don't stop... why have you stopped?"
		CASE FE_M_DAUGHTER_crying_over_a_guy		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_1"	BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRA_IG_6"	BREAK
		CASE FE_M_DAUGHTER_sleeping					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Zzzzzz..."
		CASE FE_M_DAUGHTER_couchsleep				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Zzzzzz..."
		CASE FE_M7_DAUGHTER_studying_on_phone		iRandCount = 15	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_1_0"	BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_1_1"	BREAK
		CASE FE_M7_DAUGHTER_sunbathing				SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M2_WIFE_in_face_mask				CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_10"	BREAK
		CASE FE_M7_WIFE_in_face_mask				CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_10"	BREAK
		CASE FE_M_WIFE_playing_tennis				RETURN Get_SpeechLabel_From_FamilyEvent(FAMILY_MEMBER_BUSY, iRandCount, iSpeechBit, iPlayerBitset)	BREAK	//iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_2_2"	BREAK	//"Game, set and match."
		CASE FE_M2_WIFE_doing_yoga					CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_4"	BREAK
		CASE FE_M7_WIFE_doing_yoga					CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_4"	BREAK
//		CASE FE_M_WIFE_getting_nails_done			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_2_4"	BREAK	//"Oh, I love that colour."
//		CASE FE_M_WIFE_leaving_in_car_v2			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
//		CASE FE_M_WIFE_MD_leaving_in_car_v3			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M_WIFE_leaving_in_car				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter	CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M7_WIFE_with_shopping_bags_enter	CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_idle		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_7"	BREAK
//		CASE FE_M7_WIFE_with_shopping_bags_idle		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_7"	BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_exit		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_7"	BREAK
//		CASE FE_M7_WIFE_with_shopping_bags_exit		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_7"	BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_8"	BREAK
		CASE FE_M2_WIFE_sunbathing					CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_5"	BREAK
		CASE FE_M7_WIFE_sunbathing					CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_5"	BREAK
//		CASE FE_M_WIFE_getting_botox_done			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_2_8"	BREAK	//"Don't you dare make me smile Michael."
		CASE FE_M2_WIFE_passed_out_SOFA				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_1"	BREAK
		CASE FE_M7_WIFE_passed_out_SOFA				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_1"	BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M_WIFE_screaming_at_son_P2			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M_WIFE_screaming_at_son_P3			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		
		CASE FE_M_WIFE_screaming_at_daughter
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""				BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_11"		BREAK
		CASE FE_M7_WIFE_phones_man_OR_therapist
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_11"		BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""				BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M_WIFE_using_vibrator_END			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M7_WIFE_using_vibrator				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AMr_IG_6"	BREAK
		#ENDIF
		CASE FE_M_WIFE_passed_out_BED				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "AM_IG_1"	BREAK	//"Wha, how did I end up in bed?"
		CASE FE_M2_WIFE_sleeping					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Zzzzzz..."
		CASE FE_M7_WIFE_sleeping					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK	//"Zzzzzz..."
		CASE FE_M7_WIFE_Making_juice				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M7_WIFE_shopping_with_daughter		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
//		CASE FE_M7_WIFE_shopping_with_son			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
//		CASE FE_M7_WIFE_on_phone					iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_2_3"	BREAK
		
		CASE FE_M2_MEXMAID_cleans_booze_pot_other	iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN "MA_IG_1"	BREAK
		CASE FE_M2_MEXMAID_clean_surface_a			RETURN Get_SpeechLabel_From_FamilyEvent(FE_M2_MEXMAID_cleans_booze_pot_other, iRandCount, iSpeechBit, iPlayerBitset) BREAK
		CASE FE_M2_MEXMAID_clean_surface_b			RETURN Get_SpeechLabel_From_FamilyEvent(FE_M2_MEXMAID_cleans_booze_pot_other, iRandCount, iSpeechBit, iPlayerBitset) BREAK
		CASE FE_M2_MEXMAID_clean_surface_c			RETURN Get_SpeechLabel_From_FamilyEvent(FE_M2_MEXMAID_cleans_booze_pot_other, iRandCount, iSpeechBit, iPlayerBitset) BREAK
		CASE FE_M2_MEXMAID_clean_window				iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_M_MEXMAID_MIC4_clean_window
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL
			RETURN "MA_IG_1b" BREAK
		CASE FE_M_MEXMAID_does_the_dishes			iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN "MA_IG_2"	BREAK
		CASE FE_M_MEXMAID_stealing_stuff			iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		CASE FE_M_MEXMAID_stealing_stuff_caught		iRandCount = 3	iPlayerBitset = BIT_MICHAEL	RETURN ""	BREAK
		
		CASE FE_M7_MEXMAID_cleans_booze_pot_other	iRandCount = 6	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_3_0"	BREAK
		CASE FE_M7_MEXMAID_clean_surface			iRandCount = 6	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_surface"	BREAK
		CASE FE_M7_MEXMAID_clean_window				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM7_window"	BREAK
		
		CASE FE_M_GARDENER_with_leaf_blower			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_4_0"	BREAK	//"Don't mind me Mr Michael, just getting rid of some of these leaves."
		CASE FE_M_GARDENER_planting_flowers			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
//		CASE FE_M_GARDENER_trimming_hedges			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_4_1"	BREAK	//"Don't mind me Mr Michael, just giving these hedges a trim."
		CASE FE_M_GARDENER_cleaning_pool			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "GAR_IG_4"	BREAK
		CASE FE_M_GARDENER_mowing_lawn				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_4_3"	BREAK	//"Don't mind me Mr Michael, just mowing the lawn."
		CASE FE_M_GARDENER_watering_flowers			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "GAR_IG_5"	BREAK
//		CASE FE_M_GARDENER_spraying_for_weeds		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "FMM_4_8"	BREAK
		CASE FE_M_GARDENER_on_phone					iRandCount = 4	iPlayerBitset = BIT_MICHAEL	RETURN "GAR_IG_6"	BREAK
		CASE FE_M_GARDENER_smoking_weed				SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "GAR_IG_7"	BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises		iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "DEN_IG_1"	BREAK
		CASE FE_F_AUNT_in_face_mask					iRandCount = 3	iPlayerBitset = BIT_FRANKLIN	RETURN "DEN_IG_2"	BREAK
		CASE FE_F_AUNT_watching_TV					iRandCount = 3	iPlayerBitset = BIT_FRANKLIN	RETURN "DEN_IG_3"	BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x	iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "DEN_IG_4"	BREAK
		
		CASE FE_F_AUNT_returned_to_aunts			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkLocateBounds #IF USE_TU_CHANGES | FSC_setVoiceLocationForPed #ENDIF )	iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "DEN_RETURN"	BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
//			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
//			iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "LAS_IG_1"
//			BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
//			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
//			iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "LAS_IG_2"
//			BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing
//			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
//			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
//			iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "LAS_IG_3"
//			BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
//			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
//			iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "LAS_IG_4"
//			BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering		iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN ""			BREAK
		
//		CASE FE_F_DOG_doing_something				iRandCount = 5	iPlayerBitset = BIT_FRANKLIN	RETURN "FMF_2_0"	BREAK	//""
		
		CASE FE_T0_RON_monitoring_police_frequency	iRandCount = 3	iPlayerBitset = BIT_TREVOR	RETURN "RON_IG_1"	BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast	iRandCount = 3	iPlayerBitset = BIT_TREVOR	RETURN "RON_IG_2"	BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING		CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	iRandCount = 3	iPlayerBitset = BIT_TREVOR	RETURN "RON_IG_3"	BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING		RETURN Get_SpeechLabel_From_FamilyEvent(FE_T0_RON_ranting_about_government_LAYING, iRandCount, iSpeechBit, iPlayerBitset)	BREAK
		CASE FE_T0_RON_smoking_crystal				iRandCount = 3	iPlayerBitset = BIT_TREVOR	RETURN "RON_IG_4"	BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar	iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "RON_IG_5"	BREAK
		CASE FE_T0_RON_stares_through_binoculars	iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "RON_IG_6"	BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands	iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "MIC_IG_1"	BREAK
		CASE FE_T0_MICHAEL_sunbathing				iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "MIC_IG_2"	BREAK
		CASE FE_T0_MICHAEL_drinking_beer			iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "MIC_IG_3"	BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist	iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN ""			BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN ""	BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk	CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRV_IG_1"	BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare	CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)	SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRV_IG_2"	BREAK
		CASE FE_T0_TREVOR_smoking_crystal			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRV_IG_3"	BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRV_IG_4"	BREAK
		#ENDIF 
		//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_checkTenSecDelay)
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""
		BREAK
		CASE FE_T0_TREVOR_blowing_shit_up			iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRV_IG_6"	BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk	iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "TRV_IG_7"	BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely		iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN "RONEX_IG_1"	BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals	iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL	RETURN ""	BREAK	//"Sssh, I though I heard something from the greys"
		CASE FE_T0_RONEX_working_a_moonshine_sill	iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL | BIT_FRANKLIN	RETURN ""	BREAK
		CASE FE_T0_RONEX_doing_target_practice		iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL	RETURN "RONEX_IG_4"	BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning			iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL	RETURN "PAT_IG_1"	BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work	iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL	RETURN "PAT_IG_2"	BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_checkListenerAlive)
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL	RETURN "PAT_IG_3"	BREAK
//		CASE FE_T0_KIDNAPPED_WIFE_cooking_a_meal	iRandCount = 5	iPlayerBitset = BIT_TREVOR | BIT_MICHAEL	RETURN "FMT_4_3"	BREAK	//"A little more oregano, some salt. Mmm, thats good"
		
		CASE FE_T0_MOTHER_duringRandomChar
			iRandCount = 5	iPlayerBitset = BIT_TREVOR
			CLEAR_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
			RETURN ""	BREAK
//		CASE FE_T0_MOTHER_something_b				iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN ""	BREAK
//		CASE FE_T0_MOTHER_something_c				iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN ""	BREAK
		
		CASE FE_T1_FLOYD_cleaning					iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_1"	BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position	iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_2"	BREAK
		CASE FE_T1_FLOYD_cries_on_sofa				iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_2"	BREAK
		CASE FE_T1_FLOYD_pineapple					iRandCount = 0	iPlayerBitset = BIT_TREVOR	RETURN ""	BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend		iRandCount = 12	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_3a"	BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders		iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN ""			BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a		iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN ""		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_b		RETURN Get_SpeechLabel_From_FamilyEvent(FE_T1_FLOYD_hiding_from_Trevor_a, iRandCount, iSpeechBit, iPlayerBitset) BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_c		RETURN Get_SpeechLabel_From_FamilyEvent(FE_T1_FLOYD_hiding_from_Trevor_a, iRandCount, iSpeechBit, iPlayerBitset) BREAK
		CASE FE_T1_FLOYD_is_sleeping				iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_5"	BREAK
		
//		CASE FE_T1_FLOYD_with_wade_post_trevor3		iRandCount = 5	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_6"	BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1		iRandCount = 9	iPlayerBitset = BIT_TREVOR	RETURN "FL_IG_7"	BREAK
		
		CASE FE_ANY_find_family_event				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		CASE FE_ANY_wander_family_event				iRandCount = 5	iPlayerBitset = BIT_MICHAEL	RETURN ""			BREAK
		
		CASE NO_FAMILY_EVENTS						iRandCount = 5	iPlayerBitset = BIT_NOBODY	RETURN ""			BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in Get_SpeechLabel_From_FamilyEvent()")
	
	SCRIPT_ASSERT("invalid eFamilyEvent Get_SpeechLabel_From_FamilyEvent()")
	#ENDIF
	
	iRandCount = -1	iPlayerBitset = 0	RETURN "NULL"
ENDFUNC

// *******************************************************************************************
//	FAMILY SPEECH FUNCTIONS
// *******************************************************************************************
FUNC BOOL FAMILY_Is_Any_Dialogue_Playing(PED_INDEX pedID)
	IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
		RETURN(TRUE)
	ENDIF
	IF IS_ANY_SPEECH_PLAYING(pedID)
		RETURN(TRUE)
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN(TRUE)
	ENDIF
	
	IF IS_CALLING_ANY_CONTACT()
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL Is_Family_Speech_Loaded_And_Setup(STRING strTextBlock, TEXT_BLOCK_SLOTS SlotNumber = AMBIENT_DIALOGUE_TEXT_SLOT)	//AMBIENT_DIALOGUE_TEXT_SLOT
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED(strTextBlock, SlotNumber)
		RETURN(TRUE)
	ELSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_MOBILE_PHONE_CALL_ONGOING()
			IF NOT IS_STREAMING_ADDITIONAL_TEXT(SlotNumber)
				REQUEST_ADDITIONAL_TEXT(strTextBlock, SlotNumber) 
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC



FUNC BOOL GetRandomConversationFromLabel(STRING strTextBlock, TEXT_LABEL &strLabel, TEXT_LABEL &tCreatedConvLabels[])
	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		RETURN FALSE
	ENDIF
	
	IF Is_Family_Speech_Loaded_And_Setup(strTextBlock)
		
		TEXT_LABEL tLabel = strLabel
		TEXT_LABEL tZero = ""
		INT iNumberOfRandomConvs = -1
		
		//is the conv randomline or singleconv?
		tLabel = strLabel
		tLabel += "a"
		tLabel += "_"
		tLabel += tZero
		tLabel += "1"
		IF DOES_TEXT_LABEL_EXIST(tLabel)
			tZero = ""
		ELSE
			tZero = "0"
			
			tLabel = strLabel
			tLabel += "a"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				
			ELSE
				CPRINTLN(DEBUG_FAMILY, "invalid GetRandomConversationFromLabel(", strLabel, ", ", tLabel, ") - block \"", strTextBlock, "\"")
				RETURN(FALSE)
			ENDIF
		ENDIF
		
		TEXT_LABEL tLabels[15]
		BOOL bReachedEnd = TRUE
		// does first variation exist?
		tLabel = strLabel
		tLabel += "a"
		tLabel += "_"
		tLabel += tZero
		tLabel += "1"
		IF DOES_TEXT_LABEL_EXIST(tLabel)
			iNumberOfRandomConvs++
			tLabels[iNumberOfRandomConvs]  = strLabel
			tLabels[iNumberOfRandomConvs] += "a"
			bReachedEnd = FALSE
		ENDIF
		
		// does second variation exist?
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "b"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "b"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
		
		// does third variation exist?
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "c"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "c"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does forth variation exist?
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "d"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "d"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does fifth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "e"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "e"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does sixth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "f"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "f"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does seventh variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "g"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "g"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does eigth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "h"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "h"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does nineth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "i"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "i"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does tenth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "j"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "j"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does eleventh variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "k"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "k"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does twelvth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "JJJ"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "JJJ"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does thirteenth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "m"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "m"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does fourteenth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "n"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "n"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
			
		// does fifteenth variation exist???
		IF NOT bReachedEnd
			bReachedEnd = TRUE
			
			tLabel = strLabel
			tLabel += "o"
			tLabel += "_"
			tLabel += tZero
			tLabel += "1"
			IF DOES_TEXT_LABEL_EXIST(tLabel)
				iNumberOfRandomConvs++
				tLabels[iNumberOfRandomConvs]  = strLabel
				tLabels[iNumberOfRandomConvs] += "o"
				bReachedEnd = FALSE
			ENDIF
		ENDIF
		
		// // // // //
		INT iMAX_FamilyConvs = iNumberOfRandomConvs
		
		INT iConvs
		REPEAT COUNT_OF(tCreatedConvLabels) iConvs
			
			#IF IS_DEBUG_BUILD
			IF g_bDrawDebugFamilyStuff
			CPRINTLN(DEBUG_FAMILY, "tCreatedConvLabels[", iConvs, "]: \"", tCreatedConvLabels[iConvs], "\"")
			ENDIF
			#ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tCreatedConvLabels[iConvs])
				INT iLabels
				REPEAT COUNT_OF(tLabels) iLabels
					IF NOT IS_STRING_NULL_OR_EMPTY(tLabels[iLabels])
						IF ARE_STRINGS_EQUAL(tCreatedConvLabels[iConvs], tLabels[iLabels])
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_FAMILY, "	tLabels[", iLabels, "]: \"", tLabels[iLabels], "\" matches \"", tCreatedConvLabels[iConvs], "\"")
							#ENDIF
							
							INT iNewConvs
							FOR iNewConvs = iLabels TO COUNT_OF(tLabels)-1
							
								#IF IS_DEBUG_BUILD
								IF g_bDrawDebugFamilyStuff
								CPRINTLN(DEBUG_FAMILY, "		tLabels[", iNewConvs, "] = tLabels[", iNewConvs, "+1]	")
								ENDIF
								#ENDIF
								
								IF ((iNewConvs+1) < COUNT_OF(tLabels))
									tLabels[iNewConvs] = tLabels[iNewConvs+1]
								ELSE
									tLabels[iNewConvs] = ""
								ENDIF
							ENDFOR
							
							iMAX_FamilyConvs--
						ELSE
							
							#IF IS_DEBUG_BUILD
							IF g_bDrawDebugFamilyStuff
							CDEBUG3LN(DEBUG_FAMILY, "	tLabels[", iLabels, "]: \"", tLabels[iLabels], "\" doesn't match \"", tCreatedConvLabels[iConvs], "\"")
							ENDIF
							#ENDIF
							
						ENDIF
					ELSE
						
						#IF IS_DEBUG_BUILD
						IF g_bDrawDebugFamilyStuff
						CDEBUG3LN(DEBUG_FAMILY, "	tLabels[", iLabels, "]: \"", tLabels[iLabels], "\"")
						ENDIF
						#ENDIF
						
					ENDIF
					
				ENDREPEAT
			ENDIF
			
		ENDREPEAT
		
		INT iConvName = GET_RANDOM_INT_IN_RANGE(0, iMAX_FamilyConvs+1)
		strLabel = tLabels[iConvName]
		
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugFamilyStuff
		OR IS_STRING_NULL_OR_EMPTY(strLabel)
			CPRINTLN(DEBUG_FAMILY, "strLabel = tLabels[", iConvName, "] \"", strLabel, "\"")
			
			INT iLabels
			REPEAT COUNT_OF(tLabels) iLabels
				CPRINTLN(DEBUG_FAMILY, "	tLabels[", iLabels, "]: \"", tLabels[iLabels], "\"")
			ENDREPEAT
		ENDIF
		#ENDIF
		// // // // //
		
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugFamilyStuff
		OR IS_STRING_NULL_OR_EMPTY(strLabel)
			CPRINTLN(DEBUG_FAMILY, "GetRandomConversationFromLabel(", strLabel, ") [", 0, ", ", iNumberOfRandomConvs+1, ": ", iConvName, "]")
		ENDIF
		#ENDIF
		
		IF IS_STRING_NULL_OR_EMPTY(strLabel)
			CPRINTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": GetRandomConversationFromLabel(", strLabel, ") [", 0, ", ", iNumberOfRandomConvs+1, ": ", iConvName, "] is null???")
			
			RETURN(FALSE)
		ENDIF
		
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL Is_Ped_Playing_Family_Speech(STRING strTextBlock, STRING strLabel,
		structPedsForConversation &inSpeechStruct,
		enumConversationPriority enSpeechPriority, TEXT_LABEL &tCreatedConvLabels[],
		STRING stringFacialAnimClip = NULL)
	
	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Is_Ped_Playing_Family_Speech - attempting to create a null conversation??")
		RETURN(FALSE)
	ENDIF
	
	CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Is_Ped_Playing_Family_Speech.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE(), ")")
	
	IF Is_Family_Speech_Loaded_And_Setup(strTextBlock)
		
		STRING sVoiceID = ""
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL	sVoiceID = "MICHAEL" BREAK
			CASE CHAR_FRANKLIN	sVoiceID = "FRANKLIN" BREAK
			CASE CHAR_TREVOR	sVoiceID = "TREVOR" BREAK
		ENDSWITCH
		
		IF IS_STRING_NULL_OR_EMPTY(stringFacialAnimClip)
			
			ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), sVoiceID)
			
			CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Is_Ped_Playing_Family_Speech.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE(), "/", GET_ALLOCATED_STACK_SIZE(), ") before creating animless conversation \"", strTextBlock, "\", \"", strLabel, "\"")
			
			IF CREATE_CONVERSATION(inSpeechStruct,
					strTextBlock, strLabel,
					enSpeechPriority)
				
				CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Is_Ped_Playing_Family_Speech.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE(), "/", GET_ALLOCATED_STACK_SIZE(), ") before creating animless conversation \"", strTextBlock, "\", \"", strLabel, "\"")
				
				INT iConv = COUNT_OF(tCreatedConvLabels)-1
				WHILE (iConv > 0)
					tCreatedConvLabels[iConv] = tCreatedConvLabels[iConv-1]
					iConv--
				ENDWHILE
				tCreatedConvLabels[0] = strLabel
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "CREATE_FAMILY_CONVERSATION: \"", strLabel, "\"")
				
				REPEAT COUNT_OF(tCreatedConvLabels) iConv
					CPRINTLN(DEBUG_FAMILY, "	tCreatedConvLabels[", iConv, "]: \"", tCreatedConvLabels[iConv], "\"")
				ENDREPEAT
				CPRINTLN(DEBUG_FAMILY, "")
				#ENDIF
				
				RETURN(TRUE)
			ENDIF
		ELSE
			ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), sVoiceID)
			
			CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Is_Ped_Playing_Family_Speech.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE(), "/", GET_ALLOCATED_STACK_SIZE(), ") before animated conversation \"", strTextBlock, "\", \"", strLabel, "\"")
			
			IF CREATE_CONVERSATION(inSpeechStruct,
					strTextBlock, strLabel,
					enSpeechPriority)
				
				CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Is_Ped_Playing_Family_Speech.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE(), "/", GET_ALLOCATED_STACK_SIZE(), ") before creating animated conversation \"", strTextBlock, "\", \"", strLabel, "\"")
				
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				
				INT iConv = COUNT_OF(tCreatedConvLabels)-1
				WHILE (iConv > 0)
					tCreatedConvLabels[iConv] = tCreatedConvLabels[iConv-1]
					iConv--
				ENDWHILE
				tCreatedConvLabels[0] = strLabel
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "CREATE_FAMILY_FACIAL_CONVERSATION: \"", strLabel, "\"")
				
				REPEAT COUNT_OF(tCreatedConvLabels) iConv
					CPRINTLN(DEBUG_FAMILY, "	tCreatedConvLabels[", iConv, "]: \"", tCreatedConvLabels[iConv], "\"")
				ENDREPEAT
				CPRINTLN(DEBUG_FAMILY, "")
				#ENDIF
				
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IsSpeechListenerAlive(structPedsForConversation &inSpeechStruct, STRING strTextBlock, STRING strLabel)
	
	IF IS_STRING_NULL_OR_EMPTY(strLabel)
		CASSERTLN(DEBUG_FAMILY, "IsSpeechListenerAlive - Speaker Listener string has null value.")
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL strLabel_SL = strLabel
	strLabel_SL += "SL"

	IF Is_Family_Speech_Loaded_And_Setup(strTextBlock)
		IF DOES_TEXT_LABEL_EXIST (strLabel_SL)
			STRING strLabel_AUD = GET_FILENAME_FOR_AUDIO_CONVERSATION (strLabel_SL)
			
			IF IS_STRING_NULL (strLabel_AUD) 
				CPRINTLN(DEBUG_FAMILY, "Speaker Listener string has null value.")
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_FAMILY, "IsSpeechListenerAlive - SL Label for dialogue ", strLabel_SL, " is \"", strLabel_AUD, "\"")
				
				CONST_INT iSLmanipulator		0
		
				INT startPointSpeaker = (iSLmanipulator * 3), endPointSpeaker = ((iSLmanipulator * 3)+ 1)
				TEXT_LABEL singleSpeakerCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (strLabel_AUD, startPointSpeaker, endPointSpeaker)
				CPRINTLN(DEBUG_FAMILY, " speaker:'", singleSpeakerCharacter, "' [", startPointSpeaker, ", ", endPointSpeaker)
				
				INT startPointListener = ((iSLmanipulator * 3) + 1), endPointListener = ((iSLmanipulator * 3)+ 2)
				TEXT_LABEL singleListenerCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (strLabel_AUD, startPointListener, endPointListener)
				CPRINTLN(DEBUG_FAMILY, "], listener:'", singleListenerCharacter, "' [", startPointListener, ", ", endPointListener, "]")
				
				INT iSpeaker
				IF STRING_TO_INT(singleSpeakerCharacter, iSpeaker)

					IF DOES_ENTITY_EXIST(inSpeechStruct.PedInfo[iSpeaker].Index)
						CPRINTLN(DEBUG_FAMILY, "iSpeaker [", inSpeechStruct.PedInfo[iSpeaker].VoiceID, "] exists	//", iSpeaker, " '", singleListenerCharacter, "'")
					ELSE
						CPRINTLN(DEBUG_FAMILY, "iSpeaker [", inSpeechStruct.PedInfo[iSpeaker].VoiceID, "] DOESNT EXIST	//", iSpeaker)
						
						RETURN FALSE
					ENDIF
				ELSE
					CPRINTLN(DEBUG_FAMILY, "invalid speaker string-to-int [", singleSpeakerCharacter, "] //", iSpeaker)
				ENDIF
				
				INT iListener
				IF STRING_TO_INT(singleListenerCharacter, iListener)
					
					IF DOES_ENTITY_EXIST(inSpeechStruct.PedInfo[iListener].Index)
						CPRINTLN(DEBUG_FAMILY, "iListener [", inSpeechStruct.PedInfo[iListener].VoiceID, "] exists	//", iListener, " '", singleListenerCharacter, "'")
					ELSE
						CPRINTLN(DEBUG_FAMILY, "iListener [", inSpeechStruct.PedInfo[iListener].VoiceID, "] DOESNT EXIST	//", iListener)
						
						RETURN FALSE
					ENDIF
				ELSE
					CPRINTLN(DEBUG_FAMILY, "invalid listener string-to-int [", singleListenerCharacter, "] //", iListener)
				ENDIF
				
			ENDIF
		ELSE
			CPRINTLN(DEBUG_FAMILY, "IsSpeechListenerAlive - Speaker Listener label for conversation \"", strLabel_SL, "\" does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")
			
			RETURN FALSE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_FAMILY, "IsSpeechListenerAlive - Is_Family_Speech_Loaded_And_Setup()")
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_IsPlayerAtFamilyEventSpeechBounds(PED_INDEX memberID, enumFamilyEvents eFamilyEvent)
	
	VECTOR VecOffset = <<0,0,0>>, VecBounds = <<5,5,2>>
	
	SWITCH eFamilyEvent
		CASE FE_M2_SON_gaming_loop					VecOffset = << 0.0, 2.0, 0.0>>	VecBounds = <<5.0, 7.0, 2.0>> BREAK
		CASE FE_M_SON_raids_fridge_for_food			VecOffset = << 0.0, 0.0, 0.0>>	VecBounds = <<5.0, 5.0, 2.0>> BREAK
		CASE FE_M_SON_watching_porn					VecOffset = << 0.0, 3.9, 0.0>>	VecBounds = <<5.0, 5.0, 2.0>> BREAK
		CASE FE_M_SON_rapping_in_the_shower			VecOffset = << 1.0,-2.0, 0.0>>	VecBounds = <<5.0, 5.0, 2.0>> BREAK
		CASE FE_M_SON_Fighting_with_sister_A		VecOffset = << 0.0, 5.0, 0.0>>	VecBounds = <<5.0, 7.5, 2.0>> BREAK
		CASE FE_M_SON_Fighting_with_sister_B		VecOffset = << 0.0, 5.0, 0.0>>	VecBounds = <<5.0, 7.5, 2.0>> BREAK
		CASE FE_M_SON_Fighting_with_sister_C		VecOffset = << 0.0, 5.0, 0.0>>	VecBounds = <<5.0, 7.5, 2.0>> BREAK
		CASE FE_M_SON_Fighting_with_sister_D		VecOffset = << 0.0, 5.0, 0.0>>	VecBounds = <<5.0, 7.5, 2.0>> BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing				VecOffset = <<-1.0,-0.5, 0.0>>	VecBounds = <<6.5, 3.5, 2.0>> BREAK
		CASE FE_M7_DAUGHTER_sunbathing				PRIVATE_IsPlayerAtFamilyEventSpeechBounds(memberID, FE_M2_DAUGHTER_sunbathing) BREAK
		CASE FE_M_DAUGHTER_shower					VecOffset = << 1.0,-2.0, 0.0>>	VecBounds = <<5.0, 5.0, 2.0>> BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3			VecOffset = <<-1.5, 0.5, 0.0>>	VecBounds = <<5.0, 6.5, 2.0>> BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends		VecOffset = <<-2.1, 4.1, 0.0>>	VecBounds = <<5.5, 5.5, 2.0>> BREAK
		CASE FE_M_DAUGHTER_on_phone_LOCKED			PRIVATE_IsPlayerAtFamilyEventSpeechBounds(memberID, FE_M_DAUGHTER_on_phone_to_friends) BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom	VecOffset = << 0.0, 5.0, 0.0>>	VecBounds = <<5.0, 7.5, 2.0>> BREAK
		
		CASE FE_M_GARDENER_smoking_weed				VecOffset = << 0.0, 0.0, 0.0>>	VecBounds = <<2.0, 2.0, 2.0>> BREAK
		
		CASE FE_F_AUNT_returned_to_aunts 			VecOffset = << 0.0, 0.0, 0.0>>	VecBounds = <<10.0, 10.0, 2.0>> BREAK
		
		DEFAULT
			VecOffset = <<0,0,0>>
			VecBounds = <<5,5,2>>
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in IsPlayerAtFamilyEventSpeechBounds()")
			
			SCRIPT_ASSERT("invalid eFamilyEvent IsPlayerAtFamilyEventSpeechBounds()")
			#ENDIF
			
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bEdit_speech_bounds
		
		TEXT_LABEL_63 str = "FamilyEventSpeechBounds:"
		str += Get_String_From_FamilyEvent(eFamilyEvent)
		
		VECTOR vMemberCoord = GET_ENTITY_COORDS(memberID)
		
		WIDGET_GROUP_ID family_scene_widget = START_WIDGET_GROUP(str)
			ADD_WIDGET_BOOL("bEdit_speech_bounds", bEdit_speech_bounds)
			ADD_WIDGET_VECTOR_SLIDER("VecOffset", VecOffset, -20.0, 20.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("VecBounds", VecBounds, 0.0, 20.0, 0.5)
		STOP_WIDGET_GROUP()
		
		WHILE bEdit_speech_bounds
			
			IS_ENTITY_AT_COORD(PLAYER_PED_ID(),
					vMemberCoord+VecOffset,
					VecBounds)
			
			WAIT(0)
		ENDWHILE
		
		DELETE_WIDGET_GROUP(family_scene_widget)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("IsPlayerAtFamilyEventSpeechBounds()")SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	VecOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(VecOffset)SAVE_STRING_TO_DEBUG_FILE("	VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		RETURN FALSE
	ENDIF
	#ENDIF
	
	VECTOR vMemberCoord = GET_ENTITY_COORDS(memberID)
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMemberCoord+VecOffset, VecBounds)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Is_Dialogue_Anim_Drinking(PED_INDEX memberID,
		TEXT_LABEL_63 tDialogueAnimDict, TEXT_LABEL_63 tDialogueAnimClip, INT iScene,
		structTimer &speechTimer)
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimClip)
		
		IF IS_PED_INJURED(memberID)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(memberID, tDialogueAnimDict, tDialogueAnimClip)
			RETURN FALSE
		ENDIF
		
		FLOAT ReturnStartPhase, ReturnEndPhase
		IF FIND_ANIM_EVENT_PHASE(tDialogueAnimDict, tDialogueAnimClip,
				"drinking",
				ReturnStartPhase, ReturnEndPhase)
				
//			CPRINTLN(DEBUG_FAMILY, "Is_Dialogue_Anim_Drinking() - FIND_ANIM_EVENT_PHASE(\"", tDialogueAnimDict, "\", \"", tDialogueAnimClip, "\")")
			
			IF ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@")
				
				ReturnStartPhase = 0.05
				ReturnEndPhase = 0.75
				
//				#IF IS_DEBUG_BUILD
//				TEXT_LABEL_63 str
//				str  = ("\"")
//				str += tDialogueAnimDict
//				str += ("\\")
//				str += tDialogueAnimClip
//				str += ("\" need \"drinking\" tag")
//				CPRINTLN(DEBUG_FAMILY, str)
//				#ENDIF
			ENDIF
			
		ELSE
			
//			CPRINTLN(DEBUG_FAMILY, "Is_Dialogue_Anim_Drinking() - FIND_ANIM_EVENT_PHASE(\"", tDialogueAnimDict, "\", \"", tDialogueAnimClip, "\")")
			
			IF ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@")
				
				ReturnStartPhase = 0.05
				ReturnEndPhase = 0.75
				
//				#IF IS_DEBUG_BUILD
//				TEXT_LABEL_63 str
//				str  = ("\"")
//				str += tDialogueAnimDict
//				str += ("\\")
//				str += tDialogueAnimClip
//				str += ("\" need \"drinking\" tag")
//				CPRINTLN(DEBUG_FAMILY, str)
//				#ENDIF
			
			ELIF ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@RON@MOONSHINE_IG_5")		//idle_a and idle_b have tags
			OR (ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@MICHAEL@ON_SOFAIDLE_A") OR ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@MICHAEL@ON_SOFAIDLE_B") OR ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@MICHAEL@ON_SOFAIDLE_C"))
//			OR (ARE_STRINGS_EQUAL(tDialogueAnimDict, "TIMETABLE@JIMMY@IG_5@BASE") AND ARE_STRINGS_EQUAL(tDialogueAnimClip, "base"))		//#1424988
			
				
				ReturnStartPhase = 0.25
				ReturnEndPhase = 0.75
				
//				#IF IS_DEBUG_BUILD
//				TEXT_LABEL_63 str
//				str  = ("\"")
//				str += tDialogueAnimDict
//				str += ("\\")
//				str += tDialogueAnimClip
//				str += ("\" need \"drinking\" tag")
//				CPRINTLN(DEBUG_FAMILY, str)
//				#ENDIF
			ELSE
				ReturnStartPhase = -1
				ReturnEndPhase = -1
				RETURN FALSE
			ENDIF
		ENDIF
		
		FLOAT fDialogueAnimCurrentTime = -1
		IF IS_ENTITY_PLAYING_ANIM(memberID, tDialogueAnimDict, tDialogueAnimClip, ANIM_SCRIPT)
			fDialogueAnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(memberID, tDialogueAnimDict, tDialogueAnimClip)
		ELIF IS_ENTITY_PLAYING_ANIM(memberID, tDialogueAnimDict, tDialogueAnimClip, ANIM_SYNCED_SCENE)
			fDialogueAnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
		ENDIF
		
		IF (fDialogueAnimCurrentTime > ReturnStartPhase)
		AND (fDialogueAnimCurrentTime < ReturnEndPhase)
			IF IS_TIMER_STARTED(speechTimer)
				IF NOT IS_TIMER_PAUSED(speechTimer)
					PAUSE_TIMER(speechTimer)
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Play_Coming_Home_Family_Speech(INT iPlayerBitset, INT&iComingHomeFamilySpeechStage,
		INTERIOR_INSTANCE_INDEX iPlayerInterior, INTERIOR_INSTANCE_INDEX iSafehouseInterior,
		structPedsForConversation &inSpeechStruct, STRING strTextBlock, TEXT_LABEL &tCreatedConvLabels[],
		structTimer &speechTimer)
	
	//"FMM_HOME1"	//{Michael comes home - before break up}"
	//"FMM_HOME2"	//{Michael comes home - during break up}"
	//"FMM_HOME3"	//{Michael comes home - after reunion}"
	
	SWITCH iComingHomeFamilySpeechStage
		CASE 0
			
			IF IS_VALID_INTERIOR(iSafehouseInterior)
				IF IS_INTERIOR_READY(iSafehouseInterior)
					CPRINTLN(DEBUG_FAMILY, "safehouse interior ready")
					iComingHomeFamilySpeechStage = 1
					
					IF IS_VALID_INTERIOR(iPlayerInterior)
						IF IS_INTERIOR_READY(iPlayerInterior)
							CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior ready")
							
							iComingHomeFamilySpeechStage = 99
							RETURN FALSE
						ENDIF
					ENDIF
					
					VECTOR vSafehouseInterior, vPlayerCoord
					FLOAT fSafehouseInteriorDist2
					vSafehouseInterior = GET_OFFSET_FROM_INTERIOR_IN_WORLD_COORDS(iSafehouseInterior, <<0,0,0>>)
					vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					fSafehouseInteriorDist2 = VDIST2(vSafehouseInterior, vPlayerCoord)
					
					CPRINTLN(DEBUG_FAMILY, "fSafehouseInteriorDist", SQRT(fSafehouseInteriorDist2))
					
					IF (fSafehouseInteriorDist2 < (15.0*15.0))
						CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player dist too far ", SQRT(fSafehouseInteriorDist2))
						
						iComingHomeFamilySpeechStage = 99
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1		//player started outside the house - will be entering...
			
			IF NOT IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
				CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player not iPlayerBitset")
				RETURN FALSE
			ENDIF
			
			IF IS_VALID_INTERIOR(iPlayerInterior)
				IF IS_INTERIOR_READY(iPlayerInterior)
					CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior ready")
					
					iComingHomeFamilySpeechStage = 10
					
				ELSE
					CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior not ready")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior not valid")
			ENDIF
		BREAK
		
		CASE 10
			CPRINTLN(DEBUG_FAMILY, "safehouse interior not valid, player interior ready - play dialogue")
			
			IF IS_VALID_INTERIOR(iPlayerInterior)
				IF IS_INTERIOR_READY(iPlayerInterior)
					CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior ready")
					
					iComingHomeFamilySpeechStage = 10
					
				ELSE
					CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior not ready")
					SCRIPT_ASSERT("safehouse interior ready, player interior not ready")
					iComingHomeFamilySpeechStage = 0
					RETURN FALSE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_FAMILY, "safehouse interior ready, player interior not valid")
				SCRIPT_ASSERT("safehouse interior ready, player interior not valid")
				iComingHomeFamilySpeechStage = 0
				RETURN FALSE
			ENDIF
			
			TEXT_LABEL returnedStrLabel
			returnedStrLabel = ""
			SWITCH GetMichaelScheduleStage()
				CASE MSS_M2_WithFamily			returnedStrLabel = "FMM_HOME1"		BREAK
				CASE MSS_M4_WithoutFamily		returnedStrLabel = "FMM_HOME2"		BREAK
				CASE MSS_M6_Exiled				returnedStrLabel = "" iComingHomeFamilySpeechStage = 99 RETURN FALSE BREAK
				CASE MSS_M7_ReunitedWithFamily
					
					IF (g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_FAMILY_MIC4_locked_in_room)
					OR (g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER] = FE_M_FAMILY_MIC4_locked_in_room)
					OR (g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FE_M_FAMILY_MIC4_locked_in_room)
						returnedStrLabel = "" iComingHomeFamilySpeechStage = 99 RETURN FALSE
					ENDIF
					
					returnedStrLabel = "FMM_HOME3"
				BREAK
			ENDSWITCH
			
			enumConversationPriority	enSpeechPriority
			enSpeechPriority			= CONV_PRIORITY_AMBIENT_MEDIUM
			
			IF Is_Ped_Playing_Family_Speech(strTextBlock, returnedStrLabel,
					inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
				RESTART_TIMER_NOW(speechTimer)
				iComingHomeFamilySpeechStage = 99
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 99
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
/*
"FMM_2_M0a" - Michael/Amanda Pre break up chat 1
"FMM_2_M0b" - Michael/Amanda Pre break up chat 2
"FMM_2_M0c" - Michael/Amanda Pre break up chat 3
"FMM_2_M0d" - Michael/Amanda Pre break up chat 4
"FMM_2_M0e" - Michael/Amanda Pre break up chat 5
"FMM_2_M0f" - Michael/Amanda Pre break up chat 6
"FMM_2_M0g" - Michael/Amanda Pre break up chat 7
"FMM_2_M0h" - Michael/Amanda Pre break up chat 8
"FMM_2_M0i" - Michael/Amanda Pre break up chat 9
"FMM_2_M0j" - Michael/Amanda Pre break up chat 10
"FMM_2_M0k" - Michael/Amanda Pre break up chat 11
"FMM_2_M0l" - Michael/Amanda Pre break up chat 12
"FMM_2_M0m" - Michael/Amanda Pre break up chat 13
"FMM_2_M0n" - Michael/Amanda Pre break up chat 14
"FMM_2_M0o" - Michael/Amanda Pre break up chat 15

"FMM_2_M1a" - Michael/Amanda Post break up chat 1
"FMM_2_M1b" - Michael/Amanda Post break up chat 2
"FMM_2_M1c" - Michael/Amanda Post break up chat 3
"FMM_2_M1d" - Michael/Amanda Post break up chat 4
"FMM_2_M1e" - Michael/Amanda Post break up chat 5
"FMM_2_M1f" - Michael/Amanda Post break up chat 6
"FMM_2_M1g" - Michael/Amanda Post break up chat 7
"FMM_2_M1h" - Michael/Amanda Post break up chat 8
"FMM_2_M1i" - Michael/Amanda Post break up chat 9
"FMM_2_M1j" - Michael/Amanda Post break up chat 10

"FMM_2_T0a" - Trevor/Amanda greet each other 1
"FMM_2_T0b" - Trevor/Amanda greet each other 2
"FMM_2_T0c" - Trevor/Amanda greet each other 3
"FMM_2_T0d" - Trevor/Amanda greet each other 4
"FMM_2_T0e" - Trevor/Amanda greet each other 5

"FMM_2_T1a" - Trevor/Amanda insult each other 1
"FMM_2_T1b" - Trevor/Amanda insult each other 2
"FMM_2_T1c" - Trevor/Amanda insult each other 3
"FMM_2_T1d" - Trevor/Amanda insult each other 4
"FMM_2_T1e" - Trevor/Amanda insult each other 5
"FMM_2_T1f" - Trevor/Amanda insult each other 6
"FMM_2_T1g" - Trevor/Amanda insult each other 7
"FMM_2_T1h" - Trevor/Amanda insult each other 8
"FMM_2_T1i" - Trevor/Amanda insult each other 9
"FMM_2_T1j" - Trevor/Amanda insult each other 10

"FMM_2_F01" - Franklin/Amanda greet each other 1
"FMM_2_F02" - Franklin/Amanda greet each other 2
"FMM_2_F03" - Franklin/Amanda greet each other 3
"FMM_2_F04" - Franklin/Amanda greet each other 4
"FMM_2_F05" - Franklin/Amanda greet each other 5
*/

FUNC BOOL Play_Greet_Player_Family_Context(PED_INDEX memberID, enumFamilyEvents eFamilyEvent,
		structPedsForConversation &inSpeechStruct, STRING strTextBlock, TEXT_LABEL &tCreatedConvLabels[],
		structTimer &speechTimer, enumConversationPriority enSpeechPriority)
	
	VECTOR vMemberCoord = GET_ENTITY_COORDS(memberID)
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	enumFamilyMember eFamilyMember = GET_enumFamilyMember_from_ped(memberID)
	
	TEXT_LABEL strLabel = ""
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			IF g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_sleeping
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREENLIGHT, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMM_0"
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER] = FE_M_DAUGHTER_sleeping
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREENLIGHT, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMM_1"
		BREAK
		CASE FM_MICHAEL_WIFE
			IF g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FE_M2_WIFE_sleeping
			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FE_M7_WIFE_sleeping
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREENLIGHT, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMM_2"
		BREAK
		CASE FM_MICHAEL_MEXMAID
			IF ePlayer = CHAR_FRANKLIN OR ePlayer = CHAR_TREVOR
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMM_3"
		BREAK
		CASE FM_MICHAEL_GARDENER
			IF ePlayer = CHAR_FRANKLIN OR ePlayer = CHAR_TREVOR
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMM_4"
		BREAK
		
		CASE FM_FRANKLIN_AUNT
			IF ePlayer = CHAR_MICHAEL OR ePlayer = CHAR_TREVOR
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMF_0"
		BREAK
		CASE FM_FRANKLIN_LAMAR
			IF ePlayer = CHAR_MICHAEL
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMF_1"
		BREAK
		
		CASE FM_TREVOR_0_RON
			IF ePlayer = CHAR_FRANKLIN
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMT_0"
		BREAK
		CASE FM_TREVOR_0_WIFE
			IF ePlayer = CHAR_FRANKLIN
			OR ePlayer = CHAR_MICHAEL
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			strLabel = "FMT_3"
		BREAK
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
			#ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	
	SWITCH ePlayer
		CASE CHAR_MICHAEL
			IF (eFamilyMember = FM_MICHAEL_SON)
			OR (eFamilyMember = FM_MICHAEL_DAUGHTER)
			OR (eFamilyMember = FM_MICHAEL_MEXMAID)
			OR (eFamilyMember = FM_MICHAEL_GARDENER)
				strLabel += "_M0"
			ELSE
				#if USE_CLF_DLC
					strLabel += "_M1"
				#endif
				#if USE_NRM_DLC
					strLabel += "_M1"
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
				IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
					strLabel += "_M0"
				ELSE
					strLabel += "_M1"
				ENDIF
				#endif
				#endif
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
			strLabel += "_F"
		BREAK
		CASE CHAR_TREVOR
			
			IF (eFamilyMember = FM_TREVOR_0_WIFE)
				strLabel += "_T0"
			ELSE
				#if USE_CLF_DLC
					strLabel += "_T1"
				#endif
				#if USE_NRM_DLC
					strLabel += "_T1"
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
						strLabel += "_T0"
					ELSE
						strLabel += "_T1"
					ENDIF
				#endif
				#endif

			ENDIF
			
		BREAK
	ENDSWITCH
	
	IF VDIST2(vMemberCoord, vPlayerCoord) > (10*10)
		#IF IS_DEBUG_BUILD
		DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (eFamilyEvent = FE_M_SON_rapping_in_the_shower)	//#1576971
	OR (eFamilyEvent = FE_M_DAUGHTER_shower)
	
		CONST_FLOAT fMAX_HEIGHT_FOR_SPEECH	2.0
		IF (ABSF(vPlayerCoord.z-vMemberCoord.z) > fMAX_HEIGHT_FOR_SPEECH)
			#IF IS_DEBUG_BUILD
			DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT IS_ENTITY_ON_SCREEN(memberID)
		OR NOT IS_ENTITY_VISIBLE(memberID)
			#IF IS_DEBUG_BUILD
			DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	BOOL bIgnoreLockDoorCheck = FALSE
	SWITCH eFamilyEvent
		CASE FE_M_SON_phone_calls_in_room
		CASE FE_M_SON_watching_porn
		
		CASE FE_M_FAMILY_MIC4_locked_in_room
		
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
		CASE FE_M_DAUGHTER_sex_sounds_from_room
		CASE FE_M_DAUGHTER_on_phone_LOCKED
		
		CASE FE_T1_FLOYD_with_wade_post_docks1
			bIgnoreLockDoorCheck = TRUE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	INT iMemberRoomKey = GET_ROOM_KEY_FROM_ENTITY(memberID)
	TEXT_LABEL_63 sRoom = "playerRoom: "
	sRoom += g_sShopSettings.playerRoom
	sRoom += ", famRoom: "
	sRoom += iMemberRoomKey
	
	IF bIgnoreLockDoorCheck
		sRoom += " IGNORE"
	ENDIF 
	IF g_sShopSettings.playerRoom <> iMemberRoomKey
		DrawDebugSceneTextWithOffset(sRoom, vMemberCoord, -5, HUD_COLOUR_GREEN)
	ELSE
		DrawDebugSceneTextWithOffset(sRoom, vMemberCoord, -5, HUD_COLOUR_ORANGE)
	ENDIF
	#ENDIF
	
	IF NOT IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_TALK)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_TALK)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str = "greet: \""
		str += strLabel
		str += "\""
		DrawDebugSceneTextWithOffset(str, vMemberCoord, -2, HUD_COLOUR_PURE_WHITE)
		
		TEXT_LABEL strLabel_a = strLabel
		TEXT_LABEL strLabel_b = strLabel
		strLabel_b += "_01"
		
		TEXT_LABEL strLabel_c0 = strLabel, strLabel_c1 = strLabel
		strLabel_c0 += "a_1"
		strLabel_c1 += "a_2"
		
		str  = "\""
		IF DOES_TEXT_LABEL_EXIST(strLabel_a)
			str  = "a:\""
			str += GET_STRING_FROM_TEXT_FILE(strLabel_a)
			str  = "\""
			DrawDebugSceneTextWithOffset(str, vMemberCoord, -1, HUD_COLOUR_PURE_WHITE)
		ELSE
			IF DOES_TEXT_LABEL_EXIST(strLabel_b)
				str  = "b:\""
				str += GET_STRING_FROM_TEXT_FILE(strLabel_b)
				str += "\""
				DrawDebugSceneTextWithOffset(str, vMemberCoord, -1, HUD_COLOUR_PURE_WHITE)
			ELSE
				IF DOES_TEXT_LABEL_EXIST(strLabel_c0)
					IF DOES_TEXT_LABEL_EXIST(strLabel_c1)
						str  = "c0:\""
						str += GET_STRING_FROM_TEXT_FILE(strLabel_c0)
						str += "\""
						DrawDebugSceneTextWithOffset(str, vMemberCoord, -4, HUD_COLOUR_PURE_WHITE)
						
						str  = "c1:\""
						str += GET_STRING_FROM_TEXT_FILE(strLabel_c1)
						str += "\""
						DrawDebugSceneTextWithOffset(str, vMemberCoord, -3, HUD_COLOUR_PURE_WHITE)
						
						
					ELSE
						str  = "c0:\""
						str += GET_STRING_FROM_TEXT_FILE(strLabel_c0)
						str += "\""
						DrawDebugSceneTextWithOffset(str, vMemberCoord, -1, HUD_COLOUR_PURE_WHITE)
					ENDIF
				ELSE
					str  = "d:\""
					str += GET_STRING_FROM_TEXT_FILE("unknown...")
					str += "\""
					DrawDebugSceneTextWithOffset(str, vMemberCoord, -1, HUD_COLOUR_GREENLIGHT)
				ENDIF
			ENDIF
		ENDIF
		
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT bIgnoreLockDoorCheck
		#IF NOT IS_DEBUG_BUILD
		INT iMemberRoomKey = GET_ROOM_KEY_FROM_ENTITY(memberID)
		#ENDIF

		IF g_sShopSettings.playerRoom <> iMemberRoomKey
			#IF IS_DEBUG_BUILD
			DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_GREEN, 0.1)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	TEXT_LABEL strLabelData = strLabel
	strLabelData += "_01"
	
	BOOL bRandomConv
	
	IF DOES_TEXT_LABEL_EXIST(strLabelData)
		bRandomConv = TRUE
	ELSE
		bRandomConv = FALSE
	ENDIF
	
	IF bRandomConv
	OR GetRandomConversationFromLabel(strTextBlock, strLabel, tCreatedConvLabels)
		
		IF eFamilyMember = FM_FRANKLIN_AUNT
			IF ePlayer = CHAR_MICHAEL
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), "MICHAEL")
			ELIF ePlayer = CHAR_FRANKLIN
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
			ELIF ePlayer = CHAR_TREVOR
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), "TREVOR")
			ENDIF
		ENDIF
		
		IF Is_Ped_Playing_Family_Speech(strTextBlock, strLabel,
				inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
			#IF IS_DEBUG_BUILD
			IF g_bDrawDebugFamilyStuff
				CPRINTLN(DEBUG_FAMILY, "family member ", Get_String_From_FamilyEvent(eFamilyEvent), " playing greet line speech \"", strLabel, "\" [block:\"", strTextBlock, "\"]")
				
				INT iRepeat
				REPEAT COUNT_OF(inSpeechStruct.PedInfo) iRepeat
					IF (inSpeechStruct.PedInfo[iRepeat].Index = memberID)
						CPRINTLN(DEBUG_FAMILY, "	//\"", inSpeechStruct.PedInfo[iRepeat].VoiceId, "\" speaker num# ", iRepeat)
					ENDIF
				ENDREPEAT
				
				CPRINTLN(DEBUG_FAMILY, "")
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_WHITE)
			#ENDIF
			
			RESTART_TIMER_NOW(speechTimer)
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugSceneSphere(vMemberCoord, 0.5, HUD_COLOUR_BLACK)
	#ENDIF
	
	#IF NOT IS_DEBUG_BUILD
	eFamilyEvent = eFamilyEvent
	#ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL CheckAggro()

//"FMM_AGR1" - Michael is walking after player and annoying amanda
//"FMM_AGR2" - Michael/Amanda Really grief - level of intensity / irritation up from below
//"FMM_AGR3" - Michael/Amanda REALLY really grief  - AMANDA runs off screaming
//"FMM_AGR4" - Trevor or franklin are griefing Amanda - ie walking near her, annoying her
//"FMM_AGR5" - Trevor or franklin are really griefing Amanda
//"FMM_AGR6" - Trevor or franklin are really griefing Amanda - she runs off screaming

	RETURN FALSE
ENDFUNC

FUNC BOOL Play_This_Family_Speech(PED_INDEX memberID, enumFamilyEvents eFamilyEvent,
		structPedsForConversation &inSpeechStruct, STRING strTextBlock, TEXT_LABEL &tCreatedConvLabels[],
		structTimer &speechTimer, INT &iCurrentRandCount,
		FLOAT fMAX_DIST_FOR_SPEECH_2D = 5.0,
		STRING stringOverrideLabel = NULL,
		STRING stringFacialAnimClip = NULL)
	CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " Play_This_Family_Speech.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE(), ")")
	
	enumConversationPriority	enSpeechPriority			= CONV_PRIORITY_AMBIENT_MEDIUM
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(memberID)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(memberID)
		RETURN FALSE
	ENDIF
	
	//dont play if another dialogue is playing
	IF FAMILY_Is_Any_Dialogue_Playing(memberID)
		IF IS_TIMER_STARTED(speechTimer)
			IF NOT IS_TIMER_PAUSED(speechTimer)
				PAUSE_TIMER(speechTimer)
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	//dont play if MP hud is on screen
	IF IS_TRANSITION_ACTIVE()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR g_bResultScreenDisplaying
		IF IS_TIMER_STARTED(speechTimer)
			IF NOT IS_TIMER_PAUSED(speechTimer)
				PAUSE_TIMER(speechTimer)
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	//dont play if MP hud is on screen
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
	OR Is_Player_Timetable_Scene_In_Progress()
		IF IS_TIMER_STARTED(speechTimer)
			IF NOT IS_TIMER_PAUSED(speechTimer)
				PAUSE_TIMER(speechTimer)
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vMemberCoord = GET_ENTITY_COORDS(memberID)
	
	#IF IS_DEBUG_BUILD
	FLOAT fDebugStringOffsetY = 0.2
	FLOAT fFamilyEventRow
	
	IF (eFamilyEvent >= FE_T1_FLOYD_cleaning)
		fFamilyEventRow = TO_FLOAT(ENUM_TO_INT(eFamilyEvent - FE_T1_FLOYD_cleaning))
	ELIF (eFamilyEvent >= FE_T0_MICHAEL_depressed_head_in_hands)
		fFamilyEventRow = TO_FLOAT(ENUM_TO_INT(eFamilyEvent - FE_T0_MICHAEL_depressed_head_in_hands))
	ELIF (eFamilyEvent >= FE_F_AUNT_pelvic_floor_exercises)
		fFamilyEventRow = TO_FLOAT(ENUM_TO_INT(eFamilyEvent - FE_F_AUNT_pelvic_floor_exercises))
	ELSE
		fFamilyEventRow = TO_FLOAT(ENUM_TO_INT(eFamilyEvent))
	ENDIF
	
	#ENDIF
	
	IF Play_Greet_Player_Family_Context(memberID, eFamilyEvent, inSpeechStruct, strTextBlock, tCreatedConvLabels,
			speechTimer, enSpeechPriority)
		RETURN TRUE
	ENDIF
	CheckAggro()
	
	INT iRandCount, iSpeechBit, iPlayerBitset
	STRING strLabel = Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(stringOverrideLabel)
		strLabel = stringOverrideLabel
		
		#IF IS_DEBUG_BUILD
		fDebugStringOffsetY = 0.25
		#ENDIF
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strLabel)
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str = "strLabel "
		str += Get_String_From_FamilyEvent(eFamilyEvent)
		str += " is NULL"
		
		IF DrawDebugFamilyTextWithOffset(str, vPlayerCoord,
				fFamilyEventRow * fDebugStringOffsetY,
				HUD_COLOUR_GREYDARK, 0.5)
//			CPRINTLN(DEBUG_FAMILY, str)
		ENDIF
		
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkLocateBounds)
		
		IF NOT PRIVATE_IsPlayerAtFamilyEventSpeechBounds(memberID, eFamilyEvent)
		
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = "bounds "
			str += Get_String_From_FamilyEvent(eFamilyEvent)
			str += ": "
			str += GET_STRING_FROM_FLOAT(GET_DISTANCE_BETWEEN_COORDS(vPlayerCoord, vMemberCoord, FALSE))
			DrawDebugFamilyTextWithOffset(str, vPlayerCoord, fFamilyEventRow * fDebugStringOffsetY, HUD_COLOUR_GREENDARK)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
	
		IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkDistance2d)
			//dont play if player is too far from speaker
			IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoord, vMemberCoord, FALSE) > fMAX_DIST_FOR_SPEECH_2D)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = "dist "
				str += Get_String_From_FamilyEvent(eFamilyEvent)
				str += ": "
				str += GET_STRING_FROM_FLOAT(GET_DISTANCE_BETWEEN_COORDS(vPlayerCoord, vMemberCoord, FALSE))
				str += " > "
				str += ROUND(fMAX_DIST_FOR_SPEECH_2D)
				DrawDebugFamilyTextWithOffset(str, vPlayerCoord, fFamilyEventRow * fDebugStringOffsetY, HUD_COLOUR_GREEN)
				DrawDebugFamilySphere(vMemberCoord, fMAX_DIST_FOR_SPEECH_2D, HUD_COLOUR_GREEN, 0.1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		
		IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkOnSameFloor)
			//dont play if player is on a different floor from speaker
			CONST_FLOAT fMAX_HEIGHT_FOR_SPEECH	2.0
			IF (ABSF(vPlayerCoord.z-vMemberCoord.z) > fMAX_HEIGHT_FOR_SPEECH)
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str =  ("absf ")
				str += (Get_String_From_FamilyEvent(eFamilyEvent))
				str += (": ")
				str += GET_STRING_FROM_FLOAT(ABSF(vPlayerCoord.z-vMemberCoord.z))
				DrawDebugFamilyTextWithOffset(str, vPlayerCoord, fFamilyEventRow * fDebugStringOffsetY, HUD_COLOUR_GREEN)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkOnScreen)
			//dont play if ped isnt on screen
			IF NOT IS_ENTITY_ON_SCREEN(memberID)
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str =  ("family member ")
				str += (Get_String_From_FamilyEvent(eFamilyEvent))
				str += (" not on screen")
				DrawDebugFamilyTextWithOffset(str, vPlayerCoord, fFamilyEventRow * fDebugStringOffsetY, HUD_COLOUR_GREEN)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkTenSecDelay)
		CONST_FLOAT fCONST_checkTenSecDelay 10.0
		
		FLOAT fCheckTenSecDelay = fCONST_checkTenSecDelay
		IF iCurrentRandCount >= iRandCount
//			CPRINTLN(DEBUG_FAMILY, "iCurrentRandCount[", iCurrentRandCount, "] >= iRandCount[", iRandCount, "]")
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = "iCurrentRandCount "
			str += iCurrentRandCount
			str += ">= iRandCount"
			str += iRandCount
			DrawDebugFamilyTextWithOffset(str, vPlayerCoord, fFamilyEventRow * fDebugStringOffsetY, HUD_COLOUR_PURPLE)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		fCheckTenSecDelay += ((TO_FLOAT(iCurrentRandCount)/TO_FLOAT(iRandCount-1))*fCONST_checkTenSecDelay)
		
		IF eFamilyEvent = FE_M_DAUGHTER_on_phone_to_friends
		OR eFamilyEvent = FE_M_DAUGHTER_on_phone_LOCKED
			fCheckTenSecDelay *= 1.5
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		INT iTaunterAnimOutLength
		iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(Get_String_From_FamilyEvent(eFamilyEvent))
		
		CONST_INT iCONST_MAX_EVENT_NAME_LENGTH	25
		TEXT_LABEL_63 str
		IF iTaunterAnimOutLength >= iCONST_MAX_EVENT_NAME_LENGTH
			str = GET_STRING_FROM_STRING(Get_String_From_FamilyEvent(eFamilyEvent), 0, iCONST_MAX_EVENT_NAME_LENGTH)
		ELSE
			str = Get_String_From_FamilyEvent(eFamilyEvent)
		ENDIF
		
		str += (" speechTimer: ")
		
		IF NOT IS_TIMER_STARTED(speechTimer)
			str += ("STOPPED")
		ELSE
			str += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(speechTimer))
			IF NOT IS_TIMER_PAUSED(speechTimer)
				str += (" < ")
				str += ROUND(fCheckTenSecDelay)
			ELSE
				str += (" [paused]")
			ENDIF
		ENDIF
		
		str += " "
		str += iCurrentRandCount
		str += ":"
		str += iRandCount
		DrawDebugFamilyTextWithOffset(str, vPlayerCoord, fFamilyEventRow * fDebugStringOffsetY, HUD_COLOUR_PURPLE)
		#ENDIF
		
		IF IS_TIMER_STARTED(speechTimer)
			IF IS_TIMER_PAUSED(speechTimer)
				UNPAUSE_TIMER(speechTimer)
			ENDIF
			
			IF NOT TIMER_DO_WHEN_READY(speechTimer, fCheckTenSecDelay)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
		
		//"FMM_0_FRA"	//{Franklin sneaks into Michael's house - Jimmy responds}
		//"FMM_0_TRV"	//{Trevor sneaks into Michael's house - Jimmy Responds}
		//"FMM_1_FRA"	//{Tracey sees Franklin in the house}
		//"FMM_1_TRV"	//{Tracey sees Trevor in the house}

		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		
		
		TEXT_LABEL strSneakLabel = ""
		SWITCH GET_enumFamilyMember_from_ped(memberID)
			CASE FM_MICHAEL_SON
				strSneakLabel = "FMM_0"
			BREAK
			CASE FM_MICHAEL_DAUGHTER
				strSneakLabel = "FMM_1"
			BREAK
			CASE FM_MICHAEL_WIFE
				strSneakLabel = "FMM_2"
			BREAK
			
			CASE FM_MICHAEL_MEXMAID
				strSneakLabel = "FMM_3_FT"
			BREAK
			
			CASE FM_FRANKLIN_AUNT
				strSneakLabel = "FMF_0"
				
				IF (ePed = CHAR_MICHAEL OR ePed = CHAR_TREVOR)
					strSneakLabel = ""
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strSneakLabel)
			
			IF NOT ARE_STRINGS_EQUAL(strSneakLabel, "FMM_3_FT")
				SWITCH ePed
					CASE CHAR_MICHAEL
						strSneakLabel += "_MIC"
					BREAK
					CASE CHAR_FRANKLIN
						strSneakLabel += "_FRA"
						
						IF ARE_STRINGS_EQUAL(strSneakLabel, "FMM_2_FRA")
							#if  USE_CLF_DLC
								strSneakLabel += "2"	//mid to late game
							#ENDIF
							#if  USE_NRM_DLC
								strSneakLabel += "2"	//mid to late game
							#ENDIF
							#if not USE_CLF_DLC
							#if not USE_NRM_DLC
							IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
								strSneakLabel += "1"	//early game
							ELSE
								strSneakLabel += "2"	//mid to late game
							ENDIF
							#ENDIF
							#ENDIF
						ENDIF
					BREAK
					CASE CHAR_TREVOR
						strSneakLabel += "_TRV"
					BREAK
				ENDSWITCH
			ENDIF
			
			IF Is_Ped_Playing_Family_Speech(strTextBlock, strSneakLabel,
					inSpeechStruct, enSpeechPriority, tCreatedConvLabels,
					stringFacialAnimClip)
				#IF IS_DEBUG_BUILD
				IF g_bDrawDebugFamilyStuff
					CPRINTLN(DEBUG_FAMILY, "family member ", Get_String_From_FamilyEvent(eFamilyEvent), " playing sneaked in \"", strLabel, "\" [block:\"", strTextBlock, "\"]")
					
					INT iRepeat
					REPEAT COUNT_OF(inSpeechStruct.PedInfo) iRepeat
						IF (inSpeechStruct.PedInfo[iRepeat].Index = memberID)
							CPRINTLN(DEBUG_FAMILY, "	//\"", inSpeechStruct.PedInfo[iRepeat].VoiceId, "\" speaker num# ", iRepeat)
						ENDIF
					ENDREPEAT
					
					CPRINTLN(DEBUG_FAMILY, "")
				ENDIF
				#ENDIF
				
				RESTART_TIMER_NOW(speechTimer)
				iCurrentRandCount = 99
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		ELSE
			CPRINTLN(DEBUG_FAMILY, "strSneakLabel is null")
			RETURN FALSE
		ENDIF
	ENDIF
	
	TEXT_LABEL strPlayerName = ""
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL	strPlayerName = "MICHAEL" BREAK
		CASE CHAR_FRANKLIN	strPlayerName = "FRANKLIN" BREAK
		CASE CHAR_TREVOR	strPlayerName = "TREVOR" BREAK
	ENDSWITCH
	
	
	#IF USE_TU_CHANGES
	INT YourNumberID = -1
	VECTOR YourVoiceLoc = <<0,0,0>>
	IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_setVoiceLocationForPed)
		YourNumberID = 1		//ENUM_TO_INT(GET_enumFamilyMember_from_ped(memberID))+1
		YourVoiceLoc = <<-14.3293, -1443.6351, 29.9000>>	//vMemberCoord
	ENDIF
	#ENDIF
	
	IF ARE_STRINGS_EQUAL("TRA_IG_MD", strLabel)
		IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
			//
		ELSE
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
		ENDIF
	ENDIF
	IF ARE_STRINGS_EQUAL("FMT_COUNTRY", strLabel)
		IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
			CPRINTLN(DEBUG_FAMILY, "\"FMT_COUNTRY\" has FSC_singleRandomLine set...")
			//
		ELSE
			CPRINTLN(DEBUG_FAMILY, "\"FMT_COUNTRY\" has FSC_singleRandomLine unset...")
			SET_BITMASK_AS_ENUM(iSpeechBit, FSC_singleRandomLine)
		ENDIF
	ENDIF
	
	//is the speech a single line or is it a random conv
	IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
		IF NOT IS_STRING_NULL_OR_EMPTY(strLabel)
			// 
			IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkListenerAlive)
				IF NOT IsSpeechListenerAlive(inSpeechStruct, strTextBlock, strLabel)
					RETURN FALSE
				ENDIF
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), strPlayerName)
			
			#IF USE_TU_CHANGES
			IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_setVoiceLocationForPed)
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, YourNumberID, NULL, "DENISE")
				SET_VOICE_LOCATION_FOR_NULL_PED(inSpeechStruct, YourNumberID, YourVoiceLoc)
			ENDIF
			#ENDIF
			
			IF Is_Ped_Playing_Family_Speech(strTextBlock, strLabel,
					inSpeechStruct, enSpeechPriority, tCreatedConvLabels,
					stringFacialAnimClip)
				#IF IS_DEBUG_BUILD
				IF g_bDrawDebugFamilyStuff
					CPRINTLN(DEBUG_FAMILY, "family member ", Get_String_From_FamilyEvent(eFamilyEvent), " playing single line speech \"", strLabel, "\" [block:\"", strTextBlock, "\"]")
					
					INT iRepeat
					REPEAT COUNT_OF(inSpeechStruct.PedInfo) iRepeat
						IF (inSpeechStruct.PedInfo[iRepeat].Index = memberID)
							CPRINTLN(DEBUG_FAMILY, "	//\"", inSpeechStruct.PedInfo[iRepeat].VoiceId, "\" speaker num# ", iRepeat)
						ENDIF
					ENDREPEAT
					
					CPRINTLN(DEBUG_FAMILY, "")
				ENDIF
				#ENDIF
				
				RESTART_TIMER_NOW(speechTimer)
				iCurrentRandCount++
				RETURN TRUE
			ENDIF
		ELSE
			CASSERTLN(DEBUG_FAMILY, "Play_This_Family_Speech(", Get_String_From_FamilyEvent(eFamilyEvent), ") - strLabel is a null value.")
			RETURN FALSE
		ENDIF
	ELSE
		TEXT_LABEL returnedStrLabel = strLabel
		IF GetRandomConversationFromLabel(strTextBlock, returnedStrLabel, tCreatedConvLabels)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(returnedStrLabel)
				// 
				IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_checkListenerAlive)
					IF NOT IsSpeechListenerAlive(inSpeechStruct, strTextBlock, returnedStrLabel)
						RETURN FALSE
					ENDIF
				ENDIF
				
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, PLAYER_PED_ID(), strPlayerName)
				
				#IF USE_TU_CHANGES
				IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_setVoiceLocationForPed)
					ADD_PED_FOR_DIALOGUE(inSpeechStruct, YourNumberID, NULL, "DENISE")
					SET_VOICE_LOCATION_FOR_NULL_PED(inSpeechStruct, YourNumberID, YourVoiceLoc)
				ENDIF
				#ENDIF
				
				IF Is_Ped_Playing_Family_Speech(strTextBlock, returnedStrLabel,
						inSpeechStruct, enSpeechPriority, tCreatedConvLabels,
						stringFacialAnimClip)
					#IF IS_DEBUG_BUILD
					IF g_bDrawDebugFamilyStuff
						IF HAS_THIS_ADDITIONAL_TEXT_LOADED(strTextBlock, MISSION_DIALOGUE_TEXT_SLOT)
							CPRINTLN(DEBUG_FAMILY, "family member ", Get_String_From_FamilyEvent(eFamilyEvent), " playing single speech \"", returnedStrLabel, "\" [block:\"", strTextBlock, "\"", " LOADED", "]")
						ELSE
							CPRINTLN(DEBUG_FAMILY, "family member ", Get_String_From_FamilyEvent(eFamilyEvent), " playing single speech \"", returnedStrLabel, "\" [block:\"", strTextBlock, "\"", " ?unloaded?", "]")
						ENDIF
						
						
						INT iRepeat
						REPEAT COUNT_OF(inSpeechStruct.PedInfo) iRepeat
							IF (inSpeechStruct.PedInfo[iRepeat].Index = memberID)
								CPRINTLN(DEBUG_FAMILY, "	//\"", inSpeechStruct.PedInfo[iRepeat].VoiceId, "\" speaker num# ", iRepeat)
							ENDIF
						ENDREPEAT
						
						CPRINTLN(DEBUG_FAMILY, "")
					ENDIF
					#ENDIF
					
					RESTART_TIMER_NOW(speechTimer)
					iCurrentRandCount++
					RETURN TRUE
				ENDIF
			ELSE
				CASSERTLN(DEBUG_FAMILY, "Play_This_Family_Speech(", Get_String_From_FamilyEvent(eFamilyEvent), ") - returnedStrLabel is a null value.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(FALSE)
ENDFUNC


FUNC BOOL PRIVATE_Update_Family_Context_Speech(PED_INDEX memberID, STRING Context, structTimer &speechTimer, FLOAT fContext_phase_duration = 3.0)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	IF FAMILY_Is_Any_Dialogue_Playing(memberID)
		#IF IS_DEBUG_BUILD
		str  = "Is_Any_Dialogue_Playing"
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(memberID, FALSE), 5, HUD_COLOUR_BLACK)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TIMER_STARTED(speechTimer)
		#IF IS_DEBUG_BUILD
		str  = "timer not started"
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(memberID, FALSE), 5, HUD_COLOUR_BLACK)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	FLOAT fSpeechTimer = GET_TIMER_IN_SECONDS(speechTimer)
	
	IF NOT TIMER_DO_WHEN_READY(speechTimer, 1.0)
		#IF IS_DEBUG_BUILD
		str  = "sSpeechTimer: "
		str += GET_STRING_FROM_FLOAT(fSpeechTimer)
		str += " "
		str += Context
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(memberID, FALSE), 5, HUD_COLOUR_BLACK)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	FLOAT fContextPhase = (fSpeechTimer % fCONTEXT_PHASE_DURATION) / fCONTEXT_PHASE_DURATION
	
	#IF IS_DEBUG_BUILD
	str  = "sSpeechTimer: "
	str += GET_STRING_FROM_FLOAT(fSpeechTimer)
	str += " "
	str += Context
	str += " "
	str += GET_STRING_FROM_FLOAT(fContextPhase)
	
	DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(memberID, FALSE), 5, HUD_COLOUR_RED)
	#ENDIF
	
	IF fContextPhase < 0.9
		RETURN FALSE
	ENDIF
	
	PLAY_PED_AMBIENT_SPEECH(memberID, Context)
	CPRINTLN(DEBUG_FAMILY, "PLAY_PED_AMBIENT_SPEECH(memberID, \"", Context, "\")")
	RETURN TRUE
ENDFUNC
