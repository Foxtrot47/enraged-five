USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_entity.sch"
USING "commands_interiors.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch"
USING "commands_zone.sch"
USING "commands_replay.sch"

USING "script_player.sch"

USING "waiting_taxi.sch"
USING "dialogue_public.sch"

//Network Headers
USING "net_taxi.sch"

ENUM TAXI_DIALOGUE
	TAXI_DIALOGUE_NOTHING = 0,
	
	TAXI_DIALOGUE_WHERE_TO,			
	TAXI_DIALOGUE_BEGIN_JOURNEY_1,	
	TAXI_DIALOGUE_BEGIN_JOURNEY_2,	
	TAXI_DIALOGUE_BANTER,			
	TAXI_DIALOGUE_ARRIVE,			
	TAXI_DIALOGUE_CLOSE_AS_POSS,
	TAXI_DIALOGUE_NO_MONEY,		 
	
	TAXI_DIALOGUE_TUTORIAL_LETS_GO,
	
	TAXI_DIALOGUE_CHANGE_DEST_1,	
	TAXI_DIALOGUE_CHANGE_DEST_2,	
	TAXI_DIALOGUE_CHANGE_DEST_3,

	TAXI_DIALOGUE_RUN_AWAY,		
	TAXI_DIALOGUE_OUT_EARLY,		
	TAXI_DIALOGUE_TRASHED,			
	TAXI_DIALOGUE_PART_JOURNEY,
	TAXI_DIALOGUE_FIRST_CAB,
	
	TAXI_DIALOGUE_RADIO_1,			
	TAXI_DIALOGUE_RADIO_2,			
	TAXI_DIALOGUE_RADIO_3,		
	
	TAXI_DIALOGUE_STEP_ON_IT_1, 	
	TAXI_DIALOGUE_STEP_ON_IT_2,
	TAXI_DIALOGUE_WAIT_TO_FINISH,	
	TAXI_DIALOGUE_THANKS,			

	TAXI_DIALOGUE_STOP_REQUEST,	
	TAXI_DIALOGUE_START_REQUEST,
	TAXI_DIALOGUE_DRIVE_BY_REACTION,
	
	TAXI_DIALOGUE_UNSAFE_DESTINATION,
	
	TAXI_DIALOGUE_END_OF_LIST		
ENDENUM

/// PURPOSE: areas around the map which will require special taxi setup
ENUM TAXI_SPECIAL_AREAS_ENUM
	TAXI_SPECIAL_AREAS_MICHAEL_HOUSE = 0,
	TAXI_SPECIAL_AREAS_DEL_PERRO_PIER,
	TAXI_SPECIAL_AREAS_ARMENIAN_2_ALLEYWAY,	// Melanoma St. Vespucci Beach
	TAXI_SPECIAL_AREA_MILITARY_BASE,
	TAXI_SPECIAL_AREA_MARTIN_RANCH,
	TAXI_SPECIAL_AREA_FLOYD_APARTMENT,
	TAXI_SPECIAL_AREAS_INVALID
ENDENUM

STRUCT TAXI_DEST
	BLIP_INDEX BlipID
	INT iHashname1
	INT iHashname2
ENDSTRUCT

TAXI_DEST currentTaxiDestination

CONST_FLOAT MINIMUM_DROP_OFF_DISTANCE_TO_COORD_WARPING			5.0		//10.0
CONST_FLOAT	MINIMUM_DROP_OFF_DISTANCE_TO_COORD_DRIVING			25.0
CONST_FLOAT MINIMUM_DROP_OFF_DISTANCE_TO_COORD_DRIVING_FAST		50.0

BLIP_SPRITE taxiStandardBlipSprite = GET_STANDARD_BLIP_ENUM_ID(), taxiWaypointBlipSprite = GET_WAYPOINT_BLIP_ENUM_ID()


/// PURPOSE:
///    get the taxi vehicle's model
/// RETURNS:
///    MODEL_NAMES
FUNC MODEL_NAMES GET_TAXI_MODEL()
	RETURN TAXI
ENDFUNC

/// PURPOSE:
///    get the taxi driver's model
/// RETURNS:
///    MODEL_NAMES
FUNC MODEL_NAMES GET_TAXI_DRIVER_MODEL()
	RETURN A_M_Y_STLAT_01	// new agree taxi driver ped model - see B*1424685	
ENDFUNC

/// PURPOSE:
///    check the specified vehicle is a taxi
/// PARAMS:
///    vehicleIndex - vehicle to check
/// RETURNS:
///    TRUE if vehicleIndex is a taxi model
FUNC BOOL IS_VEHICLE_A_TAXI_MODEL(VEHICLE_INDEX vehicleIndex)

	IF IS_VEHICLE_MODEL(vehicleIndex, GET_TAXI_MODEL())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Test for if two coords are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other
FUNC BOOL TAXI_IS_COORD_IN_RANGE_OF_COORD(VECTOR v1, VECTOR v2, FLOAT fRange)
	RETURN VDIST2(v1, v2) <= (fRange*fRange)
ENDFUNC

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

/// PURPOSE:
///    Very efficient test for if two entities are with fRange metres of each other
///    Will be more efficient than doing IS_ENTITY_AT_ENTITY()
///    NOTE: This function doesn't do dead checks to keep it fast, make sure you do them yourself!
/// PARAMS:
///    e1 - First entity
///    e2 - Second entity
///    fRange - Test if the entities are this close to each other
FUNC BOOL TAXI_IS_ENTITY_IN_RANGE_ENTITY(ENTITY_INDEX e1, ENTITY_INDEX e2, FLOAT fRange)
	RETURN ( VDIST2(GET_ENTITY_COORDS(e1), GET_ENTITY_COORDS(e2)) <= fRange*fRange )
ENDFUNC

/// PURPOSE:
///    Very efficient test for if two entities are with fRange metres of each other
///    Will be more efficient than doing IS_ENTITY_AT_ENTITY()
///    NOTE: This function doesn't do dead checks to keep it fast, make sure you do them yourself!
/// PARAMS:
///    e1 - First entity
///    e2 - Second entity
///    fRange - Test if the entities are this close to each other
FUNC BOOL TAXI_IS_ENTITY_IN_RANGE_COORDS(ENTITY_INDEX e1, VECTOR vCoord, FLOAT fRange)
	RETURN ( VDIST2(GET_ENTITY_COORDS(e1), vCoord) <= fRange*fRange )
ENDFUNC

/// PURPOSE:
///    Tests if a ped is either performing a script task or is waiting to start it
/// PARAMS:
///    testPed - the ped to test
///    testTask - the script task to check for
/// RETURNS:
///    TRUE if the ped is performing the task or waiting to start it, FALSE otherwise
FUNC BOOL TAXI_IS_PED_PERFORMING_TASK(PED_INDEX testPed, SCRIPT_TASK_NAME testTask)
	IF DOES_ENTITY_EXIST(testPed)
		IF NOT IS_ENTITY_DEAD(testPed)
			IF NOT IS_PED_INJURED(testPed)
				IF (GET_SCRIPT_TASK_STATUS(testPed, testTask) = PERFORMING_TASK) OR (GET_SCRIPT_TASK_STATUS(testPed, testTask) = WAITING_TO_START_TASK)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_COORD_ON_SAME_LEVEL_AS_COORD(VECTOR vCoord1, VECTOR vCoord2)
	FLOAT acceptableHeight
	FLOAT fTemp = vCoord2.z - vCoord1.z
	
	IF fTemp < 0.0
		fTemp *= -1.0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		printDebugString("\nIS_COORD_ON_SAME_LEVEL_AS_COORD - vCoord1.z = ")
		printDebugFloat(vCoord1.z)
		printDebugString("\n")
		printDebugString("\nIS_COORD_ON_SAME_LEVEL_AS_COORD - vCoord2.z = ")
		printDebugFloat(vCoord2.z)
		printDebugString("\n")
		printDebugString("\nIS_COORD_ON_SAME_LEVEL_AS_COORD - fTemp = ")
		printDebugFloat(fTemp)
		printDebugString("\n")
	#ENDIF
	
	IF vCoord1.z <= 1 // Destination is most likely a waypoint blip
		printDebugString("\nIS_COORD_ON_SAME_LEVEL_AS_COORD - vCoord1.z <= 1, probably a waypoint blip - RETURN TRUE\n")
		RETURN TRUE
	ENDIF

	
	acceptableHeight = VDIST(vCoord1, vCoord2)
	acceptableHeight = acceptableHeight/1.75
	#IF IS_DEBUG_BUILD
		printDebugString("\nIS_COORD_ON_SAME_LEVEL_AS_COORD - (VDIST(vCoord1, vCoord2)/1.75) = ")
		printDebugFloat(acceptableHeight)
		printDebugString("\n")
	#ENDIF
	
	// make sure taxi is on same level as player
	IF fTemp < acceptableHeight
		printDebugString("\nIS_COORD_ON_SAME_LEVEL_AS_COORD - fTemp < acceptableHeight, RETURN TRUE\n")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_COORDS_IN_3D_AREA(VECTOR vInCoords, VECTOR vMinCoords, VECTOR vMaxCoords)
	IF  (vInCoords.x > vMinCoords.x)
	AND (vInCoords.x < vMaxCoords.x)
	AND	(vInCoords.y > vMinCoords.y)
	AND	(vInCoords.y < vMaxCoords.y)
	AND (vInCoords.z > vMinCoords.z)
	AND (vInCoords.z < vMaxCoords.z)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

/// PURPOSE:
///   	Gets Heading given 2 coords
///    NOTE: taken from "rgeneral_include.sch"
/// PARAMS:
///     oldCoords - source point
///    	newCoords - target point
///    	invert - this is true (just keep it as true)
///	RETURNS:
///     Heading
FUNC FLOAT TAXI_GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=true)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC

/// PURPOSE:
///    check the specified vehicle is a taxi and has it's lights on
/// PARAMS:
///    vehicleIndex - vehicle to check
/// RETURNS:
///    TRUE if vehicleIndex is a taxi model and has it's lights on
FUNC BOOL DOES_TAXI_HAVE_LIGHT_ON(VEHICLE_INDEX vehicleIndex)

	IF DOES_ENTITY_EXIST(vehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
			IF IS_VEHICLE_A_TAXI_MODEL(vehicleIndex)
				IF IS_TAXI_LIGHT_ON(vehicleIndex)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE // Temp until taxis actually have lights
ENDFUNC

/// PURPOSE:
///    check the ped model is suitable taxi driver
/// PARAMS:
///    pedIndex - ped to check
/// RETURNS:
///    TRUE if pedIndex is suitable driver model
FUNC BOOL IS_PED_A_SUITABLE_TAXI_DRIVER_MODEL(PED_INDEX pedIndex)

	IF NOT IS_PED_INJURED(pedIndex)
		IF pedIndex <> PLAYER_PED_ID()
			IF IS_ENTITY_A_MISSION_ENTITY(pedIndex)
				IF IS_PED_MODEL(pedIndex, A_M_Y_STLAT_01)	// new agree taxi driver ped model - see B*1424685
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE	// not sure why this is being done yet?
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the taxi service script is safe to run
/// RETURNS:
///    FALSE if there is a reason to force terminate
FUNC BOOL IS_TAXI_SERVICE_SAFE_TO_RUN()
	
	// Player is injured
	IF IS_PED_INJURED(PLAYER_PED_ID())
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_TAXI_SERVICE_SAFE_TO_RUN() return FALSE : IS_PED_INJURED(PLAYER_PED_ID()) check")
		RETURN FALSE
	ENDIF
	
	// script cannot be holding onto assets when a lead in is playing out
	IF g_bPlayerLockedInToTrigger = TRUE
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_TAXI_SERVICE_SAFE_TO_RUN() return FALSE : g_bPlayerLockedInToTrigger check")
		RETURN FALSE
	ENDIF
	
	// script cannot be holding onto assets when a mocap is playing out
	IF IS_CUTSCENE_PLAYING()
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_TAXI_SERVICE_SAFE_TO_RUN() return FALSE : IS_CUTSCENE_PLAYING check")
		RETURN FALSE
	ENDIF
	
	IF g_bRequestTaxiServiceCleanup
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_TAXI_SERVICE_SAFE_TO_RUN() return FALSE : g_bRequestTaxiServiceCleanup has been set.")
		RETURN FALSE
	ENDIF
	
	IF g_bRockstarEditorActive
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_TAXI_SERVICE_SAFE_TO_RUN() return FALSE : g_bRockstarEditorActive has been set.")
		RETURN FALSE
	ENDIF
	
	// B*2190070 - duplicate Taxi in replay editor
	IF REPLAY_SYSTEM_HAS_REQUESTED_A_SCRIPT_CLEANUP()
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_TAXI_SERVICE_SAFE_TO_RUN() return FALSE : REPLAY_SYSTEM_HAS_REQUESTED_A_SCRIPT_CLEANUP check.")
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    request path nodes using REQUEST_PATH_NODES_IN_AREA_THIS_FRAME
/// PARAMS:
///    vPickupPosition - where the taxi picks up the player from
///    vDestinationPosition - where the taxi needs to path to
///    vPathNodeMin - the calculated minimum position we are loading paths
///    vPathNodeMax - the calculated maximum position we are loading paths
///    iExtraDist - used to add leeywa area around the position before the request
PROC REQUEST_PATH_NODES_IN_AREA_FOR_TAXI_THIS_FRAME(VECTOR vPickupPosition, VECTOR vDestinationPosition, VECTOR &vPathNodeMin, VECTOR &vPathNodeMax, INT iExtraDist = 1000)
	
	VECTOR vTempPathNodeMin, vTempPathNodeMax
	
	// determine if this is a new request 
	
	// set the area's X values
	IF vDestinationPosition.X <= vPickupPosition.X
		vTempPathNodeMin.X = vDestinationPosition.X
		vTempPathNodeMax.X = vPickupPosition.X
	ELSE
		vTempPathNodeMin.X = vPickupPosition.X
		vTempPathNodeMax.X = vDestinationPosition.X
	ENDIF
	// set the area's X values
	IF vDestinationPosition.Y <= vPickupPosition.Y
		vTempPathNodeMin.Y = vDestinationPosition.Y
		vTempPathNodeMax.Y = vPickupPosition.Y
	ELSE
		vTempPathNodeMin.Y = vPickupPosition.Y
		vTempPathNodeMax.Y = vDestinationPosition.Y
	ENDIF
	// extra leeway around the positions
	vTempPathNodeMin -= << iExtraDist, iExtraDist, iExtraDist >>
	vTempPathNodeMax += << iExtraDist, iExtraDist, iExtraDist >>
	CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : REQUEST_PATH_NODES_IN_AREA_FOR_TAXI_THIS_FRAME -locally generated vector = vMin : ", vTempPathNodeMin, " vMax : ", vTempPathNodeMax,
														" using vPickupPosition : ", vPickupPosition, " : vDestinationPosition : ", vDestinationPosition)
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vTempPathNodeMin, vPathNodeMin)
	OR NOT ARE_VECTORS_ALMOST_EQUAL(vTempPathNodeMin, vPathNodeMin)
		vPathNodeMin = vTempPathNodeMin
		vPathNodeMax = vTempPathNodeMax
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : REQUEST_PATH_NODES_IN_AREA_FOR_TAXI_THIS_FRAME - detected new request for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
	ELSE
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : REQUEST_PATH_NODES_IN_AREA_FOR_TAXI_THIS_FRAME - detected existing request for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
	ENDIF
	
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeMin.X, vPathNodeMin.Y, vPathNodeMax.X, vPathNodeMax.Y)
ENDPROC

/// PURPOSE:
///    check if the path nodes are loaded for the area
/// PARAMS:
///    vPathNodeMin - minimum positon
///    vPathNodeMax - maximum position
/// RETURNS:
///    if ARE_NODES_LOADED_FOR_AREA are TRUE
FUNC BOOL ARE_REQUEST_PATH_NODES_LOADED_FOR_TAXI(VECTOR vPathNodeMin, VECTOR vPathNodeMax)
	IF ARE_NODES_LOADED_FOR_AREA(vPathNodeMin.X, vPathNodeMin.Y, vPathNodeMax.X, vPathNodeMax.Y)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_REQUEST_PATH_NODES_LOADED_FOR_TAXI - returning TRUE for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
		RETURN TRUE
	ENDIF
	CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_REQUEST_PATH_NODES_LOADED_FOR_TAXI - returning FALSE for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Create a taxi and 
/// PARAMS:
///    vehIndexReturned - the returned taxi vehicle index
///    pedIndexReturned - the returned taxi drvier index
///    vPos - coords to spawn at
///    fHeading - heading to spawn with
/// RETURNS:
///    TRUE if taxi and driver were successfully created
FUNC BOOL CREATE_TAXI_AND_DRIVER(VEHICLE_INDEX &vehIndexReturned, PED_INDEX &pedIndexReturned, VECTOR vPos, FLOAT fHeading)

	REQUEST_MODEL(GET_TAXI_DRIVER_MODEL())
	REQUEST_MODEL(GET_TAXI_MODEL())
	IF HAS_MODEL_LOADED(GET_TAXI_MODEL())
		IF HAS_MODEL_LOADED(GET_TAXI_DRIVER_MODEL())
			
			// Create taxi
			vehIndexReturned = CREATE_VEHICLE(GET_TAXI_MODEL(), vPos, fHeading)
			
			IF DOES_ENTITY_EXIST(vehIndexReturned)
				IF IS_VEHICLE_DRIVEABLE(vehIndexReturned)
				
					SET_VEHICLE_ON_GROUND_PROPERLY(vehIndexReturned)	// B*1163822 - maybe it left through because the offset putit lower than ground z?
					SET_TAXI_LIGHTS(vehIndexReturned, TRUE)
					SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehIndexReturned, TRUE)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehIndexReturned, FALSE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndexReturned, TRUE)	// B*1163822 - task fell through world, JMart says once taxi gets tasked and has a valid follow route it will drive
					
					#IF IS_DEBUG_BUILD
						TEXT_LABEL tDebugName = "TSveh"
						SET_VEHICLE_NAME_DEBUG(vehIndexReturned, tDebugName)
					#ENDIF
							
					// Create Driver
					pedIndexReturned = CREATE_PED_INSIDE_VEHICLE(vehIndexReturned, PEDTYPE_SPECIAL, GET_TAXI_DRIVER_MODEL())
					
					IF DOES_ENTITY_EXIST(pedIndexReturned)
						IF NOT IS_PED_INJURED(pedIndexReturned)
							
							SET_VEHICLE_FORWARD_SPEED(vehIndexReturned, 5.0)
							
							IF NOT IS_ENTITY_A_MISSION_ENTITY(pedIndexReturned)
								SET_ENTITY_AS_MISSION_ENTITY(pedIndexReturned)
								CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : CREATE_TAXI_AND_DRIVER() : set pedIndexReturned as mission entity")
							ENDIF
							IF NOT IS_ENTITY_A_MISSION_ENTITY(vehIndexReturned)
								SET_ENTITY_AS_MISSION_ENTITY(vehIndexReturned)
								CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : CREATE_TAXI_AND_DRIVER() : set vehIndexReturned as mission entity")
							ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndexReturned, TRUE)
							// B*1536974 - Press to enter taxi as passenger hold to jack driver - opposite to how it worked on IV
							// to get IV behaviour you have to ensur this flag is false then task the player in script to enter as rear passenger when input detected
							SET_PED_CONFIG_FLAG(pedIndexReturned, PCF_AICanDrivePlayerAsRearPassenger, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedIndexReturned, CA_ALWAYS_FIGHT, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedIndexReturned, CA_ALWAYS_FLEE, TRUE)
							SET_PED_FLEE_ATTRIBUTES(pedIndexReturned, FA_NEVER_FLEE, FALSE)
		
							SET_AMBIENT_VOICE_NAME(pedIndexReturned, "A_M_M_EASTSA_02_LATINO_FULL_01")
							// alternative coming soon - 
							// SET_AMBIENT_VOICE_NAME(g_WaitingTaxiDriver, "S_M_M_BOUNCER_01_LATINO_FULL_01")

							SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_MODEL())
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_DRIVER_MODEL())
							
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : CREATE_TAXI_AND_DRIVER() return TRUE")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_TAXI(MODEL_NAMES TaxiModel, VECTOR pos, FLOAT heading, BOOL bCreateSlightlyToRight = FALSE, BOOL bCreateAsWaiting = TRUE)
	VEHICLE_INDEX CarID
	PED_INDEX DriverID
	REQUEST_MODEL(GET_TAXI_DRIVER_MODEL())
	REQUEST_MODEL(TaxiModel)
	IF HAS_MODEL_LOADED(TaxiModel)
		IF HAS_MODEL_LOADED(GET_TAXI_DRIVER_MODEL())
			
			// Create Cab
			CarID = CREATE_VEHICLE(TaxiModel, pos, heading)
			IF (bCreateSlightlyToRight)
				SET_ENTITY_HEADING(CarID, heading)
				SET_ENTITY_COORDS(CarID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CarID, <<2.0, 0.0, 0.0>>))
			ENDIF
			SET_VEHICLE_ON_GROUND_PROPERLY(CarID)	// B*1163822 - maybe it left through because the offset putit lower than ground z?
			SET_TAXI_LIGHTS(CarID, TRUE)
			SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(CarID, TRUE)

			// Create Driver
			DriverID = CREATE_PED_INSIDE_VEHICLE(CarID, PEDTYPE_SPECIAL, GET_TAXI_DRIVER_MODEL())
			SET_VEHICLE_FORWARD_SPEED(CarID, 5.0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(DriverID, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(TaxiModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_DRIVER_MODEL())
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(CarID, TRUE)	// B*1163822 - task fell through world, JMart says once taxi gets tasked and has a valid follow route it will drive
			
			#IF IS_DEBUG_BUILD
			
				TEXT_LABEL tDebugName = "TSveh"
				SET_VEHICLE_NAME_DEBUG(CarID, tDebugName)

				printDebugString("\n CREATE_TAXI - DONE \n")
			#ENDIF
			
			IF bCreateAsWaiting
				STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER(CarID, DriverID)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(DriverID)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)
			ENDIF
			
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC CREATE_TAXI_WITH_WAIT(MODEL_NAMES TaxiModel, VECTOR pos, FLOAT heading)
	WHILE NOT CREATE_TAXI(TaxiModel, pos, heading)
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    taken from restrictedareas.sc - perfer to move to common file
/// RETURNS:
///    TRUE if player is allowed to be in the studios
FUNC BOOL TAXI_IS_STUDIO_OPEN_FOR_CURRENT_PLAYER()
	#if USE_CLF_DLC
		return true
	#endif
	#if USE_NRM_DLC
		return true
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	enumCharacterList eTempChar = GET_CURRENT_PLAYER_PED_ENUM()

	IF eTempChar = CHAR_MICHAEL
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN)
			RETURN TRUE
		ENDIF
		// B*1405773 - Don't get a wanted level if Solomon1 is available, even if they don't go to security gates
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("ambient_Solomon")) > 0
			RETURN TRUE
		ENDIF

	ELIF eTempChar = CHAR_FRANKLIN
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN)
			RETURN TRUE
		ENDIF
		
	ELIF eTempChar = CHAR_TREVOR
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	#endif
	#endif
ENDFUNC

/// PURPOSE:
///    Used to decide if call to taxi service should be successful response
///    or position is ok to spawn a taxi
/// RETURNS:
///    TRUE if position is in a taxi restricted area

/// PURPOSE:
///    Used to decide if call to taxi service should be successful response
///    or position is ok to spawn a taxi
/// PARAMS:
///    vPosition - position to test
///    bAllowExceptionsForCallingTaxi - as this func is also used to determine if a call for a taxi should go through, some areas shouldn't block the call
///    							e.g. Floyd's apartment, don't want taxis in the apartment block but should be able to order one from inside the block to stop on the road
/// RETURNS:
///    TRUE if position is in a taxi restricted area 
FUNC BOOL IS_POSITION_IN_TAXI_RESTRICTED_AREA(VECTOR vPosition, BOOL bAllowExceptionsForCallingTaxi = FALSE)
	
	// only use this if it covers the green - still want to call cabs from the carpark
	//IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_GOLF_COURSE, vPosition)	// too big a radius check
	IF TAXI_IS_COORD_IN_RANGE_OF_COORD(vPosition, << -1144.49731, 43.01712, 51.94447 >>, 325.0)	// ensure checks only happen if player is within range of course
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_GOLF_COURSE)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_GOLF_COURSE check")
			RETURN TRUE
		ENDIF	
		// ac golf checks are too loose so need extra area checks unfortunately
		// Goes from here along the clubhouse heading clockwise
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1319.768555,29.526155,49.566158>>, <<-1336.526611,121.035141,75.618881>>, 18.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 1")
			RETURN TRUE
		ENDIF
		// Caddy carpark - leading to main carpark
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1360.019653,110.130661,52.634132>>, <<-1365.276001,172.062439,72.013123>>, 52.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 2")
			RETURN TRUE
		ENDIF
		// North East corner
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1363.006592,170.533035,50.008129>>, <<-1296.903320,178.813324,73.371040>>, 35.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 3")
			RETURN TRUE
		ENDIF
		// Along North wall
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1197.521240,199.864288,57.044708>>, <<-1298.039551,176.138412,73.332520>>, 34.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 4")
			RETURN TRUE
		ENDIF
		// Along North East corner
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1103.744873,221.136673,55.348141>>, <<-1208.393799,191.090927,79.137085>>, 45.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 5")
			RETURN TRUE
		ENDIF
		// East border
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-989.032837,-89.393761,32.488010>>, <<-1086.210938,-115.707649,50.650505>>, 70.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 6")
			RETURN TRUE
		ENDIF
		// South border
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-958.548096,-111.545547,30.764324>>, <<-1132.685303,190.784119,73.903656>>, 70.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 7")
			RETURN TRUE
		ENDIF
		// south boarder heading from southern most point up towards the club house
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1077.028198,-139.733704,38.733883>>, <<-1299.555908,-15.168939,63.437103>>, 20.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 8")
			RETURN TRUE
		ENDIF
		// south boarder West corner covering on foot entrance to green
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1319.768555,29.526155,47.566158>>, <<-1287.619263,-24.888639,67.527298>>, 20.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : golf course extra check 9")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_AIRPORT_AIRSIDE, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_AIRPORT_AIRSIDE)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_AIRPORT_AIRSIDE check")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MILITARY_BASE, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_MILITARY_BASE)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_MILITARY_BASE check")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_PRISON, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_PRISON)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_PRISON check")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_BIOTECH, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_BIOTECH)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_BIOTECH check")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MILITARY_DOCKS, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_MILITARY_DOCKS)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_MILITARY_DOCKS check")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT TAXI_IS_STUDIO_OPEN_FOR_CURRENT_PLAYER()
		IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MOVIE_STUDIO, vPosition)
			IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_MOVIE_STUDIO)
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_MOVIE_STUDIO check")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_DOWNTOWN_POLICE, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_DOWNTOWN_POLICE)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : AC_DOWNTOWN_POLICE check")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// road leading up to gov facility
	// B*1395206 - 
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<2591.626465,-268.893951,111.885857>>, <<2591.331055,-614.435547,55.369205>>, 70.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : gov facility road check")
		RETURN TRUE
	ENDIF
	
	//2025599 - Pilot School Area
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1112.586182,-2674.766846,12.568037>>, <<-1215.468140,-2854.588623,19.445877>>, 41.2500)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : Pilot School Area")
		RETURN TRUE
	ENDIF
	
	// B*1392187 - mountainous area leading to base jump in Raton Canyon
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-804.050049,4216.411621,204.487198>>, <<-509.067871,4135.190430,123.250168>>, 250.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : mountainous area Raton Canyon base jump")
		RETURN TRUE
	ENDIF
	
	// B*1533798 - West side Mount Chiliad
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-148.877655,4862.203613,305.644196>>, <<454.627441,5573.104004,804.096985>>, 250.000000)	// majority West from top of cable cart building down
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-482.893066,4990.255371,155.160110>>, <<7.830751,5009.370605,430.760376>>, 250.000000)		// minor West following down from previous check to lower ground
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : Mount Chiliad West")
		RETURN TRUE
	ENDIF
	
	// B*1533798 - East side Mount Chiliad
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<441.399902,5579.990234,802.513794>>, <<965.777588,5675.920898,601.264648>>, 250.000000)		// East from top of cable cart building down
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<954.113892,5641.050781,646.505432>>, <<2140.375000,5377.753418,149.122086>>, 250.000000)	// majority East following down from previous check to lower ground
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<2117.347656,5377.259277,173.329727>>, <<2439.933838,5297.444824,62.686623>>, 100.000000)	// minor East following down from previous check to lower ground	
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<2393.347412,5321.579590,107.062424>>, <<2523.945557,5124.745605,41.683842>>, 70.000000)		// minor East following down from previous check to base		
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : Mount Chiliad East")
		RETURN TRUE
	ENDIF
	
	//B*1485093 - Port of LS heist Setup area - restricted by gates
	IF TAXI_IS_COORD_IN_RANGE_OF_COORD(vPosition, << -99.68751, -2448.89111, 5.01731 >>, 230.0)	// ensure checks only happen if player is within range of the area
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<85.054482,-2511.883789,-2.996267>>, <<-57.599766,-2412.716309,15.000947>>, 75.000000)	// East section heading down the side of the gates		
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<7.516524,-2546.740967,1.331557>>, <<-177.326843,-2417.046631,19.160444>>, 80.000000)	// Central area heading from South East gates into area
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-260.035370,-2419.978271,1.399635>>, <<-27.263750,-2423.848389,25.000641>>, 80.000000)	// Waters edge
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-187.425598,-2516.085693,-6.849975>>, <<-186.751801,-2438.148682,25.001602>>, 40.00000)	// South gates
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-73.608131,-2538.563721,-6.989857>>, <<-183.255585,-2465.145020,25.020298>>, 70.000000)	// South area
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : Port of LS heist Setup area")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT bAllowExceptionsForCallingTaxi
		// B*1553064 Floyd's apartment (prevent taxi spawning in the alleyway)
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1171.708618,-1524.659790,23.379686>>, <<-1139.721436,-1573.588989,-1.585432>>, 55.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : Floyd's apartment")
			RETURN TRUE
		ENDIF
	
		// B*1574664 - Hayes Autos (both entrances gated) - covering front (North) entrance
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<476.539398,-1301.547974,44.824577>>, <<499.582245,-1339.024536,26.317030>>, 35.000000)	//covering front (North) entrance
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<496.981628,-1412.255127,43.293907>>, <<497.269012,-1339.363892,26.316479>>, 40.000000)	// covering back (South) entrance
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_TAXI_RESTRICTED_AREA return TRUE : Hayes Autos")
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    perform checks on vSpawnPosition to make sure it's safe for spawning taxi
/// PARAMS:
///    vPlayerCoords - player's position
///    vSpawnPosition - place to check is safe
///    fMaxSpawnDist - max dist vSpawnPosition is allowed to be from vPlayerCoords (jMart said 300m is path loading size
///    fMinSpawnDist - min dist vSpawnPosition has to be from vPlayerCoords
/// RETURNS:
///    TRUE if spawn position is safe
FUNC BOOL IS_SPAWN_POSITION_SAFE_FOR_TAXI(VECTOR vPlayerCoords, VECTOR vSpawnPosition, FLOAT fMaxSpawnDist = 300.0, FLOAT fMinSpawnDist = 20.0)

	// cheaper
	FLOAT fDistSquared = VDIST2(vPlayerCoords, vSpawnPosition)
	
	// check the spawn position isn't too far away
	IF fDistSquared >= (fMaxSpawnDist * fMaxSpawnDist)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_SPAWN_POSITION_SAFE_FOR_TAXI() : return FALSE spawn pos too far away!")
		RETURN FALSE
	ENDIF
	
	// check the spawn position isn't too close to the playe
	IF fDistSquared <= (fMinSpawnDist * fMinSpawnDist)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_SPAWN_POSITION_SAFE_FOR_TAXI() : return FALSE spawn pos to close to player!")
		RETURN FALSE
	ENDIF
	
	// check player won't see it spawn in
	IF IS_SPHERE_VISIBLE(vSpawnPosition, 2.5)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_SPAWN_POSITION_SAFE_FOR_TAXI() : return FALSE sphere at position visible")
		RETURN FALSE
	ENDIF
	
	//check the position isn't obsured
	IF IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vSpawnPosition, <<3,3,3>>)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_SPAWN_POSITION_SAFE_FOR_TAXI() : return FALSE point obsured by mission entity")
		RETURN FALSE
	ENDIF
	
	IF IS_POSITION_IN_TAXI_RESTRICTED_AREA(vSpawnPosition)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_SPAWN_POSITION_SAFE_FOR_TAXI() : return FALSE point in taxi restricted area")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

/// PURPOSE:
///    test if the specific coords are in a special taxi area...
///    basically anywhere where taxi will need to have it's spawn location / drive to position set by script
///    Work this up so you pass in an enum of the special taxi locations which gets set
/// PARAMS:
///    vInCoords - specific position to check
/// RETURNS:
///    TRUE if coords are in a special area
FUNC TAXI_SPECIAL_AREAS_ENUM GET_TAXI_SPECIAL_AREA_AT_COORDS(VECTOR vInCoords)

	// Michael's house - same areas ARE_COORDS_IN_SPECIAL_AREA
	IF ARE_COORDS_IN_3D_AREA(vInCoords, <<-874.9833, 125.7645,-100>>, <<-768.625366,193.029709,100>>)	
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREAS_MICHAEL_HOUSE")
		RETURN TAXI_SPECIAL_AREAS_MICHAEL_HOUSE
	ENDIF
	
	//Del Perro Pier
	IF IS_COORD_IN_RANGE_OF_COORD_2D(vInCoords, << -1693.30176, -1109.13049, 17.89778 >>, 240.0)
		// main area of the pier
		IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1538.980591,-941.132324,10.566226>>, <<-1715.570313,-1139.765869,52.203613>>, 110.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREAS_DEL_PERRO_PIER")
			RETURN TAXI_SPECIAL_AREAS_DEL_PERRO_PIER
		ENDIF
		// walk way leading to end of the pier
		IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1739.989014,-1115.973022,10.087262>>, <<-1800.514893,-1187.179810,52.017200>>, 25.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREAS_DEL_PERRO_PIER")
			RETURN TAXI_SPECIAL_AREAS_DEL_PERRO_PIER
		ENDIF
		// end of the pier
		IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1801.766357,-1180.251709,4.017236>>, <<-1857.717651,-1244.628418,38.496529>>, 85.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREAS_DEL_PERRO_PIER")
			RETURN TAXI_SPECIAL_AREAS_DEL_PERRO_PIER
		ENDIF
	ENDIF
	
	// Armenian 2 - Melanoma St. Vespucci Beach - dead end alleyway, also covers the dead end road which the alley way is on
	IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1140.889648,-1573.134888,-6.566939>>, <<-1067.084351,-1675.975586,23.531528>>, 52.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREAS_ARMENIAN_2_ALLEYWAY")
		RETURN TAXI_SPECIAL_AREAS_ARMENIAN_2_ALLEYWAY
	ENDIF
	
	// Military Base - don't do the more expensive check if vInCoords is no where near the area
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MILITARY_BASE, vInCoords)
		
		IF IS_COORD_IN_SPECIFIED_AREA(vInCoords, AC_MILITARY_BASE)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREA_MILITARY_BASE")
			RETURN TAXI_SPECIAL_AREA_MILITARY_BASE
		ENDIF
	ENDIF
	
	// Martin Madrazo's ranch (prevents taxi spawning around the stables)
	IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<1324.171509,1110.747925,99.654930>>, <<1523.096802,1110.473389,132.885864>>, 170.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREA_MARTIN_RANCH")
		RETURN TAXI_SPECIAL_AREA_MARTIN_RANCH
	ENDIF
	
	// B*1553064 Floyd's apartment (prevent taxi spawning in the alleyway)
	IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1171.708618,-1524.659790,23.379686>>, <<-1139.721436,-1573.588989,-1.585432>>, 55.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPECIAL_AREA_AT_COORDS return TRUE : TAXI_SPECIAL_AREA_FLOYD_APARTMENT")
		RETURN TAXI_SPECIAL_AREA_FLOYD_APARTMENT
	ENDIF

	RETURN TAXI_SPECIAL_AREAS_INVALID
ENDFUNC

/// PURPOSE:
///    Returns a safe spawn position for a taxi who need to navigate to the the specified special area
/// PARAMS:
///    vPlayerCoords - player's position to use in safe checks
///    eSpecialArea - the area the taxi is going to be navigating to
///    vReturnSpawnCoords - reference to spawn position
///    fReturnSpawnHeading - reference to spawn heading
/// RETURNS:
///    TRUE if spawn info was set successfully
FUNC BOOL GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA(VECTOR vPlayerCoords, TAXI_SPECIAL_AREAS_ENUM eSpecialArea, VECTOR &vReturnSpawnCoords, FLOAT &fReturnSpawnHeading)
	
	VECTOR vTempCoords
	FLOAT fTempHeading
	INT i, iVariations
	
	IF eSpecialArea = TAXI_SPECIAL_AREAS_MICHAEL_HOUSE
		i = 0
		iVariations = 5
		REPEAT iVariations i
			IF i = 0
				vTempCoords = <<-883.8992, 76.3237, 50.9372>>
				fTempHeading = 271.8234		// South West
			ELIF i = 1
				vTempCoords = <<-838.2045, 88.1067, 51.3396>>
				fTempHeading = 102.3721		// South East
			ELIF i = 2
				vTempCoords = <<-884.0715, 217.2857, 71.7633>>
				fTempHeading = 254.5437		// North West
			ELIF i = 3
				vTempCoords = <<-834.0018, 217.2782, 73.1540>>
				fTempHeading = 80.7037		// North East
			ELIF i = 4
				vTempCoords = <<-830.9222, 135.3548, 58.1352>>
				fTempHeading = 92.9570		// Road running along south boarder of house
			ENDIF
			
			IF IS_SPAWN_POSITION_SAFE_FOR_TAXI(vPlayerCoords, vTempCoords)
				vReturnSpawnCoords = vTempCoords
				fReturnSpawnHeading = fTempHeading
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA() : TAXI_SPECIAL_AREAS_MICHAEL_HOUSE : return TRUE Variation = ", i,
										" vReturnSpawnCoords = ", vReturnSpawnCoords, " fReturnSpawnHeading = ", fReturnSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	
	ELIF eSpecialArea =  TAXI_SPECIAL_AREAS_DEL_PERRO_PIER
		i = 0
		iVariations = 6	
		REPEAT iVariations i
			IF i = 0
				vTempCoords = <<-1587.0962, -928.5875, 14.3046>>
				fTempHeading = 139.9023		// towards bottom of the bridge
			ELIF i = 1
				vTempCoords = <<-1532.4579, -863.6670, 21.6188>>
				fTempHeading = 139.9028		// browe of the hill on the bridge
			ELIF i = 2
				vTempCoords = <<-1472.8877, -733.0396, 24.0746>>
				fTempHeading = 236.3756		// north west of entrance sign
			ELIF i = 3
				vTempCoords = <<-1397.3027, -781.1235, 19.3347>>
				fTempHeading = 48.1876		// south east of entrance sign
			ELIF i = 4
				vTempCoords = <<-1605.4087, -1012.5778, 12.0175>>
				fTempHeading = 51.1191		// on the pier close to car park
			ELIF i = 5
				vTempCoords = <<-1572.4573, -1007.1061, 12.0184>>
				fTempHeading = 141.2024		// on the pier car park
			ENDIF
			
			IF IS_SPAWN_POSITION_SAFE_FOR_TAXI(vPlayerCoords, vTempCoords)
				vReturnSpawnCoords = vTempCoords
				fReturnSpawnHeading = fTempHeading
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA() : TAXI_SPECIAL_AREAS_DEL_PERRO_PIER : return TRUE Variation = ", i,
										" vReturnSpawnCoords = ", vReturnSpawnCoords, " fReturnSpawnHeading = ", fReturnSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	ELIF eSpecialArea = TAXI_SPECIAL_AREAS_ARMENIAN_2_ALLEYWAY
		i = 0
		iVariations = 4	
		REPEAT iVariations i
			IF i = 0
				vTempCoords = <<-1126.5519, -1530.5911, 3.2940>>
				fTempHeading = 214.5477		// road leading onto dead end road North
			ELIF i = 1
				vTempCoords = <<-1043.0194, -1644.1719, 3.4894>>
				fTempHeading = 35.3163		// road leading onto dead end road South
			ELIF i = 2
				vTempCoords = <<-1039.3455, -1532.5812, 4.1467>>
				fTempHeading = 32.1734		// block away South
			ELIF i = 3
				vTempCoords = <<-1067.5765, -1501.9440, 4.0327>>
				fTempHeading = 216.1659		// block away North
			ENDIF

			IF IS_SPAWN_POSITION_SAFE_FOR_TAXI(vPlayerCoords, vTempCoords)
				vReturnSpawnCoords = vTempCoords
				fReturnSpawnHeading = fTempHeading
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA() : TAXI_SPECIAL_AREAS_ARMENIAN_2_ALLEYWAY : return TRUE Variation = ", i,
										" vReturnSpawnCoords = ", vReturnSpawnCoords, " fReturnSpawnHeading = ", fReturnSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	ELIF eSpecialArea = TAXI_SPECIAL_AREA_MILITARY_BASE
		i = 0
		iVariations = 4	
		REPEAT iVariations i
			IF i = 0
				vTempCoords = <<-1247.2906, 2549.5300, 16.4214>>
				fTempHeading = 137.1185		// just East of main bridge entrance
			ELIF i = 1
				vTempCoords = <<-1314.1100, 2470.6770, 23.3185>>
				fTempHeading = 309.2187		// just West of main bridge entrance
			ELIF i = 2
				vTempCoords = <<-1095.3818, 2686.4868, 18.5919>>
				fTempHeading = 130.0954		// East of main bridge behind building
			ELIF i = 3
				vTempCoords = <<-1373.5143, 2429.9441, 26.7492>>
				fTempHeading = 298.9626		// West to the left of main bridge entrance
			ENDIF
			
			IF IS_SPAWN_POSITION_SAFE_FOR_TAXI(vPlayerCoords, vTempCoords)
				vReturnSpawnCoords = vTempCoords
				fReturnSpawnHeading = fTempHeading
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA() : TAXI_SPECIAL_AREA_MILITARY_BASE : return TRUE Variation = ", i,
										" vReturnSpawnCoords = ", vReturnSpawnCoords, " fReturnSpawnHeading = ", fReturnSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	ELIF eSpecialArea = TAXI_SPECIAL_AREA_MARTIN_RANCH
		i = 0
		iVariations = 5
		REPEAT iVariations i
			IF i = 0
				vTempCoords = <<1299.1980, 1164.3418, 105.3728>>
				fTempHeading = 3.1765		// close to north gate heading north along road
			ELIF i = 1
				vTempCoords = <<1304.8314, 1084.5979, 104.6453>>
				fTempHeading = 8.2195		// close to south gate heading north along road
			ELIF i = 2
				vTempCoords = <<1290.8727, 1237.6844, 107.4217>>
				fTempHeading = 186.5802		// close to north gate heading south along road
			ELIF i = 3
				vTempCoords = <<1310.9749, 1001.1377, 104.9354>>
				fTempHeading = 359.3654 	// close to south gate heading south along road
			ELIF i = 4
				vTempCoords = <<1284.9357, 1355.7482, 103.4301>>
				fTempHeading = 182.4582		// over brow of the hill to the north along the road
			ENDIF
			
			IF IS_SPAWN_POSITION_SAFE_FOR_TAXI(vPlayerCoords, vTempCoords)
				vReturnSpawnCoords = vTempCoords
				fReturnSpawnHeading = fTempHeading
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA() : TAXI_SPECIAL_AREA_MARTIN_RANCH : return TRUE Variation = ", i,
										" vReturnSpawnCoords = ", vReturnSpawnCoords, " fReturnSpawnHeading = ", fReturnSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELIF eSpecialArea = TAXI_SPECIAL_AREA_FLOYD_APARTMENT
		i = 0
		iVariations = 5
		REPEAT iVariations i
			IF i = 0
				vTempCoords = <<-1208.6174, -1410.7325, 3.1853>>
				fTempHeading = 215.8772		// north up a block main road
			ELIF i = 1
				vTempCoords = <<-1172.9000, -1412.3802, 3.6078>>
				fTempHeading = 126.9322		// North east up a block turn
			ELIF i = 2
				vTempCoords = <<-1112.8418, -1437.8954, 4.0310>>
				fTempHeading = 211.4814		// East block away behind junc to the north
			ELIF i = 3
				vTempCoords = <<-1079.8854, -1472.8779, 4.0784>> 
				fTempHeading = 32.9476	 	// East block away behind junc to the south
			ELIF i = 4
				vTempCoords = <<-1086.7677, -1540.7067, 3.5549>> 
				fTempHeading = 128.3527		// South
			ENDIF
			
			IF IS_SPAWN_POSITION_SAFE_FOR_TAXI(vPlayerCoords, vTempCoords)
				vReturnSpawnCoords = vTempCoords
				fReturnSpawnHeading = fTempHeading
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_TAXI_SPAWN_INFO_FOR_TAXI_SPECIAL_AREA() : TAXI_SPECIAL_AREA_FLOYD_APARTMENT : return TRUE Variation = ", i,
										" vReturnSpawnCoords = ", vReturnSpawnCoords, " fReturnSpawnHeading = ", fReturnSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_HAIL_ANIMS_AND_DIALOGUE()
	SEQUENCE_INDEX seq
	BOOL bFlag = IS_AMBIENT_SPEECH_DISABLED(PLAYER_PED_ID())
	// flag down taxi - but it refuses to pick up player
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	
	IF NOT (g_playerIsDrunk)
		
		ANIM_DATA none
		ANIM_DATA animDataBlend
		INT iAnimFlags = 0
		
		// B*1991223 - first person specific hail anim
		IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
			animDataBlend.type = APT_SINGLE_ANIM
			animDataBlend.anim0 = "FP_HAIL_TAXI"
			animDataBlend.dictionary0 = "TAXI_HAIL"
			animDataBlend.phase0 = 0.0
			animDataBlend.rate0 = 1.0
			animDataBlend.filter = GET_HASH_KEY("UpperbodyAndIk_filter")
			iAnimFlags += ENUM_TO_INT(AF_UPPERBODY)
			iAnimFlags += ENUM_TO_INT(AF_SECONDARY)
			iAnimFlags += ENUM_TO_INT(AF_EXIT_AFTER_INTERRUPTED)
			animDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)
			animDataBlend.ikFlags = AIK_USE_FP_ARM_LEFT
		ELSE
			animDataBlend.type = APT_SINGLE_ANIM
			animDataBlend.anim0 = "HAIL_TAXI"
			animDataBlend.dictionary0 = "TAXI_HAIL"
			animDataBlend.phase0 = 0.0
			animDataBlend.rate0 = 1.0
			animDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_L_ARM")
			//iAnimFlags += ENUM_TO_INT(AF_UPPERBODY)	// don't use this flag since it makes the spine wobble
			iAnimFlags += ENUM_TO_INT(AF_SECONDARY)
			iAnimFlags += ENUM_TO_INT(AF_EXIT_AFTER_INTERRUPTED)
			animDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)
		ENDIF
		
		WEAPON_TYPE equippedWeapon
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),equippedWeapon)
				
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
		OR ((ARE_STRINGS_EQUAL(animDataBlend.anim0,"FP_HAIL_TAXI")) AND (equippedWeapon = WEAPONTYPE_PETROLCAN))//B*2132396
			IF equippedWeapon != WEAPONTYPE_PETROLCAN OR NOT ARE_STRINGS_EQUAL(animDataBlend.anim0,"FP_HAIL_TAXI")
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_KeepWeaponHolsteredUnlessFired, TRUE)
			ENDIF
			OPEN_SEQUENCE_TASK(seq)				
				TASK_SWAP_WEAPON(NULL, FALSE)
				TASK_SCRIPTED_ANIMATION(NULL, animDataBlend, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
			CLEAR_SEQUENCE_TASK(seq)
		ELSE
			TASK_SCRIPTED_ANIMATION(PLAYER_PED_ID(), animDataBlend, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
		ENDIF
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " PLAY_HAIL_ANIMS_AND_DIALOGUE - applied new scripted anim task * : ", animDataBlend.anim0)
	ELSE
		IF (bFlag)
			STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
		ENDIF
		PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "TAXI_HAIL_DRUNK", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
		IF (bFlag)
			STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_TO_RIGHT_OF_CAR(VEHICLE_INDEX inCar)
	VECTOR pos, pos2, pos3
	VECTOR vec1, vec2
	
	IF NOT IS_ENTITY_DEAD(inCar)
		
		pos = GET_ENTITY_COORDS(inCar)
		pos2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(inCar, <<1.0, 0.0, 0.0>>)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			pos3 = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		vec1 = pos2 - pos
		vec2 = pos3 - pos
		
		IF (GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y) < 90.0)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC INT GET_NEAREST_PASSENGER_SIDE(VEHICLE_INDEX ThisCar)
	
	VECTOR char_pos
	VECTOR right_pos
	VECTOR left_pos
	VECTOR right_vec
	VECTOR left_vec
	CONST_INT LEFT_PASSENGER 1
	CONST_INT RIGHT_PASSENGER 2
	
	IF IS_VEHICLE_DRIVEABLE(ThisCar)
		
		// get player coords
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			char_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ELSE
			RETURN(-1)
		ENDIF

		// right side
		right_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisCar, <<1.0, -0.5, 0.0>>)
		right_vec = right_pos - char_pos

		// left side
		left_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisCar, <<-1.0, -0.5, 0.0>>)
		left_vec = left_pos - char_pos 

		// choose the smaller of the two
		IF VMAG(right_vec)  < VMAG(left_vec)
			RETURN(RIGHT_PASSENGER)
		ELSE
			RETURN(LEFT_PASSENGER)
		ENDIF

	ELSE
		RETURN(-1)
	ENDIF

ENDFUNC

/// PURPOSE:
///    gets which side vCoordsToCheck is to entityIndex
///    Used to decide which side the player is to the taxi for entry seat selection
///    NOTE: doesn't check entity is alive
/// PARAMS:
///    entityIndex - the entity to check against
///    vEntityCoords - the entities coords
///    vCoordsToCheck - the coords we want to check
/// RETURNS:
///    Int depicting the side of entityIndex the vCoordsToCheck is.  0 = Left side, 1 = Right side, -1 = directly in line
FUNC INT TAXI_GET_SIDE_COORDS_IS_TO_ENTITY(ENTITY_INDEX entityIndex, VECTOR vEntityCoords, VECTOR vCoordsToCheck)
	FLOAT fSide
	VECTOR vPlaneNorm
	vPlaneNorm = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, << 3.0, 0.0, 0.0 >>) - vEntityCoords)	
	fSide = DOT_PRODUCT(vCoordsToCheck - vEntityCoords, vPlaneNorm)
	/*#IF IS_DEBUG_BUILD
		VECTOR vTemp
		vTemp = GET_ENTITY_FORWARD_VECTOR(entityIndex) * 60.0
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(entityIndex) + vTemp, GET_ENTITY_COORDS(entityIndex) - vTemp, 255, 0, 0, 150)
		//CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : GET_SIDE_COORDS_IS_TO_ENTITY : vEntityCoords = ", vEntityCoords, " vCoordsToCheck = ", vCoordsToCheck,
		//													" vPlaneNorm = ", vPlaneNorm, " dot returning : ", fSide)
	#ENDIF*/
	IF (fSide < 0)
    	RETURN 0	//left side
	ELIF (fSide > 0)
		RETURN 1	//right side
	ENDIF
	RETURN -1	// directly in line
ENDFUNC

/// PURPOSE:
///    Extra checks need for title update (MP) - added as new func so no changes are made to ARE_COORDS_IN_SPECIAL_AREA
///    since this is referenced in SP pushes the patch size up
/// PARAMS:
///    vInCoords - position to test
///    ReturnCoord - returned drop off position
///    ReturnHeading - returned heading
///    iMpTaxi - taxi's index
/// RETURNS:
///    TRUE if position was in a special area
FUNC BOOL ARE_COORDS_IN_SPECIAL_AREA_TITLE_UPDATE(VECTOR vInCoords, VECTOR &ReturnCoord, FLOAT &ReturnHeading, INT iMpTaxi = 1)
	//INT iVariation
	VECTOR vTempInCoords
	
	// Impound (see B*1603797) - note same area as Tow Truck Oddjob check in ARE_COORDS_IN_SPECIAL_AREA
	// however this area is better and the returnCoord isn't inside the compound
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 28.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<400.987091,-1659.755981,26.916147>>, <<437.656616,-1617.478882,48.341854>>, 45.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA_TITLE_UPDATE return TRUE : Impound")	
		// near entrance
		IF iMpTaxi < 4
			ReturnCoord = <<413.7399, -1608.9109, 28.1610>>
			ReturnHeading = 238.2568
		ELSE
		// around corner near exit
			ReturnCoord = <<416.8107, -1666.5142, 28.1758>>
			ReturnHeading = 144.8478		
		ENDIF
		RETURN TRUE		
	ENDIF
	
	//Pilot School
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1112.586182,-2674.766846,12.568037>>, <<-1215.468140,-2854.588623,19.445877>>, 41.2500)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA_TITLE_UPDATE return TRUE : Pilot School")	
		IF iMpTaxi < 4
			ReturnCoord = <<-1094.4923, -2662.2698, 12.6066>>
			ReturnHeading = 196.0365
		ELSE
			ReturnCoord = <<-1076.7362, -2684.0393, 12.8070>>
			ReturnHeading = 211.7278
		ENDIF
		RETURN TRUE		
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_COORDS_IN_SPECIAL_AREA(VECTOR vInCoords, VECTOR &ReturnCoord, FLOAT &ReturnHeading, INT iMpTaxi = 1)
	INT iVariation
	VECTOR vTempInCoords
	
	// these cover the custom waypoint 1.0 z value issue so don't need vTempInCoords settings
	
	// B*1948116 - Over the ocean where the yankton map nodes would be
	IF ARE_COORDS_IN_3D_AREA(vInCoords, <<1900.0, -6600.0, 100>>, <<6900, -3600, 140>>)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Yankton Map")
		ReturnCoord = << 1261.5804, -3332.6846, 5.4191 >> 
		ReturnHeading = 163.87	
		RETURN(TRUE)
		
	// B*1948116 - Over the ocean the north west corner
	ELIF ARE_COORDS_IN_3D_AREA(vInCoords, <<-3800.0, 5400, -100>>, <<-1900, 8500, 100>>)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : North West Pacific")
		ReturnCoord = << -1577.1599, 5166.5103, 19.1864 >> 
		ReturnHeading = 0.0
		RETURN(TRUE)
		
	// these cover the custom waypoint 1.0 z value issue so don't need vTempInCoords settings
	// Michael's house
	ELIF ARE_COORDS_IN_3D_AREA(vInCoords, <<-874.9833, 125.7645,-100>>, <<-768.625366,193.029709,100>>)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Michael's house")
		ReturnCoord = << -856.6151, 163.7361, 65.0930 >> 
		ReturnHeading = 355.3355		
		RETURN(TRUE)
		
	// Franklin's house
	ELIF ARE_COORDS_IN_3D_AREA(vInCoords, << -39.2005, -1473.5498,-100>>, << 10.6133, -1430.0988,100>>)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Franklin's house")
		ReturnCoord = << -15.5181, -1456.3999, 29.4244 >>
		ReturnHeading = 94.6893		
		RETURN(TRUE)
		
	// Trevor's trailer
	ELIF ARE_COORDS_IN_3D_AREA(vInCoords, <<1964.742065,3792.590088,-100>>, <<1992.636108,3840.253174,100>>)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Trevor's trailer")
		ReturnCoord = <<1996.3718, 3818.8308, 31.1612>>
		ReturnHeading = 170.0221		
		RETURN(TRUE)
		
	// Armenian Garage
	ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-68.218704,-1086.951050,-100>>, <<-10.545177,-1106.895264,100>>, 43.500000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Armenian Garage")
		ReturnCoord = << -63.5854, -1074.7321, 26.0995 >> 
		ReturnHeading = 324.1257		
		RETURN(TRUE)
		
	// Tow Truck Oddjob
	ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<398.789856,-1651.417358,-100>>, <<423.837250,-1619.432495,100>>, 45.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Tow Truck Oddjob")
		ReturnCoord = << 416.8710, -1613.0814, 28.1401 >>
		ReturnHeading = 227.9333		
		RETURN(TRUE)
		
	// Multistorey at airport 1
	ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-988.853516,-2553.177490,-100>>, <<-1041.486450,-2644.969971,100>>, 49.750000)
	AND NOT NETWORK_IS_GAME_IN_PROGRESS()	//Done Further Down for MP
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Multistorey at airport 1 \n")
		#endif 
		ReturnCoord = << -1039.5425, -2574.4670, 12.7566 >>
		ReturnHeading = 162.2744
		
		RETURN(TRUE)
		
	// Multistorey at airport 2
	ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-906.902039,-2600.460693,-100>>, <<-959.765137,-2692.057129,100>>, 49.750000)
	AND NOT NETWORK_IS_GAME_IN_PROGRESS()	//Done Further Down for MP
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Multistorey at airport 2 \n")
		#endif 
		ReturnCoord = << -911.1597, -2676.1316, 12.7567 >>
		ReturnHeading = 338.9414
		
		RETURN(TRUE)
	
	ENDIF
	
	// Family 4
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 28.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-209.337189,-2011.099487,26.620369>>, <<-257.222473,-2076.963135,36.620373>>, 30.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Stadium")
		ReturnCoord = <<-211.8546, -2030.7715, 26.6204>>
		ReturnHeading = 154.4302		
		RETURN(TRUE)
	ENDIF
	
	// B*1392187 - mountainous area leading to base jump in Raton Canyon - keep in sync with IS_POSITION_IN_TAXI_RESTRICTED_AREA check and used for closest minigame STATIC_BLIP_AMBIENT_BASEJUMP_ROCK_CLIFF
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 159.0	// note higher position than drop off coords
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-804.050049,4216.411621,204.487198>>, <<-509.067871,4135.190430,123.250168>>, 250.000000)		
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : mountainous area Raton Canyon base jump vTempInCoords : ", vTempInCoords)
		ReturnCoord = <<-623.3622, 3996.0000, 120.7669>>
		ReturnHeading = 37.8784
		RETURN TRUE
	ENDIF
		
	// Kortz Museum
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 180.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-2203.167480,183.354950,167.402191>>, <<-2311.587402,434.382813,195.466919>>, 138.500000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Kortz Museum")	
		ReturnCoord = <<-2294.9446, 376.2506, 173.4669>>
		ReturnHeading = 296.6963
		
		RETURN(TRUE)
	ENDIF
	
	// Pacific bluffs, covering beach nodes and cycle path, length of the row of houses.  Also covers Shrink's Office	
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 10.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-2041.478027,-553.215942,-0.089962>>, <<-1787.080078,-768.088562,37.999176>>, 220.000000)
	
		// Shrink's Office (initial values set
		ReturnCoord = <<-1897.0765, -557.3334, 10.7279>>
		ReturnHeading = 228.7709
			
		IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1882.883179,-541.744202,4.675224>>, <<-1929.626465,-598.602600,40.429798>>, 35.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Shrink's Office")			
			// values arlready set above
			RETURN(TRUE)
			
		// Pacific bluffs, covering beach
		ELSE			
			// get closest safe dropoff point - all on the highway in front of the houses from North to South
			VECTOR vDropOff[6]
			vDropOff[0] = <<-1962.4679, -503.4229, 10.8349>>
			vDropOff[1] = <<-1924.4722, -534.7357, 10.8181>>
			vDropOff[2] = <<-1896.3240, -557.7680, 10.7256>>
			vDropOff[3] = <<-1850.6611, -595.8367, 10.4649>>
			vDropOff[4] = <<-1808.9995, -632.1207, 10.0016>>
			vDropOff[5] = <<-1745.0428, -694.4053, 9.1245>>
			INT iClosest = -1
			FLOAT fClosestDist = 99999.0
			FLOAT fDist
			INT i
			FOR i = 0 TO 5
				fDist = VDIST(vDropOff[i], vTempInCoords)
				IF (fDist < fClosestDist)
					fClosestDist = fDist
					iClosest = i
				ENDIF
			ENDFOR
			// set the position
			ReturnCoord = vDropOff[iClosest]
			// set the heading
			IF iClosest = 0
				ReturnHeading = 234.3999
			ELIF iClosest = 1
				ReturnHeading = 232.2255
			ELIF iClosest = 2
				ReturnHeading = 228.2855
			ELIF iClosest = 3
				ReturnHeading = 231.4914
			ELIF iClosest = 4
				ReturnHeading = 230.6703
			ELIF iClosest = 5
				ReturnHeading = 228.7709
			ENDIF
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Pacific bluffs beach side iClosest = ", iClosest)	
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	// Observatory	
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 330.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-446.119995,1057.451538,318.616852>>, <<-405.833618,1194.486450,337.096558>>, 106.250000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Observatory")	
		ReturnCoord = <<-411.3629, 1174.5865, 324.6421>>
		ReturnHeading = 255.2881		
		RETURN(TRUE)
	ENDIF
	
	// Golf Club
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 20.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1362.090576,-29.267771,-100>>, <<-1377.953125,152.238876,100>>, 50.0)
		iVariation = iMpTaxi%8
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Golf Club : iVariation = ", iVariation)	
		SWITCH iVariation
			CASE 1
				ReturnCoord = <<-1380.1006, 95.1566, 53.5054>>
				ReturnHeading = 4.5550
			BREAK
			CASE 2
				ReturnCoord = <<-1379.1394, 84.2472, 53.0592>>
				ReturnHeading = 6.9278
			BREAK
			CASE 3
				ReturnCoord = <<-1378.4266, 74.5077, 52.6622>>
				ReturnHeading = 2.6813
			BREAK
			CASE 4
				ReturnCoord = <<-1384.1774, 73.9207, 52.7438>>
				ReturnHeading = 5.5420
			BREAK
			CASE 5
				ReturnCoord = <<-1381.1179, 35.7838, 52.6590>>
				ReturnHeading = 7.6687
			BREAK
			CASE 6
				ReturnCoord = <<-1383.7413, 64.1491, 52.6647>>
				ReturnHeading = 2.5572
			BREAK
			CASE 7
				ReturnCoord = <<-1382.6162, 49.6876, 52.6585>>
				ReturnHeading = 3.0185
			BREAK	
			DEFAULT
				ReturnCoord = <<-1383.2987, 54.4598, 52.6562>>
				ReturnHeading = 6.2155
			BREAK
		ENDSWITCH
		
		RETURN(TRUE)
		
	// Main Gun Shop - Ammunation
	ELIF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<48.310059,-1120.669434,28.030502>>, <<-12.123452,-1112.084106,36.085670>>, 60.0)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Main Gun Shop \n")
			#endif 
			
			iVariation = iMpTaxi%8
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTLN("> - ARE_COORDS_IN_SPECIAL_AREA - iVariation = ", iVariation) PRINTNL()
			#endif 
			
			SWITCH iVariation
				CASE 1
					ReturnCoord = <<50.9889, -1092.5254, 28.2152>>
					ReturnHeading = 150.8833
				BREAK
				CASE 2
					ReturnCoord = <<46.7725, -1099.7506, 28.1639>>
					ReturnHeading = 149.4322
				BREAK
				CASE 3
					ReturnCoord = <<41.6146, -1107.7419, 28.1318>>
					ReturnHeading = 154.1378
				BREAK
				CASE 4
					ReturnCoord = <<38.1922, -1113.3936, 28.1507>>
					ReturnHeading = 146.3778
				BREAK
				CASE 5
					ReturnCoord = <<13.0436, -1124.7668, 27.7359>>
					ReturnHeading = 91.1630
				BREAK
				CASE 6
					ReturnCoord = <<1.4336, -1125.0425, 27.1705>>
					ReturnHeading = 91.0578
				BREAK
				CASE 7
					ReturnCoord = <<-11.1811, -1125.8105, 26.3688>>
					ReturnHeading = 91.8660
				BREAK
				DEFAULT
					ReturnCoord = <<21.9431, -1124.5731, 27.9417>>
					ReturnHeading = 91.6961
				BREAK
			ENDSWITCH
			
			RETURN(TRUE)
			
		// Airport - Upper Taxi Rank FIRST
		ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1061.018066,-2540.235107,18.669323>>, <<-1023.332458,-2562.114502,31.576157>>, 250.0)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Airport - Upper Taxi Rank FIRST \n")
			#endif 
			
			iVariation = iMpTaxi%8
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTLN("> - ARE_COORDS_IN_SPECIAL_AREA - iVariation = ", iVariation) PRINTNL()
			#endif 
			
			SWITCH iVariation
				CASE 1
					ReturnCoord = <<-1023.9825, -2489.8818, 19.0755>>
					ReturnHeading = 148.7004
				BREAK
				CASE 2
					ReturnCoord = <<-1029.9678, -2500.4377, 19.0803>>
					ReturnHeading = 148.7144
				BREAK
				CASE 3
					ReturnCoord = <<-1070.5457, -2570.8594, 19.0836>>
					ReturnHeading = 150.0955
				BREAK
				CASE 4
					ReturnCoord = <<-1056.0647, -2545.6619, 19.0809>>
					ReturnHeading = 149.5649
				BREAK
				CASE 5
					ReturnCoord = <<-1050.5780, -2536.2185, 19.0824>>
					ReturnHeading = 150.7363
				BREAK
				CASE 6
					ReturnCoord = <<-1036.0316, -2510.9021, 19.0794>>
					ReturnHeading = 147.2113
				BREAK
				CASE 7
					ReturnCoord = <<-1044.4872, -2525.5342, 19.0790>>
					ReturnHeading = 150.7597
				BREAK
				DEFAULT
					ReturnCoord = <<-1064.5002, -2560.6528, 19.0905>>
					ReturnHeading = 150.7081
				BREAK
			ENDSWITCH
			RETURN(TRUE)
			
		// Airport - Lower Taxi Rank FIRST
		ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1061.058716,-2540.253662,10.944668>>, <<-1023.296814,-2562.052490,18.516848>>, 250.0)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Airport - Lower Taxi Rank FIRST \n")
			#endif 
			
			iVariation = iMpTaxi%8
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTLN("> - ARE_COORDS_IN_SPECIAL_AREA - iVariation = ", iVariation) PRINTNL()
			#endif 
			
			SWITCH iVariation
				CASE 1
					ReturnCoord = <<-1044.7413, -2528.7859, 12.7568>>
					ReturnHeading = 150.9439
				BREAK
				CASE 2
					ReturnCoord = <<-1050.7631, -2539.4978, 12.7566>>
					ReturnHeading = 150.8510
				BREAK
				CASE 3
					ReturnCoord = <<-1054.9523, -2546.8596, 12.7566>>
					ReturnHeading = 149.9285
				BREAK
				CASE 4
					ReturnCoord = <<-1060.3972, -2556.8979, 12.6066>>
					ReturnHeading = 150.8244
				BREAK
				CASE 5
					ReturnCoord = <<-1020.7437, -2490.0842, 12.6396>>
					ReturnHeading = 148.6134
				BREAK
				CASE 6
					ReturnCoord = <<-1076.9043, -2589.1794, 12.6858>>
					ReturnHeading = 149.0112
				BREAK
				CASE 7
					ReturnCoord = <<-1026.3866, -2501.9521, 12.6923>>
					ReturnHeading = 149.7553
				BREAK
				DEFAULT
					ReturnCoord = <<-1070.5065, -2578.3892, 12.6932>>
					ReturnHeading = 148.5232
				BREAK
			ENDSWITCH
			
			RETURN(TRUE)
			
		// Airport - Upper Taxi Rank SECOND
		ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1043.364136,-2747.290039,16.960567>>, <<-991.536804,-2657.681641,69.123489>>, 200.0)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Airport - Upper Taxi Rank SECOND\n")
			#endif 
			
			iVariation = iMpTaxi%8
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTLN("> - ARE_COORDS_IN_SPECIAL_AREA - iVariation = ", iVariation) PRINTNL()
			#endif 
			
			SWITCH iVariation
				CASE 1
					ReturnCoord = <<-1067.8463, -2708.9397, 19.0588>>
					ReturnHeading = 230.9025
				BREAK
				CASE 2
					ReturnCoord = <<-1058.2238, -2716.2166, 19.0600>>
					ReturnHeading = 237.1285
				BREAK
				CASE 3
					ReturnCoord = <<-1004.5667, -2744.7971, 19.0811>>
					ReturnHeading = 255.6885
				BREAK
				CASE 4
					ReturnCoord = <<-1049.5092, -2721.5801, 19.0546>>
					ReturnHeading = 242.3581
				BREAK
				CASE 5
					ReturnCoord = <<-1017.1544, -2740.1174, 19.0525>>
					ReturnHeading = 243.5798
				BREAK
				CASE 6
					ReturnCoord = <<-1042.0872, -2726.0767, 19.0452>>
					ReturnHeading = 240.2381
				BREAK
				CASE 7
					ReturnCoord = <<-1027.3530, -2734.4617, 19.0509>>
					ReturnHeading = 239.8703
				BREAK
				DEFAULT
					ReturnCoord = <<-1033.7404, -2730.7458, 19.0521>>
					ReturnHeading = 239.9237
				BREAK
			ENDSWITCH
			
			RETURN(TRUE)
			
		// Airport - Lower Taxi Rank SECOND
		ELIF IS_POINT_IN_ANGLED_AREA(vInCoords, <<-1047.646484,-2754.947021,2.941969>>, <<-991.731445,-2653.153320,16.892282>>, 200.0)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Airport - Lower Taxi Rank SECOND\n")
			#endif 
			
			iVariation = iMpTaxi%8
			#IF IS_DEBUG_BUILD
				PRINTSTRING("\n<")
				PRINTSTRING(GET_THIS_SCRIPT_NAME())
				PRINTLN("> - ARE_COORDS_IN_SPECIAL_AREA - iVariation = ", iVariation) PRINTNL()
			#endif 
			
			SWITCH iVariation
				CASE 1
					ReturnCoord = <<-971.9154, -2749.1221, 12.6069>>
					ReturnHeading = 265.8022
				BREAK
				CASE 2
					ReturnCoord = <<-980.6166, -2748.5349, 12.7570>>
					ReturnHeading = 263.5472
				BREAK
				CASE 3
					ReturnCoord = <<-987.6072, -2747.2734, 12.6069>>
					ReturnHeading = 257.2886
				BREAK
				CASE 4
					ReturnCoord = <<-1006.8151, -2739.3450, 12.6334>>
					ReturnHeading = 242.1315
				BREAK
				CASE 5
					ReturnCoord = <<-1051.8304, -2713.5527, 12.6333>>
					ReturnHeading = 239.9312
				BREAK
				CASE 6
					ReturnCoord = <<-1041.0035, -2719.6467, 12.6402>>
					ReturnHeading = 240.1081
				BREAK
				CASE 7
					ReturnCoord = <<-1023.8323, -2729.4646, 12.6445>>
					ReturnHeading = 239.6737
				BREAK
				DEFAULT
					ReturnCoord = <<-1012.8478, -2735.1724, 12.6656>>
					ReturnHeading = 237.6545
				BREAK
			ENDSWITCH    
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	// Gun Shop - East Los Santos
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 20.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<845.721619,-990.747314,37.469604>>, <<846.339722,-1066.880493,16.966232>>, 90.0)		//845.021484,-1045.892090,23.806255>>, <<845.281677,-990.733521,39.362217>>, 65.0)
		iVariation = iMpTaxi%8
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Gun Shop - East Los Santos : iVariation = ", iVariation)		
		SWITCH iVariation
			CASE 1
				ReturnCoord = <<868.1039, -995.9224, 29.7369>>
				ReturnHeading = 93.1320
			BREAK
			CASE 2
				ReturnCoord = <<859.2830, -996.4102, 28.7865>>
				ReturnHeading = 92.2581
			BREAK
			CASE 3
				ReturnCoord = <<849.4288, -997.1062, 27.5347>>
				ReturnHeading = 92.7140
			BREAK
			CASE 4
				ReturnCoord = <<841.1268, -997.4826, 26.5744>>
				ReturnHeading = 92.8628
			BREAK
			CASE 5
				ReturnCoord = <<831.2605, -1009.5560, 25.5990>>
				ReturnHeading = 268.6186
			BREAK
			CASE 6
				ReturnCoord = <<844.6506, -1010.0903, 26.9894>>
				ReturnHeading = 270.0760
			BREAK
			CASE 7
				ReturnCoord = <<852.2498, -1010.1324, 27.8091>>
				ReturnHeading = 269.7220
			BREAK
			DEFAULT
				ReturnCoord = <<861.4028, -1010.0249, 28.8080>>
				ReturnHeading = 270.7686
			BREAK
		ENDSWITCH
		
		RETURN(TRUE)
	ENDIF
	
	// Mod Shop - East Los Santos
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the location
		vTempInCoords.Z = 20.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<767.373596,-1077.428833,-10.786545>>, <<682.828369,-1080.685791,79.673035>>, 60.0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> - ARE_COORDS_IN_SPECIAL_AREA = TRUE - Mod Shop - East Los Santos\n")
		#endif 
		
		iVariation = iMpTaxi%8
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTLN("> - ARE_COORDS_IN_SPECIAL_AREA - iVariation = ", iVariation) PRINTNL()
		#endif
		
		SWITCH iVariation
			CASE 1
				ReturnCoord = <<703.2726, -1067.6997, 21.4765>>
				ReturnHeading = 181.3629
			BREAK
			CASE 2
				ReturnCoord = <<715.2089, -1070.3993, 21.2708>>
				ReturnHeading = 175.43624
			BREAK
			CASE 3
				ReturnCoord = <<715.2911, -1059.4200, 21.0876>>
				ReturnHeading = 181.0347
			BREAK
			CASE 4
				ReturnCoord = <<709.6841, -1086.7800, 21.4190>>
				ReturnHeading = 233.1690
			BREAK
			CASE 5
				ReturnCoord = <<703.8070, -1057.8665, 21.4152>>
				ReturnHeading = 170.1609
			BREAK
			CASE 6
				ReturnCoord = <<708.1994, -1048.1937, 21.2160>>
				ReturnHeading = 134.2729
			BREAK
			CASE 7
				ReturnCoord = <<711.1124, -1069.4229, 21.3129>>
				ReturnHeading = 177.9198
			BREAK
			DEFAULT
				ReturnCoord = <<703.5450, -1078.7177, 21.3987>>
				ReturnHeading = 180.5686
			BREAK
		ENDSWITCH
		
		RETURN(TRUE)
	ENDIF
	
	// Military Base - don't do the more expensive check if vInCoords is no where near the area
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at Lester's house
		vTempInCoords.Z = 31.0
	ENDIF
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MILITARY_BASE, vTempInCoords)
		IF IS_COORD_IN_SPECIFIED_AREA(vTempInCoords, AC_MILITARY_BASE)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Military Base")
			ReturnCoord = <<-1520.1213, 2731.5110, 16.6437>>
			ReturnHeading = 48.1572		
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	// Car Scrap Yard - property	
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 77.2
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<1535.986572,-2041.496948,68.212753>>, <<1529.356567,-2204.463379,96.111946>>, 105.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Car Scrap Yard - property	")		
		ReturnCoord = << 1540.75, -2051.49, 76.85 >>
		ReturnHeading = 255.41		
		RETURN(TRUE)
	ENDIF
	
	// Lester's house
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at Lester's house
		vTempInCoords.Z = 51.1739
	ENDIF
	// large area cover all his road (since nodes are turned off along the whole road)
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<1185.810425,-1768.367188,28.311447>>, <<1369.067993,-1701.451538,66.253891>>, 70.000000, TRUE)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Lester's house")
		// choose closest position:
		// outside Lester House - also used as default position
		IF IS_COORD_IN_RANGE_OF_COORD_2D(vTempInCoords, << 1283.32568, -1731.52222, 51.78555 >>, 17.0)
			ReturnCoord = << 1283.32568, -1731.52222, 51.78555 >>
			ReturnHeading = 275.4274
			RETURN(TRUE)
		ENDIF
		// Heading East From Lesters House up the hill
		IF IS_COORD_IN_RANGE_OF_COORD_2D(vTempInCoords, << 1334.38611, -1709.76245, 55.70131 >>, 41.0)
			ReturnCoord = << 1334.38611, -1709.76245, 55.70131 >>
			ReturnHeading = 275.4411
			RETURN(TRUE)
		ENDIF
		// Heading West From Lesters House down the hill
		IF IS_COORD_IN_RANGE_OF_COORD_2D(vTempInCoords, << 1246.16614, -1750.62463, 45.35691 >>, 28.0)
			ReturnCoord = << 1246.16614, -1750.62463, 45.35691 >>
			ReturnHeading = 301.2981
			RETURN(TRUE)
		ENDIF
		// West From Lesters House bottom of the hill round the bend to the main road
		IF IS_COORD_IN_RANGE_OF_COORD_2D(vTempInCoords, << 1196.27161, -1753.05054, 37.63976 >>, 32.0)
			ReturnCoord = << 1196.27161, -1753.05054, 37.63976 >>
			ReturnHeading = 209.4354
			RETURN(TRUE)
		ENDIF
		
		// default position outside Lester's house		
		ReturnCoord = << 1283.32568, -1731.52222, 51.78555 >>
		ReturnHeading = 275.4274
		RETURN(TRUE)
	ENDIF
	
	//Del Perro Pier
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at Lester's house
		vTempInCoords.Z = 25
	ENDIF	
	IF IS_COORD_IN_RANGE_OF_COORD_2D(vTempInCoords, << -1693.30176, -1109.13049, 17.89778 >>, 240.0)
		// main area of the pier
		IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1538.980591,-941.132324,10.566226>>, <<-1715.570313,-1139.765869,52.203613>>, 110.000000)
		// walk way leading to end of the pier
		OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1739.989014,-1115.973022,10.087262>>, <<-1800.514893,-1187.179810,52.017200>>, 25.000000)
		// end of the pier
		OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1801.766357,-1180.251709,4.017236>>, <<-1857.717651,-1244.628418,38.496529>>, 85.000000)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Del Perro Pier")
			ReturnCoord = <<-1624.4452, -976.9755, 12.0175>>
			ReturnHeading = 141.1670
			RETURN (TRUE)
		ENDIF
	ENDIF
	
	// Armenian 2 - dead end alleyway, also covers the dead end road which the alley way is on
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 10
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-1140.889648,-1573.134888,-6.566939>>, <<-1067.084351,-1675.975586,23.531528>>, 52.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Armenian 2 - dead end alleyway")
		ReturnCoord = << -1095.63, -1577.24, 3.82 >>
		ReturnHeading = 216.12
		RETURN (TRUE)
	ENDIF
	
	// Martin 1 - Martin's ranch (also could be used off mission)
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 114.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<1324.171509,1110.747925,99.654930>>, <<1523.096802,1110.473389,132.885864>>, 170.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Martin's ranch")
		iVariation = 3
		SWITCH GET_RANDOM_INT_IN_RANGE(0, iVariation)
			CASE 0	ReturnCoord = <<1369.2460, 1147.6527, 112.7592>>	ReturnHeading = 182.0998		BREAK	// front of the steps of the house
			CASE 1	ReturnCoord = <<1360.8483, 1139.1210, 112.7592>>	ReturnHeading = 83.3356			BREAK	// front of house offset to North
			CASE 2	ReturnCoord = <<1364.7513, 1154.3668, 112.7592>>	ReturnHeading = 223.2795		BREAK	// front of house offset to South
		ENDSWITCH

		RETURN (TRUE)
	ENDIF
	
	// Trevor's hangar
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 40.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<1737.390137,3287.795410,35.138969>>, <<1724.345215,3337.854004,57.200382>>, 40.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Trevor's hangar")
		ReturnCoord = <<1782.1903, 3300.0759, 40.4593>>	// towards the road since majority are tagged as offroad which taxi can't use
		ReturnHeading = 142.4260
		RETURN (TRUE)
	ENDIF
	
	// Franklin/Michael's helipad area - La Puerta / Shank St
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 7.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-695.798096,-1379.678467,24.481243>>, <<-777.373108,-1491.130005,-3.594945>>, 72.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Franklin/Michael's helipad area - La Puerta / Shank St check")
		ReturnCoord = <<-658.2056, -1388.7886, 9.4990>> 	// on the main road to avoid the road blocks
		ReturnHeading = 174.6945
		RETURN (TRUE)
	ENDIF
	
	// RC - Extreme 3 - top of Maze tower - B*1408629
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 325.0
	ENDIF
	IF VDIST2(vTempInCoords, << -75.59782, -818.60815, 325.17450 >>) < (58.0 * 58.0)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : top of Maze tower")
		ReturnCoord = <<-142.3670, -895.0251, 28.1910>>	// entrance to the tower
		ReturnHeading = 71.6555
		RETURN (TRUE)
	ENDIF
	
	// B*1497949 - Film Studio - drop outside if it isn't open to the player
	IF NOT TAXI_IS_STUDIO_OPEN_FOR_CURRENT_PLAYER()
		vTempInCoords = vInCoords
		IF vInCoords.Z = 1.0
			// because custom waypoints placed on the map don't take Z value into account
			// having to test the position with a z closer to the ground level at the Car Scrap Yard
			vTempInCoords.Z = 36.1141
		ENDIF
		IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MOVIE_STUDIO, vTempInCoords)
			IF IS_COORD_IN_SPECIFIED_AREA(vTempInCoords, AC_MOVIE_STUDIO)
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : AC_MOVIE_STUDIO check")
				ReturnCoord = <<-1012.3105, -465.1634, 36.1141>>
				ReturnHeading = 112.1485
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// B*1497949 - Airport runway / restricted area
	vTempInCoords = vInCoords
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 12.7091
	ENDIF
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_AIRPORT_AIRSIDE, vTempInCoords)
		IF IS_COORD_IN_SPECIFIED_AREA(vTempInCoords, AC_AIRPORT_AIRSIDE)
			iVariation = 4
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				SWITCH GET_RANDOM_INT_IN_RANGE(0, iVariation)
					CASE 0	ReturnCoord = <<-979.9500, -2746.1086, 12.7091>>	ReturnHeading = 91.4929			BREAK	// along the drop off slip road South/east
					CASE 1	ReturnCoord = <<-1025.1935, -2728.2175, 12.6647>>	ReturnHeading = 239.0041		BREAK	// East taxi bay outside main terminal
					CASE 2	ReturnCoord = <<-1051.3149, -2713.0686, 12.6676>>	ReturnHeading = 236.4666		BREAK	// West taxi bay outside main terminal
					CASE 3	ReturnCoord = <<-1095.7417, -2637.8713, 12.6461>>	ReturnHeading = 188.9897		BREAK	// along the drop off slip road North/west
				ENDSWITCH
			ELSE
				iVariation = iMpTaxi%8
				SWITCH iVariation
					CASE 1
						ReturnCoord = <<-1023.9825, -2489.8818, 19.0755>>
						ReturnHeading = 148.7004
					BREAK
					CASE 2
						ReturnCoord = <<-1029.9678, -2500.4377, 19.0803>>
						ReturnHeading = 148.7144
					BREAK
					CASE 3
						ReturnCoord = <<-1070.5457, -2570.8594, 19.0836>>
						ReturnHeading = 150.0955
					BREAK
					CASE 4
						ReturnCoord = <<-1056.0647, -2545.6619, 19.0809>>
						ReturnHeading = 149.5649
					BREAK
					CASE 5
						ReturnCoord = <<-1050.5780, -2536.2185, 19.0824>>
						ReturnHeading = 150.7363
					BREAK
					CASE 6
						ReturnCoord = <<-1036.0316, -2510.9021, 19.0794>>
						ReturnHeading = 147.2113
					BREAK
					CASE 7
						ReturnCoord = <<-1044.4872, -2525.5342, 19.0790>>
						ReturnHeading = 150.7597
					BREAK
					DEFAULT
						ReturnCoord = <<-1064.5002, -2560.6528, 19.0905>>
						ReturnHeading = 150.7081
					BREAK
				ENDSWITCH
			ENDIF
			
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : AC_AIRPORT_AIRSIDE check. Use Coords ", ReturnCoord)
			RETURN TRUE
		ENDIF
	ENDIF
	
	VECTOR vMtChiliadWestDropPos = <<-509.5746, 4938.9185, 146.3271>>
	FLOAT fMtChiliadWestDropHead = 232.0109
	FLOAT fDistToMtChiliadWestDropPos = FLAT_VDIST2(vInCoords, vMtChiliadWestDropPos)
	
	VECTOR vMtChiliadEastDropPos = <<2450.6035, 5129.2236, 45.9722>>
	FLOAT fMtChiliadEastDropHead = 241.1957
	FLOAT fDistToMtChiliadEastDropPos = FLAT_VDIST2(vInCoords, vMtChiliadEastDropPos)
	
	// B*1533798 - West side Mount Chiliad - keep in sync with IS_POSITION_IN_TAXI_RESTRICTED_AREA check
	vTempInCoords = vInCoords
	BOOL bInMountainRange = FALSE
	IF vInCoords.Z = 1.0
		// because custom waypoints placed on the map don't take Z value into account
		// having to test the position with a z closer to the ground level at the Car Scrap Yard
		vTempInCoords.Z = 400.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-148.877655,4862.203613,305.644196>>, <<454.627441,5573.104004,804.096985>>, 250.000000)	// majority West from top of cable cart building down
		bInMountainRange = TRUE
	ENDIF
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 200.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-482.893066,4990.255371,155.160110>>, <<7.830751,5009.370605,430.760376>>, 250.000000)		// minor West following down from previous check to lower ground
		bInMountainRange = TRUE
	ENDIF
	IF bInMountainRange	
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Mount Chiliad West")
		ReturnCoord = vMtChiliadWestDropPos
		ReturnHeading = fMtChiliadWestDropHead
		
		// if we are closer to the east side use the east side
		IF (fDistToMtChiliadEastDropPos < fDistToMtChiliadWestDropPos)
			ReturnCoord = vMtChiliadEastDropPos
			ReturnHeading = fMtChiliadEastDropHead
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : We are closer to the East Side of the mountain - Override with Mount Chiliad East")
		ENDIF
		RETURN TRUE
	ENDIF
	
	// B*1533798 - East side Mount Chiliad - keep in sync with IS_POSITION_IN_TAXI_RESTRICTED_AREA check
	bInMountainRange = FALSE	// reset
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 700.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<441.399902,5579.990234,802.513794>>, <<965.777588,5675.920898,601.264648>>, 250.000000)		// East from top of cable cart building down
		bInMountainRange = TRUE
	ENDIF
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 300.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<954.113892,5641.050781,646.505432>>, <<2140.375000,5377.753418,149.122086>>, 250.000000)	// majority East following down from previous check to lower ground
		bInMountainRange = TRUE
	ENDIF
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 100.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<2117.347656,5377.259277,173.329727>>, <<2439.933838,5297.444824,62.686623>>, 100.000000)	// minor East following down from previous check to lower ground	
		bInMountainRange = TRUE
	ENDIF
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 65.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<2393.347412,5321.579590,107.062424>>, <<2523.945557,5124.745605,41.683842>>, 70.000000)		// minor East following down from previous check to base		
		bInMountainRange = TRUE
	ENDIF
	IF bInMountainRange
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Mount Chiliad East")
		ReturnCoord = vMtChiliadEastDropPos
		ReturnHeading = fMtChiliadEastDropHead
		
		// if we are closer to the west side use the west side
		IF (fDistToMtChiliadWestDropPos < fDistToMtChiliadEastDropPos)
			ReturnCoord = vMtChiliadWestDropPos
			ReturnHeading = fMtChiliadWestDropHead
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : We are closer to the West Side of the mountain - Override with Mount Chiliad West")
		ENDIF
		RETURN TRUE
	ENDIF
	
	//B*1485093 - Port of LS heist Setup area - restricted by gates
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 6.0
	ENDIF	
	IF TAXI_IS_COORD_IN_RANGE_OF_COORD(vTempInCoords, << -99.68751, -2448.89111, 5.01731 >>, 230.0)	// ensure checks only happen if player is within range of the area
		IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<85.054482,-2511.883789,-2.996267>>, <<-57.599766,-2412.716309,15.000947>>, 75.000000)	// East section heading down the side of the gates		
		OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<7.516524,-2546.740967,1.331557>>, <<-177.326843,-2417.046631,19.160444>>, 80.000000)	// Central area heading from South East gates into area
		OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-260.035370,-2419.978271,1.399635>>, <<-27.263750,-2423.848389,25.000641>>, 80.000000)	// Waters edge
		OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-187.425598,-2516.085693,-6.849975>>, <<-186.751801,-2438.148682,25.001602>>, 40.00000)	// South gates
		OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-73.608131,-2538.563721,-6.989857>>, <<-183.255585,-2465.145020,25.020298>>, 70.000000)	// South area
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Port of LS heist Setup area")
			iVariation = 2
			IF GET_RANDOM_INT_IN_RANGE(0, iVariation) = 0
				ReturnCoord = <<29.5582, -2553.5869, 5.0004>>	// East entrance facing away
				ReturnHeading = 253.9545
			ELSE
				ReturnCoord = <<-189.6824, -2531.6492, 5.0031>>	// South entrance facing towards gate
				ReturnHeading = 0.0408
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	// B*1558280 - Barn on farm North of Mount Chiliad include the nodes leading up to the barn
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 28.4
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<412.279022,6481.867676,25.808207>>, <<468.935303,6442.752441,45.607273>>, 24.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Barn on farm North of Mount Chiliad")
		ReturnCoord = <<445.2310, 6476.9482, 28.4862>>	// on the dirt track along the side of the barn
		ReturnHeading = 219.4788
		RETURN TRUE
	ENDIF
	
	// B*1574664 - Hayes Autos (both entrances gated) - covering front (North) entrance
	//NOTE: also a taxi no spawn area
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 28.2065
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<476.539398,-1301.547974,44.824577>>, <<499.582245,-1339.024536,26.317030>>, 35.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Hayes Autos front entrance")
		ReturnCoord = <<498.1850, -1288.5354, 28.1923>> 	// near the front gate to the North
		ReturnHeading = 181.3208
		RETURN TRUE
	ENDIF
	// B*1574664 - Car Steal 1 garage / Hayes Autos (both entrances gated) - covering back (South) entrance
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 28.2065
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<496.981628,-1412.255127,43.293907>>, <<497.269012,-1339.363892,26.316479>>, 40.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Hayes Autos back entrance")
		ReturnCoord = <<504.1738, -1421.2511, 28.2065>>	// near the back gate east
		ReturnHeading = 83.2905
		RETURN TRUE
	ENDIF
	// Biker Bar near Race Course (needs to be checked before race course area)
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 73.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<951.170410,-147.004669,71.543259>>, <<1005.244934,-98.447449,95.626785>>, 70.000000)	// Covers the compound the bike bar is inside		
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Biker Bar near race course")
		ReturnCoord = <<961.3104, -150.1964, 73.4016>>	// outside compound on the main road
		ReturnHeading = 58.9938
		RETURN TRUE
	ENDIF
	
	// Horse Race course
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 52.0
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<1137.022095,89.068535,74.890221>>, <<1267.820801,292.010620,102.990440>>, 195.000000)	// North East end - from middle carpark			
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Horse Race course North")
		ReturnCoord = <<1118.7509, 262.0209, 79.8555>>	// Horse race course (North East section) car park at side of the grand stand
		ReturnHeading = 52.3086
		RETURN TRUE
	ELIF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<1137.022095,89.068535,71.890221>>, <<1010.045959,-91.566986,103.456757>>, 180.000000)	// South West end - from middle carpark			
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Horse Race course North")
		ReturnCoord = <<1134.0535, 53.1835, 79.7553>>	// Horse race course (South West section) car park in the middle of the course
		ReturnHeading = 145.5134
		RETURN TRUE
	ENDIF


	// B*1576487 - underground carpark restricted in FIB2 above maze tower
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 30.6
	ENDIF
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<191.873123,-723.553040,40.235764>>, <<79.586555,-687.668396,29.547338>>, 85.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : FIB2 underground car park above maze tower")
		iVariation = 2
		IF GET_RANDOM_INT_IN_RANGE(0, iVariation) = 0
			ReturnCoord = <<182.6361, -753.5836, 31.8051>>	// East entrance to car park. just South
			ReturnHeading = 162.0019
		ELSE
			ReturnCoord = <<81.5318, -675.4875, 30.5695>>	// West entrance under the high road, just North of car park entrance
			ReturnHeading = 341.8541
		ENDIF
		RETURN TRUE
	ENDIF
	
	// B*1651975 - Interior of Union Despository parking structure
	IF vInCoords.Z = 1.0
		vTempInCoords.Z = 33.5
	ENDIF
	
	IF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-40.0, -687.7, 36.6>>, <<-72.7, -682.0, 29.6>>, 15.000000)
	OR IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<-45.3, -687.8, 29.3>>, <<5.9, -688.7, 36.8>>, 55.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : B*1651975 - Interior of Union Despository parking structure - Front Entrance.")
		ReturnCoord = <<-88.4, -660.9, 35.0>>	// Near the main street entrance
		ReturnHeading = -20
		RETURN TRUE	
	ELIF IS_POINT_IN_ANGLED_AREA(vTempInCoords, <<8.1, -685.5, 31.2>>, <<26.3, -665.7, 35.2>>, 10.000000)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : B*1651975 - Interior of Union Despository parking structure - Rear Entrance.")
		ReturnCoord = <<61.4, -653.2, 32.0>>	// near the rear entrance
		ReturnHeading = 160
		RETURN TRUE
	ENDIF
	
	IF IS_POINT_IN_ANGLED_AREA(vInCoords, <<496.2593, -3116.5859, 13.6414>>,<<471.4366, -3116.2583, 5.7109>>, 10)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : ARE_COORDS_IN_SPECIAL_AREA return TRUE : Port securty doors")
		ReturnCoord = <<484.0132, -3033.1621, 5.0717>> 
		ReturnHeading = 140.7302
		RETURN TRUE
	ENDIF

	RETURN(FALSE)
	
ENDFUNC

/// PURPOSE:
///    check if the specified position is in a zone classed as safe for the taxi to use deadend nodes in it's path finding
///    basically anything in urban areas aren't safe because NF_IGNORE_SWITCHED_OFF_DEADENDS ae used for driveways
/// PARAMS:
///    vPos - specific position
/// RETURNS:
///    TRUE if the position is safe to allow NF_IGNORE_SWITCHED_OFF_DEADENDS
FUNC BOOL IS_POSITION_IN_ZONE_SAFE_FOR_DEADEND_NODES(VECTOR vPos)
	
	STRING tempZone
	tempZone = GET_NAME_OF_ZONE(vPos)
	
	IF ARE_STRINGS_EQUAL("SanAnd", tempZone) // San Andreas
	OR ARE_STRINGS_EQUAL("Alamo", tempZone) // Alamo Sea
	
	//OR ARE_STRINGS_EQUAL("Alta", tempZone) // Alta
	//OR ARE_STRINGS_EQUAL("Airp", tempZone) // Los Santos International Airport
	
	OR ARE_STRINGS_EQUAL("ArmyB", tempZone) // Fort Zancudo
	OR ARE_STRINGS_EQUAL("BhamCa", tempZone) // Banham Canyon
	
	//OR ARE_STRINGS_EQUAL("Banning", tempZone) // Banning
	
	OR ARE_STRINGS_EQUAL("Baytre", tempZone) // Baytree Canyon
	
	//OR ARE_STRINGS_EQUAL("Beach", tempZone) // Vespucci Beach
	
	OR ARE_STRINGS_EQUAL("BradT", tempZone) // Braddock Tunnel
	
	OR ARE_STRINGS_EQUAL("BradP", tempZone) // Braddock Pass

	//OR ARE_STRINGS_EQUAL("Burton", tempZone) // Burton

	OR ARE_STRINGS_EQUAL("CANNY", tempZone) // Raton Canyon
	OR ARE_STRINGS_EQUAL("CCreak", tempZone) // Cassidy Creek

	//OR ARE_STRINGS_EQUAL("CalafB", tempZone) // Calafia Bridge
	
	OR ARE_STRINGS_EQUAL("ChamH", tempZone) // Chamberlain Hills
	OR ARE_STRINGS_EQUAL("CHU", tempZone) // Chumash
	
	//OR ARE_STRINGS_EQUAL("CHIL", tempZone) // Vinewood Hills

	OR ARE_STRINGS_EQUAL("COSI", tempZone) // Countryside

	OR ARE_STRINGS_EQUAL("CMSW", tempZone) // Chiliad Mountain State Wilderness
	OR ARE_STRINGS_EQUAL("Cypre", tempZone) // Cypress Flats
	
	//OR ARE_STRINGS_EQUAL("Davis", tempZone) // Davis

	OR ARE_STRINGS_EQUAL("Desrt", tempZone) // Grand Senora Desert

	//OR ARE_STRINGS_EQUAL("DelBe", tempZone) // Del Perro Beach
	//OR ARE_STRINGS_EQUAL("DelPe", tempZone) // Del Perro
	//OR ARE_STRINGS_EQUAL("DelSol", tempZone) // La Puerta
	//OR ARE_STRINGS_EQUAL("Downt", tempZone) // Downtown
	//OR ARE_STRINGS_EQUAL("DTVine", tempZone) // Downtown Vinewood
	//OR ARE_STRINGS_EQUAL("Eclips", tempZone) // Eclipse
	//OR ARE_STRINGS_EQUAL("ELSant", tempZone) // East Los Santos
	//OR ARE_STRINGS_EQUAL("EBuro", tempZone) // El Burro Heights
	
	OR ARE_STRINGS_EQUAL("ELGorl", tempZone) // El Gordo Lighthouse

	//OR ARE_STRINGS_EQUAL("Elysian", tempZone) // Elysian Island

	OR ARE_STRINGS_EQUAL("Galli", tempZone) // Galileo Park
	OR ARE_STRINGS_EQUAL("Galfish", tempZone) // Galilee

	//OR ARE_STRINGS_EQUAL("Greatc", tempZone) // Great Chaparral
	//OR ARE_STRINGS_EQUAL("Golf", tempZone) // GWC and Golfing Society
	//OR ARE_STRINGS_EQUAL("GrapeS", tempZone) // Grapeseed
	//OR ARE_STRINGS_EQUAL("Hawick", tempZone) // Hawick

	OR ARE_STRINGS_EQUAL("Harmo", tempZone) // Harmony

	//OR ARE_STRINGS_EQUAL("Heart", tempZone) // Heart Attacks Beach

	OR ARE_STRINGS_EQUAL("HumLab", tempZone) // Humane Labs and Research

	//OR ARE_STRINGS_EQUAL("HORS", tempZone) // Vinewood Racetrack
	//OR ARE_STRINGS_EQUAL("Koreat", tempZone) // Little Seoul

	OR ARE_STRINGS_EQUAL("Jail", tempZone) // Bolingbroke Penitentiary
	OR ARE_STRINGS_EQUAL("LAct", tempZone) // Land Act Reservoir
	OR ARE_STRINGS_EQUAL("LDam", tempZone) // Land Act Dam
	OR ARE_STRINGS_EQUAL("Lago", tempZone) // Lago Zancudo

	//OR ARE_STRINGS_EQUAL("LegSqu", tempZone) // Legion Square	
	//OR ARE_STRINGS_EQUAL("LosSF", tempZone) // Los Santos Freeway
	//OR ARE_STRINGS_EQUAL("LMesa", tempZone) // La Mesa
	//OR ARE_STRINGS_EQUAL("LosPuer", tempZone) // La Puerta 
	//OR ARE_STRINGS_EQUAL("LosPFy", tempZone) // La Puerta Fwy
	//OR ARE_STRINGS_EQUAL("LOSTMC", tempZone) // Lost MC
	//OR ARE_STRINGS_EQUAL("Mirr", tempZone) // Mirror Park
	//OR ARE_STRINGS_EQUAL("Morn", tempZone) // Morningwood
	//OR ARE_STRINGS_EQUAL("Murri", tempZone) // Murrieta Heights

	OR ARE_STRINGS_EQUAL("MTChil", tempZone) // Mount Chiliad
	OR ARE_STRINGS_EQUAL("MTJose", tempZone) // Mount Josiah
	OR ARE_STRINGS_EQUAL("MTGordo", tempZone) // Mount Gordo	- B*1334556

	//OR ARE_STRINGS_EQUAL("Movie", tempZone) // Richards Majestic

	OR ARE_STRINGS_EQUAL("NCHU", tempZone) // North Chumash

	//OR ARE_STRINGS_EQUAL("Noose", tempZone) // N.O.O.S.E

	OR ARE_STRINGS_EQUAL("Oceana", tempZone) // Pacific Ocean

	//OR ARE_STRINGS_EQUAL("Observ", tempZone) // Galileo Observatory

	OR ARE_STRINGS_EQUAL("Palmpow", tempZone) // Palmer-Taylor Power Station

	//OR ARE_STRINGS_EQUAL("PBOX", tempZone) // Pillbox Hill

	OR ARE_STRINGS_EQUAL("PBluff", tempZone) // Pacific Bluffs
	OR ARE_STRINGS_EQUAL("Paleto", tempZone) // Paleto Bay	
	OR ARE_STRINGS_EQUAL("PalCov", tempZone) // Paleto Cove	- B*1332906
	OR ARE_STRINGS_EQUAL("PalFor", tempZone) // Paleto Forest
	OR ARE_STRINGS_EQUAL("PalHigh", tempZone) // Palomino Highlands

	//OR ARE_STRINGS_EQUAL("ProcoB", tempZone) // Procopio Beach
	//OR ARE_STRINGS_EQUAL("Prol", tempZone) // North Yankton

	OR ARE_STRINGS_EQUAL("RTRAK", tempZone) // Redwood Lights Track
	OR ARE_STRINGS_EQUAL("Rancho", tempZone) // Rancho

	//OR ARE_STRINGS_EQUAL("RGLEN", tempZone) // Richman Glen
	//OR ARE_STRINGS_EQUAL("Richm", tempZone) // Richman
	//OR ARE_STRINGS_EQUAL("Rockf", tempZone) // Rockford Hills

	OR ARE_STRINGS_EQUAL("SANDY", tempZone) // Sandy Shores
	OR ARE_STRINGS_EQUAL("TongvaH", tempZone) // Tongva Hills
	OR ARE_STRINGS_EQUAL("TongvaV", tempZone) // Tongva Valley
	//OR ARE_STRINGS_EQUAL("East_V", tempZone) // East Vinewood

	OR ARE_STRINGS_EQUAL("Zenora", tempZone) // Senora Freeway
	OR ARE_STRINGS_EQUAL("Slab", tempZone) // Stab City

	//OR ARE_STRINGS_EQUAL("SKID", tempZone) // Mission Row
	//OR ARE_STRINGS_EQUAL("SLSant", tempZone) // South Los Santos
	//OR ARE_STRINGS_EQUAL("Stad", tempZone) // Maze Bank Arena
	//OR ARE_STRINGS_EQUAL("Tatamo", tempZone) // Tataviam Mountains
	//OR ARE_STRINGS_EQUAL("Termina", tempZone) // Terminal
	//OR ARE_STRINGS_EQUAL("TEXTI", tempZone) // Textile City
	//OR ARE_STRINGS_EQUAL("WVine", tempZone) // West Vinewood
	//OR ARE_STRINGS_EQUAL("UtopiaG", tempZone) // Utopia Gardens
	//OR ARE_STRINGS_EQUAL("Vesp", tempZone) // Vespucci
	//OR ARE_STRINGS_EQUAL("VCana", tempZone) // Vespucci Canals
	//OR ARE_STRINGS_EQUAL("Vine", tempZone) // Vinewood
	//OR ARE_STRINGS_EQUAL("WMirror", tempZone) // W Mirror Drive

	OR ARE_STRINGS_EQUAL("WindF", tempZone) // Ron Alternates Wind Farm
	OR ARE_STRINGS_EQUAL("Zancudo", tempZone) // Zancudo River
	OR ARE_STRINGS_EQUAL("SanChia", tempZone) // San Chianski Mountain Range

	//OR ARE_STRINGS_EQUAL("STRAW", tempZone) // Strawberry
	
	OR ARE_STRINGS_EQUAL("zQ_UAR", tempZone) // Davis Quartz

	//OR ARE_STRINGS_EQUAL("ZP_ORT", tempZone) // Port of South Los Santos

		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_POSITION_IN_ZONE_SAFE_FOR_DEADEND_NODES return TRUE for vPos = ", vPos, " zone = ", tempZone)
	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the specified position is in a zone classed as countryside
///    used to test if the taxi should be using switched off nodes or not
/// PARAMS:
///    vPos - specific positin
/// RETURNS:
///    TRUE if the position is in the countryside
FUNC BOOL IS_POSITION_IN_COUNTRYSIDE(VECTOR vPos)
	STRING tempZone
	tempZone = GET_NAME_OF_ZONE(vPos)
	IF ARE_STRINGS_EQUAL("Alamo", tempZone)
	OR ARE_STRINGS_EQUAL("COSI", tempZone)
	OR ARE_STRINGS_EQUAL("Desrt", tempZone)
	OR ARE_STRINGS_EQUAL("GALLI", tempZone)
	OR ARE_STRINGS_EQUAL("GOcean", tempZone)
	OR ARE_STRINGS_EQUAL("Harmo", tempZone)
	OR ARE_STRINGS_EQUAL("SANDY", tempZone)
	OR ARE_STRINGS_EQUAL("Senora", tempZone)
	OR ARE_STRINGS_EQUAL("WindF", tempZone)
	OR ARE_STRINGS_EQUAL("Zancudo", tempZone)	
	OR ARE_STRINGS_EQUAL("Lago", tempZone)
	OR ARE_STRINGS_EQUAL("CANNY", tempZone)
	OR ARE_STRINGS_EQUAL("RTRAK", tempZone)
	OR ARE_STRINGS_EQUAL("CHU", tempZone)
	OR ARE_STRINGS_EQUAL("CHIL", tempZone)
	OR ARE_STRINGS_EQUAL("GrapeS", tempZone)
	OR ARE_STRINGS_EQUAL("MTJose", tempZone)
	OR ARE_STRINGS_EQUAL("Paleto", tempZone)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " IS_POSITION_IN_COUNTRYSIDE return TRUE for vPos = ", vPos, " zone = ", tempZone)
	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_COUNTRYSIDE()
	STRING tempZone
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		printDebugString("<")printDebugString(GET_THIS_SCRIPT_NAME())printDebugString("> - GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID())) = ")
		tempZone = GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		printDebugString(tempZone)
		printDebugString("\n")
		IF ARE_STRINGS_EQUAL("Alamo", tempZone)
		OR ARE_STRINGS_EQUAL("COSI", tempZone)
		OR ARE_STRINGS_EQUAL("Desrt", tempZone)
		OR ARE_STRINGS_EQUAL("GALLI", tempZone)
		OR ARE_STRINGS_EQUAL("GOcean", tempZone)
		OR ARE_STRINGS_EQUAL("Harmo", tempZone)
		OR ARE_STRINGS_EQUAL("SANDY", tempZone)
		OR ARE_STRINGS_EQUAL("Senora", tempZone)
		OR ARE_STRINGS_EQUAL("WindF", tempZone)
		OR ARE_STRINGS_EQUAL("Zancudo", tempZone)	
		OR ARE_STRINGS_EQUAL("Lago", tempZone)
		OR ARE_STRINGS_EQUAL("CANNY", tempZone)
		OR ARE_STRINGS_EQUAL("RTRAK", tempZone)
		OR ARE_STRINGS_EQUAL("CHU", tempZone)
		OR ARE_STRINGS_EQUAL("CHIL", tempZone)
		OR ARE_STRINGS_EQUAL("GrapeS", tempZone)
		OR ARE_STRINGS_EQUAL("MTJose", tempZone)
		OR ARE_STRINGS_EQUAL("Paleto", tempZone)
		
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_OFF_NODE_IF_ALLOWED(VECTOR vNodePos)
	IF IS_PLAYER_IN_COUNTRYSIDE()
		RETURN TRUE
	ELSE
		IF NOT GET_VEHICLE_NODE_IS_SWITCHED_OFF(GET_NTH_CLOSEST_VEHICLE_NODE_ID(vNodePos, 1))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SEND_TAXI_TO_COORD(VECTOR vDestination, FLOAT fDestinationH = 0.0, FLOAT taxiSpeed = 12.0, BOOL bUseOffNodes = FALSE, BOOL bInstantRelease = TRUE)
	DRIVINGMODE taxiDrivingMode 
	IF bUseOffNodes
		taxiDrivingMode = DF_SteerAroundStationaryCars|DF_StopForPeds|DF_StopAtLights|DF_ChangeLanesAroundObstructions//|DF_UseSwitchedOffNodes
	ELSE
		taxiDrivingMode = DF_SteerAroundStationaryCars|DF_StopForPeds|DF_ChangeLanesAroundObstructions
	ENDIF
	IF fDestinationH = 0
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)
		IF NOT IS_PED_INJURED(g_WaitingTaxiDriver)
			TASK_VEHICLE_MISSION_COORS_TARGET(g_WaitingTaxiDriver, g_WaitingTaxi, vDestination, MISSION_GOTO, taxiSpeed, taxiDrivingMode, 10, 10)
			IF bInstantRelease
				SET_PED_KEEP_TASK(g_WaitingTaxiDriver, TRUE)
				WAIT(0)
				CLEANUP_WAITING_TAXI()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(BLIP_INDEX &inBlipID, VECTOR vPosition, FLOAT fHeading)
	IF DOES_BLIP_EXIST(inBlipID)
		g_CustomDropOffBlip = inBlipID
		g_vCustomDropPosition = vPosition	
		g_fCustomDropHeading = fHeading
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_TAXI_DROPOFF_LOCATION_FOR_BLIP : g_vCustomDropPosition = ", g_vCustomDropPosition, " g_fCustomDropHeading = ", g_fCustomDropHeading)
	ENDIF
ENDPROC

PROC SET_BLIP_AS_FIRST_IN_TAXI_METER(BLIP_INDEX &inBlip)
	g_FirstBlipInTaxiMeter = inBlip
ENDPROC

PROC SET_BLIP_SPRITE_AS_FIRST_IN_TAXI_METER(BLIP_SPRITE inSprite)
	g_FirstBlipSpriteInTaxiMeter = inSprite
ENDPROC

PROC SET_TAXI_TO_IGNORE_BLIP(BLIP_INDEX &inBlip)
	g_BlipToIgnore = inBlip
ENDPROC

FUNC BLIP_SPRITE GET_SPRITE_FOR_BLIP(BLIP_INDEX inBlip)
	BLIP_SPRITE ReturnedBlipSprite
	IF DOES_BLIP_EXIST(inBlip)
		ReturnedBlipSprite = GET_BLIP_SPRITE(inBlip)
	ENDIF
	RETURN(ReturnedBlipSprite)
ENDFUNC

FUNC BOOL SHOULD_WE_IGNORE_THIS_BLIP(BLIP_INDEX &inBlip)//, BLIP_SPRITE BlipSprite)

	IF NOT DOES_BLIP_EXIST(inBlip)
		RETURN(TRUE)
	ENDIF
	
	IF (g_BlipToIgnore = inBlip)
		RETURN(TRUE)
	ENDIF
	
	IF GET_BLIP_COLOUR(inBlip) = BLIP_COLOUR_INACTIVE_MISSION
		RETURN TRUE
	ENDIF
	
	IF ARE_VECTORS_EQUAL(GET_BLIP_COORDS(inBlip), <<0,0,0>>)
		RETURN TRUE
	ENDIF
	
	IF GET_SPRITE_FOR_BLIP(inBlip) = RADAR_TRACE_INVALID
		RETURN TRUE
	ENDIF
	
	RETURN(FALSE)

ENDFUNC

PROC SET_TAXI_TO_DISPLAY_ONLY_THIS_MISSION_BLIP(BLIP_INDEX &inBlip)
	g_OnlyMissionBlipToDisplayOnTaxiMeter = inBlip
ENDPROC

//PROC SET_TAXI_BLIP_NAME(BLIP_INDEX &inBlip, STRING inString, BOOL bAmbient = FALSE)
//	
//	IF DOES_BLIP_EXIST(inBlip)
//		CHANGE_BLIP_NAME_FROM_TEXT_FILE(inBlip, inString)
//	ENDIF
//	
////	IF DOES_BLIP_EXIST(inBlip)
////		IF NOT (bAmbient)
////			g_CustomNamedBlip = inBlip
////			g_tlCustomBlipName = inString	
////		ELSE
////			g_CustomNamedBlipSpecial = inBlip
////			g_tlCustomBlipNameSpecial = inString	
////		ENDIF
////	ENDIF
//ENDPROC

FUNC BOOL IS_THERE_A_TAXI_WAITING_FOR_THE_PLAYER()
	IF IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)
		IF DOES_ENTITY_EXIST(g_WaitingTaxiDriver)
			IF NOT IS_PED_INJURED(g_WaitingTaxiDriver)
				IF IS_PED_IN_VEHICLE(g_WaitingTaxiDriver, g_WaitingTaxi)
					IF IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_ANY_PASSENGER)
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_CAR_A_TAXI_MODEL(VEHICLE_INDEX InCar)
	IF IS_VEHICLE_MODEL(InCar, TAXI)
	//OR IS_VEHICLE_MODEL(InCar, TAXI2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PED_IN_BACK_OF_TAXI(PED_INDEX InChar)
	VEHICLE_INDEX TempCar
	IF NOT IS_PED_INJURED(InChar)
		IF IS_PED_IN_ANY_VEHICLE(InChar)
			TempCar = GET_VEHICLE_PED_IS_IN(InChar)
			IF IS_VEHICLE_DRIVEABLE(TempCar)
				IF IS_CAR_A_TAXI_MODEL(TempCar)
					IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(TempCar) = 3)
						IF NOT IS_VEHICLE_SEAT_FREE (TempCar, VS_BACK_LEFT)
							IF (GET_PED_IN_VEHICLE_SEAT(TempCar, VS_BACK_LEFT)  = InChar)
								RETURN(TRUE)
							ENDIF
						ENDIF
						IF NOT IS_VEHICLE_SEAT_FREE (TempCar, VS_BACK_RIGHT)
							IF (GET_PED_IN_VEHICLE_SEAT(TempCar, VS_BACK_RIGHT) = InChar)
								RETURN(TRUE)
							ENDIF
						ENDIF
					ENDIF

				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_ANY_DIALOGUE_PLAYING(PED_INDEX DriverID, BOOL bReturnTrueForMessageDisplayed = TRUE)

	IF NOT IS_PED_INJURED(DriverID)
		IF IS_AMBIENT_SPEECH_PLAYING(DriverID)
			printDebugString("\ntaxi_functions - IS_ANY_DIALOGUE_PLAYING - IS_AMBIENT_SPEECH_PLAYING(DriverID)\n")
			RETURN(TRUE)
		ENDIF	
	ENDIF

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
			printDebugString("\ntaxi_functions - IS_ANY_DIALOGUE_PLAYING - IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())\n")
			RETURN(TRUE)
		ENDIF	
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		printDebugString("\ntaxi_functions - IS_ANY_DIALOGUE_PLAYING - IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()\n")
		RETURN(TRUE)
	ENDIF
	
	IF bReturnTrueForMessageDisplayed
		IF IS_MESSAGE_BEING_DISPLAYED()
			printDebugString("\ntaxi_functions - IS_ANY_DIALOGUE_PLAYING - IS_MESSAGE_BEING_DISPLAYED()\n")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PED_INDEX TempGroupPedID
	INT i, j
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
			GET_GROUP_SIZE(PLAYER_GROUP_ID(), j, i)
			IF (i > 0)
				REPEAT i j
					TempGroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), j)
					IF NOT IS_PED_INJURED(TempGroupPedID)
						IF IS_AMBIENT_SPEECH_PLAYING(TempGroupPedID)
							printDebugString("\ntaxi_functions - IS_ANY_DIALOGUE_PLAYING - IS_AMBIENT_SPEECH_PLAYING(TempGroupPedID)\n")
							RETURN(TRUE)
						ENDIF	
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF

	RETURN(FALSE)

ENDFUNC

//// rewritten function to work for japanese version so no need for big #IF blocks
//FUNC BOOL COMPARE_STREETS_FOR_AUDIO(INT street_label_hash, STRING street_label, STRING gxt_text_label)
//
//		RETURN street_label_hash = GET_HASH_KEY(gxt_text_label)
//
//ENDFUNC

FUNC VECTOR GET_RADAR_BLIP_COORDS(BLIP_INDEX inBlipID)

 	VECTOR vReturn = << 0.0 , 0.0, 0.0>>
	VEHICLE_INDEX TempCar
	PED_INDEX TempChar
	OBJECT_INDEX TempObject
	PICKUP_INDEX TempPickup
	eRADAR_BLIP_TYPE eBlipType = GET_BLIP_INFO_ID_TYPE(inBlipID)
	
	IF eBlipType = BLIPTYPE_VEHICLE 
		TempCar = GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID)
		IF IS_VEHICLE_DRIVEABLE(TempCar)
			vReturn = GET_ENTITY_COORDS(TempCar)
		ENDIF
		
	ELIF eBlipType = BLIPTYPE_CHAR
		TempChar = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID))
		IF NOT IS_PED_INJURED(TempChar)
			vReturn = GET_ENTITY_COORDS(TempChar)
		ENDIF
		
	ELIF eBlipType = BLIPTYPE_OBJECT
		TempObject = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID))
		IF DOES_ENTITY_EXIST(TempObject)
			vReturn = GET_ENTITY_COORDS(TempObject)
		ENDIF
		
	ELIF eBlipType = BLIPTYPE_COORDS
		vReturn = GET_BLIP_COORDS(inBlipID)
		
	ELIF eBlipType = BLIPTYPE_PICKUP
		TempPickup = GET_BLIP_INFO_ID_PICKUP_INDEX(inBlipID)
		IF DOES_PICKUP_EXIST(TempPickup)
			vReturn = GET_PICKUP_COORDS(TempPickup)
		ENDIF
		
	ELIF eBlipType = BLIPTYPE_CONTACT
		vReturn = GET_BLIP_COORDS(inBlipID)
	ENDIF
	
//	// not used
//	BLIPTYPE_UNUSED
//	BLIPTYPE_PICKUP
//	BLIPTYPE_RADIUS
//		vReturn = << 0.0 , 0.0, 0.0>>
//	ENDIF

	RETURN(vReturn)
ENDFUNC

FUNC FLOAT GET_BLIP_DISTANCE(BLIP_INDEX inBlipID)
	VECTOR vec = << 0.0, 0.0, 9999999.9>>
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF DOES_BLIP_EXIST(inBlipID)
			vec = GET_RADAR_BLIP_COORDS(inBlipID) - GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	RETURN(VMAG(vec))
ENDFUNC

FUNC BOOL IS_TAXI_DEST_NEARER_THAN_TAXI_DEST(TAXI_DEST inTaxiDest1, TAXI_DEST inTaxiDest2)
	IF GET_BLIP_DISTANCE(inTaxiDest1.BlipID) < GET_BLIP_DISTANCE(inTaxiDest2.BlipID)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

/// PURPOSE:
///    convert a TEXT_LABEL into type STRING
/// PARAMS:
///    tlTextLabel - the text label to convert
/// RETURNS:
///    STRING
FUNC STRING TAXI_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(STRING tlTextLabel)
	RETURN(tlTextLabel)
ENDFUNC

/// PURPOSE:
///    get the audio string for the destination
///    unfortunately audio set the strigs to use the actual location names in stead of the text key names GET_NAME_OF_ZONE returns
/// RETURNS:
///    String
FUNC STRING GET_LOCATION_AUDIO_STRING()

	TEXT_LABEL_31 tl31ZoneLabel = GET_NAME_OF_ZONE(GET_RADAR_BLIP_COORDS(currentTaxiDestination.BlipID))
	
	IF ARE_STRINGS_EQUAL("SanAnd", tl31ZoneLabel) // San Andreas
		RETURN("LOCATION_SAN_ANDREAS")		
	ELIF ARE_STRINGS_EQUAL("Alamo", tl31ZoneLabel) // Alamo Sea
		RETURN("LOCATION_ALAMO_SEA")		
	ELIF ARE_STRINGS_EQUAL("Alta", tl31ZoneLabel) // Alta
		RETURN("LOCATION_ALTA")	
	ELIF ARE_STRINGS_EQUAL("Airp", tl31ZoneLabel) // Los Santos International Airport
		RETURN("LOCATION_LOS_SANTOS_INTERNATIONAL_AIRPORT")
	ELIF ARE_STRINGS_EQUAL("ArmyB", tl31ZoneLabel) // Fort Zancudo
		RETURN("LOCATION_FORT_ZANCUDO")
	ELIF ARE_STRINGS_EQUAL("BhamCa", tl31ZoneLabel) // Banham Canyon
		RETURN("LOCATION_BANHAM_CANYON")
	ELIF ARE_STRINGS_EQUAL("Banning", tl31ZoneLabel) // Banning
		RETURN("LOCATION_BANNING")
	ELIF ARE_STRINGS_EQUAL("Baytre", tl31ZoneLabel) // Baytree Canyon
		RETURN("LOCATION_BAYTREE_CANYON")
	ELIF ARE_STRINGS_EQUAL("Beach", tl31ZoneLabel) // Vespucci Beach
		RETURN("LOCATION_VESPUCCI_BEACH")
	ELIF ARE_STRINGS_EQUAL("BradT", tl31ZoneLabel) // Braddock Tunnel
		RETURN("LOCATION_BRADDOCK_TUNNEL")
	ELIF ARE_STRINGS_EQUAL("BradP", tl31ZoneLabel) // Braddock Pass
		RETURN("LOCATION_BRADDOCK_PASS")
	ELIF ARE_STRINGS_EQUAL("Burton", tl31ZoneLabel) // Burton
		RETURN("LOCATION_BURTON")
	ELIF ARE_STRINGS_EQUAL("CANNY", tl31ZoneLabel) // Raton Canyon
		RETURN("LOCATION_RATON_CANYON")
	ELIF ARE_STRINGS_EQUAL("CCreak", tl31ZoneLabel) // Cassidy Creek
		RETURN("LOCATION_CASSIDY_CREEK")
	ELIF ARE_STRINGS_EQUAL("CalafB", tl31ZoneLabel) // Calafia Bridge
		RETURN("LOCATION_CALAFIA_BRIDGE")
	ELIF ARE_STRINGS_EQUAL("ChamH", tl31ZoneLabel) // Chamberlain Hills
		RETURN("LOCATION_CHAMBERLAIN_HILLS")
	ELIF ARE_STRINGS_EQUAL("CHU", tl31ZoneLabel) // Chumash
		RETURN("LOCATION_CHUMASH")
	ELIF ARE_STRINGS_EQUAL("CHIL", tl31ZoneLabel) // Vinewood Hills
		RETURN("LOCATION_VINEWOOD_HILLS")
	ELIF ARE_STRINGS_EQUAL("COSI", tl31ZoneLabel) // Countryside
		RETURN("LOCATION_COUNTRYSIDE")
	ELIF ARE_STRINGS_EQUAL("CMSW", tl31ZoneLabel) // Chiliad Mountain State Wilderness
		RETURN("LOCATION_CHILIAD_MOUNTAIN_STATE_WILDERNESS")
	ELIF ARE_STRINGS_EQUAL("Cypre", tl31ZoneLabel) // Cypress Flats
		RETURN("LOCATION_CYPRESS_FLATS")
	ELIF ARE_STRINGS_EQUAL("Davis", tl31ZoneLabel) // Davis
		RETURN("LOCATION_DAVIS")
	ELIF ARE_STRINGS_EQUAL("Desrt", tl31ZoneLabel) // Grand Senora Desert
		RETURN("LOCATION_GRAND_SENORA_DESERT")
	ELIF ARE_STRINGS_EQUAL("DelBe", tl31ZoneLabel) // Del Perro Beach
		RETURN("LOCATION_DEL_PERRO_BEACH")
	ELIF ARE_STRINGS_EQUAL("DelPe", tl31ZoneLabel) // Del Perro
		RETURN("LOCATION_DEL_PERRO")
	ELIF ARE_STRINGS_EQUAL("DelSol", tl31ZoneLabel) // La Puerta
		RETURN("LOCATION_LA_PUERTA")
	ELIF ARE_STRINGS_EQUAL("Downt", tl31ZoneLabel) // Downtown
		RETURN("LOCATION_DOWNTOWN")
	ELIF ARE_STRINGS_EQUAL("DTVine", tl31ZoneLabel) // Downtown Vinewood
		RETURN("LOCATION_DOWNTOWN_VINEWOOD")
	ELIF ARE_STRINGS_EQUAL("Eclips", tl31ZoneLabel) // Eclipse
		RETURN("LOCATION_ECLIPSE")
	ELIF ARE_STRINGS_EQUAL("ELSant", tl31ZoneLabel) // East Los Santos
		RETURN("LOCATION_EAST_LOS_SANTOS")
	ELIF ARE_STRINGS_EQUAL("EBuro", tl31ZoneLabel) // El Burro Heights
		RETURN("LOCATION_EL_BURRO_HEIGHTS")
	ELIF ARE_STRINGS_EQUAL("ELGorl", tl31ZoneLabel) // El Gordo Lighthouse
		RETURN("LOCATION_EL_GORDO_LIGHTHOUSE")
	ELIF ARE_STRINGS_EQUAL("Elysian", tl31ZoneLabel) // Elysian Island
		RETURN("LOCATION_ELYSIAN_ISLAND")
	ELIF ARE_STRINGS_EQUAL("Galli", tl31ZoneLabel) // Galileo Park
		RETURN("LOCATION_GALILEO_PARK")
	ELIF ARE_STRINGS_EQUAL("Galfish", tl31ZoneLabel) // Galilee
		RETURN("LOCATION_GALILEE")
	ELIF ARE_STRINGS_EQUAL("Greatc", tl31ZoneLabel) // Great Chaparral
		RETURN("LOCATION_GREAT_CHAPARRAL")
	ELIF ARE_STRINGS_EQUAL("Golf", tl31ZoneLabel) // GWC and Golfing Society
		RETURN("LOCATION_GWC_AND_GOLFING_SOCIETY")
	ELIF ARE_STRINGS_EQUAL("GrapeS", tl31ZoneLabel) // Grapeseed
		RETURN("LOCATION_GRAPESEED")
	ELIF ARE_STRINGS_EQUAL("Hawick", tl31ZoneLabel) // Hawick
		RETURN("LOCATION_HAWICK")
	ELIF ARE_STRINGS_EQUAL("Harmo", tl31ZoneLabel) // Harmony
		RETURN("LOCATION_HARMONY")
	ELIF ARE_STRINGS_EQUAL("Heart", tl31ZoneLabel) // Heart Attacks Beach
		RETURN("LOCATION_HEART_ATTACKS_BEACH")
	ELIF ARE_STRINGS_EQUAL("HumLab", tl31ZoneLabel) // Humane Labs and Research
		RETURN("LOCATION_HUMANE_LABS_AND_RESEARCH")
	ELIF ARE_STRINGS_EQUAL("HORS", tl31ZoneLabel) // Vinewood Racetrack
		RETURN("LOCATION_VINEWOOD_RACETRACK")
	ELIF ARE_STRINGS_EQUAL("Koreat", tl31ZoneLabel) // Little Seoul
		RETURN("LOCATION_LITTLE_SEOUL")
	ELIF ARE_STRINGS_EQUAL("Jail", tl31ZoneLabel) // Bolingbroke Penitentiary
		RETURN("LOCATION_BOLINGBROKE_PENITENTIARY")
	ELIF ARE_STRINGS_EQUAL("LAct", tl31ZoneLabel) // Land Act Reservoir
		RETURN("LOCATION_LAND_ACT_RESERVOIR")
	ELIF ARE_STRINGS_EQUAL("LDam", tl31ZoneLabel) // Land Act Dam
		RETURN("LOCATION_LAND_ACT_DAM")
	ELIF ARE_STRINGS_EQUAL("Lago", tl31ZoneLabel) // Lago Zancudo
		RETURN("LOCATION_LAGO_ZANCUDO")
	ELIF ARE_STRINGS_EQUAL("LegSqu", tl31ZoneLabel) // Legion Square	
		RETURN("LOCATION_LEGION_SQUARE")
	ELIF ARE_STRINGS_EQUAL("LosSF", tl31ZoneLabel) // Los Santos Freeway
		RETURN("LOCATION_LOS_SANTOS_FREEWAY")
	ELIF ARE_STRINGS_EQUAL("LMesa", tl31ZoneLabel) // La Mesa
		RETURN("LOCATION_LA_MESA")
	ELIF ARE_STRINGS_EQUAL("LosPuer", tl31ZoneLabel) // La Puerta 
		RETURN("LOCATION_LA_PUERTA")
	ELIF ARE_STRINGS_EQUAL("LosPFy", tl31ZoneLabel) // La Puerta Fwy
		RETURN("LOCATION_LA_PUERTA_FWY")
	//ELIF ARE_STRINGS_EQUAL("LOSTMC", tl31ZoneLabel) // Lost MC	// not recorded
	//	RETURN("LOCATION")
	ELIF ARE_STRINGS_EQUAL("Mirr", tl31ZoneLabel) // Mirror Park
		RETURN("LOCATION_MIRROR_PARK")
	ELIF ARE_STRINGS_EQUAL("Morn", tl31ZoneLabel) // Morningwood
		RETURN("LOCATION_MORNINGWOOD")
	ELIF ARE_STRINGS_EQUAL("Murri", tl31ZoneLabel) // Murrieta Heights
		RETURN("LOCATION_MURRIETA_HEIGHTS")
	ELIF ARE_STRINGS_EQUAL("MTChil", tl31ZoneLabel) // Mount Chiliad
		RETURN("LOCATION_MOUNT_CHILIAD")
	ELIF ARE_STRINGS_EQUAL("MTJose", tl31ZoneLabel) // Mount Josiah
		RETURN("LOCATION_MOUNT_JOSIAH")
	ELIF ARE_STRINGS_EQUAL("MTGordo", tl31ZoneLabel) // Mount Gordo	- B*1334556
		RETURN("LOCATION_MOUNT_GORDO")
	ELIF ARE_STRINGS_EQUAL("Movie", tl31ZoneLabel) // Richards Majestic
		RETURN("LOCATION_RICHARDS_MAJESTIC")
	ELIF ARE_STRINGS_EQUAL("NCHU", tl31ZoneLabel) // North Chumash
		RETURN("LOCATION_NORTH_CHUMASH")
	//ELIF ARE_STRINGS_EQUAL("Noose", tl31ZoneLabel) // N.O.O.S.E		// not recorded
	//	RETURN("LOCATION_")
	ELIF ARE_STRINGS_EQUAL("Oceana", tl31ZoneLabel) // Pacific Ocean
		RETURN("LOCATION_PACIFIC_OCEAN")
	ELIF ARE_STRINGS_EQUAL("Observ", tl31ZoneLabel) // Galileo Observatory
		RETURN("LOCATION_GALILEO_OBSERVATORY")
	ELIF ARE_STRINGS_EQUAL("Palmpow", tl31ZoneLabel) // Palmer-Taylor Power Station
		RETURN("LOCATION_PALMER-TAYLOR_POWER_STATION")
	ELIF ARE_STRINGS_EQUAL("PBOX", tl31ZoneLabel) // Pillbox Hill
		RETURN("LOCATION_PILLBOX_HILL")
	ELIF ARE_STRINGS_EQUAL("PBluff", tl31ZoneLabel) // Pacific Bluffs
		RETURN("LOCATION_PACIFIC_BLUFFS")
	ELIF ARE_STRINGS_EQUAL("Paleto", tl31ZoneLabel) // Paleto Bay
		RETURN("LOCATION_PALETO_BAY")
	ELIF ARE_STRINGS_EQUAL("PalCov", tl31ZoneLabel) // Paleto Cove	- B*1332906
		RETURN("LOCATION_PALETO_COVE")
	ELIF ARE_STRINGS_EQUAL("PalFor", tl31ZoneLabel) // Paleto Forest
		RETURN("LOCATION_PALETO_FOREST")
	ELIF ARE_STRINGS_EQUAL("PalHigh", tl31ZoneLabel) // Palomino Highlands
		RETURN("LOCATION_PALOMINO_HIGHLANDS")
	ELIF ARE_STRINGS_EQUAL("ProcoB", tl31ZoneLabel) // Procopio Beach
		RETURN("LOCATION_PROCOPIO_BEACH")
	ELIF ARE_STRINGS_EQUAL("Prol", tl31ZoneLabel) // North Yankton
		RETURN("LOCATION_NORTH_YANKTON")
	ELIF ARE_STRINGS_EQUAL("RTRAK", tl31ZoneLabel) // Redwood Lights Track
		RETURN("LOCATION_REDWOOD_LIGHTS_TRACK")
	ELIF ARE_STRINGS_EQUAL("Rancho", tl31ZoneLabel) // Rancho
		RETURN("LOCATION_RANCHO")
	ELIF ARE_STRINGS_EQUAL("RGLEN", tl31ZoneLabel) // Richman Glen
		RETURN("LOCATION_RICHMAN_GLEN")
	ELIF ARE_STRINGS_EQUAL("Richm", tl31ZoneLabel) // Richman
		RETURN("LOCATION_RICHMAN")
	ELIF ARE_STRINGS_EQUAL("Rockf", tl31ZoneLabel) // Rockford Hills
		RETURN("LOCATION_ROCKFORD_HILLS")
	ELIF ARE_STRINGS_EQUAL("SANDY", tl31ZoneLabel) // Sandy Shores
		RETURN("LOCATION_SANDY_SHORES")
	ELIF ARE_STRINGS_EQUAL("TongvaH", tl31ZoneLabel) // Tongva Hills
		RETURN("LOCATION_TONGVA_HILLS")
	ELIF ARE_STRINGS_EQUAL("TongvaV", tl31ZoneLabel) // Tongva Valley
		RETURN("LOCATION_TONGVA_VALLEY")
	ELIF ARE_STRINGS_EQUAL("East_V", tl31ZoneLabel) // East Vinewood
		RETURN("LOCATION_EAST_VINEWOOD")
	ELIF ARE_STRINGS_EQUAL("Zenora", tl31ZoneLabel) // Senora Freeway
		RETURN("LOCATION_SENORA_FREEWAY")
	ELIF ARE_STRINGS_EQUAL("Slab", tl31ZoneLabel) // Stab City
		RETURN("LOCATION_SLAB_CITY")
	ELIF ARE_STRINGS_EQUAL("SKID", tl31ZoneLabel) // Mission Row
		RETURN("LOCATION_MISSION_ROW")
	ELIF ARE_STRINGS_EQUAL("SLSant", tl31ZoneLabel) // South Los Santos
		RETURN("LOCATION_SOUTH_LOS_SANTOS")
	ELIF ARE_STRINGS_EQUAL("Stad", tl31ZoneLabel) // Maze Bank Arena
		RETURN("LOCATION_MAZE_BANK_ARENA")
	ELIF ARE_STRINGS_EQUAL("Tatamo", tl31ZoneLabel) // Tataviam Mountains
		RETURN("LOCATION_TATAVIAM_MOUNTAINS")
	ELIF ARE_STRINGS_EQUAL("Termina", tl31ZoneLabel) // Terminal
		RETURN("LOCATION_TERMINAL")
	ELIF ARE_STRINGS_EQUAL("TEXTI", tl31ZoneLabel) // Textile City
		RETURN("LOCATION_TEXTILE_CITY")
	ELIF ARE_STRINGS_EQUAL("WVine", tl31ZoneLabel) // West Vinewood
		RETURN("LOCATION_WEST_VINEWOOD")
	ELIF ARE_STRINGS_EQUAL("UtopiaG", tl31ZoneLabel) // Utopia Gardens
		RETURN("LOCATION_UTOPIA_GARDENS")
	ELIF ARE_STRINGS_EQUAL("Vesp", tl31ZoneLabel) // Vespucci
		RETURN("LOCATION_VESPUCCI")
	ELIF ARE_STRINGS_EQUAL("VCana", tl31ZoneLabel) // Vespucci Canals
		RETURN("LOCATION_VESPUCCI_CANALS")
	ELIF ARE_STRINGS_EQUAL("Vine", tl31ZoneLabel) // Vinewood
		RETURN("LOCATION_VINEWOOD")
	ELIF ARE_STRINGS_EQUAL("WMirror", tl31ZoneLabel) // W Mirror Drive
		RETURN("LOCATION_W_MIRROR_DRIVE")
	ELIF ARE_STRINGS_EQUAL("WindF", tl31ZoneLabel) // Ron Alternates Wind Farm
		RETURN("LOCATION_RON_ALTERNATES_WIND_WARM")
	ELIF ARE_STRINGS_EQUAL("Zancudo", tl31ZoneLabel) // Zancudo River
		RETURN("LOCATION_ZANCUDO_RIVER")
	ELIF ARE_STRINGS_EQUAL("SanChia", tl31ZoneLabel) // San Chianski Mountain Range
		RETURN("LOCATION_SAN_CHIANSKI_MOUNTAIN_RANGE")
	ELIF ARE_STRINGS_EQUAL("STRAW", tl31ZoneLabel) // Strawberry
		RETURN("LOCATION_STRAWBERRY")
	ELIF ARE_STRINGS_EQUAL("zQ_UAR", tl31ZoneLabel) // Davis Quartz
		RETURN("LOCATION_DAVIS_QUARTZ")
	ELIF ARE_STRINGS_EQUAL("ZP_ORT", tl31ZoneLabel) // Port of South Los Santos
		RETURN("LOCATION_PORT_OF_SOUTH_LOS_SANTOS")
	ENDIF
	
	RETURN ""

ENDFUNC	

PROC PLAYER_SAY_TAXI_DESTINATION()

	STRING stringAreaAudio = GET_LOCATION_AUDIO_STRING()
	IF NOT IS_STRING_NULL_OR_EMPTY(stringAreaAudio)
	
		BOOL bFlag = IS_AMBIENT_SPEECH_DISABLED(PLAYER_PED_ID())
		IF (bFlag)
			STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
		ENDIF
	
		PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), stringAreaAudio, SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAYER_SAY_TAXI_DESTINATION() - triggered : ", stringAreaAudio)
		IF (bFlag)
			STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAYER_SAY_TAXI_DESTINATION() - skipped area name not valid : ")
	ENDIF
ENDPROC

PROC PLAY_TAXI_DIALOGUE(PED_INDEX DriverID, TAXI_DIALOGUE &iSpeechToPlay)
	BOOL bFlag
	IF NOT IS_PED_INJURED(DriverID) 
		IF NOT IS_ANY_DIALOGUE_PLAYING(DriverID, FALSE)
			IF NOT (iSpeechToPlay = TAXI_DIALOGUE_NOTHING)
			
				IF iSpeechToPlay = TAXI_DIALOGUE_WHERE_TO
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_WHERE_TO", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXID_WHERE_TO context = TAXID_WHERE_TO ")
			
				ELIF iSpeechToPlay = TAXI_DIALOGUE_BEGIN_JOURNEY_1
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						PLAYER_SAY_TAXI_DESTINATION()
						iSpeechToPlay = TAXI_DIALOGUE_BEGIN_JOURNEY_2
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_BEGIN_JOURNEY_1")
					ENDIF
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_BEGIN_JOURNEY_2
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_BEGIN_JOURNEY", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_BEGIN_JOURNEY_2 context = TAXID_BEGIN_JOURNEY ")
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_BANTER
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_BANTER")
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_BANTER context = TAXID_BANTER ")
					
				ELIF iSpeechToPlay = TAXI_DIALOGUE_ARRIVE
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_ARRIVE_AT_DEST", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_THANKS
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_ARRIVE  context = TAXID_ARRIVE_AT_DEST ")
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_CLOSE_AS_POSS
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_CLOSE_AS_POSS", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_CLOSE_AS_POSS  context = TAXI_DIALOGUE_CLOSE_AS_POSS ")
			
				ELIF iSpeechToPlay = TAXI_DIALOGUE_NO_MONEY
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_NO_MONEY", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_NO_MONEY context = TAXI_DIALOGUE_NO_MONEY ")
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_CHANGE_DEST_1
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						bFlag = IS_AMBIENT_SPEECH_DISABLED(PLAYER_PED_ID())
						IF (bFlag)
							STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
						ENDIF
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "TAXI_CHANGE_DEST", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
						IF (bFlag)
							STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
						ENDIF
						iSpeechToPlay = TAXI_DIALOGUE_CHANGE_DEST_2
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered player : ", "TAXI_DIALOGUE_CHANGE_DEST_1 context = TAXI_CHANGE_DEST ")
					ENDIF
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_CHANGE_DEST_2
					IF IS_PLAYER_PLAYING(PLAYER_ID())								
						PLAYER_SAY_TAXI_DESTINATION()
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_CHANGE_DEST_2")
						iSpeechToPlay = TAXI_DIALOGUE_CHANGE_DEST_3
					ENDIF
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_CHANGE_DEST_3 
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_CHANGE_DEST", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_CHANGE_DEST_3 context = TAXID_CHANGE_DEST ")
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_RUN_AWAY
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_RUN_AWAY", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_RUN_AWAY context = TAXID_RUN_AWAY ")
			
				ELIF iSpeechToPlay = TAXI_DIALOGUE_OUT_EARLY
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_GET_OUT_EARLY", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_OUT_EARLY context = TAXI_DIALOGUE_OUT_EARLY ")
			
				ELIF iSpeechToPlay = TAXI_DIALOGUE_TRASHED
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_TRASHED", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_TRASHED context = TAXID_TRASHED ")
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_PART_JOURNEY
					IF NOT (g_bPlayerCannotAffordToSkip)
						PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_AFFORD_PART_JOURNEY", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
						g_bPlayerCannotAffordToSkip = TRUE
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_PART_JOURNEY context = TAXID_AFFORD_PART_JOURNEY ")
					ENDIF
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_FIRST_CAB
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_TAKE_FIRST_CAB", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_FIRST_CAB context = TAXID_TAKE_FIRST_CAB ")
				
				ELIF iSpeechToPlay = TAXI_DIALOGUE_RADIO_1
					//IF PLAY_SINGLE_SPEECH("TAX1_RADIO", nfScriptedSpeech, SPEECH_PRIORITY_AMBIENT_LOW)
					iSpeechToPlay = TAXI_DIALOGUE_RADIO_2
					//ENDIF
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_RADIO_2
					IF IS_RADIO_RETUNING()
						SETTIMERA(0)
					ELIF TIMERA() > 4000

						IF IS_PLAYER_PLAYING(PLAYER_ID())

 							// has player been shut up for mission?
							bFlag = IS_AMBIENT_SPEECH_DISABLED(PLAYER_PED_ID())
							//bFlag = TRUE
							IF (bFlag)
								STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
							ENDIF

							SWITCH GET_PLAYER_RADIO_STATION_INDEX()
								CASE 0 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_VIBE", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 1 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_LRR", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 2 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_JNR", SPEECH_PARAMS_FORCE_NORMAL_CLEAR) 		
								BREAK
								CASE 3 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_MASSIVEB", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)	
								BREAK
								CASE 4 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_K109", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 5 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_WKTT", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 6 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_LCHC", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 7 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_JOURNEY", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)	
								BREAK
								CASE 8 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_FUSION", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 9 
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_BEAT", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)	
								BREAK
								CASE 10
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_BROKER", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)	
								BREAK
								CASE 11
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_VLADIVOSTOK", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 12
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_PLR", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)	
								BREAK
								CASE 13
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_SANJUAN", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 14
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_FRANCOIS", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
								CASE 15
									PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "RADIO_REQ_CLASSICS", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
								BREAK
							ENDSWITCH

							IF (bFlag)
								STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
							ENDIF

						ENDIF

						iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered player : ", "TAXI_DIALOGUE_RADIO_2 for radio index : ", GET_PLAYER_RADIO_STATION_INDEX())
					ENDIF	
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_RADIO_3
//					IF PLAY_SINGLE_SPEECH("TAX1_RESP", nfScriptedSpeech, SPEECH_PRIORITY_AMBIENT_MEDIUM)
						iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
//					ENDIF
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_STEP_ON_IT_1				 
					IF IS_PLAYER_PLAYING(PLAYER_ID())

						// has player been shut up for mission?
						bFlag = IS_AMBIENT_SPEECH_DISABLED(PLAYER_PED_ID())
						//bFlag = TRUE
						IF (bFlag)
							STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
						ENDIF

						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "TAXI_STEP_ON_IT", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)

						IF (bFlag)
							STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
						ENDIF
						iSpeechToPlay = TAXI_DIALOGUE_STEP_ON_IT_2
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered player : ", "TAXI_DIALOGUE_STEP_ON_IT_1 context = TAXI_STEP_ON_IT ")
					ENDIF
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_STEP_ON_IT_2
					PLAY_PED_AMBIENT_SPEECH(DriverID, "TAXID_SPEED_UP", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
					iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered : ", "TAXI_DIALOGUE_STEP_ON_IT_2 context = TAXID_SPEED_UP ")
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_THANKS
					IF IS_PLAYER_PLAYING(PLAYER_ID())

						// has player been shut up for mission?
						bFlag = IS_AMBIENT_SPEECH_DISABLED(PLAYER_PED_ID())
						//bFlag = TRUE
						IF (bFlag)
							STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
						ENDIF
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "THANKS", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
						
						IF (bFlag)
							STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
						ENDIF

						iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : PLAY_TAXI_DIALOGUE() - triggered player : ", "TAXI_DIALOGUE_THANKS	context = THANKS ")
					ENDIF
						
				ELIF iSpeechToPlay = TAXI_DIALOGUE_WAIT_TO_FINISH
					IF NOT IS_ANY_DIALOGUE_PLAYING(DriverID, FALSE)
						iSpeechToPlay = TAXI_DIALOGUE_NOTHING
					ENDIF
				ENDIF
			ENDIF
		//ELSE
			//iSpeechToPlay = TAXI_DIALOGUE_NOTHING	
		ENDIF
	ENDIF
ENDPROC

PROC GET_GROUP_INTO_CAB(INT iTargetSeat)
	INT i, j
	PED_INDEX GroupPedID
	SEQUENCE_INDEX seq
	// if any group members they should also get in taxi
	IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
		PRINTLN("<",GET_THIS_SCRIPT_NAME(), "> - DOES_GROUP_EXIST(PLAYER_GROUP_ID())")
		GET_GROUP_SIZE(PLAYER_GROUP_ID(), j, i)
		IF (i > 0)
		
			GroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
			IF NOT IS_PED_INJURED(GroupPedID)
				PRINTLN("<",GET_THIS_SCRIPT_NAME(),"> - IF NOT IS_PED_INJURED(GroupPedID)")
				CLEAR_PED_TASKS(GroupPedID)
				OPEN_SEQUENCE_TASK(seq)
				IF (iTargetSeat = 1)
					TASK_ENTER_VEHICLE(NULL, g_WaitingTaxi, DEFAULT_TIME_BEFORE_WARP, VS_BACK_RIGHT)
					#IF IS_DEBUG_BUILD
						printDebugString("TAXI TRIGGER - buddy entering taxi as passenger 2a \n")
					#endif											
				ELSE
					TASK_ENTER_VEHICLE(NULL, g_WaitingTaxi, DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT)
					#IF IS_DEBUG_BUILD
						printDebugString("TAXI TRIGGER - buddy entering taxi as passenger 2b \n")
					#endif											
				ENDIF												
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(GroupPedID, seq)
				CLEAR_SEQUENCE_TASK(seq)	
			ENDIF 
			IF (i > 1)
				GroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
				IF NOT IS_PED_INJURED(GroupPedID)
					OPEN_SEQUENCE_TASK(seq)
						TASK_ENTER_VEHICLE(NULL, g_WaitingTaxi, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)	
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(GroupPedID, seq)
					CLEAR_SEQUENCE_TASK(seq)	
					printDebugString("TAXI TRIGGER - buddy entering taxi as front right \n")
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC CANCEL_GROUP_GETTING_INTO_CAB()
	INT i, j
	PED_INDEX GroupPedID
	// if any group members they should also get in taxi
	IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
		GET_GROUP_SIZE(PLAYER_GROUP_ID(), j, i)
		IF (i > 0)
			GroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
			IF NOT IS_PED_INJURED(GroupPedID)
				CLEAR_PED_TASKS(GroupPedID)											
				IF NOT IS_ENTITY_DEAD(g_WaitingTaxi)
					IF IS_PED_IN_VEHICLE(GroupPedID, g_WaitingTaxi)
						TASK_LEAVE_VEHICLE(GroupPedID, g_WaitingTaxi, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
					ENDIF
				ENDIF
			ENDIF 
			IF (i > 1)
			
				GroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
				IF NOT IS_PED_INJURED(GroupPedID)
					CLEAR_PED_TASKS(GroupPedID)											
					IF NOT IS_ENTITY_DEAD(g_WaitingTaxi)
						IF IS_PED_IN_VEHICLE(GroupPedID, g_WaitingTaxi)
							TASK_LEAVE_VEHICLE(GroupPedID, g_WaitingTaxi, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_BLIP_A_CUSTOM_MAP_BLIP//(BLIP_INDEX &inBlip, VECTOR &inCoord, FLOAT &inHeading)

////	// manhattan gun shop
//	IF (inBlip = g_mapBlips[MB_GUNS_MH].blip)
//		inCoord   = <<60.5485, -349.2893, 13.6821>>
//		inHeading = 347.5195
//		RETURN(TRUE)
//	ENDIF

	RETURN(FALSE)

ENDFUNC

PROC RAISE_COORDS_IF_IN_SPECIFIC_RAISED_ARED(VECTOR &inCoord)

	IF ARE_COORDS_IN_3D_AREA(inCoord, <<827.205, -22.4701, -1000.0>> , <<870.0048, 40.8799, -100.0>>)
		#IF IS_DEBUG_BUILD
			printDebugString("ARE_COORDS_IN_SPECIFIC_RAISED_ARED = raised to 25.0 \n")
		#endif 		
		inCoord.z = 25.0
	ENDIF								  

ENDPROC

FUNC BOOL ARE_COORDS_IN_A_SINGLE_LANE_AREA(VECTOR inCoord)
	IF inCoord.x > 0 ENDIF // For the bloody compiler
	
//	// charge island
//	IF ARE_COORDS_IN_3D_AREA(inCoord, <<433.0, 576.0, -1000.0>>, <<701.8750, 1144.0, -100.0>>)
//		#IF IS_DEBUG_BUILD
//			printDebugString("ARE_COORDS_IN_A_SINGLE_LANE_AREA = TRUE - charge island \n")
//		#endif
//		RETURN(TRUE)
//	ENDIF

	RETURN(FALSE)

ENDFUNC

FUNC BOOL ARE_COORDS_IN_DISABLE_NODES_AREA(VECTOR vInCoords)

	// down at docks
	IF ARE_COORDS_IN_3D_AREA(vInCoords, <<-1242.8979, 245.2448, -1000.0>>, <<-898.5730, 577.4948, -100.0>>)
		#IF IS_DEBUG_BUILD
				printDebugString("ARE_COORDS_IN_DISABLE_NODES_AREA = TRUE - docks area \n")
		#endif 
		RETURN(TRUE)
	ENDIF

	RETURN(FALSE)

ENDFUNC

//FUNC BOOL IS_COORD_IN_ALLOWED_INTERIOR(VECTOR vInCoord)
//	IF (GET_INTERIOR_AT_COORDS (vInCoord) = NULL)
//	INTERIOR_INSTANCE_INDEX
//	ENDIF
//	RETURN FALSE
//ENDFUNC

FUNC BOOL IS_NODE_COORD_SUITABLE_FOR_TAXI(VECTOR vBlipCoord, VECTOR vInCoord)
	
//	IF NOT (GET_INTERIOR_AT_COORDS (vInCoord) = NULL)
//		printDebugString("IS_NODE_COORD_SUITABLE_FOR_TAXI - FALSE - in interior \n ")
//		
//		RETURN(FALSE)
//	ELSE
		IF (vInCoord.z > 0.0)
			VECTOR vec
			vec = vBlipCoord - vInCoord
			vec.z = 0.0
			IF (VMAG2(vec) < (MINIMUM_DROP_OFF_DISTANCE_TO_COORD_WARPING * MINIMUM_DROP_OFF_DISTANCE_TO_COORD_WARPING))
			
				printDebugString("IS_NODE_COORD_SUITABLE_FOR_TAXI - FALSE - too close \n ")
				
				RETURN(FALSE)
			ENDIF 
		ELSE   
		
			printDebugString("IS_NODE_COORD_SUITABLE_FOR_TAXI - FALSE - z less than zero \n ")
			
			RETURN(FALSE)
		ENDIF
//	ENDIF
	
	printDebugString("IS_NODE_COORD_SUITABLE_FOR_TAXI - TRUE \n ")
	
	RETURN(TRUE)
ENDFUNC

PROC ROTATE_VECTOR(VECTOR &InVec, VECTOR rotation)
	
	FLOAT CosAngle
	FLOAT SinAngle
	VECTOR ReturnVec

	// Rotation about the x axis 
	CosAngle = COS(rotation.x)
	SinAngle = SIN(rotation.x)
	ReturnVec.x = InVec.x
	ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
	ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
	InVec = ReturnVec

	// Rotation about the y axis
	CosAngle = COS(rotation.y)
	SinAngle = SIN(rotation.y)
	ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
	ReturnVec.y = InVec.y
	ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
	InVec = ReturnVec
	
	// Rotation about the z axis 
	CosAngle = COS(rotation.z)
	SinAngle = SIN(rotation.z)
	ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
	ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
	ReturnVec.z = InVec.z
	InVec = ReturnVec

ENDPROC

PROC SHIFT_COORD_TO_THE_RIGHT(VECTOR &vInCoord, FLOAT fInHeading, FLOAT fMetersToTheRight)
	VECTOR vec1
	VECTOR vec2
	vec1 = << 0.0, fMetersToTheRight, 0.0 >>
	vec2 = << 0.0, 0.0, (fInHeading - 90.0) >>
	ROTATE_VECTOR(vec1, vec2)
	vInCoord += vec1
ENDPROC

FUNC BOOL IS_BLIP_VISIBLE(BLIP_INDEX inBlip)
	BLIP_DISPLAY ThisBlipDisplay
	IF DOES_BLIP_EXIST(inBlip)
		ThisBlipDisplay = GET_BLIP_INFO_ID_DISPLAY(inBlip)
		IF NOT (ThisBlipDisplay = DISPLAY_NOTHING) 
		AND NOT (ThisBlipDisplay = DISPLAY_MARKER) 
			RETURN(TRUE)
		ENDIF 
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC DISABLE_TAXI_HAILING(BOOL bDisable = TRUE)
	IF bDisable
		g_sTaxiHailingDisabledByThisScript = GET_THIS_SCRIPT_NAME()
		g_bTaxiHailingIsDisabled = TRUE
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : DISABLE_TAXI_HAILING() - g_bTaxiHailingIsDisabled = TRUE g_sTaxiHailingDisabledByThisScript = ", g_sTaxiHailingDisabledByThisScript)
	ELSE
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : DISABLE_TAXI_HAILING() - g_bTaxiHailingIsDisabled = FALSE g_sTaxiHailingDisabledByThisScript = ", g_sTaxiHailingDisabledByThisScript)
		g_sTaxiHailingDisabledByThisScript = "NULL"
		g_bTaxiHailingIsDisabled = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player is allowed to enter a taxi as a passenger
///    checks status of g_bTaxiHailingIsUnlocked & g_bTaxiHailingIsDisabled
/// RETURNS:
///    TRUE if allowed to enter a taxi as a passenger
FUNC BOOL IS_PLAYER_ALLOWED_TO_RIDE_IN_TAXIS()

	IF NOT g_bTaxiHailingIsUnlocked
		RETURN FALSE
	ENDIF
	
	IF g_bTaxiHailingIsDisabled
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

FUNC INT COUNT_CONTACT_BLIPS_ON_RADAR()
	INT i
	INT iCount
	BLIP_INDEX TempBlip
	iCount = 0
   	REPEAT COUNT_OF(BLIP_SPRITE) i
		TempBlip = GET_FIRST_BLIP_INFO_ID(INT_TO_ENUM(BLIP_SPRITE, i))
		WHILE DOES_BLIP_EXIST(TempBlip)
			eRADAR_BLIP_TYPE BlipType	
			BlipType = GET_BLIP_INFO_ID_TYPE(TempBlip)
			IF (BlipType = BLIPTYPE_CONTACT)
				iCount++
			ENDIF
			TempBlip = GET_NEXT_BLIP_INFO_ID(INT_TO_ENUM(BLIP_SPRITE, i))
		ENDWHILE
	ENDREPEAT
	RETURN(iCount)
ENDFUNC

FUNC BOOL IS_BLIP_FRIENDLY(BLIP_INDEX radarBlip)
	IF GET_BLIP_HUD_COLOUR(radarBlip) = HUD_COLOUR_BLUE
	OR GET_BLIP_HUD_COLOUR(radarBlip) = HUD_COLOUR_BLUEDARK
	OR GET_BLIP_HUD_COLOUR(radarBlip) = HUD_COLOUR_BLUELIGHT
	OR GET_BLIP_HUD_COLOUR(radarBlip) = HUD_COLOUR_MENU_BLUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if blipIndex's colour is allowed for a standard blip (mission blip) 
/// RETURNS:
///    TRUE if blip is blue(friendly entity) or yellow (destination)
FUNC BOOL IS_BLIP_COLOUR_ALLOWED_FOR_STANDARD_BLIP(BLIP_INDEX &blipIndex)

	IF DOES_BLIP_EXIST(blipIndex)
		HUD_COLOURS eBlipColour = GET_BLIP_HUD_COLOUR(blipIndex)
			
		// support blue blips
		IF eBlipColour = HUD_COLOUR_BLUE
		OR eBlipColour = HUD_COLOUR_BLUEDARK
		OR eBlipColour = HUD_COLOUR_BLUELIGHT
		OR eBlipColour = HUD_COLOUR_MENU_BLUE
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " IS_BLIP_COLOUR_ALLOWED_FOR_STANDARD_BLIP() : return TRUE : blip using HUD_COLOUR_BLUE...") 
			RETURN TRUE
		// support yellow blips
		ELIF eBlipColour = HUD_COLOUR_YELLOW
		OR eBlipColour = HUD_COLOUR_YELLOWDARK
		OR eBlipColour = HUD_COLOUR_YELLOWLIGHT
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " IS_BLIP_COLOUR_ALLOWED_FOR_STANDARD_BLIP() : return TRUE : blip using HUD_COLOUR_YELLOW...")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT COUNT_MISSION_BLIPS_ON_RADAR()
	
	INT iCount
	BLIP_INDEX tempBlip
	iCount = 0
	
	tempBlip = GET_FIRST_BLIP_INFO_ID(taxiStandardBlipSprite)
	WHILE DOES_BLIP_EXIST(tempBlip)
		IF IS_BLIP_VISIBLE(tempBlip)
			IF (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_COORDS)
			OR (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_PICKUP)
			OR (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_OBJECT) 
				iCount++
//				PRINTSTRING("<")
//				PRINTSTRING(GET_THIS_SCRIPT_NAME())
//				PRINTSTRING("> COUNT_MISSION_BLIPS_ON_RADAR() taxiStandardBlipSprite found. iCount = ")
//				PRINTINT(iCount)
//				PRINTNL()
							
			ENDIF
			IF (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_CHAR)
			OR (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_VEHICLE)
				IF IS_BLIP_FRIENDLY(tempBlip)
					iCount++
//					PRINTSTRING("<")
//					PRINTSTRING(GET_THIS_SCRIPT_NAME())
//					PRINTSTRING("> COUNT_MISSION_BLIPS_ON_RADAR() FRIENDLY taxiStandardBlipSprite found. iCount = ")
//					PRINTINT(iCount)
//					PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		tempBlip = GET_NEXT_BLIP_INFO_ID(taxiStandardBlipSprite)
	ENDWHILE
	
	tempBlip = GET_FIRST_BLIP_INFO_ID(taxiWaypointBlipSprite)
	WHILE DOES_BLIP_EXIST(tempBlip)
		IF IS_BLIP_VISIBLE(tempBlip)
			IF (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_COORDS)
				iCount++
//				PRINTSTRING("<")
//				PRINTSTRING(GET_THIS_SCRIPT_NAME())
//				PRINTSTRING("> COUNT_MISSION_BLIPS_ON_RADAR() taxiWaypointBlipSprite found. iCount = ")
//				PRINTINT(iCount)
//				PRINTNL()
			ENDIF
		ENDIF
		tempBlip = GET_NEXT_BLIP_INFO_ID(taxiWaypointBlipSprite)
	ENDWHILE

	RETURN(iCount)

ENDFUNC

FUNC INT COUNT_ALL_OTHER_BLIPS_ON_RADAR()
	INT i
	INT iCount
	BLIP_INDEX TempBlip
	iCount = 0

	// these relate to the enum in commands_ny, we are checking for all blip sprites between these 2 values.
	INT iStartSprite = ENUM_TO_INT(RADAR_TRACE_OBJECTIVE)
	INT iEndSprite = ENUM_TO_INT(MAX_RADAR_TRACES)

   	REPEAT COUNT_OF(BLIP_SPRITE) i
		IF (i >= iStartSprite)
		AND (i <= iEndSprite)
			TempBlip = GET_FIRST_BLIP_INFO_ID(INT_TO_ENUM(BLIP_SPRITE, i))
			WHILE DOES_BLIP_EXIST(TempBlip)
				eRADAR_BLIP_TYPE BlipType	
				BlipType = GET_BLIP_INFO_ID_TYPE(TempBlip)
				IF NOT (BlipType = BLIPTYPE_CONTACT)
					iCount++
				ENDIF
				TempBlip = GET_NEXT_BLIP_INFO_ID(INT_TO_ENUM(BLIP_SPRITE, i))
			ENDWHILE
		ENDIF
	ENDREPEAT
//	PRINTSTRING("<")
//	PRINTSTRING(GET_THIS_SCRIPT_NAME())
//	PRINTSTRING("> - COUNT_ALL_OTHER_BLIPS_ON_RADAR() = ")
//	PRINTINT(iCount)
//	PRINTNL()
	RETURN(iCount)
ENDFUNC

/// PURPOSE:
///    clears the g_CustomDropOffBlip if it's doesn't exist
PROC CLEAR_CUSTOM_DROP_OFF()
	// clear custom blip drop off if it doesn't exist
	IF NOT (g_CustomDropOffBlip = NULL)
		IF NOT DOES_BLIP_EXIST(g_CustomDropOffBlip)
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : CLEAR_CUSTOM_DROP_OFF() set g_CustomDropOffBlip = NULL")
			g_CustomDropOffBlip = NULL 
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    checks to see if this vehicle is suitable for player to enter as passenger
/// PARAMS:
///    vehicleIndex - the vehicle to check
///    mnTaxi - what model the taxi needs to be
///    mnTaxiDriver - what model the taxi driver needs to be
/// RETURNS:
///    TRUE if vehicle is suitable for player to enter
FUNC BOOL IS_PLAYER_OK_TO_ENTER_VEHICLE_AS_PASSENGER(VEHICLE_INDEX &vehicleIndex, MODEL_NAMES mnTaxi, MODEL_NAMES mnTaxiDriver)

	PED_INDEX pedIndex
	LOCK_STATE vehLockState
	
	IF DOES_ENTITY_EXIST(vehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
			IF (GET_ENTITY_MODEL(vehicleIndex) = mnTaxi)
				pedIndex = GET_PED_IN_VEHICLE_SEAT(vehicleIndex)
				IF DOES_ENTITY_EXIST(pedIndex)
					IF NOT IS_PED_INJURED(pedIndex)
						IF NOT IS_PED_FLEEING(pedIndex)
							IF NOT IS_PED_IN_COMBAT(pedIndex)
								IF IS_PED_MODEL(pedIndex, mnTaxiDriver)
									IF IS_ENTITY_UPRIGHT(vehicleIndex)
										vehLockState = GET_VEHICLE_DOOR_LOCK_STATUS(vehicleIndex)
										IF (vehLockState != VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
										AND (vehLockState != VEHICLELOCK_LOCKED)
											RETURN TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF						
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    checks to see if this vehicle is suitable for player to enter as passenger
/// PARAMS:
///    vehicleIndex - the vehicle to check
///    pedIndex - the vehicle's driver
///    mnTaxi - what model the taxi needs to be
///    mnTaxiDriver - what model the taxi driver needs to be
/// RETURNS:
///    TRUE if vehicle is suitable for player to enter
FUNC BOOL IS_TAXI_AND_DRIVER_OK_FOR_PLAYER_TO_ENTER_AS_PASSENGER(VEHICLE_INDEX &vehicleIndex, PED_INDEX &pedIndex, MODEL_NAMES mnTaxi, MODEL_NAMES mnTaxiDriver)

	IF DOES_ENTITY_EXIST(vehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
			IF DOES_ENTITY_EXIST(pedIndex)
				IF NOT IS_PED_INJURED(pedIndex)
					IF NOT IS_PED_FLEEING(pedIndex)
						IF NOT IS_PED_IN_COMBAT(pedIndex)
							IF IS_PED_SITTING_IN_VEHICLE(pedIndex, vehicleIndex)
								IF GET_ENTITY_MODEL(vehicleIndex) = mnTaxi
									IF IS_PED_MODEL(pedIndex, mnTaxiDriver)
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF					
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handle getting the player's group members into the taxi
/// PARAMS:
///    vehIndexTaxi - the taxi entering NOTE: doesn't perform safety checks on vehicle
///    eReservedPlayerSeat - Seat which player will be using
PROC SET_PLAYER_GROUP_MEMBERS_INTO_TAXI(VEHICLE_INDEX &vehIndexTaxi, VEHICLE_SEAT eReservedPlayerSeat)

	INT i, j
	PED_INDEX GroupPedID
	PED_INDEX tempPed
	BOOL bApplyEnterTask
	VEHICLE_SEAT eTargetSeat
	
	IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())	
		GET_GROUP_SIZE(PLAYER_GROUP_ID(), j, i)

		// take first member into the other back seat
		IF (i > 0)
			
			GroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
			
			IF NOT IS_PED_INJURED(GroupPedID)
			
				// set target seat (opposite to player's reserved seat)
				eTargetSeat = VS_BACK_LEFT
				
				IF eReservedPlayerSeat = VS_BACK_LEFT
					eTargetSeat = VS_BACK_RIGHT
				ENDIF

				bApplyEnterTask = TRUE				
				
				// don't allow task if needing to jack player, or if ped is already in the seat / entering
				tempPed = GET_PED_IN_VEHICLE_SEAT(vehIndexTaxi, eTargetSeat)
				
				IF DOES_ENTITY_EXIST(tempPed)
				
					// player already sitting in the seat
					IF (tempPed = PLAYER_PED_ID())
						bApplyEnterTask = FALSE
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, player sat 1st group member eTargetSeat : ", eTargetSeat)
						
					// ped is already sitting in the seat
					ELIF (tempPed = GroupPedID)
						bApplyEnterTask = FALSE
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 1st group member already sat in eTargetSeat : ", eTargetSeat)
					ENDIF
				ENDIF
				
				// don't apply task if config flag is set
				IF GET_PED_CONFIG_FLAG(GroupPedID, PCF_DontEnterVehiclesInPlayersGroup, FALSE)
					bApplyEnterTask = FALSE
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 1st group member PCF_DontEnterVehiclesInPlayersGroup is set eTargetSeat : ", eTargetSeat)
				ENDIF
				
				// don't apply task if he's already getting in the seat
				IF TAXI_IS_PED_PERFORMING_TASK(GroupPedID, SCRIPT_TASK_ENTER_VEHICLE)
					bApplyEnterTask = FALSE
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 1st group member already running enter vehicle task")
				ENDIF
				
				IF bApplyEnterTask
					//CLEAR_PED_TASKS(GroupPedID)
					TASK_ENTER_VEHICLE(GroupPedID, vehIndexTaxi, DEFAULT_TIME_BEFORE_WARP, eTargetSeat, PEDMOVEBLENDRATIO_RUN, ECF_RESUME_IF_INTERRUPTED | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_JACK_ANYONE)
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 1st group member task applied eTargetSeat : ", eTargetSeat)
				ENDIF
			ENDIF 
			
			// if group has two member set second member into front passenger seat
			IF (i > 1)
			
				GroupPedID = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
				
				IF NOT IS_PED_INJURED(GroupPedID)
				
					bApplyEnterTask = TRUE			
					eTargetSeat = VS_FRONT_RIGHT
				
					// don't allow task if needing to jack player, or if ped is already in the seat / entering
					tempPed = GET_PED_IN_VEHICLE_SEAT(vehIndexTaxi, eTargetSeat)
					
					IF DOES_ENTITY_EXIST(tempPed)
					
						// player already sitting in the seat
						IF (tempPed = PLAYER_PED_ID())
							bApplyEnterTask = FALSE
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, player sat 2nd group member eTargetSeat : ", eTargetSeat)
							
						// ped is already sitting in the seat
						ELIF (tempPed = GroupPedID)
							bApplyEnterTask = FALSE
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 2nd group member already sat in eTargetSeat : ", eTargetSeat)
						ENDIF
					ENDIF
					
					// don't apply task if config flag is set
					IF GET_PED_CONFIG_FLAG(GroupPedID, PCF_DontEnterVehiclesInPlayersGroup, FALSE)
						bApplyEnterTask = FALSE
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 2nd group member PCF_DontEnterVehiclesInPlayersGroup is set eTargetSeat : ", eTargetSeat)
					ENDIF
						
					// don't apply task if he's already getting in the seat
					IF TAXI_IS_PED_PERFORMING_TASK(GroupPedID, SCRIPT_TASK_ENTER_VEHICLE)
						/*IF IS_PED_GETTING_INTO_A_VEHICLE(GroupPedID)
							IF (GET_VEHICLE_PED_IS_ENTERING(GroupPedID) = vehTempPlayer)						
							ENDIF
						ENDIF*/
						bApplyEnterTask = FALSE
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 2nd group member already running enter vehicle task")
					ENDIF
					
					IF bApplyEnterTask
						//CLEAR_PED_TASKS(GroupPedID)
						TASK_ENTER_VEHICLE(GroupPedID, vehIndexTaxi, DEFAULT_TIME_BEFORE_WARP, eTargetSeat, PEDMOVEBLENDRATIO_RUN, ECF_RESUME_IF_INTERRUPTED | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_JACK_ANYONE)
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : SET_PLAYER_GROUP_MEMBERS_INTO_TAXI : NOT tasked, 2nd group member task applied eTargetSeat : ", eTargetSeat)
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC
