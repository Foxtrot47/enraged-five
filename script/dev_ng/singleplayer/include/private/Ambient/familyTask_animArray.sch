///private header for family task control scripts
///    alwyn.roberts@rockstarnorth.com
///    

USING "commands_task.sch"

// *******************************************************************************************
//	FAMILY ANIM ARRAY TASKS PRIVATE FUNCTIONS
// *******************************************************************************************

PROC PRIVATE_TaskPlayAnimArrayAdvanced(PED_INDEX PedIndex,
		TEXT_LABEL_63 &pAnimDictNames[],  TEXT_LABEL_63 &pAnimClipNames[], INT iFamilyAnims,
		VECTOR pos, VECTOR rot,
		TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		FLOAT fBlendInDelta = NORMAL_BLEND_IN, FLOAT fBlendOutDelta = NORMAL_BLEND_OUT,
		INT nTimeToPlay =-1,
		ANIMATION_FLAGS AnimFlags = AF_DEFAULT, FLOAT startPhase = 0.0,
		EULER_ROT_ORDER RotOrder = EULER_YXZ,
		IK_CONTROL_FLAGS ikFlags = AIK_NONE)
	
	INT iMAX_FamilyAnims = iFamilyAnims
	IF NOT IS_STRING_NULL_OR_EMPTY(tAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(tAnimClip)
		
		CPRINTLN(DEBUG_FAMILY, "old Anim scene: \"", tAnimDict, "\", \"", tAnimClip, "\"")
		
		INT iAnims
		REPEAT (iFamilyAnims+1) iAnims
			CPRINTLN(DEBUG_FAMILY, "	Anim scene[", iAnims, "]: \"", pAnimDictNames[iAnims], "\", \"", pAnimClipNames[iAnims], "\"")
			
			IF ARE_STRINGS_EQUAL(tAnimDict, pAnimDictNames[iAnims])
			AND ARE_STRINGS_EQUAL(tAnimClip, pAnimClipNames[iAnims])
				IF (iAnims <> 0)
					INT iNewAnims
					FOR iNewAnims = iAnims TO COUNT_OF(pAnimDictNames)-1
						IF ((iNewAnims+1) < COUNT_OF(pAnimDictNames))
							pAnimDictNames[iNewAnims] = pAnimDictNames[iNewAnims+1]
							pAnimClipNames[iNewAnims] = pAnimClipNames[iNewAnims+1]
						ELSE
							pAnimDictNames[iNewAnims] = ""
							pAnimClipNames[iNewAnims] = ""
						ENDIF
					ENDFOR
					
					iMAX_FamilyAnims--
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		CPRINTLN(DEBUG_FAMILY, "")
		
	ENDIF
	
	INT iAnimName = GET_RANDOM_INT_IN_RANGE(0, iMAX_FamilyAnims+1)
	
	TEXT_LABEL_63 pAnimDictName = pAnimDictNames[iAnimName]
	TEXT_LABEL_63 pAnimClipName = pAnimClipNames[iAnimName]
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pAnimDictName)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskPlayAnimArrayAdvanced(PedIndex, \"", pAnimDictName, "\", \"", pAnimClipName, "\": ", iAnimName, ")")
		#ENDIF
		
		REQUEST_ANIM_DICT(pAnimDictName)
		IF NOT HAS_ANIM_DICT_LOADED(pAnimDictName)
			REQUEST_ANIM_DICT(pAnimDictName)
		ELSE
			IF IS_BITMASK_ENUM_AS_ENUM_SET(AnimFlags, AF_LOOPING)
				CLEAR_BITMASK_ENUM_AS_ENUM(AnimFlags, AF_LOOPING)
			ELSE
				//
			ENDIF
			IF IS_BITMASK_ENUM_AS_ENUM_SET(AnimFlags, AF_HOLD_LAST_FRAME)
				//
			ELSE
				SET_BITMASK_ENUM_AS_ENUM(AnimFlags, AF_HOLD_LAST_FRAME)
			ENDIF
		
			IF IS_BITMASK_ENUM_AS_ENUM_SET(AnimFlags, AF_USE_KINEMATIC_PHYSICS)
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
				SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
			ENDIF
		/*	IF IS_BITMASK_ENUM_AS_ENUM_SET(AnimFlags, AF_DISABLE_LEG_IK)	//1144977
				SET_BITMASK_ENUM_AS_ENUM(ikFlags, AIK_DISABLE_LEG_IK)
				CLEAR_BITMASK_ENUM_AS_ENUM(AnimFlags, AF_DISABLE_LEG_IK)
			ENDIF */
			
			TASK_PLAY_ANIM_ADVANCED(PedIndex, pAnimDictName, pAnimClipName, pos, rot,
					fBlendInDelta, fBlendOutDelta, nTimeToPlay, AnimFlags,
					startPhase, RotOrder, ikFlags)
			SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			
			tAnimDict = pAnimDictName
			tAnimClip = pAnimClipName
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskPlayAnimArrayAdvanced(PedIndex, \"", "null", "\": ", iAnimName, ")")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_Get_Family_AnimArray(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		TEXT_LABEL_63 &tFamilySeqAnimDicts[6], TEXT_LABEL_63 &tFamilySeqAnimClips[6],
		INT &iFamilyAnims,
		FLOAT &fBlendInDelta, FLOAT &fBlendOutDelta, ANIMATION_FLAGS &eFamilyAnimFlag)
	
	fBlendInDelta = SLOW_BLEND_IN
	fBlendOutDelta = SLOW_BLEND_OUT
	
	TEXT_LABEL_63 tFamilySeqAnimDict
	TEXT_LABEL_63 tFamilySeqAnimRoot
	
	enumFamilyAnimProgress eFamilyAnimProgress
	
	IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
			tFamilySeqAnimDict, tFamilySeqAnimRoot, eFamilyAnimFlag, eFamilyAnimProgress)
		
		#IF IS_DEBUG_BUILD
		IF (eFamilyAnimProgress <> FAP_3_array)
			#IF NOT IS_JAPANESE_BUILD
			IF (eFamilyEvent <> FE_T0_TREVOR_doing_a_shit)		//#1113608
			#ENDIF
				TEXT_LABEL_63 str = ("eFamAnimProg <> FAP_3_array for ")
				str += Get_String_From_FamilyEvent(eFamilyEvent)
				SCRIPT_ASSERT(str)
			#IF NOT IS_JAPANESE_BUILD
			ENDIF
			#ENDIF
		ENDIF
		#ENDIF
		
		SWITCH eFamilyEvent
			CASE FE_M_FAMILY_on_laptops
			CASE FE_M7_SON_on_laptop_looking_for_jobs
			
				//	TIMETABLE@AMANDA@IG_12/amanda_base
				//	TIMETABLE@AMANDA@IG_12/amanda_idle_a
				//	TIMETABLE@AMANDA@IG_12/amanda_idle_b
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				
				IF (eFamilyMember <> FM_MICHAEL_WIFE)
					tFamilySeqAnimClips[0]  = tFamilySeqAnimRoot
					tFamilySeqAnimClips[0] += "base"
					
					tFamilySeqAnimClips[1]  = tFamilySeqAnimRoot
					tFamilySeqAnimClips[1] += "idle_a"
				
					tFamilySeqAnimClips[2]  = tFamilySeqAnimRoot
					tFamilySeqAnimClips[2] += "idle_b"
				ELSE
					tFamilySeqAnimClips[0]  = "base"
					tFamilySeqAnimClips[0] += tFamilySeqAnimRoot
					
					tFamilySeqAnimClips[1]  = "idle_a"
					tFamilySeqAnimClips[1] += tFamilySeqAnimRoot
					
					tFamilySeqAnimClips[2]  = "idle_b"
					tFamilySeqAnimClips[2] += tFamilySeqAnimRoot
				ENDIF
				
				iFamilyAnims			= 2
				
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			
			CASE FE_M7_FAMILY_finished_breakfast
			CASE FE_M7_FAMILY_finished_pizza
				
				/*
				eat_takeout
				eating_disorder
				getting_fit
				your_boyfriend
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "eat_takeout"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "eating_disorder"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "getting_fit"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "your_boyfriend"
				
				SWITCH eFamilyMember
					CASE FM_MICHAEL_WIFE
						tFamilySeqAnimClips[0] += "_Amanda"
						tFamilySeqAnimClips[1] += "_Amanda"
						tFamilySeqAnimClips[2] += "_Amanda"
						tFamilySeqAnimClips[3] += "_Amanda"
					BREAK
					CASE FM_MICHAEL_SON
						tFamilySeqAnimClips[0] += "_Jimmy"
						tFamilySeqAnimClips[1] += "_Jimmy"
						tFamilySeqAnimClips[2] += "_Jimmy"
						tFamilySeqAnimClips[3] += "_Jimmy"
					BREAK
					CASE FM_MICHAEL_DAUGHTER
						tFamilySeqAnimClips[0] += "_Tracy"
						tFamilySeqAnimClips[1] += "_Tracy"
						tFamilySeqAnimClips[2] += "_Tracy"
						tFamilySeqAnimClips[3] += "_Tracy"
					BREAK
				ENDSWITCH
				
				
				iFamilyAnims		  = 3
				
				
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			CASE FE_M7_FAMILY_watching_TV
				
				/*
				base
				IsThisTheBest
				ShouldntYouGuys
				watching_this
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "IsThisTheBest"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "ShouldntYouGuys"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "watching_this"
				
				SWITCH eFamilyMember
					CASE FM_MICHAEL_WIFE
						tFamilySeqAnimClips[0] += "_Amanda"
						tFamilySeqAnimClips[1] += "_Amanda"
						tFamilySeqAnimClips[2] += "_Amanda"
						tFamilySeqAnimClips[3] += "_Amanda"
					BREAK
					CASE FM_MICHAEL_SON
						tFamilySeqAnimClips[0] += "_Jimmy"
						tFamilySeqAnimClips[1] += "_Jimmy"
						tFamilySeqAnimClips[2] += "_Jimmy"
						tFamilySeqAnimClips[3] += "_Jimmy"
					BREAK
					CASE FM_MICHAEL_DAUGHTER
						tFamilySeqAnimClips[0] += "_Tracy"
						tFamilySeqAnimClips[1] += "_Tracy"
						tFamilySeqAnimClips[2] += "_Tracy"
						tFamilySeqAnimClips[3] += "_Tracy"
					BREAK
				ENDSWITCH
				
				
				iFamilyAnims		  = 3
				
				
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			CASE FE_M_SON_sleeping
			
			//	TIMETABLE@JIMMY@IG_3@BASE/Base.anim
			//	TIMETABLE@JIMMY@IG_3@IDLE_A/Idle_a.anim
			//	TIMETABLE@JIMMY@IG_3@IDLE_A/Idle_b.anim
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@SLEEPING"
				tFamilySeqAnimClips[0] = "Base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@BASE"
				tFamilySeqAnimClips[1] = "Base"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2] = "Idle_a"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3] = "Idle_b"
				
				iFamilyAnims			= 3
				
				fBlendInDelta = INSTANT_BLEND_IN
				fBlendOutDelta = INSTANT_BLEND_OUT
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			CASE FE_M_SON_smoking_weed_in_a_bong
				tFamilySeqAnimDicts[0] = "TIMETABLE@JIMMY@IG_1@BASE"
				tFamilySeqAnimClips[0] = "Base"
				
				tFamilySeqAnimDicts[1] = "TIMETABLE@JIMMY@IG_1@IDLE_A"
				tFamilySeqAnimClips[1] = "Thats_Some_Good_Shit"
				
				tFamilySeqAnimDicts[2] = "TIMETABLE@JIMMY@IG_1@IDLE_A"
				tFamilySeqAnimClips[2] = "Whole_Chamber_Dog"
				
				tFamilySeqAnimDicts[3] = "TIMETABLE@JIMMY@IG_1@IDLE_A"
				tFamilySeqAnimClips[3] = "Hydrotropic_Bud_Or_Something"
				
				tFamilySeqAnimDicts[4] = "TIMETABLE@JIMMY@IG_1@IDLE_A"
				tFamilySeqAnimClips[4] = "Dont_Dudge_Me_Dad"
				
//				tFamilySeqAnimDicts[5] = "TIMETABLE@JIMMY@IG_1@IDLE_B"
//				tFamilySeqAnimClips[5] = "Hot_I_Need_Ice"
				
				iFamilyAnims			= 4
			BREAK
			CASE FE_M_SON_in_room_asks_for_munchies
			
			//	TIMETABLE@JIMMY@IG_3@SLEEPING\Base.anim
			//	TIMETABLE@JIMMY@IG_3@SLEEPING\Body_High2.anim
			//	TIMETABLE@JIMMY@IG_3@SLEEPING\Bring_Me_Some_Food.anim
			//	TIMETABLE@JIMMY@IG_3@SLEEPING\Grow_This_Strain.anim
			//	TIMETABLE@JIMMY@IG_3@SLEEPING\Helps_My_Glands.anim
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "Base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "Body_High2"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "Grow_This_Strain"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = "Helps_My_Glands"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimClips[4] = "Bring_Me_Some_Food"
				
				iFamilyAnims			= 4
				
				tFamilySeqAnimDicts[5] = ""
			BREAK
			CASE FE_M7_SON_watching_TV_with_tracey
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "MICS3_15_BASE"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "IDLE_A"
				
				SWITCH eFamilyMember
					CASE FM_MICHAEL_SON
						tFamilySeqAnimClips[0] += "_Jimmy"
						tFamilySeqAnimClips[1] += "_Jimmy"
					BREAK
					CASE FM_MICHAEL_DAUGHTER
						tFamilySeqAnimClips[0] += "_Tracy"
						tFamilySeqAnimClips[1] += "_Tracy"
					BREAK
				ENDSWITCH
				
				
				iFamilyAnims		  = 1
				
				
				tFamilySeqAnimDicts[2]  = ""
				tFamilySeqAnimDicts[3]  = ""
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			
			CASE FE_M_DAUGHTER_workout_with_mp3
				
				/*
				tFamilyAnimDict = "TIMETABLE@TRACY@IG_5"
				tFamilyAnimClip = ""
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0] = "base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1] = "Idle_a"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2] = "Idle_b"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3] = "Idle_c"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "@IDLE_B"
				tFamilySeqAnimClips[4] = "Idle_d"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "@IDLE_B"
				tFamilySeqAnimClips[5] = "Idle_e"
				
				iFamilyAnims			= 5
				
				
				fBlendInDelta	= NORMAL_BLEND_IN		//FAST_BLEND_IN
				fBlendOutDelta	= NORMAL_BLEND_OUT		//FAST_BLEND_OUT
				
				eFamilyAnimFlag |= AF_HOLD_LAST_FRAME
				
			BREAK
			CASE FE_M_DAUGHTER_dancing_practice
				
				/*
				tFamilyAnimDict = "TIMETABLE@TRACY@IG_8"
				tFamilyAnimClip = ""
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0] = "base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1] = "Idle_a"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2] = "Idle_b"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3] = "Idle_c"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "@IDLE_B"
				tFamilySeqAnimClips[4] = "Idle_d"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "@IDLE_B"
				tFamilySeqAnimClips[5] = "Idle_e"
				
				iFamilyAnims			= 5
				
				
				fBlendInDelta	= NORMAL_BLEND_IN		//FAST_BLEND_IN
				fBlendOutDelta	= NORMAL_BLEND_OUT		//FAST_BLEND_OUT
				
				eFamilyAnimFlag |= AF_HOLD_LAST_FRAME
				
			BREAK
			CASE FE_M_DAUGHTER_watching_TV_sober
				/*
				tFamilyAnimDict = "TIMETABLE@TRACY@IG_2"
				tFamilyAnimClip = ""

				TIMETABLE@TRACY@IG_2@IDLE_A/
				Idle_a.anim
				Idle_b.anim
				Idle_c.anim

				TIMETABLE@TRACY@IG_2@IDLE_B/
				Idle_d.anim
				Idle_e.anim
				
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0] = "base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1] = "Idle_a"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2] = "Idle_b"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3] = "Idle_c"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "@IDLE_B"
				tFamilySeqAnimClips[4] = "Idle_d"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "@IDLE_B"
				tFamilySeqAnimClips[5] = "Idle_e"
				
				iFamilyAnims			= 5
				
			BREAK
			CASE FE_M_DAUGHTER_watching_TV_drunk
				/*
				tFamilyAnimDict = "TIMETABLE@TRACY@IG_15"
				tFamilyAnimClip = ""

				TIMETABLE@TRACY@IG_15@BASE
				base.anim
				
				TIMETABLE@TRACY@IG_15@IDLE_A
				idle_a.anim
				idle_b.anim
				idle_c.anim
				
				TIMETABLE@TRACY@IG_15@IDLE_B
				idle_d.anim
				idle_e.anim
				idle_f.anim
				
				TIMETABLE@TRACY@IG_15@IDLE_C
				idle_g.anim
				
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0] = "base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1] = "Idle_a"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2] = "Idle_b"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3] = "Idle_c"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "@IDLE_B"
				tFamilySeqAnimClips[4] = "Idle_d"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "@IDLE_B"
				tFamilySeqAnimClips[5] = "Idle_e"
				
				iFamilyAnims			= 5
				
			BREAK
			CASE FE_M_DAUGHTER_sleeping
			CASE FE_M_DAUGHTER_couchsleep
				/*
			//	TIMETABLE@TRACY@SLEEP@BASE
			//	TIMETABLE@TRACY@SLEEP@IDLE_A
			//	TIMETABLE@TRACY@SLEEP@IDLE_B
			//	TIMETABLE@TRACY@SLEEP@IDLE_C
			//	TIMETABLE@TRACY@SLEEP@IDLE_D
			//	TIMETABLE@TRACY@SLEEP@IDLE_E
			//	TIMETABLE@TRACY@SLEEP@IDLE_F
				
				tFamilyAnimDict = "TIMETABLE@TRACY@SLEEP@"
				tFamilyAnimClip = ""
				
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "Idle_a"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "Idle_b"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = "Idle_c"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimClips[4] = "Idle_d"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimClips[5] = "Idle_e"
				
				iFamilyAnims			= 5
				
				
				
//				tFamilySeqAnimDicts[6] = tFamilySeqAnimDict
//				tFamilySeqAnimClips[6] = "Idle_f"
				
			BREAK
			CASE FE_M_DAUGHTER_purges_in_the_bathroom
				/*
				
				//TIMETABLE@TRACY@IG_7@BASE/BASE.anim
				//TIMETABLE@TRACY@IG_7@IDLE_A/IDLE_A.anim
				//TIMETABLE@TRACY@IG_7@IDLE_A/IDLE_B.anim
				//TIMETABLE@TRACY@IG_7@IDLE_A/IDLE_C.anim
				//TIMETABLE@TRACY@IG_7@IDLE_B/IDLE_D.anim
				
				tFamilyAnimDict = "TIMETABLE@TRACY@IG_7@"
				tFamilyAnimClip = ""

				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "BASE"
				tFamilySeqAnimClips[0]  = "base"
				
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "IDLE_A"
				tFamilySeqAnimClips[1]  = "Idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "IDLE_A"
				tFamilySeqAnimClips[2]  = "Idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "IDLE_A"
				tFamilySeqAnimClips[3]  = "Idle_c"
				
				
				tFamilySeqAnimDicts[4]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "IDLE_B"
				tFamilySeqAnimClips[4]  = "Idle_d"
				
				iFamilyAnims			= 4
				
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_M2_WIFE_doing_yoga
			CASE FE_M7_WIFE_doing_yoga
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "IG_4_BASE"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "IG_4_IDLE"
				
				
				iFamilyAnims			= 1
				
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_M2_WIFE_with_shopping_bags_enter
			CASE FE_M7_WIFE_with_shopping_bags_enter
				/*
			//	TIMETABLE@AMANDA@IG_7/base
			//	TIMETABLE@AMANDA@IG_7/idle_a
			//	TIMETABLE@AMANDA@IG_7/idle_b
			//	TIMETABLE@AMANDA@IG_7/idle_c
			//	TIMETABLE@AMANDA@IG_7/idle_d
			//	TIMETABLE@AMANDA@IG_7/idle_e
			//	TIMETABLE@AMANDA@IG_7/ig_7_enter
			//	TIMETABLE@AMANDA@IG_7/ig_7_exit
				
				tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7"
				tFamilyAnimClip = ""
				
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "idle_a"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "idle_b"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = "idle_c"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimClips[4] = "idle_d"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimClips[5] = "idle_e"
				
				iFamilyAnims			= 5
				
			BREAK
			CASE FE_M7_WIFE_shopping_with_daughter
//			CASE FE_M7_WIFE_shopping_with_son
				/*
			//	TIMETABLE@AMANDA@IG_7/base
			//	TIMETABLE@AMANDA@IG_7/idle_a
			//	TIMETABLE@AMANDA@IG_7/idle_b
			//	TIMETABLE@AMANDA@IG_7/idle_c
			//	TIMETABLE@AMANDA@IG_7/idle_d
			//	TIMETABLE@AMANDA@IG_7/idle_e
			//	TIMETABLE@AMANDA@IG_7/ig_7_enter
			//	TIMETABLE@AMANDA@IG_7/ig_7_exit
				
				tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7"
				tFamilyAnimClip = ""
				
				*/
				
				tFamilySeqAnimDicts[0] = "TIMETABLE@REUNITED@IG_7"		//tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "AmandaBase_Amanda"
				
				iFamilyAnims			= 0
				
				tFamilySeqAnimDicts[1] = ""
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_M_WIFE_gets_drink_in_kitchen
				/*
			//	TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@amanda_gets_drunk_loop1.anim
			//	TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@amanda_gets_drunk_loop2.anim
			//	TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@amanda_gets_drunk_loop3.anim
				
				tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@"
				tFamilyAnimClip = "amanda_gets_drunk_"				
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "amanda_gets_drunk_loop1"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "amanda_gets_drunk_loop2"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "amanda_gets_drunk_loop3"
				
				iFamilyAnims			= 2
				
				
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_M2_WIFE_phones_man_OR_therapist
			CASE FE_M7_WIFE_phones_man_OR_therapist
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = tFamilySeqAnimRoot	//"IG_11_BASE"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "IG_11_IDLE_OhReallyWow"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "IG_11_IDLE_UhHuh"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = "IG_11_IDLE_UmHumYesIKnow"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimClips[4] = "IG_11_IDLE_YouRSoBad"
				
				iFamilyAnims			= 4
				
				
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_M2_MEXMAID_clean_surface_a		FALLTHRU
			CASE FE_M2_MEXMAID_clean_surface_c		FALLTHRU
			CASE FE_M7_MEXMAID_clean_surface		FALLTHRU
//			CASE FE_M_MEXMAID_MIC4_clean_surface
				
				/*
				
				tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE"
				tFamilyAnimClip = ""
				
				//TIMETABLE@MAID@CLEANING_SURFACE@BASE/base"
				//TIMETABLE@MAID@CLEANING_SURFACE@IDLE_A/idle_a"
				//TIMETABLE@MAID@CLEANING_SURFACE@IDLE_A/idle_b"
				//TIMETABLE@MAID@CLEANING_SURFACE@IDLE_A/idle_c"
				
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1]  = "idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2]  = "idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3]  = "idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			CASE FE_M2_MEXMAID_clean_surface_b
				
				/*
				
				tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"
				tFamilyAnimClip = ""
				
				//TIMETABLE@MAID@CLEANING_SURFACE_1@\IG_9_BASE.anim
				//TIMETABLE@MAID@CLEANING_SURFACE_1@\IG_9_ENTER.anim
				//TIMETABLE@MAID@CLEANING_SURFACE_1@\IG_9_EXIT.anim
				//TIMETABLE@MAID@CLEANING_SURFACE_1@\IG_9_IDLE_A.anim
				//TIMETABLE@MAID@CLEANING_SURFACE_1@\IG_9_IDLE_B.anim
				
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "IG_9_BASE"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "IG_9_IDLE_A"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "IG_9_IDLE_B"
				
				iFamilyAnims			= 2
				
				
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			CASE FE_M2_MEXMAID_clean_window	FALLTHRU
			CASE FE_M7_MEXMAID_clean_window	FALLTHRU
			CASE FE_M_MEXMAID_MIC4_clean_window
				
				/*
				
				TIMETABLE@MAID@CLEANING_WINDOW@BASE
				base.anim
				
				TIMETABLE@MAID@CLEANING_WINDOW@ENTER
				enter.anim
				
				TIMETABLE@MAID@CLEANING_WINDOW@EXIT
				exit.anim
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@ENTER"
				tFamilySeqAnimClips[1]  = "enter"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@EXIT"
				tFamilySeqAnimClips[2]  = "exit"
				
				tFamilySeqAnimDicts[3]  = ""
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
				*/
				
				/*
				
				TIMETABLE@MAID@CLEANING_WINDOW@IDLE_A
				idle_a.anim
				idle_b.anim
				idle_c.anim
				
				TIMETABLE@MAID@CLEANING_WINDOW@IDLE_B
				idle_d.anim
				idle_e.anim
				idle_f.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1]  = "idle_a"
				
//				//removed for #650600
//				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[2] += "@IDLE_A"
//				tFamilySeqAnimClips[2]  = "idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3]  = "idle_c"
				
				iFamilyAnims			= 3
				
				
				
				IF NOT g_bMagDemoActive
					//blocked for magdemo to stop arm wiping over handles
					tFamilySeqAnimDicts[4]  = tFamilySeqAnimDict
					tFamilySeqAnimDicts[4] += "@IDLE_B"
					tFamilySeqAnimClips[4]  = "idle_d"
					
					//blocked for magdemo to stop the step back
					tFamilySeqAnimDicts[5]  = tFamilySeqAnimDict
					tFamilySeqAnimDicts[5] += "@IDLE_B"
					tFamilySeqAnimClips[5]  = "idle_e"
				
				iFamilyAnims			= 5
				
				
				ENDIF
				
//				//removed for 568564
//				tFamilySeqAnimDicts[6]  = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[6] += "@IDLE_B"
//				tFamilySeqAnimClips[6]  = "idle_f"
				
			BREAK
			CASE FE_M_MEXMAID_does_the_dishes
				
				/*
				
				TIMETABLE@MAID@IG_2@
				IG_2_BASE.anim
				IG_2_IDLE_A.anim
				IG_2_IDLE_B.anim
				IG_2_IDLE_C.anim
				
				tFamilyAnimDict = "TIMETABLE@MAID@MAID@IG_2@"
				tFamilyAnimClip = ""
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "IG_2_BASE"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "IG_2_IDLE_A"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "IG_2_IDLE_B"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "IG_2_IDLE_C"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
//			CASE FE_M_MEXMAID_watching_TV
//				
//				/*
//				
//				TIMETABLE@MAID@COUCH@
//				base.anim
//				idle_a.anim
//				idle_b.anim
//				idle_c.anim
//				idle_d.anim
//				
//				tFamilyAnimDict = "TIMETABLE@MAID@MAID@COUCH@"
//				tFamilyAnimClip = ""
//				
//				*/
//				
//				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[0]  = "base"
//				
//				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[1]  = "idle_a"
//				
//				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[2]  = "idle_b"
//				
//				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[3]  = "idle_c"
//				
//				tFamilySeqAnimDicts[4]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[4]  = "idle_d"
//				
//				iFamilyAnims			= 4
//				
//				
//				
//				tFamilySeqAnimDicts[5]  = ""
//				
//			BREAK
			CASE FE_M_MEXMAID_stealing_stuff
				
				/*
				
				TIMETABLE@/MAID@/IG_8@/IG_8_BASE.anim
				TIMETABLE@/MAID@/IG_8@/IG_8_IDLE_A.anim
				TIMETABLE@/MAID@/IG_8@/IG_8_P2_IBringThisToday.anim
				TIMETABLE@/MAID@/IG_8@/IG_8_P3_ItLooksBroken.anim
				TIMETABLE@/MAID@/IG_8@/IG_8_P4_ITakeHomeToPolish.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "IG_8_BASE"
				
//				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[1]  = "IG_8_IDLE_A"
				
				iFamilyAnims			= 0	//1
				
				
				tFamilySeqAnimDicts[2]  = ""
				tFamilySeqAnimDicts[3]  = ""
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			
			CASE FE_M_GARDENER_cleaning_pool
				/*
				
				tFamilyAnimDict = "TIMETABLE@GARDENER@CLEAN_POOL@"
				tFamilyAnimClip = ""
				
				//TIMETABLE@GARDENER@CLEAN_POOL@BASE
				//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_A
				//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_B
				//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_C
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "idle_a_gardener"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "idle_b_gardener"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "idle_c_gardener"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = tFamilySeqAnimRoot
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			
			CASE FE_M_GARDENER_smoking_weed
				/*
				
				tFamilyAnimDict = "TIMETABLE@GARDENER@SMOKING_JOINT"
				tFamilyAnimClip = ""
				
				//TIMETABLE@GARDENER@SMOKING_JOINT/Idle_Cough.anim
				//TIMETABLE@GARDENER@SMOKING_JOINT/Smoke_Idle.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "Idle_Cough"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "Smoke_Idle"
				
				iFamilyAnims			= 1
				
				
				
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			
			CASE FE_F_AUNT_pelvic_floor_exercises
				
				/*
				
				tFamilyAnimDict = "TIMETABLE@DENICE@IG_1"
				tFamilyAnimClip = ""
				
				//TIMETABLE@DENICE@IG_1/base.anim
				//TIMETABLE@DENICE@IG_1/idle_A.anim
				//TIMETABLE@DENICE@IG_1/idle_B.anim
				//TIMETABLE@DENICE@IG_1/idle_C.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			CASE FE_F_AUNT_watching_TV
			CASE FE_F_AUNT_returned_to_aunts
				
				/*
				
				tFamilyAnimDict = "TIMETABLE@DENICE@IG_3"
				tFamilyAnimClip = "base"
				
				//TIMETABLE@DENICE@IG_3/base.anim
				//TIMETABLE@DENICE@IG_3/idle_A.anim
				//TIMETABLE@DENICE@IG_3/idle_B.anim
				//TIMETABLE@DENICE@IG_3/idle_C.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
				
				/*
				
				tFamilyAnimDict = "TIMETABLE@DENICE@IG_4"
				tFamilyAnimClip = "base"
				
				//TIMETABLE@DENICE@IG_4/base.anim
				//TIMETABLE@DENICE@IG_4/idle_A.anim
				//TIMETABLE@DENICE@IG_4/idle_B.anim
				//TIMETABLE@DENICE@IG_4/idle_C.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "idle_c"
				
				iFamilyAnims			= 3
				
				fBlendInDelta = INSTANT_BLEND_IN
				fBlendOutDelta = INSTANT_BLEND_OUT
				
				
				tFamilySeqAnimDicts[4]  = ""
				tFamilySeqAnimDicts[5]  = ""
				
			BREAK
			
			CASE FE_T0_MICHAEL_depressed_head_in_hands
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "BASE"
				tFamilySeqAnimClips[0] = "ON_CHAIR_Base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "IDLE_A"
				tFamilySeqAnimClips[1] = "ON_CHAIR_A"

				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "IDLE_A"
				tFamilySeqAnimClips[2] = "ON_CHAIR_B"

				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "IDLE_B"
				tFamilySeqAnimClips[3] = "ON_CHAIR_C"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "IDLE_B"
				tFamilySeqAnimClips[4] = "ON_CHAIR_D"

				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "IDLE_C"
				tFamilySeqAnimClips[5] = "ON_CHAIR_E"
				
				iFamilyAnims			= 5
				
				
				
//				tFamilySeqAnimDicts[6] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[6] += "IDLE_C"
//				tFamilySeqAnimClips[6] = "ON_CHAIR_F"
				
			BREAK
			
			CASE FE_T0_MICHAEL_sunbathing
			
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "BASE"
				tFamilySeqAnimClips[0] = "ON_CLUBCHAIR_Base"
				
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "IDLE_A"
				tFamilySeqAnimClips[1] = "ON_CLUBCHAIR_A"

				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "IDLE_A"
				tFamilySeqAnimClips[2] = "ON_CLUBCHAIR_B"


				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "IDLE_B"
				tFamilySeqAnimClips[3] = "ON_CLUBCHAIR_C"
				
				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "IDLE_B"
				tFamilySeqAnimClips[4] = "ON_CLUBCHAIR_D"
				

				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "IDLE_C"
				tFamilySeqAnimClips[5] = "ON_CLUBCHAIR_E"
				
				iFamilyAnims			= 5
				
				
				
//				tFamilySeqAnimDicts[6] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[6] += "IDLE_C"
//				tFamilySeqAnimClips[6] = "ON_CLUBCHAIR_F"
//				
//				tFamilySeqAnimDicts[7] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[7] += "IDLE_C"
//				tFamilySeqAnimClips[7] = "ON_CLUBCHAIR_G"
				
			BREAK
			
			CASE FE_T0_MICHAEL_drinking_beer
				/*
				tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_SOFA"
				tFamilyAnimClip = "SIT_Sofa"
				*/
				
				//TIMETABLE@MICHAEL@ON_SOFABASE/SIT_Sofa_Base.anim
				
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_A/SIT_Sofa_A.anim
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_A/SIT_Sofa_B.anim
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_A/SIT_Sofa_C.anim
				
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_B/SIT_Sofa_D.anim
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_B/SIT_Sofa_E.anim
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_B/SIT_Sofa_F.anim
				
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_C/SIT_Sofa_G.anim
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_C/SIT_Sofa_H.anim
				//TIMETABLE@MICHAEL@ON_SOFAIDLE_C/SIT_Sofa_I.anim
			
			
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "BASE"
				tFamilySeqAnimClips[0]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[0] += "_Base"


				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "IDLE_A"
				tFamilySeqAnimClips[1]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[1] += "_A"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "IDLE_A"
				tFamilySeqAnimClips[2]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[2] += "_B"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "IDLE_A"
				tFamilySeqAnimClips[3]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[3] += "_C"


				tFamilySeqAnimDicts[4] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[4] += "IDLE_B"
				tFamilySeqAnimClips[4]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[4] += "_D"
				
				tFamilySeqAnimDicts[5] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[5] += "IDLE_B"
				tFamilySeqAnimClips[5]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[5] += "_E"
				
				iFamilyAnims			= 5
				
				
				
//				tFamilySeqAnimDicts[6] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[6] += "IDLE_B"
//				tFamilySeqAnimClips[6]  = tFamilySeqAnimRoot
//				tFamilySeqAnimClips[6] += "_F"
//
//
//				tFamilySeqAnimDicts[3+4] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[3+4] += "IDLE_C"
//				tFamilySeqAnimClips[3+4]  = tFamilySeqAnimRoot
//				tFamilySeqAnimClips[3+4] += "_G"
//				
//				tFamilySeqAnimDicts[3+5] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[3+5] += "IDLE_C"
//				tFamilySeqAnimClips[3+5]  = tFamilySeqAnimRoot
//				tFamilySeqAnimClips[3+5] += "_H"
//				
//				tFamilySeqAnimDicts[3+6] = tFamilySeqAnimDict
//				tFamilySeqAnimDicts[3+6] += "IDLE_C"
//				tFamilySeqAnimClips[3+6]  = tFamilySeqAnimRoot
//				tFamilySeqAnimClips[3+6] += "_I"
				
			BREAK
			
			CASE FE_T0_RONEX_outside_looking_lonely
				/*
				//TIMETABLE@RON@IG_1/IG_1_BASE
				//TIMETABLE@RON@IG_1/IG_1_IDLE_A
				
				tFamilyAnimDict = "TIMETABLE@RON@IG_1"
				tFamilyAnimClip = ""
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "IG_1_BASE"
			
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "IG_1_IDLE_A"
				
				iFamilyAnims			= 1
				
				fBlendInDelta = WALK_BLEND_IN
				fBlendOutDelta = WALK_BLEND_OUT
				
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			CASE FE_T0_RON_listens_to_radio_broadcast
			CASE FE_T0_RONEX_trying_to_pick_up_signals
				/*
				//TIMETABLE@RON@IG_2/IG_2_BASE
				//TIMETABLE@RON@IG_2/IG_2_IDLE_A
				//TIMETABLE@RON@IG_2/IG_2_IDLE_B
				
				tFamilyAnimDict = "TIMETABLE@RON@IG_2"
				tFamilyAnimClip = ""
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "IG_2_BASE"
			
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "IG_2_IDLE_A"
			
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "IG_2_IDLE_B"
				
				iFamilyAnims			= 2
				
				
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
//			CASE FE_T0_RONEX_working_a_moonshine_sill
//				/*
//				//TIMETABLE@RON@IG_3/IG_3_BASE
//				//TIMETABLE@RON@IG_3/IG_3_IDLE_A
//				
//				tFamilyAnimDict = "TIMETABLE@RON@IG_3"
//				tFamilyAnimClip = ""
//				*/
//				
//				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
//				tFamilySeqAnimClips[0] = "IG_3_BASE"
//			
//				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
//				tFamilySeqAnimClips[1] = "IG_3_IDLE_A"
//				
//				iFamilyAnims			= 1
//				
//				
//				tFamilySeqAnimDicts[2] = ""
//				tFamilySeqAnimDicts[3] = ""
//				tFamilySeqAnimDicts[4] = ""
//				tFamilySeqAnimDicts[5] = ""
//			BREAK
			CASE FE_T0_RONEX_doing_target_practice
				/*
				//TIMETABLE@RON@IG_4/IG_4_BASE.anim
				//TIMETABLE@RON@IG_4/IG_4_IDLE_A.anim
				//TIMETABLE@RON@IG_4/IG_4_IDLE_A.anim
				//TIMETABLE@RON@IG_4/IG_4_IDLE_A.anim
				
				tFamilyAnimDict = "TIMETABLE@RON@IG_4"
				tFamilyAnimClip = ""
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "IG_4_BASE"
			
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "IG_4_IDLE_A"		//0.384
			
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "IG_4_IDLE_B"		//0.378
			
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = "IG_4_IDLE_C"		//0.402
				
				iFamilyAnims			= 3
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			CASE FE_T0_RON_drinks_moonshine_from_a_jar
				/*
				tFamilyAnimDict = "TIMETABLE@RON@MOONSHINE_IG_5"
				tFamilyAnimClip = "IG_5_"
				
				TIMETABLE@RON@MOONSHINE_IG_5/IG_5_BASE.anim
				TIMETABLE@RON@MOONSHINE_IG_5/IG_5_IDLE_A.anim
				TIMETABLE@RON@MOONSHINE_IG_5/IG_5_IDLE_B.anim
				
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[0] += "BASE"
			
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[1] += "IDLE_A"		//drinks 0.1603 - 0.7200
			
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[2] += "IDLE_B"		//drinks 0.1460 - 0.7890
				
				iFamilyAnims			= 2
				
				
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_T0_RON_stares_through_binoculars
				/*
				//TIMETABLE@RON@IG_6/IG_6_BASE.anim
				//TIMETABLE@RON@IG_6/IG_6_IDLE_A.anim
				//TIMETABLE@RON@IG_6/IG_6_IDLE_B.anim
				//TIMETABLE@RON@IG_6/IG_6_IDLE_C.anim
				
				tFamilyAnimDict = "TIMETABLE@RON@IG_6"
				tFamilyAnimClip = ""
				*/
				
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "IG_6_BASE"
			
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "IG_6_IDLE_A"
			
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = "IG_6_IDLE_B"
			
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = "IG_6_IDLE_C"
				
				iFamilyAnims			= 3
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_T0_TREVOR_smoking_crystal
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0] = "Base"
			
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1] = "Idle_a"
			
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2] = "Idle_b"
			
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3] = "Idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
				/*
				
				PROPS used:
				p_cs_lighter_01
				prop_cs_crackpipe
				
				Anims submitted at 10:30 AM EST, April 4/12:
				TIMETABLE@TREVOR@SMOKING_METH@\BASE\BASE.anim
				TIMETABLE@TREVOR@SMOKING_METH@\BASE\BASE.clip
				TIMETABLE@TREVOR@SMOKING_METH@\IDLE_A\IDLE_A.anim
				TIMETABLE@TREVOR@SMOKING_METH@\IDLE_A\IDLE_A.clip
				TIMETABLE@TREVOR@SMOKING_METH@\IDLE_A\IDLE_B.anim
				TIMETABLE@TREVOR@SMOKING_METH@\IDLE_A\IDLE_B.clip
				TIMETABLE@TREVOR@SMOKING_METH@\IDLE_A\IDLE_C.anim
				TIMETABLE@TREVOR@SMOKING_METH@\IDLE_A\IDLE_C.clip
				
				fbx's here:
				x:\gta5\art\anim\source_fbx\TIMETABLE@\TREVOR@\SMOKING_METH@\BASE\BASE.fbx
				x:\gta5\art\anim\source_fbx\TIMETABLE@\TREVOR@\SMOKING_METH@\IDLE_A\IDLE_A.fbx

				*/
				
		
			BREAK
			
			#IF NOT IS_JAPANESE_BUILD
			CASE FE_T0_TREVOR_doing_a_shit
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[0] += "BaseLoop"		//(Basic short loop)
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[1] += "StruggleLoop"	//(Longer Loop of Trevor Struggling
				
				iFamilyAnims			= 1
				
				
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			#ENDIF
			
			CASE FE_T0_TREVOR_passed_out_naked_drunk
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[0] += "_BASE"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[1] += "_IDLE_01"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimClips[2] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[2] += "_IDLE_02"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimClips[3] = tFamilySeqAnimRoot
				tFamilySeqAnimClips[3] += "_IDLE_03"
				
				iFamilyAnims			= 3
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_T0_KIDNAPPED_WIFE_cleaning
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimClips[0] = "Base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimClips[1] = "Clean_Up"
				
				iFamilyAnims			= 1
				
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
				tFamilySeqAnimDicts[0] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "BASE"
				tFamilySeqAnimClips[0] = "Base"
				
				tFamilySeqAnimDicts[1] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "IDLE_A"
				tFamilySeqAnimClips[1] = "IDLE_B"
				
				tFamilySeqAnimDicts[2] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "IDLE_A"
				tFamilySeqAnimClips[2] = "IDLE_B"
				
				tFamilySeqAnimDicts[3] = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "IDLE_A"
				tFamilySeqAnimClips[3] = "IDLE_C"
				
				iFamilyAnims			= 3
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_T1_FLOYD_is_sleeping
				
				/*
				tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED_IG_5@"
				tFamilyAnimClip = ""
				
				//TIMETABLE@FLOYD@CRYINGONBED_IG_5@Base
				//TIMETABLE@FLOYD@CRYINGONBED_IG_5@Idle_a
				//TIMETABLE@FLOYD@CRYINGONBED_IG_5@Idle_b
				//TIMETABLE@FLOYD@CRYINGONBED_IG_5@Idle_c
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "Idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "Idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "Idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_M_SON_raids_fridge_for_food
				
				/*
				tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED_IG_5@"
				tFamilyAnimClip = ""
				
				
				//TIMETABLE@JIMMY@IG_4@BASE
				Base.anim
				
				//TIMETABLE@JIMMY@IG_4@IDLE_A
				Idle_a.anim
				//TIMETABLE@JIMMY@IG_4@IDLE_A
				Idle_b.anim
				//TIMETABLE@JIMMY@IG_4@IDLE_A
				Idle_c.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0]  = "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1]  = "Idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2]  = "Idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3]  = "Idle_c"
				
				iFamilyAnims		  = 3
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			CASE FE_M_DAUGHTER_Coming_home_drunk
				
				/*
				//TIMETABLE@TRACY@IG_14@/IG_14_BASE_TRACY.anim
				
				//TIMETABLE@TRACY@IG_14@/IG_14_IDLE_A_WhatRUDoingHere_TRACY.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_IDLE_B_ReallyLoveUDad_TRACY.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_IDLE_C_YouGotAnyBlowDad_TRACY.anim
				
				//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_A_NOTE.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_A_PLAYER.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_A_TRACY.anim
				
				//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_B_NOTE.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_B_PLAYER.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_B_TRACY.anim
				
				//TIMETABLE@TRACY@IG_14@/IG_14_ParentingAtItsFinest_NOTE.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_ParentingAtItsFinest_PLAYER.anim
				//TIMETABLE@TRACY@IG_14@/IG_14_ParentingAtItsFinest_TRACY.anim
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "IG_14_BASE_TRACY"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "IG_14_IDLE_A_WhatRUDoingHere_TRACY"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "IG_14_IDLE_B_ReallyLoveUDad_TRACY"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "IG_14_IDLE_C_YouGotAnyBlowDad_TRACY"
				
				iFamilyAnims		  = 3
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_M_DAUGHTER_on_phone_to_friends
			CASE FE_M_DAUGHTER_on_phone_LOCKED
			CASE FE_M7_DAUGHTER_studying_on_phone
				/*
				
				//TIMETABLE@TRACY@FAMR_IG_4
				//BASE.anim
				//IDLE_A.anim
				//IDLE_B.anim
				//IDLE_C.anim
				
				tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_4"
				tFamilyAnimClip = ""
				
				*/
				
//				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
//				tFamilySeqAnimClips[0]  = "base"		//played when theres no dialogue

				tFamilySeqAnimDicts[1-1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1-1]  = "Idle_a"
				
				tFamilySeqAnimDicts[2-1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2-1]  = "Idle_b"
				
				tFamilySeqAnimDicts[3-1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3-1]  = "Idle_c"
				
				iFamilyAnims			= 3-1
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_M7_DAUGHTER_studying_does_nails
				/*
				
				//BASE.anim
				//FAMR_IG_5_IAmAboutToCrackThis.anim
				//FAMR_IG_5_IAmTotallyOnTop.anim
				//FAMR_IG_5_IAmTryingtoConcentrate.anim
				//FAMR_IG_5_ThisCollegeStuff.anim
				
				tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_5"
				tFamilyAnimClip = ""
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "FAMR_IG_5_IAmAboutToCrackThis"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "FAMR_IG_5_IAmTotallyOnTop"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "FAMR_IG_5_IAmTryingtoConcentrate"
				
				tFamilySeqAnimDicts[4]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[4]  = "FAMR_IG_5_ThisCollegeStuff"
				
				iFamilyAnims			= 4
				
				fBlendInDelta = INSTANT_BLEND_IN
				fBlendOutDelta = INSTANT_BLEND_OUT
				
				
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_M_GARDENER_mowing_lawn
				/*
				
				tFamilyAnimDict = "TIMETABLE@GARDENER@LAWNMOW@"
				tFamilyAnimClip = ""
				
				//TIMETABLE@GARDENER@CLEAN_POOL@BASE
				//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_A
				//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_B
				//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_C
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = "idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = "idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = "idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
			BREAK
			
			
//			CASE FE_F_LAMAR_and_STRETCH_arguing
//				
//				/*
//				TIMETABLE@\LAMAR@\IG_3\Base_Idle.anim
//				TIMETABLE@\LAMAR@\IG_3\003131_01_GC_LAS_IG_3_P3_Traffic_Jam.anim
//				TIMETABLE@\LAMAR@\IG_3\FAMILES_IS_FAMILES.anim
//				TIMETABLE@\LAMAR@\IG_3\LAS_IG_3_PT1_Captain_Crunch.anim
//				TIMETABLE@\LAMAR@\IG_3\LAS_IG_3_PT1_Soul_Pole.anim
//				TIMETABLE@\LAMAR@\IG_3\STEVE_JOBS_DIED.anim
//				TIMETABLE@\LAMAR@\IG_3\Soul_Pole.anim
//				*/
//				
//				/*
//				LAS_IG_3a / 1	
//				LAS_IG_3a / 2	
//				LAS_IG_3a / 3	
//				
//				LAS_IG_3b / 1	
//				LAS_IG_3b / 2	
//				LAS_IG_3b / 3	
//				*/
//				
//				tFamilySeqAnimDicts[0]  = "TIMETABLE@LAMAR@IG_3"
//				tFamilySeqAnimClips[0]  = "003131_01_GC_LAS_IG_3_P3_Traffic_Jam"
//				
//				tFamilySeqAnimDicts[1]  = "TIMETABLE@LAMAR@IG_3"
//				tFamilySeqAnimClips[1]  = "FAMILES_IS_FAMILES"
//				
//				tFamilySeqAnimDicts[2]  = "TIMETABLE@LAMAR@IG_3"
//				tFamilySeqAnimClips[2]  = "LAS_IG_3_PT1_Captain_Crunch"
//				
//				tFamilySeqAnimDicts[3]  = "TIMETABLE@LAMAR@IG_3"
//				tFamilySeqAnimClips[3]  = "LAS_IG_3_PT1_Soul_Pole"
//				
//				tFamilySeqAnimDicts[4]  = "TIMETABLE@LAMAR@IG_3"
//				tFamilySeqAnimClips[4]  = "STEVE_JOBS_DIED"
//				
//				tFamilySeqAnimDicts[5]  = "TIMETABLE@LAMAR@IG_3"
//				tFamilySeqAnimClips[5]  = "Soul_Pole"
//				
//				SWITCH eFamilyMember
//					CASE FM_FRANKLIN_LAMAR
//						tFamilySeqAnimClips[0] += "_Lamar"
//						tFamilySeqAnimClips[1] += "_Lamar"
//						tFamilySeqAnimClips[2] += "_Lamar"
//						tFamilySeqAnimClips[3] += "_Lamar"
//						tFamilySeqAnimClips[4] += "_Lamar"
//						tFamilySeqAnimClips[5] += "_Lamar"
//					BREAK
//					CASE FM_FRANKLIN_STRETCH
//						tFamilySeqAnimClips[0] += "_Stretch"
//						tFamilySeqAnimClips[1] += "_Stretch"
//						tFamilySeqAnimClips[2] += "_Stretch"
//						tFamilySeqAnimClips[3] += "_Stretch"
//						tFamilySeqAnimClips[4] += "_Stretch"
//						tFamilySeqAnimClips[5] += "_Stretch"
//					BREAK
//				ENDSWITCH
//				
//				iFamilyAnims		  = 5
//				
//			BREAK
//			CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//				
//				/*
//				TIMETABLE@\LAMAR@\IG_4\Base.anim
//				TIMETABLE@\LAMAR@\IG_4\Hey_One_Time.anim
//				TIMETABLE@\LAMAR@\IG_4\Keep_Moving.anim
//				TIMETABLE@\LAMAR@\IG_4\Looking_For_Someone_Frame_Up.anim
//				TIMETABLE@\LAMAR@\IG_4\Nothing_To_See_Here.anim
//				TIMETABLE@\LAMAR@\IG_4\Ride_On_Through.anim
//				*/
//				
//				/*
//				LAS_IG_4a / 1	
//				LAS_IG_4a / 2	
//				LAS_IG_4a / 3	
//				
//				LAS_IG_4b / 1	
//				LAS_IG_4b / 2	
//				LAS_IG_4b / 3	
//				*/
//				
//				tFamilySeqAnimDicts[0]  = "TIMETABLE@LAMAR@IG_4"
//				tFamilySeqAnimClips[0]  = "Base"
//				
//				tFamilySeqAnimDicts[1]  = "TIMETABLE@LAMAR@IG_4"
//				tFamilySeqAnimClips[1]  = "Hey_One_Time"
//				
//				tFamilySeqAnimDicts[2]  = "TIMETABLE@LAMAR@IG_4"
//				tFamilySeqAnimClips[2]  = "Keep_Moving"
//				
//				tFamilySeqAnimDicts[3]  = "TIMETABLE@LAMAR@IG_4"
//				tFamilySeqAnimClips[3]  = "Looking_For_Someone_Frame_Up"
//				
//				tFamilySeqAnimDicts[4]  = "TIMETABLE@LAMAR@IG_4"
//				tFamilySeqAnimClips[4]  = "Nothing_To_See_Here"
//				
//				tFamilySeqAnimDicts[5]  = "TIMETABLE@LAMAR@IG_4"
//				tFamilySeqAnimClips[5]  = "Ride_On_Through"
//				
//				SWITCH eFamilyMember
//					CASE FM_FRANKLIN_LAMAR
//						tFamilySeqAnimClips[0] += "_Lamar"
//						tFamilySeqAnimClips[1] += "_Lamar"
//						tFamilySeqAnimClips[2] += "_Lamar"
//						tFamilySeqAnimClips[3] += "_Lamar"
//						tFamilySeqAnimClips[4] += "_Lamar"
//						tFamilySeqAnimClips[5] += "_Lamar"
//					BREAK
//					CASE FM_FRANKLIN_STRETCH
//						tFamilySeqAnimClips[0] += "_Stretch"
//						tFamilySeqAnimClips[1] += "_Stretch"
//						tFamilySeqAnimClips[2] += "_Stretch"
//						tFamilySeqAnimClips[3] += "_Stretch"
//						tFamilySeqAnimClips[4] += "_Stretch"
//						tFamilySeqAnimClips[5] += "_Stretch"
//					BREAK
//				ENDSWITCH
//				
//				iFamilyAnims		  = 5
//				
//			BREAK
//			CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//				
//				/*
//				Bomb_As_A_Mother_Fucker
//				Grill_Protien_Like_Clockwork
//				Keep_On_Walkin
//				Nothing_ToSee_Here
//				Smokin_Is_My_Specialty
//				Swine
//				You_Want_Some_of_That
//				*/
//				
//				/*
//				LAS_IG_2a / 1	I dine on that swine, dawg, I fucks with it.
//				LAS_IG_2a / 2	This shit going to be bomb as a motherfucker.
//				LAS_IG_2a / 3	Smoking shit is my specialiiaty, you know that's how I get down.
//				
//				LAS_IG_2b / 1	Ain't nothing like some fine grilled protein to keep me regular like clock work. 
//				LAS_IG_2b / 2	Keep on walking, fool. This is man's food. 
//				LAS_IG_2b / 3	Ain't no shit for you here, nigga. 
//				*/
//				
//				tFamilySeqAnimDicts[0]  = "TIMETABLE@LAMAR@IG_2"
//				tFamilySeqAnimClips[0]  = "Swine"
//				
//				tFamilySeqAnimDicts[1]  = "TIMETABLE@LAMAR@IG_2"
//				tFamilySeqAnimClips[1]  = "Smokin_Is_My_Specialty"
//				
//				tFamilySeqAnimDicts[2]  = "TIMETABLE@LAMAR@IG_2"
//				tFamilySeqAnimClips[2]  = "Bomb_As_A_Mother_Fucker"
//				
//				tFamilySeqAnimDicts[3]  = "TIMETABLE@LAMAR@IG_2"
//				tFamilySeqAnimClips[3]  = "Grill_Protien_Like_Clockwork"
//				
//				tFamilySeqAnimDicts[4]  = "TIMETABLE@LAMAR@IG_2"
//				tFamilySeqAnimClips[4]  = "Keep_On_Walkin"
//				
//				tFamilySeqAnimDicts[5]  = "TIMETABLE@LAMAR@IG_2"
//				tFamilySeqAnimClips[5]  = "You_Want_Some_of_That"
//				
//				SWITCH eFamilyMember
//					CASE FM_FRANKLIN_LAMAR
//						tFamilySeqAnimClips[0] += "_Lamar"
//						tFamilySeqAnimClips[1] += "_Lamar"
//						tFamilySeqAnimClips[2] += "_Lamar"
//						tFamilySeqAnimClips[3] += "_Lamar"
//						tFamilySeqAnimClips[4] += "_Lamar"
//						tFamilySeqAnimClips[5] += "_Lamar"
//					BREAK
//					CASE FM_FRANKLIN_STRETCH
//						tFamilySeqAnimClips[0] += "_Stretch"
//						tFamilySeqAnimClips[1] += "_Stretch"
//						tFamilySeqAnimClips[2] += "_Stretch"
//						tFamilySeqAnimClips[3] += "_Stretch"
//						tFamilySeqAnimClips[4] += "_Stretch"
//						tFamilySeqAnimClips[5] += "_Stretch"
//					BREAK
//				ENDSWITCH
//				
//				iFamilyAnims		  = 5
//				
//			BREAK
			
			CASE FE_T0_TREVOR_and_kidnapped_wife_walk
				
				/*
				
				SWITCH eFamilyMember
					CASE FM_TREVOR_0_TREVOR
						tFamilyAnimDict = "TIMETABLE@TREVOR@IG_1"
						tFamilyAnimClip = "IG_1_"
						RETURN TRUE
					BREAK
					CASE FM_TREVOR_0_WIFE
						tFamilyAnimDict = "TIMETABLE@TREVOR@IG_1"
						tFamilyAnimClip = "IG_1_"
						RETURN TRUE
					BREAK
				ENDSWITCH
				
			//	TIMETABLE@TREVOR@IG_1/IG_1_BASE_PATRICIA.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_BASE_TREVOR.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_TheDesertIsSoBeautiful_PATRICIA.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_TheDesertIsSoBeautiful_TREVOR.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_TheDontKnowWhy_PATRICIA.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_TheDontKnowWhy_TREVOR.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_ThereAreJustSomeMoments_PATRICIA.anim
			//	TIMETABLE@TREVOR@IG_1/IG_1_ThereAreJustSomeMoments_TREVOR.anim


				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[0] += "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[1] += "TheDesertIsSoBeautiful"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[2] += "TheDontKnowWhy"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[3] += "ThereAreJustSomeMoments"
				
				SWITCH eFamilyMember
					CASE FM_TREVOR_0_TREVOR
						tFamilySeqAnimClips[0] += "_TREVOR"
						tFamilySeqAnimClips[1] += "_TREVOR"
						tFamilySeqAnimClips[2] += "_TREVOR"
						tFamilySeqAnimClips[3] += "_TREVOR"
					BREAK
					CASE FM_TREVOR_0_WIFE
						tFamilySeqAnimClips[0] += "_PATRICIA"
						tFamilySeqAnimClips[1] += "_PATRICIA"
						tFamilySeqAnimClips[2] += "_PATRICIA"
						tFamilySeqAnimClips[3] += "_PATRICIA"
					BREAK
				ENDSWITCH
				
				iFamilyAnims		  = 3
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			CASE FE_T0_TREVOR_and_kidnapped_wife_stare
				
				/*
				
				//TIMETABLE@TREVOR@TRV_IG_2/Base_Patricia.anim
				//TIMETABLE@TREVOR@TRV_IG_2/Base_Trevor.anim
				//TIMETABLE@TREVOR@TRV_IG_2/Making_Me_Blush_Patricia.anim
				//TIMETABLE@TREVOR@TRV_IG_2/Making_Me_Blush_Trevor.anim
				//TIMETABLE@TREVOR@TRV_IG_2/Met_You_20_Years_Ago_Patricia.anim
				//TIMETABLE@TREVOR@TRV_IG_2/Met_You_20_Years_Ago_Trevor.anim
				//TIMETABLE@TREVOR@TRV_IG_2/You_Blinked_Patricia.anim
				//TIMETABLE@TREVOR@TRV_IG_2/You_Blinked_Trevor.anim

				SWITCH eFamilyMember
					CASE FM_TREVOR_0_TREVOR
						tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_2"
						tFamilyAnimClip = ""
						RETURN TRUE
					BREAK
					CASE FM_TREVOR_0_WIFE
						tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_2"
						tFamilyAnimClip = ""
						RETURN TRUE
					BREAK
				ENDSWITCH

				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[0] += "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[1]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[1] += "Making_Me_Blush"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[2]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[2] += "Met_You_20_Years_Ago"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[3]  = tFamilySeqAnimRoot
				tFamilySeqAnimClips[3] += "You_Blinked"
				
				SWITCH eFamilyMember
					CASE FM_TREVOR_0_TREVOR
						tFamilySeqAnimClips[0] += "_TREVOR"
						tFamilySeqAnimClips[1] += "_TREVOR"
						tFamilySeqAnimClips[2] += "_TREVOR"
						tFamilySeqAnimClips[3] += "_TREVOR"
					BREAK
					CASE FM_TREVOR_0_WIFE
						tFamilySeqAnimClips[0] += "_PATRICIA"
						tFamilySeqAnimClips[1] += "_PATRICIA"
						tFamilySeqAnimClips[2] += "_PATRICIA"
						tFamilySeqAnimClips[3] += "_PATRICIA"
					BREAK
				ENDSWITCH
				
				iFamilyAnims		  = 3
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			/*
			CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
				
			
				
				//TIMETABLE@TREVOR@TRV_IG_5BASE\BASE_Patricia.anim
				//TIMETABLE@TREVOR@TRV_IG_5BASE\BASE_Trevor.anim
				//
				//TIMETABLE@TREVOR@TRV_IG_5IDLE_A\IDLE_A_Patricia.anim
				//TIMETABLE@TREVOR@TRV_IG_5IDLE_A\IDLE_A_Trevor.anim
				
				SWITCH eFamilyMember
					CASE FM_TREVOR_0_TREVOR
						tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_5"
						tFamilyAnimClip = ""
						RETURN TRUE
					BREAK
					CASE FM_TREVOR_0_WIFE
						tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_5"
						tFamilyAnimClip = ""
						RETURN TRUE
					BREAK
				ENDSWITCH

			
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "BASE"
				tFamilySeqAnimClips[0]  = "BASE"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "IDLE_A"
				tFamilySeqAnimClips[1]  = "IDLE_A"
				
				SWITCH eFamilyMember
					CASE FM_TREVOR_0_TREVOR
						tFamilySeqAnimClips[0] += "_TREVOR"
						tFamilySeqAnimClips[1] += "_TREVOR"
					BREAK
					CASE FM_TREVOR_0_WIFE
						tFamilySeqAnimClips[0] += "_PATRICIA"
						tFamilySeqAnimClips[1] += "_PATRICIA"
					BREAK
				ENDSWITCH
				
				iFamilyAnims		  = 1
				
				
				tFamilySeqAnimDicts[2] = ""
				tFamilySeqAnimDicts[3] = ""
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			*/
			CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
				
				/*
				
				//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_1/ALT_1_Michael.anim
				//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_1/ALT_1_Patricia.anim
				
				//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_2/ALT_2_Michael.anim
				//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_2/ALT_2_Patricia.anim
				
				//TIMETABLE@PATRICIA@PAT_IG_3@/BASE/BASE_Michael.anim
				//TIMETABLE@PATRICIA@PAT_IG_3@/BASE/BASE_Patricia.anim
				
				//TIMETABLE@PATRICIA@PAT_IG_3@/PAT_IG_3_Michael.anim
				//TIMETABLE@PATRICIA@PAT_IG_3@/PAT_IG_3_Patricia.anim

				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimClips[0]  = "PAT_IG_3"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "BASE"
				tFamilySeqAnimClips[1]  = "BASE"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "ALT_1"
				tFamilySeqAnimClips[2]  = "ALT_1"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "ALT_2"
				tFamilySeqAnimClips[3]  = "ALT_2"
				
				SWITCH eFamilyMember
					CASE FM_TREVOR_0_WIFE
						tFamilySeqAnimClips[0] += "_PATRICIA"
						tFamilySeqAnimClips[1] += "_PATRICIA"
						tFamilySeqAnimClips[2] += "_PATRICIA"
						tFamilySeqAnimClips[3] += "_PATRICIA"
					BREAK
					CASE FM_TREVOR_0_MICHAEL
						tFamilySeqAnimClips[0] += "_MICHAEL"
						tFamilySeqAnimClips[1] += "_MICHAEL"
						tFamilySeqAnimClips[2] += "_MICHAEL"
						tFamilySeqAnimClips[3] += "_MICHAEL"
					BREAK
				ENDSWITCH
				
				iFamilyAnims		  = 3
				
				fBlendInDelta = INSTANT_BLEND_IN
				fBlendOutDelta = INSTANT_BLEND_OUT
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			
			CASE FE_T1_FLOYD_cleaning
				
				/*
				tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED"
				tFamilyAnimClip = ""
					
				
				//TIMETABLE@FLOYD@CRYINGONBED@BASE/Base
				//TIMETABLE@FLOYD@CRYINGONBED@IDLE_A/Idle_a
				//TIMETABLE@FLOYD@CRYINGONBED@IDLE_A/Idle_b
				//TIMETABLE@FLOYD@CRYINGONBED@IDLE_A/Idle_c
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0]  = "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1]  = "Idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2]  = "Idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3]  = "Idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			
			CASE FE_T1_FLOYD_cries_in_foetal_position
				
				/*
				tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED"
				tFamilyAnimClip = ""
				
				//TIMETABLE@FLOYD@CRYINGONBED@BASE/Base
				//TIMETABLE@FLOYD@CRYINGONBED@IDLE_A/Idle_a
				//TIMETABLE@FLOYD@CRYINGONBED@IDLE_A/Idle_b
				//TIMETABLE@FLOYD@CRYINGONBED@IDLE_A/Idle_c
				
				*/
				
				tFamilySeqAnimDicts[0]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[0] += "@BASE"
				tFamilySeqAnimClips[0]  = "Base"
				
				tFamilySeqAnimDicts[1]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[1] += "@IDLE_A"
				tFamilySeqAnimClips[1]  = "Idle_a"
				
				tFamilySeqAnimDicts[2]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[2] += "@IDLE_A"
				tFamilySeqAnimClips[2]  = "Idle_b"
				
				tFamilySeqAnimDicts[3]  = tFamilySeqAnimDict
				tFamilySeqAnimDicts[3] += "@IDLE_A"
				tFamilySeqAnimClips[3]  = "Idle_c"
				
				iFamilyAnims			= 3
				
				
				
				tFamilySeqAnimDicts[4] = ""
				tFamilySeqAnimDicts[5] = ""
				
			BREAK
			
			
			DEFAULT
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_AnimArray()")
				
				SCRIPT_ASSERT("invalid eFamilyEvent in PRIVATE_Get_Family_AnimArray()")
				#ENDIF
				
				iFamilyAnims = -1
				RETURN FALSE
			BREAK
		ENDSWITCH
	ENDIF
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_AnimArray(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		VECTOR vSequencePos, FLOAT fSequenceHead)
	
	TEXT_LABEL_63 tFamilySeqAnimDicts[6], tFamilySeqAnimClips[6]
	INT iFamilyAnims
	FLOAT fBlendInDelta, fBlendOutDelta
	ANIMATION_FLAGS eFamilyAnimFlag
	
	IF PRIVATE_Get_Family_AnimArray(eFamilyMember, eFamilyEvent,
		tFamilySeqAnimDicts, tFamilySeqAnimClips, iFamilyAnims,
		fBlendInDelta, fBlendOutDelta, eFamilyAnimFlag)
	
		VECTOR vShitOffset
		FLOAT fShitHead
		
		IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vShitOffset, fShitHead)
			
			
			IK_CONTROL_FLAGS ikFlags
			PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(eFamilyMember, eFamilyEvent, ikFlags)
			
			CONST_FLOAT fCONST_Anim_Almost_Finished	0.99
			
			BOOL bFound = FALSE
			
			IF NOT bFound
				IF NOT IS_STRING_NULL_OR_EMPTY(tAnimDict)
					IF IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip, ANIM_SCRIPT)
						//
						
						FLOAT fAnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tAnimDict, tAnimClip)
							
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str
						str  = ("anim[")
//						str += (iClip)
						str += ("]: ")
						str += (tAnimClip)
						str += (", ")
						str += GET_STRING_FROM_FLOAT(fAnimCurrentTime)
						
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREEN)
						#ENDIF
						
						IF (fAnimCurrentTime >= fCONST_Anim_Almost_Finished)
							PRIVATE_TaskPlayAnimArrayAdvanced(PedIndex,
									tFamilySeqAnimDicts, tFamilySeqAnimClips, iFamilyAnims,
									vSequencePos+vShitOffset, <<0,0,fSequenceHead+fShitHead>>,
									tAnimDict, tAnimClip,
									fBlendInDelta, fBlendOutDelta, -1, eFamilyAnimFlag, DEFAULT, DEFAULT, ikFlags)
						ENDIF
						
						bFound = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bFound
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str
				str  = ("anim[max]: ")
				str += (tAnimClip)
				str += (", ")
				str += (" NONE")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREEN)
				#ENDIF
				
				PRIVATE_TaskPlayAnimArrayAdvanced(PedIndex,
						tFamilySeqAnimDicts, tFamilySeqAnimClips, iFamilyAnims,
						vSequencePos+vShitOffset, <<0,0,fSequenceHead+fShitHead>>,
						tAnimDict, tAnimClip,
						fBlendInDelta, fBlendOutDelta, -1, eFamilyAnimFlag, DEFAULT, DEFAULT, ikFlags)
			ENDIF
			
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tAnimDict)
			RETURN IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip)
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	tAnimDict = ""
	tAnimClip = ""
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_TaskSynchSceneArray(PED_INDEX PedIndex, INT iScene,
		TEXT_LABEL_63 &tFamilySynchDicts[],  TEXT_LABEL_63 &tFamilySynchClips[], INT iFamilySynchs,
		TEXT_LABEL_63 &tSynchDict, TEXT_LABEL_63 &tSynchClip,
		FLOAT blendInDelta, FLOAT blendOutDelta,
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_USE_PHYSICS,
		RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_PLAYER_IMPACT,
		FLOAT moverBlendInDelta = INSTANT_BLEND_IN, IK_CONTROL_FLAGS ikFlags = AIK_NONE)
	
	INT iMAX_FamilySynchs = iFamilySynchs
	IF NOT IS_STRING_NULL_OR_EMPTY(tSynchDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(tSynchClip)
		
		CPRINTLN(DEBUG_FAMILY, "old synch scene: \"", tSynchDict, "\", \"", tSynchClip, "\"")
		
		IF (iMAX_FamilySynchs <= 0)
			CPRINTLN(DEBUG_FAMILY, "zero synch scene: \"", tSynchDict, "\", \"", tSynchClip, "\"")
		ELSE
			INT iSynchs
			REPEAT (iFamilySynchs+1) iSynchs
				CPRINTLN(DEBUG_FAMILY, "	synch scene[", iSynchs, "]: \"", tFamilySynchDicts[iSynchs], "\", \"", tFamilySynchClips[iSynchs], "\"")
				
				IF ARE_STRINGS_EQUAL(tSynchDict, tFamilySynchDicts[iSynchs])
				AND ARE_STRINGS_EQUAL(tSynchClip, tFamilySynchClips[iSynchs])
					
					INT iNewSynchs
					FOR iNewSynchs = iSynchs TO COUNT_OF(tFamilySynchDicts)-1
						IF ((iNewSynchs+1) < COUNT_OF(tFamilySynchDicts))
							tFamilySynchDicts[iNewSynchs] = tFamilySynchDicts[iNewSynchs+1]
							tFamilySynchClips[iNewSynchs] = tFamilySynchClips[iNewSynchs+1]
						ELSE
							tFamilySynchDicts[iNewSynchs] = ""
							tFamilySynchClips[iNewSynchs] = ""
						ENDIF
					ENDFOR
					
					iMAX_FamilySynchs--
				ENDIF
				
			ENDREPEAT
			
			CPRINTLN(DEBUG_FAMILY, "")
			
		ENDIF
	ENDIF
	
	INT iSynchName = GET_RANDOM_INT_IN_RANGE(0, iMAX_FamilySynchs+1)
	
	TEXT_LABEL_63 pSynchDictName = tFamilySynchDicts[iSynchName]
	TEXT_LABEL_63 pSynchClipName = tFamilySynchClips[iSynchName]
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pSynchDictName)
		REQUEST_ANIM_DICT(pSynchDictName)
		IF NOT HAS_ANIM_DICT_LOADED(pSynchDictName)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneArray anim dict not loaded(\"", pSynchDictName, "\")")
			#ENDIF
			
			REQUEST_ANIM_DICT(pSynchDictName)
		ELSE
			IF IS_BITMASK_ENUM_AS_ENUM_SET(flags, SYNCED_SCENE_USE_PHYSICS)
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
				SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL sBlendInDelta, sBlendOutDelta
			IF blendInDelta = WALK_BLEND_IN				sBlendInDelta = "WALK"
			ELIF blendInDelta = REALLY_SLOW_BLEND_IN	sBlendInDelta = "REALLY_SLOW"
			ELIF blendInDelta = SLOW_BLEND_IN			sBlendInDelta = "SLOW"
			ELIF blendInDelta = NORMAL_BLEND_IN			sBlendInDelta = "NORMAL"
			ELIF blendInDelta = FAST_BLEND_IN			sBlendInDelta = "FAST"
			ELIF blendInDelta = INSTANT_BLEND_IN		sBlendInDelta = "INSTANT"
			ELSE
				sBlendInDelta = ROUND(blendInDelta)
			ENDIF
			IF blendOutDelta = WALK_BLEND_OUT			sBlendOutDelta = "WALK"
			ELIF blendOutDelta = REALLY_SLOW_BLEND_OUT	sBlendOutDelta = "REALLY_SLOW"
			ELIF blendOutDelta = SLOW_BLEND_OUT			sBlendOutDelta = "SLOW"
			ELIF blendOutDelta = NORMAL_BLEND_OUT		sBlendOutDelta = "NORMAL"
			ELIF blendOutDelta = FAST_BLEND_OUT			sBlendOutDelta = "FAST"
			ELIF blendOutDelta = INSTANT_BLEND_OUT		sBlendOutDelta = "INSTANT"
			ELSE
				sBlendOutDelta = ROUND(blendOutDelta)
			ENDIF
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneArray(PedIndex, \"", pSynchDictName, "\", \"", pSynchClipName, "\": ", iSynchName, ", blendInDelta:", sBlendInDelta, ", blendOutDelta: ", sBlendOutDelta, ")")
			#ENDIF
			
			SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
			TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
					pSynchDictName, pSynchClipName,
					blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
			SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
			tSynchDict = pSynchDictName
			tSynchClip = pSynchClipName
			
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneArray(PedIndex, \"", "null", "\": ", iSynchName, ")")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_TaskSynchSceneArrayMatch(PED_INDEX PedIndex, INT iScene,
		TEXT_LABEL_63 &tFamilySynchDicts[],  TEXT_LABEL_63 &tFamilySynchClips[], INT iFamilySynchs,
		TEXT_LABEL_63 &tSynchDict, TEXT_LABEL_63 &tSynchClip, 
		TEXT_LABEL_63 tMatchClip, STRING sMatchTail,
		FLOAT blendInDelta, FLOAT blendOutDelta,
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_USE_PHYSICS,
		RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_PLAYER_IMPACT,
		FLOAT moverBlendInDelta = INSTANT_BLEND_IN, IK_CONTROL_FLAGS ikFlags = AIK_NONE)
	
//	INT iMAX_FamilySynchs = iFamilySynchs
	
	INT iSynchName = -1 //= GET_RANDOM_INT_IN_RANGE(0, iMAX_FamilySynchs+1)
	
	IF /*NOT IS_STRING_NULL_OR_EMPTY(tSynchDict)
	AND*/ NOT IS_STRING_NULL_OR_EMPTY(tMatchClip)
		
		INT iMatchClip_length = GET_LENGTH_OF_LITERAL_STRING(tMatchClip)
		INT iMatchTail_length = GET_LENGTH_OF_LITERAL_STRING(sMatchTail)
		
		INT iMatchBody_length = iMatchClip_length - iMatchTail_length
		TEXT_LABEL_63 tMatchBody = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tMatchClip, 0, iMatchBody_length)
		
		CPRINTLN(DEBUG_FAMILY, "matchup synch scene: \"", tSynchDict, "\", \"", tMatchClip, "\" - \"", tMatchBody, "\" & \"", sMatchTail, "\"")
		
		INT iSynchs
		REPEAT (iFamilySynchs+1) iSynchs
			
//			CPRINTLN(DEBUG_FAMILY, "	GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(\"")
//			CPRINTLN(DEBUG_FAMILY, tFamilySynchClips[iSynchs])
//			CPRINTLN(DEBUG_FAMILY, "\", ")
//			CPRINTLN(DEBUG_FAMILY, 0)
//			CPRINTLN(DEBUG_FAMILY, ", ")
//			CPRINTLN(DEBUG_FAMILY, iMatchBody_length)
//			CPRINTLN(DEBUG_FAMILY, ")")
//			cprintlnNL()
			
			INT iMatchBody_storedLength = iMatchBody_length
			INT iFamilySynchClip_length = GET_LENGTH_OF_LITERAL_STRING(tFamilySynchClips[iSynchs])
			
			IF (iMatchBody_storedLength > iFamilySynchClip_length)
				iMatchBody_storedLength = iFamilySynchClip_length
			ENDIF
			TEXT_LABEL_63 tFamilySynchBody = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tFamilySynchClips[iSynchs], 0, iMatchBody_storedLength)
			
			CPRINTLN(DEBUG_FAMILY, "	match synch scene[", iSynchs, "]: \"", tFamilySynchDicts[iSynchs], "\", \"", tFamilySynchClips[iSynchs], "\"")
			
			IF ARE_STRINGS_EQUAL(tMatchBody, tFamilySynchBody)
				iSynchName = iSynchs
				
				CPRINTLN(DEBUG_FAMILY, " MATCH", "	\"", tMatchBody, "\" =  \"", tFamilySynchBody, "\"")
			ELSE
				CPRINTLN(DEBUG_FAMILY, " doesn't match", "	\"", tMatchBody, "\" <>  \"", tFamilySynchBody, "\"")
			ENDIF
			
			CPRINTLN(DEBUG_FAMILY, "")
//			
//			IF ARE_STRINGS_EQUAL(tSynchDict, tFamilySynchDicts[iSynchs])
//			AND ARE_STRINGS_EQUAL(tSynchClip, tFamilySynchClips[iSynchs])
//				
//				INT iNewSynchs
//				FOR iNewSynchs = iSynchs TO COUNT_OF(tFamilySynchDicts)-1
//					tFamilySynchDicts[iSynchs] = tFamilySynchDicts[iSynchs+1]
//					tFamilySynchClips[iSynchs] = tFamilySynchClips[iSynchs+1]
//				ENDFOR
//				
//				iMAX_FamilySynchs--
//			ENDIF
			
		ENDREPEAT
		
		CPRINTLN(DEBUG_FAMILY, "")
		
	ELSE
		CPRINTLN(DEBUG_FAMILY, "tMatchClip \"", tMatchClip, "\" null")
		EXIT
	ENDIF
	
	IF (iSynchName = -1)
		CPRINTLN(DEBUG_FAMILY, "iSynchName = -1")
		EXIT
	ENDIF
	
	TEXT_LABEL_63 pSynchDictName = tFamilySynchDicts[iSynchName]
	TEXT_LABEL_63 pSynchClipName = tFamilySynchClips[iSynchName]
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pSynchDictName)
		REQUEST_ANIM_DICT(pSynchDictName)
		IF NOT HAS_ANIM_DICT_LOADED(pSynchDictName)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneArrayMatch anim dict not loaded(\"", pSynchDictName, "\")")
			#ENDIF
			
			REQUEST_ANIM_DICT(pSynchDictName)
		ELSE
			IF IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, SYNCED_SCENE_USE_PHYSICS)
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
				SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
			ENDIF
			
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				SET_BITMASK_ENUM_AS_ENUM(ragdollFlags, SYNCED_SCENE_LOOP_WITHIN_SCENE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL sBlendInDelta, sBlendOutDelta
			IF blendInDelta = WALK_BLEND_IN				sBlendInDelta = "WALK"
			ELIF blendInDelta = REALLY_SLOW_BLEND_IN	sBlendInDelta = "REALLY_SLOW"
			ELIF blendInDelta = SLOW_BLEND_IN			sBlendInDelta = "SLOW"
			ELIF blendInDelta = NORMAL_BLEND_IN			sBlendInDelta = "NORMAL"
			ELIF blendInDelta = FAST_BLEND_IN			sBlendInDelta = "FAST"
			ELIF blendInDelta = INSTANT_BLEND_IN		sBlendInDelta = "INSTANT"
			ELSE
				sBlendInDelta = ROUND(blendInDelta)
			ENDIF
			IF blendOutDelta = WALK_BLEND_OUT			sBlendOutDelta = "WALK"
			ELIF blendOutDelta = REALLY_SLOW_BLEND_OUT	sBlendOutDelta = "REALLY_SLOW"
			ELIF blendOutDelta = SLOW_BLEND_OUT			sBlendOutDelta = "SLOW"
			ELIF blendOutDelta = NORMAL_BLEND_OUT		sBlendOutDelta = "NORMAL"
			ELIF blendOutDelta = FAST_BLEND_OUT			sBlendOutDelta = "FAST"
			ELIF blendOutDelta = INSTANT_BLEND_OUT		sBlendOutDelta = "INSTANT"
			ELSE
				sBlendOutDelta = ROUND(blendOutDelta)
			ENDIF
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneArrayMatch(PedIndex, \"", pSynchDictName, "\", \"", pSynchClipName, "\": ", iSynchName, ", blendInDelta:", sBlendInDelta, ", blendOutDelta: ", sBlendOutDelta, ")")
			#ENDIF
			
			TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
					pSynchDictName, pSynchClipName,
					blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
			SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			
//			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
			tSynchDict = pSynchDictName
			tSynchClip = pSynchClipName
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneArrayMatch(PedIndex, \"", "null", "\": ", iSynchName, ")")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_Update_Family_SynchSceneArray(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		TEXT_LABEL_63 &tSynchDict, TEXT_LABEL_63 &tSynchClip,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		INT &iScene, bool loop, bool holdLastFrame)
	TEXT_LABEL_63 tFamilySeqSyncDicts[6], tFamilySeqSyncClips[6]
	INT iFamilySynchs
	BOOL bSceneSuccessfullyStarted
	FLOAT blendInDelta, blendOutDelta
	ANIMATION_FLAGS eFamilySyncFlag
	
	VECTOR vSynchSceneOffset
	FLOAT fSynchSceneHead
	
	IF PRIVATE_Get_Family_AnimArray(eFamilyMember, eFamilyEvent,
			tFamilySeqSyncDicts, tFamilySeqSyncClips, iFamilySynchs,
			blendInDelta, blendOutDelta, eFamilySyncFlag)
	AND PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS SynchSceneHudColour = HUD_COLOUR_PINK
		#ENDIF
		
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_USE_PHYSICS
		RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_PLAYER_IMPACT
		FLOAT moverBlendInDelta = INSTANT_BLEND_IN
		
		IK_CONTROL_FLAGS ikFlags = AIK_NONE
		PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(eFamilyMember, eFamilyEvent, ikFlags)
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilySyncFlag, AF_NOT_INTERRUPTABLE)
			SET_BITMASK_ENUM_AS_ENUM(flags, SYNCED_SCENE_DONT_INTERRUPT)
		ENDIF

		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			
			CPRINTLN(DEBUG_FAMILY, "NOT IS_SYNCHRONIZED_SCENE_RUNNING(PedIndex, \"", tSynchDict, "\", \"", tSynchClip, "\")	//id: ", iScene)
			
			
			IF (eFamilyEvent = FE_T1_FLOYD_cries_in_foetal_position)
				blendInDelta = INSTANT_BLEND_IN
				moverBlendInDelta = blendInDelta
//				blendOutDelta = INSTANT_BLEND_OUT
			ELIF (eFamilyEvent = FE_M_WIFE_gets_drink_in_kitchen)
				blendInDelta = REALLY_SLOW_BLEND_IN
				moverBlendInDelta = blendInDelta
//				blendOutDelta = INSTANT_BLEND_OUT
			ELIF (eFamilyEvent = FE_M_SON_sleeping)
				blendInDelta = INSTANT_BLEND_IN
				moverBlendInDelta = blendInDelta
				blendOutDelta = INSTANT_BLEND_OUT
			ELIF (eFamilyEvent = FE_T0_TREVOR_and_kidnapped_wife_stare)
				blendInDelta = INSTANT_BLEND_IN
				moverBlendInDelta = blendInDelta
				blendOutDelta = INSTANT_BLEND_OUT
			ELIF (eFamilyEvent = FE_T0_TREVOR_passed_out_naked_drunk)
				blendInDelta = INSTANT_BLEND_IN
				moverBlendInDelta = blendInDelta
				blendOutDelta = INSTANT_BLEND_OUT
			ELSE
				blendInDelta = SLOW_BLEND_IN
				moverBlendInDelta = INSTANT_BLEND_IN
//				blendOutDelta = INSTANT_BLEND_OUT
			ENDIF
			
			REQUEST_ANIM_DICT(tFamilySeqSyncDicts[0])
			IF NOT HAS_ANIM_DICT_LOADED(tFamilySeqSyncDicts[0])
				#IF IS_DEBUG_BUILD
				str  = ("requesting dict \"")
				
				INT iTaunterAnimOutLength
				iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tFamilySeqSyncDicts[0])
				
				IF iTaunterAnimOutLength >= 10
					str += GET_STRING_FROM_STRING(tFamilySeqSyncDicts[0],
							iTaunterAnimOutLength - 10,
							iTaunterAnimOutLength)
				ELSE
					str += tFamilySeqSyncDicts[0]
				ENDIF
				
				str += ("\" for synch scene ")
				str += (iScene)
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			IF (iScene > 0)
				iScene = -1
			ENDIF
			
			iScene = CREATE_SYNCHRONIZED_SCENE(vFamilyScenePos+vSynchSceneOffset, <<0,0,fFamilySceneHead+fSynchSceneHead>>)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, loop)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, holdLastFrame)
			
			bSceneSuccessfullyStarted = PRIVATE_TaskSynchSceneArray(PedIndex, iScene,
					tFamilySeqSyncDicts,  tFamilySeqSyncClips, iFamilySynchs,
					tSynchDict, tSynchClip,
					blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
			
			#IF IS_DEBUG_BUILD
			str  = ("creating SynchScene \"")
			
			INT iTaunterAnimOutLength
			iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tSynchClip)
			
			IF iTaunterAnimOutLength >= 18
				str += GET_STRING_FROM_STRING(tSynchClip,
						iTaunterAnimOutLength - 18,
						iTaunterAnimOutLength)
			ELSE
				str += tSynchClip
			ENDIF
			
			str += ("\" ")
			str += (iScene)
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			RETURN bSceneSuccessfullyStarted
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tSynchDict, tSynchClip, ANIM_SYNCED_SCENE)
			
			CPRINTLN(DEBUG_FAMILY, "NOT IS_ENTITY_PLAYING_ANIM(PedIndex, \"", tSynchDict, "\", \"", tSynchClip, "\", ANIM_SYNCED_SCENE)")
			
			IF NOT PRIVATE_TaskSynchSceneArray(PedIndex, iScene,
					tFamilySeqSyncDicts,  tFamilySeqSyncClips, iFamilySynchs,
					tSynchDict, tSynchClip,
					blendInDelta, INSTANT_BLEND_OUT, flags, ragdollFlags, moverBlendInDelta, ikFlags)
				
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
				
				#IF IS_DEBUG_BUILD
				str  = ("tasking SynchScene \"")
				str += (tSynchClip)
				str += ("\"")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, loop)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, holdLastFrame)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		str  = ("SynchScene[] \"")
		str += (tSynchDict)
		str += ("\" id:")
		str += iScene
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
		
		str  = (tSynchClip)
		str += (":")
		str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, SynchSceneHudColour)
		
		str  = ("loop: ")
		IF loop				str += ("TRUE")	ELSE	str += ("FALSE")	ENDIF
		str += (", holdLastFrame: ")
		IF holdLastFrame	str += ("TRUE")	ELSE	str += ("FALSE")	ENDIF
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, SynchSceneHudColour)
		#ENDIF
		
//		IF loop
			IF holdLastFrame
				IF (GET_SYNCHRONIZED_SCENE_PHASE(iScene) >= 0.999)
					PRIVATE_TaskSynchSceneArray(PedIndex, iScene,
							tFamilySeqSyncDicts,  tFamilySeqSyncClips, iFamilySynchs,
							tSynchDict, tSynchClip,
							INSTANT_BLEND_IN, INSTANT_BLEND_OUT, flags, ragdollFlags, INSTANT_BLEND_IN, ikFlags)
				ENDIF
			ENDIF
//		ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_SynchSceneArray()")
	
	SCRIPT_ASSERT("invalid eFamilyEvent in PRIVATE_Update_Family_SynchSceneArray()")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_SynchSceneArrayMatch(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		TEXT_LABEL_63 &tSynchDict, TEXT_LABEL_63 &tSynchClip, 
		TEXT_LABEL_63 tMatchClip, STRING sMatchTail,
		INT &iScene)
	TEXT_LABEL_63 tFamilySeqSyncDicts[6], tFamilySeqSyncClips[6]
	INT iFamilySynchs
	FLOAT blendInDelta, blendOutDelta
	ANIMATION_FLAGS eFamilySyncFlag
	
	VECTOR vSynchSceneOffset
	FLOAT fSynchSceneHead
	
	IF PRIVATE_Get_Family_AnimArray(eFamilyMember, eFamilyEvent,
			tFamilySeqSyncDicts, tFamilySeqSyncClips, iFamilySynchs,
			blendInDelta, blendOutDelta, eFamilySyncFlag)
	AND PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS SynchSceneHudColour = HUD_COLOUR_PINK
		#ENDIF
		
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_USE_PHYSICS
		RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_PLAYER_IMPACT
		FLOAT moverBlendInDelta = INSTANT_BLEND_IN
		
		IK_CONTROL_FLAGS ikFlags = AIK_NONE
		PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(eFamilyMember, eFamilyEvent, ikFlags)
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilySyncFlag, AF_NOT_INTERRUPTABLE)
			SET_BITMASK_ENUM_AS_ENUM(flags, SYNCED_SCENE_DONT_INTERRUPT)
		ENDIF
		
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
//			
//			IF (iScene > 0)
//				DETACH_SYNCHRONIZED_SCENE(iScene)
//				iScene = -1
//			ENDIF
//			
//			iScene = CREATE_SYNCHRONIZED_SCENE(vFamilyScenePos+vSynchSceneOffset, <<0,0,fFamilySceneHead+fSynchSceneHead>>)
//			
//			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, loop)
//			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, holdLastFrame)
//			
//			PRIVATE_TaskSynchSceneArray(PedIndex, iScene,
//					tFamilySeqSyncDicts,  tFamilySeqSyncClips, iFamilySynchs,
//					tSynchDict, tSynchClip,
//					blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
//			
			#IF IS_DEBUG_BUILD
			str  = ("creating SynchScene \"")
			str += (tSynchClip)
			str += ("\" ")
			str += (iScene)
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tSynchDict, tSynchClip, ANIM_SYNCED_SCENE)
		OR GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.001
			PRIVATE_TaskSynchSceneArrayMatch(PedIndex, iScene,
					tFamilySeqSyncDicts,  tFamilySeqSyncClips, iFamilySynchs,
					tSynchDict, tSynchClip, 
					tMatchClip, sMatchTail,
					blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
			
			#IF IS_DEBUG_BUILD
			str  = ("tasking SynchScene \"")
			
			INT iTaunterAnimOutLength
			iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tSynchClip)
			
			IF iTaunterAnimOutLength >= 15
				str += GET_STRING_FROM_STRING(tSynchClip,
						iTaunterAnimOutLength - 15,
						iTaunterAnimOutLength)
			ELSE
				str += tSynchClip
			ENDIF
			
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		str  = ("SynchMatch[] \"")
		str += (tSynchDict)
		str += ("\" id:")
		str += iScene
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
		
		str  = (tSynchClip)
		str += (":")
		str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, SynchSceneHudColour)
		
		str  = ("match: \"")
		str += tMatchClip
		str += ("\"")
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_GREEN)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_SynchSceneArrayMatch()")
	
	SCRIPT_ASSERT("invalid eFamilyEvent in PRIVATE_Update_Family_SynchSceneArrayMatch()")
	#ENDIF
	
	RETURN FALSE
ENDFUNC
