USING "mission_control_public.sch"
USING "commands_clock.sch"
USING "shared_debug_text.sch"
USING "Commands_Brains.sch"
USING "flow_public_core_override.sch"
#if USE_CLF_DLC
USING "player_scene_scheduleCLF.sch"	
#endif
#if  USE_NRM_DLC
USING "player_scene_scheduleNRM.sch"
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "player_scene_schedule.sch"
#endif
#endif
USING "cellphone_public.sch"
USING "script_clock.sch"
USING "commands_stats.sch"
USING "candidate_public.sch"

#IF IS_DEBUG_BUILD
USING "completionpercentage_public.sch"
#ENDIF

//////////////////////////////////////
/// **** AMBIENT SCRIPT CHECKS **** ///
//////////////////////////////////////


ENUM AMBIENT_ALLOW_TO_RUN
	RUN_ON_MISSION_ALWAYS,
	RUN_ON_ACTIVITY_ALWAYS,
	RUN_ON_MISSION_IF_SPACE,
	RUN_ON_MISSION_NEVER
ENDENUM

PROC SUPPRESS_PLAYERS_CAR()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), TRUE)
		ENDIF
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF
ENDPROC

PROC RESET_COMPLETION_FOR_THIS_SCRIPT()
	
ENDPROC

PROC RESET_RANDOM_EVENT_LAUNCH_TIMER()
	ADD_GLOBAL_COMMUNICATION_DELAY(CC_GLOBAL_DELAY_POST_MISSION)
	Mission_Over(g_iRECandidateID)
	PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), " RESET_RANDOM_EVENT_LAUNCH_TIMER()")
	g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER()
	#IF IS_DEBUG_BUILD
	g_sLastRandomEventScript = "NULL"
	#ENDIF
ENDPROC

//#IF IS_DEBUG_BUILD
//PROC SaveNewGameToAmbienceLog()
//	
//	STRING sFile = "ambience.log"
//	STRING sPath = "X:/gta5/build/dev/"
//	
////	OPEN_NAMED_DEBUG_FILE(sPath, sFile)
//	
//	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
//	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
//	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_THIS_SCRIPT_NAME(), sPath, sFile)
//	SAVE_STRING_TO_NAMED_DEBUG_FILE(" is the selected event. Found ", sPath, sFile)
//	SAVE_STRING_TO_NAMED_DEBUG_FILE(g_sLastRandomEventScript, sPath, sFile)
//	SAVE_STRING_TO_NAMED_DEBUG_FILE(". SUCCESS!", sPath, sFile)
//	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
//	
////	CLOSE_DEBUG_FILE()
//	
//ENDPROC
//#ENDIF

FUNC BOOL CAN_RE_RUN_THROUGH_CANDIDATE_SYSTEM(BOOL &bReturnDenied)
	SWITCH Request_Mission_Launch(g_iRECandidateID,MCTID_MUST_LAUNCH , MISSION_TYPE_RANDOM_EVENT)//MCTID_MEET_CHARACTER
		CASE MCRET_ACCEPTED
			PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), " - Request_Mission_Launch is MCRET_ACCEPTED.")
			RETURN TRUE
		BREAK
		CASE MCRET_DENIED
			PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), " - Request_Mission_Launch is MCRET_DENIED.")
			IF NOT bReturnDenied
				bReturnDenied = TRUE
			ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), " is awaiting Request_Mission_Launch.")
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks current timeofday against a datestamp. If difference is >= iDelay the return true and store the current timeofday. iDelay is hours. bIgnoreLaunchTimer will skip timestamp checks.
FUNC BOOL HAVE_ENOUGH_DAYS_PASSED_SINCE_LAST_RUN(INT &iDayStamp, INT iDelay = 10, BOOL bIgnoreLaunchTimer = FALSE)

	CONST_INT ci_DelayBetweenAllowedLaunches 60000
	
	TIMEOFDAY tempTime
	INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
	
	BOOL bExit = FALSE
	
	#IF IS_DEBUG_BUILD
	
	// This section allows any event selected from XML to start without altering the datestamp
	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_KEY(g_sLastRandomEventScript)
	AND NOT GET_RANDOM_EVENT_FLAG()
	
		PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), " is the selected event. Found ", g_sLastRandomEventScript, ". SUCCESS!")
		SET_RANDOM_EVENT_FLAG(TRUE)
		WHILE NOT CAN_RE_RUN_THROUGH_CANDIDATE_SYSTEM(bExit)
		WAIT(0)
		ENDWHILE
		
		IF bExit
			RETURN FALSE
		ENDIF
		
		g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER() 
		g_sLastRandomEventScript = "NULL" // Reset debug script string.
		
		tempTime = GET_CURRENT_TIMEOFDAY()
		iDayStamp = ENUM_TO_INT(tempTime)
		
		IF g_bDontResetCompletionOnDebugLaunch
			PRINTLN("Random Event Debug: g_bDontResetCompletionOnDebugLaunch has been set, ", GET_THIS_SCRIPT_NAME(), " variation may already be complete.")
		ELSE
			RESET_COMPLETION_FOR_THIS_SCRIPT()
		ENDIF
		
		//SaveNewGameToAmbienceLog()
		
		SUPPRESS_PLAYERS_CAR()
		RETURN TRUE
		
	ELIF NOT ARE_STRINGS_EQUAL(g_sLastRandomEventScript, "NULL")
	
		// If g_sLastRandomEventScript is set to anything other than NULL then this is the wrong script.
		PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), " is not the selected event, or the random event flag is already set. Expecting ", g_sLastRandomEventScript)
		RETURN FALSE
		
	ENDIF
	
	#ENDIF
	
	IF IS_CURRENT_PLAYER_IN_SLEEP_MODE() 
		PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". New random event can't run if the player is in sleep.")
		RETURN FALSE
	ENDIF
	
	IF GET_MISSION_FLAG()
		PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". New random event can't run if there's a mission running.")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_RANDOM_EVENT)
		PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". New random event can't run if there's a mission running. Checked against CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE")
		RETURN FALSE
	ENDIF
	
	IF GET_RANDOM_EVENT_FLAG()
		PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". New random event can't run if there's already one running.")
		RETURN FALSE
	ENDIF
	
	IF NOT bIgnoreLaunchTimer
	
		IF GET_GAME_TIMER() < (g_iLastSuccessfulAmbientLaunchTime + ci_DelayBetweenAllowedLaunches)
			PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". Not enough time has passed since last successful RE launch. ", (GET_GAME_TIMER() - (g_iLastSuccessfulAmbientLaunchTime + ci_DelayBetweenAllowedLaunches)), "ms until next allowed launch.")
			RETURN FALSE
		ENDIF
		
		IF NOT g_bHoldRandomEventForSelection
		
			GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(INT_TO_ENUM(TIMEOFDAY, iDayStamp) , iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
			
			IF iHours >= iDelay
				PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". Event can run. There have been ", iHours, " game hours since last launch.")
			ELSE
				IF iDays = 0
					PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". Delay is insufficient. Only ", iHours, " game hours have passed since last launch. ", (iDelay-iHours), " to go.")
					RETURN FALSE
				ELSE
					iHours+=(iDays*24)
					IF iHours >= iDelay
						PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". Event can run. There have been ", iHours, " game hours since last launch.")
					ELSE
						PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". Delay is insufficient. Only ", iHours, " game hours have passed since last launch. ", (iDelay-iHours), " to go.")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	enumCharacterList currentCharID
	REPEAT NUM_OF_PLAYABLE_PEDS currentCharID
		VECTOR vLastKnownCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[currentCharID]
		TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[currentCharID]
		IF Is_TIMEOFDAY_Valid(sLastTimeActive)
			IF HasHourPassedSinceCharLastTimeActive(currentCharID)
				IF NOT ARE_VECTORS_EQUAL(vLastKnownCoords, <<0,0,0>>)
					IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vLastKnownCoords) < (210.0*210.0))
						IF GET_CURRENT_PLAYER_PED_ENUM() <> currentCharID
							tempTime = GET_CURRENT_TIMEOFDAY() // Ensures event isn't re-run straight after being skipped.
							iDayStamp = ENUM_TO_INT(tempTime)
							
							PRINTLN("Random Event Debug: ", GET_THIS_SCRIPT_NAME(), ". Player is near last known coords of another playable ped, random event should not run.")
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	WHILE NOT CAN_RE_RUN_THROUGH_CANDIDATE_SYSTEM(bExit)
	WAIT(0)
	ENDWHILE
	
	IF bExit
		RETURN FALSE
	ENDIF
	
	// Everything is good, script can continue.
	
	g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER() // Stores that time a RE was last successfully run.
	
	tempTime = GET_CURRENT_TIMEOFDAY() // Stores that last successful run date.
	iDayStamp = ENUM_TO_INT(tempTime)
	
	IF NOT g_bMagDemoActive
//		#IF IS_DEBUG_BUILD
//		SaveNewGameToAmbienceLog()
//		#ENDIF
		
		SET_RANDOM_EVENT_FLAG(TRUE)
	ENDIF
	
	SUPPRESS_PLAYERS_CAR()
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(AMBIENT_ALLOW_TO_RUN run_on_mission)

	SWITCH run_on_mission
	
		CASE RUN_ON_MISSION_ALWAYS //Scripts that should be running all the time i.e. barriers, toll booths etc or ones that are very light on assets. 
			RETURN TRUE
		BREAK
		
		CASE RUN_ON_ACTIVITY_ALWAYS //Scripts that should be running all the time i.e. barriers, toll booths etc or ones that are very light on assets. 
			//IF SCRIPT_MEMORY_USAGE < 16
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
				RETURN TRUE
			ELSE
				RETURN IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
			ENDIF
		BREAK
		
		CASE RUN_ON_MISSION_IF_SPACE //more asset heavy scripts. Can be active during missions if the memory allows.
			//IF SCRIPT_MEMORY_USAGE < 16
				RETURN TRUE
			//ENDIF
		BREAK
		
		CASE RUN_ON_MISSION_NEVER //scripts we never want to be active on missions and will only activate if the memory allows.
			//IF SCRIPT_MEMORY_USAGE < 16
			
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_RANDOM_EVENT)
				IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_RANDOM_EVENT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

	ENDSWITCH
	
	RETURN FALSE
			
ENDFUNC

/// PURPOSE:
///    At the start of the ambient script, checks if the script should create anything
/// PARAMS:
///    run_on_mission - Tells the proc how to handle missions. 
/// RETURNS:
///    
FUNC BOOL IS_AMBIENT_SAFE_TO_RUN(AMBIENT_ALLOW_TO_RUN run_on_mission)

	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(run_on_mission)
			RETURN TRUE
		ENDIF
	ENDIF
	
	
RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Is the player close enough to the action to start moving properly
/// PARAMS:
///    aPed - The star ped of the ambient event
///    Radius - The radius the script should start properly at from the player
/// RETURNS:
///    
FUNC BOOL IS_PED_EVENT_ACTIVATED(PED_INDEX aPed, FLOAT Radius)

	IF NOT IS_ENTITY_DEAD(aPed)
	VECTOR PedPos =	GET_ENTITY_COORDS(aPed)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), PedPos, <<Radius, Radius,Radius>>)
			IF IS_ENTITY_VISIBLE(aPed)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Is the player close enough to the action to start moving properly
/// PARAMS:
///    aCoord - The coordinate of the action
///    Radius - The radius the script should start properly at from the player
/// RETURNS:
///    
FUNC BOOL IS_COORD_EVENT_ACTIVATED(VECTOR aCoord, FLOAT Radius, BOOL DontCareIfSeen = FALSE)

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), aCoord, <<Radius, Radius,Radius>>)
		IF DontCareIfSeen = FALSE
			IF IS_SPHERE_VISIBLE(aCoord, 5) 
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks the player's vehicle against known taxi models.
///    Returns TRUE if player is in a taxi model but not driving.
FUNC BOOL IS_PLAYER_A_TAXI_PASSENGER()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TAXI)
			IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) <> PLAYER_PED_ID()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs various checks to make sure it is safe for the player to start a scripted cutscene.
/// PARAMS:
///    DoDeathChecks - Do the death checks as part of this command?
/// RETURNS:
///    BOOL         TRUE if it is safe to start the cutscene, otherwise FALSE
/// NOTES:
///    Also calls the code commands: IS_PLAYER_READY_FOR_CUTSCENE() and CAN_PLAYER_START_MISSION()
///    Unlike CAN_PLAYER_START_CUTSCENE, does not force the phone away.
FUNC BOOL CAN_PLAYER_START_CUTSCENE_IGNORE_PHONE(BOOL DoDeathChecks = FALSE)

    IF (IS_MINIGAME_IN_PROGRESS())
        RETURN FALSE
    ENDIF
    
	
	IF DoDeathChecks
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF

	ENDIF

    
    // vehicle checks
    VEHICLE_INDEX theCar = NULL


    IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
        // If the player is not sitting in the vehicle, don't start the cutscene
        IF NOT (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
            RETURN FALSE
        ENDIF
        
        // If the player is not the driver then he must be a passenger, don't start the cutscene
        theCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DoDeathChecks
			IF IS_ENTITY_DEAD(theCar)
				RETURN FALSE
			ENDIF
		ENDIF
        IF NOT (IS_VEHICLE_DRIVEABLE(theCar))
            IF (GET_PED_IN_VEHICLE_SEAT(theCar, VS_DRIVER) != PLAYER_PED_ID())
                RETURN FALSE
            ENDIF
        ENDIF

        // Unless the vehicle is pretty much upright, don't start the cutscene
        CONST_FLOAT MINIMUM_UPRIGHT_VALUE   0.950
        CONST_FLOAT MAXIMUM_UPRIGHT_VALUE   1.011
        
        IF NOT (IS_VEHICLE_DRIVEABLE(theCar))
            IF (GET_ENTITY_UPRIGHT_VALUE(theCar) < MINIMUM_UPRIGHT_VALUE)
            OR (GET_ENTITY_UPRIGHT_VALUE(theCar) > MAXIMUM_UPRIGHT_VALUE)
                RETURN FALSE
            ENDIF
        ENDIF
    ENDIF


    IF NOT (IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()))
        RETURN FALSE 
    ENDIF


    IF NOT (CAN_PLAYER_START_MISSION(PLAYER_ID()))
        RETURN FALSE 
    ENDIF
    
    
    // Everything ok
    RETURN TRUE

ENDFUNC
